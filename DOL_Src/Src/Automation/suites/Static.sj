/******************************************************************************* 
  Short description:  
   Static_links
   
   Script click on all possible links and verify pages displaying.

  Author:  Marina Ryabova
  
  Change list:
    + 28.10.2005 - created
  
*******************************************************************************/ 
//USEUNIT std_include 


function Static() {

   TestSuite.call( this );
   
   this.suiteIni = goHome; 

   this.testLinks  = Links;
   
}

   
function Links() {

   TS_ASSERT_ISPASS(); 
   
    presslink_verifypage(OVERVIEW,LBL_OVERVIEW);       
    presslink_verifypage(HOME,LBL_FAVOR);
    
    presslink_verifypage(HELP,LBL_HELP);
    presslink_verifypage(HOME,LBL_FAVOR );
       
    presslink_verifypage(SITE_MAP,LBL_SITEMAP);
    presslink_verifypage_SM(HOME,LBL_FAVOR ); 
   
    presslink_verifypage(FRIEND, LBL_FRIEND);
    presslink_verifypage(HOME,LBL_FAVOR );
    
/*
script do not work now    
    pressLink(COMPANY);
    var bg_wnd;
    bg_wnd = Sys.Process(IE).Window( IE_WNDCLASS, BG_PAGE);
    bg_wnd.Activate();
    WND = bg_wnd; 
    Assert.IsNotFail(objFind(BG_SITE_HOME)); 
    bg_wnd.Close();    
    WND = Sys.Process( IE ).Window( IE_WNDCLASS, COMMON_CAPTION ); 
    WND.Activate();        
    PAGE = WND.Page( COMMON_CAPTION ); 
*/
             
   END();

}



 






