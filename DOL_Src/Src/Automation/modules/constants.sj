/******************************************************************************* 
  Short description: 
    Controls and constants
    
  Description:
      
  Author:
  
  Change list:
  
*******************************************************************************/

//******************************Static links******************************

var OVERVIEW = "OVERVIEW";
var HOME     = "Home";
var SITE_MAP = "SITE MAP";
var HELP     = "HELP";
var COMPANY  = "COMPANY";
var FRIEND   = "TELL A FRIEND";

//************************************************************************


//***************************PAGES LABELS**************************************

 var LBL_OVERVIEW = "lblOverview";
 var LBL_HELP     = "lblHelp";
 var LBL_SITEMAP  = "lblSiteMap";
 var LBL_FRIEND   = "lblTellAFriend";
 var LBL_SELECTMODEL = "ctlHeader_lblStep";
 
//******************************************************************************

//************************Body Glove site****************************************
 var BG_SITE_HOME = "galleries";  
 var BG_PAGE = "Fellowes - Prsonalize Your Device - Microsoft Internet Explorer provided by StarSoft Development Labs, Inc.";
 
 var FELL_PAGE = "Fellowes - Microsoft Internet Explorer provided by StarSoft Development Labs, Inc."
 var FELL_SITE_HOME = "";  
 
//******************************************************************************

//*************************Site Map Page links*********************************************
 var SM_OVERVIEW = "Overview";
 var SM_FRIEND = "Tell a Friend";
 var SM_SITEMAP = "Site Map"; 
 var SM_BODYGLOVE = "Body Glove";
 var SM_FELLOWES  = "Fellowes";
//***********************************************************************************


//***********************Home page****************************************************    
    
var LOG_EMAIL    = "txtLogin";
var LOG_PASSWORD = "txtPassword";
var LNK_ENTER    = "Enter";
var LNK_LOGOUT   = "Log Out"; 
var LBL_USERNAME = "lblUserName";
var LNK_NEWUSER  = "I am a new user";
var LNK_MODIFY   = "Modify My Profile";

var LBL_SELECDEV = "lblSelectDevicesHeader";
var LBL_FAVOR    = "lblFavorites";  

var LBL_FAVORITS = "MY FAVORITES";
var SELECT_PHONE = "Cell Phone"; 

//***********************Select Model page********************************************

var LBL_CURRSTEP = "lblCurrentStep"; 

var BTN_SEL_BY_CARRIER = "Select by Carrier";
var BTN_SEL_BY_BRAND   = "Select by Brand";
var BTN_SEL_DEVICE     = "Select Another Device";

var LBL_INSTRUCTIONS   = "Instructions:";
var LBL_SEL_BY_CARRIER = "Select by Carrier:";
var LBL_SEL_BY_BRAND   = "Select by Brand:";  
var LBL_SEL_DEVICE     = "Select Device:";

//***********************************************************************************

//****************************Test data************************************************
TEST_MAIL              = "test@test.com";
TEST_FNAME             = "CPT First";
TEST_LNAME             = "CPT Last"; 
TEST_PASSWORD          = "psw";
TEST_EMAIL_OPTION      = "T";
TEST_INFO              = "0"; 
TEST_CHECKED           = "checked";
TEST_UNCHECKED         = "1"; 
//***********************************************************************************  


//*************************Customer Account********************************************

EDT_FNAME                       = "txtFirstName";
EDT_LNAME                       = "txtLastName";
EDT_EMAIL                       = "txtEmailAddress";
EDT_OLD_PASS                    = "txtOldPassword";
EDT_PASSWORD                    = "txtNewPassword";
EDT_CONFIRM                     = "txtConfirmPassword";
CHK_RECEIVE                     = "chkReceiveInfo";
BTN_CANCEL                      = "btnCancel";
BTN_SUBMIT                      = "btnOK";
LBL_CUST_ACCOUNT                = "lblCustomerAccount";

VLD_OLD_PASSWORD                = "Old Password is required";
VLD_NEW_PASSWORD                = "New Password is required";
VLD_CONF_PASSWORD               = "Confirm Password is required";


//*********************** HTML Control classes **************************************

var CLS_BUTTON_DISABLED = "ButtonSelectDisabled";


//**********************************************************************************                            
