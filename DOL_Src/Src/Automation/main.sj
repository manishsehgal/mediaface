/******************************************************************************* 
  Short description: 
    MAIN
    
  Desription:
    Here you can add new suites by registering them with suiteRunner object.
    
    NOTE: Autocomplete in IE should be turned off.

  Author: 
  
  Change list:
    + <date> - created
  
*******************************************************************************/ 

//USEUNIT suite_runner

//USEUNIT Static
//USEUNIT Site_map
//USEUNIT Cust_log
//USEUNIT ModelSel

function main( ) { 
            
   suiteRunner.addSuite( new Static(), "Static" );
   suiteRunner.addSuite( new Site_map(), "Site_map" );    
   suiteRunner.addSuite( new Cust_log(), "Cust_log");
   suiteRunner.addSuite( new ModelSel(), "ModelSelect");  
   suiteRunner.runWorld( );
}




