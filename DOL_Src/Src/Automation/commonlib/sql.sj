/**********************************************************************************
  Module: SQL
  Short Description: this functions are used to append one or more parameters to Stored Procs and execute SP.
  (see sample at the bottom of this document)
        
  
  Detailed Description:         

  Author:  Michael Borisov, Natalie Voronko, Philip Gounbin

  Change list:
    +19.05.2005 created 
    +01.07.2005  NV - modified: _createCommand modified - adCmdStoredProc hard coded 
                                (since the only command we are allowed to use
                                is execute, other command types can be missed and   
! Please mind specifing connection variable in globvars !!!! 
   + NV, MV:  sample with detailed description  added 
  
**********************************************************************************/

//USEUNIT globvars
//USEUNIT commonlib
//USEUNIT asserts
//USEUNIT debug
//USEUNIT suite_runner
//USEUNIT test_suite
//USEUNIT trace
//USEUNIT std_message

var connection;  

function createSQLConnection( ) {
   connection = ADO.CreateConnection();
   connection.ConnectionString = CONN_STR;
   connection.Open();
   return connection;
}

function closeSQLConnection( ) {
  if(connection != null) 
      connection.Close( ); 
  
}

/* Command Type
      adCmdFile = 0x100,
      // since the only command we are allowed to use is adCmdStoredProc, other command types can be missed
      adCmdStoredProc = 4,
      adCmdTable = 2,
      adCmdTableDirect = 0x200,
      adCmdText = 1,
      adCmdUnknown = 8,
      [TypeLibVar((short) 0x40)]
      adCmdUnspecified = -1
*/

function _createCommand(text) {    
  try {  
      command = ADO.CreateCommand();
      command.ActiveConnection = connection;
      command.CommandType = adCmdStoredProc;
      command.CommandText = text;  
      command.NamedParameters = true; 
      return command;
 } catch (exception) { 
      Log.Error ("Failed creation command: "+command.CommandText+"\n"+exception.description);
      return null;
   }
}

function _executeCommand(command, returnRowsAffected) {
    returnRowsAffected = returnRowsAffected || false;
    var rowsAffected;
    var recordset;
    try {
        recordset = command.Execute(rowsAffected);
    } catch( ex ) {
        traceFail( "Cannot execute SQL query: " + ex.description );
        return null;
    }
    if (returnRowsAffected) return rowsAffected;
    if (( recordset.State == 0 ) || ( recordset.EOF )) return recordset;
    recordset.MoveFirst();
    return recordset;
}

function _assertOneRow(recordset) {
    if ( assertOneRow ) Assert.AreEqual( RecSet.RecordCount, 1, "Record Set must contain one row" );
}

/* Parameter Type
      adArray = 0x2000,
      adBigInt = 20,
      adBinary = 0x80,
      adBoolean = 11,
      adBSTR = 8,
      adChapter = 0x88,
      adChar = 0x81,
      adCurrency = 6,
      adDate = 7,
      adDBDate = 0x85,
      adDBTime = 0x86,
      adDBTimeStamp = 0x87,
      adDecimal = 14,
      adDouble = 5,
      adEmpty = 0,
      adError = 10,
      adFileTime = 0x40,
      adGUID = 0x48,
      adIDispatch = 9,
      adInteger = 3,
      adIUnknown = 13,
      adLongVarBinary = 0xcd,
      adLongVarChar = 0xc9,
      adLongVarWChar = 0xcb,
      adNumeric = 0x83,
      adPropVariant = 0x8a,
      adSingle = 4,
      adSmallInt = 2,
      adTinyInt = 0x10,
      adUnsignedBigInt = 0x15,
      adUnsignedInt = 0x13,
      adUnsignedSmallInt = 0x12,
      adUnsignedTinyInt = 0x11,
      adUserDefined = 0x84,
      adVarBinary = 0xcc,
      adVarChar = 200,
      adVariant = 12,
      adVarNumeric = 0x8b,
      adVarWChar = 0xca,
      adWChar = 130
*/

/* Parameter Direction
      adParamInput = 1,
      adParamInputOutput = 3,
      adParamOutput = 2,
      adParamReturnValue = 4,
      adParamUnknown = 0
*/

function _addParameter(command, name, value, type, direction) {

    direction = direction || adParamInput;
    
    parameter = command.CreateParameter(name, type, direction);
    parameter.Value = value;
    command.Parameters.Append(parameter);
}

// this is just the same as _addParameter ( see above)  but type is hardcoded as dVarWChar and
// the direction is adParamInput

function _addStringParameter(command, name, value) {
    parameter = command.CreateParameter(name, adVarWChar, adParamInput, value.length, value);
    command.Parameters.Append(parameter);
}

// other sql function that havn't been changed 

function getFieldValue( recordset, fieldName ) {
   return recordset.Fields( fieldName ).Value;
}

function _getLastId( ) {
   return _getRecSet( "SELECT @@IDENTITY AS 'id'" ).Fields( "id" ).Value;
}

function _getRecSet( query, doAssert ) {
   if ( doAssert == null ) doAssert = true;

   try {
      var RecSet = conn.Execute_( query );
      if ( doAssert ) Assert.AreEqual( RecSet.RecordCount, 1, "Record Set contains one row" );
      if ( ( RecSet.State == 0 ) || ( RecSet.EOF ) ) return null;
      RecSet.MoveFirst();
      return RecSet;
   } catch( e ) {
      traceFail( "Cannot execute SQL query: " + e.description );
      return null;
   }
}   

function _execQuery( query ) {
   return _getRecSet( query, false );
}

function getField( recordSet, fieldName ) {
   return recordSet ? recordSet.Fields( fieldName ).Value : null;
}

// sample: append paranters and execute your command; see more details below  

function sampleFunction(parameter1Value,parameter2Value){

var command;
    try {        
      command = _createCommand(sp_name);
      _addStringParameter(command, "@parameter1", parameter1Value);
      _addStringParameter(command, "@parameter2", parameter2Value);                  
      return _executeCommand(command);
      
    } catch (ex) {
       traceFail("failed sampleFunction") 
     };    
   
}

// catch block is not nesassary for  _createCommand, _executeCommand functions provide error audit themselves
// now you can parse you recordset or check for errors.
// accodign to Ilya Gavrilov's, Intel wants us to open and close connection only for execution, not for while test run. 
// this way we can just addd open createSQLConnection() at the beginning and close it in finally block



