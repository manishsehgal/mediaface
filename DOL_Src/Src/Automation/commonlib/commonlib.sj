//USEUNIT globvars
//USEUNIT std_message

//USEUNIT asserts
//USEUNIT trace

var _CTLNAME = new ControlName( );
var _OBJECT  = new DecoratedObject( );

var EXCEPTION_DISPLAY_INDEX = 71;
   
PASS = new DecoratedObject( );
FAIL = new DecoratedObject( );

function ControlName( ) {   
   this.set = controlName_set;
   this.get = controlName_get;
   
   this.name;   
   
   function controlName_set( ctlName ) { this.name = ctlName; }
   function controlName_get( ) { return this.name; }
}

function DecoratedObject( ) {
   this.set   = decoratedObject_set;
   this.get   = decoratedObject_get;
   this.make  = decoratedObject_make;
   this.store = decoratedObject_store;
   
   this.makeSetProperty = decoratedObject_makeSetProperty;
   this.makeGetProperty = decoratedObject_makeGetProperty;
   this.makeExecuteMethod = decoratedObject_makeExecuteMethod;
   this.getPropName = decoratedObject_getPropName;
   this.getToken = decoratedObject_getToken;
      
   this.control = _CTLNAME;
   this.args = null;
            
   function decoratedObject_set() {  
      this.args = arguments;
      this.makeSetProperty();
   }
   
   function decoratedObject_get() { 
      this.args = arguments;
      return this.makeGetProperty();
   }
   
   function decoratedObject_make( methodname ) {
      this.args = arguments;
      return this.makeExecuteMethod( methodname );
   }
   
   function decoratedObject_store( obj ) {      
      this.obj = obj;       
      return this;
   }

   function decoratedObject_makeSetProperty() {
      var propName = this.getPropName();
      
      try {                       
         eval( propName + " = " + "\"" + this.args[ this.args.length - 1 ] + "\"" );
      } catch ( ex ) { THROW( MSG_NO_SUCH_PROPERTY( this.control.get( ), propName ) ); }
   }
   
   function decoratedObject_makeGetProperty( /* arguments */ ) {
      var ret = null;     
      var propName = this.getPropName() + this.getToken( this.args[ this.args.length - 1 ] );
      
      try {                       
         if ( ( ret = eval( propName ) ) == "undefined" ) throw "dummy";            
      } catch ( ex ) { 
         THROW( MSG_NO_SUCH_PROPERTY( this.control.get( ), propName ) ); 
         ret = FAIL;
      }
      return ret;      
   }

   function decoratedObject_getPropName() {
      var expr = "";
      for ( var i = 0; i < this.args.length - 1; i++ ) expr += this.getToken( this.args[i] );
      return "this.obj" + ( ( expr == "" ) ? "" : expr );
   }
       

   function decoratedObject_getToken( arg ) {
      var expr = null;
      switch ( typeof( arg ) ) {
      case "number":
         expr = "(" + arg + ")";
         break;
      case "string":
         expr = "." + arg;
         break;
      }         
      return expr;
   }

   
   function decoratedObject_makeExecuteMethod( name ) {
      var arglist = "";      
      var ret = null;
      
      for ( var i = 1; i < this.args.length; i++ ) {
         var added = this.args[i];
         if ( typeof( added ) == "string" ) added = Utilities.QuotedStr( added );
         arglist += ( i == this.args.length - 1 ) ? added : added + ",";
      }
         
      try { ret = eval( "this.obj." + name + "(" + arglist + ")" ) } 
      catch ( ex ) { 
         THROW( MSG_INVALID_METHOD( this.control.get( ), name ) ); 
         ret = FAIL;
      }
            
      return ret;   
   }   
}

function THROW( message ) {
   if ( isExecuting( _process ) ) throw message;
   else traceFail( FORMAT_MSG( message ) );    
}


/** Check selected item in combo-box
    @param cboName Name of check box
    @param itemName Item
    @return PASS / FAIL
    @author Alexander Zakshevsky
*/            
function cmpSelected( cboName, itemName ) { 
   var _cboName = cboName, _itemName = itemName;

   return _process( function _cmpSelected( ) { 
      
   index = ( cbo = objFind( _cboName ) ).get( "selectedIndex" );
   if ( cbo.get( index, "text" ) != _itemName ) {
      traceDebug( MSG_SELECTED_ITEM_ARE_NOT_EQUAL( _cboName, _itemName ) );      
      return FAIL;
   } else {
      traceDebug( MSG_SELECTED_ITEM_ARE_EQUAL( _cboName, _itemName ) );      
      return PASS;
   }      
      
   } ); 
}

/** Check 'value' property of edit field
    @param edtName Name of edit field
    @param text Text
    @param tag (optional) Tag which is compared. The default is "value".
    @return PASS / FAIL
    @author Alexander Zakshevsky
*/            
function cmpText( edtName, text, tag ) {
   var _edtName = edtName, _text = text, _tag = ( arguments.length < 3 ) ? "value" : tag;

   return _process( function _cmpText( ) {
   
   if ( objFind( _edtName ).get( _tag ) != _text ) {
      traceDebug( MSG_TEXT_NOT_EQUALS( _edtName, _text ) );      
      return FAIL;
   } else {
      traceDebug( MSG_TEXT_EQUALS( _edtName, _text ) );      
      return PASS;
   }      
   
   } );
}

/** Check 'innerText' property of edit field
    @param edtName Name of edit field
    @param text Text
    @return PASS / FAIL
    @author Alexander Zakshevsky
*/            
function cmpInnerText( edtName, text, doTrim ) {
   var _edtName = edtName, _text = text, _doTrim = doTrim;
   if ( arguments.length < 3 ) _doTrim = false;

   return _process( function _cmpInnerText( ) {
   
   var text = objFind( _edtName ).get( "innerText" );
   if ( doTrim ) text = trim( text );
   
   if ( text != _text ) {
      traceDebug( MSG_TEXT_NOT_EQUALS( _edtName, _text ) );      
      return FAIL;
   } else {
      traceDebug( MSG_TEXT_EQUALS( _edtName, _text ) );      
      return PASS;
   }      
   
   } );
}



/** Press link
    @param linkName Link name
    @param wait (optional) true/false. The default is "true".
    @param tag (optional) tag. The default is "A".  
    @return PASS / FAIL
    @author Alexander Zakshevsky
*/            
function pressLink( linkName, wait, tag ) {
   var _linkName = linkName, _wait, _tag;
   
   switch( typeof( wait ) ) {
   case "boolean" : break;
   case "string"  : tag = wait; wait = null;
   }   
   _wait = ( wait == null ) ? true : wait;
   _tag  = ( !tag ) ? "A" : tag;

   return _process( function _pressLink( ) { 
   
   objFindByProperty( "innerText", _linkName, "tagName", _tag ).make( "click", 5, 5 ); 
   traceDebug( MSG_LINK_PRESSED( _linkName ) );
      
   }, MAKE_REFRESH=_wait );                                             
}


/** Press a button
    @param btnName Button name
    @param wait (optional) true/false. The default is "true".
    @return PASS / FAIL
    @author Alexander Zakshevsky
*/            
function pressButton( btnName, wait ) {
   var _btnName = btnName, _wait = ( wait == null ) ? true : wait;

   return _process( function _pressButton( ) {   

   var btn = objFind( _btnName );
   if ( btn.get( "disabled" ) ) {
      traceDebug( MSG_CTRL_DISABLED( _btnName ) );         
      return FAIL;
   }
   
   btn.make( "click" );
   traceDebug( MSG_BUTTON_PRESSED( _btnName ) );    
   
   }, MAKE_REFRESH=_wait );   
}


/** Select item in combo-box by index or by text.
    @param cboName Button name
    @param item Item text or item number
    @param wait (optional) true/false. The default is "true".
    @return PASS / FAIL
    @author Philip Gounbin
*/            
function cboSelect( cboName, item, wait ) {
   var _cboName = cboName, _item = item, _wait = ( wait == null ) ? false : wait;
   
   return _process( function _cboSelect( ) {
   var cbo = objFind( _cboName );
   if ( cbo.get( "disabled" ) ) {
      traceDebug( MSG_CTRL_DISABLED( _cboName ) );         
      return FAIL;
   }
   
   var idx = -1;
   
   switch ( typeof( _item ) ) {
   case "number": 
      idx = _item; 
      break;
   case "string": 
      for ( var i = 0; i < cbo.get( "length" ); i++ ) {
         if ( cbo.get( i, "text" ) != _item ) continue;
         idx = i;
         break;
      }
      break;
   }
   
   if ( idx >= 0 ) {
      cbo.make( "click", idx );
      traceDebug( MSG_ITEM_SELECTED( _cboName, _item ) );      
      return PASS;
   }
   
   traceDebug( MSG_ITEM_NOT_SELECTED( _cboName, _item ) );         
   return FAIL;
   
   }, MAKE_REFRESH=_wait );   
}



/** Select multiple items in combo-box by text.
    @param name Button name
    @param str (optional) Items string of the follwing format: "item1" | "item2" | ...
      When "str" is null, all items of combo-box are selected.
    @param wait (optional) true/false. The default is "true".
    @return PASS / FAIL
    @author Philip Gounbin
*/            
function cboSelectMulti( name, str, wait ) {
   var _name = name, _str, _wait;
   
   switch ( typeof( str ) ) {
   case "boolean": _wait = str; _str = null; break;
   case "string": _str = str; _wait = ( wait == null ) ? false : wait; break;
   }

   return _process( function _cboSelectMulti( ) {
   var ctrl = objFind( _name );
   if ( ctrl.get( "disabled" ) ) {
      traceDebug( MSG_CTRL_DISABLED( _cboName ) );         
      return FAIL;
   }
   
   if ( _str == null ) {
      _str = "";
      for( i = 0; i < ctrl.get( "length" ); i++ ) _str += ctrl.get( i, "text" ) + "|"; 
   }
   
   ctrl.make( "multiselect", _str );
   }, MAKE_REFRESH=_wait );
}





/** Creates combo-box contents hash array
    @param cboname Combo-box name
    @return Items array / FAIL
    @author Philip Gounbin
*/            
function _getCboContent( cboname ) {
   var _cboname = cboname;

   return _process( function getCboContent( ) {
   
   var cbo = objFind( _cboname );
   var items = new Array( );
   
   for ( var i = 0; i < cbo.get( "length" ); i++ )
      items[ trim( cbo.get( i, "text" ) ) ] = true;

   return items;      
   
   } );
}


/** Creates grid contents hash array
    @param grdName Grid name
    @param colNum Number of column to work with
    @param filter (optional) Operation on text. Use it if you want to format text of a grid cell in a some way.
    @return Items array. Null if failed.
    @author Philip Gounbin
*/            
function _getGridContent( grdName, colNum, filter ) {
    if ( colNum == null ) return null;
    
    return _process( function getGridContent( ) {
    
    var grd = objFind( grdName );
    
    var rows = grd.get( "rows.length" );
    
    var items = new Array();
    for( var i = 1; i < rows; i++ ) {
        var text = trim( grd.get( "rows", i, "cells", colNum, "innerText" ) );
        if ( filter != null ) text = filter( text );
        items[ text ] = true;
    }
    return items;
    
    } );
}


function _verifyExpected( first, second, ctrl ) {
   return _doVerify( first, second, "expected", ctrl );        
}

function _verifyUnexpected( first, second, ctrl ) {
   return _doVerify( first, second, "unexpected", ctrl );
}

function _doVerify( cboMap, testArray, type, ctrl ) {
   if ( !testArray ) return true;
   
   var hasItem = false;

   for ( var i = 0; i < testArray.length; i++ ) {
      if ( cboMap[ testArray[ i ] ] ) hasItem = true;
      else hasItem = false;

      switch( type ) {
      case "expected"   : if ( hasItem ) break;
                          else {
                             traceDebug( MSG_ITEM_NOT_EXISTS( ctrl, testArray[ i ] ) );         
                             return false;
                          }
      case "unexpected" : if ( !hasItem ) break;
                          else {
                             traceDebug( MSG_ITEM_NOT_EXISTS( ctrl, testArray[ i ] ) );         
                             return false;                  
                          }
      }
   }
   
   return true;
}

/** Check combo-box contents
    @param cboname Combo-box name
    @param expected Array of items that should be in combo-box
    @param unexpected Array of items that shouldn't be in combo-box
    @return PASS / FAIL
    @author Philip Gounbin
*/            
function cmpCboData( cboname, expected, unexpected ) {
   var _expected = expected, _unexpected = unexpected;

   return _process( function _cmpCboData() {
   
   //get combo box content   
   var real = new Array( );
   var cbo = objFind( cboname );
   for ( var i = 0; i < cbo.get( "length" ); i++ )
      real[ trim( cbo.get( i, "text" ) ) ] = true;

   return ( _verifyExpected( real, _expected ) && _verifyUnexpected( real, _unexpected ) ) ? PASS : FAIL;
   
   } );
}


/** Creates grid contents.
    @param grdName Grid name
    @param colNum Number of column to work with
    @param expected Array of items that should be in combo-box
    @param unexpected Array of items that shouldn't be in combo-box
    @param filter (optional) Operation on text. Use it if you want to format text of a grid cell in a some way.
    @return PASS / FAIL
    @author Philip Gounbin
*/            
function cmpGridData( grdName, colNum, expected, unexpected, filter ) {
   var _expected = expected, _unexpected = unexpected;
   
   return _process( function _cmpGridData() {

   //get grid content   
   var real = new Array();
   var grd = objFind( grdName );
   for( var i = 1; i < grd.get( "rows.length" ); i++ ) {
       var text = trim( grd.get( "rows", i, "cells", colNum, "innerText" ) );
       if ( filter != null ) text = filter( text );
       real[ text ] = true;
   }

   if ( ( expected != null ) && !_verifyExpected( real, expected ) ) return FAIL;
   if ( ( unexpected != null ) && !_verifyUnexpected( real, unexpected ) ) return FAIL;
   return PASS;
   } );
}



function cmpGridRow( grdName, rowData, isExpected, rowNum ) {
   var _found = isExpected ? PASS : FAIL;
   var _notfound = isExpected ? FAIL : PASS;

   return _process( function _cmpGridRow() {
   
   if ( rowData == null ) return FAIL;
   var grd = objFind( grdName );
   var length = grd.get( "rows", 0, "cells", "length" );
   
   if ( rowNum != null ) {
      if ( _doCmpGridRow( grd.get( "rows", rowNum, "cells" ), rowData, length ) ) return _found;
      else return _notfound;
   }
   
   for ( var i = 1; i < grd.get( "rows", "length" ); i++ )
      if ( _doCmpGridRow( grd.get( "rows", i, "cells" ), rowData, length ) ) return _found;
   return _notfound;
   
   } );
} 

function _doCmpGridRow( rowObj, rowData, length ) {
   for ( var i = 0; i < length; i++ ) {
      if ( rowData[i] == null ) continue;
      if ( trim( rowObj( i ).innerText ) != rowData[i] ) return false;
   }
   return true;
}



/** Insert text into edit-field. 
    Do not use this function, use setSysText()!
    
    @param edtName Edit-field name
    @param text Text to insert
    @return PASS / FAIL
    @author Alexander Zakshevsky
*/            
function setText( edtName, text ) {
   var _edtName = edtName, _text = text;
   
   return _process( function _setText( ) { 
   
   objFind( edtName ).set( "innerText", text );       
   traceDebug( MSG_TEXT_SET( _edtName, text ) );
   
   } );
}

/** Insert text into edit-field. 
    @param edtName Edit-field name
    @param text Text to insert
    @param wait (optional) true/false. The default is "true".
    @return PASS / FAIL
    @author Alexander Zakshevsky
*/            
function setSysText( edtName, text, wait ) {
   var _edtName = edtName, _text = text, _wait = wait;

   return _process( function _setSysText( ) {

   var edt = objFind( _edtName );  
   if ( edt.get( "disabled" ) ) {
      traceDebug( MSG_CTRL_DISABLED( _edtName ) );         
      return FAIL;
   }
   
   edt.make( "click" );

   Sys.Keys( "[Home]![End][Del]" );//clear content
   Sys.Keys( _text );
   
   if ( _wait ) Sys.Keys("[Tab]");
   traceDebug( MSG_TEXT_SET( _edtName, text ) );
   
   }, MAKE_REFRESH=_wait );
   
}


/** Find object by a set of properties' values
    @param propertyX Property name
    @param valueX Value of corresponding property
    @return Object / FAIL
    @author Alexander Zakshevsky
*/            
function objFindByProperty( property0, value0 /* , ... , propertyN, valueN */ ) {   
   var obj = null;
   var j;
   
   _CTLNAME.set( value0 );
         
   var COLLECTION = PAGE.document.all;
   var PATH = "";
   for ( j = 0; j < arguments.length - ( arguments.length % 2 ); j += 2 ) {
      if(j>0) PATH += ";";
      PATH += arguments[ j ] + "='" + arguments[ j+1 ] +"'";
   }
   
   for ( var i = 0; i < COLLECTION.ChildCount; i++ ) {
      obj = COLLECTION.Child( i );   
      for ( j = 0; j < arguments.length - ( arguments.length % 2 ); j += 2 ) {
         if ( !BuiltIn.IsSupported( obj, arguments[j] ) ) break;
         if ( typeof( obj[ arguments[ j ] ] ) == "object" ) break;
         if ( trim( obj[ arguments[ j ] ] ) != arguments[ j + 1 ] ) break;
      }   
      if ( j == arguments.length - ( arguments.length % 2 ) ) {
         traceDebug( MSG_OBJECT_FOUND( PATH ) );
         return _OBJECT.store( obj );         
      }
   }      
   traceDebug( MSG_OBJECT_NOT_FOUND( PATH ) );      
   return FAIL;
}


/** Find object by name
    @param ctlname Name of object that we are looking for
    @return Object / FAIL
    @author Alexander Zakshevsky
*/            
function objFind( ctlname ) {
   _CTLNAME.set( ctlname );

   obj = WND.Page( COMMON_CAPTION ).document.all.WaitChild( ctlname );
   
   if ( obj.exists ) {
      traceDebug( MSG_OBJECT_FOUND( ctlname ) );      
      return _OBJECT.store( obj );
   }
   traceDebug( MSG_OBJECT_NOT_FOUND( ctlname ) );
   return FAIL;
}


/** Check that object exists on the page.
   Function works in two modes:
   1. Only ctlname is given. Find object using objFind() and check if it exists.
   2. Set of properties' values is given. Find object using objFindByProperty() 
      and check if it exists   

    @param ctlname Name of object that we are looking for
    @param propertyX Property name
    @param valueX Value of corresponding property
    @return Object / FAIL
    @author Alexander Zakshevsky
*/            
function objExists( ctlname, property0, value0 /* ..., propertyN, valueN */ ) {
   var _arguments = arguments;

   return _process ( function _objExists( ) {
   
      var res;
      if ( _arguments.length > 1 ) {
         var arglist = "\"" + _arguments[0] + "\"";
         for ( var i = 1; i < _arguments.length; i++ ) arglist += "," + "\"" + _arguments[i] + "\"";
         res = eval( "objFindByProperty(" + arglist + ")" );      
      } else {
         res = objFind( ctlname );
      }
   
      return res != FAIL ? PASS : FAIL;
   } );
}


/** Check page by image.
    @param imgname Image
    @return PASS / FAIL
    @author Alexander Zakshevsky
*/            
function pageCheckByImg( imgname ) {
   var _imgname = imgname;

   return _process( function _setText( ) {                  

   if ( objFind( "imgHeader", "IMG" ).get( "src" ).indexOf( _imgname ) >= 0 ) {
      traceDebug( MSG_PAGEHEADER_CORRECT( _imgname ) );
      return PASS;
   }   
   traceDebug( MSG_PAGEHEADER_INVALID( _imgname ) );
   return FAIL;
   
   } );   
}

function catchDialog( wndClass, header ) {
   wndClass = wndClass || "Internet Explorer_TridentDlgFrame";  
   header = ( header == null ) ? "*" : header;

   markAsExecuting( catchDialog );
  
   return _process( function _catchDialog( ) {
  
   var wnd = Sys.Process( "IEXPLORE" ).WaitWindow( wndClass, header, 1, 10000 );
   if ( wnd.Exists ) {
      traceDebug( MSG_DIALOG_CATCHED( wndClass ) );
      return wnd;
   }       
   traceDebug( MSG_DIALOG_NOT_CATCHED( wndClass ) );
   return FAIL; 
    
   } );
   
   unmark( catchDialog );
}

/** Set the checked/unchecked state for check box
    @param checkBoxName Name of check box
    @param state true/false
    @return PASS - Success, FAIL - Failed
    @author Philip Gounbin
*/            
function setCheckBoxState( checkBoxName, state ) {
   var _checkBoxName = checkBoxName, _state = state;

   return _process( function _setCheckBoxState( ) {   

   var checkBox = objFind( checkBoxName );
   if ( state != checkBox.get( "checked" ) ) {
      if ( checkBox.get( "disabled" ) ) {
         traceDebug( MSG_CTRL_DISABLED( _checkBoxName ) );         
         return FAIL;
      }
      checkBox.make( "click" );
   }
    
   }, MAKE_REFRESH=true );    
}


/** Set the checked/unchecked state for check box
    @param checkBoxName Name of check box
    @param state True - checked, False - unchecked
    @return boolean value of 'checked' property if success, FAIL if object not found
    @author Philip Gounbin
*/            
function getCheckBoxState( checkBoxName ) {
    var _checkBoxName = checkBoxName;

    return _process( function _getCheckBoxState( ) {
    
    return objFind( checkBoxName ).get( "checked" );
    
    } );
}

/** Get 'disabled' state of control
    @param ctlname Control name
    @return true/false if success, FAIL object if object not found
    @author Philip Gounbin
*/            
function isDisabled( ctlname ) {
   var _ctlname = ctlname;    
   
   return _process( function _isDisabled( ) {
   
   return _disabled( _ctlname );   
            
   } );
}

/** Get 'enabled' state of control
    @param ctlname Control name
    @return true/false if success, FAIL object if object not found
    @author Philip Gounbin
*/            
function isEnabled( ctlname ) {
   var _ctlname = ctlname;    
   
   return _process( function _isEnabled( ) {
   
   var rc = _disabled( _ctlname );
   if ( rc == FAIL ) return FAIL;
   else return !rc;
            
   } );
}

function _disabled( ctlname ) {
   var obj = objFind( ctlname );

   var readOnly = BuiltIn.IsSupported( obj.obj, "readOnly" );
   var disabled = BuiltIn.IsSupported( obj.obj, "disabled" );
   var enabled  = BuiltIn.IsSupported( obj.obj, "Enabled" );
   
   if ( !readOnly && !disabled && !enabled ) return FAIL;  //obj doesn't support any property
   
   if ( ( readOnly && obj.get( "readOnly" ) ) ||
        ( disabled && obj.get( "disabled" ) ) ||
        ( enabled && !obj.get( "enabled" ) ) ) return true;
   
   return false;
}

/** Check the state of validator. TODO: REWRITE this function.
    @param name Control name
    @param state Expected state true/false
    @return PASS / FAIL
    @author Philip Gounbin
*/            
function vldCheck( name, state ) {
    return _process( function _vldCheck( ) {
    
    var vld = objFind( name );     
    var len = vld.get( "attributes.length" );
    
    for( var i = 0; i < len; i++ ) {
      var type = vld.get( "attributes", i, "name" );
      if ( type != "isvalid" ) continue;              
      if ( vld.get( "attributes", i, "VisibleOnScreen" ) == state ) {
         traceDebug( "The state of validator: " + name + " is correct" );
         return PASS;
      } else {
         traceDebug( "The state of validator: " + name + " is incorrect" );
         return FAIL;
      }
    }
    //return FAIL;    
    });
}

/** Check the state of radio button
    @param ctlname Control name
    @param status Expected state true/false
    @return PASS / FAIL
    @author Philip Gounbin
*/            
function cmpRadioButtonStatus( rbtnName, status ) {
   var _rbtnName = rbtnName, _status = status;

   return _process( function _cmpRadioButtonStatus( ) {   
   
   if ( objFind( _rbtnName ).get( "status" ) != _status ) {
      traceDebug( MSG_TEXT_NOT_EQUALS( _rbtnName, _status ) );      
      return FAIL;
   } else {
      traceDebug( MSG_TEXT_EQUALS( _rbtnName, _status ) );      
      return PASS;
   }      

   } );    
}


/** Press a button with number of warning dialogs
    @param btn Button name
    @param buttonList (optional) Zero or more button captions to press
    @return array of window captions, starting from first / FAIL
    @author Alexander Zakshevsky
*/            
function pressButtonWW( btn /*, buttonList */ ) {
   var _btn = btn, _args = arguments;
   if ( pressButton( _btn, false ) == FAIL ) return FAIL;

   return _process( function _pressButtonWithWarns( ) {
   var captions = new Array();

   for ( var i = 1; i < _args.length; i++ ) {
      wnd = Sys.Process( "IEXPLORE" ).WaitWindow( "#32770", "*", 1, 10000 );
      try { 
          captions.push( wnd.Window("Static", "*", 2).WndCaption );
          btnWarn_obj = wnd.Window("Button", _args[i] );
          btnWarn_obj.Click();
      } catch ( e ) { return FAIL; }
   }
   
   return captions;
   }, MAKE_REFRESH=true);
}

  
function refresh( ) {
   PAGE = WND.Page( COMMON_CAPTION );        
   PAGE.Wait( );
} 

function checkForExceptionDisplay( page ) {
   if ( page.document.getElementById( "intel_exception_display" ) ) SYS_ERROR = "Exception"; 
}

function _process( libfunc, doRefresh ) {
   mediator.releaseBuf( );
 
   markAsExecuting( _process );  
   try { 
      if ( ( ret = libfunc( ) ) == null ) ret = PASS; 
      if ( doRefresh == true ) refresh( );
   } catch( ex ) { 
      var msg = "";
      switch ( typeof( ex ) ) {
         case "string" : msg = ex; break;
         case "object" : msg = ex.description; break;
      }
      traceFail( FORMAT_MSG( msg, libfunc ) ); 
      ret = FAIL; 
   }   
   unmark( _process );
      
   flushError( );   
   mediator.setResult( ret );   
   return ret;
}


function flushError() {
   if ( SYS_ERROR ) {
      var msg = SYS_ERROR; SYS_ERROR = null;   
      if ( SKIPTEST_ON_UNEXPWND ) AssertF.DoFail( MSG_SYSTEM_CRASHED( msg ) );      
      else Assert.DoFail( msg );
   }        
}


function markAsExecuting( func ) { 
   func.prototype = "exec";
}

function unmark( func ) {
   func.prototype = null;
}

function isExecuting( func ) {
   return func.prototype == "exec" ? true : false;  
}


function getUniqueStringByDate( pref ) {
   var res = "" || pref;  // should be string value even if pref is not passed
   var today  = new Date();   //today date 
   var month = today.getMonth( ), date = today.getDate( );
   
   res = pref 
      + today.getYear( ) 
      + ( month > 10 ? ( month + 1 ) : "0" + ( month + 1 ) ) 
      + ( date > 10 ? date : "0" + date ) 
      + today.getHours() 
      + today.getMinutes() 
      + today.getSeconds();
      
   return res;
}


function ctlIdGen( ctl, num ){
   var re = /ctl\d+/g;
   return ctl.replace( re, "ctl" + num );
}


function _toURL( url, doRefresh /* =true */ ) {
   if ( doRefresh == null ) doRefresh = true;
   
   PAGE.ToURL( url );
    
   if ( doRefresh ) _refresh_page();
   else PAGE.Wait( );
}


function _getRecSet( query, doAssert ) {
   if ( doAssert == null ) doAssert = true;

   try {
      var RecSet = conn.Execute_( query );
      if ( doAssert ) Assert.AreEqual( RecSet.RecordCount, 1, "Record Set contains one row" );
      if ( ( RecSet.State == 0 ) || ( RecSet.EOF ) ) return null;
      RecSet.MoveFirst();
      return RecSet;
   } catch( e ) {
      traceFail( "Cannot execute SQL query: " + e.description );
      return null;
   }
}   

function _execQuery( query ) {
   return _getRecSet( query, false );
}


function getField( recordSet, fieldName ) {
   return recordSet ? recordSet.Fields( fieldName ).Value : null;
}

function _getLastId( ) {
   return getField( _getRecSet( "SELECT @@IDENTITY AS 'id'" ), "id" );
}


function _refresh_page(){
    Sys.Keys("^[F5]");
    var wnd;
    if ( ( ( wnd = dlgWait() ) != FAIL ) && ( btnWarn = wnd.Window( "Button", "&Retry" )).exists ) 
        btnWarn.Click();
    PAGE.Wait();  
}

function dlgWait() { 
   wnd = Sys.Process("IEXPLORE").WaitWindow("#32770", "*", 1, 1000);
   return wnd.Exists ? wnd : FAIL;
}

function createSQLConn( ) {
   conn = ADO.CreateADOConnection();
   conn.ConnectionString = CONN_STR;
   conn.LoginPrompt = false;
   conn.Open();
}

