//USEUNIT commonlib
//USEUNIT globvars
//USEUNIT local_lib
//USEUNIT trace

//USEUNIT suite_runner

function getVar (SectionName, OptionName) {
  var ProjPath = Options.Project.Directory;   
  var FSO, Section, OptionVal;

  try {
    FSO = Storages.INI(ProjPath + "Config.ini");

     Section = FSO.GetSubSection(SectionName);
     OptionVal = (Section.GetOption(OptionName, "undefined"));

    return OptionVal;
  }
  catch (exception){
    traceFail ("Your file seems to be damaged or corrupted. Check Config.ini file.");
    Runner.Stop();
  }
}

function initGlobalVars () 
{
  START_PAGE = getVar("APP_OPTIONS", "START_PAGE" );
  APPPATH    = getVar("APP_OPTIONS", "APPPATH" );

  CONN_STR   = getVar("DB_OPTIONS", "CONN_STR" );

  EMAIL_TO   = getVar("USER_OPTIONS", "EMAIL_TO");   
  AGT        = getVar("USER_OPTIONS", "AGT");

  REPORT     = getVar("DEBUG_OPTIONS", "REPORT") == "false" ? false : true; 
  TRACE      = getVar("DEBUG_OPTIONS", "TRACE") == "false" ? false : true; 
} 

function OnStartTest( Sender ) {
   Log.LockEvents();
   initGlobalVars(); 
   
   var newProcess = false;
   var re = /iexplore/i; 
   
   for ( var i = 0; i < Sys.ChildCount; i++) 
      if ( re.test( Sys.Child(i).ProcessName ) ) break;
      
   if ( i == Sys.ChildCount ) {      
      newProcess = true;
      while( ( number = TestedApps.Find( APPPATH + APPNAME ) ) != -1 )
         TestedApps.Delete(number);
      number = TestedApps.Add( ( APPPATH + APPNAME ), START_PAGE, 1, true);
      var obj = TestedApps.Items( number );
      obj.Run( );      
      Sys.Delay( 10000 );
   }

   WND = Sys.Process( IE ).Window( IE_WNDCLASS, COMMON_CAPTION );
   WND.Activate( );      
   PAGE = WND.Page( COMMON_CAPTION );        

   //refresh();
   if ( !newProcess ) {
      PAGE = PAGE.ToUrl( START_PAGE );
      PAGE.Wait();
   }
   
   createSQLConn( );
   Utilities.DecimalSeparator = ".";
}
      
function OnStopTest( Sender ) {
    Log.UnLockEvents();
    cleanUpMemory( );
   
   conn.Close( );  //close SQL connection
//   WND.Close();    //close IE window
      
   if ( ( Log.ErrCount == 0 ) || !REPORT ) return;
   
   LastRes = Log.Results.Items( Log.Results.Count - 1 );

   var Msg = new ActiveXObject( "CDO.Message" );
   Msg.BodyPart.Charset = "windows-1251";
   Msg.From = LastRes.Data.ValueByName( "Computer" ) + "@star-sw.com";
   Msg.Subject = "Automation scripts notification";
   Msg.To = EMAIL_TO;
   Msg.TextBody = "Error occured during execution of scripts." + 
      "\n" + 
      "\nStart date: " + Utilities.DateToStr( LastRes.Data.ValueByName( "StartDate" ) ) + 
      "\nStart time: " + Utilities.TimeToStr( LastRes.Data.ValueByName( "StartTime" ) ) + 
      "\nEnd date:   " + Utilities.DateToStr( LastRes.Data.ValueByName( "EndDate" ) ) + 
      "\nEnd time:   " + Utilities.TimeToStr( LastRes.Data.ValueByName( "EndTime" ) ) + 
      "\n" + 
      "\nERROR COUNT: " + Log.ErrCount;
   Msg.TextBodyPart.Charset = "windows-1251";
   Msg.Send( );
}

function OnUnexpectedWindow( Sender, Window, LogParams ) {
   if ( !isExecuting( catchDialog ) ) {
      Window.Close( );
      SYS_ERROR = "Unexpected window";    
   }
}

function OnWebPageDownloaded(Sender, Page, URL)
{
   checkForExceptionDisplay( Page );
}




