//USEUNIT globvars

function trace(msg) {
   var rePass = /\bpass\b/i;              
   var reFail = /\bfail\b/i;
   
   var isPassMsg = rePass.exec(msg);
   var isFailMsg = reFail.exec(msg);

   if (isPassMsg && !isFailMsg) {
      tracePass(msg);
   } else if (isFailMsg && !isPassMsg) {
      traceFail(msg);
   } else {
      Log.Message( msg );
   }   
}

function tracePass(msg, type) {
   Log.Message(msg, "", 4, 1, clYellow, clGreen);
}

function traceFail(msg, type) {
   Log.Error(msg, "", 4, 1, clYellow, clRed);
}


function traceDebug( msg, isFail ) {
   if ( TRACE && isFail ) traceFail( msg );
   else if ( TRACE && !isFail ) trace( msg );
}
