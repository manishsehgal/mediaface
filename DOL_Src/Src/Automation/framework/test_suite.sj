//USEUNIT trace
//USEUNIT commonlib
//USEUNIT asserts

function TestSuite() {
   this.setUp     = testSuite_setUp;       //overridable
   this.tearDown  = testSuite_tearDown;    //overridable
   this.suiteIni  = testSuite_suiteIni;    //overridable
   this.suiteFini = testSuite_suiteFini;   //overridable       

   this.runAllTests = testSuite_runAllTests;
   this.throwException = testSuite_throwException;
               
   function testSuite_setUp() { }    
   function testSuite_tearDown() { }
   function testSuite_suiteIni() { }
   function testSuite_suiteFini() { }

   function testSuite_throwException(msg, type) {
      switch(type) {
         case "tsf_assert" : throw msg;       
         case "ts_assert"  : traceFail(msg); 
      }
   }
       
   function testSuite_runAllTests() {                                           
     try { this.suiteIni(); } catch (exception) { traceFail( "Suite Ini FAILED: " + exception ); return; }
     for (var test in this) {
         var re = /\btest\s*/i;      
         if (!re.exec(test)) continue;
         try {
            Log.CreateNode(test);
            this.setUp();
            eval("this." + test + "()");
         } catch(exception) {
            traceFail(exception);
         } finally {
            flushError( );   
            END( );
            this.tearDown();
            Log.CloseNode();
         }                  
     }
     try { this.suiteFini(); } catch (exception) { traceFail( "Suite Fini FAILED: " + exception ); }
   }
}


