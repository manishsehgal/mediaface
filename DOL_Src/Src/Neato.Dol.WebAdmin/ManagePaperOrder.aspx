<%@ Page language="c#" Codebehind="ManagePaperOrder.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.ManagePaperOrder" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>ManagePaperOrder</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
	
    <form id="Form1" method="post" runat="server">
		<table>
			<tr>
				<td>
					<span>Paper Category :</span>
				</td>
				<td>
					<asp:DropDownList Runat="server" ID="cboPaperCategoryFilter" AutoPostBack="True" />
				</td>
			</tr>
			<tr>
				<td>
					<span>Paper Brand :</span>
				</td>
				<td>
					<asp:DropDownList Runat="server" ID="cboPaperBrandFilter" AutoPostBack="True" />
				</td>
			</tr>
			<tr>
				<td>
					<span>Paper Metric :</span>
				</td>
				<td>
					<asp:DropDownList Runat="server" ID="cboPaperMetricFilter" AutoPostBack="True" />
				</td>
			</tr>
		</table>
		<asp:DataGrid Runat="server" ID="grdPapers" AutoGenerateColumns="False">
			<Columns>
				<asp:TemplateColumn HeaderText="Paper Name">
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                    <ItemStyle Wrap="True" Width="400px" />
                    <ItemTemplate>
                        <asp:Label ID="lblName" Runat="server" />
                    </ItemTemplate>
				</asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Paper State">
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                    <ItemStyle Wrap="True" Width="90px" />
                    <ItemTemplate>
                        <asp:Label ID="lblPaperState" Runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Paper Brand">
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                    <ItemStyle Wrap="True" Width="90px" />
                    <ItemTemplate>
                        <asp:Label ID="lblPaperBrand" Runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Metric">
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                    <ItemStyle Wrap="True" Width="90px" />
                    <ItemTemplate>
                        <asp:Label ID="lblPaperMetric" Runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
				<asp:TemplateColumn HeaderText="Action">
					<ItemTemplate>
                        <asp:ImageButton
                                Runat="server"
                                ID="btnItemUp"
                                ImageUrl="images/up.gif"
                                CommandName="Up"
                                CausesValidation="False"
                                AlternateText="Up" />
                        <asp:ImageButton
                                Runat="server"
                                ID="btnItemDown"
                                ImageUrl="images/down.gif"
                                CommandName="Down"
                                CausesValidation="False"
                                AlternateText="Down" />
					</ItemTemplate>
				</asp:TemplateColumn>
			</Columns>
		</asp:DataGrid>
     </form>
  </body>
</html>
