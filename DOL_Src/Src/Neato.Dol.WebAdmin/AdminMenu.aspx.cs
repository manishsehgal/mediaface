using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin {
    public class AdminMenu : Page {
        protected HyperLink hypFooterMenu;
        protected HyperLink hypParameters;
        protected HyperLink hypTermsOfUse;
        protected HyperLink hypFaq;
        protected HyperLink hypAddPhone;
        protected HyperLink hypSiteClosing;
        protected HyperLink hypManageMaintenanceClosing;
        protected HyperLink hypTrackUserAction;
        //protected HyperLink hypTrackPhones;
        protected HyperLink hypTrackPapers;
        protected HyperLink hypTrackImages;
        protected HyperLink hypTrackShapes;
        protected HyperLink hypTrackFeatures;
        protected HyperLink hypNotifications;
        protected HyperLink hypCustomerProfiles;
        protected HyperLink hypManageDeviceBrand;
        protected HyperLink hypManagePaperBrand;
        protected HyperLink hypManageDevice;
        protected HyperLink hypManageMP3PlayerPapers;
        protected HyperLink hypManagePaper;
        protected HyperLink hypManageRetailer;
        protected HyperLink hypManageLabel;
        protected HyperLink hypImageLibrary;
        protected HyperLink hypBannerManagement;
        protected HyperLink hypGoogleTracking;
        protected HyperLink hypManageIPFilter;
        protected HyperLink hypManageUpc;
        protected HyperLink hypManageUpcStaple;
        protected HyperLink hypManageLayout;
        protected HyperLink hypManageTemplate;
        protected HyperLink hypManageSpecialUser;
        protected HyperLink hypExitPage;
        protected HyperLink hypDeadLinksReport;
        protected HyperLink hypManageCategory;
        protected HyperLink hypPluginManagement;
        protected HyperLink hypPreviewHtmlManagement;
        protected HyperLink hypUpcReport;
        protected HyperLink hypUpcStapleReport;
		protected HyperLink hypManagePaperOrder;
        protected HyperLink hypWatermarkManagement;
        
        #region Url
        private const string RawUrl = "AdminMenu.aspx";
        private const string Target = "Content";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new System.EventHandler(this.AdminMenu_DataBinding);

        }
        #endregion

        private void AdminMenu_DataBinding(object sender, EventArgs e) {
            hypFooterMenu.NavigateUrl = FooterMenu.Url().PathAndQuery;
            hypFooterMenu.Target = Target;
            hypParameters.NavigateUrl = Parameters.Url().PathAndQuery;
            hypParameters.Target = Target;
            hypTermsOfUse.NavigateUrl = TermsOfUse.Url().PathAndQuery;
            hypTermsOfUse.Target = Target;
            hypFaq.NavigateUrl = Faq.Url().PathAndQuery;
            hypFaq.Target = Target;
            hypAddPhone.NavigateUrl = PhoneRequests.Url().PathAndQuery;
            hypAddPhone.Target = Target;
            hypSiteClosing.NavigateUrl = SiteClosing.Url().PathAndQuery;
            hypSiteClosing.Target = Target;
            hypManageMaintenanceClosing.NavigateUrl = ManageMaintenanceClosing.Url().PathAndQuery;
            hypManageMaintenanceClosing.Target = Target;
            //hypManageCarrier.NavigateUrl = ManageCarrier.Url().PathAndQuery;
            //hypManageCarrier.Target = Target;
            hypManageCategory.NavigateUrl = ManageCategory.Url().PathAndQuery;
            hypManageCategory.Target = Target;
            hypTrackUserAction.NavigateUrl = TrackingUserActionReport.Url().PathAndQuery;
            hypTrackUserAction.Target = Target;
            //hypTrackPhones.NavigateUrl = TrackingPhonesReport.Url().PathAndQuery;
            //hypTrackPhones.Target = Target;
            hypTrackPapers.NavigateUrl = TrackingPaperReport.Url().PathAndQuery;
            hypTrackPapers.Target = Target;
            hypTrackImages.NavigateUrl = TrackingImagesReport.Url().PathAndQuery;
            hypTrackImages.Target = Target;
            hypTrackShapes.NavigateUrl = TrackingShapesReport.Url().PathAndQuery;
            hypTrackShapes.Target = Target;
            hypTrackFeatures.NavigateUrl = TrackingFeaturesReport.Url().PathAndQuery;
            hypTrackFeatures.Target = Target;
            hypNotifications.NavigateUrl = Notifications.Url().PathAndQuery;
            hypNotifications.Target = Target;
            hypCustomerProfiles.NavigateUrl = CustomerList.Url().PathAndQuery;
            hypCustomerProfiles.Target = Target;
            hypManageDeviceBrand.NavigateUrl = ManageDeviceBrand.Url().PathAndQuery;
            hypManageDeviceBrand.Target = Target;
            hypManagePaperBrand.NavigateUrl = ManagePaperBrand.Url().PathAndQuery;
            hypManagePaperBrand.Target = Target;
            hypManagePaper.NavigateUrl = ManagePaper.Url().PathAndQuery;
            hypManagePaper.Target = Target;
            hypManageLabel.NavigateUrl = ManageLabel.Url().PathAndQuery;
            hypManageLabel.Target = Target;
            hypManageDevice.NavigateUrl = ManageDevice.Url().PathAndQuery;
            hypManageDevice.Target = Target;
            hypManageMP3PlayerPapers.NavigateUrl = ManageMP3PlayerPapers.Url().PathAndQuery;
            hypManageMP3PlayerPapers.Target = Target;
            hypImageLibrary.NavigateUrl = ImageLibraryContent.Url().PathAndQuery;
            hypImageLibrary.Target = Target;
            hypBannerManagement.NavigateUrl = BannerManagement.Url().PathAndQuery;
            hypBannerManagement.Target = Target;
            hypGoogleTracking.NavigateUrl = GoogleTracking.Url().PathAndQuery;
            hypGoogleTracking.Target = Target;
            hypManageIPFilter.NavigateUrl = ManageIPFilter.Url().PathAndQuery;
            hypManageIPFilter.Target = Target;
            hypManageUpc.NavigateUrl = Upc.Url().PathAndQuery;
            hypManageUpc.Target = Target;
            hypManageUpcStaple.NavigateUrl = UpcStaple.Url().PathAndQuery;
            hypManageUpcStaple.Target = Target;
            hypManageLayout.NavigateUrl = ManageLayout.Url().PathAndQuery;
            hypManageLayout.Target = Target;
            hypManageTemplate.NavigateUrl = ManageTemplate.Url().PathAndQuery;
            hypManageTemplate.Target = Target;
            hypManageRetailer.NavigateUrl = ManageRetailer.Url().PathAndQuery;
            hypManageRetailer.Target = Target;
            hypManageSpecialUser.NavigateUrl = ManageSpecialUser.Url().PathAndQuery;
            hypManageSpecialUser.Target = Target;
            hypExitPage.NavigateUrl = ExitPage.Url().PathAndQuery;
            hypExitPage.Target = Target;
            hypDeadLinksReport.NavigateUrl = DeadLinksReport.Url().PathAndQuery;
            hypDeadLinksReport.Target = Target;
            hypPluginManagement.NavigateUrl = PluginManagement.Url().PathAndQuery;
            hypPluginManagement.Target = Target;
            hypPreviewHtmlManagement.NavigateUrl = PreviewHtmlManagement.Url().PathAndQuery;
            hypPreviewHtmlManagement.Target = Target;
            hypUpcReport.NavigateUrl = TrackingUpcReport.Url().PathAndQuery;
            hypUpcReport.Target = Target;
            hypUpcStapleReport.NavigateUrl = TrackingUpcStapleReport.Url().PathAndQuery;
            hypUpcStapleReport.Target = Target;
            hypManagePaperOrder.NavigateUrl = ManagePaperOrder.Url().PathAndQuery;
            hypManagePaperOrder.Target = Target;
            hypWatermarkManagement.NavigateUrl = ManageWatermark.Url().PathAndQuery;
            hypWatermarkManagement.Target = Target;
        }
    }
}
