using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity.Tracking;
using Neato.Dol.WebAdmin.Controls;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin {
    public class TrackingFeaturesReport : Page {
        #region Url
        private const string RawUrl = "TrackingFeaturesReport.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        protected Button btnSubmit;
        protected DateInterval dtiDates;
        protected DataGrid grdReport;

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(TrackingFeaturesReport_DataBinding);
            btnSubmit.Click += new EventHandler(btnSubmit_Click);

        }
        #endregion

        private void TrackingFeaturesReport_DataBinding(object sender, EventArgs e) {}

        private void btnSubmit_Click(object sender, EventArgs e) {
            grdReport.Visible = false;
            if (IsValid) {
                DataSet data = BCTrackingFeatures.GetFeaturesListByDate(dtiDates.StartDate, dtiDates.EndDate);
                bindGrid(data);
                DataBind();
            }
        }

        private void bindGrid(DataSet data) {
            grdReport.DataSource = data;
            grdReport.Visible = true;
            DataTable resultTable = new DataTable();
            resultTable.Columns.Add("Feature");
            resultTable.Columns.Add("Count");
            string[] features = Feature.GetNames(typeof(Feature));
            foreach (DataRow row in data.Tables[0].Rows) {
                DataRow workrow = resultTable.NewRow();
                string feature = (string)row[0];
                workrow["Feature"] = decodeFeature(feature);
                workrow["Count"] = row[1];
                resultTable.Rows.Add(workrow);
                for (int i = 0; i < features.Length; i++) {
                    string f = features[i];
                    if (f == feature) {
                        features[i] = null;
                        break;
                    }
                }

            }
            foreach (string f in features) {
                if (f == null)
                    continue;
                if (f != "None") {
                    DataRow workrow = resultTable.NewRow();
                    workrow["Feature"] = decodeFeature(f);
                    workrow["Count"] = 0;
                    resultTable.Rows.Add(workrow);
                }

            }
            grdReport.DataSource = resultTable;
        }

        private string decodeFeature(string feature) {
            string res = "";
            switch (feature) {
                case "ColorFill":
                    res = "Set Background";
                    break;
                case "AddImage":
                    res = "Add Image from Library";
                    break;
                case "AddCustomImage":
                    res = "Upload Image";
                    break;
                case "AddText":
                    res = "Add TextBox";
                    break;
                case "AddArtisticText":
                    res = "Add Artistic Text";
                    break;
                case "Paint":
                    res = "Paint";
                    break;
                case "AddShape":
                    res = "Add Shape";
                    break;
                case "AddGradient":
                    res = "Add Gradient";
                    break;
                case "Clear":
                    res = "Clear";
                    break;
                case "UsePreviousDesign":
                    res = "Open Design";
                    break;
                case "SaveDesign":
                    res = "Save Design";
                    break;
                case "Plugins":
                    res = "Plugins panel";
                    break;
                case "QuickTools":
                    res = "Quick tools";
                    break;
                case "QuickToolsScale":
                    res = "Quick tools: Scale";
                    break;
                case "QuickToolsMove":
                    res = "Quick tools: Move";
                    break;
                case "QuickToolsRotate":
                    res = "Quick tools: Rotate";
                    break;
                case "QuickToolsAlign":
                    res = "Quick tools: Align";
                    break;
                case "QuickToolsLayers":
                    res = "Quick tools: Layers";
                    break;
                case "QuickToolsUndoRedo":
                    res = "Quick tools: Undo/Redo";
                    break;
                case "Preview":
                    res = "Preview";
                    break;
                case "PrintPDF":
                    res = "Print (PDF)";
                    break;
                case "PrintLocal":
                    res = "Print (Local Printer)";
                    break;
                case "PrintLightscribe":
                    res = "Print (LightScribe)";
                    break;
                case "CopyDesign":
                    res = "Copy design";
                    break;
                case "TransferDesign":
                    res = "Transfer design";
                    break;
                case "ApplyTemplate":
                    res = "Apply Template";
                    break;
                case "ApplyLayout":
                    res = "Apply Layout";
                    break;
                case "Zoom":
                    res = "Zoom";
                    break;
                case "AddPlaylist":
                    res = "Add Playlist (manually)";
                    break;
                case  "AddPlaylistAudioCdText":
                    res = "Add Playlist (Audio - CD Text)";
                    break;
                case  "AddPlaylistAudioScanTracks":
                    res = "Add Playlist (Audio - Scan Tracks)";
                    break;
                case  "AddPlaylistScanFolders":
                    res = "Add Playlist (Scan Folders)";
                    break;
                case  "AddPlaylistFile":
                    res = "Add Playlist (Playlist File)";
                    break;
                case  "AddPlaylistPlayerWMP":
                    res = "Add Playlist (Windows Media Player)";
                    break;
                case  "AddPlaylistPlayeriTunes":
                    res = "Add Playlist (iTunes Player)";
                    break;
                default:
                    res = feature;
                    break;
            }
            return res;
        }

    }
}