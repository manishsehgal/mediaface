using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin {
    public class Upc : Page {
        private const string LblCodeId = "lblCode";
        private const string TxtCodeId = "txtCode";
        private const string VldCodeRequiredId = "vldCodeRequired";
        private const string BtnDeleteId = "btnDelete";

        protected Button btnNew;
        protected DataGrid grdUpc;

        private bool NewMode {
            get { return (bool)ViewState["NewModeKey"]; }
            set { ViewState["NewModeKey"] = value; }
        }

        #region Url
        private const string RawUrl = "Upc.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                NewMode = false;
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Upc_DataBinding);
            grdUpc.ItemDataBound += new DataGridItemEventHandler(grdUpc_ItemDataBound);
            grdUpc.EditCommand += new DataGridCommandEventHandler(grdUpc_EditCommand);
            grdUpc.DeleteCommand += new DataGridCommandEventHandler(grdUpc_DeleteCommand);
            grdUpc.UpdateCommand += new DataGridCommandEventHandler(grdUpc_UpdateCommand);
            grdUpc.CancelCommand += new DataGridCommandEventHandler(grdUpc_CancelCommand);
            btnNew.Click += new EventHandler(btnNew_Click);
        }
        #endregion

        private void Upc_DataBinding(object sender, EventArgs e) {
            ArrayList upcList = new ArrayList(BCUpc.Enum());
            if (NewMode) {
                upcList.Insert(0, new Entity.Upc());
                grdUpc.EditItemIndex = 0;
            }
            grdUpc.DataSource = upcList;
            grdUpc.DataKeyField = Entity.Upc.IdField;
        }

        private void grdUpc_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void ItemDataBound(DataGridItem item) {
            Entity.Upc upc = (Entity.Upc)item.DataItem;
            Label lblCode = (Label)item.FindControl(LblCodeId);
            lblCode.Text = HttpUtility.HtmlEncode(upc.Code);

            Button btnDelete = (Button)item.FindControl(BtnDeleteId);
            btnDelete.Attributes["onclick"] = "return confirm('Are you sure to delete this item?')";
        }

        private void EditItemDataBound(DataGridItem item) {
            Entity.Upc upc = (Entity.Upc)item.DataItem;
            TextBox txtCode = (TextBox)item.FindControl(TxtCodeId);
            txtCode.Text = HttpUtility.HtmlEncode(upc.Code);
            JavaScriptHelper.RegisterFocusScript(this, txtCode.ClientID);

            RequiredFieldValidator vldCodeRequired = (RequiredFieldValidator)item.FindControl(VldCodeRequiredId);
            vldCodeRequired.Enabled = NewMode;
        }

        private void grdUpc_EditCommand(object source, DataGridCommandEventArgs e) {
            grdUpc.EditItemIndex = e.Item.ItemIndex;
            NewMode = false;
            DataBind();
        }

        private void grdUpc_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdUpc.EditItemIndex = -1;
            Entity.Upc upc = new Entity.Upc();
            upc.Id = (int)grdUpc.DataKeys[e.Item.ItemIndex];
            try {
                BCUpc.Delete(upc);
                Response.Redirect(Url().PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewMode = false;
            DataBind();
        }

        private void grdUpc_UpdateCommand(object source, DataGridCommandEventArgs e) {
            if (!IsValid)
                return;

            TextBox txtCode = (TextBox)e.Item.FindControl(TxtCodeId);
            Entity.Upc upc = new Entity.Upc();
            upc.Id = (int)grdUpc.DataKeys[e.Item.ItemIndex];
            upc.Code = txtCode.Text.Trim();

            try {
                if (NewMode)
                    BCUpc.Insert(upc);
                else
                    BCUpc.Update(upc);
                Response.Redirect(Url().PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            grdUpc.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdUpc_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdUpc.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewMode = true;
            DataBind();
        }
    }
}