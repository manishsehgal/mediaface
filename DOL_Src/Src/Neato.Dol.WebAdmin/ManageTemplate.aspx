<%@ Page language="c#" Codebehind="ManageTemplate.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.ManageTemplate" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>ManageTemplate</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
	<style>td.Action, td.Action input {width: 100px}</style>
  </head>
  <body MS_POSITIONING="GridTemplate">
    <form id="Form1" method="post" runat="server">
		<h3 align="center">Template management</h3>
		<asp:Panel Runat="server" ID="panTemplate">
			<table border="0" cellpadding="4" cellspacing="0">
				<tr>
					<td>Label :</td>
					<td>
						<asp:DropDownList ID="cboLabelFilter" Runat="server" style="width:250px" />
					</td>
				</tr>
				<tr>
					<td>Device :</td>
					<td>
						<asp:DropDownList ID="cboDeviceFilter" Runat="server" style="width:250px" />
					</td>
				</tr>
				<tr>
					<td>Device Type :</td>
					<td>
						<asp:DropDownList ID="cboDeviceTypeFilter" Runat="server" style="width:250px" />
					</td>
				</tr>
					<tr>
						<td colspan="2" align="right">
							<asp:Button runat="server" ID="btnSearch" Text="Search" />
						</td>
					</tr>
			</table>
			<asp:Button ID="btnNew" Runat="server" Text="New Template" CausesValidation="False" />
			<asp:DataGrid Runat="server" ID="grdTemplate" AutoGenerateColumns="False">
				<Columns>
					<asp:TemplateColumn HeaderText="Icon">
						<HeaderStyle HorizontalAlign="Center" Width="110px" />
						<ItemStyle HorizontalAlign="Center" />
						<ItemTemplate>
							<asp:Image ID="imgIcon" Runat="server" BorderStyle="Solid" BorderWidth="1" BorderColor="#d4d0c8"
								CssClass="imageIcon" />
						</ItemTemplate>
						<EditItemTemplate>
							<input ID="ctlIconFile" Runat="server" type="file" size="20" NAME="ctlIconFile"/>
							<asp:RequiredFieldValidator
							    ID="vldIconRequired" Runat="server"
                                ControlToValidate="ctlIconFile"
                                Display="Dynamic"
                                ErrorMessage="Icon&nbsp;is&nbsp;requred" />
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Label">
						<HeaderStyle HorizontalAlign="Center" Width="110px" />
						<ItemStyle HorizontalAlign="Center" />
						<ItemTemplate>
							<asp:Label Runat="server" ID="lblFaceName" />
						</ItemTemplate>
						<EditItemTemplate />
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Project">
						<HeaderStyle HorizontalAlign="Center" Width="110px" />
						<ItemStyle HorizontalAlign="Center" />
						<ItemTemplate>
							<asp:Label Runat="server" ID="lblName" />
						</ItemTemplate>
						<EditItemTemplate>
							<input ID="ctlProjectFile" Runat="server" type="file" size="20" NAME="ctlProjectFile"/>
							<asp:RequiredFieldValidator
							    ID="vldProjectFileRequired" Runat="server"
                                ControlToValidate="ctlProjectFile"
                                Display="Dynamic"
                                ErrorMessage="Project&nbsp;file&nbsp;is&nbsp;requred" />
						</EditItemTemplate>
					</asp:TemplateColumn>

					<asp:TemplateColumn HeaderText="Action">
						<ItemStyle CssClass="Action" />
						<ItemTemplate>
							<asp:Button Runat="server" ID="btnEdit" CommandName="Edit" CausesValidation="False"
								Text="Edit" Tooltip="Edit Template" />
							<!--<asp:Button Runat="server" ID="btnEditTemplateItems" CommandName="EditTemplateItems" CausesValidation="False"
								Text="Edit default text" Tooltip="Edit default text" />-->
							<asp:Button Runat="server" ID="btnDelete" CommandName="Delete"
								CausesValidation="False" Text="Remove" Tooltip="Remove Template" />
						</ItemTemplate>
						<EditItemTemplate>
							<asp:Button Runat="server" ID="btnUpdate" Text="Save" CommandName="Update" CausesValidation="True"
								Tooltip="Save Template" />
							<asp:Button Runat="server" ID="btnCancel" CommandName="Cancel" Text="Cancel"
								CausesValidation="False" Tooltip="Cancel" />
						</EditItemTemplate>
						<HeaderStyle Wrap="False" HorizontalAlign="Center" />
					</asp:TemplateColumn>
				</Columns>
			</asp:DataGrid>
		</asp:Panel>
		<!--<asp:Panel Runat="server" ID="panTemplateItem" Visible="False">
			<asp:DataGrid Runat="server" ID="grdFaceTemplateItem" AutoGenerateColumns="False">
				<Columns>
					<asp:TemplateColumn HeaderText="Default text" HeaderStyle-HorizontalAlign="Center">
						<ItemTemplate>
							<asp:TextBox Runat="server" ID="txtFaceTemplateItem" Width="200px" />
							<div>
								<asp:RequiredFieldValidator Runat="server" ID="vldFaceTemplateItem" ControlToValidate="txtFaceTemplateItem" ErrorMessage="Default text is required" Display="static" />
							</div>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:DataGrid>
			<asp:Button Runat="server" ID="btnSaveTemplateItems" Text="Save" />
			<asp:Button Runat="server" ID="btnCancelTemplateItems" Text="Cancel" CausesValidation="False" />
		</asp:Panel>-->
     </form>
	
  </body>
</html>
