using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebAdmin.Controls;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin {
    public class TrackingPaperReport : System.Web.UI.Page {
        protected DataGrid grdReport;
        protected Label lblTitle;
        protected Label lblStatus;
        protected DateInterval dtiDates;
        protected Button btnSubmit;

        #region Url
        private const string RawUrl = "TrackingPaperReport.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, System.EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }
		
        private void InitializeComponent() {    
            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(TrackingPaperReport_DataBinding);
            this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        }
        #endregion

        private void TrackingPaperReport_DataBinding(object sender, EventArgs e) {
            lblTitle.Text = "What papers have been chosen";
            lblStatus.Text = "There are no results matching your criteria.";
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            if (IsValid) {
                
                lblStatus.Visible = false;
                grdReport.Visible = false;

                DataSet data = BCTrackingPapers.GetListByDate(dtiDates.StartDate, dtiDates.EndDate);

                bool dataExisting = (data.Tables[0].Rows.Count > 0);

                if (dataExisting) {
                    grdReport.DataSource = data;
                }
                grdReport.Visible = dataExisting;
                lblStatus.Visible = !dataExisting;

                DataBind();
            }
        }
    }
}
