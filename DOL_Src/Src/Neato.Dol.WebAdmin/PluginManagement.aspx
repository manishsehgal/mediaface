<%@ Page language="c#" Codebehind="PluginManagement.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.PluginManagement" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Plugin Management</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
      <h3 align="center">Plugin Management</h3>
	  <asp:Panel ID="panContent" Runat="server">
	    <asp:DropDownList ID="cboTargetOS" AutoPostBack="true" Runat="server"/>
		<br>Additional files:<br>
		<input ID="ctlAdditionalFile" Runat="server" type="file" size="20"/><br>
		<asp:Button ID="btnAddFile2Root" Runat="server" Text="Add to root" />
		<asp:Button ID="btnAddFile2Modules" Runat="server" Text="Add to modules" />
		<br>
		<asp:ListBox ID="lstAdditionalFiles" Runat="server" Rows="6" style="width:300px" AutoPostBack="True"/>
		<asp:Button ID="btnDelFile" Runat="server" Text="Delete" />
		<br><br>Modules:<br>
		<asp:DataGrid
			Runat="server"
			ID="grdModules"
			AutoGenerateColumns="False">
			<Columns>
			<asp:TemplateColumn HeaderText="Name">
				<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="150px" />
				<ItemTemplate>
					<asp:Label ID="lblName" Runat="server" />
				</ItemTemplate>
				<EditItemTemplate>
					<asp:TextBox ID="txtName" Runat="server" MaxLength="100" style="width:100%" />
				</EditItemTemplate>
			</asp:TemplateColumn>
			<asp:TemplateColumn HeaderText="Description">
				<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="150px" />
				<ItemTemplate>
					<asp:Label ID="lblDescription" Runat="server" />
				</ItemTemplate>
				<EditItemTemplate>
					<asp:TextBox ID="txtDescription" Runat="server" MaxLength="100" style="width:100%" />
				</EditItemTemplate>
			</asp:TemplateColumn>
			<asp:TemplateColumn HeaderText="File Name">
				<HeaderStyle Wrap="False" HorizontalAlign="Center" />
				<ItemTemplate>
					<asp:Label ID="lblFileName" Runat="server" />
				</ItemTemplate>
				<EditItemTemplate>
					<asp:DropDownList ID="cboFileName" Runat="server" />
				</EditItemTemplate>
			</asp:TemplateColumn>
			<asp:TemplateColumn HeaderText="Action">
				<ItemTemplate>
				<asp:ImageButton
					Runat="server"
					ID="btnItemEdit"
					ImageUrl="images/edit.gif"
					CommandName="Edit"
					CausesValidation="False"
					ImageAlign="AbsMiddle"
					AlternateText="Edit" />
				<asp:ImageButton
					Runat="server"
					ID="btnItemUpdate"
					ImageUrl="images/save.gif"
					ImageAlign="AbsMiddle"
					CommandName="Update"
					CausesValidation="False"
					AlternateText="Save" />
				<asp:ImageButton
					Runat="server"
					ID="btnItemCancel"
					ImageUrl="images/cancel.gif"
					ImageAlign="AbsMiddle"
					CommandName="Cancel"
					CausesValidation="False"
					AlternateText="Cancel" />
				<asp:ImageButton
					Runat="server"
					ID="btnItemDelete"
					ImageUrl="images/remove.gif"
					ImageAlign="AbsMiddle"
					CommandName="Delete"
					CausesValidation="False"
					AlternateText="Remove" />
				</ItemTemplate>
				<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="100px"/>
				<ItemStyle Wrap="False"/>
			</asp:TemplateColumn>
			</Columns>
		</asp:DataGrid>
	  </asp:Panel>
	  <asp:Label ID="lblClosed" Runat="server" Visible="False" />
    </form>
  </body>
</html>
