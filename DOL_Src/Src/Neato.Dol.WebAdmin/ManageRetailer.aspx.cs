using System;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebAdmin.Helpers;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin {
    public class ManageRetailer : Page {
        private const string LblNameId = "lblName";
        private const string LblDisplayNameId = "lblDisplayName";
        private const string LblUrlId = "lblUrl";
        private const string ImgIconId = "imgIcon";
        private const string CtlIconFileId = "ctlIconFile";
        private const string TxtNameId = "txtName";
        private const string TxtDisplayNameId = "txtDisplayName";
        private const string TxtUrlId = "txtUrl";
        private const string ChkHasExitPageViewId = "chkHasExitPageView";
        private const string ChkHasExitPageEditId = "chkHasExitPageEdit";
        private const string BtnDeleteId = "btnDelete";
        private const string LblDefaultRetailerId = "lblDefaultRetailer";

        private const string VldExistNameId = "vldExistName";
        protected Button btnNew;
        protected DataGrid grdRetailers;

        private bool NewRetailerMode {
            get { return (bool)ViewState["NewRetailerModeKey"]; }
            set { ViewState["NewRetailerModeKey"] = value; }
        }

        #region Url
        private const string RawUrl = "ManageRetailer.aspx";
        private const string RefreshMenuKey = "RefreshMenu";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri Url(bool refreshMenu) {
            return UrlHelper.BuildUrl(RawUrl, RefreshMenuKey, refreshMenu);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                NewRetailerMode = false;
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManageRetailers_DataBinding);
            grdRetailers.ItemDataBound += new DataGridItemEventHandler(grdRetailers_ItemDataBound);
            grdRetailers.EditCommand += new DataGridCommandEventHandler(grdRetailers_EditCommand);
            grdRetailers.DeleteCommand += new DataGridCommandEventHandler(grdRetailers_DeleteCommand);
            grdRetailers.UpdateCommand += new DataGridCommandEventHandler(grdRetailers_UpdateCommand);
            grdRetailers.CancelCommand += new DataGridCommandEventHandler(grdRetailers_CancelCommand);
            //grdRetailers.ItemCreated += new DataGridItemEventHandler(grdRetailers_ItemCreated);
            grdRetailers.ItemCommand += new DataGridCommandEventHandler(grdRetailers_ItemCommand);
            btnNew.Click += new EventHandler(btnNew_Click);
        }
        #endregion

        private void ManageRetailers_DataBinding(object sender, EventArgs e) {
            ArrayList retailers = new ArrayList(BCRetailer.Enum());
            if (NewRetailerMode) {
                retailers.Insert(0, new Retailer());
                grdRetailers.EditItemIndex = 0;
            } else if (grdRetailers.EditItemIndex >= 0) {
                int retailerId = (int)grdRetailers.DataKeys[grdRetailers.EditItemIndex];
                Retailer tmpRetailer = new Retailer();
                tmpRetailer.Id = retailerId;
                int index = retailers.IndexOf(tmpRetailer);
                grdRetailers.EditItemIndex = index;
            }
            grdRetailers.DataSource = retailers;
            grdRetailers.DataKeyField = Retailer.IdField;
        }

        private void grdRetailers_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void grdRetailers_EditCommand(object source, DataGridCommandEventArgs e) {
            grdRetailers.EditItemIndex = e.Item.ItemIndex;
            NewRetailerMode = false;
            DataBind();
        }

        private void grdRetailers_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdRetailers.EditItemIndex = -1;
            int retailerId = (int)grdRetailers.DataKeys[e.Item.ItemIndex];
            try {
                BCRetailer.Delete(new Retailer(retailerId));
                Response.Redirect(Url(true).PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewRetailerMode = false;
            DataBind();
        }

        private void grdRetailers_UpdateCommand(object source, DataGridCommandEventArgs e) {
            Page.Validate();
            if (!IsValid) return;

            int retailerId = (int)grdRetailers.DataKeys[e.Item.ItemIndex];
            Retailer retailer;
            if (NewRetailerMode)
                retailer = new Retailer(retailerId);
            else
                retailer= BCRetailer.Get(retailerId);
            TextBox txtName = (TextBox)e.Item.FindControl(TxtNameId);
            TextBox txtDisplayName = (TextBox)e.Item.FindControl(TxtDisplayNameId);

            retailer.Name = txtName.Text.Trim();
            retailer.DisplayName = txtDisplayName.Text;

            TextBox txtUrl = (TextBox)e.Item.FindControl(TxtUrlId);
            retailer.Url = txtUrl.Text.Trim();
            //CheckBox chkHasExitPageEdit = (CheckBox)e.Item.FindControl(ChkHasExitPageEditId);
            //retailer.HasExitPage = chkHasExitPageEdit.Checked;
            retailer.HasExitPage = true;

            CustomValidator vldExistName = (CustomValidator)e.Item.FindControl(VldExistNameId);
            HtmlInputFile ctlIconFile = (HtmlInputFile)e.Item.FindControl(CtlIconFileId);
            Stream icon = ctlIconFile.PostedFile.InputStream;
            if (icon.Length > 0)
                retailer.Icon = ConvertHelper.StreamToByteArray(icon);

            vldExistName.IsValid = true;

            try {
                if (NewRetailerMode) {
                    if (BCRetailer.IsUniqueRetailerName(retailer.Name))
                        BCRetailer.Insert(retailer);
                    else {
                        vldExistName.IsValid = false;
                        return;
                    }
                } else {
                    if ((BCRetailer.GetId(retailer.Name) != retailer.Id && !BCRetailer.IsUniqueRetailerName(retailer.Name))) {
                        vldExistName.IsValid = false;
                        return;
                    }
                    BCRetailer.Update(retailer);
                }
                Response.Redirect(Url(true).PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }

            grdRetailers.EditItemIndex = -1;
            NewRetailerMode = false;
            DataBind();
        }

        private void grdRetailers_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdRetailers.EditItemIndex = -1;
            NewRetailerMode = false;
            DataBind();
        }

        private void ItemDataBound(DataGridItem item) {
            Retailer retailer = (Retailer)item.DataItem;
            Label lblName = (Label)item.FindControl(LblNameId);
            Label lblDisplayName = (Label)item.FindControl(LblDisplayNameId);
            Label lblUrl = (Label)item.FindControl(LblUrlId);
            lblName.Text = HttpUtility.HtmlEncode(retailer.Name);
            lblDisplayName.Text = HttpUtility.HtmlEncode(retailer.DisplayName);
            lblUrl.Text = HttpUtility.HtmlEncode(retailer.Url);
            //CheckBox chkHasExitPageView = (CheckBox)item.FindControl(ChkHasExitPageViewId);
            //chkHasExitPageView.Checked = retailer.HasExitPage;
            Image imgIcon = (Image)item.FindControl(ImgIconId);

            string imageUrl = IconHandler.ImageUrl(retailer).AbsoluteUri;
            imgIcon.ImageUrl = imageUrl;

            ImageButton btnDelete = (ImageButton)item.FindControl(BtnDeleteId);
            string warning = "All customer accounts relating to the retailer will be deleted.\nAre you sure to continue?";
            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", warning);

            Label lblDefaultRetailer = (Label)item.FindControl(LblDefaultRetailerId);
            if (retailer.Id == BCRetailer.defaultId) {
                btnDelete.Visible = false;
                lblDefaultRetailer.Visible = true;
            }

        }

        private void EditItemDataBound(DataGridItem item) {
            Retailer retailer = (Retailer)item.DataItem;
            TextBox txtName = (TextBox)item.FindControl(TxtNameId);
            TextBox txtDisplayName = (TextBox)item.FindControl(TxtDisplayNameId);
            TextBox txtUrl = (TextBox)item.FindControl(TxtUrlId);
            txtName.Text = retailer.Name;
            txtDisplayName.Text = retailer.DisplayName;
            txtUrl.Text = retailer.Url;
            //CheckBox chkHasExitPageEdit = (CheckBox)item.FindControl(ChkHasExitPageEditId);
            //chkHasExitPageEdit.Checked = retailer.HasExitPage;

            JavaScriptHelper.RegisterFocusScript(this, txtName.ClientID);
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewRetailerMode = true;
            DataBind();
        }

        private void grdRetailers_ItemCreated(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            if (item.ItemType != ListItemType.EditItem)
                return;

            GridTransformationHelper.EditItemCreationForm(item, grdRetailers.Columns, 6);
        }

        private void grdRetailers_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName == "DeleteIcon") {
                int retailerId = (int)grdRetailers.DataKeys[e.Item.ItemIndex];
                Retailer retailer = BCRetailer.Get(retailerId);
                if (retailer != null) {
                    try {
                        retailer.Icon = new byte[0];
                        BCRetailer.Update(retailer);
                    } catch (BaseBusinessException ex) {
                        MsgBox.Alert(this, ex.Message);
                    }
                }
            }
        }
    }
}