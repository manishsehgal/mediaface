<%@ Page language="c#" Codebehind="PreviewHtmlManagement.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.PreviewHtmlManagement" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>PreviewHtmlManagement</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <script type="text/javascript" language="javascript" src="js/Preview.js"></script>
  </head>
  <body MS_POSITIONING="GridLayout">
	
	<form id="Form1" method="post" runat="server">
		<input type="hidden" id="ctlBaseUrl" runat="server" NAME="ctlBaseUrl"/>
		<h3 align="center">Preview Html Management</h3>
		<asp:Panel ID="panContent" Runat="server">
			<br><br>User&nbsp;:
			<asp:DropDownList ID="cboBanner" Runat="server" AutoPostBack="True" style="width: 150px;" />
			<!--<br>Images&nbsp;:
			<input ID="ctlImageFile" Runat="server" type="file" size="20" NAME="ctlImageFile"/>
			<asp:Button ID="btnAddImage" Runat="server" Text="Add" />
			<asp:Button ID="btnDelImage" Runat="server" Text="Delete" />
			<br>
			<asp:ListBox ID="lstBannerImages" Runat="server" Rows="5" style="width:300px" AutoPostBack="True"/>
			<img ID="imgBannerImage" runat="server" align="top" />
			-->
			<br>Html text:<br>
			<asp:TextBox ID="txtText" Runat="server" TextMode="MultiLine" Rows="10" MaxLength="2048" style="width:100%"></asp:TextBox>
			<br><br>
			<!--<button ID="btnPreview" onclick="javascript:PreviewPage()">Preview</button>-->
			<asp:Button ID="btnSubmit" Runat="server" Text="Save Html text"/>
			<br><br>
			<!--<iframe Runat="server" ID="ctlPreviewContaner" align="absmiddle" frameborder="yes" scrolling="auto" style="width:100%;height:615px;"/>-->
		</asp:Panel>
		<asp:Label ID="lblClosed" Runat="server" Visible="False" />
     </form>
	
  </body>
</html>
