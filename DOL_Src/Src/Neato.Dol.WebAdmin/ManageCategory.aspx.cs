using System;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin {
    public class ManageCategory : Page {
        private const string LblNameId = "lblName";
        private const string ImgIconId = "imgIcon";
        private const string CtlIconFileId = "ctlIconFile";
        private const string TxtNameId = "txtName";
        private const string BtnDeleteId = "btnDelete";
        private const string chkVisibleId = "chkVisible";
        private const string litTargetPageId = "litTargetPage";
        private const string cboTargetPageId = "cboTargetPage";
        protected Button btnNew;
        protected DataGrid grdCategorys;

        private bool NewCategoryMode {
            get { return (bool)ViewState["NewCategoryModeKey"]; }
            set { ViewState["NewCategoryModeKey"] = value; }
        }

        #region Url
        private const string RawUrl = "ManageCategory.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                NewCategoryMode = false;
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManageCategory_DataBinding);
            grdCategorys.ItemDataBound += new DataGridItemEventHandler(grdCategorys_ItemDataBound);
            grdCategorys.EditCommand += new DataGridCommandEventHandler(grdCategorys_EditCommand);
            grdCategorys.DeleteCommand += new DataGridCommandEventHandler(grdCategorys_DeleteCommand);
            grdCategorys.UpdateCommand += new DataGridCommandEventHandler(grdCategorys_UpdateCommand);
            grdCategorys.CancelCommand += new DataGridCommandEventHandler(grdCategorys_CancelCommand);
            grdCategorys.ItemCommand += new DataGridCommandEventHandler(grdCategorys_ItemCommand);
            btnNew.Click += new EventHandler(btnNew_Click);
        }
        #endregion

        private void ManageCategory_DataBinding(object sender, EventArgs e) {
            ArrayList categories = new ArrayList(BCCategory.GetCategoryList());
            if (NewCategoryMode) {
                Category category = BCCategory.NewCategory();
                categories.Add(category);
                grdCategorys.EditItemIndex = categories.Count - 1;
            } else if (grdCategorys.EditItemIndex >= 0) {
                int categoryId = (int)grdCategorys.DataKeys[grdCategorys.EditItemIndex];
                int index = categories.IndexOf(new CategoryBase(categoryId));
                grdCategorys.EditItemIndex = index;
            }
            grdCategorys.DataSource = categories;
            grdCategorys.DataKeyField = Category.IdField;
        }

        private void grdCategorys_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void grdCategorys_EditCommand(object source, DataGridCommandEventArgs e) {
            grdCategorys.EditItemIndex = e.Item.ItemIndex;
            NewCategoryMode = false;
            DataBind();
        }

        private void grdCategorys_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdCategorys.EditItemIndex = -1;
            int categoryId = (int)grdCategorys.DataKeys[e.Item.ItemIndex];
            CategoryBase category = new CategoryBase(categoryId);
            try {
                BCCategory.Delete(category);
                Response.Redirect(Url().PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewCategoryMode = false;
            DataBind();
        }

        private void grdCategorys_UpdateCommand(object source, DataGridCommandEventArgs e) {
            Page.Validate();
            if (!IsValid) return;

            int prevSortOrder = 0;
            if (e.Item.ItemIndex > 0) {
                int prevItemId = (int)grdCategorys.DataKeys[e.Item.ItemIndex - 1];
                Category prevCategory = BCCategory.GetCategory(new CategoryBase(prevItemId));
                prevSortOrder = prevCategory.SortOrder;
            }

            Category category = BCCategory.NewCategory();
            category.Id = (int)grdCategorys.DataKeys[e.Item.ItemIndex];

            TextBox txtName = (TextBox)e.Item.FindControl(TxtNameId);
            CheckBox chkVisible = (CheckBox)e.Item.FindControl(chkVisibleId);
            DropDownList cboTargetPage = (DropDownList)e.Item.FindControl(cboTargetPageId);
            
            category.Name = txtName.Text.Trim();
            category.Visible = chkVisible.Checked;
            category.TargetPage = cboTargetPage.SelectedIndex + 1;
            category.SortOrder = prevSortOrder + 1;
            HtmlInputFile ctlIconFile = (HtmlInputFile)e.Item.FindControl(CtlIconFileId);
            Stream icon = ctlIconFile.PostedFile.InputStream;

            try {
                BCCategory.Save(category, icon);
                Response.Redirect(Url().PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }

            grdCategorys.EditItemIndex = -1;
            NewCategoryMode = false;
            DataBind();
        }

        private void grdCategorys_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdCategorys.EditItemIndex = -1;
            NewCategoryMode = false;
            DataBind();
        }

        private void grdCategorys_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (!(e.CommandName == "Up" || e.CommandName == "Down")) { return; }
            
            int itemIndex = e.Item.ItemIndex;
            Category[] categories = BCCategory.GetCategoryList();

            if ((e.CommandName == "Up" && itemIndex == 0) ||
                (e.CommandName == "Down" && itemIndex == categories.Length - 1)) {
                return;
            }

            int step = (e.CommandName == "Up") ? -1 : +1;
                
            int curCategoryId = (int)grdCategorys.DataKeys[itemIndex];
            int nextCategoryId = (int)grdCategorys.DataKeys[itemIndex + step];
                
            Category curCategory = BCCategory.GetCategory(new CategoryBase(curCategoryId));
            Category nextCategory = BCCategory.GetCategory(new CategoryBase(nextCategoryId));

            int order = curCategory.SortOrder;
            curCategory.SortOrder = nextCategory.SortOrder;
            nextCategory.SortOrder = order; 
            
            BCCategory.Save(curCategory);
            BCCategory.Save(nextCategory);
            DataBind();
        }

        private void ItemDataBound(DataGridItem item) {
            Category category = (Category)item.DataItem;
            Label lblName = (Label)item.FindControl(LblNameId);
            CheckBox chkVisible = (CheckBox)item.FindControl(chkVisibleId);
            Image imgIcon = (Image)item.FindControl(ImgIconId);
            Literal litTargetPage = (Literal)item.FindControl(litTargetPageId);

            lblName.Text = HttpUtility.HtmlEncode(category.Name);
            chkVisible.Checked = category.Visible;
            litTargetPage.Text = SelectPage.GetReadableNameById(category.TargetPage);
            imgIcon.ImageUrl = IconHandler.ImageUrl(category).AbsoluteUri;

            ImageButton btnDelete = (ImageButton)item.FindControl(BtnDeleteId);
            btnDelete.Visible = (category.Id > (int)DeviceType.OtherLabels);
            string warning = "Selected category and all assigned devices will be deleted." + Environment.NewLine + "Are you sure?";
            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", warning);
        }

        private void EditItemDataBound(DataGridItem item) {
            Category category = (Category)item.DataItem;
            TextBox txtName = (TextBox)item.FindControl(TxtNameId);
            CheckBox chkVisible = (CheckBox)item.FindControl(chkVisibleId);
            DropDownList cboTargetPage = (DropDownList)item.FindControl(cboTargetPageId);
            foreach (string pagename in SelectPage.GetPageNames()) {
                cboTargetPage.Items.Add(pagename);
            }
            cboTargetPage.SelectedIndex = category.TargetPage - 1;
            cboTargetPage.Enabled = !(category.IsNew || category.Id > (int)DeviceType.OtherLabels);
            txtName.Text = category.Name;
            chkVisible.Checked = category.Visible;
            JavaScriptHelper.RegisterFocusScript(this, txtName.ClientID);
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewCategoryMode = true;
            DataBind();
        }
        
    }
}