<%@ Page language="c#" Codebehind="Parameters.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.Parameters" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Parameters</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
	
    <form id="Form1" method="post" runat="server">
    
        <h3 align="center">Parameters</h3>
        <br><br>
        <asp:DataGrid ID="grdParameters" Runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateColumn HeaderText="Key" HeaderStyle-HorizontalAlign="Center">
                    <HeaderStyle Wrap="False"/>
                    <ItemTemplate>
                        <asp:Label ID="lblKey" Runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Value">
                    <HeaderStyle Wrap="False" HorizontalAlign="Center"/>
                    <ItemTemplate>
                        <asp:TextBox ID="txtValue" Runat="server" style="width:500px" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Action">
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="150px"/>
                    <ItemTemplate>
                        <asp:ImageButton
                            Runat="server"
                            ID="btnItemEdit"
                            ImageUrl="images/edit.gif"
                            CommandName="Edit"
                            CausesValidation="False"
                            AlternateText="Edit" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:ImageButton
                            Runat="server"
                            ID="btnItemUpdate"
                            ImageUrl="images/save.gif"
                            CommandName="Update"
                            CausesValidation="False"
                            AlternateText="Save" />
                        <asp:ImageButton
                            Runat="server"
                            ID="btnItemCancel"
                            ImageUrl="images/cancel.gif"
                            CommandName="Cancel"
                            CausesValidation="False"
                            AlternateText="Cancel" />
                    </EditItemTemplate>
                <ItemStyle Wrap="False" HorizontalAlign="Center"/>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
     </form>
  </body>
</html>