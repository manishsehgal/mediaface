using System;
using System.Collections;
using System.Reflection;

namespace Neato.Dol.WebAdmin.Helpers {
    public sealed class PublicSitePageHelper {
        private PublicSitePageHelper() {}

        public static string[] GetPublicSitePages() {
            ArrayList pages = new ArrayList();

            Assembly WebDesignerAssembly = Neato.Dol.WebDesigner.Global.GetAssembly();
            Type[] types = WebDesignerAssembly.GetExportedTypes();
            foreach(Type type in types) {
                if (type.Namespace != "Neato.Dol.WebDesigner")
                    continue;

                FieldInfo fieldInfo = type.GetField("RawUrl", BindingFlags.NonPublic|BindingFlags.Static);
                if (fieldInfo == null)
                    continue;

                string baseType = type.BaseType.Name;
                if (baseType != "BasePage2")
                    continue;

                pages.Add(fieldInfo.GetValue(null));
            }

            return (string[])pages.ToArray(typeof(string));
        }
    }
}