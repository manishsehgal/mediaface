using System;
using System.Collections;
using System.Net;
using System.Reflection;

using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.HttpHandlers;
using Neato.Dol.WebDesigner.HttpModules;

namespace Neato.Dol.WebAdmin.Helpers {
    public sealed class DynamicLinkHelper {
        private static string ResetDynamicLinksUrl = CheckLocationModule.AdminUrl(Configuration.IntranetDesignerSiteUrl + DynamicLinkHandler.PageName());
        private DynamicLinkHelper() {}

        public static void ResetDesignerDynamicLinks() {
            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(ResetDynamicLinksUrl);
            WebResponse response= request.GetResponse();
            response.Close();
        }

        public static string[] GetMenuPagesWithFooter(string className) {
            ArrayList pages = new ArrayList();

            Assembly WebDesignerAssembly = Neato.Dol.WebDesigner.Global.GetAssembly();
            Type[] types = WebDesignerAssembly.GetExportedTypes();
            foreach(Type type in types) {
                if (type.Namespace != "Neato.Dol.WebDesigner")
                    continue;

                FieldInfo fieldInfo = type.GetField("RawUrl", BindingFlags.NonPublic|BindingFlags.Static);
                if (fieldInfo == null)
                    continue;

                bool hasMenu = false;
                FieldInfo[] fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
                foreach(FieldInfo field in fields) {
                    if (field.FieldType.Name == className) {
                        hasMenu = true;
                        break;
                    }
                }

                if (!hasMenu)
                    continue;

                pages.Add(fieldInfo.GetValue(null));
            }

            return (string[])pages.ToArray(typeof(string));
        }
    }
}