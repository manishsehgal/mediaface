using System.Collections;

namespace Neato.Dol.WebAdmin.Helpers {
    public sealed class IndicesHelper {
        public static ICollection GetIndices(ArrayList allDevices, ArrayList devices) {
            ArrayList indices = new ArrayList();
            foreach (object device in devices) {
                int index = allDevices.IndexOf(device);
                if (index >= 0) {
                    indices.Add(index);
                }
            }
            return indices;
        }
    }
}