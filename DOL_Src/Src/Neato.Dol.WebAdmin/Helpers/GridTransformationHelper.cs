using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Neato.Dol.WebAdmin.Helpers  {
    public sealed class GridTransformationHelper {

        public static void EditItemCreationForm(DataGridItem item, DataGridColumnCollection columns, int count) {
            EditItemCreationForm(item, columns, count, "80%");
        }

        public static void EditItemCreationForm(DataGridItem item, DataGridColumnCollection columns, int count, string tableWidth) {
            HtmlTable table = new HtmlTable();
            table.Style["width"] = tableWidth;

            for(int i = 0; i < count; ++i) {
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell fieldNameCell = new HtmlTableCell();
                HtmlTableCell contentCell = new HtmlTableCell();
                
                Literal fieldName = new Literal();
                fieldName.Text = columns[i].HeaderText;

                fieldNameCell.Controls.Add(fieldName);
                ControlCollection controls = item.Cells[i].Controls;
                for(int j = controls.Count - 1; j >= 0; --j)
                    contentCell.Controls.AddAt(0, controls[j]);
                
                row.Cells.Add(fieldNameCell);
                row.Cells.Add(contentCell);
                if(contentCell.Controls.Count != 0)
                    table.Rows.Add(row);
            }

            item.Cells[0].Controls.Add(table);
            item.Cells[0].ColumnSpan = count;

            for(int i = 1; i < count; ++i)
                item.Cells.RemoveAt(1);
        }
    }
}