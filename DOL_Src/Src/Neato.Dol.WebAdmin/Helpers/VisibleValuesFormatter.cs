using System;
using System.Globalization;

namespace Neato.Dol.WebAdmin.Helpers {

    /// <summary>
    /// Helper functions for values formattion and parsing on the user interface
    /// </summary>
    public sealed class VisibleValuesFormatter {
        private const string PercentFormat = "0.00000";
        private const string MoneyFormat = "###0.#####";
        private const string DateFormat = "MM/dd/yyyy";

        public const string DateFormatJS = DateFormat;

        private VisibleValuesFormatter() {}

        public static string MoneyToString(decimal number) {
            return number.ToString(MoneyFormat, NumberFormatInfo.InvariantInfo);
        }

        public static string MoneyToStringEmptyIfZero(decimal number) {
            return number == 0
                ? string.Empty
                : number.ToString(MoneyFormat, NumberFormatInfo.InvariantInfo);
        }

        public static string PercentToString(decimal number) {
            return number.ToString(PercentFormat, NumberFormatInfo.InvariantInfo);
        }

        public static decimal StringToMoney(string value) {
            return value.Trim().Length == 0 ? 0 : decimal.Parse(value, NumberFormatInfo.InvariantInfo);
        }

        public static decimal StringToPercent(string value) {
            return value.Trim().Length == 0 ? 0 : decimal.Parse(value, NumberFormatInfo.InvariantInfo);
        }

        public static string DateToString(DateTime date) {
            return date.ToString(DateFormat, DateTimeFormatInfo.InvariantInfo);
        }

        public static DateTime StringToDate(string value) {
            return DateTime.Parse(value, new CultureInfo("en-US"));
        }
    }
}
