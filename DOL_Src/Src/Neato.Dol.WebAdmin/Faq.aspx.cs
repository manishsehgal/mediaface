using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin {
    public class Faq : Page {
        #region Constants
        private const string TxtQuestionName = "txtQuestion";
        private const string TxtAnswerName = "txtAnswer";

        private const string FaqKey = "Faq";
        private const string DataKeyField = "Id";

        private const string BtnDeleteName = "btnItemDelete";
        #endregion

        protected Button btnNew;
        protected DataGrid grdFaq;
        
        private FaqData faqData {
            get { return (FaqData)ViewState[FaqKey]; }
            set { ViewState[FaqKey] = value; }
        }


        #region Url
        private const string RawUrl = "Faq.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                faqData = BCFaq.EnumFaq();
                DataBind();
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {    
            this.grdFaq.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdFaq_CancelCommand);
            this.grdFaq.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdFaq_EditCommand);
            this.grdFaq.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdFaq_UpdateCommand);
            this.grdFaq.DeleteCommand += new DataGridCommandEventHandler(grdFaq_DeleteCommand);
            this.grdFaq.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdFaq_ItemDataBound);
            this.grdFaq.ItemCommand += new DataGridCommandEventHandler(this.grdFaq_ItemCommand);
            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new System.EventHandler(this.Faq_DataBinding);
            this.btnNew.Click += new EventHandler(btnNew_Click);

        }
        #endregion

        #region Actions
        private void grdFaq_EditCommand(object source, DataGridCommandEventArgs e) {
            faqData.RejectChanges();
            grdFaq.EditItemIndex = e.Item.ItemIndex;
            DataBind();
        }

        private void grdFaq_UpdateCommand(object source, DataGridCommandEventArgs e) {
            Validate();
            if (!IsValid) return;

            TextBox txtQuestion = (TextBox)e.Item.FindControl(TxtQuestionName);
            TextBox txtAnswer = (TextBox)e.Item.FindControl(TxtAnswerName);

            int id = (int)grdFaq.DataKeys[e.Item.ItemIndex];
            FaqData.FaqRow row = faqData.Faq.FindById(id);
            
            int maxLength = Configuration.MaxFaqSectionLength;
            if (row != null) {
                string question =
                    (txtQuestion.Text.Length > maxLength)
                    ? txtQuestion.Text.Substring(0, maxLength)
                    : txtQuestion.Text;

                string answer =
                    (txtAnswer.Text.Length > maxLength)
                    ? txtAnswer.Text.Substring(0, maxLength)
                    : txtAnswer.Text;

                row.Question = question;
                row.Answer = answer;
                BCFaq.UpdateFaq(faqData);
            }

            grdFaq.EditItemIndex = -1;
            DataBind();
        }

        private void grdFaq_CancelCommand(object source, DataGridCommandEventArgs e) {
            faqData.RejectChanges();
            grdFaq.EditItemIndex = -1;
            DataBind();
        }

        private void grdFaq_DeleteCommand(object source, DataGridCommandEventArgs e) {
            int id = (int)grdFaq.DataKeys[e.Item.ItemIndex];
            FaqData.FaqRow row = faqData.Faq.FindById(id);

            if (row != null) {
                row.Delete();
                BCFaq.UpdateFaq(faqData);
                grdFaq.EditItemIndex = -1;
                DataBind();
            }
        }
        private void grdFaq_ItemCommand(object source, DataGridCommandEventArgs e) {
            int itemIndex = e.Item.ItemIndex;
            
            int step = 0;

            if (e.CommandName == "Up" && itemIndex > 0) {
                step = -1;
            } else if (e.CommandName == "Down" &&
                itemIndex < faqData.Faq.Count - 1) {
                step = +1;
            }

            if (step != 0) {
                string question = faqData.Faq[itemIndex].Question;
                string answer = faqData.Faq[itemIndex].Answer;
                
                faqData.Faq[itemIndex].Question =
                    faqData.Faq[itemIndex + step].Question;
                faqData.Faq[itemIndex].Answer =
                    faqData.Faq[itemIndex + step].Answer;

                faqData.Faq[itemIndex + step].Question = question;
                faqData.Faq[itemIndex + step].Answer = answer;

                BCFaq.UpdateFaq(faqData);
                DataBind();
            }
        }

        private void btnNew_Click(object sender, EventArgs e) {
            int count = faqData.Faq.Count;
            int sortOrder = 0;
            if (count > 0)
                sortOrder = faqData.Faq[count - 1].SortOrder;

            faqData.Faq.AddFaqRow("", "", sortOrder + 1);
            
            grdFaq.EditItemIndex = count;
            DataBind();
        }
        #endregion

        #region Data binding
        private void Faq_DataBinding(object sender, EventArgs e) {
            grdFaq.DataSource = faqData;
            grdFaq.DataKeyField = DataKeyField;
        }

        private void grdFaq_ItemDataBound(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;

            if (item.ItemType == ListItemType.Item ||
                item.ItemType == ListItemType.AlternatingItem ||
                item.ItemType == ListItemType.EditItem) {

                FaqData.FaqRow row = (FaqData.FaqRow)
                    ((DataRowView)item.DataItem).Row;

                bool editMode = (item.ItemType == ListItemType.EditItem);
            
                TextBox txtQuestion = (TextBox)item.FindControl(TxtQuestionName);
                TextBox txtAnswer = (TextBox)item.FindControl(TxtAnswerName);

                txtQuestion.Text = row.Question;
                txtAnswer.Text = row.Answer;

                txtQuestion.ReadOnly = txtAnswer.ReadOnly = !editMode;
                if(editMode)
                    JavaScriptHelper.RegisterFocusScript(this, txtQuestion.ClientID);

                ImageButton btnDeleteItem = (ImageButton)item.FindControl(BtnDeleteName);
                if (btnDeleteItem != null)
                    btnDeleteItem.Attributes["onclick"] = "return confirm('Are you sure to delete this item?')";
            }
        }
        #endregion
    }
}