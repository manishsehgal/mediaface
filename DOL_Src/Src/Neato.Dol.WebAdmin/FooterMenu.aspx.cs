using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.WebAdmin.Helpers;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin {
    public class FooterMenu : Page {
        #region Constants
        private const string LinkItemsName = "LINKITEMS";

        private const string LblLinkName = "lblLinkName";
        private const string TxtLinkName = "txtLinkName";
        private const string LblLinkUrl = "lblLinkUrl";
        private const string TxtLinkUrl = "txtLinkUrl";
        private const string LblAlign = "lblAlign";
        private const string CboAlign = "cboAlign";
        private const string ChkIsNewWindow = "chkIsNewWindow";
        private const string ChkIsVisible = "chkIsVisible";

        private const string BtnItemDeleteName = "btnItemDelete";
        private const string BtnItemEditName = "btnItemEdit";
        private const string BtnItemUpdateName = "btnItemUpdate";
        private const string BtnItemCancelName = "btnItemCancel";

        private const string SetFocusScriptKey = "SET_FOCUS_SCRIPT_KEY";
        private const string SetFocusScript = "<script language='javascript'>var element = document.getElementById('{0}');element.focus();element.select();</script>";
        #endregion

        protected DropDownList cboPages;
        protected DataGrid grdLinks;
        protected Button btnNew;
        protected CheckBox chkUseDefaultMenu;

        private ArrayList LinkItems {
            get { return (ArrayList)ViewState[LinkItemsName]; }
            set { ViewState[LinkItemsName] = value; }
        }

        #region Url
        private const string RawUrl = "FooterMenu.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private int pageId {
            get {return BCDynamicLink.GetPageIdFromPageUrl(cboPages.SelectedValue);}
        }

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(FooterMenu_DataBinding);
            this.cboPages.SelectedIndexChanged += new EventHandler(cboPages_SelectedIndexChanged);
            this.grdLinks.DataBinding += new EventHandler(grdLinks_DataBinding);
            this.grdLinks.ItemDataBound += new DataGridItemEventHandler(grdLinks_ItemDataBound);
            this.grdLinks.EditCommand += new DataGridCommandEventHandler(grdLinks_EditCommand);
            this.grdLinks.UpdateCommand += new DataGridCommandEventHandler(grdLinks_UpdateCommand);
            this.grdLinks.CancelCommand += new DataGridCommandEventHandler(grdLinks_CancelCommand);
            this.grdLinks.DeleteCommand += new DataGridCommandEventHandler(grdLinks_DeleteCommand);
            this.btnNew.Click += new EventHandler(btnNew_Click);
            this.chkUseDefaultMenu.CheckedChanged += new EventHandler(chkUseDefaultMenu_CheckedChanged);
        }
        #endregion

        private void FooterMenu_DataBinding(object sender, EventArgs e) {
            string[] pages = DynamicLinkHelper.GetMenuPagesWithFooter("Footer");
            cboPages.Items.Clear();
            cboPages.Items.Add(new ListItem("Default Footer", BCDynamicLink.DefaultMenuPage));
            foreach (string page in pages) {
                cboPages.Items.Add(new ListItem(page, page));
            }
            cboPages.SelectedIndex = 0;

            LinkItems = new ArrayList();
            LinkItems.AddRange(BCDynamicLink.GetLinks(pageId, Place.Footer, BCDynamicLink.DefaultCulture));
            chkUseDefaultMenu.Visible = false;
        }

        private void chkUseDefaultMenu_CheckedChanged(object sender, EventArgs e) {
            bool useDefaultMenu = chkUseDefaultMenu.Checked;
            btnNew.Visible = !useDefaultMenu;
            grdLinks.Visible = !useDefaultMenu;

            if (!chkUseDefaultMenu.Visible)
                return;

            if (useDefaultMenu) {
                BCDynamicLink.DynamicLinkDelByPageId(pageId);
                LinkItems = new ArrayList();
                grdLinks.DataBind();
            } else {
                BCDynamicLink.AddPage(cboPages.SelectedItem.Text);
            }
        }

        private void cboPages_SelectedIndexChanged(object sender, EventArgs e) {
            LinkItems = new ArrayList();
            LinkItems.AddRange(BCDynamicLink.GetLinks(pageId, Place.Footer, BCDynamicLink.DefaultCulture));
            grdLinks.EditItemIndex = -1;
            grdLinks.DataBind();
            chkUseDefaultMenu.Visible = cboPages.SelectedValue != BCDynamicLink.DefaultPageId.ToString();
            chkUseDefaultMenu.Checked = LinkItems.Count == 0;
            chkUseDefaultMenu_CheckedChanged(null, null);
        }

        private void grdLinks_DataBinding(object sender, EventArgs e) {
            grdLinks.DataSource = LinkItems;
        }

        private void grdLinks_ItemDataBound(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            switch (item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.EditItem:
                    ItemBound(e.Item);
                    break;
            }
        }

        private void ItemBound(DataGridItem item) {
            LinkItem link = (LinkItem)LinkItems[item.ItemIndex];

            ImageButton btnEdit = (ImageButton)item.FindControl(BtnItemEditName);
            ImageButton btnUpdate = (ImageButton)item.FindControl(BtnItemUpdateName);
            ImageButton btnCancel = (ImageButton)item.FindControl(BtnItemCancelName);
            ImageButton btnDelete = (ImageButton)item.FindControl(BtnItemDeleteName);

            Label lblLinkName = (Label)item.FindControl(LblLinkName);
            TextBox txtLinkName = (TextBox)item.FindControl(TxtLinkName);
            Label lblLinkUrl = (Label)item.FindControl(LblLinkUrl);
            TextBox txtLinkUrl = (TextBox)item.FindControl(TxtLinkUrl);
            Label lblAlign = (Label)item.FindControl(LblAlign);
            DropDownList cboAlign = (DropDownList)item.FindControl(CboAlign);
            CheckBox chkIsNewWindow = (CheckBox)item.FindControl(ChkIsNewWindow);
            CheckBox chkIsVisible = (CheckBox)item.FindControl(ChkIsVisible);

            bool edit = item.ItemType == ListItemType.EditItem;

            if (edit) {
                txtLinkName.Text = link.Text;
                if (!IsStartupScriptRegistered(SetFocusScriptKey))
                    RegisterStartupScript(SetFocusScriptKey, string.Format(SetFocusScript, txtLinkName.ClientID));
                txtLinkUrl.Text = link.Url;
                cboAlign.Items.Clear();
                cboAlign.Items.Add(Align.Left.ToString());
                cboAlign.Items.Add(Align.Right.ToString());
                try {
                    cboAlign.SelectedValue = link.Align.ToString();
                } catch {}
            } else {
                lblLinkName.Text = link.Text;
                lblLinkUrl.Text = link.Url;
                lblAlign.Text = link.Align.ToString();
            }

            chkIsNewWindow.Checked = link.IsNewWindow;
            chkIsNewWindow.Enabled = edit;
            chkIsVisible.Checked = link.IsVisible;
            chkIsVisible.Enabled = edit;
            btnEdit.Visible = !edit;
            btnDelete.Visible = !edit;
            btnUpdate.Visible = edit;
            btnCancel.Visible = edit;

            btnDelete.Attributes["onclick"] = "return confirm('Are you sure to delete this item?')";
        }

        private void btnNew_Click(object sender, EventArgs e) {
            LinkItem item = new LinkItem();
            item.Align = Align.Right;

            if (LinkItems.Count > 0) {
                LinkItem lastItem = (LinkItem)LinkItems[LinkItems.Count - 1];
                item.SortOrder = lastItem.SortOrder + 1;
            }
            BCDynamicLink.AddDynamicLink(pageId, Place.Footer, BCDynamicLink.DefaultCulture, item);
            LinkItems.Add(item);
            grdLinks.EditItemIndex = LinkItems.Count - 1;
            grdLinks.DataBind();
            DynamicLinkHelper.ResetDesignerDynamicLinks();
        }

        private void grdLinks_EditCommand(object source, DataGridCommandEventArgs e) {
            grdLinks.EditItemIndex = e.Item.ItemIndex;
            grdLinks.DataBind();
        }

        private void grdLinks_UpdateCommand(object source, DataGridCommandEventArgs e) {
            LinkItem link = (LinkItem)LinkItems[e.Item.ItemIndex];
            if (link != null) {
                TextBox txtLinkName = (TextBox)e.Item.FindControl(TxtLinkName);
                TextBox txtLinkUrl = (TextBox)e.Item.FindControl(TxtLinkUrl);
                DropDownList cboAlign = (DropDownList)e.Item.FindControl(CboAlign);
                CheckBox chkIsNewWindow = (CheckBox)e.Item.FindControl(ChkIsNewWindow);
                CheckBox chkIsVisible = (CheckBox)e.Item.FindControl(ChkIsVisible);
                link.Text = txtLinkName.Text;
                link.Url = txtLinkUrl.Text;
                link.IsNewWindow = chkIsNewWindow.Checked;
                link.IsVisible = chkIsVisible.Checked;
                link.Align = (Align)Enum.Parse(typeof(Align), cboAlign.SelectedValue);
                try {
                    BCDynamicLink.UpdateDynamicLink(pageId, Place.Footer, BCDynamicLink.DefaultCulture, link);
                } catch (BaseBusinessException ex) {
                    MsgBox.Alert(this, ex.Message);
                }
                DynamicLinkHelper.ResetDesignerDynamicLinks();
            }

            grdLinks.EditItemIndex = -1;
            grdLinks.DataBind();
        }

        private void grdLinks_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdLinks.EditItemIndex = -1;
            grdLinks.DataBind();
        }

        private void grdLinks_DeleteCommand(object source, DataGridCommandEventArgs e) {
            LinkItem link = (LinkItem)LinkItems[e.Item.ItemIndex];
            if (link != null) {
                try {
                    BCDynamicLink.DeleteDynamicLink(pageId, link.LinkId, Place.Footer);
                } catch (BaseBusinessException ex) {
                    MsgBox.Alert(this, ex.Message);
                }
                LinkItems.RemoveAt(e.Item.ItemIndex);
                DynamicLinkHelper.ResetDesignerDynamicLinks();
            }
            grdLinks.EditItemIndex = -1;
            grdLinks.DataBind();
        }

        protected void btnItemUp_Click(object sender, ImageClickEventArgs e) {
            DataGridItem grdItem = (DataGridItem)((ImageButton)sender).Parent.Parent;
            if (grdItem.ItemIndex > 0) {
                LinkItem item = (LinkItem)LinkItems[grdItem.ItemIndex];
                LinkItem itemPrev = (LinkItem)LinkItems[grdItem.ItemIndex - 1];
                int sortOrder = itemPrev.SortOrder;
                itemPrev.SortOrder = item.SortOrder;
                item.SortOrder = sortOrder;

                LinkItems.RemoveAt(grdItem.ItemIndex);
                LinkItems.Insert(grdItem.ItemIndex - 1, item);
                try {
                    BCDynamicLink.UpdateDynamicLink(pageId, Place.Footer, BCDynamicLink.DefaultCulture, item);
                    BCDynamicLink.UpdateDynamicLink(pageId, Place.Footer, BCDynamicLink.DefaultCulture, itemPrev);
                } catch (BaseBusinessException ex) {
                    MsgBox.Alert(this, ex.Message);
                }
                DynamicLinkHelper.ResetDesignerDynamicLinks();
            }
            grdLinks.EditItemIndex = -1;
            grdLinks.DataBind();
        }

        protected void btnItemDown_Click(object sender, ImageClickEventArgs e) {
            DataGridItem grdItem = (DataGridItem)((ImageButton)sender).Parent.Parent;
            if (grdItem.ItemIndex < LinkItems.Count - 1) {
                LinkItem item = (LinkItem)LinkItems[grdItem.ItemIndex];
                LinkItem itemNext = (LinkItem)LinkItems[grdItem.ItemIndex + 1];
                int sortOrder = itemNext.SortOrder;
                itemNext.SortOrder = item.SortOrder;
                item.SortOrder = sortOrder;

                LinkItems.RemoveAt(grdItem.ItemIndex);
                LinkItems.Insert(grdItem.ItemIndex + 1, item);
                try {
                    BCDynamicLink.UpdateDynamicLink(pageId, Place.Footer, BCDynamicLink.DefaultCulture, item);
                    BCDynamicLink.UpdateDynamicLink(pageId, Place.Footer, BCDynamicLink.DefaultCulture, itemNext);
                } catch (BaseBusinessException ex) {
                    MsgBox.Alert(this, ex.Message);
                }
                DynamicLinkHelper.ResetDesignerDynamicLinks();
            }
            grdLinks.EditItemIndex = -1;
            grdLinks.DataBind();
        }
    }
}