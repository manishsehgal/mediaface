<%@ Page language="c#" Codebehind="GoogleTracking.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.GoogleTracking" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Google Tracking</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
      <h3 align="center">Google Tracking</h3>
      <span>Google code:</span>
      <asp:TextBox ID="txtCode" Runat="server" TextMode="MultiLine" Rows="10" MaxLength="2048" style="width:100%"></asp:TextBox>
      <br>
      <asp:Button ID="btnSubmit" Runat="server" Text="Save Code"/>
      <br><br>
      <asp:DataGrid
        Runat="server"
        ID="grdTrackPages"
        AutoGenerateColumns="False">
        <Columns>
          <asp:TemplateColumn HeaderText="Page URL">
            <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="150px" />
            <ItemTemplate>
                <asp:Label ID="lblPageUrl" Runat="server" />
            </ItemTemplate>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Enable Script">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:CheckBox ID="chkEnableScript" Runat="server" />
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center"/>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Action">
            <ItemTemplate>
              <asp:ImageButton
                Runat="server"
                ID="btnItemEdit"
                ImageUrl="images/edit.gif"
                CommandName="Edit"
                CausesValidation="False"
                ImageAlign="AbsMiddle"
                AlternateText="Edit" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:ImageButton
                Runat="server"
                ID="btnItemUpdate"
                ImageUrl="images/save.gif"
                ImageAlign="AbsMiddle"
                CommandName="Update"
                CausesValidation="False"
                AlternateText="Save" />
              <asp:ImageButton
                Runat="server"
                ID="btnItemCancel"
                ImageUrl="images/cancel.gif"
                ImageAlign="AbsMiddle"
                CommandName="Cancel"
                CausesValidation="False"
                AlternateText="Cancel" />
            </EditItemTemplate>
            <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="100px"/>
            <ItemStyle Wrap="False"/>
            <ItemStyle HorizontalAlign="Center"/>
          </asp:TemplateColumn>
        </Columns>
      </asp:DataGrid>
    </form>
  </body>
</html>
