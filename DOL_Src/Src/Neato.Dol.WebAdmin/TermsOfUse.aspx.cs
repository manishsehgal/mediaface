using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpModules;

namespace Neato.Dol.WebAdmin {
    public class TermsOfUse : Page {
        #region Constants
        private const string LocalizedTextName = "LocalizedText";
        private const string DefaultCultureName = "";
        private const string JSPreview = "return Preview()";
        #endregion

        protected TextBox txtText;
        protected Button btnSubmit;
        protected Button btnPreview;
        protected HtmlControl ctlPreviewContaner;

        private LocalizedText PageContent {
            get { return (LocalizedText)ViewState[LocalizedTextName]; }
            set { ViewState[LocalizedTextName] = value; }
        }

        #region Url
        private const string RawUrl = "TermsOfUse.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(TermsOfUse_DataBinding);
            this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        }
        #endregion

        private void TermsOfUse_DataBinding(object sender, EventArgs e) {
            string url = CheckLocationModule.AdminUrl(Configuration.IntranetDesignerSiteUrl + Neato.Dol.WebDesigner.TermsOfUse.PageName());
            ctlPreviewContaner.Attributes["src"] = url;
            btnPreview.Attributes["onclick"] = JSPreview;
            PageContent = BCTermsOfUsePage.Get(DefaultCultureName);
            if (PageContent == null)
                PageContent = new LocalizedText(DefaultCultureName, string.Empty);

            txtText.Text = PageContent.Text;
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            PageContent.Text = txtText.Text;

            try {
                BCTermsOfUsePage.Update(PageContent);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
        }
    }
}