using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebAdmin.Helpers;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin {
	public class ManageLayout : System.Web.UI.Page {
        protected DataGrid grdLayout;
        protected DataGrid grdBlankIcons;
        protected DropDownList cboLabelFilter;
        protected DropDownList cboDeviceFilter;
        protected DropDownList cboDeviceTypeFilter;
        protected Button btnSearch;
        protected Button btnNew;
        protected Button btnBlankLayouts;
        protected Button btnReturn;
        
        private const int NumberOfColumns = 3;

        private const string ImgIconName = "imgIcon";
        private const string CtlIconFileName = "ctlIconFile";
        private const string LblNameName = "lblName";
        private const string LblFaceName = "lblFaceName";
        private const string CtlProjectFileName = "ctlProjectFile";
        private const string TxtFacesName = "txtFaces";

        private const string EditLayoutItemsCommandName = "EditLayoutItems";

        private const string BtnItemDeleteName = "btnDelete";
        
        private ArrayList deviceTypes;
        private ArrayList allLabels;
        private ArrayList allDevices;
        private ArrayList layouts;
        private ArrayList layoutItems;

        private const string AllValue = "-1";
        private const string SelectValue = "-2";
        private const string NoDataValue = "-3";

        private const string LabelFilterKey = "LabelFilterKey";
        private const string DeviceFilterKey = "DeviceFilterKey";
        private const string DeviceTypeFilterKey = "DeviceTypeFilterKey";

        protected Panel panFaceLayout;
        protected Panel panFaceLayoutItem;
        protected Panel pnlBlankLayout;
        protected DataGrid grdFaceLayoutItem;
        protected Button btnSaveLayoutItems;
        protected Button btnCancelLayoutItems;

        private const string LblFaceLayoutItemPosition = "lblFaceLayoutItemPosition";
        private const string TxtFaceLayoutItem = "txtFaceLayoutItem";

        private PageFilter CurrentFilter {
            get { return (PageFilter)ViewState["CurrentFilter"]; }
            set { ViewState["CurrentFilter"] = value; }
        }

        private bool NewMode {
            get { return (bool)ViewState["NewModeKey"]; }
            set { ViewState["NewModeKey"] = value; }
        }

        private void Page_Load(object sender, System.EventArgs e) {
            if (!IsPostBack) {
                NewMode = false;
                InitFilters();
                DataBind();
            }
        }

        private void InitFilters() {
            CurrentFilter = new PageFilter();

            CurrentFilter[DeviceFilterKey] = Request.QueryString[DeviceFilterKey];
            if (CurrentFilter[DeviceFilterKey] == null) CurrentFilter[DeviceFilterKey] = SelectValue;

            CurrentFilter[LabelFilterKey] = Request.QueryString[LabelFilterKey];
            if (CurrentFilter[LabelFilterKey] == null) CurrentFilter[LabelFilterKey] = SelectValue;

            CurrentFilter[DeviceTypeFilterKey] = Request.QueryString[DeviceTypeFilterKey];
            if (CurrentFilter[DeviceTypeFilterKey] == null) CurrentFilter[DeviceTypeFilterKey] = SelectValue;
        }

        #region Url
        private const string RawUrl = "ManageLayout.aspx";

        public static Uri Url(string deviceFilter, string labelFilter, string deviceTypeFilter) {
            return UrlHelper.BuildUrl(RawUrl,
                DeviceFilterKey, deviceFilter,
                LabelFilterKey, labelFilter,
                DeviceTypeFilterKey, deviceTypeFilter);
        }

        public static Uri Url() 
        {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion
        
        #region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent() {    
			this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManageLayout_DataBinding);
            this.PreRender += new EventHandler(ManageLayout_PreRender);
            cboLabelFilter.DataBinding += new EventHandler(cboLabelFilter_DataBinding);
            cboDeviceFilter.DataBinding += new EventHandler(cboDeviceFilter_DataBinding);
            cboDeviceTypeFilter.DataBinding += new EventHandler(cboDeviceTypeFilter_DataBinding);
            btnSearch.Click += new EventHandler(btnSearch_Click);
            btnNew.Click += new EventHandler(btnNew_Click);
            btnBlankLayouts.Click += new EventHandler(btnBlankLayouts_Click);
            btnReturn.Click += new EventHandler(btnReturn_Click);
            grdLayout.ItemDataBound += new DataGridItemEventHandler(grdLayout_ItemDataBound);
            grdLayout.EditCommand += new DataGridCommandEventHandler(grdLayout_EditCommand);
            grdLayout.DeleteCommand += new DataGridCommandEventHandler(grdLayout_DeleteCommand);
            grdLayout.UpdateCommand += new DataGridCommandEventHandler(grdLayout_UpdateCommand);
            grdLayout.CancelCommand += new DataGridCommandEventHandler(grdLayout_CancelCommand);
            grdLayout.ItemCreated += new DataGridItemEventHandler(grdLayout_ItemCreated);
            grdLayout.ItemCommand += new DataGridCommandEventHandler(grdLayout_ItemCommand);
            grdBlankIcons.ItemDataBound += new DataGridItemEventHandler(grdBlankIcons_ItemDataBound);
            grdBlankIcons.EditCommand += new DataGridCommandEventHandler(grdBlankIcons_EditCommand);
            grdBlankIcons.UpdateCommand += new DataGridCommandEventHandler(grdBlankIcons_UpdateCommand);
            grdBlankIcons.CancelCommand += new DataGridCommandEventHandler(grdBlankIcons_CancelCommand);
            
            this.btnCancelLayoutItems.Click += new EventHandler(btnCancelLayoutItems_Click);
            this.btnSaveLayoutItems.Click += new EventHandler(btnSaveLayoutItems_Click);
            this.grdFaceLayoutItem.DataBinding += new EventHandler(grdFaceLayoutItem_DataBinding);
            this.grdFaceLayoutItem.ItemDataBound += new DataGridItemEventHandler(grdFaceLayoutItem_ItemDataBound);
        }
        #endregion

        private void ManageLayout_DataBinding(object sender, EventArgs e) {
            deviceTypes = new ArrayList(BCCategory.GetCategoryList());
            allLabels = new ArrayList(BCFace.FaceEnum());
            allDevices = new ArrayList(BCDevice.GetDeviceList());
            allDevices.Sort(Device.GetFullModelComparer(CultureInfo.InvariantCulture));

            if (IsPostBack) {
                CurrentFilter[LabelFilterKey] = cboLabelFilter.SelectedValue;
                CurrentFilter[DeviceFilterKey] = cboDeviceFilter.SelectedValue;
                CurrentFilter[DeviceTypeFilterKey] = cboDeviceTypeFilter.SelectedValue;
            }

             if (!CurrentFilter.FiltersHaveSameValue(SelectValue)) {
                if (CurrentFilter.GetFiltersWithoutValue(SelectValue).Length > 0) {
                    CurrentFilter.ReplaceFilterValue(SelectValue, AllValue);
                }

                Face face = null;
                int faceId = int.Parse(CurrentFilter[LabelFilterKey], CultureInfo.InvariantCulture.NumberFormat);
                if (faceId > 0 || faceId == Convert.ToInt32(NoDataValue)) {
                    face = new Face();
                    face.Id = faceId;
                }

                DeviceBase device = null;
                int deviceId = int.Parse(CurrentFilter[DeviceFilterKey], CultureInfo.InvariantCulture.NumberFormat);
                if (deviceId > 0) {
                    device = new DeviceBase(deviceId);
                }
                DeviceType deviceType =
                    (CurrentFilter[DeviceTypeFilterKey] == AllValue) ?
                    DeviceType.Undefined :
                    (DeviceType)(Convert.ToInt32(CurrentFilter[DeviceTypeFilterKey]));

                layouts = new ArrayList(BCFaceLayout.EnumerateFaceLayouts(deviceType, face, device));
            } 
            else {
                layouts = new ArrayList();
            }

            if (NewMode) {
                layouts.Insert(0, BCFaceLayout.NewLayout());
                grdLayout.EditItemIndex = 0;
            } 
            /*else if (grdLayout.EditItemIndex >= 0) {
                int faceLayoutId = (int)grdLayout.DataKeys[grdLayout.EditItemIndex];
                FaceLayout faceLayout = new FaceLayout(faceLayoutId);
                int index = layouts.IndexOf(faceLayout);
                grdLayout.EditItemIndex = index;
            }*/
            
            grdLayout.DataSource = layouts;
            grdLayout.DataKeyField = FaceLayout.IdField;

            grdBlankIcons.DataSource = allLabels;
            grdBlankIcons.DataKeyField = Face.IdField;
        }

        private void cboDeviceFilter_DataBinding(object sender, EventArgs e) {
            cboDeviceFilter.Items.Clear();
            foreach (Device device in allDevices) {
                ListItem item = new ListItem(device.FullModel, device.Id.ToString());
                cboDeviceFilter.Items.Add(item);
                if (item.Value == CurrentFilter[DeviceFilterKey]) item.Selected = true;
            }
            cboDeviceFilter.Items.Insert(0, new ListItem("All", AllValue));
            if (CurrentFilter[DeviceFilterKey] == SelectValue) {
                cboDeviceFilter.Items.Insert(0, new ListItem("Select", SelectValue));
            }
        }

        private void cboLabelFilter_DataBinding(object sender, EventArgs e) {
            cboLabelFilter.Items.Clear();
            foreach (Face face in allLabels) {
                ListItem item = new ListItem(face.GetName(""), face.Id.ToString());
                cboLabelFilter.Items.Add(item);
                if (item.Value == CurrentFilter[LabelFilterKey]) item.Selected = true;
            }
            cboLabelFilter.Items.Insert(0, new ListItem("All", AllValue));
            cboLabelFilter.Items.Insert(1, new ListItem("No labels", NoDataValue));
            if (CurrentFilter[LabelFilterKey] == SelectValue) {
                cboLabelFilter.Items.Insert(0, new ListItem("Select", SelectValue));
            }
            else if(CurrentFilter[LabelFilterKey] == NoDataValue) {
                cboLabelFilter.Items[1].Selected = true;
            }
        }

        private void cboDeviceTypeFilter_DataBinding(object sender, EventArgs e) {
            cboDeviceTypeFilter.Items.Clear();
            foreach (Category category in deviceTypes) {
                ListItem item = new ListItem(category.Name, category.Id.ToString());
                cboDeviceTypeFilter.Items.Add(item);
                if (item.Value == CurrentFilter[DeviceTypeFilterKey]) item.Selected = true;
            }
            cboDeviceTypeFilter.Items.Insert(0, new ListItem("All", AllValue));
            if (CurrentFilter[DeviceTypeFilterKey] == SelectValue) {
                cboDeviceTypeFilter.Items.Insert(0, new ListItem("Select", SelectValue));
            }
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewMode = true;
            DataBind();
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            grdLayout.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void ManageLayout_PreRender(object sender, EventArgs e) {
            grdLayout.Visible = grdLayout.Items.Count > 0;
        }

        private void grdLayout_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void ItemDataBound(DataGridItem item) {
            FaceLayout faceLayout = (FaceLayout)item.DataItem;

            Image imgIcon = (Image)item.FindControl(ImgIconName);
            Label lblName = (Label)item.FindControl(LblNameName);
            Label lblFace = (Label)item.FindControl(LblFaceName);
            TextBox txtFaces = (TextBox)item.FindControl(TxtFacesName);

            lblName.Text = HttpUtility.HtmlEncode(faceLayout.Name);
            lblFace.Text = HttpUtility.HtmlEncode(faceLayout.Face.GetName(string.Empty));

            StringBuilder sb = new StringBuilder();
            foreach(FaceLayoutItem faceLayoutItem in faceLayout.FaceLayoutItems) {
                if(sb.Length != 0)
                    sb.Append(Environment.NewLine);
                sb.Append(string.Format("{0} {1} {2}", HtmlHelper.BlackCircleChar, faceLayoutItem.Position, faceLayoutItem.Text));
            }
            txtFaces.Text = sb.ToString();

            string imageUrl = IconHandler.ImageUrl(faceLayout).AbsoluteUri;
            imgIcon.ImageUrl = imageUrl;

            Button btnDelete = (Button)item.FindControl(BtnItemDeleteName);
            string warning = "Do you want to remove this layout?";
            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", warning);
        }

        private void EditItemDataBound(DataGridItem item) {
            HtmlInputFile ctlIconFile = (HtmlInputFile)item.FindControl(CtlIconFileName);
            JavaScriptHelper.RegisterFocusScript(this, ctlIconFile.ClientID);
        }

        private void grdLayout_EditCommand(object source, DataGridCommandEventArgs e) {
            grdLayout.EditItemIndex = e.Item.ItemIndex;
            NewMode = false;
            DataBind();
        }

        private void grdLayout_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdLayout.EditItemIndex = -1;
            int faceLayoutId = (int)grdLayout.DataKeys[e.Item.ItemIndex];
            FaceLayout faceLayout = new FaceLayout(faceLayoutId);
            try {
                BCFaceLayout.Delete(faceLayout);
                Response.Redirect(Url(
                    CurrentFilter[DeviceFilterKey],
                    CurrentFilter[LabelFilterKey],
                    CurrentFilter[DeviceTypeFilterKey]).
                    PathAndQuery);
            } 
            catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewMode = false;
            DataBind();
        }

        private void grdLayout_UpdateCommand(object source, DataGridCommandEventArgs e) {
            if(!IsValid)
                return;

            HtmlInputFile ctlIconFile = (HtmlInputFile)e.Item.FindControl(CtlIconFileName);
            Stream iconFile = ctlIconFile.PostedFile.InputStream;

            HtmlInputFile ctlProjectFile = (HtmlInputFile)e.Item.FindControl(CtlProjectFileName);
            Stream projectFile = ctlProjectFile.PostedFile.InputStream;

            int faceLayoutId = (int)grdLayout.DataKeys[e.Item.ItemIndex];
            FaceLayout faceLayout = new FaceLayout(faceLayoutId);
            faceLayout.Name = Path.GetFileName(ctlProjectFile.PostedFile.FileName);
            
            try {
                BCFaceLayout.Save(faceLayout, projectFile, iconFile);
                Response.Redirect(Url(
                    CurrentFilter[DeviceFilterKey],
                    CurrentFilter[LabelFilterKey],
                    CurrentFilter[DeviceTypeFilterKey]).
                    PathAndQuery);
            } 
            catch (BaseBusinessException ex) 
            {
                MsgBox.Alert(this, ex.Message);
            }

            //grdLayout.EditItemIndex = -1;
            //NewMode = false;
            DataBind();
        }

        private void grdLayout_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdLayout.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdLayout_ItemCreated(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            if(item.ItemType != ListItemType.EditItem)
                return;

            GridTransformationHelper.EditItemCreationForm(item, grdLayout.Columns, NumberOfColumns);
        }

        private void grdLayout_ItemCommand(object source, DataGridCommandEventArgs e){
            switch(((Button)e.CommandSource).CommandName) {
                case EditLayoutItemsCommandName:
                    int faceLayoutId = (int)grdLayout.DataKeys[e.Item.ItemIndex];
                    FaceLayout faceLayout = new FaceLayout(faceLayoutId);
                    layoutItems = new ArrayList(BCFaceLayout.FaceLayoutItemsEnumByFaceLayoutId(faceLayout));
                    grdFaceLayoutItem.DataBind();
                    panFaceLayout.Visible = false;
                    panFaceLayoutItem.Visible = true;
                    break;
                default:
                    break;
            }
        }

        private void btnCancelLayoutItems_Click(object sender, EventArgs e) {
            panFaceLayout.Visible = true;
            panFaceLayoutItem.Visible = false;
            grdLayout.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void btnSaveLayoutItems_Click(object sender, EventArgs e) {
            foreach(DataGridItem item in grdFaceLayoutItem.Items) {
                if(item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem) {
                    int faceLayoutItemId = (int)grdFaceLayoutItem.DataKeys[item.ItemIndex];
                    TextBox txtFaceLayoutItem = (TextBox)item.FindControl(TxtFaceLayoutItem);
                    FaceLayoutItem faceLayoutItem = new FaceLayoutItem();
                    faceLayoutItem.Id = faceLayoutItemId;
                    faceLayoutItem.Text = txtFaceLayoutItem.Text;
                    try {
                        BCFaceLayout.Save(faceLayoutItem);
                    }
                    catch (BaseBusinessException ex) {
                        MsgBox.Alert(this, ex.Message);
                    }
                }
            }
            panFaceLayout.Visible = true;
            panFaceLayoutItem.Visible = false;
            grdLayout.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdFaceLayoutItem_DataBinding(object sender, EventArgs e) {
            grdFaceLayoutItem.DataSource = layoutItems;
            grdFaceLayoutItem.DataKeyField = FaceLayout.IdField;
        }

        private void grdFaceLayoutItem_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
                FaceLayoutItem faceLayoutItem = (FaceLayoutItem)e.Item.DataItem;
                Label lblFaceLayoutItemPosition = (Label)e.Item.FindControl(LblFaceLayoutItemPosition);
                TextBox txtFaceLayoutItem = (TextBox)e.Item.FindControl(TxtFaceLayoutItem);
                lblFaceLayoutItemPosition.Text = HttpUtility.HtmlEncode(faceLayoutItem.Position.ToString());
                txtFaceLayoutItem.Text = faceLayoutItem.Text;
            }
        }

        private void btnBlankLayouts_Click(object sender, EventArgs e) {
            panFaceLayout.Visible = false;
            pnlBlankLayout.Visible = true;
        }

        private void btnReturn_Click(object sender, EventArgs e) {
            panFaceLayout.Visible = true;
            pnlBlankLayout.Visible = false;
        }

        private void grdBlankIcons_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    BlankIconItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditBlankIconItemDataBound(e.Item);
                    break;
            }
        }

        private void BlankIconItemDataBound(DataGridItem item) {
            Face face = (Face)item.DataItem;

            Image imgIcon = (Image)item.FindControl("imgBlankIcon");
            Label lblFace = (Label)item.FindControl("lblBlankIconFaceName");
            lblFace.Text = HttpUtility.HtmlEncode(face.GetName(string.Empty));


            string imageUrl = IconHandler.ImageUrl(IconHandler.FaceLayoutIdParamName, face.Id).AbsoluteUri;
            imgIcon.ImageUrl = imageUrl;
        }

        private void EditBlankIconItemDataBound(DataGridItem item) {
            Face face = (Face)item.DataItem;
            Label lblFace = (Label)item.FindControl("lblBlankIconFaceNameEdit");
            lblFace.Text = HttpUtility.HtmlEncode(face.GetName(string.Empty));

            HtmlInputFile ctlIconFile = (HtmlInputFile)item.FindControl("ctlBlankIconFile");
            JavaScriptHelper.RegisterFocusScript(this, ctlIconFile.ClientID);
        }

        private void grdBlankIcons_EditCommand(object source, DataGridCommandEventArgs e) {
            grdBlankIcons.EditItemIndex = e.Item.ItemIndex;
            DataBind();
        }

        private void grdBlankIcons_UpdateCommand(object source, DataGridCommandEventArgs e) {
            if(!IsValid)
                return;

            HtmlInputFile ctlIconFile = (HtmlInputFile)e.Item.FindControl("ctlBlankIconFile");
            Stream iconFile = ctlIconFile.PostedFile.InputStream;

            int faceId = (int)grdBlankIcons.DataKeys[e.Item.ItemIndex];
            
            try {
                BCFaceLayout.UpdateBlankIcon(faceId, iconFile);
                grdBlankIcons.EditItemIndex = -1;
                DataBind();
            } 
            catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }

            //grdLayout.EditItemIndex = -1;
            //NewMode = false;
            DataBind();

        }

        private void grdBlankIcons_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdLayout.EditItemIndex = -1;
            DataBind();
        }
    }
}
