<%@ Page language="c#" Codebehind="PhoneRequests.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.PhoneRequests" %>
<%@ Register TagPrefix="cpt" TagName="DateInterval" Src="Controls/DateInterval.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DeviceRequests</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<style>
TD { PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px }
INPUT.OneRow { WIDTH: 100% }
INPUT.MultiRow { WIDTH: 100%; HEIGHT: 100% }
SELECT { WIDTH: 200px }
			</style>
			<table>
				<tr>
					<td colspan="3" align="center">
						<h3>"Can't find my device" requests</h3>
					</td>
				</tr>
				<tr>
					<td>Select carrier:</td>
					<td>
						<asp:DropDownList Runat="server" ID="cboCarrierFilter" />
					</td>
					<td rowspan="3"><cpt:DateInterval id="dtiDates" runat="server" CaptionWidth="90px" DateControlWidth="120px" AutoPostBack="True"/></td>
				<tr>
					<td>Select brand:</td>
					<td>
						<asp:DropDownList Runat="server" ID="cboManufacturerFilter" AutoPostBack="True" />
					</td>
				</tr>
				<tr>
					<td>Select model:</td>
					<td>
						<asp:DropDownList Runat="server" ID="cboModelFilter" />
					</td>
				</tr>
			</table>
			<asp:Button ID="btnSubmit" text="Search" Runat=server/>
			<asp:CustomValidator Display="Dynamic" Runat="server" ID="vldRecipientSelection" EnableClientScript="False"
				Text="Please mark recipients." />
			<br/>
			<asp:CustomValidator Display="Dynamic" Runat="server" ID="vldFilter" EnableClientScript="False"
				Text="There are no results matching your criteria." />
			<br/>
			<asp:DataGrid Runat="server" ID="grdPhoneRequests" AutoGenerateColumns="False" AllowPaging="True"
				CellPadding="0" CellSpacing="0" Width="70%">
				<Columns>
					<asp:TemplateColumn HeaderText="First name">
						<HeaderStyle HorizontalAlign="Center" Width="10%" />
						<ItemTemplate>
							<asp:Label ID="lblFirstName" Runat="server" />
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtFirstName" Runat="server" MaxLength="100" CssClass="OneRow" />
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Last name">
						<HeaderStyle HorizontalAlign="Center" Width="10%" />
						<ItemTemplate>
							<asp:Label ID="lblLastName" Runat="server" />
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtLastName" Runat="server" MaxLength="100" CssClass="OneRow" />
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="E-mail address">
						<HeaderStyle HorizontalAlign="Center" Width="10%" />
						<ItemTemplate>
							<asp:Label ID="lblEmailAddress" Runat="server" />
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtEmailAddress" Runat="server" MaxLength="100" CssClass="OneRow" />
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Carrier">
						<HeaderStyle HorizontalAlign="Center" Width="10%" />
						<ItemTemplate>
							<asp:Label ID="lblCarrier" Runat="server" />
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtCarrier" Runat="server" MaxLength="100" CssClass="OneRow" />
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Device brand">
						<HeaderStyle HorizontalAlign="Center" Width="10%" />
						<ItemTemplate>
							<asp:Label ID="lblPhoneManufacturer" Runat="server" />
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtPhoneManufacturer" Runat="server" MaxLength="100" CssClass="OneRow" />
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Device model">
						<HeaderStyle HorizontalAlign="Center" Width="10%" />
						<ItemTemplate>
							<asp:Label ID="lblPhoneModel" Runat="server" />
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtPhoneModel" Runat="server" MaxLength="100" CssClass="OneRow" />
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Comment">
						<HeaderStyle HorizontalAlign="Center" Width="20%" />
						<ItemTemplate>
							<asp:TextBox ID="txtCommentView" Runat="server"
							TextMode="MultiLine" Rows="3" ReadOnly="True" />
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtComment" Runat="server" MaxLength="2000"
							CssClass="MiltiRow" TextMode="MultiLine" Rows="5"/>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Select" ItemStyle-HorizontalAlign="Center">
						<HeaderStyle HorizontalAlign="Center" Width="5%" />
						<ItemTemplate>
							<asp:CheckBox Runat="server" ID="chkSelected" OnCheckedChanged="chkSelected_CheckedChanged" />
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Action">
						<HeaderStyle HorizontalAlign="Center" Width="10%" />
						<ItemTemplate>
							<asp:ImageButton Runat="server" ID="btnItemEdit" ImageUrl="images/edit.gif" CommandName="Edit" AlternateText="Edit"
								CausesValidation="False" />
							<asp:ImageButton Runat="server" ID="btnItemUpdate" ImageUrl="images/save.gif" CommandName="Update"
								AlternateText="Save" CausesValidation="False" />
							<asp:ImageButton Runat="server" ID="btnItemCancel" ImageUrl="images/cancel.gif" CommandName="Cancel"
								AlternateText="Cancel" CausesValidation="False" />
							<asp:ImageButton Runat="server" ID="btnItemDelete" ImageUrl="images/remove.gif" CommandName="Delete"
								AlternateText="Remove" CausesValidation="False" />
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle Mode="NumericPages" />
			</asp:DataGrid>
			<asp:Button Runat="server" ID="btnSelectAll" Text="Select all" CausesValidation="False" />
			<br/>
			<asp:Button Runat="server" ID="btnSendNotification" Text="Send notification" />
			<br/>
			<asp:Button Runat="server" ID="btnRemoveSelected" Text="Remove selected" />
		</form>
	</body>
</HTML>
