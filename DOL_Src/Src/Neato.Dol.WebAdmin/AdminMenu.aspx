<%@ Page language="c#" Codebehind="AdminMenu.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.AdminMenu" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>AdminMenu</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </HEAD>
  <body MS_POSITIONING="GridLayout">
	
    <form id="Form1" method="post" runat="server">
        <ul>
            Content
            <li><asp:HyperLink ID="hypTermsOfUse" Runat="server" Text="Terms of Use"/></li>
            <li><asp:HyperLink ID="hypFaq" Runat="server" Text="FAQ"/></li>
            <li><asp:HyperLink ID="hypFooterMenu" Runat="server" Text="Footer"/></li>
            <li><asp:HyperLink ID="hypParameters" Runat="server" Text="Parameters"/></li>
            <li><asp:HyperLink ID="hypBannerManagement" Runat="server" Text="Banners"/></li>
            <li><asp:HyperLink ID="hypPreviewHtmlManagement" Runat="server" Text="Preview HTML"/></li>
            <li style="display:none;"><asp:HyperLink ID="hypExitPage" Runat="server" Text="Exit Page" Visible="False"/></li>
            <li><asp:HyperLink ID="hypWatermarkManagement" Runat="server" Text="Watermark"/></li>
            <li><asp:HyperLink ID="hypImageLibrary" Runat="server" Text="Image library"/></li>
        </ul>
        <ul>
            Notifications
            <li><asp:HyperLink ID="hypNotifications" Runat="server" Text="Edit e-mail content"/></li>
        </ul>
        <ul>
            Maintenance
            <!--<li><asp:HyperLink ID="hypAddPhone" Runat="server" Text="&quot;Can't find my device&quot; requests"/></li>-->
            <li><asp:HyperLink ID="hypSiteClosing" Runat="server" Text="Site closing"/></li>
        </ul>
        <ul>
            Management
            <li><asp:HyperLink ID="hypManageCategory" Runat="server" Text="Categories"/></li>
            <!--<li><asp:HyperLink ID="hypManageDeviceBrand" Runat="server" Text="Manufacturers"/></li>-->
            <li><asp:HyperLink ID="hypManagePaperBrand" Runat="server" Text="Paper Brands"/></li>
            <li><asp:HyperLink ID="hypManageDevice" Runat="server" Text="Devices"/></li>
            <li><asp:HyperLink ID="hypManageMP3PlayerPapers" Runat="server" Text="Devices Papers"/></li>
            <li><asp:HyperLink ID="hypManagePaper" Runat="server" Text="MF Papers"/></li>
            <li><asp:HyperLink ID="hypManagePaperOrder" Runat="server" Text="Papers order"/></li>
            <li><asp:HyperLink ID="hypManageLabel" Runat="server" Text="Labels"/></li>
            <li><asp:HyperLink ID="hypManageLayout" Runat="server" Text="Layouts"/></li>
            <li><asp:HyperLink ID="hypManageTemplate" Runat="server" Text="Templates"/></li>
            <li><asp:HyperLink ID="hypManageRetailer" Runat="server" Text="Retailers"/></li>
            <li><asp:HyperLink ID="hypCustomerProfiles" Runat="server" Text="Customer Profiles"/></li>
            <li><asp:HyperLink ID="hypGoogleTracking" Runat="server" Text="Google Tracking"/></li>
            <li><asp:HyperLink ID="hypManageIPFilter" Runat="server" Text="IP filters"/></li>
            <li><asp:HyperLink ID="hypManageUpc" Runat="server" Text="MFO UPC Management"/></li>
            <li><asp:HyperLink ID="hypManageUpcStaple" Runat="server" Text="MFOPE UPC Management"/></li>
            <li><asp:HyperLink ID="hypManageSpecialUser" Runat="server" Text="Special Users"/></li>
            <li><asp:HyperLink ID="hypPluginManagement" Runat="server" Text="Plugin Management"/></li>
            <li><asp:HyperLink ID="hypManageMaintenanceClosing" Runat="server" Text="Close for Maintenance"/></li>
        </ul>
        <ul>
            Reports
            <li><asp:HyperLink ID="hypTrackUserAction" Runat="server" Text="User action tracking report"/></li>
            <!--<li><asp:HyperLink ID="hypTrackPhones" Runat="server" Text="Devices tracking report"/></li>-->
            <li><asp:HyperLink ID="hypTrackPapers" Runat="server" Text="Paper tracking report"/></li>
            <li><asp:HyperLink ID="hypTrackFeatures" Runat="server" Text="Features tracking report"/></li>
            <li><asp:HyperLink ID="hypTrackImages" Runat="server" Text="Images tracking report"/></li>
            <li><asp:HyperLink ID="hypTrackShapes" Runat="server" Text="Shapes tracking report"/></li>
            <li><asp:HyperLink ID="hypDeadLinksReport" Runat="server" Text="Deadlinks report"/></li>
            <li><asp:HyperLink ID="hypUpcReport" Runat="server" Text="MFO UPC codes report"/></li>
            <li><asp:HyperLink ID="hypUpcStapleReport" Runat="server" Text="MFOPE UPC codes report"/></li>
        </ul>
    </form>
	
  </body>
</HTML>
