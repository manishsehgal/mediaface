using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin {
    public class Notifications : Page {
        #region Constants
        private const string LblEventName = "lblEvent";
        private const string LblSubjectName = "lblSubject";
        private const string TxtSubjectName = "txtSubject";
        private const string TxtVariablesName = "txtVariables";
        private const string TxtBodyEditName = "txtBodyEdit";
        private const string TxtBodyName = "txtBody";

        private const string MailFormatKey = "MailFormat";
        private const string DataKeyField = "MailId";
        #endregion

        protected DataGrid grdMailFormat;
        protected TextBox txtTestEmail;
        
        private MailFormatData mailFormatData {
            get { return (MailFormatData)ViewState[MailFormatKey]; }
            set { ViewState[MailFormatKey] = value; }
        }


        #region Url
        private const string RawUrl = "Notifications.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                mailFormatData = BCMail.MailFormatEnum();
                DataBind();
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {    
            this.grdMailFormat.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdMailFormat_CancelCommand);
            this.grdMailFormat.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdMailFormat_EditCommand);
            this.grdMailFormat.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdMailFormat_UpdateCommand);
            this.grdMailFormat.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdMailFormat_ItemDataBound);
            this.grdMailFormat.ItemCommand += new DataGridCommandEventHandler(this.grdMailFormat_ItemCommand);
            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new System.EventHandler(this.Notifications_DataBinding);

        }
        #endregion

        #region Actions
        private void grdMailFormat_EditCommand(object source, DataGridCommandEventArgs e) {
            mailFormatData.RejectChanges();
            grdMailFormat.EditItemIndex = e.Item.ItemIndex;
            DataBind();
        }

        private void grdMailFormat_UpdateCommand(object source, DataGridCommandEventArgs e) {
            TextBox txtSubject = (TextBox)e.Item.FindControl(TxtSubjectName);
            TextBox txtBodyEdit = (TextBox)e.Item.FindControl(TxtBodyEditName);

            int mailId = (int)grdMailFormat.DataKeys[e.Item.ItemIndex];
            MailFormatData.MailFormatRow dr = mailFormatData.MailFormat.FindByMailId(mailId);
            if (dr != null) {
                dr.Subject = txtSubject.Text;
                dr.Body = txtBodyEdit.Text;
                BCMail.MailFormatUpd(mailFormatData);
            }

            grdMailFormat.EditItemIndex = -1;
            DataBind();
        }

        private void grdMailFormat_CancelCommand(object source, DataGridCommandEventArgs e) {
            mailFormatData.RejectChanges();
            grdMailFormat.EditItemIndex = -1;
            DataBind();
        }

        private void grdMailFormat_ItemCommand(object source, DataGridCommandEventArgs e) {
            if (e.CommandName == "Test") {
                MailType mailType = (MailType)grdMailFormat.DataKeys[e.Item.ItemIndex];
                string email = txtTestEmail.Text.Trim();
                string siteLink = string.Format("{0}{1}",
                    Configuration.PublicDesignerSiteUrl,
                    WebDesigner.DefaultPage.PageName());
                DeviceRequest deviceRequest = new DeviceRequest();
                deviceRequest.EmailAddress = email;
                deviceRequest.DeviceBrand = "Manufacturer";
                deviceRequest.Device = "Model";

                if (email.Length > 0) {
                    switch (mailType) {
                        case MailType.ForgotPassword: 
                            BCMail.SendForgotPasswordMail(Configuration.PublicDesignerSiteUrl,
                                WebDesigner.ForgotPassword.PageName(), email, "");
                            break;
                        case MailType.TellAFriend: 
                            string path = string.Format("{0}{1}",
                                Configuration.PublicDesignerSiteUrl,
                                WebDesigner.HttpHandlers.TellAFriendRefuseHandler.PageName());
                            string refuseLink = TellAFriendRefuseHandler.TellAFriendRefuseEmail(
                                path, email);
                            BCMail.SendTellAFriendMail("FriendName", email,
                                "YourName", email, "EmailText",
                                siteLink, refuseLink, false, "");
                            break;
                        case MailType.AddNewPhone:
                            BCMail.SendAddNewPhoneMail(deviceRequest, "");
                            break;
                        case MailType.UserFeedback:
                            BCMail.SendUserFeedbackMail(email, "User message", "", false);
                            break;
                        case MailType.PhoneRequestNotification:
                            BCMail.SendPhoneRequestNotificationMail(deviceRequest, siteLink, "");
                            break;
                        case MailType.AccountCreated:
                            BCMail.SendAccountCreatedMail(email, siteLink, "");
                            break;
                    }
                }
            }
        }
        #endregion

        #region Data binding
        private void Notifications_DataBinding(object sender, EventArgs e) {
            grdMailFormat.DataSource = mailFormatData;
            grdMailFormat.DataKeyField = DataKeyField;
        }

        private void grdMailFormat_ItemDataBound(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;

            if (item.ItemType == ListItemType.Item ||
                item.ItemType == ListItemType.AlternatingItem ||
                item.ItemType == ListItemType.EditItem) {

                MailFormatData.MailFormatRow row =
                    (MailFormatData.MailFormatRow)
                    ((DataRowView)item.DataItem).Row;

                bool editMode = (item.ItemType == ListItemType.EditItem);
            
                Label lblEvent = (Label)item.FindControl(LblEventName);
                Label lblSubject = (Label)item.FindControl(LblSubjectName);
                TextBox txtSubject = (TextBox)item.FindControl(TxtSubjectName);
                TextBox txtVariables = (TextBox)item.FindControl(TxtVariablesName);
                TextBox txtBody = (TextBox)item.FindControl(TxtBodyName);
                TextBox txtBodyEdit = (TextBox)item.FindControl(TxtBodyEditName);

                lblEvent.Text = row.Description;

                MailFormatData.MailVariableRow[] variables = 
                    row.GetMailVariableRows();
                for(int i = 0; i < variables.Length; i++) {
                    txtVariables.Text += variables[i].Name;
                    if (i < variables.Length - 1) {
                        txtVariables.Text += Environment.NewLine;
                    }
                }

                if (editMode) {
                    txtSubject.Text = row.Subject;
                    txtBodyEdit.Text = row.Body;
                } else {
                    lblSubject.Text = row.Subject;                
                    txtBody.Text = row.Body;
                }
            }
        }
        #endregion
    }
}