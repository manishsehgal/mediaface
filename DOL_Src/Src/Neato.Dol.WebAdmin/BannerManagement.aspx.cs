using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin {
    public class BannerManagement : Page {
        protected DropDownList cboPage;
        protected HtmlImage imgPage;
        protected DropDownList cboBanner;
        protected HtmlInputFile ctlImageFile;
        protected Button btnAddImage;
        protected Button btnDelImage;
        protected ListBox lstBannerImages;
        protected HtmlImage imgBannerImage;
        protected TextBox txtText;
        protected Button btnSubmit;
        protected HtmlControl ctlPreviewContaner;
        protected HtmlInputHidden ctlBaseUrl;
        protected CheckBox btnHidden;
        protected Label lblClosed;
        protected Panel panContent;
        protected TextBox txtBannerHeight;
        protected RegularExpressionValidator vldBannerHeight;

        private ManualResetEvent allDone = new ManualResetEvent(false);

        private int bannerId {
            get { return Convert.ToInt32(cboBanner.SelectedValue); }
        }

        private string page {
            get { return cboPage.SelectedValue; }
        }

        private string pageFull {
            get { return page + (page == "SiteClosed" ? ".html" : ".aspx"); }
        }

        private string pageIntranetUrl {
            get { return Path.Combine(Configuration.IntranetDesignerSiteUrl, pageFull).Replace('\\', '/'); }
        }

        private string pagePublicUrl {
            get { return Path.Combine(Configuration.PublicDesignerSiteUrl, pageFull).Replace('\\', '/'); }
        }

        #region Url

        private const string RawUrl = "BannerManagement.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.PreRender += new EventHandler(BannerManagement_PreRender);
            this.DataBinding += new EventHandler(BannerManagement_DataBinding);
            this.cboPage.SelectedIndexChanged += new EventHandler(cboPage_SelectedIndexChanged);
            this.cboBanner.SelectedIndexChanged += new EventHandler(cboBanner_SelectedIndexChanged);
            this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
            this.btnAddImage.Click += new EventHandler(btnAddImage_Click);
            this.btnDelImage.Click += new EventHandler(btnDelImage_Click);
            this.lstBannerImages.SelectedIndexChanged += new EventHandler(lstBannerImages_SelectedIndexChanged);
            this.btnHidden.CheckedChanged += new EventHandler(btnHidden_CheckedChanged);
        }

        #endregion

        private void BannerManagement_DataBinding(object sender, EventArgs e) {
            cboPage.Items.Clear();
            BannerPage[] pages = BCBannerPage.Enum();
            foreach (BannerPage page in pages) {
                ListItem item = new ListItem(page.Name, page.Id.ToString());
                cboPage.Items.Add(item);
            }

            bindBannerPage(pages[0].Id);
            lblClosed.Text = "Site is closing for maintenance. Please, open site, if you want to manage banners.";
        }

        private void cboPage_SelectedIndexChanged(object sender, EventArgs e) {
            bindBannerPage(page);
        }

        private void cboBanner_SelectedIndexChanged(object sender, EventArgs e) {
            bindImages(bannerId, null);
            bindText(bannerId);
        }

        private void bindBannerPage(string bannerPageId) {
            BannerPage page = BCBannerPage.Get(bannerPageId);
            imgPage.Src = page.ImgSrc;
            cboBanner.Items.Clear();
            int[] bannerIds = page.BannerIds;
            foreach (int bannerId in bannerIds) {
                ListItem item = new ListItem(bannerId.ToString(), bannerId.ToString());
                cboBanner.Items.Add(item);
            }

            bindImages(bannerIds[0], null);
            bindText(bannerIds[0]);
        }

        private void bindText(int bannerId) {
            txtText.Text = GetBannerHtml(bannerId);
            bindPreview();
        }

        private void bindPreview() {
            string bannerRootPath = Path.Combine(Configuration.IntranetDesignerSiteUrl, BCBanner.BannerRootFolder);
            string pagePath = Path.Combine(bannerRootPath, page);
            string bannerPath = Path.Combine(pagePath, bannerId.ToString());

            ctlBaseUrl.Value = string.Format("<BASE href='{0}/'/>", bannerPath);
            ctlPreviewContaner.Attributes["src"] = pageIntranetUrl;
        }

        private void bindImages(int bannerId, string imageName) {
            lstBannerImages.Items.Clear();
            string[] fileNames = GetFileNames(bannerId);
            foreach (string fileName in fileNames) {
                ListItem item = new ListItem(fileName, fileName);
                lstBannerImages.Items.Add(item);
            }

            if (fileNames.Length > 0) {
                imgBannerImage.Visible = true;
                bindBannerImage(imageName == null ? fileNames[0] : imageName);
            } else
                imgBannerImage.Visible = false;
        }

        private void lstBannerImages_SelectedIndexChanged(object sender, EventArgs e) {
            bindBannerImage(lstBannerImages.SelectedValue);
        }

        private void bindBannerImage(string fileName) {
            lstBannerImages.SelectedValue = fileName;
            imgBannerImage.Visible = true;
            string bannerRootPath = Path.Combine(Configuration.IntranetDesignerSiteUrl, BCBanner.BannerRootFolder);
            string pagePath = Path.Combine(bannerRootPath, page);
            string bannerPath = Path.Combine(pagePath, bannerId.ToString());
            string filePath = Path.Combine(bannerPath, fileName);

            imgBannerImage.Src = filePath;
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            vldBannerHeight.Validate();
            if (!vldBannerHeight.IsValid)
                return;

            SaveBannerHtml(bannerId);
            bindPreview();
        }

        private string GetBannerHtml(int bannerId) {
            string htmlText = string.Empty;
            string bannerHandler = BannerHandler.UrlForHtmlText(Configuration.IntranetDesignerSiteUrl, page, bannerId);
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(bannerHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "GET";

            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
            if (request.HaveResponse) {
                if (response.Headers[BannerHandler.BannerHeightKey] != null)
                    txtBannerHeight.Text = response.Headers[BannerHandler.BannerHeightKey];
                StreamReader reader = new StreamReader(response.GetResponseStream());
                htmlText = reader.ReadToEnd();

                reader.Close();
                response.Close();
            }

            return htmlText;
        }

        private string[] GetFileNames(int bannerId) {
            string bannerHandler = BannerHandler.UrlForEnumImages(Configuration.IntranetDesignerSiteUrl, page, bannerId);
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(bannerHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "GET";

            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
            if (!request.HaveResponse)
                return null;

            try {
                XmlDocument xmlDoc = new XmlDocument();
                if (response.GetResponseStream() != null)
                    xmlDoc.Load(response.GetResponseStream());
                response.Close();

                ArrayList list = new ArrayList();
                XmlNodeList fileList = xmlDoc.SelectNodes("//Files/File");
                for (int i = 0; i < fileList.Count; i++) {
                    XmlNode file = fileList[i];
                    list.Add(file.InnerText);
                }

                return (string[]) list.ToArray(typeof (string));
            }
            catch {
                lblClosed.Visible = true;
                panContent.Visible = false;
                return new string[0];
            }
        }

        private void SaveBannerHtml(int bannerId) {
            string bannerHandler = BannerHandler.UrlForHtmlText(Configuration.IntranetDesignerSiteUrl, page, bannerId, txtBannerHeight.Text);
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(bannerHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "POST";

            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(txtText.Text);
            writer.Close();

            request.BeginGetRequestStream(new AsyncCallback(ResponseCallback), request);
            allDone.WaitOne();

            BCMail.SendBannerNotification(page, bannerId.ToString(), null, true, pagePublicUrl);
        }

        private void ResponseCallback(IAsyncResult asynchronousResult) {
            HttpWebRequest request = (HttpWebRequest) asynchronousResult.AsyncState;
            request.EndGetRequestStream(asynchronousResult);
            allDone.Set();
        }

        private void btnAddImage_Click(object sender, EventArgs e) {
            if (ctlImageFile.PostedFile == null || ctlImageFile.PostedFile.ContentLength == 0)
                return;

            string fileName = Path.GetFileName(ctlImageFile.Value);
            string del = " .";
            fileName = fileName.Trim(del.ToCharArray());
            string bannerHandler = BannerHandler.UrlForUploadImage(Configuration.IntranetDesignerSiteUrl, page, bannerId, fileName);
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(bannerHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "POST";

            BinaryWriter writer = new BinaryWriter(request.GetRequestStream());
            writer.Write(ConvertHelper.StreamToByteArray(ctlImageFile.PostedFile.InputStream));
            writer.Close();

            // Get the response.
            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
            if (request.HaveResponse) {
                bindImages(bannerId, fileName);
                response.Close();
            }

            BCMail.SendBannerNotification(page, bannerId.ToString(), fileName, true, pagePublicUrl);
        }

        private void btnDelImage_Click(object sender, EventArgs e) {
            if (lstBannerImages.SelectedValue == string.Empty)
                return;

            string fileName = lstBannerImages.SelectedValue;
            string bannerHandler = BannerHandler.UrlForDeleteImage
                (Configuration.IntranetDesignerSiteUrl, page, bannerId, fileName);
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(bannerHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "GET";

            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
            if (request.HaveResponse) {
                response.Close();
            }

            bindImages(bannerId, null);

            BCMail.SendBannerNotification(page, bannerId.ToString(), fileName, false, pagePublicUrl);
        }

        private void btnHidden_CheckedChanged(object sender, EventArgs e) {
            BCHiddenBanner.HideBanner(!btnHidden.Checked, new HiddenBanner(int.Parse(cboBanner.SelectedValue), cboPage.SelectedValue));
        }

        private void BannerManagement_PreRender(object sender, EventArgs e) {
            string currentPage = cboPage.SelectedValue;
            int currentBanner = int.Parse(cboBanner.SelectedValue);
            btnHidden.Checked = !BCHiddenBanner.IsBannerHidden(new HiddenBanner(currentBanner, currentPage));
            if (lstBannerImages.Items.Count > 0)
                btnDelImage.Attributes["onclick"] = "return confirm('Are you sure to delete selected image?')";
        }
    }
}