<%@ Control Language="c#" AutoEventWireup="false" Codebehind="DatePicker.ascx.cs" Inherits="Neato.Dol.WebAdmin.Controls.DatePicker" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<asp:Panel Runat="server" ID="panDate" >
<table width="100%" cellpadding="0" cellspacing="0" border="0" >
    <tr>
        <td style="PADDING-RIGHT: 2px"><asp:TextBox Runat="server" ID="txtDate" ReadOnly="False" Width="100%" MaxLength="10"/></td>
        <td width="25" valign="center">
            <img runat="server" id="imageDate"
                src="DatePicker/images/calendar.gif"
                style="CURSOR: pointer"
                align="absbottom" >
        </td>
    </tr>
</table>
<div runat="server" id="divDate" style="VISIBILITY:hidden;POSITION:absolute;BACKGROUND-COLOR:white;layer-background-color:white" />
</asp:Panel>
