using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin.Controls {
    public class ImageList : UserControl {
        protected DataList grdImages;
        protected Button btnSwitchSelectMode;

        public ImageLibItem[] DataSource {
            get { return (ImageLibItem[])ViewState["DataSource"]; }
            set { ViewState["DataSource"] = value; }
        }

        public ImageLibItem[] SelectedItems {
            get {
                ArrayList list = new ArrayList();

                foreach (DataListItem item in grdImages.Items) {
                    HtmlInputCheckBox chkSelect = (HtmlInputCheckBox)item.FindControl("chkSelect");
                    if (chkSelect.Checked) {
                        int id = Convert.ToInt32(chkSelect.Value);
                        list.Add(new ImageLibItem(id));
                    }
                }

                return (ImageLibItem[])list.ToArray(typeof(ImageLibItem));
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///	Required method for Designer support - do not modify
        ///	the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.DataBinding += new EventHandler(ImageList_DataBinding);
            this.grdImages.ItemDataBound += new DataListItemEventHandler(grdImages_ItemDataBound);
        }
        #endregion

        private void ImageList_DataBinding(object sender, EventArgs e) {
            grdImages.DataSource = DataSource;
            btnSwitchSelectMode.Attributes[HtmlHelper.OnClick] = 
                "return SwitchSelectMode();";
        }

        private void grdImages_ItemDataBound(object sender, DataListItemEventArgs e) {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem) {
                ImageLibItem item = (ImageLibItem)e.Item.DataItem;

                HtmlImage imgItem = (HtmlImage)e.Item.FindControl("imgItem");
                imgItem.Src = ImageLibraryHandler.UrlGetIcon(item.Id).PathAndQuery;

                HtmlInputCheckBox chkSelect = (HtmlInputCheckBox)e.Item.FindControl("chkSelect");
                chkSelect.Value = item.Id.ToString();
            }
        }
    }
}