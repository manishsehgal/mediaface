using System;
using System.Collections;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.WebAdmin.Helpers;

namespace Neato.Dol.WebAdmin.Controls {

    /// <summary>
    /// Client date picker control
    /// </summary>
    [ValidationProperty("Text")]
    public class DatePicker : UserControl {

        private const string TODAY_TEXT_RES_KEY = "TODAY_TEXT";

        private const string READ_ONLY_KEY = "ReadOnly";
        private const string AUTOPOSTBACK_KEY = "AutoPostBack";

        private const string COMMON_SCRIPT = @"
<LINK href='controls/DatePicker/css/calendar.css' type='text/css' rel='stylesheet'>
<script language='javascript' src='controls/DatePicker/js/AnchorPosition.js'></script>
<script language='javascript' src='controls/DatePicker/js/PopupWindow.js'></script>
<script language='javascript' src='controls/DatePicker/js/date.js'></script>
<script language='javascript' src='controls/DatePicker/js/CalendarPopup.js'></script>
<script language='javascript' src='controls/DatePicker/js/DatePicker.js'></script>";

        private const string UNIQUE_SCRIPT_FORMAT = @"
<script language='javascript'>
    var {0} = new CalendarPopup('{1}');
    {0}.addDisabledDates(null,'{2}');
</script>
<script language='javascript'>
    {0}.setMonthNames({3});
    {0}.setMonthAbbreviations({4});
    //{0}.setDayHeaders({5});
    {0}.setTodayText(""{6}"");
    {0}.offsetX = 30;
    {0}.offsetY = 0;
</script>
";
        private const string IMG_DATE_ONCLICK_SCRIPT_FORMAT = @"
OpenCalendar({0}, '{1}', '{2}', '{3}', '{4}')";

        private const string TXT_DATE_ONCHANGE_SCRIPT = @"
window.document.forms[0].submit()";

        private string uniqueScriptKey {
            get { return ClientID + ".Startup"; }
        }

        private string calDateID {
            get { return ClientID + "_calDate"; }
        }

        protected Panel                 panDate;
        protected HtmlImage             imageDate;
        protected HtmlGenericControl    divDate;
        protected TextBox               txtDate;

        private   ResourceManager       _resourceManager;

        public event EventHandler ValueChanged;

        public string Text {
            get { return txtDate.Text; }
        }

        public Unit Width {
            get { return panDate.Width; }
            set { panDate.Width = value; }
        }

        public DateTime Value {
            get {
                try {
                    return VisibleValuesFormatter.StringToDate(txtDate.Text);
                } catch (FormatException) {
                    return DateTime.MinValue;
                }
            }
            set {
                if (value > DateTime.MinValue) {
                    txtDate.Text = VisibleValuesFormatter.DateToString(value);
                } else {
                    txtDate.Text = string.Empty;
                }
            }
        }

        public bool ReadOnly {
            get { return ViewState[READ_ONLY_KEY] == null ? false : (bool)ViewState[READ_ONLY_KEY]; }
            set { ViewState[READ_ONLY_KEY] = value; }
        }

        public bool AutoPostBack {
            get {
                if (ViewState[AUTOPOSTBACK_KEY] == null) return false;
                return (bool)ViewState[AUTOPOSTBACK_KEY];
            }
            set {
                ViewState[AUTOPOSTBACK_KEY] = value;
            }
        }

        public DatePicker() : base() {
            _resourceManager = new ResourceManager(typeof(DatePicker));
        }

        private void Page_Load(object sender, EventArgs e) {
            if(!IsPostBack) {
                InitConrols();
            }
        }

        protected override void OnUnload(EventArgs e) {
            base.OnUnload (e);
            _resourceManager.ReleaseAllResources();
        }

        private void Page_PreRender(object sender, EventArgs e) {
            if (!Visible) return;
            RegisterScripts();
        }

        private void txtDate_TextChanged(object sender, EventArgs e) {
            OnValueChanged();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.PreRender += new EventHandler(Page_PreRender);
            this.txtDate.TextChanged += new EventHandler(txtDate_TextChanged);
        }
        #endregion

        protected void OnValueChanged() {
            if (ValueChanged == null) return;
            ValueChanged(this, EventArgs.Empty);
        }

        private void InitConrols() {
            const string ATTR_TITLE   = "title";
            imageDate.Attributes[ATTR_TITLE] = _resourceManager.GetString(imageDate.ID);
        }

        private static string GetMonthNames() {
            ArrayList names = new ArrayList();
            string[] monthNames = Thread.CurrentThread.CurrentUICulture.DateTimeFormat.MonthNames;
            foreach (string name in monthNames) {
                if (name.Length == 0) continue;
                names.Add(string.Format(CultureInfo.InvariantCulture, "'{0}'", name));
            }
            return string.Join(",", (string[])names.ToArray(typeof(string)));
        }

        private static string GetMonthAbbreviations() {
            ArrayList names = new ArrayList();
            string [] abbreviatedMonthNames = Thread.CurrentThread.CurrentUICulture.DateTimeFormat.AbbreviatedMonthNames;
            foreach (string name in abbreviatedMonthNames) {
                if (name.Length == 0) continue;
                names.Add(string.Format(CultureInfo.InvariantCulture, "'{0}'", name));
            }
            return string.Join(",", (string[])names.ToArray(typeof(string)));
        }

        private static string GetDayHeaders() {
            ArrayList names = new ArrayList();
            string[] abbreviatedDayNames = Thread.CurrentThread.CurrentUICulture.DateTimeFormat.AbbreviatedDayNames;
            foreach (string name in abbreviatedDayNames) {
                if (name.Length == 0) continue;
                names.Add(string.Format(CultureInfo.InvariantCulture, "'{0}'", name));
            }
            return string.Join(",", (string[])names.ToArray(typeof(string)));
        }

        private void RegisterScripts(){
            const string COMMON_SCRIPT_KEY = "DatePicker.Startup";
            Page.RegisterClientScriptBlock(COMMON_SCRIPT_KEY, COMMON_SCRIPT);

            string script = string.Format(CultureInfo.InvariantCulture, 
                UNIQUE_SCRIPT_FORMAT,
                calDateID,
                divDate.ClientID,
                null,
                GetMonthNames(),
                GetMonthAbbreviations(),
                GetDayHeaders(),
                _resourceManager.GetString(TODAY_TEXT_RES_KEY));
            Page.RegisterClientScriptBlock(uniqueScriptKey, script);

            if (AutoPostBack) {
                txtDate.Attributes["onchange"] = TXT_DATE_ONCHANGE_SCRIPT;
            }

            imageDate.Attributes["onclick"] = ReadOnly
                ? string.Empty
                : string.Format(CultureInfo.InvariantCulture,
                IMG_DATE_ONCLICK_SCRIPT_FORMAT,
                calDateID,
                txtDate.ClientID,
                                imageDate.ClientID,
                divDate.ClientID,
                VisibleValuesFormatter.DateFormatJS);
        }
    }
}
