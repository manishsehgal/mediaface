using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin.Controls {
    [ToolboxData("<{0}:Selector runat=server></{0}:Selector>")]
    public class Selector : ListBox {
        #region JavaScripts
        private const string JSFunctionSelectorMoveKey = "JSFunctionSelectorMoveKey";
        private const string JSFunctionSelectorMove = @"
function SelectorMove(lstAll, source, target) {
    while (target.options.length > 0) {
        target.options.remove(0);
    }
    for (var i = 0; i < lstAll.options.length; i++) {
        var val = lstAll.options(i).value;
        var sourceIndex = -1;
        for (var j = 0; j < source.options.length; j++) {
            if (source.options(j).value == val) {
                sourceIndex = j;
                break;
            }
        }

        var createOption = (sourceIndex < 0 || source.options(sourceIndex).selected == true);
        if (createOption) {
            var oOption = document.createElement('OPTION');
            oOption.value = lstAll.options(i).value;
            oOption.text = lstAll.options(i).text;
            target.options.add(oOption);
            if (sourceIndex >= 0) {
                source.options.remove(sourceIndex);
                lstAll.options(i).selected = !lstAll.options(i).selected
            }
        }
    }
}
";
        private const string JSFunctionSelectorAddKey = "{0}JSFunctionSelectorAddKey";
        private const string JSFunctionSelectorAdd = @"
function {0}SelectorAdd() {{
    var lstAll = document.getElementById('{0}');
    var source = document.getElementById('lstSource');
    var target = document.getElementById('lstTarget');
    SelectorMove(lstAll, source, target);
}}
";
        private const string JSFunctionSelectorRemoveKey = "{0}JSFunctionSelectorRemoveKey";
        private const string JSFunctionSelectorRemove = @"
function {0}SelectorRemove() {{
    var lstAll = document.getElementById('{0}');
    var target = document.getElementById('lstSource');
    var source = document.getElementById('lstTarget');
    SelectorMove(lstAll, source, target);
}}
";
        #endregion

        private ListBox lstSource;
        private ListBox lstTarget;
        private HtmlInputButton btnAdd;
        private HtmlInputButton btnRemove;

        private void Selector_Load(object sender, EventArgs e) {
            if (!Page.IsPostBack) {
                DataBind();
            }
        }

        public ICollection SelectedItemValues {
            get {
                ArrayList selectedItemValues = new ArrayList();
                for (int index = 0; index < this.Items.Count; index++) {
                    ListItem item = this.Items[index];
                    if (item.Selected) {
                        selectedItemValues.Add(item.Value);
                    }
                }
                return selectedItemValues;
            }
        }

        public ICollection SelectedIndices {
            get {
                ArrayList selectedItemIndices = new ArrayList();
                for (int index = 0; index < this.Items.Count; index++) {
                    ListItem item = this.Items[index];
                    if (item.Selected) {
                        selectedItemIndices.Add(index);
                    }
                }
                return selectedItemIndices;
            }
            set {
                foreach (object obj in value) {
                    if (obj is int) {
                        int index = (int)obj;
                        this.Items[index].Selected = true;
                    }
                }
                OnSelectedIndexChanged(EventArgs.Empty);
            }
        }

        private void Selector_DataBinding(object sender, EventArgs e) {
            lstSource.DataSource = DataSource;
            lstSource.DataTextField = DataTextField;
            lstSource.DataValueField = DataValueField;
            lstSource.DataBind();

            btnRemove.CausesValidation = false;
            btnRemove.Value = "<";
            btnRemove.Attributes[HtmlHelper.OnClick] = string.Format("javascript: {0}SelectorRemove();", this.ClientID);

            btnAdd.CausesValidation = false;
            btnAdd.Value = ">";
            btnAdd.Attributes[HtmlHelper.OnClick] = string.Format("javascript: {0}SelectorAdd();", this.ClientID);

            string moveScript = JavaScriptHelper.SurroundJavaScript(JSFunctionSelectorMove);
            Page.RegisterClientScriptBlock(JSFunctionSelectorMoveKey, moveScript);
            string addScript = JavaScriptHelper.SurroundJavaScript(string.Format(JSFunctionSelectorAdd, this.ClientID));
            Page.RegisterClientScriptBlock(string.Format(JSFunctionSelectorAddKey, this.ClientID), addScript);
            string removeScript = JavaScriptHelper.SurroundJavaScript(string.Format(JSFunctionSelectorRemove, this.ClientID));
            Page.RegisterClientScriptBlock(string.Format(JSFunctionSelectorRemoveKey, this.ClientID), removeScript);
        }

        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Width = new Unit(0);
            this.Height = new Unit(0);
            this.SelectionMode = ListSelectionMode.Multiple;
            this.Attributes[HtmlHelper.Style] = "DISPLAY: none; VISIBILITY: hidden";

            lstSource = new ListBox();
            lstSource.ID = "lstSource";
            lstSource.Width = new Unit(100, UnitType.Percentage);
            lstSource.SelectionMode = ListSelectionMode.Multiple;

            lstTarget = new ListBox();
            lstTarget.ID = "lstTarget";
            lstTarget.Width = new Unit(100, UnitType.Percentage);
            lstTarget.SelectionMode = ListSelectionMode.Multiple;

            btnRemove = new HtmlInputButton();
            btnRemove.ID = "btnRemove";
            btnAdd = new HtmlInputButton();
            btnAdd.ID = "btnAdd";

            this.Load += new EventHandler(Selector_Load);
            this.DataBinding += new EventHandler(Selector_DataBinding);
            this.SelectedIndexChanged += new EventHandler(Selector_SelectedIndexChanged);
        }

        /// <summary> 
        /// Render this control to the output parameter specified.
        /// </summary>
        /// <param name="output"> The HTML writer to write out to </param>
        protected override void Render(HtmlTextWriter output) {
            Table table = new Table();
            TableRow row = new TableRow();
            TableCell cellLeft = new TableCell();
            TableCell cellCenter = new TableCell();
            TableCell cellRight = new TableCell();

            cellLeft.Width = new Unit(50, UnitType.Percentage);
            cellRight.Width = new Unit(50, UnitType.Percentage);

            cellLeft.Controls.Add(lstSource);
            cellCenter.Controls.Add(btnAdd);
            Literal br = new Literal();
            br.Text = HtmlHelper.BrTag;
            cellCenter.Controls.Add(br);
            cellCenter.Controls.Add(btnRemove);
            cellRight.Controls.Add(lstTarget);

            row.Cells.Add(cellLeft);
            row.Cells.Add(cellCenter);
            row.Cells.Add(cellRight);

            table.Rows.Add(row);

            table.RenderControl(output);
            base.Render(output);
        }

        private void Selector_SelectedIndexChanged(object sender, EventArgs e) {
            lstTarget.Items.Clear();
            lstSource.Items.Clear();
            foreach (ListItem item in this.Items) {
                if (item.Selected) {
                    lstTarget.Items.Add(new ListItem(item.Text, item.Value));
                } else {
                    lstSource.Items.Add(new ListItem(item.Text, item.Value));
                }
            }
        }
    }
}