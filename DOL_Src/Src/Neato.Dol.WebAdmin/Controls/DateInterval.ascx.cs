using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.WebAdmin.Helpers;

namespace Neato.Dol.WebAdmin.Controls {
    /// <summary>
    ///	Summary description for DateInterval.
    /// </summary>
    public class DateInterval : UserControl {
        protected Label lblDateFormatWarning;
        protected DatePicker dtpStartDate;
        protected DatePicker dtpEndDate;
        protected HtmlTableCell tdStartDate;
        protected HtmlTableCell tdEndDate;
        protected CustomValidator vldDateFormat;
        protected CustomValidator vldDateRange;

        public DateTime StartDate {
            get {
                return (dtpStartDate.Text == string.Empty) ?
                    DateTime.Now.AddYears(-100) : dtpStartDate.Value;
            }
            set { dtpStartDate.Value = value; }
        }

        public DateTime EndDate {
            get {
                return (dtpEndDate.Text == string.Empty)
                    ? DateTime.Now.AddYears(+100).AddDays(1).AddSeconds(-1)
                    : dtpEndDate.Value.AddDays(1).AddSeconds(-1);
            }
            set { dtpEndDate.Value = value; }
        }
        
        public bool IsDateFormatError {
            get {
                try {
                    IFormatProvider culture = new CultureInfo("en-us", true);
                    DateTime testDate1 = VisibleValuesFormatter.
                        StringToDate(
                        (dtpStartDate.Text == string.Empty)
                        ? DateTime.Now.ToString(culture)
                        : dtpStartDate.Text);
                    DateTime testDate2 = VisibleValuesFormatter.
                        StringToDate(
                        (dtpEndDate.Text == string.Empty)
                        ? DateTime.Now.ToString(culture)
                        : dtpEndDate.Text);
                    testDate1.AddMinutes(testDate2.Minute);
                    return false;
                } catch {
                    return true;
                }
            }
        }

        public bool IsDateRangeError {
            get {
                return (!IsDateFormatError &&
                    dtpStartDate.Text != string.Empty &&
                    dtpEndDate.Text != string.Empty &&
                    StartDate > EndDate);
            }
        }

        public string DateControlWidth {
            set { dtpStartDate.Width = dtpEndDate.Width = new Unit(value); }
        }

        public string CaptionWidth {
            set { tdStartDate.Width = tdEndDate.Width = value; }
        }


        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }


        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(DateInterval_DataBinding);
            this.vldDateFormat.ServerValidate += new ServerValidateEventHandler(vldDateFormat_ServerValidate);
            this.vldDateRange.ServerValidate += new ServerValidateEventHandler(vldDateRange_ServerValidate);

        }
        #endregion

        private void DateInterval_DataBinding(object sender, EventArgs e) {
            lblDateFormatWarning.Text = "If you want to type in please type in format of MM/DD/YYYY";
        }

        private void vldDateFormat_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = !IsDateFormatError;
        }

        private void vldDateRange_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = !IsDateRangeError;
        }
    }
}