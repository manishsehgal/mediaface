using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Neato.Dol.Business;
using Neato.Dol.WebAdmin.Controls;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin {
    public class TrackingUpcStapleReport : Page {
        protected DataGrid grdReport;
        protected Label lblTitle;
        protected Label lblStatus;
        protected DateInterval dtiDates;
        protected Button btnSubmit;

        #region Url
        private const string RawUrl = "TrackingUpcStapleReport.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(TrackingUpcStapleReport_DataBinding);
            this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        }

        #endregion

        private void TrackingUpcStapleReport_DataBinding(object sender, EventArgs e) {
            lblTitle.Text = "What MFOPE UPC codes have been chosen";
            lblStatus.Text = "There are no results matching your criteria.";
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            if (IsValid) {
                
                lblStatus.Visible = false;
                grdReport.Visible = false;

                DataSet data = BCTrackingUpcStapleCodes.EnumByTime(dtiDates.StartDate, dtiDates.EndDate);

                bool dataExisting = (data.Tables[0].Rows.Count > 0);

                if (dataExisting) {
                    grdReport.DataSource = data;
                }
                grdReport.Visible = dataExisting;
                lblStatus.Visible = !dataExisting;

                DataBind();
            }
        }
    }
}