using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin {
    public class TrackingUserActionReport : Page {
        protected DataGrid grdReport;
        private static int TableItemsCountPerPage = 25;
        #region Url
        private const string RawUrl = "TrackingUserActionReport.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(TrackingUserActionReport_DataBinding);
            grdReport.DataBinding +=new EventHandler(grdReport_DataBinding);
            grdReport.PageIndexChanged +=new DataGridPageChangedEventHandler(grdReport_PageIndexChanged);       

        }
        #endregion

        private void TrackingUserActionReport_DataBinding(object sender, EventArgs e) {
            grdReport.DataBind();

        }

        private void grdReport_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            grdReport.CurrentPageIndex = e.NewPageIndex;
            grdReport.DataBind();
        }

        private void grdReport_DataBinding(object sender, EventArgs e)
        {
            DataSet data = BCTracking.GetList();
            grdReport.DataSource = data;
            grdReport.PageSize = TableItemsCountPerPage;
        }
    }
}