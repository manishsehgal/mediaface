using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebAdmin.Controls;
using Neato.Dol.WebAdmin.Helpers;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin {
    public class ManageLabel : Page {
        private const string LblNameId = "lblName";
        private const string LblGuidId = "lblGuid";
        private const string ImgIconId = "imgIcon";
        private const string CtlIconFileId = "ctlIconFile";
        private const string TxtNameId = "txtName";
        private const string BtnDeleteId = "btnDelete";
        private const string VldLabelXmlFileRequiredId = "vldLabelXmlFileRequired";
        private const string CtlLabelXmlFileId = "ctlLabelXmlFile";
        private const string HypIconViewId = "hypIconView";
        private const string CtlDevicesId = "ctlDevices";
        private const string TxtDevicesId = "txtDevices";

        private const int NumberOfColumns = 5;

        private const string AllValue = "-1";
        private const string SelectValue = "-2";
        private const string NoDataValue = "-3";

        protected Button btnNew;
        protected DataGrid grdLabels;
        protected DropDownList cboDeviceFilter;
        protected DropDownList cboDeviceTypeFilter;
        protected TextBox txtLabelName;
        protected Button btnSearch;

        private ArrayList deviceTypes;
        private ArrayList allDevices;
        private ArrayList devices;
        private ArrayList labels;

        private const string DeviceFilterKey = "DeviceFilterKey";
        private const string DeviceTypeFilterKey = "DeviceTypeFilterKey";

        private PageFilter CurrentFilter {
            get { return (PageFilter)ViewState["CurrentFilter"]; }
            set { ViewState["CurrentFilter"] = value; }
        }

        private bool NewMode {
            get { return (bool)ViewState["NewModeKey"]; }
            set { ViewState["NewModeKey"] = value; }
        }

        #region Url
        private const string RawUrl = "ManageLabel.aspx";

        public static Uri Url(string deviceFilter, string deviceTypeFilter) {
            return UrlHelper.BuildUrl(RawUrl,
                DeviceFilterKey, deviceFilter,
                DeviceTypeFilterKey, deviceTypeFilter);
        }

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                InitFilters();
                NewMode = false;
                DataBind();
            }
        }

        private void InitFilters() {
            CurrentFilter = new PageFilter();

            CurrentFilter[DeviceFilterKey] = Request.QueryString[DeviceFilterKey];
            if (CurrentFilter[DeviceFilterKey] == null) CurrentFilter[DeviceFilterKey] = SelectValue;

            CurrentFilter[DeviceTypeFilterKey] = Request.QueryString[DeviceTypeFilterKey];
            if (CurrentFilter[DeviceTypeFilterKey] == null) CurrentFilter[DeviceTypeFilterKey] = SelectValue;
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManageLabel_DataBinding);
            grdLabels.ItemDataBound += new DataGridItemEventHandler(grdLabels_ItemDataBound);
            grdLabels.EditCommand += new DataGridCommandEventHandler(grdLabels_EditCommand);
            grdLabels.DeleteCommand += new DataGridCommandEventHandler(grdLabels_DeleteCommand);
            grdLabels.UpdateCommand += new DataGridCommandEventHandler(grdLabels_UpdateCommand);
            grdLabels.CancelCommand += new DataGridCommandEventHandler(grdLabels_CancelCommand);
            grdLabels.ItemCreated += new DataGridItemEventHandler(grdLabels_ItemCreated);
            btnNew.Click += new EventHandler(btnNew_Click);
            btnSearch.Click += new EventHandler(btnSearch_Click);
            this.PreRender += new EventHandler(ManageLabel_PreRender);
        }
        #endregion

        private void ManageLabel_DataBinding(object sender, EventArgs e) {
            deviceTypes = new ArrayList(BCCategory.GetCategoryList());
            allDevices = new ArrayList(BCDevice.GetDeviceList());
            allDevices.Sort(Device.GetFullModelComparer(CultureInfo.InvariantCulture));

            if (txtLabelName.Text != string.Empty &&
                cboDeviceFilter.SelectedValue == SelectValue &&
                cboDeviceTypeFilter.SelectedValue == SelectValue) {
                cboDeviceFilter.SelectedValue = AllValue;
                cboDeviceTypeFilter.SelectedValue = AllValue;
            }

            if (IsPostBack) {
                CurrentFilter[DeviceFilterKey] = cboDeviceFilter.SelectedValue;
                CurrentFilter[DeviceTypeFilterKey] = cboDeviceTypeFilter.SelectedValue;
            }

            cboDeviceFilter_DataBind();
            cboDeviceTypeFilter_DataBind();

            if (!CurrentFilter.FiltersHaveSameValue(SelectValue)) {
                if (CurrentFilter.GetFiltersWithoutValue(SelectValue).Length > 0) {
                    CurrentFilter.ReplaceFilterValue(SelectValue, AllValue);
                }

                DeviceBase device = null;
                int deviceId = int.Parse(CurrentFilter[DeviceFilterKey], CultureInfo.InvariantCulture.NumberFormat);
                if (deviceId > 0 || deviceId == Convert.ToInt32(NoDataValue)) {
                    device = new DeviceBase(deviceId);
                }
                DeviceType deviceType =
                    (CurrentFilter[DeviceTypeFilterKey] == AllValue) ?
                    DeviceType.Undefined :
                    (DeviceType)(Convert.ToInt32(CurrentFilter[DeviceTypeFilterKey]));

                labels = new ArrayList(BCFace.EnumerateFaceByParam(device, deviceType, txtLabelName.Text));
            } else {
                labels = new ArrayList();
            }

            if (NewMode) {
                labels.Insert(0, BCFace.NewFace());
                grdLabels.EditItemIndex = 0;
                devices = new ArrayList();
            } else if (grdLabels.EditItemIndex >= 0) {
                int faceId = (int)grdLabels.DataKeys[grdLabels.EditItemIndex];
                FaceBase face = new FaceBase(faceId);
                int index = labels.IndexOf(face);
                grdLabels.EditItemIndex = index;
                devices = new ArrayList(BCDevice.GetDeviceList(face));
            }
            grdLabels.DataSource = labels;
            grdLabels.DataKeyField = Face.IdField;
        }

        private void cboDeviceFilter_DataBind() {
            cboDeviceFilter.Items.Clear();
            foreach (Device device in allDevices) {
                ListItem item = new ListItem(device.FullModel, device.Id.ToString());
                cboDeviceFilter.Items.Add(item);
            }
            cboDeviceFilter.Items.Insert(0, new ListItem("All", AllValue));
            cboDeviceFilter.Items.Insert(1, new ListItem("No devices", NoDataValue));
            if (CurrentFilter[DeviceFilterKey] == SelectValue) {
                cboDeviceFilter.Items.Insert(0, new ListItem("Select", SelectValue));
                return;
            }
            foreach (ListItem item in cboDeviceFilter.Items) {
                if (item.Value == CurrentFilter[DeviceFilterKey]) {
                    item.Selected = true;
                    break;
                }
            }
        }

        private void cboDeviceTypeFilter_DataBind() {
            cboDeviceTypeFilter.Items.Clear();
            foreach (Category category in deviceTypes) {
                ListItem item = new ListItem(category.Name, category.Id.ToString());
                cboDeviceTypeFilter.Items.Add(item);
            }
            cboDeviceTypeFilter.Items.Insert(0, new ListItem("All", AllValue));
            if (CurrentFilter[DeviceTypeFilterKey] == SelectValue) {
                cboDeviceTypeFilter.Items.Insert(0, new ListItem("Select", SelectValue));
                return;
            }
            foreach (ListItem item in cboDeviceTypeFilter.Items) {
                if (item.Value == CurrentFilter[DeviceTypeFilterKey]) {
                    item.Selected = true;
                    break;
                }
            }
        }

        private void grdLabels_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void ItemDataBound(DataGridItem item) {
            Face face = (Face)item.DataItem;
            Label lblName = (Label)item.FindControl(LblNameId);
            lblName.Text = HttpUtility.HtmlEncode(face.GetName(""));
            Label lblGuid = (Label)item.FindControl(LblGuidId);
            lblGuid.Text = face.Guid.ToString().ToUpper();

            Image imgIcon = (Image)item.FindControl(ImgIconId);

            string imageUrl = IconHandler.ImageUrl(face).AbsoluteUri;
            imgIcon.ImageUrl = imageUrl;

            HyperLink hypIconView = (HyperLink)item.FindControl(HypIconViewId);
            hypIconView.NavigateUrl = imageUrl;
            hypIconView.Target = HtmlHelper.Blank;

            TextBox txtDevices = (TextBox)item.FindControl(TxtDevicesId);
            StringBuilder sb = new StringBuilder();
            foreach (Device device in BCDevice.GetDeviceList(face)) {
                if (sb.Length == 0) {
                    sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, device.FullModel);
                } else {
                    sb.Append(Environment.NewLine);
                    sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, device.FullModel);
                }
            }
            txtDevices.Text = sb.ToString();

            Button btnDelete = (Button)item.FindControl(BtnDeleteId);
            string warning = "Do you want to remove this label?";
            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", warning);
        }

        private void EditItemDataBound(DataGridItem item) {
            Face face = (Face)item.DataItem;
            TextBox txtName = (TextBox)item.FindControl(TxtNameId);
            txtName.Text = HttpUtility.HtmlEncode(face.GetName(""));
            Label lblGuid = (Label)item.FindControl(LblGuidId);
            lblGuid.Text = face.Guid.ToString().ToUpper();
            lblGuid.Visible = !NewMode;
            JavaScriptHelper.RegisterFocusScript(this, txtName.ClientID);

            RequiredFieldValidator vldLabelXmlFileRequired = (RequiredFieldValidator)item.FindControl(VldLabelXmlFileRequiredId);
            vldLabelXmlFileRequired.Enabled = NewMode;

            Selector ctlDevices = (Selector)item.FindControl(CtlDevicesId);
            ctlDevices.DataSource = allDevices;
            ctlDevices.DataValueField = Device.IdField;
            ctlDevices.DataTextField = Device.FullModelField;
            ctlDevices.DataBind();
            ctlDevices.SelectedIndices = IndicesHelper.GetIndices(allDevices, devices);
        }

        private void grdLabels_EditCommand(object source, DataGridCommandEventArgs e) {
            grdLabels.EditItemIndex = e.Item.ItemIndex;
            NewMode = false;
            DataBind();
        }

        private void grdLabels_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdLabels.EditItemIndex = -1;
            int faceId = (int)grdLabels.DataKeys[e.Item.ItemIndex];
            FaceBase face = new FaceBase(faceId);
            try {
                BCFace.Delete(face);
                Response.Redirect(Url(CurrentFilter[DeviceFilterKey], CurrentFilter[DeviceTypeFilterKey]).PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewMode = false;
            DataBind();
        }

        private void grdLabels_UpdateCommand(object source, DataGridCommandEventArgs e) {
            Selector ctlDevices = (Selector)e.Item.FindControl(CtlDevicesId);
            if (!IsValid) {
                allDevices = new ArrayList(BCDevice.GetDeviceList());
                allDevices.Sort(Device.GetFullModelComparer(CultureInfo.InvariantCulture));
                ctlDevices.DataSource = allDevices;
                ArrayList list = new ArrayList(ctlDevices.SelectedIndices);
                ctlDevices.DataBind();
                ctlDevices.SelectedIndices = list;
                return;
            }

            HtmlInputFile ctlIconFile = (HtmlInputFile)e.Item.FindControl(CtlIconFileId);
            Stream icon = ctlIconFile.PostedFile.InputStream;

            HtmlInputFile ctlLabelXmlFile = (HtmlInputFile)e.Item.FindControl(CtlLabelXmlFileId);
            Stream labelXml = ctlLabelXmlFile.PostedFile.InputStream;

            int faceId = (int)grdLabels.DataKeys[e.Item.ItemIndex];

            Face face = null;
            if (labelXml == null || labelXml.Length == 0) {
                if (NewMode) {
                    face = BCFace.NewFace();
                } else {
                    face = BCFace.GetFace(new FaceBase(faceId));
                }
            } else {
                face = BCPaperImport.ParseFace(labelXml);
            }
            face.Id = faceId;

            TextBox txtName = (TextBox)e.Item.FindControl(TxtNameId);
            if (txtName.Text != string.Empty) {
                face.AddName("", txtName.Text.Trim());
            }

            ArrayList deviceList = new ArrayList(ctlDevices.SelectedItemValues.Count);
            foreach (object itemValue in ctlDevices.SelectedItemValues) {
                if (itemValue is string) {
                    int deviceId = int.Parse((string)itemValue, CultureInfo.InvariantCulture.NumberFormat);
                    deviceList.Add(new DeviceBase(deviceId));
                }
            }
            DeviceBase[] newDevices = (DeviceBase[])deviceList.ToArray(typeof(DeviceBase));

            try {
                BCFace.Save(face, icon, newDevices);
                Response.Redirect(Url(
                    CurrentFilter[DeviceFilterKey],
                    CurrentFilter[DeviceTypeFilterKey]).
                    PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }

            grdLabels.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdLabels_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdLabels.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewMode = true;
            DataBind();
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            grdLabels.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void ManageLabel_PreRender(object sender, EventArgs e) {
            grdLabels.Visible = grdLabels.Items.Count > 0;
        }

        protected void vldLabelXml_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = true;
            if (grdLabels.EditItemIndex >= 0) {
                DataGridItem item = grdLabels.Items[grdLabels.EditItemIndex];

                HtmlInputFile ctlLabelXmlFile = (HtmlInputFile)item.FindControl(CtlLabelXmlFileId);
                if (ctlLabelXmlFile.PostedFile.InputStream.Length != 0) {
                    ArrayList errorList = new ArrayList(BCPaperImport.Validate
                        (ctlLabelXmlFile.PostedFile.InputStream, ImportSchema.Face));

                    if (errorList.Count == 0) {
                        Label lblGuid = (Label)item.FindControl(LblGuidId);
                        Guid guid = new Guid(lblGuid.Text);
                        Face face = BCPaperImport.ParseFace(ctlLabelXmlFile.PostedFile.InputStream);

                        if (!face.IsGuidValid) {
                            errorList.Add("Invalid GUID format. Valid format: GUID=\"{XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}\", X are hexadecimal number.");
                        } else {
                            if (NewMode && BCFace.IsFaceGuidPresentInDB(face.Guid)) {
                                errorList.Add(string.Format(
                                    "Label with GUID {0} already present in DB.", face.Guid));
                            }

                            if (!NewMode && guid != face.Guid) {
                                errorList.Add(string.Format(
                                    "GUID does not match: DB GUID = {0}, XML GUID = {1}.",
                                    guid, face.Guid));
                            }                        
                        }
                    }

                    string[] errors = (string[])errorList.ToArray(typeof(string));

                    args.IsValid = (errors.Length == 0);

                    StringBuilder sb = new StringBuilder();
                    foreach (string error in errors) {
                        if (sb.Length == 0) {
                            sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(error));
                        } else {
                            sb.Append(HtmlHelper.BrTag);
                            sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(error));
                        }
                    }

                    ((CustomValidator)source).ErrorMessage = sb.ToString();
                } else {
                    string fileName = ctlLabelXmlFile.Value;
                    if (fileName != string.Empty) {
                        args.IsValid = false;
                        string errorMessageFormat = "Can't open file '{0}'";
                        ((CustomValidator)source).ErrorMessage =
                            string.Format(errorMessageFormat, fileName);
                    }
                }
            }
        }

        private void grdLabels_ItemCreated(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            if (item.ItemType != ListItemType.EditItem)
                return;

            GridTransformationHelper.EditItemCreationForm(item, grdLabels.Columns, NumberOfColumns, "570px");
        }
    }
}