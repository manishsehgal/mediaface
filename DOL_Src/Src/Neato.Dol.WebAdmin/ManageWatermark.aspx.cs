using System;
using System.Web.UI;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin {
    public class ManageWatermark : Page {
        #region Url

        private const string RawUrl = "ManageWatermark.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        #endregion

        private void Page_Load(object sender, EventArgs e) {
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}