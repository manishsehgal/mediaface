<%@ Page language="c#" Codebehind="Announcements.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.Announcements" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Announcements editing</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <script type="text/javascript" language="javascript" src="js/Announcements.js"></script>
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
      <h3 align="center" >Announcements</h3>
      <span>To create new announcement click: </span>
      <asp:Button ID="btnNew" Runat="server" Text="New" CausesValidation="False"/>
      <br><br>
      <asp:DataGrid
        Runat="server"
        ID="dgAnnouncement"
        AutoGenerateColumns="False">
        <Columns>
          <asp:TemplateColumn HeaderText="Text/HTML">
            <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="60%"/>
            <ItemTemplate>
                <asp:Label ID="lblItemText" Runat="server" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtItemText" Runat="server" TextMode="MultiLine" Rows="5" MaxLength="1024" style="width:100%" />
            </EditItemTemplate>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Show at public site">
            <ItemTemplate>
              <asp:CheckBox
                Runat="server"
                ID="chkItemActive"
                OnCheckedChanged="chkItemActive_CheckedChanged"/>
            </ItemTemplate>
            <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="100px"/>
            <ItemStyle HorizontalAlign="Center"/>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Action">
            <ItemTemplate>
              <asp:ImageButton
                Runat="server"
                ID="btnItemPreview"
                ImageUrl="images/preview.gif"
                CausesValidation="False"
                AlternateText="Preview" />
              <asp:ImageButton
                Runat="server"
                ID="btnItemEdit"
                ImageUrl="images/edit.gif"
                CommandName="Edit"
                CausesValidation="False"
                AlternateText="Edit" />
              <asp:ImageButton
                Runat="server"
                ID="btnItemUpdate"
                ImageUrl="images/save.gif"
                CommandName="Update"
                CausesValidation="False"
                AlternateText="Save" />
              <asp:ImageButton
                Runat="server"
                ID="btnItemCancel"
                ImageUrl="images/cancel.gif"
                CommandName="Cancel"
                CausesValidation="False"
                AlternateText="Cancel" />
              <asp:ImageButton
                Runat="server"
                ID="btnItemDelete"
                ImageUrl="images/remove.gif"
                CommandName="Delete"
                CausesValidation="False"
                AlternateText="Remove" />
            </ItemTemplate>
            <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="100px"/>
            <ItemStyle Wrap="False"/>
          </asp:TemplateColumn>
        </Columns>
      </asp:DataGrid>
      
      <br>
      <asp:CustomValidator
        Display="Dynamic"
        Runat="server"
        ID="vldActiveAnnounces"
        EnableClientScript="False"
        Text="You cannot select more than one announcement for simultaneous appearance!"
        />
      <br>
      <br>
      <asp:Button ID="btnSubmit" Runat="server" Text="Apply to Site" CausesValidation="True"/>
      <br><br>
      <span>Please note you cannot select more than one announcement for simultaneous appearance.</span>
      <br><br>
      <iframe Runat="server" ID="ctlPreviewContaner" align="absmiddle" frameborder="yes" scrolling="auto" style="width:100%;height:745px;"/>
    </form>
  </body>
</html>
