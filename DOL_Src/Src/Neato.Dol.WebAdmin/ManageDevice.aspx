<%@ Page language="c#" Codebehind="ManageDevice.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.ManageDevice" %>
<%@ Register TagPrefix="cpt" NameSpace="Neato.Dol.WebAdmin.Controls" Assembly="Neato.Dol.WebAdmin" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
    <head>
        <title>Manage Devices</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <link type="text/css" rel="stylesheet" href="css/admin.css" />
    </head>
    <body MS_POSITIONING="GridLayout">
        <form id="Form1" method="post" runat="server">
            <h3 align="center">Devices Management</h3>
            <table border="0" cellpadding="4" cellspacing="0">
              <!--<tr>
                <td>Manufacturer :</td>
                <td>
                    <asp:DropDownList ID="cboManufacturerFilter" Runat="server"  style="width:250px" />
                </td>
              </tr>
              <tr>
                <td colspan="2" align="right">
                    <asp:Button runat="server" ID="btnDeviceSearch" Text="Search" />
                </td>
              </tr>-->
            </table>
            <br>
            <asp:Button ID="btnNew" Runat="server" Text="New Device" CausesValidation="False" />
            <br>
            <br>
            <asp:DataGrid Runat="server" ID="grdDevices" AutoGenerateColumns="False" >
                <Columns>
                    <asp:TemplateColumn HeaderText="Icon">
                        <HeaderStyle HorizontalAlign="Center" Width="110px"/>
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:HyperLink ID="hypIconView" Runat="server" >
                                <asp:Image ID="imgIcon" Runat="server"
                                    BorderStyle="Solid" BorderWidth="1" BorderColor="#d4d0c8" CssClass="imageIcon"/>
                            </asp:HyperLink>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <input ID="ctlIconFile" Runat="server" type="file" size="20" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Big Icon">
                        <HeaderStyle HorizontalAlign="Center" Width="110px" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:HyperLink ID="hypIconBigView" Runat="server" >
                                <asp:Image ID="imgBigIcon" Runat="server"
                                    BorderStyle="Solid" BorderWidth="1" BorderColor="#d4d0c8" CssClass="imageIcon" />
                            </asp:HyperLink>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <input ID="ctlBigIconFile" Runat="server" type="file" size="20"/>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Device Model">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                        <ItemStyle Wrap="True" Width="100px" />
                        <ItemTemplate>
                            <asp:Label ID="lblModel" Runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtModel" Runat="server" MaxLength="50" style="width:100%"  />
                            <asp:RequiredFieldValidator
                                ID="vldModelRequired" Runat="server"
                                ControlToValidate="txtModel" Display="Dynamic"
                                ErrorMessage="Enter the Device model" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Category">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle Wrap="False" HorizontalAlign="Center"/>
                        <ItemTemplate>
                            <asp:Label ID="lblCategory" Runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="cboCategory" Runat="server" style="width:100%" />
                            <asp:RequiredFieldValidator ID="vldCategoryRequired" Runat="server" ControlToValidate="cboCategory" Display="Dynamic"
                                InitialValue="-2" ErrorMessage="Choose Category" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Rating">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                        <ItemStyle Width="40px" />
                        <ItemTemplate>
                            <asp:Label ID="lblRating" Runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="cboRating" Runat="server" style="width:100%" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Action" >
                        <ItemTemplate>
                            <asp:ImageButton Runat="server" ID="btnEdit" ImageUrl="images/edit.gif"
                                CommandName="Edit" CausesValidation="False"
                                ImageAlign="AbsMiddle" AlternateText="Edit" />
                            <asp:ImageButton Runat="server" ID="btnDelete" ImageUrl="images/remove.gif"
                                CommandName="Delete" CausesValidation="False"
                                ImageAlign="AbsMiddle" AlternateText="Remove" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton Runat="server" ID="btnUpdate"
                                ImageUrl="images/save.gif" CommandName="Update" CausesValidation="True"
                                ImageAlign="AbsMiddle" AlternateText="Save" />
                            <asp:ImageButton Runat="server" ID="btnCancel"
                                ImageUrl="images/cancel.gif" CommandName="Cancel"
                                CausesValidation="False" ImageAlign="AbsMiddle" AlternateText="Cancel" />
                        </EditItemTemplate>
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                        <ItemStyle Wrap="False" Width="130px" />
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </form>
    </body>
</html>
