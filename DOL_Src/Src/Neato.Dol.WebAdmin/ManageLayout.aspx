<%@ Page language="c#" Codebehind="ManageLayout.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.ManageLayout" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>ManageLayout</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
	<style>td.Action, td.Action input {width: 100px}</style>
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
		<h3 align="center">Layout management</h3>
		<asp:Panel Runat="server" ID="panFaceLayout">
			<table border="0" cellpadding="4" cellspacing="0">
				<tr>
					<td>Label :</td>
					<td>
						<asp:DropDownList ID="cboLabelFilter" Runat="server" style="width:250px" />
					</td>
				</tr>
				<tr>
					<td>Device :</td>
					<td>
						<asp:DropDownList ID="cboDeviceFilter" Runat="server" style="width:250px" />
					</td>
				</tr>
				<tr>
					<td>Device Type :</td>
					<td>
						<asp:DropDownList ID="cboDeviceTypeFilter" Runat="server" style="width:250px" />
					</td>
				</tr>
					<tr>
						<td colspan="2" align="right">
							<asp:Button runat="server" ID="btnSearch" Text="Search" />
						</td>
					</tr>
			</table>
			<asp:Button ID="btnNew" Runat="server" Text="New Layout" CausesValidation="False" />
			<asp:Button ID="btnBlankLayouts" Runat="server" Text="Blank Layouts" CausesValidation="False" />
			<asp:DataGrid Runat="server" ID="grdLayout" AutoGenerateColumns="False">
				<Columns>
					<asp:TemplateColumn HeaderText="Icon">
						<HeaderStyle HorizontalAlign="Center" Width="110px" />
						<ItemStyle HorizontalAlign="Center" />
						<ItemTemplate>
							<asp:Image ID="imgIcon" Runat="server" BorderStyle="Solid" BorderWidth="1" BorderColor="#d4d0c8"
								CssClass="imageIcon" />
						</ItemTemplate>
						<EditItemTemplate>
							<input ID="ctlIconFile" Runat="server" type="file" size="20" NAME="ctlIconFile"/>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Label">
						<HeaderStyle HorizontalAlign="Center" Width="110px" />
						<ItemStyle HorizontalAlign="Center" />
						<ItemTemplate>
							<asp:Label Runat="server" ID="lblFaceName" />
						</ItemTemplate>
						<EditItemTemplate />
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Project">
						<HeaderStyle HorizontalAlign="Center" Width="110px" />
						<ItemStyle HorizontalAlign="Center" />
						<ItemTemplate>
							<asp:Label Runat="server" ID="lblName" />
						</ItemTemplate>
						<EditItemTemplate>
							<input ID="ctlProjectFile" Runat="server" type="file" size="20" NAME="ctlProjectFile"/>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Layout Items">
						<HeaderStyle HorizontalAlign="Center" Width="110px" />
						<ItemStyle HorizontalAlign="Center" />
						<ItemTemplate>
							<asp:TextBox ID="txtFaces" Runat="server"
								BorderWidth="0" BorderStyle="None"
								TextMode="MultiLine"
								ReadOnly="True"
								Width="200px"
								Height="100px"
							/>
						</ItemTemplate>
						<EditItemTemplate/>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Action">
						<ItemStyle CssClass="Action" />
						<ItemTemplate>
							<asp:Button Runat="server" ID="btnEdit" CommandName="Edit" CausesValidation="False"
								Text="Edit" Tooltip="Edit layout" />
							<asp:Button Runat="server" ID="btnEditLayoutItems" CommandName="EditLayoutItems" CausesValidation="False"
								Text="Edit default text" Tooltip="Edit default text" />
							<asp:Button Runat="server" ID="btnDelete" CommandName="Delete"
								CausesValidation="False" Text="Remove" Tooltip="Remove layout" />
						</ItemTemplate>
						<EditItemTemplate>
							<asp:Button Runat="server" ID="btnUpdate" Text="Save" CommandName="Update" CausesValidation="True"
								Tooltip="Save layout" />
							<asp:Button Runat="server" ID="btnCancel" CommandName="Cancel" Text="Cancel"
								CausesValidation="False" Tooltip="Cancel" />
						</EditItemTemplate>
						<HeaderStyle Wrap="False" HorizontalAlign="Center" />
					</asp:TemplateColumn>
				</Columns>
			</asp:DataGrid>
		</asp:Panel>
		<asp:Panel Runat="server" ID="panFaceLayoutItem" Visible="False">
			<asp:DataGrid Runat="server" ID="grdFaceLayoutItem" AutoGenerateColumns="False">
				<Columns>
					<asp:TemplateColumn HeaderText="No">
						<HeaderStyle HorizontalAlign="Center" Width="50px" />
						<ItemStyle HorizontalAlign="Center" />
						<ItemTemplate>
							<asp:Label Runat="server" ID="LblFaceLayoutItemPosition" />
						</ItemTemplate>
						<EditItemTemplate />
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Default text" HeaderStyle-HorizontalAlign="Center">
						<ItemTemplate>
							<asp:TextBox Runat="server" ID="txtFaceLayoutItem" Width="200px" />
							<div>
								<asp:RequiredFieldValidator Runat="server" ID="vldFaceLayoutItem" ControlToValidate="txtFaceLayoutItem" ErrorMessage="Default text is required" Display="static" />
							</div>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:DataGrid>
			<asp:Button Runat="server" ID="btnSaveLayoutItems" Text="Save" />
			<asp:Button Runat="server" ID="btnCancelLayoutItems" Text="Cancel" CausesValidation="False" />
		</asp:Panel>
		<asp:Panel Runat="server" ID="pnlBlankLayout" Visible="False">
		    <asp:Button ID="btnReturn" Runat="server" Text="Return" CausesValidation="False" />
		    <asp:DataGrid Runat="server" ID="grdBlankIcons" AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateColumn HeaderText="Icon">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" Width="250px" />
                        <ItemTemplate>
                            <asp:Image ID="imgBlankIcon" Runat="server" BorderStyle="Solid" BorderWidth="1" BorderColor="#d4d0c8" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <input ID="ctlBlankIconFile" Runat="server" type="file" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Label">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="160px" />
                        <ItemTemplate>
                            <asp:Label Runat="server" ID="lblBlankIconFaceName" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label Runat="server" ID="lblBlankIconFaceNameEdit" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Action">
                        <ItemTemplate>
                            <asp:ImageButton Runat="server" ID="btnBlankIconEdit"
                                ImageUrl="images/edit.gif" CommandName="Edit" CausesValidation="False"
                                ImageAlign="AbsMiddle" AlternateText="Edit" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton Runat="server" ID="btnBlankIconUpdate" 
                                ImageUrl="images/save.gif" CommandName="Update" CausesValidation="True"
                                ImageAlign="AbsMiddle" AlternateText="Save" />
                            <asp:ImageButton Runat="server" ID="btnBlankIconCancel"
                                ImageUrl="images/cancel.gif" CommandName="Cancel"
                                CausesValidation="False" ImageAlign="AbsMiddle" AlternateText="Cancel" />
                        </EditItemTemplate>
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="100px" />
                        <ItemStyle Wrap="False" />
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
		</asp:Panel>
     </form>
	
  </body>
</html>
