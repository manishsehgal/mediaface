<%@ Page language="c#" Codebehind="ManageCategory.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.ManageCategory" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
    <HEAD>
        <title>Category Management</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    </HEAD>
    <body MS_POSITIONING="GridLayout">
        <form id="Form1" method="post" runat="server">
            <h3 align="center">Category Management</h3>
            <asp:Button ID="btnNew" Runat="server" Text="New Category" CausesValidation="False"/>
            <br>
            <br>
            <asp:DataGrid Runat="server" ID="grdCategorys" AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateColumn HeaderText="Icon">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" Width="250px" />
                        <ItemTemplate>
                            <asp:Image ID="imgIcon" Runat="server" BorderStyle="Solid" BorderWidth="1" BorderColor="#d4d0c8" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <input ID="ctlIconFile" Runat="server" type="file" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Category Name">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="160px" />
                        <ItemTemplate>
                            <asp:Label ID="lblName" Runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtName" Runat="server" MaxLength="40" style="width:100%" />
                            <asp:RequiredFieldValidator
                                ID="vldNameRequired" Runat="server"
                                ControlToValidate="txtName"
                                Display="Dynamic"
                                ErrorMessage="Enter the Category name" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="Category Visibility">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="160px" />
                        <ItemTemplate>
                            <asp:CheckBox Runat="server" ID="chkVisible" Enabled="False"/>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox Runat="server" ID="chkVisible" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Category target page">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="160px" />
                        <ItemTemplate>
                            <asp:Literal ID="litTargetPage" Runat="server"/>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="cboTargetPage" Runat="server" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Action">
                        <ItemTemplate>
                            <asp:ImageButton Runat="server" ID="btnEdit"
                                ImageUrl="images/edit.gif" CommandName="Edit" CausesValidation="False"
                                ImageAlign="AbsMiddle" AlternateText="Edit" />
                            <asp:ImageButton Runat="server" ID="btnDelete" ImageUrl="images/remove.gif" CommandName="Delete"
                                CausesValidation="False" ImageAlign="AbsMiddle" AlternateText="Remove"/>
                            <asp:ImageButton
                                    Runat="server"
                                    ID="btnItemUp"
                                    ImageUrl="images/up.gif"
                                    CommandName="Up"
                                    CausesValidation="False"
                                    AlternateText="Up" />
                            <asp:ImageButton
                                    Runat="server"
                                    ID="btnItemDown"
                                    ImageUrl="images/down.gif"
                                    CommandName="Down"
                                    CausesValidation="False"
                                    AlternateText="Down" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton Runat="server" ID="btnUpdate" 
                                ImageUrl="images/save.gif" CommandName="Update" CausesValidation="True"
                                ImageAlign="AbsMiddle" AlternateText="Save" />
                            <asp:ImageButton Runat="server" ID="btnCancel"
                                ImageUrl="images/cancel.gif" CommandName="Cancel"
                                CausesValidation="False" ImageAlign="AbsMiddle" AlternateText="Cancel" />
                        </EditItemTemplate>
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="100px" />
                        <ItemStyle Wrap="False" />
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </form>
    </body>
</HTML>
