using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Xml;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpModules;

namespace Neato.Dol.WebAdmin.HttpHandlers {
    public class WatermarkHandler : IHttpHandler, IRequiresSessionState {
        #region Url
        private const string RawUrl = "Watermark.aspx";
        private const string LoadFontsKey = "LoadFonts";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri Url(bool loadFonts) {
            return UrlHelper.BuildUrl(RawUrl, LoadFontsKey, loadFonts.ToString());
        }
        #endregion

        public WatermarkHandler() {
        }

        public void ProcessRequest(HttpContext context) {
            switch (context.Request.RequestType) {
                case "GET":
                    GetMode(context);
                    break;
                case "POST":
                    PostMode(context);
                    break;
            }
        }

        public bool IsReusable {
            get { return true; }
        }

        private void GetMode(HttpContext context) {
            bool loadFonts = false;
            if (context.Request.QueryString[LoadFontsKey] != null)
                loadFonts = bool.Parse(context.Request.QueryString[LoadFontsKey]);

            string query = loadFonts ? "?LoadFonts=" + bool.TrueString : string.Empty;
            string uri = CheckLocationModule.AdminUrl(Path.Combine(Configuration.IntranetDesignerSiteUrl, WebDesigner.HttpHandlers.WatermarkHandler.PageName() + query)); 
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(uri);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "GET";

            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string data = reader.ReadToEnd();
            reader.Close();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(data);
            
            XmlWriter writer = new XmlTextWriter(context.Response.OutputStream, Encoding.UTF8);
            xmlDoc.WriteTo(writer);
            writer.Flush();
            
            response.Close();
        }

        private void PostMode(HttpContext context) {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(context.Request.InputStream);
            //byte[] data = new byte[context.Request.ContentLength];
            //context.Request.InputStream.Read(data, 0, data.Length);

            string uri = CheckLocationModule.AdminUrl(Path.Combine(Configuration.IntranetDesignerSiteUrl, WebDesigner.HttpHandlers.WatermarkHandler.PageName())); 
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(uri);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "POST";

            XmlWriter writer = new XmlTextWriter(request.GetRequestStream(), Encoding.UTF8);
            xmlDoc.WriteTo(writer);
            writer.Close();

            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
        }
    }
}