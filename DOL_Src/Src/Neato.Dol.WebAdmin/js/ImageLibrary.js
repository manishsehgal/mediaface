function SwitchDivContent(divId) {
    var ctlDiv = document.getElementById(divId);
    var state = ctlDiv.style.display;
    ctlDiv.style.display = (state == '') ? 'none': '';
}

function HighlightCurrentLink(linkId, ctlCurrentFolderId,
         txtFolderNameId, btnDeleteFolderId, ctlIsRootSelectedId) {
    var ctlCurrentFolder = document.getElementById(ctlCurrentFolderId);
    ctlCurrentFolder.value = linkId;
    
    var txtFolderName = document.getElementById(txtFolderNameId);
    var btnDeleteFolder = document.getElementById(btnDeleteFolderId);    

    for(i = 0; i < document.anchors.length; i++) {
        var oAnchor = document.anchors[i];
         
         if (oAnchor.id == linkId) {
            oAnchor.style.fontWeight = 'bold';
            oAnchor.style.color = 'red';
            txtFolderName.value = oAnchor.innerText;
            btnDeleteFolder.innerText = "Delete '" + oAnchor.innerText + "'";
         } else {
            oAnchor.style.fontWeight = 'normal';
            oAnchor.style.color = 'blue';
         }
    }
}

function OpenDivs(arrDivIdsForOpen) {
    for(var i = 0; i < arrDivIdsForOpen.length; i++) {
        var ctlDiv = document.getElementById(arrDivIdsForOpen[i]);
        if (ctlDiv != null) ctlDiv.style.display = '';
    }
}

function DoFolderLibPostback(ctlCurrentFolderLibId, cboCurrentFolderLibId) {
	var ctlCurrentFolderLibId = document.getElementById(ctlCurrentFolderLibId);
	
	ctlCurrentFolderLibId.value = cboCurrentFolderLibId;
	document.forms[0].submit();
}

function DoFolderTreePostback(ctlCurrentFolderId, currentFolderId, divId) {
    var ctlCurrentFolder = document.getElementById(ctlCurrentFolderId);
    
    if (ctlCurrentFolder.value != currentFolderId) {
        ctlCurrentFolder.value = currentFolderId;
        document.forms[0].submit();
    } else {
        SwitchDivContent(divId);
    }
}

function UnselectCurrentFolder(ctlCurrentFolderId) {
    var ctlCurrentFolder = document.getElementById(ctlCurrentFolderId);
    ctlCurrentFolder.value = '';
}

function ImagesTryDeleteFlagClear(ctlImagesTryDeleteId) {
    var ctlTry = document.getElementById(ctlImagesTryDeleteId);
    ctlTry.value = '';
}

function ConfirmDelSelectedImages(message, ctlImagesTryDeleteId) {
    var result = confirm(message);
    if (result) {
        var ctlImagesTryDelete = document.getElementById(ctlImagesTryDeleteId);
        ctlImagesTryDelete.value = 'true';
    }
    return result;
}

function IsRootSelectedFlagSet(ctlIsRootSelectedId) {
    var ctlIsRoot = document.getElementById(ctlIsRootSelectedId);
    ctlIsRoot.value = 'true';
}

function SelectRootNode(isSelect, ctlIsRootSelectedId, btnRootId) {
    var ctlIsRoot = document.getElementById(ctlIsRootSelectedId);
    var btnRoot = document.getElementById(btnRootId);

    if (isSelect == "true") {
        btnRoot.style.fontWeight = 'bold';
        btnRoot.style.color = 'red';
    } else {
        ctlIsRoot.value = '';
        btnRoot.style.fontWeight = 'normal';
        btnRoot.style.color = 'blue';
    }
}
