function PreviewPage() {
    var ctlBaseUrl = document.getElementById('ctlBaseUrl');
    var txtText = document.getElementById('txtText');
    var ctlPreviewContaner = document.getElementById('ctlPreviewContaner');
    ctlPreviewContaner.contentWindow.document.write(ctlBaseUrl.value);
	ctlPreviewContaner.contentWindow.document.write(txtText.value);
	ctlPreviewContaner.contentWindow.document.close();
}

function PreviewBanner() {
    var cboBanner = document.getElementById('cboBanner');
    var bannerId = cboBanner.value;
    var id = 'ctlBanner' + bannerId + '_frame';

    var ctlBaseUrl = document.getElementById('ctlBaseUrl');
    var txtText = document.getElementById('txtText');
    var ctlPreviewContaner = document.getElementById('ctlPreviewContaner');
    var banner = ctlPreviewContaner.contentWindow.document.getElementById(id);
    
    var ctlBannerHeight = document.getElementById('txtBannerHeight');
    var height = parseInt(ctlBannerHeight.value);
    
    if (banner != null) {
		if (!isNaN(height))
			banner.style.height = height + "px";
        banner.contentWindow.document.write(ctlBaseUrl.value);
	    banner.contentWindow.document.write(txtText.value);
	    banner.contentWindow.document.close();    
    }
    else {
        
        var label = ctlPreviewContaner.contentWindow.document.getElementById(GetLabelId(bannerId));
        if (label != null) {
            label.innerHTML = txtText.value;      
        }
    }
}

function PreviewClosePage() {
    var ctlBaseUrl = document.getElementById('ctlBaseUrl');
    var txtText = document.getElementById('txtText');
    var ctlPreviewContaner = document.getElementById('ctlPreviewContaner');
    ctlPreviewContaner.contentWindow.document.write(ctlBaseUrl.value);
	ctlPreviewContaner.contentWindow.document.write(txtText.value);
	ctlPreviewContaner.contentWindow.document.close();
}

function refresh() {
    var ctlPreviewContaner = document.getElementById('ctlPreviewContaner');
    ctlPreviewContaner.contentWindow.location.reload(true);
}

function GetLabelId(bannerId) {
    var id = "";
    if(bannerId == 3) {
        id = "lblUpcInfo";        
    }
    if(bannerId == 4) {
        id = "lblSerialInfo";
    }
    if(bannerId == 5) {
        id = "lblBuyNowInfo";        
    }
    if(bannerId == 8) {
        id = "lblUpcHeader";    
    }
    if(bannerId == 9) {
        id = "lblSerialHeader";
    }
    if(bannerId == 10) {
        id = "lblBuyNowHeader";
    }
    return id;    
}