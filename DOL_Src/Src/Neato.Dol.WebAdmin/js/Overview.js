var initialHtml = "";
var htmlMask = "&#1;";
var mask = "\1";

function Preview() {
    var txtText = document.getElementById('txtText');
    var ctlPreviewContaner = document.getElementById('ctlPreviewContaner');
    var PageContent = ctlPreviewContaner.contentWindow.document.getElementById('OverviewContent');
    
    if (initialHtml == "") {        
        PageContent.innerHTML = htmlMask;
        initialHtml = ctlPreviewContaner.contentWindow.document.body.innerHTML;
    }
        
    var tmp = initialHtml.replace(mask, txtText.value);
        
    ctlPreviewContaner.contentWindow.document.body.innerHTML = tmp;
    return false;
}

function PreviewPage() {
    var txtText = document.getElementById('txtText');
    var ctlPreviewContaner = document.getElementById('ctlPreviewContaner');
	ctlPreviewContaner.contentWindow.document.write(txtText.value);
	ctlPreviewContaner.contentWindow.document.close();
}
