using System;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin {
    public class ManagePaperBrand : Page {
        private const string LblNameId = "lblName";
        private const string ImgIconId = "imgIcon";
        private const string CtlIconFileId = "ctlIconFile";
        private const string TxtNameId = "txtName";
        private const string BtnDeleteId = "btnDelete";
        protected Button btnNew;
        protected DataGrid grdPaperBrands;


        private bool NewMode {
            get { return (bool)ViewState["NewModeKey"]; }
            set { ViewState["NewModeKey"] = value; }
        }

        #region Url
        private const string RawUrl = "ManagePaperBrand.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion


        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                NewMode = false;
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManagePaperBrand_DataBinding);
            grdPaperBrands.ItemDataBound += new DataGridItemEventHandler(grdPaperBrands_ItemDataBound);
            grdPaperBrands.EditCommand += new DataGridCommandEventHandler(grdPaperBrands_EditCommand);
            grdPaperBrands.DeleteCommand += new DataGridCommandEventHandler(grdPaperBrands_DeleteCommand);
            grdPaperBrands.UpdateCommand += new DataGridCommandEventHandler(grdPaperBrands_UpdateCommand);
            grdPaperBrands.CancelCommand += new DataGridCommandEventHandler(grdPaperBrands_CancelCommand);
            btnNew.Click += new EventHandler(btnNew_Click);
        }
        #endregion

        private void ManagePaperBrand_DataBinding(object sender, EventArgs e) {
            ArrayList paperBrands = new ArrayList(BCPaperBrand.GetPaperBrandList());
            if (NewMode) {
                paperBrands.Insert(0, BCPaperBrand.NewPaperBrand());
                grdPaperBrands.EditItemIndex = 0;
            } else if (grdPaperBrands.EditItemIndex >= 0) {
                int paperBrandId = (int)grdPaperBrands.DataKeys[grdPaperBrands.EditItemIndex];
                int index = paperBrands.IndexOf(new PaperBrandBase(paperBrandId));
                grdPaperBrands.EditItemIndex = index;
            }
            grdPaperBrands.DataSource = paperBrands;
            grdPaperBrands.DataKeyField = PaperBrand.IdField;
        }

        private void grdPaperBrands_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void grdPaperBrands_EditCommand(object source, DataGridCommandEventArgs e) {
            grdPaperBrands.EditItemIndex = e.Item.ItemIndex;
            NewMode = false;
            DataBind();
        }

        private void grdPaperBrands_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdPaperBrands.EditItemIndex = -1;
            int paperBrandId = (int)grdPaperBrands.DataKeys[e.Item.ItemIndex];
            PaperBrandBase paperBrand = new PaperBrandBase(paperBrandId);
            try {
                BCPaperBrand.Delete(paperBrand);
                Response.Redirect(Url().PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewMode = false;
            DataBind();
        }

        private void grdPaperBrands_UpdateCommand(object source, DataGridCommandEventArgs e) {
            Page.Validate();
            if (!IsValid) return;

            PaperBrand paperBrand = BCPaperBrand.NewPaperBrand();

            paperBrand.Id = (int)grdPaperBrands.DataKeys[e.Item.ItemIndex];
            TextBox txtName = (TextBox)e.Item.FindControl(TxtNameId);

            paperBrand.Name = txtName.Text.Trim();
            HtmlInputFile ctlIconFile = (HtmlInputFile)e.Item.FindControl(CtlIconFileId);

            Stream icon = ctlIconFile.PostedFile.InputStream;

            try {
                BCPaperBrand.Save(paperBrand, icon);
                Response.Redirect(Url().PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }

            grdPaperBrands.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdPaperBrands_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdPaperBrands.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void ItemDataBound(DataGridItem item) {
            PaperBrand paperBrand = (PaperBrand)item.DataItem;
            Label lblName = (Label)item.FindControl(LblNameId);
            lblName.Text = HttpUtility.HtmlEncode(paperBrand.Name);

            Image imgIcon = (Image)item.FindControl(ImgIconId);
            imgIcon.ImageUrl = IconHandler.ImageUrl(paperBrand).AbsoluteUri;
            
            ImageButton btnDelete = (ImageButton)item.FindControl(BtnDeleteId);
            string warning1 = "Do you want to remove this paper brand?"; 
            string warning2 = "All associated papers of this paper brand will be removed. Are you sure?"; 
            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", warning1, warning2);
        }

        private void EditItemDataBound(DataGridItem item) {
            PaperBrand paperBrand = (PaperBrand)item.DataItem;
            TextBox txtName = (TextBox)item.FindControl(TxtNameId);
            txtName.Text = paperBrand.Name;
            JavaScriptHelper.RegisterFocusScript(this, txtName.ClientID);
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewMode = true;
            DataBind();
        }
    }
}