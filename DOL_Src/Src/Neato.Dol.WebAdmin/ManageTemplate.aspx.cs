using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;

using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebAdmin.Helpers;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin
{
    public class ManageTemplate : System.Web.UI.Page{
	 protected DataGrid grdTemplate;
        protected DropDownList cboLabelFilter;
        protected DropDownList cboDeviceFilter;
        protected DropDownList cboDeviceTypeFilter;
        protected Button btnSearch;
        protected Button btnNew;

        private const int NumberOfColumns = 3;

        private const string ImgIconName = "imgIcon";
        private const string CtlIconFileName = "ctlIconFile";
        private const string LblNameName = "lblName";
        private const string LblFaceName = "lblFaceName";
        private const string CtlProjectFileName = "ctlProjectFile";
        private const string TxtFacesName = "txtFaces";

        //private const string EditTemplateItemsCommandName = "EditTemplateItems";

        private const string BtnItemDeleteName = "btnDelete";
        private const string BtnEditTemplateItems = "btnEditTemplateItems";
        private const string BtnItemEditName = "btnEdit";
        private const string BtnItemUpdateName = "btnUpdate";
        private const string BtnItemCancelName = "btnCancel";

        private ArrayList deviceTypes;
        private ArrayList allLabels;
        private ArrayList allDevices;
        private ArrayList Templates;

        private const string AllValue = "-1";
        private const string SelectValue = "-2";
        private const string NoDataValue = "-3";

        private const string LabelFilterKey = "LabelFilterKey";
        private const string DeviceFilterKey = "DeviceFilterKey";
        private const string DeviceTypeFilterKey = "DeviceTypeFilterKey";

        protected Panel panTemplate;
        //protected Panel panTemplateItem;
        //protected DataGrid grdTemplateItem;
        //protected Button btnSaveTemplateItems;
        //protected Button btnCancelTemplateItems;

        //private const string TxtTemplateItem = "txtTemplateItem";

        private PageFilter CurrentFilter {
            get { return (PageFilter)ViewState["CurrentFilter"]; }
            set { ViewState["CurrentFilter"] = value; }
        }

        private bool NewMode {
            get { return (bool)ViewState["NewModeKey"]; }
            set { ViewState["NewModeKey"] = value; }
        }

        private void Page_Load(object sender, System.EventArgs e) {
            if (!IsPostBack) {
                NewMode = false;
                InitFilters();
                DataBind();
            }
        }

        private void InitFilters() {
            CurrentFilter = new PageFilter();

            CurrentFilter[DeviceFilterKey] = Request.QueryString[DeviceFilterKey];
            if (CurrentFilter[DeviceFilterKey] == null) CurrentFilter[DeviceFilterKey] = SelectValue;

            CurrentFilter[LabelFilterKey] = Request.QueryString[LabelFilterKey];
            if (CurrentFilter[LabelFilterKey] == null) CurrentFilter[LabelFilterKey] = SelectValue;

            CurrentFilter[DeviceTypeFilterKey] = Request.QueryString[DeviceTypeFilterKey];
            if (CurrentFilter[DeviceTypeFilterKey] == null) CurrentFilter[DeviceTypeFilterKey] = SelectValue;
        }

        #region Url
        private const string RawUrl = "ManageTemplate.aspx";

        public static Uri Url(string deviceFilter, string labelFilter, string deviceTypeFilter) {
            return UrlHelper.BuildUrl(RawUrl,
                DeviceFilterKey, deviceFilter,
                LabelFilterKey, labelFilter,
                DeviceTypeFilterKey, deviceTypeFilter);
        }

        public static Uri Url() 
        {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion
        
        #region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent() {    
			this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManageTemplate_DataBinding);
            this.PreRender += new EventHandler(ManageTemplate_PreRender);
            cboLabelFilter.DataBinding += new EventHandler(cboLabelFilter_DataBinding);
            cboDeviceFilter.DataBinding += new EventHandler(cboDeviceFilter_DataBinding);
            cboDeviceTypeFilter.DataBinding += new EventHandler(cboDeviceTypeFilter_DataBinding);
            btnSearch.Click += new EventHandler(btnSearch_Click);
            btnNew.Click += new EventHandler(btnNew_Click);
            grdTemplate.ItemDataBound += new DataGridItemEventHandler(grdTemplate_ItemDataBound);
            grdTemplate.EditCommand += new DataGridCommandEventHandler(grdTemplate_EditCommand);
            grdTemplate.DeleteCommand += new DataGridCommandEventHandler(grdTemplate_DeleteCommand);
            grdTemplate.UpdateCommand += new DataGridCommandEventHandler(grdTemplate_UpdateCommand);
            grdTemplate.CancelCommand += new DataGridCommandEventHandler(grdTemplate_CancelCommand);
            grdTemplate.ItemCreated += new DataGridItemEventHandler(grdTemplate_ItemCreated);
        }
        #endregion

        private void ManageTemplate_DataBinding(object sender, EventArgs e) {
            deviceTypes = new ArrayList(BCCategory.GetCategoryList());
            allLabels = new ArrayList(BCFace.FaceEnum());
            allDevices = new ArrayList(BCDevice.GetDeviceList());
            allDevices.Sort(Device.GetFullModelComparer(CultureInfo.InvariantCulture));

            if (IsPostBack) {
                CurrentFilter[LabelFilterKey] = cboLabelFilter.SelectedValue;
                CurrentFilter[DeviceFilterKey] = cboDeviceFilter.SelectedValue;
                CurrentFilter[DeviceTypeFilterKey] = cboDeviceTypeFilter.SelectedValue;
            }

             if (!CurrentFilter.FiltersHaveSameValue(SelectValue)) {
                if (CurrentFilter.GetFiltersWithoutValue(SelectValue).Length > 0) {
                    CurrentFilter.ReplaceFilterValue(SelectValue, AllValue);
                }

                Face face = null;
                int faceId = int.Parse(CurrentFilter[LabelFilterKey], CultureInfo.InvariantCulture.NumberFormat);
                if (faceId > 0 || faceId == Convert.ToInt32(NoDataValue)) {
                    face = new Face();
                    face.Id = faceId;
                }

                DeviceBase device = null;
                int deviceId = int.Parse(CurrentFilter[DeviceFilterKey], CultureInfo.InvariantCulture.NumberFormat);
                if (deviceId > 0) {
                    device = new DeviceBase(deviceId);
                }
                DeviceType deviceType =
                    (CurrentFilter[DeviceTypeFilterKey] == AllValue) ?
                    DeviceType.Undefined :
                    (DeviceType)(Convert.ToInt32(CurrentFilter[DeviceTypeFilterKey]));

                Templates = new ArrayList(BCTemplate.EnumerateTemplates(deviceType, face, device));
            } 
            else {
                Templates = new ArrayList();
            }

            if (NewMode) {
                Templates.Insert(0, new Template(0));
                grdTemplate.EditItemIndex = 0;
            } 
            /*else if (grdTemplate.EditItemIndex >= 0) {
                int templateId = (int)grdTemplate.DataKeys[grdTemplate.EditItemIndex];
                Template template = new Template(templateId);
                int index = Templates.IndexOf(template);
                grdTemplate.EditItemIndex = index;
            }*/
            
            grdTemplate.DataSource = Templates;
            grdTemplate.DataKeyField = Template.IdField;
        }

        private void cboDeviceFilter_DataBinding(object sender, EventArgs e) {
            cboDeviceFilter.Items.Clear();
            foreach (Device device in allDevices) {
                ListItem item = new ListItem(device.FullModel, device.Id.ToString());
                cboDeviceFilter.Items.Add(item);
                if (item.Value == CurrentFilter[DeviceFilterKey]) item.Selected = true;
            }
            cboDeviceFilter.Items.Insert(0, new ListItem("All", AllValue));
            if (CurrentFilter[DeviceFilterKey] == SelectValue) {
                cboDeviceFilter.Items.Insert(0, new ListItem("Select", SelectValue));
            }
        }

        private void cboLabelFilter_DataBinding(object sender, EventArgs e) {
            cboLabelFilter.Items.Clear();
            foreach (Face face in allLabels) {
                ListItem item = new ListItem(face.GetName(""), face.Id.ToString());
                cboLabelFilter.Items.Add(item);
                if (item.Value == CurrentFilter[LabelFilterKey]) item.Selected = true;
            }
            cboLabelFilter.Items.Insert(0, new ListItem("All", AllValue));
            cboLabelFilter.Items.Insert(1, new ListItem("No labels", NoDataValue));
            if (CurrentFilter[LabelFilterKey] == SelectValue) {
                cboLabelFilter.Items.Insert(0, new ListItem("Select", SelectValue));
            }
            else if(CurrentFilter[LabelFilterKey] == NoDataValue) {
                cboLabelFilter.Items[1].Selected = true;
            }
        }

        private void cboDeviceTypeFilter_DataBinding(object sender, EventArgs e) {
            cboDeviceTypeFilter.Items.Clear();
            foreach (Category category in deviceTypes) {
                ListItem item = new ListItem(category.Name, category.Id.ToString());
                cboDeviceTypeFilter.Items.Add(item);
                if (item.Value == CurrentFilter[DeviceTypeFilterKey]) item.Selected = true;
            }
            cboDeviceTypeFilter.Items.Insert(0, new ListItem("All", AllValue));
            if (CurrentFilter[DeviceTypeFilterKey] == SelectValue) {
                cboDeviceTypeFilter.Items.Insert(0, new ListItem("Select", SelectValue));
            }
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewMode = true;
            DataBind();
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            grdTemplate.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void ManageTemplate_PreRender(object sender, EventArgs e) {
            grdTemplate.Visible = grdTemplate.Items.Count > 0;
        }

        private void grdTemplate_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void ItemDataBound(DataGridItem item) {
            Template template = (Template)item.DataItem;

            Image imgIcon = (Image)item.FindControl(ImgIconName);
            
            Label lblName = (Label)item.FindControl(LblNameName);
            Label lblFace = (Label)item.FindControl(LblFaceName);
//          TextBox txtFaces = (TextBox)item.FindControl(TxtFacesName);

            lblName.Text = HttpUtility.HtmlEncode(template.Name);
            Face face = BCFace.GetFace(new FaceBase(template.FaceId));
            
            lblFace.Text = HttpUtility.HtmlEncode(face.GetName(string.Empty));//template.Face.GetName(string.Empty));

            StringBuilder sb = new StringBuilder();
            /*foreach(TemplateItem templateItem in template.TemplateItems) {
                if(sb.Length != 0)
                    sb.Append(Environment.NewLine);
                sb.Append(HttpUtility.HtmlEncode(string.Format("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(templateItem.Text))));
            }*/
  //          txtFaces.Text = sb.ToString();

            string imageUrl = IconHandler.ImageUrl(template).AbsoluteUri;
            imgIcon.ImageUrl = imageUrl;

            Button btnDelete = (Button)item.FindControl(BtnItemDeleteName);
            string warning = "Do you want to remove this Template?";
            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", warning);
        }

        private void EditItemDataBound(DataGridItem item) {
            Template template = (Template)item.DataItem;

            HtmlInputFile ctlIconFile = (HtmlInputFile)item.FindControl(CtlIconFileName);
            JavaScriptHelper.RegisterFocusScript(this, ctlIconFile.ClientID);
        }

        private void grdTemplate_EditCommand(object source, DataGridCommandEventArgs e) {
            grdTemplate.EditItemIndex = e.Item.ItemIndex;
            NewMode = false;
            DataBind();
        }

        private void grdTemplate_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdTemplate.EditItemIndex = -1;
            int templateId = (int)grdTemplate.DataKeys[e.Item.ItemIndex];
            Template template = new Template(templateId);
            try {
                BCTemplate.Delete(template);
                Response.Redirect(Url(
                    CurrentFilter[DeviceFilterKey],
                    CurrentFilter[LabelFilterKey],
                    CurrentFilter[DeviceTypeFilterKey]).
                    PathAndQuery);
            } 
            catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewMode = false;
            DataBind();
        }

        private void grdTemplate_UpdateCommand(object source, DataGridCommandEventArgs e) {
            if(!IsValid)
                return;

            HtmlInputFile ctlIconFile = (HtmlInputFile)e.Item.FindControl(CtlIconFileName);
            Stream iconFile = ctlIconFile.PostedFile.InputStream;

            HtmlInputFile ctlProjectFile = (HtmlInputFile)e.Item.FindControl(CtlProjectFileName);
            Stream projectFile = ctlProjectFile.PostedFile.InputStream;

            int templateId = (int)grdTemplate.DataKeys[e.Item.ItemIndex];
            Template template = new Template(templateId);
            template.Name = Path.GetFileName(ctlProjectFile.PostedFile.FileName);
            
            try {
                BCTemplate.Save(template, projectFile, iconFile);
                Response.Redirect(Url(
                    CurrentFilter[DeviceFilterKey],
                    CurrentFilter[LabelFilterKey],
                    CurrentFilter[DeviceTypeFilterKey]).
                    PathAndQuery);
            } 
            catch (BaseBusinessException ex) 
            {
                MsgBox.Alert(this, ex.Message);
            }

            //grdTemplate.EditItemIndex = -1;
            //NewMode = false;
            DataBind();
        }

        private void grdTemplate_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdTemplate.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdTemplate_ItemCreated(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            if(item.ItemType != ListItemType.EditItem)
                return;

            GridTransformationHelper.EditItemCreationForm(item, grdTemplate.Columns, NumberOfColumns);
        }

        

        private void btnCancelTemplateItems_Click(object sender, EventArgs e) {
            panTemplate.Visible = true;
            //panTemplateItem.Visible = false;
            grdTemplate.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void btnSaveTemplateItems_Click(object sender, EventArgs e) {
            /*foreach(DataGridItem item in grdTemplateItem.Items) {
                if(item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem) {
                    int templateItemId = (int)grdTemplateItem.DataKeys[item.ItemIndex];
                    TextBox txtTemplateItem = (TextBox)item.FindControl(TxtTemplateItem);
                    TemplateItem templateItem = new TemplateItem();
                    templateItem.Id = templateItemId;
                    templateItem.Text = txtTemplateItem.Text;
                    try {
                        BCTemplate.Save(templateItem);
                    }
                    catch (BaseBusinessException ex) {
                        MsgBox.Alert(this, ex.Message);
                    }
                }
            }*/
            panTemplate.Visible = true;
            //panTemplateItem.Visible = false;
            grdTemplate.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdTemplateItem_DataBinding(object sender, EventArgs e) {
            //grdTemplateItem.DataSource = TemplateItems;
            //grdTemplateItem.DataKeyField = Template.IdField;
        }

        private void grdTemplateItem_ItemDataBound(object sender, DataGridItemEventArgs e) {
            if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
         //       TemplateItem templateItem = (TemplateItem)e.Item.DataItem;
           //     TextBox txtTemplateItem = (TextBox)e.Item.FindControl(TxtTemplateItem);
             //   txtTemplateItem.Text = templateItem.Text;
            }
        }
    }
}
