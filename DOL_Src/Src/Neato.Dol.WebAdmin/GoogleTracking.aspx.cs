using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.WebAdmin.Helpers;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin {
    public class GoogleTracking : Page {
        #region Constants
        private const string LblPageUrl = "lblPageUrl";
        private const string ChkEnableScript = "chkEnableScript";
        #endregion

        protected TextBox txtCode;
        protected Button btnSubmit;
        protected DataGrid grdTrackPages;

        #region Url
        private const string RawUrl = "GoogleTracking.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(GoogleTracking_DataBinding);
            this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
            this.grdTrackPages.DataBinding += new EventHandler(grdTrackPages_DataBinding);
            this.grdTrackPages.ItemDataBound += new DataGridItemEventHandler(grdTrackPages_ItemDataBound);
            this.grdTrackPages.EditCommand += new DataGridCommandEventHandler(grdTrackPages_EditCommand);
            this.grdTrackPages.UpdateCommand += new DataGridCommandEventHandler(grdTrackPages_UpdateCommand);
            this.grdTrackPages.CancelCommand += new DataGridCommandEventHandler(grdTrackPages_CancelCommand);
        }
        #endregion

        private void GoogleTracking_DataBinding(object sender, EventArgs e) {
            txtCode.Text = BCGoogleTracking.GetCode();
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            BCGoogleTracking.SetCode(txtCode.Text);
        }

        private void grdTrackPages_DataBinding(object sender, EventArgs e) {
            grdTrackPages.DataSource = PublicSitePageHelper.GetPublicSitePages();
        }

        private void grdTrackPages_ItemDataBound(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            switch (item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.EditItem:
                    ItemBound(e.Item);
                    break;
            }
        }

        private void ItemBound(DataGridItem item) {
            string pageUrl = (string)item.DataItem;

            Label lblPageUrl = (Label)item.FindControl(LblPageUrl);
            CheckBox chkEnableScript = (CheckBox)item.FindControl(ChkEnableScript);

            lblPageUrl.Text = pageUrl;
            chkEnableScript.Checked = BCGoogleTracking.GetGoogleTrackPage(pageUrl).EnableScript;

            bool edit = item.ItemType == ListItemType.EditItem;
            chkEnableScript.Enabled = edit;
        }

        private void grdTrackPages_EditCommand(object source, DataGridCommandEventArgs e) {
            grdTrackPages.EditItemIndex = e.Item.ItemIndex;
            grdTrackPages.DataBind();
        }

        private void grdTrackPages_UpdateCommand(object source, DataGridCommandEventArgs e) {
            Label lblPageUrl = (Label)e.Item.FindControl(LblPageUrl);
            CheckBox chkEnableScript = (CheckBox)e.Item.FindControl(ChkEnableScript);
            try {
                BCGoogleTracking.SetGoogleTrackPage(new GoogleTrackPage(lblPageUrl.Text, chkEnableScript.Checked));
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            Response.Redirect(Url().PathAndQuery);

        }

        private void grdTrackPages_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdTrackPages.EditItemIndex = -1;
            grdTrackPages.DataBind();
        }
    }
}