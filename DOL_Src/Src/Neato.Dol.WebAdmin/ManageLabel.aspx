<%@ Page language="c#" Codebehind="ManageLabel.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.ManageLabel" %>
<%@ Register TagPrefix="cpt" NameSpace="Neato.Dol.WebAdmin.Controls" Assembly="Neato.Dol.WebAdmin" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
    <head>
        <title>Manage Label</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <link type="text/css" rel="stylesheet" href="css/admin.css" />
    </head>
    <body MS_POSITIONING="GridLayout">
        <form id="Form1" method="post" runat="server">
            <h3 align="center">Label Management</h3>
            <table border="0" cellpadding="4" cellspacing="0">
                <tr>
                    <td>Device :</td>
                    <td>
                        <asp:DropDownList ID="cboDeviceFilter" Runat="server" style="width:250px" />
                    </td>
                </tr>
                <tr>
                    <td>Device Type :</td>
                    <td>
                        <asp:DropDownList ID="cboDeviceTypeFilter" Runat="server" style="width:250px" />
                    </td>
                </tr>
                <tr>
                    <td>Label Name :<br>(substring)</td>
                    <td valign="top">
                        <asp:TextBox ID="txtLabelName" Runat="server" style="width:250px"/>
                    </td>
                </tr>                
                <tr>
                    <td></td>
                    <td align="right">
                        <asp:Button runat="server" ID="btnSearch" Text="Search" />
                    </td>
                </tr>
            </table>
            <br>
            <asp:Button ID="btnNew" Runat="server" Text="New Label" CausesValidation="False" />
            <br>
            <br>
            <asp:DataGrid Runat="server" ID="grdLabels" AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateColumn HeaderText="Icon">
                        <HeaderStyle HorizontalAlign="Center" Width="110px" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:HyperLink ID="hypIconView" Runat="server">
                                <asp:Image ID="imgIcon" Runat="server" BorderStyle="Solid" BorderWidth="1" BorderColor="#d4d0c8"
                                    CssClass="imageIcon" />
                            </asp:HyperLink>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <input ID="ctlIconFile" Runat="server" type="file" size="20" NAME="ctlIconFile"/>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Name">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                        <ItemStyle Wrap="True" Width="100px" />
                        <ItemTemplate>
                            <asp:Label ID="lblName" Runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtName" Runat="server" MaxLength="64" style="width:237px" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="GUID">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                        <ItemStyle Wrap="True" Width="100px" />
                        <ItemTemplate>
                            <asp:Label ID="lblGuid" Runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Devices">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                        <ItemStyle Wrap="True" Width="290px" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtDevices" Runat="server"
                                TextMode="MultiLine"
                                BorderWidth="0" BorderStyle="None" 
                                ReadOnly="True"
                                Width="100%"
                                Height="100px"/>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <cpt:Selector id="ctlDevices" runat="server" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="XML" Visible="False">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                        <ItemStyle Width="100px"/>
                        <ItemTemplate>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <input ID="ctlLabelXmlFile" Runat="server" type="file" size="20" NAME="ctlLabelXmlFile"/>
                            <asp:RequiredFieldValidator
                            ID="vldLabelXmlFileRequired"
                            ControlToValidate="ctlLabelXmlFile"
                            Display="Static"
                            ErrorMessage="* required"
                            runat="server"/>
                            <br/>
                            <asp:CustomValidator ID="vldLabelXml" Runat="server"
                                OnServerValidate="vldLabelXml_ServerValidate" 
                                EnableClientScript="False"
                                Display="Dynamic" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Action">
                        <ItemTemplate>
                            <asp:Button Runat="server" ID="btnEdit" CommandName="Edit" CausesValidation="False"
                                Text="Edit" Tooltip="Edit label" />
                            <asp:Button Runat="server" ID="btnDelete" CommandName="Delete"
                                CausesValidation="False" Text="Remove" Tooltip="Remove label" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button Runat="server" ID="btnUpdate" Text="Save" CommandName="Update" CausesValidation="True"
                                Tooltip="Save label" />
                            <asp:Button Runat="server" ID="btnCancel" CommandName="Cancel" Text="Cancel"
                                CausesValidation="False" Tooltip="Cancel" />
                        </EditItemTemplate>
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                        <ItemStyle Wrap="False" Width="130px" />
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </form>
    </body>
</html>