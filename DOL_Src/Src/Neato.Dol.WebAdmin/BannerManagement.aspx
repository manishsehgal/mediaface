<%@ Page language="c#" Codebehind="BannerManagement.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.BannerManagement" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Banner Management</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <script type="text/javascript" language="javascript" src="js/Preview.js"></script>
  </head>
  <body MS_POSITIONING="GridLayout" onload="javascript:refresh()">
	<form id="Form1" method="post" runat="server">
		<input type="hidden" id="ctlBaseUrl" runat="server" />
		<h3 align="center">Banner Management</h3>
		<asp:Panel ID="panContent" Runat="server">
			Page&nbsp;:
			<asp:DropDownList ID="cboPage" Runat="server" AutoPostBack="True" />
			<img ID="imgPage" runat="server" align="top" />
			<br><br>Banner&nbsp;:
			<asp:DropDownList ID="cboBanner" Runat="server" AutoPostBack="True" />
			<br>Images&nbsp;:
			<input ID="ctlImageFile" Runat="server" type="file" size="20" />
			<asp:Button ID="btnAddImage" Runat="server" Text="Add" />
			<asp:Button ID="btnDelImage" Runat="server" Text="Delete" />
			<br>
			<asp:ListBox ID="lstBannerImages" Runat="server" Rows="5" style="width:300px" AutoPostBack="True"/>
			<img ID="imgBannerImage" runat="server" align="top" />
			<br>Html text:<br>
			<asp:TextBox ID="txtText" Runat="server" TextMode="MultiLine" Rows="10" MaxLength="2048" style="width:100%"></asp:TextBox>
			<br>
			<asp:CheckBox ID="btnHidden" Runat="server"  Text="Visible" AutoPostBack="True" />
			<div>
				<span>Height:&nbsp;</span>
				<asp:TextBox Runat="server" ID="txtBannerHeight" />
			</div>
			<asp:RegularExpressionValidator Runat="server" ID="vldBannerHeight" ControlToValidate="txtBannerHeight" ErrorMessage="Banner heigth value is not an integer" ValidationExpression="\d*" />
			<br><br>
			<button ID="btnPreview" onclick="javascript:PreviewBanner()">Preview</button>
			<asp:Button ID="btnSubmit" Runat="server" Text="Save Html text"/>
			<br><br>
			<iframe Runat="server" ID="ctlPreviewContaner" align="absmiddle" frameborder="yes" scrolling="auto" style="width:100%;height:615px;"/>
		</asp:Panel>
		<asp:Label ID="lblClosed" Runat="server" Visible="False" />
	</form>
  </body>
</html>
