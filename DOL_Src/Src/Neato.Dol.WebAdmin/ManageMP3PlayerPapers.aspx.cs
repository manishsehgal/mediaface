using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebAdmin.Controls;
using Neato.Dol.WebAdmin.Helpers;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin {
    public class ManageMP3PlayerPapers : Page {
        private const string LblNameId = "lblName";
        private const string ImgIconId = "imgIcon";
        private const string TxtNameId = "txtName";
        private const string BtnDeleteId = "btnDelete";
        private const string LblPaperBrandId = "lblPaperBrand";
        private const string CboPaperBrandId = "cboPaperBrand";
        private const string VldPaperBrandRequiredId = "vldPaperBrandRequired";
        private const string VldPaperXmlFileRequiredId = "vldPaperXmlFileRequired";
        private const string TxtFacesId = "txtFaces";
        private const string CtlPaperXmlFileId = "ctlPaperXmlFile";
        private const string CtlIconFileId = "ctlIconFile";
        private const string HypIconViewId = "hypIconView";
        private const string CtlDevicesId = "ctlDevices";
        private const string TxtDevicesId = "txtDevices";
        private const string LblPaperTypeId = "lblPaperType";
        private const string LblPaperStateId = "lblPaperState";
        private const string CboPaperStateId = "cboPaperState";
        private const string LblOrientationId = "lblOrientation";
        private const string CboOrientationId = "cboOrientation";
        private const string LblPaperAccessId = "lblPaperAccess";
        private const string CboPaperAccessId = "cboPaperAccess";

        private const int NumberOfColumns = 9;

        private const string AllValue = "-1";
        private const string SelectValue = "-2";

        protected Button btnNew;
        protected DataGrid grdPapers;
        protected DropDownList cboPaperBrandFilter;
        protected DropDownList cboPaperStateFilter;
        protected DropDownList cboCategoryFilter;
        protected TextBox txtPaperName;
        protected Button btnSearch;

        private ArrayList brands;
        private ArrayList allDevices;
        private ArrayList devices;
        private ArrayList papers;
        private ArrayList categories;

        private const string PaperBrandFilterKey = "PaperBrandFilterKey";
        private const string PaperStateFilterKey = "PaperStateFilterKey";
        private const string CategoryFilterKey = "CategoryFilterKey";

        private PageFilter CurrentFilter {
            get { return (PageFilter)ViewState["CurrentFilter"]; }
            set { ViewState["CurrentFilter"] = value; }
        }

        private bool NewMode {
            get { return (bool)ViewState["NewModeKey"]; }
            set { ViewState["NewModeKey"] = value; }
        }

        #region Url
        private const string RawUrl = "ManageMP3PlayerPapers.aspx";

        public static Uri Url(string brandFilter) {
            return UrlHelper.BuildUrl(RawUrl, PaperBrandFilterKey, brandFilter);
        }

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                InitFilters();
                NewMode = false;
                DataBind();
            }

        }

        private void InitFilters() {
            CurrentFilter = new PageFilter();

            CurrentFilter[PaperBrandFilterKey] = Request.QueryString[PaperBrandFilterKey];
            if (CurrentFilter[PaperBrandFilterKey] == null) CurrentFilter[PaperBrandFilterKey] = SelectValue;

            CurrentFilter[PaperStateFilterKey] = Request.QueryString[PaperStateFilterKey];
            if (CurrentFilter[PaperStateFilterKey] == null) CurrentFilter[PaperStateFilterKey] = SelectValue;

            CurrentFilter[CategoryFilterKey] = Request.QueryString[CategoryFilterKey];
            if (CurrentFilter[CategoryFilterKey] == null) CurrentFilter[CategoryFilterKey] = SelectValue;
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.cboPaperBrandFilter.DataBinding += new System.EventHandler(this.cboPaperBrandFilter_DataBinding);
            this.cboPaperStateFilter.DataBinding += new System.EventHandler(this.cboPaperStateFilter_DataBinding);
            this.cboCategoryFilter.DataBinding += new EventHandler(cboCategoryFilter_DataBinding);
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            this.grdPapers.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdPapers_ItemCreated);
            this.grdPapers.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdPapers_CancelCommand);
            this.grdPapers.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdPapers_EditCommand);
            this.grdPapers.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdPapers_UpdateCommand);
            this.grdPapers.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdPapers_DeleteCommand);
            this.grdPapers.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdPapers_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
            this.PreRender += new System.EventHandler(this.ManageMP3PlayerPapers_PreRender);
            this.DataBinding += new System.EventHandler(this.ManageMP3PlayerPapers_DataBinding);

        }
        #endregion

        private void ManageMP3PlayerPapers_DataBinding(object sender, EventArgs e) {
            brands = new ArrayList(BCPaperBrand.GetPaperBrandList());
            categories = new ArrayList(BCCategory.GetDeviceCategories());

            allDevices = new ArrayList(BCDevice.GetDeviceList(null, DeviceType.MP3Player, null));
            allDevices.Sort(Device.GetFullModelComparer(CultureInfo.InvariantCulture));

            if (txtPaperName.Text != string.Empty &&
                cboPaperBrandFilter.SelectedValue == SelectValue &&
                cboPaperStateFilter.SelectedValue == SelectValue) {
                cboPaperBrandFilter.SelectedValue = AllValue;
                cboCategoryFilter.SelectedValue = AllValue;
                cboPaperStateFilter.SelectedValue = "ait";
            }
            if (cboPaperStateFilter.SelectedValue == SelectValue)
                cboPaperStateFilter.SelectedValue = "ait";
            
            if (IsPostBack) {
                CurrentFilter[PaperBrandFilterKey] = cboPaperBrandFilter.SelectedValue;
                CurrentFilter[PaperStateFilterKey] = cboPaperStateFilter.SelectedValue;
                CurrentFilter[CategoryFilterKey] = cboCategoryFilter.SelectedValue;
            }
            if (!CurrentFilter.FiltersHaveSameValue(SelectValue)) {
                if (CurrentFilter.GetFiltersWithoutValue(SelectValue).Length > 0) {
                    CurrentFilter.ReplaceFilterValue(SelectValue, AllValue);
                }

                int brandId = int.Parse(CurrentFilter[PaperBrandFilterKey], CultureInfo.InvariantCulture.NumberFormat);
                PaperBrandBase paperBrand = null;
                if (brandId > 0) {
                    paperBrand = new PaperBrandBase(brandId);
                }

                string paperState = CurrentFilter[PaperStateFilterKey];

                int categoryId = int.Parse(CurrentFilter[CategoryFilterKey], CultureInfo.InvariantCulture.NumberFormat);
                Category category = null;
                int minDeviceTypeId = -1;

                if (categoryId == int.Parse(AllValue)) {
                    category = new Category((int)DeviceType.MP3Player);
                    minDeviceTypeId = (int)DeviceType.OtherLabels + 1;
                } else {
                    category = new Category(categoryId);
                }

                papers = new ArrayList(BCPaper.GetPapers(paperBrand, category, null, null, txtPaperName.Text, null, paperState, null, null, minDeviceTypeId, -1));
            } else {
                papers = new ArrayList();
            }

            if (NewMode) {
                papers.Insert(0, BCPaper.NewPaper());
                grdPapers.EditItemIndex = 0;
                devices = new ArrayList();
            } else if (grdPapers.EditItemIndex >= 0) {
                int paperId = (int)grdPapers.DataKeys[grdPapers.EditItemIndex];
                PaperBase paper = new PaperBase(paperId);
                int index = papers.IndexOf(paper);
                grdPapers.EditItemIndex = index;
                devices = new ArrayList(BCDevice.GetDeviceListByPaper(paper));
            }
            grdPapers.DataSource = papers;
            grdPapers.DataKeyField = Paper.IdField;
        }

        private void cboPaperBrandFilter_DataBinding(object sender, EventArgs e) {
            cboPaperBrandFilter.Items.Clear();
            foreach (PaperBrand brand in brands) {
                ListItem item = new ListItem(brand.Name, brand.Id.ToString());
                cboPaperBrandFilter.Items.Add(item);
                if (item.Value == CurrentFilter[CategoryFilterKey]) item.Selected = true;
            }
            cboPaperBrandFilter.Items.Insert(0, new ListItem("All", AllValue));
//            if (CurrentFilter[PaperBrandFilterKey] == SelectValue) {
//                cboPaperBrandFilter.Items.Insert(0, new ListItem("Select", SelectValue));
//            }
        }

        private void cboPaperStateFilter_DataBinding(object sender, EventArgs e) {
            cboPaperStateFilter.Items.Clear();
            cboPaperStateFilter.Items.Insert(0, new ListItem("All", "ait"));
            cboPaperStateFilter.Items.Insert(1, new ListItem("Active", "a"));
            cboPaperStateFilter.Items.Insert(2, new ListItem("Inactive", "i"));
            cboPaperStateFilter.Items.Insert(3, new ListItem("Test Mode", "t"));
            cboPaperStateFilter.Items.Insert(4, new ListItem("Inactive & Test Mode", "it"));
//            if (CurrentFilter[PaperStateFilterKey] == SelectValue) {
//                cboPaperStateFilter.Items.Insert(0, new ListItem("Select", SelectValue));
//            }

            foreach (ListItem item in cboPaperStateFilter.Items) {
                if (item.Value == CurrentFilter[PaperStateFilterKey]) {
                    item.Selected = true;
                    break;
                }
            }
        }

        private void cboCategoryFilter_DataBinding(object sender, EventArgs e) {
            cboCategoryFilter.Items.Clear();
            foreach (Category category in categories) {
                ListItem item = new ListItem(category.Name, category.Id.ToString());
                cboCategoryFilter.Items.Add(item);
                if (item.Value == CurrentFilter[PaperBrandFilterKey]) item.Selected = true;
            }
            cboCategoryFilter.Items.Insert(0, new ListItem("All (MP3 and newly added)", AllValue));
//            if (CurrentFilter[PaperBrandFilterKey] == SelectValue) {
//                cboCategoryFilter.Items.Insert(0, new ListItem("Select", SelectValue));
//            }
        }
        private void grdPapers_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void ItemDataBound(DataGridItem item) {
            Paper paper = (Paper)item.DataItem;
            Label lblName = (Label)item.FindControl(LblNameId);
            lblName.Text = HttpUtility.HtmlEncode(paper.Name);

            Label lblPaperType = (Label)item.FindControl(LblPaperTypeId);
            lblPaperType.Text = HttpUtility.HtmlEncode(BCPaperType.GetName(paper.PaperType));

            Image imgIcon = (Image)item.FindControl(ImgIconId);

            string imageUrl = IconHandler.ImageUrl(paper).AbsoluteUri;
            imgIcon.ImageUrl = imageUrl;

            HyperLink hypIconView = (HyperLink)item.FindControl(HypIconViewId);
            hypIconView.NavigateUrl = imageUrl;
            hypIconView.Target = HtmlHelper.Blank;

            Label lblPaperBrand = (Label)item.FindControl(LblPaperBrandId);
            lblPaperBrand.Text = HttpUtility.HtmlEncode(paper.Brand.Name);

            Label lblOrientation = (Label)item.FindControl(LblOrientationId);
            lblOrientation.Text = (paper.Orientation == "L") ? "Landscape" : "Portrait";

            Label lblPaperAccess = (Label)item.FindControl(LblPaperAccessId);
            lblPaperAccess.Text = paper.MinPaperAccess.ToString();

            Label lblPaperState = (Label)item.FindControl(LblPaperStateId);
            string stateText = "Active";
            if (paper.PaperState == "i") {
                stateText = "Inactive";
            } else if (paper.PaperState == "t") {
                stateText = "Test Mode";
            }
            lblPaperState.Text = stateText;

            TextBox txtFaces = (TextBox)item.FindControl(TxtFacesId);
            StringBuilder sb = new StringBuilder();
            foreach (string faceName in paper.GetFaceNames(WebDesigner.StorageManager.DefaultCulture)) {
                if (sb.Length == 0) {
                    sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(faceName));
                } else {
                    sb.Append(Environment.NewLine);
                    sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(faceName));
                }
            }
            txtFaces.Text = sb.ToString();

            TextBox txtDevices = (TextBox)item.FindControl(TxtDevicesId);
            sb = new StringBuilder();
            foreach (Device device in BCDevice.GetDeviceListByPaper(paper)) {
                if (sb.Length == 0) {
                    sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(device.FullModel));
                } else {
                    sb.Append(Environment.NewLine);
                    sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(device.FullModel));
                }
            }
            txtDevices.Text = sb.ToString();

            Button btnDelete = (Button)item.FindControl(BtnDeleteId);
            string warning = "Do you want to remove this paper?";
            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", warning);
        }

        private void EditItemDataBound(DataGridItem item) {
            Paper paper = (Paper)item.DataItem;
            TextBox txtName = (TextBox)item.FindControl(TxtNameId);
            txtName.Text = HttpUtility.HtmlEncode(paper.Name);
            JavaScriptHelper.RegisterFocusScript(this, txtName.ClientID);

            DropDownList cboPaperBrand = (DropDownList)item.FindControl(CboPaperBrandId);
            cboPaperBrand.DataSource = brands;
            cboPaperBrand.DataValueField = DeviceBrand.IdField;
            cboPaperBrand.DataTextField = DeviceBrand.NameField;
            cboPaperBrand.DataBind();

            int brandIndex = brands.IndexOf(paper.Brand);
            if (brandIndex < 0) {
                cboPaperBrand.Items.Insert(0, new ListItem("SELECT", SelectValue));
                cboPaperBrand.SelectedIndex = 0;
            } else {
                cboPaperBrand.SelectedIndex = brandIndex;
            }
            RequiredFieldValidator vldPaperBrandRequired = (RequiredFieldValidator)item.FindControl(VldPaperBrandRequiredId);
            vldPaperBrandRequired.InitialValue = SelectValue;

            RequiredFieldValidator vldPaperXmlFileRequired = (RequiredFieldValidator)item.FindControl(VldPaperXmlFileRequiredId);
            vldPaperXmlFileRequired.Enabled = NewMode;

            Selector ctlDevices = (Selector)item.FindControl(CtlDevicesId);
            ctlDevices.DataSource = allDevices;
            ctlDevices.DataValueField = Device.IdField;
            ctlDevices.DataTextField = Device.FullModelField;
            ctlDevices.DataBind();
            ctlDevices.SelectedIndices = IndicesHelper.GetIndices(allDevices, devices);

            DropDownList cboPaperState = (DropDownList)item.FindControl(CboPaperStateId);
            cboPaperState.Items.Insert(0, new ListItem("Active", "a"));
            cboPaperState.Items.Insert(1, new ListItem("Inactive", "i"));
            cboPaperState.Items.Insert(2, new ListItem("Test Mode", "t"));
            if (paper.PaperState == "a") {
                cboPaperState.SelectedIndex = 0;
            } else if (paper.PaperState == "i") {
                cboPaperState.SelectedIndex = 1;
            } else if (paper.PaperState == "t") {
                cboPaperState.SelectedIndex = 2;
            } else {
                cboPaperState.SelectedIndex = 0;
            }

            DropDownList cboOrientation = (DropDownList)item.FindControl(CboOrientationId);
            cboOrientation.Items.Insert(0, new ListItem("Portrait", "P"));
            cboOrientation.Items.Insert(1, new ListItem("Landscape", "L"));
            cboOrientation.SelectedIndex = (paper.Orientation == "L") ? 1 : 0;

            DropDownList cboPaperAccess = (DropDownList)item.FindControl(CboPaperAccessId);
            int index = 1;
            
            foreach (string paperAccesName in Enum.GetNames(typeof(PaperAccess))) {
                cboPaperAccess.Items.Add(new ListItem(paperAccesName, index++.ToString(CultureInfo.InvariantCulture.NumberFormat)));
            }
            cboPaperAccess.SelectedIndex = ((int)paper.MinPaperAccess - 1);
        }

        private void grdPapers_EditCommand(object source, DataGridCommandEventArgs e) {
            grdPapers.EditItemIndex = e.Item.ItemIndex;
            NewMode = false;
            DataBind();
        }

        private void grdPapers_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdPapers.EditItemIndex = -1;
            int paperId = (int)grdPapers.DataKeys[e.Item.ItemIndex];
            Paper paper = BCPaper.GetPaper(new PaperBase(paperId));
            try {
                BCPaper.DeleteWithFaces(paper);
                Response.Redirect(Url(CurrentFilter[PaperBrandFilterKey]).PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewMode = false;
            DataBind();
        }

        private void grdPapers_UpdateCommand(object source, DataGridCommandEventArgs e) {
            Selector ctlDevices = (Selector)e.Item.FindControl(CtlDevicesId);
            if (!IsValid) {
                allDevices = new ArrayList(BCDevice.GetDeviceList(null, DeviceType.MP3Player, null));
                allDevices.Sort(Device.GetFullModelComparer(CultureInfo.InvariantCulture));
                ctlDevices.DataSource = allDevices;
                ArrayList list = new ArrayList(ctlDevices.SelectedIndices);
                ctlDevices.DataBind();
                ctlDevices.SelectedIndices = list;
                return;
            }

            HtmlInputFile ctlIconFile = (HtmlInputFile)e.Item.FindControl(CtlIconFileId);
            Stream icon = ctlIconFile.PostedFile.InputStream;

            HtmlInputFile ctlPaperXmlFile = (HtmlInputFile)e.Item.FindControl(CtlPaperXmlFileId);
            Stream paperXml = ctlPaperXmlFile.PostedFile.InputStream;

            int paperId = (int)grdPapers.DataKeys[e.Item.ItemIndex];

            Paper paper = null;
            if (paperXml == null || paperXml.Length == 0) {
                if (NewMode) {
                    paper = BCPaper.NewPaper();
                } else {
                    paper = BCPaper.GetPaper(new PaperBase(paperId));
                }
            } else {
                paper = BCPaperImport.ParsePaper(paperXml, ImportSchema.MP3Player);
            }
            paper.Id = paperId;

            TextBox txtName = (TextBox)e.Item.FindControl(TxtNameId);
            paper.Name = txtName.Text.Trim();

            DropDownList cboPaperBrand = (DropDownList)e.Item.FindControl(CboPaperBrandId);
            int brandId = int.Parse(cboPaperBrand.SelectedValue, CultureInfo.InvariantCulture.NumberFormat);
            string brandName = cboPaperBrand.SelectedItem.Text;
            paper.Brand = new PaperBrand(brandId, brandName);

            DropDownList cboPaperState = (DropDownList)e.Item.FindControl(CboPaperStateId);
            string state = cboPaperState.SelectedValue;
            string oldState = paper.PaperState;
            string newState = state;
            paper.PaperState = state;

            DropDownList cboOrientation = (DropDownList)e.Item.FindControl(CboOrientationId);
            paper.Orientation = cboOrientation.SelectedValue;

            DropDownList cboPaperAccess = (DropDownList)e.Item.FindControl(CboPaperAccessId);
            paper.MinPaperAccess = (PaperAccess)(int.Parse(cboPaperAccess.SelectedValue));

            ArrayList deviceList = new ArrayList(ctlDevices.SelectedItemValues.Count);
            foreach (object itemValue in ctlDevices.SelectedItemValues) {
                if (itemValue is string) {
                    int deviceId = int.Parse((string)itemValue, CultureInfo.InvariantCulture.NumberFormat);
                    deviceList.Add(new DeviceBase(deviceId));
                }
            }
            DeviceBase[] newDevices = (DeviceBase[])deviceList.ToArray(typeof(DeviceBase));

            try {
                BCPaper.Save(paper, icon, newDevices);
                if (oldState != newState) {
                    BCMail.SendPaperStateChangeNotification(paper.Name, oldState, newState, "");
                }
                Response.Redirect(Url(CurrentFilter[PaperBrandFilterKey]).PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }

            grdPapers.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdPapers_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdPapers.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewMode = true;
            DataBind();
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            grdPapers.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void ManageMP3PlayerPapers_PreRender(object sender, EventArgs e) {
            grdPapers.Visible = grdPapers.Items.Count > 0;
        }

        protected void vldPaperXml_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = true;
            if (grdPapers.EditItemIndex >= 0) {
                DataGridItem item = grdPapers.Items[grdPapers.EditItemIndex];

                HtmlInputFile ctlPaperXmlFile = (HtmlInputFile)item.FindControl(CtlPaperXmlFileId);
                if (ctlPaperXmlFile.PostedFile.InputStream.Length != 0) {
                    string[] errors = BCPaperImport.Validate(ctlPaperXmlFile.PostedFile.InputStream, ImportSchema.MP3Player);
                    
                    args.IsValid = (errors.Length == 0);

                    StringBuilder sb = new StringBuilder();
                    foreach (string error in errors) {
                        if (sb.Length == 0) {
                            sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(error));
                        } else {
                            sb.Append(HtmlHelper.BrTag);
                            sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(error));
                        }
                    }
                    ((CustomValidator)source).ErrorMessage = sb.ToString();
                } else {
                    string fileName = ctlPaperXmlFile.Value;
                    if (fileName != string.Empty) {
                        args.IsValid = false;
                        string errorMessageFormat = "Can't open file '{0}'";
                        ((CustomValidator)source).ErrorMessage =
                            string.Format(errorMessageFormat, fileName);
                    }
                }
            }
        }

        private void grdPapers_ItemCreated(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            if (item.ItemType != ListItemType.EditItem)
                return;

            GridTransformationHelper.EditItemCreationForm(item, grdPapers.Columns, NumberOfColumns);
        }

    }
}
