using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebAdmin.Helpers;
using Neato.Dol.WebDesigner;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin {
    public class ManagePaper : Page {
        private const string LblNameId = "lblName";
        private const string ImgIconId = "imgIcon";
        private const string CtlIconFileId = "ctlIconFile";
        private const string TxtNameId = "txtName";
        private const string BtnDeleteId = "btnDelete";
        private const string LblPaperBrandId = "lblPaperBrand";
        private const string CboPaperBrandId = "cboPaperBrand";
        private const string VldPaperBrandRequiredId = "vldPaperBrandRequired";
        private const string VldPaperXmlFileRequiredId = "vldPaperXmlFileRequired";
        //private const string VldPaperXml = "vldPaperXml";
        private const string VldPaperNameRequiredId = "vldNameRequired";
        private const string TxtFacesId = "txtFaces";
        private const string CtlPaperXmlFileId = "ctlPaperXmlFile";
        private const string HypIconViewId = "hypIconView";
        private const string TxtDevicesId = "txtDevices";
        private const string LblPaperTypeId = "lblPaperType";
        private const string LblSkuId = "lblSku";
        private const string TxtSkuId = "txtSku";
        private const string LblPaperStateId = "lblPaperState";
        private const string CboPaperStateId = "cboPaperState";
        private const string LblMetric = "lblMetric";
        private const string CboMetric = "cboMetric";
        private const string LblOrientationId = "lblOrientation";
        private const string CboOrientationId = "cboOrientation";
        private const string LblPaperAccessId = "lblPaperAccess";
        private const string CboPaperAccessId = "cboPaperAccess";

        private const int NumberOfColumns = 10;

        private const string AllValue = "-1";
        //private const string SelectValue = "-2";
        private const string NoDataValue = "-3";

        protected Button btnNew;
        protected DataGrid grdPapers;
        protected DropDownList cboPaperBrandFilter;
        protected DropDownList cboLabelFilter;
        //protected DropDownList cboDeviceFilter;
        protected DropDownList cboDeviceTypeFilter;
        protected DropDownList cboSkuFilter;
        protected DropDownList cboPaperStateFilter;
        protected DropDownList cboPaperMetricFilter;
        protected TextBox txtPaperName;
        protected Button btnSearch;

        protected Panel panFaceLayout;
        protected Panel panFaceLayoutItem;

        private ArrayList brands;
        private ArrayList deviceTypes;
        private ArrayList allLabels;
        private ArrayList allDevices;
        private ArrayList papers;
        private ArrayList metrics;

        private const string PaperBrandFilterKey = "PaperBrandFilterKey";
        private const string LabelFilterKey = "LabelFilterKey";
        private const string DeviceFilterKey = "DeviceFilterKey";
        private const string CategoryFilterKey = "CategoryFilterKey";
        private const string SkuFilterKey = "SkuFilterKey";
        private const string SkuFilterTextKey = "SkuFilterTextKey";
        private const string PaperStateFilterKey = "PaperStateFilterKey";
        private const string PaperMetricFilterKey = "PaperMetricFilterKey";

        private PageFilter CurrentFilter {
            get { return (PageFilter) ViewState["CurrentFilter"]; }
            set { ViewState["CurrentFilter"] = value; }
        }

        private bool NewMode {
            get { return (bool) ViewState["NewModeKey"]; }
            set { ViewState["NewModeKey"] = value; }
        }

        #region Url

        private const string RawUrl = "ManagePaper.aspx";

        public static Uri Url(string brandFilter, string deviceFilter, string labelFilter, string deviceTypeFilter,
                              string skuFilter, string skuTextFilter, string paperStateFilter) {
            return UrlHelper.BuildUrl(RawUrl,
                                      PaperBrandFilterKey, brandFilter,
                                      DeviceFilterKey, deviceFilter,
                                      LabelFilterKey, labelFilter,
                                      CategoryFilterKey, deviceTypeFilter,
                                      SkuFilterKey, skuFilter,
                                      SkuFilterTextKey, skuTextFilter,
                                      PaperStateFilterKey, paperStateFilter);
        }

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                InitFilters();
                NewMode = false;
                DataBind();
            }

        }

        private void InitFilters() {
            CurrentFilter = new PageFilter();

            CurrentFilter[PaperBrandFilterKey] = Request.QueryString[PaperBrandFilterKey];
            if (CurrentFilter[PaperBrandFilterKey] == null) CurrentFilter[PaperBrandFilterKey] = AllValue;

            CurrentFilter[DeviceFilterKey] = Request.QueryString[DeviceFilterKey];
            if (CurrentFilter[DeviceFilterKey] == null) CurrentFilter[DeviceFilterKey] = AllValue;

            CurrentFilter[LabelFilterKey] = Request.QueryString[LabelFilterKey];
            if (CurrentFilter[LabelFilterKey] == null) CurrentFilter[LabelFilterKey] = AllValue;

            CurrentFilter[CategoryFilterKey] = Request.QueryString[CategoryFilterKey];
            if (CurrentFilter[CategoryFilterKey] == null) CurrentFilter[CategoryFilterKey] = AllValue;

            CurrentFilter[SkuFilterKey] = Request.QueryString[SkuFilterKey];
            CurrentFilter[SkuFilterTextKey] = Request.QueryString[SkuFilterTextKey];

            if (CurrentFilter[SkuFilterKey] == null) {
                CurrentFilter[SkuFilterKey] = AllValue;
                CurrentFilter[SkuFilterTextKey] = "All";
            }

            CurrentFilter[PaperStateFilterKey] = Request.QueryString[PaperStateFilterKey];
            if (CurrentFilter[PaperStateFilterKey] == null) CurrentFilter[PaperStateFilterKey] = "ait";

            CurrentFilter[PaperMetricFilterKey] = Request.QueryString[PaperMetricFilterKey];
            if (CurrentFilter[PaperMetricFilterKey] == null) CurrentFilter[PaperMetricFilterKey] = AllValue;
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManagePaper_DataBinding);
            grdPapers.ItemDataBound += new DataGridItemEventHandler(grdPapers_ItemDataBound);
            grdPapers.EditCommand += new DataGridCommandEventHandler(grdPapers_EditCommand);
            grdPapers.DeleteCommand += new DataGridCommandEventHandler(grdPapers_DeleteCommand);
            grdPapers.UpdateCommand += new DataGridCommandEventHandler(grdPapers_UpdateCommand);
            grdPapers.CancelCommand += new DataGridCommandEventHandler(grdPapers_CancelCommand);
            grdPapers.ItemCreated += new DataGridItemEventHandler(grdPapers_ItemCreated);
            btnNew.Click += new EventHandler(btnNew_Click);
            btnSearch.Click += new EventHandler(btnSearch_Click);
            this.PreRender += new EventHandler(ManagePaper_PreRender);
        }

        #endregion

        private void ManagePaper_DataBinding(object sender, EventArgs e) {
            brands = new ArrayList(BCPaperBrand.GetPaperBrandList());
            metrics = new ArrayList(BCPaperMetric.Enum());

            deviceTypes = new ArrayList(Enum.GetNames(typeof (DeviceType)));
            deviceTypes.Remove(DeviceType.Undefined.ToString());
            deviceTypes.Remove(DeviceType.MP3Player.ToString());

            allLabels = new ArrayList(BCFace.FaceEnum());

            allDevices = new ArrayList(BCDevice.GetDeviceList(DeviceType.MP3Player, true));
            allDevices.Sort(Device.GetFullModelComparer(CultureInfo.InvariantCulture));

            /*if (txtPaperName.Text != string.Empty &&
                cboPaperBrandFilter.SelectedValue == SelectValue &&
                //cboDeviceFilter.SelectedValue == SelectValue &&
                cboDeviceTypeFilter.SelectedValue == SelectValue &&
                cboPaperStateFilter.SelectedValue == SelectValue &&
                cboSkuFilter.SelectedValue == SelectValue)  
            {
                cboPaperBrandFilter.SelectedValue = AllValue;
                //cboDeviceFilter.SelectedValue = AllValue;
                cboDeviceTypeFilter.SelectedValue = AllValue;
                cboSkuFilter.SelectedValue = AllValue;
                
            }*/

            if (IsPostBack) {
                CurrentFilter[PaperBrandFilterKey] = cboPaperBrandFilter.SelectedValue;
                CurrentFilter[LabelFilterKey] = cboLabelFilter.SelectedValue;
                CurrentFilter[DeviceFilterKey] = AllValue; //cboDeviceFilter.SelectedValue;
                CurrentFilter[CategoryFilterKey] = cboDeviceTypeFilter.SelectedValue;
                CurrentFilter[PaperMetricFilterKey] = cboPaperMetricFilter.SelectedValue;
                {
                    //CurrentFilter[SkuFilterKey] = cboSkuFilter.SelectedValue;
                    //CurrentFilter[SkuFilterTextKey] = cboSkuFilter.SelectedItem.Text;
                    string[] param = cboSkuFilter.SelectedValue.Split(';');
                    CurrentFilter[SkuFilterKey] = param[0];
                    if (param.Length > 1)
                        CurrentFilter[SkuFilterTextKey] = param[1];
                    else
                        CurrentFilter[SkuFilterTextKey] = cboSkuFilter.SelectedItem.Text;
                }
                CurrentFilter[PaperStateFilterKey] = cboPaperStateFilter.SelectedValue;
            }

            cboPaperMetricFilter_DataBind();
            cboPaperBrandFilter_DataBind();
            cboLabelFilter_DataBind();
//            cboDeviceFilter_DataBind();
            cboDeviceTypeFilter_DataBind();
            cboScuFilter_DataBind();
            cboPaperStateFilter_DataBind();


            int brandId = int.Parse(CurrentFilter[PaperBrandFilterKey], CultureInfo.InvariantCulture.NumberFormat);
            PaperBrandBase paperBrand = null;
            if (brandId > 0) {
                paperBrand = new PaperBrandBase(brandId);
            }

            FaceBase face = null;
            int faceId = int.Parse(CurrentFilter[LabelFilterKey], CultureInfo.InvariantCulture.NumberFormat);
            if (faceId > 0 || faceId == Convert.ToInt32(NoDataValue)) {
                face = new FaceBase(faceId);
            }

            DeviceBase device = null;
            int deviceId = int.Parse(CurrentFilter[DeviceFilterKey], CultureInfo.InvariantCulture.NumberFormat);
            if (deviceId > 0) {
                device = new DeviceBase(deviceId);
            }

            PaperMetric paperMetric = null;
            int paperMetricId = int.Parse(CurrentFilter[PaperMetricFilterKey], CultureInfo.InvariantCulture.NumberFormat);
            if (paperMetricId > 0) {
                paperMetric = new PaperMetric(paperMetricId);
            }

            string categoryString = CurrentFilter[CategoryFilterKey];
            Category category = null;
            if (categoryString != AllValue) {
                DeviceType dt = (DeviceType) Enum.Parse(typeof (DeviceType), categoryString);
                category = new Category((int) dt);
            }

            ArrayList paperbases = new ArrayList();
            if (CurrentFilter[SkuFilterKey] != AllValue) {
                foreach (string st in CurrentFilter[SkuFilterKey].Split('|')) {
                    paperbases.Add(new PaperBase(int.Parse(st)));
                }
            }
            //PaperBase paperBase = CurrentFilter[SkuFilterKey] == AllValue ? null : new PaperBase(int.Parse(CurrentFilter[SkuFilterKey]));

            string paperState = CurrentFilter[PaperStateFilterKey];
            papers = new ArrayList();
            if (paperbases.Count < 1)
                papers.AddRange(BCPaper.GetPapers(paperBrand, category, face, device, txtPaperName.Text, null, paperState, new Category(3), paperMetric, -1, (int)DeviceType.OtherLabels));
            else {
                foreach (PaperBase pBase in paperbases) {
                    papers.AddRange(BCPaper.GetPapers(paperBrand, category, face, device, txtPaperName.Text, pBase, paperState, new Category(3), paperMetric, -1, (int)DeviceType.OtherLabels));
                }
            }

            if (NewMode) {
                papers.Insert(0, BCPaper.NewPaper());
                grdPapers.EditItemIndex = 0;
            } else if (grdPapers.EditItemIndex >= 0) {
                int paperId = (int) grdPapers.DataKeys[grdPapers.EditItemIndex];
                PaperBase paper = new PaperBase(paperId);
                int index = papers.IndexOf(paper);
                grdPapers.EditItemIndex = index;
            }
            grdPapers.DataSource = papers;
            grdPapers.DataKeyField = Paper.IdField;
        }

        private void cboPaperMetricFilter_DataBind() {
            cboPaperMetricFilter.Items.Clear();
            foreach (PaperMetric paperMetric in metrics) {
                ListItem item = new ListItem(paperMetric.Name, paperMetric.Id.ToString());
                cboPaperMetricFilter.Items.Add(item);
            }
            cboPaperMetricFilter.Items.Insert(0, new ListItem("All", AllValue));
            cboPaperMetricFilter.SelectedValue = CurrentFilter[PaperMetricFilterKey];
        }

        private void cboPaperBrandFilter_DataBind() {
            cboPaperBrandFilter.Items.Clear();
            foreach (PaperBrand brand in brands) {
                ListItem item = new ListItem(brand.Name, brand.Id.ToString());
                cboPaperBrandFilter.Items.Add(item);
            }
            cboPaperBrandFilter.Items.Insert(0, new ListItem("All", AllValue));
            /*if (CurrentFilter[PaperBrandFilterKey] == SelectValue) 
            {
                cboPaperBrandFilter.Items.Insert(0, new ListItem("Select", SelectValue));
                return;
            }*/
            foreach (ListItem item in cboPaperBrandFilter.Items) {
                if (item.Value == CurrentFilter[PaperBrandFilterKey]) {
                    item.Selected = true;
                    break;
                }
            }
        }

/*
        private void cboDeviceFilter_DataBind() 
        {
            cboDeviceFilter.Items.Clear();
            foreach (Device device in allDevices) 
            {
                ListItem item = new ListItem(device.FullModel, device.Id.ToString());
                cboDeviceFilter.Items.Add(item);
                if (item.Value == CurrentFilter[DeviceFilterKey]) item.Selected = true;
            }
            cboDeviceFilter.Items.Insert(0, new ListItem("All", AllValue));
            if (CurrentFilter[DeviceFilterKey] == SelectValue) 
            {
                cboDeviceFilter.Items.Insert(0, new ListItem("Select", SelectValue));
                return;
            }
            foreach (ListItem item in cboDeviceFilter.Items) 
            {
                if (item.Value == CurrentFilter[DeviceFilterKey]) 
                {
                    item.Selected = true;
                    break;
                }
            }
        }
/*/

        private void cboLabelFilter_DataBind() {
            cboLabelFilter.Items.Clear();
            foreach (Face face in allLabels) {
                ListItem item = new ListItem(face.GetName(""), face.Id.ToString());
                cboLabelFilter.Items.Add(item);
            }
            cboLabelFilter.Items.Insert(0, new ListItem("All", AllValue));
            cboLabelFilter.Items.Insert(1, new ListItem("No labels", NoDataValue));
            /*if (CurrentFilter[LabelFilterKey] == SelectValue) 
            {
                cboLabelFilter.Items.Insert(0, new ListItem("Select", SelectValue));
                return;
            }*/
            foreach (ListItem item in cboLabelFilter.Items) {
                if (item.Value == CurrentFilter[LabelFilterKey]) {
                    item.Selected = true;
                    break;
                }
            }
        }

        private void cboDeviceTypeFilter_DataBind() {
            cboDeviceTypeFilter.Items.Clear();
            foreach (string deviceType in deviceTypes) {
                ListItem item = new ListItem(deviceType);
                cboDeviceTypeFilter.Items.Add(item);
            }
            cboDeviceTypeFilter.Items.Insert(0, new ListItem("All", AllValue));
            /*if (CurrentFilter[CategoryFilterKey] == SelectValue) 
            {
                cboDeviceTypeFilter.Items.Insert(0, new ListItem("Select", SelectValue));
                return;
            }*/
            foreach (ListItem item in cboDeviceTypeFilter.Items) {
                if (item.Value == CurrentFilter[CategoryFilterKey]) {
                    item.Selected = true;
                    break;
                }
            }
        }

        private void cboScuFilter_DataBind() {
            cboSkuFilter.Items.Clear();
            foreach (Paper paper in BCPaper.GetPaperSkuList(null)) {
                foreach (string sku in paper.Sku.Split(',')) {
                    //              if(paper.PaperState.IndexOf("a") == -1 && BCSpecialUser.GetSpecialUsers(Thread.CurrentPrincipal.Identity.Name,1).Length <1 )
                    //                    break;
                    if (sku.Trim().Length < 1)
                        continue;
                    ListItem item = new ListItem(sku, paper.Id.ToString() + ';' + sku);
                    cboSkuFilter.Items.Add(item);
                }
                //                ListItem item = new ListItem(paper.Sku, paper.Id.ToString());
                //              cboSkuFilter.Items.Add(item);
            }
            SortDropDown(cboSkuFilter);
            cboSkuFilter.Items.Insert(0, new ListItem("All", AllValue));
            /*if (CurrentFilter[SkuFilterKey] == SelectValue) 
            {
                cboSkuFilter.Items.Insert(0, new ListItem("Select", SelectValue));
                return;
            }*/

            foreach (ListItem item in cboSkuFilter.Items) {
                /*                if (item.Value == CurrentFilter[SkuFilterKey]) {
                                    item.Selected = true;
                                    break;
                                }*/
                if (item.Text == CurrentFilter[SkuFilterTextKey] && item.Value.Split(';')[0] == CurrentFilter[SkuFilterKey]) {
                    item.Selected = true;
                    break;
                }
            }
        }

        private void cboPaperStateFilter_DataBind() {
            cboPaperStateFilter.Items.Clear();
            cboPaperStateFilter.Items.Insert(0, new ListItem("All", "ait"));
            cboPaperStateFilter.Items.Insert(1, new ListItem("Active", "a"));
            cboPaperStateFilter.Items.Insert(2, new ListItem("Inactive", "i"));
            cboPaperStateFilter.Items.Insert(3, new ListItem("Test Mode", "t"));
            cboPaperStateFilter.Items.Insert(4, new ListItem("Inactive & Test Mode", "it"));

            /*if (CurrentFilter[PaperStateFilterKey] == SelectValue) 
            {
                cboPaperStateFilter.Items.Insert(0, new ListItem("Select", SelectValue));
                return;
            }*/
            foreach (ListItem item in cboPaperStateFilter.Items) {
                if (item.Value == CurrentFilter[PaperStateFilterKey]) {
                    item.Selected = true;
                    break;
                }
            }
        }

        private void grdPapers_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void ItemDataBound(DataGridItem item) {
            Paper paper = (Paper) item.DataItem;
            Label lblName = (Label) item.FindControl(LblNameId);
            lblName.Text = HttpUtility.HtmlEncode(paper.Name);

            Label lblPaperType = (Label) item.FindControl(LblPaperTypeId);
            lblPaperType.Text = HttpUtility.HtmlEncode(BCPaperType.GetName(paper.PaperType));

            Image imgIcon = (Image) item.FindControl(ImgIconId);

            string imageUrl = IconHandler.ImageUrl(paper).AbsoluteUri;
            imgIcon.ImageUrl = imageUrl;

            HyperLink hypIconView = (HyperLink) item.FindControl(HypIconViewId);
            hypIconView.NavigateUrl = imageUrl;
            hypIconView.Target = HtmlHelper.Blank;

            Label lblPaperBrand = (Label) item.FindControl(LblPaperBrandId);
            lblPaperBrand.Text = HttpUtility.HtmlDecode(paper.Brand.Name);

            Label lblScu = (Label) item.FindControl(LblSkuId);
            lblScu.Text = HttpUtility.HtmlDecode(paper.Sku);

            Label lblPaperState = (Label) item.FindControl(LblPaperStateId);
            string stateText = "Active";
            if (paper.PaperState == "i") {
                stateText = "Inactive";
            } else if (paper.PaperState == "t") {
                stateText = "Test Mode";
            }
            lblPaperState.Text = stateText;

            Label lblOrientation = (Label) item.FindControl(LblOrientationId);
            lblOrientation.Text = (paper.Orientation == "L") ? "Landscape" : "Portrait";

            Label lblPaperAccess = (Label)item.FindControl(LblPaperAccessId);
            lblPaperAccess.Text = paper.MinPaperAccess.ToString();

            Label lblMetric = (Label) item.FindControl(LblMetric);
            lblMetric.Text = string.Empty;
            foreach (PaperMetric paperMetric in BCPaperMetric.Enum()) {
                if (paperMetric.Id == paper.MetricId)
                    lblMetric.Text = paperMetric.Name;
            }

            TextBox txtFaces = (TextBox) item.FindControl(TxtFacesId);
            StringBuilder sb = new StringBuilder();
            foreach (string faceName in paper.GetFaceNames(StorageManager.DefaultCulture)) {
                if (sb.Length == 0) {
                    sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlDecode(faceName));
                } else {
                    sb.Append(Environment.NewLine);
                    sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlDecode(faceName));
                }
            }
            txtFaces.Text = sb.ToString();

            TextBox txtDevices = (TextBox) item.FindControl(TxtDevicesId);
            sb = new StringBuilder();
            foreach (Device device in BCDevice.GetDeviceListByPaper(paper)) {
                if (sb.Length == 0) {
                    sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlDecode(device.FullModel));
                } else {
                    sb.Append(Environment.NewLine);
                    sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlDecode(device.FullModel));
                }
            }
            txtDevices.Text = sb.ToString();

            Button btnDelete = (Button) item.FindControl(BtnDeleteId);
            string warning = "Do you want to remove this paper?";
            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", warning);
        }

        private void EditItemDataBound(DataGridItem item) {
            Paper paper = (Paper) item.DataItem;
            TextBox txtName = (TextBox) item.FindControl(TxtNameId);
            txtName.Text = HttpUtility.HtmlEncode(paper.Name);
            JavaScriptHelper.RegisterFocusScript(this, txtName.ClientID);

            DropDownList cboPaperBrand = (DropDownList) item.FindControl(CboPaperBrandId);
            cboPaperBrand.DataSource = brands;
            cboPaperBrand.DataValueField = DeviceBrand.IdField;
            cboPaperBrand.DataTextField = DeviceBrand.NameField;
            cboPaperBrand.DataBind();

            int brandIndex = brands.IndexOf(paper.Brand);
/*            if (brandIndex < 0) 
            {
                //cboPaperBrand.Items.Insert(0, new ListItem("SELECT", SelectValue));
                cboPaperBrand.SelectedIndex = 0;
            } 
            else 
            {*/
            cboPaperBrand.SelectedIndex = brandIndex;
            //}
            DropDownList cboPaperMetrics = (DropDownList) item.FindControl(CboMetric);
            ArrayList m = (ArrayList) metrics.Clone();
            m.Insert(0, new PaperMetric(0, string.Empty));
            cboPaperMetrics.DataSource = m;
            cboPaperMetrics.DataValueField = PaperMetric.IdField;
            cboPaperMetrics.DataTextField = PaperMetric.NameField;
            cboPaperMetrics.DataBind();
            cboPaperMetrics.SelectedValue = paper.MetricId.ToString();

            RequiredFieldValidator vldPaperBrandRequired = (RequiredFieldValidator) item.FindControl(VldPaperBrandRequiredId);
            vldPaperBrandRequired.InitialValue = AllValue;

            RequiredFieldValidator vldPaperXmlFileRequired = (RequiredFieldValidator) item.FindControl(VldPaperXmlFileRequiredId);
            vldPaperXmlFileRequired.Enabled = NewMode;

            TextBox txtScu = (TextBox) item.FindControl(TxtSkuId);
            txtScu.Text = HttpUtility.HtmlDecode(paper.Sku);

            DropDownList cboPaperState = (DropDownList) item.FindControl(CboPaperStateId);
            cboPaperState.Items.Insert(0, new ListItem("Active", "a"));
            cboPaperState.Items.Insert(1, new ListItem("Inactive", "i"));
            cboPaperState.Items.Insert(2, new ListItem("Test Mode", "t"));
            if (paper.PaperState == "a") {
                cboPaperState.SelectedIndex = 0;
            } else if (paper.PaperState == "i") {
                cboPaperState.SelectedIndex = 1;
            } else if (paper.PaperState == "t") {
                cboPaperState.SelectedIndex = 2;
            } else {
                cboPaperState.SelectedIndex = 0;
            }

            DropDownList cboOrientation = (DropDownList) item.FindControl(CboOrientationId);
            cboOrientation.Items.Insert(0, new ListItem("Portrait", "P"));
            cboOrientation.Items.Insert(1, new ListItem("Landscape", "L"));
            cboOrientation.SelectedIndex = (paper.Orientation == "L") ? 1 : 0;

            DropDownList cboPaperAccess = (DropDownList)item.FindControl(CboPaperAccessId);
            int index = 1;
            
            foreach (string paperAccesName in Enum.GetNames(typeof(PaperAccess))) {
                cboPaperAccess.Items.Add(new ListItem(paperAccesName, index++.ToString(CultureInfo.InvariantCulture.NumberFormat)));
            }
            cboPaperAccess.SelectedIndex = ((int)paper.MinPaperAccess - 1);
        }

        private void grdPapers_EditCommand(object source, DataGridCommandEventArgs e) {
            grdPapers.EditItemIndex = e.Item.ItemIndex;
            NewMode = false;
            DataBind();
        }

        private void grdPapers_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdPapers.EditItemIndex = -1;
            int paperId = (int) grdPapers.DataKeys[e.Item.ItemIndex];
            PaperBase paper = new PaperBase(paperId);
            try {
                BCPaper.Delete(paper);
                Response.Redirect(Url(
                    CurrentFilter[PaperBrandFilterKey],
                    CurrentFilter[DeviceFilterKey],
                    CurrentFilter[LabelFilterKey],
                    CurrentFilter[CategoryFilterKey],
                    CurrentFilter[SkuFilterKey],
                    CurrentFilter[SkuFilterTextKey],
                    CurrentFilter[PaperStateFilterKey]
                    ).
                    PathAndQuery);
            }
            catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewMode = false;
            DataBind();
        }

        private void grdPapers_UpdateCommand(object source, DataGridCommandEventArgs e) {
            RequiredFieldValidator vldPaperBrandRequired = (RequiredFieldValidator) e.Item.FindControl(VldPaperBrandRequiredId);
            //vldPaperBrandRequired.Validate();
            RequiredFieldValidator vldPaperXmlFileRequired = (RequiredFieldValidator) e.Item.FindControl(VldPaperXmlFileRequiredId);
            //vldPaperXmlFileRequired.Validate();
            CustomValidator vldPaperXml = (CustomValidator) e.Item.FindControl("vldPaperXml");
            //vldPaperXml.Validate();

            if (!vldPaperBrandRequired.IsValid || !vldPaperXmlFileRequired.IsValid || !vldPaperXml.IsValid)
                return;


            HtmlInputFile ctlIconFile = (HtmlInputFile) e.Item.FindControl(CtlIconFileId);
            Stream icon = ctlIconFile.PostedFile.InputStream;

            HtmlInputFile ctlPaperXmlFile = (HtmlInputFile) e.Item.FindControl(CtlPaperXmlFileId);
            Stream paperXml = ctlPaperXmlFile.PostedFile.InputStream;

            int paperId = (int) grdPapers.DataKeys[e.Item.ItemIndex];

            Paper paper = null;
            if (paperXml == null || paperXml.Length == 0) {
                if (NewMode) {
                    paper = BCPaper.NewPaper();
                } else {
                    paper = BCPaper.GetPaper(new PaperBase(paperId));
                }
            } else {
                paper = BCPaperImport.ParsePaper(paperXml, ImportSchema.Paper);
            }
            paper.Id = paperId;

            TextBox txtName = (TextBox) e.Item.FindControl(TxtNameId);
            if (txtName.Text != string.Empty) {
                paper.Name = txtName.Text.Trim();
            } else {
                txtName.Text = paper.Name;
            }

            RequiredFieldValidator vldPaperNameRequired = (RequiredFieldValidator) e.Item.FindControl(VldPaperNameRequiredId);
            vldPaperNameRequired.Validate();

            if (!vldPaperNameRequired.IsValid)
                return;

            DropDownList cboPaperBrand = (DropDownList) e.Item.FindControl(CboPaperBrandId);
            int brandId = int.Parse(cboPaperBrand.SelectedValue, CultureInfo.InvariantCulture.NumberFormat);
            string brandName = cboPaperBrand.SelectedItem.Text;
            paper.Brand = new PaperBrand(brandId, brandName);

            TextBox txtScu = (TextBox) e.Item.FindControl(TxtSkuId);
            paper.Sku = txtScu.Text.Trim();

            DropDownList cboPaperState = (DropDownList) e.Item.FindControl(CboPaperStateId);
            string state = cboPaperState.SelectedValue;
            string oldState = paper.PaperState;
            string newState = state;
            paper.PaperState = state;

            DropDownList cboOrientation = (DropDownList) e.Item.FindControl(CboOrientationId);
            paper.Orientation = cboOrientation.SelectedValue;

            DropDownList cboPaperAccess = (DropDownList)e.Item.FindControl(CboPaperAccessId);
            paper.MinPaperAccess = (PaperAccess)(int.Parse(cboPaperAccess.SelectedValue));

            DropDownList cboPaperMetric = (DropDownList) e.Item.FindControl(CboMetric);
            int metricId = int.Parse(cboPaperMetric.SelectedValue, CultureInfo.InvariantCulture.NumberFormat);
            paper.MetricId = metricId;

            try {
                BCPaper.Save(paper, icon);
                if (NewMode) {
                    paper.Order = paper.Id;
                    BCPaper.Save(paper);
                }
                if (oldState != newState) {
                    BCMail.SendPaperStateChangeNotification(paper.Name, oldState, newState, "");
                }
                Response.Redirect(Url(
                    CurrentFilter[PaperBrandFilterKey],
                    CurrentFilter[DeviceFilterKey],
                    CurrentFilter[LabelFilterKey],
                    CurrentFilter[CategoryFilterKey],
                    CurrentFilter[SkuFilterKey],
                    CurrentFilter[SkuFilterTextKey],
                    CurrentFilter[PaperStateFilterKey]
                    ).
                    PathAndQuery);
            }
            catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }

            grdPapers.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdPapers_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdPapers.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewMode = true;
            DataBind();
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            grdPapers.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void ManagePaper_PreRender(object sender, EventArgs e) {
            grdPapers.Visible = grdPapers.Items.Count > 0;
        }

        protected void vldPaperXml_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = true;
            if (grdPapers.EditItemIndex >= 0) {
                DataGridItem item = grdPapers.Items[grdPapers.EditItemIndex];

                HtmlInputFile ctlPaperXmlFile = (HtmlInputFile) item.FindControl(CtlPaperXmlFileId);
                if (ctlPaperXmlFile.PostedFile.InputStream.Length != 0) {
                    ArrayList errorList = new ArrayList(BCPaperImport.Validate
                        (ctlPaperXmlFile.PostedFile.InputStream, ImportSchema.Paper));

                    try {
                        BCPaperImport.ParsePaper(ctlPaperXmlFile.PostedFile.InputStream, ImportSchema.Paper);
                    }
                    catch (BusinessException ex) {
                        errorList.Add(ex.Message);
                    }

                    string[] errors = (string[]) errorList.ToArray(typeof (string));

                    args.IsValid = (errors.Length == 0);

                    StringBuilder sb = new StringBuilder();
                    foreach (string error in errors) {
                        if (sb.Length == 0) {
                            sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(error));
                        } else {
                            sb.Append(HtmlHelper.BrTag);
                            sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(error));
                        }
                    }

                    ((CustomValidator) source).ErrorMessage = sb.ToString();
                } else {
                    string fileName = ctlPaperXmlFile.Value;
                    if (fileName != string.Empty) {
                        args.IsValid = false;
                        string errorMessageFormat = "Can't open file '{0}'";
                        ((CustomValidator) source).ErrorMessage =
                            string.Format(errorMessageFormat, fileName);
                    }
                }
            }
        }

        private void grdPapers_ItemCreated(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            if (item.ItemType != ListItemType.EditItem)
                return;

            GridTransformationHelper.EditItemCreationForm(item, grdPapers.Columns, NumberOfColumns);
        }

        public static void SortDropDown(DropDownList list) {
            ListItemCollection items = list.Items;
            for (int i = 1; i < items.Count; i++) {
                string text1 = items[i].Text;
                string text2 = items[i - 1].Text;
                int result = text1.CompareTo(text2);
                if (result < 0) {
                    ListItem tmp = items[i - 1];
                    items.RemoveAt(i - 1);
                    //items.Insert(i-1,items[i-1]);
                    //items.RemoveAt(i); 
                    items.Insert(i, tmp);
                    i = 1;
                } else if (result == 0) {
                    ListItem itemToRemove = items[i];
                    ListItem itemToUpdate = items[i - 1];
                    string paperIdToRemove = itemToRemove.Value.Split(';')[0];
                    string paperIdToUpdate = itemToUpdate.Value.Split(';')[0];
                    bool bFound = false;
                    if (paperIdToUpdate.IndexOf('|') != -1) {
                        foreach (string st in paperIdToUpdate.Split('|')) {
                            if (st == paperIdToRemove) {
                                bFound = true;
                                break;
                            }
                        }
                    }
                    if (!bFound) {
                        itemToUpdate.Value = paperIdToRemove + '|' + itemToUpdate.Value;
                    }
                    items.RemoveAt(i);
                    i--;
                }

            }
        }
    }
}