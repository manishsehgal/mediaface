using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin {
    public class PreviewHtmlManagement : Page {
        protected DropDownList cboBanner;
        //protected HtmlInputFile ctlImageFile;
        //protected Button btnAddImage;
        //protected Button btnDelImage;
        //protected ListBox lstBannerImages;
        //protected HtmlImage imgBannerImage;
        protected TextBox txtText;
        protected Button btnSubmit;
        //protected HtmlControl ctlPreviewContaner;
        protected HtmlInputHidden ctlBaseUrl;
        protected Label lblClosed;
        protected Panel panContent;

        private ManualResetEvent allDone = new ManualResetEvent(false);

        private int bannerId {
            get { return Convert.ToInt32(cboBanner.SelectedValue); }
        }

        private string page {
            get { return BCPreviewPage.Preview; }
        }

        private string pageFull {
            get { return page + (page == "SiteClosed" ? ".html" : ".aspx"); }
        }

        private string pageIntranetUrl {
            get { return Path.Combine(Configuration.IntranetDesignerSiteUrl, pageFull); }
        }

        private string pagePublicUrl {
            get { return Path.Combine(Configuration.PublicDesignerSiteUrl, pageFull); }
        }

        #region Url

        private const string RawUrl = "PreviewHtmlManagement.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.PreRender += new EventHandler(PreviewHtmlManagement_PreRender);
            this.DataBinding += new EventHandler(PreviewHtmlManagement_DataBinding);
            this.cboBanner.SelectedIndexChanged += new EventHandler(cboBanner_SelectedIndexChanged);
            this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
//            this.btnAddImage.Click += new EventHandler(btnAddImage_Click);
//            this.btnDelImage.Click += new EventHandler(btnDelImage_Click);
//            this.lstBannerImages.SelectedIndexChanged += new EventHandler(lstBannerImages_SelectedIndexChanged);
        }

        #endregion

        private void PreviewHtmlManagement_PreRender(object sender, EventArgs e) {
            //if (lstBannerImages.Items.Count > 0)
                //btnDelImage.Attributes["onclick"] = "return confirm('Are you sure to delete selected image?')";
        }

        private void PreviewHtmlManagement_DataBinding(object sender, EventArgs e) {
            bindBannerPage();

            lblClosed.Text = "Site is closing for maintenance. Please, open site, if you want to manage banners.";
        }

        private void cboBanner_SelectedIndexChanged(object sender, EventArgs e) {
            bindImages(bannerId, null);
            bindText(bannerId);
        }

        private void bindBannerPage() {
            cboBanner.Items.Clear();
            cboBanner.Items.AddRange(BCPreviewPage.Enum());
            cboBanner.SelectedIndex = 0;

            bindImages((int)CustomerGroup.Trial, null);
            bindText((int)CustomerGroup.Trial);
        }

        private void bindText(int bannerId) {
            txtText.Text = GetBannerHtml(bannerId);
            bindPreview();
        }

        private void bindPreview() {
            string bannerRootPath = Path.Combine(Configuration.IntranetDesignerSiteUrl, BCBanner.BannerRootFolder);
            string pagePath = Path.Combine(bannerRootPath, page);
            string bannerPath = Path.Combine(pagePath, bannerId.ToString());

            ctlBaseUrl.Value = string.Format("<BASE href='{0}/'/>", bannerPath);
//            ctlPreviewContaner.Attributes["src"] = BCBanner.BannerUrl(page, bannerId);
        }

        private void bindImages(int bannerId, string imageName) {
//            lstBannerImages.Items.Clear();
//            string[] fileNames = GetFileNames(bannerId);
//            foreach (string fileName in fileNames) {
//                ListItem item = new ListItem(fileName, fileName);
//                lstBannerImages.Items.Add(item);
//            }
//
//            if (fileNames.Length > 0) {
//                imgBannerImage.Visible = true;
//                bindBannerImage(imageName == null ? fileNames[0] : imageName);
//            }
//            else
//                imgBannerImage.Visible = false;
        }

        private void lstBannerImages_SelectedIndexChanged(object sender, EventArgs e) {
//            bindBannerImage(lstBannerImages.SelectedValue);
        }

        private void bindBannerImage(string fileName) {
//            lstBannerImages.SelectedValue = fileName;
//            imgBannerImage.Visible = true;
            string bannerRootPath = Path.Combine(Configuration.IntranetDesignerSiteUrl, BCBanner.BannerRootFolder);
            string pagePath = Path.Combine(bannerRootPath, page);
            string bannerPath = Path.Combine(pagePath, bannerId.ToString());
            string filePath = Path.Combine(bannerPath, fileName);

//            imgBannerImage.Src = filePath;
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            SaveBannerHtml(bannerId);
            bindPreview();
        }

        private string GetBannerHtml(int bannerId) {
            string htmlText = string.Empty;
            string bannerHandler = BannerHandler.UrlForHtmlText(Configuration.IntranetDesignerSiteUrl, page, bannerId);
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(bannerHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "GET";

            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
            if (request.HaveResponse) {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                htmlText = reader.ReadToEnd();

                reader.Close();
                response.Close();
            }

            return htmlText;
        }

        private string[] GetFileNames(int bannerId) {
            string bannerHandler = BannerHandler.UrlForEnumImages(Configuration.IntranetDesignerSiteUrl, page, bannerId);
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(bannerHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "GET";

            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
            if (!request.HaveResponse)
                return null;

            try {
                XmlDocument xmlDoc = new XmlDocument();
                if (response.GetResponseStream() != null)
                    xmlDoc.Load(response.GetResponseStream());
                response.Close();

                ArrayList list = new ArrayList();
                XmlNodeList fileList = xmlDoc.SelectNodes("//Files/File");
                for (int i = 0; i < fileList.Count; i++) {
                    XmlNode file = fileList[i];
                    list.Add(file.InnerText);
                }

                return (string[]) list.ToArray(typeof (string));
            }
            catch (Exception ex) {
                lblClosed.Visible = true;
                panContent.Visible = false;
                return new string[0];
            }
        }

        private void SaveBannerHtml(int bannerId) {
            string bannerHandler = BannerHandler.UrlForHtmlText(Configuration.IntranetDesignerSiteUrl, page, bannerId);
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(bannerHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "POST";

            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(txtText.Text);
            writer.Close();

            request.BeginGetResponse(new AsyncCallback(ResponseCallback), request);
            allDone.WaitOne();
        }

        private void ResponseCallback(IAsyncResult asynchronousResult) {
            allDone.Set();
        }

        private void btnAddImage_Click(object sender, EventArgs e) {
//            if (ctlImageFile.PostedFile == null || ctlImageFile.PostedFile.ContentLength == 0)
//                return;
//
//            string fileName = Path.GetFileName(ctlImageFile.Value);
//            string del = " .";
//            fileName = fileName.Trim(del.ToCharArray());
//            string bannerHandler = BannerHandler.UrlForUploadImage(Configuration.IntranetDesignerSiteUrl, page, bannerId, fileName);
//            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(bannerHandler);
//            request.Credentials = CredentialCache.DefaultCredentials;
//            request.Method = "POST";
//
//            BinaryWriter writer = new BinaryWriter(request.GetRequestStream());
//            writer.Write(ConvertHelper.StreamToByteArray(ctlImageFile.PostedFile.InputStream));
//            writer.Close();
//
//            // Get the response.
//            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
//            if (request.HaveResponse) {
//                bindImages(bannerId, fileName);
//                response.Close();
//            }
        }

        private void btnDelImage_Click(object sender, EventArgs e) {
//            if (lstBannerImages.SelectedValue == string.Empty)
//                return;
//
//            string fileName = lstBannerImages.SelectedValue;
//            string bannerHandler = BannerHandler.UrlForDeleteImage
//                (Configuration.IntranetDesignerSiteUrl, page, bannerId, fileName);
//            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(bannerHandler);
//            request.Credentials = CredentialCache.DefaultCredentials;
//            request.Method = "GET";
//
//            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
//            if (request.HaveResponse) {
//                response.Close();
//            }
//
//            bindImages(bannerId, null);
        }
    }
}