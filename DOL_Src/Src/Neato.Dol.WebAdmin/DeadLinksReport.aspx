<%@ Page language="c#" Codebehind="DeadLinksReport.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.DeadLinksReport" %>
<%@ Register TagPrefix="cpt" TagName="DateInterval" Src="Controls/DateInterval.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>DeadLinksReport</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">

  <h3 align="center">
    <asp:Label Runat=server ID="lblTitle"/>
  </h3>
  <br/>

    <form id="Form1" method="post" runat="server">

     <div id="reportFull">
        <cpt:DateInterval id="dtiDates" runat="server" CaptionWidth="90px" DateControlWidth="120px"/>
        <asp:Button ID="btnSubmit" text="Search" Runat=server/>
        <asp:DataGrid ID="grdReport" Runat="server" AutoGenerateColumns="False"
            AllowPaging="True" AllowSorting="False" PageSize="25" PagerStyle-Mode="NumericPages"
            BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1"
            CellPadding="1" CellSpacing="1" HeaderStyle-Font-Bold="True" HeaderStyle-HorizontalAlign="Center" Width ="80%">
            <Columns>
  				<asp:TemplateColumn HeaderText="Url">
  					<ItemTemplate>
  						<asp:Label Runat="server" ID="lblUrl" />
  					</ItemTemplate>
  				</asp:TemplateColumn>
  				<asp:TemplateColumn HeaderText="Count">
  					<ItemTemplate>
  						<asp:Label Runat="server" ID="lblCount" />
  					</ItemTemplate>
  				</asp:TemplateColumn>
  				<asp:TemplateColumn HeaderText="Action">
  					<ItemTemplate>
  						<asp:Button Runat="server" ID="btnDelete" CommandName="Delete" CausesValidation="False" Text="Remove" Tooltip="Remove" />
                      </ItemTemplate>
  				</asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
        <asp:Label Runat=server ID="lblStatus" Visible="False" style="color:#ff0000" />
     </form>

    </div>

    <div id="reportDetails">

        <asp:DataGrid ID="grdDetailes" Runat="server" AutoGenerateColumns="False"
            AllowPaging="False" AllowSorting="False"
            BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1"
            CellPadding="1" CellSpacing="1" HeaderStyle-Font-Bold="True" HeaderStyle-HorizontalAlign="Center" Width ="80%">
            <Columns>
				<asp:TemplateColumn HeaderText="Ip">
					<ItemTemplate>
						<asp:Label Runat="server" ID="lblIp" />
					</ItemTemplate>
				</asp:TemplateColumn>
				<asp:TemplateColumn HeaderText="Time">
					<ItemTemplate>
						<asp:Label Runat="server" ID="lblTime" />
					</ItemTemplate>
				</asp:TemplateColumn>
				<asp:TemplateColumn HeaderText="Referer">
					<ItemTemplate>
						<asp:Label Runat="server" ID="lblReferer" />
					</ItemTemplate>
				</asp:TemplateColumn>
				<asp:TemplateColumn HeaderText="Actiopn">
					<ItemTemplate>
						<asp:Button Runat="server" ID="btnDelete2" CommandName="Delete" CausesValidation="False" Text="Remove" Tooltip="Remove" />
                    </ItemTemplate>
				</asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>

    </div> 	

   </form>

</body>
</html>
