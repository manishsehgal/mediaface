using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Neato.Dol.Business;
using Neato.Dol.Data.Scheme;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin {
    public class Parameters : Page {
        #region Constants
        private const string LblKeyName = "lblKey";
        private const string TxtValueName = "txtValue";
        private const string ParametersKey = "Parameters";
        private const string DataKeyField = "Id";

        #endregion

        protected DataGrid grdParameters;

        private ParametersData ParametersData {
            get { return (ParametersData) ViewState[ParametersKey]; }
            set { ViewState[ParametersKey] = value; }
        }

        #region Url

        private const string RawUrl = "Parameters.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                ParametersData = BCParameters.EnumParameters();
                DataBind();
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.grdParameters.CancelCommand += new DataGridCommandEventHandler(this.grdParameters_CancelCommand);
            this.grdParameters.EditCommand += new DataGridCommandEventHandler(this.grdParameters_EditCommand);
            this.grdParameters.UpdateCommand += new DataGridCommandEventHandler(this.grdParameters_UpdateCommand);
            this.grdParameters.ItemDataBound += new DataGridItemEventHandler(this.grdParameters_ItemDataBound);
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(this.Parameters_DataBinding);
        }

        #endregion

        #region Actions

        private void grdParameters_EditCommand(object source, DataGridCommandEventArgs e) {
            ParametersData.RejectChanges();
            grdParameters.EditItemIndex = e.Item.ItemIndex;
            DataBind();
        }

        private void grdParameters_UpdateCommand(object source, DataGridCommandEventArgs e) {
            TextBox txtValue = (TextBox) e.Item.FindControl(TxtValueName);
            int id = (int) grdParameters.DataKeys[e.Item.ItemIndex];
            ParametersData.ParametersRow row = ParametersData.Parameters.FindById(id);
            row.Value = txtValue.Text;

            BCParameters.UpdateParameters(ParametersData);

            grdParameters.EditItemIndex = -1;
            DataBind();
        }

        private void grdParameters_CancelCommand(object source, DataGridCommandEventArgs e) {
            ParametersData.RejectChanges();
            grdParameters.EditItemIndex = -1;
            DataBind();
        }

        #endregion

        #region Data binding

        private void Parameters_DataBinding(object sender, EventArgs e) {
            grdParameters.DataSource = ParametersData;
            grdParameters.DataKeyField = DataKeyField;
        }

        private void grdParameters_ItemDataBound(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;

            if (item.ItemType == ListItemType.Item ||
                item.ItemType == ListItemType.AlternatingItem ||
                item.ItemType == ListItemType.EditItem) {
                ParametersData.ParametersRow row = (ParametersData.ParametersRow)
                    ((DataRowView) item.DataItem).Row;

                bool editMode = (item.ItemType == ListItemType.EditItem);
                Label lblKey = (Label) e.Item.FindControl(LblKeyName);
                TextBox txtValue = (TextBox) e.Item.FindControl(TxtValueName);
                lblKey.Text = row.Key;
                txtValue.Text = row.Value;
                txtValue.ReadOnly = !editMode;

                if (editMode) {
                    JavaScriptHelper.RegisterFocusScript(this, txtValue.ClientID);
                }
            }
        }

        #endregion
    }
}