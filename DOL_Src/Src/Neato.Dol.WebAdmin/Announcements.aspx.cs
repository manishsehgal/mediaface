using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.DataBase;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.WebAdmin.Helpers;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpModules;

namespace Neato.Dol.WebAdmin {
    public class Announcements : Page {
        #region Constants
        private const string BtnItemDeleteName = "btnItemDelete";
        private const string BtnItemEditName = "btnItemEdit";
        private const string BtnItemPreviewName = "btnItemPreview";
        private const string BtnItemUpdateName = "btnItemUpdate";
        private const string BtnItemCancelName = "btnItemCancel";
        private const string ChkItemActiveName = "chkItemActive";
        private const string LblItemTextName = "lblItemText";
        private const string TxtItemTextName = "txtItemText";
        private const string AnnouncementName = "ANNOUNCEMENT";

        private const string SetFocusScriptKey = "SET_FOCUS_SCRIPT_KEY";
        private const string SetFocusScript = "<script language='javascript'>var element = document.getElementById('{0}');element.focus();element.select();</script>";
        private const int NewItemIndex = 0;
        private const string JSPreview = "return Preview();";
        #endregion

        protected DataGrid dgAnnouncement;
        protected Button btnNew;
        protected Button btnSubmit;
        protected CustomValidator vldActiveAnnounces;
        protected HtmlControl ctlPreviewContaner;

        private AnnouncementData Announces {
            get { return (AnnouncementData)ViewState[AnnouncementName]; }
            set { ViewState[AnnouncementName] = value; }
        }

        #region Url
        private const string RawUrl = "Announcements.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                Announces = BCAnnouncement.EnumAnnouncements();
                DataBind();
                string url = CheckLocationModule.AdminUrl(Configuration.IntranetDesignerSiteUrl + WebDesigner.DefaultPage.PageName());
                ctlPreviewContaner.Attributes["src"] = url;
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Announcements_DataBinding);
            this.dgAnnouncement.ItemDataBound += new DataGridItemEventHandler(dgAnnouncement_ItemDataBound);
            this.dgAnnouncement.ItemCreated += new DataGridItemEventHandler(dgAnnouncement_ItemCreated);
            this.dgAnnouncement.EditCommand += new DataGridCommandEventHandler(dgAnnouncement_EditCommand);
            this.dgAnnouncement.UpdateCommand += new DataGridCommandEventHandler(dgAnnouncement_UpdateCommand);
            this.dgAnnouncement.CancelCommand += new DataGridCommandEventHandler(dgAnnouncement_CancelCommand);
            this.dgAnnouncement.DeleteCommand += new DataGridCommandEventHandler(dgAnnouncement_DeleteCommand);
            this.btnNew.Click += new EventHandler(btnNew_Click);
            this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
            this.vldActiveAnnounces.ServerValidate += new ServerValidateEventHandler(vldActiveAnnounces_ServerValidate);
        }
        #endregion

        private void btnNew_Click(object sender, EventArgs e) {
            Announces.RejectChanges();
            AnnouncementData.AnnouncementRow dr = Announces.Announcement.NewAnnouncementRow();
            dr.Text = string.Empty;
            dr.Time = DateTime.Now;
            dr.IsActive = true;
            Announces.Announcement.Rows.InsertAt(dr, NewItemIndex);

            dgAnnouncement.EditItemIndex = NewItemIndex;
            DataBind();
        }

        private void dgAnnouncement_EditCommand(object source, DataGridCommandEventArgs e) {
            Announces.RejectChanges();
            dgAnnouncement.EditItemIndex = e.Item.ItemIndex;
            DataBind();
        }

        private void dgAnnouncement_UpdateCommand(object source, DataGridCommandEventArgs e) {
            CheckBox chkItemActive = (CheckBox)e.Item.FindControl(ChkItemActiveName);
            TextBox txtItemText = (TextBox)e.Item.FindControl(TxtItemTextName);
            
            int id = (int)dgAnnouncement.DataKeys[e.Item.ItemIndex];
            AnnouncementData.AnnouncementRow dr = Announces.Announcement.FindById(id);
            if (dr != null) {
                dr.Text = txtItemText.Text;
                dr.IsActive = chkItemActive.Checked;
                BCAnnouncement.UpdateAnnouncement(Announces);
            }

            dgAnnouncement.EditItemIndex = -1;
            DataBind();
        }

        private void dgAnnouncement_CancelCommand(object source, DataGridCommandEventArgs e) {
            Announces.RejectChanges();
            dgAnnouncement.EditItemIndex = -1;
            DataBind();
        }

        private void dgAnnouncement_DeleteCommand(object source, DataGridCommandEventArgs e) {
            int id = (int)dgAnnouncement.DataKeys[e.Item.ItemIndex];
            AnnouncementData.AnnouncementRow dr = Announces.Announcement.FindById(id);
            if (dr != null) {
                dr.Delete();
                BCAnnouncement.UpdateAnnouncement(Announces);
                Announces = BCAnnouncement.EnumAnnouncements();
                Response.Redirect(Url().PathAndQuery);
            }
            
            dgAnnouncement.EditItemIndex = -1;
            DataBind();
        }

        #region Data binding
        private void Announcements_DataBinding(object sender, EventArgs e) {
            dgAnnouncement.DataSource = Announces;
            dgAnnouncement.DataKeyField = AnnouncementData.AnnouncementDataTable.Id;
        }

        private void dgAnnouncement_ItemDataBound(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            switch (item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.EditItem:
                    ItemBound(e.Item);
                    break;
            }            
        }

        private void ItemBound(DataGridItem item) {
            AnnouncementData.AnnouncementRow announcement = (AnnouncementData.AnnouncementRow)((DataRowView)item.DataItem).Row;

            bool edit = item.ItemType == ListItemType.EditItem;
            
            ImageButton btnEdit = (ImageButton)item.FindControl(BtnItemEditName);
            ImageButton btnPreview = (ImageButton)item.FindControl(BtnItemPreviewName);
            ImageButton btnUpdate = (ImageButton)item.FindControl(BtnItemUpdateName);
            ImageButton btnCancel = (ImageButton)item.FindControl(BtnItemCancelName);
            ImageButton btnDelete = (ImageButton)item.FindControl(BtnItemDeleteName);
            CheckBox chkItemActive = (CheckBox)item.FindControl(ChkItemActiveName);
            Label lblItemText = (Label)item.FindControl(LblItemTextName);
            TextBox txtItemText = (TextBox)item.FindControl(TxtItemTextName);

            if (edit) {
                txtItemText.Text = announcement.Text;
                if (!IsStartupScriptRegistered(SetFocusScriptKey))
                    RegisterStartupScript(SetFocusScriptKey, string.Format(SetFocusScript, txtItemText.ClientID));
            } else
                lblItemText.Text = announcement.Text;                
            chkItemActive.Checked = announcement.IsActive;

            btnEdit.Visible = !edit;
            btnDelete.Visible = !edit;
            btnUpdate.Visible = edit;
            btnCancel.Visible = edit;
            btnPreview.Visible = edit;
            btnPreview.Attributes["onclick"] = JSPreview;
        }

        private void dgAnnouncement_ItemCreated(object sender, DataGridItemEventArgs e) {

        }
        #endregion

        protected void chkItemActive_CheckedChanged(object sender, EventArgs e) {
            CheckBox chkActive = sender as CheckBox;

            DataGridItem item = (DataGridItem)chkActive.Parent.Parent;
            int id = (int)dgAnnouncement.DataKeys[item.ItemIndex];

            AnnouncementData.AnnouncementRow dr = Announces.Announcement.FindById(id);
            dr.IsActive = chkActive.Checked;
            try {
                BCAnnouncement.UpdateAnnouncement(Announces);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            if (!IsValid) {
                return;
            }

            try {
                BCAnnouncement.UpdateAnnouncement(Announces);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
        }

        private void vldActiveAnnounces_ServerValidate(object source, ServerValidateEventArgs args) {
            int count = 0;
            foreach(AnnouncementData.AnnouncementRow dr in Announces.Announcement.Rows) {
                if (dr.RowState != DataRowState.Deleted && dr.IsActive)
                    ++count;
            }

            args.IsValid = count <= 1;
        }
    }
}