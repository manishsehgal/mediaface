using System;
using System.Web;
using Neato.Dol.Entity;

namespace Neato.Dol.WebAdmin.HttpModules {
    public class CheckLocationModule : IHttpModule {
        public CheckLocationModule() {
        }

        #region IHttpModule members

        public void Init(HttpApplication context) {
            context.AcquireRequestState += new EventHandler(context_AcquireRequestState);
        }

        public void Dispose() {
        }

        #endregion IHttpModule members

        private void context_AcquireRequestState(object sender, EventArgs e) {
            HttpRequest request = HttpContext.Current.Request;
            HttpResponse response = HttpContext.Current.Response;

            Uri adminSiteUri = new Uri(Configuration.AdminSiteUrl);

            if (request.Url.Host != adminSiteUri.Host) {
                UriBuilder ub = new UriBuilder(adminSiteUri);
                ub.Path = request.Url.PathAndQuery;
                response.Redirect(ub.Uri.AbsoluteUri);
            }
        }
    }
}