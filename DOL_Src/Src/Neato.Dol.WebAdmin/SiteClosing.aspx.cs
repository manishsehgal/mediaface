using System;
using System.IO;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;
using Neato.Dol.WebDesigner.HttpModules;

namespace Neato.Dol.WebAdmin {
    public class SiteClosing : Page {
        protected Label lblSessionsCount;
        protected Button btnBlockSite;
        protected Button btnRefresh;
        protected Button btnBlockUsers;
        private bool isClosedForMaintenance;

        private bool IsSiteBlocked {
            get { return (bool) ViewState["IsSiteClosed"]; }
            set { ViewState["IsSiteClosed"] = value; }
        }

        private string sessionsCount;

        private void Page_Load(object sender, EventArgs e) {
            try {
                isClosedForMaintenance = BCMaintenanceClosing.SiteClosed;
                if (!IsPostBack) {
                    IsSiteBlocked = false;
                    DataBind();
                }
            }
            catch(Exception ex) {
                isClosedForMaintenance = true;
                btnBlockUsers.Visible = false;
                btnBlockSite.Visible = false;
                btnRefresh.Visible = false;
                lblSessionsCount.Text = "You have no permission to close the site.";
            }
        }

        #region Url

        private const string RawUrl = "SiteClosing.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        #endregion

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.lblSessionsCount.DataBinding += new EventHandler(this.lblSessionsCount_DataBinding);
            this.btnBlockSite.DataBinding += new EventHandler(this.btnBlockSite_DataBinding);
            this.btnBlockSite.Click += new EventHandler(this.btnBlockSite_Click);
            this.btnRefresh.DataBinding += new EventHandler(this.btnRefresh_DataBinding);
            this.btnRefresh.Click += new EventHandler(this.btnRefreash_Click);
            this.btnBlockUsers.DataBinding += new EventHandler(btnBlockUsers_DataBinding);
            this.btnBlockUsers.Click += new EventHandler(btnBlockUsers_Click);
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(this.SiteClosing_DataBinding);

        }

        #endregion

        #region Data bind

        private void lblSessionsCount_DataBinding(object sender, EventArgs e) {
            lblSessionsCount.Text = string.Format("Opened sessions: {0}", sessionsCount);
        }

        private void btnBlockSite_DataBinding(object sender, EventArgs e) {
            btnBlockSite.Text = isClosedForMaintenance ? "Unblock site" : "Block site";
        }

        #endregion

        private void btnBlockSite_Click(object sender, EventArgs e) {
            BCMaintenanceClosing.SiteClosed = isClosedForMaintenance = !isClosedForMaintenance;
            DataBind();
        }

        private void SiteClosing_DataBinding(object sender, EventArgs e) {
            GetData();
        }

        private void GetData() {
            if (isClosedForMaintenance) {
                sessionsCount = "Site is closed for maintenance";
            }
            else {
                string query = IsPostBack ? "?isSiteClosed=" + IsSiteBlocked : string.Empty;
                string uri = CheckLocationModule.AdminUrl(Configuration.IntranetDesignerSiteUrl + SessionCountHandler.PageName() + query); 
                HttpWebRequest request = (HttpWebRequest) WebRequest.Create(uri);
                request.Credentials = CredentialCache.DefaultCredentials;
                request.Method = "GET";

                HttpWebResponse response = (HttpWebResponse) request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());

                XmlDocument xmlDoc = new XmlDocument();
                string data = reader.ReadToEnd();
                xmlDoc.LoadXml(data);
                reader.Close();
                response.Close();

                IsSiteBlocked = bool.Parse(xmlDoc.DocumentElement.SelectSingleNode("@IsSiteClosed").Value);
                sessionsCount = xmlDoc.DocumentElement.SelectSingleNode("@SessionsCount").Value;
            }
        }

        private void btnRefreash_Click(object sender, EventArgs e) {
            DataBind();
        }

        private void btnRefresh_DataBinding(object sender, EventArgs e) {
            btnRefresh.Text = "Refresh";
        }

        private void btnBlockUsers_DataBinding(object sender, EventArgs e) {
            if (isClosedForMaintenance) {
                btnBlockUsers.Visible = false;
            }
            else {
                btnBlockUsers.Visible = true;
                btnBlockUsers.Text = IsSiteBlocked ? "Unblock new users" : "Block new users";
            }
        }

        private void btnBlockUsers_Click(object sender, EventArgs e) {
            IsSiteBlocked = !IsSiteBlocked;
            DataBind();
        }
    }
}