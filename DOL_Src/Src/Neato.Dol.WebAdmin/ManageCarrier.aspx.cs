using System;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.WebAdmin.Helpers;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin {
    public class ManageCarrier : Page {
        private const string LblNameId = "lblName";
        private const string ImgIconId = "imgIcon";
        private const string CtlIconFileId = "ctlIconFile";
        private const string TxtNameId = "txtName";
        private const string BtnDeleteId = "btnDelete";
        protected Button btnNew;
        protected DataGrid grdCarriers;


        private bool NewCarrierMode {
            get { return (bool)ViewState["NewCarrierModeKey"]; }
            set { ViewState["NewCarrierModeKey"] = value; }
        }

        #region Url
        private const string RawUrl = "ManageCarrier.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                NewCarrierMode = false;
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManageCarrier_DataBinding);
            grdCarriers.ItemDataBound += new DataGridItemEventHandler(grdCarriers_ItemDataBound);
            grdCarriers.EditCommand += new DataGridCommandEventHandler(grdCarriers_EditCommand);
            grdCarriers.DeleteCommand += new DataGridCommandEventHandler(grdCarriers_DeleteCommand);
            grdCarriers.UpdateCommand += new DataGridCommandEventHandler(grdCarriers_UpdateCommand);
            grdCarriers.CancelCommand += new DataGridCommandEventHandler(grdCarriers_CancelCommand);
            btnNew.Click += new EventHandler(btnNew_Click);
        }
        #endregion

        private void ManageCarrier_DataBinding(object sender, EventArgs e) {
            ArrayList carriers = new ArrayList(BCCarrier.GetCarrierList());
            if (NewCarrierMode) {
                carriers.Insert(0, BCCarrier.NewCarrier());
                grdCarriers.EditItemIndex = 0;
            } else if (grdCarriers.EditItemIndex >= 0) {
                int carrierId = (int)grdCarriers.DataKeys[grdCarriers.EditItemIndex];
                int index = carriers.IndexOf(new CarrierBase(carrierId));
                grdCarriers.EditItemIndex = index;
            }
            grdCarriers.DataSource = carriers;
            grdCarriers.DataKeyField = Carrier.IdField;
        }

        private void grdCarriers_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void grdCarriers_EditCommand(object source, DataGridCommandEventArgs e) {
            grdCarriers.EditItemIndex = e.Item.ItemIndex;
            NewCarrierMode = false;
            DataBind();
        }

        private void grdCarriers_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdCarriers.EditItemIndex = -1;
            int carrierId = (int)grdCarriers.DataKeys[e.Item.ItemIndex];
            CarrierBase carrier = new CarrierBase(carrierId);
            try {
                BCCarrier.Delete(carrier);
                Response.Redirect(Url().PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewCarrierMode = false;
            DataBind();
        }

        private void grdCarriers_UpdateCommand(object source, DataGridCommandEventArgs e) {
            Page.Validate();
            if (!IsValid) return;

            Carrier carrier = BCCarrier.NewCarrier();

            carrier.Id = (int)grdCarriers.DataKeys[e.Item.ItemIndex];
            TextBox txtName = (TextBox)e.Item.FindControl(TxtNameId);

            carrier.Name = txtName.Text.Trim();
            HtmlInputFile ctlIconFile = (HtmlInputFile)e.Item.FindControl(CtlIconFileId);

            Stream icon = ctlIconFile.PostedFile.InputStream;

            try {
                BCCarrier.Save(carrier, icon);
                Response.Redirect(Url().PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }

            grdCarriers.EditItemIndex = -1;
            NewCarrierMode = false;
            DataBind();
        }

        private void grdCarriers_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdCarriers.EditItemIndex = -1;
            NewCarrierMode = false;
            DataBind();
        }

        private void ItemDataBound(DataGridItem item) {
            Carrier carrier = (Carrier)item.DataItem;
            Label lblName = (Label)item.FindControl(LblNameId);
            lblName.Text = HttpUtility.HtmlEncode(carrier.Name);

            Image imgIcon = (Image)item.FindControl(ImgIconId);
            

            // TODO: refactoring: extract method to helper
            string imageUrl = IconHandler.ImageUrl(carrier).AbsoluteUri;
            UriBuilder appRoot = new UriBuilder(Request.Url);
            appRoot.Path = Request.ApplicationPath;
            appRoot.Query = string.Empty;
            imageUrl = imageUrl.Replace(appRoot.Uri.AbsoluteUri.TrimEnd('\\', '/'), Configuration.IntranetDesignerSiteUrl.TrimEnd('\\', '/'));
            
            imgIcon.ImageUrl = imageUrl;

            ImageButton btnDelete = (ImageButton)item.FindControl(BtnDeleteId);
            string warning = "All devices will be removed from the Carrier." + Environment.NewLine + "Are you sure?";
            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", warning);
        }

        private void EditItemDataBound(DataGridItem item) {
            Carrier carrier = (Carrier)item.DataItem;
            TextBox txtName = (TextBox)item.FindControl(TxtNameId);
            txtName.Text = carrier.Name;
            JavaScriptHelper.RegisterFocusScript(this, txtName.ClientID);
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewCarrierMode = true;
            DataBind();
        }
    }
}