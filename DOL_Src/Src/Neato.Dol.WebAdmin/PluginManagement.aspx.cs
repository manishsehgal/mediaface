using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin {
    public class PluginManagement : Page {
        protected HtmlInputFile ctlAdditionalFile;
        protected Button btnAddFile2Root;
        protected Button btnAddFile2Modules;
        protected Button btnDelFile;
        protected ListBox lstAdditionalFiles;
        protected Label lblClosed;
        protected Panel panContent;
        protected DropDownList cboTargetOS;
        string targetOS="Windows";

        #region Url
        private const string RawUrl = "PluginManagement.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(PluginManagement_DataBinding);
            this.btnAddFile2Root.Click += new EventHandler(btnAddFile2Root_Click);
            this.btnAddFile2Modules.Click += new EventHandler(btnAddFile2Modules_Click);
            this.btnDelFile.Click += new EventHandler(btnDelFile_Click);
            this.PreRender += new EventHandler(PluginManagement_PreRender);
            this.cboTargetOS.SelectedIndexChanged += new EventHandler(cboTargetOS_SelectedIndexChanged);
            this.cboTargetOS.DataBinding +=new EventHandler(cboTargetOS_DataBinding);
        }
        #endregion

        private void PluginManagement_DataBinding(object sender, EventArgs e) {
            lblClosed.Text = "Site is closing for maintenance. Please, open site, if you want to manage plugins.";
            BindAdditionalFiles();
        }

        private void btnAddFile2Root_Click(object sender, EventArgs e) {
            addFile(string.Empty);
        }

        private void btnAddFile2Modules_Click(object sender, EventArgs e) {
            addFile("modules/");
        }

        private void addFile(string path) {
            if (ctlAdditionalFile.PostedFile == null || ctlAdditionalFile.PostedFile.ContentLength == 0)
                return;

            string fileName = path + Path.GetFileName(ctlAdditionalFile.Value);
            string del = " .";
            fileName = fileName.Trim(del.ToCharArray());
            targetOS = cboTargetOS.SelectedValue;
            string pluginHandler = PluginFileHandler.UrlForUploadFile(Configuration.IntranetDesignerSiteUrl, fileName, targetOS);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(pluginHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "POST";

            BinaryWriter writer = new BinaryWriter(request.GetRequestStream());
            writer.Write(ConvertHelper.StreamToByteArray(ctlAdditionalFile.PostedFile.InputStream));
            writer.Close();

            // Get the response.
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (request.HaveResponse) {
                BindAdditionalFiles();
                response.Close();
            }
        }

        private void BindAdditionalFiles() {
            lstAdditionalFiles.Items.Clear();
            string[] fileNames = GetAdditionalFileNames();
            foreach (string fileName in fileNames) {
                ListItem item = new ListItem(fileName, fileName);
                lstAdditionalFiles.Items.Add(item);
            }
        }

        private string[] GetAdditionalFileNames() {
            targetOS = cboTargetOS.SelectedValue;
            string pluginHandler = PluginFileHandler.UrlForEnumFiles(Configuration.IntranetDesignerSiteUrl, targetOS);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(pluginHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "GET";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (!request.HaveResponse)
                return null;

            try {
                XmlDocument xmlDoc = new XmlDocument();
                if (response.GetResponseStream() != null)
                    xmlDoc.Load(response.GetResponseStream());
                response.Close();

                ArrayList list = new ArrayList();
                XmlNodeList fileList = xmlDoc.SelectNodes("//Files/File");
                for (int i = 0; i < fileList.Count; i++) {
                    XmlNode file = fileList[i];
                    list.Add(file.InnerText);
                }

                return (string[])list.ToArray(typeof(string));
            }
            catch (Exception ex) {
                lblClosed.Visible = true;
                panContent.Visible = false;
                return new string[0];
            }
        }

        private void btnDelFile_Click(object sender, EventArgs e) {
            if (lstAdditionalFiles.SelectedValue == string.Empty)
                return;

            string fileName = lstAdditionalFiles.SelectedValue;
            targetOS = cboTargetOS.SelectedValue;
            string pluginHandler = PluginFileHandler.UrlForDeleteFile(
                Configuration.IntranetDesignerSiteUrl, fileName, targetOS);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(pluginHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "GET";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (request.HaveResponse) {
                response.Close();
            }

            BindAdditionalFiles();
        }

        private void PluginManagement_PreRender(object sender, EventArgs e) {
            if (lstAdditionalFiles.Items.Count > 0) {
                btnDelFile.Attributes["onclick"] = "return confirm('Are you sure to delete selected file?')";
            }
        }

        private void cboTargetOS_SelectedIndexChanged(object sender, EventArgs e)
        {
            targetOS = cboTargetOS.SelectedValue;
            DataBind();
        }

        private void cboTargetOS_DataBinding(object sender, EventArgs e)
        {
            cboTargetOS.Items.Clear();
            int selIdx=0;
            cboTargetOS.Items.Add(new ListItem("Windows", "Windows"));
            if(targetOS == "Windows")
                selIdx = 0;
            cboTargetOS.Items.Add(new ListItem("MacOS PowerPC", "PowerMac"));
            if(targetOS == "PowerMac")
                selIdx = 1;
            cboTargetOS.Items.Add(new ListItem("MacOS Intel", "IntelMac"));
            if(targetOS == "IntelMac")
                selIdx = 2;
            cboTargetOS.SelectedIndex = selIdx;
        }
    }
}