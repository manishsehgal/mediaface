using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebAdmin.Controls;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin {
    public class TrackingPhonesReport : Page {
        protected DataGrid grdReport;
        //protected DropDownList cbManufacturers;
        protected Label lblTitle;
        protected Label lblStatus;
        protected DateInterval dtiDates;

        protected Button btnSubmit;

        #region Url
        private const string RawUrl = "TrackingPhonesReport.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(TrackingPhonesReport_DataBinding);
            btnSubmit.Click += new EventHandler(btnSubmit_Click);
        }
        #endregion

        private void TrackingPhonesReport_DataBinding(object sender, EventArgs e) {
            DataSet Carriersdata = BCTrackingPhones.GetCarriersList();

            lblTitle.Text = "What models have been chosen";

            /*int selIndex = cbManufacturers.SelectedIndex;
            cbManufacturers.Items.Clear();
            cbManufacturers.Items.Add("All");
            foreach (DataTable Tables in Carriersdata.Tables) {
                foreach (DataRow Rows in Tables.Rows) {
                    foreach (DataColumn Column in Tables.Columns) {
                        cbManufacturers.Items.Add((string)Rows[Column]);
                    }
                }
            }
            if (selIndex >= 0)
                cbManufacturers.SelectedIndex = selIndex;*/
            lblStatus.Text = "There are no results matching your criteria.";
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            if (IsValid) {
                DeviceBrand brand = null;
                /*if (cbManufacturers.SelectedItem.Text == "All")
                    brand = null;
                else {
                    brand = new DeviceBrand(0, cbManufacturers.SelectedItem.Text);

                }*/

                lblStatus.Visible = false;
                grdReport.Visible = false;

                DataSet data = BCTrackingPhones.GetListByBrandAndDate
                    (brand, dtiDates.StartDate, dtiDates.EndDate);

                bool dataExisting = (data.Tables[0].Rows.Count > 0);

                if (dataExisting) {
                    grdReport.DataSource = data;
                    grdReport.Columns[0].Visible = false;
                }
                grdReport.Visible = dataExisting;
                lblStatus.Visible = !dataExisting;

                DataBind();
            }
        }
    }
}