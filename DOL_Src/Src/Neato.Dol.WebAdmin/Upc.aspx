<%@ Page language="c#" Codebehind="Upc.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.Upc" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>MFO UPC Management</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
	<form id="Form1" method="post" runat="server">
	  <h3 align="center">MFO UPC Management</h3>
      <asp:Button ID="btnNew" Runat="server" Text="New UPC" CausesValidation="False" />
      <br>
      <br>
      <asp:DataGrid Runat="server" ID="grdUpc" AutoGenerateColumns="False">
        <Columns>
          <asp:TemplateColumn HeaderText="Code">
            <HeaderStyle HorizontalAlign="Center" Width="200px" />
            <ItemStyle HorizontalAlign="Left" />
            <HeaderStyle Wrap="False" HorizontalAlign="Center" />
            <ItemStyle Wrap="False"/>
            <ItemTemplate>
                <asp:Label ID="lblCode" Runat="server" style="width:200px;overflow:hidden;" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtCode" Runat="server" MaxLength="100" style="width:200px" />
                <asp:RequiredFieldValidator ID="vldCodeRequired" Runat="server" ControlToValidate="txtCode" Display="Dynamic"
                    ErrorMessage="Enter the UPC code" />
            </EditItemTemplate>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Action">
            <HeaderStyle HorizontalAlign="Left" />
            <ItemTemplate>
              <nobr>
                <asp:Button Runat="server" ID="btnEdit" CommandName="Edit" CausesValidation="False"
                    Text="Edit" Tooltip="Edit UPC" />
                <asp:Button Runat="server" ID="btnDelete" CommandName="Delete"
                    CausesValidation="False" Text="Remove" Tooltip="Remove UPC" />
              </nobr>
            </ItemTemplate>
            <EditItemTemplate>
              <nobr>
                <asp:Button Runat="server" ID="btnUpdate" Text="Save" CommandName="Update" CausesValidation="True"
                    Tooltip="Save UPC" />
                <asp:Button Runat="server" ID="btnCancel" CommandName="Cancel" Text="Cancel"
                    CausesValidation="False" Tooltip="Cancel" />
              </nobr>
            </EditItemTemplate>
            <HeaderStyle Wrap="False" HorizontalAlign="Center" />
            <ItemStyle Wrap="False" Width="70px" />
          </asp:TemplateColumn>
        </Columns>
      </asp:DataGrid>
    </form>	
  </body>
</html>
