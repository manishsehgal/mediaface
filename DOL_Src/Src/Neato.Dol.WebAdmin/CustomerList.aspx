<%@ Page language="c#" Codebehind="CustomerList.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.CustomerList" %>
<%@ Register TagPrefix="cpt" TagName="DateInterval" Src="Controls/DateInterval.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Customer Profiles</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
      <h3 align="center">Customer Profiles</h3>
      <table border="0" cellpadding="4" cellspacing="0">
        <tr>
          <td>First Name :</td>
          <td>
            <asp:TextBox runat="server" ID="txtFirstName" MaxLength="100" style="width:150px;" />
          </td>
          <td rowspan="3">
            <cpt:DateInterval id="dtiDates" runat="server" CaptionWidth="90px" DateControlWidth="120px" />
          </td>
        </tr>
        <tr>
          <td>Last Name :</td>
          <td>
            <asp:TextBox runat="server" ID="txtLastName" MaxLength="100" style="width:150px;" />
          </td>
        </tr>
        <tr>
          <td>Email Address :</td>
          <td><asp:TextBox runat="server" ID="txtEmail" MaxLength="100" style="width:150px;" /></td>
        </tr>
        <tr>
          <td><asp:CheckBox ID="chkShowNotActiveUsers" Runat="server" Text="Show not active users"/></td>
          <td align="right"><asp:Button runat="server" ID="btnCustomerSearch" Text="Search" /></td>
          <td colspan="2"><asp:Label ID="lblResultsCount" Runat="server" /></td>
        </tr>
      </table>
      <asp:Button ID="btnNewCustomer" Runat="server" Text="New Customer" />
      <br>
      <br>     
      <asp:DataGrid
        Runat="server"
        ID="grdCustomers"
        AllowPaging="True"
        PageSize="25"
        AutoGenerateColumns="False">
        <Columns>
          <asp:TemplateColumn HeaderText="First Name">
            <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="110px" />
            <ItemTemplate>
                <asp:Label ID="lblFirstNameItem" Runat="server" style="width:110px;overflow:hidden;" />
            </ItemTemplate>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Last Name">
            <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="110px" />
            <ItemTemplate>
                <asp:Label ID="lblLastNameItem" Runat="server" style="width:110px;overflow:hidden;" />
            </ItemTemplate>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="E-Mail Address">
            <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="190px" />
            <ItemTemplate>
                <asp:Label ID="lblEmailItem" Runat="server" style="width:190px;overflow:hidden;"/>
            </ItemTemplate>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Password">
            <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="100px" />
            <ItemTemplate>
                <asp:Label ID="lblPasswordItem" Runat="server" style="width:100px;overflow:hidden;"/>
            </ItemTemplate>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Active">
            <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="50px" />
            <ItemTemplate>
                <asp:CheckBox ID="chkActiveUser" Runat="server" Enabled="False"/>
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center"/>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Account Type">
            <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="50px" />
            <ItemTemplate>
                <asp:Label ID="lblAccountType" Runat="server" style="width:100px;overflow:hidden;"/>
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center"/>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="E-Mail Options">
            <HeaderStyle HorizontalAlign="Center" Width="60px" />
            <ItemTemplate>
                <asp:Label ID="lblEmailOptions" Runat="server" style="width: 60px;" />
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center"/>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Receive Information">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:CheckBox ID="chkReceiveInfo" Runat="server" Enabled="False"/>
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center"/>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Created">
            <HeaderStyle Wrap="False" HorizontalAlign="Center" />
            <ItemTemplate>
              <asp:Label ID="lblCreated" Runat="server" style="width: 90px;"/>
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" />
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Action">
            <ItemTemplate>
              <asp:ImageButton
                Runat="server"
                ID="btnItemEdit"
                ImageUrl="images/edit.gif"
                CommandName="Edit"
                CausesValidation="False"
                ImageAlign="AbsMiddle"
                AlternateText="Edit" />
              <asp:ImageButton
                Runat="server"
                ID="btnItemDelete"
                ImageUrl="images/remove.gif"
                ImageAlign="AbsMiddle"
                CommandName="Delete"
                CausesValidation="False"
                AlternateText="Remove" />
            </ItemTemplate>
            <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="100px"/>
            <ItemStyle Wrap="False"/>
          </asp:TemplateColumn>
        </Columns>
        <PagerStyle Mode="NumericPages" />
      </asp:DataGrid>
    </form>
  </body>
</html>
