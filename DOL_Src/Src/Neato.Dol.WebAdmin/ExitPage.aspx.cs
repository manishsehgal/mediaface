using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin {
    public class ExitPage : Page {
        protected DropDownList cboRetailer;
        protected HtmlInputFile ctlImageFile;
        protected Button btnAddImage;
        protected Button btnDelImage;
        protected ListBox lstImages;
        protected HtmlImage imgImage;
        protected TextBox txtText;
        protected Button btnSubmit;
        protected HtmlControl ctlPreviewContaner;
        protected HtmlInputHidden ctlBaseUrl;
        protected Label lblClosed;
        protected Panel panContent;

        private int retailerId {
            get { return Convert.ToInt32(cboRetailer.SelectedValue); }
        }

        #region Url
        private const string RawUrl = "ExitPage.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ExitPage_DataBinding);
            this.cboRetailer.SelectedIndexChanged += new EventHandler(cboRetailer_SelectedIndexChanged);
            this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
            this.btnAddImage.Click += new EventHandler(btnAddImage_Click);
            this.btnDelImage.Click += new EventHandler(btnDelImage_Click);
            this.lstImages.SelectedIndexChanged += new EventHandler(lstImages_SelectedIndexChanged);
            this.PreRender += new EventHandler(ExitPage_PreRender);
        }
        #endregion

        private void ExitPage_DataBinding(object sender, EventArgs e) {
            lblClosed.Text = "Site is closing for maintenance. Please, open site, if you want to manage exit page.";
            cboRetailer.Items.Clear();
            Retailer[] retailers = BCRetailer.Enum();
            foreach (Retailer retailer in retailers) {
                ListItem item = new ListItem(retailer.Name, retailer.Id.ToString());
                cboRetailer.Items.Add(item);
            }

            bindExitPage(retailers[0].Id);
        }

        private void bindExitPage(int retailerId) {
            bindImages(retailerId, null);
            bindText(retailerId);
        }

        private void bindText(int retailerId) {
            txtText.Text = GetExitPageHtml(retailerId);
            bindPreview(retailerId);
        }

        private void bindPreview(int retailerId) {
            string exitPageRootPath = Path.Combine(Configuration.IntranetDesignerSiteUrl, BCExitPage.ExitPageRootFolder);
            string exitPagePath = Path.Combine(exitPageRootPath, retailerId.ToString());

            ctlBaseUrl.Value = string.Format("<BASE href='{0}/'/>", exitPagePath);
            Banner banner = BCExitPage.GetExitPage(retailerId);
            ctlPreviewContaner.Attributes["src"] = banner.Src;
        }

        private void bindImages(int retailerId, string imageName) {
            lstImages.Items.Clear();
            string[] fileNames = GetFileNames(retailerId);
            foreach (string fileName in fileNames) {
                ListItem item = new ListItem(fileName, fileName);
                lstImages.Items.Add(item);
            }

            if (fileNames.Length > 0) {
                imgImage.Visible = true;
                bindImage(imageName == null ? fileNames[0] : imageName);
            } else
                imgImage.Visible = false;
        }

        private void bindImage(string fileName) {
            lstImages.SelectedValue = fileName;
            imgImage.Visible = true;
            string exitPageRootPath = Path.Combine(Configuration.IntranetDesignerSiteUrl, BCExitPage.ExitPageRootFolder);
            string exitPagePath = Path.Combine(exitPageRootPath, retailerId.ToString());
            string filePath = Path.Combine(exitPagePath, fileName);

            imgImage.Src = filePath;
        }

        private string[] GetFileNames(int retailerId) {
            string exitPageHandler = ExitPageHandler.UrlForEnumImages(Configuration.IntranetDesignerSiteUrl, retailerId);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(exitPageHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "GET";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (!request.HaveResponse)
                return null;

            try {
                XmlDocument xmlDoc = new XmlDocument();
                if (response.GetResponseStream() != null)
                    xmlDoc.Load(response.GetResponseStream());
                response.Close();

                ArrayList list = new ArrayList();
                XmlNodeList fileList = xmlDoc.SelectNodes("//Files/File");
                for (int i = 0; i < fileList.Count; i++) {
                    XmlNode file = fileList[i];
                    list.Add(file.InnerText);
                }

                return (string[])list.ToArray(typeof(string));
            }
            catch (Exception ex) {
                lblClosed.Visible = true;
                panContent.Visible = false;
                return new string[0];
            }
        }

        private string GetExitPageHtml(int retailerId) {
            string htmlText = string.Empty;
            string exitPageHandler = ExitPageHandler.UrlForHtmlText(Configuration.IntranetDesignerSiteUrl, retailerId);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(exitPageHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "GET";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (request.HaveResponse) {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                htmlText = reader.ReadToEnd();

                reader.Close();
                response.Close();
            }

            return htmlText;
        }

        private void cboRetailer_SelectedIndexChanged(object sender, EventArgs e) {
            bindExitPage(retailerId);
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            SaveHtml(retailerId);
            bindPreview(retailerId);
        }

        private void btnAddImage_Click(object sender, EventArgs e) {
            if (ctlImageFile.PostedFile == null || ctlImageFile.PostedFile.ContentLength == 0)
                return;

            string fileName = Path.GetFileName(ctlImageFile.Value);
            string del = " .";
            fileName = fileName.Trim(del.ToCharArray());
            string bannerHandler = ExitPageHandler.UrlForUploadImage(Configuration.IntranetDesignerSiteUrl, retailerId, fileName);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(bannerHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "POST";

            BinaryWriter writer = new BinaryWriter(request.GetRequestStream());
            writer.Write(ConvertHelper.StreamToByteArray(ctlImageFile.PostedFile.InputStream));
            writer.Close();

            // Get the response.
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (request.HaveResponse) {
                bindImages(retailerId, fileName);
                response.Close();
            }
        }

        private void btnDelImage_Click(object sender, EventArgs e) {
            if (lstImages.SelectedValue == string.Empty)
                return;

            string bannerHandler = ExitPageHandler.UrlForDeleteImage
                (Configuration.IntranetDesignerSiteUrl, retailerId, lstImages.SelectedValue);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(bannerHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "GET";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (request.HaveResponse) {
                response.Close();
            }

            bindImages(retailerId, null);
        }

        private void lstImages_SelectedIndexChanged(object sender, EventArgs e) {
            bindImage(lstImages.SelectedValue);
        }

        private void SaveHtml(int retailerId) {
            string bannerHandler = ExitPageHandler.UrlForHtmlText(Configuration.IntranetDesignerSiteUrl, retailerId);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(bannerHandler);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "POST";

            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(txtText.Text);
            writer.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (request.HaveResponse) {
                response.Close();
            }
        }

        private void ExitPage_PreRender(object sender, EventArgs e) {
            if (lstImages.Items.Count > 0) {
                btnDelImage.Attributes["onclick"] = "return confirm('Are you sure to delete selected image?')";
            }
        }
    }
}