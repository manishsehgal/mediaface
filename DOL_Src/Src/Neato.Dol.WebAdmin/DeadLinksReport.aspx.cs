using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.WebAdmin.Controls;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin {
    public class DeadLinksReport : Page {
        protected Button btnSubmit;
        protected Label lblTitle;
        protected Label lblStatus;
        protected DateInterval dtiDates;
        protected DataGrid grdReport;

        private string LblUrlId = "lblUrl";
        private string LblCountId = "lblCount";
        private string BtnDeleteId = "btnDelete";

        #region Url
        private const string RawUrl = "DeadLinksReport.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack)
                DataBind();
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(DeadLinksReport_DataBinding);
            btnSubmit.Click += new EventHandler(btnSubmit_Click);
            grdReport.ItemDataBound += new DataGridItemEventHandler(grdReport_ItemDataBound);
            grdReport.DeleteCommand += new DataGridCommandEventHandler(grdReport_DeleteCommand);
            grdReport.DataBinding += new EventHandler(grdReport_DataBinding);
            grdReport.PageIndexChanged += new DataGridPageChangedEventHandler(grdReport_PageIndexChanged);
            this.PreRender += new EventHandler(grdReport_PreRender);
        }

        #endregion

        private void DeadLinksReport_DataBinding(object sender, EventArgs e) {
            lblTitle.Text = "What deadlinks have been chosen";
            lblStatus.Text = "There are no results matching your criteria.";
        }

        private void grdReport_DataBinding(object sender, EventArgs e) {
            grdReport.DataSource = BCDeadLink.GetReport(dtiDates.StartDate, dtiDates.EndDate);
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            grdReport.DataBind();
        }

        private void grdReport_ItemDataBound(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            switch (item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                    DataRowView drv = (DataRowView)item.DataItem;
                    Label lblUrl = (Label) item.FindControl(LblUrlId);
                    Label lblCount = (Label) item.FindControl(LblCountId);
                    Button btnDelete = (Button) item.FindControl(BtnDeleteId);

                    lblUrl.Text = drv["Url"].ToString();
                    lblCount.Text = drv["Count"].ToString();
                    btnDelete.Attributes["onclick"] = "return confirm('Are you sure to delete this item?')";
                    break;
            }
        }

        private void grdReport_DeleteCommand(object source, DataGridCommandEventArgs e) {
            DataGridItem item = e.Item;
            Label lblUrl = (Label) item.FindControl(LblUrlId);
            string url  = lblUrl.Text;
            DeadLink deadlink = new DeadLink(url, string.Empty, string.Empty);
            try {
                BCDeadLink.DeleteByUrl(deadlink);
                Response.Redirect(DeadLinksReport.Url().PathAndQuery);
            }
            catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
        }

        private void grdReport_PreRender(object sender, EventArgs e) {
            DataSet ds = grdReport.DataSource as DataSet;
            bool isEmpty = ds.Tables[0].Rows.Count == 0;
            grdReport.Visible = !isEmpty;
            lblStatus.Visible = isEmpty;
        }

        private void grdReport_PageIndexChanged(object source, DataGridPageChangedEventArgs e) {
            grdReport.CurrentPageIndex = e.NewPageIndex;
            grdReport.DataBind();
        }
    }
}