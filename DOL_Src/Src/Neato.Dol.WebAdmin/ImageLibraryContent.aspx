<%@ Register TagPrefix="cpt" TagName="ImageList" Src="Controls/ImageList.ascx" %>
<%@ Page language="c#" Codebehind="ImageLibraryContent.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.ImageLibraryContent" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ImageLibrary</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script type="text/javascript" language="javascript" src="js/ImageLibrary.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<input type="hidden" id="ctlCurrentFolderId" runat="server"> <input type="hidden" id="ctlCurrentFolderLibId" runat="server">
			<input type="hidden" id="ctlImagesTryDelete" runat="server"> <input type="hidden" id="ctlIsRootSelected" runat="server">&nbsp;
			<h3 align="center">Image library</h3>
			<table border="1" width="100%" height="90%">
				<tr>
					<td width="30%" valign="top" rowspan="2">
						<div style="OVERFLOW: auto">
							<asp:LinkButton ID="btnRoot" Runat="server" /><br>
							<asp:Literal id="ctlFolderTree" runat="server" />
						</div>
					</td>
					<td width="20" valign="middle" align="center" rowspan="2">
						<asp:ImageButton Runat="server" ID="btnFolderUp" ImageUrl="images/up.gif" CommandName="Up" CausesValidation="False"
							AlternateText="Up" /><br>
						<asp:ImageButton Runat="server" ID="btnFolderDown" ImageUrl="images/down.gif" CommandName="Down"
							CausesValidation="False" AlternateText="Down" />
					</td>
					<td width="70%" valign="top">
						Images in current folder...
						<cpt:imagelist id="ctlImages" runat="server" />
					</td>
				</tr>
				<tr>
					<td>
						<asp:Button ID="btnDelSelectedImages" Runat="server" CausesValidation="False" />
					</td>
				</tr>
				<tr>
					<td width="30%" height="50" colspan="2">
						<asp:Button ID="btnDeleteFolder" Runat="server" CausesValidation="False" />
						<br>
						<asp:TextBox ID="txtFolderName" Runat="server" Width="100px" />&nbsp;
						<asp:Button ID="btnAddFolder" Runat="server" CausesValidation="True" />
						<asp:Button ID="btnEditFolder" Runat="server" CausesValidation="True" />
						<br>
						<asp:RequiredFieldValidator ID="vldFolderName" Runat="server" ErrorMessage="Please enter folder name" ControlToValidate="txtFolderName"
							EnableClientScript="False" />
					</td>
					<td width="30%" height="50">
						<input ID="ctlAddImage" Runat="server" type="file">&nbsp;
						<asp:Button ID="btnAddImage" Runat="server" CausesValidation="False" />
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
