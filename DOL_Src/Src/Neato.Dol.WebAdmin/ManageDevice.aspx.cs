using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.WebAdmin.Helpers;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin {
    public class ManageDevice : Page {
        private const string LblModelId = "lblModel";
        private const string ImgIconId = "imgIcon";
        private const string ImgBigIconId = "imgBigIcon";
        private const string CtlIconFileId = "ctlIconFile";
        private const string CtlBigIconFileId = "ctlBigIconFile";
        private const string TxtModelId = "txtModel";
        private const string BtnDeleteId = "btnDelete";
        private const string LblCategoryId = "lblCategory";
        private const string CboCategoryId = "cboCategory";
        private const string LblRatingId = "lblRating";
        private const string CboRatingId = "cboRating";
        private const string HypIconViewId = "hypIconView";
        private const string HypIconBigViewId = "hypIconBigView";
        private const string ManufactureFilterName = "Manufacture";
        private const string VldCategoryRequiredId = "vldCategoryRequired";

        private const int NumberOfColumns = 6;
        private const string SelectValue = "-2";

        protected Button btnNew;
        protected DataGrid grdDevices;
        protected Button btnDeviceSearch;

        private ArrayList ratings;
        private ArrayList categories;
        
        private bool NewMode {
            get { return (bool)ViewState["NewModeKey"]; }
            set { ViewState["NewModeKey"] = value; }
        }

        #region Url
        private const string RawUrl = "ManageDevice.aspx";

        

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                NewMode = false;
                DataBind();
            }
        }
    

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManageDevice_DataBinding);
            grdDevices.ItemDataBound += new DataGridItemEventHandler(grdDevices_ItemDataBound);
            grdDevices.EditCommand += new DataGridCommandEventHandler(grdDevices_EditCommand);
            grdDevices.DeleteCommand += new DataGridCommandEventHandler(grdDevices_DeleteCommand);
            grdDevices.UpdateCommand += new DataGridCommandEventHandler(grdDevices_UpdateCommand);
            grdDevices.CancelCommand += new DataGridCommandEventHandler(grdDevices_CancelCommand);
            grdDevices.ItemCreated += new DataGridItemEventHandler(grdDevices_ItemCreated);
            btnNew.Click += new EventHandler(btnNew_Click);
            btnDeviceSearch.Click += new EventHandler(btnDeviceSearch_Click);
        }
        #endregion

        private void ManageDevice_DataBinding(object sender, EventArgs e) {
            ratings = new ArrayList(BCDevice.GetRatingList());
            categories = new ArrayList(BCCategory.GetDeviceCategories());

            ArrayList devices = new ArrayList();
        //    int brandId = ManufactureFilter;
            DeviceBrand devBrand = null;
         //   if (brandId > 0) {
          //      string brandName = cboManufacturerFilter.SelectedItem.Text;
         //       devBrand = new DeviceBrand(brandId, brandName);
         //   }
            devices = new ArrayList(BCDevice.GetDeviceList(devBrand, DeviceType.MP3Player, null));
            
            if (NewMode) {
                devices.Insert(0, BCDevice.NewDevice());
                grdDevices.EditItemIndex = 0;
            } else if (grdDevices.EditItemIndex >= 0) {
                int deviceId = (int)grdDevices.DataKeys[grdDevices.EditItemIndex];
                DeviceBase device = new DeviceBase(deviceId);
                int index = devices.IndexOf(device);
                grdDevices.EditItemIndex = index;
            }
            grdDevices.DataSource = devices;
            grdDevices.DataKeyField = Device.IdField;
        }

        private void grdDevices_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void ItemDataBound(DataGridItem item) {
            Device device = (Device)item.DataItem;
            Label lblModel = (Label)item.FindControl(LblModelId);
            lblModel.Text = HttpUtility.HtmlEncode(device.Model);

            Image imgIcon = (Image)item.FindControl(ImgIconId);

            string imageUrl = IconHandler.ImageUrl(device, false).AbsoluteUri;
            imgIcon.ImageUrl = imageUrl;

            HyperLink hypIconView = (HyperLink)item.FindControl(HypIconViewId);
            hypIconView.NavigateUrl = imageUrl;
            hypIconView.Target = HtmlHelper.Blank;

            Image imgBigIcon = (Image)item.FindControl(ImgBigIconId);

            string imageBigUrl = IconHandler.ImageUrl(device, true).AbsoluteUri;
            imgBigIcon.ImageUrl = imageBigUrl;

            HyperLink hypIconBigView = (HyperLink)item.FindControl(HypIconBigViewId);
            hypIconBigView.NavigateUrl = imageBigUrl;
            hypIconBigView.Target = HtmlHelper.Blank;

            Label lblCategory = (Label)item.FindControl(LblCategoryId);
            lblCategory.Text = HttpUtility.HtmlEncode(device.Category.ToString(CultureInfo.InvariantCulture));

            Label lblRating = (Label)item.FindControl(LblRatingId);
            lblRating.Text = HttpUtility.HtmlEncode(device.Rating.ToString(CultureInfo.InvariantCulture));

            ImageButton btnDelete = (ImageButton)item.FindControl(BtnDeleteId);
            string warning = "Do you want to remove this device?";
            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", warning);
        }

        private void EditItemDataBound(DataGridItem item) {
            Device device = (Device)item.DataItem;
            TextBox txtModel = (TextBox)item.FindControl(TxtModelId);
            txtModel.Text = device.Model;
            JavaScriptHelper.RegisterFocusScript(this, txtModel.ClientID);

            DropDownList cboRating = (DropDownList)item.FindControl(CboRatingId);
            cboRating.DataSource = ratings;
            cboRating.DataBind();
            cboRating.SelectedIndex = device.Rating;

            DropDownList cboCategory = (DropDownList)item.FindControl(CboCategoryId);
            cboCategory.DataSource = categories;
            cboCategory.DataValueField = Category.IdField;
            cboCategory.DataTextField = Category.NameField;
            cboCategory.DataBind();

            int categoryIndex = -1;
            for (int i = 0; i < categories.Count; i++) {
                if (((Category)categories[i]).Id == device.DeviceTypeId) {
                    categoryIndex = i;
                    break;
                }
            }
            if (categoryIndex < 0) {
                cboCategory.Items.Insert(0, new ListItem("SELECT", SelectValue));
                cboCategory.SelectedIndex = 0;
            } else {
                cboCategory.SelectedIndex = categoryIndex;
            }
            RequiredFieldValidator vldCategoryRequired = (RequiredFieldValidator)item.FindControl(VldCategoryRequiredId);
            vldCategoryRequired.InitialValue = SelectValue;
        }

        private void grdDevices_EditCommand(object source, DataGridCommandEventArgs e) {
            grdDevices.EditItemIndex = e.Item.ItemIndex;
            NewMode = false;
            DataBind();
        }

        private void grdDevices_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdDevices.EditItemIndex = -1;
            int deviceId = (int)grdDevices.DataKeys[e.Item.ItemIndex];
            DeviceBase device = new DeviceBase(deviceId);
            try {
                BCDevice.Delete(device);
                Response.Redirect(Url().PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewMode = false;
            DataBind();
        }

        private void grdDevices_UpdateCommand(object source, DataGridCommandEventArgs e) {
            Page.Validate();
            if (!IsValid) return;

            Device device = BCDevice.NewDevice();
            
            DropDownList cboCategory = (DropDownList)e.Item.FindControl(CboCategoryId);
            device.DeviceTypeId = int.Parse(cboCategory.SelectedValue, CultureInfo.InvariantCulture.NumberFormat);

            device.Id = (int)grdDevices.DataKeys[e.Item.ItemIndex];

            TextBox txtModel = (TextBox)e.Item.FindControl(TxtModelId);
            device.Model = txtModel.Text.Trim();

            HtmlInputFile ctlIconFile = (HtmlInputFile)e.Item.FindControl(CtlIconFileId);
            Stream icon = ctlIconFile.PostedFile.InputStream;

            HtmlInputFile ctlBigIconFile = (HtmlInputFile)e.Item.FindControl(CtlBigIconFileId);
            Stream bigIcon = ctlBigIconFile.PostedFile.InputStream;

            DropDownList cboRating = (DropDownList)e.Item.FindControl(CboRatingId);
            device.Rating = int.Parse(cboRating.SelectedValue, CultureInfo.InvariantCulture.NumberFormat);

            try {
                BCDevice.Save(device, icon, bigIcon);
                Response.Redirect(Url().PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }

            grdDevices.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdDevices_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdDevices.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewMode = true;
            DataBind();
        }

        private void btnDeviceSearch_Click(object sender, EventArgs e) {
            grdDevices.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdDevices_ItemCreated(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            if(item.ItemType != ListItemType.EditItem)
                return;

            GridTransformationHelper.EditItemCreationForm(item, grdDevices.Columns, NumberOfColumns);
        }
    }
}