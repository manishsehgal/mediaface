<%@ Register TagPrefix="dol" NameSpace="Neato.Dol.WebAdmin.Controls" Assembly="Neato.Dol.WebAdmin" %>
<%@ Page language="c#" Codebehind="ManageMP3PlayerPapers.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebAdmin.ManageMP3PlayerPapers" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
    <HEAD>
        <title>Manage Devices Papers</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <link type="text/css" rel="stylesheet" href="css/admin.css">
    </HEAD>
    <body MS_POSITIONING="GridLayout">
        <form id="Form1" method="post" runat="server">
            <h3 align="center">Manage Devices Papers</h3>
            <table border="0" cellpadding="4" cellspacing="0">
                <tr>
                    <td>Paper Brand :</td>
                    <td>
                        <asp:DropDownList ID="cboPaperBrandFilter" Runat="server" Width="250" />
                    </td>
                    <td>Paper State :</td>
                    <td>
                        <asp:DropDownList ID="cboPaperStateFilter" Runat="server" Width="250" />
                    </td>
                </tr>
                <tr valign="top">
                    <td valign="middle">Paper Name :<br>
                        (substring)</td>
                    <td>
                        <asp:TextBox ID="txtPaperName" Runat="server" Width="250" />
                    </td>
                    <td>Category :</td>
                    <td>
                        <asp:DropDownList ID="cboCategoryFilter" Runat="server" Width="250" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td align="right">
                        <asp:Button runat="server" ID="btnSearch" Text="Search" />
                    </td>
                </tr>
            </table>
            <br>
            <asp:Button ID="btnNew" Runat="server" Text="New Paper" CausesValidation="False" />
            <br>
            <br>
            <asp:DataGrid Runat="server" ID="grdPapers" AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateColumn HeaderText="Icon">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:HyperLink ID="hypIconView" Runat="server">
                                <asp:Image ID="imgIcon" Runat="server" BorderStyle="Solid" BorderWidth="1" BorderColor="#d4d0c8"
                                    CssClass="imageIcon" />
                            </asp:HyperLink>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <input ID="ctlIconFile" Runat="server" type="file" size="20" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Paper Name">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                        <ItemStyle Wrap="True" Width="100px" />
                        <ItemTemplate>
                            <asp:Label ID="lblName" Runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtName" Runat="server" MaxLength="50" style="width:100%" />
                            <asp:RequiredFieldValidator ID="vldNameRequired" Runat="server" ControlToValidate="txtName" Display="Dynamic"
                                ErrorMessage="Enter the Paper name" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Paper Brand">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                        <ItemStyle Wrap="True" Width="90px" />
                        <ItemTemplate>
                            <asp:Label ID="lblPaperBrand" Runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="cboPaperBrand" Runat="server" style="width:100%" />
                            <asp:RequiredFieldValidator ID="vldPaperBrandRequired" Runat="server" ControlToValidate="cboPaperBrand" Display="Dynamic"
                                InitialValue="-2" ErrorMessage="Choose Paper brand" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Devices">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                        <ItemStyle Wrap="True" Width="290px" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtDevices" Runat="server" TextMode="MultiLine" BorderWidth="0" BorderStyle="None"
                                ReadOnly="True" Width="100%" Height="100px" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <dol:Selector id="ctlDevices" runat="server" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Labels">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                        <ItemStyle Width="290px" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtFaces" Runat="server" TextMode="MultiLine" BorderWidth="0" BorderStyle="None"
                                ReadOnly="True" Width="100%" Height="100px" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <input ID="ctlPaperXmlFile" Runat="server" type="file" size="20" />
                            <asp:RequiredFieldValidator ID="vldPaperXmlFileRequired" ControlToValidate="ctlPaperXmlFile" Display="Static"
                                ErrorMessage="* required" runat="server" />
                            <br />
                            <asp:CustomValidator ID="vldPaperXml" Runat="server" OnServerValidate="vldPaperXml_ServerValidate" EnableClientScript="False"
                                Display="Dynamic" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Paper State">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label ID="lblPaperState" Runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="cboPaperState" Runat="server" style="width:100%" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Paper Type">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label ID="lblPaperType" Runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Orientation">
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label ID="lblOrientation" Runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="cboOrientation" Runat="server" style="width:100%" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Paper Access">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label ID="lblPaperAccess" Runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="cboPaperAccess" Runat="server" style="width:100%" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Action">
                        <ItemTemplate>
                            <asp:Button Runat="server" ID="btnEdit" CommandName="Edit" CausesValidation="False" Text="Edit"
                                Tooltip="Edit paper" />
                            <br>
                            <asp:Button Runat="server" ID="btnDelete" CommandName="Delete" CausesValidation="False" Text="Remove"
                                Tooltip="Remove paper" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button Runat="server" ID="btnUpdate" Text="Save" CommandName="Update" CausesValidation="True"
                                Tooltip="Save paper" />
                            <br>
                            <asp:Button Runat="server" ID="btnCancel" CommandName="Cancel" Text="Cancel" CausesValidation="False"
                                Tooltip="Cancel" />
                        </EditItemTemplate>
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                        <ItemStyle Wrap="False" Width="70px" />
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </form>
    </body>
</HTML>
