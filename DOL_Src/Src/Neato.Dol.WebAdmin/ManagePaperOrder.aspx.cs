using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin {
    public class ManagePaperOrder : Page {
        protected DataGrid grdPapers;
        protected DropDownList cboPaperMetricFilter;
        protected DropDownList cboPaperBrandFilter;
        protected DropDownList cboPaperCategoryFilter;

        private const string LblNameKey = "lblName";
        private const string LblPaperStateKey = "lblPaperState";
        private const string LblPaperBrandKey = "lblPaperBrand";
        private const string LblPaperMetricKey = "lblPaperMetric";

        private const string AllValue = "0";
        private const string PaperBrandFilterKey = "PaperBrandFilterKey";
        private const string PaperMetricFilterKey = "PaperMetricFilterKey";
        private const string PaperCategoryFilterKey = "PaperCategoryFilterKey";

        #region Url
        private const string RawUrl = "ManagePaperOrder.aspx";
        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private PageFilter CurrentFilter {
            get { return (PageFilter)ViewState["CurrentFilter"]; }
            set { ViewState["CurrentFilter"] = value; }
        }

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                InitFilters();
                DataBind();
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManagePaperOrder_DataBinding);
            this.PreRender += new EventHandler(ManagePaperOrder_PreRender);
            this.cboPaperBrandFilter.DataBinding += new EventHandler(cboPaperBrandFilter_DataBinding);
            this.cboPaperMetricFilter.DataBinding += new EventHandler(cboPaperMetricFilter_DataBinding);
            this.cboPaperCategoryFilter.DataBinding += new EventHandler(cboPaperCategoryFilter_DataBinding);
            this.grdPapers.DataBinding += new EventHandler(grdPapers_DataBinding);
            this.grdPapers.ItemDataBound += new DataGridItemEventHandler(grdPapers_ItemDataBound);
            this.grdPapers.ItemCommand += new DataGridCommandEventHandler(grdPapers_ItemCommand);

            this.cboPaperBrandFilter.SelectedIndexChanged += new EventHandler(cboPaperBrandFilter_SelectedIndexChanged);
            this.cboPaperMetricFilter.SelectedIndexChanged += new EventHandler(cboPaperMetricFilter_SelectedIndexChanged);
            this.cboPaperCategoryFilter.SelectedIndexChanged += new EventHandler(cboPaperCategoryFilter_SelectedIndexChanged);
        }

        #endregion

        private void InitFilters() {
            CurrentFilter = new PageFilter();
            
            CurrentFilter[PaperBrandFilterKey] = Request.QueryString[PaperBrandFilterKey];
            if (CurrentFilter[PaperBrandFilterKey] == null)
                CurrentFilter[PaperBrandFilterKey] = AllValue;

            CurrentFilter[PaperMetricFilterKey] = Request.QueryString[PaperMetricFilterKey];
            if (CurrentFilter[PaperMetricFilterKey] == null)
                CurrentFilter[PaperMetricFilterKey] = AllValue;

            CurrentFilter[PaperCategoryFilterKey] = Request.QueryString[PaperCategoryFilterKey];
            if (CurrentFilter[PaperCategoryFilterKey] == null)
                CurrentFilter[PaperCategoryFilterKey] = AllValue;
        }

        private ListItem[] PaperBrandList(bool insertFirst) {
            ArrayList list = new ArrayList();
            if (insertFirst)
                list.Add(new ListItem("All", AllValue));
            foreach(PaperBrand brand in BCPaperBrand.GetPaperBrandList()) {
                ListItem item = new ListItem(brand.Name, brand.Id.ToString());
                list.Add(item);
            }
            return (ListItem[]) list.ToArray(typeof (ListItem));
        }

        private ListItem[] PaperMetricList(bool insertFirst) {
            ArrayList list = new ArrayList();
            if (insertFirst)
                list.Add(new ListItem("All", AllValue));
            foreach(PaperMetric paperMetric in BCPaperMetric.Enum()) {
                ListItem item = new ListItem(paperMetric.Name, paperMetric.Id.ToString());
                list.Add(item);
            }
            return (ListItem[]) list.ToArray(typeof (ListItem));
        }

        private ListItem[] PaperCategoryList(bool insertFirst) {
            ArrayList list = new ArrayList();
            if (insertFirst)
                list.Add(new ListItem("All", AllValue));
            foreach(Category category in BCCategory.GetCategoryList()) {
                ListItem item = new ListItem(category.Name, category.Id.ToString());
                list.Add(item);
            }
            return (ListItem[]) list.ToArray(typeof (ListItem));
        }

        private void ManagePaperOrder_DataBinding(object sender, EventArgs e) {
        }

        private void grdPapers_DataBinding(object sender, EventArgs e) {
            PaperBrandBase brand = CurrentFilter[PaperBrandFilterKey] == AllValue ? null : new PaperBrandBase(int.Parse(CurrentFilter[PaperBrandFilterKey]));
            PaperMetric metric = CurrentFilter[PaperMetricFilterKey] == AllValue ? null : new PaperMetric(int.Parse(CurrentFilter[PaperMetricFilterKey]));
            Category category = CurrentFilter[PaperCategoryFilterKey] == AllValue ? null : new Category(int.Parse(CurrentFilter[PaperCategoryFilterKey]));

            grdPapers.DataSource = BCPaper.GetPapers(brand, category, null, null, null, null, "ait", null, metric, -1, -1);
            grdPapers.DataKeyField = Paper.IdField;
        }

        private void cboPaperBrandFilter_DataBinding(object sender, EventArgs e) {
            cboPaperBrandFilter.Items.Clear();
            foreach(ListItem item in PaperBrandList(!IsPostBack))
                cboPaperBrandFilter.Items.Add(item);
            cboPaperBrandFilter.SelectedValue = CurrentFilter[PaperBrandFilterKey];
        }

        private void cboPaperMetricFilter_DataBinding(object sender, EventArgs e) {
            cboPaperMetricFilter.Items.Clear();
            foreach(ListItem item in PaperMetricList(!IsPostBack))
                cboPaperMetricFilter.Items.Add(item);
            cboPaperMetricFilter.SelectedValue = CurrentFilter[PaperMetricFilterKey];
        }

        private void cboPaperCategoryFilter_DataBinding(object sender, EventArgs e) {
            cboPaperCategoryFilter.Items.Clear();
            foreach(ListItem item in PaperCategoryList(!IsPostBack))
                cboPaperCategoryFilter.Items.Add(item);
            cboPaperCategoryFilter.SelectedValue = CurrentFilter[PaperCategoryFilterKey];
        }

        private void ManagePaperOrder_PreRender(object sender, EventArgs e) {
            grdPapers.Visible = grdPapers.Items.Count != 0;
        }

        private PaperMetric[] metrics;
        private PaperBrand[]  brands;
        
        private string GetMetricName(int metricId) {
            foreach (PaperMetric metric in metrics)
                if (metric.Id == metricId)
                    return metric.Name;
            return "No metric";
        }

        private string GetBrandName(int brandId) {
            foreach (PaperBrand brand in brands)
                if (brand.Id == brandId)
                    return brand.Name;
            return "No brand";
        }

        private void grdPapers_ItemDataBound(object sender, DataGridItemEventArgs e) {
            metrics = BCPaperMetric.Enum();
            brands  = BCPaperBrand.GetPaperBrandList();
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
            }
        }

        private void ItemDataBound(DataGridItem item) {
            Paper paper = (Paper) item.DataItem;
            Label lblName = (Label) item.FindControl(LblNameKey);
            Label lblPaperState = (Label) item.FindControl(LblPaperStateKey);
            Label lblPaperBrand = (Label) item.FindControl(LblPaperBrandKey);
            Label lblPaperMetric = (Label) item.FindControl(LblPaperMetricKey);
            
            lblName.Text = paper.Name;
            string stateText = "Active";
            if (paper.PaperState == "i") {
                stateText = "Inactive";
            }
            else if (paper.PaperState == "t") {
                stateText = "Test Mode";
            }
            lblPaperBrand.Text = GetBrandName(paper.Brand.Id);
            lblPaperMetric.Text = GetMetricName(paper.MetricId);
            lblPaperState.Text = stateText;
        }

        private void cboPaperBrandFilter_SelectedIndexChanged(object sender, EventArgs e) {
            CurrentFilter[PaperBrandFilterKey] = cboPaperBrandFilter.SelectedValue;
            grdPapers.DataBind();
        }

        private void cboPaperMetricFilter_SelectedIndexChanged(object sender, EventArgs e) {
            CurrentFilter[PaperMetricFilterKey] = cboPaperMetricFilter.SelectedValue;
            grdPapers.DataBind();
        }

        private void cboPaperCategoryFilter_SelectedIndexChanged(object sender, EventArgs e) {
            CurrentFilter[PaperCategoryFilterKey] = cboPaperCategoryFilter.SelectedValue;
            grdPapers.DataBind();
        }

        private void grdPapers_ItemCommand(object source, DataGridCommandEventArgs e) {
            int itemIndex = e.Item.ItemIndex;
            int nextItemIndex = -1;
            switch (e.CommandName) {
                case "Up":
                    nextItemIndex = itemIndex - 1;
                    break;
                case "Down":
                    nextItemIndex = itemIndex + 1;
                    if (nextItemIndex == grdPapers.Items.Count) {
                        nextItemIndex = -1;
                    }
                    break;
            }
            if (nextItemIndex != -1) {
                int paperId = (int) grdPapers.DataKeys[itemIndex];
                int nextPaperId = (int) grdPapers.DataKeys[nextItemIndex];

                Paper paper = BCPaper.GetPaper(new PaperBase(paperId));
                Paper nextPaper = BCPaper.GetPaper(new PaperBase(nextPaperId));
                int order = paper.Order;
                paper.Order = nextPaper.Order;
                nextPaper.Order = order;

                BCPaper.Save(paper);
                BCPaper.Save(nextPaper);

                grdPapers.DataBind();
            }
        }
    }
}