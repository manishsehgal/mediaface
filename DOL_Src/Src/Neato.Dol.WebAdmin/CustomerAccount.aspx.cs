using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Tracking;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebAdmin {
    public class CustomerAccount : Page {
        #region Constants
        private const string CustomerIdKey = "customerId";
        #endregion

        protected TextBox txtFirstName;
        protected RequiredFieldValidator vldFirstName;
        protected TextBox txtLastName;
        protected RequiredFieldValidator vldLastName;
        protected TextBox txtEmailAddress;
        protected TextBox txtPassword;
        protected RequiredFieldValidator vldEmailAddress;
        protected RequiredFieldValidator vldPassword;
        protected CustomValidator vldValidEmail;
        protected CustomValidator vldUniqueEmail;
        protected DropDownList cboEmailOptions;
        protected CheckBox chkReceiveInfo;
        protected CheckBox chkActive;
        protected Button btnResetPassword;
        protected Button btnSave;
        protected Button btnClose;
        protected Label lblMessage;
        protected DropDownList cboAccountType;

        private Customer customer {
            get { return (Customer)(ViewState[CustomerIdKey]); }
            set { ViewState[CustomerIdKey] = value; }
        }

        #region Url
        private const string RawUrl = "CustomerAccount.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri Url(int customerId) {
            return UrlHelper.BuildUrl(RawUrl, CustomerIdKey, customerId);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(CustomerAccount_DataBinding);
        
            this.btnSave.Click += new EventHandler(btnSave_Click);
            this.btnClose.Click += new EventHandler(btnClose_Click);
            this.btnResetPassword.Click += new EventHandler(btnResetPassword_Click);
            this.vldUniqueEmail.ServerValidate += new ServerValidateEventHandler(vldUniqueEmail_ServerValidate);
            this.vldValidEmail.ServerValidate += new ServerValidateEventHandler(vldValidEmail_ServerValidate);
            this.cboAccountType.DataBinding += new EventHandler(cboAccountType_DataBinding);
            this.cboAccountType.PreRender += new EventHandler(cboAccountType_PreRender);
        }
        #endregion

        private void CustomerAccount_DataBinding(object sender, EventArgs e) {
            cboEmailOptions.Items.Clear();
            cboEmailOptions.Items.Add(EmailOptions.Text.ToString());
            cboEmailOptions.Items.Add(EmailOptions.Html.ToString());

            int customerId = Convert.ToInt32(Request.QueryString[CustomerIdKey]);
            customer = BCCustomer.GetCustomerById(customerId);
            if (customer == null)
                customer = new Customer();

            txtFirstName.Text = customer.FirstName;
            txtLastName.Text = customer.LastName;
            txtEmailAddress.Text = customer.Email;
            txtPassword.Text = customer.Password;
            cboEmailOptions.SelectedValue = customer.EmailOptions.ToString();
            chkReceiveInfo.Checked = customer.ReceiveInfo;
            chkActive.Checked = customer.Active;
        }

        private void btnSave_Click(object sender, EventArgs e) {
            lblMessage.Visible = false;
            if (!IsValid) {
                return;
            }

            customer.FirstName = txtFirstName.Text.Trim();
            customer.LastName = txtLastName.Text.Trim();
            customer.Email = txtEmailAddress.Text.Trim();
            customer.Password = txtPassword.Text.Trim();
            customer.Active = chkActive.Checked;
            if (cboEmailOptions.SelectedValue == EmailOptions.Html.ToString()) {
                customer.EmailOptions = EmailOptions.Html;
            } else {
                customer.EmailOptions = EmailOptions.Text;
            }
            customer.ReceiveInfo = chkReceiveInfo.Checked;
            customer.RetailerId = 1;
            customer.Group = (CustomerGroup) Enum.Parse(typeof (CustomerGroup), cboAccountType.SelectedValue);

            try {
                if (customer.CustomerId != 0) {
                    BCCustomer.UpdateCustomer(customer);
                    lblMessage.Text = "Account was successfully updated!";
                } else {
                    BCCustomer.InsertCustomer(customer, null);
                    BCTracking.Add(UserAction.Registration, Request.UserHostAddress);
                    lblMessage.Text = "Account was successfully added!";
                }
            } catch (BusinessException ex) {
                lblMessage.Text = ex.Message;
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            lblMessage.Visible = true;
        }

        private void btnClose_Click(object sender, EventArgs e) {
            Response.Redirect(CustomerList.Url().PathAndQuery);
        }

        private void btnResetPassword_Click(object sender, EventArgs e) {
            lblMessage.Visible = false;
            if (!IsValid) {
                return;
            }

            try {
                string email = txtEmailAddress.Text.Trim();
                BCMail.SendForgotPasswordMail(Configuration.PublicDesignerSiteUrl,
                    WebDesigner.ForgotPassword.PageName(),
                    email.Trim(), BCDynamicLink.DefaultCulture);
                lblMessage.Text = "E-Mail was successfully sent!";
            } catch (BaseBusinessException ex) {
                lblMessage.Text = ex.Message;
            }
            lblMessage.Visible = true;
        }

        private void vldUniqueEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            Customer found = BCCustomer.GetCustomerByLogin(txtEmailAddress.Text.Trim(), true);
            if (customer.CustomerId != 0) {
                args.IsValid = found == null || found.CustomerId == customer.CustomerId;
            } else {
                args.IsValid = found == null;
            }

        }

        private void vldValidEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = FormatValidator.EnsureValidEmail(txtEmailAddress.Text.Trim());
        }

        private void cboAccountType_DataBinding(object sender, EventArgs e) {
            cboAccountType.Items.Clear();
            cboAccountType.Items.Add(new ListItem(CustomerGroup.MFO.ToString(), ((int)CustomerGroup.MFO).ToString()));
            cboAccountType.Items.Add(new ListItem(CustomerGroup.MFOPE.ToString(), ((int)CustomerGroup.MFOPE).ToString()));
        }

        private void cboAccountType_PreRender(object sender, EventArgs e) {
            cboAccountType.SelectedValue = ((int)customer.Group).ToString();
        }
    }
}