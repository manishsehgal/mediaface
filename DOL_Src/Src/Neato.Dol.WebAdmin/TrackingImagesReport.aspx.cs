using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.WebAdmin.Controls;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebAdmin
{
	public class TrackingImagesReport : System.Web.UI.Page
	{
        private const string ImageLibraryIconUrl = "ImageLibrary.aspx?mode=icon&id=";
        private const string ImageLibraryDirectoryUrl = "ImageLibraryEdit.aspx?FolderId=";
        
        #region Url
        private const string RawUrl = "TrackingImagesReport.aspx";

        public static Uri Url() 
        {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private struct ImageTrackingReportTableItem 
        {
            public int imageId;
            public ArrayList foldersList;
            public int count;
        }
        private struct FolderItem {
            public int folderId;
            public string folderName;
        }
        protected Button btnSubmit;
        protected Label lblTitle;
        protected Label lblStatus;
        protected DateInterval dtiDates;
        protected DataGrid grdReport; 
		private void Page_Load(object sender, System.EventArgs e)
		{
            if (!IsPostBack) 
            {
                DataBind();
            }	
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{

			InitializeComponent();
			base.OnInit(e);
		}
		

		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding +=new EventHandler(TrackingImagesReport_DataBinding);
            btnSubmit.Click +=new EventHandler(btnSubmit_Click);
		}
        #endregion
        private void TrackingImagesReport_DataBinding(object sender, EventArgs e)
        {
            lblTitle.Text =  "What images have been chosen";
        }
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            grdReport.Visible = false;
            if (IsValid) {
                DataSet data = BCTrackingImages.GetImagesListByDate(dtiDates.StartDate, dtiDates.EndDate);
                bindGrid(data);
                DataBind();
            }
        }
        
        private void bindGrid(DataSet data) {
            ArrayList grdDataList = new ArrayList();
            foreach(DataRow itemRow in data.Tables[0].Rows) {
                ImageTrackingReportTableItem item = new ImageTrackingReportTableItem();
                item.imageId = (int)itemRow["ImageId"];
                item.count = (int)itemRow["Count"];
                item.foldersList = new ArrayList();
                foreach(DataRow folderRow in data.Tables[1].Rows) {
                    if((int)folderRow["ImageId"] == item.imageId) {
                        FolderItem folderItem = new FolderItem();
                        folderItem.folderId = (int)folderRow["FolderId"];
                        folderItem.folderName = (string)folderRow["Caption"];
                        item.foldersList.Add(folderItem);
                    }
                }
                grdDataList.Add(item);
            }
            DataTable grdData = getTable(grdDataList);
            if(grdData.Rows.Count < 1) {
                lblStatus.Text = "There are no results matching your criteria";
                grdReport.DataSource = null;
            } else {
                lblStatus.Text = "";
                grdReport.Visible = true;
                grdReport.DataSource = grdData;
            }
        }
        private DataTable getTable(ArrayList arr) {
            DataTable result = new DataTable();
            result.Columns.Add("Image",typeof(string));
            
            result.Columns.Add("Count",typeof(string));
            
            foreach(ImageTrackingReportTableItem item in arr) {
               DataRow workRow = result.NewRow() ;
               workRow["Count"]=item.count.ToString();
               workRow["Image"]=renderImageCell(item);
               result.Rows.Add(workRow);
            }
            return result;
        }
        private string renderImageCell(ImageTrackingReportTableItem item) {
            string res = "";
            res += "<table><tr><td><img src=\""+ImageLibraryHandler.UrlGetIcon(item.imageId).PathAndQuery + "\"></td><td>";
            res += "<table border=0 cellspacing=2 cellpadding=2><tr><td rowspan=\""+(item.foldersList.Count+1).ToString();
            res += "\" valign=top>Directories: </td></tr>";
            foreach(FolderItem folder in item.foldersList) 
                res+="<tr><td><a href=\""+ImageLibraryContent.Url(folder.folderId).PathAndQuery+"\">"+folder.folderName+"</a></td></tr>";
            res+="</table></td></tr></table>";          
            return res;
        }


    }
}
