using System;
using System.Data;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class CustomerTest {
        private const string initialFirstName = "Initial First Name - (Test)";
        private const string initialLastName = "Initial Last Name - (Test)";
        private const string updatedName = "Updated - (Test)";
        private const string initialEmail = "Email - (Test)";

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void CreateUpdateDeleteTest() {
            Customer customer = CreateCustomer(initialFirstName, initialLastName, initialEmail);
            Assert.IsTrue(customer.CustomerId > 0, "Customer id is not changed");

            customer = ReadCustomer(initialEmail);
            Assert.IsNotNull(customer, "Cannot read newly created reminder");
            Assert.AreEqual(initialFirstName, customer.FirstName, "Expected initial first name");

            UpdateCustomer(initialEmail, updatedName);
            customer = ReadCustomer(initialEmail);
            Assert.IsNotNull(customer, "Cannot read updated customer");
            Assert.AreEqual(updatedName, customer.FirstName, "Expected updated first name");

            DOCustomer.DeleteCustomer(customer.CustomerId);
            customer = ReadCustomer(initialEmail);
            Assert.IsNull(customer, "Read deleted customer");
        }

        [Test]
        [ExpectedException(typeof(BusinessException))]
        public void CustomerDuplicateEMailTest() {
            Customer customer = CreateCustomer(initialFirstName, initialLastName, initialEmail);
            customer.FirstName = updatedName;
            customer.LastName = updatedName;
            customer.Group = CustomerGroup.MFO;
            BCCustomer.InsertCustomer(customer, null);

            Assert.Fail("Customer with non unique e-mail added");
        }

        [Test]
        public void FieldLengthTest() {
            string longString = new string('a', 100);
            Customer customer = CreateCustomer(longString, longString, longString);
            customer = ReadCustomer(longString);
            Assert.AreEqual(customer.FirstName, longString);
            Assert.AreEqual(customer.LastName, longString);
            Assert.AreEqual(customer.Email, longString);
        }

        [Test]
        public void SearchTest() {
            DateTime startDate = DateTime.Now.AddYears(-100);
            DateTime endDate = DateTime.Now.AddYears(+100);
            Customer[] data = DOCustomer.CustomerSearch("", "", "", startDate, endDate, false);
            Assert.IsNotNull(data);
            foreach (Customer customer in data) {
                DOCustomer.DeleteCustomer(customer.CustomerId);
            }

            string[] line1 = {"First Name 1", "Last Name 1", "Email 1"};
            string[] line2 = {"First Name 2", "Last Name 2", "Email 2"};
            string[] line3 = {"First Name 3", "Last Name 2", "Email 3"};
            string[][] names = {line1, line2, line3};

            foreach (string[] line in names) {
                CreateCustomer(line[0], line[1], line[2]);
            }

            data = DOCustomer.CustomerSearch("", "", "", startDate, endDate, false);
            Assert.AreEqual(data.Length, 3);

            data = DOCustomer.CustomerSearch(names[0][0], "", "", startDate, endDate, false);
            Assert.AreEqual(data.Length, 1);

            data = DOCustomer.CustomerSearch(names[0][0], names[0][1], "", startDate, endDate, false);
            Assert.AreEqual(data.Length, 1);

            data = DOCustomer.CustomerSearch(names[0][0], names[0][1], names[0][2], startDate, endDate, false);
            Assert.AreEqual(data.Length, 1);

            data = DOCustomer.CustomerSearch(names[1][0], "", "", startDate, endDate, false);
            Assert.AreEqual(data.Length, 1);

            data = DOCustomer.CustomerSearch(names[1][0], names[1][1], "", startDate, endDate, false);
            Assert.AreEqual(data.Length, 2);

            data = DOCustomer.CustomerSearch(names[1][0], names[1][1], names[1][2], startDate, endDate, false);
            Assert.AreEqual(data.Length, 2);

            data = DOCustomer.CustomerSearch("", names[2][1], "", startDate, endDate, false);
            Assert.AreEqual(data.Length, 2);

            data = DOCustomer.CustomerSearch(names[0][0], names[1][1], names[2][2], startDate, endDate, false);
            Assert.AreEqual(data.Length, 3);
        }

        #region Support
        private static Customer CreateCustomer(string firstName, string lastName, string email) {
            Customer customer = new Customer();
            customer.FirstName = firstName;
            customer.LastName = lastName;
            customer.Email = email;
            customer.RetailerId = BCRetailer.defaultId;
            customer.Group = CustomerGroup.MFO;
            BCCustomer.InsertCustomer(customer, null);
            return customer;
        }

        private static Customer ReadCustomer(string email) {
            return DOCustomer.GetCustomerByLogin(email, false);
        }

        private static void UpdateCustomer(string email, string firstName) {
            Customer customer = DOCustomer.GetCustomerByLogin(email, false);
            customer.FirstName = firstName;
            DOCustomer.UpdateCustomer(customer);
        }

        #endregion
    }
}