using System;
using System.Data;
using System.Drawing;
using System.Xml;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class BCFaceTest {
        public BCFaceTest() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void NewFaceTest() {
            Face face = BCFace.NewFace();
            Assert.IsNotNull(face);
            Assert.IsNull(face.Contour);
            Assert.AreEqual(0, face.Id);
            Assert.AreEqual(0, face.Position.X);
            Assert.AreEqual(0, face.Position.Y);
        }

        [Test]
        public void SaveOneFaceTest() {
            XmlDocument doc = new XmlDocument();
            doc.InnerXml = "<Test>Doc</Test>";

            Guid guid = Guid.NewGuid();
            Face face = BCFace.NewFace();
            face.Contour = doc.FirstChild;
            face.Position = new PointF(100, 100);
            face.Guid = guid;
            face.AddName("", "test");

            BCFace.Save(face);

            Face dbFace = DOFace.GetFace(face);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.IsTrue(dbFace.Id > 0);
            Assert.AreEqual(doc.FirstChild.OuterXml, dbFace.Contour.OuterXml);
            Assert.AreEqual(0, dbFace.Position.X);
            Assert.AreEqual(0, dbFace.Position.Y);
            Assert.AreEqual(guid, dbFace.Guid);

            doc.InnerXml = "<Test>Doc2</Test>";
            face.Contour = doc.FirstChild;

            BCFace.Save(face);

            dbFace = DOFace.GetFace(face);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.IsTrue(dbFace.Id > 0);
            Assert.AreEqual(doc.FirstChild.OuterXml, dbFace.Contour.OuterXml);
            Assert.AreEqual(0, dbFace.Position.X);
            Assert.AreEqual(0, dbFace.Position.Y);
        }

        [Test]
        public void SaveFacesTest() {
            XmlDocument doc = new XmlDocument();
            doc.InnerXml = "<Test>Doc</Test>";

            Face face1 = BCFace.NewFace();
            face1.Contour = doc.FirstChild;
            face1.Position = new PointF(100, 100);
            face1.AddName("", "face1");
            face1.Guid = Guid.NewGuid();

            Face face2 = BCFace.NewFace();
            face2.Contour = doc.FirstChild;
            face2.Position = new PointF(100, 100);
            face2.AddName("", "face2");
            face2.Guid = Guid.NewGuid();

            BCFace.Save(new Face[] {face1, face2});

            Face dbFace = DOFace.GetFace(face1);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.IsTrue(dbFace.Id > 0);
            Assert.AreEqual(doc.FirstChild.OuterXml, dbFace.Contour.OuterXml);
            Assert.AreEqual(0, dbFace.Position.X);
            Assert.AreEqual(0, dbFace.Position.Y);

            dbFace = DOFace.GetFace(face2);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.IsTrue(dbFace.Id > 0);
            Assert.AreEqual(doc.FirstChild.OuterXml, dbFace.Contour.OuterXml);
            Assert.AreEqual(0, dbFace.Position.X);
            Assert.AreEqual(0, dbFace.Position.Y);

            doc.InnerXml = "<Test>Doc2</Test>";
            face1.Contour = doc.FirstChild;
            face2.Contour = doc.FirstChild;

            BCFace.Save(new Face[] {face1, face2});

            dbFace = DOFace.GetFace(face1);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.IsTrue(dbFace.Id > 0);
            Assert.AreEqual(doc.FirstChild.OuterXml, dbFace.Contour.OuterXml);
            Assert.AreEqual(0, dbFace.Position.X);
            Assert.AreEqual(0, dbFace.Position.Y);

            dbFace = DOFace.GetFace(face2);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.IsTrue(dbFace.Id > 0);
            Assert.AreEqual(doc.FirstChild.OuterXml, dbFace.Contour.OuterXml);
            Assert.AreEqual(0, dbFace.Position.X);
            Assert.AreEqual(0, dbFace.Position.Y);
        }

        [Test]
        public void DeleteFaceTest() {
            PaperBrand paperBrand = BCPaperBrand.NewPaperBrand();
            paperBrand.Name = "test";
            DOPaperBrand.Add(paperBrand);
            paperBrand = DOPaperBrand.GetPaperBrand(paperBrand);
            Assert.IsNotNull(paperBrand);

            Paper paper = BCPaper.NewPaper();
            paper.Brand = paperBrand;
            paper.Name = "test";
            paper.Size = new SizeF(100, 100);
            DOPaper.Add(paper);

            Face face = BCFace.NewFace();
            face.Contour = null;
            face.Position = new PointF(90, 90);
            face.AddName(string.Empty, "test");
            DOFace.Add(face);

            DOFace.BindToPaper(face, paper);

            DeviceBrand deviceBrand = BCDeviceBrand.NewDeviceBrand();
            deviceBrand.Name = "test";
            DODeviceBrand.Add(deviceBrand);
            deviceBrand = DODeviceBrand.GetDeviceBrand(deviceBrand);
            Assert.IsNotNull(deviceBrand);

            Device device = BCDevice.NewDevice();
            device.Brand = deviceBrand;
            device.Model = "test";
            device.Rating = 8;
            device.DeviceTypeId = (int)DeviceType.CdDvdLabel;
            DODevice.Add(device);

            DOFace.BindToDevice(face, device);

            Face dbFace = DOFace.GetFace(face);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.IsTrue(dbFace.Id > 0);

            BCFace.Delete(face);

            dbFace = DOFace.GetFace(face);

            Assert.IsNull(dbFace);
        }

        [Test]
        public void GetFaceIdByGuidSuccessTest() {
            Face face = BCFace.NewFace();
            Guid guid = face.Guid;

            BCFace.Save(face);

            Assert.IsTrue(BCFace.GetFaceIdByGuid(guid) > 0);
        }

        [Test]
        [ExpectedException(typeof(BusinessException))]
        public void GetFaceIdByGuidFailedTest() {
            BCFace.GetFaceIdByGuid(new Guid());
        }

        [Test]
        public void FaceEnumTest() {
            foreach (Face face in BCFace.FaceEnum()) {
                BCFace.Delete(new FaceBase(face.Id));
            }

            Face newFace = BCFace.NewFace();
            newFace.AddName("", "A_Name");
            newFace.Guid = Guid.NewGuid();
            BCFace.Save(newFace);

            Face newFace2 = BCFace.NewFace();
            newFace2.AddName("", "B_Name");
            newFace2.Guid = Guid.NewGuid();
            BCFace.Save(newFace2);

            Face[] faces = BCFace.FaceEnum();

            Assert.AreEqual(2, faces.Length);
            Assert.AreEqual("A_Name", faces[0].GetName(""));
            Assert.AreEqual("B_Name", faces[1].GetName(""));
        }

        [Test]
        public void GetIconTest() {
            foreach (Face face in BCFace.FaceEnum()) {
                BCFace.Delete(new FaceBase(face.Id));
            }

            Face newFace = BCFace.NewFace();
            newFace.AddName("", "A_Name");
            BCFace.Save(newFace);

            Assert.AreEqual(0, BCFace.GetIcon(newFace).Length);
        }
    }
}