using System;
using System.Data;
using System.IO;

using Neato.Dol.Data;
using Neato.Dol.Data.Scheme;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class DOFileSystemTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void GetImageLibraryDataTest() {
            string imagePath = "..\\..\\ImageLibTestData\\MainForUTests";
            ImageLibraryData data = DOFileSystem.
                GetImageLibraryData(imagePath);

            Assert.AreEqual(3, data.Folder.Count);
            Assert.AreEqual(3, data.FolderLocalization.Count);
            Assert.AreEqual(4, data.FolderItem.Count);
            Assert.AreEqual(4, data.LibraryItem.Count);

            Assert.IsTrue(data.Folder[0].IsParentFolderIdNull());
            Assert.IsFalse(data.Folder[1].IsParentFolderIdNull());
            Assert.IsFalse(data.Folder[2].IsParentFolderIdNull());

            Assert.AreEqual(2, data.Folder[0].GetFolderRows().Length);

            Assert.AreEqual("MainForUTests", data.Folder[0].GetFolderLocalizationRows()[0].Caption);
            Assert.AreEqual("BusinessForUTests", data.Folder[1].GetFolderLocalizationRows()[0].Caption);
            Assert.AreEqual("GeneralForUTests", data.Folder[2].GetFolderLocalizationRows()[0].Caption);

            Assert.AreEqual(5212, data.Folder[0].
                GetFolderItemRows()[0].LibraryItemRow.Stream.Length);
            Assert.AreEqual(6648, data.Folder[0].
                GetFolderItemRows()[1].LibraryItemRow.Stream.Length);
            Assert.AreEqual(5212, data.Folder[1].
                GetFolderItemRows()[0].LibraryItemRow.Stream.Length);
            Assert.AreEqual(6648, data.Folder[2].
                GetFolderItemRows()[0].LibraryItemRow.Stream.Length);
        }

        [Test]
        public void CreateDirectoryTest() {
            string dirName = Guid.NewGuid().ToString("D");
            string dirPath = Path.Combine(@"..\..\", dirName);
            DOFileSystem.CreateDirectory(dirPath);

            Assert.IsTrue(DOFileSystem.DirectoryExist(dirPath));
            Directory.Delete(dirPath, true);
        }

        [Test]
        public void WriteFileTest() {
            string dirName = Guid.NewGuid().ToString("D");
            string dirPath = Path.Combine(@"..\..\", dirName);
            DOFileSystem.CreateDirectory(dirPath);
            string filePath = Path.Combine(dirPath, "test.tmp");
            byte[] file = new byte[] {1, 2, 3};
            DOFileSystem.WriteFile(file, filePath);
            Assert.IsTrue(DOFileSystem.FileExist(filePath));
            Directory.Delete(dirPath, true);
        }
    }
}