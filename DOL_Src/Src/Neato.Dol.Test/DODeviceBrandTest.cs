using System.Data;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Entity;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class DODeviceBrandTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void DeviceBrandInsertTest() {
            string brandName = string.Empty.PadRight(1000, 't');
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = brandName;

            DODeviceBrand.Add(brand);

            Assert.IsNotNull(brand);
            Assert.AreEqual(false, brand.IsNew);
            Assert.IsTrue(brand.Id > 0);

            brand = DODeviceBrand.GetDeviceBrand(brand);

            Assert.IsNotNull(brand);
            Assert.AreEqual(false, brand.IsNew);
            Assert.IsTrue(brand.Id > 0);
            Assert.AreEqual(brandName.Substring(0, 30), brand.Name);

            byte[] dbIcon = DODeviceBrand.GetDeviceBrandIcon(brand);
            Assert.AreEqual(0, dbIcon.Length);

        }

        [Test]
        public void DeviceBrandGetByIdTest() {
            string brandName = string.Empty.PadRight(1000, 't');
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = brandName;

            DODeviceBrand.Add(brand);

            Assert.IsNotNull(brand);
            Assert.AreEqual(false, brand.IsNew);
            Assert.IsTrue(brand.Id > 0);

            DeviceBrand dbDeviceBrand = DODeviceBrand.GetDeviceBrand(brand);

            Assert.IsNotNull(dbDeviceBrand);
            Assert.AreEqual(false, brand.IsNew);
            Assert.AreEqual(brand.Id, dbDeviceBrand.Id);
            Assert.AreEqual(brandName.Substring(0, 30), dbDeviceBrand.Name);
        }

        [Test]
        public void EnumerateAllDeviceBrandTest() {
            string brandName1 = string.Empty.PadRight(1000, '1');
            string brandName2 = string.Empty.PadRight(1000, '2');
            DeviceBrand brand1 = BCDeviceBrand.NewDeviceBrand();
            brand1.Name = brandName1;
            DeviceBrand brand2 = BCDeviceBrand.NewDeviceBrand();
            brand2.Name = brandName2;

            DeviceBrand[] brandsBefore = DODeviceBrand.EnumerateDeviceBrandByParam(DeviceType.Undefined, null);
            Assert.IsNotNull(brandsBefore);

            DODeviceBrand.Add(brand1);
            Assert.IsNotNull(brand1);

            DODeviceBrand.Add(brand2);
            Assert.IsNotNull(brand2);

            DeviceBrand[] brandsAfter = DODeviceBrand.EnumerateDeviceBrandByParam(DeviceType.Undefined, null);
            Assert.IsNotNull(brandsAfter);
            Assert.AreEqual(brandsBefore.Length + 2, brandsAfter.Length);
        }

        [Test]
        public void EnumerateDeviceBrandByDeviceTypeTest() {
            string brandName1 = string.Empty.PadRight(1000, '1');
            string brandName2 = string.Empty.PadRight(1000, '2');

            DeviceBrand[] brandsBefore = DODeviceBrand.EnumerateDeviceBrandByParam(DeviceType.CdDvdLabel, null);
            Assert.IsNotNull(brandsBefore);

            DeviceBrand brand1 = BCDeviceBrand.NewDeviceBrand();
            brand1.Name = brandName1;

            DeviceBrand brand2 = BCDeviceBrand.NewDeviceBrand();
            brand2.Name = brandName2;

            DODeviceBrand.Add(brand1);
            Assert.IsNotNull(brand1);

            DODeviceBrand.Add(brand2);
            Assert.IsNotNull(brand2);

            // Devices are not assigned
            DeviceBrand[] brandsAfter = DODeviceBrand.EnumerateDeviceBrandByParam(DeviceType.CdDvdLabel, null);
            Assert.IsNotNull(brandsAfter);
            Assert.AreEqual(brandsBefore.Length, brandsAfter.Length);
        }

        [Test]
        public void DeviceBrandDeleteTest() {
            string DeviceBrandName = string.Empty.PadRight(1000, 't');
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = DeviceBrandName;

            DODeviceBrand.Add(brand);

            Assert.IsNotNull(brand);
            Assert.AreEqual(false, brand.IsNew);
            Assert.IsTrue(brand.Id > 0);

            DODeviceBrand.Delete(brand);

            DeviceBrand dbDeviceBrand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNull(dbDeviceBrand);
        }

        [Test]
        public void DeviceBrandUpdateTest() {
            string brandName = string.Empty.PadRight(1000, 't');
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = brandName;

            DODeviceBrand.Add(brand);
            Assert.IsNotNull(brand);

            string brandName2 = string.Empty.PadRight(1000, 'q');
            brand.Name = brandName2;

            DODeviceBrand.Update(brand);

            Assert.IsNotNull(brand);
            Assert.AreEqual(false, brand.IsNew);
            Assert.IsTrue(brand.Id > 0);

            DeviceBrand dbDeviceBrand = DODeviceBrand.GetDeviceBrand(brand);

            Assert.IsNotNull(dbDeviceBrand);
            Assert.AreEqual(false, brand.IsNew);
            Assert.AreEqual(brand.Id, dbDeviceBrand.Id);
            Assert.AreEqual(brandName2.Substring(0, 30), dbDeviceBrand.Name);

            byte[] dbIcon = DODeviceBrand.GetDeviceBrandIcon(brand);
            Assert.AreEqual(0, dbIcon.Length);
        }

        [Test]
        public void DeviceBrandIconUpdateTest() {
            string brandName = string.Empty.PadRight(1000, 't');
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = brandName;

            DODeviceBrand.Add(brand);
            Assert.IsNotNull(brand);

            byte[] dbIcon = DODeviceBrand.GetDeviceBrandIcon(brand);
            Assert.AreEqual(0, dbIcon.Length);

            byte[] icon = {1, 2, 3};
            DODeviceBrand.UpdateIcon(brand, icon);

            dbIcon = DODeviceBrand.GetDeviceBrandIcon(brand);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }
    }
}