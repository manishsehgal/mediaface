using System.Data;

using Neato.Dol.Data;
using Neato.Dol.Entity.Helpers;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class CommandLineParserTest {
        private string[] args = new string[] {
            "-p", "C:\\Temp\\BulkImages\\", "-f", "Main", "-c"
        };

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void GetValueByKeyTest() {
            Assert.AreEqual("C:\\Temp\\BulkImages\\",
                CommandLineParser.GetValueByKey(args, "-p"));
            Assert.AreEqual("Main",
                CommandLineParser.GetValueByKey(args, "-f"));
        }

        [Test]
        public void GetValueByKeyFailedTest() {
            Assert.AreEqual(string.Empty,
                CommandLineParser.GetValueByKey(args, "-m"));
            Assert.AreEqual(string.Empty,
                CommandLineParser.GetValueByKey(new string[] {}, "-p"));
        }

        [Test]
        public void GetKeyPositionTest() {
            Assert.AreEqual(0,
                CommandLineParser.GetKeyPosition(args, "-p"));
            Assert.AreEqual(2,
                CommandLineParser.GetKeyPosition(args, "-f"));
            Assert.AreEqual(4,
                CommandLineParser.GetKeyPosition(args, "-c"));
        }

        [Test]
        public void GetKeyPositionFailedTest() {
            Assert.AreEqual(int.MinValue,
                CommandLineParser.GetKeyPosition(args, "-m"));
            Assert.AreEqual(int.MinValue,
                CommandLineParser.GetKeyPosition(new string[] {}, "-p"));
        }

        [Test]
        public void IsKeyPresentTest() {
            Assert.IsTrue(CommandLineParser.IsKeyPresent(args, "-p"));
            Assert.IsFalse(CommandLineParser.IsKeyPresent(args, "-m"));
            Assert.IsFalse(CommandLineParser.IsKeyPresent(new string[]{}, "-p"));
        }
    }
}