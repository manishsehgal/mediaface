using System.Data;
using System.Xml;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Entity;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class DeviceTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void GetDevicesByPaper() {
            Device[] devices = BCDevice.GetDeviceListByPaper(new PaperBase(-1));
            Assert.AreEqual(0, devices.Length);

            Device[] devices2 = BCDevice.GetDeviceListByPaper(new PaperBase(2001));
            Assert.AreEqual(1, devices2.Length);
        }

        // TODO: move this test to PaperTest class
        [Test]
        public void PrintDevicesToPdf() {
            PaperBase paperBase = DOPaper.EnumeratePaperByParam(null, null, null, null, null, null, null, null, null, -1, -1)[0];
            Customer customer = null;

            Project project = BCProject.GetProject(customer, paperBase);
            PdfWrapper.GeneratePdf(project, new XmlDocument(), new XmlDocument(), new XmlDocument(), new XmlDocument());
        }

        [Test]
        public void DeviceInsertTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNotNull(brand);

            string deviceModel = string.Empty.PadRight(1000, 't');
            Device device = BCDevice.NewDevice();
            device.Model = deviceModel;
            device.DeviceTypeId = (int)DeviceType.CdDvdLabel;
            device.Brand = brand;

            DODevice.Add(device);

            Assert.IsNotNull(device);
            Assert.AreEqual(false, device.IsNew);
            Assert.IsTrue(device.Id > 0);

            device = DODevice.GetDevice(device);

            Assert.IsNotNull(device);
            Assert.AreEqual(false, device.IsNew);
            Assert.IsTrue(device.Id > 0);
            Assert.AreEqual(deviceModel.Substring(0, 50), device.Model);
            Assert.AreEqual((int)DeviceType.CdDvdLabel, device.DeviceTypeId);
//            Assert.AreEqual(brand, device.Brand);

            byte[] dbIcon = DODevice.GetDeviceIcon(device);
            Assert.AreEqual(0, dbIcon.Length);

        }

        [Test]
        public void DeviceGetByIdTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNotNull(brand);

            string deviceModel = string.Empty.PadRight(1000, 't');
            Device device = BCDevice.NewDevice();
            device.Model = deviceModel;
            device.DeviceTypeId = (int)DeviceType.CdDvdLabel;
            device.Brand = brand;

            DODevice.Add(device);

            Assert.IsNotNull(device);
            Assert.AreEqual(false, device.IsNew);
            Assert.IsTrue(device.Id > 0);

            Device dbDevice = DODevice.GetDevice(device);

            Assert.IsNotNull(dbDevice);
            Assert.AreEqual(false, dbDevice.IsNew);
            Assert.AreEqual(device.Id, dbDevice.Id);
            Assert.AreEqual(deviceModel.Substring(0, 50), dbDevice.Model);
            Assert.AreEqual((int)DeviceType.CdDvdLabel, dbDevice.DeviceTypeId);
            //Assert.AreEqual(brand, dbDevice.Brand);
        }

        [Test]
        public void EnumerateAllDeviceTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNotNull(brand);

            string deviceModel1 = string.Empty.PadRight(1000, '1');
            string deviceModel2 = string.Empty.PadRight(1000, '2');
            Device device1 = BCDevice.NewDevice();
            device1.Model = deviceModel1;
            device1.DeviceTypeId = (int)DeviceType.CdDvdLabel;
            device1.Brand = brand;

            Device device2 = BCDevice.NewDevice();
            device2.Model = deviceModel2;
            device2.DeviceTypeId = (int)DeviceType.CdDvdLabel;
            device2.Brand = brand;

            Device[] devicesBefore = DODevice.EnumerateDeviceByParam(null, (int)DeviceType.Undefined, null, null, null, DeviceType.Undefined, -1);
            Assert.IsNotNull(devicesBefore);

            DODevice.Add(device1);
            Assert.IsNotNull(device1);

            DODevice.Add(device2);
            Assert.IsNotNull(device2);

            Device[] devicesAfter = DODevice.EnumerateDeviceByParam(null, (int)DeviceType.Undefined, null, null, null, DeviceType.Undefined, -1);
            Assert.IsNotNull(devicesAfter);
            Assert.AreEqual(devicesBefore.Length + 2, devicesAfter.Length);
        }

        [Test]
        public void EnumerateDeviceByDeviceTypeTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNotNull(brand);

            string deviceModel1 = string.Empty.PadRight(1000, '1');
            string deviceModel2 = string.Empty.PadRight(1000, '2');

            Device[] devicesBefore = DODevice.EnumerateDeviceByParam(null, (int)DeviceType.CdDvdLabel, null, null, null, DeviceType.Undefined, -1);
            Assert.IsNotNull(devicesBefore);

            Device device1 = BCDevice.NewDevice();
            device1.Model = deviceModel1;
            device1.DeviceTypeId = (int)DeviceType.CdDvdLabel;
            device1.Brand = brand;

            Device device2 = BCDevice.NewDevice();
            device2.Model = deviceModel2;
            device2.DeviceTypeId = (int)DeviceType.CdDvdLabel;
            device2.Brand = brand;

            DODevice.Add(device1);
            Assert.IsNotNull(device1);

            DODevice.Add(device2);
            Assert.IsNotNull(device2);

            Device[] devicesAfter = DODevice.EnumerateDeviceByParam(null, (int)DeviceType.CdDvdLabel, null, null, null, DeviceType.Undefined, -1);
            Assert.IsNotNull(devicesAfter);
            Assert.AreEqual(devicesBefore.Length + 2, devicesAfter.Length);
        }

        [Test]
        public void GetDeviceListByDeviceTypeAndMinDeviceTypeTest() {
            Device[] devicesBefore = BCDevice.GetDeviceList(null, DeviceType.MP3Player, null);
            Assert.IsNotNull(devicesBefore);

            Category newCategory = BCCategory.NewCategory();
            newCategory.Name = string.Empty.PadRight(10, '1');
            BCCategory.Save(newCategory);

            string mp3CategoryName = BCCategory.GetName(new CategoryBase((int)DeviceType.MP3Player));

            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);

            string deviceModel1 = string.Empty.PadRight(10, '1');
            string deviceModel2 = string.Empty.PadRight(10, '2');

            Device device1 = BCDevice.NewDevice();
            device1.Model = deviceModel1;
            device1.DeviceTypeId = (int)DeviceType.MP3Player;
            device1.Brand = brand;

            Device device2 = BCDevice.NewDevice();
            device2.Model = deviceModel2;
            device2.DeviceTypeId = newCategory.Id;
            device2.Brand = brand;

            DODevice.Add(device1);
            DODevice.Add(device2);

            Device[] devicesAfter = BCDevice.GetDeviceList(null, DeviceType.MP3Player, null);
            Assert.IsNotNull(devicesAfter);
            Assert.AreEqual(devicesBefore.Length + 2, devicesAfter.Length);

            bool foundDevice1 = false;
            bool foundDevice2 = false;
            foreach (Device device in devicesAfter) {
                if (device.Model == deviceModel1 &&
                    device.Category == mp3CategoryName &&
                    device.DeviceTypeId == (int)DeviceType.MP3Player) {
                    foundDevice1 = true;
                }
                if (device.Model == deviceModel2 &&
                    device.Category == newCategory.Name &&
                    device.DeviceTypeId == newCategory.Id) {
                    foundDevice2 = true;
                }
            }

            Assert.IsTrue(foundDevice1);
            Assert.IsTrue(foundDevice2);
        }

        [Test]
        public void DeviceDeleteTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNotNull(brand);

            string deviceModel = string.Empty.PadRight(1000, 't');
            Device device = BCDevice.NewDevice();
            device.Model = deviceModel;
            device.DeviceTypeId = (int)DeviceType.CdDvdLabel;
            device.Brand = brand;

            DODevice.Add(device);

            Assert.IsNotNull(device);
            Assert.AreEqual(false, device.IsNew);
            Assert.IsTrue(device.Id > 0);

            DODevice.Delete(device);

            Device dbDevice = DODevice.GetDevice(device);
            Assert.IsNull(dbDevice);
        }

        [Test]
        public void DeviceUpdateTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNotNull(brand);

            string deviceModel = string.Empty.PadRight(1000, 't');
            Device device = BCDevice.NewDevice();
            device.Model = deviceModel;
            device.DeviceTypeId = (int)DeviceType.CdDvdLabel;
            device.Brand = brand;

            DODevice.Add(device);
            Assert.IsNotNull(device);

            string deviceModel2 = string.Empty.PadRight(1000, 'q');
            device.Model = deviceModel2;

            DODevice.Update(device);

            Assert.IsNotNull(device);
            Assert.AreEqual(false, device.IsNew);
            Assert.IsTrue(device.Id > 0);

            Device dbDevice = DODevice.GetDevice(device);

            Assert.IsNotNull(dbDevice);
            Assert.AreEqual(false, dbDevice.IsNew);
            Assert.AreEqual(device.Id, dbDevice.Id);
            Assert.AreEqual(deviceModel2.Substring(0, 50), dbDevice.Model);

            byte[] dbIcon = DODevice.GetDeviceIcon(device);
            Assert.AreEqual(0, dbIcon.Length);
        }

        [Test]
        public void DeviceIconFullTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNotNull(brand);

            string deviceModel = string.Empty.PadRight(1000, 't');
            Device device = BCDevice.NewDevice();
            device.Model = deviceModel;
            device.DeviceTypeId = (int)DeviceType.CdDvdLabel;
            device.Brand = brand;

            DODevice.Add(device);
            Assert.IsNotNull(device);

            byte[] dbIcon = DODevice.GetDeviceIcon(device);
            Assert.AreEqual(0, dbIcon.Length);

            byte[] icon = {1, 2, 3};
            DODevice.UpdateIcon(device, icon);

            dbIcon = DODevice.GetDeviceIcon(device);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }
    }
}
