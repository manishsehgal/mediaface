using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.IO;
using System.Xml;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Entity;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class BCPaperTest {
        public BCPaperTest() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void NewPaperTest() {
            Paper paper = BCPaper.NewPaper();
            Assert.IsNotNull(paper);
            Assert.IsNull(paper.Brand);
            Assert.AreEqual(0, paper.Id);
            Assert.AreEqual(string.Empty, paper.Name);
            Assert.AreEqual(0, paper.Size.Width);
        }

        [Test]
        public void SaveNewPaperWithoutIconTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);

            Paper paper = BCPaper.NewPaper();
            paper.Name = string.Empty.PadRight(10, '1');
            paper.Brand = brand;
            paper.Size = new SizeF(100, 100);

            BCPaper.Save(paper);

            Assert.IsTrue(paper.Id > 0);

            Paper dbPaper = DOPaper.GetPaper(paper);

            Assert.IsNotNull(dbPaper);
            Assert.AreEqual(paper.Id, dbPaper.Id);
            Assert.AreEqual(paper.Name, dbPaper.Name);
            Assert.AreEqual(paper.Size, dbPaper.Size);
            Assert.AreEqual(paper.Size.Width, dbPaper.Size.Width);
            Assert.AreEqual(paper.Brand, dbPaper.Brand);

            byte[] icon = BCPaper.GetPaperIcon(paper, 80, 80);
            Assert.IsNotNull(icon);
            Assert.AreEqual(0, icon.Length);
        }

        [Test]
        public void SaveNewPaperWithFacesTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);

            Face face1 = BCFace.NewFace();
            face1.Contour = null;
            face1.Guid = Guid.NewGuid();
            BCFace.Save(face1);

            Face face2 = BCFace.NewFace();
            face2.Contour = null;
            face2.Guid = Guid.NewGuid();
            BCFace.Save(face2);

            Paper paper = BCPaper.NewPaper();
            paper.Name = string.Empty.PadRight(10, '1');
            paper.Brand = brand;
            paper.Size = new SizeF(100, 100);
            paper.AddFace(face1, 10, 1);
            paper.AddFace(face2, 1, 10);

            BCPaper.Save(paper);

            Assert.IsTrue(paper.Id > 0);

            Paper dbPaper = DOPaper.GetPaper(paper);

            Assert.IsNotNull(dbPaper);
            Assert.AreEqual(paper.Id, dbPaper.Id);
            Assert.AreEqual(paper.Name, dbPaper.Name);
            Assert.AreEqual(paper.Size, dbPaper.Size);
            Assert.AreEqual(paper.Size.Width, dbPaper.Size.Width);
            Assert.AreEqual(paper.Brand, dbPaper.Brand);

            ArrayList dbFaces = new ArrayList(dbPaper.Faces);
            int index = dbFaces.IndexOf(face1);
            Assert.IsTrue(index >= 0);
            Assert.AreEqual(dbPaper.Faces[index].Position, face1.Position);
            index = dbFaces.IndexOf(face2);
            Assert.IsTrue(index >= 0);
            Assert.AreEqual(dbPaper.Faces[index].Position, face2.Position);

            byte[] icon = BCPaper.GetPaperIcon(paper, 80, 80);
            Assert.IsNotNull(icon);
            Assert.AreEqual(0, icon.Length);
        }

        [Test]
        public void SaveNewPaperWithIconTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);

            Paper paper = BCPaper.NewPaper();
            paper.Name = string.Empty.PadRight(10, '1');
            paper.Brand = brand;
            paper.Size = new SizeF(100, 100);

            byte[] icon = {1, 2, 3};

            BCPaper.Save(paper, new MemoryStream(icon));

            Assert.IsTrue(paper.Id > 0);

            Paper dbPaper = DOPaper.GetPaper(paper);

            Assert.IsNotNull(dbPaper);
            Assert.AreEqual(paper.Id, dbPaper.Id);
            Assert.AreEqual(paper.Name, dbPaper.Name);
            Assert.AreEqual(paper.Size.Height, dbPaper.Size.Height);
            Assert.AreEqual(paper.Size.Width, dbPaper.Size.Width);
            Assert.AreEqual(paper.Brand, dbPaper.Brand);

            byte[] dbIcon = DOPaper.GetPaperIcon(paper);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        public void SaveExistingPaperWithoutIconTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);

            string paperName1 = string.Empty.PadRight(10, '1');
            string paperName2 = string.Empty.PadRight(10, '2');
            byte[] icon = {1, 2, 3};

            Paper paper = BCPaper.NewPaper();
            paper.Name = paperName1;
            paper.Brand = brand;
            paper.Size = new SizeF(100, 100);
            BCPaper.Save(paper, new MemoryStream(icon));

            paper.Name = paperName2;
            BCPaper.Save(paper);

            Paper dbPaper = DOPaper.GetPaper(paper);

            Assert.IsNotNull(dbPaper);
            Assert.AreEqual(paper.Id, dbPaper.Id);
            Assert.AreEqual(paper.Name, dbPaper.Name);
            Assert.AreEqual(paper.Size.Height, dbPaper.Size.Height);
            Assert.AreEqual(paper.Size.Width, dbPaper.Size.Width);
            Assert.AreEqual(paper.Brand, dbPaper.Brand);

            byte[] dbIcon = DOPaper.GetPaperIcon(paper);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        public void SaveExistingPaperWithFacesTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);

            Face face1 = BCFace.NewFace();
            face1.Contour = new XmlDocument().CreateElement("Test");
            face1.Guid = Guid.NewGuid();
            BCFace.Save(face1);

            Face face2 = BCFace.NewFace();
            face2.Contour = new XmlDocument().CreateElement("Test");
            face2.Guid = Guid.NewGuid();

            BCFace.Save(face2);

            Paper paper = BCPaper.NewPaper();
            paper.Name = string.Empty.PadRight(10, '1');
            paper.Brand = brand;
            paper.Size = new SizeF(100, 100);
            paper.AddFace(face1, 10, 1);

            BCPaper.Save(paper);

            paper.AddFace(face2, 1, 10);
            BCPaper.Save(paper);

            Assert.IsTrue(paper.Id > 0);

            Paper dbPaper = DOPaper.GetPaper(paper);

            Assert.IsNotNull(dbPaper);
            Assert.AreEqual(paper.Id, dbPaper.Id);
            Assert.AreEqual(paper.Name, dbPaper.Name);
            Assert.AreEqual(paper.Size, dbPaper.Size);
            Assert.AreEqual(paper.Size.Width, dbPaper.Size.Width);
            Assert.AreEqual(paper.Brand, dbPaper.Brand);

            ArrayList dbFaces = new ArrayList(dbPaper.Faces);
            int index = dbFaces.IndexOf(face1);
            Assert.IsTrue(index >= 0);
            Assert.AreEqual(dbPaper.Faces[index].Position, face1.Position);
            index = dbFaces.IndexOf(face2);
            Assert.IsTrue(index >= 0);
            Assert.AreEqual(dbPaper.Faces[index].Position, face2.Position);

            byte[] icon = BCPaper.GetPaperIcon(paper, 80, 80);
            Assert.IsNotNull(icon);
            Assert.AreEqual(0, icon.Length);
        }

        [Test]
        public void SaveExistingPaperWithIconTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);

            string paperName1 = string.Empty.PadRight(10, '1');
            string paperName2 = string.Empty.PadRight(10, '2');

            byte[] icon1 = {1, 2, 3};
            byte[] icon2 = {4, 5, 6, 7};

            Paper paper = BCPaper.NewPaper();
            paper.Name = paperName1;
            paper.Brand = brand;
            paper.Size = new SizeF(100, 100);

            BCPaper.Save(paper, new MemoryStream(icon1));

            paper.Name = paperName2;
            BCPaper.Save(paper, new MemoryStream(icon2));

            Paper dbPaper = DOPaper.GetPaper(paper);

            Assert.IsNotNull(dbPaper);
            Assert.AreEqual(paper.Id, dbPaper.Id);
            Assert.AreEqual(paperName2, dbPaper.Name);
            Assert.AreEqual(paper.Size, dbPaper.Size);
            Assert.AreEqual(paper.Size.Width, dbPaper.Size.Width);
            Assert.AreEqual(paper.Brand, dbPaper.Brand);

            byte[] dbIcon = DOPaper.GetPaperIcon(paper);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon2.Length, dbIcon.Length);
            for (int index = 0; index < icon2.Length; index++) {
                Assert.AreEqual(icon2[index], dbIcon[index]);
            }
        }

        [Test]
        public void ChangeIconTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);

            string paperName1 = string.Empty.PadRight(10, '1');

            byte[] icon1 = {1, 2, 3};
            byte[] icon2 = {4, 5, 6, 7};

            Paper paper = BCPaper.NewPaper();
            paper.Name = paperName1;
            paper.Brand = brand;
            paper.Size = new SizeF(100, 100);

            BCPaper.Save(paper, new MemoryStream(icon1));

            BCPaper.ChangeIcon(paper, null);

            byte[] dbIcon = DOPaper.GetPaperIcon(paper);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon1.Length, dbIcon.Length);
            for (int index = 0; index < icon1.Length; index++) {
                Assert.AreEqual(icon1[index], dbIcon[index]);
            }

            BCPaper.ChangeIcon(paper, new MemoryStream(0));

            dbIcon = BCPaper.GetPaperIcon(paper);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon1.Length, dbIcon.Length);
            for (int index = 0; index < icon1.Length; index++) {
                Assert.AreEqual(icon1[index], dbIcon[index]);
            }

            BCPaper.ChangeIcon(paper, new MemoryStream(icon2));

            dbIcon = BCPaper.GetPaperIcon(paper);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon2.Length, dbIcon.Length);
            for (int index = 0; index < icon2.Length; index++) {
                Assert.AreEqual(icon2[index], dbIcon[index]);
            }
        }

        [Test]
        public void GetPaperIconTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);

            Paper paper = BCPaper.NewPaper();
            paper.Name = string.Empty.PadRight(10, '1');
            paper.Brand = brand;
            paper.Size = new SizeF(100, 100);
            byte[] icon = {1, 2, 3};

            BCPaper.Save(paper, new MemoryStream(icon));

            byte[] dbIcon = DOPaper.GetPaperIcon(paper);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        [Ignore("Not implemented")]
        public void GetPaperListTest() {}

        [Test]
        [Ignore("Not implemented")]
        public void SynchronizeFacesTest() {}

        [Test]
        public void DeleteTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);

            Face face = BCFace.NewFace();
            face.AddName("", "test");
            DOFace.Add(face);

            Paper paper = BCPaper.NewPaper();
            paper.Name = string.Empty.PadRight(10, '1');
            paper.Brand = brand;
            paper.Size = new SizeF(100, 100);
            paper.AddFace(face, 10, 10);

            byte[] icon = {1, 2, 3};

            BCPaper.Save(paper, new MemoryStream(icon));

            BCPaper.Delete(paper);

            paper = BCPaper.GetPaper(paper);
            Assert.IsNull(paper);

            face = DOFace.GetFace(face);
            Assert.IsNotNull(face);
        }

        [Test]
        public void DeleteWithLastEditedPaperTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);

            Face face = BCFace.NewFace();
            face.AddName("", "test");
            DOFace.Add(face);

            Paper paper = BCPaper.NewPaper();
            paper.Name = string.Empty.PadRight(10, '1');
            paper.Brand = brand;
            paper.Size = new SizeF(100, 100);
            paper.AddFace(face, 10, 10);

            byte[] icon = {1, 2, 3};

            BCPaper.Save(paper, new MemoryStream(icon));

            string login = Guid.NewGuid().ToString("D");
            Customer customer = new Customer(0, "first", "last", login, "1", EmailOptions.Text, false, BCRetailer.defaultId, CustomerGroup.MFO);
            BCCustomer.InsertCustomer(customer, null);
            
            BCCustomer.ChangeLastEditedPaper(customer, paper);
            customer = DOCustomer.GetCustomerById(customer.CustomerId);
            Assert.AreEqual(0, new ArrayList(customer.LastEditedPapers).IndexOf(paper));
            Assert.AreEqual(paper, customer.LastEditedPaper);

            BCPaper.Delete(paper);

            customer = DOCustomer.GetCustomerById(customer.CustomerId);
            Assert.AreEqual(0, customer.LastEditedPapers.Length);
            Assert.IsNull(customer.LastEditedPaper);

            face = DOFace.GetFace(face);
            Assert.IsNotNull(face);

            paper = BCPaper.GetPaper(paper);
            Assert.IsNull(paper);
        }

        [Test]
        public void DeleteWithFacesTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);

            Face face = BCFace.NewFace();
            DOFace.Add(face);

            Paper paper = BCPaper.NewPaper();
            paper.Name = string.Empty.PadRight(10, '1');
            paper.Brand = brand;
            paper.Size = new SizeF(100, 100);
            paper.AddFace(face, 10, 10);

            byte[] icon = {1, 2, 3};

            BCPaper.Save(paper, new MemoryStream(icon));

            BCPaper.DeleteWithFaces(paper);

            paper = BCPaper.GetPaper(paper);
            Assert.IsNull(paper);

            face = DOFace.GetFace(face);
            Assert.IsNull(face);
        }
        [Test]
        public void SavePaperWithFacesTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);

            Face face = BCFace.NewFace();
            string faceName = "TestFace_For_UT_SavePaperWithFacesTest";
            face.AddName("", faceName);
            DOFace.Add(face);

            Face faceFromDB = BCFace.EnumerateFaceByParam
                (null, DeviceType.Undefined, faceName)[0];
            Assert.AreEqual(faceName, faceFromDB.GetName(""));

            string faceName2 = "TestFace_For_UT_SavePaperWithFacesTest2";
            face.AddName("", faceName2);

            Paper paper = BCPaper.NewPaper();
            paper.Name = string.Empty.PadRight(10, '1');
            paper.Brand = brand;
            paper.Size = new SizeF(100, 100);
            paper.AddFace(face, 10, 10);

            BCPaper.Save(paper);

            Assert.AreEqual(0, BCFace.EnumerateFaceByParam
                (null, DeviceType.Undefined, faceName2).Length);
            //If face saved with paper (i.e. face name, contour changed),
            //commented code run successful, and code above failed.
            //Else -> else :-)
            /*
            Face faceFromDB2 = BCFace.EnumerateFaceByParam
                (null, DeviceType.Undefined, faceName2)[0];
            Assert.AreEqual(faceName2, faceFromDB2.GetName(""));
            */
        }
        [Test]
        public void GerPaperFacesXmlTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);

            XmlDocument doc = new XmlDocument();

            Face face = BCFace.NewFace();
            string faceName = "TestFace_1";
            face.AddName("", faceName);
            XmlNode contour1 = doc.CreateElement("Contour");
            string contourBody1 = "<MoveTo x=\"111.11\" y=\"0\" /><CurveTo cx=\"200.02\" cy=\"0\" x=\"230.21\" y=\"13.09\" />";
            contour1.InnerXml = contourBody1;
            face.Contour = contour1;
            DOFace.Add(face);

            Face face2 = BCFace.NewFace();
            faceName = "TestFace_2";
            face2.AddName("", faceName);
            XmlNode contour2 = doc.CreateElement("Contour");
            string contourBody2 = "<MoveTo x=\"222.22\" y=\"1\" /><CurveTo cx=\"333.02\" cy=\"0\" x=\"230.21\" y=\"13.09\" />";
            contour2.InnerXml = contourBody2;
            face2.Contour = contour2;
            face2.Guid = Guid.NewGuid();
            DOFace.Add(face2);

            Paper paper = BCPaper.NewPaper();
            paper.Name = string.Empty.PadRight(10, '1');
            paper.Brand = brand;
            paper.Size = new SizeF(100, 200);

            paper.AddFace(face, 10, 15);
            paper.AddFace(face2, 20, 25);

            BCPaper.Save(paper);

            XmlDocument facesXml = BCPaper.GerPaperFacesXml(paper);

            Assert.IsNotNull(facesXml);
            Assert.AreEqual("Project", facesXml.FirstChild.Name);
            Assert.AreEqual("100", facesXml.SelectSingleNode("//Paper/@w").Value);
            Assert.AreEqual("200", facesXml.SelectSingleNode("//Paper/@h").Value);
            Assert.AreEqual(2, facesXml.SelectNodes("//Faces/Face").Count);
            Assert.AreEqual("10", facesXml.SelectSingleNode("//Faces/Face/@x").Value);
            Assert.AreEqual("15", facesXml.SelectSingleNode("//Faces/Face/@y").Value);
            Assert.AreEqual("Contour", facesXml.SelectSingleNode("//Faces/Face").ChildNodes[0].Name);
            Assert.AreEqual(contourBody1, facesXml.SelectSingleNode("//Faces/Face").ChildNodes[0].InnerXml);
        }
    }
}