using System.Data;
using System.IO;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Entity;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class BCDeviceBrandTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void NewDeviceBrandTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            Assert.IsNotNull(brand);
            Assert.AreEqual(0, brand.Id);
            Assert.AreEqual(string.Empty, brand.Name);
        }

        [Test]
        public void SaveNewDeviceBrandWithoutIconTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = string.Empty.PadRight(10, '1');
            BCDeviceBrand.Save(brand);

            Assert.IsTrue(brand.Id > 0);

            DeviceBrand dbBrand = DODeviceBrand.GetDeviceBrand(brand);

            Assert.IsNotNull(dbBrand);
            Assert.AreEqual(brand.Id, dbBrand.Id);
            Assert.AreEqual(brand.Name, dbBrand.Name);

            byte[] icon = BCDeviceBrand.GetDeviceBrandIcon(brand);
            Assert.IsNotNull(icon);
            Assert.AreEqual(0, icon.Length);
        }

        [Test]
        public void SaveNewDeviceBrandWithIconTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = string.Empty.PadRight(10, '1');

            byte[] icon = {1, 2, 3};

            BCDeviceBrand.Save(brand, new MemoryStream(icon));

            Assert.IsTrue(brand.Id > 0);

            DeviceBrand dbBrand = DODeviceBrand.GetDeviceBrand(brand);

            Assert.IsNotNull(dbBrand);
            Assert.AreEqual(brand.Id, dbBrand.Id);
            Assert.AreEqual(brand.Name, dbBrand.Name);

            byte[] dbIcon = BCDeviceBrand.GetDeviceBrandIcon(brand);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        public void SaveExistingDeviceBrandWithoutIconTest() {
            string brandName1 = string.Empty.PadRight(10, '1');
            string brandName2 = string.Empty.PadRight(10, '2');
            byte[] icon = {1, 2, 3};

            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = brandName1;
            BCDeviceBrand.Save(brand, new MemoryStream(icon));

            brand.Name = brandName2;
            BCDeviceBrand.Save(brand);
            
            DeviceBrand dbBrand = DODeviceBrand.GetDeviceBrand(brand);

            Assert.IsNotNull(dbBrand);
            Assert.AreEqual(brand.Id, dbBrand.Id);
            Assert.AreEqual(brandName2, dbBrand.Name);

            byte[] dbIcon = BCDeviceBrand.GetDeviceBrandIcon(brand);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        public void SaveExistingDeviceBrandWithIconTest() {
            string brandName1 = string.Empty.PadRight(10, '1');
            string brandName2 = string.Empty.PadRight(10, '2');

            byte[] icon1 = {1, 2, 3};
            byte[] icon2 = {4, 5, 6, 7};

            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = brandName1;

            BCDeviceBrand.Save(brand, new MemoryStream(icon1));

            brand.Name = brandName2;
            BCDeviceBrand.Save(brand, new MemoryStream(icon2));
            
            DeviceBrand dbBrand = DODeviceBrand.GetDeviceBrand(brand);

            Assert.IsNotNull(dbBrand);
            Assert.AreEqual(brand.Id, dbBrand.Id);
            Assert.AreEqual(brandName2, dbBrand.Name);

            byte[] dbIcon = BCDeviceBrand.GetDeviceBrandIcon(brand);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon2.Length, dbIcon.Length);
            for (int index = 0; index < icon2.Length; index++) {
                Assert.AreEqual(icon2[index], dbIcon[index]);
            }
        }

        [Test]
        public void DeleteTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = string.Empty.PadRight(10, '1');
            byte[] brandIcon = {1, 2, 3};

            Device device = BCDevice.NewDevice();
            device.Model = string.Empty.PadRight(10, '1');
            device.Brand = brand;
            device.DeviceTypeId = (int)DeviceType.CdDvdLabel;
            device.Rating = 9;

            BCDeviceBrand.Save(brand, new MemoryStream(brandIcon));
            BCDevice.Save(device);

            BCDeviceBrand.Delete(brand);

            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNull(brand);
        }

        [Test]
        public void ChangeIconTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = string.Empty.PadRight(10, '1');
            byte[] icon1 = {1, 2, 3};
            byte[] icon2 = {4, 5, 6, 7};

            BCDeviceBrand.Save(brand, new MemoryStream(icon1));

            BCDeviceBrand.ChangeIcon(brand, null);

            byte[] dbIcon = BCDeviceBrand.GetDeviceBrandIcon(brand);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon1.Length, dbIcon.Length);
            for (int index = 0; index < icon1.Length; index++) {
                Assert.AreEqual(icon1[index], dbIcon[index]);
            }
            
            BCDeviceBrand.ChangeIcon(brand, new MemoryStream(0));

            dbIcon = BCDeviceBrand.GetDeviceBrandIcon(brand);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon1.Length, dbIcon.Length);
            for (int index = 0; index < icon1.Length; index++) {
                Assert.AreEqual(icon1[index], dbIcon[index]);
            }

            BCDeviceBrand.ChangeIcon(brand, new MemoryStream(icon2));

            dbIcon = BCDeviceBrand.GetDeviceBrandIcon(brand);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon2.Length, dbIcon.Length);
            for (int index = 0; index < icon2.Length; index++) {
                Assert.AreEqual(icon2[index], dbIcon[index]);
            }
        }

        [Test]
        public void GetDeviceBrandIconTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = string.Empty.PadRight(10, '1');
            byte[] icon = {1, 2, 3};

            BCDeviceBrand.Save(brand, new MemoryStream(icon));

            byte[] dbIcon = BCDeviceBrand.GetDeviceBrandIcon(brand);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Ignore("Not implemented")]
        public void GetDeviceBrandListTest() {}

    }
}