using System.Data;
using Neato.Dol.Data;
using Neato.Dol.Entity.Helpers;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class KeyValidationHelperTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void GetPureNumberArrayTest() {
            string number1 = "7135a1-134368   1d66 a34d7";
            string[] result1 = KeyValidationHelper.GetPureNumberArray(number1);
            Assert.AreEqual(4, result1.Length);
            Assert.AreEqual("7135a1", result1[0]);
            Assert.AreEqual("134368", result1[1]);
            Assert.AreEqual("1d66", result1[2]);
            Assert.AreEqual("a34d7", result1[3]);

            string number2 = "15X_$!@$^$_WE9^^^^$\\Q3%$)%!#))@#%*%&$&^%$YH";
            string[] result2 = KeyValidationHelper.GetPureNumberArray(number2);
            Assert.AreEqual(4, result2.Length);
            Assert.AreEqual("15X", result2[0]);
            Assert.AreEqual("WE9", result2[1]);
            Assert.AreEqual("Q3", result2[2]);
            Assert.AreEqual("YH", result2[3]);
        }

        [Test]
        public void GetNumberInCorrectFormatTest() {
            string numberOriginal = "7135a1-134368   1d66 a34d7";
            string numberInCorrectFormat = 
                KeyValidationHelper.GetNumberInCorrectFormat(numberOriginal);
            Assert.AreEqual("7135a1-134368-1d66-a34d7", numberInCorrectFormat);

            /*string numberOriginal2 = "26rw95bc96n";
            string numberInCorrectFormat2 = 
                KeyValidationHelper.GetNumberInCorrectFormat(numberOriginal2);
            Assert.AreEqual("26rw95bc96n", numberInCorrectFormat2);*/

            string numberOriginal3 = "15x  - we2- 3H__- __6Y";
            string numberInCorrectFormat3 = 
                KeyValidationHelper.GetNumberInCorrectFormat(numberOriginal3);
            Assert.AreEqual("15X-WE2-3H-6Y", numberInCorrectFormat3);
        }

        [Test]
        public void IsSerialNumberValidSuccessTest() {
            /*Assert.IsTrue(KeyValidationHelper.IsSerialNumberValid(
                "26rw95bc96n"));*/
            Assert.IsTrue(KeyValidationHelper.IsSerialNumberValid(
                "7135a1-134368-1d66-a34d7"));
            Assert.IsTrue(KeyValidationHelper.IsSerialNumberValid(
                "7135a1-134369-1a16-d0441"));
            Assert.IsTrue(KeyValidationHelper.IsSerialNumberValid(
                "15X-WE9-Q3-YH"));
            Assert.IsTrue(KeyValidationHelper.IsSerialNumberValid(
                "15X-WE2-3H-6Y"));
        }
        [Test]
        public void IsSerialNumberValidFailedTest() {
            Assert.IsFalse(KeyValidationHelper.IsSerialNumberValid(
                "26r111bc96n"));
            Assert.IsFalse(KeyValidationHelper.IsSerialNumberValid(
                "7131a1-134368-1d66-a34d7"));
            Assert.IsFalse(KeyValidationHelper.IsSerialNumberValid(
                "7135a1-134369-1a11-d0441"));
            Assert.IsFalse(KeyValidationHelper.IsSerialNumberValid(
                "15X-WE9-Q1-YH"));
            Assert.IsFalse(KeyValidationHelper.IsSerialNumberValid(
                "15X-1E2-3H-6Y"));
        }
    }
}