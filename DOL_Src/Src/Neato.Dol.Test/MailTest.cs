using System.Data;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class MailTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void MailFormatEnumTest() {
            MailFormatData data =  BCMail.MailFormatEnum();

            Assert.IsNotNull(data);
        }

        [Test]
        public void MailFormatUpdTest() {
            MailFormatData data =  BCMail.MailFormatEnum();

            if (data.MailFormat.Count >= 2) {
                string testSubject = "TestSubject";
                string testBody = "TestBody";
                string unchangedSubject = data.MailFormat[1].Subject;
                string unchangedBody = data.MailFormat[1].Body;

                data.MailFormat[0].Subject = "TestSubject";
                data.MailFormat[0].Body = "TestBody";

                BCMail.MailFormatUpd(data);

                MailFormatData data2 =  BCMail.MailFormatEnum();

                Assert.AreEqual(testSubject, data2.MailFormat[0].Subject);
                Assert.AreEqual(testBody, data2.MailFormat[0].Body);
                Assert.AreEqual(unchangedSubject, data2.MailFormat[1].Subject);
                Assert.AreEqual(unchangedBody, data2.MailFormat[1].Body);
            }
        }
    }
}