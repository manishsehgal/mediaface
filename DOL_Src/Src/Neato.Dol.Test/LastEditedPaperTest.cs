using System.Data;
using System.Drawing;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class LastEditedPaperTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void GetLastEditedPaperTest() {
            Customer customer = new Customer();
            customer.CustomerId = int.MaxValue/2;
            customer.RetailerId = BCRetailer.defaultId;
            customer.Email = "UnitTestOnly@UnitTestOnly.com";
            customer.Group = CustomerGroup.MFO;
            LastEditedPaperData customerData = new LastEditedPaperData();
            customerData.LastEditedPaper.AddLastEditedPaperRow(customer.CustomerId, 1);
            BCCustomer.InsertCustomer(customer, customerData);
            LastEditedPaperData data =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            Assert.IsTrue(data.LastEditedPaper.Count >= 0);
        }

        [Test]
        public void UpdateLastEditedPaper() {
            Customer customer = new Customer();
            customer.CustomerId = int.MaxValue/2;
            customer.Email = "UnitTestOnly@UnitTestOnly.com";
            customer.RetailerId = BCRetailer.defaultId;
            customer.Group = CustomerGroup.MFO;
            LastEditedPaperData customerData = new LastEditedPaperData();
            int paperId = getPaperId();
            customerData.LastEditedPaper.AddLastEditedPaperRow(customer.CustomerId, paperId);
            BCCustomer.InsertCustomer(customer, customerData);

            LastEditedPaperData data =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers = data.LastEditedPaper.Count;

            //Test add 1 row for this customer
            int paperId2 = getPaperId();
            data.LastEditedPaper.AddLastEditedPaperRow(customer.CustomerId, paperId2);

            DOLastEditedPaper.UpdateLastEditedPaper(data);
            
            LastEditedPaperData data2 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers2 = data2.LastEditedPaper.Count;
            Assert.AreEqual(countPapers + 1, countPapers2);

            //Test modify 1 row for this customer
            int paperId3 = getPaperId();
            data2.LastEditedPaper[0].PaperId = paperId3;

            DOLastEditedPaper.UpdateLastEditedPaper(data2);
            
            LastEditedPaperData data3 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers3 = data3.LastEditedPaper.Count;
            Assert.AreEqual(countPapers2, countPapers3);
            Assert.AreEqual(paperId3, data3.LastEditedPaper[0].PaperId);
            Assert.AreEqual(paperId2, data3.LastEditedPaper[1].PaperId);

            //Test clear 1 row for this customer
            data3.LastEditedPaper[0].Delete();
            DOLastEditedPaper.UpdateLastEditedPaper(data3);

            LastEditedPaperData data4 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers4 = data4.LastEditedPaper.Count;
            Assert.AreEqual(countPapers3 - 1, countPapers4);

            //Test clear all rows for this customer
            int count = data4.LastEditedPaper.Count;
            for(int i = count - 1; i >= 0; i--) {
                data4.LastEditedPaper[i].Delete();
            }
            DOLastEditedPaper.UpdateLastEditedPaper(data4);
            LastEditedPaperData data5 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers5 = data5.LastEditedPaper.Count;
            Assert.AreEqual(0, countPapers5);
        }
        [Test]
        public void ChangeLastEditedPaper() {
            Customer customer = new Customer();
            customer.CustomerId = int.MaxValue/2;
            customer.Email = "UnitTestOnly@UnitTestOnly.com";
            customer.RetailerId = BCRetailer.defaultId;
            customer.Group = CustomerGroup.MFO;
            LastEditedPaperData customerData = new LastEditedPaperData();
            int paperId = getPaperId();
            customerData.LastEditedPaper.AddLastEditedPaperRow(customer.CustomerId, paperId);
            BCCustomer.InsertCustomer(customer, customerData);
            Customer customer2 = new Customer();
            customer2.CustomerId = int.MaxValue/2-1;
            customer2.Email = "UnitTestOnly2@UnitTestOnly.com";
            customer2.RetailerId = BCRetailer.defaultId;
            customer2.Group = CustomerGroup.MFO;
            LastEditedPaperData customerData2 = new LastEditedPaperData();
            customerData2.LastEditedPaper.AddLastEditedPaperRow(customer2.CustomerId, paperId);
            BCCustomer.InsertCustomer(customer2, customerData2);

            //Delete all rows
            LastEditedPaperData data =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);

            int countPapers = data.LastEditedPaper.Count;
            for(int i = countPapers - 1; i >= 0; i--) {
                data.LastEditedPaper[i].Delete();
            }
            DOLastEditedPaper.UpdateLastEditedPaper(data);

            //Test add 1 row for this customer
            BCLastEditedPaper.ChangeLastEditedPaper(customer, new PaperBase(paperId));
            
            LastEditedPaperData data2 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers2 = data2.LastEditedPaper.Count;
            Assert.AreEqual(1, countPapers2);
            Assert.AreEqual(paperId, data2.LastEditedPaper[0].PaperId);
            Assert.AreEqual(customer.CustomerId, data2.LastEditedPaper[0].CustomerId);

            //Test add +1 row for this customer
            int paperId2 = getPaperId();
            BCLastEditedPaper.ChangeLastEditedPaper(customer, new PaperBase(paperId2));
            
            LastEditedPaperData data3 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers3 = data3.LastEditedPaper.Count;
            Assert.AreEqual(2, countPapers3);
            Assert.AreEqual(paperId2, data3.LastEditedPaper[0].PaperId);
            Assert.AreEqual(paperId, data3.LastEditedPaper[1].PaperId);

            //Test add +1 row for this customer (new paper==last edit paper)
            BCLastEditedPaper.ChangeLastEditedPaper(customer, new PaperBase(paperId2));
            
            LastEditedPaperData data4 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers4 = data4.LastEditedPaper.Count;
            Assert.AreEqual(2, countPapers4);
            Assert.AreEqual(paperId2, data4.LastEditedPaper[0].PaperId);
            Assert.AreEqual(paperId, data4.LastEditedPaper[1].PaperId);

            //Test add +1 row for this customer (new paper!=last edit paper)
            int paperId3 = getPaperId();
            BCLastEditedPaper.ChangeLastEditedPaper(customer, new PaperBase(paperId3));
            
            LastEditedPaperData data5 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers5 = data5.LastEditedPaper.Count;
            Assert.AreEqual(2, countPapers5);
            Assert.AreEqual(paperId3, data5.LastEditedPaper[0].PaperId);
            Assert.AreEqual(paperId2, data5.LastEditedPaper[1].PaperId);

            //Test add 1 row for other customer
            BCLastEditedPaper.ChangeLastEditedPaper(customer2, new PaperBase(paperId));
            
            LastEditedPaperData data6 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer2);
            int countPapers6 = data6.LastEditedPaper.Count;
            Assert.AreEqual(1, countPapers6);
        }

        private int getPaperId() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);
            brand = DOPaperBrand.GetPaperBrand(brand);
            Assert.IsNotNull(brand);

            string paperName = string.Empty.PadRight(1000, 't');
            Paper paper = BCPaper.NewPaper();
            paper.Name = paperName;
            paper.Size = new SizeF(100, 100);
            paper.Brand = brand;

            DOPaper.Add(paper);

            return paper.Id;
        }
    }
}