using System.Data;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Entity;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class DOCategoryTest {
        public DOCategoryTest() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void CategoryInsertTest() {
            string CategoryName = string.Empty.PadRight(1000, 't');
            Category Category = BCCategory.NewCategory();
            Category.Name = CategoryName;

            DOCategory.Add(Category);

            Assert.IsNotNull(Category);
            Assert.AreEqual(false, Category.IsNew);
            Assert.IsTrue(Category.Id > 0);

            Category = DOCategory.GetCategory(Category);

            Assert.IsNotNull(Category);
            Assert.AreEqual(false, Category.IsNew);
            Assert.IsTrue(Category.Id > 0);
            Assert.AreEqual(CategoryName.Substring(0, 40), Category.Name);

            byte[] dbIcon = DOCategory.GetCategoryIcon(Category);
            Assert.AreEqual(0, dbIcon.Length);

        }

        [Test]
        public void CategoryGetByIdTest() {
            string CategoryName = string.Empty.PadRight(1000, 't');
            Category Category = BCCategory.NewCategory();
            Category.Name = CategoryName;

            DOCategory.Add(Category);

            Assert.IsNotNull(Category);
            Assert.AreEqual(false, Category.IsNew);
            Assert.IsTrue(Category.Id > 0);

            Category dbCategory = DOCategory.GetCategory(Category);

            Assert.IsNotNull(dbCategory);
            Assert.AreEqual(false, dbCategory.IsNew);
            Assert.AreEqual(Category.Id, dbCategory.Id);
            Assert.AreEqual(CategoryName.Substring(0, 40), dbCategory.Name);
        }

        /*[Test]
        public void EnumerateAllCategoryTest() {
            string CategoryName1 = string.Empty.PadRight(1000, '1');
            string CategoryName2 = string.Empty.PadRight(1000, '2');
            Category Category1 = BCCategory.NewCategory();
            Category1.Name = CategoryName1;
            Category Category2 = BCCategory.NewCategory();
            Category2.Name = CategoryName2;

            Category[] CategorysBefore = DOCategory.EnumerateCategoryByParam(DeviceType.Undefined, null);
            Assert.IsNotNull(CategorysBefore);

            DOCategory.Add(Category1);
            Assert.IsNotNull(Category1);

            DOCategory.Add(Category2);
            Assert.IsNotNull(Category2);

            Category[] CategorysAfter = DOCategory.EnumerateCategoryByParam(DeviceType.Undefined, null);
            Assert.IsNotNull(CategorysAfter);
            Assert.AreEqual(CategorysBefore.Length + 2, CategorysAfter.Length);
        }

        [Test]
        public void EnumerateCategoryByDeviceTypeTest() {
            string CategoryName1 = string.Empty.PadRight(1000, '1');
            string CategoryName2 = string.Empty.PadRight(1000, '2');

            Category[] CategorysBefore = DOCategory.EnumerateCategoryByParam(DeviceType.CdDvdLabel, null);
            Assert.IsNotNull(CategorysBefore);

            Category Category1 = BCCategory.NewCategory();
            Category1.Name = CategoryName1;

            Category Category2 = BCCategory.NewCategory();
            Category2.Name = CategoryName2;

            DOCategory.Add(Category1);
            Assert.IsNotNull(Category1);

            DOCategory.Add(Category2);
            Assert.IsNotNull(Category2);

            // Devices are not assigned
            Category[] CategorysAfter = DOCategory.EnumerateCategoryByParam(DeviceType.CdDvdLabel, null);
            Assert.IsNotNull(CategorysAfter);
            Assert.AreEqual(CategorysBefore.Length, CategorysAfter.Length);
        }*/

        [Test]
        public void CategoryDeleteTest() {
            string CategoryName = string.Empty.PadRight(1000, 't');
            Category Category = BCCategory.NewCategory();
            Category.Name = CategoryName;

            DOCategory.Add(Category);

            Assert.IsNotNull(Category);
            Assert.AreEqual(false, Category.IsNew);
            Assert.IsTrue(Category.Id > 0);

            DOCategory.Delete(Category);

            Category dbCategory = DOCategory.GetCategory(Category);
            Assert.IsNull(dbCategory);
        }

        [Test]
        public void CategoryUpdateTest() {
            string CategoryName = string.Empty.PadRight(1000, 't');
            Category Category = BCCategory.NewCategory();
            Category.Name = CategoryName;

            DOCategory.Add(Category);
            Assert.IsNotNull(Category);

            string CategoryName2 = string.Empty.PadRight(1000, 'q');
            Category.Name = CategoryName2;

            DOCategory.Update(Category);

            Assert.IsNotNull(Category);
            Assert.AreEqual(false, Category.IsNew);
            Assert.IsTrue(Category.Id > 0);

            Category dbCategory = DOCategory.GetCategory(Category);

            Assert.IsNotNull(dbCategory);
            Assert.AreEqual(false, dbCategory.IsNew);
            Assert.AreEqual(Category.Id, dbCategory.Id);
            Assert.AreEqual(CategoryName2.Substring(0, 40), dbCategory.Name);

            byte[] dbIcon = DOCategory.GetCategoryIcon(Category);
            Assert.AreEqual(0, dbIcon.Length);
        }

        [Test]
        public void CategoryIconUpdateTest() {
            string CategoryName = string.Empty.PadRight(1000, 't');
            Category Category = BCCategory.NewCategory();
            Category.Name = CategoryName;

            DOCategory.Add(Category);
            Assert.IsNotNull(Category);

            byte[] dbIcon = DOCategory.GetCategoryIcon(Category);
            Assert.AreEqual(0, dbIcon.Length);

            byte[] icon = {1, 2, 3};
            DOCategory.UpdateIcon(Category, icon);

            dbIcon = DOCategory.GetCategoryIcon(Category);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }
    }
}