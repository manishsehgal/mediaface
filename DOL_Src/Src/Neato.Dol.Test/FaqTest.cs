using System.Data;
using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Data.Scheme;
using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class FaqTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void EnumFaqTest() {
            FaqData data = BCFaq.EnumFaq();
            Assert.IsNotNull(data);
        }

        [Test]
        public void UpdateInsertFaqTest() {
            FaqData data = BCFaq.EnumFaq();
            int count = data.Faq.Count;

            FaqData.FaqRow row = data.Faq.NewFaqRow();
            row.Question = "Q";
            row.Answer = "A";
            row.SortOrder = 1000;
            data.Faq.AddFaqRow(row);

            DOFaq.UpdateFaq(data);
            FaqData newData = BCFaq.EnumFaq();

            Assert.AreEqual(count+1, newData.Faq.Count);
        }

        [Test]
        public void UpdateUpdateFaqTest() {
            FaqData data = BCFaq.EnumFaq();
            int index = data.Faq.Count;

            FaqData.FaqRow row = data.Faq.NewFaqRow();
            row.Question = "Q";
            row.Answer = "A";
            row.SortOrder = 1000;
            data.Faq.AddFaqRow(row);

            DOFaq.UpdateFaq(data);
            FaqData newData = BCFaq.EnumFaq();

            newData.Faq[index].Question = "Q1";
            newData.Faq[index].Answer = "A1";
            newData.Faq[index].SortOrder = 2000;

            DOFaq.UpdateFaq(newData);
            FaqData newData2 = BCFaq.EnumFaq();

            Assert.AreEqual("Q1", newData2.Faq[index].Question);
            Assert.AreEqual("A1", newData2.Faq[index].Answer);
            Assert.AreEqual(2000, newData2.Faq[index].SortOrder);
        }

        [Test]
        public void UpdateDeleteFaqTest() {
            FaqData data = BCFaq.EnumFaq();

            FaqData.FaqRow row = data.Faq.NewFaqRow();
            row.Question = "Q";
            row.Answer = "A";
            row.SortOrder = 1000;
            data.Faq.AddFaqRow(row);

            DOFaq.UpdateFaq(data);
            FaqData newData = BCFaq.EnumFaq();

            int count = newData.Faq.Count;

            newData.Faq[count-1].Delete();

            DOFaq.UpdateFaq(newData);
            FaqData newData2 = BCFaq.EnumFaq();
            int count2 = newData2.Faq.Count;

            Assert.AreEqual(count-1, count2);
        }
    }
}