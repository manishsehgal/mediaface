using System;
using System.Data;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Entity;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class BCPhoneRequestTest {
        public BCPhoneRequestTest() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        private void CreatePhoneRequest
            (string phoneManufacturer, string phoneModel,
            DateTime created) {

            DeviceRequest request = new DeviceRequest();
            request.FirstName = request.LastName = 
                request.EmailAddress = request.Carrier = 
                request.Comment = "";
            
            request.DeviceBrand = phoneManufacturer;
            request.Device = phoneModel;
            request.Created = created;
            
            DODeviceRequest.InsertPhoneRequest(request);
        }


        private void CreatePhoneRequest
            (string phoneManufacturer, string phoneModel,
            DateTime created, string emailAddress) {

            DeviceRequest request = new DeviceRequest();
            request.FirstName = request.LastName = 
                request.Carrier = request.Comment = "";
            
            request.DeviceBrand = phoneManufacturer;
            request.Device = phoneModel;
            request.Created = created;
            request.EmailAddress = emailAddress;
            DODeviceRequest.InsertPhoneRequest(request);
        }

        private void DeleteExistingRequests() {
            foreach (DeviceRequest request in BCDeviceRequest.GetPhoneRequests()) {
                DODeviceRequest.DeletePhoneRequest(request.RequestDeviceId);               
            }
        }


        [Test]
        public void GetPhoneRequestReportByDatesAndBrandTest() {
            DeleteExistingRequests();

            //Insert new requests
            CreatePhoneRequest("Nokia", "3310", new DateTime(2001, 1, 1));
            CreatePhoneRequest("Nokia", "3310", new DateTime(2003, 1, 1));
            CreatePhoneRequest("Motorola", "v180", new DateTime(2001, 1, 1));
            CreatePhoneRequest("Nakia", "3310", new DateTime(2001, 1, 1));
            CreatePhoneRequest("Nukia", "3310", new DateTime(2001, 1, 1));

            //test all
            DataSet data = BCDeviceRequest.
                GetPhoneRequestReportByDatesAndBrand(null, false,
                new DateTime(2000, 1, 1), new DateTime(2005, 1, 1));
            Assert.AreEqual(4, data.Tables[0].Rows.Count);
            Assert.AreEqual("Nokia 3310", data.Tables[0].Rows[0]["Model name"]);
            Assert.AreEqual(2, data.Tables[0].Rows[0]["Requests"]);
            Assert.AreEqual("Motorola v180", data.Tables[0].Rows[1]["Model name"]);
            Assert.AreEqual(1, data.Tables[0].Rows[1]["Requests"]);
            Assert.AreEqual("Nakia 3310", data.Tables[0].Rows[2]["Model name"]);
            Assert.AreEqual(1, data.Tables[0].Rows[2]["Requests"]);
            Assert.AreEqual("Nukia 3310", data.Tables[0].Rows[3]["Model name"]);
            Assert.AreEqual(1, data.Tables[0].Rows[3]["Requests"]);

            //test Nokia
            DataSet data2 = BCDeviceRequest.
                GetPhoneRequestReportByDatesAndBrand("Nokia", false,
                new DateTime(2000, 1, 1), new DateTime(2005, 1, 1));
            Assert.AreEqual(1, data2.Tables[0].Rows.Count);
            Assert.AreEqual("Nokia 3310", data2.Tables[0].Rows[0]["Model name"]);
            Assert.AreEqual(2, data2.Tables[0].Rows[0]["Requests"]);

            //test Others Brands
            DataSet data3 = BCDeviceRequest.
                GetPhoneRequestReportByDatesAndBrand("", true,
                new DateTime(2000, 1, 1), new DateTime(2005, 1, 1));
            Assert.AreEqual(4, data3.Tables[0].Rows.Count);
            Assert.AreEqual("Nokia 3310", data3.Tables[0].Rows[0]["Model name"]);
            Assert.AreEqual(2, data3.Tables[0].Rows[0]["Requests"]);
            Assert.AreEqual("Motorola v180", data3.Tables[0].Rows[1]["Model name"]);
            Assert.AreEqual(1, data3.Tables[0].Rows[1]["Requests"]);

            //test date range + Nokia
            DataSet data4 = BCDeviceRequest.
                GetPhoneRequestReportByDatesAndBrand("Nokia", false,
                new DateTime(2001, 1, 1), new DateTime(2002, 1, 1));
            Assert.AreEqual(1, data4.Tables[0].Rows.Count);
            Assert.AreEqual("Nokia 3310", data4.Tables[0].Rows[0]["Model name"]);
            Assert.AreEqual(1, data4.Tables[0].Rows[0]["Requests"]);
        }


        [Test]
        public void GetPhoneRequestEmailsReportByDatesTest() {
            DeleteExistingRequests();

            //Insert new requests
            CreatePhoneRequest("Nokia", "3310", new DateTime(2001, 1, 1), "user1@yahoo.com");
            CreatePhoneRequest("Nokia", "3310", new DateTime(2003, 1, 1), "user1@yahoo.com");
            CreatePhoneRequest("Motorola", "v180", new DateTime(2001, 1, 1), "user1@yahoo.com");
            CreatePhoneRequest("Nakia", "3310", new DateTime(2001, 1, 1), "user2@yahoo.com");
            CreatePhoneRequest("Nukia", "3310", new DateTime(2001, 1, 1), "user3@yahoo.com");

            DataSet data = BCDeviceRequest.
                GetPhoneRequestEmailsReportByDates
                (new DateTime(2000, 1, 1), new DateTime(2002, 1, 1));
            Assert.AreEqual(4, data.Tables[0].Rows.Count);
            Assert.AreEqual("Motorola v180", data.Tables[0].Rows[0]["Model name"]);
            Assert.AreEqual("user1@yahoo.com", data.Tables[0].Rows[0]["Email"]);
            Assert.AreEqual("Nakia 3310", data.Tables[0].Rows[1]["Model name"]);
            Assert.AreEqual("user2@yahoo.com", data.Tables[0].Rows[1]["Email"]);
            Assert.AreEqual("Nokia 3310", data.Tables[0].Rows[2]["Model name"]);
            Assert.AreEqual("user1@yahoo.com", data.Tables[0].Rows[2]["Email"]);
            Assert.AreEqual("Nukia 3310", data.Tables[0].Rows[3]["Model name"]);
            Assert.AreEqual("user3@yahoo.com", data.Tables[0].Rows[3]["Email"]);
        }
    }
}