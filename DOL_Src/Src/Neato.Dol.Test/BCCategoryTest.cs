using System.Data;
using System.IO;
using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Entity;
using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class BCCategoryTest {
        public BCCategoryTest() {}

        #region SetUp/TearDown

        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }

        #endregion

        [Test]
        public void NewCategoryTest() {
            Category Category = BCCategory.NewCategory();
            Assert.IsNotNull(Category);
            Assert.AreEqual(0, Category.Id);
            Assert.AreEqual(string.Empty, Category.Name);
        }

        [Test]
        public void SaveNewCategoryWithoutIconTest() {
            Category Category = BCCategory.NewCategory();
            Category.Name = string.Empty.PadRight(10, '1');
            BCCategory.Save(Category);

            Assert.IsTrue(Category.Id > 0);

            Category dbCategory = DOCategory.GetCategory(Category);

            Assert.IsNotNull(dbCategory);
            Assert.AreEqual(Category.Id, dbCategory.Id);
            Assert.AreEqual(Category.Name, dbCategory.Name);

            byte[] icon = BCCategory.GetCategoryIcon(Category);
            Assert.IsNotNull(icon);
            Assert.AreEqual(0, icon.Length);
        }

        [Test]
        public void SaveNewCategoryWithIconTest() {
            Category Category = BCCategory.NewCategory();
            Category.Name = string.Empty.PadRight(10, '1');

            byte[] icon = {1, 2, 3};

            BCCategory.Save(Category, new MemoryStream(icon));

            Assert.IsTrue(Category.Id > 0);

            Category dbCategory = DOCategory.GetCategory(Category);

            Assert.IsNotNull(dbCategory);
            Assert.AreEqual(Category.Id, dbCategory.Id);
            Assert.AreEqual(Category.Name, dbCategory.Name);

            byte[] dbIcon = BCCategory.GetCategoryIcon(Category);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        public void SaveExistingCategoryWithoutIconTest() {
            string CategoryName1 = string.Empty.PadRight(10, '1');
            string CategoryName2 = string.Empty.PadRight(10, '2');
            byte[] icon = {1, 2, 3};

            Category Category = BCCategory.NewCategory();
            Category.Name = CategoryName1;
            BCCategory.Save(Category, new MemoryStream(icon));

            Category.Name = CategoryName2;
            BCCategory.Save(Category);

            Category dbCategory = DOCategory.GetCategory(Category);

            Assert.IsNotNull(dbCategory);
            Assert.AreEqual(Category.Id, dbCategory.Id);
            Assert.AreEqual(CategoryName2, dbCategory.Name);

            byte[] dbIcon = BCCategory.GetCategoryIcon(Category);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        public void SaveExistingCategoryWithIconTest() {
            string CategoryName1 = string.Empty.PadRight(10, '1');
            string CategoryName2 = string.Empty.PadRight(10, '2');

            byte[] icon1 = {1, 2, 3};
            byte[] icon2 = {4, 5, 6, 7};

            Category Category = BCCategory.NewCategory();
            Category.Name = CategoryName1;

            BCCategory.Save(Category, new MemoryStream(icon1));

            Category.Name = CategoryName2;
            BCCategory.Save(Category, new MemoryStream(icon2));

            Category dbCategory = DOCategory.GetCategory(Category);

            Assert.IsNotNull(dbCategory);
            Assert.AreEqual(Category.Id, dbCategory.Id);
            Assert.AreEqual(CategoryName2, dbCategory.Name);

            byte[] dbIcon = BCCategory.GetCategoryIcon(Category);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon2.Length, dbIcon.Length);
            for (int index = 0; index < icon2.Length; index++) {
                Assert.AreEqual(icon2[index], dbIcon[index]);
            }
        }

        [Test]
        public void ChangeIconTest() {
            Category Category = BCCategory.NewCategory();
            Category.Name = string.Empty.PadRight(10, '1');
            byte[] icon1 = {1, 2, 3};
            byte[] icon2 = {4, 5, 6, 7};

            BCCategory.Save(Category, new MemoryStream(icon1));

            BCCategory.ChangeIcon(Category, null);

            byte[] dbIcon = BCCategory.GetCategoryIcon(Category);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon1.Length, dbIcon.Length);
            for (int index = 0; index < icon1.Length; index++) {
                Assert.AreEqual(icon1[index], dbIcon[index]);
            }

            BCCategory.ChangeIcon(Category, new MemoryStream(0));

            dbIcon = BCCategory.GetCategoryIcon(Category);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon1.Length, dbIcon.Length);
            for (int index = 0; index < icon1.Length; index++) {
                Assert.AreEqual(icon1[index], dbIcon[index]);
            }

            BCCategory.ChangeIcon(Category, new MemoryStream(icon2));

            dbIcon = BCCategory.GetCategoryIcon(Category);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon2.Length, dbIcon.Length);
            for (int index = 0; index < icon2.Length; index++) {
                Assert.AreEqual(icon2[index], dbIcon[index]);
            }
        }

        [Test]
        public void GetCategoryIconTest() {
            Category Category = BCCategory.NewCategory();
            Category.Name = string.Empty.PadRight(10, '1');
            byte[] icon = {1, 2, 3};

            BCCategory.Save(Category, new MemoryStream(icon));

            byte[] dbIcon = BCCategory.GetCategoryIcon(Category);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        [Ignore("Not implemented")]
        public void AddRemoveDeviceFromCategoryTest() {}

        [Test]
        public void GetCategoryListTest() {
            Category[] categories = BCCategory.GetCategoryList();
            Assert.IsNotNull(categories);
        }

        [Test]
        public void GetDeviceCategoriesTest() {
            Category[] categories = BCCategory.GetDeviceCategories();
            Assert.IsNotNull(categories);

            bool foundMP3PlayerCategory = false;
            foreach (Category category in categories) {
                if (category.Id == (int) DeviceType.MP3Player) {
                    foundMP3PlayerCategory = true;
                    break;
                }
            }
            Assert.IsTrue(foundMP3PlayerCategory);

            Category newCategory = BCCategory.NewCategory();
            newCategory.Name = string.Empty.PadRight(10, '1');
            BCCategory.Save(newCategory);

            Category[] categoriesDB = BCCategory.GetDeviceCategories();
            Assert.AreEqual(categories.Length + 1, categoriesDB.Length);

            bool foundNewCategory = false;
            foreach (Category categoryDb in categoriesDB) {
                if (categoryDb.Name == newCategory.Name) {
                    foundNewCategory = true;
                    break;
                }
            }
            Assert.IsTrue(foundNewCategory);
        }

        [Test]
        public void DeleteTest() {
            Category Category = BCCategory.NewCategory();
            Category.Name = string.Empty.PadRight(10, '1');

            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);

            Device device = BCDevice.NewDevice();
            device.Model = string.Empty.PadRight(10, '2');
            device.Brand = brand;
            device.DeviceTypeId = (int)DeviceType.CdDvdLabel;
            device.Rating = 9;

            BCCategory.Save(Category);
            BCDevice.Save(device);
            //BCDevice.AddToCategory(device, Category);

            BCCategory.Delete(Category);

            Category = DOCategory.GetCategory(Category);

            Assert.IsNull(Category);
        }

    }
}