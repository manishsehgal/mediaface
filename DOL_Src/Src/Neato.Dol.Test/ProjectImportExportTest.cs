using System.Data;
using System.IO;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class ProjectImportExportTest {
        public ProjectImportExportTest() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        [ExpectedException(typeof(BusinessException), "Wrong file structure. Please, select another one.")]
        public void ImportNullStreamTest() {
            Customer customer = null;
            BCProject.Import(customer, MemoryStream.Null);
        }

        [Test]
        [ExpectedException(typeof(BusinessException), "Wrong file structure. Please, select another one.")]
        public void ImportEmptyStreamTest() {
            Customer customer = null;
            BCProject.Import(customer, new MemoryStream(0));
        }

        [Test]
        [ExpectedException(typeof(BusinessException), "Wrong file structure. Please, select another one.")]
        public void ImportCorruptedTest() {
            Customer customer = null;
            MemoryStream data = new MemoryStream();
            byte[] buffer = new byte[] {0, 1, 2, 3, 4, 5, 6, 7};
            data.Write(buffer, 0, buffer.Length);
            BCProject.Import(customer, data);
        }

        [Test]
        public void CorrectExportImportWithoutCustomerUpdateTest() {
            Customer customer = null;
            PaperBase paper = DOPaper.EnumeratePaperByParam(null, null, null, null, null, null, null, null, null, -1, -1)[0];
            Project project = BCProject.GetProject(customer, paper);
            Stream data = BCProject.Export(project);
            Project importProject = BCProject.Import(customer, data);
            Assert.AreEqual(project.ProjectXml.OuterXml, importProject.ProjectXml.OuterXml);
            Assert.AreEqual(project.Size, importProject.Size);
            Assert.AreEqual(project.ProjectPaper, importProject.ProjectPaper);
        }

    }
}
