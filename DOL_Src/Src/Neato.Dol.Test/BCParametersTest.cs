using System;
using System.Data;
using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity.Exceptions;
using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class BCParametersTest {
        public BCParametersTest() {}

        #region SetUp/TearDown

        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }

        #endregion

        [Test]
        public void EnumParametersTest() {
            ParametersData data = BCParameters.EnumParameters();
            Assert.IsNotNull(data);
            Assert.AreEqual(2, data.Parameters.Count);

            DataRow[] rows = data.Parameters.Select("Key = " + "'NeatoDolBuyAccountUrl'");
            Assert.AreEqual(1, rows.Length);

            rows = data.Parameters.Select("Key = " + "'DefaultPageKeywords'");
            Assert.AreEqual(1, rows.Length);
        }

        [Test, ExpectedException(typeof(BusinessException), "Parameter with key 'KeyParamForUT' absent in DB")]
        public void GetParameterByKeyInvalid() {
            string key = "KeyParamForUT";
            BCParameters.GetParameterByKey(key);
        }

        [Test]
        public void GetParameterByKey() {
            string value = BCParameters.GetParameterByKey("NeatoDolBuyAccountUrl");
            Assert.IsNotNull(value);
        }

        [Test]
        public void UpdateParameters() {
            ParametersData data = BCParameters.EnumParameters();
            Assert.IsTrue(data.Parameters.Count > 0);

            string key = data.Parameters[0].Key;
            string value = data.Parameters[0].Value;
            string newValue = Guid.NewGuid().ToString();

            data.Parameters[0].Value = newValue;
            Assert.AreEqual(DataRowState.Modified, data.Parameters[0].RowState);
            BCParameters.UpdateParameters(data);

            string newValueDB = BCParameters.GetParameterByKey(key);
            Assert.AreEqual(newValue, newValueDB);
            Assert.IsTrue(value != newValueDB);
        }
    }
}