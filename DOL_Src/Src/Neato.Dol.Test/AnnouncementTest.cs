using System;
using System.Data;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Entity.DataBase;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class AnnouncementTest {
        private const string initialText = "Initial - (Test)";
        private const string updatedText = "Updated - (Test)";
        
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void EnumTest() {
            AnnouncementData data = BCAnnouncement.EnumAnnouncements();
            Assert.IsNotNull(data);
        }

        [Test]
        public void CreateTest() {
            AnnouncementData data = BCAnnouncement.EnumAnnouncements();
            Assert.IsNotNull(data);
            int count = data.Announcement.Count;

            data = NewAnnouncement(initialText, true);

            AnnouncementData newData = BCAnnouncement.EnumAnnouncements();            
            Assert.AreEqual(count+1, newData.Announcement.Count);
        }

        [Test]
        public void UpdateTest() {
            AnnouncementData data = NewAnnouncement(initialText, true);
            data = BCAnnouncement.GetLastAnnouncements();
            Assert.AreEqual(data.Announcement.Count, 1);

            AnnouncementData.AnnouncementRow row = (AnnouncementData.AnnouncementRow)data.Announcement.Rows[0];
            Assert.AreEqual(row.Text, initialText);
            Assert.AreEqual(row.IsActive, true);

            row.Text = updatedText;
            BCAnnouncement.UpdateAnnouncement(data);

            data = BCAnnouncement.GetLastAnnouncements();
            Assert.IsNotNull(data);
            Assert.AreEqual(data.Announcement.Count, 1);

            row = (AnnouncementData.AnnouncementRow)data.Announcement.Rows[0];
            Assert.AreEqual(row.Text, updatedText);
            Assert.AreEqual(row.IsActive, true);
        }

        [Test]
        public void GetLastAnnouncements() {
            AnnouncementData data = NewAnnouncement(updatedText, true);
            data = NewAnnouncement(initialText, false);

            data = BCAnnouncement.GetLastAnnouncements();
            AnnouncementData.AnnouncementRow row = (AnnouncementData.AnnouncementRow)data.Announcement.Rows[0];
            Assert.IsTrue(row.Text != initialText);
            Assert.AreEqual(row.Text, updatedText);
            Assert.AreEqual(row.IsActive, true);
        }

        [Test]
        public void DeleteTest() {
            AnnouncementData data = NewAnnouncement(updatedText, true);
            data = BCAnnouncement.EnumAnnouncements();
            int count = data.Announcement.Count;

            data = BCAnnouncement.GetLastAnnouncements();
            Assert.IsNotNull(data);
            Assert.AreEqual(data.Announcement.Count, 1);

            AnnouncementData.AnnouncementRow row = (AnnouncementData.AnnouncementRow)data.Announcement.Rows[0];
            row.Delete();
            BCAnnouncement.UpdateAnnouncement(data);
            AnnouncementData newData = BCAnnouncement.EnumAnnouncements();
            
            Assert.AreEqual(count-1, newData.Announcement.Count);
        }


        #region Support
        private static AnnouncementData NewAnnouncement(string text, bool isActive) {
            AnnouncementData data = new AnnouncementData();
            AnnouncementData.AnnouncementRow row = data.Announcement.NewAnnouncementRow();
            row.Text = text;
            row.Time = DateTime.Now;
            row.IsActive = isActive;
            data.Announcement.AddAnnouncementRow(row);
            BCAnnouncement.UpdateAnnouncement(data);

            return data;
        }
        #endregion
    }
}