using System.Data;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Entity;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class DOPaperBrandTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void PaperBrandInsertTest() {
            string brandName = string.Empty.PadRight(1000, 't');
            PaperBrand paperBrand = BCPaperBrand.NewPaperBrand();
            paperBrand.Name = brandName;

            DOPaperBrand.Add(paperBrand);

            Assert.IsNotNull(paperBrand);
            Assert.AreEqual(false, paperBrand.IsNew);
            Assert.IsTrue(paperBrand.Id > 0);

            paperBrand = DOPaperBrand.GetPaperBrand(paperBrand);

            Assert.IsNotNull(paperBrand);
            Assert.AreEqual(false, paperBrand.IsNew);
            Assert.IsTrue(paperBrand.Id > 0);
            Assert.AreEqual(brandName.Substring(0, 30), paperBrand.Name);

            byte[] dbIcon = DOPaperBrand.GetPaperBrandIconByID(paperBrand);
            Assert.AreEqual(0, dbIcon.Length);

        }

        [Test]
        public void PaperBrandGetByIdTest() {
            string brandName = string.Empty.PadRight(1000, 't');
            PaperBrand paperBrand = BCPaperBrand.NewPaperBrand();
            paperBrand.Name = brandName;

            DOPaperBrand.Add(paperBrand);

            Assert.IsNotNull(paperBrand);
            Assert.AreEqual(false, paperBrand.IsNew);
            Assert.IsTrue(paperBrand.Id > 0);

            PaperBrand dbPaperBrand = DOPaperBrand.GetPaperBrand(paperBrand);

            Assert.IsNotNull(dbPaperBrand);
            Assert.AreEqual(false, paperBrand.IsNew);
            Assert.AreEqual(paperBrand.Id, dbPaperBrand.Id);
            Assert.AreEqual(brandName.Substring(0, 30), dbPaperBrand.Name);
        }

        [Test]
        public void EnumerateAllPaperBrandTest() {
            string brandName1 = string.Empty.PadRight(1000, '1');
            string brandName2 = string.Empty.PadRight(1000, '2');
            PaperBrand brand1 = BCPaperBrand.NewPaperBrand();
            brand1.Name = brandName1;
            PaperBrand brand2 = BCPaperBrand.NewPaperBrand();
            brand2.Name = brandName2;

            PaperBrand[] brandsBefore = DOPaperBrand.EnumeratePaperBrands();
            Assert.IsNotNull(brandsBefore);

            DOPaperBrand.Add(brand1);
            Assert.IsNotNull(brand1);

            DOPaperBrand.Add(brand2);
            Assert.IsNotNull(brand2);

            PaperBrand[] brandsAfter = DOPaperBrand.EnumeratePaperBrands();
            Assert.IsNotNull(brandsAfter);
            Assert.AreEqual(brandsBefore.Length + 2, brandsAfter.Length);
        }

        [Test]
        public void EnumeratePaperBrandByDeviceTypeTest() {
            string brandName1 = string.Empty.PadRight(1000, '1');
            string brandName2 = string.Empty.PadRight(1000, '2');

            PaperBrand[] brandsBefore = DOPaperBrand.EnumeratePaperBrandsByDeviceType((int)DeviceType.CdDvdLabel);
            Assert.IsNotNull(brandsBefore);

            PaperBrand brand1 = BCPaperBrand.NewPaperBrand();
            brand1.Name = brandName1;

            PaperBrand brand2 = BCPaperBrand.NewPaperBrand();
            brand2.Name = brandName2;

            DOPaperBrand.Add(brand1);
            Assert.IsNotNull(brand1);

            DOPaperBrand.Add(brand2);
            Assert.IsNotNull(brand2);

            // Devices are not assigned
            PaperBrand[] brandsAfter = DOPaperBrand.EnumeratePaperBrandsByDeviceType((int)DeviceType.CdDvdLabel);
            Assert.IsNotNull(brandsAfter);
            Assert.AreEqual(brandsBefore.Length, brandsAfter.Length);
        }

        [Test]
        public void PaperBrandDeleteTest() {
            string PaperBrandName = string.Empty.PadRight(1000, 't');
            PaperBrand paperBrand = BCPaperBrand.NewPaperBrand();
            paperBrand.Name = PaperBrandName;

            DOPaperBrand.Add(paperBrand);

            Assert.IsNotNull(paperBrand);
            Assert.AreEqual(false, paperBrand.IsNew);
            Assert.IsTrue(paperBrand.Id > 0);

            DOPaperBrand.Delete(paperBrand);

            PaperBrand dbPaperBrand = DOPaperBrand.GetPaperBrand(paperBrand);
            Assert.IsNull(dbPaperBrand);
        }

        [Test]
        public void PaperBrandUpdateTest() {
            string brandName = string.Empty.PadRight(1000, 't');
            PaperBrand paperBrand = BCPaperBrand.NewPaperBrand();
            paperBrand.Name = brandName;

            DOPaperBrand.Add(paperBrand);
            Assert.IsNotNull(paperBrand);

            string brandName2 = string.Empty.PadRight(1000, 'q');
            paperBrand.Name = brandName2;

            DOPaperBrand.Update(paperBrand);

            Assert.IsNotNull(paperBrand);
            Assert.AreEqual(false, paperBrand.IsNew);
            Assert.IsTrue(paperBrand.Id > 0);

            PaperBrand dbPaperBrand = DOPaperBrand.GetPaperBrand(paperBrand);

            Assert.IsNotNull(dbPaperBrand);
            Assert.AreEqual(false, paperBrand.IsNew);
            Assert.AreEqual(paperBrand.Id, dbPaperBrand.Id);
            Assert.AreEqual(brandName2.Substring(0, 30), dbPaperBrand.Name);

            byte[] dbIcon = DOPaperBrand.GetPaperBrandIconByID(paperBrand);
            Assert.AreEqual(0, dbIcon.Length);
        }

        [Test]
        public void PaperBrandIconUpdateTest() {
            string brandName = string.Empty.PadRight(1000, 't');
            PaperBrand paperBrand = BCPaperBrand.NewPaperBrand();
            paperBrand.Name = brandName;

            DOPaperBrand.Add(paperBrand);
            Assert.IsNotNull(paperBrand);

            byte[] dbIcon = DOPaperBrand.GetPaperBrandIconByID(paperBrand);
            Assert.AreEqual(0, dbIcon.Length);

            byte[] icon = {1, 2, 3};
            DOPaperBrand.UpdateIcon(paperBrand, icon);

            dbIcon = DOPaperBrand.GetPaperBrandIconByID(paperBrand);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }
    }
}