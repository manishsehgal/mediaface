using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class ImageLibraryTest {
        private string imagePath = "..\\..\\ImageLibTestData\\MainForUTests";

        public ImageLibraryTest() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion
        
        private static ImageLibraryData GenerateTestData(bool insertInDifferentCultures) {
            ImageLibraryData data = new ImageLibraryData();

            //insert parent folder
            ImageLibraryData.FolderRow parentFolder = 
                data.Folder.NewFolderRow();
            data.Folder.AddFolderRow(parentFolder);
    
            string culture = (insertInDifferentCultures) ? "en" : "";
            data.FolderLocalization.AddFolderLocalizationRow(
                parentFolder, culture, "Parent");
    
            //insert items for parent folder
            ImageLibraryData.LibraryItemRow item = 
                data.LibraryItem.AddLibraryItemRow(new byte[]{0xAA});
    
            data.FolderItem.AddFolderItemRow(parentFolder, item);
    
            //insert child folder
            ImageLibraryData.FolderRow childFolder = 
                data.Folder.AddFolderRow(parentFolder, "", 0, (int)ImageLibraries.Expanded);
    
            data.FolderLocalization.AddFolderLocalizationRow(
                childFolder, "", "Child");
    
            //insert items for child folder
            ImageLibraryData.LibraryItemRow item2 = 
                data.LibraryItem.AddLibraryItemRow(new byte[]{0xBB});
    
            data.FolderItem.AddFolderItemRow(childFolder, item2);
    
            ImageLibraryData.LibraryItemRow item3 = 
                data.LibraryItem.AddLibraryItemRow(new byte[]{0xCC});
    
            data.FolderItem.AddFolderItemRow(childFolder, item3);

            return data;
        }

        private static ImageLibraryData InsertTestData
            (bool insertInDifferentCultures) {
            ImageLibraryData data =
                GenerateTestData(insertInDifferentCultures);
            DOImageLibrary.UpdateImageLibrary(data, true);

            return data;
        }

        [Test]
        public void GetImageLibraryTest() {
            string culture = Thread.CurrentThread.CurrentUICulture.Name;
			Library library = DOImageLibrary.GetImageLibrary(culture, ImageLibraries.Expanded);
            Assert.IsTrue(library.XmlData.ChildNodes.Count > 0);
        }

        [Test, Ignore("Image library is empty")]
        public void GetImageTest() {
            byte[] doImageArray = DOImageLibrary.GetImage(1);
            byte[] bcImageArray = BCImageLibrary.GetImage(1);

            Image doImage = ImageHelper.GetImage(doImageArray);
            Image bcImage = ImageHelper.GetImage(bcImageArray);

            Assert.AreEqual(ImageFormat.Jpeg, doImage.RawFormat);
            Assert.AreEqual(ImageFormat.Jpeg, bcImage.RawFormat);
        }

        [Test, Ignore("Image library is empty")]
        public void GetImageIconTest() {
            byte[] imageArray = BCImageLibrary.GetImage(10);
            byte[] iconArray = BCImageLibrary.GetImageIcon(10, 50, 50);
            Image image = ImageHelper.GetImage(imageArray);
            Image icon = ImageHelper.GetImage(iconArray);
            Assert.AreEqual(ImageFormat.Jpeg, image.RawFormat);
            Assert.AreEqual(ImageFormat.Jpeg, icon.RawFormat);
            Assert.IsTrue(icon.Width <= 50);
            Assert.IsTrue(icon.Height <= 50);
        }

        [Test]
        public void InsertImageLibFolderTest() {
            ImageLibraryData data = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);
            int count = data.Folder.Count;

            //insert parent
            int parentFolderId = BCImageLibrary.InsertImageLibFolder(
                null, "Parent", "en", ImageLibraries.Expanded);

            ImageLibraryData newData = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);

            Assert.AreEqual(count + 1, newData.Folder.Count);
            Assert.IsTrue(newData.Folder[count].IsParentFolderIdNull());
            Assert.AreEqual("Parent", newData.Folder[count].GetFolderLocalizationRows()[0].Caption);
            Assert.AreEqual("en", newData.Folder[count].GetFolderLocalizationRows()[0].Culture);
            Assert.AreEqual(newData.Folder[count].FolderId, newData.Folder[count].SortOrder);
            //insert child
            BCImageLibrary.InsertImageLibFolder(parentFolderId, "Child", "", ImageLibraries.Expanded);

            ImageLibraryData newData2 = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);

            Assert.AreEqual(count + 2, newData2.Folder.Count);
            Assert.IsFalse(newData2.Folder[count + 1].IsParentFolderIdNull());
            Assert.AreEqual(parentFolderId, newData2.Folder[count + 1].ParentFolderId);
            Assert.AreEqual("Child", newData2.Folder[count + 1].GetFolderLocalizationRows()[0].Caption);
            Assert.AreEqual("", newData2.Folder[count + 1].GetFolderLocalizationRows()[0].Culture);
            Assert.AreEqual(1, newData2.Folder[count].GetFolderRows().Length);
            Assert.AreEqual(newData2.Folder[count + 1].FolderId, newData2.Folder[count + 1].SortOrder);
        }

        [Test]
        public void UpdateImageLibFolderTest() {
            ImageLibraryData data = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);
            int count = data.Folder.Count;

            int folderId = BCImageLibrary.InsertImageLibFolder(null, "Parent", "en", ImageLibraries.Expanded);

            ImageLibraryData newData = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);
            Assert.AreEqual(count + 1, newData.Folder.Count);

            BCImageLibrary.UpdateImageLibFolder(folderId, "Parent2", "en");

            ImageLibraryData newData2 = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);

            Assert.AreEqual(count + 1, newData2.Folder.Count);
            Assert.AreEqual("Parent2", newData2.Folder[count].GetFolderLocalizationRows()[0].Caption);
            Assert.AreEqual("en", newData2.Folder[count].GetFolderLocalizationRows()[0].Culture);
        }

        [Test]
        public void DeleteImageLibFolderTest() {
            ImageLibraryData data = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);
            int count = data.Folder.Count;

            int folderId = BCImageLibrary.InsertImageLibFolder(null, "Parent", "en", ImageLibraries.Expanded);

            ImageLibraryData newData = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);
            Assert.AreEqual(count + 1, newData.Folder.Count);

            BCImageLibrary.DeleteImageLibFolder(folderId);

            ImageLibraryData newData2 = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);
            Assert.AreEqual(count, newData2.Folder.Count);
        }

        [Test]
        public void ImageLibItemEnumByFolderIdTest() {
            int parentFolderId = BCImageLibrary.InsertImageLibFolder(
                null, "Parent", "en", ImageLibraries.Expanded);

            ImageLibItem[] imagesZeroFolder =
                BCImageLibrary.ImageLibItemEnumByFolderId(parentFolderId);

            Assert.AreEqual(0, imagesZeroFolder.Length);

            /*ImageLibItem[] imagesExistingFolder =
                BCImageLibrary.ImageLibItemEnumByFolderId(1);

            Assert.IsTrue(imagesExistingFolder.Length > 0);*/
        }

        [Test]
        public void UpdateImageLibraryInsertTest() {
            InsertTestData(true);
            ImageLibraryData newData = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);

            ImageLibraryData.FolderRow parentFolder = null;
            foreach (ImageLibraryData.FolderRow folder in newData.Folder.Rows) {
                if (folder.GetFolderLocalizationRows().Length > 0 &&
                    folder.GetFolderLocalizationRows()[0].Caption == 
                    "Parent") {
                    parentFolder = folder;
                    break;
                }
            }

            int childFolderId = DOImageLibrary.
                GetImageLibFolderIdByCaption("Child");
            ImageLibraryData.FolderRow childFolder = 
                newData.Folder.FindByFolderId(childFolderId);

            //test parent folder
            Assert.IsTrue(parentFolder.IsParentFolderIdNull());
            Assert.AreEqual("Parent", parentFolder.
                GetFolderLocalizationRows()[0].Caption);
            Assert.AreEqual("en", parentFolder.
                GetFolderLocalizationRows()[0].Culture);

            //test items for parent folder
            Assert.AreEqual(1, parentFolder.
                GetFolderItemRows().Length);
            byte[] image =  BCImageLibrary.GetImage(
                parentFolder.GetFolderItemRows()[0].ItemId);
            Assert.AreEqual(0xAA, image[0]);

            //test child folder
            Assert.IsFalse(childFolder.IsParentFolderIdNull());
            Assert.AreEqual("Child", childFolder.
                GetFolderLocalizationRows()[0].Caption);
            Assert.AreEqual("", childFolder.
                GetFolderLocalizationRows()[0].Culture);

            //test items for child folder
            Assert.AreEqual(2, childFolder.GetFolderItemRows().Length);
            byte[] image2 =  BCImageLibrary.GetImage(
                childFolder.GetFolderItemRows()[0].ItemId);
            Assert.AreEqual(0xBB, image2[0]);
            byte[] image3 =  BCImageLibrary.GetImage(
                childFolder.GetFolderItemRows()[1].ItemId);
            Assert.AreEqual(0xCC, image3[0]);
        }
        [Test]
        public void UpdateImageLibraryDeleteTest() {
            InsertTestData(false);
            ImageLibraryData newData = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);

            int folderCount = newData.Folder.Count;
            int folderLocalizationCount = newData.FolderLocalization.Count;
            int libraryItemCount = newData.LibraryItem.Count;
            int folderItemCount = newData.FolderItem.Count;

            //Delete inserted rows
            int parentFolderId = DOImageLibrary.
                GetImageLibFolderIdByCaption("Parent");

            ImageLibraryData.FolderRow parentFolder = 
                newData.Folder.FindByFolderId(parentFolderId);

            parentFolder.Delete();
            DOImageLibrary.UpdateImageLibrary(newData, true);

            ImageLibraryData newData2 = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);
            
            //test counts
            Assert.AreEqual(folderCount - 2, newData2.Folder.Count);
            Assert.AreEqual(folderLocalizationCount - 2, newData2.FolderLocalization.Count);
            Assert.AreEqual(libraryItemCount - 3, newData2.LibraryItem.Count);
            Assert.AreEqual(folderItemCount - 3, newData2.FolderItem.Count);
        }

        [Test]
        public void GetImageLibFolderIdByCaptionTest() {
            int invalidFolderId = DOImageLibrary.
                GetImageLibFolderIdByCaption("qjdu3fah5s7agr24");
            Assert.AreEqual(int.MinValue, invalidFolderId);

            int existingFolderId = BCImageLibrary.
                InsertImageLibFolder(null, "Parent", "", ImageLibraries.Expanded);

            int validFolderId = DOImageLibrary.
                GetImageLibFolderIdByCaption("Parent");
            Assert.AreEqual(existingFolderId, validFolderId);
        }
        [Test]
        public void IsFolderRecursiveParentTest() {
            ImageLibraryData data = InsertTestData(false);

            //insert sub-child folder
            ImageLibraryData.FolderRow subChildFolder = 
                data.Folder.AddFolderRow
                (data.Folder[data.Folder.Count - 1], "", 0, (int)ImageLibraries.Expanded);
    
            data.FolderLocalization.AddFolderLocalizationRow(
                subChildFolder, "", "SubChild");

            int parentFolderId = DOImageLibrary.
                GetImageLibFolderIdByCaption("Parent");

            int childFolderId = DOImageLibrary.
                GetImageLibFolderIdByCaption("Child");

            int subChildFolderId = subChildFolder.FolderId;

            Assert.IsFalse(DOImageLibrary.IsFolderRecursiveParent
                (int.MinValue, subChildFolderId, data));
            Assert.IsTrue(DOImageLibrary.IsFolderRecursiveParent
                (childFolderId, subChildFolderId, data));
            Assert.IsTrue(DOImageLibrary.IsFolderRecursiveParent
                (parentFolderId, subChildFolderId, data));
        }
        [Test]
        public void GetFolderContentTest() {
            InsertTestData(false);

            int parentFolderId = DOImageLibrary.
                GetImageLibFolderIdByCaption("Parent");

            ImageLibraryData newData = DOImageLibrary.
                GetFolderContent(parentFolderId);

            //test counts
            Assert.AreEqual(2, newData.Folder.Count);
            Assert.AreEqual(2, newData.FolderLocalization.Count);
            Assert.AreEqual(3, newData.LibraryItem.Count);
            Assert.AreEqual(3, newData.FolderItem.Count);        
        }
        [Test]
        public void DeleteImageLibFolderRecursivelyTest() {
            ImageLibraryData data = InsertTestData(false);

            int parentFolderId = DOImageLibrary.
                GetImageLibFolderIdByCaption("Parent");

            DataRow[] childFolders = data.Folder.Select
                ("ParentFolderId = " + parentFolderId.ToString());
            Assert.IsTrue(childFolders.Length > 0);

            DataRow[] childItems = data.FolderItem.Select
                ("FolderId = " + parentFolderId.ToString());
            Assert.IsTrue(childItems.Length > 0);

            DOImageLibrary.DeleteImageLibFolderRecursively
                (parentFolderId, false);

            ImageLibraryData newData = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);

            Assert.IsNotNull(
                newData.Folder.FindByFolderId(parentFolderId));

            DataRow[] childFolders2 = newData.Folder.Select
                ("ParentFolderId = " + parentFolderId.ToString());
            Assert.IsTrue(childFolders2.Length == 0);

            DataRow[] childItems2 = newData.FolderItem.Select
                ("FolderId = " + parentFolderId.ToString());
            Assert.IsTrue(childItems2.Length == 0);
        }

        [Test, Description("CR = C-clearTargetFolder, R-insertIntoRoot")]
        public void AddContentRecursively_CR_Test() {
            string targetFolderCaption = "Parent";
            bool insertIntoRoot = true;
            bool clearTargetFolder = true;

            InsertTestData(false);

            ImageLibraryData data = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);

            int folderCount = data.Folder.Count;
            int folderLocalizationCount = data.FolderLocalization.Count;
            int libraryItemCount = data.LibraryItem.Count;
            int folderItemCount = data.FolderItem.Count;

            BCImageLibrary.AddContentRecursively
                (imagePath, targetFolderCaption, insertIntoRoot,
                clearTargetFolder);

            ImageLibraryData newData = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);

            Assert.AreEqual(folderCount + 3 - 2, newData.Folder.Count);
            Assert.AreEqual(folderLocalizationCount + 3 - 2, newData.FolderLocalization.Count);
            Assert.AreEqual(libraryItemCount + 4 - 3, newData.FolderItem.Count);
            Assert.AreEqual(folderItemCount + 4 - 3, newData.LibraryItem.Count);

            int idMainForUTests = DOImageLibrary.GetImageLibFolderIdByCaption("MainForUTests");
            int idBusinessForUTests = DOImageLibrary.GetImageLibFolderIdByCaption("BusinessForUTests");
            int idGeneralForUTests = DOImageLibrary.GetImageLibFolderIdByCaption("GeneralForUTests");
            int idParent = DOImageLibrary.GetImageLibFolderIdByCaption("Parent");
            int idChild = DOImageLibrary.GetImageLibFolderIdByCaption("Child");

            Assert.IsTrue(idMainForUTests != int.MinValue);
            Assert.IsTrue(idBusinessForUTests != int.MinValue);
            Assert.IsTrue(idGeneralForUTests != int.MinValue);
            Assert.IsFalse(idParent != int.MinValue);
            Assert.IsFalse(idChild != int.MinValue);
        }
        [Test, Description("NCR = NC-!clearTargetFolder, R-insertIntoRoot")]
        public void AddContentRecursively_NCR_Test() {
            string targetFolderCaption = "Parent";
            bool insertIntoRoot = true;
            bool clearTargetFolder = false;

            InsertTestData(false);

            ImageLibraryData data = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);

            int folderCount = data.Folder.Count;
            int folderLocalizationCount = data.FolderLocalization.Count;
            int libraryItemCount = data.LibraryItem.Count;
            int folderItemCount = data.FolderItem.Count;

            BCImageLibrary.AddContentRecursively
                (imagePath, targetFolderCaption, insertIntoRoot,
                clearTargetFolder);

            ImageLibraryData newData = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);

            Assert.AreEqual(folderCount + 3, newData.Folder.Count);
            Assert.AreEqual(folderLocalizationCount + 3, newData.FolderLocalization.Count);
            Assert.AreEqual(libraryItemCount + 4, newData.FolderItem.Count);
            Assert.AreEqual(folderItemCount + 4, newData.LibraryItem.Count);

            int idMainForUTests = DOImageLibrary.GetImageLibFolderIdByCaption("MainForUTests");
            int idBusinessForUTests = DOImageLibrary.GetImageLibFolderIdByCaption("BusinessForUTests");
            int idGeneralForUTests = DOImageLibrary.GetImageLibFolderIdByCaption("GeneralForUTests");
            int idParent = DOImageLibrary.GetImageLibFolderIdByCaption("Parent");
            int idChild = DOImageLibrary.GetImageLibFolderIdByCaption("Child");

            Assert.IsTrue(idMainForUTests != int.MinValue);
            Assert.IsTrue(idBusinessForUTests != int.MinValue);
            Assert.IsTrue(idGeneralForUTests != int.MinValue);
            Assert.IsTrue(idParent != int.MinValue);
            Assert.IsTrue(idChild != int.MinValue);
        }
        [Test, Description("CNR = C-clearTargetFolder, NR-!insertIntoRoot")]
        public void AddContentRecursively_CNR_Test() {
            AddContentRecursively_NCR_Test();

            string targetFolderCaption = "MainForUTests";
            bool insertIntoRoot = false;
            bool clearTargetFolder = true;

            ImageLibraryData data = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);

            int folderCount = data.Folder.Count;
            int folderLocalizationCount = data.FolderLocalization.Count;
            int libraryItemCount = data.LibraryItem.Count;
            int folderItemCount = data.FolderItem.Count;

            BCImageLibrary.AddContentRecursively
                (imagePath, targetFolderCaption, insertIntoRoot,
                clearTargetFolder);

            ImageLibraryData newData = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);

            Assert.AreEqual(folderCount, newData.Folder.Count);
            Assert.AreEqual(folderLocalizationCount, newData.FolderLocalization.Count);
            Assert.AreEqual(libraryItemCount, newData.FolderItem.Count);
            Assert.AreEqual(folderItemCount, newData.LibraryItem.Count);

            int idMainForUTests = DOImageLibrary.GetImageLibFolderIdByCaption("MainForUTests");
            int idBusinessForUTests = DOImageLibrary.GetImageLibFolderIdByCaption("BusinessForUTests");
            int idGeneralForUTests = DOImageLibrary.GetImageLibFolderIdByCaption("GeneralForUTests");
            int idParent = DOImageLibrary.GetImageLibFolderIdByCaption("Parent");
            int idChild = DOImageLibrary.GetImageLibFolderIdByCaption("Child");

            Assert.IsTrue(idMainForUTests != int.MinValue);
            Assert.IsTrue(idBusinessForUTests != int.MinValue);
            Assert.IsTrue(idGeneralForUTests != int.MinValue);
            Assert.IsTrue(idParent != int.MinValue);
            Assert.IsTrue(idChild != int.MinValue);
        }
        [Test, Description("NCNR = NC-!clearTargetFolder, NR-!insertIntoRoot")]
        public void AddContentRecursively_NCNR_Test() {
            InsertTestData(false);

            string targetFolderCaption = "Parent";
            bool insertIntoRoot = false;
            bool clearTargetFolder = false;

            ImageLibraryData data = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);

            int folderCount = data.Folder.Count;
            int folderLocalizationCount = data.FolderLocalization.Count;
            int libraryItemCount = data.LibraryItem.Count;
            int folderItemCount = data.FolderItem.Count;

            BCImageLibrary.AddContentRecursively
                (imagePath, targetFolderCaption, insertIntoRoot,
                clearTargetFolder);

            ImageLibraryData newData = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);

            Assert.AreEqual(folderCount + 3 - 1, newData.Folder.Count);
            Assert.AreEqual(folderLocalizationCount + 3 - 1, newData.FolderLocalization.Count);
            Assert.AreEqual(libraryItemCount + 4, newData.FolderItem.Count);
            Assert.AreEqual(folderItemCount + 4, newData.LibraryItem.Count);

            int idMainForUTests = DOImageLibrary.GetImageLibFolderIdByCaption("MainForUTests");
            int idBusinessForUTests = DOImageLibrary.GetImageLibFolderIdByCaption("BusinessForUTests");
            int idGeneralForUTests = DOImageLibrary.GetImageLibFolderIdByCaption("GeneralForUTests");
            int idParent = DOImageLibrary.GetImageLibFolderIdByCaption("Parent");
            int idChild = DOImageLibrary.GetImageLibFolderIdByCaption("Child");

            Assert.IsFalse(idMainForUTests != int.MinValue);
            Assert.IsTrue(idBusinessForUTests != int.MinValue);
            Assert.IsTrue(idGeneralForUTests != int.MinValue);
            Assert.IsTrue(idParent != int.MinValue);
            Assert.IsTrue(idChild != int.MinValue);
        }
        [Test]
        public void ImageLibFolderSortOrderSwap() {
            ImageLibraryData data = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);
            int count = data.Folder.Count;
 
            //insert parent
            int parentFolderId = BCImageLibrary.InsertImageLibFolder(
                null, "Parent", "en", ImageLibraries.Expanded);
 
            //insert child
            BCImageLibrary.InsertImageLibFolder(parentFolderId, "Child", "", ImageLibraries.Expanded);
 
            ImageLibraryData newData = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);
 
            int sortOrder1 = newData.Folder[count].SortOrder;
            int sortOrder2 = newData.Folder[count + 1].SortOrder;
 
            BCImageLibrary.ImageLibFolderSortOrderSwap(
                newData.Folder[count].FolderId,
                newData.Folder[count + 1].FolderId);
 
            ImageLibraryData newData2 = DOImageLibrary.EnumerateData(ImageLibraries.Expanded);
 
            Assert.AreEqual("Child", newData2.Folder[count].GetFolderLocalizationRows()[0].Caption);
            Assert.AreEqual(sortOrder1, newData2.Folder[count].SortOrder);
            Assert.AreEqual("Parent", newData2.Folder[count + 1].GetFolderLocalizationRows()[0].Caption);
            Assert.AreEqual(sortOrder2, newData2.Folder[count + 1].SortOrder);
        }
    }
}