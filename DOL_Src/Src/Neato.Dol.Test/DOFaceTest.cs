using System.Data;
using System.Drawing;
using System.Xml;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Entity;

using NUnit.Framework;

namespace Neato.Dol.Test {
    [TestFixture]
    public class DOFaceTest {
        public DOFaceTest() {}
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void FaceInsertTest() {
            XmlDocument doc = new XmlDocument();
            doc.InnerXml = "<Test>Doc</Test>";

            Face face = BCFace.NewFace();
            face.Contour = doc.FirstChild;
            face.Position = new PointF(100, 100);
            face.AddName("", "test");

            DOFace.Add(face);

            Assert.IsNotNull(face);
            Assert.AreEqual(false, face.IsNew);
            Assert.IsTrue(face.Id > 0);

            face = DOFace.GetFace(face);

            Assert.IsNotNull(face);
            Assert.AreEqual(false, face.IsNew);
            Assert.IsTrue(face.Id > 0);
            Assert.AreEqual(doc.FirstChild.OuterXml, face.Contour.OuterXml);
            Assert.AreEqual(0, face.Position.X);
            Assert.AreEqual(0, face.Position.Y);
        }

        [Test]
        public void FaceGetByIdTest() {
            Face face = BCFace.NewFace();
            face.Contour = null;
            face.Position = new PointF(100, 100);
            face.AddName("", "test");

            DOFace.Add(face);

            Assert.IsNotNull(face);
            Assert.AreEqual(false, face.IsNew);
            Assert.IsTrue(face.Id > 0);

            Face dbFace = DOFace.GetFace(face);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.AreEqual(face.Id, dbFace.Id);
            Assert.IsNull(dbFace.Contour);
            Assert.AreEqual(0, dbFace.Position.X);
            Assert.AreEqual(0, dbFace.Position.Y);
        }

        [Test]
        public void FaceDeleteTest() {
            Face face = BCFace.NewFace();
            face.Contour = null;
            face.Position = new PointF(100, 100);

            DOFace.Add(face);

            Assert.IsNotNull(face);
            Assert.AreEqual(false, face.IsNew);
            Assert.IsTrue(face.Id > 0);

            DOFace.Delete(face);

            Face dbFace = DOFace.GetFace(face);
            Assert.IsNull(dbFace);
        }

        [Test]
        public void FaceUpdateTest() {
            XmlDocument doc1 = new XmlDocument();
            doc1.InnerXml = "<Test>Doc1</Test>";

            Face face = BCFace.NewFace();
            face.Contour = doc1.FirstChild;
            face.Position = new PointF(100, 100);
            face.AddName("", "test");

            DOFace.Add(face);
            Assert.IsNotNull(face);

            XmlDocument doc2 = new XmlDocument();
            doc2.InnerXml = "<Test>Doc2</Test>";
            face.Contour = doc2.FirstChild;

            DOFace.Update(face);

            Assert.IsNotNull(face);
            Assert.AreEqual(false, face.IsNew);
            Assert.IsTrue(face.Id > 0);

            Face dbFace = DOFace.GetFace(face);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.AreEqual(face.Id, dbFace.Id);
            Assert.AreEqual(doc2.FirstChild.OuterXml, dbFace.Contour.OuterXml);
            Assert.AreEqual(0, dbFace.Position.X);
            Assert.AreEqual(0, dbFace.Position.Y);
        }

        [Test]
        public void FaceBindUnbindPaperTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);
            brand = DOPaperBrand.GetPaperBrand(brand);
            Assert.IsNotNull(brand);

            Paper paper = BCPaper.NewPaper();
            paper.Brand = brand;
            paper.Name = "test";
            paper.Size = new SizeF(100, 100);
            DOPaper.Add(paper);
            
            Face face = BCFace.NewFace();
            face.Contour = null;
            face.Position = new PointF(90, 90);
            DOFace.Add(face);

            DOFace.BindToPaper(face, paper);

            Paper dbPaper = DOPaper.GetPaper(paper);

            Assert.IsNotNull(dbPaper);
            Assert.AreEqual(1, dbPaper.Faces.Length);

            Assert.IsNull(dbPaper.Faces[0].Contour);
            Assert.AreEqual(90, dbPaper.Faces[0].Position.X);
            Assert.AreEqual(90, dbPaper.Faces[0].Position.Y);

            DOFace.UnbindFromPaper(face, paper);

            dbPaper = DOPaper.GetPaper(paper);

            Assert.IsNotNull(dbPaper);
            Assert.AreEqual(0, dbPaper.Faces.Length);
        }

        [Test]
        public void FaceBindUnbindDeviceTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNotNull(brand);

            Device device = BCDevice.NewDevice();
            device.Brand = brand;
            device.Model = "test";
            device.Rating = 8;
            device.DeviceTypeId = (int)DeviceType.CdDvdLabel;
            DODevice.Add(device);
            
            Face face = BCFace.NewFace();
            face.Contour = null;
            face.Position = new PointF(90, 90);
            DOFace.Add(face);

            DOFace.BindToDevice(face, device);

            Device[] dbDevices = DODevice.EnumerateDeviceByParam(brand, (int)DeviceType.CdDvdLabel, null, face, null, DeviceType.Undefined, -1);

            Assert.IsNotNull(dbDevices);
            Assert.AreEqual(1, dbDevices.Length);

            DOFace.UnbindFromDevice(face, device);

            dbDevices = DODevice.EnumerateDeviceByParam(brand, (int)DeviceType.CdDvdLabel, null, face, null, DeviceType.Undefined, -1);

            Assert.IsNotNull(dbDevices);
            Assert.AreEqual(0, dbDevices.Length);
        }
    }
}