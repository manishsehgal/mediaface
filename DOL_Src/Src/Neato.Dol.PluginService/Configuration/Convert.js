var fso     = new ActiveXObject("Scripting.FileSystemObject");
var b2t     = new ActiveXObject("DataConversion.Bin2Txt");
var xml     = new ActiveXObject("Msxml2.DOMDocument");
var dirSrc  = "..\\..\\Neato.Dol.PluginService\\";
var dirDest = "..\\..\\Neato.Dol.WebDesigner\\Plugins\\Windows\\";
var xmlFile = "desc.xml";

//WScript.echo(txt7);

xml.async = false;
xml.load(dirSrc + xmlFile);

var ModulesEl = xml.documentElement;
for (ModuleInfoEl=ModulesEl.firstChild; ModuleInfoEl!=null; ModuleInfoEl=ModuleInfoEl.nextSibling)
{
	ProcessNode(ModuleInfoEl);
	var DependsEl = ModuleInfoEl.getElementsByTagName("Depends").item(0);
	if (DependsEl != null)
	{
		for (FileEl=DependsEl.firstChild; FileEl!=null; FileEl=FileEl.nextSibling)
		{
			ProcessNode(FileEl);
		}
	}
}
xml.save(dirDest + xmlFile);

function ProcessNode(node)
{
	var fileName = node.getAttribute("Filename");
	var file = fso.GetFile(dirSrc + "Bin\\" + fileName);
	
	b2t.File2ZipAndBase64F(file.Path, dirDest + fileName + ".txt", false);
	var sum = b2t.GetFileSum(file.Path);
	node.setAttribute("Sum", sum);
}





//	var name = dir + fileList[i];
//	var txt = b2t.File2ZipAndBase64F(name, name+".txt", true);



