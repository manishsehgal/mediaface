#pragma once

#include <vector>

//CTxtCnv class is used for string translation between UNICODE and ANSI.

//Each method without 'x' postfix dinamically allocates new memory buffer 
//on heap, save that pointer in internal array and returns that pointer. 
//So that buffer is valid while CTxtCnv class instance is alive.

//Each method with 'x' postfix puts result at given buffer, but DO NOT check 
//the buffer length. So, be careful!

//Second constructor allows you to use different code pages.
//It may be useful for UTF-8 coding, for example. 

class CTxtCnv
{
public:
	CTxtCnv()
    {
        m_nCodePage = CP_ACP;
    }
	CTxtCnv(UINT nCodePage)
    {
        m_nCodePage = nCodePage;
    }
	CTxtCnv(const CTxtCnv & tc);        //avoid to use
	void operator=(const CTxtCnv & tc); //avoid to use
	~CTxtCnv()
	{
		Clear();
	}

protected:
    UINT m_nCodePage;
	std::vector<char*>  m_lstAChar;
	std::vector<WCHAR*> m_lstWChar;

public:
	void Clear()
	{
		UINT i;
		for (i=0; i<m_lstAChar.size(); i++) delete[] m_lstAChar[i];
		m_lstAChar.clear();
		for (i=0; i<m_lstWChar.size(); i++) delete[] m_lstWChar[i];
		m_lstWChar.clear();
	}

	WCHAR * a2w(const char * pAChar)
	{
		if(!pAChar) return 0;
		int iSize = ::MultiByteToWideChar(m_nCodePage, 0, pAChar, -1, 0, 0);
		WCHAR * p = new WCHAR[iSize];
		p[0] = 0;
		::MultiByteToWideChar(m_nCodePage, 0, pAChar, -1, p, iSize);
		m_lstWChar.push_back(p);
		return p;
	}
	WCHAR * a2wx(const char * pAChar, WCHAR * pWChar)
	{
		if(!pAChar) return 0;
		if(!pWChar) return 0;
		int iSize = strlen(pAChar);
		::MultiByteToWideChar(m_nCodePage, 0, pAChar, iSize+1, pWChar, iSize+1);
		return pWChar;
	}

	char * w2a(const WCHAR * pWChar)
	{
		if(!pWChar) return 0;
		int iSize = ::WideCharToMultiByte(m_nCodePage, 0, pWChar, -1, 0, 0, 0, 0);
		char * p = new char[iSize];
		p[0] = 0;
		::WideCharToMultiByte(m_nCodePage, 0, pWChar, -1, p, iSize, 0, 0);
		m_lstAChar.push_back(p);
		return p;
	}
	char * w2ax(const WCHAR * pWChar, char * pAChar)
	{
		if(!pWChar) return 0;
		if(!pAChar) return 0;
		int iSize = wcslen(pWChar);
		::WideCharToMultiByte(m_nCodePage, 0, pWChar, iSize+1, pAChar, iSize+1, 0, 0);
		return pAChar;
	}

	BSTR a2b(const char * pAChar)
	{
		if(!pAChar) return 0;
		int iSize = ::MultiByteToWideChar(m_nCodePage, 0, pAChar, -1, 0, 0);
		BSTR p = ::SysAllocStringLen(0, iSize);
		p[0] = 0;
		::MultiByteToWideChar(m_nCodePage, 0, pAChar, -1, p, iSize);
		return p;
	}
	BSTR a2bx(const char * pAChar, BSTR * ppBChar)
	{
		if(!pAChar) return 0;
		if(!ppBChar) return 0;
		int iSize = strlen(pAChar);
		::SysFreeString(*ppBChar);
		*ppBChar = ::SysAllocStringLen(0, iSize);
		::MultiByteToWideChar(m_nCodePage, 0, pAChar, iSize+1, *ppBChar, iSize+1);
		return *ppBChar;
	}

	char * b2a(BSTR pBChar)
	{
		if(!pBChar) return 0;
		int iSizeB = ::SysStringLen(pBChar);
        int iSizeA = ::WideCharToMultiByte(m_nCodePage, 0, pBChar, iSizeB+1, 0, 0, 0, 0);
		char * p = new char[iSizeA];
		p[0] = 0;
		::WideCharToMultiByte(m_nCodePage, 0, pBChar, iSizeB+1, p, iSizeA, 0, 0);
		m_lstAChar.push_back(p);
		return p;
	}
    char * b2ax(BSTR pBChar, char * pAChar)
	{
		if(!pBChar) return 0;
		if(!pAChar) return 0;
		int iSize = ::SysStringLen(pBChar);
		::WideCharToMultiByte(m_nCodePage, 0, pBChar, iSize+1, pAChar, iSize+1, 0, 0);
		return pAChar;
	}

#ifdef _UNICODE//========================================================================

	TCHAR * w2t(const WCHAR * pWChar)
	{
		if(!pWChar) return 0;
		int iSize = wcslen(pWChar);
		WCHAR * p = new WCHAR[iSize+1];
		wcscpy(p, pWChar);
		m_lstWChar.push_back(p);
		return p;
	}
	TCHAR * w2tx(const WCHAR * pWChar, TCHAR * pTChar)
	{
		if(!pWChar) return 0;
		if(!pTChar) return 0;
		wcscpy(pTChar, pWChar);
		return pTChar;
	}

	WCHAR * t2w(const TCHAR * pTChar)
	{
        return w2t(pTChar);
	}
	WCHAR * t2wx(const TCHAR * pTChar, WCHAR * pWChar)
	{
        return w2tx(pTChar, pWChar);
	}

	char * t2a(const TCHAR * pTChar)
	{
        return w2a(pTChar);
	}
	char * t2ax(const TCHAR * pTChar, char * pAChar)
	{
        return w2ax(pTChar, pAChar);
	}

	TCHAR * a2t(const char * pAChar)
	{
        return a2w(pAChar);
	}
	TCHAR * a2tx(const char * pAChar, TCHAR * pTChar)
	{
        return a2wx(pAChar, pTChar);
	}

#else//========================================================================

	TCHAR * w2t(const WCHAR * pWChar)
	{
        return w2a(pWChar);
	}
	TCHAR * w2tx(const WCHAR * pWChar, TCHAR * pTChar)
	{
        return w2ax(pWChar, pTChar);
	}

	WCHAR * t2w(const TCHAR * pTChar)
	{
        return a2w(pTChar);
	}
	WCHAR * t2wx(const TCHAR * pTChar, WCHAR * pWChar)
	{
        return a2wx(pTChar, pWChar);
	}

	char * t2a(const TCHAR * pTChar)
	{
		if(!pTChar) return 0;
		int iSize = strlen(pTChar);
		char * p = new char[iSize+1];
		strcpy(p, pTChar);
		m_lstAChar.push_back(p);
		return p;
	}
	char * t2ax(const TCHAR * pTChar, char * pAChar)
	{
		if(!pTChar) return 0;
		if(!pAChar) return 0;
		strcpy(pAChar, pTChar);
		return pAChar;
	}

	TCHAR * a2t(const char * pAChar)
	{
        return t2a(pAChar);
	}
	TCHAR * a2tx(const char * pAChar, TCHAR * pTChar)
	{
        return t2ax(pAChar, pTChar);
	}

#endif//========================================================================


};
