#pragma once
#include <atlbase.h>
#include <vector>
#define TIME_DELIMITER_STR ":"
typedef struct _tagTRACK
{
	_bstr_t m_bstrArtist;
	_bstr_t m_bstrAlbum;
	_bstr_t m_bstrTitle;
	_bstr_t m_bstrDuration;
	_bstr_t m_bstrUrl;
}TRACK,*LPTRACK;

class CPlayList
{
protected:
	std::vector<TRACK> m_vTracks;
public:
    CPlayList(void){};
    ~CPlayList(void){};
	_bstr_t m_bstrArtist;
	_bstr_t m_bstrAlbum;
	DWORD GetTracksCount()
	{
		return m_vTracks.size();
	}
	LPTRACK GetTrackByNum(DWORD lNum)
	{
		if(m_vTracks.size()<=lNum||m_vTracks.size()<=0)
			return FALSE;
		return (LPTRACK)&(m_vTracks[lNum]);
	}
	BOOL AddTrack(TRACK &track)
	{
		m_vTracks.push_back(track);
		return TRUE;
	}

	
};
