#pragma once


class CPlayListEx;

//==================================
class CPlayListTrackEx
{
    friend class CPlayListEx;

public:
    CPlayListTrackEx(CPlayListEx * pParent);

public:
    void SetTitle(LPCWSTR wcVal);
    void SetArtist(LPCWSTR wcVal);
    void SetAlbum(LPCWSTR wcVal);
    void SetUrl(LPCWSTR wcVal);
    void SetDuration(long lVal);
    bool GetXml(BSTR * bstrXml);

protected:
    CPlayListEx * m_pParent;
    wstring m_strTitle;
    wstring m_strArtist;
    wstring m_strAlbum;
    wstring m_strUrl;
    long    m_lDuration;
    wstring m_strSDuration;
    wstring m_strLDuration;
    bool    m_bLongFormat;
};


//==================================
class CPlayListEx
{
    friend class CPlayListTrackEx;

public:
    CPlayListEx();

protected:
    vector<CPlayListTrackEx> m_vTracks;
    long    m_lDuration;
    wstring m_strSDuration;
    wstring m_strLDuration;
    bool    m_bLongFormat;
    wstring m_strSrcType;
    wstring m_strSrcName;

public:
    int  GetTrackCount();
    CPlayListTrackEx & GetTrack(int nPos);
    int  AddEmptyTrack(int nPos = -1);
    bool GetXml(BSTR * bstrXml);

protected:
    void recalcDuration();
};