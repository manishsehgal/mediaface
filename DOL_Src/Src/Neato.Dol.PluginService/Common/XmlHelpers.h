#pragma once

class CXmlHelpers
{
public:
    //Load xml from string and returns root element
    static HRESULT LoadXml(BSTR bstrXml, MSXML2::IXMLDOMElement ** ppRootElem, MSXML2::IXMLDOMDocument ** ppDoc=0)
    {
        if (!bstrXml)    return E_POINTER;
        if (!ppRootElem) return E_POINTER;

        CComPtr<MSXML2::IXMLDOMDocument> pDoc;
        HRESULT hRes = pDoc.CoCreateInstance(L"MSXML2.DOMDocument");
        if (hRes != S_OK) return hRes;
        hRes = pDoc->put_validateOnParse(VARIANT_FALSE);
        if (hRes != S_OK) return hRes;
        hRes = pDoc->put_resolveExternals(VARIANT_FALSE);
        if (hRes != S_OK) return hRes;
        VARIANT_BOOL bOK = VARIANT_FALSE;
        hRes = pDoc->loadXML(bstrXml, &bOK);
        if (hRes != S_OK) return hRes;
        if (bOK  != VARIANT_TRUE) return E_FAIL;

        CComPtr<MSXML2::IXMLDOMElement> pRootElem;
        hRes = pDoc->get_documentElement(&pRootElem);
        if (hRes != S_OK) return hRes;

        pRootElem.CopyTo(ppRootElem);

        if (ppDoc)
        {
            pDoc.CopyTo(ppDoc);
        }

        return S_OK;
    }

    //Create new empty document
    static HRESULT CreateDocument(BSTR bstrRoot, MSXML2::IXMLDOMElement ** ppRootElem, MSXML2::IXMLDOMDocument ** ppDoc=0)
    {
        if (!ppRootElem) return E_POINTER;

        CComPtr<MSXML2::IXMLDOMDocument> pDoc;
        HRESULT hRes = pDoc.CoCreateInstance(L"MSXML2.DOMDocument");
        if (hRes != S_OK) return hRes;

        CComPtr<MSXML2::IXMLDOMElement> pNewElem;
        hRes = pDoc->createElement(bstrRoot, &pNewElem);
        if (hRes != S_OK) return hRes;

        hRes = pDoc->putref_documentElement(pNewElem);
        if (hRes != S_OK) return hRes;

        CComPtr<MSXML2::IXMLDOMElement> pRootElem;
        hRes = pDoc->get_documentElement(&pRootElem);
        if (hRes != S_OK) return hRes;

        pRootElem.CopyTo(ppRootElem);

        if (ppDoc)
        {
            pDoc.CopyTo(ppDoc);
        }

        return S_OK;
    }

    //Get value of element attribute with given name
    static HRESULT GetAttribute(MSXML2::IXMLDOMElement * pElem, LPCWSTR strAttr, BSTR * pbstrValue)
    {
        if (!pElem)    return E_POINTER;
        if (!strAttr)  return E_POINTER;
        if (!pbstrValue) return E_POINTER;

        CComVariant vt;
        HRESULT hRes = pElem->getAttribute(CComBSTR(strAttr), &vt);
        if (hRes != S_OK) return hRes;

        CComBSTR bstrRes = vt.bstrVal;
        *pbstrValue = bstrRes.Detach();
        return S_OK;
    }

    //Get value of element attribute with given name
    static HRESULT SetAttribute(MSXML2::IXMLDOMElement * pElem, LPCWSTR strAttr, LPCWSTR strValue)
    {
        if (!pElem)    return E_POINTER;
        if (!strAttr)  return E_POINTER;
        if (!strValue) return E_POINTER;

        CComVariant vt(strValue);
        return pElem->setAttribute(CComBSTR(strAttr), vt);
    }

    //Finds FIRST child element of pNode with given name
    static HRESULT GetChildElement(MSXML2::IXMLDOMElement * pNode, LPCWSTR pName, MSXML2::IXMLDOMElement ** ppElem)
    {
        if (!pNode)  return E_POINTER;
        if (!pName)  return E_POINTER;
        if (!ppElem) return E_POINTER;

        HRESULT hRes;
        CComPtr<MSXML2::IXMLDOMNodeList> pList;
        hRes = pNode->get_childNodes(&pList);
        if (hRes != S_OK) return hRes;

        long nCount = 0;
        hRes = pList->get_length(&nCount);
        if (hRes != S_OK) return hRes;

        for (long i=0; i<nCount; i++)
        {
            CComPtr<MSXML2::IXMLDOMNode> pChildNode;
            hRes = pList->get_item(i, &pChildNode);
            if (hRes != S_OK) continue;
            CComQIPtr<MSXML2::IXMLDOMElement> pChildElem = pChildNode;
            if (!pChildElem) continue;

            CComBSTR bstrName;
            hRes = pChildElem->get_nodeName(&bstrName);
            if (hRes != S_OK) continue;

            if (wcsicmp(bstrName, pName) != 0) continue;

            pChildElem.CopyTo(ppElem);
            return S_OK;
        }
        return E_INVALIDARG;
    }

    //Finds next sibling element for pNode element with the same name
    static HRESULT GetNextSiblingElement(MSXML2::IXMLDOMElement * pNode, MSXML2::IXMLDOMElement ** ppElem)
    {
        if (!pNode)  return E_POINTER;
        if (!ppElem) return E_POINTER;
        HRESULT hRes;

        CComBSTR bstrName0;
        hRes = pNode->get_nodeName(&bstrName0);
        if (hRes != S_OK) return hRes;

        CComPtr<MSXML2::IXMLDOMNode> pNode0 = pNode;
        while(true)
        {
            CComPtr<MSXML2::IXMLDOMNode> pSiblingNode;
            hRes = pNode0->get_nextSibling(&pSiblingNode);
            if (hRes != S_OK) return hRes;

            pNode0 = pSiblingNode;

            CComQIPtr<MSXML2::IXMLDOMElement> pSiblingElem = pSiblingNode;
            if (!pSiblingElem) continue;

            CComBSTR bstrName;
            hRes = pSiblingElem->get_nodeName(&bstrName);
            if (hRes != S_OK) continue;

            if (wcsicmp(bstrName, bstrName0) == 0)
            {
                pSiblingElem.CopyTo(ppElem);
                return S_OK;
            }
        }
    }

    static HRESULT AddElement(MSXML2::IXMLDOMElement * pNode, LPCWSTR pName, MSXML2::IXMLDOMElement ** ppNewElem)
    {
        if (!pNode)  return E_POINTER;
        if (!pName)  return E_POINTER;
        if (!ppNewElem) return E_POINTER;
        HRESULT hRes;

        CComPtr<MSXML2::IXMLDOMDocument> pDoc;
        hRes = pNode->get_ownerDocument(&pDoc);
        if (hRes != S_OK) return hRes;

        CComPtr<MSXML2::IXMLDOMElement> pNewElem;
        hRes = pDoc->createElement(CComBSTR(pName), &pNewElem);
        if (hRes != S_OK) return hRes;

        CComPtr<MSXML2::IXMLDOMNode> pNewNode;
        hRes = pNode->appendChild(pNewElem, &pNewNode);
        if (hRes != S_OK) return hRes;

        CComQIPtr<MSXML2::IXMLDOMElement> pNewElem2 = pNewNode;
        if (!pNewElem2) return E_FAIL;

        pNewElem2.CopyTo(ppNewElem);
        return S_OK;
    }
};