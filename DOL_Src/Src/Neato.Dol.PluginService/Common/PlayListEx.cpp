#include "stdafx.h"
#include <PlayListEx.h>
#include <XmlHelpers.h>


//====================================================

CPlayListTrackEx::CPlayListTrackEx(CPlayListEx * pParent)
{
    m_pParent = pParent;
    m_lDuration = 0;
    m_bLongFormat = false;
}

void CPlayListTrackEx::SetTitle(LPCWSTR wcVal)
{
    if (!wcVal) return;
    m_strTitle = wcVal;
}

void CPlayListTrackEx::SetArtist(LPCWSTR wcVal)
{
    if (!wcVal) return;
    m_strArtist = wcVal;
}

void CPlayListTrackEx::SetAlbum(LPCWSTR wcVal)
{
    if (!wcVal) return;
    m_strAlbum = wcVal;
}

void CPlayListTrackEx::SetUrl(LPCWSTR wcVal)
{
    if (!wcVal) return;
    m_strUrl = wcVal;
}

void CPlayListTrackEx::SetDuration(long lVal)
{
    bool bChanged = (m_lDuration != lVal);
    m_lDuration = lVal;

    WCHAR buf[128];
    swprintf(buf, L"%02d:%02d", m_lDuration/60, m_lDuration%60);
    m_strSDuration = buf;
    swprintf(buf, L"%02d:%02d:%02d", m_lDuration/3600, (m_lDuration%3600)/60, m_lDuration%60);
    m_strLDuration = buf;

    if (bChanged && m_pParent)
    {
        m_pParent->recalcDuration();
    }
}

bool CPlayListTrackEx::GetXml(BSTR * bstrXml)
{
   	CComPtr<MSXML2::IXMLDOMElement> pTrackEl;
	HRESULT hRes = CXmlHelpers::CreateDocument(CComBSTR(L"Track"), &pTrackEl);

    CXmlHelpers::SetAttribute(pTrackEl, L"Artist", m_strArtist.c_str());
    CXmlHelpers::SetAttribute(pTrackEl, L"Title", m_strTitle.c_str());
    CXmlHelpers::SetAttribute(pTrackEl, L"Album", m_strAlbum.c_str());
    CXmlHelpers::SetAttribute(pTrackEl, L"Url", m_strUrl.c_str());
    CXmlHelpers::SetAttribute(pTrackEl, L"Duration", m_bLongFormat ? m_strLDuration.c_str() : m_strSDuration.c_str());

    pTrackEl->get_xml(bstrXml);
    return true;
}

//====================================================

CPlayListEx::CPlayListEx()
{
    m_lDuration = 0;
    m_bLongFormat = false;
}

int CPlayListEx::GetTrackCount()
{
    return (int)m_vTracks.size();
}

CPlayListTrackEx & CPlayListEx::GetTrack(int nPos)
{
    if (nPos < 0 || nPos >= (int)m_vTracks.size()) throw 0;
    return m_vTracks[nPos];
}

int CPlayListEx::AddEmptyTrack(int nPos)
{
    if (nPos < 0 || nPos >= (int)m_vTracks.size()) 
    {
        nPos = (int)m_vTracks.size();
    }

    m_vTracks.insert(m_vTracks.begin() + nPos, CPlayListTrackEx(this));
    return nPos;
}

bool CPlayListEx::GetXml(BSTR * bstrXml)
{
	CComPtr<MSXML2::IXMLDOMElement> pPlayListEl;
	HRESULT hRes = CXmlHelpers::CreateDocument(CComBSTR(L"PlayList"), &pPlayListEl);

    WCHAR buf[128];
    swprintf(buf, L"%d", m_vTracks.size());
    hRes = CXmlHelpers::SetAttribute(pPlayListEl, L"TrackCount", buf);
    hRes = CXmlHelpers::SetAttribute(pPlayListEl, L"Artist", L"");
    hRes = CXmlHelpers::SetAttribute(pPlayListEl, L"Title", L"");
    hRes = CXmlHelpers::SetAttribute(pPlayListEl, L"Album", L"");
    hRes = CXmlHelpers::SetAttribute(pPlayListEl, L"Duration", m_bLongFormat ? m_strLDuration.c_str() : m_strSDuration.c_str());
    hRes = CXmlHelpers::SetAttribute(pPlayListEl, L"SrcType", m_strSrcType.c_str());
    hRes = CXmlHelpers::SetAttribute(pPlayListEl, L"SrcName", m_strSrcName.c_str());

	CComPtr<MSXML2::IXMLDOMElement> pTracksEl;
	hRes = CXmlHelpers::AddElement(pPlayListEl, L"Tracks", &pTracksEl);
    for (int i=0; i<(int)m_vTracks.size(); i++)
    {
        CPlayListTrackEx & track = m_vTracks[i];
        CComBSTR bstrTrXml;
        track.GetXml(&bstrTrXml);

    	CComPtr<MSXML2::IXMLDOMElement> pTrackEl;
		hRes = CXmlHelpers::LoadXml(bstrTrXml, &pTrackEl);
		if (hRes != S_OK) throw 13;
        swprintf(buf, L"%d", i+1);
        hRes = CXmlHelpers::SetAttribute(pTrackEl, L"Id", buf);
		CComPtr<MSXML2::IXMLDOMNode> pNewTrNode;
		pTracksEl->appendChild(pTrackEl, &pNewTrNode);
    }

    pPlayListEl->get_xml(bstrXml);
    return true;
}

void CPlayListEx::recalcDuration()
{
	m_lDuration = 0;
    for (int i=0; i<(int)m_vTracks.size(); i++)
    {
        CPlayListTrackEx & track = m_vTracks[i];
        m_lDuration += track.m_lDuration;
		track.m_bLongFormat = track.m_lDuration >= 3600;
    }
    m_bLongFormat = (m_lDuration >= 3600);

    WCHAR buf[128];
    swprintf(buf, L"%02d:%02d", m_lDuration/60, m_lDuration%60);
    m_strSDuration = buf;
    swprintf(buf, L"%02d:%02d:%02d", m_lDuration/3600, (m_lDuration%3600)/60, m_lDuration%60);
    m_strLDuration = buf;
}

