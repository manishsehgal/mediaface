 
 L a n g u a g e N a m e s = ( u s = 0 x 4 0 9 : M S G 0 0 4 0 9 )  
  
 M e s s a g e I d T y p e d e f = H R E S U L T  
  
 F a c i l i t y N a m e s = (  
         L S C A P I = 0 x 0 4  
         )  
 	  
 ; L S C A P I _ E _ L O A D L I B R A R Y _ E R R O R   0 x C 0 0 4 2 0 1  
 M e s s a g e I D = 0 x 2 0 1  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A   n e e d e d   l i b r a r y   c o u l d   n o t   b e   l o a d e d .      
 .  
  
 ; L S C A P I _ E _ M E M O R Y _ E R R O R   0 x C 0 0 4 2 1 A  
 M e s s a g e I D = 0 x 2 1 A  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 M e m o r y   e r r o r .      
 .  
  
 ; L S C A P I _ E _ D R I V E _ I N _ U S E   0 x C 0 0 4 2 1 E  
 M e s s a g e I D = 0 x 2 1 E  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A n o t h e r   d r i v e   i s   o p e n   o r   t h i s   d r i v e   i s   i n   u s e .      
 .  
  
 ; L S C A P I _ E _ R E E N T R A N T   0 x C 0 0 4 2 2 3  
 M e s s a g e I D = 0 x 2 2 3  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A   r e - e n t r a n t   c a l l   h a s   b e e n   m a d e   t o   a   c o m p o n e n t   t h a t   d o e s   n o t   a l l o w   i t .      
 .  
  
 ; L S C A P I _ E _ B A D _ A R G U M E N T   0 x C 0 0 4 2 2 6  
 M e s s a g e I D = 0 x 2 2 6  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 S o f t w a r e   E r r o r :   O n e   o f   t h e   s u p p l i e d   a r g u m e n t s   i s   e r r o n e o u s .      
 .  
  
 ; L S C A P I _ E _ D R I V E _ C H A N G E D   0 x C 0 0 4 2 3 3  
 M e s s a g e I D = 0 x 2 3 3  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   c u r r e n t   d r i v e   h a s   e i t h e r   b e e n   s w a p p e d   o u t   o r   i s   n o t   p r e s e n t .      
 .  
  
 ; L S C A P I _ E _ I N T E R N A L _ E R R O R   0 x C 0 0 4 2 2 F  
 M e s s a g e I D = 0 x 2 2 F  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I n t e r n a l   E r r o r .      
 .  
  
 ; L S C A P I _ E _ C O M M U N I C A T I O N S   0 x C 0 0 4 1 0 8 0  
 M e s s a g e I D = 0 x 1 0 8 0  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 D r i v e   c o m m u n i c a t i o n s   e r r o r .      
 .  
  
 ; L S C A P I _ E _ H A R D W A R E   0 x C 0 0 4 1 0 8 1  
 M e s s a g e I D = 0 x 1 0 8 1  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   e r r o r .      
 .  
  
 ; L S C A P I _ E _ S E R V I C E   0 x C 0 0 4 1 0 8 7  
 M e s s a g e I D = 0 x 1 0 8 7  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   L i g h t S c r i b e   s e r v i c e   h a s   p r o d u c e d   a n   e r r o r .   I t   m i g h t   b e   n e c e s s a r y   t o   r e s t a r t   t h e   s e r v i c e .      
 .  
  
 ; L S C A P I _ E _ U N S U P P O R T E D _ D R I V E   0 x C 0 0 4 2 2 B  
 M e s s a g e I D = 0 x 2 2 B  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 D r i v e   r e s o u r c e   f i l e   n o t   f o u n d .     A   L i g h t S c r i b e   s y s t e m   s o f t w a r e   u p d a t e   m a y   f i x   t h i s .      
 .  
  
 ; L S C A P I _ E _ U N S U P P O R T E D _ D R I V E _ C O M M U N I C A T I O N S   0 x C 0 0 4 2 3 9  
 M e s s a g e I D = 0 x 2 3 9  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h i s   v e r s i o n   o f   t h e   s o f t w a r e   c a n n o t   u s e   t h i s   d r i v e .     A   L i g h t S c r i b e   s y s t e m   s o f t w a r e   u p d a t e   m a y   f i x   t h i s .      
 .  
  
 ; L S C A P I _ E _ C O R R U P T _ D R F   0 x C 0 0 4 2 2 E  
 M e s s a g e I D = 0 x 2 2 E  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   r e s o u r c e   f i l e   a s s o c i a t e d   w i t h   t h i s   d r i v e   t y p e   i s   c o r r u p t .     A   L i g h t S c r i b e   s y s t e m   s o f t w a r e   u p d a t e   m a y   f i x   t h i s .      
 .  
  
 ; L S C A P I _ E _ D R F _ V E R S I O N _ M I S M A T C H   0 x C 0 0 4 2 3 4  
 M e s s a g e I D = 0 x 2 3 4  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   c u r r e n t   v e r s i o n   o f   t h e   L i g h t S c r i b e   s y s t e m   s o f t w a r e   i s   o l d e r   t h a n   t h e   d r i v e ' s   r e s o u r c e   f i l e .     A   L i g h t S c r i b e   s y s t e m   s o f t w a r e   u p d a t e   m a y   f i x   t h i s .      
 .  
  
 ; L S C A P I _ E _ N O _ M E D I A   0 x C 0 0 4 1 0 1 2  
 M e s s a g e I D = 0 x 1 0 1 2  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 N o   m e d i a   d e t e c t e d .  
 .  
  
 ; L S C A P I _ E _ N O N _ L I G H T S C R I B E _ M E D I A   0 x C 0 0 4 2 3 d  
 M e s s a g e I D = 0 x 2 3 d  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 N o n - L i g h t S c r i b e   m e d i a   d e t e c t e d .  
 .  
  
 ; L S C A P I _ E _ M E D I A _ I D _ R E A D _ E R R O R   0 x C 0 0 4 2 0 8  
 M e s s a g e I D = 0 x 2 0 8  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 U n a b l e   t o   r e a d   t h e   L i g h t S c r i b e   d i s c   i n f o r m a t i o n   -   p e r h a p s   t h e   d i s c   i s   d i r t y ,   s c r a t c h e d   o r   n o t   o r i e n t e d   f o r   l a b e l i n g .  
 .  
  
 ; L S C A P I _ E _ M E D I A _ U N S U P P O R T E D _ I D   0 x C 0 0 4 2 3 7  
 M e s s a g e I D = 0 x 2 3 7  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 U n s u p p o r t e d   m e d i a   i d e n t i f i c a t i o n   p a r a m e t e r .     A   L i g h t S c r i b e   s y s t e m   s o f t w a r e   u p d a t e   m a y   f i x   t h i s .      
 .  
  
 ; L S C A P I _ E _ M E D I A _ I N V A L I D P A R A M S   0 x C 0 0 4 2 0 9  
 M e s s a g e I D = 0 x 2 0 9  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I n v a l i d   m e d i a   i d e n t i f i c a t i o n   p a r a m e t e r .      
 .  
  
 ; L S C A P I _ E _ M E D I A _ N O T _ S U P P O R T E D _ B Y _ D R I V E   0 x C 0 0 4 2 2 0  
 M e s s a g e I D = 0 x 2 2 0  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   d o e s   n o t   s u p p o r t   t h e   m e d i a   t y p e .     A   L i g h t S c r i b e   s y s t e m   s o f t w a r e   u p d a t e   w i l l   n o t   f i x   t h i s   p r o b l e m .      
 .  
  
 ; L S C A P I _ E _ I N V A L I D _ M E D I A _ O R I E N T A T I O N   0 x C 0 0 4 2 0 5  
 M e s s a g e I D = 0 x 2 0 5  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   m e d i a   i s   n o t   o r i e n t e d   s u c h   t h a t   l a b e l i n g   c a n   p r o c e e d .      
 .  
  
 ; L S C A P I _ E _ U N S U P P O R T E D _ M E D I A   0 x C 0 0 4 2 2 C  
 M e s s a g e I D = 0 x 2 2 C  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   r e s o u r c e   f i l e   d o e s   n o t   s u p p o r t   t h i s   m e d i a   t y p e .     A   L i g h t S c r i b e   s y s t e m   s o f t w a r e   u p d a t e   m a y   f i x   t h i s .      
 .  
  
 ; L S C A P I _ E _ U N S U P P O R T E D _ M E D I A _ N O _ U P D A T E   0 x C 0 0 4 2 3 b  
 M e s s a g e I D = 0 x 2 3 b  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   r e s o u r c e   f i l e   d o e s   n o t   s u p p o r t   t h i s   m e d i a   t y p e   a n d   i s   n o   l o n g e r   b e i n g   u p d a t e d .     A   L i g h t S c r i b e   s y s t e m   s o f t w a r e   u p d a t e   w i l l   n o t   f i x   t h i s   p r o b l e m .      
 .  
  
 ; L S C A P I _ E _ I N C O M P A T I B L E _ M E D I A   0 x C 0 0 4 2 3 c  
 M e s s a g e I D = 0 x 2 3 c  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h i s   d r i v e   i s   i n c o m p a t i b l e   w i t h   t h e   c u r r e n t   m e d i a .     A   L i g h t S c r i b e   s y s t e m   s o f t w a r e   u p d a t e   w i l l   n o t   f i x   t h i s   p r o b l e m .      
 .  
  
 ; L S C A P I _ E _ P R E V I E W _ F A I L E D   0 x C 0 0 4 2 2 9  
 M e s s a g e I D = 0 x 2 2 9  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 U n a b l e   t o   c r e a t e   t h e   p r i n t   p r e v i e w   f i l e .      
 .  
  
 ; L S C A P I _ E _ P R I N T _ F I L E   0 x C 0 0 4 2 1 0  
 M e s s a g e I D = 0 x 2 1 0  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 U n a b l e   t o   p r i n t   t o   f i l e .      
 .  
  
 ; L S C A P I _ E _ I M A G E _ U N S U P P O R T E D   0 x C 0 0 4 2 1 1  
 M e s s a g e I D = 0 x 2 1 1  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 U n s u p p o r t e d   s o u r c e   i m a g e   f o r m a t .      
 .  
  
 ; L S C A P I _ E _ I M A G E _ I N V A L I D   0 x C 0 0 4 2 1 2  
 M e s s a g e I D = 0 x 2 1 2  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I n v a l i d   s o u r c e   i m a g e   f o r m a t .      
 .  
  
 ; L S C A P I _ E _ A B O R T P R I N T   0 x C 0 0 4 2 1 9  
 M e s s a g e I D = 0 x 2 1 9  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   a p p l i c a t i o n   r e q u e s t e d   t h e   c a n c e l l a t i o n   o f   t h e   p r i n t   p r o c e s s .      
 .  
  
 ; L S C A P I _ E _ C A L L B A C K _ E X C E P T I O N   0 x C 0 0 4 2 1 B  
 M e s s a g e I D = 0 x 2 1 B  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   p r i n t   p r o c e s s   w a s   c a n c e l l e d   d u e   t o   a n   i n t e r n a l   e r r o r .      
 .  
  
 ; L S C A P I _ E _ P R E V I E W _ U N S U P P O R T E D   0 x C 0 0 4 2 1 D  
 M e s s a g e I D = 0 x 2 1 D  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 U n s u p p o r t e d   p r e v i e w   i m a g e   f o r m a t .      
 .  
  
 ; L S C A P I _ E _ U N S U P P O R T E D _ P R I N T _ Q U A L I T Y   0 x C 0 0 4 2 2 D  
 M e s s a g e I D = 0 x 2 2 D  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 P r i n t   Q u a l i t y   i s   n o t   s u p p o r t e d .      
 .  
  
 ; L S C A P I _ E _ D R I V E _ T R A Y _ L O C K E D   0 x C 0 0 4 2 2 A  
 M e s s a g e I D = 0 x 2 2 A  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 D r i v e   t r a y   o p e r a t i o n   b l o c k e d   b e c a u s e   t h e   t r a y   i s   l o c k e d .      
 .  
  
 ; L S C A P I _ E _ I N V A L I D _ C O N F I G U R A T I O N   0 x C 0 0 4 2 0 4  
 M e s s a g e I D = 0 x 2 0 4  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   p r i n t   e n g i n e   c o n f i g u r a t i o n   s t r u c t u r e   h a s   a n   i n v a l i d   e n t r y  
 .  
  
 ; L S C A P I _ E _ N O _ R E A D _ D R I V E _ C A P A B I L I T I E S   0 x C 0 0 4 2 0 6  
 M e s s a g e I D = 0 x 2 0 6  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 D r i v e   c o m m u n i c a t i o n s   E r r o r :   U n a b l e   t o   r e a d   t h e   d r i v e   c a p a b i l i t i e s  
 .  
  
 ; L S C A P I _ E _ I N V A L I D _ D R I V E _ C A P A B I L I T I E S   0 x C 0 0 4 2 0 7  
 M e s s a g e I D = 0 x 2 0 7  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 D r i v e   E r r o r :   i n v a l i d   d r i v e   c a p a b i l i t y   p a r a m e t e r  
 .  
  
 ; L S C A P I _ E _ M E D I A _ I D _ D E C O D E _ F A I L E D   0 x C 0 0 4 2 0 A  
 M e s s a g e I D = 0 x 2 0 A  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 U n a b l e   t o   r e a d   t h e   L i g h t S c r i b e   d i s c   i n f o r m a t i o n   - p e r h a p s   t h e   d i s c   i s   d i r t y   o r   s c r a t c h e d  
 .  
  
 ; L S C A P I _ E _ M E D I A _ U N S U P P O R T E D _ I D _ V E R S I O N   0 x C 0 0 4 2 0 B  
 M e s s a g e I D = 0 x 2 0 B  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h i s   v e r s i o n   o f   t h e   L i g h t S c r i b e   m e d i a   i d e n t i f i c a t i o n   s t r u c t u r e   i s   n o t   s u p p o r t e d  
 .  
  
 ; L S C A P I _ E _ M E D I A _ U N K N O W N _ I D   0 x C 0 0 4 2 0 C  
 M e s s a g e I D = 0 x 2 0 C  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 U n s u p p o r t e d   m e d i a   i d e n t i f i c a t i o n   p a r a m e t e r  
 .  
  
 ; L S C A P I _ E _ M E D I A _ U N K N O W N _ S H A P E   0 x C 0 0 4 2 3 0  
 M e s s a g e I D = 0 x 2 3 0  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   m e d i a   i d e n t i f i c a t i o n   s h a p e   p a r a m e t e r   i s   n o t   r e c o g n i z e d .  
 .  
  
 ; L S C A P I _ E _ M E D I A _ S A F E T Y _ V I O L A T I O N   0 x C 0 0 4 2 0 D  
 M e s s a g e I D = 0 x 2 0 D  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A   d r i v e   r e s o u r c e   f i l e   p a r a m e t e r   v i o l a t e s   t h e   m e d i a   s a f e t y   l i m i t s .  
 .  
  
 ; L S C A P I _ E _ D R I V E _ O P E R A T I O N N O T S U P P O R T E D   0 x C 0 0 4 2 1 F  
 M e s s a g e I D = 0 x 2 1 F  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   d o e s   n o t   s u p p o r t   t h e   s p e c i f i c   o p e r a t i o n  
 .  
  
 ; L S C A P I _ E _ S P I N D L E _ S P E E D _ M I S M A T C H   0 x C 0 0 4 2 1 3  
 M e s s a g e I D = 0 x 2 1 3  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 C o m p u t e d   s p i n d l e   s p e e d   d o e s   n o t   m a t c h   d r i v e ' s   r e p o r t e d   v a l u e  
 .  
  
 ; L S C A P I _ E _ I M A G E _ E N H A N C E M E N T _ F A I L E D   0 x C 0 0 4 2 1 4  
 M e s s a g e I D = 0 x 2 1 4  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I m a g e   e n h a n c e m e n t   m o d u l e   f a i l e d  
 .  
  
 ; L S C A P I _ E _ P E _ H A L F T O N I N G _ F A I L E D   0 x C 0 0 4 2 1 5  
 M e s s a g e I D = 0 x 2 1 5  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 C o l o r   m a p p i n g   a n d   h a l f t o n i n g   m o d u l e   f a i l e d  
 .  
  
 ; L S C A P I _ E _ T R A C K _ M A P P I N G _ F A I L E D   0 x C 0 0 4 2 1 6  
 M e s s a g e I D = 0 x 2 1 6  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T r a c k   m a p p i n g   m o d u l e   f a i l e d  
 .  
  
 ; L S C A P I _ E _ M A R K I N G _ C O N T R O L _ F A I L E D   0 x C 0 0 4 2 1 7  
 M e s s a g e I D = 0 x 2 1 7  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 M a r k i n g   c o n t r o l   m o d u l e   f a i l e d  
 .  
  
 ; L S C A P I _ E _ U N S U P P O R T E D   0 x C 0 0 4 2 1 C  
 M e s s a g e I D = 0 x 2 1 C  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I n t e r n a l   E r r o r :   t h e   p r i n t   e n g i n e   e n c o u n t e r e d   a n   u n s u p p o r t e d   c o n d i t i o n  
 .  
  
 ; L S C A P I _ E _ N O T I N I T I A L I Z E D   0 x C 0 0 4 2 2 1  
 M e s s a g e I D = 0 x 2 2 1  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I n t e r n a l   E r r o r :   T h e   s y s t e m   i s   n o t   c o r r e c t l y   i n i t i a l i z e d  
 .  
  
 ; L S C A P I _ E _ N O T O P E N E D   0 x C 0 0 4 2 2 2  
 M e s s a g e I D = 0 x 2 2 2  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h i s   d r i v e / s e s s i o n   i s   n o   l o n g e r   o p e n  
 .  
  
 ; L S C A P I _ E _ N O _ R E A D _ D R I V E _ D E S C R I P T I O N   0 x C 0 0 4 2 2 7  
 M e s s a g e I D = 0 x 2 2 7  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 D r i v e   c o m m u n i c a t i o n s   E r r o r :   U n a b l e   t o   r e a d   t h e   d r i v e   d e s c r i p t i o n  
 .  
  
 ; L S C A P I _ E _ I N I T I A L I Z A T I O N _ F A I L U R E   0 x C 0 0 4 2 2 8  
 M e s s a g e I D = 0 x 2 2 8  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 S o f t w a r e   E r r o r :   f a i l e d   t o   i n i t i a l i z e  
 .  
  
 ; L S C A P I _ E _ I N V A L I D _ D R I V E _ N U M B E R   0 x C 0 0 4 2 3 1  
 M e s s a g e I D = 0 x 2 3 1  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   n u m b e r   d o e s   n o t   m a t c h   a   v a l i d   L i g h t S c r i b e   d i s c   d r i v e  
 .  
  
 ; L S C A P I _ E _ I M A G E _ E N H A N C E M E N T _ P A S S T H R O U G H   0 x C 0 0 4 2 3 2  
 M e s s a g e I D = 0 x 2 3 2  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   i m a g e   e n h a n c e m e n t   m o d u l e   w i l l   p a s s   t h e   b i t m a p   t h r o u g h   w i t h o u t   e n h a n c e m e n t .  
 .  
  
 ; L S C A P I _ E _ U N S U P P O R T E D _ D R I V E _ C A P A B I L I T Y _ V E R S I O N   0 x C 0 0 4 2 3 5  
 M e s s a g e I D = 0 x 2 3 5  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h i s   v e r s i o n   o f   t h e   L i g h t S c r i b e   d r i v e   c a p a b i l i t i e s   s t r u c t u r e   i s   n o t   s u p p o r t e d  
 .  
  
 ; L S C A P I _ E _ C O M M _ S U C C E S S   0 x C 0 0 4 1 0 0 0  
 M e s s a g e I D = 0 x 1 0 0 0  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   S C S I   c o m m a n d   s u c c e d e d  
 .  
  
 ; L S C A P I _ E _ S C S I _ F A I L E D   0 x C 0 0 4 1 0 0 1  
 M e s s a g e I D = 0 x 1 0 0 1  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A   S C S I   c o m m u n i c a t i o n   e r r o r   o c c u r e d :   H u g o   r e t u r n e d   F A I L E D  
 .  
  
 ; L S C A P I _ E _ S C S I _ P E N D I N G   0 x C 0 0 4 1 0 0 2  
 M e s s a g e I D = 0 x 1 0 0 2  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A   S C S I   c o m m u n i c a t i o n   e r r o r   o c c u r e d :   H u g o   r e t u r n e d   P E N D I N G  
 .  
  
 ; L S C A P I _ E _ S C S I _ B U S Y   0 x C 0 0 4 1 0 0 3  
 M e s s a g e I D = 0 x 1 0 0 3  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A   S C S I   c o m m u n i c a t i o n   e r r o r   o c c u r e d :   H u g o   r e t u r n e d   B U S Y  
 .  
  
 ; L S C A P I _ E _ S C S I _ I N V A L I D   0 x C 0 0 4 1 0 0 4  
 M e s s a g e I D = 0 x 1 0 0 4  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A   S C S I   c o m m u n i c a t i o n   e r r o r   o c c u r e d :   H u g o   r e t u r n e d   I N V A L I D  
 .  
  
 ; L S C A P I _ E _ S C S I _ E R R   0 x C 0 0 4 1 0 0 5  
 M e s s a g e I D = 0 x 1 0 0 5  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A   S C S I   c o m m u n i c a t i o n   e r r o r   o c c u r e d :   H u g o   r e t u r n e d   E R R  
 .  
  
 ; L S C A P I _ E _ P I P E _ E R R   0 x C 0 0 4 1 0 0 6  
 M e s s a g e I D = 0 x 1 0 0 6  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 N a m e d   p i p e   c o m m u n i c a t i o n   e r r o r  
 .  
  
 ; L S C A P I _ E _ S E R V I C E _ F A I L E D   0 x C 0 0 4 1 0 0 7  
 M e s s a g e I D = 0 x 1 0 0 7  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 U n a b l e   t o   s t a r t   t h e   G a t e w a y   S e r v i c e  
 .  
  
 ; L S C A P I _ E _ S C M _ F A I L U R E   0 x C 0 0 4 1 0 0 8  
 M e s s a g e I D = 0 x 1 0 0 8  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 O p e n S C M a n a g e r   f a i l e d  
 .  
  
 ; L S C A P I _ E _ C R E A T E S E R V I C E _ F A I L E D   0 x C 0 0 4 1 0 0 9  
 M e s s a g e I D = 0 x 1 0 0 9  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 C r e a t e S e r v i c e   f a i l e d  
 .  
  
 ; L S C A P I _ E _ D R I V E _ H A N D L E _ I N V A L I D   0 x C 0 0 4 1 0 0 A  
 M e s s a g e I D = 0 x 1 0 0 A  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I n v a l i d   d r i v e   h a n d l e  
 .  
  
 ; L S C A P I _ E _ G A T E W A Y _ P R O X Y _ E R R O R   0 x C 0 0 4 1 0 0 C  
 M e s s a g e I D = 0 x 1 0 0 C  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 G a t e w a y   P r o x y   E r r o r  
 .  
  
 ; L S C A P I _ E _ H U R R I C A N E _ C L I E N T _ A P I _ E R R O R   0 x C 0 0 4 1 0 0 E  
 M e s s a g e I D = 0 x 1 0 0 E  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A n   e r r o r   o c c u r e d   i n   t h e   G a t e w a y S e r v i c e P r o x y  
 .  
  
 ; L S C A P I _ E _ I L L E G A L _ P A R A M E T E R   0 x C 0 0 4 1 0 1 0  
 M e s s a g e I D = 0 x 1 0 1 0  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A n   i n v a l i d   p a r a m t e r   w a s   p a s s e d   i n t o   a   c o m m u n i c a t i o n   l a y e r   f u n c t i o n .  
 .  
  
 ; L S C A P I _ E _ D R I V E _ B U F F E R _ E R R O R   0 x C 0 0 4 1 0 1 3  
 M e s s a g e I D = 0 x 1 0 1 3  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 D r i v e   b u f f e r   n o t   e m p t y  
 .  
  
 ; L S C A P I _ E _ B U F F E R _ S P A C E _ U N A V A I L   0 x C 0 0 4 1 0 1 5  
 M e s s a g e I D = 0 x 1 0 1 5  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   b u f f e r   s p a c e   d a t a   i s   n o t   a v a i l a b l e   f r o m   t h e   d r i v e   ( N o t e   t h i s   i s   n o t   a n   e r r o r   c o n d i t i o n )  
 .  
  
 ; L S C A P I _ E _ T S U N A M I _ I N T E R N A L _ E R R O R   0 x C 0 0 4 1 0 1 6  
 M e s s a g e I D = 0 x 1 0 1 6  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A n   i n t e r n a l   e r r o r   o c c u r e d   i n   T s u n a m i  
 .  
  
 ; L S C A P I _ E _ T S U N A M I _ M U L T I P L E _ D R I V E S   0 x C 0 0 4 1 0 1 7  
 M e s s a g e I D = 0 x 1 0 1 7  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A t t e m p t   t o   o p e n   m o r e   t h a n   o n e   d r i v e   i n   a   t s u n a m i   o b j e c t  
 .  
  
 ; L S C A P I _ E _ C U R R E N T _ T R A C K _ I N V A L I D   0 x C 0 0 4 1 0 1 8  
 M e s s a g e I D = 0 x 1 0 1 8  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   r e t u r e d   d a t a   t o   s a y   c u r r e n t   t r a c k   n u m b e r   i n f o r m a t i o n   r e t u r n e d   w a s   i n v a l i d   -   N o t   a n   e r r o r ,   t h i s   i s   a   n o r m a l   c o n d i t i o n .    
 .  
  
 ; L S C A P I _ E _ N R _ L U _ I N _ P R O C E S S _ O F _ B E C O M I N G _ R E A D Y   0 x C 0 0 4 1 0 6 0  
 M e s s a g e I D = 0 x 1 0 6 0  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   L o g i c a l   U n i t   i s   i n   t h e   p r o c e s s   o f   b e c o m i n g   r e a d y  
 .  
  
 ; L S C A P I _ E _ N R _ I N I T _ R E Q D   0 x C 0 0 4 1 0 4 5  
 M e s s a g e I D = 0 x 1 0 4 5  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 N o t   r e a d y ,   l o g i c a l   u n i t   n o t   r e a d y ,   i n i t i a l i z i n g   c o m m a n d   r e q u i r e d  
 .  
  
 ; L S C A P I _ E _ N R _ O P _ I N _ P R O G E S S   0 x C 0 0 4 1 0 1 B  
 M e s s a g e I D = 0 x 1 0 1 B  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   i s   n o t   r e a d y ,   o p e r a t i o n   i n   p r o g e s s  
 .  
  
 ; L S C A P I _ E _ N R _ L O N G _ W R I T E   0 x C 0 0 4 1 0 1 C  
 M e s s a g e I D = 0 x 1 0 1 C  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   i s   n o t   r e a d y ,   l o n g   w r i t e   i n   p r o g r e s s  
 .  
  
 ; L S C A P I _ E _ N R _ S E L F T E S T   0 x C 0 0 4 1 0 1 D  
 M e s s a g e I D = 0 x 1 0 1 D  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   i s   n o t   r e a d y ,   s e l f   t e s t   i n   p r o g r e s s  
 .  
  
 ; L S C A P I _ E _ N R _ R E A D I N G _ M E D I A _ I D   0 x C 0 0 4 1 0 1 E  
 M e s s a g e I D = 0 x 1 0 1 E  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   i s   n o t   r e a d y ,   r e a d i n g   m e d i a   i d  
 .  
  
 ; L S C A P I _ E _ N R _ I N C O M P A T I B L E _ M E D I A   0 x C 0 0 4 1 0 1 F  
 M e s s a g e I D = 0 x 1 0 1 F  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   i s   n o t   r e a d y ,   i n c o m p a t i b l e   m e d i a  
 .  
  
 ; L S C A P I _ E _ M E _ N O T _ P R E S E N T _ T R A Y _ C L O S E D   0 x C 0 0 4 1 0 2 0  
 M e s s a g e I D = 0 x 1 0 2 0  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 M e d i a   E r r o r ,   m e d i a   n o t   p r e s e n t ,   t r a y   c l o s e d .   L i g h t S c r i b e   m e d i a   n o t   d e t e c t e d ,   p l e a s e   c h e c k  
 .  
  
 ; L S C A P I _ E _ M E _ N O T _ P R E S E N T _ T R A Y _ O P E N   0 x C 0 0 4 1 0 2 1  
 M e s s a g e I D = 0 x 1 0 2 1  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 M e d i a   E r r o r ,   m e d i a   n o t   p r e s e n t ,   t r a y   o p e n  
 .  
  
 ; L S C A P I _ E _ M E _ N O _ R E F E R E N C E   0 x C 0 0 4 1 0 2 2  
 M e s s a g e I D = 0 x 1 0 2 2  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 M e d i a   E r r o r ,   n o   r e f e r e n c e   p o s i t i o n   f o u n d  
 .  
  
 ; L S C A P I _ E _ M E _ S P O K E _ P A T T E R N _ E R R O R   0 x C 0 0 4 1 0 2 3  
 M e s s a g e I D = 0 x 1 0 2 3  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 M e d i a   E r r o r ,   s p o k e   p a t t e r n   e r r o r  
 .  
  
 ; L S C A P I _ E _ M E _ C A L _ P A T T E R N _ E R R O R   0 x C 0 0 4 1 0 2 4  
 M e s s a g e I D = 0 x 1 0 2 4  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 M e d i a   E r r o r ,   c a l i b r a t i o n   p a t t e r n   e r r o r  
 .  
  
 ; L S C A P I _ E _ M E _ M E D I A _ I D _ U N R E A D A B L E   0 x C 0 0 4 1 0 2 5  
 M e s s a g e I D = 0 x 1 0 2 5  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 M e d i a   E r r o r ,   m e d i a   i d   u n r e a d a b l e  
 .  
  
 ; L S C A P I _ E _ M E D I U M _ E R R O R   0 x C 0 0 4 1 0 8 B  
 M e s s a g e I D = 0 x 1 0 8 B  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A   m e d i u m   e r r o r   w a s   r e t u r n e d   ( s e n s e   k e y   3 )  
 .  
  
 ; L S C A P I _ E _ H E _ C O M M _ F A I L U R E   0 x C 0 0 4 1 0 2 6  
 M e s s a g e I D = 0 x 1 0 2 6  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   E r r o r ,   c o m m u n i c a t i o n   f a i l u r e  
 .  
  
 ; L S C A P I _ E _ H E _ T R A C K _ S E R V O _ E R R   0 x C 0 0 4 1 0 2 7  
 M e s s a g e I D = 0 x 1 0 2 7  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   E r r o r ,   t r a c k i n g   s e r v o   e r r o r  
 .  
  
 ; L S C A P I _ E _ H E _ F O C U S _ S E R V O _ E R R   0 x C 0 0 4 1 0 2 8  
 M e s s a g e I D = 0 x 1 0 2 8  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   E r r o r ,   f o c u s   s e r v o   e r r o r  
 .  
  
 ; L S C A P I _ E _ H E _ S P I N D L E _ S E R V O _ E R R   0 x C 0 0 4 1 0 2 9  
 M e s s a g e I D = 0 x 1 0 2 9  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   E r r o r ,   s p i n d l e   s e r v o   e r r o r  
 .  
  
 ; L S C A P I _ E _ H E _ L A S E R _ C A L _ E R R _ H U G O   0 x C 0 0 4 1 0 2 A  
 M e s s a g e I D = 0 x 1 0 2 A  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   E r r o r ,   l a s e r   c a l i b r a t i o n   e r r o r   ( H u g o   o n l y )  
 .  
  
 ; L S C A P I _ E _ H E _ F I N E _ A C T _ C A L _ E R R   0 x C 0 0 4 1 0 2 C  
 M e s s a g e I D = 0 x 1 0 2 C  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   E r r o r ,   f i n e   a c t u a t o r   ( t r a c k i n g   s e r v o )   c a l i b r a t i o n   e r r o r  
 .  
  
 ; L S C A P I _ E _ H E _ S L E D _ C A L _ E R R   0 x C 0 0 4 1 0 2 D  
 M e s s a g e I D = 0 x 1 0 2 D  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   E r r o r ,   s l e d   c a l i b r a t i o n   e r r o r  
 .  
  
 ; L S C A P I _ E _ H E _ F A I L E D _ T O _ S T A R T   0 x C 0 0 4 1 0 2 E  
 M e s s a g e I D = 0 x 1 0 2 E  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   E r r o r ,   d r i v e   f a i l e d   t o   s t a r t  
 .  
  
 ; L S C A P I _ E _ U N S U P P O R T E D _ D R I V E _ C A P A B I L I T I E S   0 x C 0 0 4 2 3 6  
 M e s s a g e I D = 0 x 2 3 6  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 U n s u p p o r t e d   d r i v e   c a p a b i l i t y   p a r a m e t e r  
 .  
  
 ; L S C A P I _ E _ O P E N _ C D R O M _ E R R O R   0 x C 0 0 4 1 0 0 B  
 M e s s a g e I D = 0 x 1 0 0 B  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 E r r o r   o p e n i n g   C D - R O M   d e v i c e  
 .  
  
 ; L S C A P I _ E _ G A T E W A Y _ P I P E _ E R R O R   0 x C 0 0 4 1 0 0 D  
 M e s s a g e I D = 0 x 1 0 0 D  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 G a t e w a y   S e r v i c e   P i p e   E r r o r  
 .  
  
 ; L S C A P I _ E _ S Y S T E M _ E R R O R   0 x C 0 0 4 1 0 0 F  
 M e s s a g e I D = 0 x 1 0 0 F  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A   W i n d o w s   s y s t e m   e r r o r   o c c u r e d  
 .  
  
 ; L S C A P I _ E _ L A S E R _ C A L _ F A I L   0 x C 0 0 4 1 0 1 1  
 M e s s a g e I D = 0 x 1 0 1 1  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   H u g o   l a s e r   c a l i b r a t i o n   r o u t i n e   f a i l e d  
 .  
  
 ; L S C A P I _ E _ H U G O _ B U F F E R _ U N A V A I L   0 x C 0 0 4 1 0 1 4  
 M e s s a g e I D = 0 x 1 0 1 4  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   H u g o   d r i v e   s h a r e d   m e m o r y   w r i t e   b u f f e r   w a s   n o t   a v a i l a b l e  
 .  
  
 ; L S C A P I _ E _ U N K N O W N _ S E N S E I N F O   0 x C 0 0 4 1 0 1 9  
 M e s s a g e I D = 0 x 1 0 1 9  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   r e t u r n e d   u n r e c o g n i z e d   s e n s e   d a t a  
 .  
  
 ; L S C A P I _ E _ H E _ T I M E O U T _ O N _ S T A R T U P   0 x C 0 0 4 1 0 2 F  
 M e s s a g e I D = 0 x 1 0 2 F  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   E r r o r ,   t i m e o u t   o n   s t a r t u p  
 .  
  
 ; L S C A P I _ E _ H E _ F I N E _ A C T _ T O O _ F A R _ O U T   0 x C 0 0 4 1 0 3 0  
 M e s s a g e I D = 0 x 1 0 3 0  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   E r r o r ,   f i n e   a c t u a t o r   t o o   f a r   o u t  
 .  
  
 ; L S C A P I _ E _ H E _ F I N E _ A C T _ T O O _ F A R _ I N   0 x C 0 0 4 1 0 3 1  
 M e s s a g e I D = 0 x 1 0 3 1  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   E r r o r ,   f i n e   a c t u a t o r   t o o   f a r   i n  
 .  
  
 ; L S C A P I _ E _ H E _ W R I T E _ E R R O R   0 x C 0 0 4 1 0 3 2  
 M e s s a g e I D = 0 x 1 0 3 2  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   E r r o r ,   w r i t e   e r r o r ,   r e c o v e r y   f a i l e d  
 .  
  
 ; L S C A P I _ E _ H E _ T I M E O U T   0 x C 0 0 4 1 0 3 3  
 M e s s a g e I D = 0 x 1 0 3 3  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   E r r o r ,   t i m e o u t   o n   l o g i c a l   u n i t  
 .  
  
 ; L S C A P I _ E _ H E _ L O A D _ E J E C T _ F A I L   0 x C 0 0 4 1 0 3 4  
 M e s s a g e I D = 0 x 1 0 3 4  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   E r r o r ,   m e d i a   l o a d / e j e c t   f a i l e d  
 .  
  
 ; L S C A P I _ E _ H E _ I N S U F _ R E S O U R C E S   0 x C 0 0 4 1 0 3 5  
 M e s s a g e I D = 0 x 1 0 3 5  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 H a r d w a r e   E r r o r ,   i n s u f f i c i e n t   r e s o u r c e s  
 .  
  
 ; L S C A P I _ E _ I R _ P A R A M _ L I S T _ L E N   0 x C 0 0 4 1 0 3 6  
 M e s s a g e I D = 0 x 1 0 3 6  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I l l e g a l   r e q u e s t ,   p a r a m e t e r   l i s t   l e n g t h  
 .  
  
 ; L S C A P I _ E _ I R _ U N S U P P O R T E D _ C O M M A N D   0 x C 0 0 4 1 0 3 7  
 M e s s a g e I D = 0 x 1 0 3 7  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I l l e g a l   r e q u e s t ,   u n s u p p o r t e d   c o m m a n d  
 .  
  
 ; L S C A P I _ E _ I R _ I N V A L I D _ C O M M A N D _ W R I T E   0 x C 0 0 4 1 0 3 8  
 M e s s a g e I D = 0 x 1 0 3 8  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I l l e g a l   r e q u e s t ,   i n v a l i d   c o m m a n d   o p - c o d e ,   i n   w r i t e - c a p a b l e   s t a t e  
 .  
  
 ; L S C A P I _ E _ I R _ I N V A L I D _ C O M M A N D _ I N _ C U R R E N T _ S T A T E _ H U G O   0 x C 0 0 4 1 0 3 9  
 M e s s a g e I D = 0 x 1 0 3 9  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I l l e g a l   r e q u e s t ,   i n v a l i d   c o m m a n d   o p - c o d e ,   i n   c u r r e n t   s t a t e   ( H u g o   o n l y )  
 .  
  
 ; L S C A P I _ E _ I R _ N O T _ I M P L E M E N T E D _ H U G O   0 x C 0 0 4 1 0 3 A  
 M e s s a g e I D = 0 x 1 0 3 A  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I l l e g a l   r e q u e s t ,   i n v a l i d   c o m m a n d   o p - c o d e ,   n o t   i m p l e m e n t e d   ( H u g o   o n l y )  
 .  
  
 ; L S C A P I _ E _ I R _ I N V A L I D _ C D B _ F I E L D   0 x C 0 0 4 1 0 3 B  
 M e s s a g e I D = 0 x 1 0 3 B  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I l l e g a l   r e q u e s t ,   i n v a l i d   f i e l d   i n   C D B  
 .  
  
 ; L S C A P I _ E _ I R _ I N V A L I D _ P A R A M _ F I E L D   0 x C 0 0 4 1 0 3 C  
 M e s s a g e I D = 0 x 1 0 3 C  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I l l e g a l   r e q u e s t ,   i n v a l i d   f i e l d   i n   p a r a m e t e r   l i s t ,   v a l u e   i n v a l i d  
 .  
  
 ; L S C A P I _ E _ I R _ S A V I N G _ N O T _ S U P P O R T E D   0 x C 0 0 4 1 0 3 D  
 M e s s a g e I D = 0 x 1 0 3 D  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I l l e g a l   r e q u e s t ,   s a v i n g   p a r a m e t e r s   n o t   s u p p o r t e d  
 .  
  
 ; L S C A P I _ E _ I R _ M E D I A _ R E M O V A L _ P R E V E N T E D   0 x C 0 0 4 1 0 3 E  
 M e s s a g e I D = 0 x 1 0 3 E  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I l l e g a l   r e q u e s t ,   m e d i a   r e m o v a l   p r e v e n t e d  
 .  
  
 ; L S C A P I _ E _ I R _ I N C O M P A T I B L E _ M E D I A   0 x C 0 0 4 1 0 9 D  
 M e s s a g e I D = 0 x 1 0 9 D  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 I l l e g a l   r e q u e s t ,   i n c o m p a t i b l e   m e d i a  
 .  
  
 ; L S C A P I _ E _ M E D I U M _ C H A N G E D   0 x C 0 0 4 1 0 A 2  
 M e s s a g e I D = 0 x 1 0 A 2  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 N o t   r e a d y   t o   r e a d y   c h a n g e ,   m e d i u m   m a y   h a v e   c h a n g e d  
 .  
  
 ; L S C A P I _ E _ N S _ P O W E R _ O N   0 x C 0 0 4 1 0 3 F  
 M e s s a g e I D = 0 x 1 0 3 F  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 N o   s e n s e ,   p o w e r   o n   o c c u r r e d  
 .  
  
 ; L S C A P I _ E _ N S _ B U S _ D E V I C E _ R E S E T   0 x C 0 0 4 1 0 4 0  
 M e s s a g e I D = 0 x 1 0 4 0  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 N o   s e n s e ,   b u s   d e v i c e   r e s e t   o c c u r r e d  
 .  
  
 ; L S C A P I _ E _ N S _ D E V I C E _ I N T E R N A L _ R E S E T   0 x C 0 0 4 1 0 4 1  
 M e s s a g e I D = 0 x 1 0 4 1  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 N o   s e n s e ,   d e v i c e   i n t e r n a l   r e s e t   o c c u r r e d  
 .  
  
 ; L S C A P I _ E _ N S _ O P _ M E D I A _ R E M O V A L _ R E Q   0 x C 0 0 4 1 0 4 2  
 M e s s a g e I D = 0 x 1 0 4 2  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 N o   s e n s e ,   o p e r a t o r   m e d i a   r e m o v a l   r e q u e s t  
 .  
  
 ; L S C A P I _ E _ N S _ T R A C E _ F A I L E D   0 x C 0 0 4 1 0 4 3  
 M e s s a g e I D = 0 x 1 0 4 3  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 N o   s e n s e ,   v e n d o r   s p e c i f i c ,   t r a c e   f a i l e d  
 .  
  
 ; L S C A P I _ E _ O P E N _ P I P E _ E R R O R   0 x C 0 0 4 1 0 4 6  
 M e s s a g e I D = 0 x 1 0 4 6  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 U n a b l e   t o   o p e n   t h e   L i g h t S c r i b e   G a t e w a y   S e r v i c e   n a m e d   p i p e  
 .  
  
 ; L S C A P I _ E _ C L O S E _ P I P E _ E R R O R   0 x C 0 0 4 1 0 4 7  
 M e s s a g e I D = 0 x 1 0 4 7  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 E r r o r   c l o s i n g   t h e   L i g h t S c r i b e   G a t e w a y   S e r v i c e   n a m e d   p i p e  
 .  
  
 ; L S C A P I _ E _ U N S U P P O R T E D _ L A B E L _ P A R A M S _ S T R U C T _ V E R S I O N   0 x C 0 0 4 1 0 4 8  
 M e s s a g e I D = 0 x 1 0 4 8  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
  
 	         T h e   p r i n t   e n g i n e   t r i e d   t o   s e n d   a   H U R R I C A N E _ D R I V E _ L A B E L I N G _ P A R A M S _ V x    
 	         s t r u c t u r e   t h a t   i s   n o t   s u p p o r t e d .  
 	 	  
 .  
  
 ; L S C A P I _ E _ U N S U P P O R T E D _ T R A C K _ H E A D E R _ S T R U C T _ V E R S I O N   0 x C 0 0 4 1 0 9 9  
 M e s s a g e I D = 0 x 1 0 9 9  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   p r i n t   e n g i n e   t r i e d   t o   s e n d   a   H U R R I C A N E _ T R A C K _ H E A D E R _ V x   s t r u c t u r e   t h a t   i s   n o t   s u p p o r t e d .  
 .  
  
 ; L S C A P I _ E _ U N S U P P O R T E D _ M E D I A _ P A R A M S _ V E R S I O N   0 x C 0 0 4 1 0 9 A  
 M e s s a g e I D = 0 x 1 0 9 A  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
  
 	         T h e   m e d i a   p a r a m s   s t r u c t u r e   r e t u r n e d   b y   t h e   d r i v e   i s   n o t   s u p p o r t e d ,   t h e r e f o r e   c a n n o t   r e t u r n   t h e   d a t a  
 	 	  
 .  
  
 ; L S C A P I _ E _ U N A B L E _ T O _ L O C K _ D R I V E   0 x C 0 0 4 1 0 4 9  
 M e s s a g e I D = 0 x 1 0 4 9  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   s y s t e m   w a s   n o t   a b l e   t o   g e t   a n   e x c l u s i v e   l o c k   o n   a   d r i v e  
 .  
  
 ; L S C A P I _ E _ F A I L E D _ T O _ U N L O C K _ D R I V E   0 x C 0 0 4 1 0 4 A  
 M e s s a g e I D = 0 x 1 0 4 A  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   s y s t e m   w a s   n o t   a b l e   t o   u n l o c k   a n   e x c l u s i v e   l o c k   o n   a   d r i v e  
 .  
  
 ; L S C A P I _ E _ R B C _ E R R O R   0 x C 0 0 4 1 0 4 B  
 M e s s a g e I D = 0 x 1 0 4 B  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   R e a d   B u f f e r   C a p a c i t y   S C S I   c o m m a n d   r e t u r n e d   a n   e r r o r   o r   a n   i n v a l i d   r e s p o n s e  
 .  
  
 ; L S C A P I _ E _ I N Q U I R Y _ E R R O R   0 x C 0 0 4 1 0 4 C  
 M e s s a g e I D = 0 x 1 0 4 C  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   I n q u i r y   S C S I   c o m m a n d   r e t u r n e d   a n   e r r o r   o r   a n   i n v a l i d   r e s p o n s e  
 .  
  
 ; L S C A P I _ E _ P R E V E N T _ A L L O W _ E R R O R   0 x C 0 0 4 1 0 4 D  
 M e s s a g e I D = 0 x 1 0 4 D  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   P r e v e n t / A l l o w   M e d i u m   R e m o v a l   S C S I   c o m m a n d   r e t u r n e d   a n   e r r o r   o r   a n   i n v a l i d   r e s p o n s e  
 .  
  
 ; L S C A P I _ E _ S T A R T _ S T O P _ E R R O R   0 x C 0 0 4 1 0 4 E  
 M e s s a g e I D = 0 x 1 0 4 E  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   S t a r t / S t o p   S C S I   c o m m a n d   r e t u r n e d   a n   e r r o r   o r   a n   i n v a l i d   r e s p o n s e  
 .  
  
 ; L S C A P I _ E _ T U R _ E R R O R   0 x C 0 0 4 1 0 4 F  
 M e s s a g e I D = 0 x 1 0 4 F  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   T e s t   U n i t   R e a d y   S C S I   c o m m a n d   r e t u r n e d   a n   e r r o r   o r   a n   i n v a l i d   r e s p o n s e  
 .  
  
 ; L S C A P I _ E _ G E T _ C O N F I G U R A T I O N _ E R R O R   0 x C 0 0 4 1 0 5 0  
 M e s s a g e I D = 0 x 1 0 5 0  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   G e t   C o n f i g u r a t i o n   S C S I   c o m m a n d   r e t u r n e d   a n   e r r o r   o r   a n   i n v a l i d   r e s p o n s e  
 .  
  
 ; L S C A P I _ E _ M O D E _ S E L E C T _ E R R O R   0 x C 0 0 4 1 0 5 1  
 M e s s a g e I D = 0 x 1 0 5 1  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   M o d e   S e l e c t   S C S I   c o m m a n d   r e t u r n e d   a n   e r r o r   o r   a n   i n v a l i d   r e s p o n s e  
 .  
  
 ; L S C A P I _ E _ M O D E _ S E N S E _ E R R O R   0 x C 0 0 4 1 0 5 2  
 M e s s a g e I D = 0 x 1 0 5 2  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   M o d e   S e n s e   S C S I   c o m m a n d   r e t u r n e d   a n   e r r o r   o r   a n   i n v a l i d   r e s p o n s e  
 .  
  
 ; L S C A P I _ E _ G E S N _ E R R O R   0 x C 0 0 4 1 0 9 E  
 M e s s a g e I D = 0 x 1 0 9 E  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   G E S N   S C S I   c o m m a n d   r e t u r n e d   a n   e r r o r   o r   a n   i n v a l i d   r e s p o n s e  
 .  
  
 ; L S C A P I _ E _ M O D E _ S E N S E _ P A G E _ C O D E _ E R R O R   0 x C 0 0 4 1 0 9 B  
 M e s s a g e I D = 0 x 1 0 9 B  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   p a g e   c o d e   r e t u r n e d   b y   t h e   d r i v e   w a s   n o t   w h a t   w a s   e x p e c t e d .  
 .  
  
 ; L S C A P I _ E _ M O D E _ S E N S E _ P A G E _ L E N G T H _ E R R O R   0 x C 0 0 4 1 0 9 C  
 M e s s a g e I D = 0 x 1 0 9 C  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   p a g e   l e n g t h   r e t u r n e d   b y   t h e   d r i v e   w a s   n o t   w h a t   w a s   e x p e c t e d .  
 .  
  
 ; L S C A P I _ E _ R E Q U E S T _ S E N S E _ E R R O R   0 x C 0 0 4 1 0 5 3  
 M e s s a g e I D = 0 x 1 0 5 3  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   R e q u e s t   S e n s e   S C S I   c o m m a n d   r e t u r n e d   a n   e r r o r   o r   a n   i n v a l i d   r e s p o n s e  
 .  
  
 ; L S C A P I _ E _ P R I N T _ E R R O R   0 x C 0 0 4 1 0 5 4  
 M e s s a g e I D = 0 x 1 0 5 4  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   P r i n t   S C S I   c o m m a n d   r e t u r n e d   a n   e r r o r   o r   a n   i n v a l i d   r e s p o n s e  
 .  
  
 ; L S C A P I _ E _ U N A B L E _ T O _ L O A D _ D R I V E _ A C C E S S _ L I B R A R Y   0 x C 0 0 4 1 0 6 1  
 M e s s a g e I D = 0 x 1 0 6 1  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 U n a b l e   t o   l o a d   t h e   d r i v e   a c c e s s   l i b r a r y   ( S o n i c   S o l u t i o n s   V x B l o c k )  
 .  
  
 ; L S C A P I _ E _ U N S U P P O R T E D _ L I G H T S C R I B E _ F I R M W A R E _ V E R S I O N   0 x C 0 0 4 1 0 6 2  
 M e s s a g e I D = 0 x 1 0 6 2  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   s u p p o r t s   L i g h t S c r i b e ,   b u t   t h e   v e r s i o n   i m p l e m e n t e d   b y   t h e   d r i v e   i s   n o t   s u p p o r t e d   b y   t h e   s o f t w a r e .   A n   s o f t w a r e   u p g r a d e   m a y   b e   r e q u i r e d .  
 .  
  
 ; L S C A P I _ E _ U N S U P P O R T E D _ D R I V E _ C A P S _ S T R U C T _ V E R S I O N   0 x C 0 0 4 1 0 8 2  
 M e s s a g e I D = 0 x 1 0 8 2  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   c a p s   s t r u c t u r e   r e t u r n e d   b y   t h e   d r i v e   i s   n o t   s u p p o r t e d ,   t h e r e f o r e   c a n n o t   r e t u r n   t h e   d a t a  
 .  
  
 ; L S C A P I _ E _ L O C K _ U N L O C K _ D R I V E _ E R R O R   0 x C 0 0 4 1 0 8 3  
 M e s s a g e I D = 0 x 1 0 8 3  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   a c c e s s   l i b r a r y   w a s   n o t   a b l e   t o   l o c k   o r   u n l o c k   t h e   d r i v e  
 .  
  
 ; L S C A P I _ E _ Q U E R Y _ D R I V E _ E R R O R   0 x C 0 0 4 1 0 8 4  
 M e s s a g e I D = 0 x 1 0 8 4  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   a c c e s s   l i b r a r y   w a s   n o t   a b l e   t o   l o c k   o r   u n l o c k   t h e   d r i v e  
 .  
  
 ; L S C A P I _ E _ D E V I C E I O C O N T R O L _ E R R O R   0 x C 0 0 4 1 0 8 5  
 M e s s a g e I D = 0 x 1 0 8 5  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A   D e v i c e I o C o n t r o l   ( S P T I )   f u n c t i o n   f a i l e d   -   c h e c k   t h e   E v e n t   V i e w e r   t o   s e e   a c t u a l   e r r o r   c o d e  
 .  
  
 ; L S C A P I _ E _ P I P E _ B U S Y _ E R R O R   0 x C 0 0 4 1 0 8 6  
 M e s s a g e I D = 0 x 1 0 8 6  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 E R R O R _ P I P E _ B U S Y   w a s   r e t u r n e d  
 .  
  
 ; L S C A P I _ N O _ S E N S E _ E R R O R   0 x C 0 0 4 1 0 8 8  
 M e s s a g e I D = 0 x 1 0 8 8  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 S C S I   N o   S e n s e   E r r o r   s e n s e   c o d e   r e t u r n e d   ( s e n s e   k e y   0 )  
 .  
  
 ; L S C A P I _ R E C O V E R E D _ E R R O R   0 x C 0 0 4 1 0 8 9  
 M e s s a g e I D = 0 x 1 0 8 9  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 S C S I   R e c o v e r e d   E r r o r   s e n s e   c o d e   r e t u r n e d   ( s e n s e   k e y   1 )  
 .  
  
 ; L S C A P I _ E _ D R I V E _ N O T _ R E A D Y   0 x C 0 0 4 1 0 8 A  
 M e s s a g e I D = 0 x 1 0 8 A  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A   d r i v e   n o t   r e a d y   e r r o r   w a s   r e t u r n e d   ( s e n s e   k e y   2 )  
 .  
  
 ; L S C A P I _ E _ H A R D W A R E _ E R R O R   0 x C 0 0 4 1 0 8 C  
 M e s s a g e I D = 0 x 1 0 8 C  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A   h a r d w a r e   e r r o r   w a s   r e t u r n e d   ( s e n s e   k e y   4 )  
 .  
  
 ; L S C A P I _ E _ I L L E G A L _ R E Q U E S T   0 x C 0 0 4 1 0 8 D  
 M e s s a g e I D = 0 x 1 0 8 D  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A n   i l l e g a l   r e q u e s t   e r r o r   w a s   r e t u r n e d   ( s e n s e   k e y   5 )  
 .  
  
 ; L S C A P I _ E _ U N I T _ A T T E N T I O N   0 x C 0 0 4 1 0 8 E  
 M e s s a g e I D = 0 x 1 0 8 E  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 U n i t   A t t e n t i o n   ( s e n s e   k e y   6 )   w a s   r e t u r n e d  
 .  
  
 ; L S C A P I _ E _ D A T A _ P R O T E C T   0 x C 0 0 4 1 0 8 F  
 M e s s a g e I D = 0 x 1 0 8 F  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 D a t a   P r o t e c t   ( s e n s e   k e y   7 )   w a s   r e t u r n e d  
 .  
  
 ; L S C A P I _ E _ B L A N K _ C H E C K   0 x C 0 0 4 1 0 9 0  
 M e s s a g e I D = 0 x 1 0 9 0  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 B l a n k   C h e c k   ( s e n s e   k e y   8 )   w a s   r e t u r n e d  
 .  
  
 ; L S C A P I _ E _ V E N D O R _ S P E C I F I C   0 x C 0 0 4 1 0 9 1  
 M e s s a g e I D = 0 x 1 0 9 1  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 V e n d o r   S p e c i f i c   ( s e n s e   k e y   9 )   w a s   r e t u r n e d  
 .  
  
 ; L S C A P I _ E _ C O P Y _ A B O R T E D   0 x C 0 0 4 1 0 9 2  
 M e s s a g e I D = 0 x 1 0 9 2  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 C o p y   A b o r t e d   ( s e n s e   k e y   0 A h )   w a s   r e t u r n e d  
 .  
  
 ; L S C A P I _ E _ A B O R T E D _ C O M M M A N D   0 x C 0 0 4 1 0 9 3  
 M e s s a g e I D = 0 x 1 0 9 3  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A b o r t e d   C o m m a n d   ( s e n s e   k e y   0 B h )   w a s   r e t u r n e d  
 .  
  
 ; L S C A P I _ E _ O B S O L E T E   0 x C 0 0 4 1 0 9 4  
 M e s s a g e I D = 0 x 1 0 9 4  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 O b s e l e t e   ( s e n s e   k e y   0 C h )   w a s   r e t u r n e d  
 .  
  
 ; L S C A P I _ E _ V O L U M E _ O V E R F L O W   0 x C 0 0 4 1 0 9 5  
 M e s s a g e I D = 0 x 1 0 9 5  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 V o l u m e   O v e r f l o w   ( s e n s e   k e y   0 D h )   w a s   r e t u r n e d  
 .  
  
 ; L S C A P I _ E _ M I S C O M P A R E   0 x C 0 0 4 1 0 9 6  
 M e s s a g e I D = 0 x 1 0 9 6  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 M i s c o m p a r e   ( s e n s e   k e y   0 E h )   w a s   r e t u r n e d  
 .  
  
 ; L S C A P I _ E _ R E S E R V E D   0 x C 0 0 4 1 0 9 7  
 M e s s a g e I D = 0 x 1 0 9 7  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 R e s e r v e d   ( s e n s e   k e y   0 F h )   w a s   r e t u r n e d  
 .  
  
 ; L S C A P I _ N R _ M E D I U M _ N O T _ P R E S E N T   0 x C 0 0 4 1 0 9 8  
 M e s s a g e I D = 0 x 1 0 9 8  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 M e d i u m   n o t   p r e s e n t   e r r o r   w a s   r e t u r n e d  
 .  
  
 ; L S C A P I _ E _ T R A Y _ O P E N _ E R R O R   0 x C 0 0 4 1 0 9 F  
 M e s s a g e I D = 0 x 1 0 9 F  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 C a n n o t   r e a d   t h e   m e d i a   b e c a u s e   t h e   t r a y   i s   o p e n .   P l e a s e   i n s e r t   L i g h t S c r i b e   m e d i a  
 .  
  
 ; L S C A P I _ E _ I N D E X _ F I N D _ E R R O R   0 x C 0 0 4 1 0 A 0  
 M e s s a g e I D = 0 x 1 0 A 0  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   c a n n o t   f i n d   t h e   m e d i a   I D   i n d e x   m a r k .  
 .  
  
 ; L S C A P I _ E _ C O M M _ N O _ M E D I A   0 x C 0 0 4 1 0 A 1  
 M e s s a g e I D = 0 x 1 0 A 1  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 N o   m e d i a   d e t e c t e d .  
 .  
  
 ; L S C A P I _ S C S I _ L A Y E R _ E R R O R   0 x C 0 0 4 1 0 A 3  
 M e s s a g e I D = 0 x 1 0 A 3  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A n   e r r o r   o c c u r e d   i n   t h e   S C S I   c o m m u n i c a t i o n s   c o d e  
 .  
  
 ; L S C A P I _ S C S I _ T I M E O U T _ E R R O R   0 x C 0 0 4 1 0 A 4  
 M e s s a g e I D = 0 x 1 0 A 4  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A   S C S I   c o m m a n d   t i m e d - o u t  
 .  
  
 ; L S C A P I _ N R _ M E D I U M _ R E M O V A L _ P R E V E N T E D   0 x C 0 0 4 1 0 A 5  
 M e s s a g e I D = 0 x 1 0 A 5  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   t r a y   h a s   b e e n   l o c k e d   a n d   c a n n o t   b e   o p e n e d .  
 .  
  
 ; L S C A P I _ E _ P R I N T _ H U N G   0 x C 0 0 4 1 0 A 6  
 M e s s a g e I D = 0 x 1 0 A 6  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 T h e   d r i v e   h a s   g o n e   i n t o   a   l o o p   w h e r e   i t   k e e p s   r e t u r n i n g   l o n g   w r i t e   i n   p r o g r e s s  
 .  
  
 ; L S C A P I _ E _ E R R O R _ S H A R I N G _ V I O L A T I O N   0 x C 0 0 4 1 0 A 7  
 M e s s a g e I D = 0 x 1 0 A 7  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
 A n o t h e r   d r i v e   i s   o p e n   o r   t h i s   d r i v e   i s   i n   u s e .      
 .  
  
 ; L S C A P I _ E _ L A S T   0 x C 0 0 4 1 0 A 8  
 M e s s a g e I D = 0 x 1 0 A 8  
 S e v e r i t y = E r r o r  
 F a c i l i t y = L S C A P I  
 L a n g u a g e = u s  
  
 .  
 