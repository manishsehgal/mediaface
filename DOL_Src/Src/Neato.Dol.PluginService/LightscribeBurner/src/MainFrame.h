#pragma once

#include "UserSwitchDetector.h"
#include "SystemStateChangeListener.h"

/**
* main window of the application
*/
class CMainFrame : public CFrameWnd
{

protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)


	// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	SystemStateChangeListener	*m_listener; //!<object that listens to events

	/**
	* handle XP quick switch
	*/
	CUserSwitchDetector m_switchDetector;

	// Generated message map functions
protected:
	/**
	* window creation event
	*/

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
public:
	/**
	* Diagnostics menu callback
	*/
	afx_msg void OnLightScribeDiagnostics();

	/**
	* Process a WM_POWERBROADCAST power event. 
	*/
	afx_msg LRESULT OnPowerBroadcast(WPARAM wPowerEvent,LPARAM lP);

	/**
	* Process a WM_QUERYENDSESSION message. 
	*/
	afx_msg LRESULT OnQueryEndSession(WPARAM wP,LPARAM lP);

	/**
	* Process a WM_ENDSESSION message. 
	*/
	afx_msg LRESULT OnEndSession(WPARAM sessionEnding,LPARAM logOff) ;

	/**
	* Process a WM_DEVICECHANGE. 
	*/
	afx_msg LRESULT OnDeviceChange(WPARAM wP,LPARAM lP);

	/**
	* Process a WM_WTSSESSION_CHANGE. 
	*/
	afx_msg LRESULT OnSessionChange(WPARAM wP,LPARAM lP);

	/**
	* Set the listener to events. Only call this from the GUI thread. 
	* @param listener New listner - can be NULL to end listening.
	*/
	void SetSystemStateChangeListener(SystemStateChangeListener *listener) 
	{
		m_listener=listener;
	}

	/**
	* Override a base class implementation, to make sure we catch WM_QUERYENDSESSION calls. 
	* We forward this to our expanded message handler. 
	*/
	afx_msg BOOL OnQueryEndSession( );

};


