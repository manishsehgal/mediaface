/** @file BitmapDoc.cpp : implementation of the CBitmapDoc class
*/
#include "stdafx.h"
#include "ImageBurner.h"
#include "BitmapDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CBitmapDoc

IMPLEMENT_DYNCREATE(CBitmapDoc, CDocument)

BEGIN_MESSAGE_MAP(CBitmapDoc, CDocument)
END_MESSAGE_MAP()

// CBitmapDoc construction/destruction

CBitmapDoc::CBitmapDoc()
{
}

CBitmapDoc::~CBitmapDoc()
{
}

BOOL CBitmapDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	m_bitmap.Empty();
	return TRUE;
}

// CBitmapDoc serialization

void CBitmapDoc::Serialize(CArchive& ar)
{
	//hand off s13n to the dib class
	m_bitmap.Serialize(ar);
}

// CBitmapDoc diagnostics

#ifdef _DEBUG
void CBitmapDoc::AssertValid() const
{
	CDocument::AssertValid();
	m_bitmap.AssertValid();
}

void CBitmapDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
	m_bitmap.Dump(dc);
}
#endif //_DEBUG

/**
* draw the bitmap. 
* @param pDC device context to draw to (may be a metafile)
* @param drawRectangle rect to draw into. 
* @param scale flag to request scaling
* @param return true if we painted anything
* @todo scaling
*/
bool CBitmapDoc::Draw(CDC* pDC,CRect drawRectangle,bool scale)  {

	//bail out early if no image
	if(!HasImage()) 
	{
		return false;
	}
	if(scale) 
	{
		CPoint origin=drawRectangle.TopLeft();
		CSize widthAndHeight=drawRectangle.Size();
		m_bitmap.Draw(pDC,origin,widthAndHeight);
	} else
	{
		CRect src=m_bitmap.GetImageRect();
		m_bitmap.SetDIBits(pDC->GetSafeHdc(),
			&src,
			&drawRectangle);
	}
	return true;
}