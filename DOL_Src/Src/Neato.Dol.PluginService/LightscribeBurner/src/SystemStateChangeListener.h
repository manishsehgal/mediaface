#pragma once
#pragma warning(disable: 4100)
#include <dbt.h>

/**
* Interface for classes to implement if they want to listen to system state changes
* sent by the toplevel window to some other dialog or system component
* that cares about them. Using an interface decouples the main window from 
* knowing or caring about who actually implemented the object. 
*/
class SystemStateChangeListener 
{
public:

	/**
	* Process power broadcast events. 
	* This message is sometimes received as a SendMessage() call, in which case the return value is 
	* important. Also, resume messages may be received before the UI is fully functional.
	* @param wPowerEvent Which of the events defined in pbt.h matter. 
	* @param data Event specific data. 
	* @return Value to be returned to whomever sent the message to the application.
	*/

	virtual LRESULT OnPowerBroadcast(WPARAM wPowerEvent,LPARAM data)
	{
		return TRUE;
	}

	/**
	* Callback for a WM_QUERYENDSESSION message.
	* @param loggingOffOnly Logging off flag; if false a full exit is in progresss. 
	*   Only useful on Windows XP and later.
	* @return true if the request to exit windows is to be allowed. 
	*/
	virtual bool OnQueryEndSession(bool loggingOffOnly )
	{
		return true;
	}

	/**
	* Callback for a WM_ENDSESSION message.
	* @param loggingOffOnly Logging off flag; if false a full exit is in progresss. 
	*   Only useful on Windows XP and later.
	*/
	virtual void OnEndSession(bool loggingOffOnly )
	{};

	/**
	* Callback on a PnP device add. 
	* (see WM_DEVICECHANGE/DBT_DEVICEARRIVAL)
	*/
	virtual void OnPnPDeviceAdd(DEV_BROADCAST_HDR *deviceInfo)
	{};

	/**
	* Callback on a PnP device remove. 
	* (see WM_DEVICECHANGE/DBT_DEVICEREMOVECOMPLETE)
	*/
	virtual void OnPnPDeviceRemoval(DEV_BROADCAST_HDR *deviceInfo)
	{};

	/**
	* Callback on a PnP device node change. 
	* (see WM_DEVICECHANGE/DBT_DEVNODES_CHANGED)
	*/
	virtual void OnPnPNodeChange(DEV_BROADCAST_HDR *deviceInfo)
	{};

	/**
	* Called on WinXP fast user switch. 
	* At this point the system has probably already switched. 
	*/
	virtual void OnFastUserSwitch(int event)
	{};


};

#pragma warning(default: 4100)
