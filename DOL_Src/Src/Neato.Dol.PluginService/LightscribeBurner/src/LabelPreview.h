#pragma once
#include "afxwin.h"
#include "cdib.h"
#include "BurnControl.h"
#include "ComHelper.h"


class CBitmapControl: public CStatic
{
public:
	/**
	* Bitmap used for painting
	*/
	CDib m_bitmap;

	/**
	* Load an image. 
	* @throws std::exception on failure
	*/
	void LoadImage(LPCTSTR pathname)
	{
		m_bitmap.Load(pathname);
	}

	virtual void DrawItem(LPDRAWITEMSTRUCT drawInfo)
	{
		if(m_bitmap.IsValid())
		{
			RECT src;
			m_bitmap.GetSizeRect(&src);
			m_bitmap.ScaleDIBits(drawInfo->hDC,&src ,&drawInfo->rcItem);
		}
	}

};


class CLabelPreview : public CDialog
{
	DECLARE_DYNAMIC(CLabelPreview);

public:
	CLabelPreview(CBurnControl &owner,
		ComHelper::PrintOptions &options,
		CWnd* pParent = NULL);   
	virtual ~CLabelPreview();

	// Dialog Data
	enum { IDD = IDD_PRINTPREVIEW };

	
	CStatic m_PreviewType;

	afx_msg void OnBnClickedChangeOptions();

	/**
	* At init time, load the preview image. 
	*/
	BOOL OnInitDialog();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()


	/**
	* Load the image from m_imageFile.
	*/
	void LoadPreviewImage();

	/**
	* Print options
	*/
	ComHelper::PrintOptions m_options;

	/**
	* Name of the image file. This is loaded when the dialog is created. 
	*/
	CString m_imageFile;

	/**
	* The preview pane. 
	*/
	CBitmapControl m_previewPane;

	/**
	* Reference to the burn control that owns this object. 
	*/
	CBurnControl& m_owner;

	/**
	* Execute the preview. 
	* @param w Image width
	* @param h Image height
	*/
	void DoPreview(int w, int h);

	/**
	* Try to run a preview with the given options - this is used for the fallback mechanism. 
	*/
	void TryPreview(int width, int height, MediaOptimizationLevel optLevel, bool ignoreMedia);

	/**
	* Force a repaint. 
	*/
	void ForceRepaint();

};
