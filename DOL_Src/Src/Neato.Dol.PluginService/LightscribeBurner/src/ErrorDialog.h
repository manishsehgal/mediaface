#pragma once
#include "afxwin.h"
#include "afxstr.h"
#include "resource.h"

class CErrorDialog : public CDialog
{
	DECLARE_DYNAMIC(CErrorDialog)

public:
	CErrorDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CErrorDialog();

	CErrorDialog(CString errorString, bool showUpdate=false, bool showGeneric=false,
				 bool showOK=true, bool showCancel=true, CWnd* pParent = NULL);

// Dialog Data
	enum { IDD = IDD_ERROR };

public:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	DECLARE_MESSAGE_MAP()

	afx_msg void OnBnClickedGeneric();
	afx_msg void OnBnClickedUpdate();
	
private:
	CButton m_btnUpdate;
	CButton m_btnOK;
	CButton m_btnCancel;
	CButton m_btnGeneric;
	CStatic m_ErrorText;
	
	CString _errorString;
	
	bool _showUpdate;
	bool _showOK;
	bool _showCancel;
	bool _showGeneric;

	bool _isCustom; // true if we have the preceding data

    /**
	* Show/Hide and layout the controls based on the configuration
	*/
	void UpdateControls();

};
