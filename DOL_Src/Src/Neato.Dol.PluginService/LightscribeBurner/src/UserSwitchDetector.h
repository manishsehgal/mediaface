#pragma once

#include "Wtsapi32.h"
/**
* This class loads and uses windows terminal server switching to register a window to receive
* switch callbacks on.
* We use runtime DLL loading because Win2K doesnt have everything in its DLL. 
*/
class CUserSwitchDetector
{
	HWND m_owner;

	HMODULE m_wtsLib;

public:

	CUserSwitchDetector()
		: m_wtsLib(NULL),
		m_owner(NULL)
	{

	}


	/**
	* Create and register 
	*/
	CUserSwitchDetector(CWnd *owner)
		: m_wtsLib(NULL),
		m_owner(NULL)
	{
		Register(owner);
	}

	/**
	* Register; this will be auto-unregistered on destroy
	*/
	void Register(CWnd *owner)
	{
		Unregister();
		m_owner=owner->m_hWnd;
		WTSRegisterSessionNotification(m_owner,NOTIFY_FOR_THIS_SESSION);
	}

	/**
	* Unregister once for this object.
	*/
	void Unregister()
	{
		if(m_owner!=NULL)
		{
			WTSUnRegisterSessionNotification(m_owner);
			m_owner=NULL;
		}
	}

	/**
	* Unregister if needed
	*/
	~CUserSwitchDetector()
	{
		Unregister();
	}

private:

	bool Bind()
	{
		UINT oldMode=SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOOPENFILEERRORBOX);
		m_wtsLib=::LoadLibrary(_T("wtsapi32.dll"));
		SetErrorMode(oldMode);
		return IsValid();
	}

	bool IsValid()
	{
		return m_wtsLib!=NULL;
	}

	/**
	* Type def of the register call
	*/
	typedef BOOL (WINAPI * _t_WTS_RSN)(HWND,DWORD);

	/**
	* Type def of the unregister call
	*/
	typedef BOOL (WINAPI *t_WTS_USN)(HWND);


	/**
	* Load and call the ::WTSRegisterSessionNotification function. 
	* @return true if the call succeeded
	*/
	bool WTSRegisterSessionNotification( HWND hWnd, DWORD dwFlags)
	{
		if(!IsValid())
		{
			return false;
		}
		_t_WTS_RSN RegisterNotification=(_t_WTS_RSN)GetProcAddress(m_wtsLib,"WTSRegisterSessionNotification");
		if(RegisterNotification==NULL)
		{
			return false;
		}
		return RegisterNotification(hWnd,dwFlags)!=0;
	}


	/**
	* Load an call the ::WTSUnRegisterSessionNotification function. 
	* @return true if the call succeeded
	*/
	bool WTSUnRegisterSessionNotification( HWND hWnd)
	{
		if(!IsValid())
		{
			return false;
		}
		t_WTS_USN method=(t_WTS_USN)GetProcAddress(m_wtsLib,"WTSUnRegisterSessionNotification");
		if(method==NULL)
		{
			return false;
		}
		return method(hWnd)!=0;
	}

};