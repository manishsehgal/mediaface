/** @file cdib.h declaration of CDib class
*/
#pragma once
#include "Bitmap.h"

/**
 * This class integrates the CDIBitmap bitmap class with 
 * MFC by way of a mixin class. 
 * One function of this <i>adaptor</i> class performs is the mapping of 
 * std::exception instances into CException classes during serialization, so that
 * that the MFC containers catch errors correctly. 
 */
class CDib : public CObject, public CDIBitmap
{
	DECLARE_SERIAL(CDib)
private:
public:
	/**
	 * simple constructor
	 */
	CDib(); 

/**
 * construct a new bitmap
 * @param width with of image
 * @param height image height
 * @param depth image depth in pixeld
 * @throws std::exceptions in the case of trouble
 */
	CDib(int width,int height,int depth);

	/**
	destructor virtualized for subclass benefit
	*/
	virtual ~CDib(); 

	/**
 	 * get X and Y dimensions of the image
	 * @return a size struct of the values
	 */
	CSize GetDimensions();
	/**
	 * draw an image
	 * @param pDC device context
	 * @param origin where to draw in the DC
	 * @param size rectangle size to draw into
	 */
	 
	BOOL Draw(CDC* pDC, CPoint origin, CSize size);
	

	/**
	* read a dib from an open file
	* @param pFile open file
	* @exception CException* if something went wrong in the file IO
	*/
	void Read(CFile* pFile);

	/**
	 * serialize to/from an MFC archive
	 * @param ar archive
	 * @exception CException* if something went wrong in the file IO
	 */
	void Serialize(CArchive& ar);

	inline LPBITMAPINFOHEADER GetBitmapHeader() 
	{
		return GetInfoHeader();
	}

	inline int GetHeaderLength()
	{
		return (int)GetHeaderSize();
	}

	inline int GetImageLength()
	{
		return (int)GetImageSize();
	}

	CRect GetImageRect()
	{
		return CRect(0,0,GetWidth(),GetHeight());
	}
	void Empty() {
	}
};