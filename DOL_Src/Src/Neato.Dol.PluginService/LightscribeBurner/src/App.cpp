/** @file App.cpp
* The application class. 
*/
#include "stdafx.h"
#include "ImageBurner.h"
#include "App.h"
#include "MainFrame.h"
#include "BurnControl.h"

#include "BitmapView.h"
#include "afxwin.h"
#include "ComHelper.h"

#include "LSCommandLineInfo.h"
#include "WindowHelper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CApp

BEGIN_MESSAGE_MAP(CApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_UPDATE_COMMAND_UI(ID_DEBUG_LASTCOMERROR, OnUpdateDebugLastcomerror)
	ON_COMMAND(ID_DEBUG_LASTCOMERROR, OnDebugLastcomerror)
END_MESSAGE_MAP()


/**
* CApp construction
*/

CApp::CApp()
{
	m_InitResult = 0;
}


// The one and only CApp object
CApp theApp;

// CApp initialization
BOOL CApp::InitInstance()
{
	// InitCommonControls() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	InitCommonControls();

	CWinApp::InitInstance();

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	// Parse command line for standard shell commands, DDE, file open
	CLSCommandLineInfo cmdInfo;

	ParseCommandLine(cmdInfo);

    m_version = cmdInfo.m_version;

	if (cmdInfo.m_strFileName[0] == _T('\0')) {
		// command-line mode
	   DetectDevice();
	   return FALSE;
    }

	// Standard initialization
	SetRegistryKey(_T("Hewlett-Packard"));

	m_bPrintToFile=false;
	m_printFilename=_T("C:\\lightscribe.hgo");

	LoadStdProfileSettings(6);  // Load standard INI file options (including MRU)
	
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CBitmapDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CBitmapView));
	AddDocTemplate(pDocTemplate);

	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it
	//m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->ShowWindow(SW_SHOWMINIMIZED);
	CWindowHelper::ActivateWindow(m_pMainWnd->m_hWnd);

	m_pMainWnd->UpdateWindow();
	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand
	// Enable drag/drop open
	m_pMainWnd->DragAcceptFiles();
 
   PrintDocument(cmdInfo.m_strFileName, cmdInfo.m_lsMode, 
					cmdInfo.m_lsQuality[0]-'0' , cmdInfo.m_silent == "1" );
   ::DeleteFile(cmdInfo.m_strFileName);
	   return FALSE;
}

int CApp::ExitInstance( )
{
	return m_InitResult;
}

bool CApp::DetectDevice()
{
	if (!ComHelper::IsPrintAPIFound())
	{
		m_InitResult = 1;
		return FALSE;
	}
	if (!ComHelper::AreAllNeededLibrariesPresent())
	{
		m_InitResult = 2;
		return FALSE;
	}
	DriveArray drives;
	ULONG size = 0;
    try {
        size = ComHelper::EnumerateDrives(&drives);
    } catch (...) {
    }
	if (size == 0)
	{
		m_InitResult = 3;
		return FALSE;
	}
	return FALSE;
}

bool CApp::PrintDocument(LPCTSTR filename, LPCTSTR mode, int quality, bool bSilent)
{
	if (!ComHelper::IsPrintAPIFound())
	{
		AfxMessageBox(IDS_PRINT_DLL_UNREGISTERED,MB_OK);
		return false;
	}
	
	CDib image;
	image.Load(filename);
	image.PatchResolution();
	CBurnControl burnControl(m_pMainWnd);
	return burnControl.BurnBitmap(image, CString(mode), quality, bSilent);
}

// Show the about dialog
void CApp::OnAppAbout()
{
}


CApp &CApp::GetApplication() 
{ return theApp;}


void CApp::OnUpdateDebugLastcomerror(CCmdUI *pCmdUI)
{
	ComHelper::ProcessedComError err=ComHelper::GetLastReportedComError();
	pCmdUI->Enable(err.hr!=S_OK);
}

bool CApp::PlaceStringOnClipboard(CString &text)
{
	if ( !AfxGetMainWnd()->OpenClipboard() )
	{
		return false;
	}
	// Remove the current Clipboard contents
	if( !EmptyClipboard() )
	{
		return false;
	}

	// Allocate a global memory object for the text. 
	int length=(text.GetLength()+1) * sizeof(TCHAR);
	HGLOBAL hData = GlobalAlloc(GMEM_MOVEABLE, length); 
	if (hData == NULL) 
	{ 
		CloseClipboard(); 
		return false; 
	} 

	// Lock the handle and copy the text to the buffer. 
	LPVOID dest= GlobalLock(hData); 
	memcpy(dest, (LPCTSTR)text,length); 
	GlobalUnlock(hData); 

	// For the appropriate data formats...
	if ( ::SetClipboardData( CF_TEXT, hData ) == NULL )
	{
		TRACE0( "Unable to set Clipboard data" );
		CloseClipboard();
		return false;
	}
	CloseClipboard();
	return true;

}

void CApp::OnDebugLastcomerror()
{
	ComHelper::ProcessedComError err=ComHelper::GetLastReportedComError();
	CString message;
	message.Format(
		_T("COM Error %008X:\r\n"
		"%s\r\n"
		"[%s]"
		),
		err.hr,
		(LPCTSTR)err.faultMessage,
		(LPCTSTR)err.errorInfo);
	PlaceStringOnClipboard(message);
	AfxMessageBox(message,MB_ICONERROR);
}


