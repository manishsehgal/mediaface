#include "stdafx.h"
#include "ImageBurner.h"
#include "BitmapView.h"
#include "Constants.h"
#include "PrintControl.h"
#include "LightScribeProgressDialog.h"
#include "ErrorDialog.h"
#include "ComHelper.h"

/**
* This message gets (re) initialized in the constructor
*/
UINT CLightScribeProgressDialog::m_progressMessage=0;

// CLightScribeProgressDialog dialog

IMPLEMENT_DYNAMIC(CLightScribeProgressDialog, CDialog)
CLightScribeProgressDialog::CLightScribeProgressDialog(CBurnControl *owner,CWnd* pParent /*=NULL*/)
: CDialog(CLightScribeProgressDialog::IDD, pParent), 
m_owner(owner),
m_previewPercentage(0),
m_printPercentage(0),
m_printTimeRemainingValue(_T("")),
m_timeRemainingValue(_T(""))
{
	ASSERT(owner!=NULL);
	m_progressMessage=::RegisterWindowMessage(Const::PROGRESS_CALLBACK_MESSAGE);
}

CLightScribeProgressDialog::~CLightScribeProgressDialog()
{
}

void CLightScribeProgressDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TIMEREMAINING, m_timeRemaining);
	DDX_Control(pDX, IDC_PERCENTCOMPLETE, m_percentageDone);
	DDX_Text(pDX, IDC_TIMEREMAINING, m_timeRemainingValue);
	DDX_Control(pDX, IDC_TIMEREMAINING_TITLE, m_TimeRemainingTitleControl);
	DDX_Control(pDX, IDC_PRINT_TITLE, m_printTimeTitle);
	DDX_Control(pDX, IDC_PRINTTIMEREMAINING, m_printTimeRemaining);
	DDX_Text(pDX, IDC_PRINTTIMEREMAINING, m_printTimeRemainingValue);
	DDX_Control(pDX, IDC_PERCENTCOMPLETE2, m_percentageDonePrinting);
}


BEGIN_MESSAGE_MAP(CLightScribeProgressDialog, CDialog)
	ON_WM_CLOSE()
	ON_REGISTERED_MESSAGE(m_progressMessage, OnProgressMessage)
END_MESSAGE_MAP()

/**
* Handle a progress message by forwarding it to the burn controller
* for action. This will usually result in calls back into this object.  
* @return 0, though as the message is 'post'ed the return value is unimportant. 
* @param event What happened - can be cast to a ProgressEvents enum
* @param details Any event specific details
*/
afx_msg LRESULT CLightScribeProgressDialog::OnProgressMessage(WPARAM event,LPARAM details) 
{
	return m_owner->OnProgressMessage(this,event,details);
}

/**
* Set the time remaining. 
* Uses ::FormatMessage to build a localized string, then puts it
* into the appropriate field. 
*/
void CLightScribeProgressDialog::SetTimeRemaining(bool printing,int hours, int minutes, int seconds) 
{
	CString message;
	bool update=false;
	message.Format(IDS_TIMEREMAINING,hours,minutes,seconds);
	if(printing)
	{
		if( message!=m_timeRemainingValue)
		{
			m_printTimeRemainingValue=message;
			update=true;
		}
	}
	else 
	{
		if( message!=m_timeRemainingValue)
		{
			m_timeRemainingValue=message;
			update=true;
		}
	}
	if(update)
	{
		UpdateData(FALSE);
	}
}


/**
* Update the percentage done indicator.
* @param percentage a value 0-100
*/
void CLightScribeProgressDialog::SetPercentageDone(bool printing,int percentage) 
{
	ASSERT(percentage>=0 && percentage<=100);

	if(printing) 
	{
		m_percentageDonePrinting.SetRange(0,100);
		m_percentageDonePrinting.SetPos(percentage);
		m_printPercentage=percentage;
		CString capture;
		capture.Format("LightScribe Print Progress: %d",percentage);
		this->SetWindowText(capture+"%");
	} 
	else
	{
		m_percentageDone.SetRange(0,100);
		m_percentageDone.SetPos(percentage);
		m_previewPercentage=percentage;
	}
}


/**
* Handle an abort request by popping up a dialog box when the
* burn is busy. If the user acceeds to the request, then
* we cancel the burn. 
*/
void CLightScribeProgressDialog::HandleAbortRequest()
{
	if(m_owner->GetState()==printing && ShowAbortDialog())
	{
		m_owner->RequestAbort(true);
	}
}


/**
* Ask the user if they really want to stop printing. 
* @return true if the user pressed OK to the request; false if they cancelled.
*/
bool CLightScribeProgressDialog::ShowAbortDialog()
{
	CString text;
	text.LoadString(IDS_ABORT_BURN);
	CString title;
	title.LoadString(AFX_IDS_APP_TITLE);
	return MessageBox(text,title,MB_YESNO)==IDYES;
}

/**
* Cancel handler delegates to HandleAbortRequest. 
*/
void CLightScribeProgressDialog::OnCancel()
{
	HandleAbortRequest();
}

/**
* Forward init info to our owning CBurnControl instance for its own use. 
*/
BOOL CLightScribeProgressDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_owner->OnWindowCreated(this);
	return true;
}

/**
* Finish with a message - the success flag indicates success or failure. 
*/
bool CLightScribeProgressDialog::EndWithText(bool successful,LPCTSTR finishedText) {
	EndDialog(successful?IDOK:IDCANCEL);
	return IDYES == MessageBox(finishedText,NULL,MB_YESNO | MB_DEFBUTTON2);
}

/**
* Finish with an error dialog, giving the option to update if that
* is appropriate. 
*/
void CLightScribeProgressDialog::EndWithError(HRESULT error, bool genericOK)
{
	_com_error ex(error);
	EndDialog(ComHelper::ShowComExceptionDialog(this, ex, genericOK));
}


/**
* Process power broadcast events. 
* This message is sometimes received as a SendMessage() call, in which case the return value is 
* important. Also, resume messages may be received before the UI is fully functional.
* @param powerEvent Which of the events defined in pbt.h matter
* @param data Event specific data
*/
LRESULT CLightScribeProgressDialog::OnPowerBroadcast(WPARAM powerEvent,LPARAM data)
{

	if(powerEvent==PBT_APMQUERYSUSPEND) {
		//UI iff bit 0 is set
		BOOL interactive=(data & 0x00000001)!=0;
		if(interactive && m_owner->GetState()==printing ) {
			// ask the user if they want to abort
			bool abort=ShowAbortDialog();
			return abort?TRUE:BROADCAST_QUERY_DENY;
		}
	}
	return TRUE;
}


/**
* Callback for a WM_QUERYENDSESSION message.. 
* @param loggingOffOnly Logging off flag; if false a 
*   full exit is in progress. Only useful on Windows XP and later.
* @return true if the request to exit windows is to be allowed. 
*/
bool CLightScribeProgressDialog::OnQueryEndSession(bool /* loggingOffOnly */)
{
	if(m_owner->GetState()==printing )
	{
		bool abort=ShowAbortDialog();
		if(!abort) {
			return false;
		}
	}
	return true;
}

/**
* Callback for a WM_ENDSESSION message.
* @param loggingOffOnly Logging off flag; if false a 
*   full exit is in progress.  Only useful on Windows XP and later.
*/
void CLightScribeProgressDialog::OnEndSession(bool /* loggingOffOnly */)
{

}

/**
* Callback on a PnP device add. 
* (see WM_DEVICECHANGE/DBT_DEVICEARRIVAL)
*/
void CLightScribeProgressDialog:: OnPnPDeviceAdd(DEV_BROADCAST_HDR * )
{
	TRACE0("Device Add detected\n");
}


/**
* Callback on a PnP device remove.
* (see WM_DEVICECHANGE/DBT_DEVICEREMOVECOMPLETE)
*/
void CLightScribeProgressDialog:: OnPnPDeviceRemoval(DEV_BROADCAST_HDR *)
{
	TRACE0("Device removal detected\n");
}
/**
* Called on WinXP fast user switch. 
* At this point the system has probably already switched. 
*/
void CLightScribeProgressDialog:: OnFastUserSwitch(int event)
{
	TRACE1("fast user switching: %02d\n",event);
}