/**
 * @file Constants.h 
 * contains declarations of all constants.
 * Some constants have to be declared as preproprocessor definitions, for easier incorporation
 * into things like MFC message maps or other bits of C++ where C++ const declarations don't
 * work. The remainder will be in the namespace Const.
 * Note that Ansi C++ lets you give constants their value in the header file. 
 */

namespace Const {

/**
* definition of the string we use to register a new windows message;
* notificationWindow windows should listen for this
*/
const LPCTSTR  PROGRESS_CALLBACK_MESSAGE=_T("CProgressCallback::event");
/**
 * what is the apartment policy for the print engine ; COINIT_APARTMENTTHREADED or COINIT_MULTITHREADED 
 */

const COINIT COM_OBJECT_APARTMENT_POLICY=COINIT_MULTITHREADED;



};