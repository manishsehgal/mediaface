#include "stdafx.h"
#include "ImageBurner.h"
#include <pbt.h>

#include "MainFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// This message is only defined for XP and up; we define it even though 
// we aren't going to see it.
#ifndef WM_WTSSESSION_CHANGE
#define WM_WTSSESSION_CHANGE            0x02B1
#endif

// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_COMMAND(ID_LightScribe_DIAGNOSTICS, OnLightScribeDiagnostics)
	ON_MESSAGE(WM_POWERBROADCAST, OnPowerBroadcast)
	ON_MESSAGE(WM_QUERYENDSESSION, OnQueryEndSession)
	ON_MESSAGE(WM_ENDSESSION, OnEndSession)
	ON_MESSAGE(WM_DEVICECHANGE, OnDeviceChange)
	ON_MESSAGE(WM_WTSSESSION_CHANGE , OnSessionChange)
END_MESSAGE_MAP()


// CMainFrame construction/destruction

CMainFrame::CMainFrame():
m_listener(NULL)
{
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
	{
		return -1;
	}

	//we want XP switch detect.
	m_switchDetector.Register(this);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style = WS_MINIMIZE | WS_ICONIC;
	cs.x = cs.y = 5000;
	if( !CFrameWnd::PreCreateWindow(cs) )
	{
		return FALSE;
	}
	return TRUE;
}


// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame message handlers


void CMainFrame::OnLightScribeDiagnostics()
{
}


/**
* Process a WM_POWERBROADCAST power event. 
*/
afx_msg LRESULT CMainFrame::OnPowerBroadcast(WPARAM wPowerEvent,LPARAM lP)
{

	LRESULT retval=TRUE;
	if(m_listener) 
	{
		retval=m_listener->OnPowerBroadcast(wPowerEvent,lP);
	}
	return retval;
}



/**
* Process a WM_QUERYENDSESSION message. 
*/

afx_msg LRESULT CMainFrame::OnQueryEndSession(WPARAM ,LPARAM logOff) 
{
	bool logoff=(logOff&ENDSESSION_LOGOFF)!=0;
	bool canExit=true;
	if(m_listener) 
	{
		canExit=m_listener->OnQueryEndSession(logoff);
	}
	return canExit?TRUE:FALSE;
}

/**
* Process a WM_ENDSESSION message. 
*/

afx_msg LRESULT CMainFrame::OnEndSession(WPARAM sessionEnding,LPARAM logOff) 
{
	bool ending=sessionEnding==TRUE;
	bool logoff=(logOff&ENDSESSION_LOGOFF)!=0;
	if(m_listener && ending) 
	{
		m_listener->OnEndSession(logoff);
	}
	return TRUE;
}


/**
* Override a base class implementation, to make sure we catch WM_QUERYENDSESSION calls. 
* We forward this to our expanded message handler. 
*/
afx_msg BOOL CMainFrame::OnQueryEndSession( )
{
	return (BOOL)OnQueryEndSession(0,FALSE);
}


/**
* Process a WM_DEVICECHANGE. 
*/

afx_msg LRESULT CMainFrame::OnDeviceChange(WPARAM wP,LPARAM lP)
{
	DEV_BROADCAST_HDR *deviceInfo = (DEV_BROADCAST_HDR*)lP;
	switch (wP)
	{

	case DBT_DEVNODES_CHANGED:
		if(m_listener)
		{
			m_listener->OnPnPNodeChange(deviceInfo);
		}
		return TRUE;

	case DBT_DEVICEARRIVAL:
		if(m_listener)
		{
			m_listener->OnPnPDeviceAdd(deviceInfo);
		}
		return TRUE;

	case DBT_DEVICEREMOVECOMPLETE:
		if(m_listener)
		{
			m_listener->OnPnPDeviceRemoval(deviceInfo);
		}
		return TRUE;


	case DBT_DEVICEQUERYREMOVE:
	case DBT_DEVICEQUERYREMOVEFAILED:
	case DBT_DEVICEREMOVEPENDING:
	default:
		return TRUE;
	}
}

/**
* Process a WM_WTSSESSION_CHANGE. 
*/
afx_msg LRESULT CMainFrame::OnSessionChange(WPARAM wP,LPARAM )
{
	int event=(int)wP;
	if(m_listener)
	{
		m_listener->OnFastUserSwitch(event);
	}
	//the return is ignored but mandatory, so we make up a number
	return 0xcafef00d;
}


