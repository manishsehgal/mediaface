/**
* @file 
* the implementation of the burn controller
*/

#include "StdAfx.h"
#include "resource.h"
#include "Constants.h"
#include "BitmapView.h"
#include "App.h"

#include "PrintControl.h"
#include "ProgressCallback.h"
#include "BurnControl.h"
#include "LightScribePrintDialog.h"
#include "LightScribeProgressDialog.h"
#include "ComHelper.h"
#include "WindowHelper.h"

/**
* Construct a burn controller bound to a view
* @param view owner
*/
CBurnControl::CBurnControl(CWnd *parentWindow) 
:m_parentWindow(parentWindow), 
m_printDialog(NULL),
m_bitmap(NULL),
m_callback(NULL),
m_notificationWindow(NULL),
m_state(beginning),
m_worker(NULL),
m_selectedDrivePath(""),
m_bScaleImage(true),
m_labelMode(label_mode_full),
m_quality(quality_normal),
m_printFile(""),
m_bPrompted(false),
m_bPrintToFile(false),
m_selectedDrive(-1), 
m_printingDrive(NULL),
m_isMediaDetectRunning(false),
m_printMore(false),
m_optLevel(media_recognized)
, _ttStartTime(0)
, _ttEndTime(0)
{
	ASSERT(parentWindow!=NULL);
	ASSERT_VALID(parentWindow);
	InvalidateCurrentMedia();
}

/**
* Destructor releases the callback and tells the App we don't want state changes any more. 
*/
CBurnControl::~CBurnControl(void)
{
	if(m_callback!=NULL)
	{
		TRACE0("CBurnControl::~CBurnControl releasing a callback reference\n");
		m_callback->Release();
	}

	CApp::GetApplication().SetSystemStateChangeListener(NULL);
	CleanUp();
}

/**
* Assign the callback.  Won't work if a callback is already set. 
* This assignment increases the reference count of the object by one.
*/
void CBurnControl::SetCallback(CProgressCallback *callback) 
{
	//ASSERT(m_callback==NULL);
	if(m_callback!=NULL)
		m_callback->Release();
	callback->AddRef();
	m_callback=callback;
}

/**
* Get the current state of the burn controller. 
* @return the current state. This can only increase monotonically.
*/
controllerState CBurnControl::GetState()
{
	return m_state;
}

/**
* Set the state. 
*/
void CBurnControl::SetState(controllerState newState) 
{
	m_state=newState;
}

/**
* Request an abort of the current burn. 
* @param userRequested a little debug flag to hint that it was user initiated 
* (as opposed to system shutdown)
*/
void CBurnControl::RequestAbort(bool /* userRequested */) 
{
	ASSERT(m_callback!=NULL);
	m_callback->AbortPrint();
}

/**
* See if the print was aborted.  
* @return true if an abort request was made
*/
bool CBurnControl::IsAborted() 
{
	if(m_callback!=NULL) 
	{
		return m_callback->IsAborted();
	} 
	else
	{
		return false;
	}
}

/**
* Burn the document in our bound view. Brings up dialogs 
* and blocks until the print is finished.
*/
bool CBurnControl::BurnBitmap(CDib& bitmap, CString mode, int lsQuality, bool bSilent)
{
	ASSERT(GetState()==beginning);

	m_bitmap = &bitmap;

	m_parentWindow->BeginWaitCursor();

	// Enumerate the drives
	int drives=0;
	try 
	{
		drives=EnumerateDrives();
	} catch (_com_error &ex)
	{
		// Show a dialog on failure
		ShowComExceptionDialog(ex);
		m_parentWindow->EndWaitCursor();
		return false;
	}

	if(drives==0) 
	{
		TRACE0("No drives found\n");
		AfxMessageBox(IDS_NO_DRIVES,MB_OK);
		m_parentWindow->EndWaitCursor();
		return false;
	}

	m_labelMode = label_mode_full;
	mode.MakeLower();
	if (mode.Find("full") == 0) {
		m_labelMode = label_mode_full;
	}
	else if (mode.Find("title") == 0) {
		m_labelMode = label_mode_title;
	}
	else if (mode.Find("content") == 0) {
		m_labelMode = label_mode_content;
	}

	m_quality = (PrintQuality)lsQuality;

	SetState(print_dialog);
	CLightScribePrintDialog printDialog( *this, m_parentWindow, m_labelMode, m_quality );
	m_printDialog = &printDialog;

	do {
		if(!bSilent) {
			// Bring up the print dialog
			m_printDialog = &printDialog;
			INT_PTR result=printDialog.DoModal();
			m_printDialog = NULL;	// we should not notify inactive dialog
			if(result==IDCANCEL) 
			{
				TRACE0("Cancelled\n");
				SetState(finished);
				m_parentWindow->EndWaitCursor();
				return false;
			}

			// Have the dialog write back the user choices
			printDialog.WriteOwnerInfo();
		}

		// for Silent mode
		if(m_selectedDrive < 0)
			m_selectedDrive = 0;

		// Instantiate the printer we want, then take it exclusively
		ComHelper::CreateDiscPrinter(m_printingDrive, m_selectedDrive);
		try {
			m_printingDrive->AddExclusiveUse();
		}
		catch (_com_error &ex)
		{
			// Couldn't get exclusive use - bail out. 
			m_printingDrive = NULL; // Releases instance too
			ShowComExceptionDialog(ex);
			m_parentWindow->EndWaitCursor();
			return false;
		}

		// Get the media information. If there is no media, ask for it. 
		int returnCode = PromptForMedia(m_printingDrive,bSilent);
		if(returnCode == 0) // failed
		{
			TRACE0("Media verification failed\n");
			m_parentWindow->EndWaitCursor();
			return false;
		}
		else if(returnCode == 2) // proceed with generic media
			m_optLevel = media_generic;

		// Get some print settings from the app
		m_bPrintToFile=CApp::GetApplication().GetPrintToFile();
		m_printFile=CApp::GetApplication().GetPrintFilename();


		// Begin the print by locking down the drive
		m_printingDrive->LockDriveTray();
		SetState(printing);

		{
			CLightScribeProgressDialog progress(this,m_parentWindow);
			// Show the progress dialog - this actually runs the print. 
			INT_PTR result=progress.DoModal();
			CWindowHelper::ActivateWindow(progress.m_hWnd);

			// When we get here the job is either finished, cancelled, or errored out.
			TRACE1("CBurnControl. Burn completed %s\n",
				result==IDOK?"successfully":"unsucessfully");
			SetState(finished);
		}

		// Reset the optimization level. 
		m_optLevel = media_recognized;

		try {
			m_printingDrive->UnlockDriveTray();
			m_printingDrive->OpenDriveTray();
		} catch (_com_error &ex)
		{
			// Show a dialog on failure
			ShowComExceptionDialog(ex);
			// And continue
		}

		m_parentWindow->EndWaitCursor();
		m_printingDrive->ReleaseExclusiveUse();
		m_printingDrive = NULL;  // Implicitly calls ->Release

		bSilent = false;	// we start Lightscribing silently on first pass only

	} while (m_printMore);

	CleanUp();

	return true;
}

/**
* Get the media information. If there is no media, ask for it. 
* @return 0 on error, 1 to proceed, or 2 to proceed with generic media. 
* @param printer The printer to use
* @param bSilent skip first prompt for media
* @param bWaitForValidMedia repeat prompts until valid media recognized
*/
int CBurnControl::PromptForMedia(ILSDiscPrinterPtr& printer, bool bSilent, bool bWaitForValidMedia) 
{
	while(m_isMediaDetectRunning)
		Sleep(100);

	if(m_bPrompted && m_currentMedia.mediaPresent)
	{
		// If the user has been asked already this burn, 
		// don't ask for it again.
		return 1;
	}
	ASSERT(printer != NULL);
	InvalidateCurrentMedia();
	m_isMediaDetectRunning = true;
	if(m_printDialog)
		m_printDialog->PostMessage(ID_UPDATE_MEDIA_INFO);
	// Set to true if we've seen media but it's upside-down
	bool upsideDown = false;
	int passN = 0;

	int returnCode = -1;

	try {
		// This loops until we either find good media or the user cancels
		do 
		{
			if(!bSilent || ( passN > 0 ) ) {
				printer->OpenDriveTray();
			
				// Prompt depends on if the disc needs to be flipped
				CString message;
				if(!upsideDown)
					message.Format(IDS_INSERT_DISC,m_selectedDrivePath);
				else
					message.Format(IDS_FLIP_DISC, m_selectedDrivePath);
				bool cancelled = (IDCANCEL == m_parentWindow->MessageBox(message,0,MB_OKCANCEL));

				time(&_ttStartTime);
			
				if(cancelled)
				{
					TRACE0("User cancelled print in insert disc stage\n");
					printer->CloseDriveTray();
					throw 0;
				}
			
				m_bPrompted=true;
			} else
				time(&_ttStartTime);

			// The user said we have media or we are in silent mode, so get media info. 
			printer->CloseDriveTray();

			CDialog d;
			d.Create(IDD_DETECTING_MEDIA, m_parentWindow);
			d.CenterWindow();
			try {
				if(bWaitForValidMedia) {	// otherwize we do not need "detecting" dialog
					d.ShowWindow(SW_SHOW);
					d.RedrawWindow();
				}
				m_currentMedia.mediaPresent = 0;
				m_currentMedia.mediaOrientedForLabeling = 0;
				m_currentMedia=printer->GetCurrentMedia(media_recognized);
			} catch (_com_error &ex) {
				SetLastError(ex);
				HRESULT err = ex.Error();
				switch(err) {
					case LSCAPI_E_NO_MEDIA:
						m_currentMedia.mediaPresent = 0;
						m_currentMedia.mediaOrientedForLabeling = 0;
						break;
					case LSCAPI_E_MEDIA_NOT_SUPPORTED_BY_DRIVE:
					case LSCAPI_E_MEDIA_ID_READ_ERROR:
					case LSCAPI_E_INVALID_MEDIA_ORIENTATION:
					case LSCAPI_E_NON_LIGHTSCRIBE_MEDIA:
					case LSCAPI_E_UNSUPPORTED_MEDIA:
					case LSCAPI_E_UNSUPPORTED_MEDIA_NO_UPDATE:
					case LSCAPI_E_INCOMPATIBLE_MEDIA:
						m_currentMedia.mediaPresent = 1;
						m_currentMedia.mediaOrientedForLabeling = 0;
						break;
				}

				if(!bWaitForValidMedia) {
					if (ComHelper::WillUpdateFix(err))
						ShowComExceptionDialog(ex, false);
					throw 0;
				}
				d.ShowWindow(SW_HIDE);
				// If the error indicates that its possible to do a generic print, ask the user. 
				if(err == LSCAPI_E_UNSUPPORTED_MEDIA || err == LSCAPI_E_UNSUPPORTED_MEDIA_NO_UPDATE)
				{
					int returnValue = ComHelper::ShowComExceptionDialog(m_parentWindow, ex, true);
					switch(returnValue) {
						case IDGENERIC:
							throw 2;
						case IDCANCEL:
						case IDUPDATE:
						// The error dialog kicks off the update itself, so we'll
						// treat it the same as a cancel. 
							throw 0;
					}
				}
				// Otherwise just show error message and repeat attempt
				if(err != LSCAPI_E_INVALID_MEDIA_ORIENTATION)  
						// for invalid orientation we show separate prompt at next attempt
					ShowComExceptionDialog(ex);
			}
			
			passN++;

			d.ShowWindow(SW_HIDE);
			
			upsideDown = m_currentMedia.mediaPresent && !m_currentMedia.mediaOrientedForLabeling;
		} while(bWaitForValidMedia && (!m_currentMedia.mediaPresent || upsideDown));
	} catch (_com_error &ex)
	{
		//show a dialog on failure
		ShowComExceptionDialog(ex);
		returnCode = 0;
	} catch (int &ex)
	{
		returnCode = ex;
	}

	//ASSERT(!bWaitForValidMedia || m_currentMedia.mediaPresent);
	m_isMediaDetectRunning = false;
	if(m_printDialog)
		m_printDialog->PostMessage(ID_UPDATE_MEDIA_INFO);

	return returnCode >=0 ? returnCode : (m_currentMedia.mediaPresent ? 1 : 0);
}

/**
* A convenience overload which creates a ILSDiscPrinter instance using 
* the currently selected index and then calls PromptForMedia with the 
* newly created instance. 
*/
int CBurnControl::PromptForMedia(bool bSilent, bool bWaitForValidMedia)
{
	ILSDiscPrinterPtr printer;
	try{
	ComHelper::CreateDiscPrinter(printer, m_selectedDrive);
	} catch (_com_error &ex)
	{
		//show a dialog on failure
		ShowComExceptionDialog(ex);
		return 0;
	}
	return PromptForMedia(printer, bSilent, bWaitForValidMedia);
}

/**
* Pop up a dialog to show text about a COM exception.
* LSCAPI errors will be turned into text through message lookup; other
* COM errors get delegated to the OS.  
*/
void CBurnControl::ShowComExceptionDialog(_com_error &ex, bool genericOK)
{
	ComHelper::ShowComExceptionDialog(m_parentWindow,ex,genericOK);
}

/**
* Event handler.
* Called from the child dialog as it is created - this triggers creation of the
* worker thread and labels the disc. 
* @param progressWindow Pointer to progress window
*/
void CBurnControl::OnWindowCreated(CLightScribeProgressDialog *progressWindow)
{
	// Basic re-entrancy catch.
	//ASSERT(m_worker==NULL);
	if(m_worker) {
		DWORD dwExitCode; 
		GetExitCodeThread(m_worker,&dwExitCode);
		ASSERT(dwExitCode != STILL_ACTIVE);
	}
	
	// Now the window is created, we can cache it
	ASSERT(progressWindow!=NULL);
	m_notificationWindow=progressWindow->m_hWnd;

	CApp::GetApplication().SetSystemStateChangeListener(progressWindow);

	// Create the thread
	m_worker=AfxBeginThread(RunWorkerThread,
		reinterpret_cast<LPVOID>(this),
		THREAD_PRIORITY_NORMAL,
		0,	   // Stack size is default
		CREATE_SUSPENDED,
		NULL); //Security is default

	// Start work
	m_worker->ResumeThread();
}


/**
* Static entry point for worker thread
* @param instance Pointer to an instance of this class (CBurnControl)
* @return Thread exit code
*/
UINT AFX_CDECL CBurnControl::RunWorkerThread(LPVOID instance)
{
	ASSERT(instance!=NULL);
	CBurnControl *This=reinterpret_cast<CBurnControl*>(instance);
	return This->BeginWorkerThread();
}

/**
* This method is called in the worker thread we start.
* It starts printing, etc.
* @return Thread exit code
*/
UINT CBurnControl::BeginWorkerThread()
{
	// Create the callback
	TRACE1("beginning worker thread %d\n",AfxGetThread());

	// Start COM
	CoInitializeEx(NULL,Const::COM_OBJECT_APARTMENT_POLICY);

	// Power Management: tell the system that we are busy and intend to stay that way
	::SetThreadExecutionState(ES_SYSTEM_REQUIRED|ES_CONTINUOUS);

	ASSERT(m_notificationWindow!=NULL);

	// Create the progress callback and assign it 
	CProgressCallback* callback = CProgressCallback::CreateCallback(m_notificationWindow);
	SetCallback(callback); // Increments the ref count, 
	callback->Release();   // so we can release our reference. 

	// Now actually start printing.
	LabelDisc();

	CoUninitialize();
	TRACE1("ending worker thread %d\n",AfxGetThread());
	::SetThreadExecutionState(0);
	return 0;
}

/**
* Get the current draw options. 
* @return Draw options ready to use
*/
DrawOptions CBurnControl::GetDrawOptions()
{
	DrawOptions drawopts=draw_default;
	if(m_bScaleImage) 
	{
		drawopts= draw_fit_smallest_to_label;
	}
	return drawopts;
}

/**
* This is the low level disc labeling routine.
* @pre m_callback!=NULL
*/
void CBurnControl::LabelDisc()
{
	HRESULT hr;
	ILSDiscPrintSessionPtr session = NULL;
	DrawOptions drawopts=GetDrawOptions();

	CProgressCallback *callback=m_callback;
	// Make sure the callback stays around during this call
	callback->AddRef();

	// Set up the image parameters
	BYTE *header,*data;
	int headerLength, dataLength;
	CDib &dib=GetLabelBitmap();
	header=(BYTE*)dib.GetBitmapHeader();
	headerLength=dib.GetHeaderLength();
	data=dib.GetImage();
	dataLength=dib.GetImageLength();

	try {
		ASSERT(m_selectedDrive!=-1);
		
		// Open a session
		m_printingDrive->OpenPrintSession(&session);
		ILSDiscPrintSessionTestPtr testAPI = session;

		//set the callback
		session->SetProgressCallback(GetCallback());
		
		if(m_bPrintToFile) 
		{
			TRACE1("Burning to file %s\n",m_printFile);
			testAPI->SetSavePrintFile(_bstr_t(m_printFile));
		} else
		{
			TRACE0("Burning to hardware\n");
		}


		// Now print the disc
		hr=session->PrintDisc(windows_bitmap,
			m_labelMode,
			drawopts,
			m_quality,
			m_optLevel,
			header,headerLength,
			data,dataLength);

		session=NULL; // Calls Release()
	} catch (_com_error &ex) {
		LPCTSTR text=ex.ErrorMessage();
		TRACE(_T("Failed to label disc : %s\n"),text);
		hr=ex.Error();

		// We get away with a potentially re-entrant call here by relying on the fact that the 
		// callback ignores later codes. Why make the re-entrant call? Well, we don't know
		// where things went wrong and we may not have got the message.  
		callback->NotifyPrintComplete(hr);
	}

	// Release this thread's hold on the callback.
	callback->Release();
}

/**
* Handle a progress message.  
* @return Anything. As the message is 'post'ed, the return value is unimportant.
* @param dialog The recipient of the message
* @param event What happened - can be cast to a ProgressEvents enum. 
* @param details Any event specific details. 
*/
LRESULT CBurnControl::OnProgressMessage(CLightScribeProgressDialog *dialog, WPARAM event, LPARAM  details ) 
{
	ProgressEvents eventType=static_cast<ProgressEvents>(event);
	bool updateGUI=true;
	bool finished=false;

	bool printing=false;

	switch(eventType) 
	{
	case PreparePrintProgress:
		printing=false;
		break;

	case PrintProgress:
		printing=true;
		break;

	case PrintComplete:
		{
			CWindowHelper::ActivateWindow(dialog->m_hWnd);
			time(&_ttEndTime);
			printing=true;
			finished=true;
			HRESULT hr=m_callback->GetPrintStatus();
			if(SUCCEEDED(hr))
			{
				CString formatString;
				formatString.LoadString(IDS_PRINT_FINISHED_SUCCESSFUL);
				CString finishedText;
				int nElapsed=_ttEndTime-_ttStartTime;
				int nMinutes=nElapsed/60;
				finishedText.Format(formatString, nMinutes, nElapsed-nMinutes*60);
				finishedText += _T(".\n\nDo you want to print the same label to another disc?");
				m_printMore = dialog->EndWithText(true,finishedText);

				m_printingDrive->UnlockDriveTray();
				m_printingDrive->OpenDriveTray();
				m_currentMedia.mediaPresent = 0;
				m_currentMedia.mediaOrientedForLabeling = 0;

				if(m_printMore) {
					CString message;
					message.Format(IDS_INSERT_DISC,m_selectedDrivePath);
					m_printMore = (IDCANCEL != m_parentWindow->MessageBox(message,0,MB_OKCANCEL));
				}
			} else 
			{
				HRESULT hr = m_callback->GetPrintStatus();
				dialog->EndWithError(hr, true);
			}
		}

	default:
		TRACE2("Unknown event type %d %d ignored\n",event,details);
		updateGUI=false;
		break;
	}

	// Update the GUI if that is enabled
	if(updateGUI) 
	{
		int percentage=m_callback->GetPercentageDone();
		dialog->SetPercentageDone(printing,percentage);
		time_t seconds=m_callback->GetSecondsRemaining();
		if(seconds>-1) 
		{
			int minutes=static_cast<int>(seconds/60);
			int hours=static_cast<int>(minutes/60);
			int seconds_only=static_cast<int>(seconds%60);
			dialog->SetTimeRemaining(printing,hours,minutes%60,seconds_only);
		}
	}

	return 0;
}

/**
* Build the drive array; returns the number of drives. 
* @throws _com_error if something went wrong
*/
ULONG  CBurnControl::EnumerateDrives()
{
	return ComHelper::EnumerateDrives(&m_driveArray);
}

/**
* Clean up all COM references we have to LSCAPI elements.
*/
void CBurnControl::CleanUp()
{
	m_selectedDrive=-1;
	m_printingDrive=NULL;  // Calls ->Release if not already NULL
	// Force an unload of any COM libraries we don't need any more. 
	CoFreeUnusedLibraries();
}

/**
* Select a drive by index value.
* @throws _com_error
* @return true if the index is in range
*/
bool CBurnControl::SelectDrive(ULONG index)
{
	m_selectedDrive=NULL;
	if(index<=m_driveArray.size()) 
	{
		m_selectedDrive = index;
		m_selectedDrivePath = m_driveArray[index].path;
		return true;
	} else 
	{
		TRACE2("Ignoring out-of-range selection of drive #%d of %d\n",
			index,m_driveArray.size());
		m_selectedDrivePath="";
		return false;
	}
}

/**
* Return the index of the currently selected drive. 
* @throws _com_error
* @return -1 if no drive has been selected. 
* @note This is meaningless if pnp has happened. 
*/
ULONG CBurnControl::GetSelectedDrive()
{
	return m_selectedDrive;
}

/**
* Return the bitmap we're going to print. 
* This is the bitmap to be used for labelling and previews. 
*/
CDib &CBurnControl::GetLabelBitmap()
{
	return *m_bitmap;
}

void CBurnControl::CloseDriveTray()
{
	ILSDiscPrinterPtr printer;
	try {
		ComHelper::CreateDiscPrinter(printer, m_selectedDrive);
		printer->CloseDriveTray();
	}
	catch (_com_error &ex) {}
}

void CBurnControl::GetLastErrorMessage(CString *message)
{
	ComHelper::FormatException(message);
}

void CBurnControl::SetLastError(_com_error &ex)
{
	ComHelper::SetLastError(ex);
}
