#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

#include "SystemStateChangeListener.h"
#include "BitmapDoc.h"
#include "BitmapView.h"
#include "MainFrame.h"
#include "App.h"
