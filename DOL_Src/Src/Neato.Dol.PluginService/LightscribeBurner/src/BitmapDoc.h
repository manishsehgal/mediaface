/** @file BitmapDoc.h define the CBitmapDoc class
*/

#pragma once
#include "cdib.h"

/**
* The document of the model-view MFC architecture. This one has a funny loader
* as we load DIB images, not MFC serialized stuff
*/

class CBitmapDoc : public CDocument
{
protected: // create from serialization only
	CBitmapDoc();
	DECLARE_DYNCREATE(CBitmapDoc)

	// Attributes
public:

	// Operations
public:

	// Overrides
public:
	/**
	* clear self for a new document
	*/
	virtual BOOL OnNewDocument();

	/**
	* load or save the bitmap
	*/
	virtual void Serialize(CArchive& ar);

	// Implementation
public:
	virtual ~CBitmapDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	/**
	* public accessor to the bitmap
	* @return reference to the bitmap (which may be empty)
	*/
	CDib &GetBitmap() {
		return m_bitmap;
	}

	/**
	* test for the bitmap having data.
	* @return true iff the bitmap contains an image
	*/

	bool HasImage() {
		return GetBitmap().GetImage()!=NULL;
	}

	/**
	* get the size of the image
	*/
	CSize GetDimensions() {
		return GetBitmap().GetDimensions();
	}

	/**
	* get the depth of the image:1-32 pixels
	*/
	int GetDepth() {
		return GetBitmapInfoHeader()->biBitCount;
	}

	/**
	* get a pointer to the bitmap info header. Will be NULL
	* when no bitmap is loaded
	* @return pointer to the header or NULL
	*/
	LPBITMAPINFOHEADER GetBitmapInfoHeader() {
		return GetBitmap().GetBitmapHeader();
	}

	/**
	* draw the bitmap. 
	* @param pDC device context to draw to (may be a metafile)
	* @param drawRectangle rect to draw into. 
	* @param scale flag to request scaling
	* @return true if we painted anything
	*/

	bool Draw(CDC* pDC,CRect drawRectangle,bool scale);
	

protected:
	DECLARE_MESSAGE_MAP()
	/** the bitmap we are to load and render */
	CDib m_bitmap;
};


