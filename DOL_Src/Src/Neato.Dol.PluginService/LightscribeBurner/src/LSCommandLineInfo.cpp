// LSCommandLineInfo.cpp : implementation file
//

#include "stdafx.h"
#include "LSCommandLineInfo.h"


// CLSCommandLineInfo

CLSCommandLineInfo::CLSCommandLineInfo() : 
	m_lsMode("full"), m_lsQuality("1"), m_silent("0"), 
	m_lsModeFlag(false), m_lsQualityFlag(false), m_SilentFlag(false)
{
}

CLSCommandLineInfo::~CLSCommandLineInfo()
{
}


// CLSCommandLineInfo member functions
void CLSCommandLineInfo::ParseParam( LPCTSTR lpszParam, BOOL bFlag, BOOL bLast )
{
	CCommandLineInfo::ParseParam(lpszParam, bFlag, bLast);

	if (bFlag) {
		CString param(lpszParam);
		m_lsModeFlag = param.Find("lsmode") == 0;
		m_lsQualityFlag = param.Find("lsquality") == 0;
		m_SilentFlag = param.Find("silent") == 0;
        int pos = param.Find("version=");
        if (pos >= 0) {
            m_version = param.Mid(pos+8);
            m_version.Trim();
        }
	} else {
		if (m_lsModeFlag) {
			m_lsMode = lpszParam;
		}
		if (m_lsQualityFlag) {
			m_lsQuality = lpszParam;
		}
		if (m_SilentFlag) {
			m_silent = lpszParam;
		}
		m_lsModeFlag = false;
		m_lsQualityFlag = false;
		m_SilentFlag = false;
	}
}