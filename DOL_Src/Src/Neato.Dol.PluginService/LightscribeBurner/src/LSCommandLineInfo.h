#pragma once



// CLSCommandLineInfo command target

class CLSCommandLineInfo : public CCommandLineInfo
{
public:
	CLSCommandLineInfo();
	virtual ~CLSCommandLineInfo();
	virtual void ParseParam( LPCTSTR lpszParam, BOOL bFlag, BOOL bLast );
	CString m_lsMode;
	CString m_lsQuality;
	CString m_silent;
    CString m_version;
private:
	bool m_lsModeFlag;
	bool m_lsQualityFlag;
	bool m_SilentFlag;
};


