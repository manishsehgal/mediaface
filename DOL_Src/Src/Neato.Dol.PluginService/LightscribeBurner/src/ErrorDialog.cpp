#include "stdafx.h"
#include "ImageBurner.h"
#include ".\errordialog.h"
#include "ComHelper.h"

IMPLEMENT_DYNAMIC(CErrorDialog, CDialog)
CErrorDialog::CErrorDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CErrorDialog::IDD, pParent)
{
	_isCustom = false;
}

CErrorDialog::CErrorDialog(CString errorString, bool showUpdate, bool showGeneric,
						   bool showOK, bool showCancel, CWnd* pParent)
: CDialog(CErrorDialog::IDD, pParent)
{
	_isCustom = true;
	_errorString = errorString;
	
	_showUpdate = showUpdate;
	_showOK = showOK;
	_showCancel = showCancel;
	_showGeneric = showGeneric;
}


CErrorDialog::~CErrorDialog()
{
}

/**
* Show/Hide and layout the controls based on the configuration
*/
void CErrorDialog::UpdateControls()
{
	if(! _isCustom)
		return;
	
	m_ErrorText.SetWindowText(_errorString);
	m_ErrorText.GetDC()->SetTextColor(RGB(255, 0, 0));
	m_ErrorText.GetDC()->UpdateColors();
	
	// Now show the buttons and move them around as required.
	CRect cancelRect;
	m_btnCancel.GetWindowRect(cancelRect);
	CPoint buttonPos = cancelRect.TopLeft();
	ScreenToClient(&buttonPos);
	CSize buttonSize = cancelRect.Size();
	int padding = 15;
	
	if(_showCancel)
	{
		m_btnCancel.MoveWindow(CRect(buttonPos, buttonSize));
		m_btnCancel.ShowWindow(SW_SHOW);
		buttonPos.x -= buttonSize.cx + padding;
	}
	else	
		m_btnCancel.ShowWindow(SW_HIDE);

	if(_showOK)
	{
		m_btnOK.MoveWindow(CRect(buttonPos, buttonSize));
		m_btnOK.ShowWindow(SW_SHOW);
		buttonPos.x -= buttonSize.cx + padding;
	}
	else
		m_btnOK.ShowWindow(SW_HIDE);

	if(_showGeneric)
	{
		m_btnGeneric.MoveWindow(CRect(buttonPos, buttonSize));
		m_btnGeneric.ShowWindow(SW_SHOW);
		buttonPos.x -= buttonSize.cx + padding;
	}
	else
		m_btnGeneric.ShowWindow(SW_HIDE);
    
	if(_showUpdate)
	{
		m_btnUpdate.MoveWindow(CRect(buttonPos, buttonSize));
		m_btnUpdate.ShowWindow(SW_SHOW);
		buttonPos.x -= buttonSize.cx + padding;
	}
	else
		m_btnUpdate.ShowWindow(SW_HIDE);
}

void CErrorDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDUPDATE, m_btnUpdate);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDERROR, m_ErrorText);
	DDX_Control(pDX, IDGENERIC, m_btnGeneric);
	
	// Must be after the control DX so they can be moved around. 
	UpdateControls();
}


BEGIN_MESSAGE_MAP(CErrorDialog, CDialog)
	ON_BN_CLICKED(IDUPDATE, OnBnClickedUpdate)
	ON_BN_CLICKED(IDGENERIC, OnBnClickedGeneric)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


void CErrorDialog::OnBnClickedUpdate()
{
	// Perform an update
	CString updateCommand = ComHelper::GetUpdateCommand();
	SHELLEXECUTEINFO execInfo;
	execInfo.cbSize = sizeof(SHELLEXECUTEINFO);
	execInfo.fMask = NULL;
	execInfo.hwnd = NULL;
	execInfo.lpVerb = "open";
	execInfo.lpFile = "explorer.exe";
	execInfo.lpParameters = updateCommand;
	execInfo.lpDirectory = NULL;
	execInfo.nShow = SW_MAXIMIZE;
	execInfo.hInstApp = NULL;
	
	::ShellExecuteEx(&execInfo);
	
	CDialog::EndDialog(IDUPDATE);
}

void CErrorDialog::OnBnClickedGeneric()
{
	CDialog::EndDialog(IDGENERIC);
}

HBRUSH CErrorDialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
   HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
   if(nCtlColor == CTLCOLOR_STATIC) 
   {
      if(pWnd->GetDlgCtrlID() == IDERROR)
         pDC->SetTextColor(RGB(255, 0, 0));
   }
   return hbr;
}