/**
* @file BurnControl.h 
* declare the burn controller object
*/

#pragma once

#include "ProgressCallback.h"

/**
* Forward declare a dependency.
*/
class CLightScribeProgressDialog;

/**
* This state enumeration is used to record the current state of
* the control object. This lets observers determine the current state,
* and is used to prevent reinstantiation.
*/
enum controllerState
{
	beginning,
	print_dialog,
	printing,
	finished
};

/**
* The burn control class implements the flow of burning a LightScribe label, 
* thus keeping it somewhat isolated from the view. 
*/
class CBurnControl 
{
private:

	/**
	* The current state. All access should go through accessor methods,
	* which may implement synchronization as needed. 
	*/
	volatile controllerState m_state;

	/**
	* Owning view.
	*/
	CWnd *m_parentWindow;

	/**
	* reference to printing dialog; may be NULL if not initialized
	* Is used for notifications only
	*/
	CDialog *m_printDialog;

	CDib* m_bitmap;

	/**
	* Callback object. Will be null when state < printing. 
	* Once assigned, this object lasts until the CBurnControl object is itself destroyed,
	* at which time its Release() method is called.
	*/
	CProgressCallback *m_callback;

	/**
	* Assign the callback.  Won't work if a callback is already set. 
	* This assignment increases the reference count of the object by one.
	*/
	void SetCallback(CProgressCallback *callback);

	/**
	* Get the callback. (may be NULL)
	*/
	CProgressCallback *GetCallback() 
	{
		return m_callback;
	}

	/** 
	* The window to recieve progress messages - this is later set to 
	* the print progress dialog, after it is created. 
	*/
	HWND m_notificationWindow;

	/**
	* The worker thread. Not NULL once printing has started, and 
	* is not autodelete so it can be used to probe the worker's state.
	*/
	CWinThread *m_worker;

	/**
	* in Silent mode Print dialog is skipped and LightScribe is started 
	* without prompt for disc insert
	*/

	bool m_silentMode;

	/**
	* Set the state. 
	*/
	void SetState(controllerState newState);

	/**
	* This is the low level disc labeling routine.
	*/
	void LabelDisc();

	/**
	* Array of enumerated drive info. 
	*/
	DriveArray m_driveArray;

	/**
	* The currently selected drive. -1 for 'no selection'.
	*/
	int m_selectedDrive;

	/**
	* The drive being used for printing.  This is non-null only during printing - 
	* all other drive instances are transient.  It is released when printing is 
	* finished. 
	*/
	ILSDiscPrinterPtr m_printingDrive;

	/**
	* status: true if we are in process of detecting media
	*/
	bool m_isMediaDetectRunning;

	/**
	* Cached information about the current media.
	*/
	MediaInformation m_currentMedia;
	
	/**
	* The drive letter of the selected drive. 
	*/
	CString m_selectedDrivePath;


	/**
	* The selected print quality. 
	*/
	PrintQuality m_quality;

	/**
	* The selected label mode. 
	*/
	LabelMode m_labelMode;

	/**
	* The optimization level to use while printing. 
	*/
	MediaOptimizationLevel m_optLevel;

	/** 
	* Flag to set to true to scale images 
	*/
	bool m_bScaleImage;

	/**
	* Flag to turn print-to-file on or off
	*/
	bool m_bPrintToFile;

	/**
	* Name of the file to print to.
	*/
	CString m_printFile;

	/**
	* Flag set to true if we have prompted the end user
	* for media. 
	*/
	bool m_bPrompted;

	/**
	* Flag set to true if user requested to print duplicate label
	*/
	bool m_printMore;

protected:
	/**
	* This method is called in the worker thread we start.
	* It starts printing, etc.
	* @return Thread exit code
	*/
	UINT BeginWorkerThread();


	/**
	* Static entry point for worker thread
	* @param instance Pointer to an instance of this class (CBurnControl)
	* @return Thread exit code
	*/
	static UINT AFX_CDECL RunWorkerThread(LPVOID instance);


	/**
	* Clean up all COM references we have to LSCAPI elements.
	*/
	void CleanUp();

	/**
	* Pop up a dialog to show text about a COM exception.
	* LSCAPI errors will be turned into text through message lookup; other
	* COM errors get delegated to the OS.  
	*/
	void ShowComExceptionDialog(_com_error &ex, bool genericOK = true);

	/**
	* Open the tray if it is open; close it if not.
	* @return true if the command is successful.
	* @remarks All _com_errors are caught and logged. 
	* This makes it usable in cleanup code in which the caller does not mind
	* that the call failed.  
	*/
	bool OpenOrCloseTray(ILSDiscPrintSessionPtr &session);

	/**
	* Get the current draw options. 
	* @return Draw options ready to use
	*/
	DrawOptions GetDrawOptions();

public:
	/**
	* Construct a burn controller bound to a view
	* @param view owner
	*/
	CBurnControl(CWnd *parentWindow);

	~CBurnControl(void);

	/**
	* Burn the document in our bound view. Brings up dialogs 
	* and blocks until the print is finished.
	*/
	bool BurnBitmap(CDib& bitmap, CString mode = "full", 
					int lsQuality = 1 /* Normal */, bool bSilent = false);

	/**
	* Get the current state of the burn controller. 
	* @return the current state. This can only increase monotonically.
	*/
	controllerState GetState();


	/**
	* Request an abort of the current burn. 
	* @param userRequested a little debug flag to hint that it was user initiated 
	* (as opposed to system shutdown)
	*/
	void RequestAbort(bool userRequested);


	/**
	* See if the print was aborted.  
	* @return true if an abort request was made
	*/
	bool IsAborted();


	/**
	* Event handler.
	* Called from the child dialog as it is created - this triggers creation of the
	* worker thread and labels the disc. 
	* @param progressWindow Pointer to progress window
	*/
	void OnWindowCreated(CLightScribeProgressDialog* progressWindow);


	/**
	* Handle a progress message.  
	* @return Anything. As the message is 'post'ed, the return value is unimportant.
	* @param dialog The recipient of the message
	* @param event What happened - can be cast to a ProgressEvents enum. 
	* @param details Any event specific details. 
	*/
	LRESULT OnProgressMessage(CLightScribeProgressDialog* dialog, WPARAM event, LPARAM details) ;


	/**
	* Get a reference to the drive array.
	*/
	DriveArray& GetDriveArray()
	{
		return m_driveArray;
	}

	/**
	* select a drive by index value.
	* @throws _com_error
	* @return true if the index is in range
	*/
	bool SelectDrive(ULONG index);


	/**
	* Return the index of the currently selected drive. 
	* @throws _com_error
	* @return -1 if no drive has been selected. 
	* @note This is meaningless if pnp has happened. 
	*/
	ULONG GetSelectedDrive();


	/**
	* Get the path to the selected drive.
	*/
	CString GetSelectedDrivePath()
	{
		return m_selectedDrivePath;
	}

	/**
	* Get status of media detection
	*/
	bool IsMediaDetectRunning() {
		return m_isMediaDetectRunning;
	}

	/**
	* Get MediaInformation for current media
	*/
	const MediaInformation& GetCurrentMedia()
	{
		return m_currentMedia;
	}

	void CloseDriveTray();

	/**
	* Get the currently selected label mode.
	* @return The current label mode
	*/
	LabelMode GetLabelMode() 
	{
		return m_labelMode;
	}


	/**
	* Set the current label mode.
	* @param mode New mode to set
	*/
	void SetLabelMode(LabelMode mode)
	{
		m_labelMode=mode;
	}


	/**
	* Select the print quality to use.
	* @param quality Print quality
	*/
	void SetPrintQuality(PrintQuality quality)
	{
		m_quality=quality;
	}


	/**
	* Get the current print quality.
	* @return The current print quality
	*/
	PrintQuality GetPrintQuality() 
	{
		return m_quality;
	}


	/**
	* Enable/disable image scaling.
	* @param scale Flag set to true to scale
	*/
	void SetScaleImage(bool scale)
	{
		m_bScaleImage=scale;
	}


	/**
	* Get the current image scale flag.
	*/
	bool GetScaleImage()
	{
		return m_bScaleImage;
	}


	/**
	* Name the file to print to; this also sets the
	* print to file flag to true. 
	* @param filename NULL or "" mean 'print to real drive'
	*/
	void SetPrintFile(LPCTSTR filename)
	{
		if(filename!=NULL && *filename!=NULL) 
		{
			m_bPrintToFile=true;
			m_printFile=filename;
		}
		else
			m_bPrintToFile=false;
	}


	/**
	* Build the drive array; returns the number of drives. 
	* @throws _com_error if something went wrong
	*/
	ULONG EnumerateDrives();


	/**
	* Get the media information. If there is no media, ask for it. 
	* @return 0 on error, 1 to proceed, or 2 to proceed with generic media. 
	* @param printer The printer to use
	* @param bSilent skip first prompt for media
	* @param bWaitForValidMedia repeat prompts until valid media recognized
	*/
	int PromptForMedia(ILSDiscPrinterPtr& printer, 
						bool bSilent = false, bool bWaitForValidMedia = true); 

	/**
	* A convenience overload which creates a ILSDiscPrinter instance using 
	* the currently selected index and then calls PromptForMedia with the 
	* newly created instance. 
	*/
	int PromptForMedia(bool bSilent = false, bool bWaitForValidMedia = true);

	int GetCurrentMediaInfo() {
		return PromptForMedia(true,false);
	}

	/**
	* Invalidate the cache of the media information.  
	*/
	void InvalidateCurrentMedia() 
	{ ZeroMemory(&m_currentMedia,sizeof(MediaInformation)); }

	///**
	//* get the bitmap we are going to print
	//*/
	//CDib &GetBitmap()
	//{ return m_view->GetDocument()->GetBitmap();}


	/**
	* Return the bitmap we're going to print. 
	* This is the bitmap to be used for labelling and previews. 
	*/
	CDib& GetLabelBitmap();
	time_t _ttStartTime;
	time_t _ttEndTime;

	void GetLastErrorMessage(CString *message);
	void SetLastError(_com_error &ex);
};
