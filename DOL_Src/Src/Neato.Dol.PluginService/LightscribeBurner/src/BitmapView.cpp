/** @file  BitmapView.cpp 
* implementation of the CBitmapView class
*/
#include "stdafx.h"
#include "ImageBurner.h"


#include "BitmapView.h"
#include "PrintControl.h"
#include "BurnControl.h"
#include "ComHelper.h"
#include "shlwapi.h"
#include "BitmapView.h"
#include "LabelPreview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CBitmapView

IMPLEMENT_DYNCREATE(CBitmapView, CView)

BEGIN_MESSAGE_MAP(CBitmapView, CView)
	// Standard printing commands
	ON_WM_ERASEBKGND()
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
	ON_UPDATE_COMMAND_UI(ID_LightScribe_PRINT, OnUpdateLightScribePrint)
	ON_COMMAND(ID_LightScribe_PRINT, OnLightScribePrint)
	ON_COMMAND(ID_LightScribe_DIAGNOSTICS, OnLightScribeDiagnostics)
	ON_UPDATE_COMMAND_UI(ID_LightScribe_PRINTTOFILE, OnUpdateLightScribePrinttofile)
	ON_COMMAND(ID_LightScribe_PRINTTOFILE, OnLightScribePrinttofile)
END_MESSAGE_MAP()

// CBitmapView construction/destruction

CBitmapView::CBitmapView()
{
	m_printApiFound = ComHelper::IsPrintAPIFound();
}

CBitmapView::~CBitmapView()
{
}

BOOL CBitmapView::PreCreateWindow(CREATESTRUCT& cs)
{
	return CView::PreCreateWindow(cs);
}

// CBitmapView drawing

void CBitmapView::OnDraw(CDC* dc)
{
	CBitmapDoc* doc = GetDocument();
	ASSERT_VALID(doc);

	//We paint the bitmap by getting the image from the doc and blitting it. 
	//Actually we ask the document to paint itself, which blurs the MVC arch somewhat, but
	//bitmaps are a special kind of model where the default rendering is built in to the OS.
	//Other views still have the right to choose alternate rendering schemes.

	//get our size
	CRect windowRect;
	GetClientRect(&windowRect);
	//now we want an aspect preserved draw. 
	CRect bmpRect;
	//we draw bitmaps so the smallest dimension sets the width of the square.
	CRect drawRect=windowRect;

	long minimum=windowRect.Height();
	if(windowRect.Width()<minimum)
	{
		minimum=windowRect.Width();
	}
	drawRect.bottom=minimum;
	drawRect.right=minimum;
	//this may leave a gap.

	//draw
	doc->Draw(dc,drawRect,true);

	//now do a circle
	CPen exterior(PS_SOLID,3,RGB(0,0,160));
	CPen *oldPen=dc->SelectObject(&exterior);
	dc->SelectObject(::GetStockObject(NULL_BRUSH));
	dc->Ellipse(drawRect);
	dc->SelectObject(oldPen);
	dc->SelectObject(::GetStockObject(WHITE_BRUSH));
	dc->SelectObject(::GetStockObject(WHITE_PEN));
	if(drawRect.right<windowRect.right)
	{
		CRect leftover=windowRect;
		leftover.left=drawRect.right;
		dc->Rectangle(leftover);
		//no need to deselect stock objects. 
	}
	if(drawRect.bottom<windowRect.bottom)
	{
		CRect leftover=windowRect;
		leftover.top=drawRect.bottom;
		dc->Rectangle(leftover);
		//no need to deselect stock objects. 
	}
}


// CBitmapView printing
BOOL CBitmapView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CBitmapView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// no actions need to take place
}

void CBitmapView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// no actions need to take place
}

// CBitmapView diagnostics
#ifdef _DEBUG
void CBitmapView::AssertValid() const
{
	CView::AssertValid();
}

void CBitmapView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CBitmapDoc* CBitmapView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CBitmapDoc)));
	return (CBitmapDoc*)m_pDocument;
}
#endif //_DEBUG


// CBitmapView message handlers

/**
* Handle a print command enabler: we can print if there is an image
* and the COM library is around
*/
void CBitmapView::OnUpdateLightScribePrint(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(GetDocument()->HasImage() && m_printApiFound);
}

/**
* begin printing
*/
void CBitmapView::OnLightScribePrint()
{
	CBurnControl burnControl(this);
	GetDocument()->GetBitmap().PatchResolution();
	burnControl.BurnBitmap(GetDocument()->GetBitmap());
}

/**
* Diagnostics window
*/
void CBitmapView::OnLightScribeDiagnostics()
{
	CString apipath;
	CString message;
	CString note;
	bool printApiFound=ComHelper::LocatePrintAPI(&apipath);
	bool happy=false;
	if(printApiFound) {
		message.FormatMessage(IDS_PRINT_DLL_LOCATION,(LPCTSTR)apipath);
		if(!PathFileExists(apipath)) 
		{
			message.Append(_T("\r\n"));
			note.LoadString(IDS_PRINT_DLL_MISSING);
			message.Append(note);
		} else
		{
			try {
				ILSDiscPrintMgrPtr printMgr;
				ILSDiscPrintMgrDiagnosticsPtr diagnostics;
				CString xmlstr;
				ComHelper::CreateDiscPrintMgr(printMgr);
				diagnostics=printMgr;
				_bstr_t xml;
				xml=diagnostics->GetDiagnosticsInfo();
				xmlstr=(LPCTSTR)xml;
				happy=diagnostics->AreAllNeededComponentsPresent()==VARIANT_TRUE;
				message.Append(_T("\r\n"));
				message.Append(xmlstr);
			} catch (_com_error ex) {
				message.Append(_T("\r\n"));
				note.LoadString(IDS_COM_ERROR);
				message.Append(note);
				message.Append(_T("\r\n"));
				message.Append(ex.ErrorMessage());
			}
		}
	} else {
		message.LoadString(IDS_PRINT_DLL_UNREGISTERED);
	}
	CString title;
	title.LoadString(IDS_DIAGNOSTICS_TITLE);
	//choose a happiness state
	int type=MB_OK;
	if(!happy)
	{
		type|=MB_ICONERROR;
	}
	//display the message box
	MessageBox(message,title,MB_OK);
}

void CBitmapView::OnUpdateLightScribePrinttofile(CCmdUI *pCmdUI)
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(CApp::GetApplication().GetPrintToFile());
}

void CBitmapView::OnLightScribePrinttofile()
{
	CApp::GetApplication().SetPrintToFile(!CApp::GetApplication().GetPrintToFile());
}

/**
* override background erasure to do no erase when we have 
* a bitmap (the bitmap can do it itself
*/
afx_msg BOOL CBitmapView::OnEraseBkgnd(CDC* pDC)
{
	//do nothing if we have bitmap
	if(GetDocument()->HasImage()) 
	{
		return TRUE;
	} 
	else
	{
		return CView::OnEraseBkgnd(pDC);
	}
}