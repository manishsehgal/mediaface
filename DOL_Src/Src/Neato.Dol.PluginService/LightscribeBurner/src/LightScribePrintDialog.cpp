#include "stdafx.h"
#include "ImageBurner.h"
#include "LightScribePrintDialog.h"
#include "ComHelper.h"
#include "Constants.h"
#include "LabelPreview.h"
#include "lightscribeprintdialog.h"
#include "HelpDialog.h"

IMPLEMENT_DYNAMIC(CLightScribePrintDialog, CDialog)

/**
* A place to preserve user selections between dialog instances. 
*/
ComHelper::PrintOptions CLightScribePrintDialog::static_options;

/**
* Standard constructor. 
* @param owner Owning burn control instance
* @param pParent Parent window
*/
CLightScribePrintDialog::CLightScribePrintDialog(CBurnControl &owner,CWnd* pParent /*=NULL*/, 
												 LabelMode labelMode, PrintQuality printQality)
: CDialog(CLightScribePrintDialog::IDD, pParent)
, m_owner(owner)
, m_driveSelection(CB_ERR)
, m_DriverVersion(_T(""))
{
	ResetDescriptions();
	ReadOwnerInfo();
	// Restore state
	m_options=static_options;
	m_options.m_labelMode = labelMode;
	m_options.m_printQuality = printQality;
	m_options.m_bScaleImage = true;
	
	// Use the dialog box background color for the status text. 
	// This probably doesn't work with themed XP
	m_StatusText.SetBkColor(::GetSysColor(COLOR_3DFACE));
}

CLightScribePrintDialog::~CLightScribePrintDialog()
{
}

/**
* Handle the mapping from print quality to GUI. 
*/
void CLightScribePrintDialog::DDX_PrintQuality(CDataExchange* pDX)
{
	int printQuality=static_cast<int>(m_options.m_printQuality);
	DDX_Radio(pDX,IDC_QUALITY_BEST,printQuality);
	if(pDX->m_bSaveAndValidate)
	{
		m_options.m_printQuality=static_cast<PrintQuality>(printQuality);
	}
}

/**
* Handle the mapping from drives to list box. 
*/
void CLightScribePrintDialog::DDX_DriveSelection(CDataExchange* pDX)
{
	if(pDX->m_bSaveAndValidate) 
	{
		// Move stuff from dialog to internal state
		m_driveSelection=m_driveList.GetCurSel();
	} else
	{
		// Move stuff from internal state to the dialog, 
		// but not to the owning app. 
		m_driveList.ResetContent();

		std::vector<CString>::iterator it;
		for(it=m_driveInfo.begin();it<m_driveInfo.end();it++)
		{
			m_driveList.AddString(*it);
		}
		static bool firstBind = true;
		if (firstBind == true)
		{
			firstBind = false;
			if (m_driveList.GetCount() > 0)
			{
				m_driveSelection = 0;
			}
		}
		if(m_driveSelection>=0) 
		{
			m_driveList.SetCurSel(m_driveSelection);
		}
	}
}

BOOL CLightScribePrintDialog::OnInitDialog( )
{
	CDialog::OnInitDialog();
	// TODO: Add extra initialization here
	OnDrivelistSelectionChanged();
	
	// it is hard to put window from background to foreground now - so some trick:
	//   we emulate attaching to input of real active window
	HWND hCurrWnd = ::GetForegroundWindow();
	int iMyTID = GetCurrentThreadId();
	int iCurrTID = GetWindowThreadProcessId(hCurrWnd,0);

	AttachThreadInput(iMyTID, iCurrTID, TRUE);

	int nRes = SetForegroundWindow();

	AttachThreadInput(iMyTID, iCurrTID, FALSE);

	// notifications from BurnControl do not work for unknown reason -  
	// so just poll m_owner by timer
	SetTimer(1,500,NULL);	
	
	SetActiveWindow();
	return TRUE;   // return TRUE unless you set the focus to a control
}

void CLightScribePrintDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDB_PREVIEW, m_btnPreview);
	DDX_Control(pDX, IDOK, m_btnPrint);

	DDX_Control(pDX, IDC_SCALEIMAGE, m_btnScaleImage);
	DDX_Check(pDX, IDC_SCALEIMAGE, m_options.m_bScaleImage);

	DDX_Control(pDX, IDC_DRIVESTATUS, m_StatusText);
	DDX_Control(pDX, IDC_DRIVELIST, m_driveList);

	DDX_Control(pDX, IDC_QUALITY_BEST,    m_qualityButtonBest);
	DDX_Control(pDX, IDC_QUALITY_NORMAL,  m_qualityButtonNormal);
	DDX_Control(pDX, IDC_QUALITY_FASTEST, m_qualityButtonDraft);

	DDX_Text(pDX, IDC_DRIVE_MANUFACTURER, m_driveManufacturer);
	DDX_Text(pDX, IDC_DRIVE_MODEL, m_driveModel);
	DDX_Text(pDX, IDC_DRIVE_CAPABILITIES, m_driveFeatures);
	DDX_Text(pDX, IDC_MEDIA_DETAILS, m_mediaDetails);
	DDX_Text(pDX, IDC_MEDIA_DETAILS_ERROR, m_mediaDetails);
    DDX_Text(pDX, IDC_PLUGIN_VERSION, CApp::GetApplication().GetPluginVersion());

	DDX_DriveSelection(pDX);
	DDX_PrintQuality(pDX);

	if (m_driveSelection == CB_ERR)
		DisableControls();
	DDX_Text(pDX, IDC_DRIVER_VERSION, m_DriverVersion);
}

BEGIN_MESSAGE_MAP(CLightScribePrintDialog, CDialog)
	ON_BN_CLICKED(IDB_PREVIEW, OnPreview)
	ON_CBN_SELCHANGE(IDC_DRIVELIST, OnDrivelistSelectionChanged)
    ON_BN_CLICKED(IDB_HELP, OnBnClickedHelp)
//	ON_UPDATE_COMMAND_UI(ID_UPDATE_MEDIA_INFO,OnUpdateMediaInfo)
	ON_WM_TIMER()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/**
* When the user hits ok, save the current settings. 
*/
void CLightScribePrintDialog::OnOK()
{
	// Save state
	static_options=m_options;
	// Do whatever else has to be done. 
	CDialog::OnOK();
}

/**
* Print preview
*/
void CLightScribePrintDialog::OnPreview()
{
	UpdateData(TRUE);

	if(!WriteOwnerInfo())
	{
		TRACE0("No drive chosen\n");
		return;
	}

	// Turn off user input for this scope
	CWindowDisabler disabler(this);

	try {
		
 		if (!m_owner.GetCurrentMedia().mediaPresent) {
 			m_owner.PromptForMedia(false, false);
 		}

		CLabelPreview preview(m_owner,m_options,this);
		int result=(int)preview.DoModal();
		switch(result) 
		{
		case IDOK:
			EndDialog(IDOK);
			break;
		case IDCANCEL:
			EndDialog(IDCANCEL);
			break;
		case IDB_CHANGE_OPTIONS:
			// Return to this dialog
			break;
		default:
			break;
		}

	} catch (_com_error &ex)
	{
		ComHelper::ShowComExceptionDialog(this,ex,false);
	}
}

/**
* Read state information from the owning CBurnControl
* @throws _com_error if bad things happen
*/
void CLightScribePrintDialog::ReadOwnerInfo()
{
	DriveArray &drives(m_owner.GetDriveArray());
	size_t size=drives.size();
	m_driveInfo.clear();
	m_driveInfo.reserve(size);
	m_driveSelection=CB_ERR;
	CString selectedPath=m_owner.GetSelectedDrivePath();

	for(DriveArray::iterator it=drives.begin(); it!=drives.end(); ++it)
	{
		CString text;
		text.Append(it->path);
		text.Append(_T(" - "));
		text.Append(it->displayName);
		TRACE1("Adding drive <%s>\n",text);
		m_driveInfo.push_back(text);
		if(it->path==selectedPath)
		{
			m_driveSelection=(int)(it - drives.begin());
		}
	}

	ASSERT(m_driveInfo.size()==size);
}

/**
* Write the updated config to the owner.
* @return true if there was a valid configuration
* @throws _com_error
* 
*/
bool CLightScribePrintDialog::WriteOwnerInfo()
{
	m_owner.SetLabelMode(m_options.m_labelMode);
	m_owner.SetPrintQuality(m_options.m_printQuality);
	m_owner.SetScaleImage(m_options.m_bScaleImage?true:false);
	return BindOwnerToSelectedDrive();
}

/**
* Push the drive selection up to the owning BurnControl. 
* @return true if there was a selection
* @throws _com_error
*/
bool CLightScribePrintDialog::BindOwnerToSelectedDrive()
{
	if(m_driveSelection==-1) 
	{
		return false;
	} else
	{
		m_owner.SelectDrive(m_driveSelection);
		return true;
	}
}


/**
* Used for PnP event handling. 
* It updates the list of drives, but strives to preserve
* the current drive selection. 
*/
void CLightScribePrintDialog::RefreshDriveList()
{
	UpdateData(TRUE);
	m_owner.EnumerateDrives();
	BindOwnerToSelectedDrive();
	ReadOwnerInfo();
	UpdateData(FALSE);
}

/**
* Callback on a PnP device add. 
* (see WM_DEVICECHANGE/DBT_DEVICEARRIVAL)
*/
void CLightScribePrintDialog::OnPnPDeviceAdd(DEV_BROADCAST_HDR*)
{
	TRACE0("Device Add detected\n");
	RefreshDriveList();
}

/**
* Callback on a PnP device remove.
* (see WM_DEVICECHANGE/DBT_DEVICEREMOVECOMPLETE)
*/
void CLightScribePrintDialog::OnPnPDeviceRemoval(DEV_BROADCAST_HDR*)
{
	TRACE0("Device removal detected\n");
	RefreshDriveList();
}

/**
* Callback on a PnP device node change. 
* (see WM_DEVICECHANGE/DBT_DEVNODES_CHANGED)
*/
void CLightScribePrintDialog::OnPnPNodeChange(DEV_BROADCAST_HDR*)
{
	TRACE0("Device node change\n");
	RefreshDriveList();
}

/**
* Called on WinXP fast user switch. 
* At this point the system has probably already switched. 
*/
void CLightScribePrintDialog::OnFastUserSwitch(int event)
{
	TRACE1("fast user switching: %02d\n",event);
}

/**
* Callback whenever the user selects a different drive.
*/
void CLightScribePrintDialog::OnDrivelistSelectionChanged()
{
	UpdateData(TRUE);
	m_owner.SelectDrive(m_driveSelection);

	DriveInfo drive = (m_owner.GetDriveArray())[m_driveSelection];
	if( drive.isValid )
	{
		EnableControls();
		m_StatusText.SetWindowText("OK");
	}
	else
	{
		// If the drive's invalid, don't allow any operations. 
		DisableControls();
		
		// Show the error text, maybe with a chance to update. 
		CString text;
		ComHelper::FormatException(drive.validationError, &text);
		if( ComHelper::WillUpdateFix(drive.validationError.Error()) )
		{
			text.Append("<u><font color=\"blue\"><a href=\"");
			text.Append(ComHelper::GetUpdateCommand());
			text.Append("\">Update now.</a></font></u>");
		}
		TRACE(text);
		m_StatusText.SetWindowText(text);
	}

	ResetDescriptions();
	GetCurrentDriveInfo();
	GetCurrentDriverVersionInfo();
	UpdateData(FALSE);
	Start_GetMediaInfo();
}

/**
* Enable all dialog controls
*/
void CLightScribePrintDialog::EnableControls()
{
	m_qualityButtonBest.EnableWindow();
	m_qualityButtonNormal.EnableWindow();
	m_qualityButtonDraft.EnableWindow();	
	m_btnPreview.EnableWindow();
	m_btnPrint.EnableWindow();	
	m_btnScaleImage.EnableWindow();
}

/**
* Disable all dialog controls except cancel.
*/
void CLightScribePrintDialog::DisableControls()
{
	m_qualityButtonBest.EnableWindow(false);
	m_qualityButtonNormal.EnableWindow(false);
	m_qualityButtonDraft.EnableWindow(false);
	m_btnPreview.EnableWindow(false);
	m_btnPrint.EnableWindow(false);	
	m_btnScaleImage.EnableWindow(false);
}

/**
* run BurnControl::GetCurrentMediaInfo in a separate thread (to avoid extra delays)
*/
void CLightScribePrintDialog::Start_GetMediaInfo() {
	// Create the thread
	CWinThread* m_detecter=AfxBeginThread(RunDetecterThread,
		reinterpret_cast<LPVOID>(this),
		THREAD_PRIORITY_NORMAL,
		0,	   // Stack size is default
		CREATE_SUSPENDED,
		NULL); //Security is default

	// Start work
	m_detecter->ResumeThread();
}


/**
* Static entry point for detecter thread
* @param instance Pointer to an instance of this class (CLightScribePrintDialog)
* @return Thread exit code
*/
UINT AFX_CDECL CLightScribePrintDialog::RunDetecterThread(LPVOID instance)
{
	ASSERT(instance!=NULL);
	CLightScribePrintDialog *This=reinterpret_cast<CLightScribePrintDialog*>(instance);
	//This->GetMediaInfo();
	CoInitializeEx(NULL,Const::COM_OBJECT_APARTMENT_POLICY);
	This->m_owner.GetCurrentMediaInfo();
	CoUninitialize();
	return 0;
}


//////////////////////////////////////////////////////////////////////////////

/**
* display media information alredy prepared in BurnControl
*/
void CLightScribePrintDialog::OnUpdateMediaInfo() {
	m_mediaDetails=_T("");
	m_mediaDetailsError = _T("");
	try 
	{
		int nDrive = m_driveSelection;
		if(nDrive==-1) 
		{
			m_mediaDetailsError=_T("No drive selected");
			throw 1;
		}

		if(m_owner.IsMediaDetectRunning()) {
			m_mediaDetailsError=_T("Detecting media ...");
			throw 2;
		}

		MediaInformation currentMedia = m_owner.GetCurrentMedia();

		if (!currentMedia.mediaPresent && !currentMedia.mediaOrientedForLabeling) {
			//no media
			this->GetDlgItem(IDC_MEDIA_DETAILS_ERROR)->ShowWindow(SW_NORMAL);
			m_owner.GetLastErrorMessage(&m_mediaDetailsError);
			throw 3;
		}

		if(currentMedia.mediaPresent && !currentMedia.mediaOrientedForLabeling)
		{
			//read error or media not properly oriented
			this->GetDlgItem(IDC_MEDIA_DETAILS_ERROR)->ShowWindow(SW_NORMAL);
			m_owner.GetLastErrorMessage(&m_mediaDetailsError);
			throw 3;
		}
		CString message;
		this->GetDlgItem(IDC_MEDIA_DETAILS_ERROR)->ShowWindow(SW_HIDE);

		//get the size
		CString size;
		UINT messageID;
		switch(currentMedia.shapeAndSize) 
		{
		case label_12cm_circle:
			messageID=IDS_LABEL_12CM_DISC;
			break;
		case label_8cm_circle:
			messageID=IDS_LABEL_8CM_DISC;
			break;
		case label_unknown:
		default:
			messageID=IDS_LABEL_UNKNOWN;
			break;
		}
		size.LoadString(messageID);

		message.Format(_T(
			"Disc Type\t\t%s\n"
			"Manufacturer\t\t#%d\n"
			//"Foreground color:\t%06x\n"
			//"Background color:\t%06x\n"
			"Label dimensions:\t%3.3fmm-%3.3fmm"),
			(LPCTSTR)size,
			currentMedia.manufacturer,
			//currentMedia.foreground,
			//currentMedia.background,
			currentMedia.innerRadius/1000.0,
			currentMedia.outerRadius/1000.0);

		m_mediaDetails=message;
	} catch (...)
	{
	}

	CDataExchange DX(this,FALSE);
	DDX_Text(&DX, IDC_MEDIA_DETAILS, m_mediaDetails);
	DDX_Text(&DX, IDC_MEDIA_DETAILS_ERROR, m_mediaDetailsError);

}

void CLightScribePrintDialog::GetCurrentDriverVersionInfo()
{
	CString apipath;
	CString message;
	CString note;
	bool printApiFound=ComHelper::LocatePrintAPI(&apipath);
	if(printApiFound) {
		if(!PathFileExists(apipath)) 
		{
			message.LoadString(IDS_PRINT_DLL_MISSING);
		}
		else
		{
			try {
				ILSDiscPrintMgrPtr printMgr;
				ILSDiscPrintMgrDiagnosticsPtr diagnostics;
				ComHelper::CreateDiscPrintMgr(printMgr);
				diagnostics=printMgr;

				if (diagnostics->AreAllNeededComponentsPresent() == VARIANT_TRUE)
				{
					_bstr_t xml;
					xml=diagnostics->GetDiagnosticsInfo();

					HRESULT hRes;
					CComPtr<MSXML2::IXMLDOMElement> pXmlDoc;
					hRes = CXmlHelpers::LoadXml(xml, &pXmlDoc);
					if (hRes != S_OK) throw _com_error(hRes, NULL, false);

					CComPtr<MSXML2::IXMLDOMNode> pVersionElem;
					hRes = pXmlDoc->selectSingleNode(L"components/component/version", &pVersionElem);
					if (hRes != S_OK) throw _com_error(hRes, NULL, false);

					CComBSTR version;
					pVersionElem->get_text(&version);

					message = version.m_str;
				}
				else
				{
					message.LoadString(IDS_PRINT_DLL_MISSING);
				}
			} catch (_com_error ex) {
				note.LoadString(IDS_COM_ERROR);
				message.Append(note);
				message.Append(_T(" "));
				message.Append(ex.ErrorMessage());
			}
		}
	} else {
		message.LoadString(IDS_PRINT_DLL_UNREGISTERED);
	}

	m_DriverVersion = message;
}


/**
* From the current drive selection, fill in the text.
*/
void CLightScribePrintDialog::GetCurrentDriveInfo()
{
	try {
		int driveIndex = m_driveSelection;
		if(driveIndex==-1) 
		{
			ResetDescriptions();
			return;
		}

		LSCAPILib::ILSDiscPrinterPtr drive;
		ComHelper::CreateDiscPrinter(drive, driveIndex);

		BSTR result = drive->GetPrinterVendorName();
		m_driveManufacturer = result;
		result = drive->GetPrinterProductName();
		m_driveModel = result;

		DeviceCapabilitiesEnum capabilities=drive->GetPrinterCapabilities();
		int messageID;
		CString message;
		messageID=IDS_DRIVE_MONOCHROME;
		if(capabilities & label_color) {
			messageID=IDS_DRIVE_COLOR;
		}
		message.Format(messageID,capabilities);
		m_driveFeatures=message;
	}
	catch (_com_error &ex)
	{
		ComHelper::ShowComExceptionDialog(this, ex, false);
	}
}

/*
* Reset the description strings. 
*/
void CLightScribePrintDialog::ResetDescriptions()
{
	m_driveManufacturer=_T("");
	m_driveModel=_T("");
	m_driveFeatures=_T("");
	m_mediaDetails=_T("");
	m_mediaDetailsError=_T("");
}
void CLightScribePrintDialog::OnBnClickedHelp()
{
    CHelpDialog dlg;
    dlg.DoModal();
}

void CLightScribePrintDialog::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	OnUpdateMediaInfo();
	CDialog::OnTimer(nIDEvent);
}

void CLightScribePrintDialog::OnDestroy() 
{
	KillTimer(1);
	CDialog::OnDestroy();
}

HBRUSH CLightScribePrintDialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
   HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
   if(nCtlColor == CTLCOLOR_STATIC) 
   {
      if(pWnd->GetDlgCtrlID() == IDC_MEDIA_DETAILS_ERROR)
         pDC->SetTextColor(RGB(255, 0, 0));
   }
   return hbr;
}