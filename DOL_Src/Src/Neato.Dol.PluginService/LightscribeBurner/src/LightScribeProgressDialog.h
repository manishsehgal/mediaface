#pragma once

#include "SystemStateChangeListener.h"
#include "BurnControl.h"
#include "afxwin.h"
#include "afxcmn.h"

/**
* Class that implements the print progress dialog.
* bound to IDD_PRINTPROGRESS.
* This class also receives incoming events from the progress callback as posted messages. 
*/
class CLightScribeProgressDialog : public CDialog, public SystemStateChangeListener
{
	DECLARE_DYNAMIC(CLightScribeProgressDialog)

	/** Text to use for the 'time remaining' text */
	CString m_timeRemainingValue;
	CString m_printTimeRemainingValue;

	/** Static text area for time remaining */
	CStatic m_timeRemaining;

	/** Percentage done control for prepare-to-print*/
	CProgressCtrl m_percentageDone;

	/** Percentage done control for printing */
	CProgressCtrl m_percentageDonePrinting;

	
	// Miscellaneous static text - later read from resources
	CStatic m_TimeRemainingTitleControl;
	CStatic m_printTimeTitle;
	CStatic m_printTimeRemaining;

public:
	/**
	* Constructor
	* @param owner The burn control that we are bound to
	* @param pParent Parent window
	*/
	CLightScribeProgressDialog(CBurnControl *owner, CWnd* pParent = NULL);
	virtual ~CLightScribeProgressDialog();

	// Dialog Data
	enum { IDD = IDD_PRINTPROGRESS };

	/**
	* Set the time remaining. 
	* Uses ::FormatMessage to build a localized string, then puts it
	* into the appropriate field. 
	*/
	void SetTimeRemaining(bool printing,int hours, int minutes, int seconds);

	/**
	* Update the percentage done indicator.
	* @param percentage a value 0-100
	*/
	void SetPercentageDone(bool printing,int percentage) ;

	/**
	* Get the current percentage done.
	* @return a value from 0-100
	*/
	inline int GetPercentageDone() 
	{ return m_previewPercentage;}

	/**
	* Handle an abort request by popping up a dialog box when the
	* burn is busy. If the user acceeds to the request, then
	* we cancel the burn. 
	*/
	void HandleAbortRequest();


protected:
	/** Owning controller.  */
	CBurnControl *m_owner;

	virtual void DoDataExchange(CDataExchange* pDX);    

	/** Percentage done cache. */
	int m_previewPercentage;

	/** Percentage done cache. */
	int m_printPercentage;

	DECLARE_MESSAGE_MAP()

	// A custom message for progress callbacks. 
	static UINT m_progressMessage;
public:

	/**
	* Cancel handler delegates to HandleAbortRequest. 
	*/
	afx_msg void OnCancel();

	/**
	* Forward init info to our owning CBurnControl instance for its own use. 
	*/
	BOOL OnInitDialog();

	/**
	* Handle a progress message by forwarding it to the burn controller
	* for action. This will usually result in calls back into this object.  
	* @return 0, though as the message is 'post'ed the return value is unimportant. 
	* @param event What happened - can be cast to a ProgressEvents enum
	* @param details Any event specific details
	*/
	afx_msg LRESULT OnProgressMessage(WPARAM event,LPARAM details);

	/**
	* Finish with a message and Yes/No choise. The success flag indicates success or failure. 
	* @return true if the user pressed Yes to the request, false if No.
	*/
	bool EndWithText(bool successful,LPCTSTR finishedText);

	/**
	* Finish with an error dialog, giving the option to update if that
	* is appropriate. 
	*/
	void EndWithError(HRESULT error, bool genericOK);

	/**
	* Ask the user if they really want to stop printing. 
	* @return true if the user pressed OK to the request; false if they cancelled.
	*/
	bool ShowAbortDialog();

	/**
	* Process power broadcast events. 
	* This message is sometimes received as a SendMessage() call, in which case the return value is 
	* important. Also, resume messages may be received before the UI is fully functional.
	* @param powerEvent Which of the events defined in pbt.h matter
	* @param data Event specific data
	*/
	LRESULT OnPowerBroadcast(WPARAM powerEvent,LPARAM data);

	/**
	* Callback for a WM_QUERYENDSESSION message.. 
	* @param loggingOffOnly Logging off flag; if false a 
	*   full exit is in progress. Only useful on Windows XP and later.
	* @return true if the request to exit windows is to be allowed. 
	*/
	bool OnQueryEndSession(bool loggingOffOnly );

	/**
	* Callback for a WM_ENDSESSION message.
	* @param loggingOffOnly Logging off flag; if false a 
	*   full exit is in progress.  Only useful on Windows XP and later.
	*/
	void OnEndSession(bool loggingOffOnly );

	/**
	* Callback on a PnP device add. 
	* (see WM_DEVICECHANGE/DBT_DEVICEARRIVAL)
	*/
	virtual void OnPnPDeviceAdd(DEV_BROADCAST_HDR* deviceInfo);

	/**
	* Callback on a PnP device remove.
	* (see WM_DEVICECHANGE/DBT_DEVICEREMOVECOMPLETE)
	*/
	virtual void OnPnPDeviceRemoval(DEV_BROADCAST_HDR* deviceInfo);

	/**
	* Called on WinXP fast user switch. 
	* At this point the system has probably already switched. 
	*/
	virtual void OnFastUserSwitch(int event);

};
