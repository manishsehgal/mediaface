#pragma once
#include "options.h"


#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	//!< some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         //!< MFC core and standard components
#include <afxext.h>         //!< MFC extensions
#include <afxdisp.h>        //!< MFC Automation classes
#include <afxmt.h>			//!< MFC multithreading classes
#include <afxdtctl.h>		//!< MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			//!< MFC support for Windows Common Controls

#endif // _AFX_NO_AFXCMN_SUPPORT

//! atl stuff
#include <atlbase.h>	

//! core STL
#include <stdexcept>

#include <msxml.h>
#import <msxml3.dll> raw_interfaces_only
#include "../../Common/XmlHelpers.h"