/** @file cdib.cpp
* adapter from CDIBitmap to MFC
*/
#include "stdafx.h"
#include "cdib.h"
#include "TextException.h"
#include <stdexcept>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

///! serilization code
IMPLEMENT_SERIAL(CDib, CObject, 0);

CDib::CDib()
{
}

/**
* construct a new bitmap
* @param width with of image
* @param height image height
* @param depth image depth in pixeld
* @throws std::exceptions in the case of trouble
*/
CDib::CDib(int width,int height,int depth)
: CDIBitmap(width,height,depth)
{

}



CDib::~CDib()
{

}

CSize CDib::GetDimensions()
{	
	return CSize(GetWidth(), GetHeight());
}



BOOL CDib::Draw(CDC* pDC, CPoint origin, CSize size)
{

	CRect dest(origin,size);
	CRect src=GetImageRect();
	ScaleDIBits(pDC->GetSafeHdc(), &src,&dest);
	return TRUE;
}


/**
* read a dib from an open file
* @param pFile open file
* @exception CException* if something went wrong in the file IO
* @exception CTextException* if the header could not be read (with meaningful english text)
*/
void CDib::Read(CFile* pFile)
{
	try 
	{
		Load(pFile->m_hFile);
	} catch(std::exception &ex)
	{
		TRACE1("Exception %s\n",ex.what());
		CString error=ex.what();
		AfxMessageBox(error,MB_ICONERROR);
		throw new CTextException(error);
	}
}

/**
* serialize to/from an MFC archive
* @param ar archive
* @exception CException* if something went wrong in the file IO
*/

void CDib::Serialize(CArchive& ar)
{
	DWORD dwPos;
	dwPos = (DWORD)ar.GetFile()->GetPosition();
	ar.Flush();
	dwPos = (DWORD) ar.GetFile()->GetPosition();
	if(ar.IsStoring())
	{
		// not writing bitmaps in the sample application
	}
	else 
	{
		Read(ar.GetFile());
	}
}



