/**
 * @file options.h
 * here we define all the various preprocessor options that we want as default
 * this file gets included into stdafx.h, but has been teased out for cleanliness.
 */
#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		///<! Exclude rarely-used stuff from Windows headers
#endif

#ifndef VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef WINVER
/** Win2K and later */
#define WINVER 0x0500		 
#endif

#ifndef _WIN32_WINNT
/** Win2K and later */
#define _WIN32_WINNT 0x0500		
#endif

#ifndef _WIN32_WINDOWS
/** Win9x options are at Win98 */
#define _WIN32_WINDOWS 0x0410 
#endif


