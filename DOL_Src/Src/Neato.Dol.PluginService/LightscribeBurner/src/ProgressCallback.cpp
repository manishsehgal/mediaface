/**
* @file ProgressCallback.cpp
* Implement the progress callbacks
*/
#include "StdAfx.h"
#include "Constants.h"
#include "PrintControl.h"
#include "ProgressCallback.h"

#include <numeric>

/**
* The constructor is private so that the factory is the only way to
* instantiate the objects (thus they are always on the heap, not the stack).
* @param notificationWindow Window to send notifications to. 
*/
CProgressCallback::CProgressCallback(HWND notificationWindow): 
m_refCount(1),
m_notificationWindow(notificationWindow),
m_aborted(0),
m_percentageDone(0),
m_printStarted(0),
m_printComplete(0),
m_estimatedTime(-1),
m_lock()
{
	TRACE1("Creating CProgressCallback instance %p\n",this);
	m_callbackMessage=::RegisterWindowMessage(Const::PROGRESS_CALLBACK_MESSAGE);
}

/**
* The destructor is private: deletion is through the Delete() call when appropriate
*/
CProgressCallback::~CProgressCallback(void)
{
	// Trace a message out so we can verify that the libraries are deleting their
	// references. 
	TRACE1("Deleting CProgressCallback instance %p\n",this);
}



/**
* Thread-safe ref count increment. 
*/
ULONG STDMETHODCALLTYPE CProgressCallback::AddRef() 
{
	return InterlockedIncrement(&m_refCount);
}

/**
* Thread-safe ref count decrement; deletes the object when it is time. 
*/
ULONG STDMETHODCALLTYPE CProgressCallback::Release() 
{

	ULONG count=InterlockedDecrement(&m_refCount);
	if(count==0) 
	{
		delete(this);
	}
	return count;
}


/**
* Break out from all QI calls
* @param riid referenc to the the IID of the interface we want
* @param ppvObject pointer to where we this QI call should put the interface pointer.
* @return return S_OK and set the pointer this when IUnknown is asked for; 
*   E_NOINTERFACE for anything else. 
*/
HRESULT STDMETHODCALLTYPE CProgressCallback::QueryInterface(REFIID iid,void **ppvObject)
{
	if(iid==IID_IUnknown) 
	{
		*ppvObject=(IUnknown*)this;
		return S_OK;
	} else 
	{
		return E_NOINTERFACE;
	}
}

/**
* Send a message to the notificationWindow window. 
* @param event
* @param data
*/
void CProgressCallback::NotifyWindow(ProgressEvents event, LPARAM data) 
{
	//only send if we are still a window
	if(::IsWindow(m_notificationWindow)) 
	{
		PostMessage(m_notificationWindow,m_callbackMessage,static_cast<WPARAM>(event),data);
	}
}

/**
* Factory method. 
* @param notificationWindow Which window 'owns' this callback and wants notification messages. 
* @return Pointer to a new callback, bound to the notificationWindow. 
*/
CProgressCallback *CProgressCallback::CreateCallback(HWND notificationWindow) 
{
	return new CProgressCallback(notificationWindow);
}

/**
* Abort the burn. Thread safe.
*/
void CProgressCallback::AbortPrint() 
{
	CSingleLock(&m_lock,TRUE);
	m_aborted=true;
}

/**
* Test for being aborted.
* @return true if an abort request was made. 
*/
bool CProgressCallback::IsAborted() 
{
	CSingleLock(&m_lock,TRUE);
	return m_aborted;
}



/**
* Reset the clock that catches time remaining info. 
*/
void CProgressCallback::ResetTimeRemaining()
{
	CSingleLock(&m_lock,TRUE);
	time_t now;
	time(&now);
	m_printStarted=now;
}

/**
* Get time remanining in seconds. 
* @return -1 if we dont know
*/
time_t CProgressCallback::GetSecondsRemaining()
{
	time_t now;
	int percentDone;
	static long iteration = 0;
	static bool printing = false;

	iteration++;

	// Anonymous scope for the lock
	{
		CSingleLock(&m_lock,TRUE);
		time(&now);
		percentDone=m_percentageDone;
	}


	time_t elapsed=now-m_printStarted;
	
	float fractionDone = percentDone/100.0f;
	float extrapolatedTotalTime = elapsed/fractionDone;

	float totalTime;
	if(m_estimatedTime != -1 && (m_estimatedTime-elapsed) > 0)
	{
		if(!printing)
		{
			// Detect when the actual printing starts so we can reset the moving average.  
			printing = true;
			m_history.clear();
		}

		if(percentDone <= 0)
		{
			totalTime = m_estimatedTime;
		}
		else 
		{
			// m_estimatedTime now holds what the print engine thinks the total 
			// time to print is, and extrapolatedTotalTime is what we think it 
			// is based on past performace.  We'll use a kind of 'hybrid' of those, 
			// since the estimation is the better of the two at the beginning of
			// the print and the extrapolation is better at the end. 
			totalTime = ((float)m_estimatedTime) * (1.0f - fractionDone) + extrapolatedTotalTime * fractionDone;
		}
	}
	else
	{
		// As a fallback, just use the extrapolation. 
		totalTime = extrapolatedTotalTime;
	}		
	
	time_t remaining = (time_t)totalTime - elapsed;


	/*** Moving average progress calulcation ***/
	// Make room in the history if needed
	if(m_history.size() >= historySize)
		m_history.pop_back();

	m_history.push_front(remaining);

	time_t average = std::accumulate(m_history.begin(), m_history.end(), (time_t)0);

	// Fallback in case something wierd happens
	if(average == 0)
		average = remaining;
	else
		average /= m_history.size();

	return average;
}



/**
* Get percentage complete. 
* @return 0-100
*/
int CProgressCallback::GetPercentageDone()
{
	CSingleLock(&m_lock,TRUE);
	return m_percentageDone;
}

/**
* Update percentage done. 
* @param newValue updated percentage
*/
void CProgressCallback::SetPercentageDone(int newValue)
{
	CSingleLock(&m_lock,TRUE);
	ASSERT(newValue>=0);
	m_percentageDone=newValue;
}

/**
* Update the exit status. 
* @param status Status at end of print
*/
void CProgressCallback::SetPrintStatus(HRESULT status)
{
	CSingleLock(&m_lock,TRUE);
	m_printStatus=status;
}


/**
* Get the exit status. 
* @return the status code
*/
HRESULT CProgressCallback::GetPrintStatus()
{
	CSingleLock(&m_lock,TRUE);
	return m_printStatus;
}

/**
* Called at the end of printing to say 'we have finished'; updates the status
* flag and marks us as complete, provided this has not been done already. 
* @return true if this was the first call to set printing complete, false otherwise
*/
bool CProgressCallback::SetPrintComplete(HRESULT status)
{
	CSingleLock(&m_lock,TRUE);
	if(!m_printComplete) 
	{
		m_printStatus=status;
		m_printComplete=true;
		return true;
	} else
	{
		return false;
	}
}

/**
* Notification of pre-print progress; the conversion of the 
* bitmap into a format that the drive can handle. Cancelling
* the process in this stage will not result in any writing to 
* the media.
* @param current The current point in the progress. 
* @param final The final point point in the progress. 
* @return S_OK if handled. 
*/
HRESULT STDMETHODCALLTYPE 
CProgressCallback::raw_NotifyPreparePrintProgress( 
	int current,int final)
{
	if(!m_printStarted) 
	{
		ResetTimeRemaining();
	}


	long percentage=(long)(current*100.f/final);
	SetPercentageDone(percentage);
	TRACE(_T("Prepare to print at %d%%\n"),percentage);
	NotifyWindow(PreparePrintProgress,percentage);
	return S_OK;
}

/**
* Notification of actual label printing. 
* The two area parameters are not used as absolute values, but ratios. 
* They provide measures of the percentage of the print completed
* accurately enough to be used in a progress indicator. 
* @param areaPrinted  The area of the disc printed
* @param totalArea	  The total area to print
* @return S_OK if handled. 
*/
HRESULT STDMETHODCALLTYPE 
CProgressCallback::raw_NotifyPrintProgress( 
	/* [in] */ int areaPrinted,
	/* [in] */ int totalArea)
{
	if(areaPrinted==0) 
	{
		//reset the counter when we begin printing
		ResetTimeRemaining();
	}

	//the area values can be very large indeed, enough to overflow
	//a long when you multiply them by a hundred...here we use a float
	//to avoid this

	long percentage=(long)(areaPrinted*100.0f/totalArea);
	TRACE(_T("Print at %d%%\n"),percentage);
	SetPercentageDone(percentage);
	NotifyWindow(PrintProgress,percentage);
	return S_OK;
}


/**
* This callback is made when the printing has finished,
* whether sucessfully or not. 
* 
* Errors are documented in the error code list.
* @param status The final status of the print process.
* @return S_OK if processed.
*/
HRESULT STDMETHODCALLTYPE 
CProgressCallback::raw_NotifyPrintComplete(/* [in] */ HRESULT status)
{

	TRACE(_T("Print complete with status %x\n"),status);

	//remember the value & mark us as done
	if(SetPrintComplete(status))
	{
		//forward to the window
		NotifyWindow(PrintComplete,status);
	}
	return S_OK;
}

/**
* A call from the print engine to see if the application/user wishes
* to cancel the print. If the method requests cancelation then the
* print engine will abort the burn and notify the application 
* accordingly.
* @return S_OK if processed.
*/
HRESULT STDMETHODCALLTYPE 
CProgressCallback::raw_QueryCancel( /* [retval][out] */ VARIANT_BOOL *bCancel) 
{
	if(IsAborted()) 
	{
		*bCancel=VARIANT_TRUE;
	} else {
		*bCancel=VARIANT_FALSE;
	}
	return S_OK;
}


HRESULT STDMETHODCALLTYPE 
CProgressCallback::raw_ReportLabelTimeEstimate(
	/*[in]*/ long seconds, /*[out,retval]*/ VARIANT_BOOL *bCancel)

{
	m_estimatedTime = seconds;
	*bCancel=VARIANT_FALSE;
	return S_OK;
}


