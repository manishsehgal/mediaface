#include "StdAfx.h"
#include "Constants.h"
#include "PrintControl.h"
#include "ComHelper.h"
#include "ErrorDialog.h"

namespace ComHelper 
{

	/**
	* The last exception processed. 
	*/
	ProcessedComError _last_reported_exception;

	
	/**
	* Get the last reported exception. 
	* This is uninteresting until something goes wrong.  Until then, the error value is S_OK. 
	*/
	ProcessedComError GetLastReportedComError()
	{
		return _last_reported_exception;
	}

	/**
	* Create a disc print manager instance.
	* @throws _com_error on failure
	*/	
	void CreateDiscPrintMgr(ILSDiscPrintMgrPtr &printMgr)
	{
		HRESULT  hr;
		hr=CoCreateInstance(__uuidof(DiscPrintMgr),
			NULL,CLSCTX_ALL,
			_uuidof(ILSDiscPrintMgr),
			reinterpret_cast<void**>(&printMgr));
		if(FAILED(hr)) 
		{
			throw _com_error(hr);
		}
	}


	/**
	* Enumerate the drives.
	*/
	void CreateEnum(ILSEnumDiscPrintersPtr &printerEnum) 
	{
		ILSDiscPrintMgrPtr mgr;
		CreateDiscPrintMgr(mgr);
		mgr->EnumDiscPrinters(&printerEnum);
	}



	/**
	* Create a discPrinter instance by index. 
	* @param discPrinter Printer to return
	* @param index Which drive to use
	*/
	void CreateDiscPrinter(ILSDiscPrinterPtr &discPrinter,int index) 
	{
		ILSEnumDiscPrintersPtr printerEnum;
		CreateEnum(printerEnum);
		printerEnum->Item(index, &discPrinter);
		discPrinter->Validate();
	}

	/**
	* Create a session instance by index
	* @param session Session to return 
	* @param index Which drive to use
	*/
	void CreateDiscPrintSession(ILSDiscPrintSessionPtr &session, int index) 
	{
		ILSDiscPrinterPtr discPrinter;
		CreateDiscPrinter(discPrinter,index);
		discPrinter->OpenPrintSession(&session);
	}

	/**
	* Ask LSCAPI for the update shell command
	*/
	CString GetUpdateCommand()
	{
		ILSDiscPrintMgrPtr mgr;
		CreateDiscPrintMgr(mgr);
		_bstr_t ret = mgr->GetUpdateShellCommand();
		CString cs;
		cs.Append(ret);
		if (cs.GetLength() == 0)
			cs = _T("http://www.lightscribe.com/go/downloads");
		return cs;
	}


	/**
	* Find the print API DLL. 
	* @param path Optional pointer to a string to be filled in with the path. 
	* @return true if the DLL was found. 
	*/
	bool  LocatePrintAPI(CString *path)
	{
		CRegKey classesRoot(HKEY_CLASSES_ROOT);
		CRegKey clsid;
		clsid.Open(classesRoot,_T("CLSID"), KEY_READ);
		CRegKey lscapi;
		const LPCTSTR keystring=_T("{5197646C-00EA-4307-A067-61319EBBE499}");
		if(ERROR_SUCCESS!=lscapi.Open(clsid,keystring,KEY_READ))
		{
			TRACE(_T("no registry entry for %s\n"),keystring);
			return false;
		}
		// We have the reg entry, now lets find the exe
		CRegKey location;
		if(ERROR_SUCCESS!=location.Open(lscapi,_T("InprocServer32"),KEY_READ))
		{
			TRACE(_T("no key of %s/InprocServer32\n"),keystring);
			return false;
		}
		TCHAR patharray[MAX_PATH];
		ULONG size=sizeof(patharray)/sizeof(TCHAR);
		if(ERROR_SUCCESS!=location.QueryStringValue(NULL,patharray,&size))
		{
			TRACE(_T("no value for %s/InprocServer32\n"),keystring);
			return false;
		}
		if(path)
		{
			*path=patharray;
		}
		return true;
	}


	/**
	* Format an error into a string, using only local or system error codes. 
	* @param exception Error code
	* @param message String to fill in 
	* @param system Flag to set to fill in from the OS, not our module
	* @return true if the message was found and 'message' duly updated.
	*/
	bool FormatException(DWORD exception,CString *message, bool system)
	{
		if(!exception)
		{
			return false;
		}
		DWORD flags;
		LPVOID buffer=NULL;
		flags = FORMAT_MESSAGE_ALLOCATE_BUFFER ;
		flags |= system? FORMAT_MESSAGE_FROM_SYSTEM : FORMAT_MESSAGE_FROM_HMODULE;
		bool formatted=0 != ::FormatMessage(flags,
			NULL,
			exception,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&buffer,
			0,
			NULL);
		if(formatted) 
		{
			*message=(LPCTSTR)buffer;
			::LocalFree(buffer);
		}
		return formatted;
	}


	/**
	* Format an error into a string, using system and local error codes. 
	* @param exception Error code
	* @param message String to fill in 
	* @return true if the message was found and 'message' duly updated.
	*/
	bool FormatException(DWORD exception,CString *message)
	{
		if(!FormatException(exception,message,false))
		{
			return FormatException(exception,message,true);
		}
		return true;
	}

	/**
	* Get an error string from a COM error; look in the local module
	* error strings first.
	* <i>But do not cache the result</i>
	*/
	void FormatExceptionNoCache(_com_error &ex, CString *message)
	{
		if(!ComHelper::FormatException(ex.Error(),message))
		{
			*message=ex.ErrorMessage();
		}
		TRACE(*message);
	}

	/**
	* Get an error string from a COM error; look in the local module
	* error strings first.
	* Also: save the exception so that it can be retrieved. 
	*/
	void FormatException(_com_error &ex, CString *message)
	{
		//build the processed exception
		ProcessedComError error(ex);
		//log the exception.
		_last_reported_exception=error;
		//extract the response string
		*message=error.faultMessage;
	}

	void SetLastError(_com_error& ex)
	{
		//build the processed exception
		ProcessedComError error(ex);
		//log the exception.
		_last_reported_exception=error;
	}

	void FormatException(CString * message)
	{
		*message=_last_reported_exception.faultMessage;
	}

		/**
		* Return true if given an error code that an update may fix.
		*/
	bool WillUpdateFix(HRESULT err)
	{
		switch (err)
		{
			case LSCAPILib::LSCAPI_E_UNSUPPORTED_DRIVE:
			case LSCAPILib::LSCAPI_E_UNSUPPORTED_DRIVE_COMMUNICATIONS:
			case LSCAPILib::LSCAPI_E_CORRUPT_DRF:
			case LSCAPILib::LSCAPI_E_DRF_VERSION_MISMATCH:
			case LSCAPILib::LSCAPI_E_MEDIA_UNSUPPORTED_ID:
			case LSCAPILib::LSCAPI_E_UNSUPPORTED_MEDIA:
				return true;
		}

		return false;
	}


	/**
	* Probe for libraries, COM DLL and below. 
	* @return true if all needed libraries are present
	* @throws _com_error if something went awry with the COM Calls
	*/
	bool AreAllNeededLibrariesPresent() 
	{
		if(!ComHelper::LocatePrintAPI(NULL))
		{
			return false;
		}
		ILSDiscPrintMgrPtr printMgr;
		ILSDiscPrintMgrDiagnosticsPtr diagnostics;
		CreateDiscPrintMgr(printMgr);
		diagnostics=printMgr;
		bool happy=diagnostics!=NULL && diagnostics->AreAllNeededComponentsPresent()==VARIANT_TRUE;
		return happy;
	}


	/**
	* Build a drive array; returns the number of drives. 
	* @param drives: arary of drives; will be cleared at the start. 
	* @throws _com_error if something went wrong. 
	*/
	ULONG  EnumerateDrives(DriveArray *drives)
	{
		ILSEnumDiscPrintersPtr printerEnum;
		CreateEnum(printerEnum);
		ULONG size=printerEnum->Count();
		drives->clear();
		drives->reserve(size);
		for(ULONG i=0;i<size;i++)
		{
			ILSDiscPrinterPtr discPrinter;
			printerEnum->Item(i,&discPrinter);
			
			bool invalid=false;
			_com_error validationError(S_OK);

			try
			{
				discPrinter->Validate();
			}
			catch(_com_error &ex)
			{
				invalid=true;
				validationError = ex;
			}

			DriveInfo info(i);
			_bstr_t temp;
			
			if(! invalid)
			{
				temp  = discPrinter->GetPrinterVendorName();
				info.vendorName.Append(temp);

				temp = discPrinter->GetPrinterProductName();
				info.productName.Append(temp);

				
				temp = discPrinter->GetPrinterPath();
				info.path.Append(temp);

				temp = discPrinter->GetPrinterDisplayName();
				info.displayName.Append(temp);
			}
			else
			{
				info.isValid = false;
				info.validationError = validationError;

				temp = discPrinter->GetPrinterPath();
				info.path.Append(temp);
				info.displayName.Append("Drive cannot be currently used. ");
			}
				
			drives->push_back(info);
		}
		ASSERT(drives->size()==size);
		return size;
	}


	/**
	* Create a print preview file given a bitmap. 
	* Mostly congruent with ILSDiscPrintSession::PrintPreview. 
	* @param sessionIn The session to use
	* @param dib Bitmap to preview
	* @param destFile Filename of the output file
	* @param width Output width (pixels)
	* @param height Output height (pixels)
	* @throws _com_error
	*/
	void PrintPreview(ILSDiscPrintSession *sessionIn, CDIBitmap &dib,LPCTSTR destFile,long width,long height,
		LabelMode labelMode,DrawOptions drawopts,PrintQuality quality, MediaOptimizationLevel optLevel, bool ignoreMedia)
	{
		BYTE *header,*data;
		int headerLength, dataLength;
		header=(BYTE*)dib.GetInfoHeader();
		headerLength=(int)dib.GetHeaderSize();
		data=dib.GetImage();
		dataLength=(int)dib.GetImageSize();
		ILSDiscPrintSessionPtr session;
		if(sessionIn!=NULL)
		{
			session=sessionIn;
		}
		else
		{
			CreateDiscPrintSession(session, 0);
		}

		HRESULT hr;

		Size size;
		size.x=width;
		size.y=height;
		_bstr_t destFileBstr(destFile);
		hr=session->PrintPreview(windows_bitmap,
			labelMode,
			drawopts,
			quality,
			optLevel,
			header,headerLength,
			data,dataLength,
			destFileBstr,
			windows_bitmap,
			&size, 
			ignoreMedia?VARIANT_TRUE:VARIANT_FALSE
			);

	}


	/**
	* Pop up a dialog to show text about a COM exception.
	* LSCAPI errors will be turned into text through message lookup; other
	* COM errors get delegated to the OS.  
	* @param window Owner window (must not be null, caller thread must be its owner)
	* @param ex What went wrong
	* @param genericOK Is it okay to offer to do a generic print
	* @return The return value of DoModal: one of IDOK, IDCANCEL, IDUPDATE, or IDGENERIC
	*/
	int ShowComExceptionDialog(CWnd*, _com_error &ex, bool genericOK)
	{
		CString text;
		ComHelper::FormatException(ex,&text);
		
		bool showUpdate = WillUpdateFix(ex.Error());
		bool showGeneric = false;

		// Some errors allow generic printing
		switch(ex.Error()) {
			case LSCAPI_E_UNSUPPORTED_MEDIA:
			case LSCAPI_E_UNSUPPORTED_MEDIA_NO_UPDATE:
				showGeneric = true;
		}

		
		// Offer to do a generic print only if the caller says it makes sense'
		// and the particular error allows. 
		if(showGeneric && genericOK)
		{
			text.Append("  It is possible to do a generic print at reduced quality.");
			// Dialog with a generic print button, no ok button, and maybe with an update button. 
			CErrorDialog dlg(text, showUpdate, true, false);
			return (int)dlg.DoModal();
		}
		else
		{
			// Show only the "OK" button by default
			CErrorDialog dlg(text, showUpdate, false, true, false);
			return (int)dlg.DoModal();
		}
	}


	/**
	* Create a dib at the appropriate resolution of the target image, then scaleBLT in the image. 
	* @param drive The drive to get resolution information from
	* @param dib The source bitmap
	* @param labelMode Our intended label mode
	* @param drawopts Drawing options
	* @param quality Desired print quality
	*/
	CDib *PreparePrintBitmap(ILSDiscPrinterPtr drive, CDIBitmap &dib,LabelMode labelMode,
		DrawOptions /* drawopts */,PrintQuality quality)
	{
		// Get the outer radius of the label
		long outerRadius=drive->GetLabelRegionOuterRadius(labelMode, media_recognized);

		// The width of the bitmap is then determined from the resolution
		int pixels_per_metre=drive->GetPrintResolution(quality, media_recognized);
		double pixels_per_cm=pixels_per_metre/100;
		double pixels_per_drive=outerRadius*pixels_per_cm/10000;
		ASSERT(pixels_per_drive<=pixels_per_cm*12);
		int bitmap_width=(int)pixels_per_drive;
		int bitmap_height=(int)pixels_per_drive;
		CDib *printDib=new CDib(bitmap_width,bitmap_height,24);
		printDib->GetBitmapHeader()->biYPelsPerMeter=pixels_per_metre;
		printDib->GetBitmapHeader()->biXPelsPerMeter=pixels_per_metre;

		// Render. No fancy scaling algorithms, just boring old ScaleDIBits.
		HDC hDC=printDib->GetDC();
		CRect src,dest;
		printDib->GetSizeRect(&dest);
		dib.GetSizeRect(&src);
		dib.ScaleDIBits(hDC,&src,&dest);	
		printDib->ReleaseDC(hDC);
		return printDib;
	}

	bool IsPrintAPIFound()
	{
		CString apipath;
		bool printApiFound=ComHelper::LocatePrintAPI(&apipath);
		if(printApiFound) {
			TRACE(_T("Print API is registered at %s\n"),(LPCTSTR)apipath);
			if(!PathFileExists(apipath)) 
			{
				TRACE(_T("The DLL is missing"));
				printApiFound=false;
			}
		}
		return printApiFound;
	}
} //end namespace