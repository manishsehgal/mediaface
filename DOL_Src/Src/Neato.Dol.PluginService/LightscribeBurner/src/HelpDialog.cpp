// HelpDialog.cpp : implementation file
//

#include "stdafx.h"
#include "HelpDialog.h"
#include ".\helpdialog.h"


// CHelpDialog dialog

IMPLEMENT_DYNAMIC(CHelpDialog, CDialog)
CHelpDialog::CHelpDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CHelpDialog::IDD, pParent)
{
}

CHelpDialog::~CHelpDialog()
{
}

void CHelpDialog::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CHelpDialog, CDialog)
    ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


BOOL CHelpDialog::OnInitDialog()
{
    AfxEnableControlContainer();
    CDialog::OnInitDialog();
    
    CRect rc;
    CWnd* browserCtrl = GetDlgItem(IDC_STATIC_BROWSER);
    if (!browserCtrl)
        return FALSE;
   
    browserCtrl->GetWindowRect(&rc);
    ScreenToClient(&rc);
    browserCtrl->DestroyWindow();
    
    m_wndBrowser.CreateControl(CLSID_WebBrowser, NULL, WS_VISIBLE|WS_CHILD, rc, this, AFX_IDW_PANE_FIRST);
    IUnknown *pUnk=m_wndBrowser.GetControlUnknown();
	ASSERT(pUnk);
	IWebBrowser *pBrowser;
	HRESULT hr=pUnk->QueryInterface(IID_IWebBrowser, (void **)&pBrowser);
	if (!SUCCEEDED(hr))
	{
		TRACE("WebBrowser interface not supported.\n");
		return FALSE;
	}
    
    TCHAR fn[MAX_PATH+1]={0};
	::GetModuleFileName(0, fn, MAX_PATH);
    TCHAR drive[_MAX_DRIVE];
    TCHAR dir[_MAX_DIR];
    TCHAR fname[_MAX_FNAME];
    TCHAR ext[_MAX_EXT];
    ::_tsplitpath(fn, drive, dir, fname, ext);

	CString url;
    url.Format(_T("%s%s%s"), drive, dir, _T("LightscribeHelp.htm"));
	BSTR bUrl=url.AllocSysString();
	hr=pBrowser->Navigate(bUrl, &COleVariant((long)0, VT_I4),
		&COleVariant((LPCTSTR)NULL, VT_BSTR), NULL, &COleVariant((LPCTSTR)NULL, VT_BSTR));
	if (!SUCCEEDED(hr))
	{
		AfxMessageBox("can't browse!!");
		return FALSE;
	}

    return TRUE;
}
// CHelpDialog message handlers

void CHelpDialog::OnBnClickedOk()
{
    // TODO: Add your control notification handler code here
    OnOK();
}

