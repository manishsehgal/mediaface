#include "stdafx.h"
#include "ImageBurner.h"
#include "LabelPreview.h"
#include "WindowHelper.h"

IMPLEMENT_DYNAMIC(CLabelPreview, CDialog)

CLabelPreview::CLabelPreview(CBurnControl &owner,ComHelper::PrintOptions &options,CWnd* pParent /*=NULL*/)
: CDialog(CLabelPreview::IDD, pParent),
m_options(options),
m_imageFile(""),
m_owner(owner)
{
}

CLabelPreview::~CLabelPreview()
{
	if(m_imageFile.GetLength()>0)
	{
		::DeleteFile(m_imageFile);
	}
}


void CLabelPreview::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PREVIEWPICTURE, m_previewPane);
	DDX_Control(pDX, IDC_PREVIEWTYPE, m_PreviewType);
}


/**
* Execute the preview. 
* @param w Image width
* @param h Image height
*/
void CLabelPreview::DoPreview(int width,int height)
{	
	try {
		// The following code uses a simple failover mechanism for the
		// preview, which should give the user the best possible preview
		// without any interaction.  Note that this is NOT a very efficient
		// way of doing things.  It is intended to show how the different preview
		// mechanisms work.  A more efficient way to do this would be to
		// probe for media once and then act based on the error code it returns. 
		// For example:
		/*
			HRESULT hr = discPrinter->GetCurrentMedia(...);
			if(hr == S_OK)
				DoOptimizedPreview();
			else if(hr <isMemberOf> {LSCAPI_E_MEDIA_UNSUPPORTED_ID, LSCAPI_E_MEDIA_INVALIDPARAMS})
				DoUnoptimizedPreview();
			else
				DoDisklessPreview();
		*/
		// For further information, see the Print Preview section in the 
		// LSCAPI Reference Manual.  
		
		// media ok
		m_owner.CloseDriveTray();
		try {
			if (m_owner.GetCurrentMedia().mediaPresent && m_owner.GetCurrentMedia().mediaOrientedForLabeling) {
				TryPreview(width, height, media_recognized, false);
				m_PreviewType.SetWindowText("Optimized Preview");
				return;
			}
		}
		catch (_com_error &ex) {}
		// media is upside down
		try {
			if (m_owner.GetCurrentMedia().mediaPresent) {
				TryPreview(width, height, media_generic, false);
				m_PreviewType.SetWindowText("Preview unoptimized - update software for best results. ");
				return;
			}
		}
		catch (_com_error &ex) {}
		// no media
		try {
			TryPreview(width, height, media_generic, true);
			m_PreviewType.SetWindowText("Preview not calculated against media");
		}
		catch (_com_error &ex) {
			throw ex;
		}
	}
    catch (_com_error &ex)
	{
		CDialog::EndDialog(ComHelper::ShowComExceptionDialog(this,ex,false));
	}
	catch (std::exception &ex)
	{
		MessageBox(ex.what());
	}
}


/**
* Try to run a preview with the given options - this is used for the fallback mechanism. 
*/
void CLabelPreview::TryPreview(int width, int height, MediaOptimizationLevel optLevel, bool ignoreMedia)
{
	TCHAR path[_MAX_PATH],filename[_MAX_PATH];
	::GetTempPath(_MAX_PATH,path);
	::GetTempFileName(path,_T("Label"),0,filename);
	
	ILSDiscPrintSessionPtr session;
	ComHelper::CreateDiscPrintSession(session, m_owner.GetSelectedDrive());
	
	ComHelper::PrintPreview(session,
		m_owner.GetLabelBitmap(),
		filename,
		width,
		height,
		m_options.m_labelMode,
		m_options.GetDrawOptions(),
		m_options.m_printQuality, optLevel, ignoreMedia);

	session->Close();

	m_imageFile=filename;
	LoadPreviewImage();
	ForceRepaint();
}

/**
* Load the image from m_imageFile. 
*/
void CLabelPreview::LoadPreviewImage()
{
	if(m_imageFile.GetLength()>0)
	{
		m_previewPane.LoadImage(m_imageFile);
	}
}


/**
* Force a repaint. 
*/
void  CLabelPreview::ForceRepaint()
{
	Invalidate();
	UpdateWindow();
}


/**
* At init time, load the preview image. 
*/
BOOL CLabelPreview::OnInitDialog()
{
	CDialog::OnInitDialog();
	UpdateData(FALSE);
	ForceRepaint();
	CRect rect;
	m_previewPane.GetWindowRect(&rect);
	DoPreview(rect.Width(),rect.Height());
	CWindowHelper::ActivateWindow(this->m_hWnd);
	return true;
}

BEGIN_MESSAGE_MAP(CLabelPreview, CDialog)
	ON_BN_CLICKED(IDB_CHANGE_OPTIONS, OnBnClickedChangeOptions)
END_MESSAGE_MAP()


void CLabelPreview::OnBnClickedChangeOptions()
{
	EndDialog(IDB_CHANGE_OPTIONS);
}

