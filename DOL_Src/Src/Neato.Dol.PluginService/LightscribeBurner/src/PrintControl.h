/** @file
* Declare/import the print control API.
*/

#pragma once
#import "LSCAPI.tlb" 
#include "LSCAPI-errorcodes.h"

#include <vector>


/**
* A class to represent the drive information that we display, along
* with its index in LSCAPI. 
*/
class DriveInfo
{
public:
	bool isValid;
	_com_error validationError; // Error from Validate()

	CString path;
	CString vendorName;
	CString productName;
	CString displayName;

	int index;

	DriveInfo(int driveIndex)
		: validationError(S_OK)
	{
		index = driveIndex;
		isValid = true;
	}
};

/**
* Declare a type for printers we want. 
*/
typedef std::vector<DriveInfo> DriveArray;


using namespace LSCAPILib;