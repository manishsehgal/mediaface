
#pragma once

/*
LightScribe Sample Application
Hewlett-Packard Confidential
Copyright 2003-2004 Hewlett-Packard Development Company, LP
*/


/**
* A simple exception with a text field. 
*/
class CTextException: public CException {

	//! Text message 
	CString m_text;

public: 
	//! Create a message 
	CTextException(LPCTSTR text) 
		: m_text(text)
	{
	}

	CTextException(const CTextException &that) 
		: 
	m_text(that.m_text)
	{
	}

	CTextException &copy(const CTextException &that) {
		m_text=that.m_text;
		return *this;
	}

	/** 
	* Override the base class and copy in the text. 
	*/
	virtual BOOL GetErrorMessage(
		LPTSTR lpszError,
		UINT nMaxError,
		PUINT  pnHelpContext = NULL ) const 
	{	
		_tcsncpy(lpszError,m_text,nMaxError);
		pnHelpContext; // get rid of a warning
		return true;
	}

};
