/** @file App.h
*/
#pragma once

#include "MainFrame.h"

/**
* main application of ImageBurner. 
*/

class CApp : public CWinApp
{
	bool m_bPrintToFile;
	UINT m_InitResult;

	CString m_printFilename;
    CString m_version;

public:
	CApp();


	// Overrides
	/**
	* Initialize application. 
	*/
	virtual BOOL InitInstance();
	virtual int ExitInstance( );


	// Implementation
	/**
	* About box handler
	*/
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()

public:

	/**
	* get the main window cast to a CMainFrame pointer
	*/

	CMainFrame *MainWindow() {
		return dynamic_cast<CMainFrame*>(GetMainWnd());
	}

	/**
	* set the listener to events. Only call this from the GUI thread
	* @param listener New listner -can be NULL to end listening.
	*/
	void SetSystemStateChangeListener(SystemStateChangeListener *listener) 
	{
		MainWindow()->SetSystemStateChangeListener(listener);
	}

	bool GetPrintToFile() 
	{ return m_bPrintToFile;}

	void SetPrintToFile(bool b) 
	{ m_bPrintToFile=b;}

	CString& GetPrintFilename()
	{ return m_printFilename;}

	void SetPrintFilename(LPCTSTR filename) 
	{ m_printFilename=filename;}

    CString GetPluginVersion() {
        return m_version;
    }

	/**
	* access the single allowed instance
	*/
	static CApp& GetApplication();
	afx_msg void OnUpdateDebugLastcomerror(CCmdUI* pCmdUI);
	afx_msg void OnDebugLastcomerror();
	bool PlaceStringOnClipboard(CString& text);

private:
	bool PrintDocument(LPCTSTR filename, LPCTSTR mode, int quality, bool bSilent);
	bool DetectDevice();
};

