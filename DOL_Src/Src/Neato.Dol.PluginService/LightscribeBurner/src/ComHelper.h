#pragma once

/**
 * @file
 * COM helper classes
 */
#include "PrintControl.h"
#include "Bitmap.h"
#include "cdib.h"



namespace ComHelper {


bool IsPrintAPIFound();

/**
* Create a disc print manager instance.
* @throws _com_error on failure
*/
void CreateDiscPrintMgr(ILSDiscPrintMgrPtr &printMgr);	

/**
* Enumerate the drives
*/
void CreateEnum(ILSEnumDiscPrintersPtr &e);

/**
* Create a discPrinter instance by index. 
* @param discPrinter Printer to return
* @param index Which drive to use
*/
void CreateDiscPrinter(ILSDiscPrinterPtr &discPrinter, int index);


/**
* Create a session instance by index
* @param session Session to return 
* @param index Which drive to use
*/
void CreateDiscPrintSession(ILSDiscPrintSessionPtr &session, int index);


/**
* Ask LSCAPI for the update shell command
*/
CString GetUpdateCommand();

/**
* Find the print API DLL. 
* @param path Optional pointer to a string to be filled in with the path. 
* @return true if the DLL was found. 
*/
bool LocatePrintAPI(CString *path);

/**
* Format an error into a string, using only local or system error codes. 
* @param exception Error code
* @param message String to fill in 
* @param system Flag to set to fill in from the OS, not our module
* @return true if the message was found and 'message' duly updated.
*/
bool FormatException(DWORD exception,CString* message, bool system);

/**
* Format an error into a string, using system and local error codes. 
* @param exception Error code
* @param message String to fill in 
* @return true if the message was found and 'message' duly updated.
*/
bool FormatException(DWORD exception,CString* message);

/**
* Get an error string from a COM error; look in the local module
* error strings first.
* <i>But do not cache the result</i>
*/
void FormatExceptionNoCache(_com_error& ex, CString* message);

/**
* Get an error string from a COM error; look in the local module
* error strings first.
* Also: save the exception so that it can be retrieved. 
*/
void FormatException(_com_error& ex, CString* message);
void FormatException(CString* message);
void SetLastError(_com_error& ex);
/**
* Return true if given an error code that an update may fix.
*/
bool WillUpdateFix(HRESULT err);

/**
* Probe for libraries, COM DLL and below. 
* @return true if all needed libraries are present
* @throws _com_error if something went awry with the COM Calls
*/
bool AreAllNeededLibrariesPresent();

/**
* Build a drive array; returns the number of drives. 
* @param drives: arary of drives; will be cleared at the start. 
* @throws _com_error if something went wrong. 
*/
ULONG EnumerateDrives(DriveArray* drives);

/**
* Create a print preview file given a bitmap. 
* Mostly congruent with ILSDiscPrintSession::PrintPreview. 
* @param sessionIn The session to use
* @param dib Bitmap to preview
* @param destFile Filename of the output file
* @param width Output width (pixels)
* @param height Output height (pixels)
* @throws _com_error
*/
void PrintPreview(ILSDiscPrintSession* sessionIn, CDIBitmap& dib, LPCTSTR destFile, long width, long height,
				  LabelMode labelMode, DrawOptions drawopts, PrintQuality quality, MediaOptimizationLevel optLevel,
				  bool ignoreMedia);

/**
* Pop up a dialog to show text about a COM exception.
* LSCAPI errors will be turned into text through message lookup; other
* COM errors get delegated to the OS.  
* @param window Owner window (must not be null, caller thread must be its owner)
* @param ex What went wrong
* @param genericOK Is it okay to offer to do a generic print
* @return The return value of DoModal: one of IDOK, IDCANCEL, IDUPDATE, or IDGENERIC
*/
int ShowComExceptionDialog(CWnd *window, _com_error &ex, bool genericOK);

/**
* Create a dib at the appropriate resolution of the target image, then scaleBLT in the image. 
* @param drive The drive to get resolution information from
* @param dib The source bitmap
* @param labelMode Our intended label mode
* @param drawopts Drawing options
* @param quality Desired print quality
*/
CDib* PreparePrintBitmap(ILSDiscPrinterPtr drive, CDIBitmap& dib, LabelMode labelMode, DrawOptions drawopts,
						 PrintQuality quality);

/**
* This is a processed com error: error strings are created from the error code
* and any IErrorInfo pointer.
* By caching errors like this, we avoid keeping IErrorInfo interfaces open. 
*/
class ProcessedComError
{
public:

	/**
	 * empty ctor sets the error code to S_OK
	 */
	ProcessedComError() : hr(S_OK) {}

	/**
	 * Copy constructor
	 */
	ProcessedComError(const ProcessedComError &that) 
	{
		*this=that;
	}

	/**
	 * Construct from a com error
	 */
	ProcessedComError(_com_error &error) 
	{
		hr=error.Error();
		FormatExceptionNoCache(error,&faultMessage);
		CComPtr<IErrorInfo> ierrorinfo=error.ErrorInfo();
		if(ierrorinfo!=NULL) 
		{
			BSTR description;
			ierrorinfo->GetDescription(&description);
			_bstr_t faultinfo;
			faultinfo.Attach(description);
			errorInfo=(LPCTSTR)faultinfo;
		} else
			errorInfo=_T("");
	}

	/**
	 * Assignment operator
	 */
	ProcessedComError& operator=(const ProcessedComError& that)
	{
		hr=that.hr;
		faultMessage=that.faultMessage;
		errorInfo=that.errorInfo;
		return *this;
	}

public:
	/** error code */
	HRESULT hr;

	/** fault message from resource files*/
	CString faultMessage;

	/** IErrorInfo information */
	CString errorInfo;
public:
	
};

/**
* Get the last reported exception. 
* This is uninteresting until something goes wrong.  Until then, the error value is S_OK. 
*/
ProcessedComError GetLastReportedComError();

/**
* This is a convenient way to pass around print options. 
*/
struct PrintOptions
{

	PrintOptions()
		:
		m_bScaleImage(FALSE),
		m_printQuality(quality_normal),
		m_labelMode(label_mode_full)
	{ }

	PrintQuality m_printQuality;
	LabelMode m_labelMode;
	BOOL m_bScaleImage;  // Should the image be scaled

	DrawOptions GetDrawOptions()
	{
		if (m_bScaleImage)
			return draw_fit_smallest_to_label;
		else
			return draw_default;
	}
};

}; // End of ComHelper namespace

