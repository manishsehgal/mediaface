/**
* @file
* bitmap support
*/
#pragma once
/**
This class represents a minimal implementation of bitmap support, written to be 
independent of MFC, yet designed for mixin use in CObject classes.

Core functionality
<ol>
<li>Load/save of BITMAPS v3,v4,v5
<li>Sectioning
<li>Upside down bitmaps
</ol>

Missing: 
<ol>
<li>palette support
<li>GDI+ integration
<li>Resource loading
<li>Thread safety. This class contains no synchronization code
</ol>
*/

class CDIBitmap
{
protected:


	/**
	section handle or NULL when not sectioned
	*/
	HBITMAP _hSectionedDib;

	/**
	width of dib
	*/
	int _width;

	/**
	height of dib (may be negative)
	*/
	int _height;

	/**
	* rounded up width
	*/
	int _trueWidth;

	/**
	absolute height of dib
	*/
	int _trueHeight;

	/**
	bitmap header 
	*/
	BITMAPINFO * _bitmapInfo;

	/**
	size of the header in bytes..includes colour table
	*/

	size_t _headerSize;

	/**
	Pointer into above block at bits *or* sectioned area
	ie. do not assume that bits=info+sizeof(BITMAPINFOHEADER)+colourtable
	*/
	BYTE * _image;


	/**
	size of image in bytes

	*/

	size_t _imageSize;

	/**
	device context if we have one out
	*/
	HDC		_hCachedDC;

	/**
	* cached bitmap for when a DC is selected
	*/

	HBITMAP _hOldBitmap;

	/**
	use count
	*/
	int _dcEntryCount;

	/**
	* how many bits per pixel
	*/
	int _bitsPerPixel;

	/**
	* full colour/paletted flag. Even though this code
	* does not support palettes, this is used to avoid
	* having a constant in everywhere. It is vestigial 
	* palette support, rather than functional
	*/
	int _dibMode;

public:

	/**
	constructor
	*/
	CDIBitmap();

	/**
	* construct a 
	* @param width with of image
	* @param height image height
	* @param depth image depth in pixeld
	* @throws std::exceptions in the case of trouble
	*/
	CDIBitmap(int width,int height,int depth);

	/**
	* destructor frees all memory
	*/
	virtual ~CDIBitmap();


	/**
	* Get the bitmap. 
	* @remarks call GDIFlush() to sync this with the bits
	*/

	inline HBITMAP GetSectionedBitmap()
	{return _hSectionedDib;}

	/**
	* test for the image being valid
	* @return true if there is a bitmap
	*/
	inline bool IsValid() 
	{
		return _hSectionedDib!=NULL;
	}


	/**
	width of dib
	@return width in bytes
	*/

	inline int GetWidth() 
	{return _width;}

	/**
	Get the height of dib -negative for Topdown bitmaps
	@return height in rows
	*/

	inline int GetHeight() 
	{return _height;}

	/**
	Get the true width of dib -this is the width rounded up to the 
	next biggest DWORD.
	@return width of every scan line in bytes
	*/

	inline int GetTrueWidth()
	{
		return _trueWidth;
	}

	/**
	* Get the true height of a bitmap
	* this is always positive
	*/

	inline int GetTrueHeight()
	{
		return _trueHeight;
	}

	/*
	are we inverted/top down dib?
	@return true if top down
	*/

	bool IsTopDown() {return _height<0;}

	/**
	* Get the BITMAPINFO data
	*/
	inline BITMAPINFO *  GetBitmapInfo() 
	{return _bitmapInfo;}

	/**
	* Get at the infoheader of the bitmap
	* This may be V3, V4 or V5: look at the size and cast appropriately
	* @return the info header
	*/
	inline BITMAPINFOHEADER * GetInfoHeader() 
	{return &_bitmapInfo->bmiHeader;}


	/**
	* query the size of the header
	* @return the size of region pointed to by GetBitmapInfo()
	*/
	inline size_t GetHeaderSize() 
	{return _headerSize;}

	/**
	* get at the color indices
	*/
	inline WORD *  GetColorIndices() 
	{return (WORD *)_bitmapInfo->bmiColors;}

	/**
	* Get a pointer to the image
	*/
	inline BYTE * GetImage() 
	{return _image;}

	/**
	* how big is the bitmap?
	* @return size of the image in bytes
	*/
	inline size_t GetImageSize() 
	{
		return _imageSize;
	}
	/**
	* get the HBITMAP instance used for this image.
	* this handle is only valid as long as the current image exists.
	* deleting an object instance, or even loading a new image, will invalidate it
	* @return the current bitmap handle
	*/
	inline HBITMAP GetAsBitmap()
	{
		return _hSectionedDib;
	}

	/**
	* get the HBITMAP instance used for this image.
	* this handle is only valid as long as the current image exists.
	* deleting an object instance, or even loading a new image, will invalidate it
	* @return the current bitmap handle
	*/
	inline operator HBITMAP()
	{
		return _hSectionedDib;
	}

	/**
	return a pointer to a device context for this DIB
	or NULL if that was not possible
	@remarks apps must handle failure , and after successful calls,
	call  ReleaseDC afterwards...
	DO not mix GDI calls and pixel bashing without calling
	GdiFlush() : ReleaseDc does this...
	*/

	HDC GetDC();

	/**
	* tidy up DC and flush GDI calls)
	*/
	void ReleaseDC(HDC hDC);

	/**
	get size as a rectangle
	*/
	void GetSizeRect(LPRECT lpRect);

	/**
	Draw the DIB to a given DC.
	*/
	void ScaleDIBits(HDC hDC, int x, int y);

	/**
	Draw the DIB to a given DC.
	not that this usually calls SetDiBits on an intentity strech anyway.
	*/
	void ScaleDIBits(HDC hDC, LPRECT srcRect,LPRECT destRect);

	/**
	Set the DIB to a given DC, setting the dest rect as the same size
	as the source
	*/
	void SetDIBits(HDC hDC, int x, int y);

	/**
	Set the DIB to a given DC, clipping as required
	revisit: what about "topdown dibs?"
	*/
	void SetDIBits(HDC hDC, LPRECT srcRect,LPRECT destRect);


	/**
	* Load the bitmap from a file
	* @param file name of the file to load
	* @throws std::exception derivatives when things go wrong
	*/
	void Load(LPCTSTR file);

	/**
	* Load the bitmap from a file
	* @param hFile handle to opened file
	* @throws std::exception derivatives when things go wrong
	*/
	void CDIBitmap::Load(HANDLE hFile);

	/**
	* free all allocated memory; reset self
	*/
	void  Empty();

	/**
	* invert a bitmap so that it becomes topdown or upside down,
	* depending upon its prior state
	*/

	void Invert();

	/**
	* if a bitmap has no resolution, give it 72 dpi
	*/
	void PatchResolution();

	/** 
	* Get the address of the start of a scan line. 
	* Works for any direction of bitmap
	* @param line the line to get
	* @return pointer to the scan line the specified co-ordinate
	* @return NULL if beyond the end of the array
	*/

	BYTE   *ScanLine(int line);

protected:

	/**
	* reset internal state
	*/
	void Reset();


	/**
	Create the bitmap section -this is the area
	that will contain the image
	The image size comes from the bitmap info
	header which must be set up.
	This call initializes _image with the memory to load a bitmap,
	and should only be called once during the life of an image
	*/
	void CreateDibSection();

private:
	/**
	* provide no implementation so that there is no
	* way anyone can accidentally copy us
	*/
	CDIBitmap& operator=(CDIBitmap &that);

};
