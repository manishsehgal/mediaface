/** @file  BitmapView.h definition of the CBitmapView class
*/

#pragma once

#include "BitmapDoc.h"

/**
* the view class
*/

class CBitmapView : public CView
{
protected: // create from serialization only
	CBitmapView();
	DECLARE_DYNCREATE(CBitmapView)


	/**
	* flag set to true if the Com object was locatable
	*/
	bool m_printApiFound;


public:
	/**
	* get the current document. May be null in inconceivable circumstances
	*/
	CBitmapDoc* GetDocument() const;

	// Operations
public:

	// Overrides
public:
	/**
	* paint routine
	*/
	virtual void OnDraw(CDC* pDC);  
	/**
	* preconstruction events
	*/
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	/** pre-print method */ 
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	/** printing begins */
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	/** printing is over */
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

	// Implementation
public:
	virtual ~CBitmapView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

	// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	/**
	* GUI enable/disable probe
	*/
	afx_msg void OnUpdateLightScribePrint(CCmdUI *pCmdUI);

	/**
	* handle print menu/button press
	*/
	afx_msg void OnLightScribePrint();
	afx_msg void OnLightScribeDiagnostics();
	afx_msg void OnUpdateLightScribePrinttofile(CCmdUI *pCmdUI);
	afx_msg void OnLightScribePrinttofile();

	/**
	* override background erasure to do no erase when we have 
	* a bitmap (the bitmap can do it itself
	*/
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

};

#ifndef _DEBUG  // debug version in BitmapView.cpp
inline CBitmapDoc* CBitmapView::GetDocument() const
{ return reinterpret_cast<CBitmapDoc*>(m_pDocument); }
#endif

