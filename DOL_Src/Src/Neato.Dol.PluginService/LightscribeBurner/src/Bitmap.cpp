/**
* @file
* bitmap support
*/
#include "stdafx.h"
#include "Bitmap.h"
#include <stdexcept>

/**
* the magic number of a bitmap
*/
const WORD BITMAP_HEADER= 0x4d42;

CDIBitmap::CDIBitmap()
{
	Reset();
}


/**
* construct a 
* @param width with of image
* @param height image height
* @param depth image depth in pixeld
* @throws std::exceptions in the case of trouble
*/

CDIBitmap::CDIBitmap(int width ,int height ,int  depth )
{
	Reset();
	_headerSize=sizeof(BITMAPINFO);
	_bitmapInfo = (LPBITMAPINFO) new BYTE[_headerSize];
	BITMAPINFOHEADER *header=GetInfoHeader();
	ZeroMemory(_bitmapInfo,sizeof(_headerSize));
	header->biSize=sizeof(BITMAPINFOHEADER);
	header->biPlanes = 1;
	header->biCompression= BI_RGB;
	_bitsPerPixel = header->biBitCount = (WORD)depth;
	header->biSizeImage=0;
	header->biWidth=width;
	header->biHeight=height;
	_width=(int)header->biWidth;
	_height=(int)GetInfoHeader()->biHeight;
	_trueWidth= (int)((_width*_bitsPerPixel+31L) / 32) * 4;
	_trueHeight=_height>=0?_height:(-_height);
	_imageSize=_trueWidth*_trueHeight;
	header->biClrUsed=0;
	header->biClrImportant=0;
	header->biYPelsPerMeter=header->biXPelsPerMeter=0;
	_dibMode=DIB_RGB_COLORS;
	CreateDibSection();
}


/**
* destructor frees all memory
*/
CDIBitmap::~CDIBitmap()
{
	Empty();
}

/**
* reset internal state
*/
void  CDIBitmap::Reset()
{
	_imageSize=0;
	_hSectionedDib=NULL;
	_width=_height=0;
	_trueWidth=0;
	_trueHeight=0;
	_bitmapInfo=NULL;
	_headerSize=0;
	_image=NULL;
	_hCachedDC=NULL;
	_dcEntryCount=0;

}

/**
* free all allocated memory; reset self
*/
void  CDIBitmap::Empty()
{
	if(_hSectionedDib)
	{
		::DeleteObject(_hSectionedDib);
	} 
	delete [] (BYTE*)_bitmapInfo;
	Reset();
}


/**
@return a pointer to a device context for this DIB
or NULL if that was not possible
@remarks apps must handle failure , and after successful calls,
call  ReleaseDC afterwards...
@remarks DO not mix GDI calls and pixel bashing without calling
GdiFlush() : ReleaseDc does this...
*/

HDC CDIBitmap::GetDC()
{
	//turn into a sectioned dib
	if(_hCachedDC)
	{
		_dcEntryCount++;
		return _hCachedDC;
	}

	//get a display DC
	HDC hDisplayDC,hDC;
	hDisplayDC=CreateDC(_T("DISPLAY"),NULL,NULL,NULL);
	if(!hDisplayDC)
	{
		return NULL;
	}

	//and a memory DC with the same driver
	hDC=CreateCompatibleDC(hDisplayDC);

	//tidy up display DC
	DeleteDC(hDisplayDC);
	if(!hDC)
	{
		return NULL;
	}

	//now select in the dib
	_hOldBitmap=static_cast<HBITMAP>(::SelectObject(hDC,_hSectionedDib));
	_hCachedDC=hDC;
	_dcEntryCount++;
	return hDC;
}

/**
//tidy up DC and flush GDI calls)
*/

void CDIBitmap::ReleaseDC(HDC hDC)
{
	if(!hDC)
	{
		hDC=_hCachedDC;
	}
	ASSERT(hDC==_hCachedDC);
	if(hDC)
	{
		GdiFlush();
		_dcEntryCount--;
		if(_dcEntryCount<=0)
		{
			::SelectObject(hDC,_hOldBitmap);
			DeleteDC(hDC);
			_hCachedDC=NULL;
			_hOldBitmap=NULL;
		}
	}
}


/**
get size as a rectangle
*/
void CDIBitmap::GetSizeRect(LPRECT lpRect)
{
	lpRect->top=lpRect->left=0;
	lpRect->bottom=_height;
	lpRect->right=_width;
}

/**
Draw the DIB to a given DC.
*/
void CDIBitmap::ScaleDIBits(HDC hDC, int x, int y)
{
	//src is dib
	RECT src;
	src.left=0;
	src.right=GetWidth();
	if(IsTopDown())
	{
		src.top=GetHeight();
		src.bottom=0;
	}
	else
	{
		src.top=0;
		src.bottom=GetHeight();
	}

	//dest is the same offset by x&y
	//and using the unsigned height value
	RECT dest;
	dest.left=x;
	dest.top=y;
	dest.right=x+GetWidth();
	dest.bottom=y+GetHeight();

	ScaleDIBits(hDC,&src,&dest);
}

/**
Draw the DIB to a given DC.
not that this usually calls SetDiBits on an intentity strech anyway.
*/
void CDIBitmap::ScaleDIBits(HDC hDC, LPRECT srcRect,LPRECT destRect)
{
	//unlike setDiBits we can avoid doing all the faffing with
	//sizing our image
	// stupid device drivers dont handle very well the situaton where
	// destrect >> origin rect, so we fix this here
	int destW,destH,srcW,srcH;

	//width
	destW=destRect->right-destRect->left;
	srcW=srcRect->right-srcRect->left;
	destH=destRect->bottom-destRect->top;
	srcH=srcRect->bottom-srcRect->top;
	::SetStretchBltMode(hDC,HALFTONE);
	HDC dcMem = ::CreateCompatibleDC(hDC);
	HBITMAP hbmOld = (HBITMAP) 	::SelectObject(dcMem,
		_hSectionedDib);
	//palette stuff here?
	::StretchBlt(hDC,
		destRect->left,
		destRect->top,
		destW,
		destH,
		dcMem,
		srcRect->left,
		srcRect->top,
		srcW,
		srcH,
		SRCCOPY);
	::SelectObject(dcMem, hbmOld);
	::DeleteDC(dcMem);
}

/**
Set the DIB to a given DC, setting the dest rect as the same size
as the source
*/
void CDIBitmap::SetDIBits(HDC hDC, int x, int y)
{
	//src is dib
	RECT src;
	src.left=0;
	src.right=GetWidth();
	if(IsTopDown())
	{
		src.top=GetHeight();
		src.bottom=0;
	}
	else
	{
		src.top=0;
		src.bottom=GetHeight();
	}
	//dest is the same offset by x&y
	//and using the unsigned height value
	RECT dest;
	dest.left=x;
	dest.top=y;
	dest.right=x+GetWidth();
	dest.bottom=y+GetTrueHeight();
	//make the call
	SetDIBits(hDC,&src,&dest);

}

/**
Set the DIB to a given DC, clipping as required
revisit: what about "topdown dibs?"
*/
void CDIBitmap::SetDIBits(HDC hDC, LPRECT srcRect,LPRECT destRect)
{
	// stupid device drivers dont handle very well the situaton where
	// destrect >> origin rect, so we fix this here
	int destW,destH,srcW,srcH,drawW,drawH;

	//width
	destW=destRect->right-destRect->left;
	srcW=srcRect->right-srcRect->left;
	destH=destRect->bottom-destRect->top;
	if(IsTopDown())
	{
		srcH=srcRect->top-srcRect->bottom;
	}
	else
	{
		srcH=srcRect->bottom-srcRect->top;
	}

	drawW=min(srcW,destW);
	drawH=min(destH,srcH);
	HDC dcMem = ::CreateCompatibleDC(hDC);
	HBITMAP hbmOld = (HBITMAP) ::SelectObject(dcMem,_hSectionedDib);
	::BitBlt(hDC,
		destRect->left,      // Destination x
		destRect->top,      // Destination y
		destW,  // Destination width
		destH, // Destination height
		dcMem,
		srcRect->left,
		srcRect->top,          // starting scan line
		SRCCOPY);
	::SelectObject(dcMem, hbmOld);
	::DeleteDC(dcMem);
}

/**
Create the bitmap section -this is the area
that will contain the image
The image size comes from the bitmap info
header which must be set up.
This call initializes _image with the memory to load a bitmap,
and should only be called once during the life of an image
*/
void CDIBitmap::CreateDibSection()
{
	BYTE* pSectionBits=NULL;
	_hSectionedDib=::CreateDIBSection(NULL,
		_bitmapInfo,
		_dibMode,
		reinterpret_cast<void**>(&pSectionBits),
		NULL,
		NULL);

	//cache the pointer
	_image=pSectionBits;
	ASSERT(_hSectionedDib!=NULL);
}


/**
* Load the bitmap from a file
* @param file name of the file to load
* @throws std::exception derivatives when things go wrong
*/
void CDIBitmap::Load(LPCTSTR file)
{
	HANDLE hFile = ::CreateFile(file, GENERIC_READ, 
		FILE_SHARE_READ, 
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_SEQUENTIAL_SCAN,
		NULL);
	if(hFile==INVALID_HANDLE_VALUE)
	{
		throw std::domain_error("File open failed");
	}
	try 
	{
		Load(hFile);
		CloseHandle(hFile);
	} catch(std::exception)
	{
		//close the file handle after use
		CloseHandle(hFile);
		throw;
	}
}

/**
* Load the bitmap from a file
* @param hFile handle to opened file
* @throws std::exception derivatives when things go wrong
*/
void CDIBitmap::Load(HANDLE hFile)
{
	Empty();
	BITMAPFILEHEADER fileHeader;
	DWORD bytesRead=0;
	ReadFile(hFile,static_cast<void*>( &fileHeader), 
		sizeof(fileHeader),&bytesRead,NULL);
	if(bytesRead!= sizeof(BITMAPFILEHEADER)) 
	{
		throw std::domain_error("Not a bitmap: wrong header size");

	}
	if(fileHeader.bfType != BITMAP_HEADER) {
		throw std::domain_error("Not a bitmap: wrong file magic number");
	}
	_headerSize = fileHeader.bfOffBits - sizeof(BITMAPFILEHEADER);

	//read in header.
	_bitmapInfo = (LPBITMAPINFO) new BYTE[_headerSize];
	ReadFile(hFile,static_cast<void*>(_bitmapInfo), 
		(DWORD)_headerSize,&bytesRead,NULL);
	if(((size_t)bytesRead)!= _headerSize) 
	{
		throw std::domain_error("Header too short");
	}

	//validate the header size
	//NB 0x28h =v3, 0x6C=V4 and 0x7c=v5 
	DWORD infoHeaderSize=GetInfoHeader()->biSize;
	if(infoHeaderSize != sizeof(BITMAPINFOHEADER)
		&& infoHeaderSize != sizeof(BITMAPV4HEADER)
		&& infoHeaderSize != sizeof(BITMAPV5HEADER))
	{
		throw std::domain_error("Not a supported bitmap:  wrong header size");
	}


	//now parse the header to determine bitmap size
	// check number of planes. Windows 3.x supports only 1 plane DIBs
	if (GetInfoHeader()->biPlanes != 1)
	{
		throw std::domain_error("Not a supported bitmap:  too many planes");
	}
	if (GetInfoHeader()->biCompression!= BI_RGB)
	{
		throw std::domain_error("Unsupported compression option");
	}
	_dibMode=DIB_RGB_COLORS;

	//bits/pixel
	_bitsPerPixel = GetInfoHeader()->biBitCount;
	_width=(int)GetInfoHeader()->biWidth;
	_height=(int)GetInfoHeader()->biHeight;
	_trueWidth= (int)((_width*_bitsPerPixel+31L) / 32) * 4;
	_trueHeight=_height>=0?_height:(-_height);
	_imageSize=_trueWidth*_trueHeight;
	CreateDibSection();
	if(!ReadFile(hFile,static_cast<void*>(_image), 
		(DWORD)_imageSize,&bytesRead,NULL) || bytesRead!=_imageSize)
	{
		throw std::domain_error("incomplete bitmap");
	}

}

/** 
* Get the address of the start of a scan line. 
* Works for any direction of bitmap
* @param line the line to get
* @return pointer to the scan line the specified co-ordinate
* @return NULL if beyond the end of the array
*/

BYTE   *CDIBitmap::ScanLine(int line)
{

	size_t h;
	if(line>=_trueHeight)
	{
		return NULL;
	}

	if(IsTopDown())
	{
		h=line;
	}
	else
	{
		h=_trueHeight-1-line;
	}

	size_t l=h * _trueWidth;
	return _image+l;
}

/**
* invert a bitmap so that it becomes topdown or upside down,
* depending upon its prior state
*/

void CDIBitmap::Invert()
{
	BYTE *first=_image;
	BYTE *last=_image+_trueWidth*(_trueHeight-1);
	BYTE *buffer=new BYTE[_trueWidth];

	//we invert first by swapping all the memory
	for(int line=0;line<_trueHeight/2;line++)
	{
		CopyMemory(buffer,last,_trueWidth);
		MoveMemory(last,first,_trueWidth);
		CopyMemory(first,buffer,_trueWidth);
	}

	//then flip the information
	_height=-_height;
	GetInfoHeader()->biHeight=-GetInfoHeader()->biHeight;

	delete [] buffer;
	//all done!
}


const long PIXELS_PER_INCH=72;
const long PIXELS_PER_METRE=(long)(PIXELS_PER_INCH*100/2.54);

/**
* if a bitmap has no resolution, give it 72 dpi
*/
void CDIBitmap::PatchResolution()
{
	LPBITMAPINFOHEADER header = GetInfoHeader();
	if(header->biXPelsPerMeter==0)
	{
		TRACE0("Patching unset XPelsPerMeter");
		header->biXPelsPerMeter=PIXELS_PER_METRE;
	}
	if(header->biYPelsPerMeter==0)
	{
		TRACE0("Patching unset YPelsPerMeter");
		header->biYPelsPerMeter=PIXELS_PER_METRE;
	}
}
