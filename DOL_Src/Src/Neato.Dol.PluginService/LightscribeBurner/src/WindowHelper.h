#define WinVerMajor()      LOBYTE(LOWORD(GetVersion()))
#define WinVerMinor()      HIBYTE(LOWORD(GetVersion()))
#define IsWinVerMEPlus()   (WinVerMajor()>=5 || WinVerMinor()>10)
#define IsWinVerXPPlus()   (WinVerMajor()>=5 && LOWORD(GetVersion())!=5)

class CWindowHelper {
public:
	static void ActivateWindow(CWnd wnd) {
		ActivateWindow(wnd.m_hWnd);
	}

	static void ActivateWindow(HWND wnd) {
		if(IsWinVerXPPlus()){
			HMODULE hLib = GetModuleHandle("user32.dll");
			void (__stdcall *SwitchToThisWindow)(HWND, BOOL);
			(FARPROC &)SwitchToThisWindow = GetProcAddress(hLib, "SwitchToThisWindow");
			SwitchToThisWindow(wnd, TRUE);
		}
		else{
			HWND hCurrWnd;
			int iMyTID;
			int iCurrTID;
			
			hCurrWnd = ::GetForegroundWindow();
			iMyTID   = GetCurrentThreadId();
			iCurrTID = GetWindowThreadProcessId(hCurrWnd,0);
			
			AttachThreadInput(iMyTID, iCurrTID, TRUE);
			
			
			SetForegroundWindow(wnd);
			
			AttachThreadInput(iMyTID, iCurrTID, FALSE);
		}
	}
};