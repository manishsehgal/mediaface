//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by e:\Depot\Neato\Dol\Src\Neato.Dol.PluginService\LightscribeBurner\res\ImageBurner.rc
//
#define IDR_MANIFEST                    1
#define IDB_PREVIEW                     3
#define IDB_CHANGE_OPTIONS              3
#define IDB_HELP                        4
#define IDP_OLE_INIT_FAILED             100
#define IDR_HTML_LABELPREVIEW           103
#define IDR_MAINFRAME                   128
#define IDS_TIMEREMAINING               129
#define IDD_PREPARETOPRINT              130
#define IDS_ABORT_BURN                  130
#define IDD_PRINTPROGRESS               131
#define IDS_PRINT_FINISHED_SUCCESSFUL   131
#define ID_LightScribe_PRINT            132
#define IDS_PRINT_FINISHED_FAILED       132
#define ID_LightScribe_DIAGNOSTICS      133
#define IDS_PRINT_CANCELED              133
#define IDS_PRINT_DLL_LOCATION          134
#define IDS_PRINT_DLL_MISSING           135
#define IDS_PRINT_DLL_UNREGISTERED      136
#define IDS_DIAGNOSTICS_TITLE           137
#define IDD_DIALOG1                     137
#define IDD_PRINTPREVIEW                137
#define IDS_COM_ERROR                   138
#define IDD_ERROR                       138
#define IDS_STRING139                   139
#define IDS_INSERT_DISC                 139
#define IDS_STRING140                   140
#define IDS_NO_DRIVES                   140
#define IDD_DIALOG3                     140
#define IDD_DETECTING_MEDIA             140
#define IDS_DRIVE_NOT_READY             141
#define IDS_DRIVE_READY                 142
#define IDD_HELP                        142
#define IDS_DRIVE_STATUS_UNKNOWN        143
#define IDS_DRIVE_MONOCHROME            144
#define IDS_DRIVE_COLOR                 145
#define IDS_LABEL_12CM_DISC             146
#define IDS_LABEL_8CM_DISC              147
#define IDS_LABEL_UNKNOWN               148
#define IDS_FLIP_DISC                   149
#define IDC_DRIVELIST                   1000
#define IDC_PERCENTCOMPLETE             1002
#define IDC_DRIVECHOICE                 1003
#define IDC_PERCENTCOMPLETE2            1003
#define IDC_DRIVE_STATUS                1005
#define IDC_DRIVE_CAPABILITIES          1006
#define IDC_DRIVE_PRODUCT_NUMBER        1007
#define IDC_MEDIA_DETAILS_ERROR         1007
#define IDC_DRIVER_VERSION              1008
#define IDC_MEDIA_VERSION               1009
#define IDC_DRIVESTATUS                 1010
#define IDC_QUALITY_BEST                1011
#define IDC_QUALITY_NORMAL              1012
#define IDC_QUALITY_FASTEST             1013
#define IDC_COPIES                      1014
#define IDC_TIMEREMAINING               1016
#define IDC_LABEL_FULL                  1017
#define IDC_LABEL_TITLE                 1018
#define IDC_LABEL_CONTENTS              1019
#define IDC_SCALEIMAGE                  1020
#define IDC_PREVIEWPICTURE              1021
#define IDC_BUTTON1                     1022
#define IDC_SCROLLBAR1                  1023
#define IDERROR                         1024
#define IDC_PREVIEWTYPE                 1025
#define IDC_DRIVE_MANUFACTURER          1026
#define IDC_DRIVE_MODEL                 1027
#define IDC_DRIVE_MODEL2                1028
#define IDC_PLUGIN_VERSION              1028
#define IDC_MEDIA_DETAILS               1029
#define IDGENERIC                       1030
#define IDC_CUSTOM_BROWSER              1031
#define IDC_STATIC_BROWSER              1031
#define IDC_TIMEREMAINING_TITLE         1032
#define IDC_TIMEREMAINING_TITLE2        1033
#define IDC_PRINT_TITLE                 1034
#define IDC_PRINTTIMEREMAINING          1035
#define IDUPDATE                        1036
#define IDC_MEDIA_INFO                  1037
#define ID_Menu                         32771
#define ID_HELP_TEST                    32773
#define ID_LightScribe_APPLICATION      32774
#define ID_DEBUG_LASTCOMERROR           32775
#define ID_DEBUG_SENDEVENT              32776
#define ID_LightScribe_PRINTTOFILE      32781
#define ID_UPDATE_MEDIA_INFO            32782

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        143
#define _APS_NEXT_COMMAND_VALUE         32782
#define _APS_NEXT_CONTROL_VALUE         1032
#define _APS_NEXT_SYMED_VALUE           106
#endif
#endif
