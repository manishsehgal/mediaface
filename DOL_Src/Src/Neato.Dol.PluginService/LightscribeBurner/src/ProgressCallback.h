/**
* @file ProgressCallback.h
*/

#pragma once
#include "Unknwn.h"
#include "PrintControl.h"

#include <deque>

/**
* events which get sent as WPARAM in progress callback messages
*/
enum ProgressEvents 
{

	PreparePrintProgress=0,		//!< progress during setup
	PrintProgress,				//!< progress during printing
	PrintComplete,				//!< printing completed
    TimeEstimate				//!< estimated printing time
};

/**
* This class implements the callbacks from the print engine.
* It implements the notification interface and basic IUnknown functionality.
* Callbacks are forwarded to the the notificationWindow window via a registered message.
*/
class CProgressCallback:public LSCAPILib::ILSDiscPrintProgressEvents
{
private:
	/**
	* Reference counter. 
	*/
	volatile LONG m_refCount;

	/** The window to recieve our callback messages.  */
	HWND m_notificationWindow;

	/** Message to send for callbacks.  */
	UINT m_callbackMessage;

	/** Aborted flag: true if an abort was requested.  */
	volatile bool m_aborted;


	/**
	* Flag set to true when the print is complete. 
	*/
	volatile bool m_printComplete;

	/**
	* Percentage done
	*/
	volatile int m_percentageDone;

	/**
	* Print status
	*/
	volatile HRESULT m_printStatus;


	/**
	* When did the print start? 
	*/
	volatile time_t m_printStarted;

	/**
	* The estimated time in seconds 
	*/
	volatile long m_estimatedTime;

	/**
	* History of calculated secondsRemaining values, used for 
	* caluclating the returned moving average. 
	*/
	std::deque<time_t> m_history;

	/**
	* The length of the moving average
	*/
	static const int historySize = 2;

	/**
	* CritSec used to synchronize access to any of the cross-thread
	* attributes available in this class. 
	*/
	CCriticalSection m_lock;

public:
	/**
	* Factory method. 
	* @param notificationWindow Which window 'owns' this callback and wants notification messages. 
	* @return Pointer to a new callback, bound to the notificationWindow. 
	*/
	static CProgressCallback *CreateCallback(HWND notificationWindow);


	/**
	* Thread-safe ref count increment. 
	*/
	virtual ULONG STDMETHODCALLTYPE AddRef(void);

	/**
	* Thread-safe ref count decrement; deletes the object when it is time. 
	*/
	virtual ULONG STDMETHODCALLTYPE Release(void) ;

	/**
	* Break out from all QI calls
	* @param riid referenc to the the IID of the interface we want
	* @param ppvObject pointer to where we this QI call should put the interface pointer.
	* @return return S_OK and set the pointer this when IUnknown is asked for; 
	*   E_NOINTERFACE for anything else. 
	*/
	virtual HRESULT STDMETHODCALLTYPE QueryInterface( 
		REFIID riid,
		void **ppvObject);

	/**
	* Send a message to the notificationWindow window. 
	* @param event
	* @param data
	*/
	void NotifyWindow(ProgressEvents event, LPARAM data) ;


	/**
	* Abort the burn. Thread safe.
	*/
	void AbortPrint() ;

	/**
	* Test for being aborted.
	* @return true if an abort request was made. 
	*/
	bool IsAborted() ;


	/**
	* Get time remanining in seconds. 
	* @return -1 if we dont know
	*/
	time_t GetSecondsRemaining();

	/**
	* Get percentage complete. 
	* @return 0-100
	*/
	int GetPercentageDone();


	/**
	* Get the exit status. 
	* @return the status code
	*/
	HRESULT GetPrintStatus();

	/**
	* Notification of pre-print progress; the conversion of the 
	* bitmap into a format that the drive can handle. Cancelling
	* the process in this stage will not result in any writing to 
	* the media.
	* @param current The current point in the progress. 
	* @param final The final point point in the progress. 
	* @return S_OK if handled. 
	*/
	virtual HRESULT STDMETHODCALLTYPE raw_NotifyPreparePrintProgress( 
		int current,int final);

	/**
	* Notification of actual label printing. 
	* The two area parameters are not used as absolute values, but ratios. 
	* They provide measures of the percentage of the print completed
	* accurately enough to be used in a progress indicator. 
	* @param areaPrinted  The area of the disc printed
	* @param totalArea	  The total area to print
	* @return S_OK if handled. 
	*/
	virtual HRESULT STDMETHODCALLTYPE raw_NotifyPrintProgress( 
		/* [in] */ int areaPrinted,
		/* [in] */ int totalArea);

	/**
	* This callback is made when the printing has finished,
	* whether sucessfully or not. 
	* 
	* Errors are documented in the error code list.
	* @param status The final status of the print process.
	* @return S_OK if processed.
	*/
	virtual HRESULT STDMETHODCALLTYPE raw_NotifyPrintComplete( 
	/* [in] */ HRESULT status);

	/**
	* A call from the print engine to see if the application/user wishes
	* to cancel the print. If the method requests cancelation then the
	* print engine will abort the burn and notify the application 
	* accordingly.
	* @return S_OK if processed.
	*/
	virtual HRESULT STDMETHODCALLTYPE raw_QueryCancel( 
	/* [retval][out] */ VARIANT_BOOL *bCancel);


	virtual HRESULT STDMETHODCALLTYPE raw_ReportLabelTimeEstimate(
	/*[in]*/ long seconds, /*[out,retval]*/ VARIANT_BOOL *bCancel);

private:
	/**
	* The constructor is private so that the factory is the only way to
	* instantiate the objects (thus they are always on the heap, not the stack).
	* @param notificationWindow Window to send notifications to. 
	*/
	CProgressCallback(HWND notificationWindow);

	/**
	* The destructor is private: deletion is through the Delete() call when appropriate
	*/
	~CProgressCallback(void);

	/**
	* Update percentage done. 
	* @param newValue updated percentage
	*/
	void SetPercentageDone(int newValue);


	/**
	* Update the exit status. 
	* @param status Status at end of print
	*/
	void SetPrintStatus(HRESULT status);

	/**
	* Reset the clock that catches time remaining info. 
	*/
	void ResetTimeRemaining();

	/**
	* Called at the end of printing to say 'we have finished'; updates the status
	* flag and marks us as complete, provided this has not been done already. 
	* @return true if this was the first call to set printing complete, false otherwise
	*/
	bool SetPrintComplete(HRESULT status);

};
