#pragma once
#include "BurnControl.h"
#include "afxwin.h"
#include "ComHelper.h"
#include "XHTMLStatic.h"


//Helper to enable and disable windows
class CWindowDisabler
{
private:
	CWnd *m_owner;
public:
	
	//Disable a window on construction. 
	CWindowDisabler(CWnd *owner)
		:m_owner(owner)
	{
		m_owner->EnableWindow(FALSE);
	}

	// Enable the window on destruction. 
	~CWindowDisabler()
	{
		m_owner->EnableWindow(TRUE);
	}

};

//Class that implements the pring dialog; bound to IDD_PREPARETOPRINT. 
class CLightScribePrintDialog : 
	public CDialog, 
	public SystemStateChangeListener
{
	DECLARE_DYNAMIC(CLightScribePrintDialog)
	DECLARE_MESSAGE_MAP()
	virtual void DoDataExchange(CDataExchange* pDX);    

protected:
	virtual  BOOL OnInitDialog( );
	virtual HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	
	//Reference to the burn control that owns this object. 
	CBurnControl& m_owner;

	//The currently selected drive. 
	int m_driveSelection;

	//Info about each drive on the system. 
	std::vector<CString> m_driveInfo;

	//Print options
	ComHelper::PrintOptions m_options;
	
	// A place to preserve user selections between dialog instances. 
	static ComHelper::PrintOptions static_options;
	
	//Dropdown list of LightScribe drives. 
	CComboBox m_driveList;

	//Drive manufacturer text
	CString m_driveManufacturer;

	//Drive model text
	CString m_driveModel;

	//Drive features text
	CString m_driveFeatures;

	//Media info text
	CString m_mediaDetails;
	CString m_mediaDetailsError;

	// string with version of LightScribe driver
	CString m_DriverVersion;

	//The three quality buttons. 
	CButton m_qualityButtonBest;
	CButton m_qualityButtonNormal;
	CButton m_qualityButtonDraft;

	//The "Scale Image" checkbox
	CButton m_btnScaleImage;
	
	// Action buttons
	CButton m_btnPreview;
	CButton m_btnPrint;

	/**
	* A label to display the status of the current drive. 
	* This one can do hyperlinks. 
	*/
	CXHTMLStatic m_StatusText;

	/**
	* Handle the mapping from print quality to GUI. 
	*/
	void DDX_PrintQuality(CDataExchange* pDX);

	/**
	* Handle the mapping from drives to list box. 
	*/
	void DDX_DriveSelection(CDataExchange* pDX);

	/**
	* Push the drive selection up to the owning BurnControl. 
	* @return true if there was a selection
	* @throws _com_error
	*/
	bool BindOwnerToSelectedDrive();

	/**
	* Used for PnP event handling. 
	* It updates the list of drives, but strives to preserve
	* the current drive selection. 
	*/
	void RefreshDriveList();

	/**
	* Enable all dialog controls
	*/
	void EnableControls();

	/**
	* Disable all dialog controls except cancel.
	*/
	void DisableControls();

	void Start_GetMediaInfo();
	static UINT AFX_CDECL RunDetecterThread(LPVOID instance);
	void GetCurrentDriveInfo();
	void GetCurrentDriverVersionInfo();
	void ResetDescriptions();

public:
	/**
	* Standard constructor. 
	* @param owner Owning burn control instance
	* @param pParent Parent window
	*/
	CLightScribePrintDialog(CBurnControl& owner,CWnd* pParent = NULL, 
							LabelMode labelMode = label_mode_full,
							PrintQuality printQuality = quality_normal);  
	virtual ~CLightScribePrintDialog();

	// Dialog Data
	enum { IDD = IDD_PREPARETOPRINT };

	/**
	* Read state information from the owning CBurnControl
	* @throws _com_error if bad things happen
	*/
	void ReadOwnerInfo();

	/**
	* Write the updated config to the owning burn control. 
	*/
	bool WriteOwnerInfo();

	/**
	* When the user hits ok, save the current settings. 
	*/
	void OnOK();

	/**
	* Print preview
	*/
	afx_msg void OnPreview();

	/**
	* Callback on a PnP device add. 
	* (see WM_DEVICECHANGE/DBT_DEVICEARRIVAL)
	*/
	virtual void OnPnPDeviceAdd(DEV_BROADCAST_HDR* deviceInfo);

	/**
	* Callback on a PnP device remove.
	* (see WM_DEVICECHANGE/DBT_DEVICEREMOVECOMPLETE)
	*/
	virtual void OnPnPDeviceRemoval(DEV_BROADCAST_HDR* deviceInfo);

	/**
	* Callback on a PnP device node change. 
	* (see WM_DEVICECHANGE/DBT_DEVNODES_CHANGED)
	*/
	virtual void OnPnPNodeChange(DEV_BROADCAST_HDR* deviceInfo);

	/**
	* Called on WinXP fast user switch. 
	* At this point the system has probably already switched. 
	*/
	virtual void OnFastUserSwitch(int event);

	/**
	* Callback whenever the user selects a different drive.
	*/
	afx_msg void OnDrivelistSelectionChanged();
    afx_msg void OnBnClickedHelp();
	afx_msg void OnUpdateMediaInfo();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
};
