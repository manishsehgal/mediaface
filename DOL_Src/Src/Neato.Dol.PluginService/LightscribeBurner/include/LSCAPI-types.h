//
// (C) Copyright 2003 Hewlett-Packard Development Company, LP
//

#ifndef _LSCAPI_TYPES_H_
#define _LSCAPI_TYPES_H_

/**
	@file 
	
	This file contains all the data types we are using in LSCAPI.
	**/

	//===================================================================
	/**
	 * enumeration of image types currently supported
	 */
	//===================================================================

    typedef enum ImageType {

		/** a BITMAP structure */

        windows_bitmap,
    } ImageType;


	//===================================================================
	/**
	 * Scaling options. This is a bitfield enumeration.
	 * The specified dimension is scaled to the entire label area regardless of
	 * the selected print mode.  Specifying title or content mode in conjunction with scaling
	 * will crop the image to the target label after it is scaled.  
	 * The effects of specifying multiple scaling options at once is undefined.  
	 * If no scaling options are set, the image will not be scaled.  The resolution set in 
	 * the bitmap header will be used.  
	 * @remarks Square images may be scaled by picking any of the scaling options - the result is the same. 
	 * @remarks A labeling application should not use any scaling modes.  It should query for the 
	 * resolution used by a particular print mode using GetPrintResolution, request the label
	 * size for the print mode using GetLabelOuterRegionRadius, and render the image with the 
	 * returned resolution and size.  Remember that they use metric units.  
	 */
	//===================================================================

	typedef enum DrawOptions
	{

		/** disable scaling of bitmaps; they will be cropped if needed */
		draw_default=0,

		/** Fit the height to the label size */
		draw_fit_height_to_label=1, 

		/** Fit the width to the label size */
		draw_fit_width_to_label=2,

		/** Fit the smallest dimension to the label size */
		draw_fit_smallest_to_label=4
		
	} DrawOptions;

	/**
	 * This structure is used when sizes or dimensions are needed
	 */
	typedef struct Size
	{
		/** first dimension, X or inner radius */
		long x;
		/** second dimension, Y or outer radius */
		long y;
	} Size;

	//===================================================================
	/**
	 * Enumeration of device capabilities.
	 * This is a bitwise enumeration, and capabilties <i>must</i> be
	 * tested for with masking tests. Example
	 * \code
	 * bool supportsMonochrome = deviceCapabilities & label_monochrome;
	 * \endcode
	 */
	//===================================================================

	typedef enum DeviceCapabilitiesEnum
	{
		/** the device can print monochrome labels */
		label_monochrome=1,

		/** the device can print color labels */
		label_color=2,
	} DeviceCapabilitiesEnum;


	//===================================================================
	/**
	 * The print quality.
	 * Print quality controls the resolution and the optical density;
	 * faster prints have lower contrast.
	*/
	//===================================================================
	typedef enum PrintQuality
	{
		/** best and slowest */
		quality_best=0,
		/** OK for everyday use */
		quality_normal=1,
		/** fast but not so good */
		quality_draft=2
	} PrintQuality;


	/**
	 * the labelling modes supported by the print engine.
	 * The exact meaning of these modes are defined in the SRS
	 */
	typedef enum LabelMode
	{
		/** label the entire disc */
		label_mode_full=0,

		/** label a title on the disk */
		label_mode_title=1,

		/** print a label with some contents information */
		label_mode_content=2
	} LabelMode;

	/**
	 * The shape and size of a disc, as determined by the
	 * shape and size parameter encoded in the media.
	 * All other values in this enumeration are reserved.
	 */
	typedef enum DiscShapeAndSize
	{
		/**
		 * the dimensions of this media are unknown */
		label_unknown=0,

		/** A full size CD or DVD */
		label_12cm_circle=1,

		/**  a CD 'single' */
		label_8cm_circle=2
	} DiscShapeAndSize;


	/**
	 * How much to use optimized media information found in the 
	 * DRF.  
	 */
	typedef enum MediaOptimizationLevel {
		/**
		 * Require that media is present and is found in the DRF.  
		 * This provides the best possible results by using optimized
		 * parameters found there.  
		 */
		media_recognized, 
		
		/**
		 * Require that media is present, but don't require DRF entries
		 * for it.  This can allow unoptimized printing using generic
		 * parameters.  
		 */
		media_generic
	} MediaOptimizationLevel;

	//===================================================================
	/**
	 * an RGB triple used when describing color values
	 */
	//===================================================================

	typedef struct ColorRGB
	{
		/** red component */
		unsigned char red;
		/** green component */
		unsigned char green;
		/** blue component */
		unsigned char blue;
	} ColorRGB;


	//===================================================================
	/**
	 * information about the media in a drive
	 */
	//===================================================================
	typedef struct MediaInformation {

		/**
		 * Boolean set to 1 when there is media present, 0 otherwise.
		 */
		int mediaPresent;


		/**
		 * Boolean set to 1 when the label side of the media
		 * is facing the laser, 0 otherwise. 
		 */
		int mediaOrientedForLabeling; 

		/** Manufacturer ID */
		short manufacturer;

		/** The inner radius of the printable area, in microns. */
		long innerRadius;

		/** The outer radius of the printable area, in microns. */
		long outerRadius;

		/**
		 * The foreground color of this media. 
		 */
		ColorRGB foreground;

		/**
		 * The background color of this media.
		 */
		ColorRGB background;

		/**
		 * The shape and size of this media.
		 */
		DiscShapeAndSize shapeAndSize;

	} MediaInformation;

#endif // _LSCAPI_TYPES_H_
