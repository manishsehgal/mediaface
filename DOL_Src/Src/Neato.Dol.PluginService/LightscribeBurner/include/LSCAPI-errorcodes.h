

//
// (C) Copyright 2003 Hewlett-Packard Development Company, LP
//

/** @file
 This file defines the error codes that are specific to the LSCAPI COM interfaces.
 Other COM errors may also be received when invoking methods on the interfaces.
 @remarks This is machine generated! Do Not Edit!
*/

#pragma once

#ifndef INCLUDED_LSCAPILib
#define INCLUDED_LSCAPILib 

/**
LSCAPI Error Codes
*/
namespace  LSCAPILib
{
	/**
	* LSCAPI error codes go into the FACILITY_ITF facility,
  * as COM likes it. COM also likes user defined errors
  * to start a bit of the way in to the facility.
	*/
const HRESULT LSCAPI_E_FACILITY=(HRESULT)0xC0040000L;

	/**
	* The base address that LSCAPI error codes begin at
	*/
const HRESULT LSCAPI_E_FIRST=(HRESULT)0xC0040200L;

	/**
	* The base address at which LSCAPI success codes begin
	*/
const HRESULT LSCAPI_S_FIRST=(HRESULT)0x00040000L;

	/**
	* helper macro - undefined at end of the header file
	*/
#define LSCAPI_ERROR(x) (LSCAPI_E_FACILITY+(x))

	/**
	* helper macro - undefined at end of the header file
	*/
#define LSCAPI_SUCCESS(x) (LSCAPI_S_FIRST+(x))


/** 
 A subsidiary library failed to load -check with diagnostics to see which particular library is missing. Reinstallation should fix this.   
*/
const HRESULT LSCAPI_E_LOADLIBRARY_ERROR=LSCAPI_ERROR(0x201);

/** 
 Memory error.   
*/
const HRESULT LSCAPI_E_MEMORY_ERROR=LSCAPI_ERROR(0x21A);

/** 
 An attempt to open a drive has been made with another drive already opened or this drive already in use.   
*/
const HRESULT LSCAPI_E_DRIVE_IN_USE=LSCAPI_ERROR(0x21E);

/** 
 API error: attempted reentrant call to a busy object.  Raised when a COM object is already busy.  For example: calling back into a PrintSession from a progess callback.   
*/
const HRESULT LSCAPI_E_REENTRANT=LSCAPI_ERROR(0x223);

/** 
 API Error: One of the supplied arguments is erroneous, possibly out of range.   
*/
const HRESULT LSCAPI_E_BAD_ARGUMENT=LSCAPI_ERROR(0x226);

/** 
 A change in drive description has indicated that the current drive is no longer valid - it may have been hotswapped for another.   
*/
const HRESULT LSCAPI_E_DRIVE_CHANGED=LSCAPI_ERROR(0x233);

/** 
 An internal error occurred.   
*/
const HRESULT LSCAPI_E_INTERNAL_ERROR=LSCAPI_ERROR(0x22F);

/** 
 External code for drive communications-related errors. This can occur when the drive does not communicate as expected or a particular request to the drive fails.  
*/
const HRESULT LSCAPI_E_COMMUNICATIONS=LSCAPI_ERROR(0x1080);

/** 
 External code for hardware (drive) related errors.   
*/
const HRESULT LSCAPI_E_HARDWARE=LSCAPI_ERROR(0x1081);

/** 
 External code for service-related errors.   
*/
const HRESULT LSCAPI_E_SERVICE=LSCAPI_ERROR(0x1087);

/** 
 The print engine was unable to find the resource file associated with this drive type. A LightScribe host software update may fix this.   
*/
const HRESULT LSCAPI_E_UNSUPPORTED_DRIVE=LSCAPI_ERROR(0x22B);

/** 
 This version of the software stack cannot use this drive.  A LightScribe host software update may fix this.   
*/
const HRESULT LSCAPI_E_UNSUPPORTED_DRIVE_COMMUNICATIONS=LSCAPI_ERROR(0x239);

/** 
 The resource file associated with this drive type is corrupt.  A LightScribe host software update may fix this.   
*/
const HRESULT LSCAPI_E_CORRUPT_DRF=LSCAPI_ERROR(0x22E);

/** 
 The current version of the LightScribe host software is older than the drive's resource file.  A LightScribe host software update may fix this.   
*/
const HRESULT LSCAPI_E_DRF_VERSION_MISMATCH=LSCAPI_ERROR(0x234);

/** 
 The drive indicates that no media is present. 
*/
const HRESULT LSCAPI_E_NO_MEDIA=LSCAPI_ERROR(0x1012);

/** 
 The drive indicates that non-LightScribe media is present. 
*/
const HRESULT LSCAPI_E_NON_LIGHTSCRIBE_MEDIA=LSCAPI_ERROR(0x23d);

/** 
 LightScribe discs contain media information encoded in the middle of the disc. If it cannot be read, this error gets raised.  The cause could be a dirty disc or a disc with the wrong side up.   
*/
const HRESULT LSCAPI_E_MEDIA_ID_READ_ERROR=LSCAPI_ERROR(0x208);

/** 
 A media identification parameter value is unsupported.  A LightScribe host software update may fix this.   
*/
const HRESULT LSCAPI_E_MEDIA_UNSUPPORTED_ID=LSCAPI_ERROR(0x237);

/** 
 A media identification parameter is invalid. A low laser power exceeds the corresponding high laser power or is zero. The low write speed value exceeds the high write speed value.  A LightScribe host software update will not fix this problem.   
*/
const HRESULT LSCAPI_E_MEDIA_INVALIDPARAMS=LSCAPI_ERROR(0x209);

/** 
 The drive does not support the media type. This error is returned if the drive does not have the proper laser for marking the media, or the drive's laser power or spindle speed limits do not support the media's speed/power curve.  A LightScribe host software update will not fix this problem.   
*/
const HRESULT LSCAPI_E_MEDIA_NOT_SUPPORTED_BY_DRIVE=LSCAPI_ERROR(0x220);

/** 
 The media is not oriented such that labeling can proceed.  This typically means that the disc needs to be flipped so the label side is exposed to the labeling laser.   
*/
const HRESULT LSCAPI_E_INVALID_MEDIA_ORIENTATION=LSCAPI_ERROR(0x205);

/** 
 The drive resource file does not support this media type.  A LightScribe host software update may fix this.   
*/
const HRESULT LSCAPI_E_UNSUPPORTED_MEDIA=LSCAPI_ERROR(0x22C);

/** 
 The drive resource file does not support this media type, and the file is no longer being updated.  A LightScribe host software update will not fix this problem.   
*/
const HRESULT LSCAPI_E_UNSUPPORTED_MEDIA_NO_UPDATE=LSCAPI_ERROR(0x23b);

/** 
 The drive resource file specifies that the current media will never work with this drive.  A LightScribe host software update will not fix this problem.   
*/
const HRESULT LSCAPI_E_INCOMPATIBLE_MEDIA=LSCAPI_ERROR(0x23c);

/** 
 The print preview command failed as the output file could not be created.   
*/
const HRESULT LSCAPI_E_PREVIEW_FAILED=LSCAPI_ERROR(0x229);

/** 
 Failed to print to a file.  The usual causes: access rights, disc space, etc apply.   
*/
const HRESULT LSCAPI_E_PRINT_FILE=LSCAPI_ERROR(0x210);

/** 
 API Error: unsupported source image format. Only those explictly supported in the enumeration of valid types are allowed.   
*/
const HRESULT LSCAPI_E_IMAGE_UNSUPPORTED=LSCAPI_ERROR(0x211);

/** 
 API error: invalid source image format.  This can include the resolution being unset or zero.   
*/
const HRESULT LSCAPI_E_IMAGE_INVALID=LSCAPI_ERROR(0x212);

/** 
 QueryCancel() requested a cancel; this message indicates that it has taken effect.   
*/
const HRESULT LSCAPI_E_ABORTPRINT=LSCAPI_ERROR(0x219);

/** 
  The progress callback threw an exception, which triggered an emergency abort of the print process.   
*/
const HRESULT LSCAPI_E_CALLBACK_EXCEPTION=LSCAPI_ERROR(0x21B);

/** 
 API Error: unsupported preview image format. Only those explictly supported in the enumeration of valid types are allowed.   
*/
const HRESULT LSCAPI_E_PREVIEW_UNSUPPORTED=LSCAPI_ERROR(0x21D);

/** 
 The specified print quality is not supported by this drive and media type.   
*/
const HRESULT LSCAPI_E_UNSUPPORTED_PRINT_QUALITY=LSCAPI_ERROR(0x22D);

/** 
 The drive tray operation was blocked because the currently selected drive's tray is locked.   
*/
const HRESULT LSCAPI_E_DRIVE_TRAY_LOCKED=LSCAPI_ERROR(0x22A);

/** 
 Not an error. This is a marker used to scope LSCAPI errors 
*/
const HRESULT LSCAPI_E_LAST=LSCAPI_ERROR(0x10AF);


//end namespace
}

#undef LSCAPI_ERROR
#undef LSCAPI_SUCCESS
#endif // INCLUDED_LSCAPILib