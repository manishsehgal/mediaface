;Files list
;Use:
;File "[path]\[name]"
;where
;	[path] - relative to this file or full path
;	[name] - filename with extension or mask like *.*,*.jpg...
;examples:
;	File "C:\mydir\1.jpg"
;	File "..\..\folder1\folder2\pic_*.*"
;
!macro FILES_LIST ;do not edit/remove this line

File /r "..\..\bin\*.exe";
File /r /x Light*.dll /x Light*.htm /x cbid.dll /x cbrcl.dll /x fp.dll /x audiocd.dll "..\..\bin\*.dll" "..\..\bin\*.htm";
File /nonfatal /r /x  Light*.bin "..\..\bin\*.bin";

!macroend ;do not edit/remove this line



!define EXE_NAME "dolcore.exe"
!define PROGRAM_NAME "MediaFace Online Plugins Service"
!define PROGRAM_SHORT_NAME "MediaFace Online Plugins Sevice"

!define LICENSE_TEXT "Please,read the license"
!define INSTALLATION_EXE_NAME "MediaFaceOnlinePluginsPack.exe"

!define ALREADY_INSTALLED_TEXT "MediaFace Online Plugins Sevice is already installed, would you like to reinstall?"

!macro HEADERS_TEXT
!ifndef MUI_PAGE_HEADER_TEXT
	!define MUI_PAGE_HEADER_TEXT "MediaFace Online Plugins Service Setup" 
!endif
!ifndef MUI_PAGE_HEADER_SUBTEXT
	!define MUI_PAGE_HEADER_SUBTEXT "Setup will install some modules to work with MediaFace Online web application"
!endif
!macroend 

!macro LICENSE_TEXT
!ifndef MUI_LICENSEPAGE_TEXT_TOP 
	!define MUI_LICENSEPAGE_TEXT_TOP "Read the license"
!endif
!macroend

!macro HEADERS_TEXT_UNINSTALL
	!ifdef MUI_PAGE_HEADER_TEXT
		!undef MUI_PAGE_HEADER_TEXT
	!endif
	
	!define MUI_PAGE_HEADER_TEXT "MediaFace Online Plugins Service Setup" 
	!ifdef MUI_PAGE_HEADER_SUBTEXT
		!undef MUI_PAGE_HEADER_SUBTEXT
	!endif
	!define MUI_PAGE_HEADER_SUBTEXT "Setup will uninstall  MediaFace Online web application modules "
!macroend
