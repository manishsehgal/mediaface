
!include "MUI.nsh"
!include "Options.nsh" ;Options file

;--------------------------------
;Installation Dir
InstallDir "$PROGRAMFILES\MediaFaceOnlinePluginsService"

; The name of the installer
Name "${PROGRAM_NAME}"

; The file to write
OutFile ".\${INSTALLATION_EXE_NAME}"

InstallDirRegKey HKLM "Software\MediaFace Online Plugins" ""

ReserveFile "welcome.ini"
;--------------------------------

; Pages


  !insertmacro HEADERS_TEXT
   Page custom WelcomePage  
  !insertmacro LICENSE_TEXT

  !insertmacro HEADERS_TEXT
  !insertmacro MUI_PAGE_DIRECTORY

  !insertmacro HEADERS_TEXT
  !define MUI_INSTFILESPAGE_FINISHHEADER_TEXT "${MUI_PAGE_HEADER_TEXT}"
  !define MUI_INSTFILESPAGE_FINISHHEADER_SUBTEXT "${MUI_PAGE_HEADER_SUBTEXT}"
  !insertmacro MUI_PAGE_INSTFILES

  !insertmacro HEADERS_TEXT_UNINSTALL
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro HEADERS_TEXT_UNINSTALL
  !insertmacro MUI_UNPAGE_INSTFILES

  ShowInstDetails nevershow

 
;--------------------------------

; The stuff to install
Section "" ;No components page, name is not important





  ;Set output path
  SetOutPath "$INSTDIR"
  DetailPrint "Copying files..."
  !insertmacro FILES_LIST ;compile files to the installation
  ;Store installation folder
  WriteRegStr HKLM "Software\MediaFace Online Plugins" "" $INSTDIR
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

  ;register application
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\run" "MediaFaceOnlinePluginsService" "$INSTDIR\${EXE_NAME}"
  ;add to addremove programs
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MediaFaceOnlinePluginsService" "DisplayName" "${PROGRAM_NAME}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MediaFaceOnlinePluginsService" "UninstallString" "$INSTDIR\Uninstall.exe"

  ;Creating start menu shortcuts

  CreateDirectory "$SMPROGRAMS\${PROGRAM_NAME}\"
  CreateShortCut "$SMPROGRAMS\${PROGRAM_NAME}\Start ${PROGRAM_SHORT_NAME}.lnk" "$INSTDIR\${EXE_NAME}" ""
  CreateShortCut "$SMPROGRAMS\${PROGRAM_NAME}\${PROGRAM_SHORT_NAME} Settings.lnk" "$INSTDIR\${EXE_NAME}" "-settings"
  CreateShortCut "$SMPROGRAMS\${PROGRAM_NAME}\Uninstall ${PROGRAM_SHORT_NAME}.lnk" "$INSTDIR\Uninstall.exe"


SectionEnd ; end the section

Function .onInit
	!insertmacro MUI_INSTALLOPTIONS_EXTRACT "welcome.ini"
       ReadRegStr $R1 HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MediaFaceOnlinePluginsService" "UninstallString"
	StrCmp $R1 "" next
        MessageBox MB_YESNO|MB_ICONINFORMATION "${ALREADY_INSTALLED_TEXT}" IDYES true IDNO false
	true:
	HideWindow
        ;ExecWait '$R1 _?=$INSTDIR'
	Call Uninstall;
	
	BringToFront;
        Goto next;
	false:
	  abort;
	next:
FunctionEnd


Function .onInstSuccess
	Exec "$INSTDIR\${EXE_NAME}";
FunctionEnd

;--------------------------------
;Uninstaller Section

Section "Uninstall"
 Call un.Uninstall
SectionEnd

Function un.Uninstall
  ExecWait '"$INSTDIR\${EXE_NAME}" -stop' ;service will close
  DeleteRegKey /ifempty HKLM "Software\MediaFace Online Plugins" ;delete registration key
  DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\run" "MediaFaceOnlinePluginsService"   ;delete uninstallation
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MediaFaceOnlinePluginsService" ;keys
  RMDir /r "$SMPROGRAMS\${PROGRAM_NAME}" ;delete start meny dir
  Delete "$INSTDIR\*.*" ;delete all files in folder
  RMDir /r "$INSTDIR" ;delete folder with all files
FunctionEnd
Function Uninstall  ; Doublicate functin for using in installer and uninstaller sections of this program
  ExecWait '"$INSTDIR\${EXE_NAME}" -stop' ;service will close
  DeleteRegKey /ifempty HKLM "Software\MediaFace Online Plugins" ;delete registration key
  DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\run" "MediaFaceOnlinePluginsService"   ;delete uninstallation
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\MediaFaceOnlinePluginsService" ;keys
  RMDir /r "$SMPROGRAMS\${PROGRAM_NAME}" ;delete start meny dir
  Delete "$INSTDIR\*.*" ;delete all files in folder
  RMDir /r "$INSTDIR" ;delete folder with all files
FunctionEnd

Function WelcomePage
 !insertmacro HEADERS_TEXT
  !insertmacro MUI_HEADER_TEXT_PAGE "${MUI_PAGE_HEADER_TEXT}" "${MUI_PAGE_HEADER_SUBTEXT}"
  !insertmacro MUI_INSTALLOPTIONS_DISPLAY "welcome.ini"
  
FunctionEnd


