#include "StdAfx.h"
#include "mp3info.h"

static int mpeg1Bitrates[] = { 0, 32, 40, 48, 56, 64, 80, 96, 112, 
                               128, 160, 192, 224, 256, 320 };
static int mpeg2Bitrates[] = { 0, 8, 16, 24, 32, 40, 48, 56, 64, 
                               80, 96, 112, 128, 144, 160 };
static int mpeg1SampleRates[] = { 44100, 48000, 32000 };
static int mpeg2SampleRates[] = { 22050, 24000, 16000 };

static int bitrate(const char *header)
{
   int id, br;

   id = (header[1] & 0x8) >> 3;
   br = (header[2] & 0xF0) >> 4;

   return id ? mpeg1Bitrates[br] : mpeg2Bitrates[br];
}

static int samplerate(const char *header)
{
   int id, sr;

   id = (header[1] & 0x8) >> 3;
   sr = (header[2] >> 2) & 0x3;

   return id ? mpeg1SampleRates[sr] : mpeg2SampleRates[sr];
}

static int stereo(const char *header)
{
   return ((header[3] & 0xc0) >> 6) != 3;
}

static int mpeg_ver(const char *header)
{
   return (!((header[1] & 0x8) >> 3) + 1); 
}

static int padding(const char *header)
{
   return (header[2] >> 1) & 0x1;
}

void mp3_init(mp3_info *info)
{
   memset(info, 0, sizeof(mp3_info));
}

void mp3_final(mp3_info *info)
{
/*oas
   if (info->badBytes > info->goodBytes)
   {
       memset(info, 0, sizeof(mp3_info));
   }
   else
oas*/
   {
       if (info->mpegVer == 1)
          info->duration = info->frames * 1152 / (info->samplerate / 1000);
       else
          info->duration = info->frames * 576 / (info->samplerate / 1000);
       info->avgBitrate /= info->frames;
   }
}

void mp3_update(mp3_info      *info,
                unsigned char *buffer, 
                unsigned       len)
{
   unsigned       size, bytesLeft;
   unsigned char *ptr, *max;
   unsigned char *temp = NULL;

   if (info->spanningSize > 0)
   {
      temp = (unsigned char *)malloc(len + info->spanningSize);
      memcpy(temp, info->spanningHeader, info->spanningSize);
      memcpy(temp + info->spanningSize, buffer, len);
      len += info->spanningSize;
      buffer = temp;
   }

   for(ptr = buffer + info->skipSize, max = buffer + len;
       ptr < max;)
   {
      if ((unsigned int)max - (unsigned int)ptr < 4)
      {
         info->spanningSize = (unsigned int)max - (unsigned int)ptr;
         memcpy(info->spanningHeader, ptr, info->spanningSize);
         info->skipSize = 0;

         if (temp)
            free(temp);

         return;
      }
 
      if (*ptr != 0xFF || ((*(ptr + 1) & 0xF0) != 0xF0 &&
                           (*(ptr + 1) & 0xF0) != 0xE0)) 
      {
          info->badBytes ++;
          ptr++;
          continue;
      }

      if (samplerate((const char *)ptr) == 0)
      { 
          info->badBytes ++;
          ptr++;
          continue;
      }

      if (mpeg_ver((const char *)ptr) == 1)
          size = (144000 * bitrate((const char *)ptr)) / samplerate((const char *)ptr) + padding((const char *)ptr);
      else
          size = (72000 * bitrate((const char *)ptr)) / samplerate((const char *)ptr) + padding((const char *)ptr);
      if (size <= 1 || size > 2048)
      {
          info->badBytes ++;
          ptr++;
          continue;
      }

      if (info->frames == 0)
      {
          info->samplerate = samplerate((const char *)ptr);
          info->bitrate = bitrate((const char *)ptr);
          info->mpegVer = mpeg_ver((const char *)ptr);
          info->stereo = stereo((const char *)ptr);
      }
      else
      {
          if (info->samplerate != samplerate((const char *)ptr))
          {
             info->badBytes ++;
             ptr++;
             continue;
          }

          if (info->bitrate && info->bitrate != bitrate((const char *)ptr))
          {
             info->bitrate = 0;
          }
      }
  
      bytesLeft = (unsigned int)max - (unsigned int)ptr;

      info->frames++;
      info->goodBytes += size;
      info->avgBitrate += bitrate((const char *)ptr);
      ptr += size;
   }

   info->skipSize = (unsigned int)ptr - (unsigned int)max;
   info->spanningSize = 0;
   if (temp)
      free(temp);
}
