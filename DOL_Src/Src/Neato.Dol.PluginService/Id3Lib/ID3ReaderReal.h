#ifndef __ID3READERREAL_H_
#define __ID3READERREAL_H_

#include <atlbase.h>
#include "ID3Reader.h"
#include "ID3v1Tag.h"
#include "mp3info.h"
#include "tag.h"
#include <vector>

/*
typedef std::auto_ptr<ID3v1Tag>     CAutoID3v1Tag;
class CAutoID3v1TagS
{
public:
    CAutoID3v1TagS(){}
    CAutoID3v1TagS(CAutoID3v1TagS & tt){tag=tt.tag;}
    CAutoID3v1TagS & operator=(CAutoID3v1TagS & tt){tag=tt.tag;return *this;}
    CAutoID3v1Tag tag;
};
typedef std::vector<CAutoID3v1TagS>  Cid3v1TagsBase;

class Cid3v1Tags : public Cid3v1TagsBase
{
public:
	void push_back(ID3v1Tag* _X)
	{
        CAutoID3v1TagS s;
        s.tag = CAutoID3v1Tag(_X);
		Cid3v1TagsBase::push_back(s);
	}
};
*/
typedef std::vector<ID3v1Tag>  Cid3v1Tags;

/////////////////////////////////////////////////////////////////////////////
// CMFID3Reader
class CID3ReaderReal : public CID3Reader
{
public:
	CID3ReaderReal() : m_bInited(FALSE), m_pid3Tag(NULL)
	{
		m_pid3Tag = NULL;
	};
	~CID3ReaderReal()
	{
		if (m_pid3Tag != NULL)
			delete m_pid3Tag;
	}

// IMFID3Reader
public:
	HRESULT Init(LPCWSTR strFile);
	HRESULT get_Duration(long *pVal);
	/*
	STDMETHOD(get_MpegVer)(long *pVal);
	STDMETHOD(get_Frames)(long *pVal);
	STDMETHOD(get_Stereo)(VARIANT_BOOL *pVal);
	STDMETHOD(get_SampleRate)(long *pVal);
	STDMETHOD(get_BitRate)(long *pVal);
	STDMETHOD(DeInit)();
	*/
	HRESULT get_tagID3v2byID(TAG tag, CID3Tag ** pVal);
	HRESULT get_tagID3v1byID(TAG tag, CID3Tag ** pVal);
	/*
	STDMETHOD(get_tagID3v2)(long lTag, IMFID3Tag* *pVal);
	STDMETHOD(get_countID3v2Tags)(long *pVal);
	STDMETHOD(get_tagID3v1)(long lTag, IMFID3Tag* *pVal);
	STDMETHOD(get_countID3v1Tags)(long *pVal);
	*/

protected:
	HRESULT InitID3v1(LPCWSTR strFile);
	HRESULT InitMP3Info(LPCWSTR strFile);

protected:
	Cid3v1Tags m_id3v1Tags;
	BOOL       m_bInited;
	ID3_Tag*   m_pid3Tag;
	mp3_info   m_mp3Info;
};

#endif //__ID3READERREAL_H_
