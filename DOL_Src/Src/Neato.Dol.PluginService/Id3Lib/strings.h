#ifndef _ID3LIB_STRINGS_H_
#define _ID3LIB_STRINGS_H_

#include <string>

namespace dami
{
  typedef std::basic_string<char>           String;
  typedef std::basic_string<unsigned char> BString;
  typedef std::basic_string<wchar_t>       WString;
};

#endif /* _ID3LIB_STRINGS_H_ */
