#include "stdafx.h"
#include "ID3v1Tag.h"

void CID3v1Tag::Init(ID3v1Tag* pTag)
{
	m_pTag = pTag;
}

HRESULT CID3v1Tag::get_ID(TAG *pVal)
{
	if (pVal == NULL)
		return E_FAIL;
	
	*pVal = m_pTag->ID;

	return S_OK;
}

HRESULT CID3v1Tag::get_TextID(BSTR *pVal)
{
	if (pVal == NULL)
		return E_FAIL;

	*pVal = m_pTag->bstrTextID.Copy();

	return S_OK;
}

HRESULT CID3v1Tag::get_Description(BSTR *pVal)
{
	if (pVal == NULL)
		return E_FAIL;
	
	*pVal = m_pTag->bstrDescription.Copy();

	return S_OK;
}

HRESULT CID3v1Tag::get_Content(BSTR *pVal)
{
	if (pVal == NULL)
		return E_FAIL;

	*pVal = m_pTag->bstrContent.Copy();

	return S_OK;
}
