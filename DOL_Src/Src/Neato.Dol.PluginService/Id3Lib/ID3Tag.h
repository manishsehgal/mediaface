#ifndef __ID3TAG_H_
#define __ID3TAG_H_

#include "tag.h"
#include "ID3Reader.h"

class CID3v2Tag : public CID3Tag
{
public:
	CID3v2Tag()
	{
	}

public:
	HRESULT get_ID(TAG * pVal);
	HRESULT get_TextID(BSTR * pVal);
	HRESULT get_Description(BSTR * pVal);
	HRESULT get_Content(BSTR * pVal);

public:
	void Init(ID3_Frame* pFrame);

protected:
	ID3_Frame* m_pFrame;
};

#endif //__ID3TAG_H_
