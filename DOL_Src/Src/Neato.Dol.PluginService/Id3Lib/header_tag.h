#ifndef _ID3LIB_HEADER_TAG_H_
#define _ID3LIB_HEADER_TAG_H_

#include "header.h"

class ID3_TagHeader : public ID3_Header
{
public:

  enum
  {
    UNSYNC       = 1 << 7,
    EXTENDED     = 1 << 6,
    EXPERIMENTAL = 1 << 5
  };

  ID3_TagHeader() : ID3_Header() { ; }
  virtual ~ID3_TagHeader() { ; }
  ID3_TagHeader(const ID3_TagHeader& rhs) : ID3_Header() { *this = rhs; }

  bool   SetSpec(ID3_V2Spec);
  size_t Size() const;
  void Render(ID3_Writer&) const;
  bool Parse(ID3_Reader&);
  ID3_TagHeader& operator=(const ID3_TagHeader&hdr)
  { this->ID3_Header::operator=(hdr); return *this; }

  bool SetUnsync(bool b)
  {
    bool changed = _flags.set(UNSYNC, b);
    _changed = _changed || changed;
    return changed;
  }
  bool GetUnsync() const { return _flags.test(UNSYNC); }
  bool SetExtended(bool b)
  {
    bool changed = _flags.set(EXTENDED, b);
    _changed = _changed || changed;
    return changed;
  }
  bool GetExtended() const { return _flags.test(EXTENDED); }
  bool SetExperimental(bool b)
  {
    bool changed = _flags.set(EXPERIMENTAL, b);
    _changed = _changed || changed;
    return changed;
  }
  bool GetExperimental() const { return _flags.test(EXPERIMENTAL); }

  // id3v2 tag header signature:  $49 44 33 MM mm GG ss ss ss ss
  // MM = major version (will never be 0xFF)
  // mm = minor version (will never be 0xFF)
  // ff = flags byte 
  // ss = size bytes (less than $80)
  static const char* const ID;
  enum
  {
    ID_SIZE        = 3,
    MAJOR_OFFSET   = 3,
    MINOR_OFFSET   = 4,
    FLAGS_OFFSET   = 5,
    SIZE_OFFSET    = 6,
    SIZE           = 10
  };
  
};

#endif /* _ID3LIB_HEADER_TAG_H_ */
