#include "stdafx.h"
#include "ID3Tag.h"
#include "misc_support.h"

void CID3v2Tag::Init(ID3_Frame* pFrame)
{
	m_pFrame = pFrame;
}

HRESULT CID3v2Tag::get_ID(TAG *pVal)
{
	if (pVal == NULL)
		return E_FAIL;
	
	ID3_FrameID eFrameID = m_pFrame->GetID();

	*pVal = (TAG)eFrameID;

	return S_OK;
}

HRESULT CID3v2Tag::get_TextID(BSTR *pVal)
{
	if (pVal == NULL)
		return E_FAIL;

	const char* pTextID = m_pFrame->GetTextID();

	CComBSTR bstrTextID(pTextID);
	*pVal = bstrTextID.Copy();

	return S_OK;
}

HRESULT CID3v2Tag::get_Description(BSTR *pVal)
{
	if (pVal == NULL)
		return E_FAIL;
	
	const char* pDescription = m_pFrame->GetDescription();

	CComBSTR bstrDescription(pDescription);
	*pVal = bstrDescription.Copy();

	return S_OK;
}

HRESULT CID3v2Tag::get_Content(BSTR *pVal)
{
	if (pVal == NULL)
		return E_FAIL;

	char* pContent = ID3_GetString(m_pFrame, ID3FN_TEXT);

	CComBSTR bstrContent(pContent);
	*pVal = bstrContent.Copy();

	delete [] pContent;

	return S_OK;
}
