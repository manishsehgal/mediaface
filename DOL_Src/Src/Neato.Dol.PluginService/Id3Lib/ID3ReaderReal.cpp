#include "stdafx.h"
#include "ID3ReaderReal.h"
#include "ID3Tag.h"
#include "ID3v1Tag.h"
#include "genre.h"
#include "tstring.h"



HRESULT CID3ReaderReal::Init(LPCWSTR strFile)
{
	USES_CONVERSION;

	try
	{
		// Extract mp3 header
		InitMP3Info(strFile);

		// Extract ID3v1
		InitID3v1(strFile);

		// Extract ID3v2
		if (m_pid3Tag != NULL)
			delete m_pid3Tag;
		m_pid3Tag = new ID3_Tag();
		m_pid3Tag->Link(W2T(strFile), ID3TT_ID3V2);

		// Check for existing information
		m_bInited = (m_pid3Tag->NumFrames() > 0) || 
					(m_id3v1Tags.size() > 0) || 
					(m_mp3Info.duration > 0);
	}
	catch(...)
	{
		return E_FAIL;
	}

	return m_bInited ? S_OK : E_FAIL;
}

/*
STDMETHODIMP CMFID3Reader::DeInit()
{
	// Clear mp3 header
	memset(&m_mp3Info, 0, sizeof(mp3_info));

	// Clear ID3v1
	m_id3v1Tags.clear();

	// Clear ID3v2
	delete m_pid3Tag;
	m_pid3Tag = NULL;

	return S_OK;
}
*/
/*
STDMETHODIMP CMFID3Reader::get_countID3v2Tags(long *pVal)
{
	if (pVal == NULL)
		return E_INVALIDARG;
	if (!m_bInited)
		return E_FAIL;

	*pVal = m_pid3Tag->NumFrames();

	return S_OK;
}
*/
/*
STDMETHODIMP CMFID3Reader::get_tagID3v2(long lTag, IMFID3Tag **pVal)
{
	if (pVal == NULL)
		return E_INVALIDARG;
	if (lTag > (long)m_pid3Tag->NumFrames())
		return E_FAIL;

	std::auto_ptr<ID3_Tag::Iterator> Iter = 
		std::auto_ptr<ID3_Tag::Iterator>(m_pid3Tag->CreateIterator());
	ID3_Frame* pFrame;
	for (size_t nFrames = 0; (long)nFrames <= lTag; nFrames++)
		pFrame = Iter->GetNext();

	if (NULL == pFrame)
		return E_FAIL;

	CComObject<CMFID3v2Tag>* pTag;
	CComObject<CMFID3v2Tag>::CreateInstance(&pTag);
	pTag->Init(pFrame);

	pTag->QueryInterface(IID_IMFID3Tag, (LPVOID*)pVal);

	return S_OK;
}
*/

HRESULT CID3ReaderReal::get_tagID3v2byID(TAG tag, CID3Tag ** pVal)
{
	if (pVal == NULL)
		return E_INVALIDARG;

	std::auto_ptr<ID3_Tag::Iterator> Iter = std::auto_ptr<ID3_Tag::Iterator>(
		m_pid3Tag->CreateIterator());
	ID3_Frame* pFrame = NULL;
	BOOL bFind = FALSE;
	for (size_t nFrames = 0; nFrames < m_pid3Tag->NumFrames(); nFrames++)
	{
		pFrame = Iter->GetNext();

		if (pFrame->GetID() == tag)
		{
			bFind = TRUE;
			break;
		}
	}

	if (NULL == pFrame || bFind == FALSE)
		return E_FAIL;

	CID3v2Tag * pTag = new CID3v2Tag;
	pTag->Init(pFrame);
	*pVal = pTag;

	return S_OK;
}

/*
STDMETHODIMP CMFID3Reader::get_countID3v1Tags(long *pVal)
{
	if (pVal == NULL)
		return E_INVALIDARG;
	if (!m_bInited)
		return E_FAIL;

	*pVal = m_id3v1Tags.size();

	return S_OK;
}
*/
/*
STDMETHODIMP CMFID3Reader::get_tagID3v1(long lTag, IMFID3Tag **pVal)
{
	if (pVal == NULL)
		return E_INVALIDARG;
	if (lTag > (long)m_id3v1Tags.size())
		return E_FAIL;
	
	Cid3v1Tags::iterator it = m_id3v1Tags.begin();
	for (long nTags = 0; nTags < lTag; nTags++)
		++it;

	CComObject<CMFID3v1Tag>* pTag;
	CComObject<CMFID3v1Tag>::CreateInstance(&pTag);
	pTag->Init(it->get());

	pTag->QueryInterface(IID_IMFID3Tag, (LPVOID*)pVal);

	return S_OK;
}
*/

HRESULT CID3ReaderReal::get_tagID3v1byID(TAG tag, CID3Tag ** pVal)
{
	if (!pVal) return E_INVALIDARG;

	BOOL bFind = FALSE;
	Cid3v1Tags::iterator it = m_id3v1Tags.begin();
	for (;it != m_id3v1Tags.end(); ++it)
	{
		if ((*it).ID == tag)
		{
			bFind = TRUE;
			break;
		}
	}
	
	if (!bFind) return E_FAIL;

	CID3v1Tag * pTag = new CID3v1Tag;
    pTag->Init(&(*it));
	*pVal = pTag;

	return S_OK;
}

/*
STDMETHODIMP CMFID3Reader::get_BitRate(long *pVal)
{
	if (pVal == NULL)
		return E_FAIL;
	*pVal = m_mp3Info.bitrate;
	return S_OK;
}
*/
/*
STDMETHODIMP CMFID3Reader::get_SampleRate(long *pVal)
{
	if (pVal == NULL)
		return E_FAIL;
	*pVal = m_mp3Info.samplerate;
	return S_OK;
}
*/
/*
STDMETHODIMP CMFID3Reader::get_Stereo(VARIANT_BOOL *pVal)
{
	if (pVal == NULL)
		return E_FAIL;
	*pVal = (m_mp3Info.stereo == 1) ? VARIANT_TRUE : VARIANT_FALSE;
	return S_OK;
}
*/

HRESULT CID3ReaderReal::get_Duration(long * pVal)
{
	if (!pVal) return E_FAIL;
	*pVal = m_mp3Info.duration;
	return S_OK;
}

/*
STDMETHODIMP CMFID3Reader::get_Frames(long *pVal)
{
	if (pVal == NULL)
		return E_FAIL;
	*pVal = m_mp3Info.frames;
	return S_OK;
}
*/
/*
STDMETHODIMP CMFID3Reader::get_MpegVer(long *pVal)
{
	if (pVal == NULL)
		return E_FAIL;
	*pVal = m_mp3Info.mpegVer;
	return S_OK;
}
*/
////////////////////////////////////////////////////////////////////
// Implementation

HRESULT CID3ReaderReal::InitID3v1(LPCWSTR strFile) 
{
	USES_CONVERSION;

	m_id3v1Tags.clear();

	struct id3 
	{
		char tag[3];
		char title[30];
		char artist[30];
		char album[30];
		char year[4];

		/* With ID3 v1.0, the comment is 30 chars long */
		/* With ID3 v1.1, if comment[28] == 0 then comment[29] == tracknum */
		char comment[30];

		unsigned char genre;
	} id3v1tag;

	FILE *fp;
	fp = fopen(W2T(strFile), "r");

	if (fp == NULL) 
		return E_FAIL;

	if (fseek(fp, -128, SEEK_END) < 0) 
	{/* problem rewinding */
	}
	else 
	{ /* we rewound successfully */ 
		if (fread(&id3v1tag, 128, 1, fp) < 0) 
			return E_FAIL;
	}

	if (fp != NULL)
		fclose(fp);
		
	if (!strncmp(id3v1tag.tag, "TAG", 3))
	{
		TCHAR szText[MAX_PATH];
		tstring strText;

		sprintf(szText, _T("%-30.30s"), id3v1tag.title);
		strText = szText;
		strText.erase(0, strText.find_first_not_of(_T(" ")));
		strText.erase(strText.find_last_not_of(_T(" ")) + 1);
		if (strText.size() > 0)
		{
			ID3v1Tag tag;
			tag.ID = ID3V1_TITLE;
			tag.bstrTextID = L"V1TIT";
			tag.bstrDescription = L"ID3v1 Title";
			tag.bstrContent = T2W(strText.c_str());
			m_id3v1Tags.push_back(tag);
		}

		sprintf(szText, _T("%-30.30s"), id3v1tag.artist);
		strText = szText;
		strText.erase(0, strText.find_first_not_of(_T(" ")));
		strText.erase(strText.find_last_not_of(_T(" ")) + 1);
		if (strText.size() > 0)
		{
			ID3v1Tag tag;
			tag.ID = ID3V1_ARTIST;
			tag.bstrTextID = L"V1ART";
			tag.bstrDescription = L"ID3v1 Artist";
			tag.bstrContent = T2W(strText.c_str());
			m_id3v1Tags.push_back(tag);
		}

		sprintf(szText, _T("%-30.30s"), id3v1tag.album);
		strText = szText;
		strText.erase(0, strText.find_first_not_of(_T(" ")));
		strText.erase(strText.find_last_not_of(_T(" ")) + 1);
		if (strText.size() > 0)
		{
			ID3v1Tag tag;
			tag.ID = ID3V1_ALBUM;
			tag.bstrTextID = L"V1ALB";
			tag.bstrDescription = L"ID3v1 Album";
			tag.bstrContent = T2W(strText.c_str());
			m_id3v1Tags.push_back(tag);
		}

		sprintf(szText, _T("%-4.4s"), id3v1tag.year);
		strText = szText;
		strText.erase(0, strText.find_first_not_of(_T(" ")));
		strText.erase(strText.find_last_not_of(_T(" ")) + 1);
		if (strText.size() > 0)
		{
			ID3v1Tag tag;
			tag.ID = ID3V1_YEAR;
			tag.bstrTextID = L"V1YEA";
			tag.bstrDescription = L"ID3v1 Year";
			tag.bstrContent = T2W(strText.c_str());
			m_id3v1Tags.push_back(tag);
		}

		if (id3v1tag.genre < genre_count && id3v1tag.genre > 0)
		{
			ID3v1Tag tag;
			tag.ID = ID3V1_GENRE;
			tag.bstrTextID = L"V1GEN";
			tag.bstrDescription = L"ID3v1 Genre";
			tag.bstrContent = T2W(genre_table[id3v1tag.genre]);
			m_id3v1Tags.push_back(tag);
		}

		if (!id3v1tag.comment[28])
		{
			sprintf(szText, _T("%-28.28s"), id3v1tag.comment);
			strText = szText;
			strText.erase(0, strText.find_first_not_of(_T(" ")));
			strText.erase(strText.find_last_not_of(_T(" ")) + 1);
			if (strText.size() > 0)
			{
				ID3v1Tag tag;
				tag.ID = ID3V1_COMMENT;
				tag.bstrTextID = L"V1COMM";
				tag.bstrDescription = L"ID3v1 Comment";
				tag.bstrContent = T2W(strText.c_str());
				m_id3v1Tags.push_back(tag);
			}

			sprintf(szText, _T("%d"), id3v1tag.comment[29]);
			strText = szText;
			strText.erase(0, strText.find_first_not_of(_T(" ")));
			strText.erase(strText.find_last_not_of(_T(" ")) + 1);
			if (strText.size() > 0)
			{
				ID3v1Tag tag;
				tag.ID = ID3V1_TRACK;
				tag.bstrTextID = L"V1TRACK";
				tag.bstrDescription = L"ID3v1 Track";
				tag.bstrContent = T2W(strText.c_str());
				m_id3v1Tags.push_back(tag);
			}
		}
		else
		{
			sprintf(szText, _T("%-30.30s"), id3v1tag.comment);
			strText = szText;
			strText.erase(0, strText.find_first_not_of(_T(" ")));
			strText.erase(strText.find_last_not_of(_T(" ")) + 1);
			if (strText.size() > 0)
			{
				ID3v1Tag tag;
				tag.ID = ID3V1_COMMENT;
				tag.bstrTextID = L"V1COMM";
				tag.bstrDescription = L"ID3v1 Comment";
				tag.bstrContent = T2W(strText.c_str());
				m_id3v1Tags.push_back(tag);
			}
		}
		
		return S_OK;
	} 
	return E_FAIL; 		
}

HRESULT CID3ReaderReal::InitMP3Info(LPCWSTR strFile)
{
	USES_CONVERSION;

	FILE *source;
	unsigned char   buffer[4096];
	int bytes;
	long pos = 0;
	
	source = fopen(W2T(strFile), "rb");
	if (source == NULL)
	{
		m_mp3Info.duration = 0;
		return E_FAIL;
	}

	mp3_init(&m_mp3Info);

	fseek(source, 0, SEEK_SET);
	for(;;)
	{
		bytes = fread(buffer, 1, 4096, source);
		if (bytes <= 0)
		{
			break;
		}
		mp3_update(&m_mp3Info, buffer, bytes);

		// for reading only first frame header.
		if (m_mp3Info.frames > 0)
			break;
		pos += bytes;
	}

	mp3_final(&m_mp3Info);

	fseek(source, 0, SEEK_END);
	long lAudioSize = ftell(source) - pos;
	
	if (m_mp3Info.bitrate == 0)
		m_mp3Info.duration = 0;
	else
		m_mp3Info.duration = lAudioSize / m_mp3Info.bitrate * 8;

	fclose(source);

	return S_OK;
}