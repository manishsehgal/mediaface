#ifndef __ID3V1TAG_H_
#define __ID3V1TAG_H_

#include "tag.h"
#include "ID3Reader.h"

struct ID3v1Tag
{
	TAG ID;
	CComBSTR bstrTextID;
	CComBSTR bstrDescription;
	CComBSTR bstrContent;
};


class CID3v1Tag : public CID3Tag
{
public:
	CID3v1Tag()
	{
	}

public:
	HRESULT get_ID(TAG * pVal);
	HRESULT get_TextID(BSTR * pVal);
	HRESULT get_Description(BSTR * pVal);
	HRESULT get_Content(BSTR * pVal);

public:
	void Init(ID3v1Tag* pTag);

protected:
	ID3v1Tag* m_pTag;
};

#endif //__ID3V1TAG_H_
