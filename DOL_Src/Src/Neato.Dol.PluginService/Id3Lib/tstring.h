#ifndef __TSTRING_H__
#define __TSTRING_H__

#pragma warning(disable:4786) 

#include <string>
#include <tchar.h>

typedef std::basic_string<TCHAR> tstring;
typedef tstring _tstring;

using namespace std;


#endif // __TSTRING_H__