#include "stdafx.h"

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#if defined HAVE_ICONV_H
# include <iconv.h>
# include <errno.h>
#endif

#include <ctype.h>
#include <iostream>
#include "utils.h"

#ifdef macintosh
#define NOCREATE ((std::ios_base::openmode)0)
#define toascii(X) (X)
#else
#define NOCREATE 0
//ios::nocreate
#endif


using namespace dami;

size_t dami::renderNumber(uchar *buffer, uint32 val, size_t size)
{
  uint32 num = val;
  for (size_t i = 0; i < size; i++)
  {
    buffer[size - i - 1] = (uchar)(num & MASK8);
    num >>= 8;
  }
  return size;
}

String dami::renderNumber(uint32 val, size_t size)
{
  String str(size, '\0');
  uint32 num = val;
  for (size_t i = 0; i < size; i++)
  {
    str[size - i - 1] = (uchar)(num & MASK8);
    num >>= 8;
  }
  return str;
}


namespace 
{
#if !defined(HAVE_ICONV_H)
  // converts an ASCII string into a Unicode one
  String mbstoucs(String data)
  {
    size_t size = data.size();
    String unicode(size * 2, '\0');
    for (index_t i = 0; i < size; i++)
    {
      unicode[i*2+1] = toascii(data[i]);
    }
    return unicode;
  }

  // converts a Unicode string into ASCII
  
  String ucstombs(String data)
  {
    size_t size = data.size() / 2;
    String ascii(size, '\0');
    for (index_t i = 0; i < size; i++)
    {
      ascii[i] = toascii(data[i*2+1]);
    }
    return ascii;
  }
#else

  String convert_i(iconv_t cd, String source)
  {
    String target;
    size_t source_size = source.size();
//    const char* source_str = source.data();
    char * source_str = new char[source.length()+1]; 
    source.copy(source_str, string::npos); 
    source_str[source.length()] = 0; 

#define BUFSIZ 1024
    char buf[BUFSIZ];
    char* target_str = buf;
    size_t target_size = BUFSIZ;
    
    do
    {
      size_t nconv = iconv(cd, 
                           &source_str, &source_size, 
                           &target_str, &target_size);
      if (nconv == (size_t) -1 && errno != EINVAL && errno != E2BIG)
      {
        return target;
      }
      target.append(buf, BUFSIZ - target_size);
      target_str = buf;
      target_size = BUFSIZ;
    }
    while (source_size > 0);
    return target;
  }

  const char* getFormat(ID3_TextEnc enc)
  {
    const char* format = NULL;
    switch (enc)
    {
      case ID3TE_ASCII:
        format = ID3_ICONV_FORMAT_ASCII;
        break;

      case ID3TE_UTF16:
        format = ID3_ICONV_FORMAT_UTF16;
        break;
        
      case ID3TE_UTF16BE:
        format = ID3_ICONV_FORMAT_UTF16BE;
        break;
        
      case ID3TE_UTF8:
        format = ID3_ICONV_FORMAT_UTF8;
        break;
        
      default:
        break;
    }
    return format;
  }
#endif
}

String dami::convert(String data, ID3_TextEnc sourceEnc, ID3_TextEnc targetEnc)
{
  String target;
#if !defined HAVE_ICONV_H
#define ID3_IS_ASCII(enc)      \
  ((enc) == ID3TE_ASCII     || \
   (enc) == ID3TE_ISO8859_1 || \
   (enc) == ID3TE_UTF8)
#define ID3_IS_UNICODE(enc)    \
  ((enc) == ID3TE_UNICODE   || \
   (enc) == ID3TE_UTF16     || \
   (enc) == ID3TE_UTF16BE)
  if (ID3_IS_ASCII(sourceEnc) && ID3_IS_UNICODE(targetEnc))
  {
    target = mbstoucs(data);
  }
  else if (ID3_IS_UNICODE(sourceEnc) && ID3_IS_ASCII(targetEnc))
  {
    target = ucstombs(data);
  }
#else
  if (sourceEnc != targetEnc)
  {
    const char* targetFormat = getFormat(targetEnc);
    const char* sourceFormat = getFormat(sourceEnc);
    
    iconv_t cd = iconv_open (targetFormat, sourceFormat);
    if (cd != (iconv_t) -1)
    {
      target = convert_i(cd, data);
    }
    iconv_close (cd);
  }
#endif
  return target;
}

size_t dami::ucslen(const unicode_t *unicode)
{
  if (NULL != unicode)
  {
    for (size_t size = 0; true; size++)
    {
      if (NULL_UNICODE == unicode[size])
      {
        return size;
      }
    }
  }
  return 0;
}

namespace
{
  bool exists(String name)
  {
    ifstream file(name.c_str(), NOCREATE);
    return file.is_open() != 0;
  }
};

ID3_Err dami::createFile(String name, fstream& file)
{
  if (file.is_open())
  {
    file.close();
  }
    
  file.open(name.c_str(), ios::out | ios::binary | ios::trunc);
  if (!file)
  {
    return ID3E_ReadOnly;
  }
    
  return ID3E_NoError;
}

size_t dami::getFileSize(fstream& file)
{
  size_t size = 0;
  if (file.is_open())
  {
    streamoff curpos = file.tellg();
    file.seekg(0, ios::end);
    size = file.tellg();
    file.seekg(curpos);
  }
  return size;
}

size_t dami::getFileSize(ifstream& file)
{
  size_t size = 0;
  if (file.is_open())
  {
    streamoff curpos = file.tellg();
    file.seekg(0, ios::end);
    size = file.tellg();
    file.seekg(curpos);
  }
  return size;
}

size_t dami::getFileSize(ofstream& file)
{
  size_t size = 0;
  if (file.is_open())
  {
    streamoff curpos = file.tellp();
    file.seekp(0, ios::end);
    size = file.tellp();
    file.seekp(curpos);
  }
  return size;
}

ID3_Err dami::openWritableFile(String name, fstream& file)
{
  if (!exists(name))
  {
    return ID3E_NoFile;
  }
    
  if (file.is_open())
  {
    file.close();
  }
  file.open(name.c_str(), ios::in | ios::out | ios::binary | NOCREATE);
  if (!file)
  {
    return ID3E_ReadOnly;
  }
    
  return ID3E_NoError;
}

ID3_Err dami::openWritableFile(String name, ofstream& file)
{
  if (!exists(name))
  {
    return ID3E_NoFile;
  }
    
  if (file.is_open())
  {
    file.close();
  }
  file.open(name.c_str(), ios::in | ios::out | ios::binary | NOCREATE);
  if (!file)
  {
    return ID3E_ReadOnly;
  }
    
  return ID3E_NoError;
}

ID3_Err dami::openReadableFile(String name, fstream& file)
{
  if (file.is_open())
  {
    file.close();
  }
  file.open(name.c_str(), ios::in | ios::binary | NOCREATE);
  if (!file)
  {
    return ID3E_NoFile;
  }
    
  return ID3E_NoError;
}

ID3_Err dami::openReadableFile(String name, ifstream& file)
{
  if (file.is_open())
  {
    file.close();
  }
  file.open(name.c_str(), ios::in | ios::binary | NOCREATE);
  if (!file)
  {
    return ID3E_NoFile;
  }
    
  return ID3E_NoError;
}

String dami::toString(uint32 val)
{
  if (val == 0)
  {
    return "0";
  }
  String text;
  while (val > 0)
  {
    String tmp;
    char ch = (val % 10) + '0';
    tmp += ch;
    text = tmp + text;
    val /= 10;
  }
  return text;
}

WString dami::toWString(const unicode_t buf[], size_t len)
{
  WString str;
  str.reserve(len);

  for (size_t i = 0; i < len; ++i)
  {
    str += static_cast<WString::value_type>(buf[i]);
  }
  return str;
}
