#include "stdafx.h"

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>

#include "readers.h"
#include "utils.h"

using namespace dami;

ID3_Reader::size_type
ID3_MemoryReader::readChars(char_type buf[], size_type len)
{
  size_type size = min(len, (size_type)(_end - _cur));
  ::memcpy(buf, _cur, size);
  _cur += size;
  return size;
}
