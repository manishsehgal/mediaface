#include "stdafx.h"
#include "CommandManager.h"
#include <XmlHelpers.h>
#include <TxtCnv.h>
#define USING_ZIP_LIB
#include "Zip\Zip.h"
#include <map>
#include "resource.h"

#define MODULES_FOLDER _T("Modules")

CCommandManager::CCommandManager(HWND hWnd)
{
    m_hWnd = hWnd;
    m_bIsMsXml = false;
    CheckMsXml();
    if (m_bIsMsXml)
    {
        ::PostMessage(m_hWnd, WM_USER_MSXML, TRUE, TRUE);
    }

	TCHAR fn[MAX_PATH+1]={0};
	::GetModuleFileName(0, fn, MAX_PATH);
    _tcscat(fn, _T(".loader"));
    ::DeleteFile(fn);
}

CCommandManager::~CCommandManager()
{
}

//Fills m_vModules with modules and commands info
bool CCommandManager::InitModules()
{
    m_vModules.clear();

    if (!m_bIsMsXml)
    {
        ::PostMessage(m_hWnd, WM_USER_MSXML, FALSE, TRUE);
        return true;
    }

    AddSelfModule();

    TCHAR buf[MAX_PATH] = {0};
    ::GetModuleFileName(NULL, buf, MAX_PATH);
    tstring strPath = buf;
    int nPos = strPath.rfind(_T('\\'));
    strPath = strPath.substr(0, nPos+1);
    strPath += MODULES_FOLDER;
    strPath += _T("\\");
    tstring strFilter = strPath + _T("*.dll");

	WIN32_FIND_DATA fd;
    HANDLE hFind = ::FindFirstFile(strFilter.c_str(), &fd);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		while (TRUE)
		{
            if (fd.cFileName[0] != _T('.'))
            {
			    tstring strFile = strPath;
			    strFile += fd.cFileName;
                AddModule(strFile);
            }
            if (!::FindNextFile(hFind, &fd)) break;
		}
	}
    return (m_vModules.size() > 0);
}

//Process one module info
void CCommandManager::AddModule(tstring strFileName)
{
    HMODULE hMod = ::LoadLibrary(strFileName.c_str());
    if (hMod != NULL)
    {
        typedef UINT (__stdcall * LPFUNC)(BSTR*);
        LPFUNC pGetInfoFunc = 0;
        pGetInfoFunc = (LPFUNC)::GetProcAddress(hMod, _T("GetInfo"));
        if (pGetInfoFunc != NULL)
        {
            CComBSTR bstrInfo;
            BOOL bRes = pGetInfoFunc(&bstrInfo);
            if (bRes)
            {
                CModule module;
                module.m_strFileName = strFileName;

                ParseModuleInfo(bstrInfo, module);

                if (module.m_vCommands.size() > 0)
                {
                    m_vModules.push_back(module);
                }
            }
        }
        ::FreeLibrary(hMod);
    }
}

//Parse module's xml info
void CCommandManager::ParseModuleInfo(BSTR bstrInfo, CModule & module)
{
    try
    {
	CComPtr<MSXML2::IXMLDOMElement> pModuleInfoElem;
    HRESULT hRes = CXmlHelpers::LoadXml(bstrInfo, &pModuleInfoElem);
	if (hRes != S_OK) throw 1;

    CComBSTR bstrName;
    hRes = CXmlHelpers::GetAttribute(pModuleInfoElem, L"Name", &bstrName);
    module.m_strName = bstrName;

    CComBSTR bstrVersion;
    hRes = CXmlHelpers::GetAttribute(pModuleInfoElem, L"Version", &bstrVersion);
    module.m_strVersion = bstrVersion;

	CComPtr<MSXML2::IXMLDOMElement> pCommandsElem;
    hRes = CXmlHelpers::GetChildElement(pModuleInfoElem, L"Commands", &pCommandsElem);
	if (hRes == S_OK)
    {
    	CComPtr<MSXML2::IXMLDOMElement> pCommandElem;
        hRes = CXmlHelpers::GetChildElement(pCommandsElem, L"Command", &pCommandElem);
	    if (hRes == S_OK)
        {
            while (true)
            {
                CCommand cmd;

                CComBSTR bstrCmdName;
                hRes = CXmlHelpers::GetAttribute(pCommandElem, L"Name", &bstrCmdName);
                if (hRes == S_OK)
                {
                    cmd.m_strName = bstrCmdName;
                }

                CComBSTR bstrCopyRq;
                cmd.m_bCopyRequest = true;
                hRes = CXmlHelpers::GetAttribute(pCommandElem, L"CopyRequest", &bstrCopyRq);
                if (hRes == S_OK)
                {
                    cmd.m_bCopyRequest = wcscmp(bstrCopyRq, L"0") != 0;
                }

                cmd.m_bSelf = false;

                module.m_vCommands.push_back(cmd);

    	        CComPtr<MSXML2::IXMLDOMElement> pCommandElemNext;
                hRes = CXmlHelpers::GetNextSiblingElement(pCommandElem, &pCommandElemNext);
    	        if (!pCommandElemNext) break;

                pCommandElem = pCommandElemNext;
            }
        }
    }
    }
    catch(int nErr)
    {
        TCHAR buf[128];
        sprintf(buf, "ParseModuleInfo() error %d", nErr);
        Trace(buf);
    }
    catch(...){}
}

void CCommandManager::AddSelfModule()
{
    CModule module;
    module.m_strName     =   L"CORE";
    module.m_strFileName = _T("dolcore.exe");
    module.m_strVersion  =   L"1.0.0.2";
    {
        CCommand cmd;
        cmd.m_strName = L"CORE_IS_COMMAND_AVAILABLE";
        cmd.m_bCopyRequest = true;
        cmd.m_bSelf = true;
        module.m_vCommands.push_back(cmd);
    }
    {
        CCommand cmd;
        cmd.m_strName = L"CORE_GET_UPDATE_INFO";
        cmd.m_bCopyRequest = false;
        cmd.m_bSelf = true;
        module.m_vCommands.push_back(cmd);
    }
    {
        CCommand cmd;
        cmd.m_strName = L"CORE_UPDATE_FILES";
        cmd.m_bCopyRequest = false;
        cmd.m_bSelf = true;
        module.m_vCommands.push_back(cmd);
    }
    {
        CCommand cmd;
		cmd.m_strName = L"CORE_IS_MODULE_AVAILABLE";
        cmd.m_bCopyRequest = false;
        cmd.m_bSelf = true;
        module.m_vCommands.push_back(cmd);
    }
    m_vModules.push_back(module);
}

CModule * CCommandManager::FindModule(LPCWSTR wcName)
{
    for (UINT m=0; m<m_vModules.size(); m++)
    {
        CModule & mod = m_vModules[m];
        if (wcscmp(mod.m_strName.c_str(), wcName) == 0) return &mod;
    }
    return 0;
}

bool CCommandManager::CheckMsXml()
{
    if (m_bIsMsXml) return true;
    CComPtr<MSXML2::IXMLDOMDocument> pDoc;
    pDoc.CoCreateInstance(L"MSXML2.DOMDocument");
    if (pDoc)
    {
        m_bIsMsXml = true;
    }
    return m_bIsMsXml;
}



// ICommandManager
void CCommandManager::Trace(TCHAR * pText)
{
    if (!m_hWnd) return;
    int nLen = _tcslen(pText);
    TCHAR * buf = new TCHAR[nLen+1];
    _tcscpy(buf, pText);
    ::PostMessage(m_hWnd, WM_USER_TRACE, 0, (LPARAM)buf);
}

void CCommandManager::Terminate()
{
    if (!m_hWnd) return;
    ::PostMessage(m_hWnd, WM_USER_TERMINATE, 0, 0);
}


#define ERR_NonIntException	1000

#define ERR_REQ_BADXML	1
#define ERR_REQ_NO_TYPE	2
#define ERR_REQ_NO_REQ	3
#define ERR_REQ_BADREQ	4
#define ERR_REQ_NO_CMD	5
#define ERR_REQ_NO_PAR	6
#define ERR_INVOKE_CMD	8
#define ERR_RESP_EMPTY	9
#define ERR_RESP_BADXML	10
#define ERR_RESP_FAIL_SET_REQ	11
#define ERR_RESP_FAIL_SET_TYPE	12
#define ERR_RESP_FAIL_SET_NAME	13
#define ERR_RESP_FAIL_XML2STR	14

#define ERR_REQ_InvokeCommand_UNKNOWN_CMD	100
#define ERR_REQ_InvokeCommand_FAIL_LoadMOD	101
#define ERR_REQ_InvokeCommand_MOD_NO_INVOKE	102
#define ERR_REQ_InvokeCommand_MOD_FAIL_Init	103
#define ERR_REQ_InvokeCommand_MOD_FAIL_Init2	104
#define ERR_REQ_InvokeCommand_MOD_FAIL_Invoke	105

#define ERR_REQ_InvokeSelfCommand_UNKNOWN_CMD	109

#define ERR_REQ_IsCommandAvailable_NO_CMD		110
#define ERR_REQ_IsCommandAvailable_NO_CMDNAME	111
#define ERR_RESP_IsCommandAvailable_FAIL_RESP	112
#define ERR_RESP_IsCommandAvailable_FAIL_ERR	113
#define ERR_RESP_IsCommandAvailable_FAIL_PARAM	114
#define ERR_RESP_IsCommandAvailable_FAIL_STAT	115
#define ERR_RESP_IsCommandAvailable_FAIL_STAT2	116
#define ERR_RESP_IsCommandAvailable_FAIL_XML2STR 119

#define ERR_REQ_IsModuleAvailable_NO_MOD		120
#define ERR_REQ_IsModuleAvailable_NO_MODNAME	121
#define ERR_RESP_IsModuleAvailable_FAIL_RESP	122
#define ERR_RESP_IsModuleAvailable_FAIL_ERR		123
#define ERR_RESP_IsModuleAvailable_FAIL_PARAM	124
#define ERR_RESP_IsModuleAvailable_FAIL_STAT	125
#define ERR_RESP_IsModuleAvailable_FAIL_STAT2	126
#define ERR_RESP_IsModuleAvailable_FAIL_STAT_NAME 127
#define ERR_RESP_IsModuleAvailable_FAIL_STAT_VER 128
#define ERR_RESP_IsModuleAvailable_FAIL_XML2STR 129

#define ERR_REQ_GetUpdateInfo_NO_MOD		130
#define ERR_REQ_GetUpdateInfo_NO_MOD2		131
#define ERR_REQ_GetUpdateInfo_NO_MOD3		132
#define ERR_RESP_GetUpdateInfo_FAIL_RESP	133
#define ERR_RESP_GetUpdateInfo_FAIL_ERR		134
#define ERR_RESP_GetUpdateInfo_FAIL_PARAM	135
#define ERR_RESP_GetUpdateInfo_FAIL_XML2STR 139

#define ERR_REQ_UpdateFiles_NO_FILES		140
#define ERR_REQ_UpdateFiles_NO_FILE_NAME	141
#define ERR_REQ_UpdateFiles_NO_FILE_BODY	142
#define ERR_RESP_UpdateFiles_FAIL_RESP		143
#define ERR_RESP_UpdateFiles_FAIL_ERR		144
#define ERR_RESP_UpdateFiles_FAIL_ERR_DESC	145
#define ERR_RESP_UpdateFiles_FAIL_PARAM		146
#define ERR_RESP_UpdateFiles_FAIL_XML2STR	149


bool CCommandManager::ProcessRequest(std::wstring strCmd, std::wstring & strData_Result)
{
    const WCHAR wcErrResFormat[] = L"<CmdInfo Type=\"Response\" Name=\"%s\">%s<Response Error=\"1\" ErrorDesc=\"%S%S\"/></CmdInfo>";
	char* pErrorDesc = "General Command Error.";

    if (!m_bIsMsXml)
    {
        if (!CheckMsXml())
        {
            ::PostMessage(m_hWnd, WM_USER_MSXML, FALSE, FALSE);
            strData_Result.resize(512);
            swprintf((WCHAR*)strData_Result.c_str(), wcErrResFormat, L"", L"", "MS XML 3 Library is absent.","");
            return true;
        }
        else
        {
            InitModules();
            ::PostMessage(m_hWnd, WM_USER_MSXML, TRUE, FALSE);
        }
    }

    CComBSTR bstrRequestText;
    CComBSTR bstrCmdName = L"";

    bool bCopyRequest = true;
	int nRes = 0;

    try
    {
		//"Debug" for Release configuration
		/*FILE *stream;
 		if( (stream = fopen( "C:\\data2.xml", "w" )) != NULL )
 		{
			fwprintf(stream, L"%ls\n==== data ===%ls", strCmd.c_str(),strData_Result.c_str());
 			fclose(stream);
 		}*/

        CComPtr<MSXML2::IXMLDOMElement>  pCmdInfoElem;
        HRESULT hRes = CXmlHelpers::LoadXml(CComBSTR(strCmd.c_str()), &pCmdInfoElem);
        if (hRes != S_OK) throw ERR_REQ_BADXML;

        CComBSTR bstrCmdType;
        hRes = CXmlHelpers::GetAttribute(pCmdInfoElem, L"Type", &bstrCmdType);
        if (wcsicmp(bstrCmdType, L"Request") != 0) throw ERR_REQ_NO_TYPE;

        CComPtr<MSXML2::IXMLDOMElement> pRequestElem;
        hRes = CXmlHelpers::GetChildElement(pCmdInfoElem, L"Request", &pRequestElem);
        if (hRes != S_OK) throw ERR_REQ_NO_REQ;

        hRes = pRequestElem->get_xml(&bstrRequestText);
        if (hRes != S_OK) throw ERR_REQ_BADREQ;
        
        hRes = CXmlHelpers::GetAttribute(pRequestElem, L"Name", &bstrCmdName);
        if (hRes != S_OK) throw ERR_REQ_NO_CMD;

		CComBSTR bstrData_Result(strData_Result.length(),strData_Result.c_str());
        CComBSTR bstrErrorDesc;

        nRes = InvokeCommand(bstrCmdName, bstrRequestText, &bstrData_Result, 
									&bstrErrorDesc, bCopyRequest);
        //BOOL bRes = InvokeCommand(bstrCmdName, bstrRequestText, &bstrResult, bCopyRequest);
        if (nRes != 0) throw ERR_INVOKE_CMD;
        if (bstrData_Result.Length() == 0) throw ERR_RESP_EMPTY;

        if (!bCopyRequest)
        {
            hRes = pCmdInfoElem->removeChild(pRequestElem, 0);
        }

        CComPtr<MSXML2::IXMLDOMElement> pResponseElem;
        hRes = CXmlHelpers::LoadXml(bstrData_Result, &pResponseElem);
        if (hRes != S_OK) throw ERR_RESP_BADXML;

		CComPtr<MSXML2::IXMLDOMNode> pNewNode;
        hRes = pCmdInfoElem->appendChild(pResponseElem, &pNewNode);
        if (hRes != S_OK) throw ERR_RESP_FAIL_SET_REQ;

        hRes = CXmlHelpers::SetAttribute(pCmdInfoElem, L"Type", L"Response");
        if (hRes != S_OK) throw ERR_RESP_FAIL_SET_TYPE;

        hRes = CXmlHelpers::SetAttribute(pCmdInfoElem, L"Name", bstrCmdName);
        if (hRes != S_OK) throw ERR_RESP_FAIL_SET_NAME;

        CComBSTR bstrResultXml;
        hRes = pCmdInfoElem->get_xml(&bstrResultXml);
        if (hRes != S_OK) throw ERR_RESP_FAIL_XML2STR;

        strData_Result = bstrResultXml;
		return true;

	}
    catch (int nErr)
    {
		switch(nErr) {
			case ERR_REQ_BADXML:
				pErrorDesc = "Wrong format of request (bad XML).";
				bCopyRequest = false;
				break;
			case ERR_REQ_NO_TYPE:
				pErrorDesc = "Wrong format of request (no 'Type' attribute).";
				break;
			case ERR_REQ_NO_REQ:
				pErrorDesc = "Wrong format of request (no 'Request' tag).";
				break;
			case ERR_REQ_BADREQ:
				pErrorDesc = "Wrong format of request (bad XML inside 'Request' tag).";
				break;
			case ERR_REQ_NO_CMD:
				pErrorDesc = "Wrong format of request (no command nName).";
				break;
			case ERR_INVOKE_CMD:
				pErrorDesc = "Unspecified error in InvokeCommand().";
				switch(nRes) {
					case ERR_REQ_InvokeCommand_UNKNOWN_CMD:
						pErrorDesc = "Unknown command in request.";
						break;
					case ERR_REQ_InvokeCommand_FAIL_LoadMOD:
						pErrorDesc = "Failure loading plugin.";
						break;
					case ERR_REQ_InvokeCommand_MOD_NO_INVOKE:
						pErrorDesc = "No 'Invoke' handler plugin.";
						break;
					case ERR_REQ_InvokeCommand_MOD_FAIL_Init:
						pErrorDesc = "Failure initializing plugin.";
						break;
					case ERR_REQ_InvokeCommand_MOD_FAIL_Init2:
						pErrorDesc = "Failure uninitializing plugin.";
						break;
					//case ERR_REQ_InvokeCommand_MOD_FAIL_Invoke: = default

					case ERR_REQ_InvokeSelfCommand_UNKNOWN_CMD:
						pErrorDesc = "Unsupported internal command.";
						break;
					case ERR_REQ_IsCommandAvailable_NO_CMD:
					case ERR_REQ_IsCommandAvailable_NO_CMDNAME:
						pErrorDesc = "IsCommandAvailable(): bad request format.";
						break;
					case ERR_RESP_IsCommandAvailable_FAIL_RESP:
					case ERR_RESP_IsCommandAvailable_FAIL_ERR:
					case ERR_RESP_IsCommandAvailable_FAIL_PARAM:
					case ERR_RESP_IsCommandAvailable_FAIL_STAT:
					case ERR_RESP_IsCommandAvailable_FAIL_STAT2:
					case ERR_RESP_IsCommandAvailable_FAIL_XML2STR:
						pErrorDesc = "IsCommandAvailable(): Failure creating response XML.";
						break;

					case ERR_REQ_IsModuleAvailable_NO_MOD:
					case ERR_REQ_IsModuleAvailable_NO_MODNAME:
						pErrorDesc = "IsModuleAvailable(): bad request format.";
						break;
					case ERR_RESP_IsModuleAvailable_FAIL_RESP:
					case ERR_RESP_IsModuleAvailable_FAIL_ERR:
					case ERR_RESP_IsModuleAvailable_FAIL_PARAM:
					case ERR_RESP_IsModuleAvailable_FAIL_STAT:
					case ERR_RESP_IsModuleAvailable_FAIL_STAT2:
					case ERR_RESP_IsModuleAvailable_FAIL_STAT_NAME:
					case ERR_RESP_IsModuleAvailable_FAIL_STAT_VER:
					case ERR_RESP_IsModuleAvailable_FAIL_XML2STR:
						pErrorDesc = "IsModuleAvailable(): Failure creating response XML.";
						break;

					case ERR_REQ_GetUpdateInfo_NO_MOD:
					case ERR_REQ_GetUpdateInfo_NO_MOD2:
					case ERR_REQ_GetUpdateInfo_NO_MOD3:
						pErrorDesc = "GetUpdateInfo(): bad request format.";
						break;
					case ERR_RESP_GetUpdateInfo_FAIL_RESP:
					case ERR_RESP_GetUpdateInfo_FAIL_ERR:
					case ERR_RESP_GetUpdateInfo_FAIL_PARAM:
					case ERR_RESP_GetUpdateInfo_FAIL_XML2STR:
						pErrorDesc = "GetUpdateInfo(): Failure creating response XML.";
						break;
					case ERR_REQ_UpdateFiles_NO_FILES:
					case ERR_REQ_UpdateFiles_NO_FILE_NAME:
					case ERR_REQ_UpdateFiles_NO_FILE_BODY:
						pErrorDesc = "UpdateFiles(): bad request format.";
						break;
					case ERR_RESP_UpdateFiles_FAIL_RESP:
					case ERR_RESP_UpdateFiles_FAIL_ERR:
					case ERR_RESP_UpdateFiles_FAIL_ERR_DESC:
					case ERR_RESP_UpdateFiles_FAIL_PARAM:
					case ERR_RESP_UpdateFiles_FAIL_XML2STR:
						pErrorDesc = "UpdateFiles(): Failure creating response XML.";
						break;

				}
				break;
			case ERR_RESP_EMPTY:
			case ERR_RESP_BADXML:
				pErrorDesc = "Invalid XML in Plugin Service response.";
				break;
			case ERR_RESP_FAIL_SET_REQ:
			case ERR_RESP_FAIL_SET_TYPE:
			case ERR_RESP_FAIL_SET_NAME:
			case ERR_RESP_FAIL_XML2STR:
				pErrorDesc = "Failure creating response XML.";
				break;
		}

        TCHAR buf[128];
		sprintf(buf, "ProcessRequest() error %d: %s", nErr, pErrorDesc);
        Trace(buf);

    }
    catch (...){}

    if (!bstrRequestText) bstrRequestText = L"";
    strData_Result.resize(wcslen(bstrRequestText)+512);
    swprintf((WCHAR*)strData_Result.c_str(), wcErrResFormat, bstrCmdName, 
		bCopyRequest ? bstrRequestText : L"", "General error in Plugin Service:\n",pErrorDesc);

    return true;
}

bool CCommandManager::FindCommand(BSTR bstrCmdName, UINT & nModInd, UINT & nCmdInd)
{
    for (UINT m=0; m<m_vModules.size(); m++)
    {
        CModule & mod = m_vModules[m];
        for (UINT c=0; c<mod.m_vCommands.size(); c++)
        {
            CCommand & cmd = mod.m_vCommands[c];
            if (wcscmp(bstrCmdName, cmd.m_strName.c_str()) == 0)
            {
                nModInd = m;
                nCmdInd = c;
                return true;
            }
        }
    }
    return false;
}

//BOOL CCommandManager::InvokeCommand(BSTR bstrCmdName, BSTR bstrCmdText, BSTR * pbstrRes, bool & bCopyRequest)
int CCommandManager::InvokeCommand(BSTR bstrCmdName, BSTR bstrCmdText,
									BSTR * pbstrData_Res, BSTR * pbstrErrorDesc, bool & bCopyRequest)
{
    try
    {
        UINT nModInd = -1;
        UINT nCmdInd = -1;
        if (!FindCommand(bstrCmdName, nModInd, nCmdInd)) throw ERR_REQ_InvokeCommand_UNKNOWN_CMD;

        CModule  & mod = m_vModules[nModInd];
        CCommand & cmd = mod.m_vCommands[nCmdInd];

        bCopyRequest = cmd.m_bCopyRequest;

        if (cmd.m_bSelf)
        {
			SysFreeString(*pbstrData_Res);
            return InvokeSelfCommand(bstrCmdName, bstrCmdText, pbstrData_Res);
        }

        HMODULE hMod = ::LoadLibrary(mod.m_strFileName.c_str());
        if (hMod == NULL) throw ERR_REQ_InvokeCommand_FAIL_LoadMOD;

        typedef UINT (__stdcall * LPINITFUNC)(BOOL);
        LPINITFUNC pInitFunc = 0;
        pInitFunc = (LPINITFUNC)::GetProcAddress(hMod, _T("Init"));

        typedef UINT (__stdcall * LPINVOKEFUNC)(BSTR,BSTR*);
        LPINVOKEFUNC pInvokeFunc = 0;
        pInvokeFunc = (LPINVOKEFUNC)::GetProcAddress(hMod, _T("Invoke"));
        if (pInvokeFunc == NULL) {::FreeLibrary(hMod); throw 3;}

		try {
			if (pInitFunc) pInitFunc(TRUE);
		} 
		catch(...) {
			throw ERR_REQ_InvokeCommand_MOD_FAIL_Init;
		}
		BOOL bRes = FALSE;
		try {
			//CComBSTR bstrResult;
			bRes = pInvokeFunc(bstrCmdText, pbstrData_Res);
		}
		catch(...) {
			throw ERR_REQ_InvokeCommand_MOD_FAIL_Invoke;
		}
		try {
			if (pInitFunc) pInitFunc(FALSE);
		}
		catch(...) {
			throw ERR_REQ_InvokeCommand_MOD_FAIL_Init2;
		}

        ::FreeLibrary(hMod);

        if (!bRes) 
			return ERR_REQ_InvokeCommand_MOD_FAIL_Invoke;

//        *pbstrRes = bstrResult.Detach();
        return 0;
    }
    catch (int nErr)
    {
        TCHAR buf[128];
        sprintf(buf, "InvokeCommand() error %d", nErr);
        Trace(buf);
		return nErr;
    }
    catch(...){}
    return ERR_NonIntException;
}

int CCommandManager::InvokeSelfCommand(BSTR bstrCmdName, BSTR bstrCmdText, BSTR * pbstrRes)
{
    if (wcscmp(bstrCmdName, L"CORE_IS_COMMAND_AVAILABLE") == 0)
    {
        return Command_IsCommandAvailable(bstrCmdText, pbstrRes);
    }
    else if (wcscmp(bstrCmdName, L"CORE_GET_UPDATE_INFO") == 0)
    {
        return Command_GetUpdateInfo(bstrCmdText, pbstrRes);
    }
    else if (wcscmp(bstrCmdName, L"CORE_UPDATE_FILES") == 0)
    {
        return Command_UpdateFiles(bstrCmdText, pbstrRes);
    }
	else if (wcscmp(bstrCmdName, L"CORE_IS_MODULE_AVAILABLE") == 0)
	{
		return Command_IsModuleAvailable(bstrCmdText, pbstrRes);
	}
    return ERR_REQ_InvokeSelfCommand_UNKNOWN_CMD;
}

int CCommandManager::Command_IsCommandAvailable(BSTR bstrCmdText, BSTR * pbstrRes)
{
    try
    {
		//=============================
		//Parse request xml
		CComPtr<MSXML2::IXMLDOMElement>  pReqElem;
		HRESULT hRes = CXmlHelpers::LoadXml(bstrCmdText, &pReqElem);
		if (hRes != S_OK) throw ERR_REQ_BADREQ;

        CComPtr<MSXML2::IXMLDOMElement> pParamsElem;
        hRes = CXmlHelpers::GetChildElement(pReqElem, L"Params", &pParamsElem);
        if (hRes != S_OK) throw ERR_REQ_NO_PAR;

        CComPtr<MSXML2::IXMLDOMElement> pCmdElem;
        hRes = CXmlHelpers::GetChildElement(pParamsElem, L"Command", &pCmdElem);
        if (hRes != S_OK) throw ERR_REQ_IsCommandAvailable_NO_CMD;

        CComBSTR bstrCmdName;
        hRes = CXmlHelpers::GetAttribute(pCmdElem, L"Name", &bstrCmdName);
        if (hRes != S_OK) throw ERR_REQ_IsCommandAvailable_NO_CMDNAME;

		UINT nModInd = -1;
		UINT nCmdInd = -1;
		bool bIsCmd = FindCommand(bstrCmdName, nModInd, nCmdInd);

		//=============================
		// Prepare response xml
		CComPtr<MSXML2::IXMLDOMElement>  pRespElem;
		hRes = CXmlHelpers::CreateDocument(CComBSTR(L"Response"), &pRespElem);
        if (hRes != S_OK) throw ERR_RESP_IsCommandAvailable_FAIL_RESP;

        hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"0");
        if (hRes != S_OK) throw ERR_RESP_IsCommandAvailable_FAIL_ERR;

        CComPtr<MSXML2::IXMLDOMElement> pRParamsElem;
		hRes = CXmlHelpers::AddElement(pRespElem, CComBSTR(L"Params"), &pRParamsElem);
        if (hRes != S_OK) throw ERR_RESP_IsCommandAvailable_FAIL_PARAM;

        CComPtr<MSXML2::IXMLDOMElement> pStatusElem;
		hRes = CXmlHelpers::AddElement(pRParamsElem, CComBSTR(L"Status"), &pStatusElem);
        if (hRes != S_OK) throw ERR_RESP_IsCommandAvailable_FAIL_STAT;

        hRes = CXmlHelpers::SetAttribute(pStatusElem, L"status", bIsCmd ? L"Ok" : L"NoSuchCommand");
		if (hRes != S_OK) throw ERR_RESP_IsCommandAvailable_FAIL_STAT2;

		hRes = pRespElem->get_xml(pbstrRes);
		if (hRes != S_OK) throw ERR_RESP_IsCommandAvailable_FAIL_XML2STR;

        return 0;
    }
    catch (int nErr)
    {
        TCHAR buf[128];
        sprintf(buf, "Command_IsCommandAvailable() error %d", nErr);
        Trace(buf);
		return nErr;
    }
    catch(...){}
    return ERR_NonIntException;
}

int CCommandManager::Command_IsModuleAvailable(BSTR bstrCmdText, BSTR * pbstrRes)
{
    try
    {
		//=============================
		//Parse request xml
		CComPtr<MSXML2::IXMLDOMElement>  pReqElem;
		HRESULT hRes = CXmlHelpers::LoadXml(bstrCmdText, &pReqElem);
		if (hRes != S_OK) throw ERR_REQ_BADREQ;

        CComPtr<MSXML2::IXMLDOMElement> pParamsElem;
        hRes = CXmlHelpers::GetChildElement(pReqElem, L"Params", &pParamsElem);
        if (hRes != S_OK) throw ERR_REQ_NO_PAR;

        CComPtr<MSXML2::IXMLDOMElement> pModElem;
        hRes = CXmlHelpers::GetChildElement(pParamsElem, L"Module", &pModElem);
        if (hRes != S_OK) throw ERR_REQ_IsModuleAvailable_NO_MOD;

        CComBSTR bstrModName;
        hRes = CXmlHelpers::GetAttribute(pModElem, L"Name", &bstrModName);
        if (hRes != S_OK) throw ERR_REQ_IsModuleAvailable_NO_MODNAME;

        CModule* module = FindModule(bstrModName);
        bool bIsMod = module != NULL;
        if (module != NULL)
            bIsMod = !module->m_bNeedReinstall;

		//=============================
		// Prepare response xml
		CComPtr<MSXML2::IXMLDOMElement>  pRespElem;
		hRes = CXmlHelpers::CreateDocument(CComBSTR(L"Response"), &pRespElem);
        if (hRes != S_OK) throw ERR_RESP_IsModuleAvailable_FAIL_RESP;

        hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"0");
        if (hRes != S_OK) throw ERR_RESP_IsModuleAvailable_FAIL_ERR;

        CComPtr<MSXML2::IXMLDOMElement> pRParamsElem;
		hRes = CXmlHelpers::AddElement(pRespElem, CComBSTR(L"Params"), &pRParamsElem);
        if (hRes != S_OK) throw ERR_RESP_IsModuleAvailable_FAIL_PARAM;

		CComPtr<MSXML2::IXMLDOMElement> pStatusElem;
		hRes = CXmlHelpers::AddElement(pRParamsElem, CComBSTR(L"Status"), &pStatusElem);
        if (hRes != S_OK) throw ERR_RESP_IsModuleAvailable_FAIL_STAT;

        hRes = CXmlHelpers::SetAttribute(pStatusElem, L"status", bIsMod ? L"Ok" : L"NoSuchModule");
		if (hRes != S_OK) throw ERR_RESP_IsModuleAvailable_FAIL_STAT2;

        hRes = CXmlHelpers::SetAttribute(pStatusElem, L"name", bstrModName);
		if (hRes != S_OK) throw ERR_RESP_IsModuleAvailable_FAIL_STAT_NAME;

		hRes = CXmlHelpers::SetAttribute(pStatusElem, L"version", module==NULL ? L"" : module->m_strVersion.c_str());
		if (hRes != S_OK) throw ERR_RESP_IsModuleAvailable_FAIL_STAT_VER;

		hRes = pRespElem->get_xml(pbstrRes);
		if (hRes != S_OK) throw ERR_RESP_IsModuleAvailable_FAIL_XML2STR;

        return 0;
    }
    catch (int nErr)
    {
        TCHAR buf[128];
        sprintf(buf, "Command_IsCommandAvailable() error %d", nErr);
        Trace(buf);
		return nErr;
    }
    catch(...){}
    return ERR_NonIntException;
}

int CCommandManager::Command_GetUpdateInfo(BSTR bstrCmdText, BSTR * pbstrRes)
{
    try
    {
		//=============================
		//Parse request xml
		CComPtr<MSXML2::IXMLDOMElement>  pReqElem;
		HRESULT hRes = CXmlHelpers::LoadXml(bstrCmdText, &pReqElem);
		if (hRes != S_OK) throw ERR_REQ_BADREQ;

        CComPtr<MSXML2::IXMLDOMElement> pParamsElem;
        hRes = CXmlHelpers::GetChildElement(pReqElem, L"Params", &pParamsElem);
        if (hRes != S_OK) throw ERR_REQ_NO_PAR;

        CComPtr<MSXML2::IXMLDOMElement> pModulesElem;
        hRes = CXmlHelpers::GetChildElement(pParamsElem, L"Modules", &pModulesElem);
        if (hRes != S_OK) throw ERR_REQ_GetUpdateInfo_NO_MOD;

        CComBSTR bstrModules;
		hRes = pModulesElem->get_xml(&bstrModules);
		if (hRes != S_OK) throw ERR_REQ_GetUpdateInfo_NO_MOD2;

		CComPtr<MSXML2::IXMLDOMElement> pRModulesElem;
		hRes = CXmlHelpers::LoadXml(bstrModules, &pRModulesElem);
		if (hRes != S_OK) throw ERR_REQ_GetUpdateInfo_NO_MOD3;

        CComPtr<MSXML2::IXMLDOMElement> pModuleInfoElem;
        CXmlHelpers::GetChildElement(pRModulesElem, L"ModuleInfo", &pModuleInfoElem);
        while (pModuleInfoElem)
        {
            CheckFileInfo(pModuleInfoElem);

            CComPtr<MSXML2::IXMLDOMElement> pDependsElem;
            hRes = CXmlHelpers::GetChildElement(pModuleInfoElem, L"Depends", &pDependsElem);
            if (hRes == S_OK)
            {
                CComPtr<MSXML2::IXMLDOMElement> pFileInfoElem;
                CXmlHelpers::GetChildElement(pDependsElem, L"File", &pFileInfoElem);
                while (pFileInfoElem)
                {
                    CheckFileInfo(pFileInfoElem);

                    CComPtr<MSXML2::IXMLDOMElement> pNextElem;
                    CXmlHelpers::GetNextSiblingElement(pFileInfoElem, &pNextElem);
                    pFileInfoElem = pNextElem;
                }
            }

            CComPtr<MSXML2::IXMLDOMElement> pNextElem;
            CXmlHelpers::GetNextSiblingElement(pModuleInfoElem, &pNextElem);
            pModuleInfoElem = pNextElem;
        }

		//=============================
		// Prepare response xml
		CComPtr<MSXML2::IXMLDOMElement>  pRespElem;
		hRes = CXmlHelpers::CreateDocument(CComBSTR(L"Response"), &pRespElem);
        if (hRes != S_OK) throw ERR_RESP_GetUpdateInfo_FAIL_RESP;

        hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"0");
        if (hRes != S_OK) throw ERR_RESP_GetUpdateInfo_FAIL_ERR;

        CComPtr<MSXML2::IXMLDOMElement> pRParamsElem;
		hRes = CXmlHelpers::AddElement(pRespElem, CComBSTR(L"Params"), &pRParamsElem);
        if (hRes != S_OK) throw ERR_RESP_GetUpdateInfo_FAIL_PARAM;

		CComPtr<MSXML2::IXMLDOMNode> pNewPLNode;
		pRParamsElem->appendChild(pRModulesElem, &pNewPLNode);

		hRes = pRespElem->get_xml(pbstrRes);
		if (hRes != S_OK) throw ERR_RESP_GetUpdateInfo_FAIL_XML2STR;

        return 0;
    }
    catch (int nErr)
    {
        TCHAR buf[128];
        sprintf(buf, "Command_GetUpdateInfo() error %d", nErr);
        Trace(buf);
		return nErr;
    }
    catch(...){}
    return ERR_NonIntException;
}

BOOL CCommandManager::CheckFileInfo(MSXML2::IXMLDOMElement * pFileInfoElem)
{
    CComBSTR bstrName;
    HRESULT hRes = CXmlHelpers::GetAttribute(pFileInfoElem, L"Name", &bstrName);
    CComBSTR bstrFilename;
    hRes = CXmlHelpers::GetAttribute(pFileInfoElem, L"Filename", &bstrFilename);
    CComBSTR bstrSum;
    hRes = CXmlHelpers::GetAttribute(pFileInfoElem, L"Sum", &bstrSum);
    CComBSTR bstrVersion;
    hRes = CXmlHelpers::GetAttribute(pFileInfoElem, L"Version", &bstrVersion);

    std::wstring strState = L"NoChange";
    std::wstring strSum = L"";
    int nRes = CalcSum(bstrFilename, strSum);
    if (nRes == 0 && wcscmp(strSum.c_str(), bstrSum) != 0)
    {
        strState = L"Updated";
    }
    else if (nRes == 1)
    {
        strState = L"New";
    }
    if (bstrName != NULL)
    {
        CModule* module = FindModule(bstrName);
        if (module != NULL)
        {
            if (compareVersion(module->m_strVersion.c_str(), bstrVersion) < 0)
            {
                module->m_bNeedReinstall = true;
                strState = L"Reinstall";
            }
        }
    }
    hRes = CXmlHelpers::SetAttribute(pFileInfoElem, L"UpdateState", strState.c_str());
    return TRUE;
}

// 0 - ok
// 1 - no such file
// 2 - open/read file error
int CCommandManager::CalcSum(BSTR bstrFilename, std::wstring & strSum)
{
	tstring strFileName;
    if (!getFullFileName(bstrFilename, strFileName)) return false;

    HANDLE hFile = ::CreateFile(strFileName.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (hFile == INVALID_HANDLE_VALUE) return 1;

	DWORD dwSize = ::GetFileSize(hFile, 0);
	BYTE * pBufIn = new BYTE[dwSize];
	DWORD dwRead = 0;
	::ReadFile(hFile, pBufIn, dwSize, &dwRead, 0);
	::CloseHandle(hFile);
    if (dwRead != dwSize) {delete[] pBufIn; return 2;}

	//calc file sum
	DWORD dwSum = 0;

	PIMAGE_DOS_HEADER dosHeader = (PIMAGE_DOS_HEADER)pBufIn;
	if (dosHeader->e_magic == IMAGE_DOS_SIGNATURE)
    {
	    PIMAGE_NT_HEADERS ntHeader = (PIMAGE_NT_HEADERS)(pBufIn + dosHeader->e_lfanew);
	    if (ntHeader->Signature == 0x00004550)
        {
            dwSum = CalcPESum(pBufIn, dwSize);
        }
    }

    if (dwSum == 0)
    {
	    for (DWORD ii=0; ii<dwSize; ii++)
	    {
		    dwSum += pBufIn[ii];
	    }
    }

	delete[] pBufIn;

	WCHAR buf[64] = {0};
	swprintf(buf, L"%u", dwSum);
    strSum = buf;

    return 0;
}

DWORD CCommandManager::CalcPESum(BYTE * pData, DWORD dwSize)
{
    DWORD dwSum = 0;

	PIMAGE_DOS_HEADER dosHeader = (PIMAGE_DOS_HEADER)pData;
	if (dosHeader->e_magic != IMAGE_DOS_SIGNATURE) return 0;
	PIMAGE_NT_HEADERS ntHeader = (PIMAGE_NT_HEADERS)(pData + dosHeader->e_lfanew);
	if (ntHeader->Signature != 0x00004550) return 0;

	DWORD secNum = ntHeader->FileHeader.NumberOfSections;

	PIMAGE_SECTION_HEADER secHeader = (PIMAGE_SECTION_HEADER)(pData + dosHeader->e_lfanew + sizeof(DWORD) + sizeof(IMAGE_FILE_HEADER) + ntHeader->FileHeader.SizeOfOptionalHeader);
	for (DWORD i=0; i<secNum; i++)
	{
		IMAGE_SECTION_HEADER & sec = secHeader[i];
		if (strncmp((char*)sec.Name, ".rdata", 6) == 0) continue;

		DWORD addr = sec.VirtualAddress;
		DWORD size = sec.Misc.VirtualSize;
		if (addr < dwSize && addr + size <= dwSize)
		{
			for (DWORD j=addr; j<addr+size; j++)
			{
				dwSum += pData[j];
			}
		}
	}

    return dwSum;
}

BOOL CCommandManager::Command_UpdateFiles(BSTR bstrCmdText, BSTR * pbstrRes)
{
    try
    {
		//=============================
		//Parse request xml
		CComPtr<MSXML2::IXMLDOMElement>  pReqElem;
		HRESULT hRes = CXmlHelpers::LoadXml(bstrCmdText, &pReqElem);
		if (hRes != S_OK) throw ERR_REQ_BADREQ;

        CComPtr<MSXML2::IXMLDOMElement> pParamsElem;
        hRes = CXmlHelpers::GetChildElement(pReqElem, L"Params", &pParamsElem);
        if (hRes != S_OK) throw ERR_REQ_NO_PAR;

        CComPtr<MSXML2::IXMLDOMElement> pFilesElem;
        hRes = CXmlHelpers::GetChildElement(pParamsElem, L"Files", &pFilesElem);
        if (hRes != S_OK) throw ERR_REQ_UpdateFiles_NO_FILES;

        CComPtr<MSXML2::IXMLDOMElement> pFileElem;
        CXmlHelpers::GetChildElement(pFilesElem, L"File", &pFileElem);
		std::wstring wsFailedToSaveFiles;

        while (pFileElem)
        {
            CComBSTR bstrFileName;
            hRes = CXmlHelpers::GetAttribute(pFileElem, L"Filename", &bstrFileName);
			if (hRes != S_OK) throw ERR_REQ_UpdateFiles_NO_FILE_NAME;
            if (hRes == S_OK)
            {
    		    CComBSTR bstrFileBin;
	    	    hRes = pFileElem->get_text(&bstrFileBin);
				if (hRes != S_OK) throw ERR_REQ_UpdateFiles_NO_FILE_BODY;
                if (hRes == S_OK)
                {
		            int nLen = bstrFileBin.Length();
					if(!saveFile(bstrFileName, bstrFileBin)) {
						if(wsFailedToSaveFiles.length() > 0)
							wsFailedToSaveFiles += L", ";
						wsFailedToSaveFiles += bstrFileName.m_str;
					}
                }
            }

            CComPtr<MSXML2::IXMLDOMElement> pNextElem;
            CXmlHelpers::GetNextSiblingElement(pFileElem, &pNextElem);
            pFileElem = pNextElem;
        }

        InitModules();

		//=============================
		// Prepare response xml
		CComPtr<MSXML2::IXMLDOMElement>  pRespElem;
		hRes = CXmlHelpers::CreateDocument(CComBSTR(L"Response"), &pRespElem);
        if (hRes != S_OK) throw ERR_RESP_UpdateFiles_FAIL_RESP;

		hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", wsFailedToSaveFiles.length() == 0 ? L"0" : L"1");
        if (hRes != S_OK) throw ERR_RESP_UpdateFiles_FAIL_ERR;

		if(wsFailedToSaveFiles.length() > 0) {
			wsFailedToSaveFiles = std::wstring(L"Failed to decode or save file(s) ")+wsFailedToSaveFiles;
			wsFailedToSaveFiles += L".";
			hRes = CXmlHelpers::SetAttribute(pRespElem, L"ErrorDesc", wsFailedToSaveFiles.c_str());
		    if (hRes != S_OK) throw ERR_RESP_UpdateFiles_FAIL_ERR_DESC;
		}

        CComPtr<MSXML2::IXMLDOMElement> pRParamsElem;
		hRes = CXmlHelpers::AddElement(pRespElem, CComBSTR(L"Params"), &pRParamsElem);
        if (hRes != S_OK) throw ERR_RESP_UpdateFiles_FAIL_PARAM;

		hRes = pRespElem->get_xml(pbstrRes);
		if (hRes != S_OK) throw ERR_RESP_UpdateFiles_FAIL_XML2STR;

		return 0;
	}
    catch (int nErr)
    {
        TCHAR buf[128];
        sprintf(buf, "Command_UpdateFiles() error %d", nErr);
        Trace(buf);
		return nErr;
    }
    catch(...){}
    return ERR_NonIntException;
}

bool CCommandManager::saveFile(LPCWSTR wcName, LPCWSTR wcData)
{
    //==================================
	//Make full file name

	tstring strFileName;
    if (!getFullFileName(wcName, strFileName)) return false;

    //==================================
	//Un-base64

    //base64 encode data
    static WCHAR TableBase64[] = L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	DWORD dwSizeB = wcslen(wcData);
	if (dwSizeB == 0) return false;
	if (dwSizeB % 4 != 0) return false;

	DWORD dwSizeC = dwSizeB / 4 * 3;
	BYTE * pBufC = new BYTE[dwSizeC];
	std::map<WCHAR, DWORD> chars;
	for (DWORD j=0; j<64; j++)
	{
		chars[TableBase64[j]] = j;
	}

	for (DWORD i=0, c=0; i<dwSizeB; i+=4)
	{
		int  nInSym  = 4;
		int  nOutSym = 3;
		if (wcData[i+2] == L'=' && wcData[i+3] == L'=')
		{
			nInSym    = 2;
			nOutSym   = 1;
		}
		else if (wcData[i+3] == L'=')
		{
			nInSym    = 3;
			nOutSym   = 2;
		}

		DWORD dwQ = (chars[wcData[i]]<<18) | (chars[wcData[i+1]]<<12);
		if (nInSym > 2)
		{
			dwQ |= chars[wcData[i+2]]<<6;
		}
		if (nInSym > 3)
		{
			dwQ |= chars[wcData[i+3]];
		}

		pBufC[c++] = (BYTE)(dwQ >> 16);
		if (nOutSym > 1)
		{
			pBufC[c++] = (BYTE)(dwQ >> 8);
		}
		if (nOutSym > 2)
		{
			pBufC[c++] = (BYTE)dwQ;
		}
	}
	dwSizeC = c;

    //==================================
	//Un-zip data

	DWORD dwSize = dwSizeC * 20;
	BYTE * pBuf = new BYTE[dwSize];
	CZip zip;
	DWORD dwRes = zip.InflateMem(pBufC, dwSizeC, pBuf, &dwSize);
	delete[] pBufC;
	if (dwRes != 0) return false;

    //==================================
	//Save file

    //check if the file is self exe
	TCHAR fn[MAX_PATH+1]={0};
	::GetModuleFileName(0, fn, MAX_PATH);
    if (_tcsicmp(strFileName.c_str(), fn) == 0)
    {
        saveSelfFile(strFileName.c_str(), pBuf, dwSize);
    }
    else
    {
        HANDLE hFile = ::CreateFile(strFileName.c_str(), GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, 0, NULL);
        if (hFile == INVALID_HANDLE_VALUE) return false;
        DWORD dwWr = 0;
        ::WriteFile(hFile, pBuf, dwSize, &dwWr, 0);
        ::CloseHandle(hFile);
    }

	delete[] pBuf;
	return true;
}

static TCHAR strLoaderTitle[] = _T("MediaFace Online Plugin Service Loader");

bool CCommandManager::saveSelfFile(LPCTSTR strName, BYTE * pData, DWORD dwSize)
{
    //================================

	HRSRC hRsrc = ::FindResource(_Module.GetResourceInstance(), MAKEINTRESOURCE(IDR_LOADER), _T("BIN"));
	if (!hRsrc) return false;
	HGLOBAL hRsrcMem = ::LoadResource(_Module.GetResourceInstance(), hRsrc);
	if (!hRsrcMem) return false;
	LPCSTR pRsrcMem = (LPCSTR)::LockResource(hRsrcMem);
	if (!pRsrcMem) return false;
    DWORD dwRsrcSize = ::SizeofResource(_Module.GetResourceInstance(), hRsrc);
    if (dwRsrcSize == 0) return false;

    //================================

	tstring strFileName = strName;
    strFileName += _T(".loader");
    HANDLE hFile = ::CreateFile(strFileName.c_str(), GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, 0, NULL);
    if (hFile == INVALID_HANDLE_VALUE) return false;
    DWORD dwWr = 0;
    ::WriteFile(hFile, pRsrcMem, dwRsrcSize, &dwWr, 0);
    ::CloseHandle(hFile);

    //================================

    STARTUPINFO si = {0};
	si.cb = sizeof(si);
    PROCESS_INFORMATION pi = {0};

	tstring cmdLine;
    cmdLine += "\"";
    cmdLine += strFileName;
    cmdLine += "\"";

	if(::CreateProcess( NULL, (LPSTR)cmdLine.c_str(), NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi ) == 0)
	{
		DWORD a_error = GetLastError();
		return false;
	}
	::CloseHandle(pi.hThread);
    ::CloseHandle(pi.hProcess);
    ::Sleep(1000);

    //================================
    HWND hWnd = ::FindWindow(0, strLoaderTitle);
    if (!hWnd) return false;

    DWORD dwDataSize = sizeof(DWORD) + 2048 + sizeof(DWORD) + dwSize;
    BYTE * pMem = new BYTE[dwDataSize];
    if (!pMem) return false;
    BYTE * p = pMem;

    DWORD dwProcessId = ::GetCurrentProcessId();
    memcpy(p, &dwProcessId, sizeof(DWORD));
    p += sizeof(DWORD);
    wcscpy((WCHAR*)p, CTxtCnv().t2w(strName));
    p += 2048;
    memcpy(p, &dwSize, sizeof(DWORD));
    p += sizeof(DWORD);
    memcpy(p, pData, dwSize);

    COPYDATASTRUCT cds;
    cds.dwData = 0;
    cds.cbData = dwDataSize;
    cds.lpData = pMem;
    ::SendMessage(hWnd, WM_COPYDATA, (WPARAM)m_hWnd, (LPARAM)(LPVOID)&cds);

    delete[] pMem;

    //================================

    Terminate();

	return true;
}

bool CCommandManager::getFullFileName(LPCWSTR wcName, tstring & strFullFileName)
{
	TCHAR buf[MAX_PATH+1]={0};
	::GetModuleFileName(0, buf, MAX_PATH);
	strFullFileName = buf;
	long nPos = strFullFileName.find_last_of(_T("/\\"));
	if (nPos == tstring::npos) return false;

	strFullFileName.resize(nPos);
	strFullFileName += _T("\\");
	strFullFileName += CTxtCnv().w2t(wcName);
    return true;
}

void parseVersion(LPCWSTR wcVersion, int* vers)
{
    LPWSTR token;
    WCHAR seps[] = L".";

    LPWSTR str = _wcsdup(wcVersion);
    token = wcstok(str, seps);
    try{
        vers[0] = _wtoi(token);
        token = wcstok(NULL,seps);
        vers[1] = _wtoi(token);
        token = wcstok(NULL,seps);
        vers[2] = _wtoi(token);
        token = wcstok(NULL,seps);
        vers[3] = _wtoi(token);
    }catch(...) {
    }

    free(str);
}

int CCommandManager::compareVersion(LPCWSTR wcVersion1, LPCWSTR wcVersion2)
{
    int vers1[]= {0,0,0,0};
    parseVersion(wcVersion1, vers1);
    int vers2[]= {0,0,0,0};
    parseVersion(wcVersion2, vers2);

    for(int i = 0; i < 4; ++i) {
        if (vers1[i] < vers2[i])
            return -1;
        if (vers1[i] > vers2[i])
            return 1;
    }

    return 0;
}