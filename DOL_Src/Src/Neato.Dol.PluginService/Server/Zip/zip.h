#ifndef __ZIP_H__
	#define __ZIP_H__

#define Z_OK			0
#define Z_STREAM_END	1
#define Z_NEED_DICT		2
#define Z_ERRNO			(-1)
#define Z_STREAM_ERROR	(-2)
#define Z_DATA_ERROR	(-3)
#define Z_MEM_ERROR		(-4)
#define Z_BUF_ERROR		(-5)
#define Z_VERSION_ERROR	(-6)

#define ZIP_PROGRESS_INIT 101
#define	ZIP_PROGRESS_END  102

typedef BOOL (ZIP_PROGRESS_CALLBACK)(PVOID pParam, long lParam);

DWORD ZipInflateFileA(LPCSTR szSourceFile, LPCSTR szTargetFile, ZIP_PROGRESS_CALLBACK pfnProgress, PVOID pParam);
DWORD ZipInflateFileW(LPCWSTR szSourceFile, LPCWSTR szTargetFile, ZIP_PROGRESS_CALLBACK pfnProgress, PVOID pParam);

DWORD ZipDeflateFileA(LPCSTR szSourceFile, LPCSTR szTargetFile, UINT nCompressionLevel, BOOL bStorePath, ZIP_PROGRESS_CALLBACK pfnProgress, PVOID pParam);
DWORD ZipDeflateFileW(LPCWSTR szSourceFile, LPCWSTR szTargetFile, UINT nCompressionLevel, BOOL bStorePath, ZIP_PROGRESS_CALLBACK pfnProgress, PVOID pParam);

#ifndef _UNICODE
	#define ZipInflateFile ZipInflateFileA
	#define ZipDeflateFile ZipDeflateFileA
#else
	#define ZipInflateFile ZipInflateFileW
	#define ZipDeflateFile ZipDeflateFileW
#endif

DWORD InflateMem(PBYTE pCompressedData, DWORD dwCompressedLen, PBYTE pUncompressedData, DWORD *pdwUncompressedLen);
DWORD DeflateMem(PBYTE pUncompressedData, DWORD dwUncompressedLen, PBYTE pCompressedData, DWORD *pdwCompressedLen, UINT uLevel);

// then you use ZIP as static linked library you should define USING_ZIP_LIB symbol, and 
// then you use ZIP as dynamic linked library you should define USING_ZIP_DLL symbol

#ifndef ZIP_LIB 

#if defined(USING_ZIP_LIB)
#	if defined(_DEBUG)	//these lib's shold be built with zip_lib.mak file
#		if defined(_MT)	
#			pragma comment (lib, "libzipmtd.lib")
#			pragma message("using zip library libzipmtd.lib")
#		else
#			pragma comment (lib, "libzipd.lib")
#			pragma message("using zip library libzipd.lib")
#		endif
#	else
#		if defined(_MT)	
#			pragma comment (lib, "libzipmt.lib")
#			pragma message("using zip library libzipmt.lib")
#		else
#			pragma comment (lib, "libzip.lib")
#			pragma message("using zip library libzip.lib")
#		endif
#	endif
#else
#	if defined(USING_ZIP_DLL)
#		if defined(_DEBUG)	//these lib's shold be built with zip_dll.mak file
#			pragma comment (lib, "zipd.lib")
#			pragma message("using zip library zipd.lib")
#		else
#			pragma comment (lib, "zip.lib")
#			pragma message("using zip library zip.lib")
#		endif	
#	else
#		define ZIP_DYNAMIC
#		if defined(_DEBUG)
#			define ZIP_MODULE_NAME _T("zipd.dll")
#			pragma message("using zip library zipd.dll")
#		else
#			define ZIP_MODULE_NAME _T("zip.dll")
#			pragma message("using zip library zip.dll")
#		endif	
#	endif
#endif


#ifdef _UNICODE
	typedef	DWORD (*ZipInflateFileProc)(LPCWSTR szSourceFile, LPCWSTR szTargetFile, ZIP_PROGRESS_CALLBACK pfnProgress, PVOID pParam);
	typedef	DWORD (*ZipDeflateFileProc)(LPCWSTR szSourceFile, LPCWSTR szTargetFile, UINT nCompressionLevel, BOOL bStorePath, ZIP_PROGRESS_CALLBACK pfnProgress, PVOID pParam);
	#define ZipInflateFileName "ZipInflateFileW"
	#define ZipDeflateFileName "ZipDeflateFileW"
#else
	typedef	DWORD (*ZipInflateFileProc)(LPCSTR szSourceFile, LPCSTR szTargetFile, ZIP_PROGRESS_CALLBACK pfnProgress, PVOID pParam);
	typedef	DWORD (*ZipDeflateFileProc)(LPCSTR szSourceFile, LPCSTR szTargetFile, UINT nCompressionLevel, BOOL bStorePath, ZIP_PROGRESS_CALLBACK pfnProgress, PVOID pParam);
	#define ZipInflateFileName "ZipInflateFileA"
	#define ZipDeflateFileName "ZipDeflateFileA"
#endif

typedef	DWORD (*InflateMemProc)(PBYTE pCompressedData, DWORD dwCompressedLen, PBYTE pUncompressedData, DWORD *dwUncompressedLen);
typedef	DWORD (*DeflateMemProc)(PBYTE pUncompressedData, DWORD dwUncompressedLen, PBYTE pCompressedData, DWORD *dwCompressedLen, UINT uLevel);
#define InflateMemName "InflateMem"
#define DeflateMemName "DeflateMem"


class CZip
{
public:
	CZip()
#if defined(ZIP_DYNAMIC)
		:m_hZipModule(NULL)
#endif
	{	
		Init(NULL);
	}
	virtual ~CZip()
	{
#if defined(ZIP_DYNAMIC)
		if (m_hZipModule != NULL)
		{
			::FreeLibrary(m_hZipModule);
			m_hZipModule = NULL;
		}
#endif
	}
	
	BOOL Init(HMODULE hModule = NULL)
	{

#if defined(ZIP_DYNAMIC)
		if (m_hZipModule == NULL)
		{
			m_hZipModule = ::LoadLibrary(ZIP_MODULE_NAME);
			
		}

		return m_hZipModule != NULL;
#else
		return TRUE;
#endif

	}

	DWORD DeflateMem(PBYTE pUncompressedData, DWORD dwUncompressedLen, PBYTE pCompressedData, DWORD *pdwCompressedLen, UINT uLevel = 6)
	{
		static DeflateMemProc pfn = NULL;
		if (pfn == NULL)
		{
#if defined(ZIP_DYNAMIC)
			pfn = (DeflateMemProc)::GetProcAddress(m_hZipModule, DeflateMemName);
#else
			pfn = ::DeflateMem;
#endif
		}
		if (pfn == NULL)
			return -1;

		return pfn(pUncompressedData, dwUncompressedLen, pCompressedData, pdwCompressedLen, uLevel);
	}

	DWORD InflateMem(PBYTE pCompressedData, DWORD dwCompressedLen, PBYTE pUncompressedData, DWORD *pdwUncompressedLen)
	{
		static InflateMemProc pfn = NULL;
		if (pfn == NULL)
		{
#if defined(ZIP_DYNAMIC)
			pfn = (InflateMemProc)::GetProcAddress(m_hZipModule, InflateMemName);
#else
			pfn = ::InflateMem;
#endif
		}
		if (pfn == NULL)
			return -1;

		return pfn(pCompressedData, dwCompressedLen, pUncompressedData, pdwUncompressedLen);
	}

	DWORD InflateFile(LPCTSTR szSourceFile, LPCTSTR szTargetFile)
	{
		static ZipInflateFileProc pfn = NULL;
		if (pfn == NULL)
		{
#if defined(ZIP_DYNAMIC)
			pfn = (ZipInflateFileProc)::GetProcAddress(m_hZipModule, ZipInflateFileName);
#else
			pfn = ZipInflateFile;
#endif
		}
		if (pfn == NULL)
			return -1;

		return pfn(szSourceFile, szTargetFile, ZipProgressCallBack, this);
	}

	DWORD DeflateFile(LPCTSTR szSourceFile, LPCTSTR szTargetFile, UINT nCompressionLevel = 6, BOOL bStorePath = FALSE )
	{
		static ZipDeflateFileProc pfn = NULL;
		if (pfn == NULL)
		{
#if defined(ZIP_DYNAMIC)
			pfn = (ZipDeflateFileProc)::GetProcAddress(m_hZipModule, ZipDeflateFileName);
#else
			pfn = &ZipDeflateFile;
#endif
		}
		if (pfn == NULL)
			return -1;

		return pfn(szSourceFile, szTargetFile, nCompressionLevel, bStorePath, ZipProgressCallBack, this);
	}

protected:

	virtual OnProgressCallback(long lParam)
	{
		return TRUE;
	}

	static BOOL ZipProgressCallBack(PVOID pParam, long lParam)
	{
		CZip* pZip = reinterpret_cast<CZip*>(pParam);
		if (pZip)
			return pZip->OnProgressCallback(lParam);
		return TRUE;
	}

protected:
#if defined(ZIP_DYNAMIC)
	HMODULE m_hZipModule;
#endif
};

#endif

#endif //__ZIP_H__



