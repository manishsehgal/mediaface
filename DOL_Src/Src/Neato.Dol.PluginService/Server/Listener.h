#pragma once

#include <Winsock2.h>
#include "Interfaces.h"


class CListener
{
public:
    CListener(ICommandManager * pCmdMgr);
    ~CListener();

    static void ListenerProc(void * pArg);
    void Listen();
    void Respond(void * pArg);

private:
	std::wstring ReadFromSocket(SOCKET& p_socket);

private:
    unsigned short m_nPort;
    ICommandManager * m_pCmdMgr;
    HANDLE m_hShutdownEvent;
    HANDLE m_hListenerThread;
};
