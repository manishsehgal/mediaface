#include "stdafx.h"
//#include <process.h>
#include <TxtCnv.h>
#include "Listener.h"

#define SERVER       L"*"//L"mfserver"
//#define CMD_PORT       1212
//#define CMD_PORT_STR L"1212"


CListener::CListener(ICommandManager * pCmdMgr)
{
    m_pCmdMgr = pCmdMgr;

    //init WinSock
    WSADATA wsadata;
    WORD wVersionRequested = MAKEWORD(2,2);
    int	nRet = ::WSAStartup(wVersionRequested, &wsadata);
    if (nRet == SOCKET_ERROR)
    {
        //printf("%s %d\n", "WSAStartup error", ::WSAGetLastError());
        //return -1;
    }

    //create shutdown event
    m_hShutdownEvent = ::CreateEvent(0, TRUE, FALSE, 0);
    m_nPort = 0;

    //run listening thread
    m_hListenerThread = (HANDLE)_beginthread(ListenerProc, 0, (void*)this);
}

CListener::~CListener()
{
    //set shutdown event and wait listening thread to terminate
    ::SetEvent(m_hShutdownEvent);
    ::WaitForSingleObject(m_hListenerThread, 10000);

    //close handles
    ::CloseHandle(m_hShutdownEvent);

    //uninit WinSock
    ::WSACleanup();
}

void CListener::ListenerProc(void * pArg)
{
    CListener * pListener = (CListener*)pArg;
    pListener->Listen();
}

void CListener::Listen()
{
    HRESULT hRes = ::CoInitialize(NULL);

    SOCKET   listener     = INVALID_SOCKET;
    WSAEVENT hAcceptEvent = WSA_INVALID_EVENT;

    try
    {
        //======================================
        //create listening socket
        listener = ::socket(AF_INET, SOCK_STREAM, 0);
        if (listener == INVALID_SOCKET) throw 1;

        //======================================
        //set event to listening socket (switch it to non-blocking state)
        hAcceptEvent = ::WSACreateEvent();
        int nRet = ::WSAEventSelect(listener, hAcceptEvent, FD_ACCEPT);
        if (nRet == SOCKET_ERROR) throw 2;

        //======================================
        //bind it to address
        SOCKADDR_IN addr;
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

        unsigned short nPort = 1212;
        for (int i=0; i<10; i++)
        {
            addr.sin_port = htons(nPort);
            nRet = ::bind(listener, (struct sockaddr *)&addr, sizeof(addr));
            if (nRet == 0)
            {
                m_nPort = nPort;
                break;
            }
            int nErr = ::WSAGetLastError();
            if (nErr == WSAEADDRINUSE)
            {
                nPort += 1234;
                continue;
            }
            throw 3;
        }
        if (m_nPort == 0) throw 4;
        
        //======================================
        //set socket to listening state
        nRet = ::listen(listener, SOMAXCONN);
        if (nRet == SOCKET_ERROR) throw 5;

        //======================================
        //event array
        WSAEVENT Events[2] = {m_hShutdownEvent, hAcceptEvent};

        //======================================
        //accept cycle
        while(1)
        {
            nRet = ::WSAWaitForMultipleEvents(2, Events, FALSE, INFINITE, FALSE);

            if (nRet == WSA_WAIT_EVENT_0 + 0)//shutdown event signaled
            {
                break;
            }
            else if (nRet == WSA_WAIT_EVENT_0 + 1)//accept event signaled
            {
                WSANETWORKEVENTS wsaEvents;
                ::WSAEnumNetworkEvents(listener, hAcceptEvent, &wsaEvents);
                if ((wsaEvents.lNetworkEvents & FD_ACCEPT) && wsaEvents.iErrorCode[FD_ACCEPT_BIT] == 0)
                {
                    SOCKET s = ::accept(listener, 0, 0);
                    if (s == INVALID_SOCKET) continue;

                    SOCKET * ps = new SOCKET;
                    *ps = s;
                    //HANDLE hThread = (HANDLE)_beginthread(ListenerReqProc, 0, (void*)ps);
                    //ListenerReqProc((void*)ps);
                    Respond((void*)ps);
                }
            }
        }
    }
    catch (int nErr)
    {
        TCHAR buf[128];
        sprintf(buf, "Listen() error %d", nErr);
        m_pCmdMgr->Trace(buf);
        ::Sleep(5000);
        m_pCmdMgr->Terminate();
    }
    catch(...){}

    //====================================
    if (listener != INVALID_SOCKET)
    {
        ::closesocket(listener);
    }
    if (hAcceptEvent != WSA_INVALID_EVENT)
    {
        ::WSACloseEvent(hAcceptEvent);
    }

    ::CoUninitialize();
}

void CListener::Respond(void * pArg)
{
    //===============================
    //get working socket
    SOCKET * ps = (SOCKET*)pArg;
    SOCKET s = *ps;
    delete ps;

	try
    {
		std::wstring strCmd = ReadFromSocket(s);
        
		//"Debug" for Release configuration
		/*char a_[10];
		itoa(strIn.size(), a_, 10);
		::MessageBox(NULL, a_, a_, MB_OK);*/

        std::wstring strOut;

        //===============================
        //check policy file request
        if (wcsicmp(strCmd.c_str(), L"<Discovery />") == 0)
        {
			//strOut += L"<Discovered />";
            strOut += L"<Discovered DataFollow=\"0\"/>";
        }
        else if (wcsicmp(strCmd.c_str(), L"<policy-file-request/>") == 0)
        {
            strOut += L"<cross-domain-policy>";
            strOut += L"<allow-access-from domain=\"";
            strOut += SERVER;
            strOut += L"\" to-ports=\"";
            WCHAR wcPort[64];
            strOut += _itow(m_nPort, wcPort, 10);
            strOut += L"\" />";
            strOut += L"</cross-domain-policy>";
        }
        else 
        {
			int Pos = strCmd.find(L">");
			if((Pos != -1) && (Pos<200)) {
				wchar_t wcsBuff[200];
				strCmd.copy(wcsBuff,Pos);
				wcsBuff[Pos]=0;
				if(wcsstr(wcsBuff,L"DataFollow=\"1\"")!=NULL) {
					int nCmdLen = strCmd.length();
					int nDataLen = strCmd.at(nCmdLen-1);
					if( nDataLen != 0) {
						nCmdLen -= nDataLen+1;
						strOut = strCmd.substr(nCmdLen,nDataLen);
						strCmd.resize(nCmdLen);
					}
					strOut += ReadFromSocket(s);
				}
			}
            if (m_pCmdMgr)
            {
                m_pCmdMgr->ProcessRequest(strCmd, strOut);
            }
        }

        //===============================
        //send response
		CTxtCnv tc(CP_UTF8);
        std::string strOutA = tc.w2a(strOut.c_str());
        ::send(s, strOutA.c_str(), strOutA.size()+1, 0);
    }
    catch (int nErr)
    {
        TCHAR buf[128];
        sprintf(buf, "Respond() error %d", nErr);
        m_pCmdMgr->Trace(buf);
    }
    catch(...){}

    //===============================
    if (s != INVALID_SOCKET)
    {
        ::closesocket(s);
    }
}

std::wstring CListener::ReadFromSocket(SOCKET& p_socket)
{
	WSAEVENT hReadEvent = WSA_INVALID_EVENT;
    
    CTxtCnv tc(CP_UTF8);
    std::wstring strIn;

	//int nN = 1;
    while (true)
    {
        //set reading event
        hReadEvent = ::WSACreateEvent();
        ::WSAEventSelect(p_socket, hReadEvent, FD_READ | FD_CLOSE);

        //wait for ready to read data
        WSAEVENT Events[2] = {m_hShutdownEvent, hReadEvent};
        int nRet = ::WSAWaitForMultipleEvents(2, Events, FALSE, 30000, FALSE);

        ::WSAEventSelect(p_socket, hReadEvent, 0);
        ::WSACloseEvent(hReadEvent);

        if (nRet != WSA_WAIT_EVENT_0 + 1) //NOT read or socked closed event signaled
        {
			if(nRet == WSA_WAIT_EVENT_0) { // socket should not be closed before response is sent
				m_pCmdMgr->Trace(_T("ReadFromSocket(): socket closed before response sent"));
				throw 1;
			}

            TCHAR buf[128] = "ReadFromSocket() error: timeout reading request";
            if(nRet != WSA_WAIT_TIMEOUT)
               sprintf(buf, "ReadFromSocket() error: nRet=%d ErrorCode=%d",nRet,WSAGetLastError());
            m_pCmdMgr->Trace(buf);

            throw 1;
        }

        //===============================
        //get incoming message size
        DWORD dwMsgSize = 0;
        ioctlsocket(p_socket, FIONREAD, &dwMsgSize);
		if (dwMsgSize == 0) 
			throw 2;

        //===============================
        //read message
        //char * pBuf = new char[dwMsgSize+1];
		std::auto_ptr<char> pBuf(new char[dwMsgSize+1]);
        nRet = ::recv(p_socket, pBuf.get(), dwMsgSize, 0);
        if (nRet == SOCKET_ERROR)
			throw 3;

		if (nRet == 0)
			break;

		pBuf.get()[nRet] = 0;
        strIn += tc.a2w(pBuf.get());

		char * pszEnd = (char*)memchr(pBuf.get(),0,nRet); 
	    if ( pszEnd != NULL)
		{
			std::wstring strRest = tc.a2w(pszEnd+1);
			strRest += wchar_t(strRest.length());
			strIn += strRest;
			break;
		}
       
       // nN++;
	}
	return strIn;
}