#pragma once

// Change these values to use different versions
#define WINVER		0x0400
//#define _WIN32_WINNT	0x0400
#define _WIN32_IE	0x0500
#define _RICHEDIT_VER	0x0100

#include <atlbase.h>
#include <atlapp.h>

extern CAppModule _Module;

#include <atlwin.h>

#include <vector>
#include <string>
typedef std::basic_string<TCHAR> tstring;

#define WM_USER_ICONNOTIFY  (WM_USER + 1)
#define WM_USER_TRACE       (WM_USER + 2)
#define WM_USER_TERMINATE   (WM_USER + 3)
#define WM_USER_SHOW        (WM_USER + 4)
#define WM_USER_MSXML       (WM_USER + 5)
#define WM_USER_LOADERDATA  (WM_USER + 6)


#import <msxml3.dll> raw_interfaces_only 

