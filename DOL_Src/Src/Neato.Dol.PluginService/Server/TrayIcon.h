#pragma once

#include <Shlwapi.h>

#define WARN_TIMER 800

static TCHAR strTrayIconTipText[]   = _T("MediaFace Online Plugin Service");
static TCHAR strTrayIconWarnText[]   = _T("MS XML Library is absent!");



class CTrayIcon
{
public:
    CTrayIcon()
    {
        m_hWnd = 0;
        m_bWarnIcon = false;
    }

private:
    HWND m_hWnd;
    bool m_bWarnIcon;

public:
    void SetHWND(HWND hWnd)
    {
        m_hWnd = hWnd;
    }

	BOOL Add()
	{
        if (!m_hWnd) return FALSE;
        Delete();
        HICON hIcon = (HICON)::LoadImage(_Module.GetResourceInstance(), MAKEINTRESOURCE(IDR_MAINFRAME), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), LR_DEFAULTCOLOR);
		NOTIFYICONDATA nid;
        PrepereHeader(nid);
		nid.uFlags = NIF_ICON|NIF_MESSAGE|NIF_TIP;
		nid.uCallbackMessage = WM_USER_ICONNOTIFY;
		nid.hIcon = hIcon;
		_tcscpy(nid.szTip, strTrayIconTipText);
		return ::Shell_NotifyIcon(NIM_ADD, &nid);
	}

	BOOL ModifyIcon(bool bWarn)
	{
        if (!m_hWnd) return FALSE;
        HICON hIcon = (HICON)::LoadImage(_Module.GetResourceInstance(), MAKEINTRESOURCE(bWarn?IDI_WARN:IDR_MAINFRAME), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), LR_DEFAULTCOLOR);
		NOTIFYICONDATA nid;
        PrepereHeader(nid);
		nid.uFlags = NIF_ICON;
		nid.hIcon = hIcon;
		return ::Shell_NotifyIcon(NIM_MODIFY, &nid);
    }

	BOOL Delete()
	{
        if (!m_hWnd) return FALSE;
		NOTIFYICONDATA nid;
        PrepereHeader(nid);
		nid.uFlags = 0;
		return ::Shell_NotifyIcon(NIM_DELETE, &nid);
	}

    void StartWarn()
    {
        if (!m_hWnd) return;
        ::SetTimer(m_hWnd, WARN_TIMER, 500, 0);
    }

    void OnTimer(UINT nTimerId)
    {
        if (nTimerId == WARN_TIMER)
        {
            m_bWarnIcon = !m_bWarnIcon;
            ModifyIcon(m_bWarnIcon);
        }
    }

    void StopWarn()
    {
        if (!m_hWnd) return;
        ::KillTimer(m_hWnd, WARN_TIMER);
        ModifyIcon(false);
        m_bWarnIcon = false;
    }

    BOOL ShowWarnTip()
    {
        if (!m_hWnd) return FALSE;
        NOTIFYICONDATA nid;
        PrepereHeader(nid);
        if (nid.cbSize < sizeof(NOTIFYICONDATA)) return FALSE;
		nid.uFlags = NIF_INFO;
        _tcscpy(nid.szInfoTitle, strTrayIconTipText);
        _tcscpy(nid.szInfo, strTrayIconWarnText);
        nid.uTimeout = 15000;
        nid.dwInfoFlags = NIIF_WARNING;
		return ::Shell_NotifyIcon(NIM_MODIFY, &nid);
    }

private:
    void PrepereHeader(NOTIFYICONDATA & nid)
    {
        memset(&nid, 0, sizeof(NOTIFYICONDATA));
        nid.cbSize = sizeof(NOTIFYICONDATA);
		nid.hWnd = m_hWnd;
		nid.uID = 567;

        HINSTANCE hDll = ::LoadLibrary(_T("Shell32.dll"));
        if(hDll)
        {
            DLLGETVERSIONPROC pDllGetVersion = (DLLGETVERSIONPROC)::GetProcAddress(hDll, "DllGetVersion");
            if(pDllGetVersion)
            {
                DLLVERSIONINFO vi = {sizeof(DLLVERSIONINFO)};
                HRESULT hr = (*pDllGetVersion)(&vi);
                if (vi.dwMajorVersion < 5)
                {
                    nid.cbSize = NOTIFYICONDATA_V1_SIZE;
                }
            }
            ::FreeLibrary(hDll);
        }
    }
};