#include "stdafx.h"

#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>

#include "resource.h"

#include "MainDlg.h"
#include "Listener.h"
#include "CommandManager.h"

CAppModule _Module;

static TCHAR strMutexName[]   = _T("MediaFacePluginServiceMutex");
static TCHAR strDialogTitle[] = _T("MediaFace Online Plugin Service");

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE /*hPrevInstance*/, LPTSTR lpstrCmdLine, int nCmdShow)
{
    // get flags from command line
    bool bExit = (_tcsicmp(lpstrCmdLine, _T("-stop")) == 0 || _tcsicmp(lpstrCmdLine, _T("/stop")) == 0);
    bool bShow = (_tcsicmp(lpstrCmdLine, _T("-settings")) == 0 || _tcsicmp(lpstrCmdLine, _T("/settings")) == 0);

    //prevent more than one process instances
    HANDLE hMutex = ::OpenMutex(MUTEX_ALL_ACCESS, FALSE, strMutexName);
    if (hMutex) 
    {
        if (bExit || bShow)
        {
            HWND hWnd = ::FindWindow(0, strDialogTitle);
            if (hWnd)
            {
				if (bExit)
                    ::PostMessage(hWnd, WM_USER_TERMINATE, 0, 0);
                if (bShow)
                    ::PostMessage(hWnd, WM_USER_SHOW, SW_SHOW, 0);
            }
        }
        ::CloseHandle(hMutex); 
        return 0;
    }

    //===============================

    if (bExit)
    {
        return 0;
    }

    hMutex = ::CreateMutex(0, TRUE, strMutexName);

	HRESULT hRes = ::CoInitialize(NULL);
	ATLASSERT(SUCCEEDED(hRes));

	// this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
	::DefWindowProc(NULL, 0, 0, 0L);

	//AtlInitCommonControls(ICC_BAR_CLASSES);	// add flags to support other controls

	hRes = _Module.Init(NULL, hInstance);
	ATLASSERT(SUCCEEDED(hRes));

	int nRet = 0;
	{

	    CMessageLoop theLoop;
	    _Module.AddMessageLoop(&theLoop);

	    CMainDlg dlgMain;
	    if (dlgMain.Create(NULL))
        {
            CCommandManager cmdMgr(dlgMain.m_hWnd);
            if (!cmdMgr.InitModules())
            {
                dlgMain.PostMessage(WM_USER_TERMINATE, 0, 0);
            }
            else
            {
                CListener listener(&cmdMgr);
    	        //dlgMain.ShowWindow(SW_HIDE);
	            nRet = theLoop.Run();
            }
        }
        _Module.RemoveMessageLoop();
	}

	_Module.Term();
	::CoUninitialize();

    if (hMutex) ::CloseHandle(hMutex);

	return nRet;
}
