//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Server.rc
//
#define IDR_MAINFRAME                   128
#define IDD_MAINDLG                     129
#define IDI_WARN                        202
#define IDR_LOADER                        214
#define IDC_LOG                         1000
#define IDC_CLOSE                       1001
#define IDC_STOP                        1002
#define IDC_WARNICON                    1003
#define IDC_WARNTEXT                    1004
#define IDC_WARNFRAME                   1005
#define IDC_WARNLEARNMORE               1006

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        215
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
