#pragma once

interface ICommandManager
{
    virtual void Trace(TCHAR * pText) = 0;
    virtual void Terminate() = 0;
    virtual bool ProcessRequest(std::wstring strCmd, std::wstring & strData_Result) = 0;
};