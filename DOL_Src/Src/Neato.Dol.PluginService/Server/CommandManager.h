#pragma once

#include "Interfaces.h"

//==========================================
class CCommand
{
public:
    std::wstring m_strName;
    bool m_bCopyRequest;
    bool m_bSelf;
};


//==========================================
class CModule
{
public:
    CModule():m_bNeedReinstall(false){};
    std::wstring m_strName;
    tstring      m_strFileName;
    std::vector<CCommand> m_vCommands;
    std::wstring m_strVersion;
    bool        m_bNeedReinstall;
};

//==========================================
class CCommandManager : public ICommandManager
{
public:
    CCommandManager(HWND hWnd);
    ~CCommandManager();

public:
    // ICommandManager
    void Trace(TCHAR * pText);
    void Terminate();
    bool ProcessRequest(std::wstring strCmd, std::wstring & strData_Result);
	// strData_Result: in-out parameter
	//		in: (optional) additional data for strCmd
	//		out: xml with response	 

public:
    bool InitModules();

private:
    void AddModule(tstring strFileName);
    void AddSelfModule();
    CModule * FindModule(LPCWSTR wcName);
    void ParseModuleInfo(BSTR bstrInfo, CModule & module);
    bool CheckMsXml();
    bool FindCommand(BSTR bstrCmdName, UINT & nModInd, UINT & nCmdInd);
    //BOOL InvokeCommand(BSTR bstrCmdName, BSTR bstrCmdText, BSTR * pbstrRes, bool & bCopyRequest);
	int InvokeCommand(BSTR bstrCmdName, BSTR bstrCmdText, BSTR * pbstrRes, BSTR * pbstrErrorDesc, bool & bCopyRequest);
    int InvokeSelfCommand(BSTR bstrCmdName, BSTR bstrCmdText, BSTR * pbstrRes);
	int Command_IsModuleAvailable(BSTR bstrCmdText, BSTR * pbstrRes);
    int Command_IsCommandAvailable(BSTR bstrCmdText, BSTR * pbstrRes);
    int Command_GetUpdateInfo(BSTR bstrCmdText, BSTR * pbstrRes);
    BOOL CheckFileInfo(MSXML2::IXMLDOMElement * pFileInfoElem);
    int  CalcSum(BSTR bstrFilename, std::wstring & strSum);
    DWORD CalcPESum(BYTE * pData, DWORD dwSize);

    BOOL Command_UpdateFiles(BSTR bstrCmdText, BSTR * pbstrRes);
    bool saveFile(LPCWSTR wcName, LPCWSTR wcData);
    bool saveSelfFile(LPCTSTR strName, BYTE * pData, DWORD dwSize);
    bool getFullFileName(LPCWSTR wcName, tstring & strFullFileName);

    int compareVersion(LPCWSTR wcVersion1, LPCWSTR wcVersion2);
private:
    HWND m_hWnd;
    std::vector<CModule> m_vModules;
    bool m_bIsMsXml;
};
