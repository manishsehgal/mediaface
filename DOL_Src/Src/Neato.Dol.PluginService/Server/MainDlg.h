#pragma once

#include "Interfaces.h"
#include "TrayIcon.h"


class CMainDlg : 
    public CDialogImpl<CMainDlg>,
    public CUpdateUI<CMainDlg>,
	public CMessageFilter, 
    public CIdleHandler
{
public:
	enum { IDD = IDD_MAINDLG };

    CMainDlg()
    {
    }
	virtual BOOL PreTranslateMessage(MSG* pMsg)
	{
		return CWindow::IsDialogMessage(pMsg);
	}
	virtual BOOL OnIdle()
	{
		return FALSE;
	}

	BEGIN_UPDATE_UI_MAP(CMainDlg)
	END_UPDATE_UI_MAP()

	BEGIN_MSG_MAP(CMainDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		COMMAND_ID_HANDLER(IDOK,            OnOK)
		COMMAND_ID_HANDLER(IDCANCEL,        OnCancel)
		COMMAND_ID_HANDLER(IDC_STOP,        OnStop)
		COMMAND_ID_HANDLER(IDC_CLOSE,       OnClose)
		COMMAND_ID_HANDLER(IDC_WARNLEARNMORE, OnWarnLearnMore)
		MESSAGE_HANDLER(WM_SYSCOMMAND,      OnSysCommand)
		MESSAGE_HANDLER(WM_USER_ICONNOTIFY, OnIconNotyfy)
		MESSAGE_HANDLER(WM_USER_TRACE,      OnTrace)
		MESSAGE_HANDLER(WM_USER_TERMINATE,  OnTerminate)
		MESSAGE_HANDLER(WM_USER_SHOW,       OnShow)
		MESSAGE_HANDLER(WM_USER_MSXML,      OnMsXml)
		MESSAGE_HANDLER(WM_TIMER,           OnTimer)
        MESSAGE_HANDLER(WM_QUERYENDSESSION, OnQueryEndSession);
    END_MSG_MAP()

//  Handler prototypes (uncomment arguments if needed):
//	LRESULT MessageHandler(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
//	LRESULT CommandHandler(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
//	LRESULT NotifyHandler(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/)

private:
    CTrayIcon m_TrayIcon;

private:
	LRESULT OnInitDialog(UINT, WPARAM, LPARAM, BOOL&)
	{
		// center the dialog on the screen
		CenterWindow();

		// set icons
  		HICON hIcon = (HICON)::LoadImage(_Module.GetResourceInstance(), MAKEINTRESOURCE(IDR_MAINFRAME), 
  			IMAGE_ICON, ::GetSystemMetrics(SM_CXICON), ::GetSystemMetrics(SM_CYICON), LR_DEFAULTCOLOR);
		SetIcon(hIcon, TRUE);

		HICON hIconSmall = (HICON)::LoadImage(_Module.GetResourceInstance(), MAKEINTRESOURCE(IDR_MAINFRAME), 
  			IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), LR_DEFAULTCOLOR);
  		SetIcon(hIconSmall, FALSE);

        HWND hWarnIcon = GetDlgItem(IDC_WARNICON);
        ::SendMessage(hWarnIcon, STM_SETIMAGE, (WPARAM)IMAGE_ICON, (LPARAM)::LoadIcon(0, IDI_WARNING));

		// register object for message filtering and idle updates
		CMessageLoop * pLoop = _Module.GetMessageLoop();
        if (pLoop)
        {
		    pLoop->AddMessageFilter(this);
		    pLoop->AddIdleHandler(this);
        }

		UIAddChildWindowContainer(m_hWnd);

        m_TrayIcon.SetHWND(m_hWnd);
        if (!m_TrayIcon.Add())
        {
			ShowWindow(SW_SHOW);
        }

		return TRUE;
	}

	LRESULT OnOK(WORD, WORD wID, HWND, BOOL&)
	{
		ShowWindow(SW_HIDE);
		return 0;
	}

	LRESULT OnCancel(WORD, WORD wID, HWND, BOOL&)
	{
		ShowWindow(SW_HIDE);
		return 0;
	}

	LRESULT OnStop(WORD, WORD wID, HWND, BOOL&)
	{
		CloseDialog(IDOK);
		return 0;
	}

	LRESULT OnClose(WORD, WORD wID, HWND, BOOL&)
	{
		ShowWindow(SW_HIDE);
		return 0;
	}

	void CloseDialog(int nVal)
	{
        m_TrayIcon.Delete();
		DestroyWindow();
		::PostQuitMessage(nVal);
	}

private:
	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
        m_TrayIcon.OnTimer(wParam);
		return 0;
	}
	LRESULT OnQueryEndSession(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
        ::TerminateProcess(GetCurrentProcess(), 0);
        //CloseDialog(IDOK);
        bHandled = true;
        return TRUE;
    }
	LRESULT OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		switch(wParam&0xFFF0)
		{
		case SC_MINIMIZE:
		case SC_CLOSE:
			{
				ShowWindow(SW_HIDE);
				return 0;
			}
			break;
		}
		bHandled = false; 
		return 1;
	}
	LRESULT OnIconNotyfy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		switch(lParam)
		{
		case  WM_LBUTTONDBLCLK:
			{
                PostMessage(WM_USER_SHOW, SW_SHOW, 0);
			}
			break;
		}
		return 0;
	}

	LRESULT OnTrace(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
        TCHAR * pText = (TCHAR*)lParam;
        HWND hLog = GetDlgItem(IDC_LOG);
        int nPos = ::SendMessage(hLog, LB_ADDSTRING, 0, (LPARAM)pText);
        ::SendMessage(hLog, LB_SETCURSEL, nPos, 0);
        delete[] pText;
        bHandled = true;
		return 0;
	}

	LRESULT OnTerminate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CloseDialog(IDOK);
		return 0;
	}

	LRESULT OnShow(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
        m_TrayIcon.StopWarn();
		ShowWindow(wParam);
        ::SetForegroundWindow(m_hWnd);
		return 0;
	}

	LRESULT OnMsXml(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
        if (wParam == TRUE)
        {
            m_TrayIcon.StopWarn();
            ShowWarnMessage(SW_HIDE);
        }
        else
        {
            m_TrayIcon.StartWarn();
            m_TrayIcon.ShowWarnTip();
            ShowWarnMessage(SW_SHOW);
            if (lParam == FALSE)
            {
                PostMessage(WM_USER_SHOW, SW_SHOW, 0);
            }
        }
		return 0;
	}

    void ShowWarnMessage(int nShow)
    {
        ::ShowWindow(GetDlgItem(IDC_WARNFRAME), nShow);
        ::ShowWindow(GetDlgItem(IDC_WARNICON), nShow);
        ::ShowWindow(GetDlgItem(IDC_WARNTEXT), nShow);
        ::ShowWindow(GetDlgItem(IDC_WARNLEARNMORE), nShow);
    }

	LRESULT OnWarnLearnMore(WORD, WORD wID, HWND, BOOL&)
	{
        TCHAR buf[MAX_PATH] = {0};
        if (!::GetModuleFileName(0, buf, MAX_PATH)) return 0;
        tstring strPath = buf;
        int nPos = strPath.rfind(_T('\\'));
        strPath = strPath.substr(0, nPos+1);
        strPath += _T("ReadMe.htm");
        if (::GetFileAttributes(strPath.c_str()) == INVALID_FILE_ATTRIBUTES) return 0;
        ::ShellExecute(m_hWnd, "open", strPath.c_str(), NULL, NULL, SW_SHOWNORMAL);
		return 0;
	}
};
