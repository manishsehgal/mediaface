#pragma once

#include <map>

#import  "..\Common\cbid.tlb" \
		raw_interfaces_only, raw_native_types, rename_namespace("MFCBID"), \
			rename ("_SERVICE_STATUS", "_SVC_STATUS"), rename ("SERVICE_STATUS", "SVC_STATUS"), \
			rename ("IMFTrack", "IMFCBIDTrack"), rename ("IMFTracks", "IMFCBIDTracks")

class CFingerPrint;

class ATL_NO_VTABLE CFingerPrintMfDbIdSink : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public IDispatchImpl<MFCBID::_IMFCBIDEvents, &__uuidof(MFCBID::_IMFCBIDEvents), 0>
{
public:
    CFingerPrintMfDbIdSink() {}
    void Init(CFingerPrint* pOwner) {m_pOwner = pOwner;}

    DECLARE_NOT_AGGREGATABLE(CFingerPrintMfDbIdSink)
    DECLARE_PROTECT_FINAL_CONSTRUCT()

    BEGIN_COM_MAP(CFingerPrintMfDbIdSink)
    	COM_INTERFACE_ENTRY(MFCBID::_IMFCBIDEvents)
	    COM_INTERFACE_ENTRY(IDispatch)
    END_COM_MAP()

protected:
    STDMETHOD(Invoke)(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS FAR * pDispParams, VARIANT FAR * pVarResult, EXCEPINFO FAR * pExcepInfo, unsigned int FAR * puArgErr);

protected:
	CFingerPrint * m_pOwner;
};

enum 
{
	FingerPrint_Indeterminate,
	FingerPrint_NotAvailable,
	FingerPrint_InProgress,
	FingerPrint_Available
};

enum 
{
	FingerPrint_ListChanged,
	FingerPrint_ArtistChanged,
	FingerPrint_AlbumChanged
};

class CFingerPrintUISink
{
public:
    virtual void SetTitle(LPCWSTR wcVal)=0;
    virtual void SetArtist(LPCWSTR wcVal)=0;
    virtual void SetDuration(LPCWSTR wcVal)=0;
};

class CFingerPrint  
{
public:
	CFingerPrint();
	virtual ~CFingerPrint();

public:
	HRESULT InitEngine(CFingerPrintUISink * pSink);
	void	TermEngine();
	
	HRESULT StartProcessing(LPCWSTR wcUrl, LPCWSTR wcSrcType, LPCWSTR wcSrcName, LPCWSTR wcId);
	bool    KillProcessing();

	void StatusChanged(long lTrack, long lStatus);
	void IndetifyComplete(long lTrack, long lStatus);

protected:
	bool CheckCanIdentifyCD();
	void CleanProcessing();
	void SetStatus(long lStatus);
	void ProcessFiles(LPCWSTR wcUrl);
	void ProcessCD(LPCWSTR strSource);

protected:
	CComPtr<MFCBID::IMFCBID> m_pEngine;
	CComPtr<IDispatch> m_pSink;
	DWORD m_dwSinkCookie;
	CFingerPrintUISink* m_pSinkUI;
	long m_lStatus;
	bool m_bAudioCD;
	bool m_bCancelMode;
    HMODULE m_hCbid;
};
