#include "StdAfx.h"
#include "FP.h"

#include <iostream>
#include <stdlib.h>
#include <string>


STDMETHODIMP CFingerPrintMfDbIdSink::Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS FAR * pDispParams, VARIANT FAR * pVarResult, EXCEPINFO FAR * pExcepInfo, unsigned int FAR * puArgErr)
{
    if (dispIdMember == 0x01)
    {
        m_pOwner->StatusChanged(pDispParams->rgvarg[1].lVal, pDispParams->rgvarg[0].lVal);
    }
    else if (dispIdMember == 0x02)
    {
        m_pOwner->IndetifyComplete(pDispParams->rgvarg[1].lVal, pDispParams->rgvarg[0].lVal);
    }
    return S_OK;
}


CFingerPrint::CFingerPrint()
{
	m_dwSinkCookie = 0;
	m_lStatus = FingerPrint_Indeterminate;	
	m_pSinkUI = NULL;
    m_bAudioCD = false;
	m_bCancelMode = false;
    m_hCbid = 0;
}

CFingerPrint::~CFingerPrint()
{
    if (m_hCbid) ::FreeLibrary(m_hCbid);
}

HRESULT CFingerPrint::InitEngine(CFingerPrintUISink * pSink)
{
	m_pSinkUI = pSink;

	SetStatus(FingerPrint_Available);

	HRESULT hr = E_FAIL;
	{
		if (!m_pEngine)
        {
            TCHAR szPath[MAX_PATH];
            ::GetModuleFileName(g_hModule, szPath, MAX_PATH);
            tstring strPath = szPath;
            tstring::size_type nPos = strPath.rfind(_T('\\'));
            if (nPos < 0) nPos = 0;
            strPath.erase(nPos+1);
            strPath += _T("cbid.dll");
            m_hCbid = ::LoadLibrary(strPath.c_str()); 
            if (!m_hCbid) return E_FAIL;

            typedef HRESULT (__stdcall * LPFUNC)(REFCLSID, REFIID, LPVOID*);
            LPFUNC pFunc = 0;
            pFunc = (LPFUNC)::GetProcAddress(m_hCbid, _T("DllGetClassObject"));
            if (pFunc == NULL) return E_FAIL;

            CComPtr<IClassFactory> pCF;
            hr = pFunc(__uuidof(MFCBID::MFCBID), IID_IClassFactory, (void**)&pCF);
            if (!pCF) return hr;

            hr = pCF->CreateInstance(0, __uuidof(MFCBID::IMFCBID), (void**)&m_pEngine);
            if (!m_pEngine) return hr;
        }

		if (!m_pSink)
		{
			CComObject<CFingerPrintMfDbIdSink> * pSink = 0;
            pSink = new CComObject<CFingerPrintMfDbIdSink>;
			pSink->Init(this);
			pSink->QueryInterface(IID_IDispatch, (void**)&m_pSink);
			
			AtlAdvise(m_pEngine, m_pSink, __uuidof(MFCBID::_IMFCBIDEvents), &m_dwSinkCookie);
		}

		hr = S_OK;
	}

	return hr;
}

HRESULT CFingerPrint::StartProcessing(LPCWSTR wcUrl, LPCWSTR wcSrcType, LPCWSTR wcSrcName, LPCWSTR wcId)
{
	if ((wcscmp(wcSrcType,L"AudioCD")==0 || wcscmp(wcSrcType,L"CDText")==0) && !CheckCanIdentifyCD())
    {
		 return S_FALSE;
    }

    try
	{
		KillProcessing();
		m_bCancelMode = false;
		
		if (wcscmp(wcSrcType,L"FileList")==0)
		{
			SetStatus(FingerPrint_InProgress);
			ProcessFiles(wcUrl);
    		m_pEngine->StartIdentify(0, 0);
		}
		
		if (wcscmp(wcSrcType,L"AudioCD")==0 || wcscmp(wcSrcType,L"CDText")==0)
		{
			SetStatus(FingerPrint_InProgress);
			ProcessCD(wcSrcName);
		    m_pEngine->StartIdentify(_wtoi(wcId), 1);
		}
	}
	catch(...)
	{
		KillProcessing();
	}
	return S_OK;
}

bool CFingerPrint::KillProcessing()
{
	if (m_bCancelMode) return false;
	{
		if (m_lStatus == FingerPrint_InProgress)
		{
			m_bCancelMode = true;
			m_pEngine->StopIdentify();
		}

		CleanProcessing();

		if (m_pEngine)
        {
			m_pEngine->DeInit();
        }
	}

	m_bCancelMode = false;
	SetStatus(FingerPrint_Available);
	return true;
}

void CFingerPrint::TermEngine()
{
	if (m_dwSinkCookie != 0 && m_pSink.p != NULL)
	{
		AtlUnadvise(m_pEngine, __uuidof(MFCBID::_IMFCBIDEvents), m_dwSinkCookie);
		m_dwSinkCookie = 0;
		m_pSink.Release();
	}

	m_pEngine.Release();
	SetStatus(FingerPrint_Indeterminate);
}

bool CFingerPrint::CheckCanIdentifyCD()
{
	VARIANT_BOOL bResult;
	m_pEngine->ImpersonateToIdentifyCD(&bResult);
	return (bResult != VARIANT_FALSE);
}

void CFingerPrint::CleanProcessing()
{
	SetStatus(FingerPrint_Available);
}

void CFingerPrint::StatusChanged(long lTrack, long lStatus)
{
}

void CFingerPrint::IndetifyComplete(long lTrack, long lStatus)
{
	try
	{
		if (lTrack < 1) return;

		if (lStatus == 0) //identified
		{

			CComPtr<MFCBID::IMFCBIDTracks> pCbTracks;
			m_pEngine->QueryInterface(__uuidof(MFCBID::IMFCBIDTracks), (void**)&pCbTracks);

			CComPtr<MFCBID::IMFCBIDTrack> pTrack;
			pCbTracks->get_Item(lTrack - 1, &pTrack);

            CComBSTR bstrTrackName;
			pTrack->get_SongName(&bstrTrackName);
            m_pSinkUI->SetTitle(bstrTrackName);

            CComBSTR bstrTrackArtist;
			pTrack->get_ArtistName(&bstrTrackArtist);
            m_pSinkUI->SetArtist(bstrTrackArtist);

            CComBSTR bstrDuration;
			pTrack->get_Duration(&bstrDuration);
            m_pSinkUI->SetDuration(bstrDuration);
		}

        KillProcessing();
        ::PostQuitMessage(0);
	}
    catch(...){}
}

void CFingerPrint::ProcessFiles(LPCWSTR wcUrl)
{
	m_bAudioCD = false;

	SAFEARRAYBOUND bounds[1];
	bounds[0].lLbound  = 0; 
	bounds[0].cElements = 1;
	SAFEARRAY * pSAValues = ::SafeArrayCreate(VT_BSTR, 1, bounds); 

    long lTrack = 0;
	::SafeArrayPutElement(pSAValues, &lTrack, CComBSTR(wcUrl).Copy());

	m_pEngine->InitFileList(&pSAValues);

	::SafeArrayDestroy(pSAValues); 
}

void CFingerPrint::ProcessCD(LPCWSTR strSource)
{
	m_bAudioCD = true;
	m_pEngine->InitCD(CComVariant(strSource));
}

void CFingerPrint::SetStatus(long lStatus)
{
	if (m_bCancelMode) return;
	m_lStatus = lStatus;
}
