#include "stdafx.h"
#include "FingerPrintHelper.h"

class CFPModule : public CAtlDllModuleT<CFPModule> {};
CFPModule _AtlModule;


CFingerPrintHelper * g_pHelper = 0;
HMODULE              g_hModule = 0;


BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
	g_hModule = (HMODULE)hModule;
    return TRUE;
}

extern "C"
BOOL CALLBACK  GetInfo(BSTR * pbstrInfo)
{
	CComBSTR bstr = 
		L"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
		L"<ModuleInfo Version=\"1.0.0.1\" Name=\"FingerPrintModule\" Description=\"FingerPrint Support Module\" Filename=\"modules\\fp.dll\">"
		L"  <Commands>"
		L"    <Command Name=\"FP_PROCESS_PLAYLIST\" />"
		L"  </Commands>"
		L"</ModuleInfo>";
	*pbstrInfo = bstr.Detach();
	return TRUE;
}

extern "C"
BOOL CALLBACK Init(BOOL bInit)
{
	if(bInit)
	{
		g_pHelper = new CFingerPrintHelper();
	}
	else
	{
		if (g_pHelper) delete g_pHelper;
	}
	return TRUE;
}

extern "C"
BOOL CALLBACK Invoke(BSTR bstrRequest, BSTR * pbstrResponse)
{
	try
	{
		CComPtr<MSXML2::IXMLDOMElement> pRequestEl;
		CXmlHelpers::LoadXml(bstrRequest, &pRequestEl);

		CComBSTR bstrCmd;
		CXmlHelpers::GetAttribute(pRequestEl, L"Name", &bstrCmd);
		if (wcsicmp(bstrCmd, L"FP_PROCESS_PLAYLIST") == 0)
		{
			return g_pHelper->ProcessPlayList(bstrRequest, pbstrResponse);
		}
		else
		{
			return FALSE;
		}
	}
	catch(...) {return FALSE;}
	return TRUE;
}

