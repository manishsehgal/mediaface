#pragma once
#include "FP.h"


class CFingerPrintHelper : public CFingerPrintUISink
{
private:
    wstring m_strSrcType;
    wstring m_strSrcName;

    wstring m_strUrl;
    wstring m_strId;

    wstring m_strTitle;
    wstring m_strArtist;
    wstring m_strDuration;

public:
	BOOL ProcessPlayList(BSTR bstrRequest, BSTR * pbstrResponse);

    void SetTitle(LPCWSTR wcVal);
    void SetArtist(LPCWSTR wcVal);
    void SetDuration(LPCWSTR wcVal);

    static void FPProc(void * pArg);
    void do_FP();

private:
    bool processTrack(MSXML2::IXMLDOMElement * pTrackElem, wstring strSelId);
};