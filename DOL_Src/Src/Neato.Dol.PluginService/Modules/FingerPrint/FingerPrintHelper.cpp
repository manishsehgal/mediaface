#include "stdafx.h"
#include <PlayListEx.h>
#include "FingerPrintHelper.h"
#include "FP.h"


BOOL CFingerPrintHelper::ProcessPlayList(BSTR bstrRequest, BSTR * pbstrResponse)
{
	BOOL    bRes = TRUE;
    HRESULT hRes = 0;
	try
	{
		//=============================
		//Parse request xml
		CComPtr<MSXML2::IXMLDOMElement>  pReqElem;
		HRESULT hRes = CXmlHelpers::LoadXml(bstrRequest, &pReqElem);
		if (hRes != S_OK) throw 1;

        CComPtr<MSXML2::IXMLDOMElement> pParamsElem;
        hRes = CXmlHelpers::GetChildElement(pReqElem, L"Params", &pParamsElem);
        if (hRes != S_OK) throw 2;

        wstring strSelId = L"";
        CComPtr<MSXML2::IXMLDOMElement> pSelElem;
        hRes = CXmlHelpers::GetChildElement(pParamsElem, L"Selection", &pSelElem);
        if (hRes == S_OK)
        {
            CComBSTR bstrSelId;
            hRes = CXmlHelpers::GetAttribute(pSelElem, L"Ids", &bstrSelId);
            if (hRes == S_OK) strSelId = bstrSelId;
        }

        CComPtr<MSXML2::IXMLDOMElement> pPlayListElem;
        hRes = CXmlHelpers::GetChildElement(pParamsElem, L"PlayList", &pPlayListElem);
        if (hRes != S_OK) throw 3;

        m_strSrcType = L"FileList";
        CComBSTR bstrSrcType;
        hRes = CXmlHelpers::GetAttribute(pPlayListElem, L"SrcType", &bstrSrcType);
        if (hRes == S_OK) m_strSrcType = bstrSrcType;

        m_strSrcName = L"";
        CComBSTR bstrSrcName;
        hRes = CXmlHelpers::GetAttribute(pPlayListElem, L"SrcName", &bstrSrcName);
        if (hRes == S_OK) m_strSrcName = bstrSrcName;

        CComPtr<MSXML2::IXMLDOMElement> pTracksElem;
        hRes = CXmlHelpers::GetChildElement(pPlayListElem, L"Tracks", &pTracksElem);
        if (hRes != S_OK) throw 4;

        CComPtr<MSXML2::IXMLDOMElement> pTrackElem;
        CXmlHelpers::GetChildElement(pTracksElem, L"Track", &pTrackElem);
        while (pTrackElem)
        {
            processTrack(pTrackElem, strSelId);

            CComPtr<MSXML2::IXMLDOMElement> pNextElem;
            CXmlHelpers::GetNextSiblingElement(pTrackElem, &pNextElem);
            pTrackElem = pNextElem;
        }

		CComBSTR bstrPlayListXml;
        pPlayListElem->get_xml(&bstrPlayListXml);

		//=============================
		// Prepare response xml
		CComPtr<MSXML2::IXMLDOMElement>  pRespElem;
		hRes = CXmlHelpers::CreateDocument(CComBSTR(L"Response"), &pRespElem);
        if (hRes != S_OK) throw 5;

        hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"0");
        if (hRes != S_OK) throw 6;

        CComPtr<MSXML2::IXMLDOMElement> pRParamsElem;
		hRes = CXmlHelpers::AddElement(pRespElem, CComBSTR(L"Params"), &pRParamsElem);
        if (hRes != S_OK) throw 7;

		//=============================
		CComPtr<MSXML2::IXMLDOMElement> pPLElem;
		hRes = CXmlHelpers::LoadXml(bstrPlayListXml, &pPLElem);
		if (hRes != S_OK) throw 8;
		CComPtr<MSXML2::IXMLDOMNode> pNewPLNode;
		pRParamsElem->appendChild(pPLElem, &pNewPLNode);

		hRes = pRespElem->get_xml(pbstrResponse);
		if (hRes != S_OK) throw 9;
	}
    catch (int)
    {
		bRes = FALSE;
    }
    catch (...){bRes = FALSE;}

	return bRes;
}

bool CFingerPrintHelper::processTrack(MSXML2::IXMLDOMElement * pTrackElem, wstring strSelId)
{
    HRESULT hRes;
    if (!pTrackElem) return false;

    //get and check track url
    m_strUrl = L"";
    CComBSTR bstrUrl;
    hRes = CXmlHelpers::GetAttribute(pTrackElem, L"Url", &bstrUrl);
    if (hRes == S_OK) m_strUrl = bstrUrl;

    //get and check track id
    m_strId = L"0";
    CComBSTR bstrId;
    hRes = CXmlHelpers::GetAttribute(pTrackElem, L"Id", &bstrId);
    if (hRes == S_OK) m_strId = bstrId;

    if (strSelId.size() > 0 && wcscmp(m_strId.c_str(), strSelId.c_str()) != 0) return false;

    m_strArtist   = L"";
    m_strTitle    = L"";
    m_strDuration = L"";

    HANDLE hFPThread = (HANDLE)_beginthread(FPProc, 0, (void*)this);
    ::WaitForSingleObject(hFPThread, 600000);//10 min

    if (m_strArtist.size() > 0)
    {
        CXmlHelpers::SetAttribute(pTrackElem, L"Artist", m_strArtist.c_str());
    }
    if (m_strTitle.size() > 0)
    {
        CXmlHelpers::SetAttribute(pTrackElem, L"Title", m_strTitle.c_str());
    }
    if (m_strDuration.size() > 0)
    {
        CXmlHelpers::SetAttribute(pTrackElem, L"Duration", m_strDuration.c_str());
    }

    return true;
}

void CFingerPrintHelper::FPProc(void * pArg)
{
    CFingerPrintHelper * fph = (CFingerPrintHelper*)pArg;
    fph->do_FP();
}

void CFingerPrintHelper::do_FP()
{
    ::CoInitialize(0);
    {
        CFingerPrint m_objFP;
    	if (m_objFP.InitEngine(this) != S_OK) return;
        m_objFP.StartProcessing(m_strUrl.c_str(), m_strSrcType.c_str(), m_strSrcName.c_str(), m_strId.c_str());

	    MSG msg;
	    while(::GetMessage(&msg, 0, 0, 0)) ::DispatchMessage(&msg);

        m_objFP.TermEngine();
    }
    ::CoUninitialize();
}

void CFingerPrintHelper::SetTitle(LPCWSTR wcVal)
{
    m_strTitle = wcVal;
}

void CFingerPrintHelper::SetArtist(LPCWSTR wcVal)
{
    m_strArtist = wcVal;
}

void CFingerPrintHelper::SetDuration(LPCWSTR wcVal)
{
    m_strDuration = wcVal;
}

