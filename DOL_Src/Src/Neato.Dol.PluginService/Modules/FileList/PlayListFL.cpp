#include "stdafx.h"
#include "PlayListFL.h"
#include "FolderContent.h"
#include "ID3Reader.h"
#include "TxtCnv.h"

//====================================================

CPlayListFL::CPlayListFL() : CPlayListEx()
{
    m_strSrcType = L"FileList";
}

bool CPlayListFL::AddFile(LPCWSTR wcFile, bool bAddSubFiles, int nPos, int * pnPos, int * pnCount)
{
    if (::GetFileAttributesW(wcFile) == INVALID_FILE_ATTRIBUTES) return false;

    WCHAR szExt[MAX_PATH] = {0};
    _wsplitpath(wcFile, 0, 0, 0, szExt);

    if (wcsicmp(szExt, EXT_M3U) == 0)
    {
        CPlayListFL pl;
		parseM3U(wcFile, pl);
        AddPlayList(pl, nPos);
    }
    else if (wcsicmp(szExt, EXT_PLS) == 0)
    {
        CPlayListFL pl;
		parsePLS(wcFile, pl);
        AddPlayList(pl, nPos);
    }
    else if (wcsicmp(szExt, EXT_ASX) == 0)
    {
        CPlayListFL pl;
		parseASX(wcFile, pl);
        AddPlayList(pl, nPos);
    }
	else if (wcsicmp(szExt, EXT_WPL) == 0)
	{
        CPlayListFL pl;
		parseWPL(wcFile, pl);
        AddPlayList(pl, nPos);
	}
    else if (wcsicmp(szExt, EXT_MP3) == 0)
    {
        CPlayListFL pl;
        pl.AddEmptyTrack();
        if (!parseMP3(wcFile, pl.GetTrack(0))) return false;
        AddPlayList(pl, nPos);
    }
    else
    {
        //not supported extension
        return false;
    }
    return true;
}

bool CPlayListFL::AddFolder(LPCWSTR wcFolder, int nFlags, int nPos, int * pnPos, int * pnCount)
{
    CFolderContent fc;
    if (!fc.Init(wcFolder, nFlags)) return false;

    vector<wstring> vFiles = fc.GetFiles();
    for (UINT i=0; i<vFiles.size(); i++)
    {
        wstring fn = fc.GetPath() + vFiles[i];
        AddFile(fn.c_str(), true);
    }

    if (nFlags & FFFD)
    {
        vector<wstring> vFolders = fc.GetFolders();
        for (UINT i=0; i<vFolders.size(); i++)
        {
            wstring fn = fc.GetPath() + vFolders[i];
            AddFolder(fn.c_str(), nFlags);
        }
    }

    return true;
}

bool CPlayListFL::AddPlayList(CPlayListFL & pl, int nPos)
{
    if (nPos < 0 || nPos >= (int)m_vTracks.size()) 
    {
        nPos = (int)m_vTracks.size();
    }
    m_vTracks.insert(m_vTracks.begin()+nPos, pl.m_vTracks.begin(), pl.m_vTracks.end());
    recalcDuration();
    return true;
}

bool CPlayListFL::parseMP3(LPCWSTR wcFile, CPlayListTrackEx & track)
{
    CID3Reader * rd = 0;

    try
    {
        //================
        //Url
        track.SetUrl(wcFile);

        //================
        //Try to parse mp3
        rd = CID3Reader::CreateInstance();
        HRESULT hRes = rd->Init(wcFile);
        if (hRes != S_OK) {throw 0;}

        //================
        //Title
        CID3Tag * pTitleTag = 0;
        rd->get_tagID3v2byID(ID3_TITLE, &pTitleTag);
        if (!pTitleTag)
        {
            rd->get_tagID3v1byID(ID3V1_TITLE, &pTitleTag);
        }
        if (pTitleTag)
        {
            CComBSTR bstrTitle;
            hRes = pTitleTag->get_Content(&bstrTitle);
            if (hRes == S_OK)
            {
                track.SetTitle(bstrTitle);
            }
            delete pTitleTag;
        }
        else
        {
            WCHAR szFN[MAX_PATH] = {0};
            _wsplitpath(wcFile, 0, 0, szFN, 0);
            track.SetTitle(szFN);
        }

        //================
        //Artist
        CID3Tag * pArtistTag = 0;
        if (!pArtistTag)
            rd->get_tagID3v2byID(ID3_LEADARTIST, &pArtistTag);
        if (!pArtistTag)
            rd->get_tagID3v2byID(ID3_ORIGARTIST, &pArtistTag);
        if (!pArtistTag)
            rd->get_tagID3v2byID(ID3_MIXARTIST, &pArtistTag);
        if (!pArtistTag)
            rd->get_tagID3v2byID(ID3_WWWARTIST, &pArtistTag);
        if (!pArtistTag)
            rd->get_tagID3v1byID(ID3V1_ARTIST, &pArtistTag);
        if (pArtistTag)
        {
            CComBSTR bstrArtist;
            hRes = pArtistTag->get_Content(&bstrArtist);
            if (hRes == S_OK)
            {
                track.SetArtist(bstrArtist);
            }
            delete pArtistTag;
        }

        //================
        //Album
        CID3Tag * pAlbumTag = 0;
        rd->get_tagID3v2byID(ID3_ALBUM, &pAlbumTag);
        if (pAlbumTag)
        {
            CComBSTR bstrAlbum;
            hRes = pAlbumTag->get_Content(&bstrAlbum);
            if (hRes == S_OK)
            {
                track.SetAlbum(bstrAlbum);
            }
            delete pAlbumTag;
        }

        //================
        //Duration
        long lDuration = 0;
        rd->get_Duration(&lDuration);
        track.SetDuration(lDuration/1000);
    }
    catch (int)
    {
        if (rd) delete rd;
        return false;
    }

    if (rd) delete rd;
    return true;
}

bool CPlayListFL::parseM3U(LPCWSTR wcFile, CPlayListFL & pl)
{
    FILE * fp = _wfopen(wcFile, L"rb");
    if (!fp) return false;

    long lDuration = 0;
    wstring strTitle;

    char entry[_MAX_PATH*2] = {0};
    while(fgets(entry, _MAX_PATH*2, fp))
	{
		if (iscntrl((unsigned char)entry[0]))
			continue;
        // get rid of nasty trailing whitespace
        for (int i=(int)strlen(entry)-1; i>=0; i--)
        {
			if(isspace((unsigned char)entry[i])) entry[i] = 0;
			else break;
		}

        // is this a comment line?
        if(entry[0] == '#')
        {
            LPSTR pszExInfo = strstr(entry, "#EXTINF:");
            if (pszExInfo != NULL)
            {
                pszExInfo += 8;
				sscanf(pszExInfo, "%ld", &lDuration);
                LPSTR psz = strstr(pszExInfo, _T(","));
                if (psz != NULL) strTitle = CTxtCnv().a2w(psz+1);
			}
			continue;
		}
        else
        {
            wstring strPath;
		    if (normalizePath(wcFile, CTxtCnv().a2w(entry), strPath) == true)
            {
                if (!pl.AddFile(strPath.c_str(), false))
                {
                    int nPos = pl.AddEmptyTrack();
                    CPlayListTrackEx & track = pl.GetTrack(nPos);
                    track.SetTitle(strTitle.c_str());
                    track.SetDuration(lDuration);
                }
            }
        }
    }

    fclose(fp);
    return true;
}

bool CPlayListFL::parsePLS(LPCWSTR wcFile, CPlayListFL & pl)
{
    return false;
}

bool CPlayListFL::parseASX(LPCWSTR wcFile, CPlayListFL & pl)
{
	CComBSTR bstrPlaylistData;

	FILE * fp = _wfopen(wcFile, L"r");

	if (fp == NULL)
		return false;

	wchar_t buffer[1024];
	wstring str;
	while(fgetws(buffer, 1024, fp))
		str += wstring(buffer);

	fclose(fp);

	int index = 0;
	wstring::size_type npos = -1;
	while ((index = str.find(L"&", index)) != npos) {
		if (str.find(L"&amp;", index) != 0) {
			str = str.replace(index++, 1, L"&amp;");
		}
	}

	bstrPlaylistData.Append(str.c_str());

	CComPtr<MSXML2::IXMLDOMElement>  pPlaylistElem;
	HRESULT hRes = CXmlHelpers::LoadXml(bstrPlaylistData, &pPlaylistElem);
	if (hRes != S_OK) throw 1;

	CComPtr<MSXML2::IXMLDOMNodeList>  pEntries;
	hRes = pPlaylistElem->selectNodes(L"Entry|ENTRY|entry", &pEntries);
	if (hRes != S_OK) throw 2;

	long length = 0;
	hRes = pEntries->get_length(&length);
	for(int i = 0; i < length; ++i)
	{
		CComPtr<MSXML2::IXMLDOMNode>  pEntryElem;
		hRes = pEntries->get_item(i, &pEntryElem);
		if (hRes != S_OK) throw 3;

		CComPtr<MSXML2::IXMLDOMNode>  pHrefElem;
		hRes = pEntryElem->selectSingleNode(L"(Ref|REF|ref)/(@Href|@HREF|@href)", &pHrefElem);
		if (hRes != S_OK) throw 4;

		CComBSTR bstrHref;
		hRes = pHrefElem->get_text(&bstrHref);
		if (hRes != S_OK) throw 5;

        wstring strPath;
		if (normalizePath(wcFile, bstrHref, strPath) == true)
        {
            if (!pl.AddFile(strPath.c_str(), false))
            {
                int nPos = pl.AddEmptyTrack();
                CPlayListTrackEx & track = pl.GetTrack(nPos);

				CComPtr<MSXML2::IXMLDOMNode>  pTitleElem;
				hRes = pEntryElem->selectSingleNode(L"Title|TITLE|title", &pTitleElem);
				if (hRes == S_OK)
				{
					CComBSTR bstrValue;
					pTitleElem->get_text(&bstrValue);
					track.SetTitle(bstrValue);
				}
				else
				{
					wstring filename;
					if(GetFileName(bstrHref, filename))
						track.SetTitle(filename.c_str());
					else
						track.SetTitle(bstrHref);
				}
				
				CComPtr<MSXML2::IXMLDOMNode>  pAuthorElem;
				hRes = pEntryElem->selectSingleNode(L"Author|AUTHOR|author", &pAuthorElem);
				if (hRes == S_OK)
				{
					CComBSTR bstrValue;
					pAuthorElem->get_text(&bstrValue);
					track.SetArtist(bstrValue);
				}

				CComPtr<MSXML2::IXMLDOMNode>  pDurationElem;
				hRes = pEntryElem->selectSingleNode(L"(Duration|DURATION|duration)/(@Value|@VALUE|@value)", &pDurationElem);
				if (hRes == S_OK)
				{
					CComBSTR bstrValue;
					pDurationElem->get_text(&bstrValue);
					long duration = ParseDuration(bstrValue);
					track.SetDuration(duration);
				}
			}
		}
	}

	CComPtr<MSXML2::IXMLDOMNodeList>  pOtherPlaylists;
	hRes = pPlaylistElem->selectNodes(L"(EntryRef|ENTRYREF|entryref)/(@Href|@HREF|@href)", &pOtherPlaylists);
	if (hRes == S_OK)
	{
		long length = 0;
		hRes = pOtherPlaylists->get_length(&length);
		for(int i = 0; i < length; ++i)
		{
			CComPtr<MSXML2::IXMLDOMNode>  pHrefElem;
			hRes = pOtherPlaylists->get_item(i, &pHrefElem);
			if (hRes == S_OK)
			{
				CComBSTR bstrValue;
				pHrefElem->get_text(&bstrValue);
				parseASX(wcFile, pl);
			}
		}
	}

	return true;
}

long CPlayListFL::ParseDuration(LPCWSTR value)
{
	wstring str(value);

	int hours = 0;
	int mins = 0;
	int secs = 0;
	int msecs = 0;

	swscanf(str.c_str(), L"%d:%d:%d.%d", &hours, &mins, &secs, &msecs);

	return secs + 60 * mins + 3600 * hours;
}

bool CPlayListFL::parseWPL(LPCWSTR wcFile, CPlayListFL & pl)
{
	CComBSTR bstrPlaylistData;

	FILE * fp = _wfopen(wcFile, L"r");

	if (fp == NULL)
		return false;

	wchar_t buffer[1024];
	while(fgetws(buffer, 1024, fp))
	{
		bstrPlaylistData.Append(buffer);
	}

	fclose(fp);

	CComPtr<MSXML2::IXMLDOMElement>  pPlaylistElem;
	HRESULT hRes = CXmlHelpers::LoadXml(bstrPlaylistData, &pPlaylistElem);
	if (hRes != S_OK) throw 1;

	CComPtr<MSXML2::IXMLDOMElement>  pBodyElem;
	hRes = CXmlHelpers::GetChildElement(pPlaylistElem, L"body", &pBodyElem);
	if (hRes != S_OK) throw 2;

	CComPtr<MSXML2::IXMLDOMElement>  pSeqElem;
	hRes = CXmlHelpers::GetChildElement(pBodyElem, L"seq", &pSeqElem);
	if (hRes != S_OK) throw 3;

	CComPtr<MSXML2::IXMLDOMElement>  pMediaElem;
	hRes = CXmlHelpers::GetChildElement(pSeqElem, L"media", &pMediaElem);
	if (hRes != S_OK) throw 4;

	while(pMediaElem != NULL)
	{
		CComBSTR bstrSrc;
		hRes = CXmlHelpers::GetAttribute(pMediaElem, L"src", &bstrSrc);
		if (hRes != S_OK) throw 5;

        wstring strPath;
		if (normalizePath(wcFile, bstrSrc, strPath) == true)
        {
            if (!pl.AddFile(strPath.c_str(), false))
            {
                int nPos = pl.AddEmptyTrack();
                CPlayListTrackEx & track = pl.GetTrack(nPos);
				wstring filename;
				if(GetFileName(bstrSrc, filename))
					track.SetTitle(filename.c_str());
				else
					track.SetTitle(bstrSrc);
                track.SetDuration((long)0);
            }
        }

		// Next
		CComPtr<MSXML2::IXMLDOMElement> pNextElem;
		hRes = CXmlHelpers::GetNextSiblingElement(pMediaElem, &pNextElem);
		pMediaElem = pNextElem;
	}

    return true;
}

bool CPlayListFL::GetFileName(LPCWSTR wcPath, wstring & strResult)
{
	wstring strPath(wcPath);
	int index = strPath.find_last_of('\\');
	if (index != -1) {
		strResult = strPath.substr(index + 1);
		return true;
	}
	return false;
}

bool CPlayListFL::normalizePath(LPCWSTR wcListFullPath, LPCWSTR wcPath, wstring & strResult)
{
	WCHAR drive[_MAX_DRIVE];
	WCHAR path[_MAX_PATH];
	_wsplitpath(wcListFullPath, drive, path, 0, 0);

	wstring strPath(wcPath);

	// check for presence of a drive letter or an unc path
	if (strPath.find(L":\\", 1) == 1 || strPath.find(L":/", 1) == 1 || strPath.find(L"\\\\") == 0)
	{
        std::replace(strPath.begin(), strPath.end(), L'/', L'\\');
	}
	else
	{
		// drive not present and not an UNC path
		// check for URL
        if (strPath.find(L":\\\\", 1) != wstring::npos  || strPath.find(L"://") != wstring::npos)
		{
			//URL is not supported
			return false;
		}
		
		if (strPath.size() > 0 && strPath[0] == L'\\')
		{
			wstring strNewPath = drive;
			strNewPath += strPath;
			strPath = strNewPath;
		}
		else
		{
			wstring strNewPath = drive;
			strNewPath += path;
			strNewPath += strPath;
			strPath = strNewPath;
		}

        std::replace(strPath.begin(), strPath.end(), L'/', L'\\');
	}

	strResult = strPath;
	return true;
}

