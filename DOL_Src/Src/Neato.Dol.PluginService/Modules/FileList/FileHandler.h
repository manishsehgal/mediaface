#pragma once

class CFileHandler
{
public:
    CFileHandler();

private:

public:
    BOOL CreatePlayList(BSTR bstrRequest, BSTR * pbstrResponse);
    BOOL GetFolderContent(BSTR bstrRequest, BSTR * pbstrResponse);

private:
};