#include "stdafx.h"
#include "FileHandler.h"
#include "PlayListFL.h"
#include "FolderContent.h"



CFileHandler::CFileHandler()
{
}

BOOL CFileHandler::CreatePlayList(BSTR bstrRequest, BSTR * pbstrResponse)
{
	BOOL bRes = TRUE;

	try
	{
		//=============================
		//Parse request xml
		CComPtr<MSXML2::IXMLDOMElement>  pReqElem;
		HRESULT hRes = CXmlHelpers::LoadXml(bstrRequest, &pReqElem);
		if (hRes != S_OK) throw 1;

        CComPtr<MSXML2::IXMLDOMElement> pParamsElem;
        hRes = CXmlHelpers::GetChildElement(pReqElem, L"Params", &pParamsElem);
        if (hRes != S_OK) throw 2;

        CPlayListFL playList;
        //parse files
        {
            CComPtr<MSXML2::IXMLDOMElement> pFileElem;
            CXmlHelpers::GetChildElement(pParamsElem, L"File", &pFileElem);
            while (pFileElem)
            {
                CComBSTR bstrFileName;
                hRes = CXmlHelpers::GetAttribute(pFileElem, L"Name", &bstrFileName);
                if (hRes == S_OK)
                {
                    playList.AddFile(bstrFileName, true);
                }

                CComPtr<MSXML2::IXMLDOMElement> pNextElem;
                CXmlHelpers::GetNextSiblingElement(pFileElem, &pNextElem);
                pFileElem = pNextElem;
            }
        }
        //parse folders
        {
            CComPtr<MSXML2::IXMLDOMElement> pFolderElem;
            CXmlHelpers::GetChildElement(pParamsElem, L"Folder", &pFolderElem);
            while (pFolderElem)
            {
                CComBSTR bstrFolderName;
                hRes = CXmlHelpers::GetAttribute(pFolderElem, L"Name", &bstrFolderName);
                CComBSTR bstrGetSubFolders;
                bool bGetSubFolders = false;
                if (CXmlHelpers::GetAttribute(pFolderElem, L"GetSubFolders", &bstrGetSubFolders) == S_OK)
                {
                    bGetSubFolders = (wcscmp(bstrGetSubFolders, L"1") == 0 || wcsicmp(bstrGetSubFolders, L"true") == 0);
                }

                if (hRes == S_OK)
                {
                    playList.AddFolder(bstrFolderName, FFSF|(bGetSubFolders?FFFD:0));
                }

                CComPtr<MSXML2::IXMLDOMElement> pNextElem;
                CXmlHelpers::GetNextSiblingElement(pFolderElem, &pNextElem);
                pFolderElem = pNextElem;
            }
        }

		//=============================
		// Prepare response xml
		CComPtr<MSXML2::IXMLDOMElement>  pRespElem;
		hRes = CXmlHelpers::CreateDocument(CComBSTR(L"Response"), &pRespElem);
        if (hRes != S_OK) throw 8;

        hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"0");
        if (hRes != S_OK) throw 9;

        CComPtr<MSXML2::IXMLDOMElement> pRParamsElem;
		hRes = CXmlHelpers::AddElement(pRespElem, CComBSTR(L"Params"), &pRParamsElem);
        if (hRes != S_OK) throw 10;

		//=============================
		CComBSTR bstrPLXml;
        playList.GetXml(&bstrPLXml);

		CComPtr<MSXML2::IXMLDOMElement> pPLElem;
		hRes = CXmlHelpers::LoadXml(bstrPLXml, &pPLElem);
		if (hRes != S_OK) throw 13;
		CComPtr<MSXML2::IXMLDOMNode> pNewPLNode;
		pRParamsElem->appendChild(pPLElem, &pNewPLNode);

		hRes = pRespElem->get_xml(pbstrResponse);
		if (hRes != S_OK) throw 16;
    }
    catch (int)
    {
		bRes = FALSE;
    }
    catch (...){bRes = FALSE;}

	return bRes;
}

BOOL CFileHandler::GetFolderContent(BSTR bstrRequest, BSTR * pbstrResponse)
{
	BOOL bRes = TRUE;

	try
	{
		//=============================
		//Parse request xml
		CComPtr<MSXML2::IXMLDOMElement>  pReqElem;
		HRESULT hRes = CXmlHelpers::LoadXml(bstrRequest, &pReqElem);
		if (hRes != S_OK) throw 1;

        CComPtr<MSXML2::IXMLDOMElement> pParamsElem;
        hRes = CXmlHelpers::GetChildElement(pReqElem, L"Params", &pParamsElem);
        if (hRes != S_OK) throw 2;

        CComPtr<MSXML2::IXMLDOMElement> pFolderElem;
        hRes = CXmlHelpers::GetChildElement(pParamsElem, L"Folder", &pFolderElem);
        if (hRes != S_OK) throw 3;

        CComBSTR bstrPath;
        hRes = CXmlHelpers::GetAttribute(pFolderElem, L"Path", &bstrPath);
        if (hRes != S_OK) throw 4;

        CComBSTR bstrGetFolders;
        bool bGetFolders = true;
        hRes = CXmlHelpers::GetAttribute(pFolderElem, L"GetFolders", &bstrGetFolders);
        if (hRes == S_OK)
        {
            bGetFolders = wcscmp(bstrGetFolders, L"1") == 0;
        }

        CComBSTR bstrGetFiles;
        bool bGetFiles = true;
        hRes = CXmlHelpers::GetAttribute(pFolderElem, L"GetFiles", &bstrGetFiles);
        if (hRes == S_OK)
        {
            bGetFiles = wcscmp(bstrGetFiles, L"1") == 0;
        }

		//=============================
		// Prepare response xml
		CComPtr<MSXML2::IXMLDOMElement>  pRespElem;
		hRes = CXmlHelpers::CreateDocument(CComBSTR(L"Response"), &pRespElem);
        if (hRes != S_OK) throw 8;

        hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"0");
        if (hRes != S_OK) throw 9;

        CComPtr<MSXML2::IXMLDOMElement> pRParamsElem;
		hRes = CXmlHelpers::AddElement(pRespElem, CComBSTR(L"Params"), &pRParamsElem);
        if (hRes != S_OK) throw 10;

		//=============================

        CFolderContent fc;
        if (fc.Init(bstrPath, FFFD|FFPL|FFSF))
        {
    		CComBSTR bstrFCXml;
            if (fc.GetXml(&bstrFCXml))
            {
        		CComPtr<MSXML2::IXMLDOMElement> pFCElem;
		        hRes = CXmlHelpers::LoadXml(bstrFCXml, &pFCElem);
		        if (hRes != S_OK) throw 13;
		        CComPtr<MSXML2::IXMLDOMNode> pNewFCNode;
		        pRParamsElem->appendChild(pFCElem, &pNewFCNode);
            }
        }

		hRes = pRespElem->get_xml(pbstrResponse);
		if (hRes != S_OK) throw 16;
    }
    catch (int)
    {
		bRes = FALSE;
    }
    catch (...){bRes = FALSE;}

	return bRes;
}


