#include "stdafx.h"
#include "FileHandler.h"


CFileHandler * g_pFileHandler = 0;
HMODULE        g_hModule      = 0;


BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
	g_hModule = (HMODULE)hModule;
    return TRUE;
}

extern "C"
BOOL CALLBACK  GetInfo(BSTR * pbstrInfo)
{
	CComBSTR bstr = 
		L"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
		L"<ModuleInfo Version=\"1.0.0.1\" Name=\"FileListModule\" Description=\"Module for import playlists from files\" Filename=\"modules\\filelist.dll\">"
		L"    <Commands>"
		L"		<Command Name=\"FILELIST_GET_PLAYLIST\" />"
		L"		<Command Name=\"FILELIST_GET_FOLDER_CONTENT\" />"
		L"	</Commands>"
		L"</ModuleInfo>";
	*pbstrInfo = bstr.Detach();
	return TRUE;
}

extern "C"
BOOL CALLBACK Init(BOOL bInit)
{
    if (g_pFileHandler) {
        delete g_pFileHandler; 
        g_pFileHandler=0;
    }

	if(bInit)
	{
		g_pFileHandler = new CFileHandler();
	}
	return TRUE;
}

extern "C"
BOOL CALLBACK Invoke(BSTR bstrRequest, BSTR * pbstrResponse)
{
	try
	{
		CComPtr<MSXML2::IXMLDOMElement> pRequestEl;
		CXmlHelpers::LoadXml(bstrRequest, &pRequestEl);

		CComBSTR bstrCmd;
		CXmlHelpers::GetAttribute(pRequestEl, L"Name", &bstrCmd);
		if (wcsicmp(bstrCmd, L"FILELIST_GET_PLAYLIST") == 0)
		{
			return g_pFileHandler->CreatePlayList(bstrRequest, pbstrResponse);
		}
		else if (wcsicmp(bstrCmd, L"FILELIST_GET_FOLDER_CONTENT") == 0)
		{
			return g_pFileHandler->GetFolderContent(bstrRequest, pbstrResponse);
		}
		else
		{
			return FALSE;
		}
	}
	catch(...) {return FALSE;}
	return TRUE;
}


