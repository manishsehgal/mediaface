#pragma once
#include <PlayListEx.h>


//==================================
class CPlayListFL : public CPlayListEx
{
public:
    CPlayListFL();

public:
    bool AddFile(LPCWSTR wcFile, bool bAddSubFiles, int nPos = -1, int * pnPos = 0, int * pnCount = 0);
    bool AddFolder(LPCWSTR wcFolder, int nFlags, int nPos = -1, int * pnPos = 0, int * pnCount = 0);
    bool AddPlayList(CPlayListFL & pl, int nPos = -1);

protected:
    bool parseM3U(LPCWSTR wcFile, CPlayListFL & pl);
    bool parsePLS(LPCWSTR wcFile, CPlayListFL & pl);
    bool parseASX(LPCWSTR wcFile, CPlayListFL & pl);
    bool parseWPL(LPCWSTR wcFile, CPlayListFL & pl);
    bool parseMP3(LPCWSTR wcFile, CPlayListTrackEx & track);
    bool normalizePath(LPCWSTR wcListFullPath, LPCWSTR wcPath, wstring & strResult);
	bool GetFileName(LPCWSTR wcPath, wstring & strResult);
	long ParseDuration(LPCWSTR bstrValue);
};