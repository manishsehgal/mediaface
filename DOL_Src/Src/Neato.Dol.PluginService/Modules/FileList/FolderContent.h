#pragma once


class CFolderContent
{
public:
    CFolderContent();

private:
    wstring m_strPath;
    vector<wstring> m_vFolders;
    vector<wstring> m_vFiles;

public:
    bool Init(LPCWSTR wcPath, int nFlags);
    wstring GetPath();
    vector<wstring> GetFolders();
    vector<wstring> GetFiles();
    bool GetXml(BSTR * bstrXml);

private:
    bool checkExt(LPCWSTR wcfn, int nFlags);

private:
    //predicat for case-insensive sorting wstring's
    class CStrLess { public: bool operator()(wstring & str1, wstring & str2) { return (wcsicmp(str1.c_str(), str2.c_str()) < 0); } };
};
