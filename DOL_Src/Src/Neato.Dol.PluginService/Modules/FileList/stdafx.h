#pragma once

#include <windows.h>
#include <atlbase.h>
#include <msxml.h>

#include <string>
#include <vector>
#include <algorithm>
using namespace std;

typedef basic_string<TCHAR> tstring;

#import <msxml3.dll> raw_interfaces_only
#include <XmlHelpers.h>

//file extensions:
#define EXT_M3U L".m3u"
#define EXT_PLS L".pls"
#define EXT_ASX L".asx"
#define EXT_WPL L".wpl"
#define EXT_MP3 L".mp3"
#define EXT_WAV L".wav"

//filter flags:
#define FFPL 0x01 //play lists files
#define FFSF 0x02 //sound files
#define FFFD 0x04 //folders






