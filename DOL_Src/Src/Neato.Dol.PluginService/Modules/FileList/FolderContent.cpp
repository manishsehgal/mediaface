#include "stdafx.h"
#include "FolderContent.h"



CFolderContent::CFolderContent()
{
}

bool CFolderContent::Init(LPCWSTR wcPath, int nFlags)
{
    wstring strPath(wcPath);
    m_vFolders.clear();
    m_vFiles.clear();

    //replace right slashes
    std::replace(strPath.begin(), strPath.end(), L'/', L'\\');

    //if folder up needed
    wstring::size_type pos = strPath.rfind(L"\\..");
    if ((pos >= 0) && (pos != wstring::npos) && (pos == strPath.size()-3))
    {
        strPath.erase(pos);
        pos = strPath.rfind(L'\\');
        if (pos != wstring::npos)
        {
            strPath.erase(pos+1);
        }
        else if (strPath.size() == 2 && strPath[1] == L':')
        {
            strPath.clear();
        }
    }

    //take default start folder
    if (strPath.size() == 0)
    {
        DWORD dwDrives = ::GetLogicalDrives();
        WCHAR wcDrive[] = L"A:";
        DWORD dwMask = 0x01;
        for (int i=0; i<32; i++)
        {
            if (dwDrives & dwMask)
            {
                m_vFolders.push_back(wcDrive);
            }
            wcDrive[0]++;
            dwMask <<= 1;
        }
        return true;
    }

    //append ending slash if needed
    if (strPath[strPath.size()-1] != L'\\') 
        strPath += L"\\";

    //check if this is correct folder path
    DWORD dwAttr = ::GetFileAttributesW(strPath.c_str());
    if (dwAttr == INVALID_FILE_ATTRIBUTES || (dwAttr & FILE_ATTRIBUTE_DIRECTORY)==0) 
        return false;

    m_strPath = strPath;

    wstring strFind = strPath + L"*.*";
    WIN32_FIND_DATAW fd;
    HANDLE hFind = ::FindFirstFileW(strFind.c_str(), &fd);
    BOOL   bFind = (hFind != INVALID_HANDLE_VALUE);
    while (bFind)
	{
        if (wcscmp(fd.cFileName, L".") != 0 && 
            wcscmp(fd.cFileName, L"..") != 0 &&
            (fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == 0 &&
            (fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) == 0)
        {
            if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            {
                if (nFlags & FFFD)
                {
                    m_vFolders.push_back(fd.cFileName);
                }
            }
            else
            {
                if (nFlags & (FFPL|FFSF))
                {
                    if (checkExt(fd.cFileName, nFlags))
                    {
                        m_vFiles.push_back(fd.cFileName);
                    }
                }
            }
        }
        bFind = ::FindNextFileW(hFind, &fd);
	}

    std::sort(m_vFolders.begin(), m_vFolders.end(), CStrLess());

    return true;
}

wstring CFolderContent::GetPath()
{
    return m_strPath;
}

vector<wstring> CFolderContent::GetFolders()
{
    return m_vFolders;
}

vector<wstring> CFolderContent::GetFiles()
{
    return m_vFiles;
}

bool CFolderContent::GetXml(BSTR * bstrXml)
{
	CComPtr<MSXML2::IXMLDOMElement> pFCEl;
	HRESULT hRes = CXmlHelpers::CreateDocument(CComBSTR(L"FolderContent"), &pFCEl);

    //replace right slashes
    wstring strPath = m_strPath;
    //std::replace(strPath.begin(), strPath.end(), L'\\', L'/');
    hRes = CXmlHelpers::SetAttribute(pFCEl, L"Path", strPath.c_str());

	CComPtr<MSXML2::IXMLDOMElement> pFoldersEl;
	hRes = CXmlHelpers::AddElement(pFCEl, L"Folders", &pFoldersEl);
    for (int i=0; i<(int)m_vFolders.size(); i++)
    {
    	CComPtr<MSXML2::IXMLDOMElement> pFolderEl;
	    hRes = CXmlHelpers::AddElement(pFoldersEl, L"Folder", &pFolderEl);
        hRes = CXmlHelpers::SetAttribute(pFolderEl, L"Name", m_vFolders[i].c_str());
    }

	CComPtr<MSXML2::IXMLDOMElement> pFilesEl;
	hRes = CXmlHelpers::AddElement(pFCEl, L"Files", &pFilesEl);
    for (int i=0; i<(int)m_vFiles.size(); i++)
    {
    	CComPtr<MSXML2::IXMLDOMElement> pFileEl;
	    hRes = CXmlHelpers::AddElement(pFilesEl, L"File", &pFileEl);
        hRes = CXmlHelpers::SetAttribute(pFileEl, L"Name", m_vFiles[i].c_str());
    }

    pFCEl->get_xml(bstrXml);
    return true;
}

bool CFolderContent::checkExt(LPCWSTR wcfn, int nFlags)
{
    wstring fn = wcfn;
    wstring::size_type pos = fn.rfind(L'.');
    if (pos != wstring::npos)
    {
        wstring ext = fn.substr(pos);
        if (nFlags & FFPL)
        {
            if (wcscmp(ext.c_str(), EXT_M3U) == 0) return true;
            if (wcscmp(ext.c_str(), EXT_PLS) == 0) return true;
            if (wcscmp(ext.c_str(), EXT_ASX) == 0) return true;
            if (wcscmp(ext.c_str(), EXT_WPL) == 0) return true;
        }
        if (nFlags & FFSF)
        {
            if (wcscmp(ext.c_str(), EXT_MP3) == 0) return true;
            if (wcscmp(ext.c_str(), EXT_WAV) == 0) return true;
        }
    }
    return false;
}

