#include "StdAfx.h"
#include "LightScribePrint.h"

#include "ILSDiscPrintMgr.h"
#include "ILSEnumDiscPrinters.h"
#include "ILSDiscPrinter.h"
#include "ILSDiscPrintSession.h"
#include <memory>

CLightScribePrint::CLightScribePrint() :
	m_hThread(0)
{
}

CLightScribePrint::~CLightScribePrint()
{
}

BOOL CLightScribePrint::Init()
{
	if (m_pPrintMgr != 0)
	{
		return FALSE;
	}

	CoUninitialize();
	HRESULT hr = CoInitializeEx(0, COINIT_MULTITHREADED);
	if (hr != S_OK)
	{
		return FALSE;
	}
	if (m_pPrintMgr.CoCreateInstance(__uuidof(DiscPrintMgr)) == S_OK)
	{
		return TRUE;
	}
	else
	{
		CoUninitialize();
		return FALSE;
	}
}

void CLightScribePrint::DeInit()
{
	CancelPrint();
	m_pPrintMgr.Release();
	CoUninitialize();
}

BOOL CLightScribePrint::GetMediaInfo(MediaInformation& mi, unsigned long iDevice)
{
	// Get enumerator interface
	CComPtr<ILSEnumDiscPrinters> pIEnum;
	HRESULT hr = m_pPrintMgr->EnumDiscPrinters(&pIEnum);
	if (hr != S_OK)
	{
		return FALSE;
	}

	// check the device availability
	unsigned long iCount;
	hr = pIEnum->Count(&iCount);
	if (iCount <= iDevice)
	{
		return FALSE;
	}

	// open the device
	CComPtr<ILSDiscPrinter> pDiskPrn;
	hr = pIEnum->Item(iDevice, &pDiskPrn);
	if (hr != S_OK)
	{
		return FALSE;
	}

	// get media info
	return pDiskPrn->GetCurrentMedia(media_recognized, &mi) == S_OK;
}

BOOL CLightScribePrint::PreviewDisc(Gdiplus::BitmapData* pBitmapData, unsigned long iSize, unsigned long iDevice)
{
	if (m_pPrintMgr == 0)
	{
		// Init has not been called
		m_ePrintStatus = eError;
		return S_FALSE;
	}

	m_iDevice = iDevice;

	CComPtr<ILSEnumDiscPrinters> pIEnum;
	HRESULT hr = m_pPrintMgr->EnumDiscPrinters(&pIEnum);

	unsigned long iCount;
	hr = pIEnum->Count(&iCount);
	if (iCount <= m_iDevice)
	{
		pIEnum.Release();
		m_ePrintStatus = eError;
		return E_NOINTERFACE;
	}

	CComPtr<ILSDiscPrinter> pDiskPrn;
	hr = pIEnum->Item(m_iDevice, &pDiskPrn);
	pIEnum.Release();

	if (hr != S_OK)
	{
		m_ePrintStatus = eError;
		return E_NOINTERFACE;
	}

	hr = pDiskPrn->AddExclusiveUse();
	hr = pDiskPrn->LockDriveTray();

	CComPtr<ILSDiscPrintSession> pSession;
	hr = pDiskPrn->OpenPrintSession(&pSession);

	Size size;
	size.x = pBitmapData->Width;
	size.y  = pBitmapData->Height;

	BITMAPINFOHEADER bitmapHeader;
	bitmapHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitmapHeader.biWidth = pBitmapData->Width;
	bitmapHeader.biHeight = pBitmapData->Height;
	bitmapHeader.biPlanes = 1;
	bitmapHeader.biBitCount = 32;
	bitmapHeader.biCompression = BI_RGB;//BI_BITFIELDS;
	bitmapHeader.biSizeImage = iSize;
	bitmapHeader.biXPelsPerMeter = bitmapHeader.biYPelsPerMeter = 2880;
	bitmapHeader.biClrUsed = 0;
	bitmapHeader.biClrImportant = 0;

	hr = pSession->PrintPreview(
		windows_bitmap,
		label_mode_full,
		draw_default,
		quality_best,
		media_recognized,
		(unsigned char*)&bitmapHeader,
		sizeof(BITMAPINFOHEADER),
		(unsigned char*)pBitmapData->Scan0,
		iSize,
		L"e:\\ls\\ls1.bmp",
		windows_bitmap,
		&size,
		-1);


	m_ePrintStatus = hr == S_OK ? eDone : eError;

	hr = pDiskPrn->UnlockDriveTray();
	hr = pDiskPrn->ReleaseExclusiveUse();
	hr = pSession->Close();
	pSession.Release();
	pDiskPrn.Release();

	return S_OK;
}

BOOL CLightScribePrint::PrintDisc(Gdiplus::BitmapData* pBitmapData, unsigned long iSize, unsigned long iDevice)
{
	if (WaitForSingleObject(m_hThread, 0) == WAIT_TIMEOUT)
	{
		// printing thread is still working
		return FALSE;
	}

	m_iWidth = pBitmapData->Width;
	m_iHeight = pBitmapData->Height;
	m_pBitmap = pBitmapData;
	m_iSize =   iSize;
	m_iDevice = iDevice;

	m_ePrintStatus = eStarting;
	m_iProgress = 0;
	m_bCancel = FALSE;

	DWORD dwId = 0;
	m_hThread = CreateThread(0, 0, PrintThread, this, 0, &dwId);
	if (m_hThread == 0)
	{
		return FALSE;
	}
	else
	{
		const int iDelay = 100; // check status every 100 milliseconds
		while (WaitForSingleObject(m_hThread, iDelay) == WAIT_TIMEOUT)
		{
			if (m_ePrintStatus == ePreparing)
			{
				return TRUE;
			}
		}
		// most likely an error has occurred
		return m_ePrintStatus != eError;
	}
}

int CLightScribePrint::GetPrintStatus()
{
	return m_ePrintStatus;
}

int CLightScribePrint::GetPrintProgress()
{
	return m_iProgress;
}

BOOL CLightScribePrint::CancelPrint()
{
	// set the cancel flag
	m_bCancel = TRUE;
	// and wait while the thread has terminated
	return WaitForSingleObject(m_hThread, INFINITE) == WAIT_OBJECT_0;
}

DWORD CLightScribePrint::PrintThread(LPVOID lpParameter)
{
	if (CoInitializeEx(0, COINIT_MULTITHREADED) != S_OK)
	{
		return -1;
	}
	CLightScribePrint* pThis = (CLightScribePrint*)lpParameter;
	DWORD dwResult = pThis->PrintThread();
	CoUninitialize();
	return dwResult;
}

DWORD CLightScribePrint::PrintThread()
{
	if (m_pPrintMgr == 0)
	{
		// Init has not been called
		m_ePrintStatus = eError;
		return S_FALSE;
	}

	CComPtr<ILSEnumDiscPrinters> pIEnum;
	HRESULT hr = m_pPrintMgr->EnumDiscPrinters(&pIEnum);

	unsigned long iCount;
	hr = pIEnum->Count(&iCount);
	if (iCount <= m_iDevice)
	{
		pIEnum.Release();
		m_ePrintStatus = eError;
		return E_NOINTERFACE;
	}

	CComPtr<ILSDiscPrinter> pDiskPrn;
	hr = pIEnum->Item(m_iDevice, &pDiskPrn);
	pIEnum.Release();

	if (hr != S_OK)
	{
		m_ePrintStatus = eError;
		return E_NOINTERFACE;
	}

	hr = pDiskPrn->AddExclusiveUse();
	hr = pDiskPrn->LockDriveTray();

	CComPtr<ILSDiscPrintSession> pSession;
	hr = pDiskPrn->OpenPrintSession(&pSession);

	hr = pSession->SetProgressCallback(
		reinterpret_cast<ILSDiscPrintProgressEvents*>(this));

	if (hr != S_OK)
	{
		hr = pDiskPrn->UnlockDriveTray();
		hr = pDiskPrn->ReleaseExclusiveUse();
		hr = pSession->Close();
		m_ePrintStatus = eError;
		return S_FALSE;
	}

	BITMAPINFOHEADER bitmapHeader;
	bitmapHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitmapHeader.biWidth = m_iWidth;
	bitmapHeader.biHeight = m_iHeight;
	bitmapHeader.biPlanes = 1;
	bitmapHeader.biBitCount = 32;
	bitmapHeader.biCompression = BI_RGB;
	bitmapHeader.biSizeImage = m_iSize;
	bitmapHeader.biXPelsPerMeter = bitmapHeader.biYPelsPerMeter = 2880;
	bitmapHeader.biClrUsed = 0;
	bitmapHeader.biClrImportant = 0;

	hr = pSession->PrintDisc(
		windows_bitmap,
		label_mode_full,
		draw_default,
		quality_best,
		media_recognized,
		(unsigned char*)&bitmapHeader,
		sizeof(BITMAPINFOHEADER),
		(unsigned char*)m_pBitmap->Scan0,
		m_iSize);

	m_ePrintStatus = hr == S_OK ? eDone : eError;

	hr = pDiskPrn->UnlockDriveTray();
	hr = pDiskPrn->ReleaseExclusiveUse();
	hr = pSession->ReleaseProgressCallback();
	hr = pSession->Close();
	pSession.Release();
	pDiskPrn.Release();

	m_hThread = 0;

	return hr;
}
