#pragma once
#include <vector>
#include <gdiplus.h>
#include "LightScribePrint.h"

#define PLUGIN_VERSION   L"1.0.0.8"

class CLightscribeHelper
{
public:
	CLightscribeHelper(void);
	~CLightscribeHelper(void);

	BOOL Print(BSTR bstrRequest, BSTR * pbstrData_Response);
	BOOL DetectDevice(BSTR bstrRequest, BSTR * pbstrResponse);
private:
//	void SaveBitmap24bitColorWithResize(int width, int height, int stride, 
//										BYTE* bmpData, LPCSTR filename);
	void SaveBitmap256Colors(int width, int height, int stride, 
										BYTE* bmpData, LPCSTR filename);
	UINT RunBurner(LPCSTR filename = NULL, const CComBSTR &lsMode = NULL, 
						const CComBSTR &lsQuality = NULL, const CComBSTR &silentMode = NULL);
//	int GetEncoderClsid(const WCHAR* format, CLSID* pClsid);
//	void StringToByteArrayR2C2_32bitColor(int width, int height, 
//								   double largeRadius, double smallRadius, 
//                                   const CComBSTR &bstrBitmapData, BYTE** data, int* strLen);
	void StringToByteArrayR2C2PR_256Colors(int width, int height, 
								   double largeRadius, double smallRadius, 
								  BSTR &bstrBitmapData, BYTE** data, int* strLen);
};
