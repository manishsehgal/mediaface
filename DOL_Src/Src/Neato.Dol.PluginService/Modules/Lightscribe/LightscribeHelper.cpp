#include "StdAfx.h"
#include "LightScribeHelper.h"
#include <math.h>

CLightscribeHelper::CLightscribeHelper(void)
{
}

CLightscribeHelper::~CLightscribeHelper(void)
{
}

BOOL CLightscribeHelper::Print(BSTR bstrRequest, BSTR * pbstrData_Response)
{
	BOOL bRes = TRUE;

	/*try
	{*/
		CComPtr<MSXML2::IXMLDOMElement>  pReqElem;
		HRESULT hRes = CXmlHelpers::LoadXml(bstrRequest, &pReqElem);
		if (hRes != S_OK) throw 1;

        CComPtr<MSXML2::IXMLDOMElement> pParamsElem;
        hRes = CXmlHelpers::GetChildElement(pReqElem, L"Params", &pParamsElem);
        if (hRes != S_OK) throw 2;

		CComPtr<MSXML2::IXMLDOMElement> pGuiDataElem;
		CComBSTR bstrLSquality="1";
		CComBSTR bstrLSsilentMode="0";
        hRes = CXmlHelpers::GetChildElement(pParamsElem, L"GuiData", &pGuiDataElem);
		if (hRes == S_OK) {
			hRes = CXmlHelpers::GetAttribute(pGuiDataElem, L"LSquality", &bstrLSquality);
			if (hRes != S_OK)
				bstrLSquality="1";	// Normal
			hRes = CXmlHelpers::GetAttribute(pGuiDataElem, L"silent", &bstrLSsilentMode);
			if (hRes != S_OK)
				bstrLSsilentMode="0";	// not silent
		}

		CComPtr<MSXML2::IXMLDOMElement> pBmpDataElem;
        hRes = CXmlHelpers::GetChildElement(pParamsElem, L"BmpData", &pBmpDataElem);
        if (hRes != S_OK) throw 3;

        CComBSTR bstrEncoding;
		hRes = CXmlHelpers::GetAttribute(pBmpDataElem, L"Encoding", &bstrEncoding);
		if (hRes != S_OK)
			bstrEncoding = "SQRT_R2C2_PROGR";

		int width = -1;
		int height = -1;
		double largeRadius = -1;
		double smallRadius = -1;

        CComBSTR bstrLSMode;
        hRes = CXmlHelpers::GetAttribute(pBmpDataElem, L"LabelMode", &bstrLSMode);
        if (hRes != S_OK) throw 4;

		CComBSTR bstrWidth;
        hRes = CXmlHelpers::GetAttribute(pBmpDataElem, L"Width", &bstrWidth);
        if (hRes != S_OK) throw 4;
		width = _wtoi((const wchar_t *)bstrWidth.m_str);

        CComBSTR bstrHeight;
        hRes = CXmlHelpers::GetAttribute(pBmpDataElem, L"Height", &bstrHeight);
        if (hRes != S_OK) throw 4;
		height = _wtoi((const wchar_t *)bstrHeight.m_str);

		TCHAR a_tmpPath[_MAX_PATH], a_tmpFileName[_MAX_PATH];
		if (::GetTempPath(_MAX_PATH, a_tmpPath) == 0)
			throw -1;
		if (::GetTempFileName(a_tmpPath, "Lightscribe", 0, a_tmpFileName) == 0)
			throw -1;

		/*if (bstrEncoding == "SQRT_R2C2") {

			CComBSTR bstrLargeRadius;
			hRes = CXmlHelpers::GetAttribute(pBmpDataElem, L"LargeRadius", &bstrLargeRadius);
			if (hRes != S_OK) throw 4;
			largeRadius = _wtof((const wchar_t *)bstrLargeRadius.m_str);

			CComBSTR bstrSmallRadius;
			hRes = CXmlHelpers::GetAttribute(pBmpDataElem, L"SmallRadius", &bstrSmallRadius);
			if (hRes != S_OK) throw 4;
			smallRadius = _wtof((const wchar_t *)bstrSmallRadius.m_str);

			CComBSTR bstrBitmapData;
			pBmpDataElem.p->get_text(&bstrBitmapData);

			BYTE* bmpData;
			int strLen;

			StringToByteArrayR2C2_32bitColor(width, height, largeRadius, smallRadius, 
									bstrBitmapData, &bmpData, &strLen);
			if(bmpData==NULL)
				throw -1;
			
			SaveBitmap24bitColorWithResize(width, height, strLen, bmpData, a_tmpFileName);

			free(bmpData);

		} else*/ if (bstrEncoding == "SQRT_R2C2_PROGR") {

			CComBSTR bstrLargeRadius;
			hRes = CXmlHelpers::GetAttribute(pBmpDataElem, L"LargeRadius", &bstrLargeRadius);
			if (hRes != S_OK) throw 4;
			largeRadius = _wtof((const wchar_t *)bstrLargeRadius.m_str);

			CComBSTR bstrSmallRadius;
			hRes = CXmlHelpers::GetAttribute(pBmpDataElem, L"SmallRadius", &bstrSmallRadius);
			if (hRes != S_OK) throw 4;
			smallRadius = _wtof((const wchar_t *)bstrSmallRadius.m_str);

			//CComBSTR bstrBitmapData;
			if( SysStringLen(*pbstrData_Response) == 0 ) { // bitmap data may be transferred either in XML or separately
				pBmpDataElem.p->get_text(pbstrData_Response);
				if( SysStringLen(*pbstrData_Response) == 0 ) {
					CComPtr<MSXML2::IXMLDOMElement> pDataElem;
					hRes = CXmlHelpers::GetChildElement(pReqElem, L"Data", &pDataElem);
					if (hRes == S_OK) 
						pDataElem.p->get_text(pbstrData_Response);
				}
			}

			BYTE* bmpData;
			int strLen;

			StringToByteArrayR2C2PR_256Colors(width, height, largeRadius, smallRadius, 
											*pbstrData_Response, &bmpData, &strLen);
			SysFreeString(*pbstrData_Response);

			if(bmpData==NULL)
				throw -1;
			
			SaveBitmap256Colors(width, height, strLen, bmpData, a_tmpFileName);

			free(bmpData);

		} else
			throw 5;

		UINT ret = RunBurner(a_tmpFileName, bstrLSMode, bstrLSquality, bstrLSsilentMode);
		
		// moved to LightScribeBurner
		//::DeleteFile(a_tmpFileName);
		
		// Prepare response xml
		CComPtr<MSXML2::IXMLDOMElement>  pRespElem;
		hRes = CXmlHelpers::CreateDocument(CComBSTR(L"Response"), &pRespElem);
        if (hRes != S_OK) throw 5;

		hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", ret ? L"0" : L"1");
        if (hRes != S_OK) throw 6;

        CComPtr<MSXML2::IXMLDOMElement> pRParamsElem;
		hRes = CXmlHelpers::AddElement(pRespElem, CComBSTR(L"Params"), &pRParamsElem);
        if (hRes != S_OK) throw 7;

		// response
		hRes = pRespElem->get_xml(pbstrData_Response);
		if (hRes != S_OK) throw 9;
		
	/*}
	catch(...)
	{
		bRes = FALSE;
	}*/

	return bRes;
}

BOOL CLightscribeHelper::DetectDevice(BSTR bstrRequest, BSTR * pbstrResponse)
{
	BOOL bRes = TRUE;

	try
	{
		// Prepare response xml
		CComPtr<MSXML2::IXMLDOMElement>  pRespElem;
		HRESULT hRes = CXmlHelpers::CreateDocument(CComBSTR(L"Response"), &pRespElem);
        if (hRes != S_OK) throw 5;

        hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"0");
        if (hRes != S_OK) throw 6;

        CComPtr<MSXML2::IXMLDOMElement> pRParamsElem;
		hRes = CXmlHelpers::AddElement(pRespElem, CComBSTR(L"Params"), &pRParamsElem);
        if (hRes != S_OK) throw 7;

		UINT ret = RunBurner();
		switch(ret)
		{
			case 1:
			case 2:
				hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"NoDll");
				if (hRes != S_OK) throw 8;
				break;
			case 3:
				hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"NoDrive");
				if (hRes != S_OK) throw 8;
				break;
			case 4:
				hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"NoLSBurner");
				if (hRes != S_OK) throw 8;
				break;
		}
		// response
		hRes = pRespElem->get_xml(pbstrResponse);
		if (hRes != S_OK) throw 9;
	}
	catch(...)
	{
		bRes = FALSE;
	}

	return bRes;
}


/*
void CLightscribeHelper::SaveBitmap24bitColorWithResize(int width, int height, int stride, 
														BYTE* bmpData, LPCSTR filename)
{
	int resolution = 600;
	int newWidth   = 2880;
	int newHeight  = 2880;

	// Initialize GDI+.
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR           gdiplusToken;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	{
	Gdiplus::Bitmap bitmap(width, height, stride, PixelFormat32bppRGB, bmpData );
	bitmap.RotateFlip(Gdiplus::Rotate90FlipX);

	Gdiplus::Bitmap newBitmap(newWidth, newHeight, PixelFormat24bppRGB);
	Gdiplus::Graphics* gr = Gdiplus::Graphics::FromImage(&newBitmap);
	gr->SetInterpolationMode(Gdiplus::InterpolationModeHighQualityBicubic);
	gr->DrawImage(	&bitmap,
		Gdiplus::Rect(0, 0, newWidth, newHeight),  // destination rectangle 
		0, 0,        // upper-left corner of source rectangle
		width,       // width of source rectangle
		height,      // height of source rectangle
		Gdiplus::UnitPixel);
	newBitmap.SetResolution(resolution, resolution);

	CLSID bmpClsid;
	GetEncoderClsid(L"image/bmp", &bmpClsid);

	CComBSTR a_fileName(filename);
	newBitmap.Save(a_fileName, &bmpClsid, NULL);

	delete gr;
	}
	Gdiplus::GdiplusShutdown(gdiplusToken);
}
*/

void CLightscribeHelper::SaveBitmap256Colors(int width, int height, int stride, 
											 BYTE* bmpData, LPCSTR filename)
{
/*	old: using GDI+
	// Initialize GDI+.
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR           gdiplusToken;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	
	{
	
		Gdiplus::Bitmap bitmap(width, height, stride, PixelFormat8bppIndexed, bmpData );
		bitmap.SetResolution(resolution, resolution);

		// set greyscale palette
		UINT palleteSize = 1032; // bitmap.GetPaletteSize();
		Gdiplus::ColorPalette* palette = (Gdiplus::ColorPalette*)malloc(palleteSize);
		palette->Flags=Gdiplus::PaletteFlagsGrayScale;
		palette->Count=256;
		for(int i=0;i<256;i++)
			palette->Entries[i]=i*0x10101;

		bitmap.SetPalette(palette);

		CLSID bmpClsid;
		GetEncoderClsid(L"image/bmp", &bmpClsid);

		CComBSTR a_fileName(filename);
		bitmap.Save(a_fileName, &bmpClsid, NULL);
	}
		
	Gdiplus::GdiplusShutdown(gdiplusToken);
*/
	
	// new: direct writing of header+palette+bitmap into file
	BITMAPFILEHEADER bmfh;
	BITMAPINFOHEADER bmih;

	const int N_COLORS = 256;
	RGBQUAD bmPalette[N_COLORS];

	bmih.biSize = sizeof(bmih); // = 40;
	bmih.biWidth = width;
	bmih.biHeight = -height;	// scan from top to bottom
	bmih.biPlanes = 1;
	bmih.biBitCount = 8;
	bmih.biCompression = 0;
	bmih.biSizeImage = stride*height;
	const double InchesPerMeter = 1000/25.4;
	bmih.biXPelsPerMeter = (LONG)(600*InchesPerMeter); // = 23622; resolution=600 bpi
	bmih.biYPelsPerMeter = (LONG)(600*InchesPerMeter); // = 23622;
	bmih.biClrUsed = N_COLORS;
	bmih.biClrImportant = N_COLORS;

	bmfh.bfType = 'MB';
	bmfh.bfOffBits = sizeof(bmfh)+sizeof(bmih)+sizeof(bmPalette); // = 1078;
	bmfh.bfSize = bmfh.bfOffBits+bmih.biSizeImage; // = 8295478;

	// bmPalette.bmih = bmih;
	for(int i=0;i<N_COLORS;i++)
		*(int*)(bmPalette+i)=i*0x10101;	// greyscale

	HANDLE hFile = ::CreateFile(filename, GENERIC_WRITE, 
		FILE_SHARE_READ, 
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_SEQUENTIAL_SCAN,
		NULL);
	if(hFile==INVALID_HANDLE_VALUE)
		return; // throw 10; returning error from LS_READ completely stops Designer

	DWORD bytesWritten;
	DWORD totalWritten = 0;

	WriteFile(hFile,&bmfh,sizeof(bmfh),&bytesWritten,NULL);
	totalWritten += bytesWritten;
	WriteFile(hFile,&bmih,sizeof(bmih),&bytesWritten,NULL);
	totalWritten += bytesWritten;
	WriteFile(hFile,bmPalette,sizeof(bmPalette),&bytesWritten,NULL);
	totalWritten += bytesWritten;
	WriteFile(hFile,bmpData,bmih.biSizeImage,&bytesWritten,NULL);
	totalWritten += bytesWritten;

	/*
	if(totalWritten != bmfh.bfSize)
		 // throw 11;  returning error from LS_READ completely stops Designer
	*/

	CloseHandle(hFile);
}

UINT CLightscribeHelper::RunBurner(LPCSTR filename, const CComBSTR &lsMode, 
								   const CComBSTR &lsQuality, const CComBSTR &lsSilent)
{
	USES_CONVERSION;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory( &si, sizeof(si) );
	si.cb = sizeof(si);
	ZeroMemory( &pi, sizeof(pi) );

	TCHAR a_commandLine[_MAX_PATH+200] = _T("");

	//_tcscat(a_commandLine, _T("\""));
	_tcscat(a_commandLine, _T("LightscribeBurner.bin"));
	//_tcscat(a_commandLine, _T("\""));

	if (filename != NULL)
	{
		_tcscat(a_commandLine, _T(" \""));
		_tcscat(a_commandLine, filename);
		_tcscat(a_commandLine, _T("\""));
		if (lsMode != NULL) {
			_tcscat(a_commandLine, _T(" "));
			_tcscat(a_commandLine, _T(" /lsmode "));
			_tcscat(a_commandLine, _T(" "));
			_tcscat(a_commandLine, OLE2CT(lsMode.m_str));
		}
		if (lsQuality != NULL) {
			_tcscat(a_commandLine, _T(" "));
			_tcscat(a_commandLine, _T(" /lsquality "));
			_tcscat(a_commandLine, _T(" "));
			_tcscat(a_commandLine, OLE2CT(lsQuality.m_str));
		}
		if (lsSilent != NULL) {
			_tcscat(a_commandLine, _T(" "));
			_tcscat(a_commandLine, _T(" /silent "));
			_tcscat(a_commandLine, _T(" "));
			_tcscat(a_commandLine, OLE2CT(lsSilent.m_str));
		}
        {
			_tcscat(a_commandLine, _T(" "));
			_tcscat(a_commandLine, _T(" /version="));
			_tcscat(a_commandLine, W2T(PLUGIN_VERSION));
			_tcscat(a_commandLine, _T(" "));
        }
	}
	
	if(::CreateProcess( NULL, a_commandLine, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi ) == 0)
	{
		DWORD a_error = GetLastError();
		return 4;
	}

	::CloseHandle(pi.hThread);
	if (filename == NULL)
		::WaitForSingleObject( pi.hProcess, INFINITE );

	DWORD exitCode;
	::GetExitCodeProcess(pi.hProcess, &exitCode);
	
	CloseHandle( pi.hProcess );
	return exitCode;
}

/*
int CLightscribeHelper::GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes

	Gdiplus::ImageCodecInfo* pImageCodecInfo = NULL;

	Gdiplus::GetImageEncodersSize(&num, &size);
	if(size == 0)
		return -1;  // Failure

	pImageCodecInfo = (Gdiplus::ImageCodecInfo*)(malloc(size));
	if(pImageCodecInfo == NULL)
		return -1;  // Failure

	Gdiplus::GetImageEncoders(num, size, pImageCodecInfo);

	for(UINT j = 0; j < num; ++j)
	{
		if( wcscmp(pImageCodecInfo[j].MimeType, format) == 0 )
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}    
	}

	free(pImageCodecInfo);
	return -1;  // Failure
}
*/

/*
void CLightscribeHelper::StringToByteArrayR2C2_32bitColor(int width, int height, double largeRadius, double smallRadius, 
										  const CComBSTR &bstrBitmapData, BYTE** bmpData, int* pStrLen)
{
	int strLen = width*4;
	int buffLen = strLen*height;

	BYTE* data = (BYTE*)malloc(buffLen);
	if(data == NULL)
		return;

	*bmpData = data;
	*pStrLen = strLen;

	memset(data,0xFF,buffLen);	// fill buffer by White color

	double center   = width / 2.0f;

	double largeR2 = largeRadius*largeRadius;
	double smallR2 = smallRadius*smallRadius;

	wchar_t * strData = (wchar_t *)bstrBitmapData.m_str;

	for(int i = 0, k = 0; i < height; ++i) {

		BYTE* lineStart = data + strLen*i;

		double dy = center - i;
		double dy2 = dy*dy;

		if (dy2 > largeR2) 
			continue;
		else {
			double dx = sqrt(largeR2 - dy2);
			int largeB = (int)floor(center - dx);
			int largeE = (int)ceil (center + dx);

			int smallE;
			int smallB;

			if (dy2 > smallR2) {
				smallE = largeE;
				smallB = largeE+1;
			} else {
				dx = sqrt(smallR2 - dy2);
				smallE = (int)floor(center - dx);
				smallB = (int)ceil (center + dx);
			}

			for(int j = largeB; j < smallE; ++j)
			{
				wchar_t buff[3];
				int color;
				buff[0] = strData[k++];
				buff[1] = strData[k++];
				buff[2] = 0;
				swscanf(buff, L"%x", &color);
				((UINT*)lineStart)[j] = color*0x10101;
			}
			for(int j = smallB; j < largeE; ++j)
			{
				wchar_t buff[3];
				int color;
				buff[0] = strData[k++];
				buff[1] = strData[k++];
				buff[2] = 0;
				swscanf(buff, L"%x", &color);
				((UINT*)lineStart)[j] = color*0x10101;
			}
		}
	}
}
*/

__forceinline void SetColor(const wchar_t * &pCurrChar, const wchar_t * pDataEnd, 
							BYTE* lineStart, int strLen, 
							int i, int j, int width, int height, int passN) {

	int color;
	BYTE* pByte = lineStart+j;

	if(pCurrChar < pDataEnd) {
		wchar_t C1 = *pCurrChar++;
		wchar_t C2 = *pCurrChar++;
		color = C1*0x10 + C2 - 65*17;	// = ( C1-65 )*0x10 + C2 - 65;
	} else {
		if((i<=0) || (j<=0) || (i>=height) || (j>=width))
			return;
		if(passN == 1) 
			color =(*(pByte-strLen-1)+
				    *(pByte-strLen+1)+
					*(pByte+strLen-1)+
					*(pByte+strLen+1)+2)/4;
		else
			color =(*(pByte-strLen)+
					*(pByte-1)+
					*(pByte+1)+
					*(pByte+strLen)+2)/4;
	}
	
	*pByte=(BYTE)color;
}

void CLightscribeHelper::StringToByteArrayR2C2PR_256Colors(int width, int height, 
										  double largeRadius, double smallRadius, 
										  BSTR &bstrBitmapData, BYTE** bmpData, int* pStrLen)
{
	int strLen = width;
	if(strLen % 4 != 0)
		strLen += (4 - strLen%4); // length of each scan line in the buffer; must be a multiple of 4
	int buffLen = strLen*height;

	BYTE* data = (BYTE*)malloc(buffLen);
	if(data == NULL)
		return;

	*bmpData = data;
	*pStrLen = strLen;

	memset(data,0xFF,buffLen);	// fill buffer by White color

	double centerX   = width / 2.0f;
	double centerY   = height / 2.0f;

	double largeR2 = largeRadius*largeRadius;
	double smallR2 = smallRadius*smallRadius;

	int delta[4][2] =  {{0,0},{1,1},{1,0},{0,1}};

	int	startI = (int)floor(centerY - largeRadius);
	int	endY = __min((int)ceil(centerY + largeRadius),height);

	//wchar_t * pData = strData.c_str;

	const wchar_t * pCurrChar = bstrBitmapData;
	const wchar_t * pDataEnd = pCurrChar + SysStringLen(bstrBitmapData);

	for(int passN=0;passN<4;passN++) {
		int deltaX = delta[passN][0];
		for (int i = startI + delta[passN][1]; i < endY; i+=2 ) {

			BYTE* lineStart = data + strLen*i;

			double dy = centerY - i;
			double dy2 = dy*dy;
			
			if (dy2 > largeR2) 
				continue;

			double dx = sqrt(largeR2 - dy2);
			int largeB = (int)floor((centerX - dx - deltaX)/2)*2 + deltaX;
			int largeE = (int)floor(centerX + dx);

			if (dy2 > smallR2) {
				for (int j=largeB; j<=largeE; j+=2)
					SetColor(pCurrChar, pDataEnd, lineStart, strLen, i, j, width, height, passN);
			} else {
				dx = sqrt(smallR2 - dy2);
				int smallB = (int)floor(centerX - dx);
				int smallE = (int)floor((centerX + dx - deltaX)/2)*2 + deltaX;

				for (int j=largeB; j<=smallB; j+=2)
					SetColor(pCurrChar, pDataEnd, lineStart, strLen, i, j, width, height, passN);
				for (int j=smallE; j<=largeE; j+=2)
					SetColor(pCurrChar, pDataEnd, lineStart, strLen, i, j, width, height, passN);
			}
		}
	}
}


