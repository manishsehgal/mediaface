#ifndef __LIGHTSCRIBEPRINT_H__
#define __LIGHTSCRIBEPRINT_H__

#include "ProgressImpl.h"
#include <gdiplus.h>

class ILSDiscPrintMgr;

class CLightScribePrint : protected CProgressImpl
{
public:
	CLightScribePrint();
	virtual ~CLightScribePrint();

	BOOL Init();

	void DeInit();

	BOOL GetMediaInfo(
		MediaInformation& mi,
		unsigned long iDevice
		);

	BOOL PrintDisc(
		unsigned char* pBitmap,
		unsigned long iSize,
		unsigned long iDevice
		);
	
	BOOL PrintDisc(
		Gdiplus::BitmapData* pBitmapData,
		unsigned long iSize,
		unsigned long iDevice
		);

	BOOL PreviewDisc(
		Gdiplus::BitmapData* pBitmapData,
		unsigned long iSize,
		unsigned long iDevice
		);

	int GetPrintStatus();

	int GetPrintProgress();

	BOOL CancelPrint();

protected:

	static DWORD __stdcall PrintThread(
		LPVOID lpParameter
		);

	DWORD PrintThread(
		);

	CComPtr<ILSDiscPrintMgr> m_pPrintMgr;
	HANDLE m_hThread;

	// printing parameters
	//unsigned char* m_pBitmap;
	Gdiplus::BitmapData* m_pBitmap;
	unsigned long m_iSize;
	unsigned long m_iDevice;
	unsigned int m_iWidth;
	unsigned int m_iHeight;
};

#endif // __LIGHTSCRIBEPRINT_H__
