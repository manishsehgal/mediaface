// Homemade class from Typelib

#ifndef __ILSPRIVATETESTING_H__
#define __ILSPRIVATETESTING_H__

// ILSPrivateTesting
[
	uuid("2CBA00CD-67F9-4067-8F18-885B41F71F15")
]
class ILSPrivateTesting : public IUnknown
{
	// ILSPrivateTesting methods
public:
	virtual HRESULT _stdcall GetAddressOfMarshalledData(
					/*[in]*/ unsigned char* header, 
					/*[in]*/ int headerLength, 
					/*[out]*/ unsigned long* address) = 0;
	virtual HRESULT _stdcall ThrowStormException(/*[in]*/ unsigned long statusCode) = 0;
	virtual HRESULT _stdcall ThrowUncaughtStdException() = 0;
	virtual HRESULT _stdcall TriggerPointerFault() = 0;
	virtual HRESULT _stdcall EnableStormLoading(/*[in]*/ VARIANT_BOOL enabled) = 0;

};

#endif // __ILSPRIVATETESTING_H__
