#ifndef __LSTYPES_H__
#define __LSTYPES_H__

typedef enum
{
	windows_bitmap = 0
} ImageType;

typedef enum
{
	draw_default = 0,
	draw_fit_height_to_label = 1,
	draw_fit_width_to_label = 2,
	draw_fit_smallest_to_label = 4
} DrawOptions;

typedef struct tagSize
{
	long x;
	long y;
} Size;

typedef enum {
	label_monochrome = 1,
	label_color = 2
} DeviceCapabilitiesEnum;

typedef enum {
	quality_best = 0,
	quality_normal = 1,
	quality_draft = 2
} PrintQuality;

typedef enum {
	label_mode_full = 0,
	label_mode_title = 1,
	label_mode_content = 2
} LabelMode;

typedef enum {
	label_unknown = 0,
	label_12cm_circle = 1,
	label_8cm_circle = 2
} DiscShapeAndSize;

typedef enum {
	media_recognized = 0,
	media_generic = 1
} MediaOptimizationLevel;

typedef struct tagColorRGB
{
	unsigned char red;
	unsigned char green;
	unsigned char blue;
} ColorRGB;

typedef struct tagMediaInformation
{
	int mediaPresent;
	int mediaOrientedForLabeling;
	short manufacturer;
	long innerRadius;
	long outerRadius;
	ColorRGB foreground;
	ColorRGB background;
	DiscShapeAndSize shapeAndSize;
} MediaInformation;

#endif // __LSTYPES_H__
