// Homemade class from Typelib

#ifndef __ILSDISKPRINTER_H__
#define __ILSDISKPRINTER_H__

#include "LSTypes.h"

class ILSDiscPrintSession;

// ILSDiscPrinter
[
	uuid("5EBF23F2-1E21-4E63-882C-D4846BAF6B34")
]
class ILSDiscPrinter : public IUnknown
{
	// ILSDiscPrinter methods
public:
	virtual HRESULT _stdcall Validate() = 0;
	virtual HRESULT _stdcall GetPrinterVendorName(/*[out, retval]*/ BSTR* vendorName) = 0;
	virtual HRESULT _stdcall GetPrinterProductName(/*[out, retval]*/ BSTR* productName) = 0;
	virtual HRESULT _stdcall GetPrinterDisplayName(/*[out, retval]*/ BSTR* printerDisplayName) = 0;
	virtual HRESULT _stdcall GetPrinterPath(/*[out, retval]*/ BSTR* printerPath) = 0;
	virtual HRESULT _stdcall GetPrinterCapabilities(/*[out, retval]*/ DeviceCapabilitiesEnum* capabilities) = 0;
	virtual HRESULT _stdcall GetDriveInnerRadius(/*[out, retval]*/ long* innerRadius) = 0;
	virtual HRESULT _stdcall GetDriveOuterRadius(/*[out, retval]*/ long* outerRadius) = 0;
	virtual HRESULT _stdcall GetCurrentMedia(
					/*[in]*/ MediaOptimizationLevel optimizationLevel, 
					/*[out, retval]*/ MediaInformation* pMedia) = 0;
	virtual HRESULT _stdcall OpenPrintSession(/*[out]*/ ILSDiscPrintSession** ppSession) = 0;
	virtual HRESULT _stdcall GetPrintResolution(
					/*[in]*/ PrintQuality quality, 
					/*[in]*/ MediaOptimizationLevel optimizationLevel, 
					/*[out, retval]*/ unsigned long* resolution) = 0;
	virtual HRESULT _stdcall GetLabelRegionOuterRadius(
					/*[in]*/ LabelMode mode, 
					/*[in]*/ MediaOptimizationLevel optimizationLevel, 
					/*[out, retval]*/ long* outerRadius) = 0;
	virtual HRESULT _stdcall GetLabelRegionInnerRadius(
					/*[in]*/ LabelMode mode, 
					/*[in]*/ MediaOptimizationLevel optimizationLevel, 
					/*[out, retval]*/ long* innerRadius) = 0;
	virtual HRESULT _stdcall OpenDriveTray() = 0;
	virtual HRESULT _stdcall CloseDriveTray() = 0;
	virtual HRESULT _stdcall LockDriveTray() = 0;
	virtual HRESULT _stdcall UnlockDriveTray() = 0;
	virtual HRESULT _stdcall AddExclusiveUse() = 0;
	virtual HRESULT _stdcall ReleaseExclusiveUse() = 0;

};

#endif // __ILSDISKPRINTER_H__
