// Homemade class from Typelib

#ifndef __ILSDISKPRINTSESSIONTEST_H__
#define __ILSDISKPRINTSESSIONTEST_H__

// ILSDiscPrintSessionTest
[
	uuid("0923A5B6-DF36-4C5A-ACA3-464CB1B75A0E")
]
class ILSDiscPrintSessionTest : public IUnknown
{
	// ILSDiscPrintSessionTest methods
public:
	virtual HRESULT _stdcall SetSavePrintFile(/*[in]*/ BSTR filename) = 0;

};

#endif // __ILSDISKPRINTSESSIONTEST_H__
