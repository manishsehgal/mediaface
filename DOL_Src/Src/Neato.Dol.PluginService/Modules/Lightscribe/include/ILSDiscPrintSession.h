// Homemade class from Typelib

#ifndef __ILSDISKPRINTSESSION_H__
#define __ILSDISKPRINTSESSION_H__

#include "LSTypes.h"

class ILSDiscPrintProgressEvents;

// ILSDiscPrintSession
[
	uuid("9661AADE-8468-43C7-BCC4-7E1687F3D2EB")
]
class ILSDiscPrintSession : public IUnknown
{
	// ILSDiscPrintSession methods
public:
	virtual HRESULT _stdcall PrintDisc(
					/*[in]*/ ImageType ImageType,
					/*[in]*/ LabelMode LabelMode,
					/*[in]*/ DrawOptions DrawOptions,
					/*[in]*/ PrintQuality quality,
					/*[in]*/ MediaOptimizationLevel optimizationLevel,
					/*[in]*/ unsigned char* header,
					/*[in]*/ int headerLength,
					/*[in]*/ unsigned char* image,
					/*[in]*/ int imageLength) = 0;
	virtual HRESULT _stdcall PrintPreview(
					/*[in]*/ ImageType imageType,
					/*[in]*/ LabelMode LabelMode,
					/*[in]*/ DrawOptions DrawOptions,
					/*[in]*/ PrintQuality quality,
					/*[in]*/ MediaOptimizationLevel optimizationLevel,
					/*[in]*/ unsigned char* header,
					/*[in]*/ int headerLength,
					/*[in]*/ unsigned char* image,
					/*[in]*/ int imageLength,
					/*[in]*/ BSTR previewFile,
					/*[in]*/ ImageType previewImageType,
					/*[in]*/ Size* previewSize,
					/*[in]*/ VARIANT_BOOL ignoreMedia) = 0;
	virtual HRESULT _stdcall SetProgressCallback(/*[in]*/ ILSDiscPrintProgressEvents* pEvents) = 0;
	virtual HRESULT _stdcall ReleaseProgressCallback() = 0;
	virtual HRESULT _stdcall Close() = 0;

};

#endif // __ILSDISKPRINTSESSION_H__
