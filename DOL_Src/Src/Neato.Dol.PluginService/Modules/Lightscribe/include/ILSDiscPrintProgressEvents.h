// Homemade class from Typelib

#ifndef __ILSDISKPRINTPROGRESSEVENTS_H__
#define __ILSDISKPRINTPROGRESSEVENTS_H__

// ILSDiscPrintProgressEvents
[
	uuid("E785F58A-5FC7-4E65-8FDE-E4799C44E269")
]
class ILSDiscPrintProgressEvents : public IUnknown
{
	// ILSDiscPrintProgressEvents methods
public:
	virtual HRESULT _stdcall NotifyPreparePrintProgress(
					/*[in]*/ int current, 
					/*[in]*/ int final) = 0;
	virtual HRESULT _stdcall NotifyPrintProgress(
					/*[in]*/ int areaPrinted, 
					/*[in]*/ int totalArea) = 0;
	virtual HRESULT _stdcall NotifyPrintComplete(/*[in]*/ HRESULT status) = 0;
	virtual HRESULT _stdcall QueryCancel(/*[out, retval]*/ VARIANT_BOOL* bCancel) = 0;
	virtual HRESULT _stdcall ReportLabelTimeEstimate(
					/*[in]*/ long seconds, 
					/*[out, retval]*/ VARIANT_BOOL* bCancel) = 0;

};

#endif // __ILSDISKPRINTPROGRESSEVENTS_H__
