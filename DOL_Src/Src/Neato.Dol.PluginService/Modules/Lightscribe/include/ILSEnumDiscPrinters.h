// Homemade class from Typelib

#ifndef __ILSENUMDISKPRINTERS_H__
#define __ILSENUMDISKPRINTERS_H__

class ILSDiscPrinter;

// ILSEnumDiscPrinters
[
	uuid("47A7B1CB-6970-495D-946B-DFA83F70FE77")
]
class ILSEnumDiscPrinters : public IUnknown
{
	// ILSEnumDiscPrinters methods
public:
	virtual HRESULT _stdcall Count(unsigned long* Count) = 0;
	virtual HRESULT _stdcall Item(unsigned long index, ILSDiscPrinter** discPrinter) = 0;

};

#endif // __ILSENUMDISKPRINTERS_H__
