// Homemade class from Typelib

#ifndef __ILSDISKPRINTMGRDIAGNOSTICS_H__
#define __ILSDISKPRINTMGRDIAGNOSTICS_H__

// ILSDiscPrintMgrDiagnostics
[
	uuid("728B6248-E5D0-4D39-819A-9E2D3257CE5C")
]
class ILSDiscPrintMgrDiagnostics : public IUnknown
{
	// ILSDiscPrintMgrDiagnostics methods
public:
	virtual HRESULT _stdcall EnableDebugOutput(/*[in]*/ VARIANT_BOOL enabled) = 0;
	virtual HRESULT _stdcall GetDiagnosticsInfo(/*[out, retval]*/ BSTR* diagnosticsXML) = 0;
	virtual HRESULT _stdcall AreAllNeededComponentsPresent(/*[out, retval]*/ VARIANT_BOOL* present) = 0;

};

#endif // __ILSDISKPRINTMGRDIAGNOSTICS_H__
