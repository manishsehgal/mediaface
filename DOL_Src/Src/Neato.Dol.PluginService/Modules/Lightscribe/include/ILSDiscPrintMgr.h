// Homemade class from Typelib

#ifndef __ILSDISKPRINTMGR_H__
#define __ILSDISKPRINTMGR_H__

#include "ILSDiscPrintMgrDiagnostics.h"
#include "ILSPrivateTesting.h"

class ILSEnumDiscPrinters;

// ILSDiscPrintMgr
[
	uuid("39658004-A44E-4F68-8F79-E1BCA9E84C0C")
]
class ILSDiscPrintMgr : public IUnknown
{
	// ILSDiscPrintMgr methods
public:
    virtual HRESULT _stdcall EnumDiscPrinters(ILSEnumDiscPrinters** ppEnumDiscPrinters) = 0;
    virtual HRESULT _stdcall GetUpdateShellCommand(BSTR* shellCommand) = 0;

};

[
    uuid(5197646C-00EA-4307-A067-61319EBBE499)
]
class DiscPrintMgr :
	private ILSDiscPrintMgr,
	private ILSDiscPrintMgrDiagnostics,
	private ILSPrivateTesting
{
};

#endif // __ILSDISKPRINTMGR_H__
