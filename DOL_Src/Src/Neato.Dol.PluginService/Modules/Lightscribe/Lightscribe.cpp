#include "stdafx.h"
#include "LightscribeHelper.h"

CLightscribeHelper * g_pHelper = 0;
HMODULE				 g_hModule = 0;

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
	g_hModule = (HMODULE)hModule;
    return TRUE;
}

extern "C"
BOOL CALLBACK  GetInfo(BSTR * pbstrInfo)
{
	CComBSTR bstr = 
		L"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
		L"<ModuleInfo Version=\""
        PLUGIN_VERSION
        L"\" Name=\"LightscribeModule\" Description=\"Lightscribe printing Module\" Filename=\"modules\\Lightscribe.dll\">"
		L"  <Commands>"
		L"    <Command Name=\"LS_PRINT\" CopyRequest=\"0\" />"
		L"    <Command Name=\"LS_DETECT_DEVICE\" />"
		L"  </Commands>"
		L"</ModuleInfo>";
	*pbstrInfo = bstr.Detach();
	return TRUE;
}

extern "C"
BOOL CALLBACK Init(BOOL bInit)
{
	if(bInit)
	{
		g_pHelper = new CLightscribeHelper();
	}
	else
	{
		if (g_pHelper) delete g_pHelper;
	}
	return TRUE;
}

extern "C"
BOOL CALLBACK Invoke(BSTR bstrRequest, BSTR * pbstrResponse)
{
	try
	{
		CComPtr<MSXML2::IXMLDOMElement> pRequestEl;
		CXmlHelpers::LoadXml(bstrRequest, &pRequestEl);

		CComBSTR bstrCmd;
		CXmlHelpers::GetAttribute(pRequestEl, L"Name", &bstrCmd);
		if (wcsicmp(bstrCmd, L"LS_PRINT") == 0)
		{
			return g_pHelper->Print(bstrRequest, pbstrResponse);
		}
		else if (wcsicmp(bstrCmd, L"LS_DETECT_DEVICE") == 0)
		{
			return g_pHelper->DetectDevice(bstrRequest, pbstrResponse);
		}
		else
		{
			return FALSE;
		}
	}
	catch(...) {return FALSE;}
	return TRUE;
}
