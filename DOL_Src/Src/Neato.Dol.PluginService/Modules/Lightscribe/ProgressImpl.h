#ifndef __PROGRESSIMPL_H__
#define __PROGRESSIMPL_H__

#include "ILSDiscPrintProgressEvents.h"
#include "LSTypes.h"

enum EPrintStatus
{
	eStarting,	// Executing LightScribe job
	ePreparing,	// LightScribe is preparing printing
	ePrinting,	// LightScribe is printing
	eDone,		// Done OK
	eError		// Initialization or printing error
};

class CProgressImpl : public ILSDiscPrintProgressEvents
{
public:
	CProgressImpl();

	virtual ~CProgressImpl();

	virtual HRESULT STDMETHODCALLTYPE QueryInterface( 
		/* [in] */ REFIID riid,
		/* [iid_is][out] */ void** ppvObject);

	virtual ULONG STDMETHODCALLTYPE AddRef();

	virtual ULONG STDMETHODCALLTYPE Release();

	virtual HRESULT _stdcall NotifyPreparePrintProgress(
					/*[in]*/ int current, 
					/*[in]*/ int final);
	virtual HRESULT _stdcall NotifyPrintProgress(
					/*[in]*/ int areaPrinted, 
					/*[in]*/ int totalArea);
	virtual HRESULT _stdcall NotifyPrintComplete(/*[in]*/ HRESULT status);
	virtual HRESULT _stdcall QueryCancel(/*[out, retval]*/ VARIANT_BOOL* bCancel);
	virtual HRESULT _stdcall ReportLabelTimeEstimate(
					/*[in]*/ long seconds, 
					/*[out, retval]*/ VARIANT_BOOL* bCancel);

protected:
	EPrintStatus m_ePrintStatus;
	BOOL m_bCancel;
	long m_iLabelTime;	// label time estimation in seconds
	int m_iProgress;	// Current in per cents

private:
	long m_iRef;
};

#endif // __PROGRESSIMPL_H__
