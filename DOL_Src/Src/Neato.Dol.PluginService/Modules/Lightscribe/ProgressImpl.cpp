#include "StdAfx.h"
#include "ProgressImpl.h"

CProgressImpl::CProgressImpl() :
	m_ePrintStatus(eStarting),
	m_bCancel(FALSE),
	m_iLabelTime(0),
	m_iProgress(0),
	m_iRef(0)
{
}

CProgressImpl::~CProgressImpl()
{
}

HRESULT CProgressImpl::QueryInterface( 
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void** ppvObject)
{
	if (riid == __uuidof(ILSDiscPrintProgressEvents))
	{
		*ppvObject =
			reinterpret_cast<ILSDiscPrintProgressEvents*>(this);
		return S_OK;
	}
	else if (riid == __uuidof(IUnknown))
	{
		*ppvObject =
			reinterpret_cast<IUnknown*>(this);
		return S_OK;
	}
	else
	{
		return E_NOTIMPL;
	}
}

ULONG CProgressImpl::AddRef()
{
	return ++m_iRef;
}

ULONG CProgressImpl::Release()
{
	return --m_iRef;
}

HRESULT CProgressImpl::NotifyPreparePrintProgress(
		/*[in]*/ int current, 
		/*[in]*/ int final)
{
	m_iProgress = current * 100 / final;

	m_ePrintStatus = ePreparing;

	return S_OK;
}

HRESULT CProgressImpl::NotifyPrintProgress(
		/*[in]*/ int areaPrinted, 
		/*[in]*/ int totalArea)
{
	// totalArea is a very large number
	m_iProgress = int(((long long) areaPrinted) * 100 / (long long)totalArea);

	m_ePrintStatus = ePrinting;

	return S_OK;
}

HRESULT CProgressImpl::NotifyPrintComplete(/*[in]*/ HRESULT status)
{
	if (status == S_OK)
	{
		m_iProgress = 100;
		m_ePrintStatus = eDone;
	}
	else
	{
		m_ePrintStatus = eError;
	}

	return S_OK;
}

HRESULT CProgressImpl::QueryCancel(/*[out, retval]*/ VARIANT_BOOL* bCancel)
{
	*bCancel = m_bCancel ? VARIANT_TRUE : VARIANT_FALSE;
	return S_OK;
}

HRESULT CProgressImpl::ReportLabelTimeEstimate(
		/*[in]*/ long seconds, 
		/*[out, retval]*/ VARIANT_BOOL* bCancel)
{
	m_iLabelTime = seconds;
	*bCancel = m_bCancel ? VARIANT_TRUE : VARIANT_FALSE;
	return S_OK;
}
