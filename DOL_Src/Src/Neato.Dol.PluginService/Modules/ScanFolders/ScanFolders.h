#pragma warning(disable:4099) 
#pragma comment(lib, "comsupp.lib")
#pragma comment(lib, "kernel32.lib")

#import <msxml3.dll> raw_interfaces_only
#include <msxml.h>
#include "xmlhelpers.h"
#include "Playlist.h"
#include "filehelpers.h"

#include <atlstr.h>

HANDLE hMod;

BOOL GetScanFolders(MSXML2::IXMLDOMElement *xmlRoot, CString folder);
BOOL GetScanFiles(MSXML2::IXMLDOMElement *xmlRoot, CString folder, BOOL includeSubFolders = FALSE);
void ReadFileMediaTag(CPlayList& mPlayList, CString filename);
