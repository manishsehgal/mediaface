// ScanFolders.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "ScanFolders.h"

#define MSGBOX(msg) MessageBox(NULL,msg,NULL,NULL);
BSTR bstrInfo,bstrReq,bstrResp;

BOOL APIENTRY DllMain( HANDLE hModule, 
					  DWORD  ul_reason_for_call, 
					  LPVOID lpReserved
					  )
{
	hMod=hModule;
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

extern "C" /*__declspec(dllexport)*/
BOOL CALLBACK Init(BOOL bInit)
{
	if(bInit)
	{
	//		CoInitialize(NULL);//Initialised in service... skipped
	}
	else
	{
		try{
			if(bstrReq!=NULL)SysFreeString(bstrReq);
			if(bstrInfo!=NULL)SysFreeString(bstrInfo);
			if(bstrResp!=NULL)SysFreeString(bstrResp);
		//	CoUninitialize();
		}catch(...){
			return FALSE;
		}

	}
	return TRUE;
}
extern "C" /*__declspec(dllexport)*/
BOOL CALLBACK  Invoke(BSTR bstrRequest, BSTR * pbstrResponse)
{
	_bstr_t bstrXMLHeader("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
	_bstr_t bstrXMLResponseDraft(bstrXMLHeader+"<Response Error=\"0\"></Response>");
	try
	{
		//MSXML2::IXMLDOMElement xmlRootElement;

		CComPtr<MSXML2::IXMLDOMElement> xmlRoot=NULL,xmlReq=NULL,xmlParams=NULL,xmlReqParam=NULL,xmlResponse=NULL;
		CXmlHelpers::LoadXml(bstrRequest,&xmlRoot);
		CXmlHelpers::LoadXml(bstrXMLResponseDraft,&xmlResponse);
		if(!xmlResponse||!xmlRoot)
		{
			*pbstrResponse=SysAllocString(L"<Response Error=\"1\" ErrorDesc=\"Cannot create response XML\"><Params/></Response>");
			return TRUE;
		}
		BSTR bstrCommand;

		CXmlHelpers::GetAttribute(xmlRoot,L"Name",&bstrCommand);
		CXmlHelpers::GetChildElement(xmlRoot,L"Params",&xmlParams);
		if(!bstrCommand)
			return FALSE;
	
		BSTR folder;
		CComPtr<MSXML2::IXMLDOMElement> xmlFolder;
		CXmlHelpers::GetChildElement(xmlParams, L"Folder", &xmlFolder);
		CXmlHelpers::GetAttribute(xmlFolder, L"Name", &folder);

		BOOL bRet = FALSE;
		if(!wcscmp(bstrCommand,L"GET_SCAN_FOLDERS"))
		{
			bRet = GetScanFolders(xmlResponse, CString(folder));
		}
		else if(!wcscmp(bstrCommand,L"GET_SCAN_FILES"))
		{
			BSTR bstrIncludeSubFolders;
			CXmlHelpers::GetAttribute(xmlFolder, L"IncludeSubFolders", &bstrIncludeSubFolders);
			BOOL includeSubFolders =  CString(bstrIncludeSubFolders).MakeLower() == L"true";
			bRet = GetScanFiles(xmlResponse, CString(folder), includeSubFolders);
		}

		if(bRet)
		{
			_bstr_t resp;
			resp+=bstrXMLHeader;
			BSTR bstrXML;
			xmlResponse->get_xml(&bstrXML);
			resp+=bstrXML;
			*pbstrResponse=resp.copy();
			SysFreeString(bstrXML);
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	catch(...)
	{
		return FALSE;
	}
}

extern "C"
BOOL CALLBACK  GetInfo(BSTR * pbstrInfo)
{
	try
	{
		HGLOBAL pchVersionInfo=LoadResource(HMODULE(hMod),FindResource(HMODULE(hMod),MAKEINTRESOURCE(IDX_VERINFOXML),TEXT("XML")));
		if(!pchVersionInfo)
			return FALSE;

		*pbstrInfo=_com_util::ConvertStringToBSTR((char*)pchVersionInfo);
	}
	catch(...)
	{return FALSE;};
	//bstrInfo=temp;
	return TRUE;
}

BOOL GetScanFolders(MSXML2::IXMLDOMElement *xmlRoot, CString folder)
{
	try
	{
		FolderList lstFolders;
		CFileHelpers::EnumSubFolders(folder, lstFolders);

		CComPtr<MSXML2::IXMLDOMElement> xmlParams;
		CComPtr<MSXML2::IXMLDOMElement> xmlFolders;

		if(lstFolders.size() != 0)
		{
			CXmlHelpers::AddElement(xmlRoot, L"Params", &xmlParams);
			CXmlHelpers::AddElement(xmlRoot, L"Folders", &xmlFolders);

			char chFoldersCount[3];
			_itoa(lstFolders.size(), chFoldersCount, 10);
			CXmlHelpers::SetAttribute(xmlFolders, L"Count", CComBSTR(chFoldersCount));
		}

		for(FolderList::iterator i = lstFolders.begin(); i != lstFolders.end(); ++i)
		{
			CComPtr<MSXML2::IXMLDOMElement> xmlFolder;
			CXmlHelpers::AddElement(xmlFolders, L"Folder", &xmlFolder);
			CXmlHelpers::SetAttribute(xmlFolder, L"Name", CComBSTR(*i));
		}
	}
	catch(...)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL GetScanFiles(MSXML2::IXMLDOMElement *xmlRoot, CString folder, BOOL includeSubFolders)
{
	try
	{
		FileList lstFiles;
		ExtensionList lstExtensions;
		//lstExtensions.push_back(PLS_EXT);
		//lstExtensions.push_back(WPL_EXT);
		//lstExtensions.push_back(ASX_EXT);
		//lstExtensions.push_back(M3U_EXT);
		lstExtensions.push_back(MP3_EXT);
		lstExtensions.push_back(WAV_EXT);
		lstExtensions.push_back(WMA_EXT);
		CFileHelpers::FindFiles(folder, lstExtensions, lstFiles, includeSubFolders);

		CComPtr<MSXML2::IXMLDOMElement> xmlParams;
		CComPtr<MSXML2::IXMLDOMElement> xmlFolders;

		CPlayList mPlayList;
		for(FileList::iterator i = lstFiles.begin(); i != lstFiles.end(); ++i)
			ReadFileMediaTag(mPlayList, *i);

		CComPtr<MSXML2::IXMLDOMElement> pPlayList=NULL,pTracks=NULL,pParams=NULL;
		//CXmlHelpers::AddElement(xmlRoot,L"Response",&xmlRoot);
		CXmlHelpers::AddElement(xmlRoot,L"Params",&pParams);

		int nTracksCount = mPlayList.GetTracksCount();
		if(nTracksCount<1)
		{
			CXmlHelpers::SetAttribute(xmlRoot,L"Error",L"1");
			switch(nTracksCount)
			{
			case 0:
					CXmlHelpers::SetAttribute(xmlRoot,L"ErrorDesc",L"Cannot read CD-TEXT");
					break;
			case -1:
					CXmlHelpers::SetAttribute(xmlRoot,L"ErrorDesc",L"Selected drive cannot read CDText");
					break;
			}
		}
		else
		{
			CXmlHelpers::SetAttribute(xmlRoot,L"Error",L"0");
			CXmlHelpers::AddElement(pParams,L"PlayList",&pPlayList);
			char chTrackCount[16];
			_itoa(nTracksCount,chTrackCount,10);
			CXmlHelpers::SetAttribute(pPlayList,L"TrackCount",CComBSTR(chTrackCount));
			CXmlHelpers::SetAttribute(pPlayList,L"Artist",mPlayList.m_bstrArtist);
			CXmlHelpers::SetAttribute(pPlayList,L"Album",mPlayList.m_bstrAlbum);
			CXmlHelpers::AddElement(pPlayList,L"Tracks",&pTracks);
			for(int i = 0; i < nTracksCount; ++i)
			{
				CComPtr<MSXML2::IXMLDOMElement> pTrack;
				CXmlHelpers::AddElement(pTracks,L"Track",&pTrack);
				CXmlHelpers::SetAttribute(pTrack,L"Artist",mPlayList.GetTrackByNum(i)->m_bstrArtist);
				CXmlHelpers::SetAttribute(pTrack,L"Title",mPlayList.GetTrackByNum(i)->m_bstrTitle);
				CXmlHelpers::SetAttribute(pTrack,L"Duration",mPlayList.GetTrackByNum(i)->m_bstrDuration);
			}
		}
		return TRUE;
	}
	catch(...)
	{
		return FALSE;
	}
}

void ReadFileMediaTag(CPlayList& mPlayList, CString filename)
{
	TRACK track;
	track.m_bstrAlbum = L"ALBUM";
	track.m_bstrArtist = L"ARTIST";
	track.m_bstrDuration = L"0";
	track.m_bstrTitle = filename;
	track.m_bstrUrl = filename;
	mPlayList.AddTrack(track);
}