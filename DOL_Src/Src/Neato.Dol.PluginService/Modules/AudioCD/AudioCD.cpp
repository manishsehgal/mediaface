// audiocd.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "audiocd.h"
#include "playlist.h"

#include "AudioCDTools.h"
#define MSGBOX(msg) MessageBox(NULL,msg,NULL,NULL);
BSTR bstrInfo,bstrReq,bstrResp;

BOOL APIENTRY DllMain( HANDLE hModule, 
					  DWORD  ul_reason_for_call, 
					  LPVOID lpReserved
					  )
{
	hMod=hModule;
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

extern "C" /*__declspec(dllexport)*/
BOOL CALLBACK Init(BOOL bInit)
{
	if(bInit)
	{
	//		CoInitialize(NULL);//Initialised in service... skipped
	}
	else
	{
		try{
			if(bstrReq!=NULL)SysFreeString(bstrReq);
			if(bstrInfo!=NULL)SysFreeString(bstrInfo);
			if(bstrResp!=NULL)SysFreeString(bstrResp);
		//	CoUninitialize();
		}catch(...){
			return FALSE;
		}

	}
	return TRUE;
}
extern "C" /*__declspec(dllexport)*/
BOOL CALLBACK  Invoke(BSTR bstrRequest, BSTR * pbstrResponse)
{
	
	_bstr_t bstrXMLHeader("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
	_bstr_t bstrXMLResponseDraft(bstrXMLHeader+"<Response Error=\"0\"></Response>");
	try
	{
		//MSXML2::IXMLDOMElement xmlRootElement;

		CComPtr<MSXML2::IXMLDOMElement> xmlRoot=NULL,xmlReq=NULL,xmlParams=NULL,xmlReqParam=NULL,xmlResponse=NULL;
		CXmlHelpers::LoadXml(bstrRequest,&xmlRoot);
		CXmlHelpers::LoadXml(bstrXMLResponseDraft,&xmlResponse);
		if(!xmlResponse||!xmlRoot)
		{
			*pbstrResponse=SysAllocString(L"<Response Error=\"1\" ErrorDesc=\"Cannot create response XML\"><Params/></Response>");
			return TRUE;
		}
		BSTR bstrCommand;

		//xmlRoot->setAttribute(L"Type",CComVariant(L"Response"));
		//CXmlHelpers::GetChildElement(xmlRoot,L"Request",&xmlReq);
		CXmlHelpers::GetAttribute(xmlRoot,L"Name",&bstrCommand);
		CXmlHelpers::GetChildElement(xmlRoot,L"Params",&xmlParams);
		if(!bstrCommand)
			return FALSE;
	
		BOOL bRet;
		if(!wcscmp(bstrCommand,L"GET_AUDIOCD_DRIVES"))
		{

			bRet=GetCDDrives(xmlResponse);
			if(bRet)
			{

				_bstr_t resp;
				resp+=bstrXMLHeader;
				BSTR bstrXML=NULL;

				xmlResponse->get_xml(&bstrXML);

				resp+=bstrXML;

				*pbstrResponse=resp.copy();

				SysFreeString(bstrXML);

			}
			else return FALSE;
		}
		else if(!wcscmp(bstrCommand,L"GET_AUDIOCD_PLAYLIST"))
		{

			CXmlHelpers::GetChildElement(xmlParams,L"Drive",&xmlReqParam);
			BSTR bstrDriveLetter;
			if(S_OK!=CXmlHelpers::GetAttribute(xmlReqParam,L"Name",&bstrDriveLetter))
			{
				bRet=TRUE;
				CXmlHelpers::SetAttribute(xmlReqParam,L"Error",L"1");
				CXmlHelpers::SetAttribute(xmlReqParam,L"ErrorDesc",L"Error drive parameter");

			}
			else
				bRet=GetCDPlayList(xmlResponse,bstrDriveLetter);
			if(bRet)
			{
				_bstr_t resp;
				resp+=bstrXMLHeader;
				BSTR bstrXML;
				xmlResponse->get_xml(&bstrXML);
				resp+=bstrXML;
				*pbstrResponse=resp.copy();
				SysFreeString(bstrXML);

			}
			else return FALSE;
			SysFreeString(bstrDriveLetter);
		

		}

		SysFreeString(bstrCommand);
		return bRet;
	}catch(...)
	{return FALSE;}
	
}
extern "C"
BOOL CALLBACK  GetInfo(BSTR * pbstrInfo)
{
	try
	{


		HGLOBAL pchVersionInfo=LoadResource(HMODULE(hMod),FindResource(HMODULE(hMod),MAKEINTRESOURCE(IDX_VERINFOXML),TEXT("XML")));
		if(!pchVersionInfo)
			return FALSE;

		*pbstrInfo=_com_util::ConvertStringToBSTR((char*)pchVersionInfo);
	}
	catch(...)
	{return FALSE;};
	//bstrInfo=temp;
	return TRUE;
}
BOOL GetCDDrives(MSXML2::IXMLDOMElement *xmlRoot)
{
	//CSCSIReader *rdr;
	try
	{
		//_bstr_t bstrRespHeader("<Response Error=\"");
		//_bstr_t bstrRespBody("\">\r\n");


		
	/*	OSVERSIONINFO sOSVers;
		sOSVers.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		GetVersionEx(&sOSVers);
		if (sOSVers.dwMajorVersion == 4 && sOSVers.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS)
		{		
			rdr = new  CSCSIReader98();
		}
		else
		{
			rdr = new CSCSIReaderNT();
		}
		if(!rdr)
		{
			CXmlHelpers::SetAttribute(xmlRoot,L"Error",L"1");
			CXmlHelpers::SetAttribute(xmlRoot,L"ErrorDesc",L"Cannot create CSCSIReader");
			return TRUE;
		}
		if(!IsCDROMSupport())
		{
			CXmlHelpers::SetAttribute(xmlRoot,L"Error",L"1");
			CXmlHelpers::SetAttribute(xmlRoot,L"ErrorDesc",L"No CD Drives found or no CDROM support");
			if(rdr)
				delete rdr;
			return TRUE;
		}*/


		/*WCHAR szDriveLetter[] = L"_";
		DWORD dwCD = IsCDROMSupport();
		DWORD dCurDrive  = 0;	*/
		CComPtr<MSXML2::IXMLDOMElement> pDrives=NULL,pDrive=NULL,pParams=NULL;
		//CXmlHelpers::AddElement(xmlRoot,L"Response",&xmlRoot);
		CXmlHelpers::SetAttribute(xmlRoot,L"Error",L"0");
		CXmlHelpers::AddElement(xmlRoot,L"Params",&pParams);
		CXmlHelpers::AddElement(pParams,L"Drives",&pDrives);
        long lCount = 0;
        wchar_t wLoop;
        wchar_t res[2]={0,0};
//        wchar_t awDrives[26];

        for (wLoop = 'A' ; wLoop <= 'Z' ; wLoop ++) 
        {
            CComBSTR cbstRoot(1, &wLoop);
            cbstRoot += L":\\";

            if (GetDriveType(_bstr_t(cbstRoot)) == DRIVE_CDROM) 
            {
                //awDrives[lCount] = wLoop;
                CComPtr<MSXML2::IXMLDOMElement>pDrive=NULL;	
                CXmlHelpers::AddElement(pDrives,L"Drive",&pDrive);
                res[0]=wLoop;
                CXmlHelpers::SetAttribute(pDrive,L"Name",_bstr_t(res));
                lCount++;
            }
        }
        if(lCount == 0){
            CXmlHelpers::SetAttribute(xmlRoot,L"Error",L"1");
            CXmlHelpers::SetAttribute(xmlRoot,L"ErrorDesc",L"No CD Drives found");
        }
	
	/*	int nDrives=0;
		while (dwCD)
		{
			if (dwCD & 1)
			{
				CComPtr<MSXML2::IXMLDOMElement>pDrive=NULL;		
				szDriveLetter[0] = L'A' + (WCHAR)dCurDrive;
				nDrives++;
				CXmlHelpers::AddElement(pDrives,L"Drive",&pDrive);
				CXmlHelpers::SetAttribute(pDrive,L"Name",(LPCWSTR)&szDriveLetter[0]);

			}

			dCurDrive++;
			dwCD >>= 1;
		}*/
		wchar_t count[2];
		_itow(lCount,count,10);
		CXmlHelpers::SetAttribute(pDrives,L"Count",count);

	}catch(...){
        return false;
    }
	return TRUE;
}

BOOL GetCDPlayList(MSXML2::IXMLDOMElement *xmlRoot,BSTR bstrDrive)
{
/*	CSCSIReader *rdr;
	*/
	try
	{
		
		_bstr_t bstrtDrive;
		bstrtDrive.Attach(bstrDrive);
		char *chDrive=bstrtDrive;
	//	if(!rdr->setWorkDrive((chDrive[0]- _T('A') + 1)))
	//	{
		//	CXmlHelpers::SetAttribute(xmlRoot,L"Error",L"1");
		//	CXmlHelpers::SetAttribute(xmlRoot,L"ErrorDesc",L"Wrong CD Letter");
		//	return TRUE;
	//	}
		int nTracksCount=0;
		
		//CPlayList mPlayList;
        CAudioCDTools audioCDTools;
		/*if(rdr->isCDROMwithCDTextReadAvailable())
		{
			rdr->ReadPlayList(mPlayList);
			nTracksCount=mPlayList.GetTracksCount();
		}
		else if(!rdr->canCDROMReadCDText())
			nTracksCount=-1;*/

		
		CComPtr<MSXML2::IXMLDOMElement> pPlayList=NULL,pTracks=NULL,pParams=NULL;
		//CXmlHelpers::AddElement(xmlRoot,L"Response",&xmlRoot);
		CXmlHelpers::AddElement(xmlRoot,L"Params",&pParams);
        long tracks[100]={0};
        nTracksCount = audioCDTools.GetTracks(chDrive[0],tracks);
		if(nTracksCount<1)
		{
			CXmlHelpers::SetAttribute(xmlRoot,L"Error",L"1");
			switch(nTracksCount)
			{
			case 0:
					CXmlHelpers::SetAttribute(xmlRoot,L"ErrorDesc",L"Cannot read tracks");
					break;
		//	case -1:
		//			CXmlHelpers::SetAttribute(xmlRoot,L"ErrorDesc",L"Selected drive cannot read tracks");
		//			break;
            default:break;
			}

		}
		else
		{
			CXmlHelpers::SetAttribute(xmlRoot,L"Error",L"0");
			CXmlHelpers::AddElement(pParams,L"PlayList",&pPlayList);
			char chTrackCount[3];
			_itoa(nTracksCount,chTrackCount,10);
			CXmlHelpers::SetAttribute(pPlayList, L"TrackCount",CComBSTR(chTrackCount));
            CXmlHelpers::SetAttribute(pPlayList, L"Artist",L"Artist");
			CXmlHelpers::SetAttribute(pPlayList, L"Album",L"Album");
            CXmlHelpers::SetAttribute(pPlayList, L"SrcType", L"AudioCD");
            CXmlHelpers::SetAttribute(pPlayList, L"SrcName", bstrDrive);

			CXmlHelpers::AddElement(pPlayList,L"Tracks",&pTracks);
			for(int i=0;i<nTracksCount;i++)
			{
				CComPtr<MSXML2::IXMLDOMElement> pTrack;
				CXmlHelpers::AddElement(pTracks,L"Track",&pTrack);

                WCHAR buf[128];
                swprintf(buf, L"%d", i+1);
				CXmlHelpers::SetAttribute(pTrack, L"Id", buf);

                CXmlHelpers::SetAttribute(pTrack, L"Artist", L"Artist");

                swprintf(buf, L"Track %i", i+1);
				CXmlHelpers::SetAttribute(pTrack, L"Title", buf);

                char temptime[16]  = {0};
                char chMinutes[16] = {0};
                char chSeconds[16] = {0};
                long iSeconds = tracks[i];
				int nSeconds = iSeconds % 60;
				int nMinutes = (iSeconds - nSeconds) / 60;

				_bstr_t bstrDur;
				sprintf(chMinutes,((nMinutes<10&&nMinutes>-10)?"0%1i":"%2i"),nMinutes);
				sprintf(chSeconds,((nSeconds<10&&nSeconds>-10)?"0%1i":"%2i"),nSeconds);
				sprintf(temptime,"%s%s%s",chMinutes,TIME_DELIMITER_STR,chSeconds);
				CXmlHelpers::SetAttribute(pTrack,L"Duration",_bstr_t(temptime));
			}
		}
		bstrtDrive.Detach();
	}catch(...)
	{return FALSE;}
	return TRUE;
}


/*DWORD IsCDROMSupport()
{
DWORD dLogicDrives = GetLogicalDrives();
TCHAR chDiskLetter[] = TEXT("A:");
DWORD dTestDrives = dLogicDrives;
dLogicDrives = 0;
while (dTestDrives)
{
bool bIsDrive = dTestDrives & 1;	
if (bIsDrive && GetDriveType(chDiskLetter) == DRIVE_CDROM)
dLogicDrives |= 1 << (chDiskLetter[0] - TEXT('A'));    
dTestDrives  >>= 1;
chDiskLetter[0]++;
}
return dLogicDrives;
}*/