
#include "StdAfx.h"
#include "audiocdtools.h"

#include "CDRipper\CoCDRipper.h"
#include <comutil.h>

CAudioCDTools::CAudioCDTools(void)
{
}

CAudioCDTools::~CAudioCDTools(void)
{
}


int CAudioCDTools::GetTracks(BSTR bstrCD,long tracks[]){
    LPTSTR pszCD = _bstr_t(bstrCD);

	if (lstrlen(pszCD) > 0)
	{
		::CharUpper(pszCD);

		long lCd = 0;

		for (int i = 0 ; i < ('Z' - 'A') ; ++i)
		{
			TCHAR szDrive[] = _T("A:");
			szDrive[0] += i;

			DWORD dwType = ::GetDriveType(szDrive);
			if (dwType == DRIVE_CDROM)
				++lCd;

			if (pszCD[0] == szDrive[0])
				return GetTracks(lCd - 1,tracks); // zero based count
		}
	}
    else return 0;

    return 0;
}
int CAudioCDTools::GetTracks(long dwCD,long tracks[]){
    try{
        CCoCDRipper *ripper = new CCoCDRipper();
        ripper->put_ActiveDevice(dwCD);
        long nTrackNum;
        ripper->GetTracksCount(&nTrackNum);
        if(nTrackNum <= 0){
            ripper->CloseDeviceHandle();
            delete ripper;
            return 0;
        }
        for(long i = 0 ;i < nTrackNum;i++){
            ripper->GetTrackLength(i+1, &tracks[i]);
        }
        ripper->CloseDeviceHandle();
        delete ripper;

    return nTrackNum;
    }catch(...) {
        return 0;
    }
}