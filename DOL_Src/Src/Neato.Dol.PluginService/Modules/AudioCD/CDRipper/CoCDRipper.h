// CoCDRipper.h : Declaration of the CCoCDRipper

#ifndef __COCDRIPPER_H_
#define __COCDRIPPER_H_

#include "..\\stdafx.h"       // main symbols
#include <ntddscsi.h>
#include <ntddcdrm.h>


#include <memory>
#include "akrip\akrip32.h"
//#include "CDRipperCP.h"
//#include "vdibase.h"

#define RAW_SECTOR_SIZE			2352
#define COOKED_SECTOR_SIZE		2048
#define SECTORS_READ_AT_ONCE	50

typedef struct 
{
  char id_riff[4];
  long len_riff;
 
  char id_chuck[4];
  char fmt[4];
  long len_chuck;
 
  short  type;
  short  channels;
  long freq;
  long bytes;
  short  align;
  short  bits;
 
  char id_data[4];
  long len_data;
} TitleWave;

extern CDROM_TOC EmptyTocData;

typedef enum eRippeStrategy {rsIOCTL_CDROM, rsIOCTL_SCSI};

class CAbstractRippeStrategy
{
public:
	CAbstractRippeStrategy():m_bStop(false), m_tocData(EmptyTocData), m_nDiskIdx(-1){}
	virtual eRippeStrategy GetType() = 0;
	virtual STDMETHODIMP GetAvailableCDDrives(long *plCount, VARIANT *pNames) = 0;
	virtual BOOL InitDevice(const long nDiskIdx)
	{
		m_nDiskIdx = nDiskIdx;
		return TRUE;
	}
	virtual void CloseDeviceHandle(){m_tocData = EmptyTocData;}
	virtual BOOL GetTOC() = 0;
	virtual bool ReadTracks(long nTrackNo, const HANDLE hWaveFile, 
		const long lSecondsToGrab, long &nTrackSizeInBytes)
	{
		m_bStop = false;
		return true;
	}
	virtual long GetTracksCount();
	virtual bool IsTrackAudio(long lTrackNr);
	virtual bool IsTrackInRange(long lTrackNr);
	virtual long GetTrackSectorCount(long lTrackNr);
	virtual long GetTrackOffset(long lTrackNr);
	long GetTrackMin(long lTrackNr);
	long GetTrackSec(long lTrackNr);
	long GetTrackFrm(long lTrackNr);
	void Stop(){m_bStop = true;}
	bool IsTOCReaded() {return (m_tocData.Length[0] + m_tocData.Length[1]) > 0;}

protected:
	

	CDROM_TOC	m_tocData;
	bool m_bStop;
	long m_nDiskIdx;
	
};

/////////////////////////////////////////////////////////////////////////////
// CCoCDRipper

class  CCoCDRipper  {
public:
	CCoCDRipper()
	{
		m_nDiskIdx			= NULL;
		m_cbstOutputFile	= "";
		m_hWaveFile			= NULL;
		m_bThread			= FALSE;
	}





// ICDRipper
public:
	STDMETHOD(GetAvailableCDDrives)(/*[out]*/ long* plCount, /*[out, retval]*/ VARIANT* pNames);
	STDMETHOD(get_ActiveDevice)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_ActiveDevice)(/*[in]*/ long newVal);
	STDMETHOD(get_OutputFile)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_OutputFile)(/*[in]*/ BSTR newVal);
	STDMETHOD(GetTracksCount)(/*[out, retval]*/ long* plTracks);
	STDMETHOD(GetTrackLength)(/*[in]*/ long lTrackNr, /*[out]*/ long* plSec);
	STDMETHOD(IsTrackAudio)(/*[in]*/ long lTrackNr, /*[out, retval]*/ VARIANT_BOOL* pbAudio);
	STDMETHOD(GrabTrack)(/*[in]*/long lTrackNr, /*[in]*/ long lSecondsToGrab, /*[in]*/ VARIANT_BOOL bSync);
	STDMETHOD(StopGrabbing)();
    BOOL CloseDeviceHandle();

void FinalRelease() 
	{   
		m_pRippeStrategy->CloseDeviceHandle();
	}

private:
	static DWORD WINAPI ThreadProc___(LPVOID lpParam);

	BOOL TrackToFile(long lTrackNr, long lSecondsToGrab);
	void TrackGrabbingComplete();

	BOOL WriteWaveHdr(long lTrackSize);
	//long GetTrackOffset(long lTrackNr);
	long GetTrackSectorCount(long lTrackNr);
		
	BOOL GetTOC();
	BOOL InitOutputFile();
	BOOL InitDevice();
    
	HRESULT InitDevice(const long nDiskIdx);
	void SwitchRippeStrategy();
	bool IsRunWin9X();
		
	std::auto_ptr<CAbstractRippeStrategy> & GetRippeStrategy(eRippeStrategy RippeStrategy = rsIOCTL_CDROM);

	bool IsOutputFileOpened() {return m_hWaveFile != INVALID_HANDLE_VALUE && m_hWaveFile != NULL;}
	bool IsTOCReaded() {return GetRippeStrategy()->IsTOCReaded();}
	void CloseFileHandle();
		
	
	
private:
	long		m_nDiskIdx;
	CComBSTR	m_cbstOutputFile;
	HANDLE		m_hWaveFile;
	BOOL        m_bThread;
	std::auto_ptr<CAbstractRippeStrategy> m_pRippeStrategy;
};
class CRippeStrategy: public CAbstractRippeStrategy
{
public:
	virtual eRippeStrategy GetType(){return rsIOCTL_CDROM;}
	CRippeStrategy():m_hCD(NULL){}
	virtual BOOL InitDevice(const long nDiskIdx);
	void CloseDeviceHandle();
	HANDLE	GetCDHandle(){return m_hCD;}
	BOOL GetTOC();
	bool ReadTracks(long nTrackNo, const HANDLE hWaveFile, 
		const long lSecondsToGrab, long &nTrackSizeInBytes);
	virtual STDMETHODIMP GetAvailableCDDrives(long *plCount, VARIANT *pNames);
protected:
	BOOL InitDevice();
private:
	bool IsDeviceOpened();
	CComBSTR GetCDROMPath(long szDiskIdx);
    CComBSTR GetCDROMPathByCharCode(long szDiskIdx);
	HANDLE		m_hCD;
	DWORD GetReadSize(){return SECTORS_READ_AT_ONCE * RAW_SECTOR_SIZE;}
	DWORD ReadChunck(LPBYTE	lpChunck, LONGLONG nStartOffset);
	LONGLONG GetStartOffset(long nTrackNo, long nPortionNo)
	{
		return (GetTrackOffset(nTrackNo) + nPortionNo * SECTORS_READ_AT_ONCE) 
			* COOKED_SECTOR_SIZE;
	}
};

class CmfRippeStrategy: public CAbstractRippeStrategy
{
public:

	CmfRippeStrategy()
	{
		memset(&m_CDList, 0, sizeof(m_CDList));
	}
	virtual eRippeStrategy GetType(){return rsIOCTL_SCSI;}
	virtual BOOL InitDevice(const long nDiskIdx);
	virtual void CloseDeviceHandle()
	{
		CAbstractRippeStrategy::CloseDeviceHandle();
		CloseCDHandle(m_hCDROM);
		m_hCDROM = NULL;
	}
	virtual BOOL GetTOC();
	virtual bool ReadTracks(long nTrackNo, const HANDLE hWaveFile, 
		const long lSecondsToGrab, long &nTrackSizeInBytes);
	virtual long GetTrackSectorCount(long lTrackNr);
	virtual STDMETHODIMP GetAvailableCDDrives(long *plCount, VARIANT *pNames);
private:
	static HCDROM m_hCDROM;
	static long m_nCDIdx;
	CDLIST m_CDList;
};

#endif //__COCDRIPPER_H_
