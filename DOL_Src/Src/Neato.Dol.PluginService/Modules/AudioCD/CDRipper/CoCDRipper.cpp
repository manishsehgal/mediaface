// CoCDRipper.cpp : Implementation of CCoCDRipper
#include "..\\stdafx.h"


#include "CoCDRipper.h"

#include "TxtCnv.h"

CDROM_TOC	EmptyTocData = {0};

const TRACKSPERSEC = 75;


struct ThreadData
{
	ThreadData(CCoCDRipper* ripper, long track, long secondsToGrab)
		: m_ripper(ripper), m_lTrack(track), m_lSecondsToGrab() {}
	
	CCoCDRipper* m_ripper;
	long		m_lTrack;
	long		m_lSecondsToGrab;
};

DWORD WINAPI CCoCDRipper::ThreadProc___(LPVOID lpParam)
{
	ThreadData* data = (ThreadData*)lpParam;
	long track = data->m_lTrack;
	long secondsToGrab = data->m_lSecondsToGrab;
	CCoCDRipper * pCCoCDRipper = data->m_ripper;
	delete data;

	pCCoCDRipper->TrackToFile(track, secondsToGrab);
	pCCoCDRipper->TrackGrabbingComplete();
	pCCoCDRipper->m_bThread = FALSE;
	return 0;
}

long CAbstractRippeStrategy::GetTracksCount()
{
	return m_tocData.LastTrack - m_tocData.FirstTrack + 1;
}


bool CAbstractRippeStrategy::IsTrackInRange(long lTrackNr)
{
	if (lTrackNr < m_tocData.FirstTrack || lTrackNr > m_tocData.LastTrack)
	{
		return false;
	}
	
	return true;
}

bool CAbstractRippeStrategy::IsTrackAudio(long lTrackNr)
{
	const int nDataControlMask = 4;
	return (m_tocData.TrackData[lTrackNr - 1].Control & nDataControlMask) == 0;
}

long CAbstractRippeStrategy::GetTrackMin(long lTrackNr)
{
	return m_tocData.TrackData[lTrackNr - 1].Address[1];
}

long CAbstractRippeStrategy::GetTrackSec(long lTrackNr)
{
	return m_tocData.TrackData[lTrackNr - 1].Address[2];
}

long CAbstractRippeStrategy::GetTrackFrm(long lTrackNr)
{
	return m_tocData.TrackData[lTrackNr - 1].Address[3];
}

long CAbstractRippeStrategy::GetTrackSectorCount(long lTrackNr)
{
	return (GetTrackFrm(lTrackNr + 1) - GetTrackFrm(lTrackNr)) 
		+ (GetTrackSec(lTrackNr + 1) - GetTrackSec(lTrackNr)) * TRACKSPERSEC + 
		(GetTrackMin(lTrackNr + 1) - GetTrackMin(lTrackNr)) * 60 *TRACKSPERSEC;
}

long CAbstractRippeStrategy::GetTrackOffset(long lTrackNr)
{
	return GetTrackFrm(lTrackNr) + GetTrackSec(lTrackNr) * TRACKSPERSEC 
		+ GetTrackMin(lTrackNr) * 60 *TRACKSPERSEC - 150;;
}

///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////

long CmfRippeStrategy::m_nCDIdx = -1;
HCDROM CmfRippeStrategy::m_hCDROM = NULL;

BOOL CmfRippeStrategy::InitDevice(const long nDiskIdx)
{

	if (GetCDList(&m_CDList) != SS_COMP) return FALSE;
	CAbstractRippeStrategy::InitDevice(nDiskIdx);
	USES_CONVERSION;
	if (m_nCDIdx == nDiskIdx && m_hCDROM != NULL) return TRUE;
	m_nCDIdx = nDiskIdx;

	ATLASSERT(nDiskIdx >= 0);
	if (nDiskIdx < 0) return E_FAIL;
	GETCDHAND cdh = {0};
	cdh.size        = sizeof(GETCDHAND);
	cdh.ver         = 1;
	cdh.ha          = m_CDList.cd[m_nCDIdx].ha;
	cdh.tgt         = m_CDList.cd[m_nCDIdx].tgt;
	cdh.lun         = m_CDList.cd[m_nCDIdx].lun;
	cdh.readType    = CDR_ANY;
	cdh.numOverlap  = (unsigned char)3;
	cdh.numJitter   = (unsigned char)1;
	m_hCDROM =  GetCDHandle( &cdh );
	return m_hCDROM != NULL;
}

BOOL CmfRippeStrategy::GetTOC()
{
	BOOL bRes = ReadTOC(m_hCDROM, (LPTOC)&m_tocData) == SS_COMP;
	
	return bRes; 
}


LPTRACKBUF newTrackBuf( DWORD numFrames )
{
	LPTRACKBUF t;
	int numAlloc;
	
	numAlloc = (((int)numFrames)*2352) + TRACKBUFEXTRA;

	t = (LPTRACKBUF)malloc( numAlloc );

	if ( !t )
		return NULL;

	t->startFrame = 0;
	t->numFrames = 0;
	t->maxLen = numFrames * 2352;
	t->len = 0;
	t->status = 0;
	t->startOffset = 0;

	return t;
}

void MSB2DWORD( DWORD *d, BYTE *b )
{
	DWORD retVal;

	retVal = (DWORD)b[0];
	retVal = (retVal<<8) + (DWORD)b[1];
	retVal = (retVal<<8) + (DWORD)b[2];
	retVal = (retVal<<8) + (DWORD)b[3];

	*d = retVal;
}

long CmfRippeStrategy::GetTrackSectorCount(long nTrackNo)
{

	DWORD nTrackStart = 0;
	MSB2DWORD(&nTrackStart, (LPBYTE)(&m_tocData.TrackData[nTrackNo - 1].Address));

	DWORD nTrackEnd = 0;
	MSB2DWORD(&nTrackEnd, (LPBYTE)(&m_tocData.TrackData[nTrackNo].Address));
    const LEADOUT = (150*TRACKSPERSEC);
    if ( !IsTrackAudio(nTrackNo) && (nTrackEnd > LEADOUT)) 
		nTrackEnd -= LEADOUT;
	return nTrackEnd - nTrackStart;
}

bool CmfRippeStrategy::ReadTracks(long nTrackNo, const HANDLE hWaveFile, 
								  const long lSecondsToGrab, long &nTrackSizeInBytes)
{

	CAbstractRippeStrategy::ReadTracks(nTrackNo, hWaveFile, lSecondsToGrab, nTrackSizeInBytes);
	nTrackSizeInBytes = 0;
	LPTRACKBUF tbuf = NULL, tover = NULL;
	tbuf = newTrackBuf(27);
	if (tbuf == NULL)
	{

		return false;
	}

	MSB2DWORD(&tbuf->startFrame, (LPBYTE)(&m_tocData.TrackData[nTrackNo - 1].Address));
	DWORD nTrackLen = GetTrackSectorCount(nTrackNo);
	if (lSecondsToGrab != 0 && nTrackLen > lSecondsToGrab * TRACKSPERSEC)
		nTrackLen = lSecondsToGrab * TRACKSPERSEC;
	nTrackLen += tbuf->startFrame;

	tbuf->numFrames = 27; 
	tbuf->maxLen = tbuf->numFrames * 2352;
	tbuf->startOffset = 0;
	DWORD dwStatus = SS_ERR;
	while(tbuf->startFrame < nTrackLen && !m_bStop)
	{
		tbuf->len = 0;
		dwStatus = SS_ERR;
		for(int retries = 3; dwStatus != SS_COMP && retries; retries-- )
		{
			dwStatus = ReadCDAudioLBA(m_hCDROM, tbuf);
		}
		if (dwStatus == SS_COMP)
		{
			DWORD NumberOfBytesWritten = 0;
			if(!WriteFile(hWaveFile, &tbuf->buf,  tbuf->len, &NumberOfBytesWritten, NULL))
			{
				dwStatus = SS_ERR;
//				Error(_T("Cannot write to output file using CSCIRippeStrategy"));
				break;
			}
			nTrackSizeInBytes += NumberOfBytesWritten;
		}
		else break;
		tbuf->startFrame += 27;
	}
	if (dwStatus != SS_COMP) 
	{
		TCHAR Buf[MAX_PATH] = {0};
		_stprintf(Buf, _T("Cannot read using CSCIRippeStrategy SectorNo - %d"), tbuf->startFrame);
//		Error(_T("Cannot read using CSCIRippeStrategy"));
	}
	free(tbuf);
	return dwStatus == SS_COMP;
}

STDMETHODIMP CmfRippeStrategy::GetAvailableCDDrives(long *plCount, VARIANT *pNames)
{

	if (GetCDList(&m_CDList) != SS_COMP) return FALSE;
	VariantInit(pNames);
	pNames->vt = VT_ARRAY | VT_BSTR;

	SAFEARRAY *pSA;
	SAFEARRAYBOUND bounds = {m_CDList.num, 0};
	
	// Create the array.
	pSA = SafeArrayCreate(VT_BSTR, 1, &bounds);
	
	BSTR *theStrings;
	SafeArrayAccessData(pSA, (void**)&theStrings);
	for (int i = 0; i < m_CDList.num; ++i)
	{
		CTxtCnv tc;
		CComBSTR cbstRoot(tc.t2w(m_CDList.cd[i].id));
		theStrings[i] = cbstRoot.Detach();
		
	}
	SafeArrayUnaccessData(pSA);
	// Set return value.
	pNames->parray = pSA;
	*plCount = m_CDList.num;

	return S_OK;
}


DWORD CRippeStrategy::ReadChunck(LPBYTE	lpChunck, LONGLONG nStartOffset)
{

	RAW_READ_INFO rawReadInfo;
	rawReadInfo.TrackMode = CDDA;
	rawReadInfo.DiskOffset.QuadPart = nStartOffset;
	rawReadInfo.SectorCount= SECTORS_READ_AT_ONCE;

	const nErrorsDecrement = 30;
	for (int nTryCount = 3 * nErrorsDecrement; nTryCount > 0;)
	{
		DWORD	dwReturned= 0;
		if (::DeviceIoControl(GetCDHandle(), IOCTL_CDROM_RAW_READ, &rawReadInfo,
				sizeof(RAW_READ_INFO), lpChunck, GetReadSize(), &dwReturned, NULL))
			return dwReturned;

		DWORD dwError = ::GetLastError();
		if (dwError == ERROR_MEDIA_CHANGED)
		{
			BOOL bRes = InitDevice();
			ATLASSERT(bRes);
		}
		nTryCount -= nErrorsDecrement;

	}
	return 0;
}

class CHeapPtrA
{
private:
	LPBYTE	m_lpChunck;
public:
	CHeapPtrA(SIZE_T nSize)
	{
		m_lpChunck = (LPBYTE)VirtualAlloc (NULL, nSize,
							  MEM_COMMIT|MEM_RESERVE,
							  PAGE_READWRITE);
	}
	~CHeapPtrA()
	{
		VirtualFree (m_lpChunck, 0, MEM_RELEASE);
		m_lpChunck = NULL;
	}
	operator LPBYTE()
	{
		return m_lpChunck;
	}
};

bool CRippeStrategy::ReadTracks(long nTrackNo, const HANDLE hWaveFile, 
								const long lSecondsToGrab, long &nTrackSizeInBytes)
{

	CAbstractRippeStrategy::ReadTracks(nTrackNo, hWaveFile, lSecondsToGrab, nTrackSizeInBytes);

	long nTrackSectorCount	= ( lSecondsToGrab ? lSecondsToGrab * TRACKSPERSEC : 
		GetTrackSectorCount(nTrackNo));
	nTrackSizeInBytes	= 0;

	CHeapPtrA lpChunck = GetReadSize();
	if (lpChunck == NULL)
	{
//		Error(_T("Error during allocating memory buffer to ripp tracks (CRippeStrategy)"));
		return false;
	}

	bool bRes = true;
	for (long i = 0; i < nTrackSectorCount/SECTORS_READ_AT_ONCE && !m_bStop && bRes; i++)
	{
		DWORD	dwReturned = ReadChunck(lpChunck, GetStartOffset(nTrackNo, i));
		if (dwReturned > 0)
		{
			DWORD	dwNotUsed = 0;
			if(WriteFile(hWaveFile, lpChunck, dwReturned, &dwNotUsed, NULL))
			{
				nTrackSizeInBytes += dwReturned;
			}
			else
			{

				bRes = false;
			}
		}
		else
		{
			TCHAR Buf[MAX_PATH] = {0};
			_stprintf(Buf, _T("Cannot read CD with DeviceIoControl (CRippeStrategy TrackNo = %d, BatchNo - %d, SectorNo - %d)"), 
				nTrackNo, i, GetStartOffset(nTrackNo, i));
			//Error(Buf);
			bRes = false;
		}
	}
	return bRes;
}

void CRippeStrategy::CloseDeviceHandle()
{
	CAbstractRippeStrategy::CloseDeviceHandle();
	if (IsDeviceOpened())
		CloseHandle(m_hCD);
	m_hCD = NULL;
}

CComBSTR CRippeStrategy::GetCDROMPath(long szDiskIdx)
{

	long nResult = -1;	
	for (TCHAR cLoop = 'A' ; cLoop <= 'Z' ; ++cLoop) 
	{
		TCHAR szPath[] = "A:\\";
		szPath[0] = cLoop;
		if (GetDriveType(szPath) == DRIVE_CDROM) 
		{
			if ((++nResult) == szDiskIdx)
			{
				TCHAR szPath_[MAX_PATH] = "\\\\.\\A:";
				szPath_[4] = cLoop;
				return szPath_;
			}
		}
	}
	return "";
}
CComBSTR CRippeStrategy::GetCDROMPathByCharCode(long szDiskIdx)
{

	long nResult = -1;	
	for (TCHAR cLoop = 'A' ; cLoop <= 'Z' ; ++cLoop) 
	{
		TCHAR szPath[] = "A:\\";
		szPath[0] = cLoop;
		if (GetDriveType(szPath) == DRIVE_CDROM) 
		{
			if ((long)(cLoop) == szDiskIdx)
			{
				TCHAR szPath_[MAX_PATH] = "\\\\.\\A:";
				szPath_[4] = cLoop;
				return szPath_;
			}
		}
	}
	return "";
}

bool CRippeStrategy::IsDeviceOpened()
{
	return m_hCD != NULL && m_hCD != INVALID_HANDLE_VALUE;
}

BOOL CRippeStrategy::InitDevice()
{
	
	return InitDevice(m_nDiskIdx);
}

BOOL CRippeStrategy::InitDevice(const long nDiskIdx)
{

	CAbstractRippeStrategy::InitDevice(nDiskIdx);
	USES_CONVERSION;
	
	CComBSTR cbstDeviceName = GetCDROMPathByCharCode(nDiskIdx);
	// if Win2K or greater, add GENERIC_WRITE
	DWORD dwFlags = GENERIC_READ;

	DWORD B = GetLastError();

	CloseDeviceHandle();
	m_hCD = CreateFileW ((cbstDeviceName), dwFlags,
		FILE_SHARE_READ,
		NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,
		NULL);

	DWORD A = GetLastError();
	
	if (!IsDeviceOpened())
	{
		// it went foobar somewhere, so try it with the GENERIC_WRITE bit flipped
		dwFlags ^= GENERIC_WRITE;
		m_hCD = CreateFileW ((cbstDeviceName), dwFlags,
			FILE_SHARE_READ,
			NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,
			NULL);
	}
	
	if (!IsDeviceOpened())
	{
		return FALSE;
	}
	
	
	return TRUE;
}


BOOL CRippeStrategy::GetTOC()
{

	memset(&m_tocData, 0, sizeof(CDROM_TOC));

	DWORD dwReturned = 0;
	
	BOOL bRes = DeviceIoControl (m_hCD, IOCTL_CDROM_READ_TOC,
								   NULL, 0, &m_tocData,
								   sizeof(CDROM_TOC), &dwReturned, NULL);
	//if (!bRes) Error("CRippeStrategy cannot get a toc");
	return bRes;
}


STDMETHODIMP CRippeStrategy::GetAvailableCDDrives(long *plCount, VARIANT *pNames)
{

	long lCount = 0;
	wchar_t wLoop;
	wchar_t awDrives[26];
		
	for (wLoop = 'A' ; wLoop <= 'Z' ; wLoop ++) 
	{
		CComBSTR cbstRoot(1, &wLoop);
		cbstRoot += L":\\";
		
		if (GetDriveTypeW(cbstRoot) == DRIVE_CDROM) 
		{
			awDrives[lCount] = wLoop;
			lCount++;
		}
	}
	
	VariantInit(pNames);
	pNames->vt = VT_ARRAY | VT_BSTR;

	SAFEARRAY *pSA;
	SAFEARRAYBOUND bounds = {lCount, 0};
	
	// Create the array.
	pSA = SafeArrayCreate(VT_BSTR, 1, &bounds);

	// Fill the array.
	BSTR *theStrings;
	SafeArrayAccessData(pSA, (void**)&theStrings);
	for (int i = 0; i < lCount; i++)
	{
		CComBSTR cbstRoot(1, &awDrives[i]);
		theStrings[i] = SysAllocString(cbstRoot);
	}
	SafeArrayUnaccessData(pSA);

	// Set return value.
	pNames->parray = pSA;
	*plCount = lCount;

	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////
// CCoCDRipper

STDMETHODIMP CCoCDRipper::get_ActiveDevice(long *pVal)
{
	*pVal = m_nDiskIdx;
	return S_OK;
}

void CCoCDRipper::SwitchRippeStrategy()
{
	eRippeStrategy CurrentStgy = m_pRippeStrategy->GetType();
	delete m_pRippeStrategy.release();
	m_pRippeStrategy = std::auto_ptr<CAbstractRippeStrategy>(NULL);
	if (CurrentStgy == rsIOCTL_SCSI)
		GetRippeStrategy(rsIOCTL_CDROM);
	else
		GetRippeStrategy(rsIOCTL_SCSI);
}

HRESULT CCoCDRipper::InitDevice(const long nDiskIdx)
{

	if (!GetRippeStrategy()->InitDevice(m_nDiskIdx))
	{
		
		return E_FAIL;
	}
	return S_OK;
}

STDMETHODIMP CCoCDRipper::put_ActiveDevice(long newVal)
{
	m_nDiskIdx = newVal;

	// try to connect to CD-ROM and init m_hCD
	HRESULT hRes = InitDevice(m_nDiskIdx);
	if (FAILED(hRes))
	{
		SwitchRippeStrategy();
		hRes = InitDevice(m_nDiskIdx);
		if (FAILED(hRes))
			return hRes;
	}

	if (!GetTOC()) 
	{
		SwitchRippeStrategy();
		HRESULT hRes = InitDevice(m_nDiskIdx);
		if (FAILED(hRes))
			return hRes;

		if (!GetTOC()) 
		{
//			Error("Cant Get TOC");
			GetRippeStrategy()->CloseDeviceHandle();
			CloseFileHandle();
			return E_FAIL;
		}
	}

	return S_OK;
}

STDMETHODIMP CCoCDRipper::get_OutputFile(BSTR *pVal)
{
	*pVal = m_cbstOutputFile.Copy();
	return S_OK;
}

STDMETHODIMP CCoCDRipper::put_OutputFile(BSTR newVal)
{
	m_cbstOutputFile = newVal;
	return S_OK;
}

STDMETHODIMP CCoCDRipper::GrabTrack(long lTrackNr, long lSecondsToGrab, VARIANT_BOOL bSync)
{


	if (m_bThread) // �.�. ����� ������ � �����, ����� ������� �������, ��� ������ �� �����
	{
//		Error("Using now");
		return E_FAIL;
	}

	if (!IsTOCReaded())
	{
//		Error("Disk name not set");
		return E_FAIL;
	}

	if (!GetRippeStrategy()->IsTrackInRange(lTrackNr))
	{
//		Error("Track No out of range");
		return E_FAIL;
	}

	if (!InitOutputFile())
	{
		CComBSTR cbstError;
		cbstError="Cant init out file";
		cbstError += "'";
		cbstError += m_cbstOutputFile;
		cbstError += "'";
		//Error(cbstError);
		return E_FAIL;
	}

	if (!bSync)
	{
		DWORD dwThreadId;
		m_bThread = TRUE;
		ThreadData* a_data = new ThreadData(this, lTrackNr, lSecondsToGrab);
		CreateThread(NULL, 1024, ThreadProc___, (LPVOID)a_data, 0, &dwThreadId);
	}
	else
	{
		if (!TrackToFile(lTrackNr, lSecondsToGrab))
		{
//			Error("Problem was occured");
			GetRippeStrategy()->CloseDeviceHandle();
			return E_FAIL;
		}
	}

	return S_OK;
}

BOOL CCoCDRipper::InitDevice()
{
	return GetRippeStrategy()->InitDevice(m_nDiskIdx);
}
BOOL CCoCDRipper::CloseDeviceHandle() {
    try{
        GetRippeStrategy()->CloseDeviceHandle();
        return TRUE;
    }catch (...){
        return FALSE;
    }

}

BOOL CCoCDRipper::InitOutputFile()
{

	USES_CONVERSION;
	//  Disk file that will hold the CD-ROM track data.
	CloseFileHandle();
	m_hWaveFile = CreateFile((W2T(m_cbstOutputFile)),
				   GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
				   FILE_ATTRIBUTE_NORMAL, NULL);

	if (!IsOutputFileOpened())
		return FALSE;
    
	return TRUE;
}

BOOL CCoCDRipper::GetTOC()
{
	if (!IsTOCReaded())
		GetRippeStrategy()->GetTOC();
	return IsTOCReaded();
}

void CCoCDRipper::TrackGrabbingComplete()
{
	// ��������� ����������� ������� ����� Connection Point

}

STDMETHODIMP CCoCDRipper::StopGrabbing()
{
	GetRippeStrategy()->Stop();
	return S_OK;
}

BOOL CCoCDRipper::TrackToFile(long lTrackNr, long lSecondsToGrab)
{

	BOOL	bRes = TRUE;
	WriteWaveHdr(0);

	long nTrackSizeInBytes;
	bRes = GetRippeStrategy()->ReadTracks(lTrackNr, m_hWaveFile, lSecondsToGrab, nTrackSizeInBytes);
	WriteWaveHdr(nTrackSizeInBytes);
	CloseFileHandle();
	return bRes;
}

long CCoCDRipper::GetTrackSectorCount(long lTrackNr)
{
	if (!GetRippeStrategy()->IsTrackInRange(lTrackNr))
		return -1;
	return GetRippeStrategy()->GetTrackSectorCount(lTrackNr);
}

/*
long CCoCDRipper::GetTrackOffset(long lTrackNr)
{
	if (!GetRippeStrategy()->IsTrackInRange(lTrackNr))
		return -1;
	return GetRippeStrategy()->GetTrackOffset(lTrackNr - 1);
}
*/

BOOL CCoCDRipper::WriteWaveHdr(long lTrackSize)
{

	TitleWave titleWave = {{'R','I', 'F', 'F'}, 4, {'W','A','V','E'}, {'f','m','t', ' '}, 
	0x10, 1, 2, 0x0000AC44, 0x2B110, 4, 0x10, {'d','a','t','a'}, 4};

	titleWave.len_riff = lTrackSize + sizeof(titleWave);
	titleWave.len_data = lTrackSize ;
	 	
	DWORD dw = 0;

	SetFilePointer(m_hWaveFile, 0, NULL, FILE_BEGIN);
	
	if (!WriteFile(m_hWaveFile, &titleWave, sizeof(titleWave), &dw, NULL))
		return FALSE;
		
	if (dw != 44)
		return FALSE;
	
	return TRUE;
}

STDMETHODIMP CCoCDRipper::GetTracksCount(long *plTracks)
{

	if (!IsTOCReaded())
	{
		//Error("Disc name isn't set");
		return E_FAIL;
	}
	*plTracks = GetRippeStrategy()->GetTracksCount();
	return S_OK;
}

STDMETHODIMP CCoCDRipper::IsTrackAudio(long lTrackNr, VARIANT_BOOL *pbAudio)
{

	if (!IsTOCReaded())
	{
		//Error("Disc name isn't set");
		return E_FAIL;
	}

	if (!GetRippeStrategy()->IsTrackInRange(lTrackNr))
	{
//		Error("Track No out of range");
		return E_FAIL;
	}

	*pbAudio = VARIANT_FALSE;
	if (GetRippeStrategy()->IsTrackAudio(lTrackNr))
		*pbAudio = VARIANT_TRUE;
	
	return S_OK;
}

STDMETHODIMP CCoCDRipper::GetTrackLength(long lTrackNr, long *plSec)
{
	if (!IsTOCReaded())
	{
//		Error("Disk name isn't set");
		return E_FAIL;
	}

	long lTrackFrames = GetTrackSectorCount(lTrackNr);
	if (lTrackFrames < 0)
	{
//		Error("Track No out of range");
		return E_FAIL;
	}

	*plSec = lTrackFrames / TRACKSPERSEC;
	return S_OK;
}

STDMETHODIMP CCoCDRipper::GetAvailableCDDrives(long *plCount, VARIANT *pNames)
{
	return GetRippeStrategy()->GetAvailableCDDrives(plCount, pNames);
}

std::auto_ptr<CAbstractRippeStrategy> & CCoCDRipper::GetRippeStrategy(eRippeStrategy RippeStrategy)
{
	if (!m_pRippeStrategy.get())
	{
		if (RippeStrategy == rsIOCTL_SCSI || IsRunWin9X()) 
			m_pRippeStrategy = std::auto_ptr<CAbstractRippeStrategy>(new CmfRippeStrategy());
		else
			m_pRippeStrategy = std::auto_ptr<CAbstractRippeStrategy>(new CRippeStrategy());
	}
	return m_pRippeStrategy;
}

bool CCoCDRipper::IsRunWin9X()
{
	OSVERSIONINFO osvi = {0};
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx(&osvi);
	return !((osvi.dwPlatformId == VER_PLATFORM_WIN32_NT) 
		&& (osvi.dwMajorVersion > 4));
}

void CCoCDRipper::CloseFileHandle()
{
	if (IsOutputFileOpened())
		CloseHandle(m_hWaveFile);
	m_hWaveFile = NULL;
}