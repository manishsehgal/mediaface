#pragma once
#include <math.h>

//const float M_PI = 3.1415926f;
const float M_RAD_2_DEG = 57.295779f;
const float M_DEG_2_RAD = 1.0f / M_RAD_2_DEG;

namespace Gdiplus
{
	//typedef Point Vector;
	typedef PointF VectorF;
	//int GetEncoderClsid(const WCHAR* format, CLSID* pClsid);
	//HRESULT SaveMatrix(IStream* pStream, const Matrix* pmatX);
	//HRESULT LoadMatrix(IStream* pStream, Matrix* pmatX);
	/*
	class Matrix2 : public Matrix
	{
	public:
		Matrix2()
		{	}
		Matrix2(const Matrix& rhs)
		{
			float data[6];
			rhs.GetElements(data);
			SetElements(data[0], data[1], data[2], data[3], data[4], data[5]);
		}
		const Matrix2& operator=(const Matrix& rhs)
		{
			float data[6];
			rhs.GetElements(data);
			SetElements(data[0], data[1], data[2], data[3], data[4], data[5]);
			return *this;
		}
		Matrix2(float m11, float m12, float m21, float m22, float dx, float dy)
			: Matrix(m11, m12, m21, m22, dx, dy)
		{	}
		Matrix2& Invert()
		{
			Matrix::Invert();
			return *this;
		}
	};
	*/
};

inline const Gdiplus::PointF& operator*=(Gdiplus::PointF& arg1, float arg2)
{
	arg1.X *= arg2, arg1.Y *= arg2;
	return arg1;
}

/*
inline const Gdiplus::PointF& operator/=(Gdiplus::PointF& arg1, float arg2)
{
	arg1.X /= arg2, arg1.Y /= arg2;
	return arg1;
}
*/
/*
inline const Gdiplus::PointF operator-(const Gdiplus::PointF& arg)
{	
	return Gdiplus::PointF(-arg.X, -arg.Y);	
}
*/

inline Gdiplus::PointF operator*(Gdiplus::PointF arg1, float arg2)
{
	return arg1 *= arg2;	
}

inline Gdiplus::PointF operator*(float arg1, Gdiplus::PointF arg2)
{
	return arg2 *= arg1;	
}

inline float dist(const Gdiplus::PointF & arg1, const Gdiplus::PointF & arg2)
{
	return sqrt((arg1.X - arg2.X) * (arg1.X - arg2.X) + (arg1.Y - arg2.Y) * (arg1.Y - arg2.Y));	
}

//inline float scalar_mul(const Gdiplus::VectorF& arg1, const Gdiplus::VectorF& arg2)
//	{	return arg1.X * arg2.X + arg1.Y * arg2.Y;	}
//inline float vector_mul_z(const Gdiplus::VectorF& arg1, const Gdiplus::VectorF& arg2)
//	{	return arg1.X * arg2.Y - arg1.Y * arg2.X;}


//inline float fsign(float fArg)
//	{	return fArg > 0 ? 1 : (fArg < 0) ? -1 : 0;	}


inline void normalize(Gdiplus::VectorF & vecN)
{
	float fLength = sqrt(vecN.X * vecN.X + vecN.Y * vecN.Y);
	if (fLength == 0) return;
	fLength = 1.0f / fLength;
	vecN *= fLength;
}


