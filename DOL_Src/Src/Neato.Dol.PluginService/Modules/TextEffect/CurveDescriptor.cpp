#include "stdafx.h"
#include "CurveDescriptor.h"
#include <algorithm>
using namespace Gdiplus;


//=============================================================

float CSegment::xMinGlobal;
float CSegment::xMaxGlobal;
float CSegment::yMinGlobal;
float CSegment::yMaxGlobal;

void CSegment::InitGlobalLimits() {
	xMinGlobal = FLT_MAX; 
	xMaxGlobal = -FLT_MAX; 
	yMinGlobal = FLT_MAX; 
	yMaxGlobal = -FLT_MAX;
}

int CSegment::parseRefPoints(LPCWSTR pwcData, int nCount)
{
	LPCWSTR pE = pwcData + wcslen(pwcData);
	LPCWSTR p0 = pwcData;
	WCHAR * wcStop = 0;
	float * pCoords = (float*)_alloca(nCount * 2 * sizeof(float));

	for (int i=0; i<nCount*2; i++)
	{
		pCoords[i] = (float)wcstod(p0, &wcStop);
		p0 = std::find(p0, pE, L':');
		if (p0 == pE) return 0;
		p0++;
	}
	for (i=0; i<nCount; i++)
	{
		PointF pnt(pCoords[i*2], pCoords[i*2+1]);
		setRefPoint(i, pnt);
		updateLimits(pnt);
	}
	return p0 - pwcData;
}

void CSegment::updateLimits(PointF pnt) {
	xMin = __min(xMin,pnt.X);
	xMax = __max(xMax,pnt.X);
	yMin = __min(yMin,pnt.Y);
	yMax = __max(yMax,pnt.Y);

	xMinGlobal = __min(xMinGlobal,pnt.X);
	xMaxGlobal = __max(xMaxGlobal,pnt.X);
	yMinGlobal = __min(yMinGlobal,pnt.Y);
	yMaxGlobal = __max(yMaxGlobal,pnt.Y);
}

//=============================================================
CBezierSegment::CBezierSegment()
{
	m_fLength = 0.0f;
}

int CBezierSegment::init(LPCWSTR pwcData)
{
	return parseRefPoints(pwcData, refCount());
}

long CBezierSegment::refCount() const
{
	return 4;
}

Gdiplus::PointF CBezierSegment::operator[](long lIndex) const
{
	return m_pptBezier[lIndex];	
}

bool CBezierSegment::setRefPoint(long lIndex, const Gdiplus::PointF& rptRef)
{
	m_pptBezier[lIndex] = rptRef;
	calc_length();
	return true;
}

float CBezierSegment::length() const
{
	return m_fLength;
}

Gdiplus::PointF CBezierSegment::point(float fT) const
{
	float t = fT, t2 = t * t, t3 = t2 * t;
	const Gdiplus::PointF &a = m_pptBezier[0], &b = m_pptBezier[1], &c = m_pptBezier[2], &d = m_pptBezier[3];
	return a + 3 * t * (b - a) + 3 * t2 * (a - 2 * b + c) + t3 * (3 * b - a - 3 * c + d);
}

Gdiplus::VectorF CBezierSegment::dir(float fT) const
{
	float t = fT, t2 = t * t;
	const Gdiplus::PointF &a = m_pptBezier[0], &b = m_pptBezier[1], &c = m_pptBezier[2], &d = m_pptBezier[3];
	return 3.0f * ((b - a) + 2 * t * (a - 2 * b + c) + t2 * (3 * b - a - 3 * c + d));	// first derivative
}

Gdiplus::VectorF CBezierSegment::norm(float fT) const
{
	Gdiplus::VectorF vecDir(dir(fT));
	normalize(vecDir);
	return Gdiplus::VectorF(-vecDir.Y, vecDir.X);
}

void CBezierSegment::calc_length()
{
	const int nPts = 100;
	int i;
	Gdiplus::PointF ptOrg = m_pptBezier[0];
	float fStep = 1.0f / (nPts - 1), fOrg = fStep;
	m_fLength = 0.0f;
	for (i=1; i<nPts; i++)
	{
		Gdiplus::PointF ptDest = point(fOrg);
		fOrg += fStep;
		m_fLength += dist(ptDest, ptOrg);
		ptOrg = ptDest;
	}
}


//=============================================================
CLineSegment::CLineSegment()
{
	m_fLength     = 0.0f;
}

int CLineSegment::init(LPCWSTR pwcData)
{
	return parseRefPoints(pwcData, refCount());
}

long CLineSegment::refCount() const
{
	return 2;
}

Gdiplus::PointF CLineSegment::operator[](long lIndex) const
{
	return m_pptLine[lIndex];	
}

bool CLineSegment::setRefPoint(long lIndex, const Gdiplus::PointF& rptRef)
{
	m_pptLine[lIndex] = rptRef;
	calc_length();
	return true;
}

float CLineSegment::length() const
{
	return m_fLength;
}

Gdiplus::PointF CLineSegment::point(float fT) const
{
	Gdiplus::VectorF vecDir(m_pptLine[1].X - m_pptLine[0].X, m_pptLine[1].Y - m_pptLine[0].Y);
	vecDir *= fT;
	return vecDir + m_pptLine[0];
}

Gdiplus::VectorF CLineSegment::dir(float fT) const
{
	return m_pptLine[1] - m_pptLine[0];
}

Gdiplus::VectorF CLineSegment::norm(float fT) const
{
	Gdiplus::VectorF vecNorm(m_pptLine[0].Y - m_pptLine[1].Y, m_pptLine[1].X - m_pptLine[0].X);
	normalize(vecNorm);
	return vecNorm;
}

void CLineSegment::calc_length()
{
	m_fLength = sqrt((m_pptLine[1].X - m_pptLine[0].X) * (m_pptLine[1].X - m_pptLine[0].X) +
				(m_pptLine[1].Y - m_pptLine[0].Y) * (m_pptLine[1].Y - m_pptLine[0].Y));
}


//=============================================================
CArcSegment::CArcSegment()
{
	m_fLength     = 0.0f;
	m_fRadius     = 0.0f;
	m_fStartAngle = 0.0f;
	m_fSweepAngle = 0.0f;
}

int CArcSegment::init(LPCWSTR pwcData)
{
	return parseRefPoints(pwcData, refCount());
}

long CArcSegment::refCount() const
{
	return 3;
}

Gdiplus::PointF CArcSegment::operator[](long lIndex) const
{
	return m_pptArc[lIndex];	
}

bool CArcSegment::setRefPoint(long lIndex, const Gdiplus::PointF& rptRef)
{
	m_pptArc[lIndex] = rptRef;
	if(lIndex == 2)
		calc_arc_params();
	return true;
}

float CArcSegment::length() const
{
	return m_fLength;
}

Gdiplus::PointF CArcSegment::point(float fT) const
{
	float fAlpha = M_DEG_2_RAD * (m_fStartAngle + fT * m_fSweepAngle);
	return PointF(cosf(fAlpha), sinf(fAlpha)) * m_fRadius + m_ptCenter;
}

Gdiplus::VectorF CArcSegment::dir(float fT) const
{
	float fAlpha = M_DEG_2_RAD * (m_fStartAngle + fT * m_fSweepAngle);
	return Gdiplus::VectorF(-sinf(fAlpha), cosf(fAlpha)) * m_fSweepAngle * M_DEG_2_RAD * m_fRadius;
}

Gdiplus::VectorF CArcSegment::norm(float fT) const
{
	Gdiplus::VectorF vec(dir(fT));
	normalize(vec);
	return Gdiplus::VectorF(-vec.Y, vec.X);
}

void CArcSegment::calc_arc_params()
{
	float x0 = m_pptArc[0].X, x1 = m_pptArc[1].X, x2 = m_pptArc[2].X, X;
	float y0 = m_pptArc[0].Y, y1 = m_pptArc[1].Y, y2 = m_pptArc[2].Y, Y;
	if (x1 == x0)
	{
		// renumerating
		x1 = m_pptArc[0].X, x2 = m_pptArc[1].X, x0 = m_pptArc[2].X, X;
		y1 = m_pptArc[0].Y, y2 = m_pptArc[1].Y, y0 = m_pptArc[2].Y, Y;
	}
	Y = 2 * ((y1 - y2) * (x1 - x0) - (y0 - y1) * (x2 - x1));
	if (Y == 0)
	{
		m_fRadius = 0;
		return;
	}
	Y = ((y2 + y1) * (y1 - y2) * (x1 - x0) - (y1 + y0) * (y0 - y1) * (x2 - x1) + (x0 - x2) * (x1 - x0) * (x2 - x1)) / Y;
	X = (Y - 0.5f * (y1 + y0)) * (y0 - y1) / (x1 - x0) + 0.5f * (x1 + x0);
	m_ptCenter.X = X, m_ptCenter.Y = Y;
	m_fRadius = dist(m_ptCenter, m_pptArc[0]);

	VectorF vec0(m_pptArc[0] - m_ptCenter), vec1(m_pptArc[1] - m_ptCenter), vec2(m_pptArc[2] - m_ptCenter);
	m_fStartAngle = M_RAD_2_DEG * atan2(vec0.Y, vec0.X);
	m_fSweepAngle = M_RAD_2_DEG * atan2(vec2.Y, vec2.X);
	float fMidAngle = M_RAD_2_DEG * atan2(vec1.Y, vec1.X);
	if (m_fStartAngle < 0)
		m_fStartAngle += 360.0;
	if (m_fSweepAngle < 0)
		m_fSweepAngle += 360.0;
	if (fMidAngle < 0)
		fMidAngle += 360.0;

	float fAngle1 = __min(m_fStartAngle,m_fSweepAngle);
	float fAngle2 = __max(m_fStartAngle,m_fSweepAngle);
	
	m_fSweepAngle -= m_fStartAngle;
	fMidAngle -= m_fStartAngle;

	if (!(m_fSweepAngle > 0 && 0 <= fMidAngle && fMidAngle <= m_fSweepAngle) && !(m_fSweepAngle <= 0 && m_fSweepAngle <= fMidAngle && fMidAngle <= 0))
	{
		if (m_fSweepAngle > 0)
			m_fSweepAngle -= 360;
		else
			m_fSweepAngle += 360;
	}

	m_fLength = fabsf(M_DEG_2_RAD * m_fSweepAngle * m_fRadius);

	if((fAngle1 < 90) && (fAngle2 > 90))
		updateLimits(m_ptCenter+m_fRadius*PointF(0,1));
	if((fAngle1 < 180) && (fAngle2 > 180))
		updateLimits(m_ptCenter+m_fRadius*PointF(-1,0));
	if((fAngle1 < 270) && (fAngle2 > 270))
		updateLimits(m_ptCenter+m_fRadius*PointF(0,-1));
}


//=============================================================
CCurveDescriptor::CCurveDescriptor()
{
	m_bClosed = false;
}

CCurveDescriptor::~CCurveDescriptor()
{
	RemoveAllSegments();
}

CSegment * CCurveDescriptor::AddSegment(SegmentType type)
{
	CSegment * pNewSeg = 0;
	switch (type)
	{
	case cstBezier:
		{
			pNewSeg = new CBezierSegment();
		}
		break;
	case cstLine:
		{
			pNewSeg = new CLineSegment();
		}
		break;
	case cstArc:
		{
			pNewSeg = new CArcSegment();
		}
		break;
	default:
		{
			return 0;
		}
		break;
	}
	add(pNewSeg);
	return pNewSeg;
}

CSegment * CCurveDescriptor::AddSegment(WCHAR wcCode)
{
	SegmentType type = cstNone;
	if (wcCode == L'A')
	{
		type = cstArc;
	}
	else if (wcCode == L'L')
	{
		type = cstLine;
	}
	else if (wcCode == L'B')
	{
		type = cstBezier;
	}
	return AddSegment(type);
}

void CCurveDescriptor::add(CSegment* pSegment)
{
	if (size() > 0)
	{
		pSegment->setRefPoint(0, getRefPoint(refCount() - 1));
	}
	push_back(pSegment);
	m_bClosed = false;
}

void CCurveDescriptor::RemoveAllSegments()
{
	iterator it;
	m_bClosed = false;
	for (it=begin(); it!=end(); it++)
	{
		delete(*it);
	}
	clear();
}

long CCurveDescriptor::refCount() const
{
	long nCnt = 0;
	const_iterator it;
	for (nCnt = 0, it=begin(); it!=end(); it++)
	{
		nCnt += (*it)->refCount() - 1;
	}
	if (!m_bClosed && size() > 0)
	{
		nCnt++;
	}
	return nCnt;
}

Gdiplus::PointF CCurveDescriptor::getRefPoint(long lIndex) const
{
	const CSegment * pSeg = 0;
	const_iterator it;

	for (it=begin(); it!=end(); lIndex -= pSeg->refCount() - 1, it++)
	{
		pSeg = *it;
		if (lIndex < pSeg->refCount() - 1)
			break;
	}
	if (it != end())
	{
		return (*pSeg)[lIndex];
	}
	else if (!m_bClosed && lIndex == 0)
	{
		return (*pSeg)[pSeg->refCount() - 1];
	}
	throw -1;
}

bool CCurveDescriptor::setRefPoint(long lIndex, const Gdiplus::PointF& rptRef)
{
	CSegment * pSeg = back();
	iterator it;
	for (it=begin(); it!=end() && lIndex >= 0; lIndex -= pSeg->refCount() - 1, it++)
	{
		pSeg = *it;
		if (lIndex < pSeg->refCount())
		{
			pSeg->setRefPoint(lIndex, rptRef);
			return true;
		}
	}
	return false;
}

float CCurveDescriptor::length() const
{
	float fLength = 0.0f;
	const_iterator it;
	for (it=begin(); it!=end(); it++)
	{
		fLength += (*it)->length();
	}
	return fLength;
}

Gdiplus::PointF CCurveDescriptor::point(float fT) const
{
	float fLength = 0.0f;
	const_iterator it;

	if (size() == 0)
		throw -1;

	for (it=begin(); it!=end(); fT -= (*it)->length(), it++)
	{
		if (fT > (*it)->length())
			continue;
		return (*it)->point(fT / (*it)->length());
	}
	return back()->point(1);
}

Gdiplus::VectorF CCurveDescriptor::norm(float fT) const
{
	float fLength = 0.0f;
	const_iterator it;

	if (size() == 0)
		throw -1;

	for (it=begin(); it!=end(); fT -= (*it)->length(), it++)
	{
		if (fT > (*it)->length())
			continue;
		return (*it)->norm(fT / (*it)->length());
	}
	return back()->norm(1);
}
