#pragma once

#include "EffectBase.h"

class CEffectCircle : public CEffectBase
{
protected:
	CCurveDescriptor m_ContourGuide;
    float m_fWidth;
public:
    CEffectCircle() : m_fWidth(0)
    {}
	~CEffectCircle(){}

	virtual bool Init(std::wstring strData);
	virtual void ApplyEffect(CTextRenderer & tr, Gdiplus::PathData & dataOut, int m_nAlign);
};
