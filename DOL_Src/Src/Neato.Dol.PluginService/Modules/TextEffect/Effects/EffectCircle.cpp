#include "stdafx.h"
#include "EffectCircle.h"
#include "..\GeomUtils.h"
#include <algorithm>
using namespace Gdiplus;


bool CEffectCircle::Init(std::wstring strData)
{
    LPCWSTR pE = strData.c_str() + strData.size();
	LPCWSTR p0 = strData.c_str();
	WCHAR * wcStop = 0;
	float pCoords[4];

	for (int i=0; i<4; i++)
	{
		pCoords[i] = (float)wcstod(p0, &wcStop);
		p0 = std::find(p0, pE, L':');
		if (p0 == pE) return 0;
		p0++;
	}

    float fX = pCoords[0];
    float fY = pCoords[1];
    float fW = pCoords[2];
    float fH = pCoords[3];
    if(fH < 0)
        fY-=fH;
    m_fWidth = fW;
    CSegment * pSeg = 0;

	pSeg = m_ContourGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX, fY + 0.5f * fH));
	pSeg->setRefPoint(1, PointF(fX, fY + 0.225f * fH));
	pSeg->setRefPoint(2, PointF(fX + 0.225f * fW, fY));
	pSeg->setRefPoint(3, PointF(fX + 0.5f * fW, fY));

	pSeg = m_ContourGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX + 0.5f * fW, fY));
	pSeg->setRefPoint(1, PointF(fX + 0.775f * fW, fY));
	pSeg->setRefPoint(2, PointF(fX + fW, fY + 0.225f * fH));
	pSeg->setRefPoint(3, PointF(fX + fW, fY + 0.5f * fH));

	pSeg = m_ContourGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX + fW, fY + 0.5f * fH));
	pSeg->setRefPoint(1, PointF(fX + fW, fY + 0.775f * fH));
	pSeg->setRefPoint(2, PointF(fX + 0.775f * fW, fY + fH));
	pSeg->setRefPoint(3, PointF(fX + 0.5f * fW, fY + fH));

	pSeg = m_ContourGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX + 0.5f * fW, fY + fH));
	pSeg->setRefPoint(1, PointF(fX + 0.225f * fW, fY + fH));
	pSeg->setRefPoint(2, PointF(fX, fY + 0.775f * fH));
	pSeg->setRefPoint(3, PointF(fX, fY + 0.5f * fH));

	return true;
}

void CEffectCircle::ApplyEffect(CTextRenderer & tr, Gdiplus::PathData & dataOut,  int m_nAlign)
{
	GraphicsPath pathOut;
	GraphicsPath path;
    tr.m_StringFormat.SetAlignment( m_nAlign == 0 ? StringAlignmentNear : (m_nAlign == 1? StringAlignmentCenter : StringAlignmentFar));
	tr.DrawTextLineToPath(m_ContourGuide.length(), path);

	RectF rcBox;
	Matrix mat;
	path.GetBounds(&rcBox);
	mat.Translate(m_nAlign*m_ContourGuide.length()/4, -rcBox.Y - 0.5f * rcBox.Height);
	path.Transform(&mat);

	path.GetPathData(&dataOut);

	for (long i=0; i<dataOut.Count; i++)
	{
		Gdiplus::VectorF & pt = dataOut.Points[i];
		pt = m_ContourGuide.norm(pt.X) * pt.Y + m_ContourGuide.point(pt.X);
	}
}
