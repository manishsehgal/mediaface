#pragma once

#include "EffectBase.h"

class CEffectRounding : public CEffectBase
{
protected:
    Gdiplus::PointF m_pt0;
    Gdiplus::PointF m_pt1;
    Gdiplus::PointF m_Frame[6];
    std::wstring    m_strGeometry;
    bool m_bInverse;

public:
    CEffectRounding(bool bInverse);
    ~CEffectRounding(){}

public:
    virtual bool Init(std::wstring strData);
    virtual void ApplyEffect(CTextRenderer & tr, Gdiplus::PathData & dataOut, int m_nAlign);
    virtual void GetGeometry(std::wstring & strGeometry);

protected:
    static Gdiplus::PointF Polar2Cart(float ro, float fi);
};
