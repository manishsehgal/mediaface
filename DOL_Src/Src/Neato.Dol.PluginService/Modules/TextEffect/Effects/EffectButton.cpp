#include "stdafx.h"
#include "EffectButton.h"
#include "..\GeomUtils.h"
#include <algorithm>
using namespace Gdiplus;


CEffectButton::CEffectButton()
: m_fWidth(0)
{
}

bool CEffectButton::Init(std::wstring strData)
{
    LPCWSTR pE = strData.c_str() + strData.size();
	LPCWSTR p0 = strData.c_str();
	WCHAR * wcStop = 0;
	float pCoords[4];

	for (int i=0; i<4; i++)
	{
		pCoords[i] = (float)wcstod(p0, &wcStop);
		p0 = std::find(p0, pE, L':');
		if (p0 == pE) return 0;
		p0++;
	}

    float fX = pCoords[0];
    float fY = pCoords[1];
    float fW = pCoords[2];
    float fH = pCoords[3];
    m_fWidth = fW;
    CSegment * pSeg = 0;

	pSeg = m_TopGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX, fY + 0.5f * fH));
	pSeg->setRefPoint(1, PointF(fX, fY + 0.225f * fH));
	pSeg->setRefPoint(2, PointF(fX + 0.225f * fW, fY));
	pSeg->setRefPoint(3, PointF(fX + 0.5f * fW, fY));
	pSeg = m_TopGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX + 0.5f * fW, fY));
	pSeg->setRefPoint(1, PointF(fX + 0.775f * fW, fY));
	pSeg->setRefPoint(2, PointF(fX + fW, fY + 0.225f * fH));
	pSeg->setRefPoint(3, PointF(fX + fW, fY + 0.5f * fH));

	pSeg = m_CenterGuide.AddSegment(cstLine);
	pSeg->setRefPoint(0, PointF(fX, fY + 0.5f * fH));
	pSeg->setRefPoint(1, PointF(fX + fW, fY + 0.5f * fH));

	pSeg = m_BottomGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX, fY + 0.5f * fH));
	pSeg->setRefPoint(1, PointF(fX, fY + 0.775f * fH));
	pSeg->setRefPoint(2, PointF(fX + 0.225f * fW, fY + fH));
	pSeg->setRefPoint(3, PointF(fX + 0.5f * fW, fY + fH));
	pSeg = m_BottomGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX + 0.5f * fW, fY + fH));
	pSeg->setRefPoint(1, PointF(fX + 0.775f * fW, fY + fH));
	pSeg->setRefPoint(2, PointF(fX + fW, fY + 0.775f * fH));
	pSeg->setRefPoint(3, PointF(fX + fW, fY + 0.5f * fH));

	return true;
}

void CEffectButton::ApplyEffect(CTextRenderer & tr, Gdiplus::PathData & dataOut, int m_nAlign)
{
    
	GraphicsPath pathOut;
	GraphicsPath path;
    if(tr.m_vString.empty())
        return;
    tr.m_StringFormat.SetAlignment( m_nAlign == 0 ? StringAlignmentNear : (m_nAlign == 1? StringAlignmentCenter : StringAlignmentFar));
    tr.m_bParaMode = true;
    tr.m_ivString = tr.m_vString.begin();
	tr.DrawTextLineToPath(m_TopGuide.length(), path);
    GuidePath(path, m_TopGuide, m_nAlign);
    pathOut.AddPath(&path, FALSE);
    path.Reset();
    tr.m_ivString++;
    if(tr.m_ivString != tr.m_vString.end()) {
        tr.m_nNextChar = 0;
	    tr.DrawTextLineToPath(m_CenterGuide.length(), path);
        GuidePath(path, m_CenterGuide, m_nAlign);
        pathOut.AddPath(&path, FALSE);
        path.Reset();
        tr.m_ivString++;
    }
    if(tr.m_ivString != tr.m_vString.end()) {
        tr.m_nNextChar = 0;
	    tr.DrawTextLineToPath(m_BottomGuide.length(), path);
        GuidePath(path, m_BottomGuide, m_nAlign);
        pathOut.AddPath(&path, FALSE);
        path.Reset();
    }
      tr.m_bParaMode = false;
   /* tr.m_bParaMode = true;
    if(tr.m_vString.empty())
        return;
    tr.m_ivString = tr.m_vString.begin();
    int iRes = 1;

    iRes = tr.DrawTextLineToPath(m_TopGuide.length(), path);
    if (iRes == 0)
    {
        tr.m_bParaMode = false;
        return;
    }
    
    GuidePath(path, m_TopGuide, m_nAlign);
    pathOut.AddPath(&path, FALSE);
    if (iRes == -1 && tr.m_ivString != tr.m_vString.end()){
        tr.m_ivString++;
    }else {
        
        tr.m_bParaMode = false;
        return;
    }
    path.Reset();

    tr.DrawTextLineToPath( m_CenterGuide.length(),path);
    if (iRes == 0)
    {
        
        tr.m_bParaMode = false;
        return;
    }
    
    GuidePath(path, m_CenterGuide, m_nAlign);
    pathOut.AddPath(&path, FALSE);
    if (iRes == -1 && tr.m_ivString != tr.m_vString.end()){
        tr.m_ivString++;
    }else {
        
        tr.m_bParaMode = false;
        return;
    }
    path.Reset();

    tr.DrawTextLineToPath(m_BottomGuide.length(), path);
    if (iRes == 0)
    {
        
        tr.m_bParaMode = false;
        return;
    }
    
    GuidePath(path, m_BottomGuide, m_nAlign);
    pathOut.AddPath(&path, FALSE);
    //if (iRes == -1)
      //  pContainer->Renderer()->BeginParaPath(false);
    path.Reset();
    tr.m_bParaMode = false;
*/
   	pathOut.GetPathData(&dataOut);

}

void CEffectButton::GuidePath(GraphicsPath & path, CCurveDescriptor & guide, int m_nAlign)
{
	RectF rcBox;
	Matrix mat;
	path.GetBounds(&rcBox);
    float fWidth = guide.length();
    //normalize path
	mat.Translate(m_nAlign*fWidth/2, -rcBox.Y - 0.5f * rcBox.Height);
	path.Transform(&mat);

    //guide path
	PathData data;
	path.GetPathData(&data);
	path.Reset();
	if (guide.length() == 0) return;

	for (long i=0; i<data.Count; i++)
	{
		Gdiplus::VectorF & pt = data.Points[i];
		pt = guide.norm(pt.X) * pt.Y + guide.point(pt.X);
	}

	path.AddPath(&GraphicsPath(data.Points, data.Types, data.Count), FALSE);
}


