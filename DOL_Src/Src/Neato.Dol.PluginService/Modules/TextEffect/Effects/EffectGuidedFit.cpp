#include "stdafx.h"
#include "EffectGuidedFit.h"
#include "..\GeomUtils.h"
using namespace Gdiplus;


CEffectGuidedFit::CEffectGuidedFit()
{
}

bool CEffectGuidedFit::Init(std::wstring strData)
{
	int nPos = 0;

	CSegment * pSeg = m_Guide.AddSegment(strData[nPos]);
	if (!pSeg) return false;
	nPos += 2;
	int nCnt = pSeg->init(strData.c_str() + nPos);
	if (!nCnt) return false;
	nPos += nCnt;

	return true;
}

void CEffectGuidedFit::ApplyEffect(CTextRenderer & tr, Gdiplus::PathData & dataOut, int m_nAlign)
{
	float fWidth = m_Guide.length();
	if (fWidth <= 0.0) return;
    /*if(fWidth > 2.0f)
        fWidth -= 2.0f;//workaround bugfix*/
   GraphicsPath pathIn;
    tr.m_StringFormat.SetAlignment( m_nAlign == 0 ? StringAlignmentNear : (m_nAlign == 1? StringAlignmentCenter : StringAlignmentFar));
    tr.AddParaToPath(pathIn, -1.0f);

	RectF rcBox;
	Matrix mat;
	pathIn.GetBounds(&rcBox);
	mat.Translate(m_nAlign*fWidth/2, -rcBox.Y - 0.5f * rcBox.Height);
    mat.Scale(fWidth / (rcBox.Width/*+rcBox.X*/), 1.0f);
	pathIn.Transform(&mat);

	pathIn.GetPathData(&dataOut);

	for (long i=0; i<dataOut.Count; i++)
	{
		Gdiplus::VectorF & pt = dataOut.Points[i];
		pt = m_Guide.norm(pt.X) * pt.Y + m_Guide.point(pt.X);
	}
}

