#include "stdafx.h"
#include "EffectCurved.h"
#include "..\GeomUtils.h"
#include <math.h>
#include <stdlib.h>
using namespace Gdiplus;


CEffectCurved::CEffectCurved()
{
}

bool CEffectCurved::Init(std::wstring strData)
{
	int nPos = 0;

	CSegment * pSeg1 = m_TopGuide.AddSegment(strData[nPos]);
	if (!pSeg1) return false;
	nPos += 2;
	int nCnt = pSeg1->init(strData.c_str() + nPos);
	if (!nCnt) return false;
	nPos += nCnt;

	CSegment * pSeg2 = m_BottomGuide.AddSegment(strData[nPos]);
	if (!pSeg2) return false;
	nPos += 2;
	nCnt = pSeg2->init(strData.c_str() + nPos);
	if (!nCnt) return false;
	nPos += nCnt;

	return true;
}

void CEffectCurved::ApplyEffect(CTextRenderer & tr, Gdiplus::PathData & dataOut, int m_nAlign)
{
	GraphicsPath pathIn;
    tr.AddParaToPath(pathIn, -1.0f);

	RectF rcBox;
	Matrix mat;
	pathIn.GetBounds(&rcBox);

	//normalize path
	pathIn.Transform(&Matrix(1.0f, 0.0f, 0.0f, 1.0f, -rcBox.X, -rcBox.Y));
	//scale path
	pathIn.Transform(&Matrix(1.0f / rcBox.Width, 0.0f, 0.0f, 1.0f / rcBox.Height, 0.0f, 0.0f));

	pathIn.GetPathData(&dataOut);

	float fTopL    = m_TopGuide.length();
	float fBottomL = m_BottomGuide.length();
	for (long i=0; i<dataOut.Count; i++)
	{
		Gdiplus::VectorF & pt = dataOut.Points[i];

		PointF ptTop(m_TopGuide.point(pt.X * fTopL));
		PointF ptBottom(m_BottomGuide.point(pt.X * fBottomL));

		VectorF vecN(ptBottom - ptTop);
		pt = vecN * pt.Y + ptTop;
	}

}

