#include "stdafx.h"
#include "EffectGuided.h"
#include "..\GeomUtils.h"
using namespace Gdiplus;


CEffectGuided::CEffectGuided()
{
}

bool CEffectGuided::Init(std::wstring strData)
{
	int nPos = 0;

	CSegment * pSeg = m_Guide.AddSegment(strData[nPos]);
	if (!pSeg) return false;
	nPos += 2;
	int nCnt = pSeg->init(strData.c_str() + nPos);
	if (!nCnt) return false;
	nPos += nCnt;

	return true;
}

void CEffectGuided::ApplyEffect(CTextRenderer & tr, Gdiplus::PathData & dataOut, int m_nAlign)
{
	float fWidth = m_Guide.length();
	if (fWidth <= 0.0) return;

	GraphicsPath pathIn;
    tr.m_StringFormat.SetAlignment( m_nAlign == 0 ? StringAlignmentNear : (m_nAlign == 1? StringAlignmentCenter : StringAlignmentFar));
    tr.m_bParaMode = true;
    if(tr.m_vString.empty())
        return;
    tr.m_ivString = tr.m_vString.begin();
    tr.m_ivString --;//for algorithm simplification
    while (++tr.m_ivString != tr.m_vString.end()) {
        tr.AddParaToPath(pathIn, fWidth);
    }
    tr.m_bParaMode = false;
	RectF rcBox;
	Matrix mat;
	pathIn.GetBounds(&rcBox);
    
    
    float fX0 = 0.0f;
    switch (m_nAlign)
    {
    case 0: fX0 = 0; break;
    case 1: fX0 = .5f * m_Guide.length() - (rcBox.X + .5f * rcBox.Width); break;
    case 2: fX0 = m_Guide.length() - (rcBox.X + rcBox.Width); break;
    default: break;
    }
    

	mat.Translate(fX0, -rcBox.Y - 0.5f * rcBox.Height);
    
	pathIn.Transform(&mat);

	pathIn.GetPathData(&dataOut);

	for (long i=0; i<dataOut.Count; i++)
	{
		Gdiplus::VectorF & pt = dataOut.Points[i];
		pt = m_Guide.norm(pt.X) * pt.Y + m_Guide.point(pt.X);
	}

}

