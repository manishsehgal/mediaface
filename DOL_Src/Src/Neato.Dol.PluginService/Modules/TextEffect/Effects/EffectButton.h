#pragma once

#include "EffectBase.h"

class CEffectButton : public CEffectBase
{
protected:
	CCurveDescriptor m_TopGuide, m_CenterGuide, m_BottomGuide;

protected:
    void GuidePath(Gdiplus::GraphicsPath & path, CCurveDescriptor & guide, int m_nAlign);
	//void ConvertPoint(Gdiplus::PointF * pptToConvert, long lCount = 1);
	/*
	STDMETHODIMP get_Guide(IMFCurveDescriptor** ppGuide)
	{
		if (ppGuide == NULL)
			return E_POINTER;
		return m_Guide.QueryInterface(ppGuide);
	}
	*/
public:
	//static void GuidePath(Gdiplus::GraphicsPath* pPathToModify, const CPolyCurveDescriptor* pGuide, HTextAlign eHAlign = htaNear);

	CEffectButton();
	~CEffectButton(){}

	virtual bool Init(std::wstring strData);
	virtual void ApplyEffect(CTextRenderer & tr, Gdiplus::PathData & dataOut, int m_nAlign);
	/*
	long RefPointsCount() const
	{
		return m_Guide.refCount();	
	}
	*/
	/*
	Gdiplus::PointF RefPoint(long nIdx, DWORD* pdwType) const
	{
		if (NULL != pdwType)
			*pdwType = rptRect | rptSnapable;
		return m_Guide.getRefPoint(nIdx);
	}
	*/
	/*
	bool DragRefPointI(long nPoint, const Gdiplus::VectorF& rvecDir, bool* pbUpdateX, bool* pbUpdateY, WPARAM wParam = 0)
	{
		Gdiplus::PointF pt(m_Guide.getRefPoint(nPoint));
		pt = pt + rvecDir;
		m_Guide.setRefPoint(nPoint, pt);
		*pbUpdateY = true;
		*pbUpdateX = true;
		return true;
	}
	*/
	/*
	CPolyCurveDescriptor * Guide()
	{
		return &m_Guide;	
	}
	*/
	/*
	MFTextEffectType GetTextEffectType() const
		{	return GuidedTextEffect;	}
		*/

	//void RegenerateDataEx(ITextEffectContainer* pContainer);

protected:
    float m_fWidth;
};
