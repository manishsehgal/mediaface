#include "stdafx.h"
#include "EffectRounding.h"
#include "..\GeomUtils.h"
#include <vector>
using namespace Gdiplus;


CEffectRounding::CEffectRounding(bool bInverse)
{
    m_bInverse = bInverse;
}

bool CEffectRounding::Init(std::wstring strData)
{
    int nPos = 0;
    CCurveDescriptor cd;
    CSegment * pLine = cd.AddSegment(strData[nPos]);
    if (!pLine) return false;
    nPos += 2;
    int nCnt = pLine->init(strData.c_str() + nPos);
    if (!nCnt) return false;
    nPos += nCnt;
    if (pLine->refCount() != 2) return false;
    m_pt0 = (*pLine)[0];
    m_pt1 = (*pLine)[1];

    return true;
}

Gdiplus::PointF CEffectRounding::Polar2Cart(float ro, float fi)
{
    return PointF(ro * cos(fi), -ro * sin(fi));
}

void CEffectRounding::ApplyEffect(CTextRenderer & tr, Gdiplus::PathData & dataOut, int m_nAlign)
{
    //draw text in rectangle
    tr.m_StringFormat.SetAlignment( m_nAlign == 0 ? StringAlignmentNear : (m_nAlign == 1? StringAlignmentCenter : StringAlignmentFar));
    GraphicsPath pathIn;
    tr.AddParaToPath(pathIn, -1.0f);

    // rotate path on 180 deg for inversed effect type
    if (m_bInverse == true) {
    	pathIn.Transform(&Matrix(-1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f));
    }
    
    // get the rectangle width and height
    RectF rcBox;
    pathIn.GetBounds(&rcBox);
    float fW = rcBox.Width;
    if (fabs(fW) < 1e-6f) return;
    float fH = rcBox.Height;
    if (fabs(fH) < 1e-6f) return;

	//normalize path
	pathIn.Transform(&Matrix(1.0f, 0.0f, 0.0f, 1.0f, -rcBox.X, -rcBox.Y));
	//scale path
	pathIn.Transform(&Matrix(1.0f / fW, 0.0f, 0.0f, 1.0f / fH, 0.0f, 0.0f));

    //translate center point of the rectangle to polar coords (ro, fi)
    Gdiplus::PointF pt = m_pt1 - m_pt0; // m_pt0 is in (0,0)
    pt.Y = -pt.Y;
    float pi = 3.1415926f;
    float ro = sqrt(pt.X * pt.X + pt.Y * pt.Y); // radius
    float minro = fH / 2.0f + fH * 0.01f;
    if (ro < minro) {
        ro = minro;
    }
    float fi = 0.0f; //angle
    if (fabs(pt.X) > 1e-6f) {
        fi = atan2f(pt.Y, pt.X);
    } else {
        if (pt.Y > 0.0f) {
            fi = pi / 2.0f;
        } else {
            fi = -pi / 2.0f;
        }
    }

    // inner and outer radiuses of text shape
    float ro0 = ro - fH / 2.0f;
    float ro1 = ro0 + fH;
    //full angle of text shape
    float angle = fW / ro;
    if (angle > 1.99f*pi) angle = 1.99f*pi;
    // start and end angles of text shape
    float fi0 = fi - angle / 2.0f;
    float fi1 = fi0 + angle;

    // points of inner arc in cartesian
    m_Frame[0] = Polar2Cart(ro0, fi0) + m_pt0;
    m_Frame[1] = Polar2Cart(ro0, fi)  + m_pt0;
    m_Frame[2] = Polar2Cart(ro0, fi1) + m_pt0;

    // points of outer arc in cartesian
    m_Frame[3] = Polar2Cart(ro1, fi0) + m_pt0;
    m_Frame[4] = Polar2Cart(ro1, fi)  + m_pt0;
    m_Frame[5] = Polar2Cart(ro1, fi1) + m_pt0;

    // inner guide
	CCurveDescriptor innerGuide;
	CSegment * pArc0 = innerGuide.AddSegment(cstArc);
    pArc0->setRefPoint(0, m_Frame[0]);
    pArc0->setRefPoint(1, m_Frame[1]);
    pArc0->setRefPoint(2, m_Frame[2]);

    // outer guige
	CCurveDescriptor outerGuide;
	CSegment * pArc1 = outerGuide.AddSegment(cstArc);
    pArc1->setRefPoint(0, m_Frame[3]);
    pArc1->setRefPoint(1, m_Frame[4]);
    pArc1->setRefPoint(2, m_Frame[5]);

    // get path points
    pathIn.GetPathData(&dataOut);

    // transform path points
	float fTopL    = innerGuide.length();
	float fBottomL = outerGuide.length();
	for (long i=0; i<dataOut.Count; i++)
	{
		Gdiplus::VectorF & pt = dataOut.Points[i];

		PointF ptTop(innerGuide.point(pt.X * fTopL));
		PointF ptBottom(outerGuide.point(pt.X * fBottomL));

		VectorF vecN(ptBottom - ptTop);
		pt = vecN * pt.Y + ptTop;
	}

    WCHAR buf[256];
    swprintf(buf, L"Width=\"%.2f\" Height=\"%.2f\" Radius=\"%.2f\" Alpha=\"%.2f\" Beta=\"%.2f\"", fW, fH, ro, fi0, angle);
    m_strGeometry = buf;
}

void CEffectRounding::GetGeometry(std::wstring & strGeometry)
{
    strGeometry = m_strGeometry;
    /*
    dataOut.Count  = 7;
    dataOut.Types  = new BYTE[dataOut.Count];
    dataOut.Points = new PointF[dataOut.Count];

    int p = 0;

    dataOut.Types[p] = 0;
    dataOut.Points[p] = m_Frame[0];
    p++;

    dataOut.Types[p] = 2;
    dataOut.Points[p] = m_Frame[1];
    p++;
    dataOut.Types[p] = 2;
    dataOut.Points[p] = m_Frame[2];
    p++;

    dataOut.Types[p] = 1;
    dataOut.Points[p] = m_Frame[5];
    p++;

    dataOut.Types[p] = 2;
    dataOut.Points[p] = m_Frame[4];
    p++;
    dataOut.Types[p] = 2;
    dataOut.Points[p] = m_Frame[3];
    p++;

    dataOut.Types[p] = 1;
    dataOut.Points[p] = m_Frame[0];
    p++;
    */

    /*
    strFrame = m_strFrame;
    //=======================================
    m_strFrame.clear();
    WCHAR buf[128];
    swprintf(buf, L"A:%.0f:%.0f:%.0f:%.0f:%.0f:%.0f:", pt0.X*100.0f, pt0.Y*100.0f, pt1.X*100.0f, pt1.Y*100.0f, pt2.X*100.0f, pt2.Y*100.0f);
    m_strFrame += buf;
    swprintf(buf, L"L:%.0f:%.0f:%.0f:%.0f:", pt2.X*100.0f, pt2.Y*100.0f, pt5.X*100.0f, pt5.Y*100.0f);
    m_strFrame += buf;
    swprintf(buf, L"A:%.0f:%.0f:%.0f:%.0f:%.0f:%.0f:", pt5.X*100.0f, pt5.Y*100.0f, pt4.X*100.0f, pt4.Y*100.0f, pt3.X*100.0f, pt3.Y*100.0f);
    m_strFrame += buf;
    swprintf(buf, L"L:%.0f:%.0f:%.0f:%.0f:", pt3.X*100.0f, pt3.Y*100.0f, pt0.X*100.0f, pt0.Y*100.0f);
    m_strFrame += buf;
    m_strFrame += L"E";
    */
}
