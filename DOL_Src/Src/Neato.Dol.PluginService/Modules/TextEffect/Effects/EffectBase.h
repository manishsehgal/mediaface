#pragma once
#include <gdiplus.h>
#include "..\CurveDescriptor.h"
#include "..\TextRenderer.h"


class CEffectBase
{
public:
    static CEffectBase * CreateEffect(std::wstring & strType);

public:
    virtual bool Init(std::wstring strData)
    {
        return false;
    }
	virtual void ApplyEffect(CTextRenderer & tr, Gdiplus::PathData & dataOut, int m_nAlign)
    {
    }
    virtual void GetGeometry(std::wstring & strGeometry)
    {
        strGeometry.clear();
    }
};
