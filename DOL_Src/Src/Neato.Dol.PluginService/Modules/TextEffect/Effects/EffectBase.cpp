#include "stdafx.h"
#include "EffectBase.h"
#include "EffectCurved.h"
#include "EffectGuided.h"
#include "EffectGuidedFit.h"
#include "EffectButton.h"
#include "EffectCircle.h"
#include "EffectRounding.h"


CEffectBase * CEffectBase::CreateEffect(std::wstring & strType)
{
	CEffectBase * pEff = 0;
	if (wcsicmp(strType.c_str(), L"Curved") == 0) {
		pEff = new CEffectCurved();
	} else if (wcsicmp(strType.c_str(), L"Guided") == 0) {
		pEff = new CEffectGuided();
	} else if (wcsicmp(strType.c_str(), L"GuidedFit") == 0) {
        pEff = new CEffectGuidedFit();
	} else if (wcsicmp(strType.c_str(), L"Button") == 0) {
		pEff = new CEffectButton();
	} else if (wcsicmp(strType.c_str(), L"Circle") == 0) {
		pEff = new CEffectCircle();
	} else if (wcsicmp(strType.c_str(), L"Rounding") == 0) {
        pEff = new CEffectRounding(false);
	} else if (wcsicmp(strType.c_str(), L"Rounding2") == 0) {
        pEff = new CEffectRounding(true);
    }
	if (!pEff) return 0;
	return pEff;
}
/*
void CEffectBase::GuidePath(Gdiplus::GraphicsPath* pPathToModify, const CPolyCurveDescriptor* pGuide, HTextAlign eHAlign)
{
    RectF rcBox;
    pPathToModify->GetBounds(&rcBox);
    float fX0 = 0.0f;
    switch (eHAlign)
    {
    case htaNear: fX0 = 0; break;
    case htaCenter: fX0 = .5f * pGuide->length() - (rcBox.X + .5f * rcBox.Width); break;
    case htaFar: fX0 = pGuide->length() - (rcBox.X + rcBox.Width); break;
    default: break;
    }
    pPathToModify->Transform(&Matrix(1.0f, 0.0f, 0.0f, 1.0f, fX0, 0.0f));
    PathData data;
    pPathToModify->GetPathData(&data);
    pPathToModify->Reset();
    if (pGuide->length() == 0)
        return;
    GuidedConvert(pGuide, data.Points, data.Count);
    pPathToModify->AddPath(&GraphicsPath(data.Points, data.Types, data.Count), FALSE);
}*/
