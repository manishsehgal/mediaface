#pragma once

#include "EffectBase.h"

class CEffectGuidedFit : public CEffectBase
{
public:
	CEffectGuidedFit();
	~CEffectGuidedFit(){}

public:
	virtual bool Init(std::wstring strData);
	virtual void ApplyEffect(CTextRenderer & tr, Gdiplus::PathData & dataOut, int m_nAlign);

protected:
	CCurveDescriptor m_Guide;
};
