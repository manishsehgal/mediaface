#pragma once
#include <gdiplus.h>
#include "Structures.h"
#include "Effects\EffectBase.h"


class CCurves
{
public:
	CCurves();
	~CCurves();

private:
	std::wstring m_strText;

	std::wstring m_strFont;
    int          m_nFontSize;
    bool         m_bUseLocalFont;
    int          m_nAlign;
    bool         m_bUnderline;

	std::wstring m_strEffType;
	std::wstring m_strEffData;

public:
	BOOL GetContour(BSTR bstrRequest, BSTR * pbstrResponse);
	BOOL SetFont(BSTR bstrRequest, BSTR * pbstrResponse);

private:
	int  makeContour(std::wstring & strContourXml, std::wstring & strGeometryXml);
    bool pathData2String(Gdiplus::PathData & data, std::wstring & strData);
	bool saveFont(LPCWSTR wcName, LPCWSTR wcData);
};

