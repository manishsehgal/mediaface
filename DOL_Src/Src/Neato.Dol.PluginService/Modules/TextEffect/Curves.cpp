#include "stdafx.h"
#include "TxtCnv.h"
#include "Curves.h"
#include "TextRenderer.h"
#define USING_ZIP_LIB
#include "Zip\Zip.h"
#include <map>
using namespace Gdiplus;

extern HMODULE g_hModule;


CCurves::CCurves()
{
    m_nFontSize = 0;
    m_bUseLocalFont = true;
}

CCurves::~CCurves()
{
}

BOOL CCurves::GetContour(BSTR bstrRequest, BSTR * pbstrResponse)
{
	BOOL bRes = TRUE;
	// Initialize GDI+.
	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	try
	{
		//=============================
		//Parse request xml
		CComPtr<MSXML2::IXMLDOMElement>  pReqElem;
		HRESULT hRes = CXmlHelpers::LoadXml(bstrRequest, &pReqElem);
		if (hRes != S_OK) throw 1;

        CComPtr<MSXML2::IXMLDOMElement> pParamsElem;
        hRes = CXmlHelpers::GetChildElement(pReqElem, L"Params", &pParamsElem);
        if (hRes != S_OK) throw 2;

        CComPtr<MSXML2::IXMLDOMElement> pTextElem;
        hRes = CXmlHelpers::GetChildElement(pParamsElem, L"Text", &pTextElem);
        if (hRes != S_OK) throw 3;

        CComPtr<MSXML2::IXMLDOMElement> pEffDescElem;
        hRes = CXmlHelpers::GetChildElement(pParamsElem, L"EffectDesc", &pEffDescElem);
        if (hRes != S_OK) throw 4;

        CComBSTR bstrEffType;
        hRes = CXmlHelpers::GetAttribute(pEffDescElem, L"Type", &bstrEffType);
        if (hRes != S_OK) throw 5;
		m_strEffType = bstrEffType;

		CComBSTR bstrEffData;
		hRes = pEffDescElem->get_text(&bstrEffData);
        if (hRes != S_OK) throw 6;
		m_strEffData = bstrEffData;

        CComBSTR bstrText;
        hRes = pTextElem->get_text(&bstrText);
        if (hRes != S_OK) throw 7;
		m_strText = bstrText;

		m_strFont = L"arial.ttf";
        m_nFontSize = 10;
        m_bUseLocalFont = true;
        CComPtr<MSXML2::IXMLDOMElement> pStyleElem;
		hRes = CXmlHelpers::GetChildElement(pTextElem, L"Style", &pStyleElem);
        if (hRes == S_OK)
		{
			CComBSTR bstrFont;
			hRes = CXmlHelpers::GetAttribute(pStyleElem, L"font", &bstrFont);
			if (hRes == S_OK)
			{
				m_strFont = bstrFont;
			}

			CComBSTR bstrFontSize;
			hRes = CXmlHelpers::GetAttribute(pStyleElem, L"size", &bstrFontSize);
			if (hRes == S_OK)
			{
            	WCHAR * wcStop = 0;
				m_nFontSize = wcstol(bstrFontSize, &wcStop, 10);
			}
            CComBSTR bstrAlign;
            hRes = CXmlHelpers::GetAttribute(pStyleElem, L"align", &bstrAlign);
            if (hRes == S_OK)
            {
                //WCHAR * wcStop = 0;

                if(!wcscmp(bstrAlign, L"left"))
                   m_nAlign = 0;
                else if(!wcscmp(bstrAlign, L"center"))
                   m_nAlign = 1;
                else if(!wcscmp(bstrAlign, L"right"))
                    m_nAlign = 2;
                else 
                    m_nAlign = 0;
                //m_nFontSize = wcstol(bstrFontSize, &wcStop, 10);
            }

            CComBSTR bstrUnderLine;
            hRes = CXmlHelpers::GetAttribute(pStyleElem, L"underline", &bstrUnderLine);
            if (hRes == S_OK)
            {
                m_bUnderline = wcscmp(bstrUnderLine, L"1")==0;
            }

			CComBSTR bstrUseLocal;
			hRes = CXmlHelpers::GetAttribute(pStyleElem, L"useLocal", &bstrUseLocal);
			if (hRes == S_OK)
			{
				m_bUseLocalFont = wcscmp(bstrUseLocal, L"1")==0;
			}
		}

		//=============================
		// Prepare response xml
		CComPtr<MSXML2::IXMLDOMElement>  pRespElem;
		hRes = CXmlHelpers::CreateDocument(CComBSTR(L"Response"), &pRespElem);
        if (hRes != S_OK) throw 8;

        hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"0");
        if (hRes != S_OK) throw 9;

        CComPtr<MSXML2::IXMLDOMElement> pRParamsElem;
		hRes = CXmlHelpers::AddElement(pRespElem, CComBSTR(L"Params"), &pRParamsElem);
        if (hRes != S_OK) throw 10;

        CComPtr<MSXML2::IXMLDOMElement> pStatusElem;
		hRes = CXmlHelpers::AddElement(pRParamsElem, CComBSTR(L"Status"), &pStatusElem);
        if (hRes != S_OK) throw 11;

		//=============================
		std::wstring strContourXml;
		std::wstring strGeometryXml;
		int nRes = makeContour(strContourXml, strGeometryXml);

		if (nRes == 0)
		{
			hRes = CXmlHelpers::SetAttribute(pStatusElem, L"status", L"Ok");
			if (hRes != S_OK) throw 12;
			
			CComPtr<MSXML2::IXMLDOMElement> pContElem;
			hRes = CXmlHelpers::LoadXml(CComBSTR(strContourXml.c_str()), &pContElem);
			if (hRes != S_OK) throw 13;
			CComPtr<MSXML2::IXMLDOMNode> pNewContNode;
			pRParamsElem->appendChild(pContElem, &pNewContNode);

            if (strGeometryXml.size() > 0)
            {
			    CComPtr<MSXML2::IXMLDOMElement> pGeomElem;
			    hRes = CXmlHelpers::LoadXml(CComBSTR(strGeometryXml.c_str()), &pGeomElem);
			    if (hRes != S_OK) throw 15;
			    CComPtr<MSXML2::IXMLDOMNode> pNewFrameNode;
			    pRParamsElem->appendChild(pGeomElem, &pNewFrameNode);
            }
		}
		else if (nRes == 1)
		{
			hRes = CXmlHelpers::SetAttribute(pStatusElem, L"status", L"NeedFont");
			hRes = CXmlHelpers::SetAttribute(pStatusElem, L"font", m_strFont.c_str());
			if (hRes != S_OK) throw 12;
		}

		hRes = pRespElem->get_xml(pbstrResponse);
		if (hRes != S_OK) throw 14;
	}
    catch (int)
    {
		bRes = FALSE;
    }
    catch (...){bRes = FALSE;}

	Gdiplus::GdiplusShutdown(gdiplusToken);

	return bRes;
}

int CCurves::makeContour(std::wstring & strContourXml, std::wstring & strGeometryXml)
{
    //=====================================
    //check if ttf is exist
    if (!CTextRenderer::IsFontAvailable(m_strFont, m_bUseLocalFont))
    {
		return 1;
	}

    //=====================================
	//init variables describing contour limits
	CSegment::InitGlobalLimits();

	//create and init appropr. effect
    CEffectBase * pEff = CEffectBase::CreateEffect(m_strEffType);
    if (!pEff || !pEff->Init(m_strEffData))
	{
        strContourXml = L"<Contour/>"; 
        return 0;
    }

    CTextRenderer tr;
    tr.Init(m_strText, m_strFont, (float)m_nFontSize, m_bUseLocalFont, m_bUnderline);

    //=====================================
	//apply effect to text path
	PathData data;
	pEff->ApplyEffect(tr, data, m_nAlign);
    std::wstring strGeometry;
    pEff->GetGeometry(strGeometry);
	delete pEff;

    //=====================================
	//make contour xml
	strContourXml.clear();
	strContourXml.reserve(data.Count*10);
	strContourXml += L"<Contour>";
    std::wstring strContour;
    pathData2String(data, strContour);
    strContourXml += strContour;
	strContourXml += L"</Contour>";

    //=====================================
	//make geometry xml
   	strGeometryXml.clear();
    if (strGeometry.size() > 0) {
        strGeometryXml += L"<Geometry ";
        strGeometryXml += strGeometry;
        strGeometryXml += L" />";
    }

	return 0;
}

bool CCurves::pathData2String(Gdiplus::PathData & data, std::wstring & strData)
{
	WCHAR buf[256];
	int p = 0;

#define FormatVERSION L"1.00"

	int radix = 36;

	// determine text parameters: number of lines nad maximum line length
	int txtLeng = m_strText.length();
	int lineCount = 0;
	int maxStrLength = 0;
	int pos = 0;
	int nextPos;
	do {
		nextPos = m_strText.find('\n',pos);
		if(nextPos == -1) 
			nextPos = txtLeng;
		lineCount++;
		maxStrLength = __max(maxStrLength,nextPos-pos);
		pos = nextPos+1;
	} while(pos < txtLeng);

	// get width and height of effect
	double effWidth  = CSegment::XMaxGlobal()-CSegment::XMinGlobal();
	double effHeight = CSegment::YMaxGlobal()-CSegment::YMinGlobal();
	if(effWidth <= 0)
		effWidth = 60;
	if(effHeight <= 0)
		effHeight = 40;

	// !!! IMPORTANT: scales should be set to powers of 2 to avoid inaccurities in flash,
	//     because flash silently rounds scales if movieclips to a multiple of 1/2^16
	// So we round values maxStrLength/effWidth*200 and maxStrLength/effWidth*200 to
	// a power of 2 (between 16 and 2^15):
	int scaleX = (int)pow(2,__max(4,__min(15,(int)ceil((double)log((double)maxStrLength/effWidth*200)/log(2.0f))))); 
	int scaleY = (int)pow(2,__max(4,__min(15,(int)ceil((double)log((double)maxStrLength/effWidth*200)/log(2.0f))))); 

	int x=0;
	int y=0;

#define XMLAppendWStr( str )  { strData += (str); strData += L":";}
#define XMLAppendInt( n )   _itow(n,buf,radix);XMLAppendWStr(buf);
#define XMLAppendInt2( n ) {_itow((n)<0?((-n)<<1)+1:(n)<<1,buf,radix);XMLAppendWStr(buf);} 
#define XMLAppendDX(new_x) {int newX=(int)(new_x+0.5); int dx=(newX)-x; XMLAppendInt2(dx); x=(newX);}
#define XMLAppendDY(new_y) {int newY=(int)(new_y+0.5); int dy=(newY)-y; XMLAppendInt2(dy); y=(newY);}
#define XMLAppendPoint( Point ) { PointF* pPoint = &(Point); XMLAppendDX(pPoint->X*scaleX); XMLAppendDY(pPoint->Y*scaleY); } 

	XMLAppendWStr(L"Ver");
	XMLAppendWStr(FormatVERSION);

	XMLAppendWStr(L"R");
	_itow((int)(radix),buf,10); 
	XMLAppendWStr(buf);

	XMLAppendWStr(L"S");
	XMLAppendInt(scaleX);
	XMLAppendInt(scaleY);


	while (true)
	{
		if (p >= data.Count) break;

		BYTE type = data.Types[p] & (BYTE)PathPointTypePathTypeMask;

		switch (type)
        {
        case 0:
		    {
			    XMLAppendWStr(L"M");
			    XMLAppendPoint(data.Points[p++]);
                break;
		    }
        case 1:
		    {
			    XMLAppendWStr(L"L");
			    XMLAppendPoint(data.Points[p++]);
                break;
		    }
        case 2:
		    {
			    if (p+2 > data.Count) break;
			    XMLAppendWStr(L"A");
			    XMLAppendPoint(data.Points[p++]);
			    XMLAppendPoint(data.Points[p++]);
                break;
		    }
        case 3:
		    {
			    if (p+3 > data.Count) break;
			    XMLAppendWStr(L"B");
			    XMLAppendPoint(data.Points[p++]);
			    XMLAppendPoint(data.Points[p++]);
			    XMLAppendPoint(data.Points[p++]);
                break;
		    }
        default:
		    {
			    break;
		    }
        }
	}

	strData += L"E";
    return true;
}

BOOL CCurves::SetFont(BSTR bstrRequest, BSTR * pbstrResponse)
{
	BOOL bRes = TRUE;
	try
	{
		//=============================
		//Parse request xml
		CComPtr<MSXML2::IXMLDOMElement>  pReqElem;
		HRESULT hRes = CXmlHelpers::LoadXml(bstrRequest, &pReqElem);
		if (hRes != S_OK) throw 1;

        CComPtr<MSXML2::IXMLDOMElement> pParamsElem;
        hRes = CXmlHelpers::GetChildElement(pReqElem, L"Params", &pParamsElem);
        if (hRes != S_OK) throw 2;

        CComPtr<MSXML2::IXMLDOMElement> pFontElem;
        hRes = CXmlHelpers::GetChildElement(pParamsElem, L"Font", &pFontElem);
        if (hRes != S_OK) throw 3;

        CComBSTR bstrFontName;
        hRes = CXmlHelpers::GetAttribute(pFontElem, L"Name", &bstrFontName);
        if (hRes != S_OK) throw 5;

		CComBSTR bstrFontBin;
		hRes = pFontElem->get_text(&bstrFontBin);
        if (hRes != S_OK) throw 6;
		int nLen = bstrFontBin.Length();

		if (!saveFont(bstrFontName, bstrFontBin)) throw 6;

		//=============================
		// Prepare response xml
		CComPtr<MSXML2::IXMLDOMElement>  pRespElem;
		hRes = CXmlHelpers::CreateDocument(CComBSTR(L"Response"), &pRespElem);
        if (hRes != S_OK) throw 8;

        hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"0");
        if (hRes != S_OK) throw 9;

        CComPtr<MSXML2::IXMLDOMElement> pRParamsElem;
		hRes = CXmlHelpers::AddElement(pRespElem, CComBSTR(L"Params"), &pRParamsElem);
        if (hRes != S_OK) throw 10;

		hRes = pRespElem->get_xml(pbstrResponse);
		if (hRes != S_OK) throw 14;
	}
    catch (int)
    {
		bRes = FALSE;
    }
    catch (...){bRes = FALSE;}

	return bRes;
}

bool CCurves::saveFont(LPCWSTR wcName, LPCWSTR wcData)
{
    //==================================
	//Make full font file name

	TCHAR buf[MAX_PATH+1]={0};
	::GetModuleFileName(g_hModule, buf, MAX_PATH);
	tstring strFileName = buf;
	long nPos = strFileName.find_last_of(_T("/\\"));
	if (nPos == tstring::npos) return false;

	strFileName.resize(nPos);
	strFileName += _T("\\Fonts");
	if (::GetFileAttributes(strFileName.c_str()) == INVALID_FILE_ATTRIBUTES)
	{
		::CreateDirectory(strFileName.c_str(), 0);
	}
	if (::GetFileAttributes(strFileName.c_str()) == INVALID_FILE_ATTRIBUTES)
	{
		return false;
	}

	strFileName += _T("\\");
	strFileName += CTxtCnv().w2t(wcName);

    //==================================
	//Un-base64

    //base64 encode data
    static WCHAR TableBase64[] = L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	DWORD dwSizeB = wcslen(wcData);
	if (dwSizeB == 0) return false;
	if (dwSizeB % 4 != 0) return false;

	DWORD dwSizeC = dwSizeB / 4 * 3;
	BYTE * pBufC = new BYTE[dwSizeC];
	std::map<WCHAR, DWORD> chars;
	for (DWORD j=0; j<64; j++)
	{
		chars[TableBase64[j]] = j;
	}

	for (DWORD i=0, c=0; i<dwSizeB; i+=4)
	{
		int  nInSym  = 4;
		int  nOutSym = 3;
		if (wcData[i+2] == L'=' && wcData[i+3] == L'=')
		{
			nInSym    = 2;
			nOutSym   = 1;
		}
		else if (wcData[i+3] == L'=')
		{
			nInSym    = 3;
			nOutSym   = 2;
		}

		DWORD dwQ = (chars[wcData[i]]<<18) | (chars[wcData[i+1]]<<12);
		if (nInSym > 2)
		{
			dwQ |= chars[wcData[i+2]]<<6;
		}
		if (nInSym > 3)
		{
			dwQ |= chars[wcData[i+3]];
		}

		pBufC[c++] = (BYTE)(dwQ >> 16);
		if (nOutSym > 1)
		{
			pBufC[c++] = (BYTE)(dwQ >> 8);
		}
		if (nOutSym > 2)
		{
			pBufC[c++] = (BYTE)dwQ;
		}
	}
	dwSizeC = c;

    //==================================
	//Un-zip data

	DWORD dwSize = dwSizeC * 20;
	BYTE * pBuf = new BYTE[dwSize];
	CZip zip;
	DWORD dwRes = zip.InflateMem(pBufC, dwSizeC, pBuf, &dwSize);
	delete[] pBufC;
	if (dwRes != 0) return false;

    //==================================
	//Save file
	HANDLE hFile = ::CreateFile(strFileName.c_str(), GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, 0, NULL);
	if (hFile == INVALID_HANDLE_VALUE) return false;
	DWORD dwWr = 0;
	::WriteFile(hFile, pBuf, dwSize, &dwWr, 0);
	::CloseHandle(hFile);
	delete[] pBuf;

	return true;
}

