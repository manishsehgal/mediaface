#include "stdafx.h"
#include "Curves.h"

CCurves * pCurves = 0;
HMODULE   g_hModule = 0;


BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
	g_hModule = (HMODULE)hModule;
    return TRUE;
}

extern "C"
BOOL CALLBACK  GetInfo(BSTR * pbstrInfo)
{
	CComBSTR bstr = 
		L"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
		L"<ModuleInfo Version=\"1.0.0.2\" Name=\"TextEffectModule\" Description=\"Text Effects Support Module\" Filename=\"modules\\txteff.dll\">"
		L"    <Commands>"
		L"		<Command Name=\"TEXTEFFECT_GET_CONTOUR\" />"
		L"		<Command Name=\"TEXTEFFECT_SET_FONT\" CopyRequest=\"0\" />"
		L"	</Commands>"
		L"</ModuleInfo>";
	*pbstrInfo = bstr.Detach();
	return TRUE;
}

extern "C"
BOOL CALLBACK Init(BOOL bInit)
{
	if(bInit)
	{
		pCurves = new CCurves();
	}
	else
	{
		if (pCurves) delete pCurves;
	}
	return TRUE;
}

extern "C"
BOOL CALLBACK Invoke(BSTR bstrRequest, BSTR * pbstrResponse)
{
	try
	{
		CComPtr<MSXML2::IXMLDOMElement> pRequestEl;
		CXmlHelpers::LoadXml(bstrRequest, &pRequestEl);

		CComBSTR bstrCmd;
		CXmlHelpers::GetAttribute(pRequestEl, L"Name", &bstrCmd);
		if (wcsicmp(bstrCmd, L"TEXTEFFECT_GET_CONTOUR") == 0)
		{
			return pCurves->GetContour(bstrRequest, pbstrResponse);
		}
		else if (wcsicmp(bstrCmd, L"TEXTEFFECT_SET_FONT") == 0)
		{
			return pCurves->SetFont(bstrRequest, pbstrResponse);
		}
		else
		{
			return FALSE;
		}
	}
	catch(...) {return FALSE;}
	return TRUE;
}

