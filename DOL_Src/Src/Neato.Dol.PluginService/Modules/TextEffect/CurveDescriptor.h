#pragma once
#include <gdiplus.h>
#include "GeomUtils.h"
#include <limits>


//===========================================================
enum SegmentType {cstNone, cstLine, cstBezier, cstArc};

class CSegment
{
public:
	CSegment() { xMin = FLT_MAX; xMax = -FLT_MAX; yMin = FLT_MAX; yMax = -FLT_MAX;}

	virtual int              init(LPCWSTR pwcData) = 0;
	virtual long             refCount() const = 0;
	virtual Gdiplus::PointF  operator[](long lIndex) const = 0;
	virtual bool             setRefPoint(long lIndex, const Gdiplus::PointF & rptRef) = 0;
	virtual float            length ()         const = 0;
	virtual Gdiplus::PointF  point  (float fT) const = 0;
	virtual Gdiplus::VectorF dir    (float fT) const = 0;
	virtual Gdiplus::VectorF norm   (float fT) const = 0;

protected:
	int parseRefPoints(LPCWSTR pwcData, int nCount);

	void updateLimits(Gdiplus::PointF point);

	float xMin;
	float xMax;
	float yMin;
	float yMax;

	static float xMinGlobal;
	static float xMaxGlobal;
	static float yMinGlobal;
	static float yMaxGlobal;

public:
	static void InitGlobalLimits();
	static float XMinGlobal() { return xMinGlobal; }
	static float XMaxGlobal() { return xMaxGlobal; }
	static float YMinGlobal() { return yMinGlobal; }
	static float YMaxGlobal() { return yMaxGlobal; }

};


//===========================================================
class CBezierSegment : public CSegment
{
public:
	CBezierSegment();
public:
	virtual int              init(LPCWSTR pwcData);
	virtual long             refCount() const;
	virtual Gdiplus::PointF  operator[](long lIndex) const;
	virtual bool             setRefPoint(long lIndex, const Gdiplus::PointF & rptRef);
	virtual float            length ()         const;
	virtual Gdiplus::PointF  point  (float fT) const;
	virtual Gdiplus::VectorF dir    (float fT) const;
	virtual Gdiplus::VectorF norm   (float fT) const;
private:
	float m_fLength;
	Gdiplus::PointF m_pptBezier[4];
private:
	void calc_length();
};


//===========================================================
class CLineSegment : public CSegment
{
public:
	CLineSegment();
public:
	virtual int              init(LPCWSTR pwcData);
	virtual long             refCount() const;
	virtual Gdiplus::PointF  operator[](long lIndex) const;
	virtual bool             setRefPoint(long lIndex, const Gdiplus::PointF & rptRef);
	virtual float            length() const;
	virtual Gdiplus::PointF  point  (float fT) const;
	virtual Gdiplus::VectorF dir    (float fT) const;
	virtual Gdiplus::VectorF norm   (float fT) const;
private:
	Gdiplus::PointF m_pptLine[2];
	float m_fLength;
private:
	void calc_length();
};


//===========================================================
class CArcSegment : public CSegment
{
public:
	CArcSegment();
public:
	virtual int              init(LPCWSTR pwcData);
	virtual long             refCount() const;
	virtual Gdiplus::PointF  operator[](long lIndex) const;
	virtual bool             setRefPoint(long lIndex, const Gdiplus::PointF & rptRef);
	virtual float            length ()         const;
	virtual Gdiplus::PointF  point  (float fT) const;
	virtual Gdiplus::VectorF dir    (float fT) const;
	virtual Gdiplus::VectorF norm   (float fT) const;
private:
	float m_fLength;
	Gdiplus::PointF m_pptArc[3];
	Gdiplus::PointF m_ptCenter;
	float m_fRadius;
	float m_fStartAngle, m_fSweepAngle;
private:
	void calc_arc_params();
};


//===========================================================
class CCurveDescriptor : protected std::vector<CSegment*>
{
public:
	CCurveDescriptor();
	~CCurveDescriptor();
public:
	CSegment *      AddSegment(SegmentType type);
	CSegment *      AddSegment(WCHAR wcCode);
	void            add(CSegment * pSegment);
	void            RemoveAllSegments();
	long            refCount() const;
	Gdiplus::PointF getRefPoint(long lIndex) const;
	bool            setRefPoint(long lIndex, const Gdiplus::PointF& rptRef);
	float           length() const;
	Gdiplus::PointF point(float fT) const;
	Gdiplus::PointF norm(float fT) const;
protected:
	bool m_bClosed;
};
