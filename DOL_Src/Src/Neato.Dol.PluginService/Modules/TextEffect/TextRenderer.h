#pragma once
#include <gdiplus.h>

class CTextRenderer
{
public:
    CTextRenderer();
    ~CTextRenderer();
protected:
    void SplitString();

public:
    bool Init(std::wstring & strText, std::wstring & strFont, float fFontHeight, bool bUseLocal, bool bUnderline);
    bool AddParaToPath(Gdiplus::GraphicsPath & pathDest, float fWidth);
    int  DrawTextLineToPath(float fWidth, Gdiplus::GraphicsPath & pathDest);
    std::wstring * AlignFormatString(std::wstring &input, int nAlign, float fWidth);
public:
	static bool IsFontAvailable(std::wstring strFont, bool bUseLocal);
	static std::wstring GetFontFileName(std::wstring strFont);
    

public:
    std::wstring m_strText;
    int          m_nNextChar;
	float        m_fYPos;
    bool        m_bParaMode;
    std::vector <std::wstring> m_vString;
    std::vector <std::wstring>::iterator m_ivString;


	Gdiplus::PrivateFontCollection * m_pFontColl;
    Gdiplus::FontFamily            * m_pFontFamily;
    Gdiplus::FontStyle               m_FontStyle;
    Gdiplus::StringFormat            m_StringFormat;
    float                            m_fFontHeight;
};