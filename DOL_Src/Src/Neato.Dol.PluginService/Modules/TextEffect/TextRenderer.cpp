#include "stdafx.h"
#include "TxtCnv.h"
#include "TextRenderer.h"
using namespace Gdiplus;

extern HMODULE g_hModule;


CTextRenderer::CTextRenderer():m_StringFormat( StringFormat::GenericTypographic())
{
    m_nNextChar = 0;
   	m_fYPos     = 0.0f;

    m_pFontColl   = 0;
    m_pFontFamily = 0;
    m_FontStyle   = FontStyleRegular;
    m_fFontHeight = 0.0f;
    
}

CTextRenderer::~CTextRenderer()
{
    if (m_pFontFamily) delete[] m_pFontFamily;
    if (m_pFontColl)   delete   m_pFontColl;
}

bool CTextRenderer::Init(std::wstring & strText, std::wstring & strFont, float fFontHeight, bool bUseLocal, bool bUnderline)
{
    m_strText   = strText;
    m_nNextChar = 0;
   	m_fYPos     = 0.0f;

    // prepare font
    if (m_pFontColl) delete m_pFontColl;
    m_pFontColl = new PrivateFontCollection();

    SplitString();
    if (bUseLocal)
    {
        m_pFontColl->AddFontFile(strFont.c_str());
    }

	int nCount = m_pFontColl->GetFamilyCount();
	if (nCount == 0)
    {
        m_pFontColl->AddFontFile(CTextRenderer::GetFontFileName(strFont).c_str());
    	nCount = m_pFontColl->GetFamilyCount();
	    if (nCount == 0) return false;
    }

    if (m_pFontFamily) delete[] m_pFontFamily;
	m_pFontFamily = new FontFamily[nCount];
	int nFamilies = 0;
	m_pFontColl->GetFamilies(nCount, m_pFontFamily, &nFamilies);
    if (nFamilies == 0) return false;

	m_FontStyle = FontStyleRegular;
    
	if      (m_pFontFamily->IsStyleAvailable(FontStyleRegular)    && !bUnderline) m_FontStyle = FontStyleRegular;
	else if (m_pFontFamily->IsStyleAvailable(FontStyleBold)       && !bUnderline) m_FontStyle = FontStyleBold;
	else if (m_pFontFamily->IsStyleAvailable(FontStyleItalic)     && !bUnderline) m_FontStyle = FontStyleItalic;
	else if (m_pFontFamily->IsStyleAvailable(FontStyleBoldItalic) && !bUnderline) m_FontStyle = FontStyleBoldItalic;
    else if (m_pFontFamily->IsStyleAvailable(FontStyleRegular    | FontStyleUnderline) && bUnderline) m_FontStyle = Gdiplus::FontStyle(FontStyleRegular    | FontStyleUnderline);
    else if (m_pFontFamily->IsStyleAvailable(FontStyleBold       | FontStyleUnderline) && bUnderline) m_FontStyle = Gdiplus::FontStyle(FontStyleBold       | FontStyleUnderline);
    else if (m_pFontFamily->IsStyleAvailable(FontStyleItalic     | FontStyleUnderline) && bUnderline) m_FontStyle = Gdiplus::FontStyle(FontStyleItalic     | FontStyleUnderline);
    else if (m_pFontFamily->IsStyleAvailable(FontStyleBoldItalic | FontStyleUnderline) && bUnderline) m_FontStyle = Gdiplus::FontStyle(FontStyleBoldItalic | FontStyleUnderline);

       
    m_fFontHeight = fFontHeight;
    m_bParaMode = false;
    return true;
}

bool CTextRenderer::AddParaToPath(Gdiplus::GraphicsPath & pathDest, float fWidth)
{
    m_nNextChar = 0;

	if (fWidth < 0.0f)
	{
    	GraphicsPath path;
		DrawTextLineToPath(fWidth, path);

		pathDest.AddPath(&path, FALSE);
		return true;
	}
    else
    {
    	RectF rc;
    	GraphicsPath path;
	    while (1)
	    {
		    int iRes = DrawTextLineToPath(fWidth, path);

		    if (iRes == -1 || iRes == 0)
		    {
			    pathDest.AddPath(&path, FALSE);
			    return true;
		    }
        }
    }

    return true;
}

int CTextRenderer::DrawTextLineToPath(float fWidth, Gdiplus::GraphicsPath & pathDest)
{
    std::wstring oldText = m_strText;
    if(m_bParaMode){
        m_strText = *m_ivString;
      //  m_nNextChar = 0;
    }
    int nLength = m_strText.size();
	if (m_nNextChar >= nLength) return -1;

   	//StringFormat sf(StringFormatFlagsMeasureTrailingSpaces|StringFormat::GenericTypographic()->GetFormatFlags());//StringFormat::GenericTypographic());
    

	int   nFit = 0;
    float fOldWidth = 0.0f;
    if (fWidth > 0.0f)
    {
        for (int i=m_nNextChar; i<(int)m_strText.size(); i++)
        {
            GraphicsPath path;
	        path.AddString(m_strText.c_str()+m_nNextChar, i-m_nNextChar+1, m_pFontFamily, m_FontStyle, m_fFontHeight, PointF(0.0f,0.0f), &m_StringFormat);

	        RectF rcBox;
	        Matrix mat;
	        path.GetBounds(&rcBox);
            

            if (rcBox.Width > fWidth){
                /*wchar_t * t = wcsrchr(m_strText.c_str(),L'\n');   
                if((int)t>m_nNextChar){
                 ///   path.Reset();
                    path.AddString(m_strText.c_str()+m_nNextChar, m_strText.size()-(t-m_strText.c_str()), m_pFontFamily, m_FontStyle, m_fFontHeight, PointF(0.0f,0.0f), &m_StringFormat);
                }*/
                break;
            }
            
        }
        nFit = i - m_nNextChar;
    }
    else
    {
        nFit = nLength;
    }

    if (nFit == 0) return 0;

	PointF pt(0.0f, m_fYPos);
	pathDest.AddString(m_strText.c_str()+m_nNextChar, nFit, m_pFontFamily, m_FontStyle, m_fFontHeight, pt, &m_StringFormat);
    m_nNextChar += nFit;
    m_fYPos += m_fFontHeight;
    if(m_bParaMode){
        //oldText = *m_ivString;
        m_strText = oldText;
    }
	if (m_nNextChar >= nLength) return -1;
	return 1;
}

std::wstring CTextRenderer::GetFontFileName(std::wstring strFont)
{
	TCHAR buf[MAX_PATH+1]={0};
	::GetModuleFileName(g_hModule, buf, MAX_PATH);
	std::wstring strFileName = CTxtCnv().t2w(buf);
	long nPos = strFileName.find_last_of(L"/\\");
	if (nPos != std::wstring::npos)
	{
		strFileName.resize(nPos + 1);
		strFileName += L"Fonts\\";
		strFileName += strFont;
	}
	return strFileName;
}

bool CTextRenderer::IsFontAvailable(std::wstring strFont, bool bUseLocal)
{
    PrivateFontCollection fc;
    if (bUseLocal)
    {
        fc.AddFontFile(strFont.c_str());
    }
	if (fc.GetFamilyCount() == 0)
    {
        fc.AddFontFile(CTextRenderer::GetFontFileName(strFont).c_str());
        if (fc.GetFamilyCount() == 0) return false;
    }
    return true;
}

std::wstring * CTextRenderer::AlignFormatString(std::wstring &input, int nAlign, float fWidth){

    WCHAR * spaceChar = L" ";
    if(nAlign == 0) //LEFT
        return &input;
    else if(nAlign > 0) { //MIDDLE

        UINT lineStart =0;
        UINT lineLength = 0;
        const WCHAR *p = input.c_str();
        if(wcschr(p,L'\n') == NULL) {
            
            RectF bounds(0.0f, 0.0f, 0.0f, 0.0f);
            GraphicsPath path;
            StringFormat sf(StringFormat::GenericTypographic());
            path.AddString(input.c_str(),input.size(),m_pFontFamily, m_FontStyle,m_fFontHeight,PointF(0.0f,m_fYPos),&sf);
            path.GetBounds(&bounds);
            while(bounds.Width < fWidth) {
                path.Reset();
                if(nAlign == 1){ // middle
                    input = spaceChar + input + spaceChar;

                }else if(nAlign == 2){ //right
                    input = spaceChar + input;
                }
            }                
            return &input;
        } else {
            while(lineStart < input.size()){
                p = input.c_str();
                RectF bounds(0.0f, 0.0f, 0.0f, 0.0f);

                WCHAR *lStart = wcschr(p+lineStart,L'\n');
                if(lStart == NULL && lineStart < input.size())
                {
                    lineLength = input.size() - lineStart;
                }
                else
                    lineLength = lStart - (p+lineStart);
                WCHAR buff [20480] = {0};
                wcsncpy(buff,p+lineStart, lineLength);
                std::wstring line = buff;

                bool bFirstTime = true;

                

                GraphicsPath path;
                StringFormat sf(StringFormat::GenericTypographic());
                UINT addedChars = 0;
                do 
                {
                    path.Reset();
                    if(!bFirstTime) {
                        if(nAlign == 1){ // middle
                            line = spaceChar + line + spaceChar;
                            addedChars += 2;
                        } else if(nAlign == 2) { //right
                            line = spaceChar + line;
                            addedChars ++;
                        }
                    }
                    bFirstTime = false;
                    path.AddString(std::wstring(line + L'.').c_str(), line.size()+1,m_pFontFamily, m_FontStyle | FontStyleUnderline, m_fFontHeight,PointF(0.0f,m_fYPos),&sf);
                    path.GetBounds(&bounds);
                } while(bounds.Width <= fWidth);
                if(addedChars >0){
                    while(addedChars >0) {
                        if(nAlign == 1){ // middle
                            input.insert(lineStart+lineLength, spaceChar, 1);
                            input.insert(lineStart, spaceChar, 1);
                            lineLength += 2;
                            addedChars -= 2;
                        }else if(nAlign == 2) { //right
                            input.insert(lineStart, spaceChar, 1);
                            lineLength ++;
                            addedChars --;
                        }
                    }
                }
                lineStart = lineStart+lineLength + 1;
            }
            return &input;
        }
        return &input;
    }
    return &input;
}

void CTextRenderer::SplitString() {
    m_vString.clear();
    if(m_strText.length()<1)
          return;
    LPCWSTR t = m_strText.c_str();

    LPWSTR pos = NULL;
    int offset = 0;
    while((pos = wcschr(t+offset,L'\n')) != NULL) {

        WCHAR buff[65535] = {0};
        wcsncpy(buff,t+offset,pos-(t+offset));
        std::wstring line(buff);
        m_vString.push_back(line);
        offset = pos- t+1;
    }
    if(wcslen(t)>offset){
        WCHAR buff[65535] = {0};
        wcsncpy(buff,t+offset,wcslen(t)-offset);
        std::wstring line(buff);
        m_vString.push_back(line);
    }


}

