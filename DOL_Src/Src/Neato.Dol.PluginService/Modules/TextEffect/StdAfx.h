#pragma once


//#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#include <windows.h>
#include <atlbase.h>
#include <msxml.h>
#include <string>
#include <vector>

typedef std::basic_string<TCHAR> tstring;

#import <msxml3.dll> raw_interfaces_only
#include <XmlHelpers.h>
