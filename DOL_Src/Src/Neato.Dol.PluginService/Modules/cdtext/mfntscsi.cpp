
#include "stdafx.h"
#include "mfntscsi.h"

/*      CSCASIReaderNT       */

CSCSIReaderNT::CSCSIReaderNT()
{
	hDriveFile = NULL;
}

CSCSIReaderNT::~CSCSIReaderNT()
{
	CloseDrive();
}

bool CSCSIReaderNT::InitDrive()
{
	CloseDrive();
	if (!dWorkCDROMDrive)
		return false;
	TCHAR chDrivePath[] = TEXT("\\\\.\\A:");
	chDrivePath[4] += (TCHAR)(dWorkCDROMDrive - 1);
	hDriveFile = CreateFile(chDrivePath,
		                    GENERIC_WRITE | GENERIC_READ,
		                    FILE_SHARE_READ | FILE_SHARE_WRITE, 
                            NULL,
                            OPEN_EXISTING,
                            0,
                            NULL);
	if (hDriveFile == INVALID_HANDLE_VALUE)
	    hDriveFile = NULL;
	return hDriveFile != NULL;
}

void CSCSIReaderNT::CloseDrive()
{
	if (hDriveFile)
	{
		CloseHandle(hDriveFile);
		hDriveFile = NULL;
	}
}

long CSCSIReaderNT::ExecSCSICommand(LPVOID pData, int iDataSize,
									BYTE cdb0, BYTE cdb1, BYTE cdb2, BYTE cdb3,
									BYTE cdb4, BYTE cdb5, BYTE cdb6, BYTE cdb7,
									BYTE cdb8, BYTE cdb9)
{
//	_ASSERT(hDriveFile != 0);	
	SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER sptd = { 0 };
	sptd.spt.Length    = sizeof(SCSI_PASS_THROUGH);
    sptd.spt.CdbLength = sizeof(sptd.spt.Cdb);
	sptd.spt.DataIn    = SCSI_IOCTL_DATA_IN;
	sptd.spt.TimeOutValue = 5;
	sptd.spt.DataTransferLength = iDataSize;
	sptd.spt.DataBuffer = pData;
	sptd.spt.SenseInfoOffset = sizeof(sptd.spt) + sizeof(sptd.Filler);
	sptd.spt.SenseInfoLength = sizeof(sptd.ucSenseBuf);
	sptd.spt.Cdb[0] = cdb0;
	sptd.spt.Cdb[1] = cdb1;
	sptd.spt.Cdb[2] = cdb2;
	sptd.spt.Cdb[3] = cdb3;
	sptd.spt.Cdb[4] = cdb4;
	sptd.spt.Cdb[5] = cdb5;
	sptd.spt.Cdb[6] = cdb6;
	sptd.spt.Cdb[7] = cdb7;
	sptd.spt.Cdb[8] = cdb8;
	sptd.spt.Cdb[9] = cdb9;
	// Reset error info
	m_scError.bError = false;
	m_scError.ScsiStatus = 0;
	m_scError.ASC  = 0;
	m_scError.ASCQ = 0;
	register DWORD dwReturned = 0;
	BOOL bRESULT = DeviceIoControl(hDriveFile, IOCTL_SCSI_PASS_THROUGH_DIRECT,
									&sptd, sizeof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER),
			   						&sptd, sizeof(SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER),
									&dwReturned, NULL);
	if (bRESULT && dwReturned)
	{
		SENSE_ERROR_BUFFER_FORMAT* pSenseBuff = (SENSE_ERROR_BUFFER_FORMAT*)sptd.ucSenseBuf;
		if (pSenseBuff->error_code)
		{
			bRESULT = FALSE;
			m_scError.bError = true;
			m_scError.ScsiStatus = sptd.spt.ScsiStatus;
			m_scError.ASC  = pSenseBuff->ASC;
			m_scError.ASCQ = pSenseBuff->ASCQ;
		}
	}
	return bRESULT;
}

bool CSCSIReaderNT::isServiceAvailable()
{
	return true;
}