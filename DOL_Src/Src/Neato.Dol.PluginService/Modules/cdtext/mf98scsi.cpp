
#include "stdafx.h"
#include "mf98scsi.h"

/*     CSCASIReader98        */

CSCSIReader98::CSCSIReader98()
{
	p_mfnGetInfo = NULL;
	p_mfnSendCommand = NULL;
	hSCSIEvent = NULL;
	memset(&m_cd, 0, sizeof(m_cd));
	m_cd.cur = 0xFF;	
	h_wnaspi_Dll = ::LoadLibrary(_T("WNASPI32"));
	if (h_wnaspi_Dll)
	{		
		p_mfnGetInfo = (FNGetASPI32SupportInfo)::GetProcAddress(h_wnaspi_Dll, "GetASPI32SupportInfo"); // !!! Not UNICODE
		p_mfnSendCommand = (FNSendASPI32Command)::GetProcAddress(h_wnaspi_Dll, "SendASPI32Command");   // !!! Not UNICODE		
		if (p_mfnGetInfo == NULL || HIBYTE(p_mfnGetInfo()) != SS_COMP || !GetCDList())
		{			
			// FreeLibrary(h_wnaspi_Dll); !!! BUG See comment on error below
			h_wnaspi_Dll = NULL;
			p_mfnGetInfo = NULL;
			p_mfnSendCommand = NULL;			
		}
	}
}

unsigned char CSCSIReader98::GetRegisterSCSIDriveLetter(BYTE btTargetID, BYTE btLunID, char* pProductID)
{
	const REGSAM regAccess = KEY_READ | KEY_EXECUTE;
	unsigned char ucFinded = 0xFF;
	int iValue;
	CRegKey keySCSI;
	UINT nErrCode = keySCSI.Open(HKEY_LOCAL_MACHINE, _T("Enum\\SCSI"), regAccess);
	if (nErrCode == ERROR_SUCCESS)
	{
		DWORD dwIndex = 0;
		DWORD dwSize = MAX_PATH;
		TCHAR szKeyName[MAX_PATH];
		while (RegEnumKeyEx((HKEY)keySCSI, dwIndex++, szKeyName, &dwSize, NULL, NULL, NULL, NULL) == ERROR_SUCCESS)
		{			
			DWORD dwValueSize = 4;
			TCHAR szKeyValue[5];
			CRegKey keySubSCSI;
			if (keySubSCSI.Open((HKEY)keySCSI, szKeyName, regAccess) == ERROR_SUCCESS)
			{				
				if (keySubSCSI.QueryStringValue(_T("SCSILUN"), szKeyValue, &dwValueSize) != ERROR_SUCCESS)
				{
					dwSize = MAX_PATH;
					DWORD dwSubKeyIndex = 0;
					while (RegEnumKeyEx((HKEY)keySubSCSI, dwSubKeyIndex++, szKeyName, &dwSize, NULL, NULL, NULL, NULL) == ERROR_SUCCESS)
					{						
						CRegKey keyLastSCSI;
						if (keyLastSCSI.Open((HKEY)keySubSCSI, szKeyName, regAccess) == ERROR_SUCCESS)							
						{							
							dwValueSize = 4;
							if (keyLastSCSI.QueryStringValue(_T("SCSILUN"), szKeyValue, &dwValueSize) == ERROR_SUCCESS)
							{
								CRegKey keyDataSCSI;
								if (keyDataSCSI.Open((HKEY)keyLastSCSI, NULL, regAccess) == ERROR_SUCCESS)
								{
									szKeyValue[dwValueSize] = 0;
									iValue = _ttoi(szKeyValue);									
									if ((btLunID == BYTE(iValue)) && (keyDataSCSI.QueryStringValue(_T("SCSITargetID"), szKeyValue, &dwValueSize) == ERROR_SUCCESS))
									{
										szKeyValue[dwValueSize] = 0;
										iValue = _ttoi(szKeyValue);
										if (btTargetID == BYTE(iValue))
										{												
											char pProductBuf[18] = { 0 };
											DWORD dwType = NULL;
											dwValueSize = 17;
											bool bGoodRead = RegQueryValueExA((HKEY)keyDataSCSI, "ProductId", NULL, &dwType, (LPBYTE)pProductBuf, &dwValueSize) == ERROR_SUCCESS;
											if (bGoodRead && !_stricmp(pProductID, pProductBuf))
											{
												dwValueSize = 9;
												if (RegQueryValueExA((HKEY)keyDataSCSI, "CurrentDriveLetterAssignment", NULL, &dwType, (LPBYTE)pProductBuf, &dwValueSize) == ERROR_SUCCESS)
												{													
													ucFinded = pProductBuf[0] - 'A' + 1;
													malloc(10);
													return ucFinded;
												}
											}
										}
									}
								}
							}
						}
						dwSize = MAX_PATH;
					}
				}
			}
			dwSize = MAX_PATH;
		}
	}
	return ucFinded;
}

bool CSCSIReader98::GetCDList()
{
	DWORD d;
	BYTE bHACount;
	BYTE bASPIStatus;
	
	d = p_mfnGetInfo();
	bASPIStatus = HIBYTE(LOWORD(d));
	bHACount    = LOBYTE(LOWORD(d));

	BYTE numAdapters = (bASPIStatus != SS_COMP && bASPIStatus != SS_NO_ADAPTERS) ? 0 : bHACount;
	if (numAdapters == 0)
		return false;

	BYTE maxTgt = 0;
	SRB_HAInquiry sHACmd;
	SRB_GDEVBlock sDEVCmd;
	int iBufSize = max(max(sizeof(SRB_HAInquiry), sizeof(SRB_GDEVBlock)), sizeof(SRB_GetDiskInfo)) + 256; // !!! For out memmory buffer
	char* pCmdBuf = new char[iBufSize];
	for (BYTE i = 0; i < numAdapters; i++)
	{
		memset(pCmdBuf, 0, iBufSize);
		((SRB_HAInquiry*)pCmdBuf)->SRB_Cmd  = SC_HA_INQUIRY;
		((SRB_HAInquiry*)pCmdBuf)->SRB_HaId = i;
		p_mfnSendCommand((LPSRB)pCmdBuf);
		memcpy(&sHACmd, pCmdBuf, sizeof(SRB_HAInquiry));

		/* on error skip to next adapter */
		if (sHACmd.SRB_Status != SS_COMP)
			continue;

		BYTE maxTgt = sHACmd.HA_Unique[3];
		if (maxTgt == 0)
			maxTgt = 8;
		for (BYTE j = 0; j < maxTgt; j++)
		{
			for (BYTE k = 0; k < 8; k++)
			{				
				memset(pCmdBuf, 0, iBufSize);
				((SRB_GDEVBlock*)pCmdBuf)->SRB_Cmd    = SC_GET_DEV_TYPE;
				((SRB_GDEVBlock*)pCmdBuf)->SRB_HaId   = i;
				((SRB_GDEVBlock*)pCmdBuf)->SRB_Target = j;
				((SRB_GDEVBlock*)pCmdBuf)->SRB_Lun    = k;
				p_mfnSendCommand((LPSRB)pCmdBuf);
				memcpy(&sDEVCmd, pCmdBuf, sizeof(SRB_GDEVBlock));
				if (sDEVCmd.SRB_Status == SS_COMP)
				{
					if (sDEVCmd.SRB_DeviceType == DTYPE_CDROM)
					{
						memset(pCmdBuf, 0, iBufSize);
						((SRB_GetDiskInfo*)pCmdBuf)->SRB_Cmd    = SC_GET_DISK_INFO;
						((SRB_GetDiskInfo*)pCmdBuf)->SRB_HaId   = i;
						((SRB_GetDiskInfo*)pCmdBuf)->SRB_Target = j;
						((SRB_GetDiskInfo*)pCmdBuf)->SRB_Lun    = k;
						p_mfnSendCommand((LPSRB)pCmdBuf);
						SRB_GetDiskInfo srbDInfo;
						memcpy(&srbDInfo, pCmdBuf, sizeof(SRB_GetDiskInfo));
						if (srbDInfo.SRB_Status == SS_COMP)
						{
							unsigned char ucDriveLetter;
							ucDriveLetter = srbDInfo.SRB_Int13HDriveInfo;
							bool bIsLetter = (srbDInfo.SRB_DriveFlags & (DISK_INT13_AND_DOS | DISK_INT13)) != 0;
							if (!bIsLetter)
							{
								if (m_cd.max >= MAXCDCOUNT)
									break;
								/* Find in registry */
								unsigned char ucOldCur = m_cd.cur;
								m_cd.cur = m_cd.max;
								m_cd.cd[m_cd.cur].ha  = i;
								m_cd.cd[m_cd.cur].tgt = j;
								m_cd.cd[m_cd.cur].lun = k;
								char sProductData[132] = { 0 };
								if (ExecSCSICommand(sProductData, 128,
													0x12 /* GET INQUIRY */, 0, 0, 0, 128,
													0, 0, 0, 0, 0))
								{
									char sProductID[17] = { 0 };
									memcpy(sProductID, &sProductData[16], 16);
									ucDriveLetter = GetRegisterSCSIDriveLetter(j, k, sProductID);
									bIsLetter = ucDriveLetter != 0xFF;
								}
								m_cd.cur = ucOldCur;
							}
							if (bIsLetter && m_cd.max < MAXCDCOUNT)
							{
								m_cd.cd[m_cd.max].ha  = i;
								m_cd.cd[m_cd.max].tgt = j;
								m_cd.cd[m_cd.max].lun = k;
								m_cd.cd[m_cd.max].ltr = ucDriveLetter;
								m_cd.max++;
							}
						}
					}
				}
			}
		}
	}
	delete[] pCmdBuf;
	return m_cd.max != 0;
}

CSCSIReader98::~CSCSIReader98()
{
	if (hSCSIEvent)
		CloseHandle(hSCSIEvent);
	/* In Windows 98 is BUG !!!
	   Unexpected exception ocure on FreeLibrary WNASPI32.dll
	   !!! NOT CALL this function in this class */
	//if (h_wnaspi_Dll)
	//	::FreeLibrary(h_wnaspi_Dll);
}

bool CSCSIReader98::InitDrive()
{
	CloseDrive();
	if (dWorkCDROMDrive)
	{
		for (int i = 0; i < m_cd.max; i++)
			if (m_cd.cd[i].ltr == dWorkCDROMDrive)
			{				
				m_cd.cur = i;
				return true;
			}
	}
	return false;
}

void CSCSIReader98::CloseDrive()
{
	m_cd.cur = 0xFF;
}

long CSCSIReader98::ExecSCSICommand(LPVOID pData, int iDataSize,
									BYTE cdb0, BYTE cdb1, BYTE cdb2, BYTE cdb3,
									BYTE cdb4, BYTE cdb5, BYTE cdb6, BYTE cdb7,
									BYTE cdb8, BYTE cdb9)
{
	if (m_cd.cur == 0xFF)
		return 0;
	if (!hSCSIEvent)
		hSCSIEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	SRB_ExecSCSICmd sSCSICmd = { 0 };
	ResetEvent(hSCSIEvent);
	sSCSICmd.SRB_Cmd        = SC_EXEC_SCSI_CMD;
	sSCSICmd.SRB_HaId       = m_cd.cd[m_cd.cur].ha;
	sSCSICmd.SRB_Target     = m_cd.cd[m_cd.cur].tgt;
	sSCSICmd.SRB_Lun        = m_cd.cd[m_cd.cur].lun;
	sSCSICmd.SRB_Flags      = SRB_DIR_IN | SRB_EVENT_NOTIFY;
	sSCSICmd.SRB_BufLen     = iDataSize;
	sSCSICmd.SRB_BufPointer = (unsigned char *)pData;
	sSCSICmd.SRB_PostProc   = hSCSIEvent;
	sSCSICmd.SRB_CDBLen     = 10;
	sSCSICmd.SRB_SenseLen   = SENSE_LEN;
	sSCSICmd.CDBByte[0] = cdb0;
	sSCSICmd.CDBByte[1] = cdb1;
	sSCSICmd.CDBByte[2] = cdb2;
	sSCSICmd.CDBByte[3] = cdb3;
	sSCSICmd.CDBByte[4] = cdb4;
	sSCSICmd.CDBByte[5] = cdb5;
	sSCSICmd.CDBByte[6] = cdb6;
	sSCSICmd.CDBByte[7] = cdb7;
	sSCSICmd.CDBByte[8] = cdb8;
	sSCSICmd.CDBByte[9] = cdb9;
	// Reset error info
	m_scError.bError = false;
	m_scError.ScsiStatus = 0;
	m_scError.ASC  = 0;
	m_scError.ASCQ = 0;
	DWORD dwStatus = p_mfnSendCommand(&sSCSICmd);
	if (dwStatus == SS_PENDING)
       WaitForSingleObject(hSCSIEvent, INFINITE);
	SENSE_ERROR_BUFFER_FORMAT* pSenseBuff = (SENSE_ERROR_BUFFER_FORMAT*)sSCSICmd.SenseArea;
	m_scError.ScsiStatus = sSCSICmd.SRB_TargStat;
	m_scError.bError = SS_COMP != sSCSICmd.SRB_Status;
	if (pSenseBuff->error_code != 0)
	{
		m_scError.ASC  = pSenseBuff->ASC;
		m_scError.ASCQ = pSenseBuff->ASCQ;
	}
	return SS_COMP != sSCSICmd.SRB_Status ? 0 : 1;
}

bool CSCSIReader98::isServiceAvailable()
{	
	return h_wnaspi_Dll != NULL;
}