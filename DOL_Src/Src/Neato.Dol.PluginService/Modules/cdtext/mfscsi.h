#ifndef __MF_SCSI_BASE_HEADER_
#define __MF_SCSI_BASE_HEADER_

#include <ntddscsi.h>
#include "..\\..\\common\\playlist.h"
#pragma warning(default: 4192) 

#define	SCSI_CHECK_CONDITION	0x02

#pragma pack(1)

struct SCDROMFEATUREHEADER
{
  BYTE length[4];
  BYTE reseved[2];
  BYTE profile[2];
};

struct __declspec(align(1)) SCDREADFEATURE_001EH
{
  BYTE fchCode[2];

  BYTE current         : 1; // bit 0
  BYTE persistent      : 1; // bit 1
  BYTE version         : 4; // bit 2..5
  BYTE reserved1       : 2; // bit 6..7 

  BYTE addlength;

  BYTE cd_text         : 1; // bit 0
  BYTE C2              : 1; // bit 1
  BYTE reserved2       : 5; // bit 2..6
  BYTE DAP             : 1; // bit 7 

  BYTE reserved3;
  BYTE reserved4;
  BYTE reserved5;
};

struct SENSE_ERROR_BUFFER_FORMAT
{
  BYTE  error_code      : 7;
  BYTE  valid           : 1;

  BYTE  segment_num;

  BYTE  sense_key       : 4;
  BYTE  reserved1       : 1;
  BYTE  ILI             : 1;
  BYTE  reserved2       : 2;

  DWORD info;
  BYTE  add_sense_len;
  DWORD command_info;
  BYTE  ASC;
  BYTE  ASCQ;
  BYTE  FRUC;

  BYTE  SKS1            : 6;
  BYTE  SKSV            : 1;
  WORD  SKS2;

  BYTE add_bytes[14];  // \/ - SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER.ucSenseBuf ( filling )
};

struct INNER_SCSI_CLASS_ERROR
{
  bool  bError;
  UCHAR ScsiStatus;
  BYTE  ASC;
  BYTE  ASCQ;
};

#pragma pack()

typedef struct
{
   SCSI_PASS_THROUGH_DIRECT spt;
   ULONG Filler;
   UCHAR ucSenseBuf[32];
} SCSI_PASS_THROUGH_DIRECT_WITH_BUFFER;

DWORD IsCDROMSupport();

class CSCSIReader
{
private:

	DWORD dCDROMDrives;	

protected:

	virtual bool InitDrive() = 0;
	virtual void CloseDrive() = 0;
	virtual long ExecSCSICommand(LPVOID pData,
		                         int    iDataSize,
								 BYTE cdb0, BYTE cdb1, BYTE cdb2, BYTE cdb3,
								 BYTE cdb4, BYTE cdb5, BYTE cdb6, BYTE cdb7,
								 BYTE cdb8, BYTE cdb9) = 0;

    CSCSIReader();

	DWORD dWorkCDROMDrive;  // One based driver num (ex: 1 = A:, 2 = B:)
	INNER_SCSI_CLASS_ERROR m_scError;

public:
	virtual ~CSCSIReader(){};

	virtual bool setWorkDrive(DWORD);
	virtual bool canCDROMReadCDText();
	virtual bool isServiceAvailable() = 0;

	bool isCDROMAvailable();
	bool isCDROMwithCDTextReadAvailable();
	bool ReadPlayList(/*MFO::IMFPlayList* pPLvoid*/CPlayList &mPlayList);
};

#endif