
//#ifdef CDTEXT_EXPORTS
#define CDTEXT_API /*CALLBACK*/
//#else
//#define CDTEXT_API extern "C" 
//#endif
#pragma warning(disable:4099) 
#pragma comment(lib, "comsupp.lib")
#pragma comment(lib, "kernel32.lib")
#include <winioctl.h>
#include <windows.h>
#include <comutil.h>
#include <msxml.h>

#include <ntddscsi.h>
#include <ntddcdrm.h>
#include "mf98scsi.h"
#include "mfNTscsi.h"
#include "CDTextFormat.h"
/*
extern "C" __declspec(dllexport)
BOOL /*CALLBACK  Init(BOOL bInit);
extern "C" __declspec(dllexport)
BOOL /*CALLBACK  Invoke(BSTR bstrRequest, BSTR * pbstrResponse);
extern "C" __declspec(dllexport)
BOOL /*CALLBACK GetInfo(BSTR * pbstrInfo);*/

BOOL GetCDDrives(MSXML2::IXMLDOMElement *xmlRoot);
BOOL GetCDPlayList(MSXML2::IXMLDOMElement *xmlRoot,BSTR bstrDrive);
//DWORD IsCDROMSupport();

HANDLE hMod;
