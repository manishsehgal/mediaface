#ifndef _CD_TEXT_FORMAT_HEADER_F_
#define _CD_TEXT_FORMAT_HEADER_F_

#pragma pack(1)

struct SCDTEXTDATA
{
  BYTE  pack_type;
    
  BYTE  track_id         : 7;
  BYTE  EF               : 1;   // Extended data indicator 
    
  BYTE  sequence;
    
  BYTE  char_pos         : 4;
  BYTE  block_num        : 3;
  BYTE  DBCC             : 1;   // Double byte char indicator

  BYTE  text_data[12];
  BYTE  crc[2];
};

struct SCDTEXTDESCRIPTOR
{
  BYTE  length[2];
  BYTE  res1;
  BYTE  res2;
  SCDTEXTDATA cdtextdata;
};

#pragma pack()

/* pack_type values */

#define	CDTEXTPACK_TITLE		0x80	// Album name and Track titles
#define	CDTEXTPACK_PERFORMER	0x81	// Singer/player/conductor/orchestra
#define	CDTEXTPACK_SONGWRITER	0x82	// Name of the songwriter
#define	CDTEXTPACK_COMPOSER		0x83	// Name of the composer
#define	CDTEXTPACK_ARRANGER		0x84	// Name of the arranger
#define	CDTEXTPACK_MESSAGE		0x85	// Message from content provider or artist
#define	CDTEXTPACK_DISK_ID		0x86	// Disk identification information
#define	CDTEXTPACK_GENRE		0x87	// Genre identification / information
#define	CDTEXTPACK_TOC			0x88	// TOC information
#define	CDTEXTPACK_TOC2			0x89	// Second TOC
#define	CDTEXTPACK_RES_8A		0x8A	// Reserved 8A
#define	CDTEXTPACK_RES_8B		0x8B	// Reserved 8B
#define	CDTEXTPACK_RES_8C		0x8C	// Reserved 8C
#define	CDTEXTPACK_CLOSED_INFO	0x8D	// For internal use by content provider
#define	CDTEXTPACK_ISRC			0x8E	// UPC/EAN code of album and ISRC for tracks
#define	CDTEXTPACK_SIZE			0x8F	// Size information of the block


#endif