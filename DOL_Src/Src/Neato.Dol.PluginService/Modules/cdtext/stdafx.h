// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#pragma warning(disable:4099) 
#import <msxml3.dll> raw_interfaces_only

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

// Windows Header Files:
#include <windows.h>
#include <atlbase.h>
#include <stdlib.h>
#include <winioctl.h>
#include <msxml.h>
#include "xmlhelpers.h"
#include "resource.h"


//#define DOUBLECHARS(X) 
// TODO: reference additional headers your program requires here
