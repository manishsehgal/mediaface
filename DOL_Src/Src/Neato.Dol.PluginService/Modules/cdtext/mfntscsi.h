#ifndef __MF_NT_SCSI_H_
#define __MF_NT_SCSI_H_

#include "mfscsi.h"

class CSCSIReaderNT : public CSCSIReader
{
private:

	HANDLE hDriveFile;

protected:

	bool InitDrive();
	void CloseDrive();
	long ExecSCSICommand(LPVOID pData,
	                     int    iDataSize,
						 BYTE cdb0, BYTE cdb1, BYTE cdb2, BYTE cdb3,
						 BYTE cdb4, BYTE cdb5, BYTE cdb6, BYTE cdb7,
						 BYTE cdb8, BYTE cdb9);

public:

    CSCSIReaderNT();
	~CSCSIReaderNT();

	bool isServiceAvailable();	
};

#endif