
#include "stdafx.h"
#include "ntddcdrm.h"  // CDROM_TOC declaration
#include "mfscsi.h"

#include "CDTextFormat.h"

DWORD IsCDROMSupport()
{
	DWORD dLogicDrives = GetLogicalDrives();
	TCHAR chDiskLetter[] = TEXT("A:");
	DWORD dTestDrives = dLogicDrives;
	dLogicDrives = 0;
	while (dTestDrives)
	{
		bool bIsDrive = dTestDrives & 1;	
		if (bIsDrive && GetDriveType(chDiskLetter) == DRIVE_CDROM)
			dLogicDrives |= 1 << (chDiskLetter[0] - TEXT('A'));    
		dTestDrives  >>= 1;
		chDiskLetter[0]++;
	}
	return dLogicDrives;
}

/*      CSCSIReader          */

CSCSIReader::CSCSIReader()
{
	dCDROMDrives = IsCDROMSupport();  // Init empty
	dWorkCDROMDrive = 0;
}

bool CSCSIReader::setWorkDrive(DWORD dDriveNum)
{
	dWorkCDROMDrive = 0;
	if ((1 << (dDriveNum - 1)) & dCDROMDrives)
	{
		dWorkCDROMDrive = dDriveNum;
		bool bDriveInit = InitDrive();
		if (!bDriveInit)
			dWorkCDROMDrive = 0;
	}
	return dWorkCDROMDrive != 0;
}

bool CSCSIReader::isCDROMAvailable()
{
	return dCDROMDrives != 0;
}

bool CSCSIReader::isCDROMwithCDTextReadAvailable()
{
	DWORD dOldWorkDrive = dWorkCDROMDrive;
	bool bCDROM = false;
	DWORD dCurDrive  = 1;
	DWORD dMaskDrive = dCDROMDrives;
	while (dMaskDrive)
	{
		if ((dMaskDrive & 1) && setWorkDrive(dCurDrive))
		{
			if (canCDROMReadCDText())
			{
				bCDROM = true;
				break;
			}
		}
		dCurDrive++;
		dMaskDrive >>= 1;
	}
	setWorkDrive(dOldWorkDrive);	
	return bCDROM;
}

bool CSCSIReader::canCDROMReadCDText()
{	
	bool bMayReadCDText = false;
	SCDROMFEATUREHEADER fchHeader = { 0 };
	long lExcResult = ExecSCSICommand(&fchHeader, sizeof(SCDROMFEATUREHEADER),
		0x46 /* GET CONFIGURATION */,
		0x02 /* RT (get feature) */, 0,
		0x1E /* Feature Number */, 0, 0, 0,
		BYTE(sizeof(SCDROMFEATUREHEADER) >> 8),
		BYTE(sizeof(SCDROMFEATUREHEADER)), 0);
	if (lExcResult)
	{
		int iFeatureLength = (fchHeader.length[0] << 24) +
			(fchHeader.length[1] << 16) +
			(fchHeader.length[2] << 8)  +
			fchHeader.length[3] + sizeof(SCDROMFEATUREHEADER);
		char* tmp = (char*)calloc(iFeatureLength, 1);
		lExcResult = ExecSCSICommand(tmp, iFeatureLength,
			0x46 /* GET CONFIGURATION */,
			0x02 /* RT (get feature) */, 0,
			0x1E /* Feature Number */, 0, 0, 0,
			BYTE(iFeatureLength >> 8),
			BYTE(iFeatureLength), 0);
		if (lExcResult)
		{
			SCDREADFEATURE_001EH* cdReadFch = (SCDREADFEATURE_001EH*)(tmp + sizeof(SCDROMFEATUREHEADER));
			bMayReadCDText = cdReadFch->fchCode[1] == 0x1E && cdReadFch->cd_text;
		}
		free(tmp);
	} else if (m_scError.ScsiStatus == SCSI_CHECK_CONDITION &&
		m_scError.ASC == 0x20 && m_scError.ASCQ == 0)
		bMayReadCDText = true; // !! Command not suported but reeding Q-chanell may be.
	return bMayReadCDText;
}

int checkStrEnd(unsigned char* pszTxt, int maxlength)
{	
	for (unsigned char* pszTmp = pszTxt; pszTmp < pszTxt + maxlength; pszTmp++)
		if (!(*pszTmp))
			return pszTmp - pszTxt + 1;
	return 0;			
}


/*BOOL GetTrackByNum(int iTrackNum, TTRACK &track)
{
//CComPtr< MFO::IMFTrack > pTrack = NULL;
bool bTrackFind  = false;
int iTracksCount = pTracks->Count;
for (int i = 0; i < iTracksCount; i++)
{
//pTracks->get_Item(CComVariant(i), &pTrack);
if (pTrack.p != NULL && pTrack->Number == iTrackNum)
{
bTrackFind = true;
break;
} else
pTrack = NULL;
}
if (!bTrackFind)
{
pTracks->raw_Add(&pTrack);
pTrack->put_Number(iTrackNum);
}
return pTrack;
}*/

bool CSCSIReader::ReadPlayList(/*MFO::IMFPlayList* pPL*//*void*/CPlayList &mPlayList)
{	
	SCDTEXTDESCRIPTOR cdDescrptr;

	long lExcResult = ExecSCSICommand(&cdDescrptr, sizeof(SCDTEXTDESCRIPTOR),
		0x43 /* SCSI_READ_TOC */, 0x02 /* MSF mode */,
		0x05 /* READ_CDTEXT */, 0, 0, 0, 0,
		BYTE(sizeof(SCDTEXTDESCRIPTOR) >> 8),
		BYTE(sizeof(SCDTEXTDESCRIPTOR)), 0);
	if (lExcResult)
	{
		int iCDTextLength = (cdDescrptr.length[0] << 8) + cdDescrptr.length[1] + sizeof(SCDTEXTDESCRIPTOR) - sizeof(SCDTEXTDATA);
		if (iCDTextLength < sizeof(SCDTEXTDESCRIPTOR))
			return false; /* No CD-Text */
		char* tmp = (char*)calloc(iCDTextLength, 1);
		lExcResult = ExecSCSICommand(tmp, iCDTextLength,
			0x43 /* SCSI_READ_TOC */, 0x02 /* MSF mode */,
			0x05 /* READ_CDTEXT */, 0, 0, 0, 0,
			BYTE(iCDTextLength >> 8),
			BYTE(iCDTextLength), 0);
		if (lExcResult)
		{
			SCDTEXTDATA* cdTextData = &((SCDTEXTDESCRIPTOR*)tmp)->cdtextdata;
			//	CComPtr< MFO::IMFTracks > pTracks = NULL;
			//			pPL->get_Tracks(&pTracks);
			int iStartOffs = 0;
			int iLastTrack = -1;
			int iLastPack  = 0; 
			int iAllocSize = 0;
			unsigned char* pszBuf = NULL;
			unsigned char* pszStartBuf = NULL;
			while (((char*)cdTextData) < ((char*)(tmp + iCDTextLength)))
			{
				TRACK track;			
				switch (cdTextData->pack_type)
				{
				case CDTEXTPACK_TITLE:
				case CDTEXTPACK_PERFORMER: {
					int iPosOfEnd = checkStrEnd(cdTextData->text_data + iStartOffs, 12 - iStartOffs);
					if (iPosOfEnd == 0)
					{
						if (!iAllocSize)
						{
							iAllocSize = 24;                     // Two text block size
							pszBuf = (unsigned char*)calloc(iAllocSize, 1);
							pszStartBuf = pszBuf;
						} else
						{
							iAllocSize += 12;
							UINT iOffsPos = pszStartBuf - pszBuf;
							pszBuf = (unsigned char*)realloc(pszBuf, iAllocSize);
							pszStartBuf = pszBuf + iOffsPos;
							memset(pszStartBuf, 0, iAllocSize - iOffsPos);
						}
						UINT iWriteSize;
						if (iStartOffs)
							memmove(pszStartBuf, cdTextData->text_data + iStartOffs, iWriteSize = 12 - iStartOffs);
						else
							memmove(pszStartBuf, cdTextData->text_data, iWriteSize = 12);
						pszStartBuf += iWriteSize;
						cdTextData++;
						iStartOffs = 0;
						continue;
					} else if (iAllocSize)
					{
						memmove(pszStartBuf, cdTextData->text_data, iPosOfEnd);
					} else
						pszBuf = cdTextData->text_data + iStartOffs;
					iStartOffs += iPosOfEnd;
					if (!iAllocSize && iPosOfEnd == 1)   // if empty string
						continue;				
					if (iLastTrack == -1)
						iLastTrack = cdTextData->track_id;
					else if (iLastPack == cdTextData->pack_type && iLastTrack == cdTextData->track_id)
						iLastTrack++;
					else
						iLastTrack = cdTextData->track_id;
					iLastPack = cdTextData->pack_type;
					LPTRACK pTrack=NULL;
					if(iLastTrack)
						pTrack=mPlayList.GetTrackByNum(iLastTrack-1);
					else 
						pTrack=NULL;
					switch (cdTextData->pack_type)
					{

					case CDTEXTPACK_TITLE:	{	
						//CComBSTR bstrTitle();
						//

						_bstr_t bstrTitle((char*)pszBuf);

						//mPlayList.AddTrack(track);

						if (iLastTrack){
							//{
							////	CComPtr< MFO::IMFTrack > pTrack = GetTrackByNum(pTracks, iLastTrack);

							//	pTrack->put_Title(bstrTitle);
							if(!pTrack)
							{
								track.m_bstrTitle=bstrTitle;
								mPlayList.AddTrack(track);
							}else
							{
								pTrack->m_bstrTitle=bstrTitle;
							}
						} 
						else
						{
							mPlayList.m_bstrAlbum=bstrTitle;	
						}
						//	pPL->put_Album(bstrTitle);
						break;
											}
					case CDTEXTPACK_PERFORMER: {
						//CComBSTR bstrArtist((char*)pszBuf);
						_bstr_t bstrArtist((char*)pszBuf);
						if (iLastTrack)
						{
							//		CComPtr< MFO::IMFTrack > pTrack = GetTrackByNum(pTracks, iLastTrack);
							//		pTrack->put_Artist(bstrArtist);
							//	pTrack.m_bstrTitle=bstrArtist;
							if(!pTrack)
							{
								track.m_bstrArtist=bstrArtist;
								mPlayList.AddTrack(track);
							}else
							{
								pTrack->m_bstrArtist=bstrArtist;
							}
							//track.m_bstrArtist=bstrArtist;
						} else
							//	pPL->put_Artist(bstrArtist);
							mPlayList.m_bstrArtist=bstrArtist;
						break;
											   }
					}

					break;
										   }
				default: {
					cdTextData++;
						 }
				}
				if (iAllocSize)
				{
					iAllocSize = 0;
					free(pszBuf);
				}
			}
		}
		free(tmp);
		if (lExcResult)
		{
			CDROM_TOC sTOCData = { 0 };			
			lExcResult = ExecSCSICommand(&sTOCData, sizeof(CDROM_TOC),
				0x43 /* SCSI_READ_TOC */, 0x02 /* MSF mode */,
				0, 0, 0, 0, 0,
				BYTE(sizeof(CDROM_TOC) >> 8),
				BYTE(sizeof(CDROM_TOC)), 0);
			if (lExcResult)
			{				
				//CComPtr< MFO::IMFTracks > pTracks = NULL;
				//pPL->get_Tracks(&pTracks);

				for (int i = sTOCData.FirstTrack; i < sTOCData.LastTrack + 1; i++)
				{
					int iSeconds = (sTOCData.TrackData[i].Address[1] - sTOCData.TrackData[i - 1].Address[1]) * 60 +
						(sTOCData.TrackData[i].Address[2] - sTOCData.TrackData[i - 1].Address[2]) - 
						((sTOCData.TrackData[i].Address[3] - sTOCData.TrackData[i - 1].Address[3]) < 0 ? 1 : 0);
					
					LPTRACK pTrack=mPlayList.GetTrackByNum(i-1);
					if(pTrack)
					{	
						char temptime[16];
						char chMinutes[8],chSeconds[8];
					
						int nMinutes,nSeconds;
						nSeconds=iSeconds%60;
						nMinutes=(iSeconds-nSeconds)/60;
						_bstr_t bstrDur;
						sprintf(chMinutes,((nMinutes<10&&nMinutes>-10)?"0%1i":"%2i"),nMinutes);
						sprintf(chSeconds,((nSeconds<10&&nSeconds>-10)?"0%1i":"%2i"),nSeconds);
						//sprintf(temptime,"%s%s%s",((nMinutes<10||nMinutes>-10)?"0%1i":"%2i") TIME_DELIMITER_STR ((nSeconds<10||nSeconds>-10)?"0%1i":"%2i"),nMinutes,nSeconds); 
						sprintf(temptime,"%s%s%s",chMinutes,TIME_DELIMITER_STR,chSeconds);
						pTrack->m_bstrDuration=temptime;

					}
					//	CComPtr< MFO::IMFTrack > pTrack = GetTrackByNum(pTracks, i);					
					//	pTrack->put_DurationTime(CComVariant(iSeconds));
				}
			}
		}
	}
	return lExcResult != 0;
}