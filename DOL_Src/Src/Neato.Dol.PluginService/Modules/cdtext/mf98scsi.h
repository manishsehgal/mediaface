#ifndef __MF_WINDOWS98_SCSI_HEADER_
#define __MF_WINDOWS98_SCSI_HEADER_

#include <windows.h>
#include "mfscsi.h"

/* From wnaspi32.h prototipe of functions */

#define SENSE_LEN         32

#define SS_COMP           0x01
#define SS_PENDING        0x00
#define SS_NO_ADAPTERS    0xE8 

#define SC_HA_INQUIRY     0x00
#define SC_GET_DEV_TYPE   0x01
#define SC_EXEC_SCSI_CMD  0x02
#define SC_GET_DISK_INFO  0x06  // Only Win98

#define SRB_DIR_IN        0x08
#define SRB_EVENT_NOTIFY  0x40

#define DTYPE_CDROM       0x05

#define DISK_INT13_AND_DOS 0x01
#define DISK_INT13         0x02

#pragma pack(1)

typedef struct
{
    BYTE        SRB_Cmd;
    BYTE        SRB_Status;
    BYTE        SRB_HaId;
    BYTE        SRB_Flags;
    DWORD       SRB_Hdr_Rsvd;
    BYTE        HA_Count;
    BYTE        HA_SCSI_ID;
    BYTE        HA_ManagerId[16];
    BYTE        HA_Identifier[16];
    BYTE        HA_Unique[16];
    WORD        HA_Rsvd1;
} SRB_HAInquiry;

typedef struct
{
    BYTE        SRB_Cmd;
    BYTE        SRB_Status;
    BYTE        SRB_HaId;
    BYTE        SRB_Flags;
    DWORD       SRB_Hdr_Rsvd;
    BYTE        SRB_Target;
    BYTE        SRB_Lun;
    BYTE        SRB_DeviceType;
    BYTE        SRB_Rsvd1;
} SRB_GDEVBlock;

typedef struct                       
{                                               
    BYTE        SRB_Cmd;                        
    BYTE        SRB_Status;                     
    BYTE        SRB_HaId;                       
    BYTE        SRB_Flags;                      
    DWORD       SRB_Hdr_Rsvd;                   
    BYTE        SRB_Target;                     
    BYTE        SRB_Lun;                        
    WORD        SRB_Rsvd1;                      
    DWORD       SRB_BufLen;                     
    BYTE        FAR *SRB_BufPointer;            
    BYTE        SRB_SenseLen;                   
    BYTE        SRB_CDBLen;                     
    BYTE        SRB_HaStat;                     
    BYTE        SRB_TargStat;                   
    VOID        FAR *SRB_PostProc;              
    BYTE        SRB_Rsvd2[20];                  
    BYTE        CDBByte[16];                    
    BYTE        SenseArea[SENSE_LEN + 2];         
} SRB_ExecSCSICmd;

typedef struct
{
    BYTE        SRB_Cmd;
    BYTE        SRB_Status;
    BYTE        SRB_HaId;
    BYTE        SRB_Flags;
    DWORD       SRB_Hdr_Rsvd;
    BYTE        SRB_Target;
    BYTE        SRB_Lun;
    BYTE        SRB_DriveFlags;
    BYTE        SRB_Int13HDriveInfo;
    BYTE        SRB_Heads;
    BYTE        SRB_Sectors;
    BYTE        SRB_Rsvd1[10];
} SRB_GetDiskInfo;

#pragma pack() 

typedef void *LPSRB;

typedef DWORD (*FNGetASPI32SupportInfo)(void);
typedef DWORD (*FNSendASPI32Command)(LPSRB);

/* ------------------------- */

#define MAXCDCOUNT 33

typedef struct
{
	BYTE ha;
	BYTE tgt;
	BYTE lun;
	BYTE ltr;
} CDINFO;

typedef struct
{
	BYTE   max;
	BYTE   cur;
	CDINFO cd[MAXCDCOUNT];
} CDLIST;

class CSCSIReader98 : public CSCSIReader
{
private:

	HINSTANCE h_wnaspi_Dll;
	HANDLE    hSCSIEvent;
	FNGetASPI32SupportInfo p_mfnGetInfo;
	FNSendASPI32Command    p_mfnSendCommand;

	CDLIST m_cd;

	bool GetCDList();
	unsigned char GetRegisterSCSIDriveLetter(BYTE btTargetID, BYTE btLunID, char* pProductID);

protected:

	bool InitDrive();
	void CloseDrive();
	long ExecSCSICommand(LPVOID pData,
	                     int    iDataSize,
						 BYTE cdb0, BYTE cdb1, BYTE cdb2, BYTE cdb3,
						 BYTE cdb4, BYTE cdb5, BYTE cdb6, BYTE cdb7,
						 BYTE cdb8, BYTE cdb9);

public:

    CSCSIReader98();
	~CSCSIReader98();

	bool isServiceAvailable();
};

#endif