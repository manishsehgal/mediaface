#pragma once

#include <PlayListEx.h>

class CPlayListIT : public CPlayListEx
{
public:
    CPlayListIT() : CPlayListEx()
    {
        m_strSrcType = L"FileList";
    }
};


class CiTunesHelper
{
public:
	BOOL GetCategories(BSTR bstrRequest, BSTR * pbstrResponse);
	BOOL GetPlayList(BSTR bstrRequest, BSTR * pbstrResponse);

private:
    bool checkiTunesIsRunning();
    int  getCategories(vector<wstring> & vCategories);
    int  fillPlayList(CPlayListIT & playList, LPCWSTR wcName);
};