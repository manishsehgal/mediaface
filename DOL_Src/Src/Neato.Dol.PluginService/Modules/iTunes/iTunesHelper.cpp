#include "stdafx.h"
#include "iTunesHelper.h"
#include "iTunesCOMInterface.h"
#include "iTunesCOMInterface_i.c"


BOOL CiTunesHelper::GetCategories(BSTR bstrRequest, BSTR * pbstrResponse)
{
	BOOL    bRes = TRUE;
    HRESULT hRes = 0;
	try
	{
		//=============================
		// Prepare response xml
		CComPtr<MSXML2::IXMLDOMElement>  pRespElem;
		hRes = CXmlHelpers::CreateDocument(CComBSTR(L"Response"), &pRespElem);
        if (hRes != S_OK) throw 1;

        hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"0");
        if (hRes != S_OK) throw 2;

        CComPtr<MSXML2::IXMLDOMElement> pRParamsElem;
		hRes = CXmlHelpers::AddElement(pRespElem, CComBSTR(L"Params"), &pRParamsElem);
        if (hRes != S_OK) throw 3;

		//=============================
        vector<wstring> vCategories;
		int nRes = getCategories(vCategories);

		if (nRes == 0)
		{
            CComPtr<MSXML2::IXMLDOMElement> pCatsElem;
		    hRes = CXmlHelpers::AddElement(pRParamsElem, CComBSTR(L"Categories"), &pCatsElem);
			if (hRes != S_OK) throw 4;

            for (int i=0; i<(int)vCategories.size(); i++)
            {
                CComPtr<MSXML2::IXMLDOMElement> pCatElem;
	    	    hRes = CXmlHelpers::AddElement(pCatsElem, CComBSTR(L"Category"), &pCatElem);
                if (!pCatElem) continue;
        		hRes = CXmlHelpers::SetAttribute(pCatElem, CComBSTR(L"Name"), vCategories[i].c_str());
            }
		}
		else if (nRes == 1)
		{
            hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"1");
			if (hRes != S_OK) throw 5;
            hRes = CXmlHelpers::SetAttribute(pRespElem, L"ErrorDesc", L"To import an iTunes playlist, the iTunes application has to be running first.");
			if (hRes != S_OK) throw 6;
		}
        else
        {
            throw 7;
        }

		hRes = pRespElem->get_xml(pbstrResponse);
		if (hRes != S_OK) throw 8;
	}
    catch (int)
    {
		bRes = FALSE;
    }
    catch (...){bRes = FALSE;}

	return bRes;
}

bool CiTunesHelper::checkiTunesIsRunning()
{
	HWND hWnd = ::FindWindow(_T("iTunes"), 0);
	return (hWnd != 0);
}

int CiTunesHelper::getCategories(vector<wstring> & vCategories)
{
    if (!checkiTunesIsRunning()) return 1;

	vCategories.clear();

	CComPtr<IiTunes> pApp;
	HRESULT hRes = pApp.CoCreateInstance(CLSID_iTunesApp);
	if (!pApp) return 0;

	CComPtr<IITSourceCollection> pSrcColl;
	pApp->get_Sources(&pSrcColl);
	if (!pSrcColl) return 0;

	long nSrcCount = 0;
	pSrcColl->get_Count(&nSrcCount);
	for (long s=1; s<=nSrcCount; s++)
	{
		CComPtr<IITSource> pSrc;
		pSrcColl->get_Item(s, &pSrc);
		if (!pSrc) continue;

		CComPtr<IITPlaylistCollection> pPLColl;
		pSrc->get_Playlists(&pPLColl);
		if (!pPLColl) continue;

		long nPLCount = 0;
		pPLColl->get_Count(&nPLCount);
		for (long p=1; p<=nPLCount; p++)
		{
			CComPtr<IITPlaylist> pPL;
			pPLColl->get_Item(p, &pPL);
			if (!pPL) continue;

            CComBSTR bstrName;
			pPL->get_Name(&bstrName);

			vCategories.push_back(wstring(bstrName));
		}
	}
    return 0;
}

BOOL CiTunesHelper::GetPlayList(BSTR bstrRequest, BSTR * pbstrResponse)
{
	BOOL bRes = TRUE;

	try
	{
		//=============================
		//Parse request xml
		CComPtr<MSXML2::IXMLDOMElement>  pReqElem;
		HRESULT hRes = CXmlHelpers::LoadXml(bstrRequest, &pReqElem);
		if (hRes != S_OK) throw 1;

        CComPtr<MSXML2::IXMLDOMElement> pParamsElem;
        hRes = CXmlHelpers::GetChildElement(pReqElem, L"Params", &pParamsElem);
        if (hRes != S_OK) throw 2;

        CComPtr<MSXML2::IXMLDOMElement> pCatElem;
        hRes = CXmlHelpers::GetChildElement(pParamsElem, L"Category", &pCatElem);
        if (hRes != S_OK) throw 3;
        CComBSTR bstrCatName;
        hRes = CXmlHelpers::GetAttribute(pCatElem, L"CatId", &bstrCatName);
        if (hRes != S_OK) throw 4;

		//=============================
		// Prepare response xml
		CComPtr<MSXML2::IXMLDOMElement>  pRespElem;
		hRes = CXmlHelpers::CreateDocument(CComBSTR(L"Response"), &pRespElem);
        if (hRes != S_OK) throw 5;

        hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"0");
        if (hRes != S_OK) throw 6;

        CComPtr<MSXML2::IXMLDOMElement> pRParamsElem;
		hRes = CXmlHelpers::AddElement(pRespElem, CComBSTR(L"Params"), &pRParamsElem);
        if (hRes != S_OK) throw 7;

		//=============================

        CPlayListIT playList;
        int nRes = fillPlayList(playList, bstrCatName);

		if (nRes == 0)
		{
		    CComBSTR bstrPLXml;
            playList.GetXml(&bstrPLXml);

		    CComPtr<MSXML2::IXMLDOMElement> pPLElem;
		    hRes = CXmlHelpers::LoadXml(bstrPLXml, &pPLElem);
		    if (hRes != S_OK) throw 8;

		    CComPtr<MSXML2::IXMLDOMNode> pNewPLNode;
		    pRParamsElem->appendChild(pPLElem, &pNewPLNode);
        }
		else if (nRes == 1)
		{
            hRes = CXmlHelpers::SetAttribute(pRespElem, L"Error", L"1");
			if (hRes != S_OK) throw 9;
            hRes = CXmlHelpers::SetAttribute(pRespElem, L"ErrorDesc", L"To import an iTunes playlist, the iTunes application has to be running first.");
			if (hRes != S_OK) throw 10;
		}
        else
        {
            throw 11;
        }

        hRes = pRespElem->get_xml(pbstrResponse);
	    if (hRes != S_OK) throw 12;
    }
    catch (int)
    {
		bRes = FALSE;
    }
    catch (...){bRes = FALSE;}

	return bRes;
}

int CiTunesHelper::fillPlayList(CPlayListIT & playList, LPCWSTR wcName)
{
    if (!checkiTunesIsRunning()) return 1;

	CComPtr<IiTunes> pApp;
	HRESULT hRes = pApp.CoCreateInstance(CLSID_iTunesApp);
	if (!pApp) return 0;

	CComPtr<IITSourceCollection> pSrcColl;
	pApp->get_Sources(&pSrcColl);
	if (!pSrcColl) return 0;

	long nSrcCount = 0;
	pSrcColl->get_Count(&nSrcCount);
	for (long s=1; s<=nSrcCount; s++)
	{
		CComPtr<IITSource> pSrc;
		pSrcColl->get_Item(s, &pSrc);
		if (!pSrc) continue;

		CComPtr<IITPlaylistCollection> pPLColl;
		pSrc->get_Playlists(&pPLColl);
		if (!pPLColl) continue;

		long nPLCount = 0;
		pPLColl->get_Count(&nPLCount);
		for (long p=1; p<=nPLCount; p++)
		{
			CComPtr<IITPlaylist> pPL;
			pPLColl->get_Item(p, &pPL);
			if (!pPL) continue;

			CComPtr<IITTrackCollection> pTrColl;
			pPL->get_Tracks(&pTrColl);
			if (!pTrColl) continue;

            CComBSTR bstrName;
			pPL->get_Name(&bstrName);
            if (wcscmp(bstrName, wcName) == 0)
            {
    			long nTrCount = 0;
	    		pTrColl->get_Count(&nTrCount);
		    	for (long t=1; t<=nTrCount; t++)
			    {
				    CComPtr<IITTrack> pTr;
				    pTrColl->get_Item(t, &pTr);
				    if (!pTr) continue;

                    int nPos = playList.AddEmptyTrack();
                    CPlayListTrackEx & track = playList.GetTrack(nPos);

                    CComBSTR bstrAlbum;
				    pTr->get_Album(&bstrAlbum);
                    track.SetAlbum(bstrAlbum);
                    CComBSTR bstrArtist;
				    pTr->get_Artist(&bstrArtist);
                    track.SetArtist(bstrArtist);
                    CComBSTR bstrTitle;
				    pTr->get_Name(&bstrTitle);
                    track.SetTitle(bstrTitle);
                    long lTime = 0;
				    pTr->get_Duration(&lTime);
                    track.SetDuration(lTime);

				    CComQIPtr<IITFileOrCDTrack> pFileTr = pTr;
				    if (pFileTr)
				    {
                        CComBSTR bstrFile;
					    pFileTr->get_Location(&bstrFile);
                        track.SetUrl(bstrFile);
				    }
                }
                break;
            }
		}
	}
    return 0;
}



