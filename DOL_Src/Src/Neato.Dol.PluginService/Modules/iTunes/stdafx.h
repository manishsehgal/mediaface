#pragma once

#include <windows.h>
#include <atlbase.h>
#include <msxml.h>

#include <string>
#include <vector>
using namespace std;

typedef std::basic_string<TCHAR> tstring;

#import <msxml3.dll> raw_interfaces_only
#include <XmlHelpers.h>
