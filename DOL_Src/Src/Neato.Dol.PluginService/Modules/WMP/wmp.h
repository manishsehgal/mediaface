#include "stdafx.h"

#pragma warning(disable:4099) 
#pragma comment(lib, "comsupp.lib")
#pragma comment(lib, "kernel32.lib")
#include <comutil.h>
#include <msxml.h>

HANDLE hMod;
BOOL bIsAvaible = false;
IWMPPlayerPtr spWMPPlayer2;

BOOL GetWMPCategories(MSXML2::IXMLDOMElement *xmlRoot);
BOOL GetWMPPlayList(MSXML2::IXMLDOMElement *xmlRoot,int nCategoryID);
