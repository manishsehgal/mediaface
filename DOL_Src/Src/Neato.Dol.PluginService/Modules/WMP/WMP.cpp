#include "stdafx.h"
#include "wmp.h"

#define MSGBOX(msg) MessageBox(NULL,msg,NULL,NULL);


BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
    hMod = hModule;
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

extern "C"
BOOL CALLBACK Init(BOOL bInit)
{
    if(bInit)
    {
        try
        {
            HRESULT hr;
            hr = spWMPPlayer2.CreateInstance(_T("WMPlayer.OCX"));
            if(FAILED(hr))
                return false;
             bIsAvaible=true;
        }
        catch(...){return 0;}
    }
    else
    {
        try{
            if(spWMPPlayer2!=NULL) {
                spWMPPlayer2->close();
                spWMPPlayer2.Release();
            }
            ::CoFreeUnusedLibraries();
        }catch(...){
            return FALSE;
        }

    }
    return TRUE;
}

extern "C"
BOOL CALLBACK  Invoke(BSTR bstrRequest, BSTR * pbstrResponse)
{

    _bstr_t bstrXMLHeader("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
    _bstr_t bstrXMLResponseDraft(bstrXMLHeader+"<Response Error=\"0\"></Response>");
    try
    {
        CComPtr<MSXML2::IXMLDOMElement> xmlRoot=NULL,xmlReq=NULL,xmlParams=NULL,xmlReqParam=NULL,xmlResponse=NULL;
        CXmlHelpers::LoadXml(bstrRequest,&xmlRoot);
        CXmlHelpers::LoadXml(bstrXMLResponseDraft,&xmlResponse);
        if(!xmlResponse||!xmlRoot)
        {
            *pbstrResponse=SysAllocString(L"<Response Error=\"1\" ErrorDesc=\"Cannot create response XML\"><Params/></Response>");
            return TRUE;
        }

        try {
            IWMPMediaCollectionPtr pMediaCollection = spWMPPlayer2->GetmediaCollection();
            IWMPPlaylistCollectionPtr spWMPPlaylistCollection = spWMPPlayer2->playlistCollection;
            IWMPPlaylistArrayPtr spWMPPlaylistArray= spWMPPlaylistCollection->getAll();
        } catch (_com_error & e) {
            if (e.Error() == E_ACCESSDENIED)
                *pbstrResponse = SysAllocString(L"<Response Error=\"1\" ErrorDesc=\"Access Denied to WMP Library.\"><Params/></Response>");
			else
                *pbstrResponse = SysAllocString(L"<Response Error=\"1\" ErrorDesc=\"Unsupported version of WMP\nor error reading WMP Library.\n\"><Params/></Response>");
            return TRUE;
        }

        BSTR bstrCommand;
        CXmlHelpers::GetAttribute(xmlRoot,L"Name",&bstrCommand);
        CXmlHelpers::GetChildElement(xmlRoot,L"Params",&xmlParams);
        if(!bstrCommand)
            return FALSE;
        BOOL bRet=FALSE;
        if(!wcscmp(bstrCommand,L"WMP_GET_CATEGORIES"))
        {
            bRet=GetWMPCategories(xmlResponse);
            if(bRet)
            {
                _bstr_t resp;
                resp+=bstrXMLHeader;
                BSTR bstrXML=NULL;
                xmlResponse->get_xml(&bstrXML);
                resp+=bstrXML;
                *pbstrResponse=resp.copy();
                SysFreeString(bstrXML);
            }
            else return FALSE;
        }
        else if(!wcscmp(bstrCommand,L"WMP_GET_PLAYLIST"))
        {

            CXmlHelpers::GetChildElement(xmlParams,L"Category",&xmlReqParam);
            BSTR bstrCatID;
            int nCatID=-1;
            if(S_OK!=CXmlHelpers::GetAttribute(xmlReqParam,L"CatId",&bstrCatID))
            {
                bRet=TRUE;
                CXmlHelpers::SetAttribute(xmlReqParam,L"Error",L"1");
                CXmlHelpers::SetAttribute(xmlReqParam,L"ErrorDesc",L"Error id parameter");

            }
            else
            {
                nCatID=atoi(_bstr_t(bstrCatID));
                if(nCatID<0)
                {
                    bRet=TRUE;
                    CXmlHelpers::SetAttribute(xmlReqParam,L"Error",L"1");
                    CXmlHelpers::SetAttribute(xmlReqParam,L"ErrorDesc",L"Error id parameter");
                }
                else
                    bRet=GetWMPPlayList(xmlResponse,nCatID);
            }
            if(bRet)
            {
                _bstr_t resp;
                resp+=bstrXMLHeader;
                BSTR bstrXML;
                xmlResponse->get_xml(&bstrXML);
                resp+=bstrXML;
                *pbstrResponse=resp.copy();
                SysFreeString(bstrXML);

            }
            else return FALSE;
            SysFreeString(bstrCatID);


        }

        SysFreeString(bstrCommand);
        return bRet;
    }catch(...)
    {return FALSE;}

}

extern "C"
BOOL CALLBACK  GetInfo(BSTR * pbstrInfo)
{
    try
    {
        HGLOBAL pchVersionInfo=LoadResource(HMODULE(hMod),FindResource(HMODULE(hMod),MAKEINTRESOURCE(IDX_VERINFOXML),TEXT("XML")));
        if(!pchVersionInfo)
            return FALSE;

        *pbstrInfo=_com_util::ConvertStringToBSTR((char*)pchVersionInfo);
    }
    catch(...) { return FALSE; }

    return TRUE;
}

BOOL GetWMPCategories(MSXML2::IXMLDOMElement *xmlRoot){
    try
    {
        IWMPMediaCollectionPtr pMediaCollection = spWMPPlayer2->GetmediaCollection();
        IWMPPlaylistCollectionPtr spWMPPlaylistCollection = spWMPPlayer2->playlistCollection;
        IWMPPlaylistArrayPtr spWMPPlaylistArray= spWMPPlaylistCollection->getAll();
        long lCount = spWMPPlaylistArray->count;
        CComPtr<MSXML2::IXMLDOMElement> pCategories=NULL,pParams=NULL;
        CXmlHelpers::SetAttribute(xmlRoot,L"Error",L"0");
        CXmlHelpers::AddElement(xmlRoot,L"Params",&pParams);
        CXmlHelpers::AddElement(pParams,L"Categories",&pCategories);
        for (long i = 0; i < lCount; i++)
        {
            IWMPPlaylistPtr spWMPList = spWMPPlaylistArray->Item(i);
            _bstr_t bstrPLName = spWMPList->name;
            CComPtr<MSXML2::IXMLDOMElement>pCategory=NULL;		
            CXmlHelpers::AddElement(pCategories,L"Category",&pCategory);
            CXmlHelpers::SetAttribute(pCategory,L"Name",bstrPLName);
        }
        return TRUE;
    }
    catch(...){return FALSE;}
}

BOOL GetWMPPlayList(MSXML2::IXMLDOMElement *xmlRoot,int nCategoryID) {
    try
    {
        LPCTSTR lpszArtistAttr = _T("Artist");
        LPCTSTR lpszAlbumAttr = _T("Album");
        IWMPMediaCollectionPtr pMediaCollection = spWMPPlayer2->GetmediaCollection();
        IWMPPlaylistCollectionPtr spWMPPlaylistCollection = spWMPPlayer2->playlistCollection;
        IWMPPlaylistArrayPtr spWMPPlaylistArray= spWMPPlaylistCollection->getAll();
        if(nCategoryID < 0 || nCategoryID>= spWMPPlaylistArray->count)
            return FALSE;
        IWMPPlaylistPtr spWMPList = spWMPPlaylistArray->Item(nCategoryID);
        CComPtr<MSXML2::IXMLDOMElement> pPlayList=NULL,pTracks=NULL,pParams=NULL;
        //CXmlHelpers::AddElement(xmlRoot,L"Response",&xmlRoot);
        CXmlHelpers::SetAttribute(xmlRoot,L"Error",L"0");
        CXmlHelpers::AddElement(xmlRoot,L"Params",&pParams);
        CXmlHelpers::AddElement(pParams,L"PlayList",&pPlayList);
        //CXmlHelpers::SetAttribute(pPlayList,L"TrackCount",);
        long lPLCount = spWMPList->count;
		WCHAR buf[128];
		_itow(lPLCount, buf, 10);
		CXmlHelpers::SetAttribute(pPlayList, L"TrackCount", buf);
        CXmlHelpers::SetAttribute(pPlayList,L"Artist", spWMPList->getItemInfo(lpszArtistAttr));
        CXmlHelpers::SetAttribute(pPlayList,L"Album", spWMPList->getItemInfo(lpszAlbumAttr));
        CXmlHelpers::SetAttribute(pPlayList, L"SrcType", L"FileList");
        CXmlHelpers::SetAttribute(pPlayList, L"SrcName", L"");
        CXmlHelpers::AddElement(pPlayList,L"Tracks",&pTracks);
        for (long j = 0; j < lPLCount; j++) {
            IWMPMediaPtr spWMPMedia = spWMPList->GetItem(j);
            _bstr_t bstrAttrName = _bstr_t(_T("SourceURL"));
            _bstr_t bstrAttrValue = spWMPMedia->getItemInfo(bstrAttrName);
            IWMPPlaylistPtr pTmpList = pMediaCollection->getByAttribute(bstrAttrName, bstrAttrValue);
            long lTmpCount = pTmpList->Getcount();

            CComPtr<MSXML2::IXMLDOMElement> pTrack;
            CXmlHelpers::AddElement(pTracks,L"Track",&pTrack);
    		_itow(j+1, buf, 10);
    		CXmlHelpers::SetAttribute(pTrack, L"Id", buf);
            CXmlHelpers::SetAttribute(pTrack, L"Artist",_bstr_t((BSTR) spWMPMedia->getItemInfo(_bstr_t(lpszArtistAttr))));
            CXmlHelpers::SetAttribute(pTrack, L"Title",_bstr_t((BSTR)spWMPMedia->name));
            CXmlHelpers::SetAttribute(pTrack, L"Album",_bstr_t((BSTR) spWMPMedia->getItemInfo(_bstr_t(lpszAlbumAttr))));
            CXmlHelpers::SetAttribute(pTrack, L"Url",_bstr_t((BSTR)spWMPMedia->sourceURL));
            CXmlHelpers::SetAttribute(pTrack, L"Duration",_bstr_t((BSTR)spWMPMedia->durationString));
        }
    }
    catch(...){return FALSE;}
    return TRUE;
}
