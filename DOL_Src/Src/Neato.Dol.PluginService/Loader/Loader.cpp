#include "stdafx.h"

#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>

#include "Loader.h"

#include "resource.h"

CAppModule _Module;


int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE /*hPrevInstance*/, LPTSTR lpstrCmdLine, int nCmdShow)
{
	// this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
	::DefWindowProc(NULL, 0, 0, 0L);

	HRESULT hRes = _Module.Init(NULL, hInstance);
	ATLASSERT(SUCCEEDED(hRes));

	int nRet = 0;
	{
	    CMessageLoop theLoop;
	    _Module.AddMessageLoop(&theLoop);

	    CMainDlg dlgMain;
	    if (dlgMain.Create(NULL))
        {
            nRet = theLoop.Run();
        }

        _Module.RemoveMessageLoop();
	}

	_Module.Term();
	return nRet;
}