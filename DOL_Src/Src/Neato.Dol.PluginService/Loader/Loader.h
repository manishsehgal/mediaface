#pragma once
#include "resource.h"
#include <TxtCnv.h>

#define WM_USER_DORELOAD  (WM_USER + 1)

class CMainDlg : 
    public CDialogImpl<CMainDlg>,
	public CMessageFilter, 
    public CIdleHandler
{
public:
	enum { IDD = IDD_MAINDLG };

    CMainDlg()
    {
    }
	virtual BOOL PreTranslateMessage(MSG* pMsg)
	{
		return CWindow::IsDialogMessage(pMsg);
	}
	virtual BOOL OnIdle()
	{
		return FALSE;
	}

	BEGIN_MSG_MAP(CMainDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		//COMMAND_ID_HANDLER(IDCANCEL,        OnCancel)
		MESSAGE_HANDLER(WM_COPYDATA,       OnCopyData)
		MESSAGE_HANDLER(WM_USER_DORELOAD,       OnDoReload)
    END_MSG_MAP()

private:
    DWORD   m_dwProcessId;
    tstring m_strFileName;
    DWORD   m_dwSize;
    BYTE *  m_pData;

private:
	LRESULT OnInitDialog(UINT, WPARAM, LPARAM, BOOL&)
	{
		//ShowWindow(SW_SHOW);
		return TRUE;
    }
    /*
	LRESULT OnCancel(WORD, WORD wID, HWND, BOOL&)
	{
		DestroyWindow();
		::PostQuitMessage(IDOK);
		return 0;
	}
    */
	LRESULT OnCopyData(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
        COPYDATASTRUCT * cds = (COPYDATASTRUCT*)lParam;
        if (!cds) return 0;
        BYTE * pMem = (BYTE*)cds->lpData;
        if (!pMem) return 0;
        BYTE * p = pMem;

        memcpy(&m_dwProcessId, p, sizeof(DWORD));
        p += sizeof(DWORD);

        WCHAR wcFN[2048] = {0};
        memcpy(wcFN, p, 2048);
        m_strFileName = CTxtCnv().w2t(wcFN);
        p += 2048;

        memcpy(&m_dwSize, p, sizeof(DWORD));
        p += sizeof(DWORD);

        m_pData = new BYTE[m_dwSize];
        memcpy(m_pData, p, m_dwSize);

        PostMessage(WM_USER_DORELOAD, 0, 0);

		return 0;
	}

	LRESULT OnDoReload(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
        HANDLE hProcess = ::OpenProcess(SYNCHRONIZE, FALSE, m_dwProcessId);
        if (!hProcess) return 0;

        ::WaitForSingleObject(hProcess, 5000);
	    ::CloseHandle(hProcess);

        HANDLE hFile = ::CreateFile(m_strFileName.c_str(), GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, 0, NULL);
        if (hFile == INVALID_HANDLE_VALUE) return false;
        DWORD dwWr = 0;
        ::WriteFile(hFile, m_pData, m_dwSize, &dwWr, 0);
        ::CloseHandle(hFile);
        delete[] m_pData;

        STARTUPINFO si = {0};
	    si.cb = sizeof(si);
        PROCESS_INFORMATION pi = {0};

	    tstring cmdLine;
        cmdLine += "\"";
        cmdLine += m_strFileName;
        cmdLine += "\"";

	    if(::CreateProcess( NULL, (LPSTR)cmdLine.c_str(), NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi ) == 0)
	    {
		    return 0;
    	}
	    ::CloseHandle(pi.hThread);
        ::CloseHandle(pi.hProcess);

		DestroyWindow();
		::PostQuitMessage(IDOK);

		return 0;
    }
};