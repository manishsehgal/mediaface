using System;

using Neato.Dol.Business;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.FillImageLib {
    /// <summary>
    /// Console application for filling Image Library
    /// </summary>
    internal class MainApplication {
        private static string helpString = @"
Usage: -p ""%ImagePath"" [-f ""%FolderName""]

[] - optional parameters

-p %ImagePath: the dir path to images will be copying into database

-f %FolderName: the name of the target folder in database
(if parameter not present, a bulk of folders and images will be
created in root node)

!!! connectionString to DB locate in Neato.Dol.FillImageLib.exe.config file

Example:
1)
Neato.Dol.FillImageLib.exe -p ""C:\Temp\BulkImages"" -f ""Main""
- copy all images from ""c:\Temp\BulkImages"" to image folder ""Main""

2)
Neato.Dol.FillImageLib.exe -p ""BulkImages\Temp""
- copy all images from ""BulkImages\Temp"" to root
";

        [STAThread]
        private static void Main(string[] args) {
            try {
                if (
                    args.Length == 0 ||
                    args[0] == "/?" ||
                    args[0] == "-?" ||
                    args[0] == "?" ||
                    args[0] == "/help" ||
                    args[0] == "-help" ||
                    args[0] == "help" ||
                    args[0] == "/h" ||
                    args[0] == "-h" ||
                    args[0] == "h" ||
                    args.Length > 4) {
                    Console.Write(helpString);
                    return;
                }

                string imagePath = CommandLineParser.GetValueByKey(args, "-p");
                string folderName = CommandLineParser.GetValueByKey(args, "-f");
                bool insertIntoRoot = !CommandLineParser.IsKeyPresent(args, "-f");
                bool clearTargetFolder = false;//CommandLineParser.IsKeyPresent(args, "-c");

                Console.WriteLine(@"
imagePath = {0},
folderName = {1},
insertIntoRoot = {2}",
                    imagePath, folderName, insertIntoRoot/*, clearTargetFolder*/);

                BCImageLibrary.AddContentRecursively(imagePath,
                    folderName, insertIntoRoot, clearTargetFolder);
                Console.WriteLine("Process complete succesifully");
            } catch (Exception ex) {
                Console.WriteLine("!!! ERROR !!! Data rollback...");
                Console.WriteLine("Error message = " + ex.Message);
                Console.Write(helpString);
            }
        }
    }
}