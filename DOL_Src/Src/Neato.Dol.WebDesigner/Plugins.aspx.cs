using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Controls;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner {
	public class Plugins : BasePage2 {

        #region Url
        private const string RawUrl = "Plugins.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        new public static string PageName() {
            return RawUrl;
        }
        public static string BannerFolder {
            get { return "Plugins"; }
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
            pageTemplateUrl = SimplePageTemplate.Url().AbsolutePath;

			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Plugins_DataBinding);
		}
		#endregion

        private void Plugins_DataBinding(object sender, EventArgs e) {
        }
	}
}
