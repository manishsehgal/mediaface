using System;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner {
    public class Overview : StandardPage {
        protected Literal ctlPageContent;

        #region Url
        //private const string RawUrl = "OverView.aspx";

        //public static string PageName() {
          //  return RawUrl;
        //}

        //public static Uri Url() {
          //  return UrlHelper.BuildUrl(RawUrl);
        //}
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Overview_DataBinding);
        }
        #endregion

        #region DataBinding
        private void Overview_DataBinding(object sender, EventArgs e) {
            string culture = StorageManager.CurrentLanguage;
            LocalizedText pageContent = BCOverviewPage.Get(culture);
            if (pageContent == null)
                pageContent = new LocalizedText(culture, string.Empty);
            ctlPageContent.Text = pageContent.Text;
            //rawUrl = RawUrl;
            headerText = OverviewStrings.TextOverview();
        }
        #endregion
    }
}