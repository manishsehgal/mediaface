using System;
using System.Collections;
using System.Web;
using System.Web.UI.WebControls;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Controls;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebDesigner {
    public class MP3PlayerSelect : BasePage2 {
        protected ImageLibrary ctlMP3PlayerSelection;

        protected Label lblCategory;
        protected Label lblCategoryName;
        protected Label lblNumberOfItems;
        protected Label lblNothingFound;
        protected Label lblSearch;
        protected HyperLink hypSwitchCategory;
        protected TextBox txtSearch;
        protected ImageButton btnSearch;

        private string search {
            get {
                return 
                    (txtSearch.Text != string.Empty)
                    ? txtSearch.Text : null;
            }
        }


        private Category Category {
            get { return (Category) ViewState["Category"]; }
            set { ViewState["Category"] = value; }
        }


        #region Url

        private const string RawUrl = "MP3PlayerSelect.aspx";
        private const string CategoryKey = "Category";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri Url(string category) {
            return UrlHelper.BuildUrl(RawUrl, CategoryKey, category);
        }
        
        new public static string PageName() {
            return RawUrl;
        }

        #endregion

        private void Page_Load(object sender, EventArgs e) {
            int categoryId = Convert.ToInt32(Request.QueryString[CategoryKey]);
            Category = new Category(categoryId);
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            pageTemplateUrl = PaperSelectPageTemplate.Url().AbsolutePath;
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(MP3PlayerSelect_DataBinding);
            this.ctlMP3PlayerSelection.RedirectToUrl += new ImageLibrary.SelectItemEventHandler(ctlMP3PlayerSelection_RedirectToUrl);
            this.PreRender += new EventHandler(MP3PlayerSelect_PreRender);
            this.btnSearch.Click += new System.Web.UI.ImageClickEventHandler(btnSearch_Click);
        }

        #endregion

        private void ctlMP3PlayerSelection_RedirectToUrl(Object sender, ImageLibrarySelectItemEventArgs e) {
            StorageManager.GenerateNewProject = true;
            Response.Redirect(e.NavigateUrl);
        }

        private void MP3PlayerSelect_DataBinding(object sender, EventArgs e) {
            hypSwitchCategory.NavigateUrl = DefaultPage.Url().PathAndQuery;
            headerText = MP3PlayerSelectStrings.TextCaption();
            headerHtml = string.Empty;

            lblSearch.Text = MP3PlayerSelectStrings.TextSearch();
            lblCategory.Text = MP3PlayerSelectStrings.TextCategory();
            lblCategoryName.Text = HttpUtility.HtmlEncode(BCCategory.GetName(Category));
            lblNothingFound.Text = MP3PlayerSelectStrings.TextNothinFound();

            Paper[] papers = BCDevice.GetPaperList(Category.Id, search);
            ctlMP3PlayerSelection.DataSource = GetPapers(papers);
        }

        private ListSelectItem[] GetPapers(Paper[] papers) {
            ArrayList list = new ArrayList();
            int iconNormalWidth = 105;
            int iconNormalHeight = 90;

            foreach (Paper paper in papers) {
                ListSelectItem item = new ListSelectItem();
                item.Text = HttpUtility.HtmlEncode(paper.Name);
                item.ImageUrl = IconHandler.ImageUrl(IconHandler.PaperIdParamName, paper.Id, iconNormalWidth, iconNormalHeight).PathAndQuery;
                item.NavigateUrl = Designer.Url(paper).PathAndQuery;;
                list.Add(item);
            }

            lblNumberOfItems.Text = string.Format("<strong>{0}</strong> {1}", list.Count, MP3PlayerSelectStrings.TextItems());

            return (ListSelectItem[]) list.ToArray(typeof (ListSelectItem));
        }

        private void MP3PlayerSelect_PreRender(object sender, EventArgs e) {
            ListSelectItem[] devices = (ListSelectItem[])ctlMP3PlayerSelection.DataSource;
            lblNothingFound.Visible = devices == null || devices.Length == 0;
        }

        private void btnSearch_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
            DataBind();
        }
    }
}