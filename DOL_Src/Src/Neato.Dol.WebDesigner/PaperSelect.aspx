<%@ Page language="c#" Codebehind="PaperSelect.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.PaperSelect" %>
<%@ Register TagPrefix="cpt" TagName="ImageLibrary" Src="Controls/ImageLibrary.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>MediaFace Online - Select a paper</title>
    <meta name="keywords" content="Neato, NEATO CD/DVD Labels and Inserts, Neato iPod Wraps, LightScribe, direct to cd printing, Canon InkJet printers, Epson InkJet printers" />
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout" id="one-column">
	<form id="Form1" method="post" runat="server">
		<div class="additional-info">
			<ul class="select-product" >
				<li style>
					<asp:Label runat="server" ID="lblCategory" />
					<strong>
						<asp:Label runat="server" ID="lblCategoryName" />
					</strong>
				</li>
				<li class="switch1">
					<asp:HyperLink runat="server" ID="hypSwitchCategory" Visible="False" />
					<asp:DropDownList Runat="server" ID="cboPaperMetricFilter" AutoPostBack="True" Style="width:50px" />
				</li>
				<li>
					<asp:Label runat="server" ID="lblNumberOfItems" />
				</li>
				<li>
					<asp:DropDownList Runat="server" ID="cboSkuFilter" AutoPostBack="True"  />
				</li>
				<li class="split">
					<asp:Label Runat="server" ID="lblSearch" />
				</li>
				<li class="split">
					<asp:TextBox Runat="server" ID="txtSearch" />
				</li>
				<li class="end">
					<asp:ImageButton Runat="server" ID="btnSearch" ImageUrl="Images/buttons/go.gif"
						onmouseup="src='Images/buttons/go.gif'"
						onmousedown="src='Images/buttons/go_pressed.gif'"
						onmouseout="src='Images/buttons/go.gif'"
						CssClass="button"/>
				</li>
			</ul>	
			<div class="clear"></div>
		</div>
		<br />
		<div class="content">
			<div class="clear"></div>
			<cpt:ImageLibrary Runat="server" ID="ctlPaperSelection" />
			<asp:Label Runat="server" ID="lblNothingFound" />
			<div class="clear"></div>
		</div>
	</form>
  </body>
</html>
