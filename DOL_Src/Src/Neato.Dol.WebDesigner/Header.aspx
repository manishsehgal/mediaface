<%@ Page language="c#" Codebehind="Header.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.Header" %>
<%@ Register TagPrefix="dol" TagName="Header2" Src="Controls/Header2.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>MediaFace Online - Header</title>
    <meta name="keywords" content="Neato, NEATO CD/DVD Labels and Inserts, Neato iPod Wraps, LightScribe, direct to cd printing, Canon InkJet printers, Epson InkJet printers" />
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <script type="text/javascript" language="javascript" src="js/Designer.js"></script>
    <link href="css/style2.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" language="javascript" src="js/Header.js"></script>
  </head>
  <body MS_POSITIONING="GridLayout">
	
    <form id="Form1" method="post" runat="server">
      <div align="left">
        <dol:Header2 id="ctlHead" runat="server"/>
      </div>
    </form>
	
  </body>
</html>
