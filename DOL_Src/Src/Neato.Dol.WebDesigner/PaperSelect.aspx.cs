using System;
using System.Collections;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Controls;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebDesigner {
    public class PaperSelect : BasePage2 {
        protected ImageLibrary ctlPaperSelection;

        protected Label lblCategory;
        protected Label lblCategoryName;
        protected Label lblNumberOfItems;
        protected Label lblNothingFound;
        protected Label lblSearch;
        protected HyperLink hypSwitchCategory;
        protected TextBox txtSearch;
        protected ImageButton btnSearch;
        protected DropDownList cboSkuFilter;
        protected DropDownList cboPaperMetricFilter;

        private PaperBase[] paperBase = null;

        private Category Category {
            get { return (Category) ViewState["Category"]; }
            set { ViewState["Category"] = value; }
        }

        private const string ScuFilterKey = "ScuFilterKey";
        private const string PaperMetricFilterKey = "PaperMetricFilterKey";
        private const string ScuFilterTextKey = "ScuFilterTextKey";

        private PageFilter CurrentFilter {
            get { return (PageFilter) ViewState["CurrentFilter"]; }
            set { ViewState["CurrentFilter"] = value; }
        }

        private const string AllValue = "0";
        private const string UsValue = "1";

        private string search {
            get {
                return
                    (txtSearch.Text != string.Empty &&
                        CurrentFilter[ScuFilterKey] == AllValue)
                        ? txtSearch.Text : null;
            }
        }

        private void InitFilters() {
            CurrentFilter = new PageFilter();

            CurrentFilter[ScuFilterKey] = Request.QueryString[ScuFilterKey];
            if (CurrentFilter[ScuFilterKey] == null) CurrentFilter[ScuFilterKey] = AllValue;

            CurrentFilter[PaperMetricFilterKey] = Request.QueryString[PaperMetricFilterKey];
            if (CurrentFilter[PaperMetricFilterKey] == null) {
                CurrentFilter[PaperMetricFilterKey] = (Category.Id == 1) ? UsValue : AllValue;
            }
        }

        #region Url

        private const string RawUrl = "PaperSelect.aspx";
        private const string CategoryKey = "Category";
        public const string CdDvdLabels = "CdDvdLabels";
        public const string Inserts = "Inserts";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri Url(string category) {
            return UrlHelper.BuildUrl(RawUrl, CategoryKey, category);
        }

        new public static string PageName() {
            return RawUrl;
        }

        #endregion

        private void Page_Load(object sender, EventArgs e) {
            rawUrl = RawUrl;

            int categoryId = Convert.ToInt32(Request.QueryString[CategoryKey]);
            Category = new Category(categoryId);

            if (!IsPostBack) {
                InitFilters();
            }
            /*switch (Request.QueryString[CategoryKey]) {
               /* case CdDvdLabels:
                    Category = new Category(0);
                    break;
                case Inserts:
                    Category = new Category(0);
                    break;
            }*/
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            pageTemplateUrl = PaperSelectPageTemplate.Url().AbsolutePath;
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(PaperSelect_DataBinding);
            this.ctlPaperSelection.RedirectToUrl += new ImageLibrary.SelectItemEventHandler(ctlPaperSelection_RedirectToUrl);
            this.cboSkuFilter.DataBinding += new EventHandler(cboScuFilter_DataBinding);
            this.cboSkuFilter.SelectedIndexChanged += new EventHandler(cboSkuFilter_SelectedIndexChanged);
            this.btnSearch.Click += new ImageClickEventHandler(btnSearch_Click);
            this.PreRender += new EventHandler(PaperSelect_PreRender);
            this.cboPaperMetricFilter.DataBinding += new EventHandler(cboPaperMetricFilter_DataBinding);
            this.cboPaperMetricFilter.SelectedIndexChanged += new EventHandler(cboPaperMetricFilter_SelectedIndexChanged);
        }

        #endregion

        private void ctlPaperSelection_RedirectToUrl(Object sender, ImageLibrarySelectItemEventArgs e) {
            StorageManager.GenerateNewProject = true;
            Response.Redirect(e.NavigateUrl);
        }

        private void PaperSelect_DataBinding(object sender, EventArgs e) {
            //hypSwitchCategory.Text = PaperSelectStrings.TextSwitchCategory();
            hypSwitchCategory.NavigateUrl = DefaultPage.Url().PathAndQuery;
            headerText = PaperSelectStrings.TextCaption();
            headerHtml = string.Empty;

            lblSearch.Text = MP3PlayerSelectStrings.TextSearch();
            lblCategory.Text = PaperSelectStrings.TextCategory();
            lblCategoryName.Text = HttpUtility.HtmlEncode(BCCategory.GetName(Category));
            lblNothingFound.Text = PaperSelectStrings.TextNothinFound();
            //btnSearch.Text = PaperSelectStrings.TextGo();
            ArrayList papers = new ArrayList();

            PaperMetric paperMetric = null;
            if (CurrentFilter[PaperMetricFilterKey] != AllValue) {
                paperMetric = new PaperMetric(int.Parse(CurrentFilter[PaperMetricFilterKey]));
            }

            if (paperBase != null) {
                foreach (PaperBase paperbase in paperBase) {
                    papers.AddRange(BCPaper.GetPapers(null, Category, null, null, search, paperbase, null, null, null, -1, -1));
                }
            } else
                papers.AddRange(BCPaper.GetPapers(null, Category, null, null, search, null, null, null, paperMetric, -1, -1));

            lblNumberOfItems.Text = string.Format("<strong>{0}</strong> {1}", papers.Count, PaperSelectStrings.TextItems());
            ctlPaperSelection.DataSource = GetPapers((Paper[]) papers.ToArray(typeof (Paper)));
        }

        private ListSelectItem[] GetPapers(Paper[] papers) {
            ArrayList list = new ArrayList();

            foreach (Paper paper in papers) {
                ListSelectItem item = new ListSelectItem();
                item.Text = HttpUtility.HtmlEncode(paper.Name);
                item.ImageUrl = IconHandler.ImageUrl(IconHandler.PaperIdParamName, paper.Id).PathAndQuery;
                item.NavigateUrl = Designer.Url(paper).PathAndQuery;
                //  item.SkuNumber = paper.Sku;
                item.Description = "";
                list.Add(item);
            }

            return (ListSelectItem[]) list.ToArray(typeof (ListSelectItem));
        }

        private void cboScuFilter_DataBinding(object sender, EventArgs e) {
            cboSkuFilter.Items.Clear();
            Paper[] papers = BCPaper.GetPaperSkuList(Category);

            foreach (Paper paper in papers) {
                foreach (string sku in paper.Sku.Split(',')) {
                    if (paper.PaperState.IndexOf("a") == -1 && BCSpecialUser.GetSpecialUsers(Thread.CurrentPrincipal.Identity.Name, 1).Length < 1)
                        break;
                    string Sku = sku.Trim();
                    if (Sku.Length < 1)
                        continue;
                    ListItem item = new ListItem(Sku, paper.Id.ToString() + ";" + Sku);
                    cboSkuFilter.Items.Add(item);
                }
            }
            SortDropDown(cboSkuFilter);
            foreach (ListItem item in cboSkuFilter.Items) {
                if (item.Text == CurrentFilter[ScuFilterTextKey] && item.Value.Split(';')[0] == CurrentFilter[ScuFilterKey])
                    item.Selected = true;
            }
            cboSkuFilter.Items.Insert(0, new ListItem(PaperSelectStrings.TextSelectSKU(), AllValue));
        }

        private void cboSkuFilter_SelectedIndexChanged(object sender, EventArgs e) {
            CurrentFilter[ScuFilterKey] = AllValue;
            CurrentFilter[PaperMetricFilterKey] = AllValue;
            txtSearch.Text = string.Empty;
            if (IsPostBack) {
                string[] param = cboSkuFilter.SelectedValue.Split(';');
                CurrentFilter[ScuFilterKey] = param[0];
                if (param.Length > 1)
                    CurrentFilter[ScuFilterTextKey] = param[1];
                else
                    CurrentFilter[ScuFilterTextKey] = PaperSelectStrings.TextSelectSKU();
            }
            if (CurrentFilter[ScuFilterKey] != AllValue) {
                string[] paperIDS = cboSkuFilter.SelectedValue.Split(';')[0].Split('|');
                int i = 0;
                paperBase = new PaperBase[paperIDS.Length];
                foreach (string pid in paperIDS) {
                    paperBase[i] = new PaperBase(int.Parse(pid));
                    i++;
                }
            } else {
                paperBase = null;
            }

            this.DataBind();
        }

        private void PaperSelect_PreRender(object sender, EventArgs e) {
            ListSelectItem[] papers = (ListSelectItem[]) ctlPaperSelection.DataSource;
            lblNothingFound.Visible = papers == null || papers.Length == 0;
        }

        public static void SortDropDown(DropDownList list) {
            ListItemCollection items = list.Items;
            for (int i = 1; i < items.Count; i++) {
                string text1 = items[i].Text;
                string text2 = items[i - 1].Text;
                int result = text1.CompareTo(text2);
                if (result < 0) {
                    ListItem tmp = items[i - 1];
                    items.RemoveAt(i - 1);
                    //items.Insert(i-1,items[i-1]);
                    //items.RemoveAt(i); 
                    items.Insert(i, tmp);
                    i = 1;
                } else if (result == 0) {
                    ListItem itemToRemove = items[i];
                    ListItem itemToUpdate = items[i - 1];
                    string paperIdToRemove = itemToRemove.Value.Split(';')[0];
                    string paperIdToUpdate = itemToUpdate.Value.Split(';')[0];
                    bool bFound = false;
                    if (paperIdToUpdate.IndexOf('|') != -1) {
                        foreach (string st in paperIdToUpdate.Split('|')) {
                            if (st == paperIdToRemove) {
                                bFound = true;
                                break;
                            }
                        }
                    }
                    if (!bFound) {
                        itemToUpdate.Value = paperIdToRemove + '|' + itemToUpdate.Value;
                    }
                    items.RemoveAt(i);
                    i--;
                }

            }
            /* items.Clear();
            foreach(ListItem item in res) 
                items.Add(item);*/


        }

        private void btnSearch_Click(object sender, ImageClickEventArgs e) {
            CurrentFilter[ScuFilterKey] = AllValue;
            //CurrentFilter[PaperMetricFilterKey] = AllValue;
            CurrentFilter[ScuFilterTextKey] = PaperSelectStrings.TextSelectSKU();
            paperBase = null;
            DataBind();
        }

        private void cboPaperMetricFilter_DataBinding(object sender, EventArgs e) {
            cboPaperMetricFilter.Items.Clear();
            foreach (PaperMetric paperMetric in BCPaperMetric.Enum()) {
                cboPaperMetricFilter.Items.Add(new ListItem(paperMetric.Name, paperMetric.Id.ToString()));
            }
            cboPaperMetricFilter.Items.Add(new ListItem(PaperSelectStrings.TextAllMetric(), AllValue));
            cboPaperMetricFilter.SelectedValue = CurrentFilter[PaperMetricFilterKey];
        }

        private void cboPaperMetricFilter_SelectedIndexChanged(object sender, EventArgs e) {
            CurrentFilter[ScuFilterKey] = AllValue;
            CurrentFilter[PaperMetricFilterKey] = cboPaperMetricFilter.SelectedValue;
            this.DataBind();
        }
    }
}