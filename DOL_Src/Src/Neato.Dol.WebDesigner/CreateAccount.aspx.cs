using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Controls;
using Neato.Dol.WebDesigner.Helpers;

using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Tracking;

namespace Neato.Dol.WebDesigner {
    public class CreateAccount : BasePage2 {
        protected HtmlGenericControl lblUpcHeader;
        protected Label lblUpc;
        //protected Label lblUpcInfo;
        protected HtmlGenericControl lblUpcInfo;
        protected TextBox txtUpc;
        protected HyperLink hypUpcHowFind;
        protected ImageButton btnSubmitUpc;
        protected RequiredFieldValidator vldUpcRequired;
        protected CustomValidator vldUpcExist;
        protected RequiredFieldValidator vldSerialRequired;
        protected CustomValidator vldSerialExist;
        
        protected HtmlGenericControl lblSerialHeader;
        protected Label lblSerial;
        //protected Label lblSerialInfo;
        protected HtmlGenericControl lblSerialInfo;
        protected TextBox txtSerial;
        protected HyperLink hypSerialHowFind;
        protected ImageButton btnSubmitSerial;
        
        protected HtmlGenericControl lblBuyNowHeader;
        //protected Label lblBuyNowInfo;
        protected HtmlGenericControl lblBuyNowInfo; 
        protected ImageButton btnSubmitBuyNow;

        protected BannerControl ctlBanner1;
        protected BannerControl ctlBanner2;

        #region Url
        private const string RawUrl = "CreateAccount.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        new public static string PageName() {
            return RawUrl;
        }
        public static string BannerFolder {
            get { return "CreateAccount"; }
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (StorageManager.NewCustomer == null)
                Response.Redirect(CustomerAccount.Url(true).PathAndQuery);
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            pageTemplateUrl = SimplePageTemplate.Url().AbsolutePath;

            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(CreateAccount_DataBinding);
            this.btnSubmitUpc.Click += new System.Web.UI.ImageClickEventHandler(btnSubmitUpc_Click);
            this.vldUpcExist.ServerValidate += new ServerValidateEventHandler(vldUpcExist_ServerValidate);
            this.vldSerialExist.ServerValidate += new ServerValidateEventHandler(vldSerialExist_ServerValidate);
            this.btnSubmitSerial.Click += new System.Web.UI.ImageClickEventHandler(btnSubmitSerial_Click);
            this.btnSubmitBuyNow.Click += new System.Web.UI.ImageClickEventHandler(btnSubmitBuyNow_Click);
            ctlBanner1.DataBinding += new EventHandler(BannerDataBinding);
            ctlBanner2.DataBinding += new EventHandler(BannerDataBinding);
        }
        #endregion

        private void CreateAccount_DataBinding(object sender, EventArgs e) {
            lblUpcHeader.InnerText = BCBanner.GetLabelText(BannerFolder, 8); //CreateAccountStrings.TextUpcHeader();
            lblUpcInfo.InnerHtml = BCBanner.GetLabelText(BannerFolder, 3);
//            if (lblUpcInfo.Text.Length == 0) {
//                lblUpcInfo.Text = CreateAccountStrings.TextUpcInfo();            
//            }

            lblUpc.Text = CreateAccountStrings.TextUpc();
            hypUpcHowFind.Text = CreateAccountStrings.TextUpcHowFind();
            vldUpcRequired.Text = vldUpcExist.Text =
                CreateAccountStrings.TextVldUpcExist();
            vldSerialRequired.Text = vldSerialExist.Text =
                CreateAccountStrings.TextVldSerialExist();

            lblSerialHeader.InnerText = BCBanner.GetLabelText(BannerFolder, 9); //CreateAccountStrings.TextSerialHeader();
            lblSerialInfo.InnerHtml = BCBanner.GetLabelText(BannerFolder, 4);
//            if (lblSerialInfo.Text.Length == 0) {
//                lblSerialInfo.Text = CreateAccountStrings.TextSerialInfo();            
//            }
            
            lblSerial.Text = CreateAccountStrings.TextSerial();
            hypSerialHowFind.Text = CreateAccountStrings.TextSerialHowFind();
            hypSerialHowFind.Attributes["onClick"] = String.Format(hypSerialHowFind.Attributes["onClick"], BCBanner.GetPopUpSource(BannerFolder, 7));

            lblBuyNowHeader.InnerText = BCBanner.GetLabelText(BannerFolder, 10); //CreateAccountStrings.TextBuyNowHeader();
            
            lblBuyNowInfo.InnerHtml = BCBanner.GetLabelText(BannerFolder, 5);
//            if (lblBuyNowInfo.Text.Length == 0) {
//                lblBuyNowInfo.Text = CreateAccountStrings.TextBuyNowInfo();            
//            }
        }

        private void btnSubmitUpc_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
            vldUpcRequired.Validate();
            vldUpcExist.Validate();

            if (vldUpcExist.IsValid && vldUpcRequired.IsValid) {

                //the next line was temporary commented to escape retailer page.
                //Response.Redirect(SelectRetailer.Url().PathAndQuery);

                //retailer is hardcoded with Neato:
                //----------->
                Retailer retailer = BCRetailer.Get(1);//1 is id for Neato
                if (StorageManager.NewCustomer == null) return;
                StorageManager.NewCustomer.RetailerId = retailer.Id;
                StorageManager.CurrentCustomer = StorageManager.NewCustomer;
                CustomerGroup customerGroup = CustomerGroup.MFO;
                try {
                    BCCustomer.ActivateCustomer(
                            StorageManager.CurrentCustomer.Email,
                            StorageManager.CurrentCustomer.FirstName,
                            StorageManager.CurrentCustomer.LastName,
                            StorageManager.CurrentCustomer.Password,
                            StorageManager.CurrentCustomer.ReceiveInfo,
                            DefaultPage.Url().AbsoluteUri, retailer, customerGroup);
                    BCTracking.Add(UserAction.Registration,
                            Request.UserHostAddress);

                    Response.Redirect(DefaultPage.Url().PathAndQuery);
                } catch (BaseBusinessException) {
                    StorageManager.CurrentCustomer = null;
                } 
                finally {
                    StorageManager.NewCustomer = null;
                }
                //----------->
            }
            DataBind();
        }

        private void vldUpcExist_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = BCUpc.Validate(txtUpc.Text);
        }

        private void btnSubmitSerial_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
            vldSerialRequired.Validate();
            vldSerialExist.Validate();

            if (vldSerialExist.IsValid && vldSerialRequired.IsValid) {
                //                XmlDocument doc = Customer.
                //                    GetXmlForBuyAccountWithSerialNumber(
                //                    StorageManager.NewCustomer.Email,
                //                    txtSerial.Text);
                //                Response.Write(doc.OuterXml);

                string number = KeyValidationHelper.
                    GetNumberInCorrectFormat(txtSerial.Text);
                string neatoDolBuyAccountUrl = 
                    BCParameters.GetParameterByKey("NeatoDolBuyAccountUrl");
                string url = string.Format("{0}?{1}={2}&{3}={4}",
                    neatoDolBuyAccountUrl,
                    "Email", StorageManager.NewCustomer.Email,
                    "Serial", number);
                Response.Redirect(url);
                /*Response.Redirect(DefaultPage.Url().PathAndQuery);



                string handlerUrl = BCParameters.GetParameterByKey("NeatoDolBuyAccountUrl");

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(handlerUrl);
                request.Method = "POST";
                request.Credentials = CredentialCache.DefaultCredentials;

                StreamWriter writer = new StreamWriter(request.GetRequestStream());

                XmlDocument doc = Customer.
                    GetXmlForBuyAccountWithSerialNumber(
                    StorageManager.NewCustomer.Email,
                    txtSerial.Text);
                writer.Write(doc.OuterXml);
                writer.Close();

                request.BeginGetRequestStream(new AsyncCallback(ResponseCallback), request);
                */
            }
            DataBind();
        }

        private void btnSubmitBuyNow_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
            //            XmlDocument doc = Customer.
            //                GetXmlForBuyAccountWithoutSerialNumber(
            //                StorageManager.NewCustomer.Email);
            //            Response.Write(doc.OuterXml);

            string neatoDolBuyAccountUrl = 
                BCParameters.GetParameterByKey("NeatoDolBuyAccountUrl");
            string url = string.Format("{0}?{1}={2}", 
                neatoDolBuyAccountUrl,
                "Email", StorageManager.NewCustomer.Email);
            Response.Redirect(url);
            
            /*Response.Redirect(DefaultPage.Url().PathAndQuery);

            
            
            string handlerUrl = BCParameters.GetParameterByKey("NeatoDolBuyAccountUrl");

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(handlerUrl);
            request.Method = "POST";
            request.Credentials = CredentialCache.DefaultCredentials;

            StreamWriter writer = new StreamWriter(request.GetRequestStream());

            XmlDocument doc = Customer.
                GetXmlForBuyAccountWithoutSerialNumber(
                StorageManager.NewCustomer.Email);
            writer.Write(doc.OuterXml);
            writer.Close();

            request.BeginGetRequestStream(new AsyncCallback(ResponseCallback), request);
            */
        }
        
        //        private void ResponseCallback(IAsyncResult asynchronousResult) {
        //            HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
        //            request.EndGetRequestStream(asynchronousResult);
        //        }

        private void vldSerialExist_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = KeyValidationHelper.
                IsSerialNumberValid(txtSerial.Text);
        }
        private void BannerDataBinding(object sender, EventArgs e) {
            BannerControl banner = (BannerControl) sender;
            int bannerId = banner.BannerId;
            banner.Banner = BCBanner.GetBanner(BannerFolder, bannerId);
            int height = BCBanner.GetBannerHeight(BannerFolder, bannerId);
            if (height != -1)
                banner.Height = height;
        }
    }
}