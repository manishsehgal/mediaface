using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner {
    public class Feedback : BasePage2 {
        protected Label lblCaption;
        protected Label lblThank;
        protected Label lblMessage;
        protected ImageButton btnSend;
        protected ImageButton btnCancel;
        protected ImageButton btnClose;
        protected TextBox txtEmailText;

        protected Label lblRequired;
        protected Label lblYourEmail;
        protected TextBox txtYourEmail;
        protected RequiredFieldValidator vldEmailAddress;
        protected CustomValidator vldValidEmail;
        protected Panel panFeedbackMessage;
        protected Panel panFeedbackAnswer;

        #region Url
        private const string RawUrl = "Feedback.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            if (!IsPostBack) {
                DataBind();
            }
        }
        new public static string PageName() {
            return RawUrl;
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.vldValidEmail.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(this.vldValidEmail_ServerValidate);
            this.btnSend.Click += new System.Web.UI.ImageClickEventHandler(this.btnSend_Click);
            this.btnCancel.Click += new System.Web.UI.ImageClickEventHandler(this.btnCancel_Click);
            this.btnClose.Click += new System.Web.UI.ImageClickEventHandler(this.btnClose_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new System.EventHandler(this.Feedback_DataBinding);

        }
        #endregion

        #region DataBinding
        private void Feedback_DataBinding(object sender, EventArgs e) {
            lblCaption.Text = FeedbackStrings.TextCaption();
            lblThank.Text = FeedbackStrings.TextThank();

            lblRequired.Text = FeedbackStrings.TextRequired();
            lblYourEmail.Text = FeedbackStrings.TextYourEmail();
            lblMessage.Text = FeedbackStrings.TextMessage();
            vldEmailAddress.Text = FeedbackStrings.ValidatorEmailAddress();
            vldValidEmail.Text = FeedbackStrings.ValidatorInvalidEmailAddress();

            if (HttpContext.Current.User.IsInRole(Authentication.RegisteredUser)) {
                txtYourEmail.Text = StorageManager.CurrentCustomer.Email;
            }

            SetControlsAvaliability(false);

            rawUrl = RawUrl;
            headerText = FeedbackStrings.TextFeedback();
            headerHtml = "<br><br><br>";
        }
        #endregion

        private void SetControlsAvaliability(bool isSendPressed) {
            panFeedbackMessage.Visible = !isSendPressed;
            panFeedbackAnswer.Visible = isSendPressed;
        }

        private void btnSend_Click(object sender, ImageClickEventArgs e) {
            vldEmailAddress.Validate();
            vldValidEmail.Validate();
            if (!(vldEmailAddress.IsValid && vldValidEmail.IsValid))
                return;

            int maxLength = Configuration.MaxFeedbackEmailLength;

            string userMessage = txtEmailText.Text;
            if (userMessage.Length > maxLength)
                userMessage = userMessage.Substring(0, maxLength);
            string userEmail = txtYourEmail.Text;

            BCMail.SendUserFeedbackMail(userEmail, userMessage, StorageManager.CurrentLanguage, true);

            txtEmailText.Text = userMessage;
            SetControlsAvaliability(true);
        }

        private void btnCancel_Click(object sender, ImageClickEventArgs e) {
            Response.Redirect(Support.Url().PathAndQuery);
        }

        private void btnClose_Click(object sender, ImageClickEventArgs e) {
            Response.Redirect(Support.Url().PathAndQuery);
        }

        private void vldValidEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = FormatValidator.EnsureValidEmail(args.Value.Trim());
        }
    }
}