using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Web;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.HttpModules;

namespace Neato.Dol.WebDesigner {
    public class Global : HttpApplication {
        public static Assembly GetAssembly() {
            return Assembly.GetExecutingAssembly();
        }

        private IContainer components = null;

        public static readonly string Version = GetApplicationVersion();

        private static string GetApplicationVersion() {
            AssemblyName thisAssemblyName = Assembly.GetExecutingAssembly().GetName(false);
            return thisAssemblyName.Version.ToString(4);
        }

        public Global() {
            InitializeComponent();
        }

        protected void Application_Start(Object sender, EventArgs e) {
            StorageManager.IsSiteClosed = false;
            SessionsAdmCount = 0;
            SessionsUsrCount = 0;
            Configuration.ApplicationRoot = Context.Request.PhysicalApplicationPath;
        }

        protected void Application_BeginRequest(Object sender, EventArgs e) {
            const string PostWord = "POST";
            const string SessionKey = "ASP.NET_SessionId";


            string sessionValue = Request.QueryString[SessionKey];
            int index = Request.Browser.Browser.ToUpper().IndexOf("MSIE");
            if (sessionValue != null && index == -1) {
                if(sessionValue != "null") {
                    HttpCookie sessionCookie = Request.Cookies.Get(SessionKey);
                    if (sessionCookie == null)
                        sessionCookie = new HttpCookie(SessionKey);

                    if (sessionCookie.Value != sessionValue) {
                        sessionCookie.Values.Set(null, sessionValue);
                        sessionCookie.Expires = DateTime.Now.AddMinutes(1);
                        Request.Cookies.Set(sessionCookie);
                    }
                }
            }
        }

        protected void Application_EndRequest(Object sender, EventArgs e) {}

        protected void Application_AuthenticateRequest(Object sender, EventArgs e) {}

        protected void Application_Error(Object sender, EventArgs e) {
            Exception ex = Server.GetLastError().GetBaseException();
            FileNotFoundException fileNotFoundEx = ex as FileNotFoundException;
            if (fileNotFoundEx != null) {
                if (Request.Url.AbsolutePath.EndsWith(".aspx")) {
                    BCDeadLink.Insert(new DeadLink(Request.Url.AbsoluteUri, Request.UserHostAddress, Request.UrlReferrer.AbsoluteUri));
                    Response.Redirect(DefaultPage.Url().PathAndQuery);
                }
            }

            ExceptionPublisher.Publish(ex.GetBaseException());
            Server.ClearError();
            Server.Transfer(ErrorPage.Url().PathAndQuery);
        }

        public int SessionsCount {
            get { return SessionsUsrCount; }
        }

        protected int SessionsAdmCount {
            get {
                if (Application["WebDesigner.SessionAdm.Count"] != null) {
                    return (int)Application["WebDesigner.SessionAdm.Count"];
                }
                return 0;
            }
            set { Application["WebDesigner.SessionAdm.Count"] = value; }
        }

        protected int SessionsUsrCount {
            get {
                if (Application["WebDesigner.SessionUsr.Count"] != null) {
                    return (int)Application["WebDesigner.SessionUsr.Count"];
                }
                return 0;
            }
            set { Application["WebDesigner.SessionUsr.Count"] = value; }
        }

        protected void Session_Start(Object sender, EventArgs e) {
            if (!CheckLocationModule.IsHttpHandler && !CheckLocationModule.IsAdminMode && !StorageManager.IsSiteClosed) {
                SessionsUsrCount++;
            } else {
                SessionsAdmCount++;
            }
        }

        protected void Session_End(Object sender, EventArgs e) {
            if (SessionsAdmCount > 0) {
                SessionsAdmCount--;
            } else if (SessionsUsrCount > 0) {
                SessionsUsrCount--;
            }
        }

        protected void Application_End(Object sender, EventArgs e) {}

        #region Web Form Designer generated code
        private void InitializeComponent() {
            this.components = new Container();
        }
        #endregion
    }
}