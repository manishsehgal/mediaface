<%@ Page language="c#" Codebehind="SelectRetailer.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.SelectRetailer" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>MediaFace Online - Select Retailer</title>
    <meta name="keywords" content="Neato, NEATO CD/DVD Labels and Inserts, Neato iPod Wraps, LightScribe, direct to cd printing, Canon InkJet printers, Epson InkJet printers" />
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="css/style.css" type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="GridLayout">
	
    <form id="Form1" method="post" runat="server">
    
        <asp:Label id="lblUnexpectedError" runat="server"
         CssClass="Text Alert" Visible="False"/>

	    <div class="content"><br><br>
    	<div class="clear"></div>
    	
	    <asp:DataList ID="lstItems"
            Runat="server" 
            RepeatDirection="Horizontal"
            RepeatLayout="Table"
            RepeatColumns="4"
            EnableViewState="true">
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            <ItemTemplate>
		        <div class="product green">
			        <div class="decor-t">
				        <div></div>
			        </div>
			        <div class="note">
				        <asp:ImageButton Runat="server" ID="btnRetailer" CommandName="Click"/>
			        </div>
			        <div class="decor-bw">
				        <div></div>
			        </div>
			        <div class="label">
					    <h3>
						    <asp:Label Runat="server" ID="lblRetailer" />
					    </h3>
						<asp:HyperLink ID="hypRetailer" Runat="server" CssClass="SelectRetailerLink" Target="_blank"/>
				        <div class="decor-b">
					        <div></div>
				        </div>
			        </div>
		        </div>
            </ItemTemplate>
        </asp:DataList>
        </div>

     </form>
	
  </body>
</html>
