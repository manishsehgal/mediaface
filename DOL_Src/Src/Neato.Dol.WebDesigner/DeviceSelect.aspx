<%@ Page language="c#" Codebehind="DeviceSelect.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.DeviceSelect" %>
<%@ Register TagPrefix="cpt" TagName="ChoiceArea" Src="Controls/ChoiceArea.ascx" %>
<%@ Register TagPrefix="cpt" TagName="ImageLibrary" Src="Controls/ImageLibrary.ascx" %>
<%@ Register TagPrefix="cpt" TagName="AddNewPhone" Src="Controls/AddNewPhone.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>MediaFace Online</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/style.css" type="text/css" rel="stylesheet">
		<script type="text/javascript" language="javascript" src="js/SelectDevice.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
	  <form id="Form1" method="post" runat="server">
	    <img src="Images\HeaderBackground.gif" border="0" GALLERYIMG="no" STYLE="position:absolute; z-index:1;">
	    <div style="position:absolute; z-index:1;">
	        <asp:HyperLink ID="hypHome" Runat="server" ImageUrl="images/logo.gif" STYLE="position:absolute;left:2px;top:6px;" />
	        <asp:Label ID="lblStep1" Runat="server" CssClass="StepTextSelected" STYLE="position:absolute;left:590px;top:6px;" />
	        <asp:Label ID="lblStep2" Runat="server" CssClass="StepText" STYLE="position:absolute;left:720px;top:26px;" />
	        <asp:Label ID="lblStep3" Runat="server" CssClass="StepText" STYLE="position:absolute;left:826px;top:50px;" />
	    </div>
		<table cellSpacing="0" cellPadding="0" border="0" width=100% style="margin-top:75px;">
			<tr>
				<td vAlign="top" class="DeviceSelectLeftPanel">
					<table cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td valign="bottom" class="DeviceSelectLeftPanelUp"  style="padding: 0px 10px 0px 10px;">
								<asp:Label ID="lblSelectDevice" Runat="server" CssClass="Text Black" />
							</td>
						</tr>
						<tr style="	background-color: #EE560D">
							<td style="padding: 0px 10px 25px 10px;">
								<cpt:ChoiceArea id="ctlChoiceArea" runat="server"></cpt:ChoiceArea>
							</td>
						</tr>
						<tr style="	background-color: #EE560D">
							<td style="padding: 0px 10px 0px 10px;>
								<div style="padding-top: 0px">
									<asp:Label ID="lblCantFindYourDevice" Runat="server" CssClass="Text Black Header" />
							    </div>
							    <div style="padding-top: 5px">
									<asp:Label ID="lblRequestPhoneHint1" Runat="server" CssClass="Text Black" />
								</div>
							    <div style="padding-top: 5px">
									<asp:LinkButton ID="btnRequestPhone" Runat="server" CssClass="Text White Header" CausesValidation="False" />
									<asp:ImageButton ID="btnRequestPhoneArrow" Runat="server" CausesValidation="False" ImageUrl="images/arrow.gif"/>
								</div>
							    <div style="padding-top: 5px">
									<asp:Label ID="lblRequestPhoneHint2" Runat="server" CssClass="Text Black" />
								</div>
							</td>
						</tr>
						<tr align="center" valign="top" style="	background-color: #EE560D; padding: 0px 10px 0px 10px;">
							<td height="380px;">
								<br/>
								<asp:label id="lblPreviousModelSelection" CssClass="Text Black Header" Runat="server"></asp:label>
								<div style="width: 100%; height: 100%;">
    								<cpt:imagelibrary id="ctlPrevModelList" runat="server" ColumsNum="1" ImageCssStyle="ImageLibrarySmall" HyperLinkCssStyle="LinkEnlargeNavy"></cpt:imagelibrary>
    							</div>
    						</td>
    					</tr>
					</table>
				</td>
				<td class="RightCell" vAlign="top" rowSpan="3" style="padding: 0px">
					<cpt:AddNewPhone runat="server" id="ctlRequestPhone" />
					<div style="padding: 10px;">
						<asp:Label Runat="server" ID="lblCurrentStep" CssClass="Text YellowHeader"/>
						<div style="padding-top: 20px">
							<cpt:imagelibrary id="ctlDeviceList" runat="server"></cpt:imagelibrary>
						</div>
						<div style="width:70%">
							<div style="width:100%">
								<asp:label id="lblInfo" CssClass="Text" Runat="server"></asp:label>
							</div>
							<div style="width:100%">
								<asp:label id="lblPhoneRequestInfo" Visible="False" CssClass="Text" Runat="server"></asp:label>
							</div>
							<div style="width:100%" align="center">
								<asp:ImageButton Runat="server" ID="btnClose"
									ImageUrl="images/buttons/close.gif" Visible="False"
									onmouseup="src='images/buttons/close.gif';"
									onmousedown="src='images/buttons/close_pressed.gif';"
									onmouseout="src='images/buttons/close.gif';"/>
								<img src="images/buttons/close_pressed.gif" style="display: none;">
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
	  </form>
	</body>
</HTML>
