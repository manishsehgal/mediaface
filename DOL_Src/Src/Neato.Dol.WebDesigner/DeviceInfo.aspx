<%@ Page language="c#" Codebehind="DeviceInfo.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.DeviceInfo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title><asp:Literal runat="server" id="pageTitle" /></title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="css/style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout" class="DeviceInfo">
		<center>
			<asp:Image ID="imgDevice" Runat="server" /><br>
			<asp:Label ID="lblDevice" CssClass="Paragraph" Runat="server" />
			<br><br>
			<asp:HyperLink Runat="server" CssClass="LinkEnlarge" ID="hypClose" />
		</center>
	</body>
</HTML>
