using System.Web;

namespace Neato.Dol.WebDesigner.Helpers {
    /// <summary>
    /// HTML element attributes and values
    /// </summary>
    public sealed class HtmlHelper {
        private HtmlHelper() {}

        // JavaScript events
        public const string OnScroll = "onscroll";
        public const string OnChange = "onchange";
        public const string OnClick = "onclick";
        public const string OnKeyUp = "onkeyup";
        public const string OnKeyDown = "onkeydown";
        public const string OnMouseOver = "onmouseover";

        // HTML attributes
        public const string Display = "display";
        public const string Visibility = "visibility";
        public const string Style = "style";
        public const string Cursor = "cursor";

        // HTML attribute values
        public const string DisplayNone = "none";
        public const string DisplayInline = "inline";
        public const string VisibilityHidden = "hidden";
        public const string CursorHand = "hand";
        public const string Blank = "_blank";

        // Other constants
        public const string ObjectWindow = "window";
        public const string NbspTag = "&nbsp;";
        public const string BrTag = "<br/>";
        public const string EmptyLink = "#";

        public static readonly string BlackCircleChar = HttpUtility.HtmlDecode("&#x25CF;");

    }
}