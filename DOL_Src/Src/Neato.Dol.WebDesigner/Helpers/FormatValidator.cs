using System.Text.RegularExpressions;

namespace Neato.Dol.WebDesigner.Helpers {
    public sealed class FormatValidator {
        private FormatValidator() {}

        private const string EMailPattern = @"^[^\(\)<>@,;:""\\\s\.\[\]]+([\.][^\(\)<>@,;:""\\\s\.\[\]]+)*@[a-zA-Z0-9][\w-]*[a-zA-Z0-9](\.[a-zA-Z0-9][\w-]*[a-zA-Z0-9])+$";
        private static readonly Regex EMailRegex = new Regex(EMailPattern);

        public static bool EnsureValidEmail(string val) {
            if (val.Length > 0) {                
                return EMailRegex.Matches(val).Count > 0;
            } else {
                return true;
            }
        }

    }
}