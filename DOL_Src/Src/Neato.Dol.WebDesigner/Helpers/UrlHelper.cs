using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Web;

namespace Neato.Dol.WebDesigner.Helpers {
    /// <summary>
    /// Helper functions for page URL handling
    /// </summary>
    public sealed class UrlHelper {
        private UrlHelper() {}

        /// <summary>
        /// Build page Uri by the relative page url and parameters
        /// </summary>
        /// <param name="rawUrl">relative page url</param>
        /// <param name="parameters">Parameters for query string</param>
        /// <returns></returns>
        public static Uri BuildUrl(string rawUrl, params object[] parameters) {
            // Build url
            UriBuilder result = new UriBuilder(HttpContext.Current.Request.Url);
            result.Path = Path.Combine(HttpContext.Current.Request.ApplicationPath, rawUrl);
            result.Query = BuildParameters(parameters);

            return result.Uri;
        }

        public static string BuildParameters(params object[] parameters) {
            // Prepare parameters
            Debug.Assert(parameters.Length % 2 == 0, "Incorrect parameters count");
            
            StringBuilder urlParameters = new StringBuilder();
            for (int i = 0; i < parameters.Length - 1; i += 2) {
                Debug.Assert(parameters[i] != null);
                Debug.Assert(parameters[i] is string);
                Debug.Assert(parameters[i+1] != null);
                Debug.Assert(parameters[i+1] is string || parameters[i+1] is int);

                if (i != 0) urlParameters.Append("&");
                urlParameters.AppendFormat("{0}={1}", parameters[i],
                    HttpUtility.UrlEncode(parameters[i + 1].ToString()));
            }

            return urlParameters.ToString();
        }


    }
}
