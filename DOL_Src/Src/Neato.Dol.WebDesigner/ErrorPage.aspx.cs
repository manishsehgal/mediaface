using System;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;

using Neato.Dol.WebDesigner.Controls;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner {
    public class ErrorPage : BasePage2 {
        protected Literal litErrorMessage;
        

        #region Url
        private const string RawUrl = "ErrorPage.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Error_DataBinding);
        }

        private void Error_DataBinding(object sender, EventArgs e) {
            litErrorMessage.Text = ErrorPageStrings.TextErrorMessage();
            rawUrl = RawUrl;
            headerText = ErrorPageStrings.TextError();
        }
        #endregion
    }
}