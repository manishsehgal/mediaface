<%@ Control Language="c#" AutoEventWireup="false" Codebehind="DefaultPageTemplate.ascx.cs" Inherits="Neato.Dol.WebDesigner.DefaultPageTemplate" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="dol" TagName="Header2" Src="Controls/Header2.ascx" %>
<%@ Register TagPrefix="dol" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="dol" TagName="FooterMenu" Src="Controls/FooterMenu.ascx" %>
<link href="css/style2.css" type="text/css" rel="stylesheet">
<div align="left">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<dol:Header2 id="ctlHeader" runat="server"/>
			</td>
		</tr>
		<tr>
			<td class="Content">
				<asp:PlaceHolder ID="dynamicContent" Runat="server" />
			</td>
		</tr>
		<tr>
			<td>
				<dol:Footer id="ctlFooter" runat="server" />
			</td>
		</tr>
	</table>
</div>
