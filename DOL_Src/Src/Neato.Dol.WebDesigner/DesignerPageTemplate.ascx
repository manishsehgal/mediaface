<%@ Control Language="c#" AutoEventWireup="false" Codebehind="DesignerPageTemplate.ascx.cs" Inherits="Neato.Dol.WebDesigner.DesignerPageTemplate" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="dol" TagName="Footer" Src="Controls/Footer.ascx" %>
<link href="css/style2.css" type="text/css" rel="stylesheet">
<link href="css/style.css" type="text/css" rel="stylesheet">
<div align="left">
  <div id="idDesigner" style="position:absolute;">
    <iframe Runat="server" ID="frmHeader" frameborder="no" scrolling="no"
      style="width:905;height:60;" src="Header.aspx"/>
    <asp:PlaceHolder ID="dynamicContent" Runat="server" />
    <dol:Footer id="ctlFooter" runat="server" ShowRequiredLinks="False" />
  </div>
  <iframe Runat="server" ID="frmLogin" frameborder="no" scrolling="no"
    style="width:905;height:650;display:none" src="Login.aspx?DesignerMode=true" />
</div>
