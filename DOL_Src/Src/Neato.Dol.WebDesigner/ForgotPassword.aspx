<%@ Page language="c#" Codebehind="ForgotPassword.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.ForgotPassword" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
  <head>
    <title>MediaFace Online - Forgot Password</title>
    <meta name="keywords" content="Neato, NEATO CD/DVD Labels and Inserts, Neato iPod Wraps, LightScribe, direct to cd printing, Canon InkJet printers, Epson InkJet printers" />
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="css/style.css" type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
		<div style="padding-left:170px;">
			<asp:Literal ID="ctlHeaderHtml" Runat="server"/>
			<div id="root">
				<div id="content-sidebar-container">
					<!-- content -->
					<div id="content" style="margin:auto">
						<div class="box">
							<div class="label-caption">
								<div class="decor-t">
									<div></div>
								</div>
								<h2 id="ctlHeaderCaption" runat="server"/>
							</div>
							<div class="decor-bw">
								<div></div>
							</div>
							<div class="content">
								<div class="clear"></div>
									<table ID="tblForgotPassword" Runat="server" cellpadding="0" cellspacing="0" class="FormTable">
										<tr valign="middle">
											<td colspan="3">
												<div style="margin-top:20px; margin-bottom:20px;">
													<asp:Label ID="lblDescription" Runat="server" CssClass="LeftPanelText"/>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<asp:Label ID="lblEmailAddress" Runat="server" CssClass="LeftPanelText"/>
											</td>
											<td>
												<asp:TextBox ID="txtEmailAddress" Runat="server" CssClass="Field" MaxLength="100"/>
											</td>
											<td>
												<asp:ImageButton Runat="server" ID="btnSend" 
													CausesValidation="False"
													ImageAlign="AbsMiddle"
													ImageUrl="Images/buttons/send.gif"
													onmouseup="src='images/buttons/send.gif';"
													onmousedown="src='images/buttons/send_pressed.gif';"
													onmouseout="src='images/buttons/send.gif';"
													CssClass="button"/>
												<img src="images/buttons/send_pressed.gif" style="display: none;">
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<div>
													<asp:Label ID="lblSendMail" Runat="server" CssClass="ValidationText" Visible="False"/>
												</div>
												<div>
													<asp:Label ID="lblError" Runat="server" CssClass="LeftPanelText" Visible="False"/>
												</div>
												<div>
													<asp:RequiredFieldValidator ControlToValidate = "txtEmailAddress" Display="Dynamic" CssClass="LeftPanelText" 
														Runat="server" ID="vldEmailAddress"/>
												</div>
												<div>
													<asp:CustomValidator ControlToValidate="txtEmailAddress" Display="Dynamic" CssClass="LeftPanelText"
														Runat="server" ID="vldValidEmail" EnableClientScript="False"/>
												</div>
												<div>
													<asp:CustomValidator ControlToValidate = "txtEmailAddress" Display="Dynamic" CssClass="LeftPanelText" Runat="server" ID="vldEmailNotRegistered"/> 
												</div>
											</td>
										</tr>
									</table>
									<table ID="tblResetPassword" Runat="server" cellpadding="0" cellspacing="0" class="FormTable">
										<tr>
											<td>
												<asp:Label ID="lblEmailAddressText" Runat="server" CssClass="LeftPanelText"/>
											</td>
											<td>
												<asp:Label ID="lblEmailAddressValue" Runat="server" CssClass="LeftPanelText"/>
											</td>
										</tr>
										<tr>
											<td>
												<asp:Label ID="lblPasswordText" Runat="server" CssClass="LeftPanelText" />
											</td>
											<td>
												<asp:Label ID="lblPasswordValue" Runat="server" CssClass="LeftPanelText" />
											</td>
										</tr>
									</table>
									<table ID="tblWrongPasswordUID" Runat="server" Visible="False" cellpadding="0" cellspacing="0" class="FormTable">
										<tr>
											<td>
												<asp:Label ID="lblDescription2" Runat="server" CssClass="LeftPanelText"/>
											</td>
										</tr>
										<tr>
											<td align="center" >
												<asp:ImageButton Runat="server" ID="btnContinue"
													CausesValidation="False"
													ImageUrl="images/buttons/continue.gif"
													onmouseup="src='images/buttons/continue.gif';"
													onmousedown="src='images/buttons/continue_pressed.gif';"
													onmouseout="src='images/buttons/continue.gif';"/>
												<img src="images/buttons/continue_pressed.gif" style="display: none;">
											</td>
										</tr>
									</table>
							</div>
							<div class="decor-b">
								<div></div>
							</div>
						</div>
					</div>
					<!-- end of content -->    
				</div>
			</div>    
		</div>
    </form>
  </body>
</html>

