<%@ Page language="c#" Codebehind="TellAFriend.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.TellAFriend" %>
<%@ Register TagPrefix="cpt" TagName="MainMenu" Src="Controls/MainMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="FooterMenu" Src="Controls/FooterMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="Header" Src="Controls/Header.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
  <head>
    <title>MediaFace Online - Tell a Friend</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="css/style.css" type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
		<table cellpadding="0" cellspacing="0" class="FormTable">
			<tr>
				<td colspan="3">
					<asp:Label ID="lblRequired" Runat="server" CssClass="Text Required"/>
				</td>
			</tr>
			<tr>
                <td>
                    <span class="Text Required">&nbsp;*&nbsp;</span>
                </td>
				<td>
					<asp:Label ID="lblFriendName" Runat="server" CssClass="Text Header"/>
				</td>
				<td>
					<asp:TextBox ID="txtFriendName" Runat="server" CssClass="Field100" MaxLength="100"/>
				</td>
			</tr>
			<tr>
			    <td/>
				<td colspan="2">
					<asp:RequiredFieldValidator ControlToValidate = "txtFriendName" Display="Dynamic" CssClass="Text Alert" Runat="server" ID="vldFriendName" EnableClientScript="False"/>
				</td>
			</tr>
			<tr>
                <td>
                    <span class="Text Required">&nbsp;*&nbsp;</span>
                </td>
				<td>
					<asp:Label ID="lblFriendEmail" Runat="server" CssClass="Text Header"/>
				</td>
				<td>
					<asp:TextBox ID="txtFriendEmail" Runat="server" CssClass="Field100" MaxLength="100"/>
				</td>
			</tr>
			<tr>
			    <td/>
				<td colspan="2">
					<asp:RequiredFieldValidator ControlToValidate = "txtFriendEmail" Display="Dynamic" CssClass="Text Alert" Runat="server" ID="vldEmailFriendAddress" EnableClientScript="False"/>
					<asp:CustomValidator ControlToValidate="txtFriendEmail" Display="Dynamic" CssClass="Text Alert" Runat="server" ID="vldValidFriendEmail" EnableClientScript="False"/>
				</td>
			</tr>
			<tr>
                <td>
                    <span class="Text Required">&nbsp;*&nbsp;</span>
                </td>
				<td>
					<asp:Label ID="lblYourName" Runat="server" CssClass="Text Header"/>
				</td>
				<td>
					<asp:TextBox ID="txtYourName" Runat="server" CssClass="Field100" MaxLength="100"/>
				</td>
			</tr>
			<tr>
			    <td/>
				<td colspan="2">
					<asp:RequiredFieldValidator ControlToValidate = "txtYourName" Display="Dynamic" CssClass="Text Alert" Runat="server" ID="vldName" EnableClientScript="False"/>
				</td>
			</tr>
			<tr>
                <td>
                    <span class="Text Required">&nbsp;*&nbsp;</span>
                </td>
				<td>
					<asp:Label ID="lblYourEmail" Runat="server" CssClass="Text Header"/>
				</td>
				<td>
					<asp:TextBox ID="txtYourEmail" Runat="server" CssClass="Field100" MaxLength="100"/>
				</td>
			</tr>
			<tr>
			    <td/>
				<td colspan="2">
					<asp:RequiredFieldValidator ControlToValidate = "txtYourEmail" Display="Dynamic" CssClass="Text Alert" Runat="server" ID="vldEmailAddress" EnableClientScript="False"/>
					<asp:CustomValidator ControlToValidate="txtYourEmail" Display="Dynamic" CssClass="Text Alert" Runat="server" ID="vldValidEmail" EnableClientScript="False"/>
				</td>
			</tr>
			<tr>
                <td>
                    <span>&nbsp;</span>
                </td>
				<td>
					<asp:Label ID="lblMessage" Runat="server" CssClass="Text RequiredHeader"/>
				</td>
				<td align="right">
					<asp:CheckBox ID="chkSendCopy" Runat="server" CssClass="Text"/>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="2">
					<asp:TextBox ID="txtEmailText" Runat="server" TextMode="MultiLine" Rows="5" CssClass="Multiline"/>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="right">
                    <img src="images/buttons/send_pressed.gif" style="display: none;">
                    <img src="images/buttons/cancel_pressed.gif" style="display: none;">
					<asp:ImageButton Runat="server" ID="btnOK"
						CausesValidation="False"
						ImageUrl="images/buttons/send.gif"
                        onmouseup="src='images/buttons/send.gif';"
                        onmousedown="src='images/buttons/send_pressed.gif';"
                        onmouseout="src='images/buttons/send.gif';"
                        style="margin-left:20px;"
                    />
                    <asp:ImageButton Runat="server" ID="btnClose"
						CausesValidation="False"
  						ImageUrl="images/buttons/cancel.gif"
                        onmouseup="src='images/buttons/cancel.gif';"
                        onmousedown="src='images/buttons/cancel_pressed.gif';"
                        onmouseout="src='images/buttons/cancel.gif';"
                    />
				</td>
			</tr>
			<tr>
				<td/>
				<td colspan="2">
					<asp:Label ID="lblSendMail" Runat="server" CssClass="Text Required" Visible="False"/>
					<asp:Label ID="lblError" Runat="server" CssClass="Text Alert" Visible="False"/>
				</td>
			</tr>
		</table>
    </form>
  </body>
</html>

