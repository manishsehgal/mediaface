<%@ Page language="c#" Codebehind="Login.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.Login" %>
<%@ Register TagPrefix="dol" TagName="BannerControl" Src="Controls/BannerControl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
    <HEAD>
        <title>MediaFace Online - Login</title>
        <meta name="keywords" content="Neato, NEATO CD/DVD Labels and Inserts, Neato iPod Wraps, LightScribe, direct to cd printing, Canon InkJet printers, Epson InkJet printers" />
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <link href="css/style.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" language="javascript" src="js/Login.js"></script>
    </HEAD>
    <body MS_POSITIONING="GridLayout" onload="FocusEmail()">
        <form id="Form1" method="post" runat="server">
			<table>
				<tr>
					<td>
						<dol:BannerControl id="ctlBanner1" runat="server" LEFT="8" WIDTH="890" TOP="65" HEIGHT="95" ShowBorder="False" BannerId="1"/>
					</td>
				</tr>
				<tr>
					<td>
						<div style="padding-left:170px;">
							<asp:Literal ID="ctlHeaderHtml" Runat="server"/>
							<div id="root">
								<div id="content-sidebar-container">
									<!-- content -->
									<div id="content">
										<div class="box">
											<div class="label-caption">
												<div class="decor-t">
													<div></div>
												</div>
												<h2 id="ctlHeaderCaption" runat="server"/>
											</div>
											<div class="decor-bw">
												<div></div>
											</div>
											<div class="content" style="text-align:center">
												<table cellpadding="0" cellspacing="0" style="margin:auto">
													<tr>
														<td>&nbsp;</td>
													</tr>
													<tr>
														<td style="width:40%; text-align:right; padding-right:25px;">
															<asp:Label Runat="server" ID="lblEmail" CssClass="LeftPanelText" />
														</td>
														<td style="width:60%">
															<asp:TextBox Runat="server" ID="txtEmail" CssClass="LoginInput" TabIndex="20"/>
														</td>
													</tr>
													<tr>
														<td style="text-align:right; padding-right:25px;">
															<asp:Label Runat="server" ID="lblPassword" CssClass="LeftPanelText"/>
														</td>
														<td>
															<asp:TextBox Runat="server" ID="txtPassword" TextMode="Password" CssClass="LoginInput" TabIndex="21"/>
														</td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">
															<img src="Images/Buttons/Question.GIF" />
															<asp:HyperLink Runat="server" ID="hypForgotPassword" CssClass="ForgotPassword" TabIndex="25"/>
														</td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">
															<asp:CustomValidator Runat="server" ID="vldLogin" Display="Static" CssClass="LeftPanelText Validation"/>
														</td>
													</tr>
													<tr>
														<td>&nbsp;</td>
													</tr>
													<tr>
														<td style="text-align:right">
															<asp:ImageButton Runat="server" ID="btnCancel" CausesValidation="False"
																ImageUrl="images/buttons/cancel.gif"
																onmouseup="src='images/buttons/cancel.gif';"
																onmousedown="src='images/buttons/cancel_pressed.gif';"
																onmouseout="src='images/buttons/cancel.gif';"
																TabIndex="23"
																CssClass="button"/>
																&nbsp;&nbsp;&nbsp;
														</td>
														<td style="text-align:left">
															&nbsp;&nbsp;&nbsp;
															<asp:ImageButton Runat="server" ID="btnLogin" CausesValidation="False" ImageUrl="Images/buttons/login.gif"
																onmouseup="src='Images/buttons/login.gif'"
																onmousedown="src='Images/buttons/login_pressed.gif'"
																onmouseout="src='Images/buttons/login.gif'"
																TabIndex="22"
																CssClass="button"/>
														</td>
													</tr>
												</table>
											</div>
											<div class="decor-b">
												<div></div>
											</div>
										</div>
									</div>
									<!-- end of content -->    
								</div>
							</div>    
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<dol:BannerControl id="ctlBanner2" runat="server" LEFT="8" WIDTH="890" TOP="422" HEIGHT="95" ShowBorder="False" BannerId="2"/>
					</td>
				</tr>
			</table>
        </form>
    </body>
</HTML>
