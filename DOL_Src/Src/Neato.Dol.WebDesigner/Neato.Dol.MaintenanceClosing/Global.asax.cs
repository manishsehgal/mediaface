using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.IO;
using System.Web.SessionState;

using Neato.Dol.Entity;

namespace Neato.Dol.MaintenanceClosing 
{
	public class Global : System.Web.HttpApplication
	{
		private System.ComponentModel.IContainer components = null;

		public Global()
		{
			InitializeComponent();
		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{

		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{
            HttpContext context = HttpContext.Current;
            string closePageUrl = Constants.ClosePageFileName;
            
            if (File.Exists(Path.Combine(Request.PhysicalApplicationPath, closePageUrl)))
                closePageUrl = Path.Combine(Request.ApplicationPath, closePageUrl);
            else
                closePageUrl = Path.Combine(Request.ApplicationPath, Constants.ClosePageFileName);

            Response.Redirect(closePageUrl);
      	}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_Error(Object sender, EventArgs e)
		{

		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{

		}
			
		#region Web Form Designer generated code
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

