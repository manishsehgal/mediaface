using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Dol.WebDesigner.Controls;

namespace Neato.Dol.WebDesigner {
    public class Lang : Page {
        protected DropDownList cboLang;
        protected ImageButton btnSubmit;
        protected AppVersion ctlVersion;

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Lang_DataBinding);
            this.btnSubmit.Click += new ImageClickEventHandler(btnSubmit_Click);
        }
        #endregion

        private struct LangInfo {
            private string codeValue;
            private string nameValue;

            public string Code {
                get { return codeValue; }
                set { codeValue = value; }
            }

            public string Name {
                get { return nameValue; }
                set { nameValue = value; }
            }

            public LangInfo(string code, string name) {
                this.codeValue = code;
                this.nameValue = name;
            }
        }

        private void Lang_DataBinding(object sender, EventArgs e) {
            LangInfo[] languageCodes = new LangInfo[] {
                new LangInfo("af", "Afrikaans"),
                new LangInfo("af-ZA", "Afrikaans - South Africa"),
                new LangInfo("sq", "Albanian"),
                new LangInfo("sq-AL", "Albanian - Albania"),
                new LangInfo("ar", "Arabic"),
                new LangInfo("ar-DZ", "Arabic - Algeria"),
                new LangInfo("ar-BH", "Arabic - Bahrain"),
                new LangInfo("ar-EG", "Arabic - Egypt"),
                new LangInfo("ar-IQ", "Arabic - Iraq"),
                new LangInfo("ar-JO", "Arabic - Jordan"),
                new LangInfo("ar-KW", "Arabic - Kuwait"),
                new LangInfo("ar-LB", "Arabic - Lebanon"),
                new LangInfo("ar-LY", "Arabic - Libya"),
                new LangInfo("ar-MA", "Arabic - Morocco"),
                new LangInfo("ar-OM", "Arabic - Oman"),
                new LangInfo("ar-QA", "Arabic - Qatar"),
                new LangInfo("ar-SA", "Arabic - Saudi Arabia"),
                new LangInfo("ar-SY", "Arabic - Syria"),
                new LangInfo("ar-TN", "Arabic - Tunisia"),
                new LangInfo("ar-AE", "Arabic - United Arab Emirates"),
                new LangInfo("ar-YE", "Arabic - Yemen"),
                new LangInfo("hy", "Armenian"),
                new LangInfo("hy-AM", "Armenian - Armenia"),
                new LangInfo("az", "Azeri"),
                new LangInfo("az-AZ-Cyrl", "Azeri (Cyrillic) - Azerbaijan"),
                new LangInfo("az-AZ-Latn", "Azeri (Latin) - Azerbaijan"),
                new LangInfo("eu", "Basque"),
                new LangInfo("eu-ES", "Basque - Basque"),
                new LangInfo("be", "Belarusian"),
                new LangInfo("be-BY", "Belarusian - Belarus"),
                new LangInfo("bg", "Bulgarian"),
                new LangInfo("bg-BG", "Bulgarian - Bulgaria"),
                new LangInfo("ca", "Catalan"),
                new LangInfo("ca-ES", "Catalan - Catalan"),
                new LangInfo("zh-HK", "Chinese - Hong Kong SAR"),
                new LangInfo("zh-MO", "Chinese - Macau SAR"),
                new LangInfo("zh-CN", "Chinese - China"),
                new LangInfo("zh-CHS", "Chinese (Simplified)"),
                new LangInfo("zh-SG", "Chinese - Singapore"),
                new LangInfo("zh-TW", "Chinese - Taiwan"),
                new LangInfo("zh-CHT", "Chinese (Traditional)"),
                new LangInfo("hr", "Croatian"),
                new LangInfo("hr-HR", "Croatian - Croatia"),
                new LangInfo("cs", "Czech"),
                new LangInfo("cs-CZ", "Czech - Czech Republic"),
                new LangInfo("da", "Danish"),
                new LangInfo("da-DK", "Danish - Denmark"),
                new LangInfo("div", "Dhivehi"),
                new LangInfo("div-MV", "Dhivehi - Maldives"),
                new LangInfo("nl", "Dutch"),
                new LangInfo("nl-BE", "Dutch - Belgium"),
                new LangInfo("nl-NL", "Dutch - The Netherlands"),
                new LangInfo("en", "English"),
                new LangInfo("en-AU", "English - Australia"),
                new LangInfo("en-BZ", "English - Belize"),
                new LangInfo("en-CA", "English - Canada"),
                new LangInfo("en-CB", "English - Caribbean"),
                new LangInfo("en-IE", "English - Ireland"),
                new LangInfo("en-JM", "English - Jamaica"),
                new LangInfo("en-NZ", "English - New Zealand"),
                new LangInfo("en-PH", "English - Philippines"),
                new LangInfo("en-ZA", "English - South Africa"),
                new LangInfo("en-TT", "English - Trinidad and Tobago"),
                new LangInfo("en-GB", "English - United Kingdom"),
                new LangInfo("en-US", "English - United States"),
                new LangInfo("en-ZW", "English - Zimbabwe"),
                new LangInfo("et", "Estonian"),
                new LangInfo("et-EE", "Estonian - Estonia"),
                new LangInfo("fo", "Faroese"),
                new LangInfo("fo-FO", "Faroese - Faroe Islands"),
                new LangInfo("fa", "Farsi"),
                new LangInfo("fa-IR", "Farsi - Iran"),
                new LangInfo("fi", "Finnish"),
                new LangInfo("fi-FI", "Finnish - Finland"),
                new LangInfo("fr", "French"),
                new LangInfo("fr-BE", "French - Belgium"),
                new LangInfo("fr-CA", "French - Canada"),
                new LangInfo("fr-FR", "French - France"),
                new LangInfo("fr-LU", "French - Luxembourg"),
                new LangInfo("fr-MC", "French - Monaco"),
                new LangInfo("fr-CH", "French - Switzerland"),
                new LangInfo("gl", "Galician"),
                new LangInfo("gl-ES", "Galician - Galician"),
                new LangInfo("ka", "Georgian"),
                new LangInfo("ka-GE", "Georgian - Georgia"),
                new LangInfo("de", "German"),
                new LangInfo("de-AT", "German - Austria"),
                new LangInfo("de-DE", "German - Germany"),
                new LangInfo("de-LI", "German - Liechtenstein"),
                new LangInfo("de-LU", "German - Luxembourg"),
                new LangInfo("de-CH", "German - Switzerland"),
                new LangInfo("el", "Greek"),
                new LangInfo("el-GR", "Greek - Greece"),
                new LangInfo("gu", "Gujarati"),
                new LangInfo("gu-IN", "Gujarati - India"),
                new LangInfo("he", "Hebrew"),
                new LangInfo("he-IL", "Hebrew - Israel"),
                new LangInfo("hi", "Hindi"),
                new LangInfo("hi-IN", "Hindi - India"),
                new LangInfo("hu", "Hungarian"),
                new LangInfo("hu-HU", "Hungarian - Hungary"),
                new LangInfo("is", "Icelandic"),
                new LangInfo("is-IS", "Icelandic - Iceland"),
                new LangInfo("id", "Indonesian"),
                new LangInfo("id-ID", "Indonesian - Indonesia"),
                new LangInfo("it", "Italian"),
                new LangInfo("it-IT", "Italian - Italy"),
                new LangInfo("it-CH", "Italian - Switzerland"),
                new LangInfo("ja", "Japanese"),
                new LangInfo("ja-JP", "Japanese - Japan"),
                new LangInfo("kn", "Kannada"),
                new LangInfo("kn-IN", "Kannada - India"),
                new LangInfo("kk", "Kazakh"),
                new LangInfo("kk-KZ", "Kazakh - Kazakhstan"),
                new LangInfo("kok", "Konkani"),
                new LangInfo("kok-IN", "Konkani - India"),
                new LangInfo("ko", "Korean"),
                new LangInfo("ko-KR", "Korean - Korea"),
                new LangInfo("ky", "Kyrgyz"),
                new LangInfo("ky-KZ", "Kyrgyz - Kazakhstan"),
                new LangInfo("lv", "Latvian"),
                new LangInfo("lv-LV", "Latvian - Latvia"),
                new LangInfo("lt", "Lithuanian"),
                new LangInfo("lt-LT", "Lithuanian - Lithuania"),
                new LangInfo("mk", "Macedonian"),
                new LangInfo("mk-MK", "Macedonian - FYROM"),
                new LangInfo("ms", "Malay"),
                new LangInfo("ms-BN", "Malay - Brunei"),
                new LangInfo("ms-MY", "Malay - Malaysia"),
                new LangInfo("mr", "Marathi"),
                new LangInfo("mr-IN", "Marathi - India"),
                new LangInfo("mn", "Mongolian"),
                new LangInfo("mn-MN", "Mongolian - Mongolia"),
                new LangInfo("no", "Norwegian"),
                new LangInfo("nb-NO", "Norwegian (Bokmal) - Norway"),
                new LangInfo("nn-NO", "Norwegian (Nynorsk) - Norway"),
                new LangInfo("pl", "Polish"),
                new LangInfo("pl-PL", "Polish - Poland"),
                new LangInfo("pt", "Portuguese"),
                new LangInfo("pt-BR", "Portuguese - Brazil"),
                new LangInfo("pt-PT", "Portuguese - Portugal"),
                new LangInfo("pa", "Punjabi"),
                new LangInfo("pa-IN", "Punjabi - India"),
                new LangInfo("ro", "Romanian"),
                new LangInfo("ro-RO", "Romanian - Romania"),
                new LangInfo("ru", "Russian"),
                new LangInfo("ru-RU", "Russian - Russia"),
                new LangInfo("sa", "Sanskrit"),
                new LangInfo("sa-IN", "Sanskrit - India"),
                new LangInfo("sr-SP-Cyrl", "Serbian (Cyrillic) - Serbia"),
                new LangInfo("sr-SP-Latn", "Serbian (Latin) - Serbia"),
                new LangInfo("sk", "Slovak"),
                new LangInfo("sk-SK", "Slovak - Slovakia"),
                new LangInfo("sl", "Slovenian"),
                new LangInfo("sl-SI", "Slovenian - Slovenia"),
                new LangInfo("es", "Spanish"),
                new LangInfo("es-AR", "Spanish - Argentina"),
                new LangInfo("es-BO", "Spanish - Bolivia"),
                new LangInfo("es-CL", "Spanish - Chile"),
                new LangInfo("es-CO", "Spanish - Colombia"),
                new LangInfo("es-CR", "Spanish - Costa Rica"),
                new LangInfo("es-DO", "Spanish - Dominican Republic"),
                new LangInfo("es-EC", "Spanish - Ecuador"),
                new LangInfo("es-SV", "Spanish - El Salvador"),
                new LangInfo("es-GT", "Spanish - Guatemala"),
                new LangInfo("es-HN", "Spanish - Honduras"),
                new LangInfo("es-MX", "Spanish - Mexico"),
                new LangInfo("es-NI", "Spanish - Nicaragua"),
                new LangInfo("es-PA", "Spanish - Panama"),
                new LangInfo("es-PY", "Spanish - Paraguay"),
                new LangInfo("es-PE", "Spanish - Peru"),
                new LangInfo("es-PR", "Spanish - Puerto Rico"),
                new LangInfo("es-ES", "Spanish - Spain"),
                new LangInfo("es-UY", "Spanish - Uruguay"),
                new LangInfo("es-VE", "Spanish - Venezuela"),
                new LangInfo("sw", "Swahili"),
                new LangInfo("sw-KE", "Swahili - Kenya"),
                new LangInfo("sv", "Swedish"),
                new LangInfo("sv-FI", "Swedish - Finland"),
                new LangInfo("sv-SE", "Swedish - Sweden"),
                new LangInfo("syr", "Syriac"),
                new LangInfo("syr-SY", "Syriac - Syria"),
                new LangInfo("ta", "Tamil"),
                new LangInfo("ta-IN", "Tamil - India"),
                new LangInfo("tt", "Tatar"),
                new LangInfo("tt-RU", "Tatar - Russia"),
                new LangInfo("te", "Telugu"),
                new LangInfo("te-IN", "Telugu - India"),
                new LangInfo("th", "Thai"),
                new LangInfo("th-TH", "Thai - Thailand"),
                new LangInfo("tr", "Turkish"),
                new LangInfo("tr-TR", "Turkish - Turkey"),
                new LangInfo("uk", "Ukrainian"),
                new LangInfo("uk-UA", "Ukrainian - Ukraine"),
                new LangInfo("ur", "Urdu"),
                new LangInfo("ur-PK", "Urdu - Pakistan"),
                new LangInfo("uz", "Uzbek"),
                new LangInfo("uz-UZ-Cyrl", "Uzbek (Cyrillic) - Uzbekistan"),
                new LangInfo("uz-UZ-Latn", "Uzbek (Latin) - Uzbekistan"),
                new LangInfo("vi", "Vietnamese"),
                new LangInfo("vi-VN", "Vietnamese - Vietnam")
            };

            cboLang.DataSource = languageCodes;
            cboLang.DataValueField = "Code";
            cboLang.DataTextField = "Name";

            ctlVersion.Version = Global.Version;
            ctlVersion.Title = "build ";

        }

        private void btnSubmit_Click(object sender, ImageClickEventArgs e) {
            StorageManager.CurrentLanguage = cboLang.SelectedValue;
            Response.Redirect(DefaultPage.Url().AbsoluteUri);
        }

        public override void DataBind() {
            base.DataBind();
            cboLang.Items.FindByValue("en").Selected = true;
        }
    }
}