using System;
using System.Web.UI;

namespace Neato.Dol.WebDesigner {
    public class StandardPageTemplate : UserControl {
        private void Page_Load(object sender, EventArgs e) {}

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}