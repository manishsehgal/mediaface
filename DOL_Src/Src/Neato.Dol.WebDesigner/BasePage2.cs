using System;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Controls;

namespace Neato.Dol.WebDesigner {
    public abstract class BasePage2 : BasePage {
        protected Header2 ctlHeader;
        protected Footer ctlFooter;
        
        protected HtmlGenericControl ctlHeaderCaption;
        protected Literal ctlHeaderHtml;

        protected string headerText = string.Empty;
        protected string headerHtml = string.Empty;

        protected string pageTemplateUrl = BasePageTemplate.Url().AbsolutePath;
        public static  string PageName() {
            return "default.aspx";
        }
        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            LoadCustomTemplate(pageTemplateUrl);

            ctlHeader = FindControl("ctlHeader") as Header2;
            ctlFooter = FindControl("ctlFooter") as Footer;
            ctlHeaderCaption = FindControl("ctlHeaderCaption") as HtmlGenericControl;
            ctlHeaderHtml = FindControl("ctlHeaderHtml") as Literal;

            this.Load += new EventHandler(BasePage2_Load);
            this.DataBinding += new EventHandler(BasePage2_DataBinding);

            if (ctlHeader != null) {
                this.ctlHeader.Logout += new EventHandler(ctlHeader_Logout);
            }
        }
        #endregion
        
        
        private void BasePage2_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        private void BasePage2_DataBinding(object sender, EventArgs e) {
            ctlFooter.DataSourceLeft = BCDynamicLink.GetLinks(PageName(), Place.Footer, Align.Left, "");
            ctlFooter.DataSourceRight = BCDynamicLink.GetLinks(PageName(), Place.Footer, Align.Right, "");

            if (ctlHeaderCaption != null) {
                ctlHeaderCaption.InnerText = headerText;
            }

            if (ctlHeaderHtml != null) {
                ctlHeaderHtml.Text = headerHtml;
            }

            if (rawUrl != null && ctlHeader != null) {
                ctlHeader.ReferralUrl = HttpUtility.HtmlEncode(Request.RawUrl);
            }
        }

        private void ctlHeader_Logout(object sender, EventArgs e) {
            DataBind();
        }
    }
}