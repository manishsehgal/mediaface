function OpenUploadCheck(url) {
    HideContent();
    
    var ctlCheckingContainer = window.parent.frames['ctlCheckingContainer'];
    ctlCheckingContainer.location.href = url;
}

function Check(hdnSizeId, oversizeLink) {
    var hdnSize = document.getElementById(hdnSizeId);
    if (hdnSize != null) {
        if (hdnSize.value == 'oversize') {
            var ctlUploadContaner = window.parent.frames['ctlUploadContaner'];
            ctlUploadContaner.location.href = oversizeLink;
        }
    }
}
