function GetDesignerMovie() {
    var designerObject = document.getElementById('Designer');
    var designerEmbed = document.getElementById('DesignerEmbed');
    return (designerEmbed == null) ? designerObject: designerEmbed;
}

function OpenPdf() {
    var url = document.getElementById('ctlPrintUrl').value;
    OpenWindowUsingBlockerNotifier(url, "CPTPDF");
}

function GoHome() {
    var msg = document.getElementById('ctlLeaveDesignerMessage').value;
    var bConfirmed = window.confirm(msg);
    if (!bConfirmed)
        return;
        
    var url = document.getElementById('ctlHomeUrl').value;
    window.open(url, "_self");
}

function callSetVarSWF(movieid, query){
	var divcontainer = "flash_setvariables_" + movieid;
	if(document.getElementById(divcontainer) == null) {
		var divholder = document.createElement("div");
		divholder.id = divcontainer;
		document.body.appendChild(divholder);
	}
	document.getElementById(divcontainer).innerHTML = "";
	var divinfo = "<embed src='Flash/SetVar.swf' FlashVars='lc="+movieid+"&"+query+"' width='0' height='0' type='application/x-shockwave-flash'></embed>";
	document.getElementById(divcontainer).innerHTML = divinfo;
}
   
function setFlashVariables(movieid, flashquery){
	callSetVarSWF(movieid, "fq="+escape(flashquery));
}

function ResetDesigner(value) {
	if (value == true) {
	    RefreshDesigner();
	}
}

function RefreshDesigner() {
    setFlashVariables("Designer", "cmd=reload");
}

function ChkPSPort(port){
	callSetVarSWF("Designer", "chkPSPort="+port);
}

function ScrolToDesigner() {
    var designerMovie = GetDesignerMovie();
    window.scrollTo(designerMovie.offsetLeft -1, designerMovie.offsetTop - 1);
}

function CheckFlashVer() {
    var result = DetectFlashVer(8, 0, 22);
    if (result) document.getElementById('flashRequiredDiv').style.display = "none";
    else {
		document.getElementById('flashClipDiv').style.visibility = "hidden";
		document.getElementById('flashRequiredDiv').style.display = "block";
	}
}

function SetDuplicatedFlash(isDuplicated) {
    var ctlDuplicatedFlash = document.getElementById('ctlDuplicatedFlash');
    var doPost = false;
    
    if (ctlDuplicatedFlash.value != "checked") {
        doPost = true;
		ctlDuplicatedFlash.value = isDuplicated;
    }
    
    if (isDuplicated == "true") {
        var msg = document.getElementById('ctlDuplicatedDesignerMessage').value;
        alert(msg);
        var url = document.referrer;
        if(url=="") {
           url = document.getElementById('ctlHomeUrl').value;
        }
        window.location.href = url;
        return;
    }

    if (doPost)    
        document.forms[0].submit();
}

function ShowExitPage() {
    var ctlShowExitPage = document.getElementById('ctlShowExitPage');
    if (ctlShowExitPage.value == "true") {
        window.open('ExitPage.aspx','_blank','height=600,width=800,resizable=1,menubar=1,location=1,scrollbars=1,titlebar=1,toolbar=1,status=1,left=200,top=200,');
    }
}

function DesignerLogin() {
    var idDesigner = window.top.document.getElementById('idDesigner');
    idDesigner.style.visibility = "hidden";
    var frmLogin = window.top.document.getElementById('frmLogin');
    frmLogin.style.display = "block";
    frmLogin.contentWindow.location.assign("Login.aspx?DesignerMode=true");
        
    return false;
}

function UpdateSettings() {
    setFlashVariables("Designer", "cmd=update_settings");
    var frmHeader = window.top.document.getElementById('frmHeader');
    var time = new Date();
    frmHeader.contentWindow.location.href = "Header.aspx?time="+time.getTime();
}

function RefreshHeader() {
    var frmHeader = window.top.document.getElementById('frmHeader');
    var time = new Date();
    frmHeader.contentWindow.location.href = "Header.aspx?time="+time.getTime();
}

function LeavingConfirm() {
    var msg = document.getElementById('ctlLeaveDesignerMessage').value;
	return window.confirm(msg);
}

function SaveToClient() {
    var time = new Date();
    var url = document.getElementById('ctlDownloadProjectUrl').value+"?time="+time.getTime();
    location.href = url;
}