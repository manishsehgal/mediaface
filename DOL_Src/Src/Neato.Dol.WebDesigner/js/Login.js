function CloseLogin() {
    var idDesigner = window.top.document.getElementById('idDesigner');
    idDesigner.style.visibility = "visible";
    var frmLogin = window.top.document.getElementById('frmLogin');
    frmLogin.style.display = "none";
    window.top.UpdateSettings();
}

function blockEnter(e) {
    if (navigator.appName == 'Netscape') {
		if (e.which == 13) {
			var elem = e.target;
			var currBtnSearch = document.getElementById(elem.getAttribute("param"));
			currBtnSearch.click();
			return false;
		}
    }
    if (event.keyCode == 13) {
		var elem = event.srcElement;
		if (elem != null && elem.getAttribute("param") != null) {
			var currBtnSearch = document.getElementById(elem.getAttribute("param"));
			currBtnSearch.click();
			return false;
		} else {
			return false;
		}
    }
}

function FocusEmail() {
    try {
        var controlToFocus = document.getElementById('txtEmail'); 
        if (controlToFocus && !controlToFocus.disabled)
            controlToFocus.focus();
    }
    catch(e){
    }
    
}

document.onkeypress = blockEnter;
