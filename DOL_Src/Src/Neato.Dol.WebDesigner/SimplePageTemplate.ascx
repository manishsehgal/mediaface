<%@ Register TagPrefix="dol" TagName="Header2" Src="Controls/Header2.ascx" %>
<%@ Register TagPrefix="dol" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="SimplePageTemplate.ascx.cs" Inherits="Neato.Dol.WebDesigner.SimplePageTemplate" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<link href="css/style2.css" type="text/css" rel="stylesheet">
<div align="left">
	<table>
		<tr>
			<td>
				<dol:Header2 id="ctlHeader" runat="server"/>
			</td>
		</tr>
		<tr>
			<td class="Content">
				<div>
					<asp:Literal ID="ctlHeaderHtml" Runat="server"/>
					<asp:PlaceHolder ID="dynamicContent" Runat="server" />
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<dol:Footer id="ctlFooter" runat="server" />
			</td>
		</tr>
	</table>
</div>