<%@ Register TagPrefix="dol" TagName="Header2" Src="Controls/Header2.ascx" %>
<%@ Register TagPrefix="dol" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PaperSelectPageTemplate.ascx.cs" Inherits="Neato.Dol.WebDesigner.PaperSelectPageTemplate" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<link href="css/style2.css" type="text/css" rel="stylesheet">
<link href="css/style.css" type="text/css" rel="stylesheet">
<div align="left">
	<table>
		<tr>
			<td>
				<dol:Header2 id="ctlHeader" runat="server"/>
			</td>
		</tr>
		<tr>
			<td class="Content">
				<div>
					<asp:Literal ID="ctlHeaderHtml" Runat="server"/>
					<div id="root">
						<div id="content-sidebar-container" style="width:100%">
							<!-- content -->
							<div id="content">
								<div class="box grey">
									<div class="label-caption">
										<div class="decor-t">
											<div></div>
										</div>
										<h2 id="ctlHeaderCaption" runat="server"/>
									</div>
									<div class="decor-bw">
										<div></div>
									</div>
									<div>
										<asp:PlaceHolder ID="dynamicContent" Runat="server" />
									</div>
									<div class="decor-b">
										<div></div>
									</div>
								</div>
							</div>
						</div>
						<!-- end of content -->
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<dol:Footer id="ctlFooter" runat="server" />
			</td>
		</tr>
	</table>
</div>
