<%@ Register TagPrefix="cpt" TagName="FooterMenu" Src="Controls/FooterMenu.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="StepPageTemplate.ascx.cs" Inherits="Neato.Dol.WebDesigner.StepPageTemplate" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<table align="center" border="0" width="990" cellspacing="0" bgcolor="black" cellpadding="0" style="border-collapse:collapse;">
    <tr>
        <td valign="top">
          <div align=left style="overflow: hidden;">
            <asp:PlaceHolder ID="content" Runat="server"/>
          </div>
        </td>
    </tr>
    <tr>
        <td bgcolor="#666666">
          <div class="FooterMenu" style="overflow: hidden;">
            <cpt:FooterMenu id="ctlFooterMenu" runat="server"/>
          </div>
        </td>
    </tr>
</table>

