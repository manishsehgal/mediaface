using System;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Tracking;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebDesigner {
    public class SelectRetailer : BasePage2 {
        protected Label lblUnexpectedError;
        protected DataList lstItems;

        public Retailer[] retailers {
            get { return (Retailer[])ViewState["Retailers"]; }
            set { ViewState["Retailers"] = value; }
        }

        #region Url
        private const string RawUrl = "SelectRetailer.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        new public static string PageName() {
            return RawUrl;
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            pageTemplateUrl = PaperSelectPageTemplate.Url().AbsolutePath;

            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(SelectRetailer_DataBinding);
            this.lstItems.ItemDataBound += new DataListItemEventHandler(lstItems_ItemDataBound);
            this.lstItems.ItemCommand += new DataListCommandEventHandler(lstItems_ItemCommand);
        }
        #endregion

        private void SelectRetailer_DataBinding(object sender, EventArgs e) {
            headerText = SelectRetailerStrings.TextCaption();
            headerHtml = string.Empty;

            retailers = BCRetailer.Enum();
            lstItems.DataSource = retailers;
        }

        private void lstItems_ItemDataBound(object sender, DataListItemEventArgs e) {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem) {

                Retailer retailer = (Retailer)e.Item.DataItem;
                
                ImageButton btnRetailer = (ImageButton) e.Item.FindControl("btnRetailer");
                btnRetailer.ImageUrl = IconHandler.ImageUrl(retailer).AbsoluteUri;
                
                Label lblRetailer = (Label) e.Item.FindControl("lblRetailer");
                lblRetailer.Text = retailer.Name;
                
                HyperLink hypRetailer = (HyperLink) e.Item.FindControl("hypRetailer");
                string url = retailer.Url;
                if (url.IndexOf("www.") == -1) url = "www." + url;
                if (url.IndexOf("http://") == -1) url = "http://" + url;
                hypRetailer.Text = retailer.Url;
                hypRetailer.NavigateUrl = url;
            }
        }

        private void lstItems_ItemCommand(object source, DataListCommandEventArgs e) {
            if (StorageManager.NewCustomer == null) return;

            Retailer retailer = retailers[e.Item.ItemIndex];
            StorageManager.NewCustomer.RetailerId = retailer.Id;
            StorageManager.CurrentCustomer = StorageManager.NewCustomer;
            CustomerGroup customerGroup = StorageManager.CurrentCustomer.Group;

            try {
                BCCustomer.ActivateCustomer(
                    StorageManager.CurrentCustomer.Email,
                    StorageManager.CurrentCustomer.FirstName,
                    StorageManager.CurrentCustomer.LastName,
                    StorageManager.CurrentCustomer.Password,
                    StorageManager.CurrentCustomer.ReceiveInfo,
                    DefaultPage.Url().AbsoluteUri, retailer, customerGroup);
            
                BCTracking.Add(UserAction.Registration,
                    Request.UserHostAddress);

                Response.Redirect(DefaultPage.Url().PathAndQuery);
            } catch (BaseBusinessException ex) {
                lblUnexpectedError.Text = ex.Message;
                lblUnexpectedError.Visible = true;
                StorageManager.CurrentCustomer = null;
            } 
            finally {
                StorageManager.NewCustomer = null;
            }
        }
    }
}