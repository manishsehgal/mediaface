<%@ Register TagPrefix="cpt" TagName="Header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="cpt" TagName="LinkList" Src="Controls/LinkList.ascx" %>
<%@ Register TagPrefix="cpt" TagName="FooterMenu" Src="Controls/FooterMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="MainMenu" Src="Controls/MainMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="Faq" Src="Controls/Faq.ascx" %>
<%@ Page language="c#" Codebehind="Support.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.Support" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
    <HEAD>
        <title>MediaFace Online - FAQ</title>
        <meta name="keywords" content="Neato, NEATO CD/DVD Labels and Inserts, Neato iPod Wraps, LightScribe, direct to cd printing, Canon InkJet printers, Epson InkJet printers" />
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <link href="css/style.css" type="text/css" rel="stylesheet">
    </HEAD>
    <body MS_POSITIONING="GridLayout">
        <form id="Form1" method="post" runat="server">
			<div style="padding-left:170px;">
				<asp:Literal ID="ctlHeaderHtml" Runat="server"/>
				<div id="root">
					<div id="content-sidebar-container">
					<!-- content -->
						<div id="content">
							<div class="box">
								<div class="label-caption">
									<div class="decor-t">
										<div></div>
									</div>
									<h2 id="ctlHeaderCaption" runat="server"/>
								</div>
								<div class="decor-bw">
									<div></div>
								</div>
								<div class="content">
									<div class="clear"></div>
									<div>
										<div class="Paragraph" id="DynamicContent">
											<asp:Literal id="ctlPageContent" runat="server" />
										</div>
										<div>
											<cpt:Faq id="ctlFaq" runat="server"/>
										</div>
										&nbsp;
									</div>
								</div>
								<div class="decor-b">
									<div></div>
								</div>
							</div>
						</div>
						<!-- end of content -->    
					</div>
				</div>    
			</div>
		<div>
        </form>
    </body>
</HTML>
