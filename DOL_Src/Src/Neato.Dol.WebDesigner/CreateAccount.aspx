<%@ Page language="c#" Codebehind="CreateAccount.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.CreateAccount" %>
<%@ Register TagPrefix="dol" TagName="BannerControl" Src="Controls/BannerControl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
    <HEAD>
        <title>MediaFace Online - Create account</title>
        <meta name="keywords" content="Neato, NEATO CD/DVD Labels and Inserts, Neato iPod Wraps, LightScribe, direct to cd printing, Canon InkJet printers, Epson InkJet printers" />
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <link href="css/style.css" type="text/css" rel="stylesheet">
    </HEAD>
    <body MS_POSITIONING="GridLayout" id="one-column">
        <form id="Form1" method="post" runat="server">
			<table>
				<tr>
					<td>
						<dol:BannerControl id="ctlBanner1" runat="server" LEFT="8" WIDTH="890" TOP="65" HEIGHT="70" BannerId="1"/> 
					</td>
				</tr>
				<tr>
					<td>
						<div id="root">
						<div id="content-sidebar-container">
						<div id="content">

						<div class="box-medium-grey" style="margin-left:12px;">
							<div class="label-caption" id="DIV1">
								<div class="decor-t"><div></div>
								</div>
								<h2 id="lblUpcHeader" runat="server" style="height:22px;"/>
							</div>
							<div class="decor-bw"><div></div>
							</div>
							<div class="content" align="center">
								<div ID="lblUpcInfo" Runat="server"
								class="CreateAccountInfoText"></div>
								<br><br>
								<table border=0 >
									<tr>
										<td height="28px">
											<asp:Label ID="lblUpc" Runat="server" 
											CssClass="CreateAccountTypeText"/>
										</td>
										<td>
											<asp:TextBox ID="txtUpc" Runat="server" CssClass="LoginInput" />
										</td>
									</tr>
									<tr>
										<td/>
										<td align="center">
											<img src="Images/Buttons/Question.GIF" />
											<asp:HyperLink ID="hypUpcHowFind" Runat="server"
											CssClass="CreateAccountHowFoundLink" NavigateUrl="#"
											onclick="javascript:window.open('UpcHowFind.htm','_blank','height=290,width=400,resizable=yes,menubar=no,location=no,left=200,top=200,scrollbars=yes');"/>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<asp:RequiredFieldValidator ControlToValidate = "txtUpc"
											Display="Dynamic" CssClass="Text Alert"
											Runat="server" ID="vldUpcRequired" EnableClientScript="False"/>
											<asp:CustomValidator ControlToValidate="txtUpc" Display="Dynamic"
											Runat="server" ID="vldUpcExist" EnableClientScript="False"
											CssClass="Text Alert"/>
											&nbsp;
										</td>
									</tr>
								</table>
								<br>
								<asp:ImageButton Runat="server" ID="btnSubmitUpc"
								CausesValidation="False" ImageUrl="Images/buttons/submit.gif"
								onmouseup="src='Images/buttons/submit.gif'"
								onmousedown="src='Images/buttons/submit_pressed.gif'"
								onmouseout="src='Images/buttons/submit.gif'"
								CssClass="button"/>
								<br><br><br>
							</div>
							<div class="decor-b"><div></div>
							</div>
						</div>
						<div class="box-medium">
							<div class="label-caption">
								<div class="decor-t"><div></div>
								</div>
								<h2 id="lblSerialHeader" runat="server" style="height:22px;"/>
							</div>
							<div class="decor-bw"><div></div>
							</div>
							<div class="content" align="center">
								<div ID="lblSerialInfo" Runat="server"
								class="CreateAccountInfoText"></div>
								<br><br>
								<table border=0>
									<tr>
										<td height="28px">
											<asp:Label ID="lblSerial" Runat="server"
											Width="30px" CssClass="CreateAccountTypeText"/>
										</td>
										<td>
											<asp:TextBox ID="txtSerial" Runat="server"
											CssClass="LoginInput" />
										</td>
									</tr>
									<tr>
										<td/>
										<td align="center">
											<img src="Images/Buttons/Question.GIF" />
											<asp:HyperLink ID="hypSerialHowFind" Runat="server"
											CssClass="CreateAccountHowFoundLink" NavigateUrl="#"
											onclick="javascript:window.open('{0}','_blank','height=290,width=400,resizable=yes,menubar=no,location=no,left=200,top=200,scrollbars=yes');"/>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<asp:RequiredFieldValidator ControlToValidate = "txtSerial"
											Display="Dynamic" CssClass="Text Alert"
											Runat="server" ID="vldSerialRequired" EnableClientScript="False"/>
											<asp:CustomValidator ControlToValidate="txtSerial" Display="Dynamic"
											Runat="server" ID="vldSerialExist" EnableClientScript="False"
											CssClass="Text Alert"/>
											&nbsp;
										</td>
									</tr>
								</table>
								<br>
								<asp:ImageButton Runat="server" ID="btnSubmitSerial"
								CausesValidation="False" ImageUrl="Images/buttons/submit.gif"
								onmouseup="src='Images/buttons/submit.gif'"
								onmousedown="src='Images/buttons/submit_pressed.gif'"
								onmouseout="src='Images/buttons/submit.gif'"
								CssClass="button"/>
								<br><br><br>
							</div>
							<div class="decor-b"><div></div>
							</div>
						</div>
						<div class="">
						<div class="box-medium-green">
							<div class="label-caption">
								<div class="decor-t"><div></div>
								</div>
								<h2 id="lblBuyNowHeader" runat="server" style="height:22px;"/>
							</div>
							<div class="decor-bw"><div></div>
							</div>
							<div class="content" align="center">
								<div ID="lblBuyNowInfo" Runat="server"
								class="CreateAccountInfoText"></div>
								<br><br>
								<table border=0>
								<tr><td height="28px">&nbsp;</td><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
								<tr><td colspan="2">&nbsp;</td></tr>
								</table>
								<br>
								<asp:ImageButton Runat="server" ID="btnSubmitBuyNow"
								CausesValidation="False" ImageUrl="Images/buttons/submit.gif"
								onmouseup="src='Images/buttons/submit.gif'"
								onmousedown="src='Images/buttons/submit_pressed.gif'"
								onmouseout="src='Images/buttons/submit.gif'"
								CssClass="button"/>
								<br><br><br>
							</div>
							<div class="decor-b"><div></div>
							</div>
						</div>
						</div
						<div class="clear"></div>
			            
						</div>
						</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div>&nbsp;</div>
						<dol:BannerControl id="ctlBanner2" runat="server" LEFT="8" WIDTH="890" TOP="480" HEIGHT="70" BannerId="2"/>
					</td>
				</tr>
			</table>
        </form>
    </body>
</HTML>