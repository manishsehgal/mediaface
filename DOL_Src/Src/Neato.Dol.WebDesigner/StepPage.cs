using System;
using System.Diagnostics;
using System.Web.UI;
using System.Web.UI.HtmlControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Controls;

namespace Neato.Dol.WebDesigner {
    public class StepPage : BasePage {
        protected FooterMenu ctlFooterMenu;
        
        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            LoadTemplate();
            this.DataBinding += new EventHandler(StepPage_DataBinding);
        }
        #endregion

        void LoadTemplate() {
            Control htmlForm = null;
            foreach (Control control in this.Controls) {
                if (control is HtmlForm) {
                    htmlForm = control;
                    break;
                }
            }

            Debug.Assert(htmlForm != null);
            if (htmlForm == null)
                return;

            Control pageTemplate = null;
            pageTemplate = LoadControl("StepPageTemplate.ascx");

            Debug.Assert(pageTemplate != null);
            if (pageTemplate == null)
                return;

            pageTemplate.DataBind();

            Control content = pageTemplate.FindControl("content");
            Debug.Assert(content != null);
            if (content == null)
                return;

            int index = pageTemplate.Controls.IndexOf(content);

            for (int i = index - 1; i >= 0; --i)
                htmlForm.Controls.AddAt(0, pageTemplate.Controls[i]);
            
            while(pageTemplate.Controls.Count > 1)
                htmlForm.Controls.Add(pageTemplate.Controls[1]);
        }

        private void StepPage_DataBinding(object sender, EventArgs e) {
            string culture = StorageManager.CurrentLanguage;
            ctlFooterMenu = FindControl("ctlFooterMenu") as FooterMenu;
            ctlFooterMenu.DataSourceLeft = BCDynamicLink.GetLinks(rawUrl, Place.Footer, Align.Left, culture);
            ctlFooterMenu.DataSourceRight = BCDynamicLink.GetLinks(rawUrl, Place.Footer, Align.Right, culture);
        }
    }
}