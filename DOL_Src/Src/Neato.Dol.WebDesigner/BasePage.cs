using System;
using System.Diagnostics;
using System.Web.UI;
using System.Web.UI.HtmlControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;

namespace Neato.Dol.WebDesigner {
    abstract public class BasePage : Page {
        private const string scriptKey="GoogleTracking";
        
        protected string rawUrl;

        private void BasePage_Load(object sender, EventArgs e) {
            GoogleTrackPage trackPage = BCGoogleTracking.GetGoogleTrackPage(rawUrl);
            if (!trackPage.EnableScript)
                return;

            if (!IsClientScriptBlockRegistered(scriptKey))
                RegisterClientScriptBlock(scriptKey, BCGoogleTracking.GetCode());
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(BasePage_Load);
        }
        #endregion

        protected void LoadCustomTemplate(string templateControlUrl) {
            Control htmlForm = null;
            foreach (Control control in this.Controls) {
                if (control is HtmlForm) {
                    htmlForm = control;
                    break;
                }
            }

            Debug.Assert(htmlForm != null);
            if (htmlForm == null)
                return;

            Control pageTemplate = null;
            pageTemplate = LoadControl(templateControlUrl);

            Debug.Assert(pageTemplate != null);
            if (pageTemplate == null)
                return;

            pageTemplate.DataBind(); //TODO: Is this line need?

            Control content = pageTemplate.FindControl("dynamicContent");
            Debug.Assert(content != null);
            if (content == null)
                return;

            int index = pageTemplate.Controls.IndexOf(content);

            for (int i = index - 1; i >= 0; --i)
                htmlForm.Controls.AddAt(0, pageTemplate.Controls[i]);
            
            while(pageTemplate.Controls.Count > 1)
                htmlForm.Controls.Add(pageTemplate.Controls[1]);
            
        }
    }
}