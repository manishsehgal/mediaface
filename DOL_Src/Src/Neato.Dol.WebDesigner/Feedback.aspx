<%@ Page language="c#" Codebehind="Feedback.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.Feedback" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>MediaFace Online - Feedback</title>
    <meta name="keywords" content="Neato, NEATO CD/DVD Labels and Inserts, Neato iPod Wraps, LightScribe, direct to cd printing, Canon InkJet printers, Epson InkJet printers" />
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="css/style.css" type="text/css" rel="stylesheet">
  </HEAD>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
		<asp:Panel Runat="server" ID="panFeedbackMessage">
			<table>
				<tr>
					<td class="valueTblRow" colspan="3">
						<asp:Label ID="lblCaption" Runat="server"/>
					</td>
				</tr>
				<tr>
					<td class="valueTblRow" colspan="3">
						<asp:Label ID="lblRequired" Runat="server" CssClass="requiredText"/>
					</td>
				</tr>
				<tr>
					<td class="valueTblRow">
						<asp:Label ID="lblYourEmail" Runat="server"/>
					</td>
					<td class="valueTblRow" width="50%">
						<asp:TextBox ID="txtYourEmail" Runat="server" CssClass="inputTblField" MaxLength="100"/>
					</td>
					<td>
					    <span class="requiredText" runat="server" ID="Span1">&nbsp;*</span>
					</td>
				</tr>
				<tr>
				    <td/>
					<td class="valueTblRow">
						<asp:RequiredFieldValidator ControlToValidate = "txtYourEmail" Display="Dynamic" CssClass="alert" 
							Runat="server" ID="vldEmailAddress" EnableClientScript="False"/>
						<asp:CustomValidator ControlToValidate="txtYourEmail" Display="Dynamic" CssClass="alert"
							Runat="server" ID="vldValidEmail" EnableClientScript="False"/>
					</td>
					<td/>
				</tr>
				<tr>
					<td class="captionTblRow" colspan="3">
						<asp:Label ID="lblMessage" Runat="server"/>
					</td>
				</tr>
				<tr>
					<td class="valueTblRow" colspan="2">
						<asp:TextBox ID="txtEmailText" Runat="server" TextMode="MultiLine" Rows="10" CssClass="textareaTblField"/>
					</td>
					<td/>
				</tr>
				<tr>
					<td class="valueTblRow" align="right" colspan="2">
						<asp:ImageButton Runat="server" ID="btnSend"
							CausesValidation="False"
							ImageUrl="Images/buttons/send.gif"
							onmouseup="src='images/buttons/send.gif';"
                            onmousedown="src='images/buttons/send_pressed.gif';"
                            onmouseout="src='images/buttons/send.gif';"
                            CssClass="button"/>
                        <img src="images/buttons/send_pressed.gif" style="display: none;">
						<asp:ImageButton Runat="server" ID="btnCancel"
							CausesValidation="False"
							ImageUrl="Images/buttons/cancel.gif"
							onmouseup="src='images/buttons/cancel.gif';"
                            onmousedown="src='images/buttons/cancel_pressed.gif';"
                            onmouseout="src='images/buttons/cancel.gif';"
                            style="margin-left:20px;"
                            CssClass="button"/>
					</td>
					<td/>
				</tr>
			</table>
		</asp:Panel>
		<asp:Panel Runat="server" ID="panFeedbackAnswer">
			<table>
				<tr>
					<td>
						<asp:Label ID="lblThank" Runat="server"/>
					</td>
				</tr>
				<tr>
					<td>
						<asp:ImageButton Runat="server" ID="btnClose"
							CausesValidation="False"
							ImageUrl="images/buttons/Ok.gif"
                            onmouseup="src='images/buttons/Ok.gif';"
                            onmousedown="src='images/buttons/Ok_pressed.gif';"
                            onmouseout="src='images/buttons/Ok.gif';"
                            CssClass="button"/>
					</td>
				</tr>
			</table>
		</asp:Panel>
    </form>
  </body>
</HTML>
