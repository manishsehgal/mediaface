using System;
using System.Globalization;
using System.Threading;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Tracking;
using Neato.Dol.WebDesigner.Controls;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebDesigner {
	public class Designer : BasePage2 {
		protected HtmlInputHidden ctlDuplicatedFlash;
        protected HtmlInputHidden ctlDownloadProjectUrl;
		protected HtmlInputHidden ctlHomeUrl;
		protected HtmlInputHidden ctlLeaveDesignerMessage;
        protected HtmlInputHidden ctlDuplicatedDesignerMessage;
        protected HtmlInputHidden ctlLocalConnectionId;
        protected HtmlInputHidden ctlShowExitPage;
        protected PopUpBlockerNotifier ctlNotifier;
		protected Label lblPersonalizeDesign;
		protected Label lblFlashRequired;
        
		#region Url
		private const string RawUrl = "Designer.aspx";
		private const string PaperIdParamName = "PaperId";
        private const string DeviceIdParamName = "DeviceId";

		public static Uri Url() {
			return UrlHelper.BuildUrl(RawUrl);
		}

		public static Uri Url(PaperBase paper) {
            return UrlHelper.BuildUrl(RawUrl, PaperIdParamName, paper.Id);
        }
        public static Uri Url(PaperBase paper,DeviceBase device) 
        {
            return UrlHelper.BuildUrl(RawUrl, PaperIdParamName, paper.Id, DeviceIdParamName, device.Id);
        }
		#endregion
        
        new public static string PageName() {
            return RawUrl;
        }
		private void Page_Load(object sender, EventArgs e) {
			if (StorageManager.CurrentProject == null && PaperId == UndefinedPaperId) {
				Response.ClearContent();
				Response.ClearHeaders();
				Response.Redirect(DefaultPage.Url().PathAndQuery, true);
			}
            
//			Response.Cache.SetCacheability(HttpCacheability.NoCache);
//			Response.Cache.SetExpires(DateTime.Now);
//			Response.Cache.SetValidUntilExpires(false);

            PdfCalibration calibration = StorageManager.CookieCalibration;
            StorageManager.SessionCalibration = calibration;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);

            DataBind();
			
            if (ctlDuplicatedFlash.Value == "false") {
                ctlDuplicatedFlash.Value = "checked";
                BCTracking.Add(UserAction.Designer, Request.UserHostAddress);
                if(Request.QueryString[DeviceIdParamName]!= null) {
                    try 
                    {
                        DeviceBase devicebase = new DeviceBase(int.Parse(Request.QueryString[DeviceIdParamName]));
                        if(BCPaper.GetPaperList(devicebase).Length < 1 && BCDevice.GetDevice(devicebase)==null)
                            StorageManager.CurrentDevice = null;
                        else 
                            StorageManager.CurrentDevice = BCDevice.GetDevice(devicebase);
                    }
                    catch
                    {}
                }
                LoadFace();
                StorageManager.ChangeLastEditedPaper(StorageManager.CurrentProject.ProjectPaper);
            }
		}

		#region Web Form Designer generated code
		protected override void OnInit(EventArgs e) {
            pageTemplateUrl = DesignerPageTemplate.Url().AbsolutePath;
			InitializeComponent();
			base.OnInit(e);
		}

		private void InitializeComponent() {
            rawUrl = RawUrl;
			this.Load += new EventHandler(this.Page_Load);
			this.DataBinding += new EventHandler(this.Designer_DataBinding);

		}
		#endregion

		#region Loading project
		private const int UndefinedPaperId = 0;

		private int PaperId {
			get {
				try {
					return int.Parse(Request.QueryString[PaperIdParamName], CultureInfo.InvariantCulture);
				} catch {
					return UndefinedPaperId;
				}
			}
		}

		// TODO: move to business
		private void LoadFace() {
			if (PaperId != UndefinedPaperId) {
                if(StorageManager.GenerateNewProject || StorageManager.CurrentProject == null) {
                    StorageManager.GenerateNewProject = false;
                    StorageManager.CurrentProject = BCProject.GetProject(StorageManager.CurrentCustomer, new PaperBase(PaperId));

                    if (StorageManager.CurrentProject.DeviceType == DeviceType.Lightscribe) {
                        Paper paper = BCPaper.GetPaper(StorageManager.CurrentProject.ProjectPaper);
                        string paperName = paper.Name.ToLower();
                        if (paperName.IndexOf("content") != -1 || paperName.IndexOf("title") != -1) {
                            if (paper.Faces.Length > 0) {
                                Face face = paper.Faces[0];
                                if (face != null) {
                                    Template[] templates = BCTemplate.EnumTemplates(face);
                                    if (templates.Length > 0) {
                                        Template template = templates[0];
                                        if (template != null) {
                                            XmlNode facesNode = StorageManager.CurrentProject.ProjectXml.SelectSingleNode(@"/Project/Faces");
                                            XmlNode faceNode  = facesNode.SelectSingleNode(@"Face");
                                
                                            XmlDocumentFragment docFrag = StorageManager.CurrentProject.ProjectXml.CreateDocumentFragment();
                                            docFrag.InnerXml = template.XmlValue;
                                
                                            facesNode.ReplaceChild(docFrag, faceNode);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if(StorageManager.CurrentDevice!=null)
                        BCTrackingPhones.Add(StorageManager.CurrentDevice, Request.UserHostAddress);

                    BCTrackingPapers.Add(BCPaper.GetPaper(new PaperBase(PaperId)), Request.UserHostAddress);
                }
			}
		}
		#endregion

		private void Designer_DataBinding(object sender, EventArgs e) {
            ctlDownloadProjectUrl.Value = ProjectFileHandler.UrlDownloadProject().AbsoluteUri;
            ctlHomeUrl.Value = DefaultPage.Url().AbsoluteUri;
			ctlNotifier.Message = DesignerStrings.PopUpBlockerMessage();
			lblFlashRequired.Text = DesignerStrings.FlashRequired();
			ctlLeaveDesignerMessage.Value = DesignerStrings.TextLeaveDesignerMessage();
            ctlDuplicatedDesignerMessage.Value = DesignerStrings.TextDuplicatedDesignerMessage();
            int retailerId = StorageManager.CurrentCustomer == null ? BCRetailer.defaultId : BCRetailer.defaultId;
            ctlShowExitPage.Value = BCRetailer.Get(retailerId).HasExitPage ? "true" : "false";
		}
	}
}