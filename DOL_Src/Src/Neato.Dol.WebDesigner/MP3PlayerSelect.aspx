<%@ Page language="c#" Codebehind="MP3PlayerSelect.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.MP3PlayerSelect" %>
<%@ Register TagPrefix="cpt" TagName="ImageLibrary" Src="Controls/ImageLibrary.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>MediaFace Online - MP3Player Select</title>
    <meta name="keywords" content="Neato, NEATO CD/DVD Labels and Inserts, Neato iPod Wraps, LightScribe, direct to cd printing, Canon InkJet printers, Epson InkJet printers" />
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout" id="one-column">
	<form id="Form1" method="post" runat="server">
		<div class="additional-info">
			<ul class="select-product">
				<li>
					<asp:Label runat="server" ID="lblCategory" />
					<strong class="highlight">
						<asp:Label runat="server" ID="lblCategoryName" />
					</strong>
				</li>
				<li class="switch">
					<asp:HyperLink runat="server" ID="hypSwitchCategory" />
				</li>
				<li>
					<asp:Label runat="server" ID="lblNumberOfItems" />
				</li>
				<li class="split">
					<asp:Label Runat="server" ID="lblSearch" />
				</li>
				<li class="split">
					<asp:TextBox Runat="server" ID="txtSearch" />
				</li>
				<li class="end">
					<asp:ImageButton Runat="server" ID="btnSearch" ImageUrl="Images/buttons/go.gif"
						onmouseup="src='Images/buttons/go.gif'"
						onmousedown="src='Images/buttons/go_pressed.gif'"
						onmouseout="src='Images/buttons/go.gif'"
						CssClass="button"/>
				</li>
			</ul>	
			<div class="clear"></div>
		</div>
		<br />
		<div class="content">
			<div class="clear"></div>
			<cpt:ImageLibrary Runat="server" ID="ctlMP3PlayerSelection" />
			<asp:Label Runat="server" ID="lblNothingFound" />
			<div class="clear"></div>
		</div>
	</form>
  </body>
</html>
