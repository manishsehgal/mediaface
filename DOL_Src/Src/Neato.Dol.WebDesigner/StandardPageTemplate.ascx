<%@ Register TagPrefix="cpt" TagName="AccessToAccount" Src="Controls/AccessToAccount.ascx" %>
<%@ Register TagPrefix="cpt" TagName="Announcements" Src="Controls/Announcements.ascx" %>
<%@ Register TagPrefix="cpt" TagName="FooterMenu" Src="Controls/FooterMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="MainMenu" Src="Controls/MainMenu.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="StandardPageTemplate.ascx.cs" Inherits="Neato.Dol.WebDesigner.StandardPageTemplate" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table align="center" border="0" width="900" cellspacing="0" bgcolor="black" cellpadding="0" style="border-collapse:collapse;">
    <tr>
        <td class="LeftPanel" valign="top" bgcolor="#EA7501">
            <img src="Images/LeftPanelStandard.gif" border="0" GALLERYIMG="no"
                STYLE="position:absolute; z-index:1;">
            <div STYLE="position:absolute; z-index:2;">
                <asp:HyperLink ID="hypHome" Runat="server" ImageUrl="images/logo.gif"
                    STYLE="position:absolute;left:10px;top:15px;" />
                <div class="Account" style="overflow: hidden;">
                <div style="padding-left:5px;">
                    <cpt:AccessToAccount ID="ctlAccessToAccount" runat="server" />
                </div>
                </div>
                <div class="Announcements" style="overflow: hidden;">
                <div style="padding-left:5px;">
                    <cpt:Announcements ID="ctlAnnouncements" runat="server"/>
                </div>
                </div>
            </div>
        </td>
        <td style="width:692px;" valign="top">
          <div class="MainMenu" style="overflow: hidden;" align="right">
            <cpt:MainMenu id="ctlMainMenu" runat="server"/>
          </div>
          <div style="position:relative; top:16px;">
            <img src="Images/StandardBackground.gif" GALLERYIMG="no">
            <asp:Label id="lblHeader" Runat="server" CssClass="HeaderText" style="position:absolute;top:55;left:20"/>
          </div>
          <div align=left style="overflow: hidden; padding: 25px 10px 10px 35px;">
            <asp:PlaceHolder ID="content" Runat="server"/>
          </div>
        </td>
    </tr>
    <tr>
        <td colspan="2" bgcolor="#666666">
          <div class="FooterMenu" style="overflow: hidden;">
            <cpt:FooterMenu id="ctlFooterMenu" runat="server"/>
          </div>
        </td>
    </tr>
</table>
