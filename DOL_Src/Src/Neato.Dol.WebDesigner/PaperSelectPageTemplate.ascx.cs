using System;
using System.Web.UI;

using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner {
    public class PaperSelectPageTemplate : UserControl {
        protected System.Web.UI.WebControls.Literal ctlHeaderHtml;
        protected System.Web.UI.WebControls.PlaceHolder dynamicContent;
        protected System.Web.UI.WebControls.Panel panContent;
        #region Url
        private const string RawUrl = "PaperSelectPageTemplate.ascx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {}

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}