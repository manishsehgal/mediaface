<%@ Page language="c#" Codebehind="Upc.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.Upc" %>
<%@ Register TagPrefix="cpt" TagName="MainMenu" Src="Controls/MainMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="FooterMenu" Src="Controls/FooterMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="Header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="dol" TagName="BannerControl" Src="Controls/BannerControl.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
  <head>
    <title>MediaFace Online - Customer Account</title>
    <meta name="keywords" content="Neato, NEATO CD/DVD Labels and Inserts, Neato iPod Wraps, LightScribe, direct to cd printing, Canon InkJet printers, Epson InkJet printers" />
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" language="javascript" src="js/Login.js"></script>
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
		<table>
			<tr>
				<td>
					<div style="padding-left:170px;">
						<dol:BannerControl id="ctlBanner1" runat="server" LEFT="170" WIDTH="573" TOP="0" HEIGHT="105" ShowBorder="true" BannerId="1"/>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div style="padding-left:170px;">
						<asp:Literal ID="ctlHeaderHtml" Runat="server"/>
						<div id="root">
							<div id="content-sidebar-container">
							<!-- content -->
								<div id="content">
									<div class="box">
										<div class="label-caption">
											<div class="decor-t">
												<div></div>
											</div>
											<h2 id="ctlHeaderCaption" runat="server"/>
										</div>
										<div class="decor-bw">
											<div></div>
										</div>
										<div class="content">
											<div class="clear"></div>
												<table Runat="server" ID="tblCustomer" cellpadding="0" cellspacing="0" class="FormTable" width="480px" border="0" align="center">
													<tr>
														<td colspan="3">
															<asp:Label ID="lblRequired" Runat="server" CssClass="LeftPanelText TextRequired"/>
														</td>
													</tr>
													<tr>
														<td colspan="2">
															<asp:Label ID="lblFirstName" Runat="server" CssClass="LeftPanelText"/>
														</td>
														<td>
															<asp:TextBox ID="txtFirstName" Runat="server" CssClass="Field100" MaxLength="100" TabIndex="1" />                
														</td>
													</tr>
													<tr>
														<td colspan="2">
															<asp:Label ID="lblLastName" Runat="server" CssClass="LeftPanelText"/>
														</td>
														<td>
															<asp:TextBox ID="txtLastName" Runat="server" CssClass="Field100" MaxLength="100" TabIndex="2"/>
														</td>
													</tr>
													<tr>
														<td colspan="2">
															<asp:Label ID="lblEmailAddress" Runat="server" CssClass="LeftPanelText"/>
															<span class="Text Required">&nbsp;*&nbsp;</span>
														</td>
														<td>
															<asp:TextBox ID="txtEmailAddress" Runat="server" CssClass="Field100" MaxLength="100" TabIndex="3"/>
														</td>
													</tr>
													<tr>
														<td colspan="2"/>
														<td>
															<div>
																<asp:RequiredFieldValidator ControlToValidate = "txtEmailAddress" Display="Dynamic" CssClass="LeftPanelText Validation" 
																	Runat="server" ID="vldEmailAddress" EnableClientScript="False"/>
															</div>
															<div>
																<asp:CustomValidator ControlToValidate="txtEmailAddress" Display="Dynamic" CssClass="LeftPanelText Validation"
																	Runat="server" ID="vldValidEmail" EnableClientScript="False"/>
															</div>
															<div>
																<asp:CustomValidator ControlToValidate = "txtEmailAddress" Display="Dynamic" CssClass="LeftPanelText Validation" 
																	Runat="server" ID="vldUniqueEmail" EnableClientScript="False"
																	Text="MFO PE user with specified e-mail have been registered already"/>
															</div>
														</td>
														<td/>
													</tr>
													<tr id="NewPasswordRow">
														<td colspan="2">
															<asp:Label ID="lblNewPassword" Runat="server" CssClass="LeftPanelText"/>
															<span id="spanRequiredNewPassword" runat="server" class="Text Required">&nbsp;*&nbsp;</span>
														</td>
														<td>
															<asp:TextBox ID="txtNewPassword" Runat="server" CssClass="Password" TextMode="Password" MaxLength="100" TabIndex="5"/>
														</td>
													</tr>
													<tr>
														<td colspan="2"/>
														<td>
															<asp:RequiredFieldValidator ControlToValidate = "txtNewPassword" Display="Dynamic" CssClass="LeftPanelText Validation" 
																Runat="server" ID="vldNewPassword" EnableClientScript="False"/>
														</td>
													</tr>
													<tr id="ConfirmPasswordRow">
														<td colspan="2">
															<asp:Label ID="lblConfirmPassword" Runat="server" CssClass="LeftPanelText"/>
															<span id="spanRequiredConfirmPassword" runat="server" class="Text Required">&nbsp;*&nbsp;</span>
														</td>
														<td>
															<asp:TextBox ID="txtConfirmPassword" Runat="server" CssClass="Password" TextMode="Password" MaxLength="100" TabIndex="6"/>
														</td>
													</tr>
													<tr>
														<td colspan="2"/>
														<td>
															<div>
																<asp:RequiredFieldValidator ControlToValidate = "txtConfirmPassword" Display="Dynamic" CssClass="LeftPanelText Validation" 
																	Runat="server" ID="vldConfirmPassword" EnableClientScript="False"/>
															</div>
															<div>
																<asp:CustomValidator Display="Dynamic" CssClass="LeftPanelText Validation" 
																	Runat="server" ID="vldCompareConfirmPassword" EnableClientScript="False"/>
															</div>
														</td>
													</tr>
													<tr>
														<td colspan="2">
															<asp:Label ID="lblUpc" Runat="server" CssClass="LeftPanelText" />
															<span class="Text Required">&nbsp;*&nbsp;</span>
														</td>
														<td>
															<asp:TextBox ID="txtUpc" Runat="server" CssClass="Field100" TabIndex="7" />
														</td>
													</tr>
										            <tr>
											            <td colspan="2"/>
										                <td align="center">
											                <img src="Images/Buttons/Question.GIF" />
											                <asp:HyperLink ID="hypUpcHowFind" Runat="server"
											                CssClass="CreateAccountHowFoundLink" NavigateUrl="#"
											                onclick="javascript:window.open('{0}','_blank','height=290,width=400,resizable=yes,menubar=no,location=no,left=200,top=200,scrollbars=yes');"/>
										                </td>
										            </tr>
													<tr>
														<td colspan="2">
														</td>
														<td>
															<div>
																<asp:RequiredFieldValidator ID="vldUpcRequired" Runat="server" EnableClientScript="False" Display="Dynamic" CssClass="LeftPanelText Validation" ControlToValidate="txtUpc" />
															</div>
															<div>
																<asp:CustomValidator ID="vldUpcExist" Runat="server" EnableClientScript="False" Display="Dynamic" CssClass="LeftPanelText Validation" ControlToValidate="txtUpc" />
															</div>
														</td>
													</tr>
													<tr>
														<td colspan="2">
															<asp:Label ID="lblEmailOptions" Runat="server" CssClass="LeftPanelText"/>
														</td>
														<td>
															<asp:DropDownList ID="cboEmailOptions" Runat="server" CssClass="Field100" width="100px" TabIndex="8"/>
														</td>
													</tr>
													<tr>
														<td colspan="3">
															<asp:Label ID="lblUnexpectedError" Runat="server" CssClass="LeftPanelText Validation" Visible="False"/>
														</td>
													</tr>
													<tr>
														<td colspan="3">
															<asp:CheckBox ID="chkReceiveInfo" Runat="server" CssClass="LeftPanelText" Checked="True" TabIndex="9"/>
														</td>
										            </tr>
										            <tr>
											            <td colspan="3">
												            <asp:Label ID="lblViewPolicy" Runat="server" CssClass="LeftPanelText"/>
											            </td>
													</tr>
													<tr>
														<td colspan="3">&nbsp;</td>
													</tr>
													<tr>
														<td colspan="2"/>
														<td align="right">
															<asp:ImageButton Runat="server" ID="btnCancel" CssClass="button" TabIndex="11"
																ImageUrl="images/buttons/cancel.gif"
																onmouseup="src='images/buttons/cancel.gif';"
																onmousedown="src='images/buttons/cancel_pressed.gif';"
																onmouseout="src='images/buttons/cancel.gif';"
																CausesValidation="False"
																style="margin-left:20px;"/>
															<asp:ImageButton Runat="server" ID="btnOK" CssClass="button" TabIndex="10"
																CausesValidation="False"
																ImageUrl="images/buttons/ok.gif"
																onmouseup="src='images/buttons/ok.gif';"
																onmousedown="src='images/buttons/ok_pressed.gif';"
																onmouseout="src='images/buttons/ok.gif';"/>
														</td>
													</tr>
													</table>
										</div>
										<div class="decor-b">
											<div></div>
										</div>
									</div>
								</div>
								<!-- end of content -->    
							</div>
						</div>    
					</div>
				</td>
			</tr>
		</table>
    </form>
  </body>
</html>