<%@ Page language="c#" Codebehind="PrintSetup.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.PrintSetup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
    <HEAD>
        <title>MediaFace Online - Print Setup</title>
        <meta name="keywords" content="Neato, NEATO CD/DVD Labels and Inserts, Neato iPod Wraps, LightScribe, direct to cd printing, Canon InkJet printers, Epson InkJet printers" />
    </HEAD>
    <body bgcolor=#FFFFFF>
    <center>
        <table runat="server" style="HEIGHT:100%;width:530;color:#3493CD;">
            <tr>
                <td>
                    <h1>
                        <asp:Label ID="lblHdr" Runat="server" />
                    </h1>
                    Make sure you:
                    <ul>
                        <li>Set Print Range to "All"</li>
                        <li>Uncheck resize, rotation and centering options</li>
                        <li>Set Page Scaling to "None"</li>
                        <li>Select Properties and make sure the Orientation (Portrait/Landscape) matches the Orientation of your design in the Adobe Reader window</li>
                        <li>Select Properties and change paper size to A4 if you are printing to our DVD inserts or Slim DVD inserts</li>
                    </ul>
                    Ensure that marked controls in your Adobe Reader's Print Dialog have the same state as on the picture below:
                    <p>
                        <asp:Image ID="imgScreenShot" Runat="server" />
                    </p>
                </td>
            </tr>
        </table>
    </center>
    </body>
</HTML>
