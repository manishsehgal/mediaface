<%@ Page language="c#" Codebehind="TermsOfUse.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.TermsOfUse" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>MediaFace Online - Terms Of Use</title>
    <meta name="keywords" content="Neato, NEATO CD/DVD Labels and Inserts, Neato iPod Wraps, LightScribe, direct to cd printing, Canon InkJet printers, Epson InkJet printers" />
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="css/style.css" type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
		<div style="padding-left:170px;">
			<asp:Literal ID="ctlHeaderHtml" Runat="server"/>
			<div id="root">
				<div id="content-sidebar-container">
				<!-- content -->
					<div id="content">
						<div class="box">
							<div class="label-caption">
								<div class="decor-t">
									<div></div>
								</div>
								<h2 id="ctlHeaderCaption" runat="server"/>
							</div>
							<div class="decor-bw">
								<div></div>
							</div>
							<div class="content">
								<div class="clear"></div>
								<div class="Paragraph" id="DynamicContent">
									<asp:Literal id="ctlPageContent" runat="server"/>
								</div>
							</div>
							<div class="decor-b">
								<div></div>
							</div>
						</div>
					</div>
					<!-- end of content -->    
				</div>
			</div>    
		</div>
    </form>
  </body>
</html>
