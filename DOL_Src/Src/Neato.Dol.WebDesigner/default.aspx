<%@ Page language="c#" Codebehind="default.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.DefaultPage" %>
<%@ Register TagPrefix="dol" TagName="BannerControl" Src="Controls/BannerControl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>MediaFace Online</title>
    <meta name="keywords" content="<%=GetKeywords()%>"/>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <meta name="description" content="Web based design software for NEATO, Avery, Memorex, CD Stomper CD/DVD Labels and Inserts.  Also supports NEATO iPod Wraps, LightScribe Direct Disc Labeling and direct to CD/DVD printing on Epson InkJet printers.">
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" language="javascript" src="js/Cookies.js"></script>
</HEAD>
  <body MS_POSITIONING="GridLayout">
    <form id="HomePageForm" method="post" runat="server">
		<div class="alert" align="center">
        	<script language="javascript">
				<!--
				CheckCookies("<asp:Literal id="litCookieDisabled" runat="Server"/>");
				-->
			</script>
		</div>
		
		<table>
			<tr>
				<td>
					<table>
						<tr>
							<td>
								<dol:BannerControl id="ctlBanner1" runat="server" LEFT="8" WIDTH="145" TOP="64" HEIGHT="155" BannerId="1"/>
							</td>
						</tr>
						<tr>
							<td>
								<dol:BannerControl id="ctlBanner2" runat="server" LEFT="8" WIDTH="145" TOP="229" HEIGHT="162" BannerId="2"/>
							</td>
						</tr>
						<tr>
							<td>
								<dol:BannerControl id="ctlBanner3" runat="server" LEFT="8" WIDTH="145" TOP="401" HEIGHT="111" BannerId="3"/>
							</td>
						</tr>
					</table>
				</td>
				<td>
					<table>
						<tr>
							<td>
								<div>
									<asp:Literal ID="ctlHeaderHtml" Runat="server"/>
									<div id="root">
										<div id="content-sidebar-container">
											<!-- content -->
											<div id="content">
												<div class="box">
													<div class="label-caption">
														<div class="decor-t">
															<div></div>
														</div>
														<h2 id="ctlHeaderCaption" runat="server"/>
													</div>
													<div class="decor-bw">
														<div></div>
													</div>
													<div class="content">
														<div class="clear"></div>
														<asp:DataList ID="lstTypes"
															Runat="server" 
															RepeatDirection="Horizontal"
															RepeatLayout="Table"
															RepeatColumns="3"
															EnableViewState="true">
															<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
															<ItemTemplate>
																<a id="hypCategory" runat="server" class="product" style="text-decoration:none;"> 
																	<div class="decor-t">
																		<div></div>
																	</div>
																	<div>
																		<asp:Image runat="server" id="imgType"/>
																	</div>
																	<div class="decor-bw">
																		<div></div>
																	</div>
																	<div class="label">
																		<h3>
																			<asp:Literal runat="server" id="litText"/>
																		</h3>
																		<div class="decor-b">
																			<div></div>
																		</div>
																	</div>
																</a>
															</ItemTemplate>
														</asp:DataList>
														<div class="clear"></div>
														<div class="clear"></div>
													</div>
													<div class="decor-b">
														<div></div>
													</div>
												</div>
											</div>
											<!-- end of content -->
										</div>
									</div>    
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<dol:BannerControl id="ctlBanner7" runat="server" LEFT="173" WIDTH="559" TOP="462" HEIGHT="105" ShowBorder="false" BannerId="7"/>
							</td>
						</tr>
					</table>
				</td>
				<td>
					<table>
						<tr>
							<td>
								<dol:BannerControl id="ctlBanner4" runat="server" LEFT="749" WIDTH="145" TOP="64" HEIGHT="155" BannerId="4"/>
							</td>
						</tr>
						<tr>
							<td>
								<dol:BannerControl id="ctlBanner5" runat="server" LEFT="749" WIDTH="145" TOP="229" HEIGHT="162" BannerId="5"/>
							</td>
						</tr>
						<tr>
							<td>
								<dol:BannerControl id="ctlBanner6" runat="server" LEFT="749" WIDTH="145" TOP="401" HEIGHT="111" BannerId="6"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
     </form>
  </body>
</HTML>
