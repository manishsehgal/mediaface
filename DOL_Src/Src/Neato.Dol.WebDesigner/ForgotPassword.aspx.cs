using System;
using System.Globalization;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner {
    public class ForgotPassword : BasePage2 {
        protected Label lblDescription;
        protected Label lblEmailAddress;
        protected TextBox txtEmailAddress;
        protected RequiredFieldValidator vldEmailAddress;
        protected CustomValidator vldValidEmail;
        protected CustomValidator vldEmailNotRegistered;
        protected ImageButton btnSend;
        protected ImageButton btnContinue;
        protected Label lblSendMail;
        protected Label lblEmailAddressText;
        protected Label lblEmailAddressValue;
        protected Label lblPasswordText;
        protected Label lblPasswordValue;
        protected Label lblError;
        protected Label lblDescription2;
        protected HtmlTable tblForgotPassword;
        protected HtmlTable tblResetPassword;
        protected HtmlTable tblWrongPasswordUID;
        
        #region Url
        private const string RawUrl = "ForgotPassword.aspx";

        new public static string PageName() {
            return RawUrl;
        }
        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        #region Request values
        private string email {
            get {
                string emailValue = Request[BCMail.Email];
                if (null != emailValue)
                    return emailValue;
                return string.Empty;
            }
        }

        private string pswuid {
            get {
                string pswuidValue = Request[BCMail.Pswuid];
                if (null != pswuidValue)
                    return pswuidValue;
                return string.Empty;
            }
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            if (!IsPostBack) {
                DataBind();
                tblResetPassword.Visible = false;
                if (email != string.Empty && pswuid != string.Empty)
                    ResetPassord();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ForgotPassword_DataBinding);
            this.btnSend.Click += new ImageClickEventHandler(btnSend_Click);
            this.btnContinue.Click += new ImageClickEventHandler(btnContinue_Click);
            this.vldEmailNotRegistered.ServerValidate += new ServerValidateEventHandler(vldEmailNotRegistered_ServerValidate);
            this.vldValidEmail.ServerValidate += new ServerValidateEventHandler(vldValidEmail_ServerValidate);
            this.PreRender += new EventHandler(ForgotPassword_PreRender);
        }
        #endregion

        #region DataBinding
        private void ForgotPassword_DataBinding(object sender, EventArgs e) {
            lblDescription.Text = ForgotPasswordStrings.TextDescription();
            lblDescription2.Text = ForgotPasswordStrings.TextDescription2();
            lblEmailAddress.Text = ForgotPasswordStrings.TextEmailAddress();
            vldEmailAddress.Text = ForgotPasswordStrings.TextEmailAddressRequired();
            vldValidEmail.Text = ForgotPasswordStrings.TextEmailAddressInvalid();
            vldEmailNotRegistered.Text = ForgotPasswordStrings.TextEmailNotRegistered();
            lblEmailAddressText.Text = ForgotPasswordStrings.TextEmailAddress();
            lblPasswordText.Text = ForgotPasswordStrings.TextPasswordText();
            lblSendMail.Visible = false;

            rawUrl = RawUrl;
            headerText = ForgotPasswordStrings.TextForgotPassword();
        }
        #endregion

        private void ResetPassord() {
            tblForgotPassword.Visible = false;
            try {
                BCCustomer.UpdatePasswordUid(email, pswuid);
                Customer customer = BCCustomer.GetCustomerByLogin(email, true);
                lblEmailAddressValue.Text = email;
                lblPasswordValue.Text = customer.Password;
                tblResetPassword.Visible = true;
            } catch {
                tblWrongPasswordUID.Visible = true;
            }
        }

        private void btnSend_Click(object sender, ImageClickEventArgs e) {
            lblSendMail.Visible = false;
            vldValidEmail.Validate();
            if(!vldValidEmail.IsValid) {
                DataBind();
                return;
            }

            vldEmailAddress.Validate();
            vldEmailNotRegistered.Validate();
            if(!(vldEmailAddress.IsValid
                 && vldEmailNotRegistered.IsValid)) {
                DataBind();
                return;
            }

            try {
                string email = txtEmailAddress.Text;
                BCMail.SendForgotPasswordMail(string.Empty, Request.Url.AbsoluteUri,
                    email.Trim(), StorageManager.CurrentLanguage);
                lblSendMail.Text = string.Format(ForgotPasswordStrings.FormatSendMail(txtEmailAddress.Text.Trim()));
                lblSendMail.Visible = true;
            } catch (BusinessException ex) {
                DataBind();
                lblError.Text = ex.Message;
                lblError.Visible = true;
            }
        }

        private void btnContinue_Click(object sender, ImageClickEventArgs e) {
            Response.Redirect(RawUrl);
        }

        private void vldEmailNotRegistered_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = BCCustomer.GetCustomerByLogin(txtEmailAddress.Text.Trim(), true) != null;
        }

        private void vldValidEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = FormatValidator.EnsureValidEmail(txtEmailAddress.Text.Trim());
        }

        private void ForgotPassword_PreRender(object sender, EventArgs e) {
            JavaScriptHelper.RegisterFocusScript(this, txtEmailAddress.ClientID);
        }
    }
}