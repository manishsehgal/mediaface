<%@ Page language="c#" Codebehind="Plugins.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.Plugins" %>
<%@ Register TagPrefix="dol" TagName="BannerControl" Src="Controls/BannerControl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
  <head>
    <title>MediaFace Online - Plugins Download</title>
    <meta name="keywords" content="Neato, NEATO CD/DVD Labels and Inserts, Neato iPod Wraps, LightScribe, direct to cd printing, Canon InkJet printers, Epson InkJet printers" />
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="css/style.css" type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="GridLayout" id="one-column">
	
    <form id="Form1" method="post" runat="server">
    
            <div id="root">
            <div id="content-sidebar-container">
            <div id="content">
    
    
    
<div class="box grey">

	<div class="label-caption">
		<div class="decor-t"><div></div></div>
		<h2>MediaFace Online Plugins</h2>
	</div>
	
	<div class="decor-bw transparent"><div></div></div>

	<div class="content no-margin">
	
	<div class="clear"></div>
	
	<!--
	<div class="panel-plugins">
	  <table>
	    <tr>
	      <td valign="top" width="10%"><img src="Images/PluginEngine.png"></td>
	      <td valign="top" width="50%">
	        <div class="info">
	          <h1>MediaFace Online Plugins Support Core</h1>
	          <p class="status">Not Installed</p>
			  <a href="download/plugins/x86/MediaFaceOnlineCore.exe" class="download">Click here to download and install</a>
			  &nbsp;&nbsp;&nbsp;
			  <a href="javascript:;">Read instructions</a> 
		    </div>
	      </td>
	      <td valign="top" width="40%">
	        <div class="note">
			  <p>MediaFace Online Plugins Support Core is required to run all the MediaFace plugins and extensions.</p>
			  <br>
			  <p><strong>Please <a href="download/plugins/x86/MediaFaceOnlineCore.exe">install</a> it first</strong></p>
		    </div>
	      </td>
	    </tr>
	  </table>
	</div>

    <br>
    -->
	<div class="panel-plugins disabled">
		<table>
	      <tr>
	        <td valign="top" width="10%"><img src="Images/PluginEngine.png"></td>
	        <td valign="top" width="50%">
	          <div class="info">
			    <h1>MediaFace Online Plugins Pack</h1>
			    <!--
			    <p class="status">Not Installed - please install and start Support Core first</p>
			    -->
			    <a href="download/plugins/x86/MediaFaceOnlinePluginsPack.exe" class="download">Click here to download and install</a>
		      </div>
	        </td>
	        <td valign="top" width="40%">
	          <div class="note">
			  <p>MediaFACE Online Plugins offer the following features:<br><br>
                - Enhanced Text Effects like circular text<br>
                - Import iTunes playlists<br>
                - Import Windows Media Playlists<br>
                - Supports AudioCD and CD Text formats
              </p>
			  </div>
	      </td>
	    </tr>
	  </table>
	</div>
	
	<div class="clear"></div>

	</div>
	
	<div class="decor-b"><div></div></div>

</div>

            <div class="clear"></div>
            
            </div>
            </div>
            </div>


     </form>
	
  </body>
</html>
