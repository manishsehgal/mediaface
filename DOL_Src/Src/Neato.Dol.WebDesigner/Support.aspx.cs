using System;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Controls;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner {
    public class Support : BasePage2 {
        protected Literal ctlPageContent;
        protected Faq ctlFaq;
        
        #region Url
        private const string RawUrl = "Support.aspx";

        new public static string PageName() {
            return RawUrl;
        }

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Support_DataBinding);
        }
        #endregion

        #region DataBinding
        private void Support_DataBinding(object sender, EventArgs e) {
            string culture = StorageManager.CurrentLanguage;
            
            LocalizedText pageContent = BCSupportPage.Get(culture);
            if (pageContent == null)
                pageContent = new LocalizedText(culture, string.Empty);

            ctlPageContent.Text = pageContent.Text;
            ctlFaq.DataSource = BCFaq.EnumFaq();

            rawUrl = RawUrl;
            headerText = SupportStrings.TextFaq();
            headerHtml = string.Empty;
        }
        #endregion
    }
}