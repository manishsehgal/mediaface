using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Xml;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner {
    public class Print : Page {
        private const string PdfContentType = "application/pdf";

        #region Url
        private const string RawUrl = "Print.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl, "time", DateTime.Now.Ticks);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Request.InputStream);

            Project project = StorageManager.CurrentProject.Clone();
            project.Xml = xmlDoc;

            byte[] content = BCPrinting.GetPdf(project, Request.PhysicalApplicationPath);
            Response.Clear();
            Response.ClearHeaders();

            Response.ContentType = PdfContentType;
            Response.AddHeader("content-length", content.Length.ToString(CultureInfo.InvariantCulture));
            Response.BinaryWrite(content);
            Response.End();
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
        }
        #endregion
    }
}