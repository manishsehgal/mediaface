using System;
using System.Diagnostics;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Tracking;
using Neato.Dol.WebDesigner.Controls;

namespace Neato.Dol.WebDesigner {
    public class StandardPage : BasePage {
        protected MainMenu ctlMainMenu;
        protected FooterMenu ctlFooterMenu;
        protected Announcements ctlAnnouncements;
        protected AccessToAccount ctlAccessToAccount;
        protected Label lblHeader;
        protected HyperLink hypHome;
        
        protected string headerText=string.Empty;
        protected bool isHome = false;
        
        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void StandardPage_Load(object sender, EventArgs e) {
            this.DataBind();
            if(StorageManager.CurrentCustomer != null) {
                ctlAccessToAccount.UserLogined = true;
                ctlAccessToAccount.CurrentCustomer = StorageManager.CurrentCustomer;
            }
        }

        private void InitializeComponent() {
            LoadTemplate();
            this.Load += new EventHandler(StandardPage_Load);
            this.DataBinding += new EventHandler(StandardPage_DataBinding);

            this.ctlAccessToAccount = FindControl("ctlAccessToAccount") as AccessToAccount;
            this.ctlAccessToAccount.Login += new AccessToAccount.AccessToAccountEventHandler(ctlAccessToAccount_Login);
            this.ctlAccessToAccount.LoginValidate += new AccessToAccount.AccessToAccountEventHandler(ctlAccessToAccount_LoginValidate);
            this.ctlAccessToAccount.Logout += new EventHandler(ctlAccessToAccount_Logout);
            this.ctlAccessToAccount.Redirect += new AccessToAccount.RedirectEventHandler(ctlAccessToAccount_Redirect);
        }
        #endregion

        private void LoadTemplate() {
            Control htmlForm = null;
            foreach (Control control in this.Controls) {
                if (control is HtmlForm) {
                    htmlForm = control;
                    break;
                }
            }

            Debug.Assert(htmlForm != null);
            if (htmlForm == null)
                return;

            Control pageTemplate = null;
            if (isHome)
                pageTemplate = LoadControl("HomePageTemplate.ascx");
            else
                pageTemplate = LoadControl("StandardPageTemplate.ascx");

            Debug.Assert(pageTemplate != null);
            if (pageTemplate == null)
                return;

            pageTemplate.DataBind();

            Control content = pageTemplate.FindControl("content");
            Debug.Assert(content != null);
            if (content == null)
                return;

            int index = pageTemplate.Controls.IndexOf(content);

            for (int i = index - 1; i >= 0; --i)
                htmlForm.Controls.AddAt(0, pageTemplate.Controls[i]);
            
            while(pageTemplate.Controls.Count > 1)
                htmlForm.Controls.Add(pageTemplate.Controls[1]);
            
        }

        private void StandardPage_DataBinding(object sender, EventArgs e) {
            string culture = StorageManager.CurrentLanguage;
            ctlMainMenu = FindControl("ctlMainMenu") as MainMenu;
            ctlMainMenu.DataSource = BCDynamicLink.GetLinks(rawUrl, Place.Header, Align.None, culture);
            ctlFooterMenu = FindControl("ctlFooterMenu") as FooterMenu;
            ctlFooterMenu.DataSourceLeft = BCDynamicLink.GetLinks(rawUrl, Place.Footer, Align.Left, culture);
            ctlFooterMenu.DataSourceRight = BCDynamicLink.GetLinks(rawUrl, Place.Footer, Align.Right, culture);
            ctlAnnouncements = FindControl("ctlAnnouncements") as Announcements;
            ctlAnnouncements.DataSource = BCAnnouncement.GetLastAnnouncements();
            ctlAccessToAccount.NewUserUrl = CustomerAccount.Url().PathAndQuery;
            ctlAccessToAccount.ForgotPasswordUrl = ForgotPassword.Url().PathAndQuery;
            ctlAccessToAccount.ModifyProfileUrl = CustomerAccount.Url().PathAndQuery;
            //ctlAccessToAccount.LearnMoreUrl = LearnMore.Url().PathAndQuery;
            lblHeader = FindControl("lblHeader") as Label;
            if (lblHeader != null)
                lblHeader.Text = headerText;
            hypHome = FindControl("hypHome") as HyperLink;
            hypHome.NavigateUrl = DefaultPage.Url().PathAndQuery;
        }

        private void ctlAccessToAccount_Login(Object sender, AccessToAccountEventArgs e) {
            Customer customer = e.Customer;
            customer = BCCustomer.GetCustomer(customer.Email.Trim(), customer.Password);
            if (customer != null) {
                e.CustomerExists = true;
                e.Customer = customer;
                StorageManager.CurrentCustomer = customer;
            }
        }

        private void ctlAccessToAccount_Logout(object sender, EventArgs e) {
            StorageManager.CurrentCustomer = null;
            BCTracking.Add(UserAction.Logout, Request.UserHostAddress);
        }

        private void ctlAccessToAccount_Redirect(Object sender, RedirectEventArgs e) {
            Response.Redirect(e.NavigateUrl);
        }

        private void ctlAccessToAccount_LoginValidate(Object sender, AccessToAccountEventArgs e) {
            Customer customer = e.Customer;
            e.CustomerExists = BCCustomer.GetCustomer(customer.Email.Trim(), customer.Password) != null;
        }
    }
}
