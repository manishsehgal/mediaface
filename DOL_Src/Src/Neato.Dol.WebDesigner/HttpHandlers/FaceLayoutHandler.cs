using System;
using System.Web;
using System.Web.SessionState;

using Neato.Dol.Business;
using Neato.Dol.Entity;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class FaceLayoutHandler : IHttpHandler, IRequiresSessionState {
        private const string SessionExpired = "Your session is expired!";

        public FaceLayoutHandler() {}

        public void ProcessRequest(HttpContext context) {
            HttpResponse Response = context.Response;

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetValidUntilExpires(false);

            Response.ClearContent();
            Response.ClearHeaders();

            try {
                PaperBase paper = StorageManager.CurrentProject.ProjectPaper;//CookieLastEditedPapers[0];
                string data = BCFaceLayout.EnumerateFaceLayouts(paper).GetXml();
                Response.Write(data);
                Response.ContentType = "text/xml";
            } catch {
                Response.Write(SessionExpired);
            }

            Response.End();
        }

        public bool IsReusable {
            get { return true; }
        }
    }
}