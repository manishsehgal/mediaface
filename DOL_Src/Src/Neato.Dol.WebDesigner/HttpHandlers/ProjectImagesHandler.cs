using System;
using System.Web;
using System.Web.SessionState;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class ProjectImagesHandler : IHttpHandler, IRequiresSessionState {
        private const string SessionExpired = "Your session is expired!";

        #region Url
        private const string RawUrl = "ProjectImage.aspx";
        private const string ModeKey = "mode";
        private const string ImageMode = "image";
        private const string IconMode = "icon";
        private const string IdKey = "id";

        private const string ContrastEffectKey = "contrast";
        private const string BrightnessEffectKey = "brigntness";
        private const string BlurSharpnessEffectKey = "blursharp";
        private const string ImageEffectKey = "effect";
        private const string TemplateIdKey = "TemplateId";
        public static Uri UrlImage(Guid id) {
            return UrlHelper.BuildUrl(RawUrl, ModeKey, ImageMode, IdKey, id.ToString("N"));
        }

        public static Uri UrlIcon(Guid id) {
            return UrlHelper.BuildUrl(RawUrl, ModeKey, IconMode, IdKey, id.ToString("N"));
        }

        private Guid GetIdParam(HttpContext context) {
            try {
                return new Guid(context.Request.QueryString[IdKey]);
            } catch (FormatException) {
                return Guid.Empty;
            } catch (ArgumentNullException) {
                return Guid.Empty;
            }
        }
        private ImageEffectProperties GetEffect(HttpContext context) {
            try {
                ImageEffectProperties props = new ImageEffectProperties();
                props.BlurSharp = int.Parse(context.Request.QueryString[BlurSharpnessEffectKey]);
                props.Brightness = int.Parse(context.Request.QueryString[BrightnessEffectKey]);
                props.Contrast = int.Parse(context.Request.QueryString[ContrastEffectKey]);
                props.Effect =(ImageEffect) ImageEffect.Parse(typeof(ImageEffect),context.Request.QueryString[ImageEffectKey]);
                return props;
            } catch {
                return null;
            }
            

        }
        private Template GetTemplate(HttpContext context) {
            try {
                int templateId = int.Parse(context.Request.QueryString[TemplateIdKey]);
                return new Template(templateId);
            } catch {
                return null	;
            }
        }
        #endregion

        public ProjectImagesHandler() {}

        public void ProcessRequest(HttpContext context) {
            string mode = context.Request.QueryString[ModeKey];
            switch (mode) {
                case ImageMode:
                    this.GetImage(context);
                    break;
                case IconMode:
                    this.GetIcon(context);
                    break;
                default:
                    return;
            }
        }

        public bool IsReusable {
            get { return true; }
        }

        public void GetImage(HttpContext context) 
        {
            HttpResponse Response = context.Response;

            Response.ContentType = "image/jpeg";

            Guid id = GetIdParam(context);
            Template template = GetTemplate(context);
            if(template != null && StorageManager.CurrentProject.GetImage(id).Length < 1) {
                byte[] templImage = BCTemplate.GetMediaByTemplateAndGuid(template, id);
                if(templImage.Length > 0)
                    StorageManager.CurrentProject.AddTemplateImageToProject(templImage, id);
            }
            ImageEffectProperties props = GetEffect(context);
            
            byte[] image = StorageManager.CurrentProject.GetOriginalImage(id);
            image = ImageEffectHelper.ProcessImage(image,props);
            image = ImageHelper.ConvertImageToJpegCond(image);

            Response.OutputStream.Write(image, 0, image.Length);
            Response.End();
        }

        public void GetIcon(HttpContext context) {
            HttpResponse Response = context.Response;
            Guid id = GetIdParam(context);
            try {
                byte[] image = StorageManager.CurrentProject.GetImage(id);
                byte[] icon = ImageHelper.GetIcon(image, 50, 50);
                Response.OutputStream.Write(icon, 0, icon.Length);
                Response.ContentType = "image/jpeg";
            } catch {
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Write(SessionExpired);
            }
            Response.End();
        }
    }
}