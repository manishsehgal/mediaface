using System;
using System.IO;
using System.Web;
using System.Web.SessionState;
using System.Xml;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpModules;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class ExitPageHandler: IHttpHandler, IRequiresSessionState {
        #region Url
        private const string RawUrl = "ExitPage.aspx";
        private const string IdKey = "id";
        private const string ImageKey = "image";
        private const string ActionKey = "action";
        private const string EnumAction = "enum";
        private const string DeleteAction = "delete";
        private const string SkuKey = "Code";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static string NeatoExitPageUrl(string sku) {
            return sku == null ? Configuration.NeatoExitPageUrl : string.Format("{0}?{1}={2}", Configuration.NeatoExitPageUrl, SkuKey, sku);
        }

        public static string UrlForHtmlText(string siteName, int retailerId) {
            string url = string.Format("{0}?{1}={2}",
                Path.Combine(siteName, RawUrl),
                IdKey, retailerId.ToString());
            return CheckLocationModule.AdminUrl(url);
        }

        public static string UrlForUploadImage(string siteName, int retailerId, string imageFile) {
            string url = string.Format("{0}?{1}={2}&{3}={4}",
                Path.Combine(siteName, RawUrl),
                IdKey, retailerId.ToString(),
                ImageKey, imageFile);
            return CheckLocationModule.AdminUrl(url);
        }

        public static string UrlForDeleteImage(string siteName, int retailerId, string imageFile) {
            string url = string.Format("{0}?{1}={2}&{3}={4}&{5}={6}",
                Path.Combine(siteName, RawUrl),
                IdKey, retailerId.ToString(),
                ImageKey, imageFile,
                ActionKey, DeleteAction);
            return CheckLocationModule.AdminUrl(url);
        }

        public static string UrlForEnumImages(string siteName, int retailerId) {
            string url = string.Format("{0}?{1}={2}&{3}={4}",
                                       Path.Combine(siteName, RawUrl),
                IdKey, retailerId.ToString(),
                ActionKey, EnumAction);
            return CheckLocationModule.AdminUrl(url);
        }
        #endregion

        public void ProcessRequest(HttpContext context) {
            switch (context.Request.RequestType) {
                case "GET":
                    GetMode(context);
                    break;
                case "POST":
                    PostMode(context);
                    break;
            }
        }

        public bool IsReusable {
            get { return true; }
        }

        private void PostMode(HttpContext context) {
            string idString = context.Request.QueryString[IdKey];
            string imageName = context.Request.QueryString[ImageKey];
            int retailerId = Convert.ToInt32(idString);
            
            try {
                if (imageName == null) {
                    StreamReader reader = new StreamReader(context.Request.InputStream);
                    string htmlSource = reader.ReadToEnd();
                    BCExitPage.SaveHtml(retailerId, htmlSource);
                } else {
                    BCExitPage.SaveImage(retailerId, imageName, ConvertHelper.StreamToByteArray(context.Request.InputStream));
                }
            } catch (Exception ex) {
                throw ex;
            }
        }

        private void GetMode(HttpContext context) {
            string idString = context.Request.QueryString[IdKey];
            string action = context.Request.QueryString[ActionKey];
            string imageName = context.Request.QueryString[ImageKey];
            int retailerId = Convert.ToInt32(idString);

            if (idString == null) {
                Paper paper = BCPaper.GetPaper(StorageManager.CurrentProject.ProjectPaper);
                string sku = null;
                if (paper != null) {
                    if (paper.Sku != null && paper.Sku != string.Empty) {
                        string [] split = paper.Sku.Split(new char[] {','});
                        if (split.Length != 0)
                            sku = split[0].Trim();
                    }
                }
                string neatoExitPageUrl = NeatoExitPageUrl(sku);
                context.Response.Redirect(neatoExitPageUrl);
            }

            try {
                byte[] data;
                if (action == null) {
                    data = BCExitPage.LoadHtml(retailerId);
                    context.Response.ContentType = "text/html";
                    context.Response.OutputStream.Write(data, 0, data.Length);
                } else if (action == DeleteAction && imageName != null) {
                    BCExitPage.DeleteImage(retailerId, imageName);
                } else if (action == EnumAction) {
                    XmlDocument xmlDoc = BCExitPage.GetImageNames(retailerId);
                    data = ConvertHelper.StringToByteArray(xmlDoc.OuterXml);
                    context.Response.ContentType = "text/xml";
                    context.Response.OutputStream.Write(data, 0, data.Length);
                }
            } catch (Exception ex) {
                throw ex;
            }
        }

    }
}