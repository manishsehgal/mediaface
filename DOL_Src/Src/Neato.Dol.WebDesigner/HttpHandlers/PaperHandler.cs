using System;
using System.Globalization;
using System.Web;
using System.Web.SessionState;
using System.Xml;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class PaperHandler : IHttpHandler, IRequiresSessionState {
        public PaperHandler() {}

        #region Url
        private const string RawUrl = "Paper.aspx";

        private const string ModeKey = "mode";
        private const string XmlMode = "xml";
        private const string IconMode = "icon";
        private const string PaperMode = "paper";
        private const string IdKey = "id";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        public void ProcessRequest(HttpContext context) {
            string mode = context.Request.QueryString[ModeKey];
            switch (mode) {
                case XmlMode:
                    GetXml(context);
                    break;
                case IconMode:
                    GetIcon(context);
                    break;
                case PaperMode:
                    GetPaper(context);
                    break;
                default:
                    return;
            }
        }

        public bool IsReusable {
            get { return true; }
        }

        private int GetIdParam(HttpContext context) {
            try {
                return int.Parse(context.Request.QueryString[IdKey], CultureInfo.InvariantCulture);
            } catch (FormatException) {
                return 0;
            } catch (OverflowException) {
                return 0;
            } catch (ArgumentNullException) {
                return 0;
            }
        }

        private void GetXml(HttpContext context) {
            HttpResponse Response = context.Response;

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetValidUntilExpires(false);

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "text/xml";

            XmlDocument xml = BCPaper.EnumeratePaperXmlData();
            if (xml != null) {
                Response.Write(xml.OuterXml);
            }
            Response.End();
        }

        private void GetIcon(HttpContext context) {
            HttpResponse Response = context.Response;

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "image/jpeg";

            int id = GetIdParam(context);
            byte[] imageArray = BCPaper.GetPaperIcon(new PaperBase(id), 80, 80);

            if (imageArray != null && imageArray.Length > 0) {
                Response.OutputStream.Write(imageArray, 0, imageArray.Length);
            }
            Response.End();
        }

        private void GetPaper(HttpContext context) {
            HttpResponse Response = context.Response;

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetValidUntilExpires(false);

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "text/xml";

            int id = GetIdParam(context);
            XmlDocument xml = BCPaper.GerPaperFacesXml(new PaperBase(id));
            if (xml != null) {
                Response.Write(xml.OuterXml);
            }
            Response.End();
        }
    }
}