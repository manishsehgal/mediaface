using System;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using System.Xml;

using Neato.Dol.Business;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpModules;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class PluginFileHandler : IHttpHandler, IRequiresSessionState {
        public PluginFileHandler() {}

        #region Url
        private const string RawUrl = "PluginFile.aspx";
        private const string NameKey = "name";
        private const string FileKey = "file";
        private const string ClientOSKey = "clientOS";
        private const string DependsKey = "depends";
        private const string ActionKey = "action";
        private const string EnumAction = "enum";
        private const string DeleteAction = "delete";
        private const string PackageAction = "package";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static string UrlForUploadFile(string siteName, string fileName, string targetOS) {
            string url = string.Format("{0}?{1}={2}&{3}={4}",
                                       Path.Combine(siteName, RawUrl),
                                       NameKey, fileName,
                                       ClientOSKey, targetOS);
            return CheckLocationModule.AdminUrl(url);
        }

        public static string UrlForEnumFiles(string siteName, string targetOS) {
            string url = string.Format("{0}?{1}={2}&{3}={4}",
                                       Path.Combine(siteName, RawUrl),
                                       ActionKey, EnumAction,
                                       ClientOSKey, targetOS);
            return CheckLocationModule.AdminUrl(url);
        }

        public static string UrlForDeleteFile(string siteName, string fileName, string targetOS) {
            string url = string.Format("{0}?{1}={2}&{3}={4}&{5}={6}",
                                       Path.Combine(siteName, RawUrl),
                                       NameKey, fileName,
                                       ActionKey, DeleteAction,
                                       ClientOSKey, targetOS);
            return CheckLocationModule.AdminUrl(url);
        }
        #endregion

        public void ProcessRequest(HttpContext context) {
            switch (context.Request.RequestType) {
                case "GET":
                    GetMode(context);
                    break;
                case "POST":
                    PostMode(context);
                    break;
            }
        }

        public bool IsReusable {
            get { return true; }
        }

        private void PostMode(HttpContext context) {
            string fileName = context.Request.QueryString[NameKey];
            string targetOS = context.Request.QueryString[ClientOSKey];
            try {
                if (fileName != null) {
                    BCPlugin.SaveFileContent(fileName, ConvertHelper.StreamToByteArray(context.Request.InputStream), targetOS);
                }
            } catch (Exception ex) {
                throw ex;
            }
        }

        private void GetMode(HttpContext context) {
            string fileName = context.Request.QueryString[NameKey];
            string fileDesk = context.Request.QueryString[FileKey];
            string clientOS = context.Request.QueryString[ClientOSKey];
            clientOS = context.Request.UserAgent;

            string depends = context.Request.QueryString[DependsKey];
            string action = context.Request.QueryString[ActionKey];
            if(fileDesk != null) {
                byte[] data = BCPlugin.GetDescFile(clientOS);
                context.Response.ContentType = "text/xml";
                context.Response.OutputStream.Write(data, 0, data.Length);
            }
            try {
                byte[] data;
                if (fileName != null && depends != null) {
                    GetPluginFileXml(context, fileName, depends,clientOS);
                } else if (action == DeleteAction && fileName != null) {
                    BCPlugin.DeleteFile(fileName, clientOS);
                } else if (action == PackageAction) {
                    context.Response.Redirect(BCPlugin.GetPackageUrl(clientOS));
                } else if (action == EnumAction) {
                    XmlDocument xmlDoc = BCPlugin.GetAddittionalFileNames(clientOS);
                    data = ConvertHelper.StringToByteArray(xmlDoc.OuterXml);
                    context.Response.ContentType = "text/xml";
                    context.Response.OutputStream.Write(data, 0, data.Length);
                }
            } catch (ThreadAbortException ex) {
                ex.ToString();
            } catch (Exception ex) {
                throw ex;
            }
        }

        private void GetPluginFileXml(HttpContext context, string FileName, string Depends, string clientOS) {
            HttpResponse Response = context.Response;

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "text/xml";

            if (Depends.Length > 0) {
                FileName += ";" + Depends;
            }
            string[] files = FileName.Split(';');

            Response.Write("<Files>");
            foreach (string file in files) {
                string fileString = BCPlugin.GetFileContent(file, clientOS);
                Response.Write("<File Status=\"");
                if (fileString == null || fileString.Length == 0) {
                    Response.Write("Failed\" >");
                } else {
                    Response.Write("Ok\" ");
                    Response.Write("Filename=\"" + file + "\" >");
                    Response.Write(fileString);
                }
                Response.Write("</File>");
            }
            Response.Write("</Files>");
            Response.End();
        }
    }
}