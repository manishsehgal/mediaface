using System;
using System.Web;
using System.Web.SessionState;

using Neato.Dol.Business;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class DynamicLinkHandler : IHttpHandler, IRequiresSessionState {
        #region Url
        private const string RawUrl = "ResetDynamicLinks.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static string PageName() {
            return RawUrl;
        }
        #endregion

        public void ProcessRequest(HttpContext context) {
            BCDynamicLink.ResetDynamicLinks();
        }

        public bool IsReusable {
            get { return true; }
        }
    }
}