using System;
using System.Web;
using System.Web.SessionState;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class IconHandler : IHttpHandler, IRequiresSessionState {
        #region Url
        private const string RawUrl = "DeviceIcon.aspx";

        public const string DeviceIdParamName = "DeviceId";
        public const string DeviceIdParamNameBigPic = "DeviceIdBig";
        
        public const string CarrierIdParamName = "CarrierId";
        public const string BrandIdParamName = "BrandId";
        public const string PaperIdParamName = "PaperId";
        public const string FaceIdParamName = "FaceId";
        public const string PaperBrandIdParamName = "PaperBrandId";
        public const string FaceLayoutIdParamName = "FaceLayoutId";
        public const string LayoutIdParamName = "LayoutId";
        public const string LayoutIconIdParamName = "LayoutIconId";
        public const string RetailerIdParamName = "Retailer";
        public const string TemplateIdParamName = "Template";
        public const string CategoryIdParamName = "Category";
        public const string DeviceIdScaledParamName = "DeviceIdScaled";
        public const string WidthParamName = "Width";
        public const string HeightParamName = "Height";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri ImageUrl(string paramName, int Id) {
            return UrlHelper.BuildUrl(RawUrl, paramName, Id);
        }

        public static Uri ImageUrl(string paramName, int Id, int width, int height) {
            return UrlHelper.BuildUrl(RawUrl, paramName, Id, WidthParamName, width, HeightParamName, height);
        }

        public static Uri ImageUrl(CarrierBase carrier) {
            return ImageUrl(CarrierIdParamName, carrier.Id);
        }

        public static Uri ImageUrl(DeviceBase device, bool isBigIcon) {
            return ImageUrl(isBigIcon ? DeviceIdParamNameBigPic : DeviceIdParamName, device.Id);
        }
        public static Uri ImageUrl(DeviceBase device, int width, int height) {
            return ImageUrl(DeviceIdScaledParamName, device.Id, width, height);
        }

        public static Uri ImageUrl(DeviceBrandBase brand) {
            return ImageUrl(BrandIdParamName, brand.Id);
        }

        public static Uri ImageUrl(PaperBrandBase paperBrand) {
            return ImageUrl(PaperBrandIdParamName, paperBrand.Id);
        }

        public static Uri ImageUrl(PaperBase paper) {
            return ImageUrl(PaperIdParamName, paper.Id);
        }

        public static Uri ImageUrl(PaperBase paper, int width, int height) {
            return ImageUrl(DeviceIdScaledParamName, paper.Id, width, height);
        }

        public static Uri ImageUrl(FaceBase face) {
            return ImageUrl(FaceIdParamName, face.Id);
        }

        public static Uri ImageUrl(FaceLayout faceLayout) {
            return ImageUrl(LayoutIconIdParamName, faceLayout.ImageLibItem.Id);
        }

        public static Uri ImageUrl(Retailer retailer) {
            return ImageUrl(RetailerIdParamName, retailer.Id);
        }

        public static Uri ImageUrl(Template template) {
            return ImageUrl(TemplateIdParamName, template.Id);
        }

        public static Uri ImageUrl(Category category) {
            return ImageUrl(CategoryIdParamName, category.Id);
        }
        #endregion

        public void ProcessRequest(HttpContext context) {
            HttpRequest Request = context.Request;
            HttpResponse Response = context.Response;

            Response.ContentType = "image/jpeg";
            byte[] icon = null;

            if (Request.QueryString[DeviceIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[DeviceIdParamName]);
                DeviceBase device = new DeviceBase(id);
                icon = BCDevice.GetDeviceIcon(device);
            } else if (Request.QueryString[DeviceIdScaledParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[DeviceIdScaledParamName]);
                Int32 width = Int32.Parse(Request.QueryString[WidthParamName]);
                Int32 height = Int32.Parse(Request.QueryString[HeightParamName]);
                DeviceBase device = new DeviceBase(id);
                icon = ImageHelper.GetIcon(BCDevice.GetDeviceIcon(
                    device), width, height);
            } else if (Request.QueryString[CarrierIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[CarrierIdParamName]);
                CarrierBase carrier = new CarrierBase(id);
                icon = BCCarrier.GetCarrierIcon(carrier);
            } else if (Request.QueryString[BrandIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[BrandIdParamName]);
                DeviceBrandBase brand = new DeviceBrandBase(id);
                icon = BCDeviceBrand.GetDeviceBrandIcon(brand);
            } else if (Request.QueryString[DeviceIdParamNameBigPic] != null) {
                Int32 id = Int32.Parse(Request.QueryString[DeviceIdParamNameBigPic]);
                DeviceBase device = new DeviceBase(id);
                icon = BCDevice.GetDeviceBigIcon(device);
            } else if (Request.QueryString[PaperIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[PaperIdParamName]);
                icon = DOPaper.GetPaperIcon(new PaperBase(id));
                if (Request.QueryString[WidthParamName] != null && Request.QueryString[HeightParamName] != null) {
                    Int32 width = Int32.Parse(Request.QueryString[WidthParamName]);
                    Int32 height = Int32.Parse(Request.QueryString[HeightParamName]);
                    icon = ImageHelper.GetIcon(icon, width, height);
                }
            } else if (Request.QueryString[FaceIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[FaceIdParamName]);
                icon = BCFace.GetIcon(new FaceBase(id));
            } else if (Request.QueryString[PaperBrandIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[PaperBrandIdParamName]);
                icon = DOPaperBrand.GetPaperBrandIconByID(new PaperBrandBase(id));
            } else if (Request.QueryString[FaceLayoutIdParamName] != null) {
                Int32 faceId = Int32.Parse(Request.QueryString[FaceLayoutIdParamName]);
                Int32 layoutId = 0;
                if (Request.QueryString[LayoutIdParamName] != null)
                    layoutId = Int32.Parse(Request.QueryString[LayoutIdParamName]);
                icon = BCFaceLayout.GetIconByFaceIdLayoutId(new FaceBase(faceId), layoutId);
            } else if (Request.QueryString[LayoutIconIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[LayoutIconIdParamName]);
                icon = DOFaceLayout.GetFaceLayoutIconById(new ImageLibItem(id));
            } else if (Request.QueryString[RetailerIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[RetailerIdParamName]);
                icon = BCRetailer.GetIcon(id);
            } else if (Request.QueryString[TemplateIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[TemplateIdParamName]);
                icon = BCTemplate.GetTemplateIcon(new Template(id));
                if (Request.QueryString[WidthParamName] != null && Request.QueryString[HeightParamName] != null) {
                    Int32 width = Int32.Parse(Request.QueryString[WidthParamName]);
                    Int32 height = Int32.Parse(Request.QueryString[HeightParamName]);
                    icon = ImageHelper.GetIcon(icon, width, height);
                }
            } else if (Request.QueryString[CategoryIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[CategoryIdParamName]);
                icon = BCCategory.GetCategoryIcon(new Category(id));
            }
            if (icon != null) {
                Response.OutputStream.Write(icon, 0, icon.Length);
            }

            Response.End();
        }

        public bool IsReusable {
            get { return true; }
        }
    }
}