using System;
using System.IO;
using System.Web;
using System.Web.SessionState;
using System.Xml;

using Neato.Dol.Business;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpModules;

namespace Neato.Dol.WebDesigner.HttpHandlers
{
	public class BannerHandler : IHttpHandler, IRequiresSessionState {
        public const string BannerHeightKey = "BannerHeight";
        //public const string BannerWidthKey = "BannerWidth";

        #region Url
        private const string RawUrl = "Banner.aspx";
        private const string IdKey = "id";
        private const string PageKey = "page";
        private const string ImageKey = "image";
        private const string ActionKey = "action";
        private const string EnumAction = "enum";
        private const string DeleteAction = "delete";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static string UrlForHtmlText(string siteName, string page, int bannerId) {
            string url = string.Format("{0}?{1}={2}&{3}={4}",
                Path.Combine(siteName, RawUrl),
                PageKey, page,
                IdKey, bannerId.ToString());
            return CheckLocationModule.AdminUrl(url);
        }

        public static string UrlForHtmlText(string siteName, string page, int bannerId, string bannerHeight) {
            string url = string.Format("{0}?{1}={2}&{3}={4}&{5}={6}",
                Path.Combine(siteName, RawUrl),
                PageKey, page,
                IdKey, bannerId.ToString(),
                BannerHeightKey, bannerHeight);
            return CheckLocationModule.AdminUrl(url);
        }

        public static string UrlForUploadImage(string siteName, string page, int bannerId, string imageFile) {
            string url = string.Format("{0}?{1}={2}&{3}={4}&{5}={6}",
                Path.Combine(siteName, RawUrl),
                PageKey, page,
                IdKey, bannerId.ToString(),
                ImageKey, imageFile);
            return CheckLocationModule.AdminUrl(url);
        }

        public static string UrlForDeleteImage(string siteName, string page, int bannerId, string imageFile) {
            string url = string.Format("{0}?{1}={2}&{3}={4}&{5}={6}&{7}={8}",
                Path.Combine(siteName, RawUrl),
                PageKey, page,
                IdKey, bannerId.ToString(),
                ImageKey, imageFile,
                ActionKey, DeleteAction);
            return CheckLocationModule.AdminUrl(url);
        }

        public static string UrlForEnumImages(string siteName, string page, int bannerId) {
            string url = string.Format("{0}?{1}={2}&{3}={4}&{5}={6}",
                Path.Combine(siteName, RawUrl),
                PageKey, page,
                IdKey, bannerId.ToString(),
                ActionKey, EnumAction);
            return CheckLocationModule.AdminUrl(url);
        }
        #endregion

        public void ProcessRequest(HttpContext context) {
            switch (context.Request.RequestType) {
                case "GET":
                    GetMode(context);
                    break;
                case "POST":
                    PostMode(context);
                    break;
            }
        }

        public bool IsReusable {
            get { return true; }
        }

        private void PostMode(HttpContext context) {
            string idString = context.Request.QueryString[IdKey];
            string page = context.Request.QueryString[PageKey];
            string imageName = context.Request.QueryString[ImageKey];
            string bannerHeight = context.Request.QueryString[BannerHeightKey];
            //string bannerWidth = context.Request.QueryString[BannerWidthKey];
            int bannerId = Convert.ToInt32(idString);
            
            try {
                if (imageName == null) {
                    StreamReader reader = new StreamReader(context.Request.InputStream);
                    string htmlSource = reader.ReadToEnd();
                    BCBanner.SaveHtml(page, htmlSource, bannerId);

                    BCBanner.SetBannerMetric(page, bannerId, string.Empty, bannerHeight);
                } else {
                    BCBanner.SaveImage(page, imageName, ConvertHelper.StreamToByteArray(context.Request.InputStream), bannerId);
                }
            } catch (Exception ex) {
                throw ex;
            }
        }

        private void GetMode(HttpContext context) {
            string idString = context.Request.QueryString[IdKey];
            string page = context.Request.QueryString[PageKey];
            string action = context.Request.QueryString[ActionKey];
            string imageName = context.Request.QueryString[ImageKey];
            int bannerId = Convert.ToInt32(idString);


            try {
                byte[] data;
                if (action == null) {
                    int height = BCBanner.GetBannerHeight(page, bannerId);
                    context.Response.AddHeader(BannerHeightKey, height == -1 ? string.Empty : height.ToString());

                    data = BCBanner.LoadHtml(page, bannerId);
                    context.Response.ContentType = "text/html";
                    context.Response.OutputStream.Write(data, 0, data.Length);
                } else if (action == DeleteAction && imageName != null) {
                    BCBanner.DeleteImage(page, imageName, bannerId);
                } else if (action == EnumAction) {
                    XmlDocument xmlDoc = BCBanner.GetImageNames(page, bannerId);
                    data = ConvertHelper.StringToByteArray(xmlDoc.OuterXml);
                    context.Response.ContentType = "text/xml";
                    context.Response.OutputStream.Write(data, 0, data.Length);
                }
            } catch (Exception ex) {
                throw ex;
            }
        }
	}
}
