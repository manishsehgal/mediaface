using System;
using System.IO;
using System.Web;
using System.Web.SessionState;
using System.Xml;

using Neato.Dol.Business;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class FontFileHandler : IHttpHandler, IRequiresSessionState {
        public FontFileHandler() {}

        #region Url
        private const string RawUrl  = "FontFile.aspx";
        private const string NameKey = "name";
        #endregion

        public void ProcessRequest(HttpContext context) {
            string FileName = context.Request.QueryString[NameKey];
            GetFontFileXml(context, FileName);
        }

        public bool IsReusable {
            get { return true; }
        }

        private void GetFontFileXml(HttpContext context, string FileName) {
            HttpResponse Response = context.Response;

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "text/xml";

            string fileString = BCFontFile.GetFile(FileName);

            Response.Write("<Font status=\"");
            if (fileString == null || fileString.Length==0) {
                Response.Write("Failed\">");
            }
            else {
                Response.Write("Ok\">");
                Response.Write(fileString);
            }
            Response.Write("</Font>");

            Response.End();
        }
    }
}