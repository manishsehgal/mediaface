using System;
using System.Globalization;
using System.IO;
using System.Runtime.Remoting.Contexts;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using System.Xml;

using Neato.Dol.Business;
using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class ImageLibraryHandler : IHttpHandler, IRequiresSessionState {
        public ImageLibraryHandler() {}

        #region Url
        private const string ModeKey = "mode";
        private const string LibraryXmlMode = "xml";
        private const string ImageMode = "image";
        private const string IconMode = "icon";
        private const string AddMode = "add";
        private const string DeleteMode = "delete";
        private const string LastInsertedImageMode = "GetLastImageInfo";
        private const string UploadMode = "upload";
        private const string RawUrl = "ImageLibrary.aspx";
        private const string IdKey = "id";

        public static Uri UrlGetLibraryXml() {
            return UrlHelper.BuildUrl(RawUrl, ModeKey, LibraryXmlMode);
        }

        public static Uri UrlGetImage(int id) {
            return UrlHelper.BuildUrl(RawUrl, ModeKey, ImageMode, IdKey, id);
        }

        public static Uri UrlGetIcon(int id) {
            return UrlHelper.BuildUrl(RawUrl, ModeKey, IconMode, IdKey, id);
        }

        public static Uri UrlAddImage(int id) {
            return UrlHelper.BuildUrl(RawUrl, ModeKey, AddMode, IdKey, id);
        }
        #endregion

        public void ProcessRequest(HttpContext context) {
            string mode = context.Request.QueryString[ModeKey];
            switch (mode) {
                case LibraryXmlMode:
                    GetImageLibraryXml(context);
                    break;
                case ImageMode:
                    this.GetImage(context);
                    break;
                case IconMode:
                    this.GetIcon(context);
                    break;
                case AddMode:
                    this.AddImageToProject(context);
                    break;
                case UploadMode:
                    this.UploadImage(context);
                    break;
                case DeleteMode:
                    this.DeleteImage(context);
                    break;
                case LastInsertedImageMode:
                    GetLastInsertedImageInfo(context);
                    break;
                default:
                    return;
            }
        }

        public bool IsReusable {
            get { return true; }
        }

        private int GetIdParam(HttpContext context) {
            try {
                return int.Parse(context.Request.QueryString[IdKey], CultureInfo.InvariantCulture);
            } catch (FormatException) {
                return 0;
            } catch (OverflowException) {
                return 0;
            } catch (ArgumentNullException) {
                return 0;
            }
        }


        private void GetImageLibraryXml(HttpContext context) {
            HttpResponse Response = context.Response;

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetValidUntilExpires(false);

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "text/xml";

            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
			
			CustomerGroup customerGroup = StorageManager.CurrentCustomer == null
				? CustomerGroup.Trial
				: StorageManager.CurrentCustomer.Group;

            XmlDocument libraryXml = BCImageLibrary.GetImageLibrary(StorageManager.CurrentLanguage, customerGroup).XmlData;
            if (libraryXml != null) {
                Response.Write(libraryXml.OuterXml);
            }
            Response.End();
        }

        private void GetImage(HttpContext context) {
            HttpResponse Response = context.Response;

            SetImageExpires(context);

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "image/jpeg";

            int id = GetIdParam(context);

            byte[] imageArray = BCImageLibrary.GetImage(id);
            if (imageArray != null && imageArray.Length > 0) {
                Response.OutputStream.Write(imageArray, 0, imageArray.Length);
            }
            Response.End();
        }

        private void GetIcon(HttpContext context) {
            HttpResponse Response = context.Response;

            SetImageExpires(context);

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "image/jpeg";

            int id = GetIdParam(context);

            byte[] imageArray = BCImageLibrary.GetImageIcon(id, 50, 50);

            if (imageArray != null && imageArray.Length > 0) {
                Response.OutputStream.Write(imageArray, 0, imageArray.Length);
            }
            Response.End();
        }

        private void DeleteImage(HttpContext context) {
            HttpResponse Response = context.Response;

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "text/plain";

            BCProject.RemoveImage(StorageManager.CurrentProject, context.Request.QueryString[IdKey]);

            Response.Write(" ");
            Response.End();
        }

        public void AddImageToProject(HttpContext context) {
            HttpResponse Response = context.Response;

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "text/plain";

            int id = GetIdParam(context);

            string newImage = BCProject.AddImageFromLibrary(StorageManager.CurrentProject, id);
            StorageManager.LastInsertedImage = newImage;
            BCTrackingImages.Add(new ImageLibItem(id), context.Request.UserHostAddress);
            string result = UrlHelper.BuildParameters("added", newImage != null, "LastInsertedImage", newImage);
            Response.Write(result);
            Response.End();
        }

        private void SetImageExpires(HttpContext context) {
            int expares = Configuration.LibraryImageCacheSec;
            if (expares > 0) {
                context.Response.Cache.SetCacheability(HttpCacheability.Public);
                context.Response.Cache.SetExpires(DateTime.Now.AddSeconds(expares));
            } else {
                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                context.Response.Cache.SetExpires(DateTime.Now);
            }
        }

        public void UploadImage(HttpContext context) {
            if (context.Request.Files.Count == 0)
                context.Response.End();
            Stream fileData = context.Request.Files.Get(0).InputStream;

            int size = context.Request.Files.Get(0).ContentLength;
            if (fileData != null && size > 0) {
                if (!BCProject.IsImageAdditionAllowed(StorageManager.CurrentProject, size)) {
                    return;
                }
                string newImage = BCProject.AddImage(StorageManager.CurrentProject, fileData);
                StorageManager.LastInsertedImage = newImage;
            }
            context.Response.Write(" ");
        }

        public void GetLastInsertedImageInfo(HttpContext context) {
            if (StorageManager.LastInsertedImage != null) {
                HttpResponse Response = context.Response;

                string result = UrlHelper.BuildParameters("added", StorageManager.LastInsertedImage != null, "LastInsertedImage", StorageManager.LastInsertedImage);
                Response.Write(result);
                StorageManager.LastInsertedImage = null;

                Response.End();
            }
        }
    }
}