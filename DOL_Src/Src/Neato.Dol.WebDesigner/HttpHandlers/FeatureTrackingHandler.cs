using System;
using System.Web;
using System.Web.SessionState;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Tracking;

namespace Neato.Dol.WebDesigner.HttpHandlers
{
    public class FeatureTrackingHandler: IHttpHandler, IRequiresSessionState {
	
        public FeatureTrackingHandler(){}
        public void ProcessRequest(HttpContext context) {
            if(context.Request.QueryString["type"] != null ) {
                try {
                    Feature feature = (Feature)Feature.Parse
                        (typeof(Feature),context.Request.QueryString["type"],false);
                    string Id = context.Request.QueryString["Id"];
                    if(feature != Feature.None) {
                        if((feature == Feature.QuickTools) && (Id != null)) {
                            try {
                                feature = (Feature)Feature.Parse
                                            (typeof(Feature),feature.ToString()+Id,false);
                            } catch {}
                        }
                        BCTrackingFeatures.Add(feature, context.Request.UserHostAddress);
                        if((feature == Feature.AddShape) && (Id != null)) 
                           BCTrackingShapes.Add(Id, context.Request.UserHostAddress);
                    }        
                } catch {}
            }
            HttpResponse Response = context.Response;

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetValidUntilExpires(false);

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "text/xml";
            Response.Write("<?xml version=\"1.0\" encoding=\"utf-8\" ?><Node/>");
            Response.End();
        }

        public bool IsReusable {
            get { return true; }
        }
    }
}
