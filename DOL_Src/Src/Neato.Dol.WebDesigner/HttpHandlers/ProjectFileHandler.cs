using System;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Web.SessionState;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class ProjectFileHandler : IHttpHandler, IRequiresSessionState {
        private const string SessionExpired = "Your session is expired!";

        public ProjectFileHandler() {}

        #region Url
        private const string ModeKey = "mode";
        private const string UploadMode = "Upload";
        private const string RawUrl = "ProjectFile.aspx";

        public static Uri UrlDownloadProject() {
			//Workaround:Netscape 6.x&7.x add extension .aspx to the project filename
			// in case of after url there is some data like ..../file.aspx/file.ext
			//netscape 7 will save it with name file.ext
			if(HttpContext.Current.Request.UserAgent.IndexOf("Netscape/7.0")>-1){
				return UrlHelper.BuildUrl(RawUrl+"/"+Path.ChangeExtension(Configuration.DesignProjectDefaultFileName, Configuration.DesignProjectFileType));
			}
            return UrlHelper.BuildUrl(RawUrl);
        }

        // TODO: does not used
        public static Uri UrlUploadProject() {
            return UrlHelper.BuildUrl(RawUrl, ModeKey, UploadMode);
        }
        #endregion

        private string CreateFileName(string fileName) {
            return Path.ChangeExtension(fileName, Configuration.DesignProjectFileType);
        }

        #region IHttpHandler methods
        public void ProcessRequest(HttpContext context) {
            string mode = context.Request.QueryString[ModeKey];
            switch (mode) {
                case UploadMode:
                    Upload(context);
                    break;
                default:
                    Download(context);
                    break;
            }
        }

        public bool IsReusable {
            get { return true; }
        }
        #endregion
        // TODO: does not used
        private void Upload(HttpContext context) {
            HttpRequest Request = context.Request;
            HttpResponse Response = context.Response;

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetValidUntilExpires(false);

            Response.ClearContent();
            Response.ClearHeaders();
            
            if (Request.Files.Count == 0)
                Response.End();

            HttpPostedFile postedFile = Request.Files.Get(0);
            Stream fileData = postedFile.InputStream;

            try {
                StorageManager.CurrentProject = BCProject.Import(StorageManager.CurrentCustomer, fileData);
                Response.ContentType = "text/html";
            } catch {
                Response.Write(SessionExpired);
            }
            Response.Write(" ");
        }

        private void Download(HttpContext context) {
            HttpResponse Response = context.Response;

            Response.ClearContent();
            Response.ClearHeaders();

            Stream projectFileStream = null;
            byte[] buffer = new Byte[10000];
            int projectFileStreamLength;
            long dataToRead;
            string filename = CreateFileName(Configuration.DesignProjectDefaultFileName);

            try {
                Project proj = StorageManager.CurrentProject.Clone();
                if (proj == null)
                    throw new BusinessException("The project doesn't exist!");
                proj.SynchronizeImageContent();
                projectFileStream = BCProject.Export(proj);
                dataToRead = projectFileStream.Length;

                Response.ContentType = Configuration.DesignProjectContentType;
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + filename+"\"");

                while (dataToRead > 0) 
                {
                    if (Response.IsClientConnected) 
                    {
                        projectFileStreamLength = projectFileStream.Read(buffer, 0, 10000);
                        Response.OutputStream.Write(buffer, 0, projectFileStreamLength);
                        Response.Flush();
                        buffer = new Byte[10000];
                        dataToRead = dataToRead - projectFileStreamLength;
                    } 
                    else 
                    {
                        //prevent infinite loop if user disconnects
                        dataToRead = -1;
                    }
                }
            } catch(BusinessException ex) {
                Debug.WriteLine(ex);
                //Response.Redirect(DefaultPage.Url().PathAndQuery);
            } finally {
                if (projectFileStream != null) {
                    projectFileStream.Close();
                }
            }
        }
    }
}