using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Xml;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class ActivateAccountHandler : IHttpHandler, IRequiresSessionState {
        public ActivateAccountHandler() {}


        #region Url
        private const string RawUrl = "ActivateAccount.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        public void ProcessRequest(HttpContext context) {
            string data = ConvertHelper.ByteArrayToString(ConvertHelper.StreamToByteArray(context.Request.InputStream));
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(data);

            XmlNode root = xmlDoc.DocumentElement;
            XmlNode emailNode     = root.SelectSingleNode("Email");
            XmlNode firstNameNode = root.SelectSingleNode("FirstName");
            XmlNode lastNameNode  = root.SelectSingleNode("LastName");
            XmlNode passwordNode  = root.SelectSingleNode("Password");
            XmlNode productTypeNode = root.SelectSingleNode("ProductType");

            string email     = emailNode == null ? string.Empty : emailNode.InnerText;
            string firstName = firstNameNode == null ? string.Empty : firstNameNode.InnerText;
            string lastName  = lastNameNode == null ? string.Empty : lastNameNode.InnerText;
            string password  = passwordNode == null ? string.Empty : passwordNode.InnerText;
            CustomerGroup customerGroup = productTypeNode == null
                                              ? CustomerGroup.MFO
                                              : (CustomerGroup)Enum.Parse(typeof (CustomerGroup), productTypeNode.InnerText);

            XmlNode checkEmail = emailNode.Attributes["Check"];
            if (checkEmail != null) {
                if (bool.Parse(checkEmail.InnerText)) {
                    Customer customer = BCCustomer.GetCustomerByLogin(email, true);
                    string accountStatus = "NoAccount";
                    if (customer != null) {
                        accountStatus = customer.Active ? customer.Group.ToString() : "Inactive";
                    }
                    context.Response.Write(accountStatus);
                    context.Response.End();
                }
            }

            BCCustomer.ActivateCustomer(email, firstName, lastName, password, false, DefaultPage.Url().AbsoluteUri, null, customerGroup);
        }

        public bool IsReusable {
            get { return true; }
        }
    }
}