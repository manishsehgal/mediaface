using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Xml;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class WatermarkHandler : IHttpHandler, IRequiresSessionState {
        public const string BannerKey = "Watermark";

        #region Url
        private const string RawUrl = "Watermark.aspx";
        private const string LoadFontsKey = "LoadFonts";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri Url(bool loadFonts) {
            return UrlHelper.BuildUrl(RawUrl, LoadFontsKey, loadFonts.ToString());
        }

        public static string PageName() {
            return RawUrl;
        }
        #endregion

        public WatermarkHandler() {
        }

        public void ProcessRequest(HttpContext context) {
            switch (context.Request.RequestType) {
                case "GET":
                    GetMode(context);
                    break;
                case "POST":
                    PostMode(context);
                    break;
            }
        }

        public bool IsReusable {
            get { return true; }
        }

        private void GetMode(HttpContext context) {
            bool loadFonts = false;
            if (context.Request.QueryString[LoadFontsKey] != null)
                loadFonts = bool.Parse(context.Request.QueryString[LoadFontsKey]);

            try {
                XmlDocument watermarkXml = BCBanner.LoadBannerXml(BannerKey, 0) as XmlDocument;

                if (loadFonts) {
                    XmlNode fontsNode = watermarkXml.CreateElement("Fonts");
                    watermarkXml.DocumentElement.AppendChild(fontsNode);

                    XmlDocument fontsXml = new XmlDocument();
                    fontsXml.Load(Path.Combine(Configuration.ApplicationRoot, @"fonts.xml"));

                    foreach(XmlNode node in fontsXml.SelectNodes(@"/fonts/font[@desc!='_']/@name")) {
                        XmlNode fontNode = watermarkXml.CreateElement("Font");
                        fontsNode.AppendChild(fontNode);
                        fontNode.InnerText = node.InnerText;
                    }
                }

                context.Response.ContentEncoding = Encoding.UTF8;
                context.Response.ContentType = "text/xml";

                XmlWriter writer = new XmlTextWriter(context.Response.OutputStream, Encoding.UTF8);
                watermarkXml.WriteTo(writer);
                writer.Flush();
            }
            catch (Exception ex) {
                string a = "";
            }
        }

        private void PostMode(HttpContext context) {
            XmlDocument xmlDoc = new XmlDocument();
            try {
                xmlDoc.Load(context.Request.InputStream);
                BCBanner.SaveBannerXml(BannerKey, 0, xmlDoc);
            }
            catch (Exception ex) {
                string a = "";
            }
        }
    }
}