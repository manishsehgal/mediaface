using System;
using System.Web;
using System.Web.SessionState;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class PaperTrackingHandler: IHttpHandler, IRequiresSessionState {
        private const int UndefinedPaperId = 0;

        #region Url
        private const string PaperIdKey = "paperId";
        private const string RawUrl = "PaperTracking.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static string UrlForPaperTracking(int paperId) {
            return string.Format("{0}?{1}={2}",
                RawUrl,
                PaperIdKey, paperId);
        }
        #endregion
        
        public PaperTrackingHandler() {}

        public void ProcessRequest(HttpContext context) {
            if(context.Request.QueryString["paperId"] != null ) {
                try {
                    int paperId = int.Parse(context.Request.QueryString["paperId"]);

                    if(paperId != UndefinedPaperId) {
                        Paper paper = BCPaper.GetPaper(new PaperBase(paperId));
                        BCTrackingPapers.Add(BCPaper.GetPaper(paper), context.Request.UserHostAddress);
                    }
                
                } catch {}
            }
            HttpResponse Response = context.Response;

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetValidUntilExpires(false);

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "text/xml";
            Response.Write("<?xml version=\"1.0\" encoding=\"utf-8\" ?><Node/>");
            Response.End();
        }

        public bool IsReusable {
            get { return true; }
        }
    }
}


