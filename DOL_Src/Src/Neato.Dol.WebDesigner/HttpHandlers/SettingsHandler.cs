using System;
using System.Globalization;
using System.Web;
using System.Web.SessionState;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class SettingsHandler : IHttpHandler, IRequiresSessionState {
        private const string TypeKey = "type";
        private const string IsDTCDKey = "isDTCD";
        private const string SaveCalibrationKey = "SaveCalibration";

        public SettingsHandler() {}

        public void ProcessRequest(HttpContext context) {
            HttpRequest Request = context.Request;
            HttpResponse Response = context.Response;
            if (Request.QueryString[TypeKey] == SaveCalibrationKey) {
                PdfCalibration calibration = new PdfCalibration();
                calibration.X = float.Parse(Request.QueryString["x"], CultureInfo.InvariantCulture);
                calibration.Y = float.Parse(Request.QueryString["y"], CultureInfo.InvariantCulture);

                if (bool.Parse(Request.QueryString[IsDTCDKey]))
                    StorageManager.CookieCalibrationDirectToCD = calibration;
                else
                    StorageManager.CookieCalibration = calibration;
            } else {
                PdfCalibration calibration = StorageManager.CookieCalibration;
                PdfCalibration calibrationDirectToCD = StorageManager.CookieCalibrationDirectToCD;
                StorageManager.SessionCalibration = calibration;
                PdfCalibration calibr = StorageManager.SessionCalibration;

                string xcalibration = (calibr.IsSpecified) ?
                    calibr.X.ToString("g", CultureInfo.InvariantCulture) : "none";
                string ycalibration = (calibr.IsSpecified) ?
                    calibr.Y.ToString("g", CultureInfo.InvariantCulture) : "none";

                string xcalibrationDirectToCD = (calibrationDirectToCD.IsSpecified) ?
                    calibrationDirectToCD.X.ToString("g", CultureInfo.InvariantCulture) : "none";
                string ycalibrationDirectToCD = (calibrationDirectToCD.IsSpecified) ?
                    calibrationDirectToCD.Y.ToString("g", CultureInfo.InvariantCulture) : "none";

                string basePlatform = BCPlugin.GetOS(Request.UserAgent).ToString();
                CustomerGroup customerGroup = StorageManager.CurrentCustomer == null
                    ? CustomerGroup.Trial
                    : StorageManager.CurrentCustomer.Group;
                int userLevelIndex = (int) customerGroup + 1;
                string htmlText = string.Empty;
                htmlText = ConvertHelper.ByteArrayToString(BCBanner.LoadHtml(BCPreviewPage.Preview, (int) customerGroup));
                if (htmlText == null)
                    htmlText = string.Empty;

                if (StorageManager.CurrentCustomer != null) {
                    htmlText = htmlText.Replace("Email=%20",
                                                string.Format("Email={0}", HttpUtility.UrlEncode(StorageManager.CurrentCustomer.Email)));
                }

                String vars = UrlHelper.BuildParameters(
                    "xcalibration", xcalibration,
                    "ycalibration", ycalibration,
                    "xcalibrationDirectToCD", xcalibrationDirectToCD,
                    "ycalibrationDirectToCD", ycalibrationDirectToCD,
                    "culture", StorageManager.CurrentLanguage,
                    "browser", context.Request.Browser.Browser,
                    "platform", context.Request.Browser.Platform,
                    "sessionId", context.Session.SessionID,
                    "userlevel", customerGroup.ToString(),
                    "userLevelIndex", userLevelIndex.ToString(),
                    "basePlatform", basePlatform,
                    "extension", Configuration.DesignProjectFileType,
                    "freeSize", Configuration.MaxProjectSize*1024 - StorageManager.CurrentProject.Size,
                    "previewHtmlText", htmlText
                    );
                Response.Write(vars);
            }
            Response.End();
        }

        public bool IsReusable {
            get { return true; }
        }
    }
}