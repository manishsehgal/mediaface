using System;
using System.Diagnostics;
using System.Web;
using System.Web.SessionState;
using System.Xml;

using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class ProjectHandler : IHttpHandler, IRequiresSessionState {
        private const string SessionExpired = "Your session is expired!";
        public ProjectHandler() {}

        #region Url
        private const string ModeKey = "type";
        private const string SaveProjectXmlMode = "SaveXmlProject";
        private const string GetProjectXmlMode = "XmlProject";
        private const string RawUrl = "Project.aspx";

        public static Uri UrlGetProjectXml() {
            return UrlHelper.BuildUrl(RawUrl, ModeKey, GetProjectXmlMode);
        }

        public static Uri UrlSaveProjectXml() {
            return UrlHelper.BuildUrl(RawUrl, ModeKey, SaveProjectXmlMode);
        }
        #endregion

        public void ProcessRequest(HttpContext context) {
            string mode = context.Request.QueryString[ModeKey];
            switch (mode) {
                case SaveProjectXmlMode:
                    SaveProjectXml(context);
                    break;
                case GetProjectXmlMode:
                    GetProjectXml(context);
                    break;
                default:
                    return;
            }
        }

        public bool IsReusable {
            get { return true; }
        }

        private void GetProjectXml(HttpContext context) {
            HttpResponse Response = context.Response;

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetValidUntilExpires(false);

            Response.ClearContent();
            Response.ClearHeaders();
            
            try {
                Response.Write(StorageManager.CurrentProject.ProjectXml.OuterXml);
                Debug.Write(StorageManager.CurrentProject.ProjectXml.OuterXml);
                Response.ContentType = "text/xml";
            } catch {
                Response.Write(SessionExpired);
            }

            Response.End();
        }

        private void SaveProjectXml(HttpContext context) {
            HttpRequest Request = context.Request;
            HttpResponse Response = context.Response;

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetValidUntilExpires(false);

            Response.ClearContent();
            Response.ClearHeaders();
            
            try {
                XmlDocument project = new XmlDocument();
                project.Load(Request.InputStream);
                StorageManager.CurrentProject.Xml = project;
                Response.ContentType = "text/xml";
            } catch {
                Response.Write(SessionExpired);
            }

            Response.End();
        }

    }
}