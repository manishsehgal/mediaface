using System;
using System.Web;
using System.Web.SessionState;
using System.Web.UI.WebControls;
using System.Xml;

using Neato.Dol.Business;
using Neato.Dol.Entity;

namespace Neato.Dol.WebDesigner.HttpHandlers
{

	public class TemplateHandler : IHttpHandler, IRequiresSessionState {
		public TemplateHandler(){}
        private const string FaceIdParamName = "FaceId";
        private const string FacePUIDParamName = "PUID";
        public void ProcessRequest(HttpContext context) 
        {
            HttpResponse Response = context.Response;
            HttpRequest request = context.Request;
            if(request.QueryString[FacePUIDParamName] != null) {
                Guid id = new Guid(request.QueryString[FacePUIDParamName]);
                int faceId = BCFace.GetFaceIdByGuid(id);
                Face face = BCFace.GetFace(new FaceBase(faceId));
                XmlDocument xml = BCTemplate.GetTemplatesForFace(face);
                Response.ContentType="text/xml";
                Response.Write(xml.OuterXml);
            } 
            Response.End();
        }

        public bool IsReusable 
        {
            get { return true; }
        }
	}
}
