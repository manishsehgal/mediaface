using System;
using System.Web;
using System.Web.SessionState;
using System.Xml;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner.HttpHandlers 
{
	public class SessionCountHandler: IHttpHandler, IRequiresSessionState {
		public SessionCountHandler() {
		}

        #region Url
        private const string RawUrl = "SessionCount.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static string PageName() {
            return RawUrl;
        }
        #endregion

        public static string GetXmlData() {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml("<SiteMaintenance SessionsCount=\"\" IsSiteClosed=\"\" />");
            xmlDoc.DocumentElement.SelectSingleNode("@SessionsCount").Value = ((Global)(HttpContext.Current.ApplicationInstance)).SessionsCount.ToString();
            xmlDoc.DocumentElement.SelectSingleNode("@IsSiteClosed").Value = StorageManager.IsSiteClosed.ToString();
            return xmlDoc.OuterXml;
        }

        public void ProcessRequest(HttpContext context) {
            string isSiteClosed = context.Request.QueryString["isSiteClosed"];
            if(isSiteClosed != null)
                StorageManager.IsSiteClosed = bool.Parse(isSiteClosed);

            context.Response.Clear();
            context.Response.ContentType = "text/plain";
            string data = GetXmlData();
            context.Response.Write(data);
            context.Response.End();
        }

        public bool IsReusable {
            get { return true; }
        }
    }
}
