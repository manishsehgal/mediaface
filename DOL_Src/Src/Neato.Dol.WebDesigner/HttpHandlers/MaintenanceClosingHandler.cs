using System;
using System.IO;
using System.Web;
using System.Web.SessionState;

using Neato.Dol.Business;
using Neato.Dol.Entity.Helpers;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner.HttpHandlers {
    public class MaintenanceClosingHandler : IHttpHandler, IRequiresSessionState {
        public MaintenanceClosingHandler() {}

        #region Url
        private const string RawUrl = "MaintenanceClosing.aspx";
        public const string RetailerKey = "Retailer";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static string PageName() {
            return RawUrl;
        }
        #endregion

        public void ProcessRequest(HttpContext context) {
            switch (context.Request.RequestType) {
                case "GET":
                    GetMode(context);
                    break;
                case "POST":
                    PostMode(context);
                    break;
            }
        }

        public bool IsReusable {
            get { return true; }
        }

        private void PostMode(HttpContext context) {
            StreamReader reader = new StreamReader(context.Request.InputStream);
            string htmlSource = reader.ReadToEnd();

            string message = string.Empty;

            try {
                BCMaintenanceClosing.SaveClosePage(htmlSource, context.Request.PhysicalApplicationPath);
                message = "Close page was successfully updated";
            } catch (Exception ex) {
                message = ex.Message;
            }
            byte[] data = ConvertHelper.StringToByteArray(message);
            context.Response.OutputStream.Write(data, 0, data.Length);
        }

        private void GetMode(HttpContext context) {
            try {
                byte[] data = BCMaintenanceClosing.LoadClosePage(context.Request.PhysicalApplicationPath);
                context.Response.ContentType = "text/html";
                context.Response.AddHeader("FromHandler", bool.TrueString);
                context.Response.OutputStream.Write(data, 0, data.Length);
            } catch (Exception ex) {
                throw ex;
            }
        }
    }
}