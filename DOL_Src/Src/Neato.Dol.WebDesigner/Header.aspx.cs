using System;
using System.Web.UI;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Controls;

namespace Neato.Dol.WebDesigner {
    public class Header : Page {
        protected Header2 ctlHead;
        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.ctlHead.Logout += new EventHandler(ctlHead_Logout);
            this.DataBinding += new EventHandler(Header_DataBinding);
        }
        #endregion

        private void ctlHead_Logout(object sender, EventArgs e) {
            DataBind();
            Page.RegisterStartupScript("script", "<script>try{window.top.UpdateSettings();}catch(ex){};</script>");
        }

        private void Header_DataBinding(object sender, EventArgs e) {
            if (StorageManager.CurrentProject != null) {
                PaperBase paperBase = StorageManager.CurrentProject.ProjectPaper;
                string paperName = BCPaper.GetPaper(paperBase).Name;
                ctlHead.PaperName = paperName;
            }
        }
    }
}