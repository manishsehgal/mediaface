using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Tracking;
using Neato.Dol.WebDesigner.Controls;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebDesigner {
    public class DefaultPage : BasePage2 {
        protected Literal litCookieDisabled;

        protected BannerControl ctlBanner1;
        protected BannerControl ctlBanner2;
        protected BannerControl ctlBanner3;
        protected BannerControl ctlBanner4;
        protected BannerControl ctlBanner5;
        protected BannerControl ctlBanner6;
        protected BannerControl ctlBanner7;
        protected DataList lstTypes;

        private const string imgTypeName = "imgType";
        private const string hypCategoryName = "hypCategory";
        private const string litTextName = "litText";
        private const string keywordsParamKey = "DefaultPageKeywords";

        #region Url

        private const string RawUrl = "default.aspx";
        private const string CategoryKey = "Category";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        new public static string PageName() {
            return RawUrl;
        }

        public static string BannerFolder {
            get { return "Default"; }
        }

        private string Url(Category category) {
            return UrlHelper.BuildUrl(SelectPage.GetPageNameById(category.TargetPage), CategoryKey, category.Id).PathAndQuery;
        }

        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
//            Response.Cache.SetCacheability(HttpCacheability.NoCache);
//            Response.Cache.SetExpires(DateTime.Now);
//            Response.Cache.SetValidUntilExpires(false);
            Session["IsWasAtHomePage"] = new bool();
            if (!IsPostBack) {
                if (User.IsInRole(Authentication.RegisteredUser)) {
                    BCTracking.Add(UserAction.Login, Request.UserHostAddress);
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            pageTemplateUrl = DefaultPageTemplate.Url().AbsolutePath;
            rawUrl = RawUrl;
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Page_DataBinding);
            this.lstTypes.ItemDataBound += new DataListItemEventHandler(lstTypes_ItemDataBound);
            ctlBanner1.DataBinding += new EventHandler(BannerDataBinding);
            ctlBanner2.DataBinding += new EventHandler(BannerDataBinding);
            ctlBanner3.DataBinding += new EventHandler(BannerDataBinding);
            ctlBanner4.DataBinding += new EventHandler(BannerDataBinding);
            ctlBanner5.DataBinding += new EventHandler(BannerDataBinding);
            ctlBanner6.DataBinding += new EventHandler(BannerDataBinding);
            ctlBanner7.DataBinding += new EventHandler(BannerDataBinding);
        }

        #endregion

        private void Page_DataBinding(object sender, EventArgs e) {
            litCookieDisabled.Text = DefaultPageStrings.TextCookieDisabled();
            headerText = DefaultPageStrings.TextCaption();
            headerHtml = string.Empty;

            bool isSpecialUser = false;
            Customer customer = StorageManager.CurrentCustomer;
            if (customer != null) {
                SpecialUser[] specialUsers = BCSpecialUser.GetSpecialUsers(customer.Email);
                if (specialUsers.Length != 0)
                    isSpecialUser = true;
            }

            lstTypes.DataSource = BCCategory.GetVisibleCategoryList(isSpecialUser);
        }

        private void lstTypes_ItemDataBound(object sender, DataListItemEventArgs e) {
            Category item = e.Item.DataItem as Category;
            DataListItem listItem = e.Item;
            HtmlAnchor hypCategory = (HtmlAnchor) listItem.FindControl(hypCategoryName);
            Literal litText = (Literal) listItem.FindControl(litTextName);
            Image imgType = (Image) listItem.FindControl(imgTypeName);
            hypCategory.HRef = Url(item);
            litText.Text = HttpUtility.HtmlEncode(item.Name);
            imgType.ImageUrl = IconHandler.ImageUrl(item).PathAndQuery;
        }

        private void BannerDataBinding(object sender, EventArgs e) {
            BannerControl banner = (BannerControl) sender;
            int bannerId = banner.BannerId;
            banner.Banner = BCBanner.GetBanner(BannerFolder, bannerId);
            int height = BCBanner.GetBannerHeight(BannerFolder, bannerId);
            if (height != -1)
                banner.Height = height;
        }

        public string GetKeywords() {
            return BCParameters.GetParameterByKey(keywordsParamKey);
        }
    }
}