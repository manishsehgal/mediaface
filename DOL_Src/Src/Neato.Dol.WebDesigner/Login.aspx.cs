using System;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Controls;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner {
    public class Login : BasePage2 {
        protected Label lblEmail;
        protected TextBox txtEmail;
        protected Label lblPassword;
        protected TextBox txtPassword;
        protected CustomValidator vldLogin;
        protected ImageButton btnLogin;
        protected ImageButton btnCancel;
        protected HyperLink hypForgotPassword;
        protected HtmlImage imgNewAccount;
        protected BannerControl ctlBanner1;
        protected BannerControl ctlBanner2;

        public string ReferralUrl {
            get {
                object url = ViewState["referralUrl"];
                return (url != null) ? url.ToString(): null;
            }
            set { ViewState["referralUrl"] = value; }
        }

        public const string DesignerModeParam = "DesignerMode";
        public bool DesignerMode {
            get {
                string mode = Request.QueryString[DesignerModeParam];
                if (mode == null)
                    return false;
                bool result;
                try {
                    result = bool.Parse(mode);
                } catch {
                    result = false;
                }
                return result;
            }
        }

        #region Url
        private const string RawUrl = "Login.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        new public static string PageName() {
            return RawUrl;
        }
        public static string BannerFolder {
            get { return "Login"; }
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            ReferralUrl = Request.QueryString["ReferralUrl"];
            //if (!IsPostBack) {
                DataBind();
            //}
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            rawUrl = RawUrl;
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Login_DataBinding);
            this.btnLogin.Click += new System.Web.UI.ImageClickEventHandler(btnLogin_Click);
            this.btnCancel.Click += new System.Web.UI.ImageClickEventHandler(btnCancel_Click);
            this.vldLogin.ServerValidate += new ServerValidateEventHandler(vldLogin_ServerValidate);
            this.ctlBanner1.DataBinding += new EventHandler(BannerDataBinding);
            this.ctlBanner2.DataBinding += new EventHandler(BannerDataBinding);
        }
        #endregion

        private void Login_DataBinding(object sender, EventArgs e) {
            lblEmail.Text = LoginStrings.TextEmail();
            lblPassword.Text = LoginStrings.TextPassword();
            vldLogin.ErrorMessage = LoginStrings.TextLoginFailed();
            hypForgotPassword.Text = LoginStrings.TextForgotPassword();
            hypForgotPassword.NavigateUrl = ForgotPassword.Url().PathAndQuery;

            headerText = LoginStrings.TextCaption();

            txtPassword.Attributes["param"] = btnLogin.ClientID;
            txtEmail.Attributes["param"] = btnLogin.ClientID;
        }

        private void RedirectToPreviousPage() {
            if (ReferralUrl != null) {
                bool isReferrelPageForgotPassword = 
                    ReferralUrl.IndexOf(ForgotPassword.PageName()) >= 0;
                if (!isReferrelPageForgotPassword)
                    Response.Redirect(HttpUtility.HtmlDecode(ReferralUrl));
            }
            Response.Redirect(DefaultPage.Url().PathAndQuery);
        }

        private void btnLogin_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
            vldLogin.Validate();

            if(vldLogin.IsValid) {
                Customer customer = BCCustomer.GetCustomer(txtEmail.Text.Trim(), txtPassword.Text);
                if (customer != null) {
                    StorageManager.CurrentCustomer = customer;
                    if (DesignerMode) {
                        RegisterStartupScript("script", "<script>CloseLogin();</script>");
                        return;
                    }
                }

                RedirectToPreviousPage();
            }
            DataBind();
        }

        private void vldLogin_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = BCCustomer.GetCustomer
                (txtEmail.Text.Trim(), txtPassword.Text) != null;
        }

        private void btnCancel_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
            if (DesignerMode) {
                RegisterStartupScript("script", "<script>CloseLogin();</script>");
            } else RedirectToPreviousPage();
        }
    
        private void BannerDataBinding(object sender, EventArgs e) {
            BannerControl banner = (BannerControl)sender;
            int bannerId = banner.BannerId;
            banner.Banner = BCBanner.GetBanner(BannerFolder, bannerId);
            int height = BCBanner.GetBannerHeight(BannerFolder, bannerId);
            if (height != -1)
                banner.Height = height;
        }
    }
}