<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FooterMenu.ascx.cs" Inherits="Neato.Dol.WebDesigner.Controls.FooterMenu" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="dol" TagName="LinkMenu" Src="LinkMenu.ascx" %>

<div id="footer">
	<!--<div id="copyright">MediaFace Online &copy; 2006 Neato. Inc.</div>
	
	<ul class="footer-nav">
		<li><a id="hypTermsOfUse" runat="server" target="_top">Terms of use</a></li>
		<li class="end"><a href="javascript:;">Legal Agreement</a></li>
	</ul>
	
	<div class="clear"></div>-->
	
<table style="width:100%;height:100%;">
    <tr>
        <td align="left">
            <div id="copyright">
            <dol:LinkMenu id="ctlLinkMenuLeft" runat="server" LinkCssClass="footerLink" LabelCssClass="copyright"/>
            </div>
        </td>
        <td align="right">
            <div id="footer-nav">
                <dol:LinkMenu id="ctlLinkMenuRight" runat="server" LinkCssClass="footerLink" LabelCssClass="footerNavCellClass"/>
            </div>
        </td>
    </tr>
</table>
</div>
<!-- end of footer -->

</div>
<!-- end of root -->

</div>
