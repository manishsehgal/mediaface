<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Header2.ascx.cs" Inherits="Neato.Dol.WebDesigner.Controls.Header2" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="dol" TagName="LoginPanel" Src="LoginPanel.ascx" %>
<div style="HEIGHT: 59px;"></div>
<div id="one-column">

<!-- START root -->
<div id="root">

<div id="logo"><a id="hypLogo" runat="server" target="_top"><img src="images/logo2.gif" alt="MediaFace online" /></a></div>
<!-- meta -->
<div id="meta">
	<!-- meta navigation -->
	<div id="meta-nav">
		<h2 class="offscreen">Meta navigation</h2>
		<ul>
			<li class="help"><a id="hypHelp" runat="server" target="_top">Help</a></li>
		</ul>
	</div>
	<!-- end of meta navigation -->

   <div style="float:right;height:32px;"></div>
	<dol:LoginPanel id="ctlLoginPanel" runat="server"/>
	
</div>
<!-- end of meta -->

<div id="powered-by"><a href="http://www.neato.com/" target="_blank"><img src="images/neato.gif" width="108" height="24" alt="Powered by NEATO" /></div></a>

</div>
<!-- end of root -->

</div>