<%@ Control Language="c#" AutoEventWireup="false" Codebehind="LinkMenu.ascx.cs" Inherits="Neato.Dol.WebDesigner.Controls.LinkMenu" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<asp:Repeater id="repItems"
        Runat="server"
        EnableViewState="True">
    <ItemTemplate>
        <asp:Literal id="ctlItemTemplate" Runat="server" />
    </ItemTemplate>
</asp:Repeater>