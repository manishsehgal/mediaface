using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner.Controls {
    public class LoginPanel : UserControl {
        protected HtmlAnchor hypCreateAccount;
        protected HtmlAnchor hypLogin;
        protected HtmlAnchor hypLogout;
        protected HtmlAnchor hypEditAccount;
        protected Label lblLogged;
        protected Panel panLogin;
        protected Panel panLogout;
        protected Panel pnlDesigner;
        protected Label lblPaperName;

        public event EventHandler Logout;
        protected virtual void OnLogout() {
            if (Logout != null)
                Logout(this, EventArgs.Empty);
        }
        
        private bool userLoginedValue;
        public bool UserLogined {
            get { return userLoginedValue; }
            set { userLoginedValue = value; }
        }

        private string loggedText {
            get {
                return string.Format("<b>Logged in as {0}</b>", StorageManager.CurrentCustomer.FullName);
            }
        }

        private string paperNameValue = String.Empty;
        public string PaperName {
            get { return paperNameValue; }
            set { paperNameValue = value; }
        }

        private string referralUrl;
        public string ReferralUrl {
            get { return referralUrl; }
            set { referralUrl = value; }
        }

        private void Page_Load(object sender, EventArgs e) {
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(this.LoginPanel_DataBinding);
            this.hypLogout.ServerClick += new EventHandler(hypLogout_ServerClick);

        }
        #endregion

        private void LoginPanel_DataBinding(object sender, EventArgs e) {
            panLogin.Visible = !UserLogined;
            panLogout.Visible = UserLogined;

            hypLogin.HRef = 
                (ReferralUrl != null)
                ? UrlHelper.BuildUrl
                (Login.Url().PathAndQuery, "ReferralUrl", 
                ReferralUrl).PathAndQuery
                : Login.Url().PathAndQuery;

            hypCreateAccount.HRef =
                UrlHelper.BuildUrl
                (CustomerAccount.Url().PathAndQuery, "mode",
                "new").PathAndQuery;

            hypEditAccount.HRef = 
                UrlHelper.BuildUrl
                (CustomerAccount.Url().PathAndQuery, "mode",
                "edit").PathAndQuery;

            lblLogged.Text = (UserLogined) ? loggedText : "";

            pnlDesigner.Visible = paperNameValue.Length > 0;
            lblPaperName.Text = HttpUtility.HtmlEncode(paperNameValue);

				if(pnlDesigner.Visible) {
	            hypCreateAccount.Target = "_blank";
	            hypEditAccount.Target = "_blank";
				}
        }

        private void hypLogout_ServerClick(object sender, EventArgs e) {
            StorageManager.CurrentCustomer = null;
            if (Page is CustomerAccount) {
                Response.Redirect(DefaultPage.Url().PathAndQuery);
            } else {
                OnLogout();
            }
        }
    }
}