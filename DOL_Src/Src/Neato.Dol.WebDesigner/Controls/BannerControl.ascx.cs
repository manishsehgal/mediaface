using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

using Neato.Dol.Entity;

namespace Neato.Dol.WebDesigner.Controls {
    public class BannerControl : UserControl {
        protected HtmlGenericControl frame;
        protected HtmlGenericControl divBannerBox;
        protected HtmlGenericControl divBannerDecorTop;
        protected HtmlGenericControl divBannerDecorBottom;
        protected HtmlGenericControl divBanner;

        private int left;
        private int top;
        private int width;
        private int height;
        private Banner banner;
        private int bannerId;
        private bool showBorder = true;

        public int BannerId {
            get { return bannerId; }
            set { bannerId = value; }
        }

        public int Left {
            get { return left; }
            set { left = value; }
        }

        public int Top {
            get { return top; }
            set { top = value; }
        }

        public int Width {
            get { return width; }
            set { width = value; }
        }

        public int Height {
            get { return height; }
            set { height = value; }
        }

        public Banner Banner {
            get { return banner; }
            set { banner = value; }
        }

        public bool ShowBorder {
            get { return showBorder; }
            set { showBorder = value; }
        }


        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.PreRender += new EventHandler(BannerControl_PreRender);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
        }

        private void BannerControl_PreRender(object sender, EventArgs e) {
            frame.Style["width"] = width + "px";
            frame.Style["height"] = height + "px";

            frame.Attributes["src"] = (banner.IsVisible) ? banner.Src : "";
            if (!showBorder) {
                divBannerBox.Style["border-width"] = "0px";
                divBannerDecorTop.Style["visibility"] = "hidden";
                divBannerDecorBottom.Style["visibility"] = "hidden";
            }

            if (banner.IsHidden) {
                divBanner.Style["visibility"] = "hidden";            
            }else {
                divBanner.Style["visibility"] = "visible";             
            }
        }
    }
}