using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.WebDesigner.Controls {
    public class LinkMenu : UserControl {
        protected Repeater repItems;
        protected HtmlTable menuTable;

        private object dataSourceValue;
        private string textFieldValue = string.Empty;
        private string urlFieldValue = string.Empty;
        private string isNewWindowFieldValue = string.Empty;
        private string cellCssClassValue = string.Empty;
        private string linkCssClassValue = string.Empty;
        private string currentLinkCssClassValue = string.Empty;
        private string labelCssClassValue = string.Empty;
        private string delimeterValue = string.Empty;
        

        #region Properties
        public object DataSource {
            get { return dataSourceValue; }
            set { dataSourceValue = value; }
        }

        public string TextField {
            get { return textFieldValue; }
            set { textFieldValue = value; }
        }

        public string UrlField {
            get { return urlFieldValue; }
            set { urlFieldValue = value; }
        }

        public string IsNewWindowField {
            get { return isNewWindowFieldValue; }
            set { isNewWindowFieldValue = value; }
        }

        public string CellCssClass {
            get { return cellCssClassValue; }
            set { cellCssClassValue = value; }
        }
        
        public string LinkCssClass {
            get { return linkCssClassValue; }
            set { linkCssClassValue = value; }
        }
        public string CurrentLinkCssClass {
            get { return currentLinkCssClassValue; }
            set { currentLinkCssClassValue = value; }
        }
        public string LabelCssClass {
            get { return labelCssClassValue; }
            set { labelCssClassValue = value; }
        }
        public string Delimeter {
            get { return delimeterValue; }
            set { delimeterValue = value; }
        }
        #endregion Properties

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.DataBinding += new EventHandler(LinkMenu_DataBinding);
            this.repItems.ItemDataBound += new RepeaterItemEventHandler(lstItems_ItemDataBound);
        }
        #endregion

        #region Data binding
        private void LinkMenu_DataBinding(object sender, EventArgs e) {
            repItems.DataSource = dataSourceValue;
        }

        private void lstItems_ItemDataBound(object sender, RepeaterItemEventArgs e) {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem) {
                ItemDataBound(e.Item);
            }
        }

        private void ItemDataBound(RepeaterItem repeaterItem) {
            Object item = repeaterItem.DataItem;

            string text = ReflectionHelper.GetFieldOrPropertyValue(item, TextField).ToString();
            string url = ReflectionHelper.GetFieldOrPropertyValue(item, UrlField).ToString();
            string target = (bool)ReflectionHelper.GetFieldOrPropertyValue(item, IsNewWindowField) ? "_blank" : "";
            
            Literal lit = (Literal)repeaterItem.FindControl("ctlItemTemplate");
            bool isCurrentLink = Request.Url.AbsoluteUri.IndexOf(url) > 0;
            string linkStyle =  (isCurrentLink && currentLinkCssClassValue.Length > 0) ? currentLinkCssClassValue : linkCssClassValue;
            
            if(dataSourceValue != null 
                && dataSourceValue.GetType() == typeof(LinkItem[]) 
                && ((Array)dataSourceValue).Length > 0 
                && !((Array)dataSourceValue).GetValue(0).Equals(item))
            lit.Text = delimeterValue == string.Empty ? string.Empty : string.Format("<img src='{0}'>", delimeterValue);
            
            if (labelCssClassValue.Length > 0){
                lit.Text += string.Format("<span class='{0}'>{1}</span>",
                    labelCssClassValue, text);
            } else if (cellCssClassValue.Length > 0){
                lit.Text += string.Format("<span class='{0}'><a class='{1}' href='{2}' target='{3}'>{4}</a></span>",
                    cellCssClassValue, linkStyle, url, target, text);
            } else {
                lit.Text += string.Format("<a class='{0}' href='{1}' target='{2}'>&nbsp;&nbsp;{3}&nbsp;</a>",
                    linkStyle, url, target, text);
            }
        }
        #endregion //Data binding
    }
}