<%@ Control Language="c#" AutoEventWireup="false" Codebehind="LinkList.ascx.cs" Inherits="Neato.Dol.WebDesigner.Controls.LinkList" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="cpt" TagName="LinkMenu" Src="LinkMenu.ascx" %>
<dl class="dynamicLinks">
	<asp:Label ID="lblHeader" Runat="server" CssClass="ListCaption"/>
    <cpt:LinkMenu id="ctlLinkItems" runat="server" LinkCssClass="LinkText Active"/>
</dl>