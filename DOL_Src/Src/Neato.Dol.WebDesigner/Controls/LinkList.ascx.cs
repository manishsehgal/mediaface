using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Neato.Dol.WebDesigner.Controls {
    public class LinkList : UserControl {
        protected LinkMenu ctlLinkItems;
        protected Label lblHeader;
        private object dataSourceValue;
        private string textFieldValue = "Text";
        private string urlFieldValue = "Url";
        private string isNewWindowFieldValue = "IsNewWindow";
        private string headerFieldValue = string.Empty;

        #region Properties
        public object DataSource {
            get { return dataSourceValue; }
            set { dataSourceValue = value; }
        }

        public string TextField {
            get { return textFieldValue; }
            set { textFieldValue = value; }
        }

        public string UrlField {
            get { return urlFieldValue; }
            set { urlFieldValue = value; }
        }

        public string IsNewWindowField {
            get { return isNewWindowFieldValue; }
            set { isNewWindowFieldValue = value; }
        }

        public string HeaderField {
            get { return headerFieldValue; }
            set { headerFieldValue = value; }
        }
        #endregion Properties

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.DataBinding += new EventHandler(LinkList_DataBinding);
        }
        #endregion

        #region Data binding
        private void LinkList_DataBinding(object sender, EventArgs e) {
            lblHeader.Text = headerFieldValue;
            ctlLinkItems.DataSource = dataSourceValue;
            ctlLinkItems.TextField = textFieldValue;
            ctlLinkItems.UrlField = urlFieldValue;
            ctlLinkItems.IsNewWindowField = isNewWindowFieldValue;
        }
        #endregion //Data binding
    }
}