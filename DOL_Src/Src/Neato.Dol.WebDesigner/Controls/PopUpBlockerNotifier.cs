using System;
using System.Web.UI.WebControls;

namespace Neato.Dol.WebDesigner.Controls {
    public class PopUpBlockerNotifier : WebControl {
        private string message;

        private const string popUpBlockerNotifierKey = "PopUpBlockerNotifierKey";
        private const string jsText =
        @"  <script type='text/javascript' language='javascript'> 
            function OpenWindowUsingBlockerNotifier(url, name){{
            var wnd = window.open(url, name);
            if (wnd == null) alert('{0}'); else wnd.focus();}}
            </script>";
        
        public string Message {
            get { return message; }
            set { message = value; }
        }

        public PopUpBlockerNotifier() {
            this.Load += new EventHandler(PopUpBlockerNotifier_Load);
        }

        private void PopUpBlockerNotifier_Load(object sender, EventArgs e) {
            Page.RegisterClientScriptBlock
                (popUpBlockerNotifierKey, string.Format(jsText, message));
        }
    }
}