using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace Neato.Dol.WebDesigner.Controls {
    public class Header2 : UserControl {
        protected LoginPanel ctlLoginPanel;
        protected HtmlAnchor hypLogo;
        protected HtmlAnchor hypHelp;

        public event EventHandler Logout;
        protected virtual void OnLogout() {
            if (Logout != null)
                Logout(this, EventArgs.Empty);
        }
        
        private string paperNameValue = String.Empty;
        public string PaperName {
            get { return paperNameValue; }
            set { paperNameValue = value; }
        }

        private void Page_Load(object sender, EventArgs e) {
        }

        public string ReferralUrl {
            set { ctlLoginPanel.ReferralUrl = value; }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Header2_DataBinding);
            this.ctlLoginPanel.Logout += new EventHandler(ctlLoginPanel_Logout);
        }
        #endregion

        private void Header2_DataBinding(object sender, EventArgs e) {
            ctlLoginPanel.UserLogined = 
                (StorageManager.CurrentCustomer != null);
            ctlLoginPanel.PaperName = paperNameValue;
            hypLogo.HRef = DefaultPage.Url().PathAndQuery;
            hypHelp.HRef = Support.Url().PathAndQuery;
        }

        private void ctlLoginPanel_Logout(object sender, EventArgs e) {
            OnLogout();
        }
    }
}