using System.Web.UI;

namespace Neato.Dol.WebDesigner.Controls {
    using System;
    using System.Web.UI.WebControls;

    public class StepSelector : System.Web.UI.UserControl {

        protected ImageButton btnStep1;
        protected HyperLink btnStep2;
        protected HyperLink btnStep3;
        
        private static string imgFolder = "../images/";
        private static string imgStep1Prfx = "step1_";
        private static string imgStep2Prfx = "step2_";
        private static string imgStep3Prfx = "step3_";

        private int currentStep = 1;
        public int Step {
            set {
                if( value > 0 && value < 4)
                    currentStep = value;
                else 
                    currentStep = 1;
            }
        }
        private void Page_Load(object sender, System.EventArgs e) {
			
            // Put user code to initialize the page here
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.DataBinding += new EventHandler(Header_DataBinding);
        }
        #endregion

        private void Header_DataBinding(object sender, EventArgs e) {
            btnStep1.ImageUrl = imgFolder + imgStep1Prfx + ((currentStep == 1) ? "active" : "inactive") + ".jpg";
            btnStep2.ImageUrl = imgFolder + imgStep2Prfx + ((currentStep == 2) ? "active" : "inactive") + ".jpg";
            btnStep3.ImageUrl = imgFolder + imgStep3Prfx + ((currentStep == 3) ? "active" : "inactive") + ".jpg";

            if (currentStep < 2) {
                btnStep2.Attributes["OnClick"] = "";
                btnStep3.Attributes["OnClick"] = "";

                btnStep1.Style["cursor"]="pointer";
                btnStep2.Style["cursor"]="default";
                btnStep3.Style["cursor"]="default";
            } else {
                btnStep2.Attributes["OnClick"] = @"setFlashVariables('Designer', 'gotoStep=2');";
                btnStep3.Attributes["OnClick"] = @"setFlashVariables('Designer', 'gotoStep=3');";

                btnStep1.Style["cursor"]="pointer";
                btnStep2.Style["cursor"]="default";
                btnStep3.Style["cursor"]="pointer";
            }
        }

        protected void btnStep1_Click(object sender, ImageClickEventArgs e) {
            Response.Redirect(DefaultPage.Url().PathAndQuery);
        }
    }
}