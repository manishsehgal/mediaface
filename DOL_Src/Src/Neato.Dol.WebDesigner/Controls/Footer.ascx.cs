using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace Neato.Dol.WebDesigner.Controls {
    public class Footer : UserControl {
        
        protected HtmlControl requiredLinks;
        protected LinkMenu ctlLinkMenuLeft;
        protected LinkMenu ctlLinkMenuRight;
        private object dataSourceLeftValue;
        private object dataSourceRightValue;
        private string textFieldValue = "Text";
        private string urlFieldValue = "Url";
        private string isNewWindowFieldValue = "IsNewWindow";

        #region Properties
        public object DataSourceLeft 
        {
            get { return dataSourceLeftValue; }
            set { dataSourceLeftValue = value; }
        }

        public object DataSourceRight 
        {
            get { return dataSourceRightValue; }
            set { dataSourceRightValue = value; }
        }

        public string TextField 
        {
            get { return textFieldValue; }
            set { textFieldValue = value; }
        }

        public string UrlField 
        {
            get { return urlFieldValue; }
            set { urlFieldValue = value; }
        }

        public string IsNewWindowField 
        {
            get { return isNewWindowFieldValue; }
            set { isNewWindowFieldValue = value; }
        }
        #endregion Properties

        private bool showRequiredLinksValue = true;

        public bool ShowRequiredLinks {
            get { return showRequiredLinksValue; }
            set { showRequiredLinksValue = value; }
        }

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Footer_DataBinding);
        }
        #endregion

        private void Footer_DataBinding(object sender, EventArgs e) {
        //    hypTermsOfUse.HRef = TermsOfUse.Url().PathAndQuery;
            requiredLinks.Visible = showRequiredLinksValue;
            ctlLinkMenuLeft.DataSource = dataSourceLeftValue;
            ctlLinkMenuLeft.TextField = textFieldValue;
            ctlLinkMenuLeft.UrlField = urlFieldValue;
            ctlLinkMenuLeft.IsNewWindowField = isNewWindowFieldValue;

            ctlLinkMenuRight.DataSource = dataSourceRightValue;
            ctlLinkMenuRight.TextField = textFieldValue;
            ctlLinkMenuRight.UrlField = urlFieldValue;
            ctlLinkMenuRight.IsNewWindowField = isNewWindowFieldValue;
            //ctlLinkMenuRight.CellCssClass = "footerNavCellClass";
            ctlLinkMenuRight.Delimeter = "images/decor/footer_nav_split.gif";
        }
    }
}