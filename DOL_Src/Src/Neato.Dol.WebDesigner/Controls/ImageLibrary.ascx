<%@ Control Language="c#" AutoEventWireup="false" EnableViewState="true" Codebehind="ImageLibrary.ascx.cs" Inherits="Neato.Dol.WebDesigner.Controls.ImageLibrary" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<asp:DataList ID="lstItems"
    Runat="server" 
    RepeatDirection="Horizontal"
    RepeatLayout="Table"
    RepeatColumns="6"
    EnableViewState="true">
    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
    <ItemTemplate>
		<div class="product small">
			<div class="decor-t">
				<div></div>
			</div>
			<div class="note">
				<asp:ImageButton Runat="server" ID="hypImage" CommandName="RedirectToUrl"/>
			</div>
			<div class="decor-bw">
				<div></div>
			</div>
			<div class="label">
				<div>
					<h3>
						<asp:Label Runat="server" ID="lblSku" />
					</h3>
				</div>
				<div>
					<h3>
						<asp:LinkButton ID="hypText" Runat="server" CommandName="RedirectToUrl"/>
					</h3>
				</div>
				<div>
					<h3>
						<asp:Label Runat="server" ID="lblDescription" />
					</h3>
				</div>
				<div class="decor-b">
					<div></div>
				</div>
			</div>
		</div>
    </ItemTemplate>
</asp:DataList>
