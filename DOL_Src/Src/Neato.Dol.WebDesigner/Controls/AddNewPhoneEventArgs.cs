using System;
using Neato.Dol.Entity;

namespace Neato.Dol.WebDesigner.Controls {
	public class AddNewPhoneEventArgs : EventArgs {
        private DeviceRequest deviceRequestValue;

        public AddNewPhoneEventArgs(DeviceRequest deviceRequest) : base() {
            deviceRequestValue = deviceRequest;
        }

        public AddNewPhoneEventArgs() : base() {
            deviceRequestValue = new DeviceRequest();
        }

        public DeviceRequest DeviceRequest 
        {
            get { return deviceRequestValue; }
	    }
	}
}
