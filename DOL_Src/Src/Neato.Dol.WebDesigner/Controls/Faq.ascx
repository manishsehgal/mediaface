<%@ Register TagPrefix="cpt" TagName="FaqSection" Src="FaqSection.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Faq.ascx.cs" Inherits="Neato.Dol.WebDesigner.Controls.Faq" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<asp:Repeater id="repItems" Runat="server" EnableViewState="True">
    <ItemTemplate>
        <cpt:FaqSection id="ctlFaqSection" runat="server"/><br>
    </ItemTemplate>
</asp:Repeater>