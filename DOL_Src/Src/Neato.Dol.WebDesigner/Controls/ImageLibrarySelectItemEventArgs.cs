using System;

namespace Neato.Dol.WebDesigner.Controls {
	public sealed class ImageLibrarySelectItemEventArgs : EventArgs {
        private string navigateUrlValue;

        public string NavigateUrl {
            get { return navigateUrlValue; }
        }

		public ImageLibrarySelectItemEventArgs(string navigateUrl) : base() {
            navigateUrlValue = navigateUrl;
		}
	}
}