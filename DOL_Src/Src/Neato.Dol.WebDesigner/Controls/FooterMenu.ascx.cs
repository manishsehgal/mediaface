using System;
using System.Web.UI;

namespace Neato.Dol.WebDesigner.Controls {
    public class FooterMenu : UserControl {
        protected LinkMenu ctlLinkMenuLeft;
        protected LinkMenu ctlLinkMenuRight;
        private object dataSourceLeftValue;
        private object dataSourceRightValue;
        private string textFieldValue = "Text";
        private string urlFieldValue = "Url";
        private string isNewWindowFieldValue = "IsNewWindow";

        #region Properties
        public object DataSourceLeft {
            get { return dataSourceLeftValue; }
            set { dataSourceLeftValue = value; }
        }

        public object DataSourceRight {
            get { return dataSourceRightValue; }
            set { dataSourceRightValue = value; }
        }

        public string TextField {
            get { return textFieldValue; }
            set { textFieldValue = value; }
        }

        public string UrlField {
            get { return urlFieldValue; }
            set { urlFieldValue = value; }
        }

        public string IsNewWindowField {
            get { return isNewWindowFieldValue; }
            set { isNewWindowFieldValue = value; }
        }
        #endregion Properties

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.DataBinding += new EventHandler(FooterMenu_DataBinding);
        }
        #endregion

        #region Data binding
        private void FooterMenu_DataBinding(object sender, EventArgs e) {
            ctlLinkMenuLeft.DataSource = dataSourceLeftValue;
            ctlLinkMenuLeft.TextField = textFieldValue;
            ctlLinkMenuLeft.UrlField = urlFieldValue;
            ctlLinkMenuLeft.IsNewWindowField = isNewWindowFieldValue;

            ctlLinkMenuRight.DataSource = dataSourceRightValue;
            ctlLinkMenuRight.TextField = textFieldValue;
            ctlLinkMenuRight.UrlField = urlFieldValue;
            ctlLinkMenuRight.IsNewWindowField = isNewWindowFieldValue;
            //ctlLinkMenuRight.CellCssClass = "footerNavCellClass";
            ctlLinkMenuRight.Delimeter = "images/decor/footer_nav_split.gif";
        }
        #endregion //Data binding
    }
}