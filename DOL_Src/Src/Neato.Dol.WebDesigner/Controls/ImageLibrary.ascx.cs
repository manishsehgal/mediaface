using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.WebDesigner.Controls {
    public class ImageLibrary : UserControl {
        protected DataList lstItems;

        private const string HypImageId = "hypImage";
        private const string HypTextId = "hypText";
        private const string LblSkuId = "lblSku";
        private const string LblDescriptionId = "lblDescription";

        public object DataSource {
            get { return ViewState["ImageLibraryDataSource"]; }
            set { ViewState["ImageLibraryDataSource"] = value; }
        }

		public int ColumsNum {
			get{return lstItems.RepeatColumns;}
			set{lstItems.RepeatColumns= value;}
		}
		
        public delegate void SelectItemEventHandler(Object sender, ImageLibrarySelectItemEventArgs e);

        public event SelectItemEventHandler RedirectToUrl;

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.lstItems.ItemDataBound += new DataListItemEventHandler(lstItems_ItemDataBound);
            this.DataBinding += new EventHandler(ImageLibrary_DataBinding);
            this.lstItems.ItemCommand += new DataListCommandEventHandler(lstItems_ItemCommand);
        }
        #endregion

        #region Data binding
        private void ImageLibrary_DataBinding(object sender, EventArgs e) {
            lstItems.DataSource = DataSource;
        }

        private void lstItems_ItemDataBound(object sender, DataListItemEventArgs e) {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem) {
                ItemDataBound(e.Item);
            }
        }

        private void ItemDataBound(DataListItem listItem) {
            ListSelectItem item = (ListSelectItem)listItem.DataItem;

            ImageButton hypImage = (ImageButton)listItem.FindControl(HypImageId);
            LinkButton hypText = (LinkButton)listItem.FindControl(HypTextId);
            Label lblSku = (Label)listItem.FindControl(LblSkuId);
            Label lblDescription = (Label)listItem.FindControl(LblDescriptionId);

            if (item.ImageUrl != null)
                hypImage.ImageUrl = item.ImageUrl;
            else
                hypImage.Visible = false;
            if (item.Text != null)
                hypText.Text = item.Text;
            else
                hypText.Visible = false;
            if (item.SkuNumber != null)
                lblSku.Text = item.SkuNumber;
            else
                lblSku.Visible = false;
            if (item.Description != null)
                lblDescription.Text = item.Description;
            else
                lblDescription.Visible = false;
        }
        #endregion

        protected virtual void OnRedirectToUrl(DataListItem dataListItem) {
            ListSelectItem selectedItem = ((ListSelectItem[])DataSource)[dataListItem.ItemIndex];
            string url = selectedItem.NavigateUrl;
            if (RedirectToUrl != null) {
                ImageLibrarySelectItemEventArgs e = new ImageLibrarySelectItemEventArgs(url);
                RedirectToUrl(this, e);
            }
        }

        private void lstItems_ItemCommand(object source, DataListCommandEventArgs e) {
            switch(e.CommandName) {
                case "RedirectToUrl":
                    OnRedirectToUrl(e.Item);
                    break;
            }
        }
    }
}