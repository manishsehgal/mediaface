using System;
using Neato.Dol.Entity;

namespace Neato.Dol.WebDesigner.Controls {
    public sealed class AccessToAccountEventArgs : EventArgs {
        private Customer customerValue;
        private bool customerExistsValue;

        public Customer Customer {
            get { return customerValue; }
            set { customerValue = value; }
        }

        public bool CustomerExists {
            get { return customerExistsValue; }
            set { customerExistsValue = value; }
        }

        public AccessToAccountEventArgs(Customer customer) : base() {
            customerValue = customer;
            customerExistsValue = false;
        }
    }
}
