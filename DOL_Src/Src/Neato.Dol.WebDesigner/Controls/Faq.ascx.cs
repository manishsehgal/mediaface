using System;
using System.Data;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Data.Scheme;

namespace Neato.Dol.WebDesigner.Controls
{
	public class Faq : System.Web.UI.UserControl
	{
        protected Repeater repItems;

		private void Page_Load(object sender, System.EventArgs e){
            if (!IsPostBack) DataBind();
		}

        public object faqData;

        public object DataSource {
            get { return faqData; }
            set { faqData = value; }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent() {
			this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Faq_DataBinding);
            this.repItems.ItemDataBound += new RepeaterItemEventHandler(repItems_ItemDataBound);
		}
        #endregion

        private void Faq_DataBinding(object sender, EventArgs e) {
            repItems.DataSource = DataSource;
        }

        private void repItems_ItemDataBound(object sender, RepeaterItemEventArgs e) {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem) {

                FaqData.FaqRow row = (FaqData.FaqRow)((DataRowView)e.Item.DataItem).Row;
                FaqSection faqSection = (FaqSection) e.Item.FindControl("ctlFaqSection");

                faqSection.SetData(row.Question, row.Answer);
            }
        }
    }
}