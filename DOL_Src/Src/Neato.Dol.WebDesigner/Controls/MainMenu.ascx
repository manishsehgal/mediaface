<%@ Control Language="c#" AutoEventWireup="false" Codebehind="MainMenu.ascx.cs" Inherits="Neato.Dol.WebDesigner.Controls.MainMenu" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="cpt" TagName="LinkMenu" Src="LinkMenu.ascx" %>
<cpt:LinkMenu id="ctlLinkMenu" runat="server" CellCssClass="MainMenuCell"
    CurrentLinkCssClass="MainMenuLinkInactive" LinkCssClass="MainMenuLinkActive"
    Delimeter="images/Spacer.gif" />
