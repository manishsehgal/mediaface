<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BannerControl.ascx.cs" Inherits="Neato.Dol.WebDesigner.Controls.BannerControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<div id="divBanner" runat="server" style="visibility: visible; text-align:center;">
	<div class="box-small" id="divBannerBox" runat="server">
		<div class="decor-t" id="divBannerDecorTop" runat="server">
			<div></div>
		</div>
		<iframe class="Banner" id="frame" runat="server" frameborder="0" scrolling="no"/>
		<div class="decor-b" id="divBannerDecorBottom" runat="server">
			<div></div>
		</div>
	</div>
</div>