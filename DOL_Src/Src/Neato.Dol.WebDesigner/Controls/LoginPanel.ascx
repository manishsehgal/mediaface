<%@ Control Language="c#" AutoEventWireup="false" Codebehind="LoginPanel.ascx.cs" Inherits="Neato.Dol.WebDesigner.Controls.LoginPanel" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<div id="utility-links">
    <asp:Panel Runat="server" ID="panLogin">
        <ul>
            <li>
                |&nbsp;&nbsp;&nbsp;<a id="hypCreateAccount" runat="server" target="_top">Create an account</a>
            <li>
                <a id="hypLogin" runat="server" onclick="try{return window.top.DesignerLogin();}catch(ex){return true;}">Log in</a>
            </li>
        </ul>
    </asp:Panel>
    <asp:Panel Runat="server" ID="panLogout">
        <ul>
            <li>
                |&nbsp;&nbsp;&nbsp;<a id="hypLogout" runat="server">Logout</a>
            <li>
                |&nbsp;&nbsp;&nbsp;<a id="hypEditAccount" runat="server" target="_top">Edit account</a>
            <li>            
                <a href="#"><asp:Label id="lblLogged" Runat="server"></asp:Label></a>
            </li>
        </ul>
    </asp:Panel>
    <asp:Panel Runat="server" ID="pnlDesigner">
      <div id="DesinerPanel">
        <b>DESIGNING </b>&nbsp;&nbsp;
        <asp:Label id="lblPaperName" Runat="server"></asp:Label>
        <!--&nbsp;|&nbsp;
        <span onclick="javascript:SwitchProduct();" style="CURSOR:pointer">switch product</span>
        -->
      </div>
    </asp:Panel>
</div>
