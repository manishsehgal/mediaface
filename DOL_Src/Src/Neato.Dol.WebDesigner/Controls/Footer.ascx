<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Footer.ascx.cs" Inherits="Neato.Dol.WebDesigner.Controls.Footer" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="dol" NameSpace="Neato.Dol.WebDesigner.Controls" Assembly="Neato.Dol.WebDesigner" %>
<%@ Register TagPrefix="dol" TagName="LinkMenu" Src="LinkMenu.ascx" %>
<div id="one-column">

<!-- START root -->
<div id="root">

<!-- footer -->
<div id="requiredLinks" runat="server">
  <br clear="all">
  <div id="required-links">
	<h2 class="offscreen">Required programms</h2>
	
	<ul>
	
	<li><span>Adobe Flash Player<br /> is required to run MediaFace Online</span>
	<a href="http://www.adobe.com/go/getflashplayer/" target="_blank"><img src="images/flash_player.gif" alt="Adobe Flash Player" /></a></li>

	<li><span>Adobe Acrobat Reader<br /> is required to print your labels</span>
	<a href="http://www.adobe.com/products/acrobat/readstep2.html" target="_blank"><img src="images/adobe_reader.gif" alt="Adobe Acrobat Reader" /></a></li>
	
	</ul>
  </div>
</div>


<div id="footer">
	<!--<div id="copyright">MediaFace Online &copy; 2006 Neato. Inc.</div>
	
	<ul class="footer-nav">
		<li><a id="hypTermsOfUse" runat="server" target="_top">Terms of use</a></li>
		<li class="end"><a href="javascript:;">Legal Agreement</a></li>
	</ul>
	
	<div class="clear"></div>-->
	
<table style="width:100%;height:30px;">
    <tr>
        <td align="left" style="width:50%">
            <div id="copyright" class="copyright">
            <dol:LinkMenu id="ctlLinkMenuLeft" runat="server"  LabelCssClass="copyrightText"/>
            </div>
        </td>
        <td align="right" style="width:50%">
            <div id="footer-nav">
                <dol:LinkMenu id="ctlLinkMenuRight" runat="server"  CellCssClass="footerNavCellClass"/>
            </div>
        </td>
    </tr>
</table>
</div>
<!-- end of footer -->

</div>
<!-- end of root -->

</div>
