using System;
using System.Web.UI.WebControls;
using Neato.Dol.Entity;

namespace Neato.Dol.WebDesigner.Controls {
    public class AccessToAccount : System.Web.UI.UserControl {
        protected System.Web.UI.WebControls.Label lblAccessToAccount;
        protected System.Web.UI.WebControls.Label lblEmail;
        protected System.Web.UI.WebControls.TextBox txtEmail;
        protected System.Web.UI.WebControls.Label lblPassword;
        protected System.Web.UI.WebControls.TextBox txtPassword;
        protected System.Web.UI.WebControls.ImageButton btnLogin;
        protected System.Web.UI.WebControls.LinkButton btnForgotPassword;
        protected System.Web.UI.WebControls.ImageButton btnNewUser;
        protected System.Web.UI.WebControls.Label lblUserBenefits;
        protected System.Web.UI.WebControls.Label lblPersonalSettings;
        protected System.Web.UI.WebControls.Label lblInformtion;
        protected System.Web.UI.WebControls.Label lblLearnMore;
        protected System.Web.UI.WebControls.Panel panLoginForm;
        protected System.Web.UI.WebControls.Label lblLogged;
        protected System.Web.UI.WebControls.Label lblUserName;
        protected System.Web.UI.WebControls.LinkButton btnModifyProfile;
        protected System.Web.UI.WebControls.LinkButton btnLogout;
        protected System.Web.UI.WebControls.Panel panManageAccount;
        protected System.Web.UI.WebControls.CustomValidator vldLogin;

        private bool userLoginedValue;
        private Customer currentCustomerValue = null;

        public string NewUserUrl {
            get { return (string)ViewState["Neato.Dol.WebDesigner.Controls.AccessToAccount.NewUserUrl"]; }
            set { ViewState["Neato.Dol.WebDesigner.Controls.AccessToAccount.NewUserUrl"] = value; }
        }

        public string ForgotPasswordUrl {
            get { return (string)ViewState["Neato.Dol.WebDesigner.Controls.AccessToAccount.ForgotPasswordUrl"]; }
            set { ViewState["Neato.Dol.WebDesigner.Controls.AccessToAccount.ForgotPasswordUrl"] = value; }
        }

        public string ModifyProfileUrl {
            get { return (string)ViewState["Neato.Dol.WebDesigner.Controls.AccessToAccount.ModifyProfileUrl"]; }
            set { ViewState["Neato.Dol.WebDesigner.Controls.AccessToAccount.ModifyProfileUrl"] = value; }
        }

        public bool UserLogined {
            get { return userLoginedValue; }
            set { userLoginedValue = value; }
        }

        public Customer CurrentCustomer {
            get { return currentCustomerValue; }
            set {
                if((currentCustomerValue = value) != null) {
                    lblUserName.Text = currentCustomerValue.FullName;
                }
                else {
                    lblUserName.Text = string.Empty;
                }
            }
        }

        public delegate void AccessToAccountEventHandler(Object sender, AccessToAccountEventArgs e);
        public delegate void RedirectEventHandler(Object sender, RedirectEventArgs e);

        public event AccessToAccountEventHandler Login;
        public event AccessToAccountEventHandler LoginValidate;
        public event EventHandler Logout;
        public event RedirectEventHandler Redirect;

		private void Page_Load(object sender, System.EventArgs e) {
            if(UserLogined) {
                this.panLoginForm.Visible = false;
                this.panManageAccount.Visible = true;
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent() {
            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(AccessToAccount_DataBinding);
            
            this.btnForgotPassword.Click += new EventHandler(btnForgotPassword_Click);
            this.btnLogin.Click += new System.Web.UI.ImageClickEventHandler(btnLogin_Click);
            this.btnLogout.Click += new EventHandler(btnLogout_Click);
            this.btnModifyProfile.Click += new EventHandler(btnModifyProfile_Click);
            this.btnNewUser.Click += new System.Web.UI.ImageClickEventHandler(btnNewUser_Click);

            this.vldLogin.ServerValidate += new ServerValidateEventHandler(vldLogin_ServerValidate);
        }
        #endregion

        #region Data Binding
        private void AccessToAccount_DataBinding(object sender, EventArgs e) {
            this.lblAccessToAccount.Text = AccessToAccountStrings.TextAccessYourAccount();
            this.lblEmail.Text = AccessToAccountStrings.TextEmail();
            this.lblInformtion.Text = AccessToAccountStrings.TextInformation();
            this.lblLogged.Text = AccessToAccountStrings.TextLogged();
            this.lblPassword.Text = AccessToAccountStrings.TextPassword();
            this.lblPersonalSettings.Text = AccessToAccountStrings.TextPersonalSettings();
            this.lblUserBenefits.Text = AccessToAccountStrings.TextUserBenefits();
            this.lblLearnMore.Text = AccessToAccountStrings.TextLearnMore();

            this.btnForgotPassword.Text = AccessToAccountStrings.TextForgotPassword();
            this.btnLogout.Text = AccessToAccountStrings.TextLogout();
            this.btnModifyProfile.Text = AccessToAccountStrings.TextModifyProfile();

            vldLogin.ErrorMessage = AccessToAccountStrings.TextLoginFailed();
        }
        #endregion

        protected virtual Customer OnLogin(Customer customer) {
            AccessToAccountEventArgs args = new AccessToAccountEventArgs(customer);
            if (Login != null) 
                Login(this, args);
            return args.Customer;
        }

        protected virtual void OnLogout() {
            if (Logout != null)
                Logout(this, EventArgs.Empty);
        }

        protected virtual bool OnLoginValidate(Customer customer) {
            AccessToAccountEventArgs args = new AccessToAccountEventArgs(customer);
            if (LoginValidate != null)
                LoginValidate(this, args);
            return args.CustomerExists;
        }

        protected virtual void OnRedirect(string url) {
            if (Redirect != null) {
                RedirectEventArgs args = new RedirectEventArgs(url);
                Redirect(this, args);
            }
        }

        private void btnLogin_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
            vldLogin.Validate();

            if(vldLogin.IsValid) {
                Customer customer = new Customer();
                customer.Email = txtEmail.Text;
                customer.Password = txtPassword.Text;
                customer = OnLogin(customer);
                if (customer != null) {
                    CurrentCustomer = customer;
                    panLoginForm.Visible = false;
                    panManageAccount.Visible = true;
                }
            }
        }

        private void btnLogout_Click(object sender, EventArgs e) {
            OnLogout();
            CurrentCustomer = null;
            panLoginForm.Visible = true;
            panManageAccount.Visible = false;
        }

        private void btnForgotPassword_Click(object sender, EventArgs e) {
            OnRedirect(ForgotPasswordUrl);
        }

        private void btnModifyProfile_Click(object sender, EventArgs e) {
            OnRedirect(ModifyProfileUrl);
        }

        private void btnNewUser_Click(object sender, System.Web.UI.ImageClickEventArgs e){
            OnRedirect(NewUserUrl);
        }

        private void vldLogin_ServerValidate(object source, ServerValidateEventArgs args) {
            Customer customer = new Customer();
            customer.Email = txtEmail.Text;
            customer.Password = txtPassword.Text;
            args.IsValid = OnLoginValidate(customer);
        }
    }
}
