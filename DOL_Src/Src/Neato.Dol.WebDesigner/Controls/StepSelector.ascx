<%@ Control Language="c#" AutoEventWireup="false" Codebehind="StepSelector.ascx.cs" Inherits="Neato.Dol.WebDesigner.Controls.StepSelector" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<table cellspacing=0 cellpadding=0 class="stepMenu" width="100%">
    <tr>
        <td><asp:ImageButton ID="btnStep1" Runat="server" OnClick="btnStep1_Click"/></td>
        <td><asp:HyperLink ID="btnStep2" Runat="server" /></td>
        <td><asp:HyperLink ID="btnStep3" Runat="server" /></td>
    </tr>
</table>