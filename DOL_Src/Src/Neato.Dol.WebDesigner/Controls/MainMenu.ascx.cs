using System;
using System.Web.UI;

namespace Neato.Dol.WebDesigner.Controls {
    public class MainMenu : UserControl {
        protected LinkMenu ctlLinkMenu;
        private object dataSourceValue;
        private string textFieldValue = "Text";
        private string urlFieldValue = "Url";
        private string isNewWindowFieldValue = "IsNewWindow";

        #region Properties
        public object DataSource {
            get { return dataSourceValue; }
            set { dataSourceValue = value; }
        }

        public string TextField {
            get { return textFieldValue; }
            set { textFieldValue = value; }
        }

        public string UrlField {
            get { return urlFieldValue; }
            set { urlFieldValue = value; }
        }

        public string IsNewWindowField {
            get { return isNewWindowFieldValue; }
            set { isNewWindowFieldValue = value; }
        }
        #endregion Properties

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.DataBinding += new EventHandler(HeaderMenu_DataBinding);
        }
        #endregion

        #region Data binding
        private void HeaderMenu_DataBinding(object sender, EventArgs e) {
            ctlLinkMenu.DataSource = dataSourceValue;
            ctlLinkMenu.TextField = textFieldValue;
            ctlLinkMenu.UrlField = urlFieldValue;
            ctlLinkMenu.IsNewWindowField = isNewWindowFieldValue;

        }
        #endregion //Data binding
    }
}