using System;
using System.Collections;
using System.Web;
using System.Web.UI.WebControls;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Controls;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebDesigner {
    public class DiscSelect : BasePage2 {
        protected ImageLibrary ctlPaperSelection;
        protected Label lblNothingFound;
        protected Label lblCategory;
        protected Label lblCategoryName;
        protected Label lblNumberOfItems;
        protected HyperLink hypSwitchCategory;

        private Category Category {
            get { return (Category) ViewState["Category"]; }
            set { ViewState["Category"] = value; }
        }


        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                Category = new Category((int) DeviceType.DirectToCD);
            }

            try {
                int categoryId = int.Parse(Request.QueryString[CategoryKey]);
                Category = new Category(categoryId);
            }
            catch {
                Category = new Category(0);
            }
        }

        #region Url

        private const string RawUrl = "DiscSelect.aspx";
        private const string CategoryKey = "Category";
        public const string DirectToCD = "DirectToCD";
        public const string Lightscribe = "Lightscribe";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri Url(Category category) {
            return UrlHelper.BuildUrl(RawUrl, CategoryKey, category.Id);
        }

        new public static string PageName() {
            return RawUrl;
        }

        #endregion

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            pageTemplateUrl = PaperSelectPageTemplate.Url().AbsolutePath;
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(DiscSelect_DataBinding);
            this.ctlPaperSelection.RedirectToUrl += new ImageLibrary.SelectItemEventHandler(ctlPaperSelection_RedirectToUrl);
            this.PreRender += new EventHandler(DiscSelect_PreRender);
        }

        #endregion

        private void ctlPaperSelection_RedirectToUrl(Object sender, ImageLibrarySelectItemEventArgs e) {
            StorageManager.GenerateNewProject = true;
            Response.Redirect(e.NavigateUrl);
        }

        private void DiscSelect_DataBinding(object sender, EventArgs e) {
            lblCategory.Text = PaperSelectStrings.TextCategory();
            lblCategoryName.Text = HttpUtility.HtmlEncode(BCCategory.GetName(Category));
            bool isLighScribe = lblCategoryName.Text.StartsWith("Light");
            lblNothingFound.Text = PaperSelectStrings.TextNothinFound();
            hypSwitchCategory.NavigateUrl = DefaultPage.Url().PathAndQuery;
            lblNothingFound.Text = PaperSelectStrings.TextNothinFound();
            Paper[] papers = BCPaper.GetPapers(null, Category, null, null, null, null, null, null, null, -1, -1);
            ListSelectItem[] items = GetPapers(papers);
            if (items.Length == 1) {
                ListSelectItem item = items[0];
                StorageManager.GenerateNewProject = true;
                Response.Redirect(item.NavigateUrl);
            }
            lblNumberOfItems.Text = string.Format("<strong>{0}</strong> {1}", papers.Length, PaperSelectStrings.TextItems());
            ctlPaperSelection.DataSource = items;

            headerText = (isLighScribe ?
                (papers.Length <= 3 ?
                    DiscSelectStrings.TextCaptionSelectLSMode() :
                    DiscSelectStrings.TextCaptionSelectDiscLSMode()) :
                DiscSelectStrings.TextCaptionSelectDisc()
                );
            headerHtml = string.Empty;

        }

        private ListSelectItem[] GetPapers(Paper[] papers) {
            ArrayList list = new ArrayList();

            foreach (Paper paper in papers) {
                ListSelectItem item = new ListSelectItem();
                item.Text = HttpUtility.HtmlEncode(paper.Name);
                item.ImageUrl = IconHandler.ImageUrl(IconHandler.PaperIdParamName, paper.Id).PathAndQuery;
                item.NavigateUrl = Designer.Url(paper).PathAndQuery;
                item.SkuNumber = null;
                item.Description = null;
                list.Add(item);
            }

            return (ListSelectItem[]) list.ToArray(typeof (ListSelectItem));
        }

        private void DiscSelect_PreRender(object sender, EventArgs e) {
            ListSelectItem[] papers = (ListSelectItem[]) ctlPaperSelection.DataSource;
            lblNothingFound.Visible = papers == null || papers.Length == 0;
        }
    }
}