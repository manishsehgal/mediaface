using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Tracking;
using Neato.Dol.WebDesigner.Controls;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner {
    public class CustomerAccount : BasePage2 {
        protected Label lblRequired;
        protected Label lblFirstName;
        protected TextBox txtFirstName;
        protected Label lblLastName;
        protected TextBox txtLastName;
        protected Label lblEmailAddress;
        protected TextBox txtEmailAddress;
        protected RequiredFieldValidator vldEmailAddress;
        protected CustomValidator vldValidEmail;
        protected CustomValidator vldUniqueEmail;
        protected Label lblOldPassword;
        protected TextBox txtOldPassword;
        protected RequiredFieldValidator vldOldPassword;
        protected CustomValidator vldWrongOldPassword;
        protected Label lblNewPassword;
        protected TextBox txtNewPassword;
        protected RequiredFieldValidator vldNewPassword;
        protected Label lblConfirmPassword;
        protected TextBox txtConfirmPassword;
        protected RequiredFieldValidator vldConfirmPassword;
        protected CustomValidator vldCompareConfirmPassword;
        protected Label lblUPC;
        protected TextBox txtUPC;
        protected HyperLink hypUpcHowFind;
        protected CustomValidator vldCompareUPC;
        protected Label lblEmailOptions;
        protected DropDownList cboEmailOptions;
        protected CheckBox chkReceiveInfo;
        protected ImageButton btnOK;
        protected ImageButton btnCancel;
        protected ImageButton btnClose;
        protected Label lblUnexpectedError;
        protected HtmlTable tblCustomer;
        protected HtmlTable tblConfirmation;
        protected Label lblConfirmation;
        protected Label lblViewPolicy;
        protected HtmlGenericControl spanRequiredNewPassword;
        protected HtmlGenericControl spanRequiredConfirmPassword;
        protected BannerControl ctlBanner1;
        protected BannerControl ctlBanner2;
        protected BannerControl ctlBanner3;

        private string mode {
            get { return ViewState["mode"].ToString(); }
            set { ViewState["mode"] = value; }
        }

        #region Url
        private const string RawUrl = "CustomerAccount.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri Url(bool newAccount) {
            if (newAccount)
                return UrlHelper.BuildUrl(RawUrl, "mode", "new");
            else
                return Url();
        }
        
        new public static string PageName() {
            return RawUrl;
        }

        public static string BannerFolder {
            get { return "CreateAccount"; }
        }
        
        #endregion
        
        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            mode = Request.QueryString["mode"];
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
//            this.ctlAccessToAccount.Logout += new EventHandler(ctlAccessToAccount_Logout);
//            this.ctlAccessToAccount.Login += new Neato.Dol.WebDesigner.Controls.AccessToAccount.AccessToAccountEventHandler(ctlAccessToAccount_Login);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(this.CustomerAccount_DataBinding);
            this.PreRender += new EventHandler(CustomerAccount_PreRender);
            this.btnOK.Click += new ImageClickEventHandler(btnOK_Click);
            this.btnCancel.Click += new ImageClickEventHandler(btnCancel_Click);
            this.btnClose.Click += new ImageClickEventHandler(btnClose_Click);
            this.vldWrongOldPassword.ServerValidate += new ServerValidateEventHandler(vldWrongOldPassword_ServerValidate);
            this.vldUniqueEmail.ServerValidate += new ServerValidateEventHandler(vldUniqueEmail_ServerValidate);
            this.vldValidEmail.ServerValidate += new ServerValidateEventHandler(vldValidEmail_ServerValidate);
            this.vldCompareConfirmPassword.ServerValidate += new ServerValidateEventHandler(vldCompareConfirmPassword_ServerValidate);
            this.vldCompareUPC.ServerValidate += new ServerValidateEventHandler(vldCompareUPC_ServerValidate);
            ctlBanner1.DataBinding += new EventHandler(BannerDataBinding);
            ctlBanner2.DataBinding += new EventHandler(BannerDataBinding);
            ctlBanner3.DataBinding += new EventHandler(BannerDataBinding);
        }
        #endregion

        #region DataBinding
        private void CustomerAccount_DataBinding(object sender, EventArgs e) {
            rawUrl = RawUrl;
            headerText = CustomerAccountStrings.TextCustomerAccount();
            lblRequired.Text = CustomerAccountStrings.TextRequired();
            lblFirstName.Text = CustomerAccountStrings.TextFirstName();
            lblLastName.Text = CustomerAccountStrings.TextLastName();
            lblEmailAddress.Text = CustomerAccountStrings.TextEmailAddress();
            vldEmailAddress.Text = CustomerAccountStrings.ValidatorEmailAddress();
            vldValidEmail.Text = CustomerAccountStrings.ValidatorInvalidEmailAddress();
            lblOldPassword.Text = CustomerAccountStrings.TextOldPassword();
            vldOldPassword.Text = CustomerAccountStrings.ValidatorOldPassword();
            vldWrongOldPassword.Text = CustomerAccountStrings.ValidatorWrongOldPassword();
            lblNewPassword.Text = CustomerAccountStrings.TextPassword();
            vldNewPassword.Text = CustomerAccountStrings.ValidatorPassword();
            lblConfirmPassword.Text = CustomerAccountStrings.TextConfirmPassword();
            vldConfirmPassword.Text = CustomerAccountStrings.ValidatorConfirmPassword();
            vldCompareConfirmPassword.Text = CustomerAccountStrings.ValidatorCompareConfirmPassword();
            lblUPC.Text = CustomerAccountStrings.TextUPC();
            vldCompareUPC.Text = CustomerAccountStrings.ValidatorCompareUPC();
            lblEmailOptions.Text = CustomerAccountStrings.TextEmailOptions();
            chkReceiveInfo.Text = CustomerAccountStrings.TextReceiveInfo();
            //btnOK.Text = CustomerAccountStrings.TextOK();
            //btnCancel.Text = CustomerAccountStrings.TextCancel();
            vldUniqueEmail.Text = CustomerAccountStrings.ValidatorUniqueEmail();
            lblConfirmation.Text = CustomerAccountStrings.TextCustomerAdded();
            lblViewPolicy.Text = CustomerAccountStrings.TextViewPolicy();
            hypUpcHowFind.Text = CustomerAccountStrings.TextUpcHowFind();
            hypUpcHowFind.Attributes["onClick"] = String.Format(hypUpcHowFind.Attributes["onClick"], BCBanner.GetPopUpSource(BannerFolder, 4));

            if (cboEmailOptions.Items.Count == 0) {
                cboEmailOptions.Items.Add(new ListItem(CustomerAccountStrings.TextEmailOptionText(), EmailOptions.Text.ToString()));
                cboEmailOptions.Items.Add(new ListItem(CustomerAccountStrings.TextEmailOptionHtml(), EmailOptions.Html.ToString()));
            }
            
            cboEmailOptions.EnableViewState = true;
            chkReceiveInfo.EnableViewState = true;

            txtFirstName.Attributes["param"] = btnOK.ClientID;
            txtLastName.Attributes["param"] = btnOK.ClientID;
            txtEmailAddress.Attributes["param"] = btnOK.ClientID;
            txtOldPassword.Attributes["param"] = btnOK.ClientID;
            txtNewPassword.Attributes["param"] = btnOK.ClientID;
            txtConfirmPassword.Attributes["param"] = btnOK.ClientID;
            txtUPC.Attributes["param"] = btnOK.ClientID;
            cboEmailOptions.Attributes["param"] = btnOK.ClientID;
            chkReceiveInfo.Attributes["param"] = btnOK.ClientID;
        }
        #endregion

        private void CustomerAccount_PreRender(object sender, EventArgs e) {
            JavaScriptHelper.RegisterFocusScript(this, txtFirstName.ClientID);
            if (!HttpContext.Current.User.IsInRole(Authentication.RegisteredUser)) {
                foreach (HtmlTableRow row in tblCustomer.Rows) {
                    if (row.ID == "OldPasswordRow")
                        row.Visible = false;
                    if (row.ID == "OldPasswordRowValidator")
                        row.Visible = false;
                }
                return;
            }

            foreach (HtmlTableRow row in tblCustomer.Rows) {
                if (row.ID == "UPCNoteRow")
                    row.Visible = false;
                if (row.ID == "UPCRow")
                    row.Visible = false;
                if (row.ID == "UPCRowValidator")
                    row.Visible = false;
            }
            spanRequiredNewPassword.Visible = 
                spanRequiredConfirmPassword.Visible =
                !HttpContext.Current.User.IsInRole(Authentication.RegisteredUser);

            Customer customer = StorageManager.CurrentCustomer;
            Debug.Assert(customer != null);
            if (customer == null)
                return;

            txtFirstName.Text = customer.FirstName;
            txtLastName.Text = customer.LastName;
            txtEmailAddress.Text = customer.Email;
            cboEmailOptions.SelectedValue = customer.EmailOptions.ToString();
            chkReceiveInfo.Checked = customer.ReceiveInfo;
            vldCompareConfirmPassword.Text = CustomerAccountStrings.ValidatorCompareConfirmNewPassword();
            lblNewPassword.Text = CustomerAccountStrings.TextNewPassword();
            vldNewPassword.Text = CustomerAccountStrings.ValidatorNewPassword();
            lblConfirmation.Text = CustomerAccountStrings.TextCustomerUpdated();
        }

        private void btnOK_Click(object sender, ImageClickEventArgs e) {
            vldEmailAddress.Validate();
            vldValidEmail.Validate();
            vldUniqueEmail.Validate();
            vldCompareConfirmPassword.Validate();

            if (HttpContext.Current.User.IsInRole(Authentication.RegisteredUser)) {
                vldOldPassword.Validate();
                vldWrongOldPassword.Validate();
                if(!(vldOldPassword.IsValid && vldWrongOldPassword.IsValid)) {
                    DataBind();
                    return;
                }
            }
            else {
                vldNewPassword.Validate();
                vldConfirmPassword.Validate();
                vldCompareUPC.Validate();
            }

            if (!(  vldEmailAddress.IsValid
                 && vldValidEmail.IsValid
                 && vldUniqueEmail.IsValid
                 && vldNewPassword.IsValid
                 && vldConfirmPassword.IsValid
                 && vldCompareConfirmPassword.IsValid
                 && vldCompareUPC.IsValid)) {
                DataBind();
                return;
            }

            Customer customer = new Customer();

            customer.FirstName = txtFirstName.Text.Trim();
            customer.LastName = txtLastName.Text.Trim();
            customer.Email = txtEmailAddress.Text.Trim();
			customer.PasswordUid = System.Guid.NewGuid().ToString();
            if(HttpContext.Current.User.IsInRole(Authentication.UnregisteredUser))
                customer.Password = txtConfirmPassword.Text;
            else if(txtConfirmPassword.Text != string.Empty)
                customer.Password = txtConfirmPassword.Text;
            if (cboEmailOptions.SelectedValue == EmailOptions.Html.ToString()) {
                customer.EmailOptions = EmailOptions.Html;
            } else {
                customer.EmailOptions = EmailOptions.Text;
            }

            customer.ReceiveInfo = chkReceiveInfo.Checked;

            try {
                if (mode == "new") {
                    Retailer retailer = BCRetailer.Get(1);//1 is id for Neato
                    customer.RetailerId = retailer.Id;
                    StorageManager.NewCustomer = customer;
                    customer.Active = true;
                    customer.Group = CustomerGroup.MFO;

                    if (txtUPC.Text.Length > 0) {
                        BCTracking.Add(UserAction.Registration,
                            Request.UserHostAddress);
                        BCTrackingUpcCodes.Insert(txtUPC.Text, customer.Email, DateTime.Now);
                        BCCustomer.ActivateCustomer(
                            customer.Email,
                            customer.FirstName,
                            customer.LastName,
                            customer.Password,
                            customer.ReceiveInfo,
                            DefaultPage.Url().AbsoluteUri, retailer, customer.Group);
                        StorageManager.CurrentCustomer =
                            BCCustomer.GetCustomerByLogin(customer.Email, true);
                        Response.Redirect(DefaultPage.Url().PathAndQuery);
                    } else {
                        string neatoDolBuyAccountUrl = 
                            BCParameters.GetParameterByKey("NeatoDolBuyAccountUrl");
                        BCCustomer.InsertCustomer(customer,
                            BCLastEditedPaper.ShowPapersAsDataset(
                            StorageManager.CookieLastEditedPapers));
                        string url = string.Format("{0}?{1}={2}", neatoDolBuyAccountUrl, "Email", StorageManager.NewCustomer.Email);
                        Response.Redirect(url);
                    }
                } else if (mode == "edit") {
                    StorageManager.CurrentCustomer.FirstName = customer.FirstName;
                    StorageManager.CurrentCustomer.LastName = customer.LastName;
                    StorageManager.CurrentCustomer.Email = customer.Email;
                    if(HttpContext.Current.User.IsInRole(Authentication.UnregisteredUser))
                        StorageManager.CurrentCustomer.Password = txtConfirmPassword.Text;
                    else if(txtConfirmPassword.Text != string.Empty)
                        StorageManager.CurrentCustomer.Password = txtConfirmPassword.Text;
                    StorageManager.CurrentCustomer.EmailOptions = customer.EmailOptions;
                    StorageManager.CurrentCustomer.ReceiveInfo = customer.ReceiveInfo;
                    StorageManager.CurrentCustomer.Group = customer.Group;
                    BCCustomer.UpdateCustomer(StorageManager.CurrentCustomer);
                }
                lblUnexpectedError.Visible = false;
                tblCustomer.Visible = false;
                tblConfirmation.Visible = true;
                DataBind();
            } catch (BaseBusinessException ex) {
                lblUnexpectedError.Text = ex.Message;
                lblUnexpectedError.Visible = true;
            }
        }

        private void btnCancel_Click(object sender, ImageClickEventArgs e) {
            Response.Redirect(DefaultPage.Url().PathAndQuery);
        }

        private void vldWrongOldPassword_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = txtOldPassword.Text == StorageManager.CurrentCustomer.Password;
        }

        private void vldUniqueEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            Customer customer = BCCustomer.GetCustomerByLogin(txtEmailAddress.Text.Trim(), false);
            if (HttpContext.Current.User.IsInRole(Authentication.RegisteredUser)) {
                args.IsValid = customer == null || customer.CustomerId == StorageManager.CurrentCustomer.CustomerId;
            } else {
                args.IsValid = customer == null;
            }
        }

        private void vldValidEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = FormatValidator.EnsureValidEmail(txtEmailAddress.Text.Trim());
        }

        private void btnClose_Click(object sender, ImageClickEventArgs e) {
            Response.Redirect(DefaultPage.Url().PathAndQuery);
        }

//        private void ctlAccessToAccount_Logout(object sender, EventArgs e) {
//            Response.Redirect(DefaultPage.Url().PathAndQuery);
//        }
//
//        private void ctlAccessToAccount_Login(Object sender, Neato.Dol.WebDesigner.Controls.AccessToAccountEventArgs e) {
//            Response.Redirect(DefaultPage.Url().PathAndQuery);
//        }

        private void vldCompareConfirmPassword_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = txtNewPassword.Text == txtConfirmPassword.Text;
            if(!args.IsValid) {
                if(txtNewPassword.Text.Length == 0) {
                    vldNewPassword.IsValid = false;
                    args.IsValid = true;
                }
                if(txtConfirmPassword.Text.Length == 0) {
                    vldConfirmPassword.IsValid = false;
                    args.IsValid = true;
                }
            }
        }

        private void vldCompareUPC_ServerValidate(object source, ServerValidateEventArgs args) {
            if (txtUPC.Text.Length == 0)
                args.IsValid = true;
            else
                args.IsValid = BCUpc.Validate(txtUPC.Text);
        }
 
        private void BannerDataBinding(object sender, EventArgs e) {
            BannerControl banner = (BannerControl) sender;
            int bannerId = banner.BannerId;
            banner.Banner = BCBanner.GetBanner(BannerFolder, bannerId);
            int height = BCBanner.GetBannerHeight(BannerFolder, bannerId);
            if (height != -1)
                banner.Height = height;
        }
    }
}