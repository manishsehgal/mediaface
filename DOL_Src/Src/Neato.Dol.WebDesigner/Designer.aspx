<%@ Page language="c#" Codebehind="Designer.aspx.cs" AutoEventWireup="false" Inherits="Neato.Dol.WebDesigner.Designer" %>
<%@ Register TagPrefix="dol" NameSpace="Neato.Dol.WebDesigner.Controls" Assembly="Neato.Dol.WebDesigner" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD>
    <title>MediaFace Online</title>
    <meta name="keywords" content="Design custom CD/DVD Labels and Inserts, iPod Wraps and more using NEATO�s MediaFACEonline software.  Supports NEATO, Avery, CD Stomper and Memorex labels and inserts along with Epson direct to CD/DVD printing" />
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" language="javascript" src="js/DetectFlashVersion.js"></script>
    <script type="text/javascript" language="javascript" src="js/Designer.js"></script>
    <script type="text/javascript" language="javascript" src="js/Cookies.js"></script>
    <script type="text/javascript" language="javascript">
        // Flash fscommand: Hook for Internet Explorer.
        if (navigator.appName && navigator.appName.indexOf("Microsoft") != -1 && navigator.userAgent.indexOf("Windows") != -1 && navigator.userAgent.indexOf("Windows 3.1") == -1) {
	        document.write("<script type=\"text/vbscript\" >\n");
	        document.write("On Error Resume Next\n");
	        document.write("Sub Designer_FSCommand(ByVal command, ByVal args)\n");
	        document.write("	Call Designer_DoFSCommand(command, args)\n");
	        document.write("End Sub\n");
	        document.write("</script\>\n");
        }
        var Designer = document.getElementById('Designer');
        function Designer_DoFSCommand(command, args) {
            DoCommand(command, args);
        }
        function DoCommand(command, args) { 
            switch (command) {
	            case "SetDuplicatedFlash":
	            SetDuplicatedFlash(args);
	            break;
	            case "print":
	            OpenPdf();
	            break;
	            case "home":
	            GoHome();
	            break;
	            case "refresh":
	            RefreshDesigner();
	            break;
	            case "calibration":
	            SetCookie("Calibration", args);
	            break;
	            case "calibrationDirectToCD":
	            SetCookie("CalibrationDirectToCD", args);
	            break;
	            case "ShowExitPage":
	            ShowExitPage();
	            break;
	            case "RefreshHeader":
	            RefreshHeader();
	            break;
	            case "save":
	            SaveToClient();
	            break;
	            case "ChkPSPort":
	            ChkPSPort(args);
	            break;
	            default:
	            eval(command+"("+args+")");
	        }
	        return "";
        }
        Designer = document.getElementById('Designer');
    </script>
    </HEAD>
    <body MS_POSITIONING="GridLayout">
    <form id="form1" method="post" runat="server">
      <div style="height:540px;">
        <dol:PopUpBlockerNotifier id="ctlNotifier" runat="server" />
        <input type="hidden" id="ctlDuplicatedFlash" runat="server"/>
        <input type="hidden" id="ctlHomeUrl" runat="server" />
        <input type="hidden" id="ctlLeaveDesignerMessage" runat="server" />
        <input type="hidden" id="ctlDuplicatedDesignerMessage" runat="server" />
        <input type="hidden" id="ctlShowExitPage" runat="server" />
        <input type="hidden" id="ctlDownloadProjectUrl" runat="server" />
        <div id="flashRequiredDiv" style="text-align: center; display: none; color: #000000">
            <asp:Label id="lblFlashRequired" Runat="server"/>
        </div>
        
        <div id="flashClipDiv">
            
            <OBJECT id="Designer"   
                codeBase="http://fpdownload.adobe.com/pub/shockwave/cabs/flash/swflash.cab"
                height="100%" width="100%" align="center" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
                VIEWASTEXT>
                <PARAM NAME="Movie" VALUE="Flash/Starter.swf">
                <PARAM NAME="Play" VALUE="false">
                <PARAM NAME="Loop" VALUE="false">
                <PARAM NAME="Quality" VALUE="High">
                <PARAM NAME="Menu" VALUE="false">
                <PARAM NAME="AllowScriptAccess" VALUE="sameDomain">
                <PARAM NAME="Scale" VALUE="noscale">
                <PARAM NAME="BGColor" VALUE="#ffffff">
                <PARAM NAME="SeamlessTabbing" VALUE="true">
                <PARAM NAME="FlashVars" VALUE="localwork=false">
                <PARAM NAME="salign" VALUE="lt">
                <embed name="Designer" id="DesignerEmbed" src="Flash/Starter.swf"
                    quality="high" bgcolor="#ffffff" width="100%" height="100%"
                    swLiveConnect="true" align="middle" allowScriptAccess="sameDomain"
                    FlashVars="localwork=false" scale="noscale" salign="lt"
                    menu="false" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer" />
            </OBJECT>
            <script type="text/javascript" language="javascript">
                Designer = document.getElementById('Designer');
            </script>
        </div>
      </div>
    </form>
    <script type="text/javascript" language="javascript">
        Designer = document.getElementById('Designer');
        ScrolToDesigner();
        CheckFlashVer();
    </script>
</body>
</HTML>
