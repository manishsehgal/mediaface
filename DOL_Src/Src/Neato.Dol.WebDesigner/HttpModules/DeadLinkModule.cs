using System;
using System.Text.RegularExpressions;
using System.Web;
using Neato.Dol.Business;
using Neato.Dol.Entity;

namespace Neato.Dol.WebDesigner.HttpModules {
    public class DeadLinkModule : IHttpModule {
        public DeadLinkModule() : base() {}

        #region IHttpModule members

        public void Init(HttpApplication context) {
            context.BeginRequest += new EventHandler(context_AcquireRequestState);
        }

        public void Dispose() {}

        #endregion IHttpModule members

        private void context_AcquireRequestState(object sender, EventArgs e) {
            HttpRequest request = HttpContext.Current.Request;
            HttpResponse response = HttpContext.Current.Response;
            string referer = request.UrlReferrer == null ? string.Empty : request.UrlReferrer.AbsoluteUri;
            if (BCDeadLink.CheckDeadLink(request.Url.AbsoluteUri, request.UserHostAddress, referer))
                response.Redirect(DefaultPage.Url().PathAndQuery);
        }
    }
}