using System.Collections;
using System.Security.Principal;
using System.Threading;
using System.Web;

using Neato.Dol.Entity;

namespace Neato.Dol.WebDesigner.HttpModules
{
	public class AuthenticationModule : IHttpModule {
        public AuthenticationModule() : base() {}

        #region IHttpModule members
        public void Init(HttpApplication context) {
            context.AcquireRequestState += new System.EventHandler(context_AcquireRequestState); 
        }
        public void Dispose() {}
        #endregion IHttpModule members

        private void context_AcquireRequestState(object sender, System.EventArgs e) {
            ArrayList list = new ArrayList();
            string name;
            Customer customer = StorageManager.CurrentCustomer;
            if (customer != null) {
                list.Add(Authentication.RegisteredUser);
                list.Add(customer.Group.ToString());
                name = customer.Email;
            } else {
                list.Add(Authentication.UnregisteredUser);
                list.Add(CustomerGroup.Trial.ToString());
                name = Authentication.GuestLogin;
            }

            string[] roles = (string[])list.ToArray(typeof(string));
            GenericIdentity identity = new GenericIdentity(name);
            Thread.CurrentPrincipal = new GenericPrincipal(identity, roles);
            HttpContext.Current.User = Thread.CurrentPrincipal;
        }
    }
}
