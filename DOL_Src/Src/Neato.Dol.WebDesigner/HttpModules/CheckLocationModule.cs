using System;
using System.Globalization;
using System.Web;
using System.IO;

using Neato.Dol.WebDesigner.HttpHandlers;
using Neato.Dol.Entity;

namespace Neato.Dol.WebDesigner.HttpModules {
    public class CheckLocationModule : IHttpModule {
        #region Url
        public const string ModeKey = "mode";
        public const string AdminMode = "Admin";

        public static string AdminUrl(string url) {
            string adminUrl;
            if (url.IndexOf("?") > 0)
                adminUrl = string.Format("{0}&{1}={2}", url, ModeKey, AdminMode);
            else
                adminUrl = string.Format("{0}?{1}={2}", url, ModeKey, AdminMode);
            return adminUrl;
        }
        #endregion

        public CheckLocationModule() : base() {}

        #region IHttpModule members
        public void Init(HttpApplication context) {
            context.AcquireRequestState += new EventHandler(context_AcquireRequestState);

        }

        public void Dispose() {}
        #endregion IHttpModule members

        private void context_AcquireRequestState(object sender, EventArgs e) {
            HttpRequest request = HttpContext.Current.Request;

            bool isHomePageVisited = HttpContext.Current.Session != null && HttpContext.Current.Session["IsWasAtHomePage"] != null;
            
            if(HttpContext.Current.Request.QueryString[ModeKey] == AdminMode)
                HttpContext.Current.Session["IsAdminMode"] = bool.TrueString;

            if (!IsAdminMode && !isHomePageVisited) {
                Uri[] nonCheckedUrl = {
                    DefaultPage.Url(), 
                    TellAFriendRefuseHandler.Url(),
                    IconHandler.Url(),
                    PaperHandler.Url(),
                    PaperHandler.Url(),
                    ForgotPassword.Url(),
                    WatermarkHandler.Url(),
                    WatermarkHandler.Url(true),
                    DeviceInfo.Url(),
                    ActivateAccountHandler.Url(),
                    ProlongSessionHandler.Url(),
                    Upc.Url(),
                    Upc.DirectoryUrl()
                };
                bool gotoHomePage = true;
                foreach (Uri url in nonCheckedUrl) {
                    if (request.Url.AbsolutePath.ToLower(CultureInfo.InvariantCulture) == url.AbsolutePath.ToLower(CultureInfo.InvariantCulture)) {
                        gotoHomePage = false;
                        break;
                    }
                }
                if (gotoHomePage) {
                    HttpContext.Current.Response.Redirect(DefaultPage.Url().PathAndQuery, true);
                }
            }
            if (request.CurrentExecutionFilePath != DeviceSelect.Url().PathAndQuery
                && request.CurrentExecutionFilePath != IconHandler.Url().PathAndQuery) {
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["FaceSelection"] != null) {
                    HttpContext.Current.Session["FaceSelection"] = null;
                }
            }

            if(!IsHttpHandler
                && (HttpContext.Current.Session.IsNewSession || StorageManager.IsSiteClosed)
                && StorageManager.IsSiteClosed
                && !IsAdminMode
                && !IsDesignerPage) {
                string closePageUrl = Path.Combine(HttpContext.Current.Request.ApplicationPath, Constants.MaintenanceClosingDirectory);
                closePageUrl = Path.Combine(closePageUrl, Constants.ClosePageFileName);
                
                HttpContext.Current.Session.Abandon();
                HttpContext.Current.Response.Redirect(closePageUrl);
            }
            return;
        }

        public static bool IsAdminMode {
            get {
                bool isAdminMode = false;
                try {
                    isAdminMode = bool.Parse((string)HttpContext.Current.Session["IsAdminMode"]);
                } catch {
                    isAdminMode = false;
                }
                
                return (HttpContext.Current.Request.QueryString[ModeKey] == AdminMode) || isAdminMode;
            }
        }

        public static bool IsDesignerPage {
            get {
                string uploadBlankUrl = Neato.Dol.WebDesigner.Helpers.UrlHelper.BuildUrl("UploadBlank.aspx").PathAndQuery;
                return (HttpContext.Current.Handler is Print
                      || HttpContext.Current.Request.CurrentExecutionFilePath.ToLower() == uploadBlankUrl.ToLower()); }
        }
        
        public static bool IsHttpHandler {
            get {
 
                return (HttpContext.Current.Handler is ActivateAccountHandler
                       || HttpContext.Current.Handler is BannerHandler
                       || HttpContext.Current.Handler is CalibrationHandler
                       || HttpContext.Current.Handler is DynamicLinkHandler
                       || HttpContext.Current.Handler is ExitPageHandler
                       || HttpContext.Current.Handler is FaceLayoutHandler
                       || HttpContext.Current.Handler is FeatureTrackingHandler
                       || HttpContext.Current.Handler is FontFileHandler
                       || HttpContext.Current.Handler is IconHandler
                       || HttpContext.Current.Handler is ImageLibraryHandler
                       || HttpContext.Current.Handler is MaintenanceClosingHandler
                       || HttpContext.Current.Handler is PaperHandler
                       || HttpContext.Current.Handler is PluginFileHandler
                       || HttpContext.Current.Handler is ProjectFileHandler
                       || HttpContext.Current.Handler is ProjectHandler
                       || HttpContext.Current.Handler is ProjectImagesHandler
                       || HttpContext.Current.Handler is ProlongSessionHandler
                       || HttpContext.Current.Handler is SessionCountHandler
                       || HttpContext.Current.Handler is SettingsHandler
                       || HttpContext.Current.Handler is TellAFriendRefuseHandler
                       || HttpContext.Current.Handler is TemplateHandler
                       || HttpContext.Current.Handler is PaperTrackingHandler 
                       || HttpContext.Current.Handler is WatermarkHandler
                    );
            }
        }
    }

}