<%@ Register TagPrefix="dol" TagName="Header2" Src="Controls/Header2.ascx" %>
<%@ Register TagPrefix="dol" TagName="Footer" Src="Controls/Footer.ascx" %>
<%@ Register TagPrefix="dol" TagName="FooterMenu" Src="Controls/FooterMenu.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BasePageTemplate.ascx.cs" Inherits="Neato.Dol.WebDesigner.BasePageTemplate" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<link href="css/style2.css" type="text/css" rel="stylesheet">
<div align="left">
	<table>
		<tr>
			<td>
				<dol:Header2 id="ctlHeader" runat="server"/>
			</td>
		</tr>
		<tr>
			<td class="Content">
				<asp:PlaceHolder ID="dynamicContent" Runat="server" />
			</td>
		</tr>
		<tr>
			<td>
				<dol:Footer id="ctlFooter" runat="server" />
			</td>
		</tr>
	</table>
</div>
