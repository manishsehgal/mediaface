using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebDesigner
{
	/// <summary>
	/// Shows info and image about device
	/// </summary>
	public class DeviceInfo : Page
	{
		protected Image imgDevice;
		protected Label lblDevice;
		protected Literal pageTitle;
		protected HyperLink hypClose;

        #region Url
        private const string RawUrl = "DeviceInfo.aspx";

        public static Uri Url() 
        {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
			imgDevice=(Image)FindControl("imgDevice");
			lblDevice=(Label)FindControl("lblDevice");
			pageTitle=(Literal)FindControl("pageTitle");
			hypClose=(HyperLink)FindControl("hypClose");
			hypClose.Attributes.Add("onClick","javascript:window.close();");
			hypClose.NavigateUrl="#";
			hypClose.Text=DeviceInfoStrings.TextClose();
			if(Request.QueryString[IconHandler.DeviceIdParamName]!=null)
			{
				Device device=DODevice.GetDevice(new DeviceBase(Int32.Parse(Request.QueryString[IconHandler.DeviceIdParamName])));
				imgDevice.ImageUrl=IconHandler.ImageUrl(IconHandler.DeviceIdParamNameBigPic, device.Id).PathAndQuery;
				lblDevice.Text=device.FullModel;
				pageTitle.Text=device.FullModel;
				
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
