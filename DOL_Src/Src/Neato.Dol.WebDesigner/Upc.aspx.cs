using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Tracking;
using Neato.Dol.WebDesigner.Controls;
using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner {
    public class Upc : BasePage2 {
        protected Label lblRequired;
        protected Label lblFirstName;
        protected TextBox txtFirstName;
        protected Label lblLastName;
        protected TextBox txtLastName;
        protected Label lblEmailAddress;
        protected TextBox txtEmailAddress;
        protected RequiredFieldValidator vldEmailAddress;
        protected CustomValidator vldValidEmail;
        protected CustomValidator vldUniqueEmail;
        protected Label lblNewPassword;
        protected TextBox txtNewPassword;
        protected RequiredFieldValidator vldNewPassword;
        protected Label lblConfirmPassword;
        protected TextBox txtConfirmPassword;
        protected RequiredFieldValidator vldConfirmPassword;
        protected CustomValidator vldCompareConfirmPassword;
        protected Label lblEmailOptions;
        protected DropDownList cboEmailOptions;
        protected CheckBox chkReceiveInfo;
        protected ImageButton btnOK;
        protected ImageButton btnCancel;
        protected Label lblUnexpectedError;
        protected Label lblViewPolicy;
        protected HtmlTable tblCustomer;
        protected HtmlGenericControl spanRequiredNewPassword;
        protected HtmlGenericControl spanRequiredConfirmPassword;

        protected Label lblUpc;
        protected TextBox txtUpc;
        protected HyperLink hypUpcHowFind;
        protected RequiredFieldValidator vldUpcRequired;
        protected CustomValidator vldUpcExist;

        protected BannerControl ctlBanner1;

        public static string BannerFolder {
            get { return "Upc"; }
        }

        #region Url

        private const string RawUrl = "Upc.aspx";
        private const string RawDirectoryUrl = "upc";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri DirectoryUrl() {
            return UrlHelper.BuildUrl(RawDirectoryUrl);
        }

        public static Uri Url(bool newAccount) {
            if (newAccount)
                return UrlHelper.BuildUrl(RawUrl, "mode", "new");
            else
                return Url();
        }

        new public static string PageName() {
            return RawUrl;
        }

        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
            //            this.ctlAccessToAccount.Logout += new EventHandler(ctlAccessToAccount_Logout);
            //            this.ctlAccessToAccount.Login += new Neato.Dol.WebDesigner.Controls.AccessToAccount.AccessToAccountEventHandler(ctlAccessToAccount_Login);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(this.Upc_DataBinding);
            this.btnOK.Click += new ImageClickEventHandler(btnOK_Click);
            this.btnCancel.Click += new ImageClickEventHandler(btnCancel_Click);
            this.vldUniqueEmail.ServerValidate += new ServerValidateEventHandler(vldUniqueEmail_ServerValidate);
            this.vldValidEmail.ServerValidate += new ServerValidateEventHandler(vldValidEmail_ServerValidate);
            this.vldCompareConfirmPassword.ServerValidate +=
                new ServerValidateEventHandler(vldCompareConfirmPassword_ServerValidate);
            this.vldUpcExist.ServerValidate += new ServerValidateEventHandler(vldUpcExist_ServerValidate);
            this.ctlBanner1.DataBinding += new EventHandler(BannerDataBinding);
        }

        #endregion

        #region DataBinding

        private void Upc_DataBinding(object sender, EventArgs e) {
            if (IsPostBack)
                return;

            rawUrl = RawUrl;
            lblUpc.Text = UpcStrings.TextUpc();
            vldUpcRequired.ErrorMessage = UpcStrings.TextVldUpcExist();
            vldUpcExist.ErrorMessage = UpcStrings.TextVldUpcExist();
            headerText = CustomerAccountStrings.TextCustomerAccount();
            lblRequired.Text = CustomerAccountStrings.TextRequired();
            lblFirstName.Text = CustomerAccountStrings.TextFirstName();
            lblLastName.Text = CustomerAccountStrings.TextLastName();
            lblEmailAddress.Text = CustomerAccountStrings.TextEmailAddress();
            vldEmailAddress.Text = CustomerAccountStrings.ValidatorEmailAddress();
            vldValidEmail.Text = CustomerAccountStrings.ValidatorInvalidEmailAddress();
            lblNewPassword.Text = CustomerAccountStrings.TextPassword();
            vldNewPassword.Text = CustomerAccountStrings.ValidatorPassword();
            lblConfirmPassword.Text = CustomerAccountStrings.TextConfirmPassword();
            vldConfirmPassword.Text = CustomerAccountStrings.ValidatorConfirmPassword();
            vldCompareConfirmPassword.Text = CustomerAccountStrings.ValidatorCompareConfirmPassword();

            lblEmailOptions.Text = CustomerAccountStrings.TextEmailOptions();
            chkReceiveInfo.Text = CustomerAccountStrings.TextReceiveInfo();
            lblViewPolicy.Text = CustomerAccountStrings.TextViewPolicy();
            hypUpcHowFind.Text = CustomerAccountStrings.TextUpcHowFind();
            hypUpcHowFind.Attributes["onClick"] = String.Format(hypUpcHowFind.Attributes["onClick"], BCBanner.GetPopUpSource(BannerFolder, 2));
            
            if (cboEmailOptions.Items.Count == 0) {
                cboEmailOptions.Items.Add(
                    new ListItem(CustomerAccountStrings.TextEmailOptionText(), EmailOptions.Text.ToString()));
                cboEmailOptions.Items.Add(
                    new ListItem(CustomerAccountStrings.TextEmailOptionHtml(), EmailOptions.Html.ToString()));
            }

            cboEmailOptions.EnableViewState = true;
            chkReceiveInfo.EnableViewState = true;

            txtFirstName.Attributes["param"] = btnOK.ClientID;
            txtLastName.Attributes["param"] = btnOK.ClientID;
            txtEmailAddress.Attributes["param"] = btnOK.ClientID;
            txtNewPassword.Attributes["param"] = btnOK.ClientID;
            txtConfirmPassword.Attributes["param"] = btnOK.ClientID;
            cboEmailOptions.Attributes["param"] = btnOK.ClientID;
            chkReceiveInfo.Attributes["param"] = btnOK.ClientID;

            Customer customer = StorageManager.CurrentCustomer;
            if (customer == null)
                return;

            txtFirstName.Text = customer.FirstName;
            txtLastName.Text = customer.LastName;
            txtEmailAddress.Text = customer.Email;
            cboEmailOptions.SelectedValue = customer.EmailOptions.ToString();
            chkReceiveInfo.Checked = customer.ReceiveInfo;
        }

        #endregion

        private void btnOK_Click(object sender, ImageClickEventArgs e) {
            vldEmailAddress.Validate();
            vldValidEmail.Validate();
            vldUniqueEmail.Validate();
            vldCompareConfirmPassword.Validate();

            vldNewPassword.Validate();
            vldConfirmPassword.Validate();
            vldUpcRequired.Validate();
            vldUpcExist.Validate();

            if (!(vldEmailAddress.IsValid
                  && vldValidEmail.IsValid
                  && vldUniqueEmail.IsValid
                  && vldNewPassword.IsValid
                  && vldConfirmPassword.IsValid
                  && vldCompareConfirmPassword.IsValid
                  && vldUpcRequired.IsValid
                  && vldUpcExist.IsValid)) {
                DataBind();
                return;
            }

            Customer customer = new Customer();

            customer.FirstName = txtFirstName.Text.Trim();
            customer.LastName = txtLastName.Text.Trim();
            customer.Email = txtEmailAddress.Text.Trim();
            if (HttpContext.Current.User.IsInRole(Authentication.UnregisteredUser))
                customer.Password = txtConfirmPassword.Text;
            else if (txtConfirmPassword.Text != string.Empty)
                customer.Password = txtConfirmPassword.Text;
            if (cboEmailOptions.SelectedValue == EmailOptions.Html.ToString()) {
                customer.EmailOptions = EmailOptions.Html;
            }
            else {
                customer.EmailOptions = EmailOptions.Text;
            }

            customer.ReceiveInfo = chkReceiveInfo.Checked;

            try {
                customer.RetailerId = 1;
                StorageManager.NewCustomer = customer;
                customer.Active = false;
                customer.Group = CustomerGroup.MFOPE;

                Retailer retailer = BCRetailer.Get(1);//1 is id for Neato
                if (StorageManager.NewCustomer == null) return;
                StorageManager.NewCustomer.RetailerId = retailer.Id;
                
                BCTracking.Add(UserAction.Registration,
                    Request.UserHostAddress);
                BCTrackingUpcStapleCodes.Insert(txtUpc.Text, customer.Email, DateTime.Now);

                BCCustomer.ActivateCustomer(
                    customer.Email,
                    customer.FirstName,
                    customer.LastName,
                    customer.Password,
                    customer.ReceiveInfo,
                    DefaultPage.Url().AbsoluteUri, retailer, customer.Group);

                StorageManager.CurrentCustomer =
                    BCCustomer.GetCustomerByLogin(customer.Email, true);
                Response.Redirect(DefaultPage.Url().PathAndQuery);
            }
            catch (BaseBusinessException ex) {
                lblUnexpectedError.Text = ex.Message;
                lblUnexpectedError.Visible = true;
            }
            DataBind();
        }

        private void btnCancel_Click(object sender, ImageClickEventArgs e) {
            Response.Redirect(DefaultPage.Url().PathAndQuery);
        }

        private void vldUniqueEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            Customer customer = BCCustomer.GetCustomerByLogin(txtEmailAddress.Text.Trim(), false);
            if (customer == null) {
                args.IsValid = true;
            } else {
                args.IsValid = customer.Group != CustomerGroup.MFOPE;
            }
        }

        private void vldValidEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = FormatValidator.EnsureValidEmail(txtEmailAddress.Text.Trim());
        }

        private void vldCompareConfirmPassword_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = txtNewPassword.Text == txtConfirmPassword.Text;
            if (!args.IsValid) {
                if (txtNewPassword.Text.Length == 0) {
                    vldNewPassword.IsValid = false;
                    args.IsValid = true;
                }
                if (txtConfirmPassword.Text.Length == 0) {
                    vldConfirmPassword.IsValid = false;
                    args.IsValid = true;
                }
            }
        }

        private void vldUpcExist_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = BCUpcStaple.Validate(txtUpc.Text);
        }
    
        private void BannerDataBinding(object sender, EventArgs e) {
            BannerControl banner = (BannerControl)sender;
            int bannerId = banner.BannerId;
            banner.Banner = BCBanner.GetBanner(BannerFolder, bannerId);
            int height = BCBanner.GetBannerHeight(BannerFolder, bannerId);
            if (height != -1)
                banner.Height = height;
        }
    }
}