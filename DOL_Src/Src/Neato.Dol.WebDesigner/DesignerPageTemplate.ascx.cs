using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Dol.WebDesigner.Helpers;

namespace Neato.Dol.WebDesigner {
    public class DesignerPageTemplate : UserControl {
        protected PlaceHolder dynamicContent;

        #region Url
        private const string RawUrl = "DesignerPageTemplate.ascx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {}
        #endregion
    }
}