using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Dol.Business;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.WebDesigner.Controls;
using Neato.Dol.WebDesigner.Helpers;
using Neato.Dol.WebDesigner.HttpHandlers;

namespace Neato.Dol.WebDesigner {
    public class TellAFriend : StandardPage {
        //protected Label lblTellAFriend;
        protected Label lblRequired;
        protected Label lblFriendName;
        protected TextBox txtFriendName;
        protected Label lblFriendEmail;
        protected TextBox txtFriendEmail;
        protected Label lblYourName;
        protected TextBox txtYourName;
        protected Label lblYourEmail;
        protected TextBox txtYourEmail;
        protected TextBox txtEmailText;
        protected Label lblMessage;
        protected CheckBox chkSendCopy;
        protected ImageButton btnOK;
        protected ImageButton btnClose;
        protected Label lblError;
        protected Label lblSendMail;
        protected RequiredFieldValidator vldFriendName;
        protected RequiredFieldValidator vldEmailFriendAddress;
        protected CustomValidator vldValidFriendEmail;
        protected RequiredFieldValidator vldName;
        protected RequiredFieldValidator vldEmailAddress;
        protected CustomValidator vldValidEmail;
/*
        #region Url
        private const string RawUrl = "TellAFriend.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion
*/
        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(TellAFriend_DataBinding);
            this.btnOK.Click += new ImageClickEventHandler(btnOK_Click);
            this.btnClose.Click += new ImageClickEventHandler(btnClose_Click);
            this.vldValidEmail.ServerValidate += new ServerValidateEventHandler(vldValidEmail_ServerValidate);
            this.vldValidFriendEmail.ServerValidate += new ServerValidateEventHandler(vldValidEmail_ServerValidate);
        }
        #endregion

        #region DataBinding
        private void TellAFriend_DataBinding(object sender, EventArgs e) {
            //lblTellAFriend.Text = TellAFriendStrings.TextTellAFriend();
            lblRequired.Text = TellAFriendStrings.TextRequired();
            lblFriendName.Text = TellAFriendStrings.TextFriendName();
            lblFriendEmail.Text = TellAFriendStrings.TextFriendEmail();
            lblYourName.Text = TellAFriendStrings.TextYourName();
            lblYourEmail.Text = TellAFriendStrings.TextYourEmail();
            lblMessage.Text = TellAFriendStrings.TextMessage();
            chkSendCopy.Text = TellAFriendStrings.TextSendCopy();
            //btnOK.Text = TellAFriendStrings.TextOK();
            //btnClose.Text = TellAFriendStrings.TextClose();
            lblSendMail.Text = TellAFriendStrings.TextSendMail();
            vldFriendName.Text = TellAFriendStrings.ValidatorFriendName();
            vldEmailFriendAddress.Text = TellAFriendStrings.ValidatorFriendEmailAddress();
            vldValidFriendEmail.Text = TellAFriendStrings.ValidatorInvalidEmailAddress();
            vldName.Text = TellAFriendStrings.ValidatorName();
            vldEmailAddress.Text = TellAFriendStrings.ValidatorEmailAddress();
            vldValidEmail.Text = TellAFriendStrings.ValidatorInvalidEmailAddress();

            if (HttpContext.Current.User.IsInRole(Authentication.RegisteredUser)) {
                txtYourName.Text = StorageManager.CurrentCustomer.FullName;
                txtYourEmail.Text = StorageManager.CurrentCustomer.Email;
            }

            //rawUrl = RawUrl;
            headerText = TellAFriendStrings.TextTellAFriend();
        }
        #endregion

        private void btnOK_Click(object sender, ImageClickEventArgs e) {
            lblSendMail.Visible = false;
            vldEmailAddress.Validate();
            vldEmailFriendAddress.Validate();
            vldFriendName.Validate();
            vldName.Validate();
            vldValidEmail.Validate();
            vldValidFriendEmail.Validate();
            if (!(vldEmailAddress.IsValid
                && vldEmailFriendAddress.IsValid
                && vldFriendName.IsValid
                && vldName.IsValid
                && vldValidEmail.IsValid
                && vldValidFriendEmail.IsValid))
                return;

            try {
				if(txtEmailText.Text.Length>1000)
					txtEmailText.Text=txtEmailText.Text.Remove(1000,txtEmailText.Text.Length-1000);
                BCMail.SendTellAFriendMail(
                    txtFriendName.Text.Trim(), txtFriendEmail.Text.Trim(),
                    txtYourName.Text.Trim(), txtYourEmail.Text.Trim(),
                    txtEmailText.Text,
                    DefaultPage.Url().AbsoluteUri,
                    TellAFriendRefuseHandler.TellAFriendRefuseEmail(
                    string.Empty,
                    txtFriendEmail.Text.Trim()),
                    chkSendCopy.Checked,
                    StorageManager.CurrentLanguage);

                lblSendMail.Visible = true;
                txtFriendEmail.Text = string.Empty;
            } catch (BusinessException ex) {
                lblError.Text = ex.Message;
                lblError.Visible = true;
            }

        }

        private void btnClose_Click(object sender, ImageClickEventArgs e) {
            Response.Redirect(DefaultPage.Url().PathAndQuery);
        }

        private void vldValidEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = FormatValidator.EnsureValidEmail(args.Value.Trim());
        }
    }
}