use swf2xml;

if (@ARGV==0) {
    print STDERR <<USAGE;
swf2xml.pl - Convert SWF file with face into XML.
  perl swf2xml.pl swfname [saveto]

USAGE

    exit(1);
} 

swf2xml($ARGV[0],$ARGV[1]);

