#!/usr/bin/perl

use strict;

use SWF::Parser;
use SWF::Element;
use SWF::BinStream;
use SWF::BinStream::File;

my $scale = 2;

my $coe=0.05/$scale;
my $align="\t";
my $defaultPaperWidth  = 612;
my $defaultPaperHeight = 396;


my $X;
my $Y;
my $minX;
my $maxX;
my $minY;
my $maxY;
my $paperL;
my $paperR;
my $paperU;
my $paperB;
my $faceBL;
my $faceBR;
my $faceBU;
my $faceBB;
my $skip;
my $count;
my $findPaperBounds;
my $generate;
my $isVLine;
my $isHLine;
my $nFaces;

$::face_width;
$::face_height;


#my $frame = 0;


sub swf2xml {

	my ($swfFile, $xmlFile, $nFaceCount) = @_;

	$nFaces = defined $nFaceCount ? $nFaceCount : 1; 

	if (defined $xmlFile) {
   	open(F, ">$xmlFile") or die "Can't open $xmlFile";
		select F;
	}

	print "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n";
	print "<Project>\n";

	print "<Faces>\n";

	swf2xml0($swfFile, $xmlFile);


	print "</Faces>\n";
	zprintf ( "<Paper w=\"%.2f\" h=\"%.2f\"/>\n", ($paperR-$paperL)*$coe, ($paperB-$paperU)*$coe );
	print "</Project>\n";

   close F;

	my $FName = substr($swfFile,0,length($swfFile)-4) ;
	open(F, ">chk_$FName.bat");
	select F;

	zprintf ( "JoinPDF diff_%%2 %%2 x=%.2f y=%.2f %%1\n", $paperL*$coe, $paperU*$coe ) ;

	close F;

}

sub swf2xml0 {

	my ($swfFile, $xmlFile) = @_;

	$paperL=10000000;
	$paperR=-10000000;
	$paperU=10000000;
	$paperB=-10000000;

	$skip=-1;
	$count = 0;

	my $p=SWF::Parser->new('header-callback' => \&header, 'tag-callback' => \&data);

	$generate=0;
	$findPaperBounds=1;
	$p->parse_file($swfFile);

	if($paperL > 100000) { $paperL = 0 }
	if($paperU > 100000) { $paperU = 0 }
	if($paperR <= $paperL ) { $paperR = $paperL + $defaultPaperWidth/$coe }
	if($paperB <= $paperU ) { $paperB = $paperU + $defaultPaperHeight/$coe }

for(my $nFaceId = 0; $nFaceId<$nFaces ; $nFaceId++) {

	$faceBL=$paperL+3/$coe;
	$faceBR=$paperR-3/$coe;
	$faceBU=$paperU+3/$coe;
	$faceBB=$paperB-3/$coe;

	my $centerH = ($paperL+$paperR)/2;
	my $centerV = ($paperU+$paperB)/2;

	if($nFaces>1) {
		if($nFaceId % 2 == 0) {
	   	$faceBR = $centerH - 5/$coe;
		} else {
	   	$faceBL = $centerH + 5/$coe;
		}
	}
	if($nFaces>2) {
		if($nFaceId < 2) {
	   	$faceBB = $centerV - 5/$coe;
		} else {
	   	$faceBU = $centerV + 5/$coe;
		}
	}

	$minX=10000000;
	$maxX=-10000000;
	$minY=10000000;
	$maxY=-10000000;

	$generate=0;
	$findPaperBounds=0;
	$p=SWF::Parser->new('header-callback' => \&header, 'tag-callback' => \&data);
	$p->parse_file($swfFile);

	$::face_width=($maxX-$minX)*$coe;
	$::face_height=($maxY-$minY)*$coe;

	zprintf ( "<Face id=\"Face%d\" x=\"%.2f\" y=\"%.2f\"%s>\n", $nFaceId+1, ($minX-$paperL)*$coe, ($minY-$paperU)*$coe, $nFaces==2 ? " name=\"" . ( $nFaceId==0 ? "Front" : "Back" ) . "\"" : "" ) ;
	print "<Contour x=\"0\" y=\"0\">\n";

	$generate=1;
	$findPaperBounds=0;
	$p=SWF::Parser->new('header-callback' => \&header, 'tag-callback' => \&data);
	$p->parse_file($swfFile);

	print "</Contour>\n";
	print "</Face>\n";
}

}

sub header {
    my ($self, $signature, $version, $length, $xmin, $ymin, $xmax, $ymax, $rate, $count)=@_;
#    print STDERR <<HEADER;
#Header:
#SIGNATURE = $signature
#VERSION = $version
#File length = $length
#Rect size = ($xmin, $ymin)-($xmax, $ymax)
#Frame rate = $rate
#Frame count = $count
#
#HEADER

}


sub data {

	my ($self, $tag, $length, $stream)=@_;
	my $t = SWF::Element::Tag->new(Tag=>$tag, Length=>$length);
	my ($tagname) = $t->tag_name;
 
	$count++;
	eval {
		$t->unpack($stream);
	};
	if ($@) {
		my $mes = $@;
		$t->dumper;
		die $mes;
	}
	my @names=$t->element_names;

	for my $key (@names) {
		no warnings 'uninitialized';
		my $t2=$t->$key;
		if (($t->element_type($key) !~/^\$/) && ($t2->defined)) {

			if($key eq "Shapes") {
   	 		my @names=$t->$key->element_names;

    			for my $key2 (@names) {
					no warnings 'uninitialized';
					if ($t->$key->element_type($key2) =~/^\$/) {
			   		my $p = $t->$key->$key2;
					 	$p = "\"$p\"" unless $p=~/^[-\d.]+$/;
					} elsif (($t->$key->$key2->defined) && ($key2 eq "ShapeRecords")) {
						my $k2 = $t->$key->$key2;
						for my $i (@$k2) {
							my $type = ref($i);
							my $newtype = "";
							$isVLine = 0;
							$isHLine = 0;
							if ($type =~ "SWF::Element::SHAPERECORD[0-9]::STYLECHANGERECORD") { 
#								print "<MoveTo x=\"" . $X . "\" y=\"" . $Y . "\" />\n";
#								if(($i->MoveDeltaX<5) && ($i->MoveDeltaY<5)) {
#									$skip=3;
#								} else {
									$skip=-1;
									my $pX=setX(0,$i->MoveDeltaX);
									my $pY=setY(0,$i->MoveDeltaY);
									if($generate && $X>=$minX && $X<=$maxX && $Y>=$minY && $Y<=$maxY) { 
										zprintf("$align<MoveTo x=\"%.2f\" y=\"%.2f\" />\n",$pX,$pY); 
									}
#								}
							}
							if ($type eq "SWF::Element::SHAPERECORDn::STRAIGHTEDGERECORD") {
								if($i->DeltaX == 0) { $isVLine = 1; }
								if($i->DeltaY == 0) { $isHLine = 1; }
#								print "<LineTo x=\"" . ($X+=$i->DeltaX) . "\" y=\"" . ($Y+=$i->DeltaY) . "\" />\n";
								if($skip>=0) {
									$skip--
								} else {
									my $pX=setX($i->DeltaX);
									my $pY=setY($i->DeltaY);
									if($generate && $X>=$minX && $X<=$maxX && $Y>=$minY && $Y<=$maxY) { 
      								zprintf("$align<LineTo x=\"%.2f\" y=\"%.2f\" />\n",$pX,$pY); 
									}
								}
							}
							if ($type eq "SWF::Element::SHAPERECORDn::CURVEDEDGERECORD") { 
								my $pCX=setX($i->ControlDeltaX);
								my $pCY=setY($i->ControlDeltaY);
								my $pX=setX($i->AnchorDeltaX);
								my $pY=setY($i->AnchorDeltaY);
#								print "<CurveTo cx=\"" . ($X+=$i->ControlDeltaX) . "\" cy=\"" . ($Y+=$i->ControlDeltaY) . "\" x=\"" . ($X+=$i->AnchorDeltaX) . "\" y=\"" . ($Y+=$i->AnchorDeltaY) . "\" />\n";
								if($generate && $X>=$minX && $X<=$maxX && $Y>=$minY && $Y<=$maxY) { 
									zprintf("$align<CurveTo cx=\"%.2f\" cy=\"%.2f\" x=\"%.2f\" y=\"%.2f\" />\n",$pCX,$pCY,$pX,$pY); 
								}
							}
						}
#						if($generate) { print "\n"; }
					}
				}
			}
		}
	}
}

sub setX {
	my ($deltaX,$currX) = @_;
	if(defined $currX) { $X=$currX };
	$X+=$deltaX;
	if($findPaperBounds) {
		if($isVLine) {
			if($X<$paperL) { $paperL = $X; }
			if($X>$paperR) { $paperR = $X; }
		}
		return;
	}
	if(!$generate) {
#		my $isHVLine = ($isHLine || $isVLine);
#		if(($X<$minX) && !defined $currX && !($X<=$faceBL && $isHVLine)) { $minX=$X; }
#		if(($X>$maxX) && !defined $currX && !($X>=$faceBR && $isHVLine)) { $maxX=$X; }
		my $isInFaceBounds = $X>=$faceBL && $X<=$faceBR && $Y>=$faceBU && $Y<=$faceBB ;
		if(($X<$minX) && $isInFaceBounds) { $minX=$X; }
		if(($X>$maxX) && $isInFaceBounds) { $maxX=$X; }
	} else {
		($X-$minX)*$coe;
	}
}

sub setY {
	my ($deltaY,$currY) = @_;
	if(defined $currY) { $Y=$currY };
	$Y+=$deltaY;
	if($findPaperBounds) {
		if($isHLine) {
			if($Y<$paperU) { $paperU = $Y; }
			if($Y>$paperB) { $paperB = $Y; }
		}
		return;
	}
	if(!$generate) {
#		my $isHVLine = ($isHLine || $isVLine);
#		if(($Y<$minY) && !defined $currY && !($Y<=$faceBU && $isHVLine)) { $minY=$Y; }
#		if(($Y>$maxY) && !defined $currY && !($Y>=$faceBB && $isHVLine)) { $maxY=$Y; }
		my $isInFaceBounds = $X>=$faceBL && $X<=$faceBR && $Y>=$faceBU && $Y<=$faceBB ;
		if(($Y<$minY) && $isInFaceBounds) { $minY=$Y; }
		if(($Y>$maxY) && $isInFaceBounds) { $maxY=$Y; }
	} else {
#		($maxY-$Y)*$coe;
		($Y-$minY)*$coe;
	}
}

sub zprintf { 
my $fmt = shift ;
my $s = sprintf($fmt,@_) ;
$s =~ s/\.0+\"/\"/g ;
$s =~ s/(\.[1-9]+)0+\"/$1\"/g ;
print $s ;
}


1;