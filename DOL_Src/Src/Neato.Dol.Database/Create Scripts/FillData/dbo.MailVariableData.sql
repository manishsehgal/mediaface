SET IDENTITY_INSERT [dbo].[MailVariable] ON
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (1, '%UserName')
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (2, '%NewPasswordLink')
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (3, '%PrintzHomeLink')
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (4, '%Login')
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (5, '%Password')
SET IDENTITY_INSERT [dbo].[MailVariable] OFF