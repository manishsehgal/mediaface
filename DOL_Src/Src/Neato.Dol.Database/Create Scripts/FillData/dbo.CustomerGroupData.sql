BEGIN TRANSACTION

SET IDENTITY_INSERT [dbo].[CustomerGroup] ON

if (select count(*) from [dbo].[CustomerGroup] where Id=1) = 0
	insert into [dbo].[CustomerGroup] ([Id], [Name]) values (1, N'MFO')

if (select count(*) from [dbo].[CustomerGroup] where Id=2) = 0
	insert into [dbo].[CustomerGroup] ([Id], [Name]) values (2, N'MFOPE')

SET IDENTITY_INSERT [dbo].[CustomerGroup] OFF

COMMIT
 