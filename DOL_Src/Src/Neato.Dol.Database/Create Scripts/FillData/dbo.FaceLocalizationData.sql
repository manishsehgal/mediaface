    
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1001, N'', N'Business Card Sleeve')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1002, N'', N'Audio J Card (inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1003, N'', N'Slim DVD Case Insert')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1004, N'', N'DAT Label 2')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1005, N'', N'VHS Video Sleeve')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1006, N'', N'Floppy Disk Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1007, N'', N'Super Jewel Box DVD Booklet (Inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1008, N'', N'Audio J Card (outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1009, N'', N'Super Jewel Box CD Booklet (Inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1010, N'', N'CD Jewel Case Insert Card (outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1011, N'', N'Zip Insert (outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1012, N'', N'Super Disk Insert (outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1013, N'', N'Business Card Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1014, N'', N'Utility Label 3')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1015, N'', N'Jewel Case Spine Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1016, N'', N'Super Jewel Box CD Booklet (Outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1017, N'', N'Audio Cassette Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1018, N'', N'Super Jewel Box DVD Tray Liner (Inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1019, N'', N'VHS Face Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1020, N'', N'DAT Label 1')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1021, N'', N'Zip Insert (inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1022, N'', N'JAZ Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1023, N'', N'Utility Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1024, N'', N'Minidisk Label 2')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1025, N'', N'Super Disk Insert (inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1026, N'', N'Video Wrap (Insert)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1027, N'', N'CD Print & File Pouch (non-adhesive)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1028, N'', N'Minidisk Label 3')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1029, N'', N'CD Jewel Case Tray Liner')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1030, N'', N'Minidisk Label 1')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1031, N'', N'Mini CD/DVD Case Insert')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1032, N'', N'JAZ Insert')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1033, N'', N'VHS Spine Labels')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1034, N'', N'Anylabel')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1035, N'', N'CD/DVD Label (3up)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1036, N'', N'SlimLine Jewel Case Tray Liner (outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1037, N'', N'Utility Label 2')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1038, N'', N'CD Jewel Case Booklet')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1039, N'', N'CD Print & File Pouch (adhesive)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1040, N'', N'DAT J Card (inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1041, N'', N'CD/DVD Full Coverage Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1042, N'', N'Super Jewel Box CD Tray Liner (Inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1043, N'', N'Mini CD Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1044, N'', N'CD Jewel Case Insert Card (inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1045, N'', N'CD/DVD Core Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1046, N'', N'Adhesive Spine (CD Plus Sheets)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1047, N'', N'Super Jewel Box CD Tray Liner (Outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1048, N'', N'CD/DVD Label (2up)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1049, N'', N'DAT J Card (outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1050, N'', N'Super Jewel Box DVD Tray Liner (Outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1051, N'', N'Super Jewel Box DVD Booklet (Outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1052, N'', N'SlimLine Jewel Case Tray Liner (inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1053, N'', N'HandiCD Sleeve')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1054, N'', N'Super Disk Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1055, N'', N'DVD Case Insert')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1056, N'', N'DVD Case Booklet (inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1057, N'', N'DVD Case Booklet (outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1058, N'', N'HandiCD Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1059, N'', N'Digital Vinyl CD Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1060, N'', N'Zip label')
GO

--- DirectToCD ---

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1070, N'', N'DirectToCD CD/DVD')

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1071, N'', N'DirectToCD Mini CD')

/*
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1072, N'', N'DirectToCD CD/DVD (3up)')

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1073, N'', N'DirectToCD CD/DVD Full coverage')

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1074, N'', N'DirectToCD CD/DVD Core')
*/

--- LightScribe ---

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1080, N'', N'LightScribe CD/DVD')

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1081, N'', N'LightScribe Mini CD')

/*
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1082, N'', N'LightScribe CD/DVD (3up)')

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1083, N'', N'LightScribe CD/DVD Full coverage')

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1084, N'', N'LightScribe CD/DVD Core')
*/


--- MP3 players ---
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1090, N'', N'iPod Nano Back')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1091, N'', N'iPod Nano Front')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1092, N'', N'iPod Video Back')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1093, N'', N'iPod Video Front')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1094, N'', N'iPod Mini Back')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1095, N'', N'iPod Mini Front')
