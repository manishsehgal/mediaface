SET IDENTITY_INSERT PaperAccess ON

INSERT INTO PaperAccess ([Id], [Name]) VALUES (1, 'Unregistered')
INSERT INTO PaperAccess ([Id], [Name]) VALUES (2, 'MFO')
INSERT INTO PaperAccess ([Id], [Name]) VALUES (3, 'MFOPE')

SET IDENTITY_INSERT PaperAccess OFF