SET IDENTITY_INSERT [dbo].[MailType] ON
INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (1, 'Forgot Password')
INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (2, 'Account created')
SET IDENTITY_INSERT [dbo].[MailType] OFF