SET IDENTITY_INSERT [dbo].[Page] ON
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (1, N'Default Menu Page', N'Default Menu Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (2, N'default.aspx', N'Home Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (3, N'', N'CopyRight Label')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (4, N'mailto:feltech@neato.com', N'Contacts Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (5, N'Default Footer', N'DefaultFooter')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (6, N'TermsOfUse.aspx', N'Terms Of Use')
SET IDENTITY_INSERT [dbo].[Page] OFF
