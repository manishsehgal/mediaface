SET IDENTITY_INSERT [dbo].[MailTypeVariable] ON
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (1, 1, 1)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (2, 1, 2)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (3, 2, 3)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (4, 2, 4)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (5, 2, 5)
SET IDENTITY_INSERT [dbo].[MailTypeVariable] OFF
