INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (1, '', N'Password Reminder From Neato', N'Hello %UserName:
Here is the password you requested. Click %NewPasswordLink to generate your new password. After, please use it with your email address to log in at Neato.')
INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (2, '', N'Your account was created!', N'Your account on MediaFACE online was created successfully.
You can login on MediaFACE online now.
%PrintzHomeLink
Your Login: %Login
Your Password: %Password')
