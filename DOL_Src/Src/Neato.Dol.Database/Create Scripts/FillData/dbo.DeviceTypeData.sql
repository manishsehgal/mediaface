set identity_insert dbo.DeviceType on
INSERT INTO DeviceType ([Id], [Visible], [Target], [SortOrder], [Name]) VALUES (1, 1, 1, 1, 'CD/DVD Labels')
INSERT INTO DeviceType ([Id], [Visible], [Target], [SortOrder], [Name]) VALUES (2, 1, 1, 2, 'Insert & Sleeves')
INSERT INTO DeviceType ([Id], [Visible], [Target], [SortOrder], [Name]) VALUES (3, 1, 2, 3, 'MP3 Players')
INSERT INTO DeviceType ([Id], [Visible], [Target], [SortOrder], [Name]) VALUES (4, 1, 3, 4, 'DirectToCD')
INSERT INTO DeviceType ([Id], [Visible], [Target], [SortOrder], [Name]) VALUES (5, 1, 3, 5, 'Lightscribe')
INSERT INTO DeviceType ([Id], [Visible], [Target], [SortOrder], [Name]) VALUES (6, 0, 1, 6, 'Other Labels')
set identity_insert dbo.DeviceType off