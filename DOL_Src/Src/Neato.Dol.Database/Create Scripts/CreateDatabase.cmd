@echo off
REM: Command File Created by Microsoft Visual Database Tools
REM: Date Generated: 13.10.2005
REM: Authentication type: SQL Server
REM: Usage: CommandFilename [Server] [Database] [Login] [Password]

if '%1' == '' goto usage
if '%2' == '' goto usage
if '%3' == '' goto usage

if '%1' == '/?' goto usage
if '%1' == '-?' goto usage
if '%1' == '?' goto usage
if '%1' == '/help' goto usage

REM: Parameter -d Master do not use. If use command like this:
REM: osql -S %1 -d Master -U %3 -P %4 -b -i "Database\db_dol.DBS"
REM: ... than error occured:
REM: "Cannot open database requested in login 'Master'. Login fails" Windows 2003 server


echo>>%2create.sql DECLARE @DatabaseName NVARCHAR(100)
if %ERRORLEVEL% NEQ 0 goto errors
echo>>%2create.sql SET @DatabaseName = '%2'
if %ERRORLEVEL% NEQ 0 goto errors
copy %2create.sql + "Database\db_dol.DBS" %2create.sql 
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -U %3 -P %4 -b -i "%2create.sql"
if %ERRORLEVEL% NEQ 0 goto errors
del "%2create.sql"
if %ERRORLEVEL% NEQ 0 goto errors
SET ExitCode=0
goto finish

REM: How to use screen
:usage
echo.
echo Usage: MyScript Server Database User [Password]
echo Server: the name of the target SQL Server
echo Database: the name of the target database
echo User: the login name on the target server
echo Password: the password for the login on the target server (optional)
echo.
echo Example: MyScript.cmd MainServer MainDatabase MyName MyPassword
echo.
echo.
goto done

REM: error handler
:errors
if exist "%2create.sql" del "%2create.sql"
echo.
echo WARNING! Error(s) were detected!
echo --------------------------------
echo Please evaluate the situation and, if needed,
echo restart this command file. You may need to
echo supply command parameters when executing
echo this command file.
echo.
pause
SET ExitCode=-1
goto done

REM: finished execution
:finish
echo.
echo Script execution is complete!
:done
@echo on
exit /b %ExitCode%