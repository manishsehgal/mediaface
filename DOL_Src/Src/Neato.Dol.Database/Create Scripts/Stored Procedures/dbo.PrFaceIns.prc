SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrFaceIns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrFaceIns]
GO



CREATE PROCEDURE dbo.PrFaceIns
(
    @Contour IMAGE = NULL,
    @Guid uniqueidentifier,
    @FaceId INT OUTPUT
)
AS
BEGIN
    INSERT INTO dbo.Face (Contour, Guid) VALUES (@Contour, @Guid)
    SET @FaceId = SCOPE_IDENTITY()
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrFaceIns]  TO [dol_users]
GO

