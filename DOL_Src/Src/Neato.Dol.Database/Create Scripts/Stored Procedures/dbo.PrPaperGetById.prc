SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPaperGetById]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrPaperGetById]
GO



CREATE PROCEDURE dbo.PrPaperGetById
(
    @PaperId int
)
AS
    SELECT
        Id,
        [Name],
        BrandId,
        Width,
        Height,
        Scu,
        PaperType,
        State,
        Orientation,
        [Order],
        MetricId,
        MinPaperAccessId
    FROM dbo.Paper
    WHERE
        Paper.Id = @PaperId

    SELECT
        Id,
        Contour,
        Guid
    FROM dbo.Face
    WHERE Face.Id IN
    (
        SELECT FaceToPaper.FaceId
        FROM dbo.FaceToPaper
        WHERE
            PaperId = @PaperId
    )
    
    SELECT
        FaceLocalization.FaceId,
        FaceLocalization.Culture,
        FaceLocalization.FaceName
    FROM dbo.FaceLocalization
        INNER JOIN dbo.Face ON Face.Id = FaceLocalization.FaceId
        INNER JOIN dbo.FaceToPaper ON Face.Id = FaceToPaper.FaceId
    WHERE
        FaceToPaper.PaperId = @PaperId

    SELECT
        FaceId,
        PaperId,
        FaceX,
        FaceY
    FROM dbo.FaceToPaper
    WHERE
        PaperId = @PaperId
    ORDER BY Id

RETURN
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrPaperGetById]  TO [dol_users]
GO

