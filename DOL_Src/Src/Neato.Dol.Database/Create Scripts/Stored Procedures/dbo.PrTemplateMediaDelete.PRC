SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTemplateMediaDelete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrTemplateMediaDelete]
GO




CREATE PROCEDURE dbo.PrTemplateMediaDelete
(
        @TemplateId INT
)
AS
SET NOCOUNT ON
DELETE FROM TemplateMedia
WHERE
	TemplateId = @TemplateId
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrTemplateMediaDelete]  TO [dol_users]
GO

