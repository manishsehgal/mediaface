SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceAddToCarrier]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceAddToCarrier]
GO

CREATE PROCEDURE dbo.PrDeviceAddToCarrier
(
    @DeviceId INT, 
    @CarrierId INT
)
AS
BEGIN

    INSERT INTO dbo.DeviceToCarrier
    (
        DeviceId, 
        CarrierId
    )
    SELECT
        Inserted.DeviceId, Inserted.CarrierId
    FROM (SELECT @DeviceId AS DeviceId, @CarrierId AS CarrierId) Inserted
        LEFT OUTER JOIN dbo.DeviceToCarrier
        ON Inserted.DeviceId = DeviceToCarrier.DeviceId AND Inserted.CarrierId = DeviceToCarrier.CarrierId
    WHERE DeviceToCarrier.DeviceId IS NULL
    GROUP BY Inserted.DeviceId, Inserted.CarrierId
    
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceAddToCarrier]  TO [dol_users]
GO

  