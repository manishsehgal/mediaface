SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrFaceGetById]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrFaceGetById]
GO



CREATE PROCEDURE dbo.PrFaceGetById
    @FaceId INT
AS
BEGIN
    SELECT  
        Face.Id as Id,
        Face.Contour as Contour,
        Face.Guid as Guid,
	FaceLocalization.FaceName as FaceName
    FROM dbo.Face INNER JOIN FaceLocalization
    ON Face.Id = FaceLocalization.FaceId
    WHERE
       Id = @FaceId
	AND
     FaceLocalization.Culture = ''

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrFaceGetById]  TO [dol_users]
GO

