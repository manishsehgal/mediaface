 SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrFaceLocalizationDel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrFaceLocalizationDel]
GO

CREATE  PROCEDURE dbo.PrFaceLocalizationDel
(
    @FaceId INT,
    @Culture NVARCHAR(10) = NULL
)
AS
BEGIN
    DELETE FROM dbo.FaceLocalization
    WHERE
        @FaceId = FaceLocalization.FaceId
        AND
        (@Culture IS NULL OR FaceLocalization.Culture = @Culture)
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrFaceLocalizationDel]  TO [dol_users]
GO

  