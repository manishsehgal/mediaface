SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrFaceGetIdByGuid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrFaceGetIdByGuid]
GO

CREATE PROCEDURE dbo.PrFaceGetIdByGuid
    @Guid uniqueidentifier
AS
BEGIN
    SELECT Id FROM dbo.Face WHERE Guid = @Guid
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrFaceGetIdByGuid]  TO [dol_users]
GO

