SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPaperIconUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrPaperIconUpd]
GO

CREATE PROCEDURE dbo.PrPaperIconUpd
    @PaperId INT,
    @Icon image
AS

UPDATE dbo.Paper SET Icon = @Icon WHERE Id = @PaperId

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrPaperIconUpd]  TO [dol_users]
GO

  