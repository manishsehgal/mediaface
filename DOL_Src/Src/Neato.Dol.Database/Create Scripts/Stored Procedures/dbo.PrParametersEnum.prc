SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrParametersEnum]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrParametersEnum]
GO

CREATE PROCEDURE dbo.PrParametersEnum
AS

SELECT  Id, [Key], [Value]
FROM    Parameters

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrParametersEnum]  TO [dol_users]
GO