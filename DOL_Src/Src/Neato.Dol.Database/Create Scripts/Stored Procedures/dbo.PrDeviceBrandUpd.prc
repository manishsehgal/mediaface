SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceBrandUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceBrandUpd]
GO

CREATE PROCEDURE dbo.PrDeviceBrandUpd
(
    @DeviceBrandId INT,
    @Name NVARCHAR(30)
)
AS
BEGIN
    UPDATE dbo.DeviceBrand
    SET
        [Name] = @Name
    WHERE
        Id = @DeviceBrandId
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceBrandUpd]  TO [dol_users]
GO

 