SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceEnumerate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceEnumerate]
GO




CREATE PROCEDURE dbo.PrDeviceEnumerate
    @DeviceTypeId INT = NULL,
    @MinDeviceTypeId INT = NULL,
    @ProhibitedDeviceTypeId INT = NULL,
    @BrandId      INT = NULL,
    @CarrierId    INT = NULL,
    @FaceId       INT = NULL,
    @DeviceModel  NVARCHAR(50) = NULL
AS
SET NOCOUNT ON

SELECT 
    Device.Id
FROM
    dbo.DeviceType
    INNER JOIN dbo.Device
    ON DeviceType.Id = Device.DeviceTypeId
    LEFT OUTER JOIN dbo.DeviceToCarrier
    ON DeviceToCarrier.DeviceId = Device.Id
    LEFT OUTER JOIN dbo.FaceToDevice
    ON FaceToDevice.DeviceId = Device.Id
WHERE
    ((@MinDeviceTypeId IS NULL
        AND (@DeviceTypeId IS NULL OR Device.DeviceTypeId = @DeviceTypeId))
    OR
    (@MinDeviceTypeId IS NOT NULL
        AND
            (Device.DeviceTypeId >= @MinDeviceTypeId
            OR (@DeviceTypeId IS NULL OR Device.DeviceTypeId = @DeviceTypeId))))
    AND
    (@ProhibitedDeviceTypeId IS NULL OR Device.DeviceTypeId <> @ProhibitedDeviceTypeId)
    AND
    (@BrandId IS NULL OR Device.BrandId = @BrandId)
    AND
    (@CarrierId IS NULL OR DeviceToCarrier.CarrierId = @CarrierId)
    AND
    (@FaceId IS NULL OR FaceToDevice.FaceId = @FaceId)
    AND
    (@DeviceModel IS NULL OR Device.Model LIKE '%' + @DeviceModel + '%')
GROUP BY
    DeviceType.Name,
    Device.SortOrder,
    Device.Model,
    Device.BrandId,
    Device.DeviceTypeId,
    Device.Id
ORDER BY DeviceType.Name, Device.SortOrder DESC, Device.Model



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceEnumerate]  TO [dol_users]
GO

