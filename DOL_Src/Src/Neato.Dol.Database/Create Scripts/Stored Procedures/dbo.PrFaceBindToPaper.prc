SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrFaceBindToPaper]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrFaceBindToPaper]
GO


CREATE  PROCEDURE dbo.PrFaceBindToPaper
(
    @FaceId INT,
    @PaperId INT,
    @FaceX REAL,
    @FaceY REAL
)
AS
BEGIN
    INSERT INTO dbo.FaceToPaper
    (
        FaceId, 
        PaperId,
        FaceX,
        FaceY
    )
    VALUES
    (
        @FaceId,
        @PaperId,
        @FaceX,
        @FaceY
    )
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrFaceBindToPaper]  TO [dol_users]
GO

