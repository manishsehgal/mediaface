SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTrackingShapesEnumByTime]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrTrackingShapesEnumByTime]
GO
CREATE PROCEDURE dbo.PrTrackingShapesEnumByTime
(

    @TimeStart datetime ,
    @TimeEnd datetime
  
)
AS
begin
SELECT [ShapeId],COUNT([ShapeId]) as Count
    FROM dbo.Tracking_Shapes
    WHERE (
		Time <= @TimeEnd AND
	        Time >= @TimeStart 
	)
    GROUP by [ShapeId]
    ORDER BY Count DESC, [ShapeId] ASC

end
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrTrackingShapesEnumByTime]  TO [dol_users]
GO

