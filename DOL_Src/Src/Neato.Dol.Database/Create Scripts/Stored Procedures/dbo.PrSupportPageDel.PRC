SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrSupportPageDel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrSupportPageDel]
GO

CREATE PROCEDURE dbo.PrSupportPageDel
(
    @Culture NVARCHAR(10)
)
AS
DELETE FROM dbo.SupportPage
WHERE Culture = @Culture


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrSupportPageDel]  TO [dol_users]
GO

