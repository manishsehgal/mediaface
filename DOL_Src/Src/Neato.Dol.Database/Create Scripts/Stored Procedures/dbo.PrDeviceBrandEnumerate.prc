SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceBrandEnumerate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceBrandEnumerate]
GO

CREATE PROCEDURE dbo.PrDeviceBrandEnumerate
    @CarrierId INT = NULL,
    @DeviceTypeId INT = NULL
AS
SET NOCOUNT ON


SELECT
    DeviceBrand.Id,
    DeviceBrand.Name
FROM
    dbo.DeviceBrand
    LEFT OUTER JOIN dbo.Device
    ON DeviceBrand.Id = Device.BrandId
    LEFT OUTER JOIN DeviceToCarrier
    ON Device.Id = DeviceToCarrier.DeviceId
WHERE
    (@DeviceTypeId IS NULL OR Device.DeviceTypeId = @DeviceTypeId)
    AND
    (@CarrierId IS NULL OR DeviceToCarrier.CarrierId = @CarrierId)
GROUP BY DeviceBrand.Id, DeviceBrand.Name
ORDER BY DeviceBrand.Name

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceBrandEnumerate]  TO [dol_users]
GO

