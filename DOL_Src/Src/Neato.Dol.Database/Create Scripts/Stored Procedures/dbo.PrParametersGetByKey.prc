SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrParametersGetByKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrParametersGetByKey]
GO

CREATE PROCEDURE dbo.PrParametersGetByKey
(
	@Key nvarchar (40)
)
AS
BEGIN
    SELECT
        [Value]
    FROM
        dbo.Parameters
    WHERE
        [Key] = @Key
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT EXECUTE ON [dbo].[PrParametersGetByKey] TO [dol_users]
GO