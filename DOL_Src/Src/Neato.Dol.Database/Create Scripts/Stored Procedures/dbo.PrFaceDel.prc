SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrFaceDel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrFaceDel]
GO

CREATE  PROCEDURE dbo.PrFaceDel
(
    @FaceId INT
)
AS
DELETE FROM dbo.LayoutFaceToIcon WHERE FaceId = @FaceId
DELETE FROM dbo.Face WHERE Id = @FaceId

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrFaceDel]  TO [dol_users]
GO
  