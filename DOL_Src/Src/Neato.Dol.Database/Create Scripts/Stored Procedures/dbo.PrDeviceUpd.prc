SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceUpd]
GO

CREATE PROCEDURE dbo.PrDeviceUpd
(
    @DeviceId INT,
    @Model NVARCHAR(50), 
    @BrandId INT,
    @DeviceTypeId INT,
    @SortOrder INT
)
AS
BEGIN
    UPDATE dbo.Device
    SET
        Model = @Model,
        BrandId = @BrandId,
        DeviceTypeId = @DeviceTypeId,
        SortOrder = @SortOrder
    WHERE
        Id = @DeviceId
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceUpd]  TO [dol_users]
GO

  