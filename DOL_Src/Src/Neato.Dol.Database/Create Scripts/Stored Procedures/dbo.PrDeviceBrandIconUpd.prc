SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceBrandIconUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceBrandIconUpd]
GO

CREATE PROCEDURE dbo.PrDeviceBrandIconUpd
    @DeviceBrandId INT,
    @Icon image
AS

UPDATE dbo.DeviceBrand SET Icon = @Icon WHERE Id = @DeviceBrandId

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceBrandIconUpd]  TO [dol_users]
GO

 