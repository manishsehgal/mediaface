SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrImageLibEnum]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrImageLibEnum]
GO

CREATE PROCEDURE dbo.PrImageLibEnum
(
@LibId INT
)
AS

SELECT
    FolderId,
    ParentFolderId
INTO
    #tmp
FROM dbo.ImageLibFolder
WHERE (@LibId = 1 OR (LibId = @LibId))
    AND (ParentFolderId IN (
        SELECT FolderId
        FROM dbo.ImageLibFolder
        WHERE @LibId = 1 OR LibId = @LibId)
    OR ParentFolderId IS NULL)
  

SELECT
    ilf.FolderId,
    ilf.ParentFolderId,
    ilf.SortOrder,
    ilf.LibId
FROM dbo.ImageLibFolder AS ilf
JOIN #tmp ON #tmp.FolderId = ilf.FolderId
ORDER BY SortOrder

SELECT
    loc.FolderId,
    loc.Culture,
    loc.Caption
FROM dbo.ImageLibFolderLocalization AS loc
JOIN #tmp ON #tmp.FolderId = loc.FolderId

SELECT
    ItemId
FROM dbo.ImageLibItem
WHERE ItemId IN (
    SELECT ilfi.ItemId
    FROM dbo.ImageLibFolderItem AS ilfi
    JOIN #tmp ON #tmp.FolderId = ilfi.FolderId
)

SELECT
    ilfi.FolderId,
    ilfi.ItemId
FROM dbo.ImageLibFolderItem AS ilfi
JOIN #tmp ON #tmp.FolderId = ilfi.FolderId

DROP TABLE #tmp

RETURN
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrImageLibEnum]  TO [dol_users]
GO

