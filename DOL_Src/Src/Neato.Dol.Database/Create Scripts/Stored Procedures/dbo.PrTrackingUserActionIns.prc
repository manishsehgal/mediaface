SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTrackingUserActionIns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrTrackingUserActionIns]
GO
CREATE PROCEDURE dbo.PrTrackingUserActionIns
(
    @Id int output,
    @Action varchar(200),
    @Login varchar(200),
    @Time datetime
)
AS
    INSERT INTO dbo.Tracking_UserAction (Action, Login, [Time]) VALUES (@Action, @Login, @Time)
    SET @Id = SCOPE_IDENTITY()
RETURN 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrTrackingUserActionIns]  TO [dol_users]
GO

