SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTemplateIconEnumById]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrTemplateIconEnumById]
GO



CREATE PROCEDURE dbo.PrTemplateIconEnumById
(
    @TemplateId INT
)
AS
begin
	select Stream from TemplateIcon where TemplateId = @TemplateId
end

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrTemplateIconEnumById]  TO [dol_users]
GO

