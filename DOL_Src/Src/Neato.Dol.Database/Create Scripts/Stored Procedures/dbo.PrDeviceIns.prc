SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceIns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceIns]
GO

CREATE PROCEDURE dbo.PrDeviceIns
(
    @Model NVARCHAR(50), 
    @BrandId INT,
    @DeviceTypeId INT,
    @SortOrder INT,
    @DeviceId INT OUTPUT
)
AS
BEGIN
    INSERT INTO dbo.Device
    (
        Model,
        BrandId,
        DeviceTypeId,
        SortOrder
    )
    VALUES
    (
        @Model,
        @BrandId,
        @DeviceTypeId,
        @SortOrder
    )
    SET @DeviceId = SCOPE_IDENTITY()
    set 
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceIns]  TO [dol_users]
GO

  