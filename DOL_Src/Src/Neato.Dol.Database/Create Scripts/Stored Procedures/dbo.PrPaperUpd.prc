SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPaperUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrPaperUpd]
GO




CREATE PROCEDURE dbo.PrPaperUpd
(
    @PaperId INT,
    @Name NVARCHAR(64), 
    @PaperBrandId INT,
    @Width REAL,
    @Height REAL,
    @PaperType INT,
    @Scu NVARCHAR(256),
    @PaperState NCHAR(1),
    @Orientation NCHAR(1),
    @Order INT,
    @MetricId INT = NULL,
    @MinPaperAccessId INT
)
AS
BEGIN
    UPDATE dbo.Paper
    SET
        [Name] = @Name,
        BrandId = @PaperBrandId,
        Width = @Width,
        Height = @Height,
        PaperType = @PaperType,
        Scu = @Scu,
        State = @PaperState,
        Orientation = @Orientation,
        [Order] = @Order,
        MetricId = @MetricId,
        MinPaperAccessId = @MinPaperAccessId
    WHERE
        Id = @PaperId
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrPaperUpd]  TO [dol_users]
GO

