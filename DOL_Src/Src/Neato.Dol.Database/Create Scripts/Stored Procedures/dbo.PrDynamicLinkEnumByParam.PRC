SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDynamicLinkEnumByParam]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDynamicLinkEnumByParam]
GO

CREATE    PROCEDURE dbo.PrDynamicLinkEnumByParam
(
    @PageId INT,
    @Place VARCHAR(50),
    @Culture NVARCHAR(10)
)
AS
SELECT
    LinkPlace.LinkId, 
    LinkPlace.Align, 
    LinkPlace.IsNewWindow, 
    LinkPage.Url,
    LinkPlace.SortOrder,
    LinkPlace.IsVisible,
    LinkLocalization.Text
FROM
    LinkPlace
    INNER JOIN Link 
        ON LinkPlace.LinkId = Link.LinkId
    INNER JOIN Page AS LinkPage
        ON Link.PageId = LinkPage.PageId
    LEFT JOIN Page
        ON LinkPlace.PageId = Page.PageId
    INNER JOIN LinkLocalization
        ON LinkLocalization.LinkId = Link.LinkId
    WHERE LinkPlace.Place = @Place
        AND Page.PageId = @PageId
        AND LinkLocalization.Culture = @Culture
ORDER BY LinkPlace.SortOrder
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDynamicLinkEnumByParam]  TO [dol_users]
GO

