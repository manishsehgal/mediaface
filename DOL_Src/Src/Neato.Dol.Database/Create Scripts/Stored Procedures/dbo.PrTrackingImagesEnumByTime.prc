SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTrackingImagesEnumByTime]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrTrackingImagesEnumByTime]
GO
CREATE PROCEDURE dbo.PrTrackingImagesEnumByTime
(

    @TimeStart datetime ,
    @TimeEnd datetime
  
)
AS
begin
SELECT [ImageId],COUNT([ImageId]) as Count
    FROM dbo.Tracking_Images
    WHERE (
		Time <= @TimeEnd AND
	        Time >= @TimeStart 
	)
    GROUP by [ImageId]
    ORDER BY Count DESC
SELECT
	ImageLibFolderLocalization.[Caption] as Caption,
	ImageLibFolderItem.[FolderId] as FolderId,
	Tracking_Images.[ImageId] as ImageId
FROM dbo.Tracking_Images
	INNER JOIN dbo.ImageLibFolderItem
	ON ImageLibFolderItem.ItemId = Tracking_Images.ImageId
	INNER JOIN dbo.ImageLibFolderLocalization
	ON ImageLibFolderLocalization.FolderId = ImageLibFolderItem.FolderId
    WHERE (
	        (@TimeEnd IS NULL OR Time <= @TimeEnd) AND
	        (@TimeStart IS NULL OR Time >= @TimeStart) AND
		ImageLibFolderLocalization.Culture = '' 
	)
    GROUP by
    ImageLibFolderItem.FolderId,
    Tracking_Images.[ImageId],
    ImageLibFolderLocalization.[Caption]
    Order by Tracking_Images.[ImageId]

end
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrTrackingImagesEnumByTime]  TO [dol_users]
GO

