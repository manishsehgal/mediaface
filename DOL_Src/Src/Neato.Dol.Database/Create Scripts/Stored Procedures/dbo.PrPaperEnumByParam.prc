SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPaperEnumByParam]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrPaperEnumByParam]
GO

CREATE PROCEDURE dbo.PrPaperEnumByParam
(
    @PaperBrandId int = NULL,
    @DeviceTypeId int = NULL,    
    @MinDeviceTypeId INT = NULL,
    @MaxDeviceTypeId INT = NULL,
    @ProhibitedDeviceTypeId int = NULL,    
    @DeviceId int = NULL,
    @FaceId int = NULL,    
    @PaperName NVARCHAR(64) = NULL,
    @PaperId INT = NULL,
    @MetricId INT = NULL,
    @PaperState NCHAR(16) = NULL
)
AS
    SELECT DISTINCT
	    Paper.Id,
	    Paper.[Name],
	    Paper.BrandId,
	    Paper.Width,
	    Paper.Height,
        Paper.Scu,
        Paper.PaperType,
        Paper.State,
        Paper.Orientation,
        Paper.MetricId,
        Paper.[Order],
        Paper.MinPaperAccessId
    FROM dbo.Paper
        LEFT JOIN dbo.FaceToPaper ON FaceToPaper.PaperId = Paper.Id
            LEFT JOIN dbo.FaceToDevice ON FaceToDevice.FaceId = FaceToPaper.FaceId
                LEFT JOIN dbo.Device ON Device.Id = FaceToDevice.DeviceId
    WHERE
        (@PaperBrandId IS NULL OR Paper.BrandId = @PaperBrandId)
        AND
        ((@MinDeviceTypeId IS NULL
            AND (@DeviceTypeId IS NULL OR Device.DeviceTypeId = @DeviceTypeId))
        OR
        (@MinDeviceTypeId IS NOT NULL
            AND
                (Device.DeviceTypeId >= @MinDeviceTypeId
                OR (@DeviceTypeId IS NULL OR Device.DeviceTypeId = @DeviceTypeId))))
        AND
        (@MaxDeviceTypeId IS NULL OR Device.DeviceTypeId <= @MaxDeviceTypeId)
        AND
        (@ProhibitedDeviceTypeId IS NULL OR Device.DeviceTypeId <> @ProhibitedDeviceTypeId)
        AND
        (@FaceId IS NULL OR
        (@FaceId > 0 AND FaceToPaper.FaceId = @FaceId) OR
        (@FaceId < 0 AND NOT EXISTS
        (SELECT * FROM FaceToPaper WHERE FaceToPaper.PaperId = Paper.Id)))
        AND
        (@DeviceId IS NULL OR FaceToDevice.DeviceId = @DeviceId)
        AND
        (@PaperId IS NULL OR Paper.Id = @PaperId)
        AND
        (@PaperName IS NULL OR Paper.[Name] LIKE '%' + @PaperName + '%')
        AND
        (
        	(@PaperState IS NULL AND Paper.State = 'a')
	        OR
	        (
	            @PaperState IS NOT NULL AND
		        (
		            (CHARINDEX('a',@PaperState)>0 AND Paper.State = 'a')
		            OR
		            (CHARINDEX('i',@PaperState)>0 AND Paper.State = 'i')
		            OR
		            (CHARINDEX('t',@PaperState)>0 AND Paper.State = 't')
		        )
	        )
        )
        AND
        (@MetricId IS NULL OR Paper.MetricId = @MetricId)
    ORDER BY Paper.BrandId, Paper.MetricId, Paper.[Order]
RETURN
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrPaperEnumByParam]  TO [dol_users]
GO

