SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceBrandIns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceBrandIns]
GO

CREATE PROCEDURE dbo.PrDeviceBrandIns
(
    @Name NVARCHAR(30), 
    @DeviceBrandId INT OUTPUT
)
AS
BEGIN
    INSERT INTO dbo.DeviceBrand
    (
        [Name]
    )
    VALUES
    (
        @Name
    )
    SET @DeviceBrandId = SCOPE_IDENTITY()
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceBrandIns]  TO [dol_users]
GO

 