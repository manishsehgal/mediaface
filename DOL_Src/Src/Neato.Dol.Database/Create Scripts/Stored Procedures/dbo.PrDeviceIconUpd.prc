SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceIconUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceIconUpd]
GO

CREATE PROCEDURE dbo.PrDeviceIconUpd
    @DeviceId INT,
    @Icon image
AS

UPDATE dbo.Device SET Icon = @Icon WHERE Id = @DeviceId

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceIconUpd]  TO [dol_users]
GO

  