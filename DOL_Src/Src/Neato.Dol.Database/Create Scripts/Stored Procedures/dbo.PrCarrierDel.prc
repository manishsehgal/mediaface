SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrCarrierDel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrCarrierDel]
GO


CREATE  PROCEDURE dbo.PrCarrierDel
(
    @CarrierId INT
)
AS
DELETE FROM dbo.Carrier WHERE Id = @CarrierId

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrCarrierDel]  TO [dol_users]
GO
 