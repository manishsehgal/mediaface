SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrSpecialUserDel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrSpecialUserDel]
GO


CREATE  PROCEDURE dbo.PrSpecialUserDel
(
    @SpecialUserId INT
)
AS
DELETE FROM dbo.SpecialUser WHERE Id = @SpecialUserId
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT  EXECUTE  ON [dbo].[PrSpecialUserDel]  TO [dol_users]
GO

