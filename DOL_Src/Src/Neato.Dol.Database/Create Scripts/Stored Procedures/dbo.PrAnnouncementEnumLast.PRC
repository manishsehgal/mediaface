SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrAnnouncementEnumLast]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrAnnouncementEnumLast]
GO


CREATE PROCEDURE dbo.PrAnnouncementEnumLast
AS
SELECT TOP 1
    Id,
    Text,
    [Time],
    IsActive
FROM
    dbo.Announcement
WHERE
    IsActive = 1
ORDER BY [Time] DESC
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrAnnouncementEnumLast]  TO [dol_users]
GO

