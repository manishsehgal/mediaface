SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrCarrierIns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrCarrierIns]
GO

CREATE PROCEDURE dbo.PrCarrierIns
(
    @Name NVARCHAR(40), 
    @CarrierId INT OUTPUT
)
AS
BEGIN
    INSERT INTO dbo.Carrier
    (
        [Name]
    )
    VALUES
    (
        @Name
    )
    SET @CarrierId = SCOPE_IDENTITY()
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrCarrierIns]  TO [dol_users]
GO

 