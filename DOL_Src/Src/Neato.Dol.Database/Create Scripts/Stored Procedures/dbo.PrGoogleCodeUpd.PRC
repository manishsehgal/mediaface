SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrGoogleCodeUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrGoogleCodeUpd]
GO

CREATE  PROCEDURE dbo.PrGoogleCodeUpd
(
    @Id INT,
    @Code NTEXT
)
AS
BEGIN
    UPDATE dbo.GoogleCode
    SET
        Code = @Code
    WHERE
        [Id] = @Id
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrGoogleCodeUpd]  TO [dol_users]
GO

