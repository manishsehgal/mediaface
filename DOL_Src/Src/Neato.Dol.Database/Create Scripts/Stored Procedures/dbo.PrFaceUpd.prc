SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrFaceUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrFaceUpd]
GO



CREATE PROCEDURE dbo.PrFaceUpd
(
    @FaceId INT,
    @Contour IMAGE,
    @Guid uniqueidentifier
)
AS
BEGIN
    UPDATE dbo.Face
    SET
        Contour = @Contour,
        Guid = @Guid
    WHERE
        Id = @FaceId
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrFaceUpd]  TO [dol_users]
GO

