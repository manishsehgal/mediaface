SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDynamicLinkDelByParam]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDynamicLinkDelByParam]
GO


CREATE  PROCEDURE dbo.PrDynamicLinkDelByParam
(
    @ParentPageId INT,
    @Place VARCHAR(50),
    @LinkId INT
)
AS
BEGIN

    DELETE FROM LinkPlace
    WHERE LinkPlace.Place = @Place AND LinkPlace.LinkId = @LinkId AND LinkPlace.PageId = @ParentPageId
    DELETE FROM LinkLocalization
    WHERE NOT EXISTS (
        SELECT LinkPlace.LinkId FROM LinkPlace 
        WHERE LinkPlace.LinkId = LinkLocalization.LinkId
    )
 
    DELETE FROM Link
    WHERE NOT EXISTS (
        SELECT LinkPlace.LinkId FROM LinkPlace 
        WHERE LinkPlace.LinkId = Link.LinkId
    )

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDynamicLinkDelByParam]  TO [dol_users]
GO

