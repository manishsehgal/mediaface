SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrCustomerGetById]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrCustomerGetById]
GO


CREATE PROCEDURE dbo.PrCustomerGetById
    @CustomerId INT
AS
BEGIN
    SELECT
        CustomerId,
        FirstName,
        LastName,
        Email,
        Password,
        pswuid,
        EmailOptions,
        ReceiveInfo,
        Created,
        RetailerId,
        Active,
        GroupId
    FROM dbo.Customer
    WHERE
        CustomerId = @CustomerId
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrCustomerGetById]  TO [dol_users]
GO

