SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrCarrierGetById]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrCarrierGetById]
GO

CREATE PROCEDURE dbo.PrCarrierGetById
    @CarrierId INT
AS
SET NOCOUNT ON

SELECT TOP 1
    Id,
    [Name]
FROM dbo.Carrier 
WHERE
    Id = @CarrierId

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrCarrierGetById]  TO [dol_users]
GO

  