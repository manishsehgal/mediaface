SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPaperDel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrPaperDel]
GO

CREATE  PROCEDURE dbo.PrPaperDel
(
    @PaperId INT
)
AS
DELETE FROM dbo.Paper WHERE Id = @PaperId

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrPaperDel]  TO [dol_users]
GO
  