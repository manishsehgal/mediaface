SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTrackingShapesIns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrTrackingShapesIns]
GO
CREATE PROCEDURE dbo.PrTrackingShapesIns
(
    @Id int output,
    @ShapeId varchar(60),
    @Login varchar(200),
    @Time datetime
)
AS
    INSERT INTO dbo.Tracking_Shapes ([ShapeId],[User], [Time]) VALUES (@ShapeId,@Login, @Time)
    SET @Id = SCOPE_IDENTITY()
RETURN 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrTrackingShapesIns]  TO [dol_users]
GO

