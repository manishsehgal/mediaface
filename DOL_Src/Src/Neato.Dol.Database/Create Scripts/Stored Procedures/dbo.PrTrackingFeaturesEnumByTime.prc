SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTrackingFeaturesEnumByTime]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrTrackingFeaturesEnumByTime]
GO
CREATE PROCEDURE dbo.PrTrackingFeaturesEnumByTime
(

    @TimeStart datetime ,
    @TimeEnd datetime
  
)
AS
BEGIN
    SELECT [Action], COUNT([Action]) as Count
    FROM dbo.Tracking_Features
    WHERE (
	        (@TimeEnd IS NULL OR Time <= @TimeEnd) AND
	        (@TimeStart IS NULL OR Time >= @TimeStart) 
	  )
    GROUP by [Action]
    Order by Count DESC,[Action] ASC


END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrTrackingFeaturesEnumByTime]  TO [dol_users]
GO

