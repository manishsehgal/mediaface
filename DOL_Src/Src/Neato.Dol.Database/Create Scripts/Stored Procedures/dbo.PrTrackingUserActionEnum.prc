SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTrackingUserActionEnum]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrTrackingUserActionEnum]
GO
CREATE PROCEDURE dbo.PrTrackingUserActionEnum
AS
    SELECT [Time], Login, Action FROM dbo.Tracking_UserAction
    ORDER BY [Time] DESC 
RETURN 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrTrackingUserActionEnum]  TO [dol_users]
GO

