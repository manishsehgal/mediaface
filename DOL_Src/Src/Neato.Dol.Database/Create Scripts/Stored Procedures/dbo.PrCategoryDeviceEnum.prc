SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrCategoryDeviceEnum]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrCategoryDeviceEnum]
GO

CREATE PROCEDURE dbo.PrCategoryDeviceEnum
(
    @Mp3CategoryId INT,
    @MinCategoryId INT
)
AS
SELECT 
    [Id],
    [Visible],
    [Target],
    [Name],
    [SortOrder]
FROM dbo.DeviceType
WHERE (Id = @Mp3CategoryId) OR (Id >= @MinCategoryId)
ORDER BY [SortOrder]

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrCategoryDeviceEnum]  TO [dol_users]
GO

