SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrParametersUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrParametersUpd]
GO

CREATE PROCEDURE dbo.PrParametersUpd
(
    @Id INT,
    @Value [ntext]
)
AS
BEGIN
    UPDATE dbo.Parameters
    SET
        [Value] = @Value
    WHERE
        [Id] = @Id
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT EXECUTE ON [dbo].[PrParametersUpd] TO [dol_users]
GO

