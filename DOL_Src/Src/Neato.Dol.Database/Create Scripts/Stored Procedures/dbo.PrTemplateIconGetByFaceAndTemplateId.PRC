SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTemplateIconGetByFaceAndTemplateId]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrTemplateIconGetByFaceAndTemplateId]
GO


CREATE PROCEDURE dbo.PrTemplateIconGetByFaceAndTemplateId
(
    @FaceId INT,
    @TemplateId INT
)
AS
begin
SET NOCOUNT ON
SELECT 
	TemplateIcon.Stream 
FROM 
	[dbo].[TemplateIcon]
	inner join Template On TemplateIcon.TemplateId = Template.Id
WHERE

     Template.[FaceId] = @FaceId AND
    (Template.[Id] = @TemplateId OR (@TemplateId = 0))


end
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrTemplateIconGetByFaceAndTemplateId]  TO [dol_users]
GO

