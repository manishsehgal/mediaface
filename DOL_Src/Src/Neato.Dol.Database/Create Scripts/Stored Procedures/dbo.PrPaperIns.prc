SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPaperIns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrPaperIns]
GO

CREATE PROCEDURE dbo.PrPaperIns
(
    @Name NVARCHAR(64), 
    @PaperBrandId INT,
    @Width REAL,
    @Height REAL,
    @PaperType INT,
    @Scu NVARCHAR(256),
    @PaperState NCHAR(1),
    @Orientation NCHAR(1),
    @MetricId INT = NULL,
    @MinPaperAccessId INT,
    @PaperId INT OUTPUT
)
AS
BEGIN
    INSERT INTO dbo.Paper
    (
        [Name],
        BrandId,
        Width,
        Height,
        PaperType,
        Scu,
        State,
        Orientation,
        MetricId,
        MinPaperAccessId
    )
    VALUES
    (
        @Name,
        @PaperBrandId,
        @Width,
        @Height,
        @PaperType,
        @Scu,
        @PaperState,
        @Orientation,
        @MetricId,
        @MinPaperAccessId
    )
    SET @PaperId = SCOPE_IDENTITY()
    Update dbo.Paper set [Order] = @PaperId where [Id] = @PaperId
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrPaperIns]  TO [dol_users]
GO

