SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrBrandGetById]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrBrandGetById]
GO


CREATE PROCEDURE dbo.PrBrandGetById
    @BrandId INT
AS
SET NOCOUNT ON

SELECT TOP 1
    Id,
    [Name]
FROM dbo.DeviceBrand 
WHERE
    Id = @BrandId

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrBrandGetById]  TO [dol_users]
GO

  