@echo off
REM: Command File Created by Microsoft Visual Database Tools
REM: Date Generated: 16.11.2006
REM: Authentication type: SQL Server
REM: Usage: CommandFilename [Server] [Database] [Login] [Password]

if '%1' == '' goto usage
if '%2' == '' goto usage
if '%3' == '' goto usage

if '%1' == '/?' goto usage
if '%1' == '-?' goto usage
if '%1' == '?' goto usage
if '%1' == '/help' goto usage

osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrAnnouncementDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrAnnouncementEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrAnnouncementEnumLast.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrAnnouncementIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrAnnouncementUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrBrandGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarrierDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarrierGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarrierIconGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarrierIconUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarrierIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarriersEnumerate.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarrierUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCategoryDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCategoryEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCategoryDeviceEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCategoryGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCategoryIconGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCategoryIconUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCategoryIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCategoryUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerActivate.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerCalibrationDTCDGet.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerCalibrationDTCDSet.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerCalibrationGet.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerCalibrationSet.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerGetByEmail.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerSearch.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeadLinkDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeadLinkDelByUrl.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeadLinkEnumerate.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeadLinkGetReportByTime.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeadLinkIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceAddToCarrier.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceBrandDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceBrandEnumerate.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceBrandIconGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceBrandIconUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceBrandIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceBrandUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceEnumByPaper.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceEnumerate.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceIconBigGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceIconBigUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceIconGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceIconUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRemoveFromCarrier.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRequestDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRequestEmailsGetByDates.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRequestEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRequestEnumByCarrierPhoneManufacturerPhoneModel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRequestGetByDatesAndBrand.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRequestIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRequestUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceTypeGetByPaperId.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceTypeIconGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceTypesEnumerate.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDynamicLinkDelByPageId.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDynamicLinkDelByParam.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDynamicLinkEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDynamicLinkEnumByParam.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDynamicLinkIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDynamicLinkUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceBindToDevice.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceBindToPaper.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceEnumByParam.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceGetIdByGuid.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceIconGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceIconUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceLayoutDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceLayoutEnumByDeviceTypeIdDeviceIdFaceId.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceLayoutEnumById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceLayoutEnumByPaperId.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceLayoutIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceLayoutItemDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceLayoutItemEnumByFaceLayoutId.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceLayoutItemIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceLayoutItemUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceLayoutUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceLocalizationDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceLocalizationIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceUnbindFromDevice.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceUnbindFromPaper.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaqDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaqEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaqIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaqUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrGoogleCodeGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrGoogleCodeUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrGoogleTrackPageGetByPageUrl.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrGoogleTrackPageIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrGoogleTrackPageUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrHelpPageDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrHelpPageEnumByCulture.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrHelpPageIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrHelpPageUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrHiddenBannerDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrHiddenBannerGet.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrHiddenBannerIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibFolderDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibFolderGetIdByCaption.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibFolderIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibFolderLibUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibFolderLocalizationUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibFolderSortOrderSwap.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibItemDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibItemEnumByFolderId.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibItemGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibItemIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrIPFilterDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrIPFilterEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrIPFilterIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrIPFilterUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLastEditedPaperDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLastEditedPaperEnumByCustomer.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLastEditedPaperIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLastEditedPaperUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLayoutFaceToIconIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLayoutIconDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLayoutIconEnumById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLayoutIconGetByFaceAndLayoutId.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLayoutIconIdGetByFaceId.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLayoutIconIdGetByFaceLayoutId.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLayoutIconIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLayoutIconUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrMailFormatEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrMailFormatGetByCulture.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrMailFormatUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrOverviewPageDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrOverviewPageEnumByCulture.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrOverviewPageIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrOverviewPageUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPageEnumByUrl.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPageIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandDel.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandIconGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandIconUpd.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandIns.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandsEnumByDeviceTypeId.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandsEnumerate.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandUpd.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperEnumByParam.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperIconGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperIconUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperMetricEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperScuGet.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperTypeEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrParametersEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrParametersGetByKey.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrParametersUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailerDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailerEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailerGet.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailerIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailerUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSpecialUserDel.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSpecialUserEnumByParam.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSpecialUserGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSpecialUserIns.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSpecialUserUpd.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSupportPageDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSupportPageEnumByCulture.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSupportPageIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSupportPageUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTellAFriendRefusedUserGet.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTellAFriendRefusedUserIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTemplateDel.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTemplateEnumByDeviceTypeIdDeviceIdFaceId.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTemplateEnumByFaceId.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTemplateEnumById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTemplateGetMediaByTemplateAndGuid.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTemplateIconDel.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTemplateIconEnumById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTemplateIconGetByFaceAndTemplateId.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTemplateIconIns.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTemplateIconUpd.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTemplateIns.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTemplateMediaDelete.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTemplateMediaInsert.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTemplateUpd.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTermsOfUsePageEnumByCulture.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTermsOfUsePageIns.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTermsOfUsePageUpd.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTestImageLibItemIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingDevicesEnumBrands.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingDevicesEnumByTimeAndBrand.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingDevicesIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingFeaturesEnumByTime.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingFeaturesIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingImagesEnumByTime.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingImagesIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingPapersEnumByTime.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingPapersIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingShapesEnumByTime.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingShapesIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingUpcCodeIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingUpcCodesEnumByTime.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingUpcStapleCodeIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingUpcStapleCodesEnumByTime.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingUserActionEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingUserActionIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrUpcDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrUpcEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrUpcGet.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrUpcIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrUpcStapleDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrUpcStapleEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrUpcStapleGet.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrUpcStapleIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrUpcStapleUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrUpcUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors

goto finish

REM: How to use screen
:usage
echo.
echo Usage: MyScript Server Database User [Password]
echo Server: the name of the target SQL Server
echo Database: the name of the target database
echo User: the login name on the target server
echo Password: the password for the login on the target server (optional)
echo.
echo Example: MyScript.cmd MainServer MainDatabase MyName MyPassword
echo.
echo.
goto done

REM: error handler
:errors
echo.
echo WARNING! Error(s) were detected!
echo --------------------------------
echo Please evaluate the situation and, if needed,
echo restart this command file. You may need to
echo supply command parameters when executing
echo this command file.
echo.
pause
goto done

REM: finished execution
:finish
echo.
echo Script execution is complete!
:done
@echo on
