-- Create dbo.CustomerGroup table begin
BEGIN TRANSACTION

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CustomerGroup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE dbo.CustomerGroup
	(
	Id int NOT NULL IDENTITY (1, 1),
	Name nvarchar(50) NOT NULL
	)  ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_CustomerGroup') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
BEGIN
ALTER TABLE dbo.CustomerGroup ADD CONSTRAINT
	PK_CustomerGroup PRIMARY KEY CLUSTERED 
	(
	Id
	) ON [PRIMARY]
END
GO

COMMIT

BEGIN TRANSACTION

SET IDENTITY_INSERT [dbo].[CustomerGroup] ON

if (select count(*) from [dbo].[CustomerGroup] where Id=1) = 0
	insert into [dbo].[CustomerGroup] ([Id], [Name]) values (1, N'MFO')

if (select count(*) from [dbo].[CustomerGroup] where Id=2) = 0
	insert into [dbo].[CustomerGroup] ([Id], [Name]) values (2, N'MFOPE')

SET IDENTITY_INSERT [dbo].[CustomerGroup] OFF

COMMIT

-- Create dbo.CustomerGroup table end

-- Alter table dbo.Customer begin
BEGIN TRANSACTION

EXEC sp_columns @table_name = 'Customer', @table_owner='dbo', @column_name = 'GroupId'
IF @@ROWCOUNT = 0
CREATE TABLE dbo.TempTable (id int)

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TempTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
ALTER TABLE dbo.Customer ADD
	GroupId int NOT NULL CONSTRAINT DF_Customer_GroupId DEFAULT 1
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TempTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	if not exists (select * from dbo.sysobjects where id = object_id(N'FK_Customer_CustomerGroup') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
		ALTER TABLE dbo.Customer ADD CONSTRAINT
			FK_Customer_CustomerGroup FOREIGN KEY
			(
			GroupId
			) REFERENCES dbo.CustomerGroup
			(
			Id
			) ON UPDATE CASCADE
			ON DELETE CASCADE
			
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TempTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	DROP TABLE dbo.TempTable
	update dbo.Customer set GroupId = 2
	where Active = 1
END
GO
COMMIT
-- Alter table dbo.Customer end
 