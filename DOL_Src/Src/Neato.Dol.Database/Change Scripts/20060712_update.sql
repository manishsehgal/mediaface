/* Changes
- added tables  Template, TemplateMedia, TemplateIcon
*/

BEGIN TRANSACTION
-- Template begin
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Template]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Template] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[FaceId] [int] NOT NULL ,
	[Name] [char] (100) COLLATE Latin1_General_CS_AS NULL ,
	[Xml] [ntext] COLLATE Latin1_General_CS_AS NOT NULL 

) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TemplateIcon]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[TemplateIcon] (
	[TemplateId] [int] NOT NULL ,
	[Stream] [image] NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TemplateMedia]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[TemplateMedia] (
	[TemplateId] [int] NOT NULL ,
	[Guid] [uniqueidentifier] NOT NULL ,
	[Stream] [image] NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO


if not exists (select * from dbo.sysobjects where id = object_id(N'PK_Template') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
BEGIN
ALTER TABLE [dbo].[Template] WITH NOCHECK ADD 
	CONSTRAINT [PK_Template] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'FK_Template_Face') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
BEGIN
ALTER TABLE [dbo].[Template] WITH NOCHECK ADD 
	CONSTRAINT [FK_Template_Face] FOREIGN KEY 
	(
		[FaceId]
	) REFERENCES [dbo].[Face] (
		[Id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'FK_TemplateMedia_Template') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
BEGIN
ALTER TABLE [dbo].[TemplateMedia] WITH NOCHECK ADD 
	CONSTRAINT [FK_TemplateMedia_Template] FOREIGN KEY 
	(
		[TemplateId]
	) REFERENCES [dbo].[Template] (
		[Id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'FK_TemplateIcon_Template') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
BEGIN
	ALTER TABLE [dbo].[TemplateIcon] WITH NOCHECK ADD 
	CONSTRAINT [FK_TemplateIcon_Template] FOREIGN KEY 
	(
		[TemplateId]
	) REFERENCES [dbo].[Template] (
		[Id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
END
GO
-- Template end

COMMIT