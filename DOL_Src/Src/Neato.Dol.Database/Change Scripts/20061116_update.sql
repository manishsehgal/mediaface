BEGIN TRANSACTION
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Tracking_UpcStapleCodes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[Tracking_UpcStapleCodes] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Upc] [nvarchar] (100) COLLATE Latin1_General_BIN NOT NULL ,
	[Time] [datetime] NOT NULL ,
	[User] [nvarchar] (200) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_Tracking_UpcStapleCodes') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[Tracking_UpcStapleCodes] WITH NOCHECK ADD 
	CONSTRAINT [PK_Tracking_UpcStapleCodes] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UpcStaple]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[UpcStaple] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Code] [nvarchar] (100) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_UpcStaple') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[UpcStaple] WITH NOCHECK ADD 
	CONSTRAINT [PK_UpcStaple] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

COMMIT 