 /* Changes
- 1. add PaperAccess table
- 2. add to PaperAccess 3 rows (MFOPE, MFO, Unregistered)
- 3. add to table Paper field MinPaperAccessId (default=3)
- 4. update field Paper.MinPaperAccessId by business rules
- (MFOPE - all papers w/o watermark, MFO -  papers w/o watermark
- only if Brand=Neato AND !Lightscribe AND !DirectToCD)
*/

BEGIN TRANSACTION
--1
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PaperAccess]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
    CREATE TABLE [dbo].[PaperAccess] (
	    [Id] [int] IDENTITY (1, 1) NOT NULL ,
	    [Name] [nvarchar] (20) COLLATE Latin1_General_BIN NOT NULL 
    ) ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_PaperAccess') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
BEGIN
    ALTER TABLE [dbo].[PaperAccess] WITH NOCHECK ADD 
	    CONSTRAINT [PK_PaperAccess] PRIMARY KEY  CLUSTERED 
	    (
		    [Id]
	    )  ON [PRIMARY] 
END
GO

--2
if not exists (select * from [dbo].[PaperAccess])
BEGIN
    SET IDENTITY_INSERT PaperAccess ON
    INSERT INTO PaperAccess ([Id], [Name]) VALUES (1, 'Unregistered')
    INSERT INTO PaperAccess ([Id], [Name]) VALUES (2, 'MFO')
    INSERT INTO PaperAccess ([Id], [Name]) VALUES (3, 'MFOPE')
    SET IDENTITY_INSERT PaperAccess OFF
END
GO

--3
EXEC sp_columns @table_name = 'Paper', @table_owner='dbo', @column_name = 'MinPaperAccessId'
IF @@ROWCOUNT = 0
BEGIN
    ALTER TABLE dbo.Paper ADD [MinPaperAccessId] [int] NOT NULL
    CONSTRAINT [DF_Paper_MinPaperAccessId] DEFAULT (3)
END	
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'FK_Paper_PaperAccess') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
BEGIN
    ALTER TABLE dbo.Paper ADD CONSTRAINT
	    FK_Paper_PaperAccess FOREIGN KEY
	    (
		    [MinPaperAccessId]
	    ) REFERENCES [dbo].[PaperAccess] (
		    [Id]
	    )
END
GO

--4
UPDATE dbo.Paper
SET MinPaperAccessId = 2
WHERE dbo.Paper.Id > 2000 AND dbo.Paper.Id < 2500

GO

COMMIT