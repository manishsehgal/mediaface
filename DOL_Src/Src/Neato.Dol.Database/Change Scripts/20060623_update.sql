/* Changes
- add UPC table
*/

BEGIN TRANSACTION
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Upc]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Upc] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Code] [nvarchar] (100) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_Upc') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[Upc] WITH NOCHECK ADD 
	CONSTRAINT [PK_Upc] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

COMMIT

-- Paper table - Scu 
BEGIN TRANSACTION
EXEC sp_columns @table_name = 'Paper', @table_owner='dbo', @column_name = 'Scu'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Paper ADD
	Scu nvarchar(16) NULL
END
GO
COMMIT
