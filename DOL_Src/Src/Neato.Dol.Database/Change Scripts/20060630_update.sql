-- DeadLinks begin
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DeadLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[DeadLink] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Url] [text] COLLATE Latin1_General_BIN NOT NULL ,
	[Time] [datetime] NOT NULL ,
	[IP] [nvarchar] (20) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_DeadLink') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[DeadLink] WITH NOCHECK ADD 
	CONSTRAINT [PK_DeadLink] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO
-- DeadLinks end