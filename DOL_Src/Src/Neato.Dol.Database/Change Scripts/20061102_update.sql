BEGIN TRANSACTION
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Metric]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE dbo.TempTable (id int)
CREATE TABLE dbo.Metric
	(
	Id int NOT NULL IDENTITY (1, 1),
	Name nvarchar(50) NOT NULL
	)  ON [PRIMARY]
END
GO
if not exists (select * from dbo.sysobjects where id = object_id(N'PK_Metric') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE dbo.Metric ADD CONSTRAINT
	PK_Metric PRIMARY KEY CLUSTERED 
	(
	Id
	) ON [PRIMARY]

GO
COMMIT

BEGIN TRANSACTION
EXEC sp_columns @table_name = 'Paper', @table_owner='dbo', @column_name = 'MetricId'
IF @@ROWCOUNT = 0
ALTER TABLE dbo.Paper ADD
	MetricId int NULL,
	[Order] int NOT NULL CONSTRAINT DF_Paper_Order DEFAULT 1
GO
COMMIT

BEGIN TRANSACTION
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TempTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	SET IDENTITY_INSERT [dbo].[Metric] ON
	insert into [dbo].[Metric] ([Id], [Name]) values (1, N'US')
	insert into [dbo].[Metric] ([Id], [Name]) values (2, N'UK')
	SET IDENTITY_INSERT [dbo].[Metric] OFF
END
GO
COMMIT

BEGIN TRANSACTION
if not exists (select * from dbo.sysobjects where id = object_id(N'FK_Paper_Metric') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.Paper ADD CONSTRAINT
	FK_Paper_Metric FOREIGN KEY
	(
	MetricId
	) REFERENCES dbo.Metric
	(
	Id
	) ON UPDATE CASCADE
	 ON DELETE CASCADE
	
GO
COMMIT

BEGIN TRANSACTION
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TempTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	DROP TABLE dbo.TempTable
	update [Paper] set [Paper].[Order] = [Paper].[Id]
	update [dbo].[Paper] set [MetricId] = 1 where CHARINDEX('US', Name) =  1 and Id > 2000 and Id < 2500
	update [dbo].[Paper] set [MetricId] = 2 where CHARINDEX('US', Name) <> 1 and Id > 2000 and Id < 2500
END
GO
COMMIT

-- Tracking UPC Codes
BEGIN TRANSACTION
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Tracking_UpcCodes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE dbo.Tracking_UpcCodes
	(
	Id int NOT NULL IDENTITY (1, 1),
	UpcId int NOT NULL,
	[Time] datetime NOT NULL,
	[User] nvarchar(200) NOT NULL
	)  ON [PRIMARY]
GO
if not exists (select * from dbo.sysobjects where id = object_id(N'PK_Tracking_UpcCodes') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE dbo.Tracking_UpcCodes ADD CONSTRAINT
	PK_Tracking_UpcCodes PRIMARY KEY CLUSTERED 
	(
	Id
	) ON [PRIMARY]

GO
if not exists (select * from dbo.sysobjects where id = object_id(N'FK_Tracking_UpcCodes_Upc') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.Tracking_UpcCodes ADD CONSTRAINT
	FK_Tracking_UpcCodes_Upc FOREIGN KEY
	(
	UpcId
	) REFERENCES dbo.Upc
	(
	Id
	) ON UPDATE CASCADE
	 ON DELETE CASCADE
	
GO
COMMIT