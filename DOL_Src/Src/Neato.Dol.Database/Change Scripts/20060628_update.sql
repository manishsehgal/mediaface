
/* Changes
- add column "State" in table "Paper"
- add column "Retailer" in table "Customer"
- add special user table
*/

BEGIN TRANSACTION

EXEC sp_columns @table_name = 'Paper', @table_owner='dbo', @column_name = 'State'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Paper ADD
	State nchar(1) COLLATE Latin1_General_BIN NOT NULL DEFAULT ('a')
END
GO

--Add field
--Add field
EXEC sp_columns @table_name = 'Customer', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
ALTER TABLE dbo.Customer ADD
	[RetailerId] [int] NOT NULL CONSTRAINT [DF_Customer_RetailerId] DEFAULT (1)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Customer_Retailer]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Customer] DROP CONSTRAINT FK_Customer_Retailer
GO

ALTER TABLE [dbo].[Customer] ADD 
	CONSTRAINT [FK_Customer_Retailer] FOREIGN KEY 
	(
		[RetailerId]
	) REFERENCES [dbo].[Retailer] (
		[Id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO


if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SpecialUser]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[SpecialUser] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Email] [varchar] (100) COLLATE Latin1_General_BIN NOT NULL DEFAULT (''),
	[ShowPaper] [bit] NOT NULL DEFAULT (0),
	[NotifyPaperState] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
END
GO

-- Add field to 'SpecialUser' table
EXEC sp_columns @table_name = 'SpecialUser', @table_owner='dbo', @column_name = 'NotifyBanner'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.SpecialUser ADD
	NotifyBanner bit NOT NULL DEFAULT (0)
END
GO

COMMIT 


-- Device Type begin
BEGIN TRANSACTION

UPDATE DeviceType set [Name]='CdDvdLabel' WHERE [Id]=1
UPDATE DeviceType set [Name]='Insert' WHERE [Id]=2
UPDATE DeviceType set [Name]='MP3Player' WHERE [Id]=3
UPDATE DeviceType set [Name]='DirectToCD' WHERE [Id]=4
UPDATE DeviceType set [Name]='Lightscribe' WHERE [Id]=5

COMMIT
-- Device Type end

