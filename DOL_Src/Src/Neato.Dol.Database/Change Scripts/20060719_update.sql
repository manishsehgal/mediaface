/*
Add column Referrer to DeadLink

Add colums Visible and Target to DeviceType (Categories)

Expand SCU in Paper and set SKU for MF papers


*/

-- Add column Referrer to DeadLink

BEGIN TRANSACTION

EXEC sp_columns @table_name = 'DeadLink', @table_owner='dbo', @column_name = 'Referrer'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.DeadLink ADD
	Referrer [nvarchar] (255) COLLATE Latin1_General_BIN 
END
GO

COMMIT

-- Add colums Visible and Target to DeviceType (Categories)

BEGIN TRANSACTION

EXEC sp_columns @table_name = 'DeviceType', @table_owner='dbo', @column_name = 'Visible'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.DeviceType ADD
	Visible [bit] NOT NULL DEFAULT (1),
	Target [int] NOT NULL DEFAULT (1)
END
GO


-- Expand SCU in Paper

ALTER TABLE dbo.Paper ALTER COLUMN Scu nvarchar(256)

COMMIT TRANSACTION


-- and set SKU for MF papers

UPDATE dbo.Paper SET Scu = '181441, 181661, 181443, 180741' WHERE Id = 2041
UPDATE dbo.Paper SET Scu = '181450, 181460, 181453, 180750' WHERE Id = 2059
UPDATE dbo.Paper SET Scu = '181570' WHERE Id = 2075
UPDATE dbo.Paper SET Scu = '180817, 180792' WHERE Id = 2101
UPDATE dbo.Paper SET Scu = '863517,863516' WHERE Id = 2002
UPDATE dbo.Paper SET Scu = '180815, 180794, 180798' WHERE Id = 2039
UPDATE dbo.Paper SET Scu = '181375' WHERE Id = 2079
UPDATE dbo.Paper SET Scu = '863100, 181585, 863105' WHERE Id = 2001
UPDATE dbo.Paper SET Scu = '181441, 181661, 181443, 180741' WHERE Id = 2076
UPDATE dbo.Paper SET Scu = '181406' WHERE Id = 2048
UPDATE dbo.Paper SET Scu = '180935, 181600, 180955' WHERE Id = 2019
UPDATE dbo.Paper SET Scu = '181170' WHERE Id = 2036
UPDATE dbo.Paper SET Scu = '181503, 181505, 181509' WHERE Id = 2018
UPDATE dbo.Paper SET Scu = '' WHERE Id = 2049
UPDATE dbo.Paper SET Scu = '181511, 181513, 181518' WHERE Id = 2047
UPDATE dbo.Paper SET Scu = '181601, 180930' WHERE Id = 2083
UPDATE dbo.Paper SET Scu = '' WHERE Id = 2031
UPDATE dbo.Paper SET Scu = '180945, 181610, 180985' WHERE Id = 2087
UPDATE dbo.Paper SET Scu = '180312' WHERE Id = 2060
UPDATE dbo.Paper SET Scu = '181516' WHERE Id = 2022
UPDATE dbo.Paper SET Scu = '180980, 180984' WHERE Id = 2034
UPDATE dbo.Paper SET Scu = '' WHERE Id = 2046
GO