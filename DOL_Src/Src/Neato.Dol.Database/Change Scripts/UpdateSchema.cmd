@echo off
REM: Command File Created by Microsoft Visual Database Tools
REM: Date Generated: 16.08.2005
REM: Authentication type: SQL Server
REM: Usage: CommandFilename [Server] [Database] [Login] [Password]

if '%1' == '' goto usage
if '%2' == '' goto usage
if '%3' == '' goto usage

if '%1' == '/?' goto usage
if '%1' == '-?' goto usage
if '%1' == '?' goto usage
if '%1' == '/help' goto usage

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060418_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060420_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060426_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060502_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060503_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060510_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060529_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060530_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060614_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060623_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060626_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060628_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060630_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060704_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060706_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060712_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060713_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060717_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060719_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060721_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060726_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060802_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060803_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060814_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060818_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060821_update.sql"
if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060822_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060823_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060831_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060905_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060907_update.sql"
if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060913_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20061030_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20061102_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20061103_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20061116_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20070305_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

osql -S %1 -d %2 -U %3 -P %4 -b -i "20070309_update.sql"
if %ERRORLEVEL% NEQ 0 goto errors

goto finish

REM: How to use screen
:usage
echo.
echo Usage: MyScript Server Database User [Password]
echo Server: the name of the target SQL Server
echo Database: the name of the target database
echo User: the login name on the target server
echo Password: the password for the login on the target server (optional)
echo.
echo Example: MyScript.cmd MainServer MainDatabase MyName MyPassword
echo.
echo.
goto done

REM: error handler
:errors
echo.
echo WARNING! Error(s) were detected!
echo --------------------------------
echo Please evaluate the situation and, if needed,
echo restart this command file. You may need to
echo supply command parameters when executing
echo this command file.
echo.
pause
goto done

REM: finished execution
:finish
echo.
echo Script execution is complete!
:done
@echo on
