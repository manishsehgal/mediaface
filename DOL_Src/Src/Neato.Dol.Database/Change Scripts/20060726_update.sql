BEGIN TRANSACTION

EXEC sp_columns @table_name = 'DeviceType', @table_owner='dbo', @column_name = 'SortOrder'
IF @@ROWCOUNT = 0
ALTER TABLE [DeviceType] ADD [SortOrder] INT CONSTRAINT [DF_DeviceType_SortOrder] DEFAULT (1) 
GO

EXEC sp_columns @table_name = 'DeviceType', @table_owner='dbo', @column_name = 'SortOrder'
IF @@ROWCOUNT = 1
UPDATE DeviceType SET SortOrder = 1 WHERE SortOrder IS NULL AND [Id] = 1
UPDATE DeviceType SET SortOrder = 2 WHERE SortOrder IS NULL AND [Id] = 2
UPDATE DeviceType SET SortOrder = 3 WHERE SortOrder IS NULL AND [Id] = 3
UPDATE DeviceType SET SortOrder = 4 WHERE SortOrder IS NULL AND [Id] = 4
UPDATE DeviceType SET SortOrder = 5 WHERE SortOrder IS NULL AND [Id] = 5
UPDATE DeviceType SET SortOrder = 6 WHERE SortOrder IS NULL AND [Id] = 6

UPDATE DeviceType SET SortOrder = 1 WHERE SortOrder IS NULL

GO

EXEC sp_columns @table_name = 'DeviceType', @table_owner='dbo', @column_name = 'SortOrder'
IF @@ROWCOUNT = 1
ALTER TABLE [DeviceType] ALTER COLUMN [SortOrder] INT NOT NULL


COMMIT TRANSACTION 