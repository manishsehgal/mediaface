/* Changes
- add column "Active" in table "Customer" (Active=true by default)
*/

BEGIN TRANSACTION

EXEC sp_columns @table_name = 'Customer', @table_owner='dbo', @column_name = 'Active'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Customer ADD
	Active [bit] NOT NULL DEFAULT (1)
END
GO

COMMIT