BEGIN TRANSACTION
EXEC sp_columns @table_name = 'Customer', @table_owner='dbo', @column_name = 'XCalibrationDTCD'
IF @@ROWCOUNT = 0
BEGIN
	ALTER TABLE dbo.Customer ADD
		XCalibrationDTCD float(53) NULL,
		YCalibrationDTCD float(53) NULL
END
GO

EXEC sp_columns @table_name = 'FaceLayoutItem', @table_owner='dbo', @column_name = 'ScaleX'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.FaceLayoutItem ADD
	[ScaleX] [real] NOT NULL DEFAULT(100),
	[ScaleY] [real] NOT NULL DEFAULT(100)
END
GO

COMMIT
GO
