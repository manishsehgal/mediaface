BEGIN TRANSACTION
EXEC sp_columns @table_name = 'Tracking_UpcCodes', @table_owner='dbo', @column_name = 'UpcId'
IF @@ROWCOUNT > 0
CREATE TABLE dbo.TempTable (id int)

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TempTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Tracking_UpcCodes_Upc]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
		ALTER TABLE dbo.Tracking_UpcCodes
			DROP CONSTRAINT FK_Tracking_UpcCodes_Upc
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TempTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Tracking_UpcCodes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
		drop table [dbo].[Tracking_UpcCodes]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TempTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Tracking_UpcCodes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
		CREATE TABLE [dbo].[Tracking_UpcCodes] (
			[Id] [int] IDENTITY (1, 1) NOT NULL ,
			[Upc] [nvarchar] (100) COLLATE Latin1_General_BIN NOT NULL ,
			[Time] [datetime] NOT NULL ,
			[User] [nvarchar] (200) COLLATE Latin1_General_BIN NOT NULL 
		) ON [PRIMARY]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TempTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	if not exists (select * from dbo.sysobjects where id = object_id(N'PK_Tracking_UpcCodes') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
		ALTER TABLE [dbo].[Tracking_UpcCodes] WITH NOCHECK ADD 
			CONSTRAINT [PK_Tracking_UpcCodes] PRIMARY KEY  CLUSTERED 
			(
				[Id]
			)  ON [PRIMARY] 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TempTable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.TempTable
COMMIT

BEGIN TRANSACTION
EXEC sp_columns @table_name = 'ImageLibFolder', @table_owner='dbo', @column_name = 'LibId'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.ImageLibFolder ADD
	LibId [INT] NOT NULL CONSTRAINT [DF_ImageLibFolder_LibId] DEFAULT (1)
END
GO
COMMIT


BEGIN TRANSACTION
EXEC sp_columns @table_name = 'Tracking_Papers', @table_owner='dbo', @column_name = 'Brand'
IF @@ROWCOUNT = 0
ALTER TABLE dbo.Tracking_Papers ADD
	Brand nvarchar(50) NOT NULL CONSTRAINT DF_Tracking_Papers_Brand DEFAULT N'Neato'
GO
COMMIT
