/* Changes
 - added to units for layout ver
 - issue 358 FIX
*/

BEGIN TRANSACTION
DECLARE @ImgId int
if (select count(*) from dbo.FaceLayoutItem where FaceLayoutId = 167) = 3 
BEGIN
INSERT INTO [dbo].[FaceLayoutItem] (/*[Id],*/ [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align],[UnitType],[EffectNode])  VALUES (/*SCOPE_IDENTITY(),*/167, 428.0, 25.0, 4,'Title', 'Arial', 14, 0, 0, 0, 90, 5.9375, 116.9291, 'left','MultilineTextUnit','')
INSERT INTO [dbo].[FaceLayoutItem] (/*[Id],*/ [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align],[UnitType],[EffectNode])  VALUES (/*SCOPE_IDENTITY(),*/167, 428.0, 165.1, 5,'SubTitle', 'Arial', 14, 0, 0, 0, 90, 7.5, 172.7362, 'left','MultilineTextUnit','')

END

UPDATE [dbo].[FaceLayout] SET FaceId = 1039 WHERE FaceId = 1027 AND (Id > 106 AND Id < 111)
UPDATE [dbo].[LayoutFaceToIcon] SET FaceId = 1039 WHERE FaceId = 1027 AND (LayoutId > 106 AND LayoutId < 111)


if not exists (SELECT * FROM dbo.FaceLayout WHERE FaceId = 1057 OR FaceId = 1057)
begin
DELETE FROM [dbo].[FaceLayout] WHERE Id > 296
SET IDENTITY_INSERT [dbo].[FaceLayout] ON
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId], [Name]) VALUES (297, 1058, N'HandyCard Label70')
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId], [Name]) VALUES (298, 1058, N'HandyCard Label71')
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId], [Name]) VALUES (299, 1058, N'HandyCard Label72')
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId], [Name]) VALUES (300, 1058, N'HandyCard Label73')

INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId], [Name]) VALUES (301, 1057, N'DVD Case Booklet (outside)131')
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId], [Name]) VALUES (302, 1057, N'DVD Case Booklet (outside)132')
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId], [Name]) VALUES (303, 1057, N'DVD Case Booklet (outside)133')
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId], [Name]) VALUES (304, 1057, N'DVD Case Booklet (outside)134')

SET IDENTITY_INSERT [dbo].[FaceLayout] OFF
SET IDENTITY_INSERT [dbo].[FaceLayoutItem] ON
DELETE FROM [dbo].[FaceLayoutItem] WHERE Id > 709
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (710, 297, 49.96062, 8.149606, 0, N'Title', N'Arial', 16, 0, 0, 0, 0, 5.9375, 137.3031, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (711, 297, 30.47243, 146.3386, 1, N'Subtitle', N'Arial', 14, 0, 0, 0, 0, 6.5625, 170.9646, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (712, 297, 12.40156, 55.80709, 3, N'PlayList', N'Arial', 12, 0, 0, 0, 0, 25.3125, 206.3976, 'left', 'TableUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (713, 298, 116.0433, 27.46063, 3, N'PlayList', N'Arial', 12, 0, 0, 0, 0, 45.9375, 98.32677, 'left', 'TableUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (714, 298, 31.99093, 159.449, 0, N'Title', N'Arial', 16, 0, 0, 0, -90, 5.9375, 137.3031, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (715, 298, 66.15856, 178.8109, 1, N'Subtitle', N'Arial', 14, 0, 0, 0, -90, 6.5625, 170.9646, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (716, 299, 48.18896, 8.149606, 0, N'Title', N'Arial', 18, 0, 0, 0, 0, 8.580729, 137.3031, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (717, 299, 22.49999, 35.67674, 1, N'SubTitle', N'Arial', 14, 0, 0, 0, 0, 9.063802, 191.3386, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (718, 299, 14.17322, 113.3858, 3, N'PlayList', N'Arial', 11, 0, 0, 0, 0, 23.75, 204.626, 'center', 'TableUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (719, 300, -2.303753, 180.0913, 0, N'Title', N'Arial', 18, 0, 0, 0, -90, 5.9375, 137.3031, 'center', 'MultilineTextUnit', N'<TextEffect name="Fit" type="Guided" subtype="Curve4"><Geometry><RefPoint id="0" x="0" y="55.89676"/><RefPoint id="1" x="27.58963" y="3.941382"/><RefPoint id="2" x="125.7474" y="0"/><RefPoint id="3" x="158.562" y="55.01096"/></Geometry><Settings><Angle value="0" /> <Font size="12" align="left" scaled="true" /> </Settings><Icon><Rectangle x="0" y="40" w="100" h="20" /> </Icon></TextEffect>', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (720, 300, 213.8357, -73.88327, 1, N'SubTitle', N'Arial', 14, 0, 0, 0, 90, 12.5, 191.3386, 'center', 'MultilineTextUnit', N'<TextEffect name="Fit" type="Guided" subtype="Curve4"><Geometry><RefPoint id="0" x="25.68898" y="58.117"/><RefPoint id="1" x="59.60822" y="5.853188"/><RefPoint id="2" x="144.3029" y="0"/><RefPoint id="3" x="189.3512" y="57.14143"/></Geometry><Settings><Angle value="0" /> <Font size="12" align="left" scaled="true" /> </Settings><Icon><Rectangle x="0" y="40" w="100" h="20" /> </Icon></TextEffect>', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (721, 300, 53.1496, 113.3858, 3, N'PlayList', N'Arial', 9, 0, 0, 0, 0, 23.75, 123.1299, 'center', 'TableUnit', N'', 100, 100)

INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (722, 301, 9.537213, 9.048393, 0, N'Title', N'Arial', 36, 0, 0, 0, 0, 17.5, 335.6378, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (723, 301, 6.865502, 63.37347, 1, N'SubTitle', N'Arial', 28, 0, 0, 0, 0, 23.87545, 335.6378, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (724, 301, 22.13533, 134.3638, 3, N'PlayList', N'Arial', 18, 0, 0, 0, 0, 127.0187, 312.7874, 'left', 'TableUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (725, 302, 367.4112, 161.4106, 0, N'Title', N'Arial', 36, 0, 0, 0, 0, 31.875, 335.6378, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (726, 302, 368.2828, 268.8853, 1, N'SubTitle', N'Arial', 28, 0, 0, 0, 0, 26.37545, 335.6378, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (727, 302, 23.90695, 20.97796, 3, N'PlayList', N'Arial', 18, 0, 0, 0, 0, 171.3937, 312.7874, 'left', 'TableUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (728, 303, 673.3864, 14.85254, 0, N'Title', N'Arial', 48, 0, 0, 0, 90, 35.00002, 502.173, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (729, 303, 544.6535, 10.40883, 1, N'SubTitle', N'Arial', 28, 0, 0, 0, 90, 28.87547, 509.2596, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (730, 303, 17.66797, 504.5712, 3, N'PlayList', N'Arial', 22, 0, 0, 0, -90, 111.3937, 477.5506, 'left', 'TableUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (731, 304, 367.8015, 138.4952, 0, N'Title', N'Arial', 36, 0, 0, 0, 0, 31.875, 335.6378, 'center', 'MultilineTextUnit', N'<TextEffect name="Fit" type="Curved" subtype="BottomArc"><Geometry><RefPoint id="0" x="0" y="0"/><RefPoint id="1" x="335.6378" y="0"/><RefPoint id="2" x="24.80315" y="88.58268"/><RefPoint id="3" x="167.8189" y="57.13583"/><RefPoint id="4" x="309.063" y="92.12598"/></Geometry><Settings><Angle value="0" /> <Font size="12" align="left" scaled="true" /> </Settings><Icon><Rectangle x="0" y="40" w="100" h="20" /> </Icon></TextEffect>', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (732, 304, 370.0544, 288.3735, 1, N'SubTitle', N'Arial', 28, 0, 0, 0, 0, 26.37545, 335.6378, 'center', 'MultilineTextUnit', N'<TextEffect name="Fit" type="Curved" subtype="TopArc"><Geometry><RefPoint id="0" x="26.5748" y="3.543312"/><RefPoint id="1" x="167.8189" y="25.77789"/><RefPoint id="2" x="317.9213" y="0"/><RefPoint id="3" x="0" y="72.99342"/><RefPoint id="4" x="335.6378" y="72.99342"/></Geometry><Settings><Angle value="0" /> <Font size="12" align="left" scaled="true" /> </Settings><Icon><Rectangle x="0" y="40" w="100" h="20" /> </Icon></TextEffect>', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (733, 304, 11.50536, 22.74961, 3, N'PlayList', N'Arial', 18, 0, 0, 0, 0, 171.3937, 312.7874, 'left', 'TableUnit', N'', 100, 100)
SET IDENTITY_INSERT [dbo].[FaceLayoutItem] OFF
SET @ImgId = (SELECT ImageId FROM [dbo].[LayoutFaceToIcon] WHERE FaceId = 1013 AND LayoutId = 70)
INSERT INTO [dbo].[LayoutFaceToIcon] ([FaceId], [LayoutId], [ImageId]) VALUES (1058, 297, @ImgId)
SET @ImgId = (SELECT ImageId FROM [dbo].[LayoutFaceToIcon] WHERE FaceId = 1013 AND LayoutId = 71)
INSERT INTO [dbo].[LayoutFaceToIcon] ([FaceId], [LayoutId], [ImageId]) VALUES (1058, 298, @ImgId)
SET @ImgId = (SELECT ImageId FROM [dbo].[LayoutFaceToIcon] WHERE FaceId = 1013 AND LayoutId = 72)
INSERT INTO [dbo].[LayoutFaceToIcon] ([FaceId], [LayoutId], [ImageId]) VALUES (1058, 299, @ImgId)
SET @ImgId = (SELECT ImageId FROM [dbo].[LayoutFaceToIcon] WHERE FaceId = 1013 AND LayoutId = 73)
INSERT INTO [dbo].[LayoutFaceToIcon] ([FaceId], [LayoutId], [ImageId]) VALUES (1058, 300, @ImgId)
SET @ImgId = (SELECT ImageId FROM [dbo].[LayoutFaceToIcon] WHERE FaceId = 1013 AND LayoutId IS NULL)
INSERT INTO [dbo].[LayoutFaceToIcon] ([FaceId], [LayoutId], [ImageId]) VALUES (1058, NULL,@ImgId)

SET @ImgId = (SELECT ImageId FROM [dbo].[LayoutFaceToIcon] WHERE FaceId = 1056 AND LayoutId = 131)
INSERT INTO [dbo].[LayoutFaceToIcon] ([FaceId], [LayoutId], [ImageId]) VALUES (1057, 301, @ImgId)
SET @ImgId = (SELECT ImageId FROM [dbo].[LayoutFaceToIcon] WHERE FaceId = 1056 AND LayoutId = 132)
INSERT INTO [dbo].[LayoutFaceToIcon] ([FaceId], [LayoutId], [ImageId]) VALUES (1057, 302, @ImgId)
SET @ImgId = (SELECT ImageId FROM [dbo].[LayoutFaceToIcon] WHERE FaceId = 1056 AND LayoutId = 133)
INSERT INTO [dbo].[LayoutFaceToIcon] ([FaceId], [LayoutId], [ImageId]) VALUES (1057, 303, @ImgId)
SET @ImgId = (SELECT ImageId FROM [dbo].[LayoutFaceToIcon] WHERE FaceId = 1056 AND LayoutId = 134)
INSERT INTO [dbo].[LayoutFaceToIcon] ([FaceId], [LayoutId], [ImageId]) VALUES (1057, 304, @ImgId)
SET @ImgId = (SELECT ImageId FROM [dbo].[LayoutFaceToIcon] WHERE FaceId = 1056 AND LayoutId IS NULL)
INSERT INTO [dbo].[LayoutFaceToIcon] ([FaceId], [LayoutId], [ImageId]) VALUES (1057, NULL, @ImgId)


end

if not exists(SELECT * FROM [dbo].[FaceLayoutItem] WHERE FaceLayoutId > 106 AND FaceLayoutId < 111)
begin
SET IDENTITY_INSERT [dbo].[FaceLayoutItem] ON
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (158, 107, 27.28338, 37.20473, 0, N'Title', N'Arial', 20, 0, 0, 0, 0, 19.375, 295.8661, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (159, 107, 29.05503, 157.6772, 1, N'SubTitle', N'Arial', 16, 0, 0, 0, 0, 48.75, 295.8661, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (160, 107, 332.4629, 688.1609, 2, N'TEXT TEXT TEXT TEXT', N'Arial', 14, 0, 0, 0, -180, 110, 304.7244, 'left', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (161, 108, 16.14716, 325.6809, 0, N'Title', N'Arial', 20, 0, 0, 0, -90, 19.375, 295.8661, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (162, 108, 110.298, 327.2003, 1, N'SubTitle', N'Arial', 16, 0, 0, 0, -90, 48.75, 295.8661, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (163, 108, 332.4629, 688.1609, 3, N'PlayList', N'Arial', 14, 0, 0, 0, -180, 110, 304.7244, 'left', 'TableUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (164, 109, 16.14716, 325.6809, 0, N'Title', N'Arial', 20, 0, 0, 0, -90, 19.375, 295.8661, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (165, 109, 110.298, 327.2003, 1, N'SubTitle', N'Arial', 16, 0, 0, 0, -90, 48.75, 295.8661, 'center', 'MultilineTextUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (166, 109, 19.68998, 689.4277, 3, N'PlayList', N'Arial', 14, 0, 0, 0, -90, 110, 304.7244, 'left', 'TableUnit', N'', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (167, 110, 10.87008, 35.68979, 0, N'Title', N'Arial', 20, 0, 0, 0, 0, 19.375, 295.8661, 'center', 'MultilineTextUnit', N'<TextEffect name="Fit" type="Curved" subtype="Balloon"><Geometry><RefPoint id="0" x="0" y="30.48291"/><RefPoint id="1" x="164.2143" y="0"/><RefPoint id="2" x="328.4287" y="30.48291"/><RefPoint id="3" x="0" y="60.96581"/><RefPoint id="4" x="164.2143" y="30.48291"/><RefPoint id="5" x="328.4287" y="60.96581"/></Geometry><Settings><Angle value="0" /> <Font size="12" align="left" scaled="true" /> </Settings><Icon><Rectangle x="0" y="40" w="100" h="20" /> </Icon></TextEffect>', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (168, 110, 11.17934, 116.3249, 1, N'SubTitle', N'Arial', 16, 0, 0, 0, 0, 48.75, 295.8661, 'center', 'MultilineTextUnit', N'<TextEffect name="Fit" type="Curved" subtype="Balloon"><Geometry><RefPoint id="0" x="0" y="0"/><RefPoint id="1" x="164.9641" y="90.94271"/><RefPoint id="2" x="328.4288" y="0"/><RefPoint id="3" x="0" y="76.69896"/><RefPoint id="4" x="163.4647" y="150.3992"/><RefPoint id="5" x="328.4288" y="76.69896"/></Geometry><Settings><Angle value="0" /> <Font size="12" align="left" scaled="true" /> </Settings><Icon><Rectangle x="0" y="40" w="100" h="20" /> </Icon></TextEffect>', 100, 100)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align], [UnitType], [EffectNode], [ScaleX], [ScaleY]) VALUES (169, 110, 332.4629, 688.1609, 3, N'PlayList', N'Arial', 14, 0, 0, 0, -180, 110, 304.7244, 'left', 'TableUnit', N'', 100, 100)
SET IDENTITY_INSERT [dbo].[FaceLayoutItem] OFF
End


COMMIT 