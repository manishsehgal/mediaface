-- Name column (FaceLayout table)
BEGIN TRANSACTION
CREATE TABLE #t(x BIT)

EXEC sp_columns @table_name = 'FaceLayout', @table_owner='dbo', @column_name = 'Name'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.FaceLayout ADD
	Name nvarchar(50) NULL

INSERT INTO #t VALUES (1)
END
GO

DECLARE @rows INT
SELECT @rows=count(*) FROM #t

IF @rows > 0
BEGIN

UPDATE [dbo].[FaceLayout] SET [Name] = N''

ALTER TABLE dbo.FaceLayout
    ALTER COLUMN Name nvarchar(50) NOT NULL
END

DROP TABLE #t


COMMIT




BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
if exists (select * from dbo.sysobjects where id = object_id(N'FK_FaceLayoutItem_FaceLayout') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.FaceLayoutItem
	DROP CONSTRAINT FK_FaceLayoutItem_FaceLayout
GO
COMMIT
BEGIN TRANSACTION
ALTER TABLE dbo.FaceLayoutItem
	DROP CONSTRAINT DF_FaceLayoutItem_X
GO
ALTER TABLE dbo.FaceLayoutItem
	DROP CONSTRAINT DF_FaceLayoutItem_Y
GO
ALTER TABLE dbo.FaceLayoutItem
	DROP CONSTRAINT DF_FaceLayoutItem_Position
GO
ALTER TABLE dbo.FaceLayoutItem
	DROP CONSTRAINT DF_FaceLayoutItem_Text
GO
ALTER TABLE dbo.FaceLayoutItem
	DROP CONSTRAINT DF_FaceLayoutItem_Font
GO
ALTER TABLE dbo.FaceLayoutItem
	DROP CONSTRAINT DF_FaceLayoutItem_Size2
GO
ALTER TABLE dbo.FaceLayoutItem
	DROP CONSTRAINT DF_FaceLayoutItem_Bold
GO
ALTER TABLE dbo.FaceLayoutItem
	DROP CONSTRAINT DF_FaceLayoutItem_Italic
GO
ALTER TABLE dbo.FaceLayoutItem
	DROP CONSTRAINT DF_FaceLayoutItem_Color
GO
ALTER TABLE dbo.FaceLayoutItem
	DROP CONSTRAINT DF_FaceLayoutItem_Angle
GO
ALTER TABLE dbo.FaceLayoutItem
	DROP CONSTRAINT DF_FaceLayoutItem_Height
GO
ALTER TABLE dbo.FaceLayoutItem
	DROP CONSTRAINT DF_FaceLayoutItem_Width
GO
ALTER TABLE dbo.FaceLayoutItem
	DROP CONSTRAINT DF_FaceLayoutItem_Align
GO
ALTER TABLE dbo.FaceLayoutItem
	DROP CONSTRAINT DF_FaceLayoutItem_UnitType
GO
CREATE TABLE dbo.Tmp_FaceLayoutItem
	(
	Id int NOT NULL IDENTITY (1, 1),
	FaceLayoutId int NOT NULL,
	X real NOT NULL,
	Y real NOT NULL,
	[Position] tinyint NOT NULL,
	Text nvarchar(50) NOT NULL,
	Font nvarchar(50) NOT NULL,
	[Size] tinyint NOT NULL,
	Bold bit NOT NULL,
	Italic bit NOT NULL,
	Color int NOT NULL,
	Angle real NOT NULL,
	Height real NOT NULL,
	Width real NOT NULL,
	Align varchar(10) NOT NULL,
	UnitType varchar(50) COLLATE Latin1_General_CS_AS NOT NULL,
	EffectNode ntext COLLATE Latin1_General_CS_AS NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_FaceLayoutItem ADD CONSTRAINT
	DF_FaceLayoutItem_X DEFAULT (0) FOR X
GO
ALTER TABLE dbo.Tmp_FaceLayoutItem ADD CONSTRAINT
	DF_FaceLayoutItem_Y DEFAULT (0) FOR Y
GO
ALTER TABLE dbo.Tmp_FaceLayoutItem ADD CONSTRAINT
	DF_FaceLayoutItem_Position DEFAULT (0) FOR [Position]
GO
ALTER TABLE dbo.Tmp_FaceLayoutItem ADD CONSTRAINT
	DF_FaceLayoutItem_Text DEFAULT (N'Enter text') FOR Text
GO
ALTER TABLE dbo.Tmp_FaceLayoutItem ADD CONSTRAINT
	DF_FaceLayoutItem_Font DEFAULT (N'Arial') FOR Font
GO
ALTER TABLE dbo.Tmp_FaceLayoutItem ADD CONSTRAINT
	DF_FaceLayoutItem_Size2 DEFAULT (16) FOR [Size]
GO
ALTER TABLE dbo.Tmp_FaceLayoutItem ADD CONSTRAINT
	DF_FaceLayoutItem_Bold DEFAULT (0) FOR Bold
GO
ALTER TABLE dbo.Tmp_FaceLayoutItem ADD CONSTRAINT
	DF_FaceLayoutItem_Italic DEFAULT (0) FOR Italic
GO
ALTER TABLE dbo.Tmp_FaceLayoutItem ADD CONSTRAINT
	DF_FaceLayoutItem_Color DEFAULT (0) FOR Color
GO
ALTER TABLE dbo.Tmp_FaceLayoutItem ADD CONSTRAINT
	DF_FaceLayoutItem_Angle DEFAULT (0) FOR Angle
GO
ALTER TABLE dbo.Tmp_FaceLayoutItem ADD CONSTRAINT
	DF_FaceLayoutItem_Height DEFAULT (17) FOR Height
GO
ALTER TABLE dbo.Tmp_FaceLayoutItem ADD CONSTRAINT
	DF_FaceLayoutItem_Width DEFAULT (60) FOR Width
GO
ALTER TABLE dbo.Tmp_FaceLayoutItem ADD CONSTRAINT
	DF_FaceLayoutItem_Align DEFAULT ('left') FOR Align
GO
ALTER TABLE dbo.Tmp_FaceLayoutItem ADD CONSTRAINT
	DF_FaceLayoutItem_UnitType DEFAULT ('TextUnit') FOR UnitType
GO
SET IDENTITY_INSERT dbo.Tmp_FaceLayoutItem ON
GO
IF EXISTS(SELECT * FROM dbo.FaceLayoutItem)
	 EXEC('INSERT INTO dbo.Tmp_FaceLayoutItem (Id, FaceLayoutId, X, Y, [Position], Text, Font, [Size], Bold, Italic, Color, Angle, Height, Width, Align, UnitType, EffectNode)
		SELECT Id, FaceLayoutId, X, Y, [Position], Text, Font, [Size], Bold, Italic, Color, Angle, CONVERT(real, Height), CONVERT(real, Width), Align, UnitType, EffectNode FROM dbo.FaceLayoutItem (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_FaceLayoutItem OFF
GO
DROP TABLE dbo.FaceLayoutItem
GO
EXECUTE sp_rename N'dbo.Tmp_FaceLayoutItem', N'FaceLayoutItem', 'OBJECT'
GO
ALTER TABLE dbo.FaceLayoutItem ADD CONSTRAINT
	PK_FaceLayoutItem PRIMARY KEY CLUSTERED 
	(
	Id
	) ON [PRIMARY]

GO
ALTER TABLE dbo.FaceLayoutItem WITH NOCHECK ADD CONSTRAINT
	FK_FaceLayoutItem_FaceLayout FOREIGN KEY
	(
	FaceLayoutId
	) REFERENCES dbo.FaceLayout
	(
	Id
	) ON UPDATE CASCADE
	 ON DELETE CASCADE
	
GO
COMMIT


-- removing NULL field 
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
CREATE TABLE dbo.Tmp_LayoutFaceToIcon
	(
	FaceId int NOT NULL,
	LayoutId int NOT NULL,
	ImageId int NOT NULL
	)  ON [PRIMARY]
GO
IF EXISTS(SELECT * FROM dbo.LayoutFaceToIcon)
	 EXEC('INSERT INTO dbo.Tmp_LayoutFaceToIcon (FaceId, LayoutId, ImageId)
		SELECT FaceId, LayoutId, ImageId FROM dbo.LayoutFaceToIcon (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.LayoutFaceToIcon
GO
EXECUTE sp_rename N'dbo.Tmp_LayoutFaceToIcon', N'LayoutFaceToIcon', 'OBJECT'
GO
COMMIT

-- Foreign keys
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
if exists (select * from dbo.sysobjects where id = object_id(N'FK_LayoutFaceToIcon_FaceLayout') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.LayoutFaceToIcon
	DROP CONSTRAINT FK_LayoutFaceToIcon_Face
ALTER TABLE dbo.LayoutFaceToIcon ADD CONSTRAINT
	FK_LayoutFaceToIcon_FaceLayout FOREIGN KEY
	(
	LayoutId
	) REFERENCES dbo.FaceLayout
	(
	Id
	) ON UPDATE CASCADE
	 ON DELETE CASCADE
	
GO
if exists (select * from dbo.sysobjects where id = object_id(N'FK_LayoutFaceToIcon_LayoutIcon') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.LayoutFaceToIcon
	DROP CONSTRAINT FK_LayoutFaceToIcon_LayoutIcon
ALTER TABLE dbo.LayoutFaceToIcon ADD CONSTRAINT
	FK_LayoutFaceToIcon_LayoutIcon FOREIGN KEY
	(
	ImageId
	) REFERENCES dbo.LayoutIcon
	(
	IconId
	) ON UPDATE CASCADE
	 ON DELETE CASCADE
	
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
if exists (select * from dbo.sysobjects where id = object_id(N'FK_LayoutFaceToIcon_Face') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.LayoutFaceToIcon
	DROP CONSTRAINT FK_LayoutFaceToIcon_Face
ALTER TABLE dbo.LayoutFaceToIcon ADD CONSTRAINT
	FK_LayoutFaceToIcon_Face FOREIGN KEY
	(
	FaceId
	) REFERENCES dbo.Face
	(
	Id
	)
GO
COMMIT
