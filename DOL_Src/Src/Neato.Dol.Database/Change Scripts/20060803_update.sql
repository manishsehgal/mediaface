/* Changes
- Update icons for DirectToCD (2 icons)
*/

BEGIN TRANSACTION

IF Not Exists (SELECT * FROM [dbo].[MailFormat] WHERE [MailId] = 2)
BEGIN
INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (2, '', N'Your account was created!', N'Your account on MediaFACE online was created successfully.
You can login on MediaFACE online now.
%PrintzHomeLink
Your Login: %Login
Your Password: %Password')
END
ELSE
BEGIN
IF Exists (SELECT * FROM [dbo].[MailFormat] WHERE [MailId] = 2)
UPDATE [dbo].[MailFormat] SET [Body] = N'Your account on MediaFACE online was created successfully.
You can login on MediaFACE online now.
%PrintzHomeLink
Your Login: %Login
Your Password: %Password'
WHERE [MailId] = 2
END

IF Not Exists (SELECT * FROM [dbo].[MailVariable] WHERE [Id] = 4)
BEGIN
SET IDENTITY_INSERT [dbo].[MailVariable] ON
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (4, '%Login')
SET IDENTITY_INSERT [dbo].[MailVariable] OFF
END

IF Not Exists (SELECT * FROM [dbo].[MailTypeVariable] WHERE [MailId] = 2 AND [MailVariableId] = 4)
BEGIN
SET IDENTITY_INSERT [dbo].[MailTypeVariable] ON
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (4, 2, 4)
SET IDENTITY_INSERT [dbo].[MailTypeVariable] OFF
END

IF Not Exists (SELECT * FROM [dbo].[MailVariable] WHERE [Id] = 5)
BEGIN
SET IDENTITY_INSERT [dbo].[MailVariable] ON
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (5, '%Password')
SET IDENTITY_INSERT [dbo].[MailVariable] OFF
END

IF Not Exists (SELECT * FROM [dbo].[MailTypeVariable] WHERE [MailId] = 2 AND [MailVariableId] = 5)
BEGIN
SET IDENTITY_INSERT [dbo].[MailTypeVariable] ON
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (5, 2, 5)
SET IDENTITY_INSERT [dbo].[MailTypeVariable] OFF
END

COMMIT 