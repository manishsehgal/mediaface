-- creating tables [LayoutFaceToIcon] and [LayoutIcon]


if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LayoutFaceToIcon]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[LayoutFaceToIcon] (
	[FaceId] [int] NOT NULL ,
	[LayoutId] [int] NULL ,
	[ImageId] [int] NOT NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LayoutIcon]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[LayoutIcon] (
	[IconId] [int] IDENTITY (1, 1) NOT NULL ,
	[Icon] [image] NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_LayoutIcon') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[LayoutIcon] WITH NOCHECK ADD 
	CONSTRAINT [PK_LayoutIcon] PRIMARY KEY  CLUSTERED 
	(
		[IconId]
	)  ON [PRIMARY] 
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'FK_LayoutFaceToIcon_Face') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[LayoutFaceToIcon] ADD 
	CONSTRAINT [FK_LayoutFaceToIcon_Face] FOREIGN KEY 
	(
		[FaceId]
	) REFERENCES [dbo].[Face] (
		[Id]
	),
	CONSTRAINT [FK_LayoutFaceToIcon_FaceLayout] FOREIGN KEY 
	(
		[LayoutId]
	) REFERENCES [dbo].[FaceLayout] (
		[Id]
	),
	CONSTRAINT [FK_LayoutFaceToIcon_LayoutIcon] FOREIGN KEY 
	(
		[ImageId]
	) REFERENCES [dbo].[LayoutIcon] (
		[IconId]
	)
GO




-- Add Name column to FaceLayout
BEGIN TRANSACTION

CREATE TABLE #t(x BIT)

EXEC sp_columns @table_name = 'FaceLayout', @table_owner='dbo', @column_name = 'Name'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.FaceLayout ADD
	[Name] nvarchar(50) NULL

INSERT INTO #t VALUES (1)

END
GO

DECLARE @rows INT
SELECT @rows=count(*) FROM #t

SELECT @rows=count(*) FROM #t
IF @rows > 0
	UPDATE [dbo].[FaceLayout] SET [Name] = ''
GO

DECLARE @rows INT
SELECT @rows=count(*) FROM #t
IF @rows > 0
	ALTER TABLE dbo.FaceLayout
		DROP CONSTRAINT FK_FaceLayout_Face
GO

DECLARE @rows INT
SELECT @rows=count(*) FROM #t
IF @rows > 0
	CREATE TABLE dbo.Tmp_FaceLayout
		(
		Id int NOT NULL IDENTITY (1, 1),
		FaceId int NOT NULL,
		[Name] nvarchar(50) NOT NULL
		)  ON [PRIMARY]
GO

DECLARE @rows INT
SELECT @rows=count(*) FROM #t
IF @rows > 0
	SET IDENTITY_INSERT dbo.Tmp_FaceLayout ON
GO

DECLARE @rows INT
SELECT @rows=count(*) FROM #t
IF @rows > 0
	IF EXISTS(SELECT * FROM dbo.FaceLayout)
		EXEC('INSERT INTO dbo.Tmp_FaceLayout (Id, FaceId, Name)
			SELECT Id, FaceId, Name FROM dbo.FaceLayout (HOLDLOCK TABLOCKX)')
GO

DECLARE @rows INT
SELECT @rows=count(*) FROM #t
IF @rows > 0
	SET IDENTITY_INSERT dbo.Tmp_FaceLayout OFF
GO

DECLARE @rows INT
SELECT @rows=count(*) FROM #t
IF @rows > 0
	ALTER TABLE dbo.LayoutFaceToIcon
		DROP CONSTRAINT FK_LayoutFaceToIcon_FaceLayout
GO

DECLARE @rows INT
SELECT @rows=count(*) FROM #t
IF @rows > 0
	ALTER TABLE dbo.FaceLayoutItem
		DROP CONSTRAINT FK_FaceLayoutItem_FaceLayout
GO

DECLARE @rows INT
SELECT @rows=count(*) FROM #t
IF @rows > 0
	DROP TABLE dbo.FaceLayout
GO

DECLARE @rows INT
SELECT @rows=count(*) FROM #t
IF @rows > 0
	EXECUTE sp_rename N'dbo.Tmp_FaceLayout', N'FaceLayout', 'OBJECT'
GO

DECLARE @rows INT
SELECT @rows=count(*) FROM #t
IF @rows > 0
	ALTER TABLE dbo.FaceLayout ADD CONSTRAINT
		PK_FaceLayout PRIMARY KEY CLUSTERED 
		(
		Id
		) ON [PRIMARY]
GO

DECLARE @rows INT
SELECT @rows=count(*) FROM #t
IF @rows > 0
	ALTER TABLE dbo.FaceLayout WITH NOCHECK ADD CONSTRAINT
		FK_FaceLayout_Face FOREIGN KEY
		(
		FaceId
		) REFERENCES dbo.Face
		(
		Id
		) ON UPDATE CASCADE
		ON DELETE CASCADE
GO

DECLARE @rows INT
SELECT @rows=count(*) FROM #t
IF @rows > 0
	ALTER TABLE dbo.FaceLayoutItem WITH NOCHECK ADD CONSTRAINT
		FK_FaceLayoutItem_FaceLayout FOREIGN KEY
		(
		FaceLayoutId
		) REFERENCES dbo.FaceLayout
		(
		Id
		) ON UPDATE CASCADE
		ON DELETE CASCADE
GO

DECLARE @rows INT
SELECT @rows=count(*) FROM #t
IF @rows > 0
	ALTER TABLE dbo.LayoutFaceToIcon WITH NOCHECK ADD CONSTRAINT
		FK_LayoutFaceToIcon_FaceLayout FOREIGN KEY
		(
		LayoutId
		) REFERENCES dbo.FaceLayout
		(
		Id
		)
GO

DROP TABLE #t
COMMIT
GO
