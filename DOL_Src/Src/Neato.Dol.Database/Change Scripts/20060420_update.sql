/*
    Alter collation on FaceLocalization.FaceName
*/
ALTER TABLE [dbo].[FaceLocalization] ALTER COLUMN [FaceName] [nvarchar] (255) COLLATE Latin1_General_CI_AS NOT NULL