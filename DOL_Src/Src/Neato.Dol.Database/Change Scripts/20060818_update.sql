/* Changes
- Paper Tracking
*/

BEGIN TRANSACTION

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Tracking_Papers]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[Tracking_Papers] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Paper] [nvarchar] (64) COLLATE Latin1_General_BIN NOT NULL ,
	[User] [varchar] (200) COLLATE Latin1_General_BIN NOT NULL ,
	[Time] [datetime] NOT NULL 
) ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_Tracking_Papers') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[Tracking_Papers] WITH NOCHECK ADD 
	CONSTRAINT [PK_Tracking_Papers] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

COMMIT 