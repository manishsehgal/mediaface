/* Changes
- add Parameters table
- add 2 rows (NeatoDolBuyAccountUrl and DefaultPageKeywords)
*/
BEGIN TRANSACTION
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Parameters]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
    CREATE TABLE [dbo].[Parameters] (
	    [Id] [int] IDENTITY (1, 1) NOT NULL ,
	    [Key] [nvarchar] (40) COLLATE Latin1_General_BIN NOT NULL ,
	    [Value] [ntext] COLLATE Latin1_General_BIN NOT NULL 
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_Parameters') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
BEGIN
    ALTER TABLE [dbo].[Parameters] WITH NOCHECK ADD 
	    CONSTRAINT [PK_Parameters] PRIMARY KEY  CLUSTERED 
	    (
		    [Id]
	    )  ON [PRIMARY]
    ALTER TABLE [dbo].[Parameters] ADD 
	    CONSTRAINT [DF_Parameters_Key] DEFAULT ('') FOR [Key],
	    CONSTRAINT [DF_Parameters_Value] DEFAULT ('') FOR [Value],
	    CONSTRAINT [IX_Parameters] UNIQUE  NONCLUSTERED 
	    (
		    [Key]
	    )  ON [PRIMARY]
END
GO

if not exists (select * from [dbo].[Parameters] WHERE [Key] LIKE 'NeatoDolBuyAccountUrl')
BEGIN
INSERT INTO [dbo].[Parameters] ([Key], [Value])
VALUES('NeatoDolBuyAccountUrl', 'http://orange2cat.star-sw.com/WebStore/DolBuyAccount.aspx')
END

if not exists (select * from [dbo].[Parameters] WHERE [Key] = 'DefaultPageKeywords')
BEGIN
INSERT INTO [dbo].[Parameters] ([Key], [Value])
VALUES('DefaultPageKeywords', 'Neato, NEATO CD/DVD Labels and Inserts, Neato iPod Wraps, LightScribe, direct to cd printing, Canon InkJet printers, Epson InkJet printers')
END

COMMIT