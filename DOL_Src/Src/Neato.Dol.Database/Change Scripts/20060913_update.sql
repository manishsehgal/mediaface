-- Create Tracking_Shapes table

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Tracking_Shapes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[Tracking_Shapes] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[ShapeId] [varchar] (60) COLLATE Latin1_General_BIN NOT NULL ,
	[User] [varchar] (200) COLLATE Latin1_General_BIN NOT NULL ,
	[Time] [datetime] NOT NULL 
) ON [PRIMARY]
END

GO
