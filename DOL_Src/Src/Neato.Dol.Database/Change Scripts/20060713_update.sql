/* Changes
- data to tables:
    - MailType (Activate account notification)
*/

BEGIN TRANSACTION

IF Not Exists (SELECT * FROM [dbo].[MailType] WHERE [MailId] = 2)
BEGIN
SET IDENTITY_INSERT [dbo].[MailType] ON
INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (2, 'Account created')
SET IDENTITY_INSERT [dbo].[MailType] OFF
END

IF Not Exists (SELECT * FROM [dbo].[MailFormat] WHERE [MailId] = 2)
INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (2, '', N'Your account was created!', N'Your account on MediaFACE online was created successfully.
You can login on MediaFACE online now.
%PrintzHomeLink')

IF Not Exists (SELECT * FROM [dbo].[MailVariable] WHERE [Id] = 3)
BEGIN
SET IDENTITY_INSERT [dbo].[MailVariable] ON
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (3, '%PrintzHomeLink')
SET IDENTITY_INSERT [dbo].[MailVariable] OFF
END

IF Not Exists (SELECT * FROM [dbo].[MailTypeVariable] WHERE [MailId] = 2)
BEGIN
SET IDENTITY_INSERT [dbo].[MailTypeVariable] ON
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (3, 2, 3)
SET IDENTITY_INSERT [dbo].[MailTypeVariable] OFF
END

COMMIT