/* Changes
- Update icons for DirectToCD (2 icons)
*/

BEGIN TRANSACTION

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HiddenBanner]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[HiddenBanner] (
	[PageName] [nvarchar] (255) COLLATE Latin1_General_BIN NOT NULL ,
	[BannerId] [int] NOT NULL 
) ON [PRIMARY]
END
GO

EXEC sp_columns @table_name = 'Paper', @table_owner='dbo', @column_name = 'Orientation'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Paper ADD
	Orientation nchar(1) COLLATE Latin1_General_BIN NOT NULL DEFAULT ('P')
END
GO

UPDATE dbo.Paper
SET Orientation = 'L'
WHERE (Id = 13092 OR Id = 13093 OR Id = 13721)

COMMIT 