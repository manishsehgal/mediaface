/* Changes
update face #1039
*/

BEGIN TRANSACTION

DECLARE @Id int
SET @Id = (SELECT [FaceId] FROM dbo.FaceLocalization WHERE FaceName = 'CD Print & File Pouch (adhesive)' AND Culture = '')
SELECT @Id
UPDATE Face SET Contour = '<Contour x="0" y="0">
  <MoveTo x="0.0" y="0.0" /> 
  <LineTo x="-56.69" y="17.01" /> 
  <LineTo x="-56.69" y="345.83" /> 
  <LineTo x="0.0" y="362.82" /> 
  <LineTo x="0.0" y="725.67" /> 
  <LineTo x="56.69" y="725.67" /> 
  <LineTo x="85.04" y="708.66" /> 
  <LineTo x="269.29" y="708.66" /> 
  <LineTo x="297.64" y="725.67" /> 
  <LineTo x="354.33" y="725.67" /> 
  <LineTo x="354.33" y="362.82" /> 
  <LineTo x="411.02" y="345.83" /> 
  <LineTo x="411.02" y="17.01" /> 
  <LineTo x="354.33" y="0.0" /> 
  <LineTo x="0.0" y="0.0" /> 
  <MoveTo x="354.33" y="0.1" /> 
  <LineTo x="354.33" y="362.81" /> 
  <MoveTo x="0.0" y="0.1" /> 
  <LineTo x="0.0" y="362.81" /> 
  <MoveTo x="0.1" y="362.82" /> 
  <LineTo x="354.32" y="362.82" /> 
  </Contour>
'
WHERE [Id] = @Id

COMMIT