/*
    Change paper size to "US" for DirectToCD devices
*/ 

BEGIN TRANSACTION

UPDATE dbo.Paper
SET Width = 612, Height = 792
WHERE   [Name] = N'DirectToCD CD/DVD' OR
        [Name] = N'DirectToCD Mini CD'

COMMIT TRANSACTION