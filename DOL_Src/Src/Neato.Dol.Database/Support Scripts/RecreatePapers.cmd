@echo off
REM: Command File Created by Microsoft Visual Database Tools
REM: Date Generated: 28.10.2005
REM: Authentication type: SQL Server
REM: Usage: CommandFilename [Server] [Database] [Login] [Password]

if '%1' == '' goto usage
if '%2' == '' goto usage
if '%3' == '' goto usage

if '%1' == '/?' goto usage
if '%1' == '-?' goto usage
if '%1' == '?' goto usage
if '%1' == '/help' goto usage

osql -S %1 -d %2 -U %3 -P %4 -b -i "dbo.TruncatePapers.sql"

osql -S %1 -d %2 -U %3 -P %4 -b -i "..\Create Scripts\FillData\dbo.DeviceTypeData.sql"
echo DeviceTypeData.sql done
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "..\Create Scripts\FillData\dbo.BrandData.sql"
echo BrandData.sql done
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "..\Create Scripts\FillData\dbo.DeviceData.sql"
echo DeviceData.sql done
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "..\Create Scripts\FillData\dbo.FaceData.sql"
echo FaceData.sql done
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "..\Create Scripts\FillData\dbo.FaceLocalizationData.sql"
echo FaceLocalizationData.sql done
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "..\Create Scripts\FillData\dbo.FaceToDeviceData.sql"
echo FaceToDeviceData.sql done
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "..\Create Scripts\FillData\dbo.CarrierData.sql"
echo CarrierData.sql done
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "..\Create Scripts\FillData\dbo.DeviceToCarrierData.sql"
echo DeviceToCarrierData.sql done
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "..\Create Scripts\FillData\dbo.PaperTypeData.sql"
echo PaperTypeData.sql done
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "..\Create Scripts\FillData\dbo.PaperBrandData.sql"
echo PaperBrandData.sql done
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "..\Create Scripts\FillData\dbo.PaperData.sql"
echo PaperData.sql done
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "..\Create Scripts\FillData\dbo.FaceToPaperData.sql"
echo FaceToPaperData.sql done
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "..\Create Scripts\FillData\dbo.FaceLayoutItemData.sql"
echo FaceLayoutItemData.sql done
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "..\Create Scripts\FillData\dbo.FaceLayoutData.sql"
echo FaceLayoutData.sql done
if %ERRORLEVEL% NEQ 0 goto errors

goto finish

REM: How to use screen
:usage
echo.
echo Usage: MyScript Server Database User [Password]
echo Server: the name of the target SQL Server
echo Database: the name of the target database
echo User: the login name on the target server
echo Password: the password for the login on the target server (optional)
echo.
echo Example: MyScript.cmd MainServer MainDatabase MyName MyPassword
echo.
echo.
goto done

REM: error handler
:errors
echo.
echo WARNING! Error(s) were detected!
echo --------------------------------
echo Please evaluate the situation and, if needed,
echo restart this command file. You may need to
echo supply command parameters when executing
echo this command file.
echo.
pause
goto done

REM: finished execution
:finish
echo.
echo Script execution is complete!
:done
@echo on
