/*  $Header: pcmain.c 1     4/18/96 5:38p dlazaron $     */
/*  $Log: pcmain.c $
 * 
 * 1     4/18/96 5:38p dlazaron
 * Add ADMTwirl example.
 * 
 * 1     2/21/96 10:41a NAN_NT
 * 
 * 3     2/08/96 10:08p dlazaron
 * Changed AMPPlatformInstance to AMPPlatformInstanceRef.
 * 
 * 2     2/02/96 3:48p NAN_NT
 * With the new Illustrator 7.0 plugin and MP Headers.
 * 
 * 1     2/01/96 10:30a NAN_NT
 * 
 *    Rev 1.0   04 Apr 1995 12:05:50   NAN_NT
 * Initial revision.
*/

/**
	pcmain.c
**/
#include "IllustratorSDK.h"

// #include <windows.h>


BOOL APIENTRY DllMain(
	HANDLE 	hDLL,
	DWORD	dwReason,
	LPVOID	lpReserved)
{
	switch(dwReason)
   	{
        case DLL_PROCESS_ATTACH:
			break;

        case DLL_PROCESS_DETACH:
			break;

        case DLL_THREAD_ATTACH:
        case DLL_THREAD_DETACH:
        default:
            break;

   	} // end switch()

	return TRUE;
}

