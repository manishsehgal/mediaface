

extern AIErr WriteText( FILE *fp, AIArtHandle theText );
extern AIErr ReadText( FILE *fp, AIStreamHandle stream  );

extern AIErr closeAndPositionText( AIArtHandle label, AIStreamHandle stream );
extern AIErr openDefaultText( AIArtHandle *label, AIStreamHandle *stream );
