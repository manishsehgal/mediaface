/**

	handleError.h
	Copyright (c) 1995 Adobe Systems Incorporated.

	Adobe Illustrator 7.0 Useful Plug-in Utilities.

 **/

#include "SPInterf.h"


/**-----------------------------------------------------------------------------
 **
 **	Types
 **
 **/


#define kBadSelectionErr  '!sel'


/**-----------------------------------------------------------------------------
 **
 **	Functions
 **
 **/
//typedef	AIErr FXErr;

// string stuff
extern void AIErrToCString( AIErr error_to_report, char *errorText );
extern void CStringToAIErr( char *errorText, AIErr *error_to_report );

// ui stuff
extern void report_error( SPInterfaceMessage *message, AIErr error_to_report );
