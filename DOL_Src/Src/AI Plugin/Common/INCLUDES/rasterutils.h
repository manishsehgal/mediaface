/* ---------------------------------------------------------------------------

	raster utils.h
 
	Copyright (c) 1995 Adobe Systems Incorporated
	All Rights Reserved
	
   --------------------------------------------------------------------------- */


#define kMyOffscreenErr '!Off'


typedef struct {

	GWorldPtr 				fWorld;			// pass this in to newRasterForGWorld
	// Scratch Fields
	Rect					fRasterBounds;  // filled out by newRasterForGWorld
	int16					fColorSpace;	// passed into CreateGWorldTile
	Fixed					fScale;
	FixedRect				fSelectionBounds;

	Slice					fRasterSlice;
	Slice					fWorldSlice;
 	Tile					fWorldTile ;
 		
} MakeGWorldTileRequest;
	


extern FXErr newRasterArtForGWorld( GWorldPtr theWorld, Fixed scale, 
			MakeGWorldTileRequest *request, AIArtHandle *newRaster );
extern FXErr CreateGWorldTile( MakeGWorldTileRequest *request );
extern FXErr GetColorSpaceDepth( int16 colorSpace, int16 *depthPointer );
extern FXErr createOffscreen( GWorldPtr *offscreen, Rect *bounds, short depth );
extern void disposeOffscreen( GWorldPtr *offscreen );
extern FXErr copyGWorldToRasterArt( MakeGWorldTileRequest *request, AIArtHandle destRaster );
