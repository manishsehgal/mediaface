/* ---------------------------------------------------------------------------

	artset utils.h
 
	Copyright (c) 1995 Adobe Systems Incorporated
	All Rights Reserved
	
   --------------------------------------------------------------------------- */

#include "AITypes.h"

//#define __INCLUDERASTER__

#define kWantsPathArt	(1L<<0)
#define kWantsTextArt	(1L<<1)
#define kWantsPlacedArt	(1L<<2)
#define kWantsRasterArt	(1L<<3)
#define kWantsGroupArt	(1L<<4)
#define kWantsCompoundArt (1L<<5)


extern AIErr getSelectionSet( AIArtSet *matches, long whichMatches, long *numMatches, AIBoolean justLooking );
extern AIErr getArtSetBounds( AIArtSet matches, AIRealRect *totalBrushBounds );


#ifdef __INCLUDERASTER__
extern AIErr rasterizeArtSet( AIArtSet theArt, GWorldPtr *rasterizedArt, 
			AIReal scale, Rect *boundsRect );
extern void releaseRasterizedArtSet( GWorldPtr *offscreen );
#endif