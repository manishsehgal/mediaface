#if !defined(AFX_IllustratorSDK_H__12A7FFEC_D764_41F9_95E0_762655A3DD4B__INCLUDED_)
#define AFX_IllustratorSDK_H__12A7FFEC_D764_41F9_95E0_762655A3DD4B__INCLUDED_

#include "SPConfig.h"

// Derective for Codewarrior to build the pre-compiled header file
#if (defined(__PIMWCWMacPPC__) && !(defined(MakingPreCompiledHeader)))
	#if _DEBUG
		#include "IllustratorSDKDebug.ch"
	#else
		#include "IllustratorSDKRelease.ch"
	#endif
#else

// std library 
#include <stdio.h>
#include <string.h>
#include <vector>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <math.h>

#ifdef MAC_ENV
	#include <Carbon.h>
#endif

#ifdef WIN_ENV
	#include "windows.h"
	#include <time.h>
#endif

using namespace std;


// sweet pea headers
#include "SPTypes.h"
#include "SPBlocks.h"

// adm headers
#include "ADMBasic.h"
#include "ADMDialog.h"
#include "ADMItem.h"
#include "ADMIcon.h"
#include "ADMDialogGroup.h"

// adm C++ wrapper classes
#include "IADMDialog.hpp"
#include "IADMEntry.hpp"
#include "IADMHierarchyList.hpp"
#include "IADMEntry.hpp"
#include "IADMIcon.hpp"
#include "IADMItem.hpp"
#include "IADMList.hpp"
#include "IADMListEntry.hpp"
#include "IADMNotifier.hpp"
#include "IADMTracker.hpp"

#include "BaseADMDialog.hpp"


// illustrator headers
#include "AITypes.h"                        

#include "AIArt.h"
#include "AIArtSet.h"
#include "AIDictionary.h"
#include "AIDocument.h"
#include "AIHardSoft.h"
#include "AILayer.h"
#include "AILegacyTextConversion.h"
#include "AIMatchingArt.h"
#include "AIMdMemory.h"
#include "AIMenu.h"
#include "AIMenuGroups.h"
#include "AINotifier.h"
#include "AIPath.h"
#include "AIPathStyle.h"
#include "AIPlugin.h"
#include "AIPreference.h"
#include "AITextFrame.h"
#include "AITimer.h"
#include "AITool.h"
#include "AIRuntime.h"
#include "AIUndo.h"
#include "AIUser.h"

// Legacy headers
#include "AI110Art.h"
#include "AI110Document.h"
#include "AI110Font.h"
#include "AI110Layer.h"
#include "AI110Menu.h"
#include "AI110Path.h"
#include "AI110User.h"

// ATE text API
#include "IText.h"

// SDK common headers
#include "Suites.hpp"
#include "stringUtils.h"
#include "Plugin.hpp"

#endif // #if (defined(__PIMWCWMacPPC__) && !(defined(MakingPreCompiledHeader)))
#endif //  !defined(AFX_IllustratorSDK_H__12A7FFEC_D764_41F9_95E0_762655A3DD4B__INCLUDED_)
