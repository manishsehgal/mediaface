/* ---------------------------------------------------------------------------

	stringUtils.h

	Copyright (c) 1997 Adobe Systems Incorporated 
	All Rights Reserved

	
   --------------------------------------------------------------------------- */

#ifndef __PSTRING__
#define __PSTRING__

#ifdef __cplusplus
extern "C" {
#endif

unsigned char *SUctopstr( char *s );
char *SUptocstr( unsigned char *p );
unsigned char *SUpstrcpy( unsigned char *dst, const unsigned char *src );
unsigned long SUstrlen(char* s);

void SUpstrcat( unsigned char *dst, unsigned char *src );
void SUpstrinsert( unsigned char *dst, unsigned char *src );

char *SUsafestrncpy(char *dst, const char *src, int dstsize);
char *SUsafestrncat(char *dst, const char *src, int dstsize);

void SUpasToPostscriptStr( unsigned char dst, unsigned char src );

#ifdef __cplusplus
}
#endif


#endif
