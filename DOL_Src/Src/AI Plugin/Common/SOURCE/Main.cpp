#include "IllustratorSDK.h"

#include "Plugin.hpp"

extern "C"
{
	SPBasicSuite *sSPBasic = NULL;
}

static bool unloaded = true;

DLLExport SPAPI SPErr PluginMain(char *caller, char *selector, void *message)
{
	SPErr error = kNoErr;

	SPMessageData *msgData = static_cast<SPMessageData *>(message);
		
	Plugin *plugin = static_cast<Plugin *>(msgData->globals);
	
	bool shouldDelete = false;

	try 
	{
		sSPBasic = msgData->basic;

		if (plugin != NULL && unloaded)
			throw((SPErr)kBadParameterErr);

		if (plugin == NULL)
		{
			plugin = AllocatePlugin(msgData->self);
			
			if (plugin != NULL)
			{
				msgData->globals = plugin;
				unloaded = false;
			}
			else
			{
				error = kOutOfMemoryErr;
				throw((SPErr)error);
			}
			
		}

		error = plugin->Message(caller, selector, message);
		if (error) throw((SPErr)error);

		if (plugin->IsUnloadMsg(caller, selector))
			shouldDelete = true;
	}

	catch (SPErr inError)
	{
		error = inError;

		if (plugin != NULL && ( plugin->IsUnloadMsg(caller, selector) || 
		     					plugin->IsReloadMsg(caller, selector) ))
			shouldDelete = true;
	}

	catch (...)
	{
		error = kBadParameterErr;

		if (plugin != NULL && ( plugin->IsUnloadMsg(caller, selector) || 
		     					plugin->IsReloadMsg(caller, selector) ))
			shouldDelete = true;
	}

	if (shouldDelete)
	{
		delete plugin;
		msgData->globals = plugin = NULL;
		unloaded = true;
	}

	return error;
}
