#include "IllustratorSDK.h"
#include "common.h"
#include "pluginHandler.h"

#include "NeatoXmlFormatter.h"

AIErr handlePluginCaller( char* caller, char* selector, void *message ) 
{
	AIErr error = kNoErr;

	// This is just an easy way of not having to cast message to ( SPMessageData* )
	// everytime we use IsEqual()
	SPBasicSuite *sSPBasic = ( ( SPMessageData* )message)->basic;

	if ( sSPBasic->IsEqual( caller, kCallerAIFileFormat ) ) 
	{
		if ( sSPBasic->IsEqual( selector, kSelectorAIGetFileFormatParameters ) )
			error = goGetFormatParms( ( AIFileFormatMessage* )message );
	
		else if ( sSPBasic->IsEqual( selector, kSelectorAIGoFileFormat ) )
			error = goFileFormat( ( AIFileFormatMessage* )message );
			
		else if ( sSPBasic->IsEqual( selector, kSelectorAICheckFileFormat ) )
			error = goCheckFormat( ( AIFileFormatMessage* )message );
	}

	return error;
}


// extend startup here
AIErr pluginStartup( SPInterfaceMessage *message ) 
{
	return ( addFileFormat( message ) );
}


/*	There are no suites acquired at this point, as you should mostly be doing
	system stuff here. */
void pluginShutdown( SPInterfaceMessage *message ) 
{
}


AIErr reloadPluginSuiteFunctions( SPAccessMessage *message ) 
{
	return kNoErr;
}


void unloadPluginSuiteFunctions( SPAccessMessage *message ) 
{
}
// end pluginHandler.cpp