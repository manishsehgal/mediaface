/* ---------------------------------------------------------------------------

	common.h
 
	Copyright (c) 1997 Adobe Systems Incorporated
	All Rights Reserved
	
   --------------------------------------------------------------------------- */
#ifndef __COMMON__H__
#define __COMMON__H__

#include "IllustratorSDK.h"
#include "AISymbol.h"
#include "AIPattern.h"
#include "AIColorConversion.h"

/**-----------------------------------------------------------------------------
 **
 **	Types
 **
 **/


#ifndef GLOBALSTRUCT
#define GLOBALSTRUCT

typedef struct {
	AIFileFormatHandle fileFormatFace;
	AIFileFormatHandle fileFormatPaperOutline;
	AIFileFormatHandle fileFormatPaperUniv;
} Globals;

#endif

/**-----------------------------------------------------------------------------
 **
 **	Globals
 **
 **/


// general globals
extern Globals *g;
extern SPAccessRef gSelfAccess;
extern SPAccessSuite *sSPAccess;
extern "C" SPBasicSuite *sSPBasic;

// suite stuff
extern "C" SPBlocksSuite *sSPBlocks;
extern SPAccessSuite *sSPAccess;

extern AIMdMemorySuite *sAIMDMemory;
extern AI110UserSuite *sAIUser;
extern AIArtSetSuite *sAIArtSet;
extern AIFileFormatSuite *sAIFileFormat;
extern AIDocumentSuite *sAIDocument;
extern AITextFrameSuite	*sAITextFrame;
extern AIArtSuite *sAIArt;
extern AISymbolSuite *sAISymbol;
extern AIPatternSuite *sAIPattern;
extern AIPathSuite *sAIPath;
extern AIPathStyleSuite *sAIPathStyle;
extern AIMatchingArtSuite *sAIMatchingArt;
extern AIMdMemorySuite *sAIMdMemory;
extern "C" AIUnicodeStringSuite *sAIUnicodeString;
extern "C" AIFilePathSuite *sAIFilePath;
extern "C" AIColorConversionSuite *sAIColorConversion;

extern ADMBasicSuite *sADMBasic;
extern "C" ADMDialogSuite5 *sADMDialog;
extern "C" ADMItemSuite7	*sADMItem;




/**-----------------------------------------------------------------------------
 **
 **	suite support
 **
 **/

extern	AIErr acquireSuites( SPBasicSuite *sBasic );
extern	AIErr releaseSuites( SPBasicSuite *sBasic );

#endif