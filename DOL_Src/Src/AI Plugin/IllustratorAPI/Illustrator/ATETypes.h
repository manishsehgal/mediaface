/* -------------------------------------------------------------------------------

                                    ADOBE CONFIDENTIAL
                                _________________________

    (c) Copyright 2000-2004 Adobe Systems Incorporated
    All Rights Reserved.

    NOTICE:  All information contained herein is, and remains
     the property of Adobe Systems Incorporated and its suppliers,
     if any.  The intellectual and technical concepts  contained
     herein are proprietary to Adobe Systems Incorporated and its
     suppliers and may be covered by U.S. and Foreign Patents,
     patents in process, and are protected by trade secret or copyright law.
     Dissemination of this information or reproduction of this material
     is strictly forbidden unless prior written permission is obtained
     from Adobe Systems Incorporated.

 ----------------------------------------------------------------------------------

	File:	ATETypes.h
		
	Notes:	
	
 ---------------------------------------------------------------------------------- */
#ifndef __ATETypes__
#define __ATETypes__

#ifdef __MWERKS__
	#if defined(__MC68K__) || defined(__POWERPC__)
		#pragma enumsalwaysint on
	#endif
#endif // __MWERKS__

#ifndef __cplusplus
#define bool ASBoolean
#endif

typedef unsigned short ASUTF16;

#ifdef __cplusplus
namespace ATE
{
#endif

typedef int	ATEGlyphID;

enum ATEErr {
	kNoError = 0,
	kBadParameter,
	kOutOfMemory,
	kOutOfRange,
	kNullPointer,
	kInternalError,
	kMissingSpellingDictionary
};
#ifndef __cplusplus
typedef enum ATEErr ATEErr;
#endif


enum StreamVersion
{
	kStreamVersion1 = 1, kStreamVersion2, kStreamVersion3, kStreamVersion4, kStreamVersion5, kStreamVersion6, kStreamVersion7, kStreamVersion8, kStreamVersion9, 
	kStreamVersion10, kStreamVersion11, kStreamVersioStreamVersion13, kStreamVersion14, kStreamVersion15, kStreamVersion16, kStreamVersion17, kStreamVersion18, kStreamVersion19, 
	kStreamVersion20, kStreamVersion21, kStreamVersion22, kStreamVersion23, kStreamVersion24, kStreamVersion25, kStreamVersion26, kStreamVersion27, kStreamVersion28, kStreamVersion29, 
	kStreamVersion30, kStreamVersion31, kStreamVersion32, kStreamVersion33, kStreamVersion34, kStreamVersion35, kStreamVersion36, kStreamVersion37, kStreamVersion38, kStreamVersion39
};
#ifndef __cplusplus
typedef enum StreamVersion StreamVersion;
#endif

// ----------------------------------------------------------------------------------
		
enum LineCapType {
	kButtCap = 0,
	kRoundCap = 1,
	kSquareCap = 2,
	kNumLineCapTypes
};
#ifndef __cplusplus
typedef enum LineCapType LineCapType;
#endif

// ----------------------------------------------------------------------------------
		
enum ASCharType {
	/** space character */
	kASSpaceChar = 0,
	/** punctuation character */
	kPunctuationChar,
	/** paragraph end character CR */
	kParagraphEndChar,
	/** this character is anything but space, punctuation or paragraphend */
	kNormalChar
};
#ifndef __cplusplus
typedef enum ASCharType ASCharType;
#endif

// ----------------------------------------------------------------------------------
		
enum LineJoinType {
	/** A mitered join */
	kMiterJoin = 0,
	/** A round join */
	kRoundJoin = 1,
	/** A beveled join */
	kBevelJoin = 2,
	kNumLineJoinTypes
};
#ifndef __cplusplus
typedef enum LineJoinType LineJoinType;
#endif

// ----------------------------------------------------------------------------------
		
enum ParagraphJustification
{
	kLeftJustify = 0,
	kRightJustify,
	kCenterJustify,

	kFullJustifyLastLineLeft,
	kFullJustifyLastLineRight,
	kFullJustifyLastLineCenter,

	kFullJustifyLastLineFull
};
#ifndef __cplusplus
typedef enum ParagraphJustification ParagraphJustification;
#endif

// ----------------------------------------------------------------------------------

enum WariChuJustification
{
	kWariChuLeftJustify = 0,
	kWariChuRightJustify,
	kWariChuCenterJustify,

	kWariChuFullJustifyLastLineLeft,
	kWariChuFullJustifyLastLineRight,
	kWariChuFullJustifyLastLineCenter,

	kWariChuFullJustifyLastLineFull,

	kWariChuAutoJustify
};
#ifndef __cplusplus
typedef enum WariChuJustification WariChuJustification;
#endif

// ----------------------------------------------------------------------------------

enum PreferredKinsokuOrder
{
	kPushIn = 0,
	kPushOutFirst,
	kPushOutOnly
};
#ifndef __cplusplus
typedef enum PreferredKinsokuOrder PreferredKinsokuOrder;
#endif

// ----------------------------------------------------------------------------------

enum BurasagariType
{
	kBurasagariNone = 0,
	kBurasagariStandard,
	kBurasagariStrong
};
#ifndef __cplusplus
typedef enum BurasagariType BurasagariType;
#endif

// ----------------------------------------------------------------------------------

enum LeadingType
{
	kRomanLeadingType = 0,
	kJapaneseLeadingType = 1
};
#ifndef __cplusplus
typedef enum LeadingType LeadingType;
#endif

// ----------------------------------------------------------------------------------

enum TabType
{
	kLeftTabType = 0,
	kCenterTabType,
	kRightTabType,
	kDecimalTabType
};
#ifndef __cplusplus
typedef enum TabType TabType;
#endif

// ----------------------------------------------------------------------------------

enum AutoKernType { kNoAutoKern, kMetricKern, kOpticalKern };
#ifndef __cplusplus
typedef enum AutoKernType AutoKernType;
#endif

// ----------------------------------------------------------------------------------

enum JapaneseAlternateFeature
{
	kDefaultForm,
	kTraditionalForm,
	kExpertForm,
	kJIS78Form,
	kJIS83Form,
	kHalfWidthForm,
	kThirdWidthForm,
	kQuarterWidthForm,
	kFullWidthForm,
	kProportionalWidthForm
};
#ifndef __cplusplus
typedef enum JapaneseAlternateFeature JapaneseAlternateFeature;
#endif

// ----------------------------------------------------------------------------------

enum BaselineDirection
{
	kNeverUseMustBeKnown = 0,				// we don't allow "unknown" like we used to
	kBaselineWithStream = 1,	// rotate roman glyphs to vertical orientation in vertical
	kBaselineVerticalRotated = 2,					// default baseline -- roman glyphs on side in vertical
	kBaselineVerticalCrossStream = 3				// Tate-Chu-Yoko in vertical
};
#ifndef __cplusplus
typedef enum BaselineDirection BaselineDirection;
#endif

// ----------------------------------------------------------------------------------

enum UnderlinePosition
{
	kUnderlineOff,
	kUnderlineOn_RightInVertical,
	kUnderlineOn_LeftInVertical
};

#ifndef __cplusplus
typedef enum UnderlinePosition UnderlinePosition;
#endif

// ----------------------------------------------------------------------------------

enum StrikethroughPosition
{
	kStrikethroughOff,
	kStrikethroughOn_XHeight,
	kStrikethroughOn_EMBox
};

#ifndef __cplusplus
typedef enum StrikethroughPosition StrikethroughPosition;
#endif

// ----------------------------------------------------------------------------------

enum StyleRunAlignment
{
	kAlignStylesByBottom = 0,
	kAlignStylesByICFBottom = 1,
	
	kAlignStylesByRomanHorizontalCenterVertical = 2,
	
	kAlignStylesByCenterHorizontalRomanVertical = 3,
	
	kAlignStylesByICFTop = 4,
	kAlignStylesByTop = 5
};
#ifndef __cplusplus
typedef enum StyleRunAlignment StyleRunAlignment;
#endif

// ----------------------------------------------------------------------------------

enum CaseChangeType
{
	kUppercase = 0,
	kLowercase,
	kTitleCase,
	kSentenceCase
};
#ifndef __cplusplus
typedef enum CaseChangeType CaseChangeType;
#endif

// ----------------------------------------------------------------------------------

enum Language
{
	kEnglishLanguage,
	kFinnishLanguage,
	kStandardFrenchLanguage,
	kCanadianFrenchLanguage,
	kStandardGermanLanguage,
	kOldGermanLanguage,
	kSwissGermanLanguage,
	kItalianLanguage,
	kBokmalNorwegianLanguage,
	kNynorskNorwegianLanguage,
	kStandardPortugueseLanguage,
	kBrazillianPortugueseLanguage,
	kSpanishLanguage,
	kSwedishLanguage,
	kUKEnglishLanguage,
	kDutchLanguage,
	kDanish,
	kCatalan,
	kRussian,
	kUkranian,
	kBulgarian,
	kSerbian,
	kCzech,
	kPolish,
	kRumanian,
	kGreek,
	kTurkish,
	kIcelandic,
	kHungarian,
	kChineseLanguage,
	kJapaneseLanguage,
	kNumberOfLanguages
};
#ifndef __cplusplus
typedef enum Language Language;
#endif

// ----------------------------------------------------------------------------------

enum SyntheticFontType
{
	kNoSynthetic = 0,
	kItalicSynthetic,
	kBoldSynthetic,
	kBoldItalicSynthetic
};
#ifndef __cplusplus
typedef enum SyntheticFontType SyntheticFontType;
#endif

enum FigureStyle
{
	kDefaultFigureStyle = 0,
	kTabularLining,
	kProportionalOldStyle,
	kProportionalLining,
	kTabularOldStyle
};
#ifndef __cplusplus
typedef enum FigureStyle FigureStyle;
#endif

// ----------------------------------------------------------------------------------

enum FontCapsOption
{
	kFontNormalCaps = 0,
	kFontSmallCaps,
	kFontAllCaps,
	kFontAllSmallCaps
};
#ifndef __cplusplus
typedef enum FontCapsOption FontCapsOption;
#endif

// ----------------------------------------------------------------------------------

enum FontBaselineOption
{
	kFontNormalBaseline = 0,
	kFontFauxedSuperScript,
	kFontFauxedSubScript
};

#ifndef __cplusplus
typedef enum FontBaselineOption FontBaselineOption;
#endif

// ----------------------------------------------------------------------------------

enum FontOpenTypePositionOption
{
	kFontOTNormalPosition,
	kFontOTSuperscriptPosition,
	kFontOTSubscriptPosition,
	kFontOTNumeratorPosition,
	kFontOTDenominatorPosition
};

#ifndef __cplusplus
typedef enum FontOpenTypePositionOption FontOpenTypePositionOption;
#endif

// ----------------------------------------------------------------------------------

enum MakeEmptyMemoryHint
{
	kMakeEmptyAndFreeAllocations,
	kMakeEmptyAndPreserveAllocations
};

#ifndef __cplusplus
typedef enum MakeEmptyMemoryHint MakeEmptyMemoryHint;
#endif

// ----------------------------------------------------------------------------------

enum Direction { kForward = +1, kStationary = 0, kBackward = -1 };
#ifndef __cplusplus
typedef enum Direction Direction;
#endif

// ----------------------------------------------------------------------------------

enum FlattenWithParentStyles
{
	kDoNotFlattenWithParent = 0,
	kFlattenWithParent = 1
};
#ifndef __cplusplus
typedef enum FlattenWithParentStyles FlattenWithParentStyles;
#endif

// ----------------------------------------------------------------------------------

enum SearchScope
{
	kSearchEntireDocument = 0,
	kSearchStory = 1
};
#ifndef __cplusplus
typedef enum SearchScope SearchScope;
#endif

// ----------------------------------------------------------------------------------

enum CollapseDirection
{
	CollapseEnd,
	CollapseStart
};
#ifndef __cplusplus
typedef enum CollapseDirection CollapseDirection;
#endif


// ----------------------------------------------------------------------------------

enum
{
	kOpenParenthesis_MojiKumiCharacterClass = 1,
	kCloseParenthesis_MojiKumiCharacterClass = 2,
	kCantBeginLineCharacters_MojiKumiCharacterClass = 3,
	kBreakingPunctuation_MojiKumiCharacterClass = 4,
	kMiddlePunctuation_MojiKumiCharacterClass = 5,
	kPeriods_MojiKumiCharacterClass = 6,
	kComma_MojiKumiCharacterClass = 7,
	kIndivisibleCharacters_MojiKumiCharacterClass = 8,
	kPreAbbreviationSymbols_MojiKumiCharacterClass = 9,
	kPostAbbreviationSymbols_MojiKumiCharacterClass = 10,
	kJapaneseSpace_MojiKumiCharacterClass = 11,
	kHiragana_MojiKumiCharacterClass = 12,
	kDoubleByteNumber_MojiKumiCharacterClass = 13,
	kOtherJapaneseCharacters_MojiKumiCharacterClass = 14,
	kSingleByteNumber_MojiKumiCharacterClass = 15,
	kRomanNonSpaceCharacters_MojiKumiCharacterClass = 16,
	kTopOrEndOfLine_MojiKumiCharacterClass = 17,
	kTopOfParagraph_MojiKumiCharacterClass = 18
};

// ----------------------------------------------------------------------------------

enum CompositeFontClassType
{
	kCompositeFontClassOverride = 0,

	// Following are predefined composite font class types
	kCompositeFontClassBase,					// Kanji class
	kCompositeFontClassKana,					// Full Width
	kCompositeFontClassPunctuation,
	kCompositeFontClassFWSymbolsAndAlphabetic,	// Full Width
	kCompositeFontClassHWSymbolsAndAlphabetic,	// Half Width
	kCompositeFontClassHWNumerals,				// Half Width
	kCompositeFontClassGaiji0,
	kCompositeFontClassGaiji1,
	kCompositeFontClassGaiji2,
	kCompositeFontClassGaiji3,
	kCompositeFontClassGaiji4,
	kCompositeFontClassGaiji5,
	kCompositeFontClassGaiji6,
	kCompositeFontClassGaiji7,
	kCompositeFontClassGaiji8,
	kCompositeFontClassGaiji9,
	kCompositeFontClassGaiji10,
	kCompositeFontClassGaiji11,

	kEndOfCompositeFontClassType
};
#ifndef __cplusplus
typedef enum CompositeFontClassType CompositeFontClassType;
#endif

// ----------------------------------------------------------------------------------

enum MojiKumiCodeClass
{
	kOpen = 1,
	kClose,
	kNoBegin,
	kEndPunctuation,
	kMiddlePunctuation,
	kPeriodPunctuation,
	kCommaPunctuation,
	kNotSeparatePunctuation,
	kSymbolsPrecedingNumbers,
	kSymbolsFollowingNumbers,
	kIdeographic,
	kHiragana,
	kDoubleByteNumbers,
	kOtherJapaneseCharacters,
	kSingleByteNumbers,
	kRomanNonSpace,
	kLineEdgeClass,
	kParagraphStartClass,

	kEndOfCodeClasses
};
#ifndef __cplusplus
typedef enum MojiKumiCodeClass MojiKumiCodeClass;
#endif

// ----------------------------------------------------------------------------------

enum ClassMetricRestriction
{
	// class metric restrictions for a composite font component.
	kClassMetricRestrictionNone = 0,
	kClassMetricRestrictionSize = 1 << 0,
	kClassMetricRestrictionBaseline =  1 << 1,
	kClassMetricRestrictionHorizontalScale =  1 << 2,
	kClassMetricRestrictionVerticalScale =  1 << 3,
	kClassMetricRestrictionCenterGlyph = 1 << 4
};
#ifndef __cplusplus
typedef enum ClassMetricRestriction ClassMetricRestriction;
#endif

// ----------------------------------------------------------------------------------

enum LineOrientation
{
	kHorizontalLines = 0,
	/// This is not supported by Illustrator
	kVerticalLeftToRight = 1,	
	kVerticalRightToLeft = 2
};
#ifndef __cplusplus
typedef enum LineOrientation LineOrientation;
#endif


// ----------------------------------------------------------------------------------

enum GlyphOrientation
{
	/// horizontal left to right
	kHorizontalGlyphs,				
	/// standard vertical -- straight from the font as vertical glyphs
	kVerticalGlyphs,			
	/// horizontal glyphs rotated 90 degrees to be on side in vertical
	kHorizontalGlyphsRotated,
	/// We are no longer setting this constant but it may exist in files:
	kVerticalUprightRomanGlyphs
};
#ifndef __cplusplus
typedef enum GlyphOrientation GlyphOrientation;
#endif

enum FrameType
{
	kPointTextFrame = 0,
	kInPathTextFrame = 1,
	kOnPathTextFrame = 2
};
#ifndef __cplusplus
typedef enum FrameType FrameType;
#endif

// ----------------------------------------------------------------------------------

enum SpellCheckingResult
{
	kNoProblems = 0,
	kUnknownWord = 1,	
	kRepeatedWords = 2,
	kUncappedStartOfSentence = 3
};
#ifndef __cplusplus
typedef enum SpellCheckingResult SpellCheckingResult;
#endif

// ----------------------------------------------------------------------------------

enum KinsokuPredefinedTag
{
	kUserDefinedKinsokuTag = 0,
	kPredefinedHardKinsokuTag = 1,
	kPredefinedSoftKinsokuTag = 2
};
#ifndef __cplusplus
typedef enum KinsokuPredefinedTag KinsokuPredefinedTag;
#endif

// ----------------------------------------------------------------------------------

enum MojikumiTablePredefinedTag
{
	kUserDefinedMojikumiTableTag = 0,
	kPredefinedYakumonoHankakuMojikumiTableTag = 1,
	kPredefinedYakumonoZenkakuMojikumiTableTag = 2,
	kPredefinedGyomatsuYakumonoHankakuMojikumiTableTag = 3,
	kPredefinedGyomatsuYakumonoZenkakuMojikumiTableTag = 4
};
#ifndef __cplusplus
typedef enum MojikumiTablePredefinedTag MojikumiTablePredefinedTag;
#endif

// ----------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#ifdef __MWERKS__
	#if defined(__MC68K__) || defined(__POWERPC__)
		#pragma enumsalwaysint reset
	#endif
#endif // __MWERKS__

#endif //__ATETypes__
