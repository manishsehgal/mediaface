#ifndef __AIRandom__
#define __AIRandom__

/*
 *        Name:	AIRandom.h
 *   $Revision: 6 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 8.0 Random Number Suite.
 *
 * Copyright (c) 1986-1998 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIRandom.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIRandomSuite			"AI Random Suite"
#define kAIRandomSuiteVersion3	AIAPI_VERSION(3)
#define kAIRandomSuiteVersion	kAIRandomSuiteVersion3
#define kAIRandomVersion		kAIRandomSuiteVersion


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** There are often times when a plug-in wants to add a degree of noise to the
	effect it is implementing. Illustrator provides two suites that aid in doing
	this: the AIRandomSuite and the AIRandomBellCurveSuite. The Random Suite
	gives an even distibution while the Random Bell Curve Suite yields numbers
	on a standard probability curve.
*/
typedef struct {

	/** Returns a random number in the range 0.0 to 1.0. */
	AIAPI AIReal (*FloatRnd) ( void );

	/** Sets the random number seed used to generate subsequent random
		numbers. Use this to restart a sequence of random numbers, for example for
		debugging.

@code
	sRandom->SetRndSeed( 1 );
	AIReal r = sRandom->FloatRnd(); // will always be the same
@endcode

		Note: The initial seed is based on the current date and time. */
	AIAPI void (*SetRndSeed) ( long seed );

	/** To restart a random number sequence, get the seed with GetRndSeed,
		generate the random numbers, then reset the seed with SetRndSeed and
		generate the sequence again.
		
		These two random loops will generate the same sequence:

@code
	AIReal r;
	short i;
	long seed = sRandom->GetRndSeed();
	for ( i = 0; i < 10; ++i ) {
		r = sRandom->FloatRnd();
	}
	sRandom->SetRndSeed( seed );
	for ( i = 0; i < 10; ++i ) {
		r = sRandom->FloatRnd();
	}
@endcode
	*/
	AIAPI long (*GetRndSeed) ( void );

} AIRandomSuite;

#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
