#ifndef __AIGroup__
#define __AIGroup__

/*
 *        Name:	AIGroup.h
 *   $Revision: 6 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Group Object Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/


#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIGroup.h */

/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIGroupSuite			"AI Group Suite"
#define kAIGroupSuiteVersion	AIAPI_VERSION(3)
#define kAIGroupVersion			kAIGroupSuiteVersion

/** There's more than one way to normalize a compound path. This enumeration
	lists the available algorithms. */
typedef enum
{
	/** Use the best algorithm currently available. */
	kAINormalizeCompoundPathBest,
	/** Use the Illustrator 10 algorithm. */
	kAINormalizeCompoundPathAI10,
	/** Forces the size of the enum to be 32-bits */
	kAIMax_NormalizeCompoundPathAlgorithm = 0xFFFFFFFF
} AINormalizeCompoundPathAlgorithm;

/*******************************************************************************
 **
 **	Suite
 **
 **/

/** The Group Suite provides specialized APIs for working with groups. The
	most common APIs for working with groups of objects such as traversing
	their children and reordering their contents can be found in the
	AIArtSuite.
	
	An Illustrator group object is an ordered collection of art objects.
	There are a number of different kinds of groups.
	
	- A normal group is nothing more than a user defined collection of
		objects typically created through the Group command. The group
		as a whole may have a style or transparency blending attributes
		attached to it but otherwise it does not affect the rendering
		of its contents.
	- A clip group is similar to a normal group except that some of its
		objects are designated as clipping objects. Each clipping object
		defines a clip path. The intersection of the clip paths is used to
		clip the rendering of the other objects.
	- A compound path is also a group. The contents of a compound path
		are restricted to be groups and path objects.
*/
typedef struct {

	/** Returns whether the group is a clip group. The AIArtSuite::GetArtUserAttr()
		API can be used to determine which members of the group are clipping
		objects. */
	AIAPI AIErr (*GetGroupClipped) ( AIArtHandle group, AIBoolean *clipped );
	/** Makes the group a clipping group. The AIArtSuite::SetArtUserAttr()
		API can then be used to set the objects that are clipping objects. */
	AIAPI AIErr (*SetGroupClipped) ( AIArtHandle group, AIBoolean clipped );

	/** Obsolete. Always returns true. */
	AIAPI AIErr (*GetGroupMaskLock) ( AIArtHandle group, AIBoolean *maskLocked );
	/** Obsolete. */
	AIAPI AIErr (*SetGroupMaskLock) ( AIArtHandle group, AIBoolean maskLocked );

	/**	This API normalizes the components of a compound path. It sorts them
		such that the object that appears to be on the "outside" is in the
		back and orders the points of the backmost so that the path is shown as
		not being reversed in the attributes palette. The "holes" of the compound
		path are stacked on top in a defined order. The stacking order of compound
		paths doesn't affect how they are filled, so it will not modify the fill
		area. It will, however, provide the following benefits:

		(1)	It will be easier to direct-select the outlines of the holes, since they
			will be in front, instead of the outer path getting hit-tested first.
		(2)	If they have a complex stroke applied to them, such as a brush stroke,
			the strokes for the holes will be in front of the strokes for the outer
			paths, which is probably the effect you want,
		(3)	When blending from one path to another, the component paths will be matched
			to the ones that the user is likely to think of as corresponding.

		It is especially important for Type Outlines, since curiously, the way fonts
		are designed is typically with the holes in the back, causing blends between
		single outline and multiple outline characters to match the single path of
		the one-path character to an inner path of the multiple-path character,
		without this step.

		There are a number of different algorithms for normalizing compound paths.
		The algorithm parameter allows the appropriate one to be selected.
	*/
	AIAPI AIErr (*NormalizeCompoundPath) ( AIArtHandle compound, AINormalizeCompoundPathAlgorithm algorithm );

} AIGroupSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
