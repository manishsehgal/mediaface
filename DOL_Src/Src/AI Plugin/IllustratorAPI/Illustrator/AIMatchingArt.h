#ifndef __AIMatchingArt__
#define __AIMatchingArt__

/*
 *        Name:	AIMatchingArt.h
 *   $Revision: 4 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 11.0 Matching Art Suite.
 *
 * Copyright (c) 1986-2002 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AILayerList__
#include "AILayerList.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIMatchingArt.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIMatchingArtSuite			"AI Matching Art Suite"
#define kAIMatchingArtSuiteVersion	AIAPI_VERSION(5)
#define kAIMatchingArtVersion		kAIMatchingArtSuiteVersion


/*******************************************************************************
 **
 **	Types
 **
 **/

/** An AIMatchingArtSpec is a filter for matching art objects that posess specific
	properties. An array of these is passed to the APIs of the AIMatchingArtSuite
	to specify the art objects that should be returned.

	The type field identifies the types of art objects that match the specification.
	See #AIArtType for the art object types. The special type kAnyArt can be
	used to match any kind of art object.

	The whichAttr and attr fields allow for filtering of art objects based on their
	user attributes. See #AIArtUserAttr for the attribute values. whichAttr specifies
	the collection of attributes to be considered when filtering objects. attr
	specifies the values those attributes must have to match an object. For
	example specifying kSelected for whichAttr and attr will match only art objects
	that are selected.
	
	There are a number of values in #AIArtUserAttr that are not art attributes
	but instead specify additional options to the matching process. These
	options should be specified in the whichAttr field and only need to be
	specified in one of the art specifications.
*/
typedef struct {
	short type;
	long whichAttr, attr;
} AIMatchingArtSpec;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** The Matching Art Suite allows you to: (1) get a list of currently selected art
	objects, or (2) get a list of art objects based on a matching criterion.
	The AIArtSetSuite provides another mechanism as does the AIArtSuite. The
	functions in the suite can request all available artwork or a subset. The
	artwork is return as a list of art objects, eliminating the need to traverse the
	tree.

	The calls in this suite flatten the requested hierarchy of objects into an art
	object array. There may be any combination of atomic objects, such as paths,
	and compound objects, such as groups. Your code will need to handle them
	accordingly.

	You need to handle the situation when a group is partially selected, that is
	not all its members are selected. In this case, both the group and art
	object(s) will be in the returned object array and will need to be treated
	correctly. If, for instance, your plug-in moves the objects in a group, you
	should ignore the member object(s) in the array so they are not moved
	twice.

	The matching art suite functions return the art as an array of AIArtHandle.
	It is the caller's responsibility to free the memory allocated using the
	AIMdMemorySuite.

	The matching art suite contains APIs that operate on the current layer list
	and APIs that specify the layer list on which they operate. See the
	AILayerListSuite for a discussion of layer lists.
*/
struct AIMatchingArtSuite {

	/** Many filters don't care about the organization of the artwork tree, but
		instead operate on the selected objects independently. GetSelectedArt is
		a shortcut for walking the document and finding the selected objects
		yourself. This API returns the selected art on the current layer list. */
	AIAPI AIErr (*GetSelectedArt) ( AIArtHandle ***matches, long *numMatches );
	/** GetMatchingArt is similar to GetSelectedArt in that it returns a list of
		AIArtHandles. This function, however, allows you to specify just what sorts
		of objects you want returned. In immediate contrast to GetSelectedArt, you
		can use GetMatchingArt to get a list of the unselected objects.
		You specify what you want by giving GetMatchingArt one or more
		AIMatchingArtSpec structures. Indicate the number of structures in
		numSpecs.This API returns the matching art on the current layer list */
	AIAPI AIErr (*GetMatchingArt) ( AIMatchingArtSpec *specs, short numSpecs,
			AIArtHandle ***matches, long *numMatches );

	// new to Illustrator 9.0

	/** Identical to GetSelectedArt() except that the layer list to be used is
		explicitly specified. */
	AIAPI AIErr (*GetSelectedArtFromLayerList) ( AILayerList list, AIArtHandle ***matches,
			long *numMatches );
	/** Identical to GetMatchingArt() except that the layer list to be used is
		explicitly specified. */
	AIAPI AIErr (*GetMatchingArtFromLayerList) ( AILayerList list, AIMatchingArtSpec *specs,
			short numSpecs, AIArtHandle ***matches, long *numMatches );
	
	// new to Illustrator 10.0

	/** Get the art used to determine the current path style, displayed in the color
	   palette. This is a convenience function, since the current path style is now
	   determined not just by the targeted or selected objects, but rather by a combination
	   of both. The art returned may include objects inside of graphs. The art will
	   always be on the current layer list. */
	AIAPI AIErr (*GetArtForCurrentPathStyle) ( AIArtHandle ***matches, long *numMatches );

	/** This function can be used to avoid some setup if there is nothing selected.
		Returns simply whether or not there are some art objects selected, in art editing mode.
		(If you want to know whether there is a text editing selection, call
		AIDocumentSuite::HasTextFocus(). IsArtSomeSelected() will return false when there
		is a text editing selection and when there is nothing selected at all.) */
	AIAPI AIBoolean (*IsSomeArtSelected) ( void );

	/** Test searchArt and its descendants if it is a group or other container against the specs.
		If the #kMatchDictionaryArt flag is set, will also search the dictionaries of searchArt
		and any of its descendants. This is useful for when only certain parts of the artwork
		are of interest, or when the searchArt is not in the regular artwork, e.g., if it is the
		definition art of a pattern, brush, or symbol. */
	AIAPI AIErr (*GetMatchingArtFromArt) ( AIArtHandle searchArt, AIMatchingArtSpec *specs,
			short numSpecs, AIArtHandle ***matches, long *numMatches );
	
	// new to Illustrator 11.0
	
	/** Deselect all objects in the document. */
	AIAPI AIErr (*DeselectAll) ( void );

};


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
