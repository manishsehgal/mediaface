#ifndef __AIColorConversion__
#define __AIColorConversion__

/*
 *        Name:	AIColorConversion.h
 *   $Revision: 6 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 8.0 Color Conversion Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIColorConversion.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIColorConversionSuite			"AI Color Conversion Suite"
#define kAIColorConversionVersion7		AIAPI_VERSION(7)
#define kAIColorConversionSuiteVersion	kAIColorConversionVersion7
#define kAIColorConversionVersion		kAIColorConversionSuiteVersion

/** The color spaces that can be converted between. */
enum AIColorConversionSpaceValue {
	kAIMonoColorSpace	=		0,
	kAIGrayColorSpace	=		1,
	kAIRGBColorSpace	=		2,
	kAIARGBColorSpace	=		3,
	kAICMYKColorSpace	=		4,
	kAIACMYKColorSpace	=		5,
	kAIAGrayColorSpace	=		6,
	kAILabColorSpace	=		7
};


/** @ingroup Errors */
#define kColorConversionErr			'CCER'


/*******************************************************************************
 **
 **	Types
 **
 **/

typedef AIReal AISampleComponent;

typedef unsigned char AIImageComponent;

/** Structure that defines color conversion options. For convenience 
	the structure has constructors so that it can be constructed and
	passed into the color conversion APIs directly as a parameter. */
class AIColorConvertOptions {
public:
	/** The purpose for performing the conversion. When converting CMYK
		to gray or RGB for preview or export the appropriate black
		preservation setting is taken into account. */
	enum Purpose {
		kDefault,
		kForPreview,
		kForExport,
		kDummy = 0xFFFFFFFFL
	};
	AIColorConvertOptions () :
		purpose(kDefault)
	{
	}
	/** Note that this constructor allows the options to be implicitly
		constructed from a purpose. */
	AIColorConvertOptions (const Purpose& _purpose) :
		purpose(_purpose)
	{
	}
	AIColorConvertOptions (const AIColorConvertOptions& src) :
		purpose(src.purpose)
	{
	}
	AIColorConvertOptions& operator= (const AIColorConvertOptions& src)
	{
		purpose = src.purpose;
		return *this;
	}

	Purpose purpose;
};

/*******************************************************************************
 **
 **	Suite
 **
 **/

/** The color conversion suite contains basic facilities for conversion of color
	values and image sample values beween color spaces.
	
	If color management is on, it uses the document profile if it has one, otherwise
	the profiles from the color settings dialog.

	If color management is off, device conversions are done.
*/
typedef struct {

	/** Convert a color from the source space to the destination. See
		#AIColorConversionSpaceValue for the possible values for srcSpace and
		dstSpace. The srcColor and dstColor parameters must be arrays holding
		the appropriate number of color values for the space. If inGamut is
		non-NULL then on return it indicates whether the source color was
		within the gamut of the destination space. */
	AIAPI AIErr (*ConvertSampleColor) ( long srcSpace, AISampleComponent *srcColor,
		                                long dstSpace, AISampleComponent *dstColor,
										const AIColorConvertOptions& options,
										ASBoolean *inGamut);
									   
	/** Convert an array of image pixels from the source space to the destination. See
		#AIColorConversionSpaceValue for the possible values for srcSpace and
		dstSpace. The srcColor and dstColor parameters must be arrays of "pixelCount"
		pixels with the appropriate number of components. The flag #kRasterInvertBits
		can be specified in order to indicate that the bits of a 1-bit color space
		are inverted. */
	AIAPI AIErr (*ConvertImageColor)   ( long srcSpace, AIImageComponent *srcColor,
		                               	 long dstSpace, AIImageComponent *dstColor,
									   	 long pixelCount, unsigned short flags,
										 const AIColorConvertOptions& options);

} AIColorConversionSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
