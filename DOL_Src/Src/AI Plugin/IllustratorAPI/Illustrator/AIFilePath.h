#pragma once
#ifndef __AIFilePath__
#define __AIFilePath__

/*
 *        Name:	AIFilePath.h
 *     Purpose:	Adobe Illustrator File Path Suite.
 *
 * Copyright (c) 2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#include "IAIFilePath.hpp"

extern "C" {

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIFilePath.h */

/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIFilePathSuite			"AI File Path Suite"
#define kAIFilePathSuiteVersion		AIAPI_VERSION(1)
#define kAIFilePathVersion			kAIFilePathSuiteVersion

struct AIFilePathSuite
{
	/// Create a new empty FilePath object.
	AIErr AIAPI (*NewFilePath)(ai::FilePath&);

	/// Dispose of a FilePath object (not a file).
	AIErr AIAPI (*DeleteFilePath)(ai::FilePath&);

	/// Copy a FilePath object (not a file).
	AIErr AIAPI (*Copy)(const ai::FilePath &src, ai::FilePath &dest);

	/// Query a FilePath object for emptiness.
	bool AIAPI (*IsEmpty)(const ai::FilePath&);

	/// Empty a FilePath object (set it to a null path).
	void AIAPI (*MakeEmpty)(ai::FilePath&);

	/// Test two FilePath objects for equality.
	/** Return true if they are identical, whether or not the file exists. If
		they are not identical and @e resolveLinks is false, return false. If
		@e resolveLinks is true, determine if the two FilePaths refer to the
		same file through links, aliases, and/or shortcuts by querying the
		file system.
		
		@param a, b			(in) Paths to compare.
		@param resolveLinks	(in) Resolve links, aliases, and/or shortcuts if a & b are not identical.
	*/
	bool AIAPI (*Equal)(const ai::FilePath &a, const ai::FilePath &b, const bool resolveLinks);

	/// Test if one FilePath is less than another (based on file path string)
	/** Return true if a < b

		@param a, b			(in) Paths to compare.
	*/
	bool AIAPI (*LessThan)(const ai::FilePath &a, const ai::FilePath &b);

	/// Return true if path starts with a delimiter.
	bool AIAPI (*StartsWithDelimiter)(const ai::FilePath&);

	/// Return true if path ends with a delimiter.
	bool AIAPI (*EndsWithDelimiter)(const ai::FilePath&);

	/// Add a component to the path and insert the appropriate delimiter character as necessary.
	/** Delimiters will not be added to the end of a path unless explicitly
		requested by sending a delimiter as the addend.

		@param addend		(in) Path to be added to @e augend
		@param augend		(in/out) Path to be lengthened
	*/
	AIErr AIAPI (*AddComponent)(const ai::UnicodeString &addend, ai::FilePath &augend);

	/// Remove the end component from the path.
	void AIAPI (*RemoveComponent)(ai::FilePath&);

	/// Add/remove a file extension. Dots will be added or ignored as appropriate so only one dot appears before the extension.
	void AIAPI (*AddExtension)(const ai::UnicodeString &ext, ai::FilePath&);
	void AIAPI (*RemoveExtension)(ai::FilePath&);

	/// Return true if the file or directory exists after querying the file system.
	/** Return value of the optional parameters not defined if file or directory
		does not exist. Set @e longPath and @e isFile to NULL to ignore.

		@param path			(in) Path to check.
		@param resolveLinks	(in) Resolve links, aliases, and shortcuts for @e longPath.
		@param longPath		(out, optional) A full long, Unicode version of 'path'. This can be used
							to convert short path names to their long counterparts. May be null.
		@param isFile		(out, optional) True if file, false if directory. May be null.
	*/
	bool AIAPI (*Exists)(const ai::FilePath &path, const bool resolveLinks,
								ai::UnicodeString *longPath, bool *isFile);

	///	Resolve alias or shortcut by querying file system.
	/**	File or directory must exist.
	*/
	AIErr AIAPI (*Resolve)(ai::FilePath &path);

	/*****************************************************************************/
	/* Set operations */

	/// Set a FilePath from a Unicode string.
	/**	String may be a path native to Windows, Unix, or Mac OS or a URL.
	*/
	AIErr AIAPI (*Set)(const ai::UnicodeString&, ai::FilePath&);

	/// Set a FilePath from a SPPlatformFileSpecification.
	void AIAPI (*SetFromSPFileSpec)(const SPPlatformFileSpecification&, ai::FilePath&);

	#ifdef MAC_ENV

	/// Set a FilePath from a CFString (Macintosh only).
	AIErr AIAPI (*SetFromCFString)(const CFStringRef, ai::FilePath&);

	/// Set a FilePath from an FSRef (Macintosh only).
	AIErr AIAPI (*SetFromFSRef)(const FSRef&, ai::FilePath&);

	/// Set a FilePath from an FSSpec (Macintosh only outside Illustrator core).
	AIErr AIAPI (*SetFromFSSpec)(const FSSpec&, ai::FilePath&);

	/// Set a FilePath from a CFURL (Macintosh only).
	void AIAPI (*SetFromCFURL)(const CFURLRef, ai::FilePath&);

	/// Set a FilePath from a Macintosh alias record (Macintosh only).
	OSStatus SetFromAlias(AliasHandle);

	#endif	// MAC_ENV

	/*****************************************************************************/
	/* Get operations */

	/// Return the file name including an extension, if applicable, but without the path.
	/**
		@param path			(in) FilePath from which to extract a file name.
		@param displayName	(in) Set to true to get the display name. See @ref DisplayNames.
		@param fileName		(out) File name string.
	*/
	AIErr AIAPI (*GetFileName)(const ai::FilePath &path, const bool displayName, ai::UnicodeString &fileName);

	/// Return the file name without the extension and path.
	AIErr AIAPI (*GetFileNameNoExt)(const ai::FilePath &path, ai::UnicodeString &fileNameNoExt);

	/// Return the file extension without the dot.
	AIErr AIAPI (*GetFileExtension)(const ai::FilePath &path, ai::UnicodeString &ext);

	/// Return the full path in notation native to the current platform.
	/**
		@param path			(in) FilePath for which to the full path.
		@param displayName	(in) Pass true to get the display name. See @ref DisplayNames.
		@param fullPath		(out) Path string.
	*/
	AIErr AIAPI (*GetFullPath)(const ai::FilePath &path, const bool displayName, ai::UnicodeString &fullPath);

	/// Return the directory ending with a delimiter, without the filename, in notation native to the current platform.
	/** If path represents a directory, the result will be the same as calling GetFullPath.
		@param path			(in) FilePath from which to get the directory.
		@param displayName	(in) Pass true to get the display name. See @ref DisplayNames.
		@param directory	(out) Directory name string.
	*/
	AIErr AIAPI (*GetDirectory)(const ai::FilePath &path, const bool displayName, ai::UnicodeString &directory);

	/// Return an OS-specific short version of the path. The file or folder must exist.
	/** On Windows the path will conform to 8.3 format. On Mac OS X, the name may
		be truncated and a file ID appended; If the file ID is required the path
		will not be valid across processes. This call requires accessing the file
		system.
	*/
	AIErr AIAPI (*GetShortPath)(const ai::FilePath &path, ai::UnicodeString &shortPath);

	AIErr AIAPI (*GetParent)(const ai::FilePath &path, ai::FilePath &parent);

	/// Return the relative path of 'path' to 'base'.
	/** Given a call like
		GetRelativePath("c:\files\links\logo.tif", "c:\files\illustrator\", ...),
		"..\links\logo.tif" would be returned. If there is no relative path between
		the two an error status and an empty path will be returned. For example
		this would happen if the paths referenced different logical drives. Aliases
		and shortcuts will not be resolved here.

		@param path			(in) The absolute path for which you want a relative path.
		@param base			(in) The absolute base path to which the return value is relative. Must be a directory.
		@param relative		(out) The relative path from base to path.
	*/
	//On hold due to lack of demand
	//AIErr AIAPI (*GetRelativePath)(const ai::FilePath &path, const ai::FilePath &base, ai::FilePath &relative);

	/// Return the absolute path for 'relative' from 'base'.
	/** Given the call
		GetAbsolutePath("..\links\logo.tif", "c:\files\illustrator\", ...),
		"c:\files\links\logo.tif" would be returned. Aliases and shortcuts will not be
		resolved here.

		@param relative		(in) The relative path for which you want an absolute path.
		@param base			(in) The absolute base path to which 'relative' is relative. Must be a directory.
		@param absolute		(out) The absolute path representing 'relative'.
	*/
	//On hold due to lack of demand
	//AIErr AIAPI (*GetAbsolutePath)(const ai::FilePath &relative, const ai::FilePath &base, ai::FilePath &absolute);

	/// Return the path component delimiter for the current platform (ex. "/", "\").
	char AIAPI (*GetDelimiter)(void);

	#ifdef MAC_ENV
	/// Return the volume and parent of the path (one pointer may be null) (Macintosh only).
	void AIAPI (*GetVolumeAndParent)(const ai::FilePath &path, FSVolumeRefNum *vol, UInt32 *parent);
	#endif

	/// Return MacOS-type creator and type information as four character codes.
	/** This routine is for Macintosh only outside Illustrator core. FSSpecs are
		emulated on Windows in the Illustrator core.
	*/
	AIErr AIAPI (*GetCreatorAndType)(const ai::FilePath &path, unsigned long *creator, unsigned long *type);

	/// Return path as a Uniform Resource Locator (URL).
	/** The resulting URL will consist of only ASCII characters with any special
		characters escaped with URL percent (\%) encoding.

		@param path			(in) Input path.
		@param displayName	(in) Pass true to get the display name. See @ref DisplayNames.
		@param url			(out) Output URL.
	*/
	AIErr AIAPI (*GetAsURL)(const ai::FilePath &path, const bool displayName, ai::UnicodeString &url);

	AIErr AIAPI (*GetAsSPPlatformFileSpec)(const ai::FilePath&, SPPlatformFileSpecification&);
	
	/// Return the parent of the object represented by path.
	/**	@e path and @e parent may be the same FilePath. If path is a directory, its
		parent directory will be returned. If path is a top-level volume, ex.
		"c:\", an error will be returned. This call does not access the file system
		so you may want to call Exists or Resolve to make sure you're working with
		a good path.
	*/
	#ifdef MAC_ENV

	/// Return an FSSpec created from the FilePath (Macintosh only outside Illustrator core).
	OSStatus AIAPI (*GetAsFSSpec)(const ai::FilePath&, FSSpec&);

	/// Return a CFString created from the FilePath (Macintosh only). Caller responsible for releasing.
	CFStringRef AIAPI (*GetAsCFString)(const ai::FilePath&);

	/// Return an FSRef created from the FilePath (Macintosh only).
	OSStatus AIAPI (*GetAsFSRef)(const ai::FilePath&, FSRef&);

	/// Return a CFURL created from the FilePath (Macintosh only).
	CFURLRef AIAPI (*GetAsCFURL)(const ai::FilePath&);

	/// Return a handle to a Macintosh alias record to the FilePath (Macintosh only).
	AliasHandle GetAsAlias() const;

	#endif	// MAC_ENV

	void AIAPI (*ResetVolumeCache)(void);
};

#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

} // extern "C"

#endif // __AIFilePath__
