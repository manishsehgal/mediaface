#ifndef __AIStringPool__
#define __AIStringPool__

/*
 *        Name:	AIStringPool.h
 *   $Revision: 4 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 String Pool Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIStringPool.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIStringPoolSuite		"AI String Pool Suite"
#define kAIStringPoolSuiteVersion	AIAPI_VERSION(2)
#define kAIStringPoolVersion		kAIStringPoolSuiteVersion


/** @ingroup Errors */
#define kStringPoolErr			'SPER'


/*******************************************************************************
 **
 **	Types
 **
 **/

typedef struct StringPool AIStringPool;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**	A string pool contains a collection of null terminated character sequences. No
	single string appears in the pool more than once. Given two strings from a pool
	it is therefore possible to compare them for equality by comparing their
	addresses. The application has a string pool that can be obtained by the
	AIRuntimeSuite::GetAppStringPool() API. Alternatively plugins can create their
	own pools.
 */
typedef struct {

	/** Create a new string pool. It is the plugin's responsibility to dispose the
		pool when no longer needed. */
	AIAPI AIErr (*AllocateStringPool) ( AIStringPool **pool );
	/** Dispose of a string */
	AIAPI AIErr (*DisposeStringPool) ( AIStringPool *pool );
	/** Get the entry for string from the pool. If the string is already in the pool
		then the pre-existing entry is returned. Otherwise a new entry is created
		and returned. */
	AIAPI AIErr (*MakeWString) ( AIStringPool *pool, const char *string, char **wstring );

} AIStringPoolSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
