#ifndef __AIRealBezier__
#define __AIRealBezier__

/*
 *        Name:	AIRealBezier.h
 *     Purpose:	Adobe Illustrator bezier suite.
 *
 * Copyright (c) 1986-2003 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIRealBezier.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIRealBezierSuite				"AI Real Bezier Suite"
#define kAIRealBezierSuiteVersion2		AIAPI_VERSION(2)
#define kAIRealBezierSuiteVersion		kAIRealBezierSuiteVersion2
#define kAIRealBezierVersion			kAIRealBezierSuiteVersion

/** Return values for AIRealBezierSuite::AdjustThroughPoint(). */
enum AIAdjustThroughPointResult {
	/** same slope, but opposite side of p0 */
	kAIBezierAdjustReflectedp1		= 1,
	/** same slope, but opposite side of p3 */
	kAIBezierAdjustReflectedp2		= 2,
	/** p0 to p1 has different slope */
	kAIBezierAdjustRotatedp1		= 4,
	/** p3 to p2 has different slope */
	kAIBezierAdjustRotatedp2		= 8
};

/*******************************************************************************
 **
 **	Typedefs
 **
 **/

/** A cubic bezier defined by its four control points. */
typedef struct {
	AIRealPoint	p0;
	AIRealPoint	p1;
	AIRealPoint	p2;
	AIRealPoint	p3;
} AIRealBezier, *AIRealBezierPtr;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** Utilities for working with cubic beziers */
typedef struct {

	/** Set a bezier from its four control points. */
	AIAPI void		(*Set)			(AIRealBezier* b, AIRealPoint* p0, AIRealPoint* p1,
									AIRealPoint* p2, AIRealPoint* p3);
	/** Translate the bezier by the vector. */
	AIAPI void		(*Offset)		(AIRealBezier* b, AIReal dh, AIReal dv);
	/** Apply the transformation matrix to the bezier. */
	AIAPI void		(*Transform)	(AIRealBezier* b, AIRealMatrix* m);
	/** Determine the point on the bezier corresponding to the parameter t. */
	AIAPI void		(*Evaluate)		(AIRealBezier* b, AIReal t, AIRealPoint* p);
	/** Determine the tangent to the bezier corresponding to the parameter t. */
	AIAPI void		(*Tangent)		(AIRealBezier* b, AIReal t, AIRealPoint* v);
	/** Determine the normal to the bezier corresponding to the parameter t. */
	AIAPI void		(*Normal)		(AIRealBezier* b, AIReal t, AIRealPoint* v);
	/** Split b at the parameter t returning the two halves as b1 and b2. */
	AIAPI void		(*Divide)		(AIRealBezier* b, AIReal t, AIRealBezier* b1,
									AIRealBezier* b2);
	/** Test whether the bezier intersects the supplied rectangle. */
	AIAPI AIBoolean	(*IntersectsRect)(AIRealBezier* b, AIRealRect* r, AIReal scale);

	// New for AI8.0.

	/** Calculate the length of the bezier. The flatness parameter controls the accuracy
		with which the curve is approximated. */
	AIAPI AIReal	(*Length)			(AIRealBezier* b, AIReal flatness);
	/** Equivalent to calling Evaluate() and Tangent() with half the work. */
	AIAPI void		(*PointAndTangent)	(AIRealBezier* b, AIReal t, AIRealPoint* p, AIRealPoint *v);

	/** Adjust the bezier such that it passes through the point p at the parameter value
		t. See #AIAdjustThroughPointResult for return values. */
	AIAPI unsigned short (*AdjustThroughPoint) (AIRealBezier* b, AIRealPoint* p, AIReal t);

	// New for AI12

	/** Given a length along a bezier that is less than the total length of the curve, determine the 
		corresponding t value. The flatness value controls the degree of subdivision : a value of 1e-2
		is suggested. */
	AIAPI AIErr 	(*TAtLength) ( AIRealBezier*		bez ,
								   AIReal				length ,
								   AIReal				total_length ,
								   AIReal				flatness,
								   AIReal*				t );
										     
} AIRealBezierSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
