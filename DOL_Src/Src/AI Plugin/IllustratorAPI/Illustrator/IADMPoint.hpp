/***********************************************************************/
/*                                                                     */
/* IADMPoint.hpp                                                        */
/* ADMPoint object wrapper class                                        */
/*                                                                     */
/* Copyright 1996-2002 Adobe Systems Incorporated.                     */
/* All Rights Reserved.                                                */
/*                                                                     */
/* Patents Pending                                                     */
/*                                                                     */
/* NOTICE: All information contained herein is the property of Adobe   */
/* Systems Incorporated. Many of the intellectual and technical        */
/* concepts contained herein are proprietary to Adobe, are protected   */
/* as trade secrets, and are made available only to Adobe licensees    */
/* for their internal use. Any reproduction or dissemination of this   */
/* software is strictly forbidden unless prior written permission is   */
/* obtained from Adobe.                                                */
/*                                                                     */
/***********************************************************************/

#ifndef __IADMPoint_hpp__
#define __IADMPoint_hpp__

/*
 * Includes
 */
 
#ifndef __ADMTypes__
#include "ADMStandardTypes.h"
#endif

/*
 * Wrapper Class
 */

struct IADMPoint : public ADMPoint
{
public:	
	IADMPoint();
	IADMPoint(const ADMPoint& p);
	IADMPoint(int h, int v);
	
	friend inline ADMBoolean operator == (const ADMPoint& a, const ADMPoint& b);
	friend inline ADMBoolean operator != (const ADMPoint& a, const ADMPoint& b);
	
	friend inline ADMPoint operator + (const ADMPoint& a, const ADMPoint& b);
	friend inline ADMPoint operator - (const ADMPoint& a, const ADMPoint& b);

	friend inline ADMPoint operator * (const ADMPoint& p, int s);
	friend inline ADMPoint operator * (int s, const ADMPoint& p);

	friend inline ADMPoint operator / (const ADMPoint& p, int s);

	friend inline ADMPoint operator - (const ADMPoint& p);

	void operator = (const ADMPoint& p);
	void operator += (const ADMPoint& p);
	void operator -= (const ADMPoint& p);

	void operator *= (int s);
	void operator /= (int s);
};

inline IADMPoint::IADMPoint()
{
}

inline IADMPoint::IADMPoint(const ADMPoint& p)	
{ 
	h = p.h; 
	v = p.v; 
}

inline IADMPoint::IADMPoint(int h, int v)
{ 
	this->h = h; this->v = v; 
}

inline ADMBoolean operator == (const ADMPoint& a, const ADMPoint& b)
{
	return a.h == b.h && a.v == b.v;
}
	

inline ADMBoolean operator != (const ADMPoint& a, const ADMPoint& b)
{
	return a.h != b.h || a.v != b.v;
}

inline ADMPoint operator + (const ADMPoint& a, const ADMPoint& b)
{
	return IADMPoint(a.h + b.h, a.v + b.v);
}

inline ADMPoint operator - (const ADMPoint& a, const ADMPoint& b)
{
	return IADMPoint(a.h - b.h, a.v - b.v);
}

inline ADMPoint operator * (const ADMPoint& p, int s)
{
	return IADMPoint(p.h * s, p.v * s);
}

inline ADMPoint operator * (int s, const ADMPoint& p)
{
	return IADMPoint(p.h * s, p.v * s);
}

inline ADMPoint operator / (const ADMPoint& p, int s)
{
	return IADMPoint(p.h / s, p.v / s);
}

inline ADMPoint operator - (const ADMPoint& p)
{
	return IADMPoint(-p.h, -p.v);
}

inline void IADMPoint::operator = (const ADMPoint& p) 
{
	h = p.h;
	v = p.v;
}

inline void IADMPoint::operator += (const ADMPoint& p) 
{ 
	h += p.h;
	v += p.v; 
}

inline void IADMPoint::operator -= (const ADMPoint& p) 
{ 
	h -= p.h; 
	v -= p.v; 
}

inline void IADMPoint::operator *= (int s)
{ 
	h *= s;
	v *= s; 
}

inline void IADMPoint::operator /= (int s)
{
	h /= s;
	v /= s; 
}

#endif
