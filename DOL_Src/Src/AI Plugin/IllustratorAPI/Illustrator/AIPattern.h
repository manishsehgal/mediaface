#ifndef __AIPattern__
#define __AIPattern__

/*
 *        Name:	AIPattern.h
 *   $Revision: 10 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Pattern Fill Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AIColor__
#include "AIColor.h"
#endif

#ifndef __AIPathStyle__
#include "AIPathStyle.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/** @file AIPattern.h */

/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIPatternSuite				"AI Pattern Suite"
#define kAIPatternSuiteVersion8		AIAPI_VERSION(8)
#define kAIPatternSuiteVersion		kAIPatternSuiteVersion8
#define kAIPatternVersion			kAIPatternSuiteVersion

#define kAIArtPatternChangedNotifier		"AI Art Pattern Changed Notifier"


/*******************************************************************************
 **
 **	Types
 **
 **/

typedef void (*AIPatternProcPtr)(AIArtHandle art);

typedef AIPatternProcPtr AIPatternUPP;

#define CallAIPatternProc(userRoutine, art)		\
		(*(userRoutine))((art))
#define NewAIPatternProc(userRoutine)		\
		(AIPatternUPP)(userRoutine)


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	Patterns in Adobe Illustrator are used to fill or stroke with a repeating tile
	of art objects. The application of a pattern consists of two parts: the pattern
	definition and the instance parameters.
	
	The pattern definition is the artwork that defines the pattern tile. It is a
	group of art objects with the rear-most object being a square that defines the
	size of the tile. The square must be parallel to the x and y axes.

	The instance parameters are defined by the AIPatternStyle structure. They define
	how a pattern is used to paint a particular instance of a fill or stroke. In
	particular they define a transformation matrix that is used to position the
	pattern for the painting operation.

	Use the Pattern Suite to create, delete, and modify patterns.
 */
typedef struct {

	/** Creates a new pattern and initializes it. */
	AIAPI AIErr (*NewPattern) ( AIPatternHandle *newPattern );
	/** Deletes a pattern from the document global list. If the pattern is used for
		an object�s fill, the fill will made black. */
	AIAPI AIErr (*DeletePattern) ( AIPatternHandle pattern );
	/** The art of the specified pattern is placed into the current document at the
		center of the current view. */
	AIAPI AIErr (*GetPattern) ( AIPatternHandle pattern );
	/** SetPattern collects the artwork selected in the document and creates a
		pattern out of it. The artwork must be usable as a pattern. Basically, this
		is any object or group of objects making up the pattern with the rear-most
		object being a square the size of the tile. The square must be parallel to
		the x and y axes. Using invalid art will result in an AIErr 105. */
	AIAPI AIErr (*SetPattern) ( AIPatternHandle pattern );
	/** Returns the number of patterns in the document global list. */
	AIAPI AIErr (*CountPatterns) ( long *count );
	/** Returns the nth pattern in the document global list. If a pattern with that
		index is not found (index is out of bounds) the value #kNameNotFoundErr is
		return in the error code.*/
	AIAPI AIErr (*GetNthPattern) ( long n, AIPatternHandle *pattern );
	/** The function returns a reference to the art defining a pattern. The art object
		returned will be of type #kGroupArt. The AIArtHandle returned can be processed
		as any other AIArtHandle. */
	AIAPI AIErr (*GetPatternArt) ( AIPatternHandle pattern, AIArtHandle *art );
	/** Sets the art used for the pattern to the group passed in art.
		The art object must be a group. The backmost object of the group must be a
		bounding rectangle with no fill and no stroke, and will be used in tiling the
		pattern. If you want to create a pattern with a particular fill, you would
		create a second rectangle with the appropriate fill and stroke above and
		with the same bounds as the bottommost one. If the backmost object of a group
		is not a no fill, no stroke rectangle, an error 105 will be returned. */
	AIAPI AIErr (*SetPatternArt) ( AIPatternHandle pattern, AIArtHandle art );
	/** Gets the pattern name. The returned name is a pascal string. */
	AIAPI AIErr (*GetPatternName) ( AIPatternHandle pattern, ai::UnicodeString& name );
	/** Sets the pattern name from the pascal string. Pattern names must be unique
		within a document's global list. */
	AIAPI AIErr (*SetPatternName) ( AIPatternHandle pattern, const ai::UnicodeString& name );
	/** Pass a Pascal string with the name of an existing pattern to the function,
		which returns the corresponding pattern handle. If a pattern with that name
		is not found, the value kNameNotFoundErr is returned in the error code. */
	AIAPI AIErr (*GetPatternByName) ( const ai::UnicodeString& name, AIPatternHandle *pattern );
	/** Call IteratePattern with a handle to a path object and a pointer to a path
		style with a pattern fill. The path will be broken into tiles the size of the
		pattern. For each tile, any artwork needed to draw that tile on the page is
		generated. The generated artwork is passed to the AIPatternProc supplied to
		IteratePattern, which can process it further. */
	AIAPI AIErr (*IteratePattern) ( AIArtHandle art, AIPathStyle *style, AIPatternProcPtr proc );
	/** Given a candidate name for a new pattern generates a name that is guaranteed
		to be unique. If the name is already unique it is not modified. */
	AIAPI AIErr (*NewPatternName) ( ai::UnicodeString& newName );
	/** Returns the bounds of the pattern tile. */
	AIAPI AIErr (*GetPatternTileBounds) ( AIPatternHandle pattern, AIRealRectPtr bounds );
	/** Returns true if the input pattern handle identifies a pattern in the current
		document's global list. */
	AIAPI AIBoolean (*ValidatePattern) ( AIPatternHandle pattern );

} AIPatternSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
