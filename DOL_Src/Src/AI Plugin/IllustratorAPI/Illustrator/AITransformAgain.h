#ifndef __AITransformAgain__
#define __AITransformAgain__

/*
 *        Name:	AITransformAgain.h
 *   $Revision: 6 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Transform Again Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIPlugin__
#include "AIPlugin.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AITransformAgain.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAITransformAgainSuite			"AI Transform Again Suite"
#define kAITransformAgainSuiteVersion	AIAPI_VERSION(2)
#define kAITransformAgainVersion		kAITransformAgainSuiteVersion

/** @ingroup Callers
	This is the transform again caller. See AITransformAgainSuite. */
#define kCallerAITransformAgain 		"AI Transform Again"

/** @ingroup Selectors
	This is the transform again selector. See AITransformAgainSuite. */
#define kSelectorAITransformAgain		"AI Go"


/*******************************************************************************
 **
 ** Types
 **
 **/

/** The contents of a transform again message. See AITransformAgainSuite. */
typedef struct {
	SPMessageData d;
} AITransformAgainMessage;


/*******************************************************************************
 **
 **	Suite Record
 **
 **/

/**	When Adobe Illustrator does a transformation using a built in tool or menu
	item, the user can repeat the action again easily by using the Transform
	Again command. The functions in this suite are used to register a plug-in to
	receive the transform again command. If a tool does a rotation in 3D, for
	instance, it could allow the user to repeat the 3D transform.
 
	If a plugin implements a transformation command or tool it should use this
	suite. When the command or tool is invoked it should call SetTransformAgain().
	This registers the plugin to receive the transform again command.

	When a plugin has registered to receive the transform again command and
	that command is selected it will receive a message with caller #kCallerAITransformAgain
	and selector #kSelectorAITransformAgain. In response to the message it
	should transform the current selected objects using the same parameters as
	the last transform it performed. The data of the message is defined by
	AITransformAgainMessage.
 */
typedef struct {

	/** The SetTransformAgain function is used after a transformation is done on
		some artwork to allow the transform to be repeated with the Transform
		Again menu. */
	AIAPI AIErr (*SetTransformAgain)( SPPluginRef self );
	/** Returns the plug-in that will handle the next transform again menu
		command. If the host application is the last to do a transformation, the
		function will return nil. */
	AIAPI AIErr (*GetTransformAgain)( SPPluginRef *plugin );

} AITransformAgainSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
