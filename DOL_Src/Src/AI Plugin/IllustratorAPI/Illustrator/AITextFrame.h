#ifndef __AITextFrame__
#define __AITextFrame__

/*
 *        Name:	AITextFrame.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 11.0 Text Frame Object Suite.
 *
 * Copyright (c) 1986-2002 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AIFONT__
#include "AIFont.h"
#endif

#ifndef __AIHitTest__
#include "AIHitTest.h"
#endif

#ifndef __ATESuites__
#include "ATESuites.h"
#endif

#ifdef __cplusplus
using ATE::TextRangesRef;
using ATE::TextRangeRef;
using ATE::TextFrameRef;
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AITextFrame.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAITextFrameSuite			"AI Text Frame Suite"
#define kAITextFrameSuiteVersion1	AIAPI_VERSION(1)
#define kAITextFrameSuiteVersion	kAITextFrameSuiteVersion1
#define kAITextFrameVersion			kAITextFrameSuiteVersion

#define kAITextFrameHitSuite			"AI Text Frame Hit Suite"
#define kAITextFrameHitSuiteVersion1	AIAPI_VERSION(1)
#define kAITextFrameHitSuiteVersion		kAITextFrameHitSuiteVersion1
#define kAITextFrameHitVersion			kAITextFrameHitSuiteVersion

/** Types of text frames */
typedef enum {
	kUnknownTextType = -1,
	kPointTextType,
	kInPathTextType,
	kOnPathTextType
} AITextType;

typedef AITextType		AITextFrameType;

/** Parts of a text object which can be hit */
enum AITextPart {
	kAITextNowhere				= 0,
	/** right on text */
	kAITextText					= 1,
	/** in port of either TextInPath or TextOnPath */
	kAITextInport				= 2,
	/** out port of either TextInPath or TextOnPath */
	kAITextOutPort				= 3,
	/** start point of TextOnPath */
	kAITextStart				= 4,
	/** middle point of TextOnPath */
	kAITextMiddle				= 5,
	/** end point of TextOnPath */
	kAITextEnd					= 6
};


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** The text frame suite is new to Illustrator 11. It contains APIs for managing art
	objects of type #kTextFrameArt.

	There are three types of text frames. These are point text, in path text and on
	path text. Point text is defined by an anchor point which defines the position
	of the first character. In path and on path text have an associated path object
	that defines the area the text flows into and along respectively.

	In path and on path text frames can be linked together. Unlike prior versions
	of Illustrator linked text frames do not have to be in the same group as each
	other. The text frames can be in different groups or even on different layers.
	Furthermore the linked frames do not have to be the same type. In path text can
	be linked to on path text and vice versa.

	The text content of a set of 1 or more linked frames is called a story. The
	ATE APIs are used to access and manipulate the story and its visual attributes.
	Other suites related to text are:

	- AIATECurrentTextFeaturesSuite
	- AIATEPaintSuite
	- AIATETextUtilSuite
	- AITextFrameHitSuite
 */
typedef struct {

	/** Create a new point text object, specifying paint order, and prep object as they are in
		NewArt() in AIArtSuite. AITextOrientation is defined in AIFont.h. anchor is the start point 
		of the baseline of the return point text object. */
	AIAPI AIErr (*NewPointText)		( short paintOrder, AIArtHandle prep, AITextOrientation orient,
									AIRealPoint anchor, AIArtHandle *newTextFrame );
	/** Create a new in path text object, specifying paint order, and prep object as they are in
		NewArt() in AIArtSuite. AITextOrientation is defined in AIFont.h. path is a PathObject 
		which will be turned to an InPathTextObject. If baseFrame is not NULL, the new text object 
		will be linked to it. append will decide whether it is append or preappend. */
	AIAPI AIErr (*NewInPathText)	( short paintOrder, AIArtHandle prep, AITextOrientation orient,
									AIArtHandle path, AIArtHandle baseFrame, bool append, AIArtHandle *newTextFrame );
	/** Create a new on path text object, specifying paint order, and prep object as they are in
		NewArt() in AIArtSuite. AITextOrientation is defined in AIFont.h. path is a PathObject 
		which will be turned to an OnPathTextObject. If baseFrame is not NULL, the new text object 
		will be linked to it. append will decide whether it is append or preappend. startT and endT 
		will be >= 0 and <= number of segments in path. startT < endT. if endT <= startT, endT will be 
		default to the number of segments, which maps to the end of path. */
	AIAPI AIErr (*NewOnPathText)	( short paintOrder, AIArtHandle prep, AITextOrientation orient,
									AIArtHandle path, AIReal startT, AIReal endT, AIArtHandle baseFrame,
									bool append, AIArtHandle *newTextFrame );
	/** Create a new on path text object, specifying paint order, and prep object as they are in
		NewArt() in AIArtSuite. AITextOrientation is defined in AIFont.h. path is a PathObject 
		which will be turned to an OnPathTextObject. If baseFrame is not NULL, the new text object 
		will be linked to it. append will decide whether it is append or preappend. anchor will be 
		the start point of text. */
	AIAPI AIErr (*NewOnPathText2)	( short paintOrder, AIArtHandle prep, AITextOrientation orient,
									AIArtHandle path, AIRealPoint anchor, AIArtHandle baseFrame,
									bool append, AIArtHandle *newTextFrame );

	/** Get the type of a text frame (point text, in path or on path) */
	AIAPI AIErr (*GetType)			( AIArtHandle textFrame, AITextFrameType *type );

	/** Get the orientation */
	AIAPI AIErr (*GetOrientation)	( AIArtHandle textFrame, AITextOrientation *orient );
	/** Set the orientation */
	AIAPI AIErr (*SetOrientation)	( AIArtHandle textFrame, AITextOrientation orient );

	/** Applies to PointText only.  #kBadParameter error will be returned for anything other than
		#kPointTextType. AITransformArtSuite can be used to change a #kPointTextType's anchor. */
	AIAPI AIErr (*GetPointTextAnchor)( AIArtHandle textFrame, AIRealPoint *anchor );

	/** Applies to InPath and OnPath text. The returned PathObject can be manipulated through AIPathSuite.
		If applied to a #kPointTextType object, pathObject will be set to NULL and kNoErr returned.
		Note: AIArtSuite::DuplicateArt() may be used to copy the pathObject, however the target location
		cannot  be specified in terms of the returned path object.  For example, you cannot ask to place
		the duplicate  path above or below the source path. */
	AIAPI AIErr (*GetPathObject)	( AIArtHandle textFrame, AIArtHandle *pathObject );
	
	/** startT <= endT */
	AIAPI AIErr (*GetOnPathTextTRange)( AIArtHandle textFrame, AIReal *startT, AIReal *endT );
	AIAPI AIErr (*SetOnPathTextTRange)( AIArtHandle textFrame, AIReal startT, AIReal endT );

	/** Gets you the ATE::TextRangeRef for the text contained by the frame. The TextRangeRef is
		reference counted. The client must release it when it is no longer needed. The best way
		to do this is to use the ITextRange class to manage the object. If this frame is the last
		one in the chain AND it has overflow text, then ITextRange(textRange).getEnd() will
		include the hidden text. */
	AIAPI AIErr (*GetATETextRange)	( AIArtHandle textFrame, TextRangeRef*	textRange );
	/** Gets you the ATE::TextFrameRef corresponding to the Illustrator text frame object. The
		TextFrameRef is reference counted. The client must release it when it is no longer needed.
		The best way to do this is to use the ITextFrame class to manage the object. */
	AIAPI AIErr (*GetATETextFrame)	( AIArtHandle textFrame, TextFrameRef*	ATE_textFrame );
	/** Gets the Illustrator text frame object corresponding to the ATE::TextFrameRef. */
	AIAPI AIErr (*GetAITextFrame)	( TextFrameRef	ATE_textFrame, AIArtHandle* textFrame );
	/** Gets you the ATE::TextRangesRef for the selected text contained by the frame. Note that
		selections may be discontiguous. The TextRangesRef is reference counted. The client must
		release it when it is no longer needed. The best way to do this is to use the ITextRanges
		class to manage the object. */
	AIAPI AIErr (*GetATETextSelection)( AIArtHandle textFrame, TextRangesRef*	Selection );
	/** Given an AIHitRef identifying a hit on a text frame object returns the ATE::TextRangeRef
		identifying the insertion point that corresponds to the hit. The AIHitRef can be obtained
		using the AIHitTestSuite. The TextRangeRef is reference counted. The client must release
		it when it is no longer needed. The best way to do this is to use the ITextRange class
		to manage the object. */
	AIAPI AIErr (*DoTextFrameHit)	( const AIHitRef hitRef, TextRangeRef*	textRange );
	
	/** Converts the text to outlines returning the resulting art object. If the text frame
		has an active style then it is applied to the resulting outlines. Other attributes
		including the opacity mask are not copied. If the text frame is set to clip then
		a single clipping compound path is created containing the outlines. The style and
		opacity mask are not applied to this compound path. The text frame is not deleted. */
	AIAPI AIErr (*CreateOutline)	( AIArtHandle textFrame, AIArtHandle *outline );
	
	/** You can link two in path text objects, after link they will share the same story. */
	AIAPI AIErr (*Link)				( AIArtHandle baseFrame, AIArtHandle nextFrame );

	/** You can unlink a text object from its current story:
		
		- if before is true, after is false, frames in the story will be broken into
			two groups right before this frame;
		- if before is false, after is true, frames in the story will be broken into
			two groups right after this frame;
		- if before is true, after is true, frame will be removed from the story and
			be itself;
		- if before is false, after is false, nothing happens.
	*/
	AIAPI AIErr (*Unlink)			( AIArtHandle frame, bool before, bool after );

	/** Create a new text object based on ATE (Adobe Text Engine) version 2 blob data, 
		specifying paint order, and prep object as they are in AIArtSuite::NewArt(). 
		data and size refer to binary blob. index is the index of text object in the 
		SLO TextObject list. List is zero based. */
	AIAPI AIErr (*CreateATEV2Text)	( short paintOrder, AIArtHandle prep, void* data,
									long size, long index, AIArtHandle *newTextFrame );

	/** Return ATE (Adobe Text Engine) version 2 blob data for the current arwork.
		client is in charge of free memory for data. Please call SPFreeBlock(*data). */
	AIAPI AIErr (*GetATEV2Data)		( void** data, long* size );

	/** Return the story index of a given frame. index will be set to -1 
		if the frame does not belong to the current artwork or the frame is part of 
		result art of plugin group or styled art. */
	AIAPI AIErr (*GetStoryIndex)	( AIArtHandle frame, long* index );

	/** Return the frame index of a given frame in its story. index will be set to -1 
		if the frame does not belong to the current artwork or the frame is part of 
		result art of plugin group or styled art. */
	AIAPI AIErr (*GetFrameIndex)	( AIArtHandle frame, long* index );

	/** Check whether a give frame is part of a linked text */
	AIAPI AIErr (*PartOfLinkedText)	( AIArtHandle frame, bool* linked );

} AITextFrameSuite;


/** The AIHitTestSuite returns an AIHitRef which identifies the part of the object
	hit. If the object is a text frame then the methods of this suite may be used to
	further interrogate the hit.
 */
typedef struct {

	/** GetPart returns an identifier indicating the element of the text frame which
		was hit. See AITextPart */
	AIAPI long (*GetPart) (AIHitRef hit);

} AITextFrameHitSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
