#ifndef __AIMaskFlattener__
#define __AIMaskFlattener__

/*
 *        Name:	AIMaskFlattener.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 12.0 Mask Flattener Suite.
 *
 * Copyright (c) 1986-2005 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AIArtSet__
#include "AIArtSet.h"
#endif

#ifndef __AIPathStyle__
#include "AIPathStyle.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIMaskFlattener.h */

/*******************************************************************************
 **
 **	Suite name and version
 **
 **/

#define kAIMaskFlattenerSuite					"AI Mask Flattener Suite"
#define kAIMaskFlattenerSuiteVersion3			AIAPI_VERSION(3)
#define kAIMaskFlattenerSuiteVersion			kAIMaskFlattenerSuiteVersion3
#define kAIMaskFlattenerVersion					kAIMaskFlattenerSuiteVersion

/*******************************************************************************
 **
 **	Constants
 **
 **/


#define kAIFlattenDefaultResolution (300.0f)
#define kAIFlattenMinResolution (1.0f)
#define kAIFlattenMaxResolution (9600.0f)
#define kAIFlattenMinBalance 0
#define kAIFlattenMaxBalance 100

/** The maximum number of distinct spot colors that can appear in artwork to be flattened. */
#define kAIFlattenMaxSpots			27

#define kAIFlattenHasLinkErr		'FLnk'
#define kAIFlattenTooManySpotsErr	'FSpt'


/*******************************************************************************
 **
 **	Types
 **
 **/

/** Flag values that may be or'ed together to specify flattening options. */
typedef enum
{
	kAIFlattenDefault 						= 0,
	/** Output text outlines instead of native text */
	kAIFlattenOutlineText					= 1 << 1,
	/** Converts stroke to fill even when object is not involved in transparency */
	kAIFlattenOutlineStrokes				= 1 << 2,
	/** */
	kAIFlattenClipComplexRegions			= 1 << 3,	
	/** Preserve overprint (rather than simulating it) in opaque regions */
	kAIFlattenPreserveOverprints			= 1 << 4,
	/** Show a progress bar */
	kAIFlattenShowProgress   				= 1 << 7,	
	/** Preserve simple transparency (flatten blend mode) */
	kAIFlattenPreserveSimpleTransparency	= 1 << 9,  
	/** Always use the planar map, even on opaque regions */
	kAIFlattenPlanarizeOpaqueRegions		= 1 << 10,

	/** Flattener will not call UndoChanges on an error (or cancel).
		Intended for clients that call the flattener, but handle their own undo, i.e. live effects */
	kAIFlattenDontUndoChangesOnError		= 1 << 11,	
	/** By default flattener may produce kForeignArt art to preserve things like spot colors
		Setting this flag will cause result in no foreign objects in the flattening result, but 
		the process is lossy. See kAIFOConversionFull in AIFOConversion.h */
	kAIFlattenFOConversionFull				= 1 << 12,

	/** This flag converts the paths output by the flattener into simplified (non-self-intersecting)
		paths. See #kAIFOConversionSimplifyPaths in AIFOConversion.h */
	kAIFlattenFOConversionSimplifyPaths		= 1 << 13,

	/** Normally artwork is flattened into a non-kockout group. Setting this flag causes it to
		be flattened into a knockout group. */
	kAIFlattenIntoKnockoutGroup				= 1 << 14,

	/** Set this flag to disallow colorized grayscale images in the flattened output. */
	kAIFlattenDisallowColorizedGray			= 1 << 15

} AIFlatteningFlags;


/** AI9's "flattening quality" setting is replaced in AI10 with
	kAI10_RasterVectorBalance, which is an integer from 1 (most rasters)
	to 99 (most vectors), along with four specific toggles: kAI10_OutlineText,
	kAI10_OutlineStrokes, kAI10_ClipComplexRegions, and kAI10_PreserveOverprints. */
typedef enum
{
	kAIFlatteningQualityLevel0, // lower quality - more rasters
	kAIFlatteningQualityLevel1,
	kAIFlatteningQualityLevel2,
	kAIFlatteningQualityLevel3,
	kAIFlatteningQualityLevel4, // higher quality - more vectors
	kAIFlatteningQualityLevelCount
		
} AIFlatteningQuality;


/** Flattening options */
typedef struct
{
	/** See AIFlatteningFlags */
	unsigned long flags;
	/** integer from 1 (most rasters) to 99 (most vectors) */
	unsigned short balance;
	/** Rasterization resolution for high frequency areas */
	AIReal rasterResolution;
	/** Rasterization resolution for low frequency areas (smooth shades) */
	AIReal meshResolution;
	
	/** */
	struct
	{
		/** usually 0.0 */
		AIReal start;
		/** usually 1.0 */
		AIReal end;
	} progress;
	
} AIFlatteningOptions;


/** CollectDocumentFlatteningInfo can be used to collect information about how
	a document will be flattened. The information is sent as a sequence of
	information snippets to a caller supplied receiver. Each snippet contains
	a single piece of information about how the document will be flattened.
	
	A receiever should not depend on these being the only types of information
	that can be sent. Future versions may send additional information without
	incrementing the API version so long as it does not change the meaning of
	the information sent by prior versions. */
typedef enum
{
	kAIFlatteningInfoLinkedEPSObject,
	kAIFlatteningInfoPlacedArtObject
} AIFlatteningInfoType;

/** Linked EPS object -- indicates that a linked EPS was drawn as a part of
	the artwork to be flattened. Provides the handle to the linked EPS object and a
	boolean indicating whether or not the linked EPS interracts with transparency.
	A linked EPS which does not interract with transparency can be passed
	through to a PostScript device for printing purposes. One which does
	interract needs to be parsed into objects for correct printing.
	
	The parseOK parameter indicates whether the contents of the linked EPS
	can be succesfully parsed for print purposes. For example, as of AI11
	a linked DCS could not be parsed in order to recover the original
	content.

	Since a linked EPS is a placed object a receiver will be sent both this
	snippet and the AIFlatteningInfoPlacedArtObject snippet. The order in
	which these will be sent is not guaranteed. In fact all linked EPS
	snippets will usually be sent as a group at the end. */
typedef struct
{
	AIArtHandle object;
	AIBoolean interacts;
	AIBoolean parseOK;
} AIFlatteningInfoLinkedEPSObject;

/** Placed object -- indicates that a placed object was drawn as a part of
	the artwork to be flattened. */
typedef struct
{
	AIArtHandle object;
} AIFlatteningInfoPlacedArtObject;

/** The information packet sent to an AIFlatteningInfoReceiver. */
typedef struct
{
	/** Indicates the type of information being sent. */
	AIFlatteningInfoType what;
	/** Contains the information. The valid member is determined based on
		the type of information. */
	union
	{
		AIFlatteningInfoLinkedEPSObject linkedEPSObject;
		AIFlatteningInfoPlacedArtObject placedArtObject;
	} x;
} AIFlatteningInfoSnippet;

/** Prototype for a receiver of flattening information. */
typedef AIErr (*AIFlatteningInfoReceiver) (void* receiverdata, AIFlatteningInfoSnippet* snippet);


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	The transparency flattening APIs provide facilities for flattening
	transparent artwork into an equivalent collection of opaque artwork.
 */
typedef struct {

	AIAPI AIErr (*FlattenArt)(AIArtSet artSet, AIFlatteningOptions *options, short paintOrder, AIArtHandle prep);
	AIAPI AIErr (*CreateTemporaryFlatteningLayer)(AILayerHandle *layer);
	/** The flattner info is obtained from document's dict which is set from Document Setup dialog. */
	AIAPI AIErr (*FlattenDocumentToLayer)(AILayerHandle layer);
	AIAPI AIErr (*RemoveTemporaryFlatteningLayer)();
	AIAPI AIErr (*IsTemporaryFlatteningLayer)(AILayerHandle layer, AIBoolean *flattening);
	AIAPI AIErr (*ReportError)(AIErr error);

	AIAPI AIErr (*CollectDocumentFlatteningInfo)(AIFlatteningInfoReceiver receiver, void* receiverdata);

	/** The flattner info is obtained from document's dict which is set from AI or EPS Save Options dialog. */
	AIAPI AIErr (*FlattenAIDocumentToLayer)(AILayerHandle layer, ASBoolean isEPS);
	
} AIMaskFlattenerSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
