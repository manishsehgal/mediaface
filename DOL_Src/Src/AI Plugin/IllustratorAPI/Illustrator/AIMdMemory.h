
#ifndef __AIMdMemory__
#define __AIMdMemory__

/*
 *        Name:	AIMdMemory.h
 *   $Revision: 1 $
 *      Author:	
 *        Date:
 *     Purpose:	Adobe Illustrator 6.0 Handle based memory suite
 *
 *				AIMdMemory provide a Machine dependent (Md) memory suite.  
 *				i.e. It use NewHandle() on the mac and GlobalAlloc on MSWINDOWS
 *				to allocate handle base memory.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIMdMemory.h */

/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIMdMemorySuite		"AI MdMemory Suite"
#define kAIMdMemorySuiteVersion		AIAPI_VERSION(1)
#define kAIMdMemoryVersion			kAIMdMemorySuiteVersion


/*******************************************************************************
 **
 ** Types
 **
 **/

typedef void **AIMdMemoryHandle;

/*******************************************************************************
 **
 **	Suite Record
 **
 **/

/** The AIMdMemorySuite provides APIs for allocating and disposing handles. A handle
	is a pointer to a pointer to a block of memory. Usage of handles is deprecated
	but is still necessary for some APIs. For example, the AIMatchingArtSuite
	returns a handle that must be freed by the client. */
typedef struct {

	AIAPI AIErr (*MdMemoryNewHandle) ( long size, AIMdMemoryHandle *hMem );
	AIAPI AIErr (*MdMemoryDisposeHandle) ( AIMdMemoryHandle hMem );
	AIAPI AIErr (*MdMemoryGetSize) ( AIMdMemoryHandle hMem, long *size );
	AIAPI AIErr (*MdMemoryResize) ( AIMdMemoryHandle hMem, long newSize );
	AIAPI AIErr (*MdMemoryLock) ( AIMdMemoryHandle hMem, void **lockedPtr );
	AIAPI AIErr (*MdMemoryUnLock) ( AIMdMemoryHandle hMem );

} AIMdMemorySuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif