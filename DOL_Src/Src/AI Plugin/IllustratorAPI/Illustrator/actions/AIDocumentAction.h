#ifndef _AICOREDOCUMENTACTION_H_
#define _AICOREDOCUMENTACTION_H_

/*
 *        Name:	AICoreDocumentAction.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Actions defined in the core.
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifndef __AIActionManager_h__
#include "AIActionManager.h"
#endif

// -----------------------------------------------------------------------------
// Action: kAINewDocumentAction
// Purpose: makes a new document
// Parameters:
//	- kAINewDocumentNameKey, string: the document title. if omitted it will be
//			named 'untitled-n'.
//	- kAINewDocumentColorModelKey, integer: the color model to use--rgb or cmyk.
//			the allowed values are kDocRGBColor or kDocCMYKColor defined in
//			AIDocument.h
//	- kAINewDocumentWidthKey, real: the page width in points
//	- kAINewDocumentHeightKey, real: the page height in points
//	- kAINewDocumentRulerUnitsKey, integer: the ruler units to be used (defined in AIDocument.h)
// -----------------------------------------------------------------------------

#define kAINewDocumentAction							"adobe_newDocument"
const ActionParamKeyID kAINewDocumentNameKey			= 'name'; // string
const ActionParamKeyID kAINewDocumentColorModelKey		= 'colr'; // integer
const ActionParamKeyID kAINewDocumentWidthKey			= 'wdth'; // real
const ActionParamKeyID kAINewDocumentHeightKey			= 'heit'; // real
const ActionParamKeyID kAINewDocumentRulerUnitsKey		= 'rulr'; // integer
// -----------------------------------------------------------------------------
// Action: kAIOpenDocumentAction
// Purpose: Open a document
// Parameters:
//	- kAIOpenDocumentNameKey, string: is the name of the file to open.
//	- kAIOpenDocumentColorModelKey, integer: is the color model to use--rgb or cmyk.
//			the allowed values are kDocRGBColor or kDocCMYKColor defined in
//			AIDocument.h
// -----------------------------------------------------------------------------

#define kAIOpenDocumentAction							"adobe_openDocument"
const ActionParamKeyID kAIOpenDocumentNameKey			= 'name'; // string
const ActionParamKeyID kAIOpenDocumentColorModelKey		= 'colr'; // integer

// -----------------------------------------------------------------------------
// Action: kAISaveDocumentAction
// Purpose: Save the current document
// Parameters: None
// Comments:
//	The document is saved under its current name and according to its current
//	format.
// -----------------------------------------------------------------------------

#define kAISaveDocumentAction							"adobe_saveDocument"

// -----------------------------------------------------------------------------
// Action: kAISaveDocumentAsAction
// Purpose: Save the current document
// Parameters:
//	- kAISaveDocumentAsNameKey, string: is the file name to save as. if no name
//			is specified a dialog will be presented.
//	- kAISaveDocumentAsFormatKey, string: is the format to save as. if unspecified
//			the current format of the file will be used. See the action headers
//			for individual formats for definitions of the format strings.
//	- kAISaveDocumentAsGetParamsKey, bool: controls whether the file format
//			presents its options dialog. if true the options dialog is presented.
//			if omitted no dialog will be presented.
// Comments:
//	Allows a name and format to be specified.
//	Additional parameters can be specified for the action dependent on the
//	target file format. See the action information for individual format
//	for the definitions of these parameters.
// -----------------------------------------------------------------------------

#define kAISaveDocumentAsAction							"adobe_saveDocumentAs"
const ActionParamKeyID kAISaveDocumentAsNameKey			= 'name'; // string
const ActionParamKeyID kAISaveDocumentAsFormatKey		= 'frmt'; // string
const ActionParamKeyID kAISaveDocumentAsGetParamsKey	= 'gprm'; // bool

// -----------------------------------------------------------------------------
// Action: kAIRevertDocumentAction
// Purpose: Revert the current document to its state when last saved
// Parameters: None
// -----------------------------------------------------------------------------

#define kAIRevertDocumentAction							"adobe_revertDocument"

// -----------------------------------------------------------------------------
// Action: kAICloseDocumentAction
// Purpose: Close the current document
// Parameters:
//	- kAICloseAllDocumentsKey, bool: specifies whether to close just the current
//			or all documents.
//	- kAICloseAndSaveDocumentKey, bool: specifies whether to save documents
//			before closing.
// -----------------------------------------------------------------------------

#define kAICloseDocumentAction							"adobe_closeDocument"
const ActionParamKeyID kAICloseAllDocumentsKey			= 'all.'; // bool
const ActionParamKeyID kAICloseAndSaveDocumentKey		= 'save'; // bool

// -----------------------------------------------------------------------------
// Action: kAIExportDocumentAction
// Purpose: Export the current document
// Parameters:
//	- kAIExportDocumentNameKey, string: is the file name to export as.
//	- kAIExportDocumentFormatKey, string: is the format to export as. if unspecified
//			the export dialog is presented.
//	- kAIExportDocumentExtensionKey, string: is the file extension to use. if
//			unspecified the export dialog is presented.
// Comments:
//	The combination of kAIExportDocumentFormatKey and kAIExportDocumentExtensionKey
//	are used to select the exporter to use. Additional parameters can be specified
//	for the action dependent on the target file format. See the action headers for
//	individual formats for definitions of the format strings, extensions and
//	additional parameters.
// -----------------------------------------------------------------------------

#define kAIExportDocumentAction							"adobe_exportDocument"
const ActionParamKeyID kAIExportDocumentNameKey			= 'name'; // string
const ActionParamKeyID kAIExportDocumentFormatKey		= 'frmt'; // string
const ActionParamKeyID kAIExportDocumentExtensionKey	= 'extn'; // string

// -----------------------------------------------------------------------------
// Action: kAIPrintDocumentAction
// Purpose: Print the current document
// Parameters: None
// -----------------------------------------------------------------------------

#define kAIPrintDocumentAction							"adobe_printDocument"

// -----------------------------------------------------------------------------
// Action: kAIPlaceDocumentAction
// Purpose: Place a file
// Parameters: None
//	- kAIPlaceDocumentActionNameKey, string: is the name of the file to place
//	- kAIPlaceDocumentActionLinkKey, bool: is true to link and false to embed
//	- kAIPlaceDocumentActionReplaceKey, bool: is true to replace the current selection
//	- kAIPlaceDocumentActionTemplateKey, bool: is true to create a template layer
//			and place the file on that layer
// Comments:
//	Additional parameters can be specified for the action dependent on the
//	target file format. See the action headers for individual formats for
//	definitions of the additional parameters.
// -----------------------------------------------------------------------------

#define kAIPlaceDocumentAction							"adobe_placeDocument"
const ActionParamKeyID kAIPlaceDocumentActionNameKey	= 'name'; // string
const ActionParamKeyID kAIPlaceDocumentActionLinkKey	= 'link'; // bool
const ActionParamKeyID kAIPlaceDocumentActionReplaceKey = 'rplc'; // bool
const ActionParamKeyID kAIPlaceDocumentActionTemplateKey = 'tmpl'; // bool


#endif
