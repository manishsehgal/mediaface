#ifndef _AIENVELOPEACTION_H_
#define _AIENVELOPEACTION_H_

/*
 *        Name:	AIEnvelopeAction.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Actions for envelopes.
 *
 * Copyright 2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifndef __AIActionManager_h__
#include "AIActionManager.h"
#endif

// Action 'menu' identifiers; stored as actionTypeEnum values when recorded
#define kAIEnvelopeActionMenuRelease			0
#define kAIEnvelopeActionMenuOptions			1
#define kAIEnvelopeActionMenuExpand				2
#define kAIEnvelopeActionMenuToggle				3

// Action 'type' identifiers; stored as actionTypeEnum values when recorded
#define kAIEnvelopeActionTypeWarp				0
#define kAIEnvelopeActionTypeMesh				1
#define kAIEnvelopeActionTypeTopObject			2



// -----------------------------------------------------------------------------
// Action: kAIEnvelopeMenuAction
// Purpose: runs some functionality accessible through the Envelope submenus
//
// Parameters: 
//	- kAIEnvelopeMenuItem, enum: the menu item to run
//								Release: kAIEnvelopeActionMenuRelease
//								Options: kAIEnvelopeActionMenuOptions
//								Expand:  kAIEnvelopeActionMenuExpand
//								Toggle Edit Contents: kAIEnvelopeActionMenuToggle
//
// The following parameters are needed only if kAIEnvelopeMenuItem == kAIEnvelopeActionMenuOptions
//
//	- kAIEnvelopeAntiAliasRaster, bool: whether to antialias rasters in envelopes
//	- kAIEnvelopePreserveRasterShape, enum: how to preserve raster shapes
//								1 => use transparency
//								0 => use clipping mask
//	- kAIEnvelopeFidelity, integer: the fidelity value [0..100]
//	- kAIEnvelopeDistortAppearance, bool: whether to distort appearances
//	- kAIEnvelopeDistortPatterns, bool: whether to distort patterns
//	- kAIEnvelopeDistortGradients, bool: whether to distort gradients
// -----------------------------------------------------------------------------
#define kAIEnvelopeMenuAction							"ai_plugin_envelope"
const ActionParamKeyID kAIEnvelopeMenuItem				= 'menu'; // enum
const ActionParamKeyID kAIEnvelopeAntiAliasRaster		= 'aars'; // bool
const ActionParamKeyID kAIEnvelopePreserveRasterShape	= 'alph'; // enum
const ActionParamKeyID kAIEnvelopeFidelity				= 'fidt'; // integer
const ActionParamKeyID kAIEnvelopeDistortAppearance		= 'app.'; // bool
const ActionParamKeyID kAIEnvelopeDistortPatterns		= 'patt'; // bool
const ActionParamKeyID kAIEnvelopeDistortGradients		= 'grad'; // bool



// -----------------------------------------------------------------------------
// Action: kAIEnvelopeMakeAction
// Purpose: makes an envelope from the selected objects on the artboard
// Parameters: 
//	- kAIEnvelopeMakeType, enum: how to make the envelope
//								Make with Warp:	kAIEnvelopeActionTypeWarp
//								Make with Mesh:	kAIEnvelopeActionTypeMesh
//								Make with Top Object: kAIEnvelopeActionTypeTopObject
//
// The following parameters are used if kAIEnvelopeMakeType == kAIEnvelopeActionTypeMesh
//	- kAIEnvelopeNumRows, integer: number of rows
//	- kAIEnvelopeNumCols, integer: number of columns
//
// The following parameters are used if kAIEnvelopeMakeType == kAIEnvelopeActionTypeWarp
//	- kAIEnvelopeWarpStyle, enum: AIWarpStyle, defined in AIEnvelope.h
//	- kAIEnvelopeBend, unit real (using unitPercent): bend value [-100..100]
//	- kAIEnvelopeHorizDistort, unit real (using unitPercent):	horizontal distort [-100..100]
//	- kAIEnvelopeVertDistort, unit real (using unitPercent): vertical distort [-100..100]
//	- kAIEnvelopeRotate90, enum: whether bend is applied horizontally or vertically
//								1 => vertical warp
//								0 => horizontal warp

// -----------------------------------------------------------------------------
#define kAIEnvelopeMakeAction							"ai_plugin_envelope_make"
const ActionParamKeyID kAIEnvelopeMakeType				= 'type'; // enum
const ActionParamKeyID kAIEnvelopeNumRows				= 'rows'; // integer
const ActionParamKeyID kAIEnvelopeNumCols				= 'cols'; // integer
const ActionParamKeyID kAIEnvelopeWarpStyle				= 'warp'; // enum
const ActionParamKeyID kAIEnvelopeBend					= 'bend'; // unit real
const ActionParamKeyID kAIEnvelopeHorizDistort			= 'horz'; // unit real
const ActionParamKeyID kAIEnvelopeVertDistort			= 'vert'; // unit real
const ActionParamKeyID kAIEnvelopeRotate90				= 'rot.'; // enum



// -----------------------------------------------------------------------------
// Action: kAIEnvelopeResetAction
// Purpose: resets the selected envelopes on the artboard
// Parameters: 
//	- kAIEnvelopeResetType, enum: how to reset the envelope
//								Reset with Warp:	kAIEnvelopeActionTypeWarp
//								Reset with Mesh:	kAIEnvelopeActionTypeMesh
//
// The following parameters are used if kAIEnvelopeResetType == kAIEnvelopeActionTypeMesh
//	- kAIEnvelopeNumRows, integer: number of rows
//	- kAIEnvelopeNumCols, integer: number of columns
//	- kAIEnvelopeMaintainShape, bool: whether to maintain envelope shape
//
// The following parameters are used if kAIEnvelopeResetType == kAIEnvelopeActionTypeWarp
//	- kAIEnvelopeWarpStyle, enum: AIWarpStyle, defined in AIEnvelope.h
//	- kAIEnvelopeBend, unit real (using unitPercent): bend value [-100..100]
//	- kAIEnvelopeHorizDistort, unit real (using unitPercent):	horizontal distort [-100..100]
//	- kAIEnvelopeVertDistort, unit real (using unitPercent): vertical distort [-100..100]
//	- kAIEnvelopeRotate90, enum: whether bend is applied horizontally or vertically
//								1 => vertical warp
//								0 => horizontal warp

// -----------------------------------------------------------------------------
#define kAIEnvelopeResetAction							"ai_plugin_envelope_reset"
const ActionParamKeyID kAIEnvelopeResetType				= 'type'; // enum
const ActionParamKeyID kAIEnvelopeMaintainShape			= 'shap'; // bool
// The following keys are reused from the kAIEnvelopeMakeAction action and are already defined
//const ActionParamKeyID kAIEnvelopeNumRows				= 'rows'; // integer
//const ActionParamKeyID kAIEnvelopeNumCols				= 'cols'; // integer
//const ActionParamKeyID kAIEnvelopeWarpStyle			= 'warp'; // enum
//const ActionParamKeyID kAIEnvelopeBend				= 'bend'; // unit real
//const ActionParamKeyID kAIEnvelopeHorizDistort		= 'horz'; // unit real
//const ActionParamKeyID kAIEnvelopeVertDistort			= 'vert'; // unit real
//const ActionParamKeyID kAIEnvelopeRotate90			= 'rot.'; // enum


#endif // _AIENVELOPEACTION_H_