#ifndef _AITRACINGACTION_H_
#define _AITRACINGACTION_H_

/*
 *        Name:	AITracingAction.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Actions for envelopes.
 *
 * Copyright 2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifndef __AIActionManager_h__
#include "AIActionManager.h"
#endif



// -----------------------------------------------------------------------------
// Action: kAITracingMenuAction
// Purpose: runs some functionality accessible through the Tracing submenus
//
// Parameters: 
//	- kAITracingMenuItem, enum: the menu item to run
// -----------------------------------------------------------------------------

#define kTracingPluginActionName	"ai_plugin_tracing"

const ActionParamKeyID kAITracingMenuItem	= 'menu'; // enum

#define kTracingActionMake						1
#define kTracingActionMakeAndExpand				2
#define kTracingActionMakeAndLivePaint			3
#define kTracingActionRelease					4
#define kTracingActionOptions					5
#define kTracingActionExpand					6
#define kTracingActionExpandAsViewed			7
#define kTracingActionExpandAsLivePaint			8
#define kTracingActionShowNoTracingResult		9
#define kTracingActionShowTracingResult			10
#define kTracingActionShowOutlines				11
#define kTracingActionShowOutlinesAndTracing	12
#define kTracingActionShowNoImage				13
#define kTracingActionShowOriginalImage			14
#define kTracingActionShowAdjustedImage			15
#define kTracingActionShowTransparentImage		16
#define kTracingActionPresets					17




#endif // _AITRACINGACTION_H_