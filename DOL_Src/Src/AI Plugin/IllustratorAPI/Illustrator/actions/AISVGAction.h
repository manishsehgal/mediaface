#ifndef _AISVGACTION_H_
#define _AISVGACTION_H_

/*
 *        Name:	AISVGAction.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 SVG Action Parameters.
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifndef _AICOREDOCUMENTACTION_H_
#include "AIDocumentAction.h"
#endif

// -----------------------------------------------------------------------------
//	Types.
// -----------------------------------------------------------------------------

// ???
typedef enum _AISVGImageFormats 
{ 
	kAISVGFormatDefault  = 1,
	kAISVGFormatJPEG,
	kAISVGFormatGIF, 
	kAISVGFormatPNG  
} AISVGImageFormats;

// Numbers are rounded to have this many digits of precision after the
// decimal point.
typedef enum _AISVGCoordinatePrecision 
{ 
	kAISVGPrecision_1 = 1, 
	kAISVGPrecision_2, 
	kAISVGPrecision_3, 
	kAISVGPrecision_4, 
	kAISVGPrecision_5, 
	kAISVGPrecision_6,
	kAISVGPrecision_7

} AISVGCoordinatePrecision;

// ???
typedef enum _AISVGRendering 
{ 
	kAISVGRenderingProgressive = 1,
	kAISVGRenderingBatched 
} AISVGRendering;

// How to subset fonts in the SVG.
typedef enum _AISVGFontSubsetting 
{
	kAISVGSubsettingNone = 1,
	kAISVGSubsettingGlyphsOnly,
	kAISVGSubsettingCommonEnglishOnly,
	kAISVGSubsettingGlyphsPlusEnglish,
	kAISVGSubsettingCommonRomanOnly,
	kAISVGSubsettingGlyphsPlusRoman,
	kAISVGSubsettingOutputEntireFont
} AISVGFontSubsetting;

// Whether to embed fonts or create separate, linked files
typedef enum _AISVGFileLocation
{
	kAISVGLocationEmbed = 1,
	kAISVGLocationLink
} AISVGFileLocation;

// Character set to use.
typedef enum _AISVGDocumentEncoding
{
	kAISVGISO8859Encoding = 1,		// i.e. ASCII
	kAISVGUTF8Encoding,
	kAISVGUTF16Encoding

} AISVGDocumentEncoding;

// Compression to use.
typedef enum _AISVGFileCompression
{
	kAISVGCompressionNone = 1,
	kAISVGCompressionGZIP

} AISVGFileCompression;

// Specifies how style information should be exported.
typedef enum _AISVGStyle
{
	kAISVGInLineStyle = 1,			// 'style' attributes on the SVG elements
	kAISVGEntityStyle,				// 'style' attributes that contain entity references
	kAISVGCSSStyle,					// 'class' attributes with corresponding CSS styles
	kAISVGPresentationAttrsStyle	// presentation attributes on the SVG elements
} AISVGStyle;

// Units to use for dimensions and positions
typedef enum _AISVGUnits
{	
	kAISVGUnitCentimeters = 0,
	kAISVGUnitEms,
	kAISVGUnitInches,
	kAISVGUnitPicas,
	kAISVGUnitPixels,
	kAISVGUnitPoints,
	kAISVGUnitMillimeters
} AISVGUnits;	

// ???
typedef enum _AISVGResolution
{
	kAISVGPixelsPerInch = 0, 
	kAISVGPixelsPerCentimeter
} AISVGResolution;

// ???
typedef enum _AISVGEmbedFontFormats
{
	kAISVGEmbedFontTrueType = 0, 
	kAISVGEmbedFontType1 , 
	kAISVGEmbedFontBoth
} AISVGEmbedFontFormats;

// Should images be embedded or saved as separate files.
typedef enum _AISVGRasterLocation
{
	kAISVGImageEmbed = 1, 
	kAISVGImagelink
} AISVGRasterLocation;

// ???
typedef enum _AISVGDTD
{
	kAISVGDTD10 = -1, 
	kAISVGDTD11,
	kAISVGDTDTiny11,
	kAISVGDTDTiny11Plus,
	kAISVGDTDBasic11
} AISVGDTD;

typedef enum _AISVGFontType
{
	kAISVGFontCEF = 0, 
	kAISVGFontSVG,
	kAISVGFontOutline
} AISVGFontType;

typedef enum _AISVGTextOnPath
{
	kAISVGTextOnPathSVG = 0, 
	kAISVGTextOnPathIllustrator
} AISVGTextOnPath;

// Rectangle. Defined by corner point, width and height
typedef struct _AISVGRect
{
	float x;
	float y;
	float width;
	float height;
} AISVGRect;


// -----------------------------------------------------------------------------
// Action: kAIExportDocumentAction
// Purpose: These are additional parameters for export to SVG
// Parameters:
//	The following parameters are standard to the export action 
//	- kAIExportDocumentNameKey, string: the file name to export as.
//	- kAIExportDocumentFormatKey, string: one of...
//			kAISVGFileFormat to export uncompressed SVG
//			kAISVGCompressedFileFormat to export compressed SVG
//			kAISVGRoundTripFileFormat to export SVG with round trip data
//	- kAIExportDocumentExtensionKey, string: one of...
//			kAISVGFileFormatExtension if exporting uncompressed SVG
//			kAISVGCompressedFileFormat if exporting compressed SVG
//			kAISVGRoundTripFileFormatExtension if exporting SVG with round trip
//				data
//
//	The following are standard SVG options
//	- kAISVGImageFormatKey, enum AIImageFormats
//	- kAISVGPrecisionKey, enum AISVGCoordinatePrecision
//	- kAISVGRenderingKey, enum AISVGRendering
//	- kAISVGZoomKey, bool
//	- kAISVGSystemFontKey, bool
//	- kAISVGFontSubsettingKey, enum AISVGFontSubsetting
//	- kAISVGFontLocationKey, enum AISVGFileLocation
//	- kAISVGEncodingKey, enum AISVGDocumentEncoding
//	- kAISVGCompressionKey, enum AISVGFileCompression
//	- kAISVGStyleTypeKey, enum AISVGStyle
//
// The following are Illustrator specific SVG export options
//	- kAISVGWidthUnitKey, enum AISVGUnits
//	- kAISVGHeightUnitKey, enum AISVGUnits
//	- kAISVGResolutionUnitKey, enum AISVGResolution
//	- kAISVGEmbedFontFormatsKey, enum AISVGEmbedFontFormats
//	- kAISVGEmbedRasterLocationKey, enum AISVGRasterLocation
//	- kAISVGGradientTolerenceKey, real
//	- kAISVGTextOnPathKey, enum AISVGTextOnPath
//	- kAISVGAntiAliasKey, bool
//	- kAISVGExportHiddenObjectKey, bool
//	- kAISVGExportLayerAsTitleKey, bool
//	- kAISVGExportAlsoExportKey, bool
//	- kAISVGExportConstrainProportionsKey, bool
//	- kAISVGExportClipToArtboardKey, bool
//	- kAISVGExportRoundTripKey, bool
//	- kAISVGIncludeAdobeNameSpaceKey, bool
//	- kAISVGIncludeTemplateKey, bool
//	- kAISVGViewBoxKey, data AISVGRect
//	- kAISVGIncludePGFKey, bool: if true Illustrator round trip data will
//			be included in the SVG, if false it will not.
//	- kAISVGIncludeXAPKey, bool: if true XAP Metadata will be included in
//			the SVG, if false it will not.
//	- kAISVGIncludeSlicesKey, bool: if true slicing information will be
//			included in the SVG, if false it will not.
//	- kAISVGDisbleAutoKerningKey, bool: if true AutoKerning will be disabled in
//			the SVG, if false it will not.
//	- kAISVGUseSVGTextOnPathKey, bool: if true we use SVG Text On Path
//			if false we write each character separate.
//
// Comments:
// The following parameters can be specifed when saving to SVG. To save a file
// as SVG issue an export action (kAISaveDocumentAsAction). Specify one of the
// formats and extensions below for the format key.
// -----------------------------------------------------------------------------

// file format and extensions for saving as an uncompressed SVG file.
#define  kAISVGFileFormat								"svg file format"
#define kAISVGFileFormatExtension						"svg"

// file format and extensions for saving as an compressed SVG file.
#define kAISVGCompressedFileFormat						"svg compressed file format"
#define kAISVGCompressedFileFormatExtension				"svgz"


// SVG Options
const ActionParamKeyID kAISVGImageFormatKey				= 'iFmt'; // enum AIImageFormats
const ActionParamKeyID kAISVGPrecisionKey				= 'Prcs'; // enum AISVGCoordinatePrecision
const ActionParamKeyID kAISVGRenderingKey				= 'Rndr'; // enum AISVGRendering
const ActionParamKeyID kAISVGZoomKey					= 'ZooM'; // bool
const ActionParamKeyID kAISVGSystemFontKey				= 'SYFt'; // bool
const ActionParamKeyID kAISVGFontSubsettingKey			= 'FSbs'; // enum AISVGFontSubsetting
const ActionParamKeyID kAISVGFontLocationKey			= 'FLct'; // enum AISVGFileLocation
const ActionParamKeyID kAISVGEncodingKey				= 'Encd'; // enum AISVGDocumentEncoding
const ActionParamKeyID kAISVGCompressionKey				= 'Cmzn'; // enum AISVGFileCompression
const ActionParamKeyID kAISVGStyleTypeKey				= 'StTp'; // enum AISVGStyle
const ActionParamKeyID kAISVGFontFormatKey				= 'fFmt'; // enum AIImageFormats


// Plugin Options
const ActionParamKeyID kAISVGWidthUnitKey				= 'WunT'; // enum AISVGUnits
const ActionParamKeyID kAISVGHeightUnitKey				= 'HunT'; // enum AISVGUnits
const ActionParamKeyID kAISVGResolutionUnitKey			= 'RzUt'; // enum AISVGResolution
const ActionParamKeyID kAISVGEmbedFontFormatsKey		= 'EFFt'; // enum AISVGEmbedFontFormats
const ActionParamKeyID kAISVGEmbedRasterLocationKey		= 'RLoc'; // enum AISVGRasterLocation
const ActionParamKeyID kAISVGRasterResolutionKey		= 'RsRl'; // integer
const ActionParamKeyID kAISVGGradientTolerenceKey		= 'GrTl'; // real
const ActionParamKeyID kAISVGTextOnPathKey				= 'ToPt'; // enum AISVGTextOnPath
const ActionParamKeyID kAISVGAntiAliasKey				= 'AAlz'; // bool
const ActionParamKeyID kAISVGExportHiddenObjectKey		= 'HoBj'; // bool
const ActionParamKeyID kAISVGExportLayerAsTitleKey		= 'LasT'; // bool
const ActionParamKeyID kAISVGExportAlsoExportKey		= 'Aexp'; // bool
const ActionParamKeyID kAISVGExportConstrainProportionsKey = 'Cstp'; // bool
const ActionParamKeyID kAISVGExportClipToArtboardKey	= 'CTab'; // bool
const ActionParamKeyID kAISVGExportRoundTripKey			= 'Rrtr'; // bool
const ActionParamKeyID kAISVGIncludeAdobeNameSpaceKey	= 'Anxp'; // bool
const ActionParamKeyID kAISVGIncludeTemplateKey			= 'Temt'; // bool
const ActionParamKeyID kAISVGViewBoxKey					= 'VieW'; // data AISVGRect
const ActionParamKeyID kAISVGIncludePGFKey				= 'Ipgf'; // bool
const ActionParamKeyID kAISVGIncludeXAPKey				= 'IXAP'; // bool
const ActionParamKeyID kAISVGIncludeSlicesKey			= 'ISlc'; // bool
const ActionParamKeyID kAISVGDTDKey						= 'DTDt'; // enum AISVGDTD
const ActionParamKeyID kAISVGFontTypeKey				= 'FTKt'; // enum AISVGFontType
const ActionParamKeyID kAISVGDisbleAutoKerningKey		= 'IDAK'; // bool
const ActionParamKeyID kAISVGUseSVGTextOnPathKey		= 'ITOP'; // bool

#endif
