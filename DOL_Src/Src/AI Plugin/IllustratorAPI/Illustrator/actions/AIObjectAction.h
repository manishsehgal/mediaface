#ifndef _AICOREOBJECTACTION_H_
#define _AICOREOBJECTACTION_H_

/*
 *        Name:	AICoreObjectAction.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Actions defined in the core.
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifndef __AIActionManager_h__
#include "AIActionManager.h"
#endif

// -----------------------------------------------------------------------------
// Action: kAIDoMenuCommandAction
// Purpose: Invokes the menu command with the given id
// Parameters: 
//	- kAIDoMenuCommandIdKey, integer: the command id
// -----------------------------------------------------------------------------

#define kAIDoMenuCommandAction								"adobe_commandManager"
const ActionParamKeyID kAIDoMenuCommandIdKey				= 'cmid'; // integer

// -----------------------------------------------------------------------------
// Action: kAITranslateSelectionAction
// Purpose: Translate the selected objects
// Parameters: 
//	- kAITranslateSelectionHorizontalKey, real: is the horizontal offset in points.
//	- kAITranslateSelectionVerticalKey, real: is the vertical offset in points.
//	- kAITranslateSelectionCopyKey, bool: is whether to translate a copy of the objects.
//	- kAITranslateSelectionObjectsKey, bool: is whether to translate the object geometry.
//	- kAITranslateSelectionPatternsKey, bool: is whether to translate patterns.
// -----------------------------------------------------------------------------

#define kAITranslateSelectionAction							"adobe_move"
const ActionParamKeyID kAITranslateSelectionHorizontalKey	= 'hozn'; // real
const ActionParamKeyID kAITranslateSelectionVerticalKey		= 'vtcl'; // real
const ActionParamKeyID kAITranslateSelectionCopyKey			= 'copy'; // bool
const ActionParamKeyID kAITranslateSelectionObjectsKey		= 'objt'; // bool
const ActionParamKeyID kAITranslateSelectionPatternsKey		= 'patn'; // bool

// -----------------------------------------------------------------------------
// Action: kAIRotateSelectionAction
// Purpose: Rotate the selected objects
// Parameters: 
//	- kAIRotateSelectionAngleKey, real: is the angle of rotation.
//	- kAIRotateSelectionAboutDXKey, real: is the x offset of the origin with
//			respect to the center of the selection.
//	- kAIRotateSelectionAboutDYKey, real: is the y offset of the origin with
//			respect to the center of the selection.
//	- kAIRotateSelectionCopyKey, bool: is whether to rotate a copy of the objects.
//	- kAIRotateSelectionObjectsKey, bool: is whether to rotate the object geometry.
//	- kAIRotateSelectionPatternsKey, bool: is whether to rotate patterns.
// -----------------------------------------------------------------------------

#define kAIRotateSelectionAction							"adobe_rotate"
const ActionParamKeyID kAIRotateSelectionAngleKey			= 'angl'; // real
const ActionParamKeyID kAIRotateSelectionAboutDXKey			= 'detX'; // real
const ActionParamKeyID kAIRotateSelectionAboutDYKey			= 'detY'; // real
const ActionParamKeyID kAIRotateSelectionCopyKey			= 'copy'; // bool
const ActionParamKeyID kAIRotateSelectionObjectsKey			= 'objt'; // bool
const ActionParamKeyID kAIRotateSelectionPatternsKey		= 'patn'; // bool

// -----------------------------------------------------------------------------
// Action: kAIScaleSelectionAction
// Purpose: Scale the selected objects
// Parameters: 
//	- kAIScaleSelectionUniformKey, bool: whether the scale is uniform or not.
//	- kAIScaleSelectionLinesKey, bool: whether to scale stroke weights.
//	- kAIScaleSelectionUniformScaleKey, real: the scale factor when when 'unfm' == true
//	- kAIScaleSelectionHorizontalKey, real: horizontal scale factor when 'unfm' == false
//	- kAIScaleSelectionVerticalKey, real: vertical scale factor when 'unfm' == false
//	- kAIScaleSelectionAboutDXKey, real: is the x offset of the origin with
//			respect to the center of the selection.
//	- kAIScaleSelectionAboutDYKey, real: is the y offset of the origin with
//			respect to the center of the selection.
//	- kAIScaleSelectionCopyKey, bool: is whether to scale a copy of the objects.
//	- kAIScaleSelectionObjectsKey, bool: is whether to scale the object geometry.
//	- kAIScaleSelectionPatternsKey, bool: is whether to scale patterns.
// -----------------------------------------------------------------------------

#define kAIScaleSelectionAction								"adobe_scale"
const ActionParamKeyID kAIScaleSelectionUniformKey			= 'unfm'; // bool
const ActionParamKeyID kAIScaleSelectionLinesKey			= 'line'; // bool
const ActionParamKeyID kAIScaleSelectionUniformScaleKey		= 'scle'; // real
const ActionParamKeyID kAIScaleSelectionHorizontalKey		= 'hozn'; // real
const ActionParamKeyID kAIScaleSelectionVerticalKey			= 'vtcl'; // real
const ActionParamKeyID kAIScaleSelectionAboutDXKey			= 'detX'; // real
const ActionParamKeyID kAIScaleSelectionAboutDYKey			= 'detY'; // real
const ActionParamKeyID kAIScaleSelectionCopyKey				= 'copy'; // bool
const ActionParamKeyID kAIScaleSelectionObjectsKey			= 'objt'; // bool
const ActionParamKeyID kAIScaleSelectionPatternsKey			= 'patn'; // bool

// -----------------------------------------------------------------------------
// Action: kAIReflectSelectionAction
// Purpose: Reflect the selected objects
// Parameters: 
//	- kAIReflectSelectionAngleKey, real: is the angle of the axis of reflection.
//	- kAIReflectSelectionAboutDXKey, real: is the x offset of the origin with
//			respect to the center of the selection.
//	- kAIReflectSelectionAboutDYKey, real: is the y offset of the origin with
//			respect to the center of the selection.
//	- kAIReflectSelectionCopyKey, bool: is whether to reflect a copy of the objects.
//	- kAIReflectSelectionObjectsKey, bool: is whether to reflect the object geometry.
//	- kAIReflectSelectionPatternsKey, bool: is whether to reflect patterns.
// -----------------------------------------------------------------------------
		
#define kAIReflectSelectionAction							"adobe_reflect"
const ActionParamKeyID kAIReflectSelectionAngleKey			= 'angl'; // real
const ActionParamKeyID kAIReflectSelectionAboutDXKey		= 'detX'; // real
const ActionParamKeyID kAIReflectSelectionAboutDYKey		= 'detY'; // real
const ActionParamKeyID kAIReflectSelectionCopyKey			= 'copy'; // bool
const ActionParamKeyID kAIReflectSelectionObjectsKey		= 'objt'; // bool
const ActionParamKeyID kAIReflectSelectionPatternsKey		= 'patn'; // bool

// -----------------------------------------------------------------------------
// Action: kAIShearSelectionAction
// Purpose: Shear the selected objects
// Parameters: 
//	- kAIShearSelectionShearAngleKey, real: is the amount of shearing.
//	- kAIShearSelectionAngleKey, real: is the angle along which shearing takes place.
//	- kAIShearSelectionAboutDXKey, real: is the x offset of the origin with respect
//			to the center of the selection.
//	- kAIShearSelectionAboutDYKey, real: is the y offset of the origin with respect
//			to the center of the selection.
//	- kAIShearSelectionCopyKey, bool: is whether to reflect a copy of the objects.
//	- kAIShearSelectionObjectsKey, bool: is whether to reflect the object geometry.
//	- kAIShearSelectionPatternsKey, bool: is whether to reflect patterns.
// -----------------------------------------------------------------------------

#define kAIShearSelectionAction								"adobe_shear"
const ActionParamKeyID kAIShearSelectionShearAngleKey		= 'shag'; // real
const ActionParamKeyID kAIShearSelectionAngleKey			= 'angl'; // real
const ActionParamKeyID kAIShearSelectionAboutDXKey			= 'detX'; // real
const ActionParamKeyID kAIShearSelectionAboutDYKey			= 'detY'; // real
const ActionParamKeyID kAIShearSelectionCopyKey				= 'copy'; // bool
const ActionParamKeyID kAIShearSelectionObjectsKey			= 'objt'; // bool
const ActionParamKeyID kAIShearSelectionPatternsKey			= 'patn'; // bool

// -----------------------------------------------------------------------------
// Action: kAITransformSelectionAgainAction
// Purpose: Repeat the last transformation
// Parameters: None
// -----------------------------------------------------------------------------

#define kAITransformSelectionAgainAction					"adobe_transformAgain"

// -----------------------------------------------------------------------------
// Action: kAISendSelectionToFrontAction
// Purpose: Send to front
// Parameters: None
//
// Action: kAISendSelectionToBackAction
// Purpose: Send to back
// Parameters: None
//
// Action: kAISendSelectionForwardAction
// Purpose: Send forward
// Parameters: None
//
// Action: kAISendSelectionBackwardAction
// Purpose: Send backward
// Parameters: None
// -----------------------------------------------------------------------------

#define kAISendSelectionToFrontAction						"adobe_sendToFront"
#define kAISendSelectionToBackAction						"adobe_sendToBack"
#define kAISendSelectionForwardAction						"adobe_sendForward"
#define kAISendSelectionBackwardAction						"adobe_sendBackward"

// -----------------------------------------------------------------------------
// Action: kAICutSelectionAction
// Purpose: Cut
// Parameters: None
//
// Action: kAICopySelectionAction
// Purpose: Copy
// Parameters: None
//
// Action: kAIClearSelectionAction
// Purpose: Clear
// Parameters: None
// -----------------------------------------------------------------------------

#define kAICutSelectionAction								"adobe_cut"
#define kAICopySelectionAction								"adobe_copy"
#define kAIClearSelectionAction								"adobe_clear"

// -----------------------------------------------------------------------------
// Action: kAIPasteClipboardAction
// Purpose: Paste
// Parameters: None
//
// Action: kAIPasteClipboardInFrontAction
// Purpose: Paste in front
// Parameters: None
//
// Action: kAIPasteClipboardInBackAction
// Purpose: Paste in back
// Parameters: None
// -----------------------------------------------------------------------------

#define kAIPasteClipboardAction								"adobe_paste"
#define kAIPasteClipboardInFrontAction						"adobe_pasteInFront"
#define kAIPasteClipboardInBackAction						"adobe_pasteInBack"

// -----------------------------------------------------------------------------
// Action: kAISelectAllAction
// Purpose: Select all
// Parameters: None
//
// Action: kAIDeselectAllAction
// Purpose: Select none
// Parameters: None
// -----------------------------------------------------------------------------

#define kAISelectAllAction									"adobe_selectAll"
#define kAIDeselectAllAction								"adobe_deselectAll"

// -----------------------------------------------------------------------------
// Action: kAIGroupSelectionAction
// Purpose: Group
// Parameters: None
//
// Action: kAIUngroupSelectionAction
// Purpose: Ungroup
// Parameters: None
// -----------------------------------------------------------------------------

#define kAIGroupSelectionAction								"adobe_group"
#define kAIUngroupSelectionAction							"adobe_ungroup"

// -----------------------------------------------------------------------------
// Action: kAILockSelectionAction
// Purpose: Lock
// Parameters: None
//
// Action: kAIUnlockAllAction
// Purpose: Unlock
// Parameters: None
// -----------------------------------------------------------------------------

#define kAILockSelectionAction								"adobe_lock"
#define kAIUnlockAllAction									"adobe_unlockAll"

// -----------------------------------------------------------------------------
// Action: kAIHideSelectionAction
// Purpose: Hide
// Parameters: None
//
// Action: kAIShowAllAction
// Purpose: Show
// Parameters: None
// -----------------------------------------------------------------------------

#define kAIHideSelectionAction								"adobe_hideSelection"
#define kAIShowAllAction									"adobe_showAll"

// -----------------------------------------------------------------------------
// Action: kAIMakeMaskFromSelectionAction
// Purpose: Make mask
// Parameters: None
//
// Action: kAIReleaseSelectedMasksAction
// Purpose: Release mask
// Parameters: None
// -----------------------------------------------------------------------------

#define kAIMakeMaskFromSelectionAction						"adobe_makeMask"
#define kAIReleaseSelectedMasksAction						"adobe_releaseMask"

// -----------------------------------------------------------------------------
// Action: kAIMakeCompoundFromSelectionAction
// Purpose: Make compound
// Parameters: None
//
// Action: kAIReleaseSelectedCompoundAction
// Purpose: Release compound
// Parameters: None
// -----------------------------------------------------------------------------

#define kAIMakeCompoundFromSelectionAction					"adobe_makeCompound"
#define kAIReleaseSelectedCompoundAction					"adobe_releaseMask"

// -----------------------------------------------------------------------------
// Action: kAIMakeCropMarksFromSelectionAction
// Purpose: Make crop marks
// Parameters: None
//
// Action: kAIReleaseCropMarksAction
// Purpose: Release crop marks
// Parameters: None
// -----------------------------------------------------------------------------

#define kAIMakeCropMarksFromSelectionAction					"adobe_makeCropMark"
#define kAIReleaseCropMarksAction							"adobe_releaseCropMark"

// -----------------------------------------------------------------------------
// Action: kAIShowHideGuidesAction
// Purpose: Show or hide guides
// Parameters:
//	- kAIShowHideGuidesKey, bool:
//
// Action: kAILockOrUnlockGuidesAction
// Purpose: Lock or unlock guides
// Parameters:
//	- kAIAILockOrUnlockGuidesKey, bool:
// -----------------------------------------------------------------------------

#define kAIShowHideGuidesAction								"adobe_showGuide"
const ActionParamKeyID kAIShowHideGuidesKey					= 'show'; // bool

#define kAILockOrUnlockGuidesAction							"adobe_lockGuide"
const ActionParamKeyID kAIAILockOrUnlockGuidesKey			= 'lock'; // bool

// -----------------------------------------------------------------------------
// Action: kAIMakeGuidesFromSelectionAction
// Purpose: Makes guides
// Parameters: None
//
// Action: kAIReleaseAllGuidesAction
// Purpose: Releases all guides
// Parameters: None
//
// Action: kAIDeleteAllGuidesAction
// Purpose: Deletes all guides
// Parameters: None
// -----------------------------------------------------------------------------

#define kAIMakeGuidesFromSelectionAction					"adobe_makeGuide"
#define kAIReleaseAllGuidesAction							"adobe_releaseGuide"
#define kAIDeleteAllGuidesAction							"adobe_clearGuide"

// -----------------------------------------------------------------------------
// Action: kAISetObjectAttributesAction
// Purpose: Sets object attributes as defined in attributes palette
// Parameters:
//	- kAISetObjectAttributesShowCenterKey, bool:
//	- kAISetObjectAttributesReverseDirectionKey, bool:
//	- kAISetObjectAttributesFillRuleKey, enum:
//	- kAISetObjectAttributesResolutionKey, real:
//	- kAISetObjectAttributesOverprintFillKey, bool:
//	- kAISetObjectAttributesOverprintStrokeKey, bool:
//	- kAISetObjectAttributesNoteKey, string:
//	- kAISetObjectAttributesURLKey, string:
//	- kAISetObjectAttributesImageMapKey, enum:
// -----------------------------------------------------------------------------

#define kAISetObjectAttributesAction						"adobe_attributePalette"
const ActionParamKeyID kAISetObjectAttributesShowCenterKey	= 'cntr'; // bool
const ActionParamKeyID kAISetObjectAttributesReverseDirectionKey = 'rvpt'; // bool
const ActionParamKeyID kAISetObjectAttributesFillRuleKey	= 'rule'; // enum
const ActionParamKeyID kAISetObjectAttributesResolutionKey	= 'rslt'; // real
const ActionParamKeyID kAISetObjectAttributesOverprintFillKey = 'fill'; // bool
const ActionParamKeyID kAISetObjectAttributesOverprintStrokeKey = 'strk'; // bool
const ActionParamKeyID kAISetObjectAttributesNoteKey		= 'note'; // string
const ActionParamKeyID kAISetObjectAttributesURLKey			= 'url.'; // string
const ActionParamKeyID kAISetObjectAttributesImageMapKey	= 'imap'; // enum


#endif
