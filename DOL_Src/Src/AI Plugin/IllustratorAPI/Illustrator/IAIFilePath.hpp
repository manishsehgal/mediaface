/*
 *        Name:	IAIFilePath.hpp
 *     Purpose:	C++ access class for File Path suite
 *
 * Copyright (c) 2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#pragma once
#ifndef __IAIFilePath__
#define __IAIFilePath__

#include "IAIUnicodeString.h"
#include <memory>

#ifdef MAC_ENV
#include <CFURL.h>
#include <Files.h>
#else
typedef signed long OSStatus;
#endif

struct FSSpec;
struct MiFile;

namespace ai {

#ifdef WIN_ENV
const unsigned MaxPath = 259;
#else
const unsigned MaxPath = 1023;
#endif
const unsigned MaxFile = 255;

class FilePath
{
	public:

	/// Reset cached information about volumes.
	/** Some file system operations can be very slow. For example, on Windows,
		UNC path names are used to access files on remote volumes. Determining
		the availability of those remote volumes can require significant time
		especially in the presence of the Windows client for NFS. For this
		reason volume availability information is cached. This interface can
		be used to reset the cache. */
	static void ResetVolumeCache() AINOTHROW;

	FilePath() AINOTHROW;
	FilePath(const FilePath&);

	/// Set a FilePath from a Unicode string.
	/**	String may be a path native to Windows, Unix, or Mac OS or a URL.
	*/
	explicit FilePath(const ai::UnicodeString&);

	/// Set a FilePath from a SPPlatformFileSpecification.
	explicit FilePath(const SPPlatformFileSpecification&);

	/// Set a FilePath from a MiFile (Illustrator core only).
	explicit FilePath(const MiFile&);

	/// Set a FilePath from an FSSpec (Macintosh only outside Illustrator core).
	explicit FilePath(const FSSpec&);

	#ifdef MAC_ENV

	/// Set a FilePath from a CFString (Macintosh only).
	explicit FilePath(const CFStringRef);

	/// Set a FilePath from an FSRef (Macintosh only).
	explicit FilePath(const FSRef&);

	/// Set a FilePath from a CFURL (Macintosh only).
	explicit FilePath(const CFURLRef);

	/// Set a FilePath from a Macintosh alias record  (Macintosh only).
	explicit FilePath(AliasHandle);

	#endif	// MAC_ENV

	~FilePath();

	/// Same as AIFilePathSuite::Copy
	FilePath &operator=(const FilePath&);

	/// Query a FilePath object for emptiness.
	bool IsEmpty() const;

	/// Empty a FilePath object (set it to a null path).
	void MakeEmpty();

	/// Test two FilePath objects for equality.
	/** Return true if they are identical, whether or not the file exists. If
		they are not identical and @e resolveLinks is false, return false. If
		@e resolveLinks is true, determine if the two FilePaths refer to the
		same file through links, aliases, and/or shortcuts by querying the
		file system.
		
		@param a, b			(in) Paths to compare.
		@param resolveLinks	(in) Resolve links, aliases, and/or shortcuts if a & b are not identical.
	*/
	bool Equal(const FilePath&, const bool resolveLinks = false) const;
	bool operator==(const FilePath&) const;
	bool operator!=(const FilePath&) const;

	/// Test if one FilePath is less than another (based on file path string)
	bool operator<(const FilePath&) const;

	/// Return true if path starts with a delimiter.
	bool StartsWithDelimiter() const;

	/// Return true if path ends with a delimiter.
	bool EndsWithDelimiter() const;

	/// Add a component to the path. Appropriate delimiters will be added or ignored as appropriate.
	/** Delimiters will not be added to the end of a path unless explicitly
		requested by sending an empty addend. 

		@param augend		(in/out) Path to be lengthened
	*/
	void AddComponent(const ai::UnicodeString &addend = ai::UnicodeString());
	void AddComponent(const FilePath &addend);

	/// Remove the end component from the path.
	void RemoveComponent();

	/// Add/remove a file extension. Dots will be added or ignored as appropriate so only one dot appears before the extension.
	void AddExtension(const ai::UnicodeString &ext);
	void AddExtension(const std::string &ext);
	void RemoveExtension();

	/// Return true if the file or directory exists after querying the file system.
	/** Return value of the optional parameters not defined if file or directory
		does not exist. Set @e longPath and @e isFile to NULL to ignore.

		@param resolveLinks	(in) Resolve links, aliases, and shortcuts for @e longPath.
		@param longPath		(out, optional) A full long, Unicode version of 'path'. This can be used
							to convert short path names to their long counterparts. May be null.
		@param isFile		(out, optional) True if file, false if directory. May be null.
	*/
	bool Exists(const bool resolveLinks,
				ai::UnicodeString *longPath = 0, bool *isFile = 0) const;

	///	Resolve alias or shortcut by querying file system.
	/**	@e path and @e resolvedPath may be the same FilePath object. File or
		directory must exist. 
	*/
	void Resolve();

	/*****************************************************************************/
	/* Set operations */

	/// Set a FilePath from a Unicode string.
	/**	String may be a path native to Windows, Unix, or Mac OS or a URL.
	*/
	void Set(const ai::UnicodeString &string);

	/// Set a FilePath from an SPPlatformFileSpecification.
	void SetFromSPFileSpec(const SPPlatformFileSpecification&);

	/// Set a FilePath from a MiFile (Illustrator core only).
	void SetFromMiFile(const MiFile&);

	/// Set a FilePath from an FSSpec (Macintosh only outside Illustrator core).
	OSStatus SetFromFSSpec(const FSSpec&);

	#ifdef MAC_ENV

	/// Set a FilePath from a CFString (Macintosh only).
	void SetFromCFString(const CFStringRef);

	/// Set a FilePath from an FSRef (Macintosh only).
	OSStatus SetFromFSRef(const FSRef&);

	/// Set a FilePath from a CFURL (Macintosh only).
	void SetFromCFURL(const CFURLRef);

	/// Set a FilePath from a Macintosh alias record  (Macintosh only).
	OSStatus SetFromAlias(AliasHandle);

	#endif	// MAC_ENV


	/*****************************************************************************/
	/* Get operations */

	/// Return the file name including an extension, if applicable, but without the path.
	/**
		@param displayName	(in) Set to true to get the display name. See @ref DisplayNames.
		@param fileName		(out) File name string.
	*/
	ai::UnicodeString GetFileName(const bool displayName = false) const;

	/// Return the file name without the extension and path.
	ai::UnicodeString GetFileNameNoExt() const;

	/// Return the file extension without the dot. If the file name ends with a dot the extension is null.
	ai::UnicodeString GetFileExtension() const;

	/// Return the full path in notation native to the current platform.
	/**
		@param displayName	(in) Pass true to get the display name. See @ref DisplayNames.
		@param fullPath		(out) Path string.
	*/
	ai::UnicodeString GetFullPath(const bool displayName = false) const;

	/// Return the directory ending with a delimiter, without the filename, in notation native to the current platform.
	/** If path represents a directory, the result will be the same as calling GetFullPath.
		@param displayName	(in) Pass true to get the display name. See @ref DisplayNames.
		@param directory	(out) Directory name string.
	*/
	ai::UnicodeString GetDirectory(const bool displayName = false) const;

	/// Return an OS-specific short version of the path. The file or folder must exist.
	/** On Windows the path will conform to 8.3 format. On Mac OS X, the name may
		be truncated and a file ID appended; If the file ID is required the path
		will not be valid across processes. This call requires accessing the file
		system.
	*/
	ai::UnicodeString GetShortPath() const;

	/// Return the parent of the object represented by path.
	/**	@e path and @e parent may be the same FilePath. If path is a directory, its
		parent directory will be returned. If path is a top-level volume, ex.
		"c:\", an empty path will be returned. This call does not access the file system
		so you may want to call Exists or Resolve to make sure you're working with
		a good path.
	*/
	FilePath GetParent() const;

	/// Return the relative path of 'path' to 'base'.
	//On hold due to lack of demand
	//FilePath GetRelativePath(const FilePath &base) const;

	/// Return the absolute path for 'relative' from 'base'.
	//On hold due to lack of demand
	//FilePath GetAbsolutePath(const FilePath &base) const;

	/// Return the path component delimiter for the current platform (ex. "/", "\").
	static const char GetDelimiter() AINOTHROW;

	#ifdef MAC_ENV
	// Get volume and parent (one pointer may be null) (Macintosh only).
 	void GetVolumeAndParent(FSVolumeRefNum *vol, UInt32 *parent) const;
	#endif

	/// Return MacOS-type creator and type information as four character codes (Macintosh only outside Illustrator core).
	AIErr GetCreatorAndType(unsigned long *creator, unsigned long *type) const;
	
	/// Return path as a Uniform Resource Locator (URL).
	/** The resulting URL will consist of only ASCII characters with any special
		characters escaped with URL percent (\%) encoding.

		@param displayName	(in) Pass true to get the display name. See @ref DisplayNames.
		@param url			(out) Output URL.
	*/
	ai::UnicodeString GetAsURL(const bool displayName) const;

	void GetAsSPPlatformFileSpec(SPPlatformFileSpecification&) const;

	/// Fill in the existing MiFile from the FilePath (Illustrator core only).
	AIErr GetAsMiFile(MiFile&) const;

	/// Return a newly allocated MiFile based on the FilePath (Illustrator core only).
	AIErr GetAsMiFile(MiFile*&) const;

	/// Return an FSSpec created from the FilePath (Macintosh only outside Illustrator core).
	OSStatus GetAsFSSpec(FSSpec&) const;

	#ifdef MAC_ENV

	/// Return a CFString created from the FilePath (Macintosh only).
	CFStringRef GetAsCFString() const;

	/// Return an FSRef created from the FilePath (Macintosh only).
	OSStatus GetAsFSRef(FSRef&) const;

	/// Return a CFURL created from the FilePath (Macintosh only).
	CFURLRef GetAsCFURL() const;

	/// Return a handle to a Macintosh alias record to the FilePath (Macintosh only).
	AliasHandle GetAsAlias() const;

	#endif	// MAC_ENV

	private:
	class FilePathImpl *impl;
};

} // namespace ai

////////////////////////////////////////////////////////////////////////////////
// Inline members
//

////////////////////////////////////////////////////////////////////////////////
inline void ai::FilePath::AddComponent(const FilePath &addend)
{
	AddComponent(addend.GetFullPath());
}

////////////////////////////////////////////////////////////////////////////////
inline void ai::FilePath::AddExtension(const std::string &ext)
{
	AddExtension(ai::UnicodeString(ext));
}


#endif // __IAIFilePath__
