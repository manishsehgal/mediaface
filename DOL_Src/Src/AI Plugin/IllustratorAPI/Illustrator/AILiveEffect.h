#ifndef __AILiveEffect__
#define __AILiveEffect__

/*
 *        Name:	AILiveEffect.h
 *   $Revision: 6 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Effect Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AIDict__
#include "AIDictionary.h"
#endif

#ifndef __AIFilter__
#include "AIFilter.h"
#endif

#ifndef __AIMenu__
#include "AIMenu.h"
#endif

#ifndef __AIPlugin__
#include "AIPlugin.h"
#endif

#ifndef __AIXMLElement__
#include "AIXMLElement.h"
#endif



#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AILiveEffect.h */

/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAILiveEffectSuite					"AI Live Effect Suite"
#define kAILiveEffectSuiteVersion			AIAPI_VERSION(3)
#define kAILiveEffectVersion				kAILiveEffectSuiteVersion


/** @ingroup Callers
	This is the effect caller */
#define kCallerAILiveEffect						"AI Live Effect"


// These are the effect messages.

/** @ingroup Selectors
	The data to the message is specified by the AILiveEffectEditParamMessage structure. */
#define kSelectorAIEditLiveEffectParameters		"AI Live Effect Edit Parameters"
/** @ingroup Selectors
	The data to the message is specified by the AILiveEffectGoMessage structure. */
#define kSelectorAIGoLiveEffect					"AI Live Effect Go Live"
/** @ingroup Selectors
	The data to the message is specified by the AILiveEffectInterpParamMessage structure. */
#define kSelectorAILiveEffectInterpolate		"AI Live Effect Interpolate Parameters"
/** @ingroup Selectors
	The data to the message is specified by the AILiveEffectInputTypeMessage structure. */
#define kSelectorAILiveEffectInputType			"AI Live Effect Input Type"
/** @ingroup Selectors
	The data to the message is specified by the AILiveEffectConvertColorMessage structure. */
#define kSelectorAILiveEffectConverColorSpace	"AI Live Effect Convert Color Space"
/** @ingroup Selectors
	The data to the message is specified by the AILiveEffectScaleParamMessage structure. */
#define kSelectorAILiveEffectScaleParameters	"AI Live Effect Scale Parameters"
/** @ingroup Selectors
	The data to the message is specified by the AILiveEffectHandleMergeMessage structure. */
#define kSelectorAILiveEffectHandleMerge		"AI Live Effect Handle Merge"
/** @ingroup Selectors
	The data to the message is specified by the AILiveEffectGetSVGFilterMessage structure. */
#define kSelectorAILiveEffectGetSVGFilter		"AI Live Effect Get SVG Filter"


// Special strings for the parameter dictionary

/** @ingroup DictKeys
	kDisplayStringKey is a string entry that will be displayed by the palette in preference
	to the filter name if it is set.  For example, Punk & Bloat might set it to "Punk" or
	"Bloat" to tell which filter is used.  For a stroke or fill filter, it is displayed
	next to the color swatch. */
#define kDisplayStringKey "DisplayString"

/** @ingroup DictKeys
	kExtraStringKey is a string entry that gives some extra information for the palette
	to display for this filter, for example as a tool tip.  It should give a little bit
	of extra information about the filter, such as its most important parameters. */
#define kExtraStringKey "ExtraString"

/** @ingroup DictKeys
	kFillStyleKey is used for fill filters and is of type FillStyleType.  It is set
	for the filter when the filter is installed, and gives the fill color to use. */
#define kFillStyleKey "FillStyle"

/** @ingroup DictKeys
	kEvenOddKey is also used for fill filters and is of type BooleanType. */
#define kEvenOddKey "UseEvenOdd"

/** @ingroup DictKeys
	kStrokeStyleKey is of type StrokeStyleType and is used for stroke filters. */
#define kStrokeStyleKey "StrokeStyle"

/** @ingroup DictKeys
	kScaleFactorKey is of type AIReal and is used to scale the parameters while processing
	the go message. This scale factor will be found in the instance dictionary during a go
	message. Plugins should scale their parameters locally but not modify the values found
	in the actual parameter dictionary. */
#define kScaleFactorKey "ScaleFactorString"


/*******************************************************************************
 **
 ** Types
 **
 **/

// This is a reference to an effect. It is never dereferenced.
typedef struct _t_AILiveEffectOpaque* AILiveEffectHandle;
typedef struct _t_AILiveEffectParamContext* AILiveEffectParamContext;


/** Defines the menu item for a live effect. */
typedef struct {
	char *category;
	char *title;
	/** Same semantics as options parameter to AIFilterSuite::AddFilter() */
	long options;
} AddLiveEffectMenuData;


/** The parameters for a live effect are stored in a dictionary. */
typedef AIDictionaryRef AILiveEffectParameters;


/** Flags that specify properties of a live effect when it is registered. */
typedef enum {
	kNoFlags 				= 0,
	kPreEffectFilter		= 0x1,
	kPostEffectFilter		= 0x2,
	kStrokeFilter			= 0x3,
	kFillFilter				= 0x4,

	kFilterTypeMask			= 0x0ffff,
	kSpecialGroupPreFilter	= 0x010000,
	kHasScalableParams		= 0x020000,
	kUsesAutoRasterize		= 0x040000,
	kCanGenerateSVGFilter	= 0x080000
} AIStyleFilterFlags;


/** When a live effect is requested to execute it is given an input art object to process.
	The effect can specify what kinds of objects the input art may contain. If the input
	art contains objects the effect does not handle then an attempt is made to find a
	conversion filter that will convert the input art to a type that is handled. If no
	conversion filter is available the input art is passed unchanged. The general rule is
	that a filter will pass through unchanged any art that it does not know how to handle, */
typedef enum {
	/** The #kSelectorAILiveEffectInputType message will be sent to the plugin to find out the input
		type */
	kInputArtDynamic			= 0,
	kGroupInputArt 				= 1L << ((int) kGroupArt - 1),
	kPathInputArt 				= 1L << ((int) kPathArt - 1),
	kCompoundPathInputArt 		= 1L << ((int) kCompoundPathArt - 1),

	/** Obsolete */
	kTextInputArtUnsupported	= 1L << ((int) kTextArtUnsupported - 1),
	/** Obsolete */
	kTextPathInputArtUnsupported= 1L << ((int) kTextPathArtUnsupported - 1),
	/** Obsolete */
	kTextRunInputArtUnsupported	= 1L << ((int) kTextRunArtUnsupported - 1),
	
	kPlacedInputArt 			= 1L << ((int) kPlacedArt - 1),
	kMysteryPathInputArt 		= 1L << ((int) kMysteryPathArt - 1),
	kRasterInputArt 			= 1L << ((int) kRasterArt - 1),

	/** If kPluginInputArt is not specified, the filter will receive the result group of a plugin
		group instead of the plugin group itself */
	kPluginInputArt				= 1L << ((int) kPluginArt - 1),
	kMeshInputArt 				= 1L << ((int) kMeshArt - 1),

	kTextFrameInputArt 			= 1L << ((int) kTextFrameArt - 1),
	
	kSymbolInputArt 			= 1L << ((int) kSymbolArt - 1),
	
	kForeignInputArt			= 1L << ((int) kForeignArt - 1),
	kLegacyTextInputArt			= 1L << ((int) kLegacyTextArt - 1),

	/** Indicates that the effect can operate on any input art. */
	kAnyInputArt 				= 0xFFFF,
	/** Indicates that the effect can operate on any input art other than plugin groups which
		are replaced by their result art. */
	kAnyInputArtButPluginArt	= kAnyInputArt & ~kPluginInputArt,

	// Special values that don't correspond to regular art types should be in the high half word

	/** Wants strokes to be converted to outlines before being filtered (not currently implemented) */
	kOutlinedStrokeInputArt		= 0x10000,
	/** Doesn't want to take objects that are clipping paths or clipping text (because it destroys them,
		e.g. by rasterizing, or by splitting a single path into multiple non-intersecting paths,
		or by turning it into a plugin group, like the brush filter).
		This flag is on for "Not OK" instead of on for "OK" because destroying clipping paths is
		an exceptional condition and we don't want to require normal filters to explicitly say they're OK.
		Also, it is not necessary to turn this flag on if you can't take any paths at all. */
	kNoClipMasksInputArt		= 0x20000

} AIStyleFilterPreferredInputArtType;


/** Ways that a live effect can be merged into the styles of selected objects.  If #kSendHandleMergeMessage
	is or'ed into the action, a #kSelectorAILiveEffectHandleMerge message will be sent to handle the parameters
	of the merge  NOTE -- not currently implemented for #kAppendLiveEffectToStyle */
typedef enum {
	kAppendLiveEffectToStyle,
	kReplaceFillWithLiveEffect,
	kReplaceStrokeWithLiveEffect,
	kSendHandleMergeMessage = 0x10000
} AILiveEffectMergeAction;


/** Information that is supplied when registering a live effect. */
typedef struct {
	/** The plugin that is registering the effect. Messages are sent to this
		plugin in order to execute the effect, edit its parameters etc. */
	SPPluginRef self;
	/** Non-localized name of the effect */
	char* name;
	/** Localized title of the effect */
	char *title;
	long majorVersion;
	long minorVersion;
	/** #AIStyleFilterPreferredInputArtType */
	long prefersAsInput;
	/** #AIStyleFilterFlags */
	long styleFilterFlags;
} AILiveEffectData;



//
// The contents of Live Effect messages.
//

/** Message sent to a registered effect plug-in when it is to execute on some art.
	
	- In: 'effect' handle returned when the effect was registered.
	- In: 'parameters' handle to live effect parameter set to be used.
	- In/Out: 'instanceInfo' handle to a set of instance parameters the plug-in may have stored (e.g.
		random number seeds, etc )
	- In/Out: 'art' handle to the art to be edited.  The effect is responsible for the
		proper deletion of any objects that are part of the source art that it wishes to
		remove.  It may also set 'art' to nil to indicate that there is no
		representation for its result.  If 'art' is set to nil, the core will attempt 
		disposal of the input art.

	During a go message the effect may NOT modify ancesteral objects to 'art', and
	should not interact with the document art tree.
*/
typedef struct {
	SPMessageData d;
	AILiveEffectHandle effect;			// In
	AILiveEffectParameters parameters;	// In
	AILiveEffectParameters instanceInfo;// In/Out
	AIArtHandle	art;					// In/Out
} AILiveEffectGoMessage;

/** Message sent to a registered effect plug-in to edit its parameter set.

	- In: 'effect' handle returned when the effect was registered.
	- In/Out: 'parameters' handle to live effect parameter set to be edited.
	- In: 'context' handle to be passed back to various APIs.
	- In: 'allowPreview' bool specifies whether preview should be enabled or not.
		if enabled, the effect may call UpdateParameters() after it modifies
		the passed parameters to update the associated artwork.  The effect should
		be ready to receive an AILiveEffectGoMessage if it calls UpdateParameters().
*/
typedef struct {
	SPMessageData d;
	AILiveEffectHandle effect;			// In
	AILiveEffectParameters parameters;	// In/Out
	AILiveEffectParamContext context;	// In
	AIBoolean allowPreview;				// In
} AILiveEffectEditParamMessage;

/** Message sent to a registered effect plug-in to request it to interpolate its parameters.

	- In: 'effect' handle returned when the effect was registered.
	- In: 'startParams' handle to live effect parameter set that represents the initial parameter values.
	- In: 'endParams' handle to live effect parameter set that represents the final parameter values.
	- Out: 'outParams' handle to live effect parameter set to be filled in by the effect.
	- In: 'percent' value in [0,1]
*/
typedef struct {
	SPMessageData d;
	AILiveEffectHandle effect;			// In
	AILiveEffectParameters startParams; // In
	AILiveEffectParameters endParams;	// In
	AILiveEffectParameters outParams;	// Out
	AIReal percent;						// In: percent between startParams and endParams for outParams
} AILiveEffectInterpParamMessage;

/** Message sent to a registered effect plug-in to get its input type. 
	Only sent if the input type of the plug-in is kInputArtDynamic.
	If the type computation is more complex and depends upon the input art, the
	message handler should examine the input art, and if it's something that it can
	filter set the typeMask to kAnyInputArt.  If it can't filter it, it should set the
	mask to what it would like the art converted to.

	- In: 'effect' handle returned when the effect was registered.
	- In: 'params' handle to live effect parameter set
	- In: 'inputArt' handle to art that is going to be styled
	- Out: 'typeMask' the types of art that can be operated upon with this parameter set
*/
typedef struct {
	SPMessageData d;
	AILiveEffectHandle effect;			// In
	AILiveEffectParameters parameters;	// In
	AIArtHandle inputArt;				// In
	long typeMask;						// Out
} AILiveEffectInputTypeMessage;

/** Message sent to a registered effect plug-in to convert any private color data
	it may have stored in a dictionary to the indicated color type.  Note that this
	message is only for private color information; normal fill and stroke color 
	information will be converted for the effect.
	
	- In: 'effect' handle returned when the effect was registered.
	- In: 'parameters' handle to live effect parameter set
	- In: 'newColorSpace' the color space to which private data should be converted
*/
typedef struct {
	SPMessageData d;
	AILiveEffectHandle effect;			// In
	AILiveEffectParameters parameters;	// In
	AIColorTag newColorSpace;			// In
} AILiveEffectConvertColorMessage;

/** Message sent to a registered effect plug-in to scale its parameters

	- In: 'effect' handle returned when the effect was registered.
	- In/Out: 'parameters' handle to live effect parameter set to be scaled
	- In: 'scaleFactor' factor by which to scale the params
	- Out: 'scaledParams' boolean returned stating whether the effect modified the 
		parameters at all.
*/
typedef struct {
	SPMessageData d;
	AILiveEffectHandle effect; 				// In
	AILiveEffectParameters parameters;		// In/Out
	AIReal scaleFactor;						// In
	AIBoolean scaledParams;				// Out
} AILiveEffectScaleParamMessage;

/** Message sent to the plugin to handle merge.  Only sent if the plugin initiated
	the merge itself through MergeLiveEffectIntoSelection.

	- In: 'effect' handle returned when the effect was registered
	- In: 'oldEffect' effect currently there (if any).  Will be nil if replacing a default fill or stroke
		with an effect
	- Out: 'dontMerge' if set to false, do not do the merge
	- Out: 'keepInstanceInfo' if set to true, preserve the instance info from the existing filter (if any)
	- In/Out: 'parameters' parameter dictionary of existing filter (if any) on input, and
		new parameter dictionary on output.  May be set to nil, which
		will cause EditParameters message to be sent
*/
typedef struct {
	SPMessageData d;
	AILiveEffectHandle effect; 				// In
	AILiveEffectHandle oldEffect;			// In
	ASBoolean dontMerge;					// Out
	ASBoolean keepInstanceInfo;				// Out
	AILiveEffectParameters parameters;		// In/Out
} AILiveEffectHandleMergeMessage;

/** Message sent to an effect plug-in to obtain an SVG filter representation of that effect.
	This message is only sent to an effect plug-in that claimed it can generate an SVG filter
	to represent itself.

	- In: 'effect' handle returned when the effect was registered.
	- In: 'parameters' handle to live effect parameter set to be used.
	- Out: 'svgFilterUID' return a handle to the UID representing the SVG filter.
*/
typedef struct {
	SPMessageData d;
	AILiveEffectHandle effect; 				// In
	AILiveEffectParameters parameters;		// In
	AISVGFilterHandle svgFilter;			// Out
} AILiveEffectGetSVGFilterMessage;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	A live effect is a filter that can be attached to an art object. Any time the art
	object is changed the filter is automatically re-executed. The effect is attached
	to the art object as a part of its style. More than one filter can be attached to
	an object through its style enabling chains and stacks of filters. A chain of
	filters is a pipeline where the output of one filter becomes the input to the next.
	A stack passes the same input art to a set of filters the output of which is then
	stacked together.

	A live effect can be implemented in a plugin. If a document contains a live effect
	that is not available the document can still be opened. However, any art with the live
	effect applied cannot be edited.

	As a part of an Illustrator document a live effect has a number of responsibilities
	it must implement in order to interact with the document structure. For example,
	when the art it is attached to is edited the effect must execute. These responsibilities
	are communicated to the plugin that implements the live effect through messages. The
	messages all have the caller #kCallerAILiveEffect and one of the following selectors:

	- #kSelectorAIEditLiveEffectParameters
	- #kSelectorAIGoLiveEffect
	- #kSelectorAILiveEffectInterpolate
	- #kSelectorAILiveEffectInputType
	- #kSelectorAILiveEffectConverColorSpace
	- #kSelectorAILiveEffectScaleParameters
	- #kSelectorAILiveEffectHandleMerge
	- #kSelectorAILiveEffectGetSVGFilter

	Most live effects have a set of parameters that control their behaviour. For
	example the roughen effect has parameters for the size and detail of the roughening.
	Associated with instance of a live effect in a style is a parameter dictionary
	where these parameters may be stored. The AIDictionarySuite can be used to access
	the contents of the dictionary. The live effect can store any information it chooses
	in the dictionary. However the following keys in the parameter dictionary have special
	interpretations.

	- #kDisplayStringKey
	- #kExtraStringKey
	- #kFillStyleKey
	- #kEvenOddKey
	- #kStrokeStyleKey
	- #kScaleFactorKey
*/
typedef struct {

	/** Call made by a plug-in to register a live effect */
	AIAPI AIErr (*AddLiveEffect) ( AILiveEffectData* effectInfo, AILiveEffectHandle *liveEffectHandle );

	/** Add a menu item for a live effect to the Effects menu using the passed
		filterHandle's menu item to provide the category, title, and options 
		for the created live effect menu item.
		Menu parameters may be nil if the plugin doesn't care about them */
	AIAPI AIErr (*AddLiveEffectFilterItem) ( AILiveEffectHandle liveEffectHandle,
		AIFilterHandle filterHandle, AIMenuItemHandle *menuItem, AIMenuGroup *menuGroup );
	/** Add a menu item for a live effect to the Effects menu from scratch.
		Menu parameters can be nil if the plugin doesn't care about them.
		menuData's 'category' pointer may be set to nil if the menu item is to be 
		placed in the top-level clusters of the Effects menu.  Note, if nil is passed
		for the 'category', nil will be returned for the menuGroup. */
	AIAPI AIErr (*AddLiveEffectMenuItem) ( AILiveEffectHandle liveEffectHandle, const char* menuName,
		 AddLiveEffectMenuData *menuData, AIMenuItemHandle *menuItem, AIMenuGroup *menuGroup );

	/** Asks the passed live effect item to execute on the passed art with the
		passed parameter set. */
	AIAPI AIErr (*ExecuteLiveEffect) ( AILiveEffectHandle effect,
				AILiveEffectParameters parameters, AIArtHandle art );

	/** Asks the passed live effect item to get parameters from the user */
	AIAPI AIErr (*EditParameters) ( AILiveEffectParamContext context, ASBoolean enablePreview );
	AIAPI AIErr (*UpdateParameters) ( AILiveEffectParamContext context );
	
	/** Asks the passed live effect to interpolate its parameters between startParams and
		endParams at the 'percent' point between them.  'percent' should be in the range [0,1] */
	AIAPI AIErr (*InterpolateEffectParameters) ( AILiveEffectHandle effect,
				AILiveEffectParameters startParams, AILiveEffectParameters endParams,
				AIReal percent, AILiveEffectParameters resultParams );

	/** Returns the number of live effects that have been registered. */
	AIAPI AIErr (*CountLiveEffects) ( long* count );
	/** An index into the list of known live effects 'n' is passed to the function
		which returns the handle for the corresponding live effect. */
	AIAPI AIErr (*GetNthLiveEffect) ( long n, AILiveEffectHandle* effect );

	/** Return the name of the live effect associated with the passed handle. */
	AIAPI AIErr (*GetLiveEffectName) ( AILiveEffectHandle effect, const char** name );
	/** Return the title of the live effect associated with the passed handle. */
	AIAPI AIErr (*GetLiveEffectTitle) ( AILiveEffectHandle effect, const char** name );
	/** Return the version of the live effect associated with the passed handle. */
	AIAPI AIErr (*GetLiveEffectVersion) ( AILiveEffectHandle effect, long* major, long* minor );
	/** Return the preferred input types of the live effect associated with the passed handle. */
	AIAPI AIErr (*GetInputPreference) ( AILiveEffectHandle effect, long* inputPreference );
	/** Return the filter flags of the live effect associated with the passed handle. */
	AIAPI AIErr (*GetStyleFilterFlags) ( AILiveEffectHandle effect, long* styleFilterFlags );

	/** Merge a live effect into the style of all selected objects. */
	AIAPI AIErr (*MergeLiveEffectIntoSelection) ( AILiveEffectHandle effect, AILiveEffectMergeAction action );

	/** Valid only while handling an AILiveEffectEditParamMessage; returns menu item used
		to invoke the live effect. Returns nil if called during other times, or if the
		message was not a result of the user invoking a menu item. */
	AIAPI AIMenuItemHandle (*GetMenuItem) ( AILiveEffectParamContext context );
	
	/** Create a parameter dictionary to be used in constructing a style with the
		AIArtStyleParserSuite.  Should not be used in other circumstances.
		Initial reference count for this dictionary is 1; caller must release it
		with sAIDictionary->Release when finished using it. */
	AIAPI AIErr (*CreateLiveEffectParameters) ( AILiveEffectParameters* params );

	/** Get the minimum AI version that this live effect supports. Values for the appVersion
		are defined in AITypes.h. Live effects that do not specifically use this api will default to
		version 9. Setting the version prior to version 9 will return an error, since live effects 
		are not supported in AI versions prior to 9.

		When Illustrator writes out an object with this live effect, it will check the app version, 
		and if it is writing out to a prior file format, it will only write out the styled art. */
	AIAPI AIErr (*GetLiveEffectAppVersion) ( AILiveEffectHandle effect, AIVersion* appVersion );
	/** Set the minimum AI version that this live effect supports. See GetLiveEffectAppVersion()
		for more information. */
	AIAPI AIErr (*SetLiveEffectAppVersion) ( AILiveEffectHandle effect, AIVersion appVersion );

} AILiveEffectSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif



#endif
