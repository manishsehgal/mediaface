#ifndef __AIMesh__
#define __AIMesh__

/*
 *        Name:	AIMesh.h
 *   $Revision: 27 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Mesh Object Suite.
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AIColor__
#include "AIColor.h"
#endif

#ifndef __AIHitTest__
#include "AIHitTest.h"
#endif

#ifndef __AIRealBezier__
#include "AIRealBezier.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIMesh.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIMeshSuite							"AI Mesh Suite"
#define kAIMeshSuiteVersion3					AIAPI_VERSION(3)
#define kAIMeshSuiteVersion						kAIMeshSuiteVersion3
#define kAIMeshVersion							kAIMeshSuiteVersion

#define kAIMeshHitSuite							"AI Mesh Hit Suite"
#define kAIMeshHitSuiteVersion1					AIAPI_VERSION(1)
#define kAIMeshHitSuiteVersion					kAIMeshHitSuiteVersion1
#define kAIMeshHitVersion						kAIMeshHitSuiteVersion

#define kAIMeshSelectionSuite					"AI Mesh Selection Suite"
#define kAIMeshSelectionSuiteVersion1			AIAPI_VERSION(1)
#define kAIMeshSelectionSuiteVersion			kAIMeshSelectionSuiteVersion1
#define kAIMeshSelectionVersion					kAIMeshSelectionSuiteVersion

#define kAIMeshPatchIteratorSuite				"AI Mesh Patch Suite"
#define kAIMeshPatchIteratorSuiteVersion1		AIAPI_VERSION(1)
#define kAIMeshPatchIteratorSuiteVersion		kAIMeshPatchIteratorSuiteVersion1
#define kAIMeshPatchIteratorVersion				kAIMeshPatchIteratorSuiteVersion

#define kAIMeshVertexIteratorSuite				"AI Mesh Vertex Suite"
#define kAIMeshVertexIteratorSuiteVersion1		AIAPI_VERSION(1)
#define kAIMeshVertexIteratorSuiteVersion		kAIMeshVertexIteratorSuiteVersion1
#define kAIMeshVertexIteratorVersion			kAIMeshVertexIteratorSuiteVersion

#define kAIMeshSegmentIteratorSuite				"AI Mesh Segment Suite"
#define kAIMeshSegmentIteratorSuiteVersion1		AIAPI_VERSION(1)
#define kAIMeshSegmentIteratorSuiteVersion		kAIMeshSegmentIteratorSuiteVersion1
#define kAIMeshSegmentIteratorVersion			kAIMeshSegmentIteratorSuiteVersion


/** Kinds of gradient mesh */
enum AIMeshKind {
	kAICartesianMesh				= 0,
	kAIPolarMesh					= 1
};

/** Axes */
enum AIMeshAxis {
	kAIMeshIAxis					= 0,
	kAIMeshJAxis					= 1
};

/** Directions along axes */
enum AIMeshAxisSense {
	kAIMeshAxisIncreasing			= 0,
	kAIMeshAxisDecreasing			= 1
};

/** Directions around patches */
enum AIMeshPatchDirection {
	kAIMeshClockwise				= 0,
	kAIMeshCounterClockwise			= 1
};

/** Parts of a mesh which can be hit */
enum AIMeshPart {
	kAIMeshNowhere					= 0,
	kAIMeshVertex					= 1,
	kAIMeshTangent					= 2,
	kAIMeshEdge						= 3,
	kAIMeshPatch					= 4
};

/** Variants for selection bounds */
enum AIMeshSelectionBoundsVariant {
	/** Computes a bounding box of the parts of the mesh that transform rigidly when
		a selection is transformed. This excludes from the bounds segments that are
		attached to a pair of vertices one of which transforms and the other of which
		remains fixed. */
	kAIMeshSelectionRigidBounds		= 0
};


/*******************************************************************************
 **
 **	Types
 **
 **/

typedef struct _AIMeshSelection			*AIMeshSelection;
typedef struct _AIMeshPatchIterator		*AIMeshPatchIterator;
typedef struct _AIMeshVertexIterator	*AIMeshVertexIterator;
typedef struct _AIMeshSegmentIterator	*AIMeshSegmentIterator;

typedef void (*AIMeshPointMap) (AIRealPoint* point, void* userData);
typedef void (*AIMeshColorMap) (AIColor* color, void* userData);
typedef void (*AIMeshColorQuery) (AIColor* color, void* userData);


/*******************************************************************************
 **
 **	Suite
 **
 **/


/**
	A mesh object is an object in an Illustrator artwork. It is
	identified by an AIArtHandle and created using NewArt. All the
	plug-in API methods which apply to generic artwork objects also
	apply to mesh objects.

	Two types of mesh were originally conceived�polar and
	cartesian. Only the cartesian variant has been implemented.
	A mesh can be though of in two ways. On the one hand it is an
	array of patches. Each patch has four sides which it shares with
	its neighbors. The corners of a patch are called nodes and it is to
	these that color is applied. The colors of the corners then blend
	across the interior of the patch.

	The other view is as a set of cubic bezier curves joined together
	to form a mesh. The point at which two or more beziers meet is
	termed a knot. To reduce the complexity of a mesh as seen by a
	user only a subset of the knots are visible. Those which are
	visible are termed vertices. The sequence of beziers between two
	vertices is termed a segment.

	Just as for the beziers describing a path, the user interface
	provides a method for adjusting the shape of the curve between
	two vertices. The points which manipulate the shape are referred
	to as controls. Unlike a path, the curve may not be a single
	bezier. Consequently the controls presented for shape
	adjustment may not be coincident with the points of the bezier
	control polygon.

	A coordinate system can be imposed on a mesh. For an M by N patch
	mesh the coordinates range from 0 to M and from 0 to N. Integer values
	of the coordinates refer to nodes whilst non-integral values refer
	to points in the interior or on the sides of patches. When using
	strictly integral values we refer to the axes as the I and J axes.
	When using real values we refer to them as the U and V axes.
	
	The obvious correspondence is made between patches and their
	coordinates within the array.

	When traversing a mesh it is useful to refer to directions with
	respect to a given element. Directions along the axes are referred
	to as increasing or decreasing. Those around a patch are referred
	to as clockwise or counter clockwise. For example, if the current
	element is the node at (2,2) then:

	- (3,1) is the next node increasing along the I axis,
	- (2,1) is the next node decreasing along the J axis,
	- (1,2) is the next node counter clockwise around patch (1,1),
	- (2,1) is the next node clockwise around patch (1,1).
	
	The term sense is used to refer to direction along an axis whilst
	direction is reserved for direction around a patch.

	There are six plug-in API suites which are specific to meshes.
	Three of these provide iterators for traversing and manipulating
	the elements of a mesh (AIMeshPatchIteratorSuite, AIMeshVertexIteratorSuite,
	AIMeshSegmentIteratorSuite). One suite interprets the information
	returned when hit testing a mesh (AIMeshHitSuite). The remaining two
	suites manipulate the mesh as a whole (AIMeshSuite) and selections of
	a mesh (AIMeshSelectionSuite).

	Most Illustrator objects couple the selection and the object such
	that there is exactly one selection for an object. Meshes do not
	do this. There may be more than one selection for a mesh. There
	is, however, one unique selection associated with a mesh object
	which is the selection as presented in the user interface.

	Mesh objects are traversed and manipulated through iterators.
	There are iterators for traversing the patches, vertices and
	segments of a mesh.

	Each iterator is reference counted. This means it is necessary to
	call the Release method of an iterator when you are no longer
	using it. When you obtain an iterator its reference count is
	initially 1 so there is usually no need to call AddRef. Failure to
	release iterators will result in a memory leak.

	For traversal the iterators have an AtEnd method and a set of
	Next... and Prev... methods.

	AtEnd returns true when an iterator has walked off the mesh.
	Once it returns true the iterator cannot be reversed. For
	example, if using NextI to iterate the patches along the I axis,
	AtEnd will return true once the last patch has been visited. PrevI
	cannot then be used to return to the last patch of the axis.

	All of the iterators have the Next and Prev methods. These are
	only useful for iterators obtained from a GetStart... method of a
	mesh object. Repeated calls to Next will guarantee that every
	element is visited once and only once. The order of traversal is
	undefined.

	NextI, PrevI, NextJ, PrevJ are for traversing the elements along a
	given axis.

	The iterators for segments are unique because a segment iterator
	is directed. That is, a segment iterator has a start vertex and an
	end vertex. These vertices together indicate the axis and
	direction in which it travels.

	The NextOnPatch and PrevOnPatch methods for segment iterators
	differ from NextOnAxis and PrevOnAxis only when they
	encounter a corner of a patch. In this case they switch to a new
	axis by making a half turn either clockwise or counter clockwise.
*/
typedef struct {

	/** Sets the dimensions and type of a mesh object. The dimensions are specified in
		terms of patches. The geometric and color data is set to default values. */
	AIAPI AIErr (*InitCartesian) (AIArtHandle mesh, long i, long j);
	/** Not implemented. */
	AIAPI AIErr (*InitPolar) (AIArtHandle mesh, long i, long j);
	/** Returns the type of the mesh. See #AIMeshKind for values. */
	AIAPI AIErr (*GetKind) (AIArtHandle mesh, long *kind);
	/** Return the size of the mesh in terms of patches. */
	AIAPI AIErr (*GetSize) (AIArtHandle mesh, long *i, long *j);

	/** NewSelection returns a new selection for the mesh. The selection
		is distinct from that seen through the user interface. */
	AIAPI AIErr (*NewSelection) (AIArtHandle mesh, AIBoolean all, AIMeshSelection* selection);
	/** GetSelection returns the user interface selection for the mesh. Changing this
		selection changes the selection seen by the user. */
	AIAPI AIErr (*GetSelection) (AIArtHandle mesh, AIMeshSelection* selection);

	/** Returns a patch iterator corresponding the the patch (i,j). */	
	AIAPI AIErr (*GetPatch) (AIArtHandle mesh, long i, long j, AIMeshPatchIterator* patch);
	/** Returns a patch iterator for which calling AIMeshPatchIteratorSuite::Next()
		repeatedly is guaranteed to visit all patches. */
	AIAPI AIErr (*GetStartPatch) (AIArtHandle mesh, AIMeshPatchIterator* patch);

	/** Returns a vertex iterator corresponding the the node (i,j). */	
	AIAPI AIErr (*GetNode) (AIArtHandle mesh, long i, long j, AIMeshVertexIterator* vertex);
	/** Returns a vertex iterator for which calling AIMeshVertexIteratorSuite::Next()
		repeatedly is guaranteed to visit all vertices. */
	AIAPI AIErr (*GetStartVertex) (AIArtHandle mesh, AIMeshVertexIterator* vertex);

	/** Returns a segment iterator for which calling AIMeshSegmentIteratorSuite::Next()
		repeatedly is guaranteed to visit all segments. */
	AIAPI AIErr (*GetStartSegment) (AIArtHandle mesh, AIMeshSegmentIterator* segment);

	/** GetColorSpace returns the color space used to render the mesh.
		The colors of the nodes of the mesh can be drawn from many
		color spaces. These are all converted to a single color space for
		rendering. */
	AIAPI void (*GetColorSpace) (AIArtHandle mesh, AIColorTag* kind, AICustomColorHandle* custom);
	/** InvertPoint takes a point in artwork cooordinates and returns the
		corresponding (u,v) coordinates of that point within the mesh. */
	AIAPI AIBoolean (*InvertPoint) (AIArtHandle mesh, AIRealPoint* point, AIReal* u, AIReal* v);
	/** EvalPoint takes a (u,v) coordinat and returns the artwork coordinate of
		that point. */
	AIAPI void (*EvalPoint) (AIArtHandle mesh, AIReal u, AIReal v, AIRealPoint* point);
	/** EvalColor takes a (u,v) coordinate and returns the color of the mesh
		at that point. */
	AIAPI void (*EvalColor) (AIArtHandle mesh, AIReal u, AIReal v, AIColor* color);

	/** Transforms the points of the mesh by the matrix. */
	AIAPI AIErr (*Transform) (AIArtHandle mesh, AIRealMatrix* matrix);
	/** The function f is assumed to have continuous first derivative. The points of
		the mesh are transformed according to the function. */
	AIAPI AIErr (*MapPoints) (AIArtHandle mesh, AIMeshPointMap f, void* userData);
	/** The colors of the gradient mesh are transformed according to the color
		mapping function. */
	AIAPI AIErr (*MapColors) (AIArtHandle mesh, AIMeshColorMap f, void* userData);
	/** The colors of the mesh are passed to the function. This does not modify
		the mesh colors. */
	AIAPI void (*QueryColors) (AIArtHandle mesh, AIMeshColorQuery f, void* userData);

	/** Splits the mesh at the specified u value and returns the index of the
		resulting grid line. */
	AIAPI AIErr (*InsertU) (AIArtHandle mesh, AIReal u, long* i);
	/** Splits the mesh at the specified v value and returns the index of the
		resulting grid line. */
	AIAPI AIErr (*InsertV) (AIArtHandle mesh, AIReal v, long* j);
	/** DeleteGridLines removes those lines of the mesh grid passing
		through (u,v). To only delete a horizontal or vertical line v or u
		should be non-integral. */
	AIAPI AIErr (*DeleteGridLines) (AIArtHandle mesh, AIReal u, AIReal v);
	
	AIAPI void (*TrackBegin) (AIArtHandle mesh);
	AIAPI void (*TrackEnd) (AIArtHandle mesh);

	AIAPI AIErr (*EndSelectionChanges) (void);

	AIAPI AIErr (*DropColor) (AIHitRef hit, const AIColor* color);
	
	/** Return the relative u and v coordinates (in the range [0,1]) that the given
		i and j nodes correspond to in the given mesh. */
	AIAPI AIErr (*GetRelativeU) (AIArtHandle mesh, long i, AIReal* u);
	AIAPI AIErr (*GetRelativeV) (AIArtHandle mesh, long j, AIReal* v);

} AIMeshSuite;


/** The AIHitTestSuite returns an AIHitRef which identifies the
	part of the object hit. If the object is a mesh then the methods
	of this suite may be used to further interrogate the hit. */
typedef struct {

	/** GetPart returns an identifier indicating the element of the mesh
		which was hit. See #AIMeshPart. */
	AIAPI long (*GetPart) (AIHitRef hit);
	/** GetUV returns the (u,v) coordinates of the hit. */
	AIAPI void (*GetUV) (AIHitRef hit, AIReal* u, AIReal* v);
	/** GetSelection constructs a new selection from the hit. The selection
		contains the element which was hit. */
	AIAPI AIErr (*GetSelection) (AIHitRef hit, AIMeshSelection* selection);
	
	/** If a vertex or tangent was hit returns an iterator for the vertex. */
	AIAPI AIErr (*GetVertex) (AIHitRef hit, AIMeshVertexIterator* vertex);
	/** If a tangent was hit returns the tangent index. */
	AIAPI AIErr (*GetTangentIndex) (AIHitRef hit, long* index);
	/** If a segment was hit returns an iterator for the segment. */
	AIAPI AIErr (*GetSegment) (AIHitRef hit, AIMeshSegmentIterator* segment);
	/** If a patch was hit returns an iterator for the patch. */
	AIAPI AIErr (*GetPatch) (AIHitRef hit, AIMeshPatchIterator* patch);

} AIMeshHitSuite;


/**
	Selections are reference counted. This means it is necessary to call the Release()
	method of a selection when you are no longer using it. When you obtain a selection
	its reference count is initially 1 so there is usually no need to call AddRef().
	Failure to release selections will result in a memory leak.
 */
typedef struct {

	AIAPI ASInt32 (*AddRef) (AIMeshSelection selection);
	AIAPI ASInt32 (*Release) (AIMeshSelection selection);

	/** Copies the selection from the given selection. */
	AIAPI AIErr (*Copy) (AIMeshSelection selection, AIMeshSelection from);
	/** Makes a duplicate of the selection. */
	AIAPI AIErr (*Clone) (AIMeshSelection selection, AIMeshSelection* clone);

	AIAPI AIBoolean (*IsEmpty) (AIMeshSelection selection);
	AIAPI AIBoolean (*IsFull) (AIMeshSelection selection);
	AIAPI AIBoolean (*IsEqual) (AIMeshSelection selection1, AIMeshSelection selection2);
	AIAPI AIBoolean (*Contains) (AIMeshSelection selection1, AIMeshSelection selection2);
	AIAPI AIBoolean (*ContainsVertex) (AIMeshSelection selection, AIMeshVertexIterator vertex);
	AIAPI AIBoolean (*ContainsSegment) (AIMeshSelection selection, AIMeshSegmentIterator segment);
	/** RendersControl tests whether the reshaping control of a given segment would be
		visible under the given selection. */
	AIAPI AIBoolean (*RendersControl) (AIMeshSelection selection, AIMeshSegmentIterator segment);

	/** Modifies selection1 to be the intersection of itself and selection2. */
	AIAPI AIErr (*Intersect) (AIMeshSelection selection1, AIMeshSelection selection2);
	/** Modifies selection1 to be the union of itself and selection2. */
	AIAPI AIErr (*Union) (AIMeshSelection selection1, AIMeshSelection selection2);
	/** Modifies selection1 to invert the selected sense of the parts of the mesh identified
		by selection2 (e.g if a vertex is identified by selection2 its selection state will
		be toggled in selection1. */
	AIAPI AIErr (*Invert) (AIMeshSelection selection1, AIMeshSelection selection2);

	/** Adds or subtracts the patch from the selection. */
	AIAPI AIErr (*SelectPatch) (AIMeshSelection selection, AIMeshPatchIterator patch, AIBoolean select);
	/** Adds or subtracts the vertex from the selection. */
	AIAPI AIErr (*SelectVertex) (AIMeshSelection selection, AIMeshVertexIterator vertex, AIBoolean select);
	/** Adds or subtracts the segment from the selection. */
	AIAPI AIErr (*SelectSegment) (AIMeshSelection selection, AIMeshSegmentIterator segment, AIBoolean select);

	/** The same as AIMeshSuite::Transform() except that it applies to the selected parts of the mesh. */
	AIAPI AIErr (*Transform) (AIMeshSelection selection, AIRealMatrix* matrix);
	/** The same as AIMeshSuite::MapPoints() except that it applies to the selected parts of the mesh. */
	AIAPI AIErr (*MapPoints) (AIMeshSelection selection, AIMeshPointMap f, void* userData);
	/** The same as AIMeshSuite::MapColors() except that it applies to the selected parts of the mesh. */
	AIAPI AIErr (*MapColors) (AIMeshSelection selection, AIMeshColorMap f, void* userData);
	/** The same as AIMeshSuite::QueryColors() except that it applies to the selected parts of the mesh. */
	AIAPI void (*QueryColors) (AIMeshSelection selection, AIMeshColorQuery f, void* userData);

	/** Bounds the pieces of the mesh identified by the selection. The variant parameter specifies
		options on how the bounds are computed. See #AIMeshSelectionBoundsVariant. */
	AIAPI void (*BoundPieces) (AIMeshSelection selection, AIRealMatrix* matrix, long variant, AIRealRect* bounds);

} AIMeshSelectionSuite;


/**
	See AIMeshSuite for an overview of iterators.

	Iterators are reference counted. This means it is necessary to call the Release()
	method of an iterator when you are no longer using it. When you obtain an iterator
	its reference count is initially 1 so there is usually no need to call AddRef().
	Failure to release iterators will result in a memory leak.
 */
typedef struct {

	AIAPI ASInt32 (*AddRef) (AIMeshPatchIterator patch);
	AIAPI ASInt32 (*Release) (AIMeshPatchIterator patch);

	AIAPI AIErr (*Copy) (AIMeshPatchIterator patch, AIMeshPatchIterator from);
	AIAPI AIErr (*Clone) (AIMeshPatchIterator patch, AIMeshPatchIterator* clone);

	AIAPI AIBoolean (*IsEqual) (AIMeshPatchIterator patch1, AIMeshPatchIterator patch2);

	AIAPI AIBoolean (*AtEnd) (AIMeshPatchIterator patch);
	AIAPI void (*Next) (AIMeshPatchIterator patch);
	AIAPI void (*Prev) (AIMeshPatchIterator patch);
	AIAPI void (*NextI) (AIMeshPatchIterator patch);
	AIAPI void (*PrevI) (AIMeshPatchIterator patch);
	AIAPI void (*NextJ) (AIMeshPatchIterator patch);
	AIAPI void (*PrevJ) (AIMeshPatchIterator patch);

	/** CountNodes returns the number of patch nodes�always 4 at present. */
	AIAPI long (*CountNodes) (AIMeshPatchIterator patch);
	/** GetNodeN returns an iterator for the Nth node. The nodes are numbered from 0 to 3 counter
		clockwise starting at the bottom left of the patch. */
	AIAPI AIErr (*GetNodeN) (AIMeshPatchIterator patch, long n, AIMeshVertexIterator* vertex);
	/** GetNodeIJ returns an iterator for the node identified by an (I,J) offset relative to the
		bottom left node of the patch.*/
	AIAPI AIErr (*GetNodeIJ) (AIMeshPatchIterator patch, long i, long j, AIMeshVertexIterator* vertex);
	/** GetSegmentN returns an iterator for one of the two segments adjacent to the patch and
		connected to the Nth node. The nodes are numbered from 0 to 3 counter clockwise starting
		at the bottom left of the patch. The direction parameter selects the segment. See
		#AIMeshPatchDirection. */
	AIAPI AIErr (*GetSegmentN) (AIMeshPatchIterator patch, long n, long direction, AIMeshSegmentIterator* segment);
	/** GetSegmentIJ returns an iterator for one of the two segments adjacent to the patch and
		connected to the node identified by an (I,J) offset relative to the bottom left node of
		the patch.  The direction parameter selects the segment. See #AIMeshPatchDirection. */
	AIAPI AIErr (*GetSegmentIJ) (AIMeshPatchIterator patch, long i, long j, long direction, AIMeshSegmentIterator* segment);

	/** Returns the number of vertices around the patch including nodes. */
	AIAPI long (*CountVerticies) (AIMeshPatchIterator patch);
	/** The corresponding operation to GetNodeN() for vertices. */
	AIAPI AIErr (*GetVertexM) (AIMeshPatchIterator patch, long m, AIMeshVertexIterator* vertex);
	/** The corresponding operation to GetSegmentN() for vertices. */
	AIAPI AIErr (*GetSegmentM) (AIMeshPatchIterator patch, long m, long direction, AIMeshSegmentIterator* segment);

	/** FindVertex given a vertex iterator determines the ordinal position of the vertex around
		the patch. */
	AIAPI AIErr (*FindVertex) (AIMeshPatchIterator patch, AIMeshVertexIterator vertex, long* m);

	/** Returns the (i,j) coordinate of the patch within the mesh. */
	AIAPI void (*GetIJ) (AIMeshPatchIterator patch, long* i, long* j);

} AIMeshPatchIteratorSuite;


/**
	See AIMeshSuite for an overview of iterators.

	Iterators are reference counted. This means it is necessary to call the Release()
	method of an iterator when you are no longer using it. When you obtain an iterator
	its reference count is initially 1 so there is usually no need to call AddRef().
	Failure to release iterators will result in a memory leak.

	The segments and tangents associated with a vertex are addressed by their index. This
	index ranges from 0 to the number of segments connected.
 */
typedef struct {

	AIAPI ASInt32 (*AddRef) (AIMeshVertexIterator vertex);
	AIAPI ASInt32 (*Release) (AIMeshVertexIterator vertex);

	AIAPI AIErr (*Copy) (AIMeshVertexIterator vertex, AIMeshVertexIterator from);
	AIAPI AIErr (*Clone) (AIMeshVertexIterator vertex, AIMeshVertexIterator* clone);

	AIAPI AIBoolean (*IsEqual) (AIMeshVertexIterator vertex1, AIMeshVertexIterator vertex2);

	AIAPI AIBoolean (*AtEnd) (AIMeshVertexIterator vertex);
	AIAPI void (*Next) (AIMeshVertexIterator vertex);
	AIAPI void (*Prev) (AIMeshVertexIterator vertex);
	AIAPI void (*NextI) (AIMeshVertexIterator vertex);
	AIAPI void (*PrevI) (AIMeshVertexIterator vertex);
	AIAPI void (*NextJ) (AIMeshVertexIterator vertex);
	AIAPI void (*PrevJ) (AIMeshVertexIterator vertex);

	/** CountSegments returns the number of segments connected to a vertex. */
	AIAPI long (*CountSegments) (AIMeshVertexIterator vertex);
	/** IsNode returns true if the vertex iterator identifies a node. */
	AIAPI AIBoolean (*IsNode) (AIMeshVertexIterator vertex);
	/** GetSegmentIndex returns the index of the segment in a given direction along an axis.
		See #AIMeshAxis and #AIMeshAxisSense. */
	AIAPI long (*GetSegmentIndex) (AIMeshVertexIterator vertex, long axis, long sense);
	/** GetSegmentOpposite returns the index of the segment which is paired with a given one.
		It returns -1 if there is no pair. */
	AIAPI long (*GetSegmentOpposite) (AIMeshVertexIterator vertex, long i);
	
	/** Only valid for a node. */
	AIAPI AIErr (*SetColor) (AIMeshVertexIterator vertex, AIColor* c);
	/** Only valid for a node. */
	AIAPI void (*GetColor) (AIMeshVertexIterator vertex, AIColor* c);

	/** Sets the position of the ends of all segments attached to the node. */
	AIAPI AIErr (*SetPoint) (AIMeshVertexIterator vertex, AIRealPoint* point);
	/** Gets the position of the ends of all segments attached to the node. */
	AIAPI void (*GetPoint) (AIMeshVertexIterator vertex, AIRealPoint* point);
	/** GetUV returns the (U,V) coordinates of the node. */
	AIAPI void (*GetUV) (AIMeshVertexIterator vertex, AIReal* u, AIReal* v);

	/** Sets the second point of the bezier control polygon of the indicated segment. */
	AIAPI AIErr (*SetTangent) (AIMeshVertexIterator vertex, long i, AIRealPoint* point);
	/** Gets the second point of the bezier control polygon of the indicated segment. */
	AIAPI void (*GetTangent) (AIMeshVertexIterator vertex, long i, AIRealPoint* point);
	/** Sets whether adjusting the tangent of the segment similarly adjusts that of its opposite. */
	AIAPI AIErr (*SetSmooth) (AIMeshVertexIterator vertex, long i, AIBoolean smooth);
	/** Gets whether adjusting the tangent of the segment similarly adjusts that of its opposite. */
	AIAPI AIBoolean (*GetSmooth) (AIMeshVertexIterator vertex, long i);

	/** Gets an iterator for the segment. */
	AIAPI AIErr (*GetSegment) (AIMeshVertexIterator vertex, long i, AIMeshSegmentIterator* segment);
	/** GetPatch obtains an iterator for one of the patches adjacent to the given segment. */
	AIAPI AIErr (*GetPatch) (AIMeshVertexIterator vertex, long i, AIMeshPatchIterator* segment);

	/** IsOnBoundary is true if the vertex is on the edge of the mesh. */
	AIAPI AIBoolean (*IsOnBoundary) (AIMeshVertexIterator vertex);

} AIMeshVertexIteratorSuite;


/**
	See AIMeshSuite for an overview of iterators.

	Iterators are reference counted. This means it is necessary to call the Release()
	method of an iterator when you are no longer using it. When you obtain an iterator
	its reference count is initially 1 so there is usually no need to call AddRef().
	Failure to release iterators will result in a memory leak.
 */
typedef struct {

	AIAPI ASInt32 (*AddRef) (AIMeshSegmentIterator segment);
	AIAPI ASInt32 (*Release) (AIMeshSegmentIterator segment);

	AIAPI AIErr (*Copy) (AIMeshSegmentIterator segment, AIMeshSegmentIterator from);
	AIAPI AIErr (*Clone) (AIMeshSegmentIterator segment, AIMeshSegmentIterator* clone);

	AIAPI AIBoolean (*IsEqual) (AIMeshSegmentIterator segment1, AIMeshSegmentIterator segment2);

	AIAPI AIBoolean (*AtEnd) (AIMeshSegmentIterator segment);
	AIAPI void (*Next) (AIMeshSegmentIterator segment);
	AIAPI void (*Prev) (AIMeshSegmentIterator segment);
	AIAPI void (*NextOnAxis) (AIMeshSegmentIterator segment);
	AIAPI void (*PrevOnAxis) (AIMeshSegmentIterator segment);
	/** Advance to the next segment around the current patch. The direction parameter indicates
		whether to make clockwise turns at patch corners or counter-clockwise turns (and thus
		indicates which patch to follow). See #AIMeshPatchDirection. */
	AIAPI void (*NextOnPatch) (AIMeshSegmentIterator segment, long direction);
	/** Move to the previous segment around the current patch. The direction parameter indicates
		whether to make clockwise turns at patch corners or counter-clockwise turns (and thus
		indicates which patch to follow). See #AIMeshPatchDirection. */
	AIAPI void (*PrevOnPatch) (AIMeshSegmentIterator segment, long direction);
	/** Reverse switches the start and end vertices of the iterator. */
	AIAPI void (*Reverse) (AIMeshSegmentIterator segment);

	/** CountKnots returns the number of knots forming the segment excluding the start and end knots. */
	AIAPI long (*CountKnots) (AIMeshSegmentIterator segment);
	/** GetKnotUV returns the (U,V) coordinates of a knot. */
	AIAPI void (*GetKnotUV) (AIMeshSegmentIterator segment, long n, AIReal* u, AIReal* v);
	/** InsertKnots is used to add knots. The first parameter is the index and the second the count.
		The indices exclude the start and end knots. */
	AIAPI AIErr (*InsertKnots) (AIMeshSegmentIterator segment, long m, long n);
	/** DeleteKnots is used to remove knots. The first parameter is the index and the second the count.
		The indices exclude the start and end knots. */
	AIAPI AIErr (*DeleteKnots) (AIMeshSegmentIterator segment, long m, long n);

	/** CountPoints returns the number of points in the control polygon of the beziers describing the
		segment. */
	AIAPI long (*CountPoints) (AIMeshSegmentIterator segment);
	/** Sets the Nth points of the bezier control polygon. */
	AIAPI AIErr (*SetPoint) (AIMeshSegmentIterator segment, long i, AIRealPoint* point);
	/** Gets the Nth points of the bezier control polygon. */
	AIAPI void (*GetPoint) (AIMeshSegmentIterator segment, long i, AIRealPoint* point);

	/** Returns a vertex iterator for the start of the segment. */
	AIAPI AIErr (*GetStart) (AIMeshSegmentIterator segment, AIMeshVertexIterator* vertex);
	/** Returns a vertex iterator for the end of the segment. */
	AIAPI AIErr (*GetEnd) (AIMeshSegmentIterator segment, AIMeshVertexIterator* vertex);

	/** Split breaks a segment into two segments at the given knot. As a result the indexed knot
		becomes a vertex. The segment iterator identifies the initial segment after the split. */
	AIAPI AIErr (*Split) (AIMeshSegmentIterator segment, long n);
	/** Join joins a segment with its successor and returns the index of the knot at the join. The
		iterator identifies the joined segment. */
	AIAPI AIErr (*Join) (AIMeshSegmentIterator segment, long* n);

	/** GetControl returns the location of the control handle at the segment start used to
		manipulate its overall shape. */
	AIAPI void (*GetControl) (AIMeshSegmentIterator segment, AIRealPoint* control);
	/** Reshape given a new position for the start of the segment and its control handle modifies
		the control polygon to reshape the segment. */
	AIAPI AIErr (*Reshape) (AIMeshSegmentIterator segment, AIRealPoint* point, AIRealPoint* control);

	/** IsOnBoundary is true if the segment is on the edge of the mesh. */
	AIAPI AIBoolean (*IsOnBoundary) (AIMeshSegmentIterator segment);

} AIMeshSegmentIteratorSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
