#ifndef __AIPathConstruction__
#define __AIPathConstruction__

/*
 *        Name:	AIPathConstruction.h
 *   $Revision: 11 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 8.0 Path Construction Suite.
 *
 * Copyright (c) 1986-1998 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIPath__
#include "AIPath.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIPathConstruction.h */


/*******************************************************************************
 **
 **	Types
 **
 **/

/** A series of line segments is described to the APIs of the AIPathConstructionSuite
	by its corresponding vertex points. A point may be marked as a corner point
	indicating that it was either generated from a corner point of a path or that
	it should result in a corner point when producing an interpolating path. */
typedef struct {
	AIRealPoint point;
	AIBoolean corner;
} AIPathConstructionPoint;


/** The AIPathConstructionSuite APIs require that the client supply the
	routines to use for allocating and deallocating memory. These routines
	are supplied via this structure. The routines should conform to the
	semantics of the C standard library malloc and free. Most clients
	will hook these up to the corresponding SPBlocksSuite APIs. */
typedef struct {
	void *(*allocate)( long size );
	void (*dispose)( void *pointer );
} AIPathConstructionMemoryObject;


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIPathConstructionSuite			"AI PathConstruction Suite"
#define kAIPathConstructionSuiteVersion2	AIAPI_VERSION(2)
#define kAIPathConstructionSuiteVersion		kAIPathConstructionSuiteVersion2
#define kAIPathConstructionVersion			kAIPathConstructionSuiteVersion

/*******************************************************************************
 **
 ** Suite
 **
 **/

/**
	The path construction suite contains utilities for working with paths.
	The common feature of these utilities is that they convert paths to
	and from line segments as a part of their operation.
 */
typedef struct {

	/** Given a sequence of path segments describing a single open path this API
		returns a sequence of points that describe a series of line segments
		approximating the path. maxPointDistance puts an upper bound on the distance
		between successive point samples. flatness puts an upper bound on the
		error between the line segments and the true curve. The corner member
		of the points corresponding to segments is set from the segment. For
		intermediate points it is not set therefore its value depends on the
		behaviour of the memory allocator. The client is responsible for
		supplying a memory object for memory allocations and deallocation. */
	AIAPI AIErr (*CurvesToPoints) ( long segmentCount, AIPathSegment *segments,
					  long *pointCount, AIPathConstructionPoint **points,
					  AIReal maxPointDistance, AIReal flatness,
					  AIPathConstructionMemoryObject *memory );


	/** Given a sequence of points describing a single open polygon this API
		returns a sequence of path segments describing an open path approximating
		the polygon. A tolerance of 1.0 will give the most exact fit but also
		the most segments. The algorithm used is best suited to generating
		curves from points that were produced sampling a path. It is not well
		suited for samples of hand drawn points. The client is responsible for
		supplying a memory object for memory allocations and deallocation. */
	AIAPI AIErr (*PointsToCurves) ( long *pointCount, AIPathConstructionPoint *points,
					  long *segmentCount, AIPathSegment **segments,
					  AIReal *tolerance,
					  AIReal *threshold,
					  short *cornerRadius,
					  AIReal *scale,
					  AIPathConstructionMemoryObject *memory );
	
	/** Modifies the segments of the path by first converting them to a series
		of points and then re-fitting curves to the points. In some cases
		this may reduce the total number of path segments, especially if
		many are redundant. Any corner segments are preserved. */
	AIAPI AIErr (*ReducePathSegments) ( AIArtHandle path, AIReal flatness, 
					  AIPathConstructionMemoryObject *memory );

} AIPathConstructionSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
