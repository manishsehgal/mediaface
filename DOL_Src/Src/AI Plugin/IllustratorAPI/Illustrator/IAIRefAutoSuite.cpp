/*
 *        Name:  IAIRefAutoSuite.cpp
 *   $Revision:  1 $
 *      Author:  dmaclach
 *        Date:  Thursday, March 25, 2004 11:26:12
 *     Purpose:  
 *
 *   Copyright (c) 1986-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

/** @file IAIRefAutoSuite.cpp*/
#define AICOUNTEDOBJECTSUITE_DEFINED 1
#include "IAIRef.h"
#include "AutoSuite.h"

use_suite(AICountedObject)

/**
	This implements the GetAICountedObjectSuitePtr for autosuited AICountedObjectSuites.
*/

AICountedObjectSuite *ai::GetAICountedObjectSuitePtr()
{
	return const_cast<AICountedObjectSuite *>(sAICountedObject.operator->());
}
