/*
 *        Name:	IAIUnicodeString.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Interface to the wrapper class for AIUnicodeStringSuite & the 
 *				primary interface for the AI core to
 *				the ai::UnicodeString objects.
 *
 * Copyright (c) 2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


#ifndef _IAIUNICODESTRING_H_
#define _IAIUNICODESTRING_H_

#pragma once

#include "AITypes.h"
#include "AICharacterEncoding.h"

#include <string>

#ifdef MAC_ENV
#include "CFString.h"
#endif // MAC_ENV


/** @file IAIUnicodeString.h */

class CAIUnicodeStringImpl;

/** @ingroup Errors */
/** kUnicodeStringBadIndex indicates an out-of-range index was used. */
#define kUnicodeStringBadIndex				'US!I'
/** kUnicodeStringLengthError indicates an attempt to create string longer than maximum allowed length */
#define kUnicodeStringLengthError			'US#L'
/** kUnicodeStringMalformedError indicates that a string is malformed */
#define kUnicodeStringMalformedError		'US!F'


namespace ai {

/**
	Allocator object class for the AutoBuffer template.
*/
class SPAlloc {
public:
	static void* AllocateBlock(size_t byteCount);
	static void DeleteBlock(void* block);
};

/** Const Pascal string adapter object. */
class const_PStr
{
public:
	explicit const_PStr(const unsigned char* pascalString) : fConstStr(pascalString) {};
	const_PStr(const const_PStr& p) : fConstStr(p.fConstStr) {};
	const unsigned char* get() const 
	{ return fConstStr; } 
	const unsigned char& operator[] (unsigned i) const
	{ return fConstStr[i]; }
protected:
	union {
		const unsigned char* fConstStr;
		unsigned char* fStr;
	};

};

/** Mutable Pascal string adapter object. */
class PStr : public const_PStr
{
public:
	explicit PStr(unsigned char* pascalString) : const_PStr(pascalString) {};
	PStr(const PStr& p) : const_PStr(p) {};
	unsigned char* get() const 
	{ return fStr; }
	unsigned char& operator[] (unsigned i) const
	{ return fStr[i]; }
	operator const const_PStr& ()
	{ return *this; }
};


/**
	AutoBuffer is a buffer class object used to facilitate clients retrieving string contents 
	into a buffer though it can be used to pass/retrieve any array of POD (plain old data) types.  
	Its use relieves the client of managing the buffer's memory.
	
	Important Note: elem's type must be limited to POD (plain old data) types for AutoBuffer objects
	that are to be passed across the Illustrator SuitePea suite boundary.  This is required as the 
	ctor and dtor for non-POD types are not available on both sides of this boundary.  For single-side
	of the API boundary usage, clients are free to construct AutoBuffers of any type.

	std::vector is not used because AI has no control over the default allocator for the vector and
	the implementations of std::vector could be different on each side of the API boundary causing problems.
*/
template<class elem, typename size_type=unsigned long, class A=SPAlloc> class AutoBuffer {
public:
	/** Constructor.
		@param count initial count of elem that the buffer should be able to hold
	*/
	explicit AutoBuffer (size_type count = 0)
	: fCapacity(count),
	  fBuffer(0)
	{
		if ( fCapacity > 0 )
		{
			fBuffer = reinterpret_cast<elem*>(A::AllocateBlock(ByteCount(fCapacity)));
			Init();
		}
	}
	/** Copy constructor.
		@param b buffer to be copied.
	*/
	explicit AutoBuffer (const AutoBuffer& b)
	: fCapacity(b.fCount),
	  fBuffer(0)
	{
		if ( fCapacity > 0 )
		{
			fBuffer = reinterpret_cast<elem*>(A::AllocateBlock(ByteCount(fCapacity)));
			Copy(b.fBuffer);
		}
	}
	/**
		Destructor
	*/
	~AutoBuffer ()
	{
		if ( fBuffer )
		{
			Destroy(fBuffer);
			A::DeleteBlock(fBuffer);
		}
	}

	/**	Does the buffer object have a handle to a valid buffer.
		@return true is returned if the buffer pointer is valid.
	*/
	bool IsValid () const
	{
		return fBuffer != 0;
	}

	/** Returns a writable pointer to the buffer.  The returned pointer
		may be invalidated by calls to Resize().
		@return a pointer to the buffer.
	*/	
	elem* GetBuffer () const
	{
		return fBuffer;
	}
	operator elem* () const
	{
		return GetBuffer();
	}
	/** Returns a writable reference to the element at position n in the buffer.
		Note there is no protection for clients indexing off the end of the array.
		@param n the index of the element desired.
		@return a writable reference to the object at the requested index.
	*/
	elem& operator[] (unsigned long n)
	{
		return fBuffer[n];
	}
	/** Return the current capacity of the buffer.
		@return the capacity
	*/
	size_type GetCount () const
	{
		return fCapacity;
	}
	/** Resize the buffer to hold newSize elements.
		If newSize is less than the current capacity, the buffer is shortened and
		truncated elements are destroyed.
		If newSize is greater than the current capacity, the buffer is grown and the new elements
		are initialized.
		@param newSize desired size of the buffer.
	*/
	void Resize (size_type newSize)
	{
		if ( newSize != fCapacity )
		{
			elem *newBuffer = reinterpret_cast<elem*>(A::AllocateBlock(ByteCount(newSize)));
			if ( fBuffer )
			{
				elem* oldBuffer = fBuffer;
				fBuffer = newBuffer;
				Copy(oldBuffer);

				Destroy(oldBuffer);
				A::DeleteBlock(oldBuffer);
			}
			else
			{
				fBuffer = newBuffer;
				Init();
			}
			fCapacity = newSize;
		}
	}

	AutoBuffer& operator= (const AutoBuffer& rhs)
	{
		if ( this != &rhs )
		{
			if ( fCapacity != rhs.fCapacity )
			{
				Destroy(fBuffer);
				A::DeleteBlock(fBuffer);
				fBuffer = reinterpret_cast<elem*>(A::AllocateBlock(ByteCount(rhs.fCapacity)));
			}
			Copy(rhs.fBuffer);
		}
		return *this;
	}

	static size_type lastIndex ()
	{ return -1; }
private:
	void Init (size_type start = 0, size_type end = lastIndex())
	{
		size_type elemCount = (end == lastIndex() ? GetCount() : end);
		for (size_type i = start; i < elemCount; ++i)
		{
			new (&fBuffer[i])elem;
		}
	}
	void Destroy (elem* e, size_type start = 0, size_type end = lastIndex())
	{
		size_type elemCount = (end == lastIndex() ? GetCount() : end);
		for (size_type i = start; i < elemCount; ++i)
		{
			e[i].~elem();
		}
	}
	void Copy (const elem* e, size_type start = 0, size_type end = lastIndex())
	{
		size_type elemCount = (end == lastIndex() ? GetCount() : end);
		for (size_type i = start; i < elemCount; ++i)
		{
			fBuffer[i] = e[i];
		}
	}
	size_type ByteCount (size_type elemCount)
	{
		return elemCount * sizeof(elem);
	}
private:
	size_type fCapacity;
	elem* fBuffer;
};


/** UnicodeString object for AI.
	
	UnicodeString objects provide basic string functionality on Unicode
	based strings.  This interface attempts to mimick the familiar std::basic_string
	interface.

	The client interface to this string is strictly UTF code point based.  That is, the client
	interaction is based on treating the contents of the string as if all characters are represented
	as full 32-bit Unicode characters.  This allows the client to be oblivious to the internal represenation of
	the string and alleviates the need for clients to deal with surrogate pairs during any editing or searching
	operations.  Clients may interact with UnicodeString objects on a full character basis.  The only exception to 
	this is if the client uses construction methods that deal with UTF-16 filled buffers.  It is 
	the client's responsibility to validate that UTF-16 filled buffers used to construct a UnicodeString are valid.
	The extraction methods are guaranteed to always return full characters to UTF-16 buffers.

*/
class UnicodeString {
public:
	typedef size_t size_type;
	static const size_type npos;
	typedef ASUInt32 UTF32TextChar;
	typedef ASUnicode UTF16Char;

public:
	//----------------------------------------------------------------------
	/** @name Constructors & Destructors */
	//----------------------------------------------------------------------
	//@{
	/** Empty string constructor.  Creates a valid, empty string.  This method is guaranteed 
		to not throw any exceptions. */
	explicit UnicodeString (void) AINOTHROW;
	/** Construct a UnicodeString from an encoded byte array.
		@param string array of bytes to construct from.
		@param srcByteLen the length of the array.
		@param encoding the encoding of the contents of the byte array. Default 
		is the current platform encoding*/
	explicit UnicodeString (const char* string, long srcByteLen, 
		AICharacterEncoding encoding = kAIPlatformCharacterEncoding);
	/** Construct a UnicodeString using a single UTF32 code point.  The string is filled with @a count 
		copies of @a ch.
		@param count number of characters to insert in this string.
		@param ch the UTF code point to use to initialize the string. */
	explicit UnicodeString (size_type count, UTF32TextChar ch);
	/** Convenience constructor from a char buffer that is known to be 0 terminated.
		@param string initial contents for this UnicodeString.
		@param encoding the encoding of the contents of the byte array. */
	explicit UnicodeString (const char* string, AICharacterEncoding encoding = kAIPlatformCharacterEncoding);
	/** Convenience constructor from a std::string.
		@param string std::string that supplies contents for this UnicodeString.
		@param encoding is the encoding of the contents of string. */
	explicit UnicodeString (const std::string& string, AICharacterEncoding encoding = kAIPlatformCharacterEncoding);
	/** Constructor from 0 terminated, platform byte ordered, UTF-16 array.  Surrogate pairs are okay. 
		Exception raised if string is malformed.
		@param string point to a 0 terminated array of ASUnicode. */
	explicit UnicodeString (const ASUnicode* string);
	/** Constructor from the ZString referred to by a ZString key. 
		@param zStringKey is the ZString key to be looked-up. */
	explicit UnicodeString (const ZRef zStringKey);
	/** Constructor from a non 0 terminated platform byte ordered, UTF-16 array.  Surrogate pairs are okay. 
		Exception raised if string is malformed.
		@param string point to an array of ASUnicode.
		@param srcUTF16Count the number of UTF-16 code units to be read from string.*/
	explicit UnicodeString (const ASUnicode* string, size_type srcUTF16Count);
	/** Constructor from a std::basic_string of ASUnicode characters.  ASUnicode characters must be 
		platform byte ordered.  Surrogate pairs are okay. 
		Exception raised if string is malformed.
		@param string std::basic_string<ASUnicode> to be internalized.	*/
	explicit UnicodeString (const std::basic_string<ASUnicode>& string);
	/** Copy Constructor: create a copy of string s 
		@param s the string to copy.
	*/
	UnicodeString (const UnicodeString& s);
	/** Destructor */
	~UnicodeString (void);
	//@}

	//----------------------------------------------------------------------
	/** @name Factory methods */
	//----------------------------------------------------------------------
	//@{
	/** Convenience factory method that constructs a UnicodeString from a
	    non-zero terminated char array whose encoding is ISO Roman/Latin.
	    @param string input char array.
	    @param count the number of bytes (chars) in string.
	    @return a UnicodeString representation of string */
	static UnicodeString FromRoman (const char* string, size_type count);

	/** Convenience factory method that constructs a UnicodeString from a
	    zero terminated char array whose encoding is ISO Roman/Latin.
	    @param string input char array.
	    @return a UnicodeString representation of string */
	static UnicodeString FromRoman (const char* string);

	/** Convenience factory method that constructs a UnicodeString from a
	    std::string whose encoding is ISO Roman/Latin.
	    @param string input string.
	    @return a UnicodeString representation of string */
	static UnicodeString FromRoman (const std::string& string);

	/** Convenience factory method that constructs a UnicodeString from a
	    Pascal string whose encoding is ISO Roman/Latin.
	    @param input pascal string.
	    @return a UnicodeString representation of string */
	static UnicodeString FromRoman (const const_PStr& pascalString);

	/** Convenience factory method that constructs a UnicodeString from a
	    non-zero terminated char array whose encoding is the current platform encoding.
	    @param string input char array.
	    @param count the number of bytes (chars) in string.
	    @return a UnicodeString representation of string */
	static UnicodeString FromPlatform (const char* string, size_type count);

	/** Convenience factory method that constructs a UnicodeString from a
	    zero terminated char array whose encoding is the current platform encoding.
	    @param string input char array.
	    @return a UnicodeString representation of string */
	static UnicodeString FromPlatform (const char* string);

	/** Convenience factory method that constructs a UnicodeString from a
	    std::string whose encoding is the current platform encoding.
	    @param string input string.
	    @return a UnicodeString representation of string */
	static UnicodeString FromPlatform (const std::string& string);

	/** Convenience factory method that constructs a UnicodeString from a
	    Pascal string whose encoding is the current platform encoding.
	    @param input pascal string.
	    @return a UnicodeString representation of string */
	static UnicodeString FromPlatform (const const_PStr&  pascalString);

	/** Convenience factory method that constructs a UnicodeString from a
	    zero terminated char array whose encoding is UTF8.
	    @param string input char array.
	    @return a UnicodeString representation of string */
	static UnicodeString FromUTF8 (const char* string);

	/** Convenience factory method that constructs a UnicodeString from a
	    std::string whose encoding is UTF8.
	    @param string input string.
	    @return a UnicodeString representation of string */
	static UnicodeString FromUTF8 (const std::string& string);

	/** Convenience factory method that constructs a UnicodeString from a
	    Pascal string whose encoding is UTF8.
	    @param input pascal string.
	    @return a UnicodeString representation of string */
	static UnicodeString FromUTF8 (const const_PStr&  pascalString);

	//@}

	/* Basic operations (a la std::basic_string) */

	/** Append str to this string.
		@param str string to append.
		@return a reference to this string.
	*/
	UnicodeString& append (const UnicodeString& str);
	/** Append a sub-string of str to this string starting at startOffset.  
		At most count characters will be appended.
		@param str string to append.
		@param startOffset the index of the first character in str to append.
		@param count the number of characters from str to append.
		@return a reference to this string.
	*/
	UnicodeString& append (const UnicodeString& str, size_type startOffset, 
		size_type count);
	/** Append a string of count characters ch to this string.  
		@param count the number of characters to append.
		@param ch the character to append.
		@return a reference to this string.
	*/
	UnicodeString& append (size_type count, UTF32TextChar ch)
	{ return append(UnicodeString(count, ch)); }


	/** Replace the contents of this string with str.
		@param str string to assign to this string.
		@return a reference to this string.
	*/
	UnicodeString& assign (const UnicodeString& str);
	/** Assign a substring of str to this.
		@param str the string to assign from.
		@param offset start offset in str.
		@param count the maximum number of characters to assign.
		@return a reference to this string.
	*/
	UnicodeString& assign (const UnicodeString& str, size_type offset, 
		size_type count)
	{ return assign(str.substr(offset, count)); }
	/** Returns the UTF32 code point at the position offset. kUnicodeStringBadIndex exception 
		is raised if offset is out of range.
		@param offset the index of the character to retrieve.
		@return the UTF32 value of the character at offset.
	*/
	UTF32TextChar at (size_type offset) const;
	/** Synonym for erase(0, npos).
	    Uninitialized strings remain unchanged.
		@see erase()
	*/
	void clear ();
	/** Code point based comparison of strings.
		Uninitialized strings are equal to other uninitialized strings and 
		empty strings.
		Uninitialized and empty strings are always less than initialized, 
		non-empty strings.
		@param str is the right hand side string.
		@return A negative value if the operand string is less than str;
		zero if the two strings are equal; 
		a positive value if the operand string is greater than str.
	*/ 
	int compare (const UnicodeString& str) const;
	/** Code point based comparison of this to a substring of str.
		Uninitialized strings are equal to other uninitialized strings and 
		empty strings.
		Uninitialized and empty strings are always less than initialized, 
		non-empty strings.
		@param pos the index of the first character from this string to compare.
		@param num the number of characters to compare.
		@param str is the right hand side string.
		@return A negative value if the operand string is less than the substring of str;
		zero if the two strings are equal; 
		a positive value if the operand string is greater than the substring of str.
	*/ 
	int compare (size_type pos, size_type num, const UnicodeString& str) const;
	/** Code point based comparison of a substring of this string to a substring of the 
		parameter string str.
		Uninitialized strings are equal to other uninitialized strings and 
		empty strings.
		Uninitialized and empty strings are always less than initialized, 
		non-empty strings.
		@param pos the index of the first character from this string to compare.
		@param num the number of characters to compare.
		@param str is the right hand side string.
		@param startOffset the index of the first character from @str to compare.
		@param count the number of characters from @str to compare.
		@return A negative value if the operand string is less than the substring of str;
		zero if the two strings are equal; 
		a positive value if the operand string is greater than the substring of str.
	*/ 
	int compare (size_type pos, size_type num, const UnicodeString& str, 
		size_type startOffset, size_type count) const;

	/** Return the number of characters (UTF code points) in this string. 
		@return the number of UTF code points in this string. */
	size_type length () const;
	/** Determine if this string is an empty string.
		@return true if the string is empty. */
	bool empty () const;
	/** Remove characters (UTF code points) from this string.
		@param pos the offset of the first character to remove.
		@param count the number of characters to remove.
		@return a reference to the resulting UnicodeString object. */
	UnicodeString&  erase (size_type pos=0, size_type count = npos);

	/** search for the character ch starting at startOffset.  
		returns the position of the found char.  
	    returns npos if character is not found */
	size_type find (UTF32TextChar ch, size_type startOffset = 0 ) const;
	/** search for the string target starting at startOffset.  
		returns the position of the found char.  
	    returns npos if character is not found */
	size_type find (const UnicodeString& target, size_type startOffset = 0) const;
	/** search for a substring of target.  search starts at startOffset 
  		of "this".  only the first count characters of target
	    are searched for. */
	size_type find (const UnicodeString& target, size_type startOffset, size_type count) const;

	/** Search string target starting at startOffset.  
		The position of the string found is returned.  npos is returned if the target is not found.
		@param target the string to search for.
		@param startOffset offset to start the search at.
		@return the index of the first character of target in this string. */
	size_type caseFind (UTF32TextChar ch, size_type startOffset = 0 ) const
	{ return caseFind(ai::UnicodeString(1, ch), startOffset, 1); }
	size_type caseFind (const UnicodeString& target, size_type startOffset = 0) const
	{ return caseFind(target, startOffset, target.length()); }
	size_type caseFind (const UnicodeString& target, size_type startOffset, size_type count) const;

	/** 
		Search for the character ch starting at startOffset moving backward through this string.
		@param ch character to search for.
		@param startOffset character offset in this string to begin the search.
		@return position of the found character. npos is returned if character is not found 
	*/
	size_type rfind (UTF32TextChar ch, size_type startOffset = npos ) const;
	/** 
		Search for the string target starting at startOffset moving backward through this string.  
		@param target the string to search for.
		@param startOffset character offset in this string to begin the search.
		@return position of the found string. npos is returned if the string is not found.
	*/
	size_type rfind (const UnicodeString& target, size_type startOffset = npos) const;
	/** 
		Search for a substring of target.  The search begins at startOffset 
  		of moving backward through this string.  Only the first count characters of the 
		target are searched for.
		@param target the string to search for.
		@param startOffset character offset in this string to begin the search.
		@param count the number of characters from target to look for.
		@return position of the found string. npos is returned if the string is not found.
		*/
	size_type rfind (const UnicodeString& target, size_type startOffset, size_type count) const;

	size_type find_first_of (const UnicodeString& target, size_type startOffset = 0) const
	{ return find_first_of(target, startOffset, npos); }
	size_type find_first_of (const UnicodeString& target, size_type startOffset, size_type count) const;
	size_type find_last_of (const UnicodeString& target, size_type startOffset = npos) const
	{ return find_last_of(target, startOffset, npos); }
	size_type find_last_of (const UnicodeString& target, size_type startOffset, size_type count) const;
	size_type find_first_not_of (const UnicodeString& target, size_type startOffset = 0) const
	{ return find_first_not_of(target, startOffset, npos); }
	size_type find_first_not_of (const UnicodeString& target, size_type startOffset, size_type count) const;
	size_type find_last_not_of (const UnicodeString& target, size_type startOffset = npos) const
	{ return find_last_not_of (target, startOffset, npos); }
	size_type find_last_not_of (const UnicodeString& target, size_type startOffset, size_type count) const;

	UnicodeString& replace (size_type pos, size_type num, const UnicodeString& str)
	{ return replace(pos, num, str, 0, npos); }
	UnicodeString& replace (size_type pos, size_type num, const UnicodeString& str, size_type count)
	{ return replace(pos, num, str, 0, count); }
	UnicodeString& replace (size_type pos, size_type num, const UnicodeString& str, 
		size_type startOffset, size_type count);

	/** insert string
		@param insertOffset the position behind the point of insertion of the new characters.
		@param str the string to be partially or wholly inserted.
	*/
	UnicodeString& insert (size_type insertOffset, const UnicodeString& str)
	{ return insert(insertOffset, str, 0, npos); }
	/** insert string
		@param insertOffset the position behind the point of insertion of the new characters.
		@param str the string to be partially or wholly inserted.
		@param offset index to the part of the source string to supply the inserted characters.
		@param count the number of characters to insert.
	*/
	UnicodeString& insert (size_type insertOffset, const UnicodeString& str,
		size_type offset, size_type count);

	/** insert characters into string
		@param insertOffset the position behind the point of insertion of the new characters.
		@param count the number of characters to insert.
		@param ch the character to insert.
	*/
	UnicodeString& insert (size_type insertOffset, size_type count, 
		UTF32TextChar ch)
	{ return insert(insertOffset, UnicodeString(count, ch), 0, count); }

	/** Adds the passed element to the end of the string
		@param ch the character to append.
	*/
	void push_back(UTF32TextChar ch)
	{ (void) append(1, ch); }

	/** Resize this string to hold count elements.  If the string is longer than count
		elements it is truncated.  If it is shorter than count elements it is 
		grown to hold count elements and new elements are initialized with ch.
		@param count the new size of this string in UTF32TextChar.
		@param ch the UTF code point value to initialize new elements with.
	*/
	void resize (size_type count, UTF32TextChar ch = UTF32TextChar());

	/** Synonym for length().
		@see length()
	*/
	ai::UnicodeString::size_type size (void) const
	{ return length(); }

	/** Create a copy of the substring starting with the character at offset containing
		at most count characters.
		@param offset is the position of the first character to be copied to the substring.
		@param count is the maximum number of characters to copy to the new substring.
		@return a UnicodeString containing the requested substring. */
	UnicodeString substr (size_type offset = 0, size_type count = npos) const;

	/** Swap the contents of this string with str 
		@param str the string to swap contents with.*/
	void swap (UnicodeString& str);

	/* Operators */

	/** Assignment operator. 
		@param rhs the UnicodeString object to assign into this one.
		@return a reference to this string. */
	UnicodeString& operator= (const UnicodeString& rhs);
	/** Append operator.
		@param ch the character to to append to this string.
		@return a reference to this string. */
	UnicodeString& operator += (UTF32TextChar ch)
	{ return append(1, ch); }
	/** Append operator.
		@param rhs the UnicodeString object to append to this one.
		@return a reference to this string. */
	UnicodeString& operator += (const UnicodeString& rhs)
	{ return append(rhs); }
	/** Return the character at the position offset.  NOTE: This method
		deviates from the std::basic_string operator[] behavior.  This
		method returns the character at offset.  std::basic_string::operator[]
		returns a writable reference to the elemement at the given offset.
		@param offset the position to retrieve a character from.
		@return the character from position offset. */
	UTF32TextChar operator[] (size_type offset) const;
	/** Equality operator.  Does a simple, direct, code point based
		comparison of rhs to this string.
		@param rhs the string to compare this string to.
		@return true if the strings are equal. */
	bool operator== (const UnicodeString& rhs) const
	{ return compare(rhs) == 0; }
	bool operator!= (const UnicodeString& rhs) const
	{ return !(operator==(rhs)); }
	bool operator< (const UnicodeString& rhs) const
	{ return compare(rhs) < 0; }

	/* non-std::basic_string based functionality */

	/** Caseless code point based comparison of strings.
		Uninitialized strings are equal to other uninitialized strings and 
		empty strings.
		Uninitialized and empty strings are always less than initialized, 
		non-empty strings.
		@param str is the right hand side string.
		@return A negative value if the operand string is less than str;
		zero if the two strings are equal; 
		a positive value if the operand string is greater than str.
	*/ 
	int caseCompare (const UnicodeString& str) const
	{ return caseCompare(0, npos, str, 0, npos); }

	/** Caseless code point based comparison of this to a substring of @a str.
		Uninitialized strings are equal to other uninitialized strings and 
		empty strings.
		Uninitialized and empty strings are always less than initialized, 
		non-empty strings.
		@param pos the index of the first character from this string to compare.
		@param num the number of characters to compare.
		@param str is the right hand side string.
		@return A negative value if the operand string is less than the substring of str;
		zero if the two strings are equal; 
		a positive value if the operand string is greater than the substring of str.
	*/ 
	int caseCompare (size_type pos, size_type num, const UnicodeString& str) const
	{ return caseCompare(pos, num, str, 0, npos); }

	/** Caseless code point based comparison of a substring of this string to a substring of the 
		parameter string @a str.
		Uninitialized strings are equal to other uninitialized strings and 
		empty strings.
		Uninitialized and empty strings are always less than initialized, 
		non-empty strings.
		@param pos the index of the first character from this string to compare.
		@param num the number of characters to compare.
		@param str is the right hand side string.
		@param startOffset the index of the first character from @str to compare.
		@param count the number of characters from @str to compare.
		@return A negative value if the operand string is less than the substring of str;
		zero if the two strings are equal; 
		a positive value if the operand string is greater than the substring of str.
	*/ 
	int caseCompare (size_type pos, size_type num, const UnicodeString& str, 
		size_type startOffset, size_type count) const;

	/** Canonical code point based comparison of strings.  
	    Compares the strings for canonical equivalence of their normalized forms
		(NFD or NFC).
		Uninitialized strings are equal to other uninitialized strings and 
		empty strings.
		Uninitialized and empty strings are always less than initialized, 
		non-empty strings.
		Note: canonical comparison may require temporary allocation of memory.  canonicalCompare
		may throw out of memory errors.
		Note: substring comparison of non-normalized strings is not directly available because
		it is problematic in that substrings may be equivelent between strings, but 
		their offset and/or lengths may be different.  Clients may create their
		own substrings and pass them to this function.
		@param str is the right hand side string.
		@return A negative value if the operand string is less than str;
		zero if the two strings are equal; 
		a positive value if the operand string is greater than str.
	*/ 
	int canonicalCompare (const UnicodeString& str) const;

	/** Canonical caseless code point based comparison of strings.  
	    Compares the strings for canonical equivalence of their normalized forms
		(NFD or NFC).
		Uninitialized strings are equal to other uninitialized strings and 
		empty strings.
		Uninitialized and empty strings are always less than initialized, 
		non-empty strings.
		Note: canonical comparison may require temporary allocation of memory.  canonicalCompare
		may throw out of memory errors.
		Note: substring comparison of non-normalized strings is not directly available because
		it is problematic in that substrings may be equivelent between strings, but 
		their offset and/or lengths may be different.  Clients may create their
		own substrings and pass them to this function.
		@param str is the right hand side string.
		@return A negative value if the operand string is less than str;
		zero if the two strings are equal; 
		a positive value if the operand string is greater than str.
	*/ 
	int canonicalCaseCompare (const UnicodeString& str) const;

	/** Check if this string contains surrogate pairs.
		@return true if there are surrogate pairs in this string. */
	bool hasSurrogates () const;

	/** Returns a const pointer to a buffer containing the UTF-16 code units
		for this string.  The buffer contents are in platform byte order.  
		Returned size_type is the size of the buffer in number of UTF-16 code units.  
		The buffer pointer returned may be 0 if the this string
		is empty. Note: The returned buffer is not guaranteed to be 0 terminated. Use as_ASUnicode
		if you want a 0 terminated buffer.

		This method is guaranteed to not allocate any memory, and it will return in constant time.
		@param buffer is a reference to a const pointer to UTF16Char that will be filled in with a pointer 
		to a buffer containing the contents of this string as UTF-16 code units. This buffer pointer
		is only valid at most for the lifetime of this string.
		@return a size_type indicating the number of UTF16Chars in the returned buffer. */
	size_type utf_16 (const UTF16Char*& buffer) const;

	/**
		Get the contents of this string in std::basic_string<ASUnicode> as
		a sequence of UTF-16 code units.
		@return the contents of this string as UTF-16 code units. */
	std::basic_string<ASUnicode> as_ASUnicode ( ) const;

	/** Convenience method for retrieving UTF-8 encoded version of string.
		@return the contents of this string as UTF-8 encoded std::string. */
	std::string as_UTF8 ( ) const;
	/** Convenience method for retrieving platform encoded version of string.
		@return the contents of this string as UTF-8 encoded std::string. */
	std::string as_Platform () const;
	/** Convenience method for retrieving ISO Latin/Roman encoded version of string.
		@return the contents of this string as UTF-8 encoded std::string. */
	std::string as_Roman () const;
	/**	Return the contents of the string in a client provided buffer.  This method
		follows the strlcpy paradigm.  The buffer returned will always be 0 terminated.
		bufferMax should be large enough for the string contents plus a 0 terminator.
		@param buffer a pointer to a byte array that the contents are to be written to.
		@param bufferMax the maximum byte count to write to buffer including 0 terminator.
		@param encoding the encoding desired for the buffer contents.
		@return the size necessary to write the entire string contents to a buffer including a 0 terminator. */
	size_type getToBuffer ( char* buffer, size_type bufferMax, AICharacterEncoding encoding ) const;
	/** Pascal string version of getToBuffer(). */
	size_type getToBuffer ( const PStr&  pascalString, size_type bufferMax, AICharacterEncoding encoding ) const;

	/** Convenience form of getToBuffer() */
	size_type as_ASUnicode (ASUnicode* buffer, size_type bufferMax ) const;
	/** Convenience form of getToBuffer() */
	size_type as_Platform ( const PStr&  pascalString, size_type bufferMax ) const;
	/** Convenience form of getToBuffer() */
	size_type as_Platform ( char* buffer, size_type bufferMax ) const;
	/** Convenience form of getToBuffer() */
	size_type as_Roman ( char* buffer, size_type bufferMax ) const;
	/** Convenience form of getToBuffer() */
	size_type as_Roman ( const PStr&  pascalString, size_type bufferMax ) const;

	/** 
		Get the contents of this string in a std::string in the requested encoding.
		This method may throw an out of memory error.
		@param encoding the desired encoding.
		@return a std::string with the contents of string in requested encoding. */
	std::string getInStdString (AICharacterEncoding encoding) const;

	/**
		Retrieve the contents of this string in an ai::AutoBuffer<char> in the
		requested encoding.
		This method may throw an out of memory error.
		@param encoding the desired encoding.
		@param b the buffer to place the contents of this string into in the
		requested encoding.
		@return the number of characters (bytes) used in the buffer b.  Note: 
		the contents of b may NOT be 0 terminated.
	*/
	size_type getAs (AICharacterEncoding encoding, ai::AutoBuffer<char>& b) const;

#ifdef MAC_ENV
	//----------------------------------------------------------------------
	/** @name Macintosh Specific Methods */
	//----------------------------------------------------------------------
	//@{
	/** Construct a UnicodeString from a Macintosh CFString.
	    @param cfString the CFString to construct this string from.
	*/
	explicit UnicodeString (const CFStringRef& cfString);
	/** Convenience method for retrieving the contents of this string as a Macintosh CFString.
	    The client is responsible for providing a valid CFAllocatorRef and is responsible for
	    proper disposal of the returned CFString.
	    @param alloc a valid CFAllocatorRef for allocating the CFString.
		@return An immutable CFString object containing chars or NULL if there was a problem
		creating the object. The client is responsible for releasing this object. */
	CFStringRef as_CFString (CFAllocatorRef alloc) const;
	//@}
#endif // MAC_ENV


public:	// internal use public interface
	void deleteImpl();

protected:
	explicit UnicodeString(class CAIUnicodeStringImpl* impl);

private:
	CAIUnicodeStringImpl* fImpl;
};

//
// Inline implementations  - Yes, some of these could cause code bloat.  Later these can be moved to the impl file.
//	It is easier now to implement them here.
//
inline UnicodeString& UnicodeString::insert (size_type insertOffset, const UnicodeString& str,
		size_type offset, size_type count)
{
	if ( insertOffset > length() || offset > str.length() )
		throw ai::Error(kUnicodeStringBadIndex);
	UnicodeString result = substr(0, insertOffset);
	result.append(str, offset, count);
	result.append(substr(insertOffset));
	
	*this = result;
	return *this;
}

inline UnicodeString& UnicodeString::replace (size_type pos, size_type num, const UnicodeString& str, 
						size_type startOffset, size_type count)
{
	if ( pos > length() || startOffset > str.length() )
		throw ai::Error(kUnicodeStringBadIndex);
	erase(pos, num);
	insert(pos, str, startOffset, count);

	return *this;
}

inline std::string UnicodeString::as_UTF8 ( ) const
{
	return getInStdString(kAIUTF8CharacterEncoding);
}

inline std::string UnicodeString::as_Platform () const
{
	return getInStdString(kAIPlatformCharacterEncoding);
}

inline std::string UnicodeString::as_Roman () const
{
	return getInStdString(kAIRomanCharacterEncoding);
}

inline UnicodeString::size_type UnicodeString::as_ASUnicode (ASUnicode* buffer, size_type bufferMax ) const
{
	const UTF16Char* bufPtr = 0;
	const size_type kThisUTF16Len = utf_16(bufPtr) + 1;
	const size_type kCopyMax = (bufferMax < kThisUTF16Len ? bufferMax : kThisUTF16Len) - 1;
	memcpy(buffer, bufPtr, kCopyMax*sizeof(UTF16Char));
	buffer[kCopyMax] = 0;

	return kThisUTF16Len;
}

inline UnicodeString::size_type UnicodeString::as_Platform ( char* buffer, size_type bufferMax ) const
{
	return getToBuffer( buffer, bufferMax, kAIPlatformCharacterEncoding);
}

inline UnicodeString::size_type UnicodeString::as_Roman ( char* buffer, size_type bufferMax ) const
{
	return getToBuffer( buffer, bufferMax, kAIRomanCharacterEncoding);
}

inline UnicodeString::size_type UnicodeString::as_Platform ( const ai::PStr&  pascalString, size_type bufferMax ) const
{
	return getToBuffer( pascalString, bufferMax, kAIPlatformCharacterEncoding );
}

inline UnicodeString::size_type UnicodeString::as_Roman ( const ai::PStr&  pascalString, size_type bufferMax ) const
{
	return getToBuffer( pascalString, bufferMax, kAIRomanCharacterEncoding );
}


} // end of namespace ai

#endif	// _IAIUNICODESTRING_H_
