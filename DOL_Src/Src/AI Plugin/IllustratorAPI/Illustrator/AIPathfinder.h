#ifndef __AIPathfinder__
#define __AIPathfinder__

/*
 *        Name:	AIPathfinder.h
 *   $Revision: 7 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Pathfinder Suite.
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AIFilter__
#include "AIFilter.h"
#endif

#ifndef __AIMenu__
#include "AIMenu.h"
#endif

#ifndef __AIPlugin__
#include "AIPlugin.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/** @file AIPathfinder.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIPathfinderSuite			"AI Pathfinder Suite"
#define kAIPathfinderSuiteVersion4	AIAPI_VERSION(4)
#define kAIPathfinderSuiteVersion	kAIPathfinderSuiteVersion4
#define kAIPathfinderVersion		kAIPathfinderSuiteVersion

/** The default value for AIOptions::ipmPrecision */
#define kDefaultPrecision				10.0
/** The default value for AIOptions::removeRedundantPoints */
#define kRemoveRedundantPointsDefault	0
/** The default value for the AIOptions::flags #kExtractUnpaintedArtwork flag */
#define kExtractUnpaintedArtworkDefault	0
/** The default value for the AIOptions::flags #kAllowEmptyOutput flag */
#define kAllowEmptyOutputDefault		0
/** The default value for the AIOptions::flags #kIgnoreEvenOddFillRule flag */
#define kIgnoreEvenOddFillRuleDefault	0

/** The default value for AIMixParameters::softRate */
#define kSoftRateDefault 				0.5
/** The default value for AIMixParameters::convertCustomColors */
#define kMixConvertCustomColorsDefault 	1

// Trap parameters
#define kTrapThicknessDefault			0.25
#define kHeightWidthAspectRatioDefault 	1.0
#define kTrapTintDefault 				0.4
#define kMaximumTintDefault 			1.0
#define kTintToleranceDefault 			0.05
#define kReverseTrapDefault				0
#define kTrapConvertCustomColorsDefault	0


/** The number of points in a micron. */
#define pointsPerMicron (72.0 / 25400.0)

#define kNoResultResID		16004

/** Flag values for AIOptions::flags */
enum AIOptionsFlagValues {
	kExtractUnpaintedArtwork	= 0x00001,
	kAllowEmptyOutput			= 0x10000,
	kIgnoreEvenOddFillRule		= 0x20000
};

/*******************************************************************************
 **
 ** Types
 **
 **/

typedef void *AIPathfinderParameters;
typedef double			dbl;

typedef struct {
	/** Ipm precision, in microns. The number of microns in a point is given by
		#pointsPerMicron. Default value #kDefaultPrecision */
	double				ipmPrecision;
	/** Nonzero iff removing redundant points. Default value #kRemoveRedundantPointsDefault */
	long				removeRedundantPoints;
	/** kExtractUnpaintedArtwork if removing unpainted artwork;
		kAllowEmptyOutput if Minus Front, Minus Back, Intersect and Exclude can
		produce empty output. See #AIOptionsFlagValues. */
	long				flags;
} AIOptions;

typedef struct {
	/** Default value #kSoftRateDefault */
	double				softRate;
	/** Nonzero iff converting custom colors. Default value #kMixConvertCustomColorsDefault */
	long				convertCustomColors;
} AIMixParameters;

typedef struct {
	/** Trap thickness, in points. Default value #kTrapThicknessDefault */
	double				trapThickness;
	/** Height to width aspect ratio. Default value #kHeightWidthAspectRatioDefault */
	double				heightWidthAspectRatio;
	/** Default value #kTrapTintDefault */
	double				trapTint;
	/** Default value #kMaximumTintDefault */
	double				maximumTint;
	/** Default value #kTintToleranceDefault */
	double				tintTolerance;
	/** Nonzero iff reversing trap. Default value #kReverseTrapDefault */
	long				reverseTrap;
	/** Nonzero iff converting custom colors. Default value #kTrapConvertCustomColorsDefault */
	long				convertCustomColors;
} AITrapParameters;

typedef struct {
	AIMixParameters		mixParameters;
	AITrapParameters		trapParameters;
} AIParameters;

typedef struct {
	AIOptions				options;
	AIParameters			parameters;
	AIArtHandle					*fSelectedArt;
	long						fSelectedArtCount;
	long						fAlertInfoID;
} AIPathfinderData;


/** The contents of a Pathfinder message. */
typedef struct {
	SPMessageData				d;
	AIPathfinderData			pathfinderData;
} AIPathfinderMessage;

/** New for AI 10.0: compound shape modes */
typedef enum AIShapeMode
{
	kAIShapeModeAdd = 0,
	kAIShapeModeSubtract,
	kAIShapeModeIntersect,
	kAIShapeModeExclude
} AIShapeMode;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** The pathfinder suite provides APIs for performing pathfinder operations on
	artwork. In addition it provides APIs for working with compound shapes. */
typedef struct {

	AIAPI AIErr (*DoUniteEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoIntersectEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoExcludeEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoBackMinusFrontEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoFrontMinusBackEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoDivideEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoOutlineEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoTrimEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoMergeEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoCropEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoHardEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoSoftEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoTrapEffect) ( AIPathfinderData *data, AIFilterMessage *message );

	AIAPI AIErr (*GetHardEffectParameters) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*GetSoftEffectParameters) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*GetTrapEffectParameters) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*GetGlobalOptions) ( AIPathfinderData *data, AIFilterMessage *message );

	// New for AI 10.0: compound shapes
	AIAPI AIShapeMode (*GetShapeMode) ( AIArtHandle art );
	AIAPI AIErr (*SetShapeMode) ( AIArtHandle art, AIShapeMode mode );
	AIAPI AIErr (*NewCompoundShape) ( short paintOrder, AIArtHandle prep, AIArtHandle *newArt );
	AIAPI AIErr (*IsCompoundShape) ( AIArtHandle art, AIBoolean *isCompoundShape );
	AIAPI AIErr (*FlattenArt) ( AIArtHandle *inOutArt );	// Expands the given art into a single path or compound path.

} AIPathfinderSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
