/* -------------------------------------------------------------------------------

                                    ADOBE CONFIDENTIAL
                                _________________________

    Copyright 2000-2002 Adobe Systems Incorporated
    All Rights Reserved.

    NOTICE:  All information contained herein is, and remains
     the property of Adobe Systems Incorporated and its suppliers,
     if any.  The intellectual and technical concepts  contained
     herein are proprietary to Adobe Systems Incorporated and its
     suppliers and may be covered by U.S. and Foreign Patents,
     patents in process, and are protected by trade secret or copyright law.
     Dissemination of this information or reproduction of this material
     is strictly forbidden unless prior written permission is obtained
     from Adobe Systems Incorporated.

 ----------------------------------------------------------------------------------

	File:	Exception.h
		
	Notes:	define ATE exception class.
	
 ---------------------------------------------------------------------------------- */
#ifndef __Exception__
#define __Exception__
#include <exception>
#include "ATETypes.h" //for error values.

namespace ATE
{

class Exception : public std::exception
{
public:
	Exception(ATEErr errorCode)
	:error(errorCode)
	{
	}
// error code
	ATEErr error;
};


}// namespace ATE

#endif //__Exception__
