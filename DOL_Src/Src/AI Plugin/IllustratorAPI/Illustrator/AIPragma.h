/***********************************************************************/
/*                                                                     */
/* AIPragma.h                                                          */
/* Makes #defines for #pragmas to eliminate compiler dependencies      */
/*                                                                     */
/* Copyright 2001 Adobe Systems Incorporated.                     		*/
/* All Rights Reserved.                                                */
/*                                                                     */
/* Patents Pending                                                     */
/*                                                                     */
/* NOTICE: All information contained herein is the property of Adobe   */
/* Systems Incorporated. Many of the intellectual and technical        */
/* concepts contained herein are proprietary to Adobe, are protected   */
/* as trade secrets, and are made available only to Adobe licensees    */
/* for their internal use. Any reproduction or dissemination of this   */
/* software is strictly forbidden unless prior written permission is   */
/* obtained from Adobe.                                                */
/*                                                                     */
/* Started by Dave MacLachlan, 09/27/2001                               */
/*                                                                     */
/***********************************************************************/

#ifndef __AIPragma__
#define __AIPragma__

#ifndef __ASConfig__
#include "ASConfig.h"
#endif


#ifdef MACPPC_ENV
#define	AIPRAGMA_ALIGN_BEGIN		options align=power_gcc
#define AIPRAGMA_ALIGN_END			options align=reset
#define AIPRAGMA_ALIGN_MAC68K		options align=mac68k
#define AIPRAGMA_ALIGN_MAC68K_END	options align=reset
#define AIPRAGMA_ENUM_BEGIN			enumsalwaysint on
#define AIPRAGMA_ENUM_END			enumsalwaysint reset
#endif

#ifdef WIN_ENV
#define	AIPRAGMA_ALIGN_BEGIN		pack(push, 4)			
#define AIPRAGMA_ALIGN_END			pack(pop)		
#define AIPRAGMA_ENUM_BEGIN			
#define AIPRAGMA_ENUM_END			
#endif


#endif
