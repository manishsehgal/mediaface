#ifndef __AIPlaced__
#define __AIPlaced__

/*
 *        Name:	AIPlaced.h
 *   $Revision: 25 $
 *      Author:
 *        Date:
 *     Purpose:	Adobe Illustrator Placed Object Suite.
 *
 * Copyright (c) 1986-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef _AIRaster_
#include "AIRaster.h"
#endif

#include "IAIFilePath.hpp"

extern "C" {

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIPlaced.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIPlacedSuite			"AI Placed Suite"
#define kAIPlacedSuiteVersion6	AIAPI_VERSION(6)
#define kAIPlacedSuiteVersion	kAIPlacedSuiteVersion6
#define kAIPlacedVersion		kAIPlacedSuiteVersion

/** Types of placed object. */
enum AIPlacedObjectType {
	kEPSType = 0,
	kOtherType
};

/** Methods for positioning a linked object */
enum PlaceMethod {
	/** Preserve original dimensions regardless of bounding box size and transformations. */
	kAsIs,
	/** Fill bounding box while preserving proportions. May overlap edges in one dimension. */
	kFill,
	/** Fit fully inside bounding box while preserving proportions. */
	kFit,
	/** Fit to bounding box. Replaced file will preserve bounds but not proportions. */
	kConform,
	/** Fit to bounding box. Replaced file will preserve transformations and attempt to preserve size.
		This was the only method previous to Illustrator 10 and is still the default. */
	kReconform
};

/** Alignment for positioning a linked object. */
enum PlaceAlignment {
	kTopLeft,
	kMidLeft,
	kBotLeft,

	kTopMid,
	kMidMid,
	kBotMid,

	kTopRight,
	kMidRight,
	kBotRight
};

/** Specifies the request type to AIPlacedSuite::ExecPlaceRequest() */
typedef enum PlaceRequestMode {
	/** Place the file. */
	kVanillaPlace = 0,
	/** Consult selection for place/replace. */
	kQueryReplace,
	/** Replace the file. */
	kForceReplace,
	/** Replace placed art by its parsed contents. */
	kUnlinkPlaced,
	/** Create new art in current layer's top group. */
	kCreateNewArt,
	/** Same as kForceReplace except that this validates SPPlatformFileSpecification. */
	kForceReplaceEx,
	/** Replace placed art by its parsed contents, and ask for param. */
	kQueryUnlinkPlaced
} ePlaceRequestMode;


/*******************************************************************************
 **
 ** Types
 **
 **/

/** On passing AIPlaceRequestData to ExecPlaceRequest:

	The value of m_lPlaceMode, taken from the #PlaceRequestMode enum,
	determines how the place request is processed.

	If a #kVanillaPlace request is made, then the new art,
	if successfully read, is placed, and a handle to the
	new art is in the m_hNewArt field of the AIPlaceRequestData
	structure.

	If #kQueryReplace is specified, then the current selection
	is consulted to determine whether to place or replace.  If
	there is exactly one placed object selected, then the checkbox
	on the "File.Place" dialog (which will be invoked) is used to
	determine whether to just place the new art or use it to replace
	replace the old art (obtained from the selection).  If there is
	no selection, or if more than one placed object is selected, then
	the new art is just placed.

	If #kForceReplace is requested, then (a handle to) the
	art to be replaced must be in the m_hOldArt field of the
	AIPlaceRequestData structure.  A handle to the new art,
	if the read is successful, can be obtained from the
	m_hNewArt field of the AIPlaceRequestData structure.

	If an #kUnlinkPlaced request is made, and is successful, then
	look for the group art handle containing the contents
	in m_hNewArt.

	If the #kCreateNewArt option is specified, a new art
	object will be created in the current layer inside the
	top level group art of the layer.  You specify the art
	type you want created in m_lParam.  Look for your new
	art in m_hNewArt.  Such creation is subject to editability
	and visibility of said layer.

	If m_pFilePath is NULL, a "File.Place" dialog is used
	to get the file and any relevant place options.

	NOTE:  Be sure to set m_pSPFSSpec to either NULL or
	       to a valid file spec.

	       In fact, it is a good idea to initialize all
	       fields before using this structure ... even
	       if you think that a field may be irrelevant.

	If filemethod == 0, it is place/import;
	if filemethod == 1, it is link.
*/
struct AIPlaceRequestData
{
	AIPlaceRequestData(): m_pFilePath(0) {}
	
	/** Determines type of place request */
	ASInt32 m_lPlaceMode;
	/** Art to be replaced */
	AIArtHandle m_hOldArt;
	/** Art just read from file */
	AIArtHandle m_hNewArt;
	/** File path of the placed file. */
	const ai::FilePath *m_pFilePath;
	/** Depends on place mode */
	ASInt32 m_lParam;
	/** Determines how the file is read */
	short m_filemethod;
	/** Disables the Template placing option */
	ASBoolean m_disableTemplate;
};

/*******************************************************************************
 **
 **	Suite
 **
 **/


/**
	Placed art is Adobe Illustrator's means of linking artwork contained in
	external files. For a file to be linked there must be a file format plugin
	installed that supports linking that format. For all file types except
	EPS a placed object is defined by its file specification, a matrix positioning
	the file content in the document and a group of art objects that represents
	the parsed contents of the file. EPS differs in that it does not have a
	group of art objects representing the parsed contents. When a file format is
	requested to link a file other than an EPS it does the following:

	- Creates a placed object for the linked file setting its file specification
		to reference the file.
	- Calls SetPlacedObject() to get a group that will contain the artwork
		representing the content of the linked file.
	- Reads the content of the linked file creating the artwork to represent
		the content.
	- Sets the matrix of the placed object as needed to position the content
		in the document.

	A plugin can determine whether a linked file is an EPS or not by using the
	GetPlacedType() API.

	Illustrator 10 added placement options to the properties of a placed object.
	These options affect how a linked object is positioned when it is used to
	replace another object and how a linked object is scaled. In the first case
	a new object is being placed into the bounds of an original object. In the
	second case you can think of the bounds of the original object as being
	scaled and then the object being placed into those bounds. The placement
	options provide control over how the object is modified to fit those bounds.
 */
typedef struct AIPlacedSuite {

	/** Returns the file specification identifying the linked file. If the linked file is
		missing then the file specification only supplies the file name. */
	AIAPI AIErr (*GetPlacedFileSpecification) ( AIArtHandle placed, ai::FilePath &file );
	/** Set the file specification for the placed object to identify the linked file. If
		the file specification identifies an EPS then its contents are immediately parsed.
		If it identifies any other file type parsing of the file contents is deferred until
		specifically requested by AIArtSuite::UpdateArtworkLink() or AIDocumentSuite::UpdateLinks(). */
	AIAPI AIErr (*SetPlacedFileSpecification) ( AIArtHandle placed, const ai::FilePath &file );

	/** Get the matrix that positions the content of the linked file in the document. */
	AIAPI AIErr (*GetPlacedMatrix) ( AIArtHandle placed, AIRealMatrix *matrix );
	/** Set the matrix that positions the content of the linked file in the document. */
	AIAPI AIErr (*SetPlacedMatrix) ( AIArtHandle placed, AIRealMatrix *matrix );

	/** Get the bounding box of the bounding box of the content of the linked file. */
	AIAPI AIErr (*GetPlacedBoundingBox) ( AIArtHandle placed, AIRealRect *bbox );

	/** File format plugins call this API when reading the content of a linked file. The API
		returns a group to hold the parsed content of the file. Any previous contents are
		discarded. */
	AIAPI AIErr (*SetPlacedObject) ( AIArtHandle placed, AIArtHandle *group );

	/** Returns #kBadParameterErr if the linked file is not an EPS otherwise returns a count
		of the number of spot colors used by the EPS. */
	AIAPI AIErr (*CountPlacedCustomColors) ( AIArtHandle art, long *count );
	/** Returns #kBadParameterErr if the linked file is not an EPS otherwise returns the name
		of the Nth spot color used by the EPS. */
	AIAPI AIErr (*GetNthPlacedCustomColorName) ( AIArtHandle art, long num, ai::UnicodeString& name );

	/** Embeds the placed object into the document returning the embedded object. If
		askForParam is true then the embedding process is allowed to present dialogs
		asking for user input if needed. If false it must not request user input. */
	AIAPI AIErr (*MakePlacedObjectNative) ( AIArtHandle placed, AIArtHandle *native, ASBoolean askForParam );

	/** Get the type of the placed object. See #AIPlacedObjectType. */
	AIAPI AIErr (*GetPlacedType) ( AIArtHandle placed, short *pPlacedType );
	/** Returns #kBadParameterErr for a linked EPS otherwise returns the group of artwork
		representing the parsed content of the linked file. */
	AIAPI AIErr (*GetPlacedChild) ( AIArtHandle placed, AIArtHandle *group );

	/** Executes a place request. See AIPlaceRequestData for details. */
	AIAPI AIErr (*ExecPlaceRequest)( AIPlaceRequestData &placeRequestData );

	/** The SPPlatformFileInfo contains information about the linked file such as the time
		it was created and last modified. The placed object stores the information from
		the time the file was last read. This API returns the file information stored with
		the placed object. It can be compared against the current information for the file
		itself to determine if the file has changed. */
	AIAPI AIErr (*GetPlacedFileInfoFromArt)( AIArtHandle placed, SPPlatformFileInfo *spFileInfo );
	/** The SPPlatformFileInfo contains information about the linked file such as the time
		it was created and last modified. This API returns the file information of the linked
		file itself. It can be compared against the information stored with the placed object
		to determine if the file has changed. */
	AIAPI AIErr (*GetPlacedFileInfoFromFile)( AIArtHandle placed, SPPlatformFileInfo *spFileInfo );
	/** Get the path specification for the placed object. A platform specific path is
		returned. */
	AIAPI AIErr (*GetPlacedFilePathFromArt)( AIArtHandle placed, ai::UnicodeString &path );

	/** Post-concatenate a transformation onto the matrix applied to the placed object. */
	AIAPI AIErr (*ConcatPlacedMatrix) ( AIArtHandle placed, AIRealMatrix *concat );

	// New for Illustrator 10

	/** Specify the placement options for the object. These options are used when the object
		is scaled or replaced by a new object. */
	AIAPI AIErr (*SetPlaceOptions) ( AIArtHandle placed, enum PlaceMethod method, enum PlaceAlignment alignment, ASBoolean clip );
	/** Get the placement options for the object. These options are used when the object
		is scaled or replaced by a new object. */
	AIAPI AIErr (*GetPlaceOptions) ( AIArtHandle placed, enum PlaceMethod *method, enum PlaceAlignment *alignment, ASBoolean *clip );
	/** */
	AIAPI AIErr (*GetPlacedDimensions) ( AIArtHandle placed, ASRealPoint *size, ASRealRect *viewBounds, AIRealMatrix *viewMatrix,
			ASRealRect *imageBounds, AIRealMatrix *imageMatrix );

	// New for Illustrator 12

	/** Set the bounding box information for the placed object. This is useful
		for a plugin that wants to create an empty placed object and then call
		AIArtSuite::UpdateArtworkLink() in order to cause the content of the
		referenced file to be read. If the file is read succesfully then the
		bounds supplied by this method will be replaced by the true bounds from
		the file. But, if the read fails the bounds will not be changed and,
		if this API were not used to otherwise set them, the result would be a
		placed object without width or height. */
	AIAPI AIErr (*SetPlacedBoundingBox) ( AIArtHandle art, AIRealRect* bbox );

	/** If the placed object represents a placed, linked image then this API
		returns a raster record describing the image. If it does not represent
		an image then the israster boolean will be returned as false. */
	AIAPI AIErr (*GetRasterInfo) ( AIArtHandle art, AIRasterRecord* info, AIBoolean* israster );

} AIPlacedSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

}

#endif
