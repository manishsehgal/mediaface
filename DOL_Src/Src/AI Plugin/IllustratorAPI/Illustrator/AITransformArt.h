#ifndef __AITransformArt__
#define __AITransformArt__

/*
 *        Name:	AITransformArt.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 8.0 Transform Art Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AIPlugin__
#include "AIPlugin.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AITransformArt.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAITransformArtSuite			"AI Transform Art Suite"
#define kAITransformArtSuiteVersion1	AIAPI_VERSION(1)
#define kAITransformArtSuiteVersion		kAITransformArtSuiteVersion1
#define kAITransformArtVersion			kAITransformArtSuiteVersion


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** Options that control the behaviour of AITransformArtSuite::TransformArt() */
enum AITransformArtOptions {
	/** Transform object geometry */
	kTransformObjects			= ((long) (1<<0)),
	/** Apply the transform to gradient fills */
	kTransformFillGradients		= ((long) (1<<1)),
	/** Apply the transform to pattern fills */
	kTransformFillPatterns		= ((long) (1<<2)),
	/** Apply the transform to pattern strokes */
	kTransformStrokePatterns	= ((long) (1<<3)),
	/** Scale the stroke weights by the linescale factor */
	kScaleLines					= ((long) (1<<4)),
	/** Transforms any opacity masks attached to the objects if they are set
		as linked. */
	kTransformLinkedMasks		= ((long) (1<<5)),
	/** Causes the transformation to be applied recursively. */
	kTransformChildren			= ((long) (1<<6)),
	/** Applies only to path objects. If set then only selected path segments are
		transformed. */
	kTransformSelectionOnly		= ((long) (1<<7)),
	/** Notify parent plugin groups of the transformation. Should avoid calling with
		this flag if calling TransformArt() during a mouse drag loop, as it incurs some
		performance cost. */
	kTransformNotifyPluginGroups = ((long) (1<<8))
};

/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	The Transform Art suite exports a single function which transforms an arbitrary
	art object by a given transformation matrix. This function operates on
	all art types and therefore frees the developer from having to write special
	transformation routines for the different art type.

	Path art objects in Illustrator do not have transformation matrices associated
	with them. Although you may be inclined to do so, you will not be able to
	simply modify a path object's current transformation matrix in order to
	transform it. Path art objects are simply collections of connected line
	segments.

	Some other art object types do have transformation matrices associated with them.
	Examples are placed and raster objects. With these art types, there are specific
	accessor functions to get and set each object�s current transformation matrix.
 */
typedef struct {

	/** This function transforms an arbitrary art object by a given transformation
		matrix. If the object has a stroke, the linescale parameter is where you
		specify the scale factor to be applied to the stroke. The appropriate linescale
		can be easily calculated as a function of the horizontal and vertical scale
		factors of the matrix: linescale = sqrt(sx) * sqrt(sy). The flags specify
		options for the behaviour of the transformation as defined by
		#AITransformArtOptions. */
	AIAPI AIErr (*TransformArt) ( AIArtHandle art, AIRealMatrix *matrix, AIReal lineScale, long flags );
	
} AITransformArtSuite;

#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
