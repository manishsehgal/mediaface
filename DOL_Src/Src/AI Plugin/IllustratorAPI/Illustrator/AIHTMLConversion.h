#ifndef __AIHTMLCONVERSION__
#define __AIHTMLCONVERSION__

/*
 *        Name:	AIHTMLConversion.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10 HTML Conversion Suite.
 *
 * Copyright 2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIDataFilter__
#include "AIDataFilter.h"
#endif

#ifndef __SPSuites__
#include "SPSuites.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIHTMLConversion.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIHTMLConversionSuite						"AI HTML Art Conversion Suite"
#define kAIHTMLConversionSuiteVersion				1001
#define kAIHTMLConversionFirstInternalSuiteVersion	1

#define kAIHTMLConversionSuitesSuite				"AI HTML Art Conversion Suites Suites"
#define kAIHTMLConversionSuitesSuiteVersion			1001



/*******************************************************************************
 **
 **	Definitions
 **
 **/

/** @ingroup Errors */
#define kAIHTMLUnsupportedTypeError			'HT!T'
/** @ingroup Errors */
#define kAIHTMLHBufferOverflowError			'HTBO'

enum AIHTMLConversionOptions {
	kAIHTMLNoOptions					= 0L,

	/** The first bit is for all convertable types.  If there are other options, they
		are defined by the converter (which would also define a UI/rules for setting
		them).
		
		Set the dynamic link between the slice and its AI art. */
	kAIHTMLTextLinkTextOption			= (1L<<0)
};

/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	This suite is not provided by the host application, but 
	a default converter for text is part of the slicing plug-in.
	If provided this suite should convert the art in the specified  
	AIArtHandle object to an HTML text flow, mapping styles as it
	sees fit.
 
	The AISlicing suite, for instance, will use this to get the 
	"best possible" HTML respresentation of a "sliced" text object.  
	This, in turn, allows Save for Web to export Illustrator text as
	html text.
*/
typedef struct 
{
	/** Returns whether the art can be converted as specifed by the options. See
		#AIHTMLConversionOptions for option values. */
	AIAPI AIBoolean (*ArtCanBeConverted)(AIArtHandle art, short artType, long options);

	/** For the given text art object, convert the AITextRuns into appropriate HTML
		expressions in the stream. Return error codes are:

		- #kAIHTMLTextUnsupportedType, the art cannot be converted to HTML, there is no translator
		- #kAIHTMLTextBufferOverflow, there was more text than the buffer could hold
	*/
	AIAPI AIErr (*GetArtAsHTMLText)(AIArtHandle art, long options, AIDataFilter *stream);

	/** Return the provider of the suite. Ideally this isn't needed because the suite
		always does the 'right' thing, but... */
	AIAPI AIErr (*GetConverterInfo)(char **converterName, void *otherInfo);

} AIHTMLConversionSuite;


/** This suite enables multiple HTML conversion suites to be registered. */
typedef struct 
{
	/** Call this to get the internal version you should use when adding a suite */
	SPAPI SPErr (*GetNextInternalVersion)(long apiVersion, long *internalVersion);

	/** With the internal version obtained using the function above, use this to add
		the suite. */
	SPAPI SPErr (*AddHTMLConversionSuite)(SPPluginRef host, 
				long apiVersion, long internalVersion, 
				const void *suiteProcs, SPSuiteRef *suite);

	/** The HTMLConversion suites list can be used with other SPSuitesSuite
		functions */
	SPAPI SPErr (*GetHTMLConversionSuiteList)(SPSuiteListRef *suiteList);

} AIHTMLConversionSuitesSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
