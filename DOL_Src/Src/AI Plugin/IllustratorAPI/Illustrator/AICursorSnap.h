#ifndef __AICursorSnap__
#define __AICursorSnap__

/*
 *        Name:	AICursorSnap.h
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 8.0 Cursor Snapping Suite.
 *
 * Copyright (c) 1986-1998 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif
#ifndef __AIDocumentView__
#include "AIDocumentView.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AICursorSnap.h */

/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAICursorSnapSuite				"AI Cursor Snap Suite"
#define kAICursorSnapSuiteVersion		AIAPI_VERSION(1)
#define kAICursorSnapVersion			kAICursorSnapSuiteVersion


/**
	Kinds of custom constraints
 */
enum {
	kPointConstraint = 1,						///< a single point
	kLinearConstraintAbs,						///< a line whose angle is relative to the page coordinates
	kLinearConstraintRel						///< a line whose angle is relative to the constraint angle
};


/**
	Flags for custom constraints
 */
#define kShiftConstraint			(1<<0L)		///< snap to constraint when shift key is down


/**
	Boundary editors for the transformation tools.
 */
enum {
	kTranslateBoundaryEditor = 1
};


/*******************************************************************************
 **
 ** Types
 **
 **/

/**
	Structure describing a custom constraint.
 */
typedef struct {
	/** Type of constraint: point or line */
	long				kind;
	/** Flags for the constraint */
	long				flags;
	/** Origin point for the constraint */
	AIRealPoint			point;
	/** For a line constraint the angle of the line */
	AIFloat				angle;
	/** Label for the constraint presented to the user */
	char*				label;
} AICursorConstraint;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	The cursor snap suite provides facilities for snapping the cursor. Tools that
	want to utilize the smart guides snapping facilities should use these APIs in
	their cursor tracking code as follows:

	First call the smart guides APIs to snap the cursor
 
	- view = the view we're working on
	- event = the event identifying the cursor location
	- control = a string that controls snapping (see below)
	- p = the cursor position (usually from AIToolMessage member "cursor")

@code
	result = sAICursorSnap->Track(view, p, &event, control, &p);
	if (result)
		throw ai::Error(result);
@endcode

	Second, if desired, find out what the snapped location hit

@code
	AIHitRef hit;
	long option = whatever options we want
	result = sAIHitTest->HitTest(NULL, &p, option, &hit);
	if (result)
		throw ai::Error(result);
@endcode

	The control string specifies how smart guides should do its snapping. It
	consists of a sequence of upper case characters, lower case characters and
	spaces. Each character in the string is a "command" to the snapping engine.
	They are processed sequentially from left to right.

	Upper case characters enable constraints. Constraints are things that can
	be snapped to. Examples are art objects, the grid, page boundaries and the
	automatically generated guide lines.

	A lower case character is an instruction to go look for the best snap location
	of a particular type amongst the currently enabled constraints. The types of
	snap locations that can be looked for including vertices, intersections and
	edges.

	A space character is an instruction to return the best snap location found
	so far. If no snap location has been found then the process keeps going.

	Here are the possible characters and their definitions:

	- A, enable snapping to art objects
	- T, enable snapping to custom constraints defined by the current tool
	- F, enable snapping to automatically generated guidelines
	- G, enable snapping to the grid (actually never used because turning on the grid turns off smart guides)
	- P, enable snapping to the page boundaries

	- i, look for intersections of two or more constraints
	- o, try to snap onto a constraint line
	- v, try to snap to a point (vertex)
	- f, look for filled objects
 
	Tools can define custom constraints through the SetCustom() API. Most tools
	do not. Any custom constraints are cleared when the user selects a new tool
	(if memory serves).

	Here's a list of the control strings used by the various tools built
	into Illustrator. The control strings are actually saved into the preferences
	file. The first entry in each line is the key used in preferences. The second
	is the control string for when the mouse is up and the third is the control
	string for when the mouse is down.
 
	- "Blend", "A v", "A v"
	- "Select", "A v o f", "ATFGP v i o f"
	- "RangeSelect", "", ""
	- "Zoom", "", ""
	- "Page", "ATFGP v i o", "ATFGP v i o"
	- "Ruler", "ATFGP v i o", "ATFGP v i o"
	- "Guide", "ATFGP v i o", "ATFGP v i o"
	- "Scissor", "ATFGP v i o", "ATFGP v i o"
	- "SquareCircle", "ATFGP v i o", "ATFGP v i o"
	- "Pen", "ATFGP v i o", "ATFGP v i o"
	- "Translate", "ATFGP v i o", "ATFGPB v i o"
	- "Scale", "ATFGP v i o", "ATFGPB v i o"
	- "Rotate", "ATFGP v i o", "ATFGP v i o"
	- "Reflect", "ATFGP v i o", "ATFGP v i o"
	- "Shear", "ATFGP v i o", "ATFGP v i o"
	- "Eyebucket", "A v o f", "A v o f"
*/

typedef struct {

	/**
		Returns true if smart guides should be employed when snapping the
		cursor in the document view.
	 */
	AIAPI AIBoolean		(*UseSmartGuides)		(AIDocumentViewHandle view);

	/**
		Resets the snapping engine. This clears out all custom constraints
		and all automatically generated constraint lines.
	 */
	AIAPI AIErr			(*Reset)				(void);

	/**
		Clear all custom constraints without clearing automatically
		generated constraint lines.
	 */
	AIAPI AIErr			(*ClearCustom)			(void);
	/**
		Replace the current set of custom constraints with the specified set.
		The first parameter is the number of constraints being defined. The
		second is an array of custom constraints. A copy of the constraints
		is made so the caller may free the area afterward.
	 */
	AIAPI AIErr			(*SetCustom)			(long count, const AICursorConstraint* constraints);

	/**
		Snap the cursor in the given document view. The input point is the
		actual cursor location in view coordinates. The event record is used
		to determine the state of modifier keys. The control string specifies
		how snapping is performed. On return dstpoint is filled in with the
		snapped cursor position.
	 */
	AIAPI AIErr			(*Track)				(AIDocumentViewHandle view, AIRealPoint srcpoint,
												const AIEvent* event, const char* control,
												AIRealPoint* dstpoint);

} AICursorSnapSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
