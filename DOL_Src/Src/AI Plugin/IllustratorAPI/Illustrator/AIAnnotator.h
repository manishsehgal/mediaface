#ifndef __AIAnnotator__
#define __AIAnnotator__

/*
 *        Name:	AIAnnotator.h
 *   $Revision: 8 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Annotator Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIDocumentView__
#include "AIDocumentView.h"
#endif

#ifndef __AIFixedMath__
#include "AIFixedMath.h"
#endif

#ifndef __AIRealMath__
#include "AIRealMath.h"
#endif

#ifndef __AIPlugin__
#include "AIPlugin.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIAnnotator.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIAnnotatorSuite			"AI Annotator Suite"
#define kAIAnnotatorSuiteVersion2	AIAPI_VERSION(2)
#define kAIAnnotatorSuiteVersion3	AIAPI_VERSION(3)
#define kAIAnnotatorSuiteVersion	kAIAnnotatorSuiteVersion3
#define kAIAnnotatorVersion			kAIAnnotatorSuiteVersion

/** @ingroup Callers
	This is the annotation caller */
#define kCallerAIAnnotation "AI Annotation"

/** @ingroup Selectors
	Sent when the plugin implementing a plugin annotator should draw its annotations. */
#define kSelectorAIDrawAnnotation		"AI Draw"
/** @ingroup Selectors
	Sent when the plugin implementing a plugin annotator should invalidate the regions
	occupied by its annotations. */
#define kSelectorAIInvalAnnotation		"AI Invalidate"


#define kAnnotateNothing			0
#define kAnnotateBetaObject			1
#define kAnnotateBetaObjects		2
#define kAnnotateBeziers			3
#define kAnnotateText				4
#define kAnnotateArtObject			5


#if defined(MAC_ENV) || defined(WIN_ENV)
#define kAnnotateNoPortType			0
#define kAnnotateWindowPortType		1
#define kAnnotateGWorldPortType		2
#endif


/*******************************************************************************
 **
 ** Types
 **
 **/

/** This is a reference to an annotator. It is never dereferenced. */
typedef struct _t_AIAnnotatorOpaque *AIAnnotatorHandle;

typedef struct {
	AIRealPoint p0;					/* Initial knot point */
	AIRealPoint p1;					/* First control point */
	AIRealPoint p2;					/* Second control point */
	AIRealPoint p3;					/* Final knot point */
	AIBoolean updateIt;
	AIBoolean drawIt;
} AIUpdateBezier;

/** A plugin that has registered an annotator will receive this message to request
	that it draw its annotations. */
typedef struct {
	SPMessageData d;
	AIAnnotatorHandle annotator;
	AIDocumentViewHandle view;
	long updateType;
	void *updateData;
	long portType;
	AIPortRef port;
	AIBoolean showEdges;

	/** New for AI 12.0
	
		These rectangles describe the regions that have been invalidated by an annotator
	    and need to be redrawn. The memory is owned and deleted by Illustrator after a
		draw message completes. When any annotator invalidates a region all the annotators
		are asked to draw themselves regardless of whether their annotations lie within 
		the invalidation rects. When a region is invalidated by an annotator Illustrator
		will erase annotations only from the invalidated region. This means that an 
		annotator may be asked to draw itself when the area it is drawing still contains
		the drawing	that was done during the previous draw message.
			Most annotations are light weight and can ignore invalidationRects and just
		draw the entire annotation on a draw #kSelectorAIDrawAnnotation message. If 
		there are performance issues drawing an annotator or an annotator uses 
		transparency/blending modes when drawing then the annotator should clip its drawing
		to the invalidation rectangles.

		Using Transparency with Annotators
			If the annotator is using transparency then the transparency will tend to 
		"build up" since drawing is being done in areas where the previous drawing was
		not cleared. If your annotator is drawing using transparency, clip the drawing to
		the invalidation rectangles to avoid this problem.
	*/
	AIRealRect *invalidationRects;
	int numInvalidationRects;

} AIAnnotatorMessage;


/*******************************************************************************
 **
 **	Suite Record
 **
 **/


/**
	The annotator suite allows plugins to draw annotations into the document window
	that are not a part of the artwork. These annotations are drawn on top after all
	artwork has been drawn. For example, selection handles are annotations.

	In order to be given an opportunity to draw annotations a plugin must first call
	"AddAnnotator" to register itself as an annotator. Typically this is done at
	plugin startup but it may be done at any time. An annotator may be active or
	inactive. Only active annotators receive requests to draw.

	A plugin annotator can receive two messages via its main entry point. These
	messages have caller kCallerAIAnnotation. They are described below:
	- kSelectorAIDrawAnnotation is a request for the annotator to draw its
		annotations. In this case the message structure is an AIAnnotatorMessage
		that supplies information about the document view to be drawn.
	- kSelectorAIInvalAnnotation is a request to invalidate the bounds of any
		annotations in the current document view. In response the plugin should
		call InvalAnnotationRect for each annotation.

	In addition to responding to the previous two messages a plugin annotator
	will typically call InvalAnnotationRect whenever its set of annotations
	changes. This indicates the parts of the document view that need to be
	redrawn as a consequence of the changes.
 */

typedef struct {

	AIAPI AIErr (*AddAnnotator) ( SPPluginRef self, char *name, AIAnnotatorHandle *notifier );

	AIAPI AIErr (*GetAnnotatorName) ( AIAnnotatorHandle notifier, char **name );
	AIAPI AIErr (*GetAnnotatorActive) ( AIAnnotatorHandle notifier, AIBoolean *active );
	AIAPI AIErr (*SetAnnotatorActive) ( AIAnnotatorHandle notifier, AIBoolean active );
	AIAPI AIErr (*GetAnnotatorPlugin) ( AIAnnotatorHandle notifier, SPPluginRef *plugin );

	AIAPI AIErr (*CountAnnotators) ( long *count );
	AIAPI AIErr (*GetNthAnnotator) ( long n, AIAnnotatorHandle *notifier );

	AIAPI AIErr (*InvalAnnotationRect)( AIDocumentViewHandle view, AIRect *annotationBounds );

	AIAPI AIErr (*SetAnnotatorAlternatePlugin) ( AIAnnotatorHandle notifier, SPPluginRef plugin );
	AIAPI AIErr (*GetAnnotatorAlternatePlugin) ( AIAnnotatorHandle notifier, SPPluginRef *plugin );

} AIAnnotatorSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
