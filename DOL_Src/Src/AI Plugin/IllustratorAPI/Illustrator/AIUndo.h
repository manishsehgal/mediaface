#ifndef __AIUndo__
#define __AIUndo__

/*
 *        Name:	AIUndo.h
 *   $Revision: 6 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Undo Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIUndo.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIUndoSuite			"AI Undo Suite"
#define kAIUndoSuiteVersion		AIAPI_VERSION(4)
#define kAIUndoVersion			kAIUndoSuiteVersion


/** Kinds of undo contexts. */
enum AIUndoContextKind {
	/** A standard context results in the addition of a new transaction which
		can be undone/redone by the user */
	kAIStandardUndoContext = 0,
	/** A silent context behaves in a manner similar to selection changes. that is
		it does not cause redos to be discarded and it is skipped over when
		undoing and redoing. */
	kAISilentUndoContext,
	/** An appended API context is similar to a standard context except that
		it is coallesced with the preceding transaction. the user does not
		see it as a separate transaction. it could be used, for example, to
		coallesce sequential changes to the color of an object into a
		single undo/redo transaction from the user's perspective. */
	kAIAppendUndoContext
};


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	The Undo Suite provides basic access to Illustrator's undo mechanism. It
	allows your plug-in to revert the artwork state to what it was when the
	plug-in was called. You can also set the text of the Undo/Redo menus.

	In order for a plug-in to access the Illustrator API an API context must be
	established. Typically this context is set up by Illustrator before giving
	control to the plug-in. Amongst other things the API context defines an
	undo context. The changes made by the plug-in are accumulated in the undo
	context. When the API context is terminated the undo context is inspected to
	see if any changes were made. If so, an entry is added to the list of
	operations that the user can undo and redo. When the user does an Undo from
	the Edit menu, the changes of the last complete context are undone. Redo
	restores the context changes.

	API contexts can be nested. This happens when an API context is established
	in order to communicate with plug-ins while some other plug-in is already
	in control. For example, if notifications are sent while a plug-in is
	executing nested contexts are created to send the notifications. Some
	properties of a nested context are local to that context while others are
	shared with the outer context. The undo context is shared with the outer
	context.

	Exactly when an API context is established and terminated depends on the
	flow of control that results in executing the plug-in. Typically each time
	Illustrator sends a plug-in a selector, it creates an API context.
	
	Plug-in tools handle API contexts a little differently. Instead of generating
	a context for each selector, Adobe Illustrator bundles the selectors:
	#kSelectorAIToolMouseDown, #kSelectorAIToolMouseDrag, and #kSelectorAIToolMouseUp
	into a single context. On each mouse movement the plug-in usually undoes the
	changes it made last time and then modifies the document in response to the
	current mouse position. Illustrator then redraws the document when the plug-in
	returns control.

	There are three types of undo context. These are defined by #AIUndoContextKind.
	Illustrator normally establishes a #kAIStandardUndoContext but notifications
	are sent in a #kAISilentUndoContext. A plug-in can change the undo context
	kind through the SetSilent() and SetKind() APIs. These are only effective
	if the plug-in is not operating in a nested context. This prevents a plug-in
	in a nested context from modifying the behaviour of the outer context.

	Undo entries can be tagged with a string and/or an integer. Only standard
	contexts can be tagged.

	A plug-in can undo the changes within the context Illustrator has made for it
	with the UndoChanges() function. It cannot undo changes outside of this
	context.
*/
typedef struct {

	/** This function set the text to use for the undo and redo menus. Illustrator will
		keep track of the strings and use them for the menus as needed. The strings passed
		to the routine are C strings. */
	AIAPI AIErr (*SetUndoText) ( const char *undoText, const char *redoText );
	/** Clears any changes made to the artwork since the undo context of the plugin
		was created. */
	AIAPI AIErr (*UndoChanges) ( void );

	AIAPI AIErr (*SetActionPaletteUndo) ( void );
	/** Deprecated. Remove all of the undoable changes currently accumulated. */
	AIAPI AIErr (*PurgeAllUndos) ( void );

	/** Mark the current API context as being silent for undo purposes. This is the
		same as calling SetKind(kAISilentUndoContext). */
	AIAPI AIErr (*SetSilent) ( AIBoolean silent );
	/** Set the kind of undo for the current API context. The kinds are defined by
		#AIUndoContextKind */
	AIAPI AIErr (*SetKind) ( long kind );
	/** Set a tag string and integer for the undo transaction which will be generated
		by the current API context. */
	AIAPI AIErr (*SetTag) ( char* tagString, long tagInteger );

	/** Get a count of the number of undos and the number of redos that a user may
		perform. This does not count silent or appended transactions. */
	AIAPI AIErr (*CountTransactions) ( long* past, long* future );
	/** Gets the tag string and integer of the nth undo/redo. -ve integers indicate
		undoable transactions and +ve integers indicate redoable transactions. No
		transaction has value 0. Note that this does not include silent or appended
		transactions. */
	AIAPI AIErr (*GetNthTransactionTag) ( long n, char** tagString, long* tagInteger );

	/** Get whether the current API context is marked as silent for undo purposes.
		If the current context is nested this API may return true while the outer
		context is not silent. */
	AIAPI AIErr (*IsSilent) ( AIBoolean* silent );
} AIUndoSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
