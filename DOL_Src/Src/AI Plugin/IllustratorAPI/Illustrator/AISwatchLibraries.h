#ifndef __AISwatchLibraries__
#define __AISwatchLibraries__

/*
 *        Name:	AISwatchLibraries.h
 *   $Revision: 2 $
 *      Author:	Neal Tucker
 *        Date:	3/11/97
 *     Purpose:	AI SwatchLibraries Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifndef __ASTypes__
#include "ASTypes.h"
#endif

#ifndef __AIColor__
#include "AIColor.h"
#endif

#ifndef __AISwatchList__
#include "AISwatchList.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AISwatchLibraries.h */

// AISwatchLibs Suite
#define kAISwatchLibrariesSuite			"AI Swatch Libraries Suite"
#define kAISwatchLibrariesSuiteVersion	AIAPI_VERSION(2)
#define kAISwatchLibrariesVersion		kAISwatchLibrariesSuiteVersion

/** @ingroup Callers */
/**	Caller id for messages sent to plugins from the Swatch Libraries plugin. 
*/
#define kAISwatchLibsCaller				"Swatch Libraries"

/** The swatch libraries suite is supplied by the swatch libraries plugin. This plugin
	allows the user to open libraries of color, pattern and gradient swatches. Once
	opened these libraries are accessible through this suite. The AISwatchListSuite
	provides APIs for manipulating a swatch list.

	The swatch libraries suite also contains APIs that allow the swatches in the open
	libraries to be accessed in aggregate. This APIs collect up all the swatches
	from all open libraries into one big list that is sorted by name and allow
	access to the entries in that list.
 */
typedef struct 
{	
	/** Return a count of the number of currently open libraries. */
	ASErr ASAPI (*CountLibraries)(long *num);
	/** Get the swatch list of the nth open library or if num < 0 the current document
		swatch list. The AISwatchListSuite can be used to access and modify the 
		returned list. */
	ASErr ASAPI (*GetNthLibrary)(long num, AISwatchListRef *swatchlist);
	/** Count the aggregate number of all swatches in all open libraries. */
	ASErr ASAPI (*CountLibrarySwatches)(long *num);
	/** Get the nth swatch in the aggregate of all open libraries. */
	ASErr ASAPI (*GetNthLibrarySwatch)(long num, AISwatchRef *swatch);
	/** Count the aggregate number of all pattern swatches in all open libraries. */
	ASErr ASAPI (*CountLibraryPatterns)(long *num);
	/** Get the nth pattern swatch in the aggregate of all open libraries. */
	ASErr ASAPI (*GetNthLibraryPattern)(long num, AIPatternHandle *pattern);
	/** Count the aggregate number of all gradient swatches in all open libraries. */
	ASErr ASAPI (*CountLibraryGradients)(long *num);
	/** Get the nth gradient swatch in the aggregate of all open libraries. */
	ASErr ASAPI (*GetNthLibraryGradient)(long num, AIGradientHandle *gradient);
	/** Count the aggregate number of all custom color swatches in all open libraries. */
	ASErr ASAPI (*CountLibraryCustomColors)(long *num);
	/** Get the nth custom color swatch in the aggregate of all open libraries. */
	ASErr ASAPI (*GetNthLibraryCustomColor)(long num, AICustomColorHandle *ccolor);

	/** The color books contain collections of pre-defined colors. Examples include
		the Pantone colors. FindStandardBookColor() looks in the color books to find a
		color with the specified name. If found it returns true and fills in the
		custom color definition. */
	ASBoolean ASAPI (*FindStandardBookColor)(const ai::UnicodeString& name, AICustomColor* color);
	/** The color books contain collections of pre-defined colors. The definitions
		in some cases have changed from previous versions (for example in AI12
		Pantone solids are Lab whereas they were formerly CMYK). FindLegacyBookColor()
		looks in the color books to find a color with the specified name. If a
		legacy definition is found it returns true and fills in the custom color
		definition with the legacy values. */
	ASBoolean ASAPI (*FindLegacyBookColor)(const ai::UnicodeString& name, AICustomColor* color);


	/** Get the name of the nth open library or if num < 0 the current document
		swatch list's name returns an empty string "".
	*/
	ASErr ASAPI (*GetNthLibraryName)(long num, ai::UnicodeString& name);

	/** The names used for the standard colors (such as Pantone) sometimes change
		between versions of Illustrator. This function tries to map such a name
		from a previous version to its name in the current version. If no mapping
		is defined the result is an empty string. */
	ASErr ASAPI (*MapLegacyColorName)(const ai::UnicodeString& src, ai::UnicodeString& dst);


} AISwatchLibrariesSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif