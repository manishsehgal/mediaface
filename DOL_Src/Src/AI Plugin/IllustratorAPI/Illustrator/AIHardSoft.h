#ifndef __AIHardSoft__
#define __AIHardSoft__

/*
 *        Name:	AIHardSoft.h
 *   $Revision: 3 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 8.0 Hard/Soft Suite.
 *
 * Copyright (c) 1986-1998 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIHardSoft.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIHardSoftSuite			"AI Hard Soft Suite"
#define kAIHardSoftSuiteVersion2	AIAPI_VERSION(2)
#define kAIHardSoftSuiteVersion		kAIHardSoftSuiteVersion2
#define kAIHardSoftVersion			kAIHardSoftSuiteVersion


/*******************************************************************************
 **
 **	Types
 **
 **/


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** 
	The geometry of an object is described by coordinates. Illustrator expresses the
	geometry of most objects using artboard coordinates. These are coordinates that
	are relative to a pair of imaginary axes imposed on the artboard. Obviously there
	are an infinite number of ways of placing these axes. Illustrator uses two of
	these: one for storing coordinate information internally and another for passing
	coordinate information across the plugin API functions.
	
	The plugin API places the origin of the coordinate system at the ruler origin.
	This is the point that corresponds to the 0 marks on the ruler when it is visible.
	By default it is the bottom left corner of the page but it may be changed by the
	user. The x axis is horizontal with the positive direction being to the right. The
	y axis is vertical with the positive direction pointing up. The plugin API
	coordinate system is sometimes called "soft" coordinates.

	The internal coordinate system places the origin at the top left corner of the
	artboard. This is the top left corner of the white area that can be seen when
	zoomed out as far as possible. The x axis is horizontal with the positive direction
	being to the right. The y axis is vertical with the positive direction pointing
	down. The internal coordinate system is sometimes called "hard" coordinates.
	
	When object geometry is not specified in artboard coordinates a mapping is needed
	from the coordinates used to the artboard. The mapping need not be direct. It may
	first map to some other coordinate system which can in turn be mapped to the
	artboard. Such a mapping is usually described by a matrix. The values specified
	by such a matrix will be different depending on whether the mapping is to the
	plugin API coordinate system or the internal coordinate system.

	Plugin authors should not usually need to worry about the fact that Illustrator
	makes use of two artboard coordinate systems. All API coordinates that are artboard
	coordinates should be expressed in the API coordinate system. All matrices passed
	across the API that map to or from artboard coordinates should map to or from API
	coordinates.
	
	Unfortunately this is not the case. There are APIs that use Illustrator's internal
	coordinate system. Where matrices are concerned there are also APIs that attempt
	to adjust the matrix to map to API coordinates but fail to do so properly. In these
	cases the plugin author needs to unravel the mess. The AIHardSoftSuite APIs
	provide some tools to help.
*/
typedef struct {		// AI 8.0
	
	/** Given a point in the plugin API artboard coordinate system converts it to a
		point in the internal artboard coordinate system. If the input point is (0, 0)
		this gives the vector from the origin of the internal coordinate system to
		that of the API. */
	AIAPI AIErr (*AIRealPointHarden) ( AIRealPoint* srcpoint, AIRealPoint* dstpoint );
	/** Given a point in the internal artboard coordinate system converts it to a
		point in the plugin API artboard coordinate system. If the input point is (0, 0)
		this gives the vector from the origin of the API coordinate system to that of
		the internal system. */
	AIAPI AIErr (*AIRealPointSoften) ( AIRealPoint* srcpoint, AIRealPoint* dstpoint );
	/** Purports to take a matrix that maps to plugin API artboard coordinates and
		convert it to one that maps to internal artboard coordinates. In fact it
		does not do so. It merely applies AIRealPointHarden() to the translation
		component of the matrix. This can be used to re-harden a matrix returned by
		an API that has been incorrectly softened. */
	AIAPI AIErr (*AIRealMatrixHarden) ( AIRealMatrix* matrix );
	/** Purports to take a matrix that maps to internal artboard coordinates and
		convert it to one that maps to plugin API artboard coordinates. In fact it
		does not do so. It merely applies AIRealPointSoften() to the translation
		component of the matrix. This can be used to pre-soften a matrix being passed
		into an API that will apply the incorrect hardening. */
	AIAPI AIErr (*AIRealMatrixSoften) ( AIRealMatrix* matrix );
	/** Another API that is not to be trusted. Softening and hardening matrices
		requires an understanding of the specific mappings involved. */
	AIAPI AIErr (*AIRealMatrixRealHard) ( AIRealMatrix* matrix );
	/** Another API that is not to be trusted. Softening and hardening matrices
		requires an understanding of the specific mappings involved. */
	AIAPI AIErr (*AIRealMatrixRealSoft) ( AIRealMatrix* matrix );

} AIHardSoftSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
