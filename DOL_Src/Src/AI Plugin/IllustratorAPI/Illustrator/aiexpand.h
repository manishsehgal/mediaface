/*
 *
 *	AIExpand.h
 *	Copyright (c) 1995 Adobe Systems Incorporated.
 *	
 */

#ifndef __ExpandSuite__
#define __ExpandSuite__

/** @file AIExpand.h */

/* Imports */

#include "AIArt.h"


/* Constants */

#define kAIExpandSuite			"AI Expand Suite"
#define kAI70ExpandSuite		kAIExpandSuite
#define kAIExpandSuiteVersion	AIAPI_VERSION(2)
#define kAI70ExpandSuiteVersion	AIAPI_VERSION(1)

/** Flag values for what should be expanded. */
enum AIExpandFlagValue {
	kExpandPluginArt		= 0x0001,
	kExpandText				= 0x0002,
	kExpandStroke			= 0x0004,
	kExpandPattern			= 0x0008,
	kExpandGradientToMesh	= 0x0010,
	kExpandGradientToPaths	= 0x0020,
	kExpandSymbolInstances	= 0x0040,

	kExpandOneAtATime		= 0x4000,
	kExpandShowProgress		= 0x8000,
	/** By default objects that are locked such as those on a locked layer
		cannot be expanded. Setting this flag allows them to be expanded. */
	kExpandLockedObjects	= 0x10000
};

/* Suite */


typedef struct {

	AIAPI AIErr (*Expand)( AIArtHandle art, long steps );

} AI70ExpandSuite;

/** The expand suite provides APIs for expanding an art object into a set of
	objects with equivalent appearance. */
typedef struct {

	/** This function breaks artwork up into individual parts and works just like
		calling "expand" from the Object menu in illustrator. Calling this function
		on path art with a fill of CMYK or RGB color has no effect. Calling this function
		on artwork filled with a gradient breaks the artwork up into different
		"steps". These steps relate to the gradients stops, i.e., an artwork with four
		stops and a setting of "2" in the steps field will produce an artwork that has
		two steps between each of the four stops. The resulting artwork will contain
		a mask. On text art it turns the text into outlines. On stroked objects it
		outlines the stroke and creates a closed path, this is especially useful for art
		objects created using brushes. For objects filled with a pattern it draws the
		entire pattern tile on the page with the orginal object acting as a mask. The
		following flags are available: see #AIExpandFlagValue. */
	AIAPI AIErr (*Expand)( AIArtHandle art, long flags, long steps );

} AIExpandSuite;


#endif // __ExpandSuite__

