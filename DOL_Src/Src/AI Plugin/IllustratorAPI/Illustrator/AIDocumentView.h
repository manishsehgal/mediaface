#ifndef __AIDocumentView__
#define __AIDocumentView__

/*
 *        Name:	AIDocumentView.h
 *   $Revision: 20 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Document View Suite.
 *
 * Copyright 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIRealMath__
#include "AIRealMath.h"
#endif

#ifndef __AIDocument__
#include "AIDocument.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIDocumentView.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIDocumentViewSuite			"AI Document View Suite"
#define kAIDocumentViewSuiteVersion8	AIAPI_VERSION(8)

// latest version
#define kAIDocumentViewSuiteVersion		kAIDocumentViewSuiteVersion8
#define kAIDocumentViewVersion			kAIDocumentViewSuiteVersion

/** @ingroup Notifiers */
#define kAIDocumentViewChangedNotifier				"AI Document View Changed Notifier"
/** @ingroup Notifiers
	see AIDocumentViewSuite::GetDocumentViewInvalidRect() */
#define kAIDocumentViewInvalidRectChangedNotifier	"AI Document View Invalid Rect Changed Notifier"
/** @ingroup Notifiers */
#define kAIDocumentViewStyleChangedNotifier			"AI Document View Style Changed Notifier"

/*******************************************************************************
 **
 **	Types
 **
 **/

/** Only applies to version 2
	AIDocumentPlatformViewBitmap is the same as a Macintosh GWorldPtr. */
typedef void *AIDocumentPlatformViewBitmap;

/** Only applies to version 3 and later */
typedef struct _t_AIDocumentViewOpaque *AIDocumentViewHandle;

/** Only applies to version 4 and later */
typedef enum {
	/** Only when there is no visibile document */
	kNoScreenMode,
	/** The normal display mode. Multiple windows are visible. */
	kMultiWindowMode,
	/** A single view takes up the whole screen but the menu is visible. */
	kFullScreenWithMenuMode,
	/** A single view takes up the whole screen, the menu is not visible. */
	kFullScreenNoMenuMode
} AIScreenMode;


/** Only applies to version 5 and later. The page tiling structure describes
	how the artwork will be printed on one or more pages. */
typedef struct {
	/** Horizontal and vertical number of page tiles on each art piece. A tile counts iff some
		of its imageable area intersects the art piece bounds. */
	AIPoint tilesPerArt;
	/** tilesPerArt.x * tilesPerArt.y */
	ASInt16 tilesPerArtPiece;
	/** The bounds of all the art pieces. This is just the bounds of the artboard whose
		dimensions are configured when creating a new document or through document setup.
		Page tiles may extend beyond these bounds, but they must intersect it. */
	AIRealRect bounds;
	/** Size of paper, in points */
	AIRealPoint paperSize;
	/** Top left of first piece of paper, in src space  */
	AIRealPoint paperOrigin;
	/** Size of imageable page area, in points */
	AIRealPoint imageSize;
	/** Top left of first imageable page area, in src space */
	AIRealPoint imageOrigin;
	/** Unused. Art size plus gap */
	AIRealPoint pieceDelta;
	/* Whether to show outlines of page tiles */
	ASBoolean showTiling;
} AIPageTiling;

// only applies to version 6 and later
// View styles

/** Outline mode. */
#define kVsArtwork			0x0001
/** Preview mode. */
#define kVsPreview			0x0002
/** Pixel preview mode. */
#define kVsRaster			0x0040
/** Unimplemented. Transparency attributes and masks are ignored. */
#define kVsOpaque			0x0080
/** OPP preview mode. */
#define kVsInk				0x0100

/*******************************************************************************
 **
 **	Suite
 **
 **/


/** Use the functions in this suite to obtain and set properties of a specified
	document view. In most cases, this will be the current document view. The
	functions access the bounds, center, zoom, and screen mode of Illustratorís
	document views.

	All functions take an AIDocumentViewHandle. In most cases, you will need
	the current view. In order to get the current view, you can usually pass
	NULL for the view. The view handle of the current view can be obtained by
	passing zero into the AIDocumentViewSuite::GetNthDocumentView() function.
*/
typedef struct {

	/** Returns the bounds of the document view in artwork coordinates. That is, the
		bounds of the artboard that are visible in the window. */
	AIAPI AIErr (*GetDocumentViewBounds) ( AIDocumentViewHandle view, AIRealRect *bounds );
	/** Returns the center of the document view in artwork coordinates. That is, the
		point of the artboard that maps to the center of the window. */
	AIAPI AIErr (*GetDocumentViewCenter) ( AIDocumentViewHandle view, AIRealPoint *center );
	/** Sets the point of the artboard that maps to the center of the window. */
	AIAPI AIErr (*SetDocumentViewCenter) ( AIDocumentViewHandle view, AIRealPoint *center );
	/** Gets the zoom factor for the view. This is the scale factor from artwork
		coordinates to window coordinates. */
	AIAPI AIErr (*GetDocumentViewZoom) ( AIDocumentViewHandle view, AIReal *zoom );
	/** Sets the scale factor from artwork coordinates to window coordinates. The scale
		factor is silently clamped to lie between the minimum and maximum values supported
		(currently between 1/32 and 64). After adjusting the zoom factor the document view
		center is unchanged. */
	AIAPI AIErr (*SetDocumentViewZoom) ( AIDocumentViewHandle view, AIReal zoom );

	/** Convert a point from artwork coordinates to view (window) coordinates. The resulting
		view coordinate is rounded to the nearest pixel. See AIDocumentViewSuite::FixedArtworkPointToViewPoint()
		for a version that does not round. */
	AIAPI AIErr (*ArtworkPointToViewPoint) ( AIDocumentViewHandle view, AIRealPoint *artworkPoint, AIPoint *viewPoint );
	/** Convert a point from view coordinates to artwork coordinates. This version takes pixel
		coordinates as an input. See AIDocumentViewSuite::FixedViewPointToArtworkPoint() for a
		version that takes floating point input. */
	AIAPI AIErr (*ViewPointToArtworkPoint) ( AIDocumentViewHandle view, AIPoint *viewPoint, AIRealPoint *artworkPoint );

	/** Count the number of open view for the current document. */
	AIAPI AIErr (*CountDocumentViews)( long *count );
	/** Get the Nth document view for the current document. The index is 1 based. Passing 0
		for the index will return the current view of the current document. */
	AIAPI AIErr (*GetNthDocumentView)( long n, AIDocumentViewHandle *view );

	/** Convert a point from artwork coordinates to view (window) coordinates. The resulting
		view coordinate is not rounded to the nearest pixel. See AIDocumentViewSuite::ArtworkPointToViewPoint()
		for a version that does rounding. The prefix "Fixed" is used for historical reasons. */
	AIAPI AIErr (*FixedArtworkPointToViewPoint) ( AIDocumentViewHandle view, AIRealPoint *artworkPoint, AIRealPoint *viewPoint );
	/** Convert a point from view coordinates to artwork coordinates. This version takes floating
		point coordinates as an input. See AIDocumentViewSuite::ViewPointToArtworkPoint() for a
		version that takes integer inputs. The prefix "Fixed" is used for historical reasons. */
	AIAPI AIErr (*FixedViewPointToArtworkPoint) ( AIDocumentViewHandle view, AIRealPoint *viewPoint, AIRealPoint *artworkPoint );

	/** This function sets the screen mode of the specified view. The screen mode is
		selected via the three screen mode icons on the bottom of the tool palette. */
	AIAPI AIErr (*SetScreenMode) ( AIDocumentViewHandle view, AIScreenMode mode );
	/** This function gets the screen mode of the specified view. The screen mode is
		selected via the three screen mode icons on the bottom of the tool palette. */
	AIAPI AIErr (*GetScreenMode) ( AIDocumentViewHandle view, AIScreenMode *mode );

	/** Get the page tiling information that describes how the artwork will be printed onto
		one or more pages. */
	AIAPI AIErr (*GetPageTiling) ( AIPageTiling *pageTiling );

	/** True if there is a visible template layer. */
	AIAPI AIErr (*GetTemplateVisible) ( AIDocumentViewHandle view, AIBoolean *visible );

	/** Scrolls the document window by a vector in artwork coordinates. */
	AIAPI AIErr (*DocumentViewScrollDelta)( AIDocumentViewHandle view, AIRealPoint *delta);
	/** Returns a rectangle in artwork coordinates that encloses (at least) the portions of the
		document that have been changed and so need to be redrawn. This rectangle is reset to
		be empty each time the #kAIDocumentViewInvalidRectChangedNotifier is sent. */
	AIAPI AIErr (*GetDocumentViewInvalidRect)( AIDocumentViewHandle view, AIRealRect *invalidRect );
	/** Sets the rectangle returned by AIDocumentViewSuite::GetDocumentViewInvalidRect() */
	AIAPI AIErr (*SetDocumentViewInvalidRect)( AIDocumentViewHandle view, AIRealRect *invalidRect );

	/** Get the display mode for the current view. This is a set of flags whose values may be
		#kVsArtwork, #kVsPreview, #kVsRaster, #kVsOpaque, #kVsInk. */
	AIAPI AIErr (*GetDocumentViewStyle)( AIDocumentViewHandle view,  short *style );

	/** Invalidates the rectangle in artwork coordinates. This will cause all views of the
		document that contain the given rectangle to update at the next opportunity. */
	AIAPI AIErr (*SetDocumentViewInvalidDocumentRect)( AIDocumentViewHandle view, AIRealRect *invalidRect );

	/** Is page tiling being shown? This API operates on the current view though each view
		maintains its own setting. */
	AIAPI AIErr (*GetShowPageTiling) ( ASBoolean *show );
	/** Set whether page tiling is being shown. This API operates on the current view though
		each view maintains its own setting. */
	AIAPI AIErr (*SetShowPageTiling) ( ASBoolean show );

	/** Get options for the grid: whether it is visible and whether snapping is enabled in
		the view. */
	AIAPI AIErr (*GetGridOptions) ( AIDocumentViewHandle view, ASBoolean* show, ASBoolean* snap );
	/** Get options for the grid: whether it is visible and whether snapping is enabled in
		the view. */
	AIAPI AIErr (*SetGridOptions) ( AIDocumentViewHandle view, ASBoolean show, ASBoolean snap );

	/** Is the transparency grid shown in the view. */
	AIAPI AIErr (*GetShowTransparencyGrid) ( AIDocumentViewHandle view, ASBoolean* show );
	/** Set whether the transparency grid is shown in the view. */
	AIAPI AIErr (*SetShowTransparencyGrid) ( AIDocumentViewHandle view, ASBoolean show );

	/** Get the document displayed in the view. */
	AIAPI AIErr (*GetDocumentViewDocument) ( AIDocumentViewHandle view, AIDocumentHandle *document );
	
	/** Force all document views to not be obscured by the Control Palette */
	AIAPI AIErr (*ForceDocumentViewsOnScreen) ( void );

	/** Get the shown state of the guides. */
	AIAPI AIErr (*GetShowGuides) ( AIDocumentViewHandle view, ASBoolean* show );
	
	/** Set the shown state of the guides. */
	AIAPI AIErr (*SetShowGuides) ( AIDocumentViewHandle view, ASBoolean show );
	
} AIDocumentViewSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
