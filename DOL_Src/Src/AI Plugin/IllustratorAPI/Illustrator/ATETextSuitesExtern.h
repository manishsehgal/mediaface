/* -------------------------------------------------------------------------------

                                    ADOBE CONFIDENTIAL
                                _________________________

    Copyright 2000-2003 Adobe Systems Incorporated
    All Rights Reserved.

    NOTICE:  All information contained herein is, and remains
     the property of Adobe Systems Incorporated and its suppliers,
     if any.  The intellectual and technical concepts  contained
     herein are proprietary to Adobe Systems Incorporated and its
     suppliers and may be covered by U.S. and Foreign Patents,
     patents in process, and are protected by trade secret or copyright law.
     Dissemination of this information or reproduction of this material
     is strictly forbidden unless prior written permission is obtained
     from Adobe Systems Incorporated.

 ----------------------------------------------------------------------------------

	File:	ATETextSuitesExtern.h
		
	Notes:	Machine Generated file from script version 1.45
	        Please don't modify manually!
	
 ---------------------------------------------------------------------------------- */
#ifndef __ATETextSuitesExtern__
#define __ATETextSuitesExtern__

namespace ATE
{
extern "C" ApplicationPaintSuite* sApplicationPaint;
extern "C" CompFontSuite* sCompFont;
extern "C" CompFontClassSuite* sCompFontClass;
extern "C" CompFontClassSetSuite* sCompFontClassSet;
extern "C" CompFontComponentSuite* sCompFontComponent;
extern "C" CompFontSetSuite* sCompFontSet;
extern "C" GlyphRunSuite* sGlyphRun;
extern "C" GlyphRunsIteratorSuite* sGlyphRunsIterator;
extern "C" MojiKumiSuite* sMojiKumi;
extern "C" MojiKumiSetSuite* sMojiKumiSet;
extern "C" TextFrameSuite* sTextFrame;
extern "C" TextFramesIteratorSuite* sTextFramesIterator;
extern "C" TextLineSuite* sTextLine;
extern "C" TextLinesIteratorSuite* sTextLinesIterator;
extern "C" TextResourcesSuite* sTextResources;
extern "C" ApplicationTextResourcesSuite* sApplicationTextResources;
extern "C" DocumentTextResourcesSuite* sDocumentTextResources;
extern "C" VersionInfoSuite* sVersionInfo;
extern "C" ArrayApplicationPaintRefSuite* sArrayApplicationPaintRef;
extern "C" ArrayRealSuite* sArrayReal;
extern "C" ArrayBoolSuite* sArrayBool;
extern "C" ArrayIntegerSuite* sArrayInteger;
extern "C" ArrayLineCapTypeSuite* sArrayLineCapType;
extern "C" ArrayFigureStyleSuite* sArrayFigureStyle;
extern "C" ArrayLineJoinTypeSuite* sArrayLineJoinType;
extern "C" ArrayWariChuJustificationSuite* sArrayWariChuJustification;
extern "C" ArrayStyleRunAlignmentSuite* sArrayStyleRunAlignment;
extern "C" ArrayAutoKernTypeSuite* sArrayAutoKernType;
extern "C" ArrayBaselineDirectionSuite* sArrayBaselineDirection;
extern "C" ArrayLanguageSuite* sArrayLanguage;
extern "C" ArrayFontCapsOptionSuite* sArrayFontCapsOption;
extern "C" ArrayFontBaselineOptionSuite* sArrayFontBaselineOption;
extern "C" ArrayFontOpenTypePositionOptionSuite* sArrayFontOpenTypePositionOption;
extern "C" ArrayUnderlinePositionSuite* sArrayUnderlinePosition;
extern "C" ArrayStrikethroughPositionSuite* sArrayStrikethroughPosition;
extern "C" ArrayParagraphJustificationSuite* sArrayParagraphJustification;
extern "C" ArrayArrayRealSuite* sArrayArrayReal;
extern "C" ArrayBurasagariTypeSuite* sArrayBurasagariType;
extern "C" ArrayPreferredKinsokuOrderSuite* sArrayPreferredKinsokuOrder;
extern "C" ArrayKinsokuRefSuite* sArrayKinsokuRef;
extern "C" ArrayMojiKumiRefSuite* sArrayMojiKumiRef;
extern "C" ArrayMojiKumiSetRefSuite* sArrayMojiKumiSetRef;
extern "C" ArrayTabStopsRefSuite* sArrayTabStopsRef;
extern "C" ArrayLeadingTypeSuite* sArrayLeadingType;
extern "C" ArrayFontRefSuite* sArrayFontRef;
extern "C" ArrayGlyphIDSuite* sArrayGlyphID;
extern "C" ArrayRealPointSuite* sArrayRealPoint;
extern "C" ArrayRealMatrixSuite* sArrayRealMatrix;
extern "C" CharFeaturesSuite* sCharFeatures;
extern "C" CharInspectorSuite* sCharInspector;
extern "C" CharStyleSuite* sCharStyle;
extern "C" CharStylesSuite* sCharStyles;
extern "C" CharStylesIteratorSuite* sCharStylesIterator;
extern "C" FindSuite* sFind;
extern "C" FontSuite* sFont;
extern "C" GlyphSuite* sGlyph;
extern "C" GlyphsSuite* sGlyphs;
extern "C" GlyphsIteratorSuite* sGlyphsIterator;
extern "C" KinsokuSuite* sKinsoku;
extern "C" KinsokuSetSuite* sKinsokuSet;
extern "C" ParaFeaturesSuite* sParaFeatures;
extern "C" ParagraphSuite* sParagraph;
extern "C" ParagraphsIteratorSuite* sParagraphsIterator;
extern "C" ParaInspectorSuite* sParaInspector;
extern "C" ParaStyleSuite* sParaStyle;
extern "C" ParaStylesSuite* sParaStyles;
extern "C" ParaStylesIteratorSuite* sParaStylesIterator;
extern "C" SpellSuite* sSpell;
extern "C" StoriesSuite* sStories;
extern "C" StorySuite* sStory;
extern "C" TabStopSuite* sTabStop;
extern "C" TabStopsSuite* sTabStops;
extern "C" TabStopsIteratorSuite* sTabStopsIterator;
extern "C" TextRangeSuite* sTextRange;
extern "C" TextRangesSuite* sTextRanges;
extern "C" TextRangesIteratorSuite* sTextRangesIterator;
extern "C" TextRunsIteratorSuite* sTextRunsIterator;
extern "C" WordsIteratorSuite* sWordsIterator;
}// namespace ATE
#endif //__ATETextSuitesExtern__


