#ifndef _IAIPAINT_H_
#define _IAIPAINT_H_

/*
 *        Name:	IAIPaint.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	C++ wrapper class for AIColor.
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#include	"AIColor.h"
#include	"AIRealMath.h"

namespace ai {

//--------------------------------------------------------------------------------
// AIColor is a misnomer. In fact it describes how an object is painted. This
// class can only be used to represent a fully defined paint. A "PartialPaint"
// should be used to represent a paint that may be partially defined.
//--------------------------------------------------------------------------------

class Paint : public AIColor {
public:
	// Cast a color to a paint
	static Paint& cast (AIColor& c);
	// Cast a const color to a const paint
	static const Paint& constcast (const AIColor& c);

	// Constructs a "none" paint. 
	Paint ();
	
	// Test if paints are equal
	bool operator == (const AIColor& b) const;
};


//--------------------------------------------------------------------------------
// A partial paint describes a paint that may have some unknown values.
//--------------------------------------------------------------------------------

class PartialPaint {
public:
	// Constructs a completely undefined paint
	PartialPaint ();
	// Constructs a paint from a color and map
	PartialPaint (const AIColor& color, const AIColorMap& colormap);

	// True iff the paint is fully determined
	bool IsFullyDefined () const;

protected:
	Paint fPaint;
	AIColorMap fMap;
};


//--------------------------------------------------------------------------------
// Paint implementation
//--------------------------------------------------------------------------------

inline Paint& Paint::cast (AIColor& c)
{
	return static_cast<Paint&>(c);
}

inline const Paint& Paint::constcast (const AIColor& c)
{
	return static_cast<const Paint&>(c);
}

inline Paint::Paint ()
{
	kind = kNoneColor;
}

inline bool Paint::operator == (const AIColor& b) const
{

	bool result = false;

	if (kind == b.kind)
	{
		switch (kind)
		{
			case kGrayColor:
			{
				if (c.g.gray == b.c.g.gray)
				{
					result = true;
				}
				break;
			}
			case kFourColor:
			{
				if (c.f.cyan == b.c.f.cyan &&
					c.f.magenta == b.c.f.magenta &&
					c.f.yellow == b.c.f.yellow &&
					c.f.black == b.c.f.black)
				{
					result = true;
				}
				break;
			}
			case kPattern:
			{
				if (c.p.pattern == b.c.p.pattern &&
					c.p.shiftDist == b.c.p.shiftDist &&
					c.p.shiftAngle == b.c.p.shiftAngle &&
					c.p.scale.h == b.c.p.scale.h &&
					c.p.scale.v == b.c.p.scale.v &&
					c.p.rotate == b.c.p.rotate &&
					c.p.reflect == b.c.p.reflect &&
					c.p.reflectAngle == b.c.p.reflectAngle &&
					c.p.shearAngle == b.c.p.shearAngle &&
					c.p.shearAxis == b.c.p.shearAxis &&
					sAIRealMath->AIRealMatrixEqual((AIRealMatrix *)&c.p.transform, (AIRealMatrix *)&b.c.p.transform))
				{
					result = true;
				}
				break;
			}
			case kCustomColor:
			{
				if (c.c.color == b.c.c.color &&
					c.c.tint == b.c.c.tint)
				{
					result = true;
				}
				break;
			}
			case kGradient:
			{
				if (c.b.gradient == b.c.b.gradient &&
					c.b.gradientOrigin.h == b.c.b.gradientOrigin.h &&
					c.b.gradientOrigin.v == b.c.b.gradientOrigin.v &&
					c.b.gradientAngle == b.c.b.gradientAngle &&
					c.b.gradientLength == b.c.b.gradientLength &&
					sAIRealMath->AIRealMatrixEqual((AIRealMatrix *)&c.b.matrix, (AIRealMatrix *)&b.c.b.matrix) &&
					c.b.hiliteAngle == b.c.b.hiliteAngle &&
					c.b.hiliteLength == b.c.b.hiliteLength)
				{
					result = true;
				}
				break;
			}
			case kThreeColor:
			{
				if (c.rgb.red == b.c.rgb.red &&
					c.rgb.green == b.c.rgb.green &&
					c.rgb.blue == b.c.rgb.blue)
				{
					result = true;
				}
				break;
			}
		}
	}

	return result;
}


//--------------------------------------------------------------------------------
// PartialPaint implementation
//--------------------------------------------------------------------------------

inline PartialPaint::PartialPaint () :
	fPaint()
{
	fMap.kind = false;
}

inline PartialPaint::PartialPaint (const AIColor& color, const AIColorMap& colormap) :
	fPaint(static_cast<const Paint&>(color)), fMap(colormap)
{
}

inline bool PartialPaint::IsFullyDefined () const
{
	bool result = false;

	if (fMap.kind)
	{
		switch (fPaint.kind)
		{
			case kGrayColor:
				result = fMap.c.g.gray != 0;
				break;

			case kThreeColor:
				result = fMap.c.rgb.red && 
						fMap.c.rgb.green && 
						fMap.c.rgb.blue;
				break;

			case kFourColor:
				result = fMap.c.f.cyan && 
						fMap.c.f.magenta && 
						fMap.c.f.yellow && 
						fMap.c.f.black;
				break;

			case kCustomColor:
				result = fMap.c.c.color && 
						fMap.c.c.tint;
				break;

			case kPattern:
				result = fMap.c.p.pattern &&
						fMap.c.p.pattern &&
						fMap.c.p.shiftDist &&
						fMap.c.p.shiftAngle &&
						fMap.c.p.scale &&
						fMap.c.p.rotate &&
						fMap.c.p.reflect &&
						fMap.c.p.reflectAngle &&
						fMap.c.p.shearAngle &&
						fMap.c.p.shearAxis &&
						fMap.c.p.transform;
				break;

			case kGradient:
				result = fMap.c.b.gradient &&
						fMap.c.b.gradient &&
						fMap.c.b.gradientOrigin &&
						fMap.c.b.gradientAngle &&
						fMap.c.b.gradientLength &&
						fMap.c.b.matrix &&
						fMap.c.b.hiliteAngle &&
						fMap.c.b.hiliteLength;
				break;
		}
	}

	return result;
}

// end of namespace ai
}


#endif
