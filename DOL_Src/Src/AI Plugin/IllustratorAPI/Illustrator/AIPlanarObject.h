#ifndef __AIPlanarObject__
#define __AIPlanarObject__

/*
 *        Name:	AIPlanarObject.h
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 12.0 Planar Object Suite.
 *
 * Copyright (c) 1986-2005 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AIArt__
#include "AIArt.h"
#endif
#ifndef __AIPath__
#include "AIPath.h"
#endif
#ifndef __AIArtStyle__
#include "AIArtStyle.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIPlanarObject.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIPlanarObjectSuite					"AI Planar Object Suite"
#define kAIPlanarObjectSuiteVersion1			AIAPI_VERSION(1)
#define kAIPlanarObjectSuiteVersion				kAIPlanarObjectSuiteVersion1
#define kAIPlanarObjectVersion					kAIPlanarObjectSuiteVersion

#define kLivePaintComplexityWarning				"LivePaintComplexityWarning"	// The AIPreference suffix for whether complexity
																				// warnings have been disabled.

/*******************************************************************************
 **
 **	Types
 **
 **/

/** An opaque reference to the "underconstruction" object that exists between a call to New and CreateArt.
	This is NOT a kind of AIArtHandle, and should not be passed to any API's except those in this suite.
*/
typedef struct _AIPlanarObject*	AIPlanarObjectHandle;

/** Planar Object options
	The gapMax is the maximum size gap between real edges that will be treated as an implied edge for separating faces.
	The gapArea is the minimum size area of that a gap-generated face must have to avoid pruning the gap as trivial.
*/
typedef struct 
{
	AIReal			gapMax;
	AIBoolean		gapDetectEnabled;
} AIGapOptions;


/* Restrictions:
		The paths added by AddPath, collectively, must specify a consistent planar map.  They must not intersect other
		than at endpoints of the B�zier curves, although intersections from rounding and path flattening inaccuracy
		are allowed. Each face of the map must have a consistent style as defined by the styles of each bounding path.
		The paths may intersect as long as each path has a B�zier endpoint at each intersection.

		Violating these conditions is not always possible to detect. If a violation is detected, AddPath will return
		kNonPlanarGeometryErr, but the results of a violation are, in general, undefined.
*/

/*******************************************************************************
 **
 **	Suite
 **
 **/


/** APIs for creating, populating and querying attributes of a Planar Object plugin group. */
typedef struct {

	/** Begin construction of a planar object with the given options (see #AIGapOptions.)
		The matrix translates from the AIArtObject space into the PlanarObject space. The matrix
		parameter is required; the gapOptions parameter can be NULL. If NULL, gap detection is off
		by default.
	*/
	AIAPI AIPlanarObjectHandle (*NewPlanarObject) ( const AIRealMatrix* matrix, const AIGapOptions* gapOptions );

	/** Adds a path to the specified planar object.

		The leftFill, rightFill, and stroke arrays all must have segmentCount members. If closed is true,
		the segments array must also have segmentCount members. If closed is false, the segments array should
		have segmentCount+1 members, since despite its name, an AIPathSegment is really an anchor point with
		its two handles, and an open path has one more anchor point than it has bezier segments.

		The styles correspond to the paint styles to the left, right, and along each bezier segment. Only the
		fill is used from the left and right styles, and only the stroke is used from the stroke style. Note that
		there is no coalescing of consecutive segments that share the same stroke and same styles on each side
		into a multi-segment data structure. So a path that separates only two faces would have the same style
		handles repeated multiple times in each style array. 

		A style array entry may be nil, which corresponds to no fill or stroke. Entries in the leftFill and
		rightFill arrays that refer to the outside of the planar object must be nil.

		The closed parameter has the same meaning as in AIPath.
	*/
	AIAPI AIErr (*AddPath) (
			AIPlanarObjectHandle object, int segmentCount, AIPathSegment *segments,
			AIArtStyleHandle *leftFill, AIArtStyleHandle *rightFill, AIArtStyleHandle *stroke,
			AIBoolean closed );

	/* Same as above, except that only one set of attributes is passed in. This API is intended for paths that
		have uniform stroke style and separate only two faces, such as closed shapes surrounded entirely by one
		other face.
	*/
	AIAPI AIErr (*AddPathUniform) (
			AIPlanarObjectHandle object, int segmentCount, AIPathSegment *segments,
			AIArtStyleHandle leftFill, AIArtStyleHandle rightFill, AIArtStyleHandle stroke,
			AIBoolean closed );

	/** Abandons the creation of a planar object.  The object handle is no longer valid after calling this method. */
	AIAPI AIErr (*Abandon) ( AIPlanarObjectHandle object );

	/** Creates a new planar plugin group art object from object, at the location specified by paintOrder and prep
		(interpreted the same way as the arguments to NewArt in the AIArt suite).

		The edit group will contain paths constructed from the segments and styles passed in to AddPath. (There may
		not be a one-to-one correspondence between the number of paths added via AddPath and the final contents of
		the edit group, since CreateArt reserves the right to coalesce or split paths as necessary to maximize
		editability and preserve validity of the planar map.) The result group will be populated taking into account
		the gap options, and the plugin object newArt will be "clean".

		The planar object handle is no longer valid after calling this method.
	*/
	AIAPI AIErr (*CreateArt) ( AIPlanarObjectHandle object, short paintOrder, AIArtHandle prep, AIArtHandle *newArt );

	/** Tests whether an arbitrary art object is a Planar Object plugin group. */
	AIAPI AIBoolean (*IsPlanarArtObject) ( AIArtHandle art );

	/** Reports whether the PlanarPlugin is prepared to handle conversion of a selection of the indicated complexity,
		based on the amount of memory available and the given gap options. (gapOptions can be null and if so will be
		interpreted the same as it is by NewPlanarObject.)

		If fromPlanarMap is true, then pathOrEdgeCount represents the number of edges in the planar map.
		If fromPlanarMap is false, then pathOrEdgeCount represents the number of legacy paths in a selection
			or in the result art of a tracing object.

		Can return the following results:
			kNoErr			Either the results were judged to be simple enough that no warning was necessary,
								or user interaction was disabled (e.g., when running from a script), or a
								complexity warning was displayed and the user clicked Continue to accept the risk.
							If the client cares which is the case, then it can be optionally requested in the
							warningDisplayed output parameter. This information might be wanted, for example,
							to prevent displaying a redundant warning if complexity checks are potentially made
							at multiple places.
			kCanceledErr	The results were judged to be complex enough to warrant a warning, and the
								user responded with a Cancel
	*/
	AIAPI AIErr (*ComplexConversionCanceled) ( AIBoolean fromPlanarMap, ASInt32 pathOrEdgeCount,
											const AIGapOptions* gapOptions, AIBoolean *warningDisplayed /*output*/ );

} AIPlanarObjectSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif	// __AIPlanarObject__
