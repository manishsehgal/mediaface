#ifndef __AISVGFilter__
#define __AISVGFilter__

/*
 *        Name:	AISVGFilter.h
 *   $Revision: 2 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Suite.
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AIArtStyle.h"
#include "AIUID.h"
#include "AIXMLElement.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AISVGFilter.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAISVGFilterSuite				"AI SVG Filter Suite"
#define kAISVGFilterSuiteVersion1		AIAPI_VERSION(1)
#define kAISVGFilterSuiteVersion2		AIAPI_VERSION(2)
#define kAISVGFilterSuiteVersion		kAISVGFilterSuiteVersion2
#define kAISVGFilterVersion				kAISVGFilterSuiteVersion

/** @ingroup Notifiers */
#define kAISVGFilterChangedNotifier		"AI SVG Filter Changed Notifier"
/** @ingroup Notifiers
	See AISVGFilterAddedNotifierData for data. */
#define kAISVGFilterAddedNotifier		"AI SVG Filter Added Notifier"

/** @ingroup Errors
	ID In Use error */
#define kXMLIDCollisionErr	kUIDNotUnique
/** @ingroup Errors
	ID Changed Notice */
#define kXMLIDChangedErr	'IDCN'
/** @ingroup Errors
	no svg filter error */
#define kNoSVGFilterErr		'!SFE'
/** @ingroup Errors
	attempt to redef. existing filter with new XMLID */
#define kSVGFilterRedefErr	'SRDE'

/*******************************************************************************
 **
 **	Types
 **
 **/

/** Notification data for the #kAISVGFilterAddedNotifier */
typedef struct {
	AIDocumentHandle document;
	AISVGFilterHandle filter;
} AISVGFilterAddedNotifierData;

/*******************************************************************************
 **
 **	Suite
 **
 **/

/** The SVG filter effect makes it possible to apply SVG filters to Illustrator artwork.
	When an SVG file is written these filters are preserved where possible. The SVG
	filter effect applies filters from a global list associated with the current document.
	
	The APIs in SVG filter suite provide facilities for managing the global list of
	filters, for attaching filters to art and manipulating art with SVG filters attached.

	There are two notifiers associated with SVG filters:

	- #kAISVGFilterChangedNotifier
	- #kAISVGFilterAddedNotifier

	The following error codes are associated with SVG filters:

	- #kXMLIDCollisionErr
	- #kXMLIDChangedErr
	- #kNoSVGFilterErr
	- #kSVGFilterRedefErr
 */
typedef struct {

	/** Make a new SVG filter in the current document. */
	AIAPI AIErr (*NewSVGFilter) ( AISVGFilterHandle *newFilter );
	/** Delete the SVG filter from the current document. */
	AIAPI AIErr (*DeleteSVGFilter) ( AISVGFilterHandle filter );
	/** Get a count of the number of SVG filters in the current document. */
	AIAPI AIErr (*CountSVGFilters) ( long *count );
	/** Get the nth filter. */
	AIAPI AIErr (*GetNthSVGFilter) ( long n, AISVGFilterHandle *filter );

	/** SetSVGFilterContents will return #kXMLIDCollisionErr if the xml id specified as an attribute 
		of the "filter" element is already in use in the current document and forceUniqueID is false.
		If forceUniqueID is true, SetSVGFilterContents will rename the filter to have a unique id.
		It will return #kXMLIDChangedErr, if it changed the ID.  You can retreive the ID with
		GetSVGFilterUIDName(). */
	AIAPI AIErr (*SetSVGFilterContents) ( AISVGFilterHandle filter, AIXMLNodeRef filterContents, 
		bool forceUniqueID );
	AIAPI AIErr (*GetSVGFilterContents) ( AISVGFilterHandle filter, AIXMLNodeRef *filterContents );

	/** Get the filter handle associated with the id "name". */
	AIAPI AIErr (*GetSVGFilter) ( const ai::UnicodeString& name, AISVGFilterHandle *filter );
	/** Get the name/UID for the filter handle "filter" */
	AIAPI AIErr (*GetSVGFilterUIDName) (AISVGFilterHandle filter, ai::UnicodeString& name);

	/** Returns a copy of the input art that the SVG Filter Effect is seeing; clients MUST dispose
		this art via the DisploseFilterArt() call below. */
	AIAPI AIErr (*GetSVGFilterArt) ( AIArtHandle art, AIArtHandle* svgArt );
	/** Disposes of the inout art to an SVG filter. See GetSVGFilterArt(). */
	AIAPI AIErr (*DisposeFilterArt) ( AIArtHandle art );

	/** Apply the SVG Filter "filter" to "art" */
	AIAPI AIErr (*AttachFilter) ( AIArtHandle art, AISVGFilterHandle filter );

	/** Given an AIArtStyleHandle, if the style is capable of generating an SVGFilter to represent 
		iteself, it will return a handle to said filter in "filter".  If the style isn't representable
		as SVG SVGFilterFromAIArtStyle() will return #kNoSVGFilterErr */
	AIAPI AIErr (*SVGFilterFromAIArtStyle) ( const AIArtStyleHandle artStyle, AISVGFilterHandle *filter);

} AISVGFilterSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
