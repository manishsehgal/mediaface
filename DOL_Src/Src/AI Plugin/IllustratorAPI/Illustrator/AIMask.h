#ifndef __AIMask__
#define __AIMask__

/*
 *        Name:	AIMask.h
 *   $Revision: 3 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 11.0 Mask Suite.
 *
 * Copyright (c) 1986-2003 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AIArtStyle__
#include "AIArtStyle.h"
#endif

#ifndef __AIDictionary__
#include "AIDictionary.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/*******************************************************************************
 **
 **	Suite name and version
 **
 **/

#define kAIBlendStyleSuite				"AI Blend Style Suite"
#define kAIBlendStyleSuiteVersion3		AIAPI_VERSION(3)
#define kAIBlendStyleSuiteVersion		kAIBlendStyleSuiteVersion3
#define kAIBlendStyleVersion			kAIBlendStyleSuiteVersion

#define kAIMaskSuite					"AI Mask Suite"
#define kAIMaskSuiteVersion3			AIAPI_VERSION(3)
#define kAIMaskSuiteVersion				kAIMaskSuiteVersion3
#define kAIMaskVersion					kAIMaskSuiteVersion


/*******************************************************************************
 **
 **	Types
 **
 **/


/** An opaque reference to a mask. There is a one-to-one correspondence between
	objects and masks. */
typedef struct _AIMask*					AIMaskRef;


/** The various blending modes which can be used to composite an object. These
	modes correspond to the ones in Photoshop. See #AIBlendingModeValues. */
typedef long AIBlendingMode;

enum AIBlendingModeValues {
	kAINormalBlendingMode			= 0L,
	kAIMultiplyBlendingMode			= 1L,
	kAIScreenBlendingMode			= 2L,
	kAIOverlayBlendingMode			= 3L,
	kAISoftLightBlendingMode		= 4L,
	kAIHardLightBlendingMode		= 5L,
	kAIColorDodgeBlendingMode		= 6L,
	kAIColorBurnBlendingMode		= 7L,
	kAIDarkenBlendingMode			= 8L,
	kAILightenBlendingMode			= 9L,
	kAIDifferenceBlendingMode		= 10L,
	kAIExclusionBlendingMode		= 11L,
	kAIHueBlendingMode				= 12L,
	kAISaturationBlendingMode		= 13L,
	kAIColorBlendingMode			= 14L,
	kAILuminosityBlendingMode		= 15L,
	kAINumBlendingModes				= 16L
};


/** Knockout tri-state */
typedef enum AIKnockout
{
	kAIKnockoutUnknown = -1,
	kAIKnockoutOff = 0,
	kAIKnockoutOn,
	kAIKnockoutInherit
} AIKnockout;


/* Dictionary keys for compositing attributes */

/** @ingroup DictKeys */
#define kAIBlendModeKey					"Mode"
/** @ingroup DictKeys */
#define kAIBlendOpacityKey				"Opacity"
/** @ingroup DictKeys */
#define kAIBlendIsolatedKey				"Isolated"
/** @ingroup DictKeys */
#define kAIBlendKnockoutKey				"Knockout"
/** @ingroup DictKeys */
#define kAIBlendAlphaIsShapeKey			"AlphaIsShape"


/*******************************************************************************
 **
 **	Suite
 **
 **/


/** When something is drawn it is composited with what was drawn before
	it. The manner in which this compositing takes place is controlled by
	a blend style. Each art object has a blend style that controls the
	compositing of the entire object. This blend style is a part of the
	object's art style. In addition fills and strokes can have their own
	compositing attributes.
	
	See the PDF language specification for more details of the transparency
	mode used by Illustrator. */
typedef struct {

	/** Get the blending mode applied to the object as a whole. */	
	AIAPI AIBlendingMode (*GetBlendingMode) (AIArtHandle art);
	/** Set the blending mode applied to the object as a whole. */	
	AIAPI AIErr (*SetBlendingMode) (AIArtHandle art, AIBlendingMode mode);
	
	/** Get the opacity applied to the object as a whole. The range is from
		0 for completely transparent to 1 for completely opaque. */	
	AIAPI AIReal (*GetOpacity) (AIArtHandle art);
	/** Set the opacity applied to the object as a whole. The range is from
		0 for completely transparent to 1 for completely opaque. */	
	AIAPI AIErr (*SetOpacity) (AIArtHandle art, AIReal opacity);
	
	/** Get whether any blending modes used in the rendering of the object
		contents are isolated from what has already been drawn. This does
		not affect the blending mode used for compositing the entire object
		into its backdrop. */
	AIAPI AIBoolean (*GetIsolated) (AIArtHandle art);
	/** Set whether any blending modes used in the rendering of the object
		contents are isolated from what has already been drawn. This does
		not affect the blending mode used for compositing the entire object
		into its backdrop. */
	AIAPI AIErr (*SetIsolated) (AIArtHandle art, AIBoolean isolated);

	/** Get the knockout setting for this object. */
	AIAPI AIKnockout (*GetKnockout) (AIArtHandle art);
	/** Get the knockout setting for this object after resolving inheritance.
		The value will always be either on or off. */
	AIAPI AIKnockout (*GetInheritedKnockout) (AIArtHandle art);
	/** Set the knockout setting for this object. */
	AIAPI AIErr (*SetKnockout) (AIArtHandle art, AIKnockout knockout);
	
	/** Get the alpha is shape setting for this object. */
	AIAPI AIBoolean (*GetAlphaIsShape) (AIArtHandle art);
	/** Set the alpha is shape setting for this object. */
	AIAPI AIErr (*SetAlphaIsShape) (AIArtHandle art, AIBoolean alphaIsShape);
	
	/** Copy the compositing attributes applied to the source art as a whole to
		those for the destination art.*/
	AIAPI AIErr (*Copy) (const AIArtHandle source, AIArtHandle destination);

	/** Get the compositing attributes applied to the object as a whole via a
		dictionary. This is a short hand for first getting the object's style and
		then using the GetStyleAttrs() method. The keys for the compositing
		attributes are:
		
		- #kAIBlendModeKey
		- #kAIBlendOpacityKey
		- #kAIBlendIsolatedKey
		- #kAIBlendKnockoutKey
		- #kAIBlendAlphaIsShapeKey
	*/
	AIAPI AIErr (*GetArtAttrs) (AIArtHandle art, AIDictionaryRef attrs);
	/** Set the compositing attributes applied to the object as a whole via a
		dictionary. This is a short hand for first getting the object's style and
		then using the SetStyleAttrs() method. See GetArtAttrs() for definitions
		of the keys. A subset of the keys may be specified in order to modify a
		subset of the compositing attributes. */
	AIAPI AIErr (*SetArtAttrs) (AIArtHandle art, AIDictionaryRef attrs);

	/** Get the compositing attributes specified by the style that would be
		applied to an object as a whole via a dictionary. See GetArtAttrs() for
		definitions of the keys. */
	AIAPI AIErr (*GetStyleAttrs) (AIArtStyleHandle style, AIDictionaryRef attrs);
	/** Set the compositing attributes specified by the style that would be
		applied to an object as a whole via a dictionary. A new style is returned
		that represents the modified style. The original style is not modified.
		See GetArtAttrs() for definitions of the keys. A subset of the keys may be
		specified in order to modify a subset of the compositing attributes. */
	AIAPI AIErr (*SetStyleAttrs) (AIArtStyleHandle style, AIDictionaryRef attrs,
								  AIArtStyleHandle* newStyle);

	/** Get the compositing attributes for the current selection in the document.
		Separate dictionaries are returned for the common attributes applied
		to objects as a whole, the common attributes of their focal fills and
		their focal strokes. Only the common attributes are returned. For
		example, if two objects are selected with different opacities the
		opacity entry will not be present in the dictionary. If the client is not
		interested in the values for a specific dictionary NULL may be supplied.
		See GetArtAttrs() for definitions of the keys. */
	AIAPI AIErr (*GetCurrentTransparency) (AIDictionaryRef styleAttrs,
										   AIDictionaryRef fillAttrs,
										   AIDictionaryRef strokeAttrs);
	/** Set the compositing attributes for the current selection in the document.
		Separate dictionaries are supplied for applying to objects as a whole,
		focal fills and focal strokes. The dictionaries can specify a subset of
		keys in order to modify a subset of the compositing attributes. Additionally
		a dictionary may be omitted if none of the corresponding attributes are
		to be modified. See GetArtAttrs() for definitions of the keys. */
	AIAPI AIErr (*SetCurrentTransparency) (AIDictionaryRef styleAttrs,
										   AIDictionaryRef fillAttrs,
										   AIDictionaryRef strokeAttrs);

	/** Get the compositing attributes for the focal fill of the art style via a
		dictionary. See GetArtAttrs() for definitions of the keys. */	
	AIAPI AIErr (*GetFocalFillAttrs) (AIArtStyleHandle artStyle, AIDictionaryRef attrs);
	/** Get the compositing attributes for the focal stroke of the art style via a
		dictionary. See GetArtAttrs() for definitions of the keys. */	
	AIAPI AIErr (*GetFocalStrokeAttrs) (AIArtStyleHandle artStyle, AIDictionaryRef attrs);
	/** Set the compositing attributes for the focal fill of the art style via a
		dictionary. See GetArtAttrs() for definitions of the keys. */	
	AIAPI AIErr (*SetFocalFillAttrs) (AIArtStyleHandle artStyle, AIDictionaryRef attrs,
											 AIArtStyleHandle* newStyle);
	/** Set the compositing attributes for the focal stroke of the art style via a
		dictionary. See GetArtAttrs() for definitions of the keys. */	
	AIAPI AIErr (*SetFocalStrokeAttrs) (AIArtStyleHandle artStyle, AIDictionaryRef attrs,
											   AIArtStyleHandle* newStyle );

	/** Return whether the object (or group, or layer) contains any artwork with
		non-normal blending that is not isolated at the level of the object. */
	AIAPI AIBoolean (*ContainsNonIsolatedBlending) (AIArtHandle object);

	// New to Illustrator 11:

	/** Get the isolation attribute of the page group, which contains all the layers in the
		document. */
	AIAPI AIBoolean (*GetDocumentIsolated) (void);
	/** Set the isolation attribute of the page group, which contains all the layers in the
		document. */
	AIAPI AIErr (*SetDocumentIsolated) (AIBoolean isolated);

	/** Get the knockout attribute of the page group, which contains all the layers in the
		document. */
	AIAPI AIBoolean (*GetDocumentKnockout) (void);
	/** Set the knockout attribute of the page group, which contains all the layers in the
		document. */
	AIAPI AIErr (*SetDocumentKnockout) (AIBoolean knockout);

} AIBlendStyleSuite;


/** Each art object may have an associated opacity mask. This is a source of opacity
	values that are combined with the object's own opacity when compositing it. */
typedef struct {

	/** Masks are reference counted. Call Release once you are done with
		a mask to free its memory. */
	AIAPI ASInt32 (*AddRef) (AIMaskRef mask);
	AIAPI ASInt32 (*Release) (AIMaskRef mask);

	/** GetMask obtains the mask associated with an object if any. */
	AIAPI AIErr (*GetMask) (AIArtHandle object, AIMaskRef* mask);

	/** Create a mask for an object if it doesn't already have one. The
		art of the newly created mask is an empty group object. */
	AIAPI AIErr (*CreateMask) (AIArtHandle object);

	/** Delete the mask associated with an object. */
	AIAPI AIErr (*DeleteMask) (AIArtHandle object);

	/** The link state controls whether the mask is linked to the object.
		When the mask is linked certain actions on the object automatically
		apply to the mask. For example, rotating the object will also
		rotate the mask. */
	AIAPI AIBoolean (*GetLinked) (AIMaskRef mask);
	AIAPI AIErr (*SetLinked) (AIMaskRef mask, AIBoolean linked);

	/** The disabled state controls whether the mask is ignored for rendering
		purposes. */
	AIAPI AIBoolean (*GetDisabled) (AIMaskRef mask);
	AIAPI AIErr (*SetDisabled) (AIMaskRef mask, AIBoolean disabled);

	/** The inverted state controls whether the opacity of the mask is
		inverted before being applied. */
	AIAPI AIBoolean (*GetInverted) (AIMaskRef mask);
	AIAPI AIErr (*SetInverted) (AIMaskRef mask, AIBoolean inverted);

	/** Copy the mask and the link status. */
	AIAPI AIErr (*Copy) (const AIArtHandle source, AIArtHandle destination);

	/** GetArt returns the handle to the mask art which will be NULL if there
		is none. */
	AIAPI AIArtHandle (*GetArt) (AIMaskRef mask);

	/** Returns true if the mask art is currently being edited. */
	AIAPI AIBoolean (*IsEditingArt) (AIMaskRef mask);
	/** Initiates or terminates editing of the mask art depending on the
		values of isedit. Editing mask art causes a new layer to be created
		at the top of the current layer list containing the mask art. When
		editing is terminated the layer is deleted. See the AILayerListSuite
		for information on layer lists. */
	AIAPI AIErr (*SetEditingArt) (AIMaskRef mask, AIBoolean isedit);

	/** If "mask" is a group object which is the mask art for some other object
		then on return "masked" will be the masked object. If not then
		"masked" will be nil on return. */
	AIAPI AIErr (*GetMaskedArt) (AIArtHandle mask, AIArtHandle* masked);

	/** The clipping state controls whether the area outside the mask art is
		considered to have 0% alpha (clipping) or 100% alpha (not clipping). */
	AIAPI AIBoolean (*GetClipping) (AIMaskRef mask);
	AIAPI AIErr (*SetClipping) (AIMaskRef mask, AIBoolean clipping);

} AIMaskSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
