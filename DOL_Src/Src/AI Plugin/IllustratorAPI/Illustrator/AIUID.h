#ifndef __AIUID__
#define __AIUID__

/*
 *        Name:	AIUID.h
 *		$Id $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator Unique ID Suite.
 *
 * Copyright (c) 2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AIEntry__
#include "AIEntry.h"
#endif

#include "IAIUnicodeString.h"


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIUID.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIUIDPoolSuite							"AI UID Pool Suite"
#define kAIUIDPoolSuiteVersion3					AIAPI_VERSION(4)
#define kAIUIDPoolSuiteVersion					kAIUIDPoolSuiteVersion3
#define kAIUIDPoolVersion						kAIUIDPoolSuiteVersion

#define kAIUIDSuite								"AI UID Suite"
#define kAIUIDSuiteVersion4						AIAPI_VERSION(6)
#define kAIUIDSuiteVersion						kAIUIDSuiteVersion4
#define kAIUIDVersion							kAIUIDSuiteVersion

#define kAIUIDREFSuite							"AI UIDREF Suite"
#define kAIUIDREFSuiteVersion4					AIAPI_VERSION(6)
#define kAIUIDREFSuiteVersion					kAIUIDREFSuiteVersion4
#define kAIUIDREFVersion						kAIUIDREFSuiteVersion

#define kAIUIDUtilsSuite						"AI UID Utils Suite"
#define kAIUIDUtilsSuiteVersion5				AIAPI_VERSION(7)
#define kAIUIDUtilsSuiteVersion					kAIUIDUtilsSuiteVersion5
#define kAIUIDUtilsVersion						kAIUIDUtilsSuiteVersion


/** kUIDBadSyntax indicates that a UID name was specified with an invalid syntax. */
#define kUIDBadSyntax				'UIDx'
/** kUIDNotUnique indicates that the name specified is already in use by another UID. */
#define kUIDNotUnique				'UIDu'
/** kUIDNotFound indicates that UID was not found .*/
#define kUIDNotFound				'UID!'


/**
	kAIXMLNameChangedNotifier indicates that the XML name pool changed.
	As a result any ids or references associated with that name will now
	have a different name.
*/
#define kAIXMLNameChangedNotifier				"AI XML Name Changed Notifier"


/*******************************************************************************
 **
 **	Types
 **
 **/

/**
	A pool of names. These names can then be associated with objects such
	as ids, each one having its own distinct name. Each document has its
	own set of name pools. A name pool may impose syntactic constraints on
	the names it may contain.
	@see AIUIDRef
	@see AIUIDREFRef
*/
typedef struct _t_AINamePool *AINamePoolRef;

// A unique id. Unique ids are subclasses of container entries. This allows
// them to be stored within dictionaries and arrays. The AIEntry suite
// provides methods to convert between AIEntryRef and AIUIDRef. A unique id
// may not be put into more than one container or more than once in a single
// container. Any attempt to do so will result in the error kUIDNotUnique. 
//typedef struct _t_AIUID *AIUIDRef;

// A reference to a unique id. Unique id references are subclasses of container
// entries. This allows them to be stored within dictionaries and arrays. The
// AIEntry suite provides methods to convert between AIEntryRef and AIUID.
//typedef struct _t_AIUIDREF *AIUIDREFRef;


/*******************************************************************************
 **
 **	Suite
 **
 **/


/**
	The unique id suites provide facilities for creating, querying and
	managing unique ids and references to those ids.

	A unique id is an object that can be stored in a container such as
	a dictionary or an array. Given a unique id it is possible to find
	its container. One particularly useful place to store a unique id is
	in the dictionary of an art object. Given such an id it is possible
	to find the associated art object. In fact art object names are
	ultimately stored as specially encoded unique ids in their associated
	art object dictionaries.

	Unique ids have three important properties. 1) Each id is unique within
	a given pool of ids. 2) Each id has a name which is also unique within
	that pool. 3) Each id can be stored in at most one container. 
	
	AIUIDRef:

	Unique ids are subclasses of container entries. This allows
	them to be stored within dictionaries and arrays. The AIEntry suite
	provides methods to convert between AIEntryRef and AIUIDRef. A unique id
	may not be put into more than one container or more than once in a single
	container. Any attempt to do so will result in the error kUIDNotUnique.

	AIUIDREFRef:

	A reference to a unique id. Unique id references are subclasses of container
	entries. This allows them to be stored within dictionaries and arrays. The
	AIEntry suite provides methods to convert between AIEntryRef and AIUID.
 */

typedef struct AIUIDPoolSuite {

	// reference count maintenance.
	AIAPI ASInt32 (*AddRef) ( void* pool );
	AIAPI ASInt32 (*Release) ( void* pool );

	/** Get a name pool by its name. At present there is a single pool for
	XML unique id names. This pool is returned regardless of the name
	supplied. */
	AIAPI AIErr (*GetPool) ( const char* name, AINamePoolRef* pool );

	/** Make a new unique id with the given name from the pool. If no name is
	specified then the pool will manufacture a name which is guaranteed
	not to be in use. If the name is specified then it must conform to the
	syntax imposed by the pool and must not be in use. UIDs are reference
	counted. */
	AIAPI AIErr (*NewUID) ( AINamePoolRef pool, const ai::UnicodeString& name, AIUIDRef* uid );

	/** Make a new reference to the unique id in the pool with the given name.
	If no name is specified then the pool will manufacture a name which is
	guaranteed not to be in use. If the name is specified then it must conform to
	the syntax imposed by the pool and must either not be in use of must be
	in use as a name for a UID or UIDREF. UIDREFs are reference counted. */
	AIAPI AIErr (*NewUIDREF) ( AINamePoolRef pool, const ai::UnicodeString& name, AIUIDREFRef* uidref );

	/** Make a new unique id based on the given name from the pool. A name that is
	guaranteed to be unique is automatically generated from the base name. If
	the base name is specified then it must conform to the syntax imposed by the
	pool. UIDs are reference counted. */
	AIAPI AIErr (*NewUIDFromBase) ( AINamePoolRef pool, const ai::UnicodeString& base, AIUIDRef* uid );

} AIUIDPoolSuite;


/**
	APIs for working with unique ids.
*/
typedef struct AIUIDSuite {

	// reference count maintenance.
	AIAPI ASInt32 (*AddRef) ( void* uid );
	AIAPI ASInt32 (*Release) ( void* uid );

	/** Returns true if the UID is currently stored within some container */
	AIAPI AIBoolean (*IsInUse) ( AIUIDRef uid );

	/** Returns the pool to which the uid's name belongs. The result is NULL if the
	uid is not valid. */
	AIAPI void (*GetPool) ( AIUIDRef uid, AINamePoolRef* pool );

	/** Returns the name of the uid as a UnicodeString. An error is returned if
		name cannot be filled in or if the uid is not valid.
		@param uid to get the name of.
		@param name UnicodeString to return the name in.
		@return status of the call. */
	AIAPI AIErr (*GetName) ( AIUIDRef uid, ai::UnicodeString& name );

	/** Get an entry for the container in which the uid is currently
	stored. A NULL reference is returned if the uid is not stored
	in a container. */
	AIAPI AIEntryRef (*GetContainer) ( AIUIDRef uid );

	/** Make a new reference to the unique id. UIDREFs are reference counted. */
	AIAPI AIErr (*NewUIDREF) ( AIUIDRef uid, AIUIDREFRef* ruidref );

} AIUIDSuite;


/**
	APIs for working with references to unique ids.
*/
typedef struct AIUIDREFSuite {

	// reference count maintenance.
	AIAPI ASInt32 (*AddRef) ( void* uidref );
	AIAPI ASInt32 (*Release) ( void* uidref );

	/** Returns true if the UIDREF is currently stored within some container */
	AIAPI AIBoolean (*IsInUse) ( AIUIDREFRef uidref );

	/** Returns the pool to which the UIDREF's name belongs. The result is NULL if the
	UIDREF is not valid. */
	AIAPI void (*GetPool) ( AIUIDREFRef uidref, AINamePoolRef* pool );

	/** Returns the name of the UIDREF as a UnicodeString. An error is returned if
		name cannot be filled in or if the UIDREF is not valid.
		@param uidref to get the name of.
		@param name UnicodeString to return the name in.
		@return status of the call. */
	AIAPI AIErr (*GetName) ( AIUIDREFRef uidref, ai::UnicodeString& name );

	/** Get an entry for the container in which the UIDREF is currently
	stored. A NULL reference is returned if the UIDREF is not stored
	in a container. */
	AIAPI AIEntryRef (*GetContainer) ( AIUIDREFRef uidref );

	/** Get the unique id referred to. NULL if there is none. */
	AIAPI void (*GetUID) ( AIUIDREFRef uidref, AIUIDRef* uid );

} AIUIDREFSuite;


/**
	convenience APIs for working with art object UIDs.
*/
typedef struct AIUIDUtilsSuite {

	/** Get the unique id of the art object. If the object has no UID and create
		is TRUE then a machine generated UID will be assigned to the object. */
	AIAPI AIErr (*GetArtUID) ( AIArtHandle art, AIBoolean create, AIUIDRef* uid );
	/** Set the unique id of the art object replacing its current id if any. */
	AIAPI AIErr (*SetArtUID) ( AIArtHandle art, AIUIDRef uid );
	/** Transfer the id from the source to the destination object. The id
		of the source art is set to NULL. */
	AIAPI AIErr (*TransferArtUID) ( AIArtHandle srcart, AIArtHandle dstart );

	/** Get a new UIDREF for the art object. The object will be assigned a uid if
		it does not already have one. */
	AIAPI AIErr (*NewArtUIDREF) ( AIArtHandle art, AIUIDREFRef* uidref );
	/** Get the art object referenced by the UIDREF. If the UIDREF is associated
		with a text story (sequence of linked text frames) then the first frame
		of the story is returned. */
	AIAPI AIErr (*GetReferencedArt) ( AIUIDREFRef uidref, AIArtHandle* art );

	/** Get the UID name of the art object. An empty string indicates no id
		is assigned. */
	AIAPI AIErr (*GetArtUIDName) ( AIArtHandle art, ai::UnicodeString& name );
	/** Set the UID name of the art object. Returns an error if the id is
		already in use. */
	AIAPI AIErr (*SetArtUIDName) ( AIArtHandle art, const ai::UnicodeString& name );

	/** Get the name or XML ID of the art object depending on the setting of
		the user preference. isDefaultName may be nil, if not then it is set
		to true if the art object has no name or id and consequently the default
		name for the art was returned. */
	AIAPI AIErr (*GetArtNameOrUID) ( AIArtHandle art, ai::UnicodeString& name, ASBoolean* isDefaultName );
	/** Set the name or XML ID of the art object depending on the setting of
		the user preference. Note that names are converted to valid XML IDs
		and then used to specify the XML ID of the object. */
	AIAPI AIErr (*SetArtNameOrUID) ( AIArtHandle art, const ai::UnicodeString& name );

	/** Manufacture a UID based on an existing UID */
	AIAPI AIErr (*MakeUIDFromBase) ( AIUIDRef base, AIUIDRef* uid );
	/** Manufacture a unique name from a base name */
	AIAPI AIErr (*MakeUniqueNameFromBase) ( const ai::UnicodeString& base, ai::UnicodeString& name );

	/** Get the unique id of the story associated with the art object. The
		object must be of type AIArtType::kTextFrameArt. If the story has no
		UID and create is TRUE then a machine generated UID will be assigned
		to the story. */
	AIAPI AIErr (*GetStoryUID) (AIArtHandle art, AIBoolean create, AIUIDRef* ruid);
	/** Set the unique id of the story associated with the art object. The
		object must be of type AIArtType::kTextFrameArt. */
	AIAPI AIErr (*SetStoryUID) ( AIArtHandle art, AIUIDRef uid );
	/** Transfer the id from the story of the source object to the story of
		the destination object. The id of the source story is set to null. */
	AIAPI AIErr (*TransferStoryUID) ( AIArtHandle srcart, AIArtHandle dstart );
	/** Get a new UIDREF for the story associated with the art object. The
		art object must be of type AIArtType::kTextFrameArt. When the UID
		reference is passed to GetReferencedArt() it will return the first
		frame of the story no matter which frame was used to create the
		reference. */
	AIAPI AIErr (*NewStoryUIDREF) ( AIArtHandle art, AIUIDREFRef* uidref );

	/** Transfer the unique id of the story associated with the text frame
		to the art object. Any type of art object may be supplied including
		a text frame. After the transfer the story will no longer have an id. */
	AIAPI AIErr (*TransferStoryUIDToArt) ( AIArtHandle frame, AIArtHandle art );
	/** Transfer the unique id of the art object to the story associated with
		the text frame. Any type of art object can be supplied including a
		text frame. After the transfer the story will no longer have an id. */
	AIAPI AIErr (*TransferArtUIDToStory) ( AIArtHandle art, AIArtHandle frame );

	/* New for AI 12 */

	/** Search for the target AIUIDRef in the current copy context.  Return the AIUIDRef that it 
	    maps to on the original object. 
		@param target the AIUIDRef to search for.
		@param equiv return value for the equivalent AIUIDRef.
		@return error kUIDNotFound if the UID is not found in the current copy context. */
	AIAPI AIErr (*FindEquivUID) ( AIUIDRef target, AIUIDRef& equiv );


} AIUIDUtilsSuite;

#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
