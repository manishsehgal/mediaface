#ifndef __AILayerList__
#define __AILayerList__

/*
 *        Name:	AILayerList.h
 *   $Revision: 2 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Layer List Suite.
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __SPPlugins__
#include "SPPlugs.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AILayerList.h */

/*******************************************************************************
 **
 **	Suite name and version
 **
 **/

#define kAILayerListSuite				"AI Layer List"
#define kAILayerListSuiteVersion2		AIAPI_VERSION(2)
#define kAILayerListSuiteVersion		kAILayerListSuiteVersion2
#define kAILayerListVersion				kAILayerListSuiteVersion


/*******************************************************************************
 **
 **	Types
 **
 **/


/** An opaque reference to a layer list. */
typedef struct LayerList*			AILayerList;


/** @ingroup Callers
	Caller id for layer list messages. */
#define kCallerAILayerList				"AI Layer List"

/** @ingroup Selectors
	The push layer list message is sent to the plugin owning the layer
	list when it is pushed. It provides the plugin with an opportunity to
	set up the layer list. Within the scope of the push message both the
	previous layer list and the newly pushed list may be modified. On
	return the previous layer list becomes unmodifiable.
	
	The data of the message is an AILayerListMessage. The data
	field of this message is the value passed to "Push". */
#define kSelectorAIPushLayerList		"AI Push Layer List"

/** @ingroup Selectors
	The pop layer list message is sent to the plugin owning the layer
	list when it is popped. It provides the plugin with an opportunity
	to clean up. Within the scope of the pop message both the previous
	layer list and the newly pushed one may be modified. The data of
	the message is an AILayerListMessage. */
#define kSelectorAIPopLayerList			"AI Pop Layer List"

/** @ingroup Selectors
	The delete layer list message is sent to the plugin owning the list
	to request that it dispose of any data it allocated on push. The
	data of the message is an AILayerListMessage. */
#define kSelectorAIDeleteLayerList		"AI Delete Layer List"

/** The message data for the various layer list messages. */
typedef struct {
	SPMessageData		d;
	AILayerList			list;
	void*				data;
} AILayerListMessage;


/** @ingroup Notifiers
	The following notifier is sent whenever a layer list is either pushed or
	popped. */
#define kAILayerListChangedNotifier	"AI Edit Layer List Changed Notifier"


/** The layer list mode controls how the contents of a layer list are
	rendered. It contains two independent controls for the rendering
	that are packed into a single integer by selecting one value from
	each set of control values and oring them together. The controls are
	defined by #AILayerListModeRenderingOptions and #AILayerListPreviewOptions.
	#AILayerListModeValues defines pre-canned combinations of these controls. */
typedef long AILayerListMode;

/** Bits 0-7 control what gets rendered when the layer list is drawn. */
enum AILayerListModeRenderingOptions {
	/** Mask value to isolate the rendering option bits from the AILayerListMode. */
	kAILayerListRenderMask			= (0x000000FFL),
	/** Render only the selection highlighting on the layer list. */
	kAILayerListRenderSelection		= (1L<<0),
	/** Render both the selection highlighting and the previews of the objects
		on the layer list. */
	kAILayerListRenderPreview		= (1L<<1)
};

/** Bits 8-11 define how the previews of objects on the layer list are drawn
	if they are drawn at all. These values are relevant only if
	AILayerListModeRenderingOptions::kAILayerListRenderPreview is specified. */
enum AILayerListModePreviewOptions {
	/** Mask value to isolate the rendering option bits from the AILayerListMode. */
	kAILayerListPreviewMask			= (0x00000F00L),
	/** Draw a full color preview. */
	kAILayerListPreviewColor		= (0L<<8),
	/** Draw a grayscale preview. */
	kAILayerListPreviewGray			= (1L<<8)
};

/** Pre-defined combinations of #AILayerListModeRenderingOptions and
	#AILayerListModePreviewOptions. */
enum AILayerListModeValues {
	/** Invisible mode draws neither selection highlighting nor the preview
		of objects. */
	kAILayerListInvisibleMode		= 0,
	/** Selection mode only draws the selection highlighting of selected
		objects in the layer list. */
	kAILayerListSelectionMode		= kAILayerListRenderSelection,
	/** Preview mode draws both selection highlighting and preview of the
		objects. */
	kAILayerListPreviewMode			= (kAILayerListRenderSelection|kAILayerListRenderPreview),
	/** Grayscale mode draws both the selection highlighting and a grayscale
		preview of the objects. */
	kAILayerListGrayscaleMode		= (kAILayerListRenderSelection|kAILayerListRenderPreview|kAILayerListPreviewGray)
};


/** The editability modes control the types of editing operations allowed
	for a layer list. The values are defined by #AILayerListEditabilityModeValues. */
typedef long AILayerListEditabilityMode;

/** The various layer list editability modes. New for AI10.
	
	By default, pushing a new layer list will set the editability mode to 
	kAIRegularEditabilityMode, allowing any and all types of edits to occur 
	in this layer list.
	
	kAINoNewLayers forces the layers palette to disable the new layer and new
	sublayer functionality. (Transparency mask layer lists use this mode.)
*/
enum AILayerListEditabilityModeValues {
	kAIRegularEditabilityMode		= 0,
	kAINoNewLayersMode				= (1L<<0)
};


/* Standard layer list tags */
#define kAIDocumentLayerList			"AI Document Layer List"
#define kAITransparencyMaskLayerList	"AI Transparency Mask Layer List"


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** 
	The artwork in an Illustrator document is contained within layers. There is a list
	of layers that contains the artwork that comprises the document. It is saved when
	the document is saved. This is called the document layer list.

	Most of the time when a document is open and being edited the only list of layers
	is the document layer list. However, it is possible to create new layer lists.
	These layer lists are maintained in a stack. The bottom most list on the stack is
	always the document layer list. The top most list on the stack is called the current
	layer list. The current layer list contains the artwork that the user is currently
	editing. Editing operations do not normally affect objects in layer lists below
	the current list.
	
	Some plugin APIs need to know which layer list they should operate on. For example,
	the AILayerSuite contains APIs for accessing the layers in a list sequentially but
	the APIs do not take a layer list as a parameter. Similarly the AIMatchingArtSuite
	contains APIs to find art satisfying some criteria that do not take a layer list
	as a parameter. In general all such APIs operate on the current layer list. This
	supports the notion that the current layer list is the one being edited.

	Each layer list has a display mode and an editability mode. The display mode affects
	how the contents of the layer list are drawn. The editability mode affects the
	editing operations allowed on the list.

	Layer lists are a general mechanism that can be used for many different purposes.
	Sometimes plugins need to know the purpose of a particular layer list. For this
	reason each layer list is given a tag when it is pushed onto the stack. The tag is
	a string identifying the purpose of the list. Illustrator defines the following
	tags for its layer lists:

	- #kAIDocumentLayerList
	- #kAITransparencyMaskLayerList

	There is one notifier associated with the layer lists:

	- #kAILayerListChangedNotifier
*/
typedef struct {

	/** Push a new layer list. The plugin identified by the 'self' message will receive
		a #kSelectorAIPushLayerList before the Push() call returns. This message gives
		the plugin an opportunity to initialize the list. The data parameter can be
		any data that the plugin wants to associate with the layer list. It is passed
		back to the plugin via the AILayerListMessage::data field when a message is
		sent to the plugin.

		When the layer list is popped from the stack the plugin that pushed it will
		receive a #kSelectorAIPopLayerList message. This gives the plugin an opportunity
		to perform any actions associated with the pop even though it may not have
		initiated the pop.
		
		The memory needed for the layer list is not actually disposed until it is no
		longer needed for undo/redo. Since the Push() and Pop() operations can be
		undone and redone this means that the plugin's private data associated with
		the list must be preserved beyond the pop operation. The plugin that pushed
		the layer list will receive a #kSelectorAIDeleteLayerList message when the
		data can be safely deleted.
	*/
	AIAPI AIErr (*Push) (SPPluginRef self, char* tag, void* data,
			AILayerListMode mode, AILayerList* list);

	/** Pop the current layer list from the top of the layer list stack. The plugin which
		pushed the layer list is called with ##kSelectorAIPopLayerList message. See Push()
		for more information. */
	AIAPI AIErr (*Pop) (void);

	/** Gets both the layer list and the layer on which the art object resides if any.
		NULL may be passed for either of the output parameters if the information
		is not required. */
	AIAPI AIErr (*GetLayerOfArt) (AIArtHandle art, AILayerList* list, AILayerHandle* layer);

	/** Count the number of layer lists on the stack. */
	AIAPI AIErr (*Count) (long* count);
	/** Get the layer list on the top of the stack (current layer list). */
	AIAPI AIErr (*GetFirst) (AILayerList* list);
	/** Get the layer list on the bottom of the stack (document layer list). */
	AIAPI AIErr (*GetLast) (AILayerList* list);
	/** Get the next lower layer in the stack. */
	AIAPI AIErr (*GetNext) (AILayerList list, AILayerList* next);

	/** Count the number of layers in a layer list. */
	AIAPI AIErr (*CountLayers) (AILayerList list, long* count);
	/** Get the top most (in stacking order) layer in the list. */
	AIAPI AIErr (*GetFirstLayer) (AILayerList list, AILayerHandle* layer);
	/** Get the bottom most (in stacking order) layer in the list. */
	AIAPI AIErr (*GetLastLayer) (AILayerList list, AILayerHandle* layer);
	/** Get the next lower layer in stacking order to the given layer in the list. */
	AIAPI AIErr (*GetNextLayer) (AILayerList list, AILayerHandle layer, AILayerHandle* next);
	/** Get the next higher layer in stacking order to the given layer in the list. */
	AIAPI AIErr (*GetPrevLayer) (AILayerList list, AILayerHandle layer, AILayerHandle* prev);

	/** Set the display mode to be used when rendering the layer list. */
	AIAPI AIErr (*SetDisplayMode) (AILayerList list, AILayerListMode mode);
	/** Get the display mode to be used when rendering the layer list. */
	AIAPI AILayerListMode (*GetDisplayMode) (AILayerList list);

	/** Get the tag specified when the layer list was pushed. The tag identifies
		the purpose of the list. For example the document layer list has the
		#kAIDocumentLayerList tag. */
	AIAPI const char* (*GetTag) (AILayerList list);
	
	/** Sets the editability mode of the layer list. Modifying the editability mode is
		best done right after initializing the layer list while processing the
		#kSelectorAIPushLayerList message. Plugins are not allowed to modify the
		editability mode of the document layer list. */
	AIAPI AIErr (*SetEditabilityMode) ( AILayerList list, AILayerListEditabilityMode mode );
	/** Gets the editability mode of the layer list. */
	AIAPI AILayerListEditabilityMode (*GetEditabilityMode) (AILayerList list);
	
} AILayerListSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
