#ifndef __AIRuntime__
#define __AIRuntime__

/*
 *        Name:	AIRuntime.h
 *   $Revision: 11 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator Runtime Environment.
 *
 * Copyright (c) 1986-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AINameSpace__
#include "AINameSpace.h"
#endif

#ifndef __AIStringPool__
#include "AIStringPool.h"
#endif

#ifndef __AIPlugin__
#include "AIPlugin.h"
#endif

#ifndef __AIUnicodeString__
#include "AIUnicodeString.h"
#endif //__AIUnicodeString__

#ifndef __AIFont__
#include "AIFont.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIRuntime.h */

/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIRuntimeSuite			"AI Runtime Suite"
#define kAIRuntimeSuiteVersion8	AIAPI_VERSION(8)
#define kAIRuntimeSuiteVersion	kAIRuntimeSuiteVersion8
#define kAIRuntimeVersion		kAIRuntimeSuiteVersion

/** @ingroup Notifiers
	Your plug�in can subscribe to this notifier when it receives the startup
	selector. You will receive the notification only once, when the application
	has completed its startup process. This provides an opportunity to perform
	addtional set up operations such as restoring plug�in window positions and
	visibility or operations that depend on suites unavailable at an earlier time.
*/
#define kAIApplicationStartedNotifier		"AI Application Started Notifier"
/** @ingroup Notifiers */
#define kAIApplicationShutdownNotifier		"AI Application Shutdown Notifier"

/** Operating system versions. */
enum AISystemOSVersion {
	kAIMacSystemNineOne		= 0x0910,
	kAIMacSystemTen			= 0x1000,
	kAIMacSystemOne			= 0x1010,
	kAIMacSystemTwo			= 0x1020,
	kAIMacSystemThree		= 0x1030,
	kAIMacSystemFour		= 0x1040
};

/*******************************************************************************
 **
 **	Types
 **
 **/

typedef const char *AIAppName;
typedef const char *AIAppVersion;
typedef const char *AIUserName;
typedef const char *AIUserOrganization;
typedef const char *AIUserSerialNumber;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** Use the Runtime Suite to obtain information about the name and version
	number of the host application. You can also access application instances of
	data structures, such as the name space and string pool.

	User information, such as the name, organization, and serial number is also
	available.

	The following notifiers are associated with the runtime suite:

	- #kAIApplicationStartedNotifier
	- #kAIApplicationShutdownNotifier

	The host application has a string pool which any application can access.
	String references returned from the string pool should not be modified since
	they point to the C string data in the pool. If you need to modify it, copy the
	string to local storage. The strings returned by functions in the Runtime
	Suite belong to the application string pool. (See the AIStringPoolSuite for
	more details.)

	For the reasons outlined in the function descriptions below, other hosts
	implementing the Adobe plug�in architecture are strongly encouraged to
	return their application�s name and version from the corresponding callback
	function. This will enable correct identification of the host by a plug�in
	without resorting to platform specific function calls.
*/
struct AIRuntimeSuite {

	/** You can use the application name to verify that your plug�in is running in a
		supported host application. You may also use the name and serial number to
		key your plug�in to an installed application. You should not modify the
		string returned by GetAppName. */
	AIAPI AIErr (*GetAppName) ( AIAppName *name );
	/** If your plug�in needs the location on disk of the application�perhaps to
		access a support file�use this function. */
	AIAPI AIErr (*GetAppFileSpecification) ( ai::FilePath &file );

	/** When Illustrator is first installed it asks for a user name, organization, and
		serial number. This function returns a pointer to the registered user's name
		(a C string). You should not modify the string returned by GetUserName() */
	AIAPI AIErr (*GetUserName) ( AIUserName *name );
	/** When Illustrator is first installed it asks for a user name, organization, and
		serial number. This function returns a pointer to the registered user's organization
		(a C string). You should not modify the string returned by GetUserOrganization(). */
	AIAPI AIErr (*GetUserOrganization) ( AIUserOrganization *organization );
	/** When Illustrator is first installed it asks for a user name, organization, and
		serial number. This function returns a pointer to the registered user's application
		serial number (as a C string). You should not modify the string
		returned by GetUserSerialNumber(). */
	AIAPI AIErr (*GetUserSerialNumber) ( AIUserSerialNumber *serialNumber );

	/** While a plug�in can create it's own name space in which it can store arbitrary
		data, it can also access the application's name space. Among other
		things, the application keeps its preferences here. Your plug�in should probably
		not modify the application's existing name space data directly, but it
		can create new data within it for some reason. */
	AIAPI AIErr (*GetAppNameSpace) ( AINameSpace **space );
	/** While a plug�in can create it's own string pool in which it can store and
		compare strings, it can also access the application's pool. The runtime data
		for a host is actually stored in this string pool and string pointers returned
		by the runtime suite functions are from the string pool. */
	AIAPI AIErr (*GetAppStringPool) ( AIStringPool **pool );

	/**	 GetAppFreeMem will always return the amount of physical RAM on the machine.
		 If really doesn't make a lot of sense on virtual memory systems.
		 Most clients calling this are trying to get an upper bound on how much
		 memory they can use. The amount of physical RAM is as good a value as any. */
	AIAPI ASInt32 (*GetAppFreeMem) ( void );

	/** Show the about box. */
	AIAPI AIErr (*ShowAppAboutBox) ( void );
	
	/** Get the version of the OS the application is running on. See #AISystemOSVersion. */
	AIAPI ASInt32 (*GetSystemVersion) ( void );
	/** True if the OS is Mac OS X. */
	AIAPI AIBoolean (*GetIsSystemMacOSX) ( void );
	
	/** Gets X from the version number where the version number is defined as X.Y.Z. */
	AIAPI ASInt32 (*GetAppMajorVersion) ( void );
	/** Gets Y from the version number where the version number is defined as X.Y.Z. */
	AIAPI ASInt32 (*GetAppMinorVersion) ( void );
	/** Gets Z from the version number where the version number is defined as X.Y.Z. */
	AIAPI ASInt32 (*GetAppRevisionVersion) ( void );
	
	/** Get the App Name as Unicode */
	AIAPI AIErr (*GetAppNameUS) ( ai::UnicodeString& name );
	/** Get the user name as Unicode */
	AIAPI AIErr (*GetUserNameUS) ( ai::UnicodeString& name );
	/** Get the Organisation name as Unicode */
	AIAPI AIErr (*GetUserOrganizationUS) ( ai::UnicodeString& organization );
	/** Get a default script **/
	AIAPI AIErr (*GetDefaultScript) ( AIFaceScript *script);
	/** True if this is an Honesty build */
	AIAPI AIBoolean (*GetIsHonestyBuild) ( void );
	/** True if this is a Try and Die build */
	AIAPI AIBoolean (*GetIsProductTryAndDie) ( void );
};


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
