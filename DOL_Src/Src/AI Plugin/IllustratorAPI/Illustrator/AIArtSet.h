#ifndef __AIArtSet__
#define __AIArtSet__

/*
 *        Name:	AIArtSet.h
 *   $Revision: 6 $
 *      Author:
 *        Date:
 *     Purpose:	Adobe Illustrator 9.0 Art Set Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AILayer__
#include "AILayer.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIArtSet.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIArtSetSuite			"AI Art Set Suite"
#define kAIArtSetSuiteVersion	AIAPI_VERSION(6)
#define kAIArtSetVersion		kAIArtSetSuiteVersion


/*******************************************************************************
 **
 **	Types
 **
 **/

/** An AIArtSpec is a filter for matching art objects that posess specific
	properties. An array of these is passed to AIArtSetSuite::MatchingArtSet()
	to specify the art objects that should be returned.

	The type field identifies the types of art objects that match the specification.
	See #AIArtType for the art object types. The special type kAnyArt can be
	used to match any kind of art object.

	The whichAttr and attr fields allow for filtering of art objects based on their
	user attributes. See #AIArtUserAttr for the attribute values. whichAttr specifies
	the collection of attributes to be considered when filtering objects. attr
	specifies the values those attributes must have to match an object. For
	example specifying kSelected for whichAttr and attr will match only art objects
	that are selected.
	
	There are a number of values in AIArtUserAttr that are not art attributes
	but instead specify additional options to the matching process. These
	options should be specified in the whichAttr field and only need to be
	specified in one of the art specifications. */
typedef struct {
	short type;
	long whichAttr, attr;
} AIArtSpec;

/** Opaque type for an ordered list of art object handles. See AIArtSetSuite. */
typedef struct _t_AIArt *AIArtSet;

/*******************************************************************************
 **
 **	Suite
 **
 **/

/** An art set is an ordered list of art object handles. An art object handle should
	appear at most once in the list. This is enforced by most but not all of the
	APIs for manipulating the set. */

typedef struct {

	/** Create an empty art set. */
	AIAPI AIErr (*NewArtSet) ( AIArtSet *artSet );
	/** Dispose of an art set. As a side effect the artSet pointer is set to NULL. Note
		that this function only disposes the art set, it does not dispose any artwork
		which is referenced by the set. */
	AIAPI AIErr (*DisposeArtSet) ( AIArtSet *artSet );
	/** Returns the number of art handles stored in the set. */
	AIAPI AIErr (*CountArtSet) ( AIArtSet artSet, long *count );

	/** Returns the art handle stored at the given index in the set. The index is
		zero based. An error is returned if the index is out of range. Note that
		ReplaceArtInArtSet() can put NULL entries into an art set. This is the
		most efficient way to access elements of an art set. Access speed is O(1). */
	AIAPI AIErr (*IndexArtSet) ( AIArtSet artSet, long index, AIArtHandle *art );

	/** Set the contents of the art set to be the list of art handles specified by
		the array. The previous contents of the art set if any are lost. It is the
		caller's responsibility to ensure that the array does not contain duplicates. */
	AIAPI AIErr (*ArrayArtSet) ( AIArtSet artSet, AIArtHandle *artArray, long count );
	/** Set the contents of the art set to be a list of the selected art objects. The
		previous contents of the art set if any are lost. This is the same as calling
		MatchingArtSet() with a request for { kAnyArt, kArtSelected, kArtSelected } */
	AIAPI AIErr (*SelectedArtSet) ( AIArtSet artSet );
	/** Set the contents of the art set to be a list of the objects in the document
		that match the criteria of the array of art specifications. The art objects
		are put into the set in the order they are encountered by a pre-order
		traversal of the document tree. An art object is inserted into the array
		if it matches any of the art specifications. See AIArtSpec for a detailed
		description of an art specification. */
	AIAPI AIErr (*MatchingArtSet) ( AIArtSpec *specs, short numSpecs, AIArtSet artSet );
	/** Set the contents of the art set to be a list of the objects in the layer. */
	AIAPI AIErr (*LayerArtSet) ( AILayerHandle layer, AIArtSet artSet );

	/** Deprecated. This function will fill the art set dst with all artwork in the
		document that is not in the art set src. This function has not been kept up
		to date with changes to the object model. */
	AIAPI AIErr (*NotArtSet) ( AIArtSet src, AIArtSet dst );
	/** Fills an art set with all art objects contained in 2 art sets without duplicating
		common art objects. */
	AIAPI AIErr (*UnionArtSet) ( AIArtSet src0, AIArtSet src1, AIArtSet dst );
	/** Fills an art set with all art objects that are common to 2 art sets. */
	AIAPI AIErr (*IntersectArtSet) ( AIArtSet src0, AIArtSet src1, AIArtSet dst );

	/** Given an art handle prevArt in an art set artSet, this function returns a
		pointer to the next art in the art set as nextArt. If prevArt is NULL
		returns the first element of the set. If prevArt is not in the set or there
		is no next element returns NULL. Unlike IndexArtSet this function skips
		NULL entries in the set. Access speed is O(n). Use IndexArtSet if possible. */
	AIAPI AIErr (*NextInArtSet) ( AIArtSet artSet, AIArtHandle prevArt, AIArtHandle *nextArt );

	/* AI 9 additions. */

	/** Appends the art handle to the art set if it is not already there. */
	AIAPI AIErr (*AddArtToArtSet) (AIArtSet artSet, AIArtHandle art);
	/** Removes all occurrences of the art handle from the art set. */
	AIAPI AIErr (*RemoveArtFromArtSet) (AIArtSet artSet, AIArtHandle art);

	/* AI 9.01 addition */

	/** Replaces the first occurrence of oldArt by newArt. If oldArt is not found
		then newArt is appended. newArt may be NULL in which case it will still
		replace oldArt or be appended. */
	AIAPI AIErr (*ReplaceArtInArtSet)(AIArtSet artSet, AIArtHandle oldArt, AIArtHandle newArt);

} AIArtSetSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
