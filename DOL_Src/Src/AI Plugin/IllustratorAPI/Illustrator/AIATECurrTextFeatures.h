/* -------------------------------------------------------------------------------

                                    ADOBE CONFIDENTIAL
                                _________________________

    Copyright 2002 Adobe Systems Incorporated
    All Rights Reserved.

    NOTICE:  All information contained herein is, and remains
    the property of Adobe Systems Incorporated and its suppliers,
    if any.  The intellectual and technical concepts  contained
    herein are proprietary to Adobe Systems Incorporated and its
    suppliers and may be covered by U.S. and Foreign Patents,
    patents in process, and are protected by trade secret or copyright law.
    Dissemination of this information or reproduction of this material
    is strictly forbidden unless prior written permission is obtained
    from Adobe Systems Incorporated.

	REQUIREMENT: This won't compile without a C++ compiler!

 ----------------------------------------------------------------------------------

	File:	AIATECurrTextFeatures.h

	Author:	Carlos M. Icaza

	Notes:

 ---------------------------------------------------------------------------------- */

#ifndef __ATETextCurrentStyle__
#define __ATETextCurrentStyle__


#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __ATESuites__
#include "ATESuites.h"
#endif

#ifndef __ATETypesDef__
#include "ATETypesDef.h"
#endif


extern "C" {

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIATECurrTextFeatures.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIATECurrentTextFeaturesSuite			"AI ATE Current Text Features Suite"
#define kAIATECurrentTextFeaturesSuiteVersion1	AIAPI_VERSION(1)
#define kAIATECurrentTextFeaturesSuiteVersion	kAIATECurrentTextFeaturesSuiteVersion1
#define kAIATECurrentTextFeaturesVersion		kAIATECurrentTextFeaturesSuiteVersion



/*******************************************************************************
 **
 **	Suite
 **
 **/

/** When a new text object is created its contents must be supplied with
	style attributes. The current paragraph and character styles together
	with their respective overrides define these. Note that these do not
	represent the attributes of the current selection. The character and
	paragraph palettes usually display the attributes of the curent
	selection. The attributes they display can be obtained with
	GetCurrentCharFeature() and GetCurrentParaFeature().
 */
typedef struct {

	/** Returns the flattened attribute chain, i.e. what is displayed in the
		character attribute palette. */
	AIAPI AIErr (*GetCurrentCharFeature) ( ATE::CharFeaturesRef *charFeatures );
	/** Returns the flattened attribute chain, i.e. what is displayed in the
		paragraph attribute palette. */
	AIAPI AIErr (*GetCurrentParaFeature) ( ATE::ParaFeaturesRef *paraFeatures );

	AIAPI AIErr (*SetCurrentCharFeature) ( ATE::CharFeaturesRef charFeatures );
	AIAPI AIErr (*SetCurrentParaFeature) ( ATE::ParaFeaturesRef paraFeatures );

	/** Gets the named style that would be applied to new text objects.  This is
		by default set to the normal style in new documents.  If no text selection
		is present, overrides are cleared. */
	AIAPI AIErr (*GetCurrentCharStyle) ( ATE::CharStyleRef* pStyle );
	/** Gets the named style that would be applied to new text objects.  This is
		by default set to the normal style in new documents.  If no text selection
		is present, overrides are cleared. */
	AIAPI AIErr (*GetCurrentParaStyle) ( ATE::ParaStyleRef* pStyle );

	/** Sets the named style that would be applied to new text objects. */
	AIAPI AIErr (*SetCurrentCharStyle) ( ATE::CharStyleRef pStyle );
	/** Sets the named style that would be applied to new text objects. */
	AIAPI AIErr (*SetCurrentParaStyle) ( ATE::ParaStyleRef pStyle );

	/** When a plugin modifies the current style or feature, they should call
		this method to ensure that the UI reflects those changes. */
	AIAPI AIErr (*Invalidate) ();

	/** Returns the current local character override attributes that would
		be applied to new text objects. */
	AIAPI AIErr (*GetCurrentCharOverrides) ( ATE::CharFeaturesRef *pFeatures );
	/** Returns the current local paragraph override attributes that would
		be applied to new text objects. */
	AIAPI AIErr (*GetCurrentParaOverrides) ( ATE::ParaFeaturesRef *pFeatures );

	/** Sets the current local character override attributes.  Passing in a 
		features where all attributes are unassigned clears the current
		overrides. */
	AIAPI AIErr (*SetCurrentCharOverrides) ( ATE::CharFeaturesRef pFeatures );
	/** Sets the current local paragraph override attributes.  Passing in a 
		features where all attributes are unassigned clears the current
		overrides. */
	AIAPI AIErr (*SetCurrentParaOverrides) ( ATE::ParaFeaturesRef pFeatures );

} AIATECurrentTextFeaturesSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

}


#endif
