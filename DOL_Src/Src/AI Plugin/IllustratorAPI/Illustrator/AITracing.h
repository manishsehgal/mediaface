#ifndef __AITracing__
#define __AITracing__

/*
 *        Name:	AITracing.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 12 Tracing Suite.
 *
 * Copyright (c) 1986-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIRaster__
#include "AIRaster.h"
#endif

#ifndef __AIDictionary__
#include "AIDictionary.h"
#endif

#ifndef __AIImageOptimization__
#include "AIImageOptimization.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AITracing.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAITracingSuite				"AI Tracing Suite"
#define kAITracingSuiteVersion1		AIAPI_VERSION(1)
#define kAITracingSuiteVersion		kAITracingSuiteVersion1
#define kAITracingVersion			kAITracingSuiteVersion


#define kAITracingIPSuite			"AI Tracing IP Suite"
#define kAITracingIPSuiteVersion1	AIAPI_VERSION(1)
#define kAITracingIPSuiteVersion	kAITracingIPSuiteVersion1
#define kAITracingIPVersion			kAITracingIPSuiteVersion


/*******************************************************************************
 * kAITracingUpdateNotifier is sent after an Update call is made and completed.
 * notifyData is an AIArtHandle to the tracing object being updated.
 */
#define kAITracingUpdateNotifier			"AI Tracing Update Notifier"
#define kAITracingPresetNameChangedNotifier	"AI Tracing Preset Name Changed Notifier"



/*******************************************************************************
 **
 ** Catalog Keys - Presets
 **
 **/
#define kAITracingPresetsCatalogName 		"Adobe Tracing Presets"

/*******************************************************************************
 **
 ** Dictionary Keys - Options
 **
 **/

#define kTracingGlobalPresetKey				"-adobe/tracing/options/global/preset"	// string, the document dictionary key used to specify the MRU preset.  if the key is missing or NULL then "Custom" global preset is assumed.


#define kTracingOptionsKey					"adobe/tracing/options"					// dictionary, the main options dictionary.
#define kTracingOptionsOldKey				"-adobe/tracing/options/old"			// dictionary, the old options dictionary.
#define kTracingCacheKey					"-adobe/tracing/cache"					// dictionary, the artwork cache dictionary.
#define kTracingStatisticsKey				"adobe/tracing/statistics"				// dictionary, the tracing statistics: path count, color count, ppi, etc..

// options contents
#define kTracingPresetKey					"adobe/tracing/preset"					// string
#define kTracingResampleKey					"adobe/tracing/resample"				// bool
#define kTracingResampleResolutionKey		"adobe/tracing/resolution"				// float
#define kTracingModeKey						"adobe/tracing/mode"					// enum/long AITracingMode
#define kTracingBlurKey						"adobe/tracing/ip/blur"					// float
#define kTracingThresholdKey				"adobe/tracing/ip/threshold"			// long
#define kTracingPaletteKey					"adobe/tracing/ip/palette"				// string
#define kTracingEmbeddedPaletteKey			"adobe/tracing/ip/embedded/palette"		// dictionary (hidden, use EmbedSwatches instead)
#define kTracingCustomColorsKey				"adobe/tracing/ip/custom/colors"		// dictionary (hidden, do not access, housekeeping of referenced custom colors)
#define kTracingSwatchNameKey				"adobe/tracing/swatch/name"				// string
#define kTracingSwatchColorKey				"adobe/tracing/swatch/color"			// fill style (used to store embedded custom colors (as fill styles))
#define kTracingMaxColorsKey				"adobe/tracing/ip/maxcolors"			// long
#define kTracingFillsKey					"adobe/tracing/tracing/fills"			// bool
#define kTracingStrokesKey					"adobe/tracing/tracing/strokes"			// bool
#define kTracingMaxStrokeWeightKey			"adobe/tracing/tracing/maxstrokeweight"	// float
#define kTracingMinStrokeLengthKey			"adobe/tracing/tracing/minstrokelength"	// float
#define kTracingPathTightnessKey			"adobe/tracing/tracing/pathtightness"	// float
#define kTracingCornerAngleKey				"adobe/tracing/tracing/cornerangle"		// float
#define kTracingMinimumAreaKey				"adobe/tracing/tracing/minimumarea"		// long
#define kTracingVisualizeVectorKey			"adobe/tracing/visualize/vector"		// enum/long AITracingVisualizeVectorType
#define kTracingVisualizeRasterKey			"adobe/tracing/visualize/raster"		// enum/long AITracingVisualizeRasterType
#define kTracingLivePaintKey				"adobe/tracing/output/livepaint"		// bool
#define kTracingOutputToSwatchesKey			"adobe/tracing/output/outputtoswatches"	// bool


// cache contents
#define kTracingTracingArtKey				"adobe/tracing/tracing/art"				// art handle
#define kTracingPreprocessedImageKey		"adobe/tracing/preprocessed/image"		// raster art handle
#define kTracingSourceArtChangedKey			"adobe/tracing/src/changed"				// bool (for optimization of art regeneration)
#define kTracingScratchGroupKey				"adobe/tracing/scratch/group"			// group art handle (private use)
#define kTracingAdjustedRasterMatrixKey		"adobe/tracing/raster/matrix"			// matrix
#define kTracingAdjustedRasterChecksumKey	"adobe/tracing/raster/checksum"			// integer checksum

// statistics contents
#define kTracingPathCountKey				"adobe/tracing/statistics/pathcount"	// long, number of paths generated
#define kTracingAnchorCountKey				"adobe/tracing/statistics/anchorcount"	// long, number of anchors generated
#define kTracingColorCountKey				"adobe/tracing/statistics/colorcount"	// long, number of colors used
#define kTracingAreaCountKey				"adobe/tracing/statistics/areacount"	// long, number of enclosed areas generated
#define kTracingSourceImagePPIKey			"adobe/tracing/statistics/sourceppi"	// float, the effective ppi of the source image


#define kTracingResampleDefault				(false)
#define kTracingResampleResolutionDynamic	(0.0f)		// secret resolution used to indicate use of "native" dynamically derived resolution.
#define kTracingResampleResolutionDefault	(72.0f)
#define kTracingResampleResolutionMinimum	(1.0f)
#define kTracingResampleResolutionMaximum	(600.0f)
#define kTracingResampleResolutionIncrement	(1.0f)
#define kTracingModeDefault					(2)			// kAITracingImageBitmap
#define kTracingBlurDefault					(0.0f)
#define kTracingBlurMinimum					(0.0f)
#define kTracingBlurMaximum					(20.0f)
#define kTracingBlurIncrement				(0.1f)
#define kTracingThresholdDefault			(128)
#define kTracingThresholdMinimum			(1)
#define kTracingThresholdMaximum			(255)
#define kTracingPaletteDefault				("")		// "" means Automatic
#define kTracingMaxColorsDefault			(6)
#define kTracingMaxColorsMinimum			(2)
#define kTracingMaxColorsMaximum			(256)
#define kTracingMaxColorsIncrement			(1)
#define kTracingFillsDefault				(true)
#define kTracingStrokesDefault				(false)
#define kTracingMaxStrokeWeightDefault		(10.0f)
#define kTracingMaxStrokeWeightMinimum		(0.01f)
#define kTracingMaxStrokeWeightMaximum		(100.0f)
#define kTracingMaxStrokeWeightIncrement	(1.0f)
#define kTracingMinStrokeLengthDefault		(20.0f)
#define kTracingMinStrokeLengthMinimum		(0.0f)
#define kTracingMinStrokeLengthMaximum		(200.0f)
#define kTracingMinStrokeLengthIncrement	(1.0f)
#define kTracingPathTightnessDefault		(2.0f)
#define kTracingPathTightnessMinimum		(0.0f)
#define kTracingPathTightnessMaximum		(10.0f)
#define kTracingPathTightnessIncrement		(0.1f)
#define kTracingMinimumAreaDefault			(10)
#define kTracingMinimumAreaMinimum			(0)
#define kTracingMinimumAreaMaximum			(3000)
#define kTracingMinimumAreaIncrement		(1)
#define kTracingVisualizeVectorDefault		(1)			// kAITracingVisualizeVectorArtwork
#define kTracingVisualizeRasterDefault		(0)			// kAITracingVisualizeRasterNone
#define kTracingLivePaintDefault			(false)
#define kTracingOutputToSwatchesDefault		(false)
#define kTracingROIInitialStateDefault		(true)
#define kTracingSourceArtChangedDefault		(true)		// private

#define kTracingCornerAngleDefault			(20.0f)
#define kTracingCornerAngleMinimum			(0.0f)
#define kTracingCornerAngleMaximum			(180.0f)


/*******************************************************************************
 **
 ** Types
 **
 **/

/** This is a reference to an opaque tracing state object. It is never dereferenced. */
typedef struct _t_AITracingState *AITracingStateHandle;

/** This is a reference to an opaque view options object. It is never dereferenced. */
typedef struct _t_AITracingViewOptions *AITracingViewOptionsHandle;



/** AITracingMode is an enumeration of the different color models the IP code can
	reduce to.
*/
typedef long AITracingMode;
enum AITracingModeValue 
{
	/** Color.  Either RGB or CMYK depending on source image.  */
	kAITracingModeColor,
	
	/** Gray. */
	kAITracingModeGray,
	
	/** Bitmap. */
	kAITracingModeBlackAndWhite
};



/** AITracingVisualizeVectorType is an enumeration of the different visualization modes for the vector
	portion of the tracing output.
*/
typedef long AITracingVisualizeVectorType;
enum AITracingVisualizeVectorValue 
{
	/** No Artwork.  */
	kAITracingVisualizeVectorArtworkNone,

	/** Artwork.  The full result, paths and fills, of the tracing.  */
	kAITracingVisualizeVectorArtwork,

	/** Paths.  A paths only version of the "Artwork" mode with black stroked paths.  */
	kAITracingVisualizeVectorPaths,

	/** Paths and Transparency.  A "transparent" version of the "Artwork" mode with black stroked paths overlayed.  */
	kAITracingVisualizeVectorPathsAndTransparency
};


/** AITracingVisualizeRasterType is an enumeration of the different visualization modes for the raster
	portion of the tracing output.
*/
typedef long AITracingVisualizeRasterType;
enum AITracingVisualizeRasterValue 
{
	/** None.  No raster is included in the visualization.  */
	kAITracingVisualizeRasterNone,

	/** Original.  The original raster input (before preprocessing).  */
	kAITracingVisualizeRasterOriginal,

	/** Preprocessed.  The preprocessed image.  */
	kAITracingVisualizeRasterPreprocessed,

	/** Transparency.  A "transparent" version of the "Original" mode.  */
	kAITracingVisualizeRasterTransparency
};



/*******************************************************************************
 **
 **	Suite Record
 **
 **/


/**
  * The AITracingSuite is intended to reveal the object model for the Tracing art type.
  * This Tracing is a plugin group where the main contents of the plugin group are:
  * 
  *   -# the source image.
  *   -# the posterized/preprocessed version of the source image.
  *   -# the tracing options used to trace the options.
  *   -# the internal tracing state kept around by the vectorization engine.
  *   -# the view options used to determine which elements of the plugin group to display during update.
  *
 */

typedef struct {

	/** Plugin Group creation. 
		Input "art" can be either a raster or a foreign object.
		Ouput "tracing" is a plugin group where the source art is the input "art" and the result group,
		once "Update" has occured, is the traced result of the input art.
	 */
	AIAPI AIErr (*CreateTracing) (short paintOrder, AIArtHandle prep, AIArtHandle art, AIArtHandle *tracing);
	
	/** Plugin Group identification. */
	AIAPI AIBoolean (*IsTracing) (AIArtHandle tracing);

	/** Source Image procedures. */
	AIAPI AIErr (*GetSourceImage) (AIArtHandle tracing, AIArtHandle *raster);

	/** Aquisition of the traced/result art. */
	AIAPI AIErr (*CopyTracingArt) (AIArtHandle tracing, short paintOrder, AIArtHandle prep, AIArtHandle *art, bool copyAsViewed);
	
	/** Tracing Option aquistition. 
		Setting options involes using dictionary entries and known keys.
		Note: the options dictionary is a counted object (AICountedObject.h).  Recommend using the IAIRef.h "Ref" class.
	 */
	AIAPI AIErr (*AcquireTracingOptions) (AIArtHandle tracing, AIDictionaryRef *options);

	/** Tracing Statistics aquistition. 
		Getting statistics involes using dictionary entries and known keys.  These statistics are intended to be read-only.
		Note: the statistics dictionary is a counted object (AICountedObject.h).  Recommend using the IAIRef.h "Ref" class.
	 */
	AIAPI AIErr (*AcquireTracingStatistics) (AIArtHandle tracing, AIDictionaryRef *options);

	/** Embedded Swatch Stuff. */
	
	/** Trigger the embedding of swatches.  Should be called after setting the options->kTracingPaletteKey
		value to something new.
	 */
	AIAPI AIErr (*EmbedSwatches) (AIArtHandle tracing);
	
	/** Update cause the vectorization engine to vectorize the preprocessed image based on the various
	  * input options.
	  */
	AIAPI AIErr (*Update) (AIArtHandle tracing);


	/** Get the native resolution (if any) of the input art.
		Returns 0 if no native resolution is available,
		Returns non-zero value, in dpi units, if strictly uniform scaling has been applied to the source art.
	 
		Input 'art' can be either a tracing object, a raster, or an FO (or anything else actually).
	 */
	AIAPI AIErr (*GetNativeResolution) (AIArtHandle art, AIReal *dpi);

	/** Get the effective resolution of the tracing object.
		Returns "native resolution" no native resolution is available,
		Returns the resampling dpi if resampling is turned on,
		Returns non-zero value, in dpi units, if strictly uniform scaling has been applied to the source art.
		Returns non-zero value, in dpi units, if strictly non-uniform scaling has been applied averaging the x/y dpi.
	 
		Input 'art' can be a tracing object, that's all.
	 */
	AIAPI AIErr (*GetEffectiveResolution) (AIArtHandle art, AIReal *dpi);

	AIAPI AIErr (*SetSourceArtChanged) (AIArtHandle tracing, AIBoolean changed);

} AITracingSuite;





/**
  * The AITracingIPSuite provides routines to "preprocess" images for use in the tracing feature.
  * "IP" stands for Image Processing.  The suite contains all image processing functions relating
  * to the pre-processing of the raster to ready it for vectorization by the vectorization engine.
  * The input image can be preprocessed in various ways (see below), and the image can either be
  * manipulated in-place or a new resulting image can be created.
  *
 */

typedef struct {

	/** Posterization. */
	AIAPI AIErr (*Posterize) (AIArtHandle image, AIColorPaletteHandle palette);

	/** Threshold. 0 <= level <= 255 */
	AIAPI AIErr (*Threshold) (AIArtHandle image, long level);
	
	/** Convert To Grayscale. */
	AIAPI AIErr (*ConvertToGray) (AIArtHandle image);
	

} AITracingIPSuite;



#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
