#ifndef __AITool__
#define __AITool__

/*
 *        Name:	AITool.h
 *   $Revision: 31 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Tool Suite.
 *
 * Copyright (c) 1986-1998 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AIEvent__
#include "AIEvent.h"
#endif

#ifndef __AIFixedMath__
#include "AIFixedMath.h"
#endif

#ifndef __AIPlugin__
#include "AIPlugin.h"
#endif

#ifndef __AIToolNames__
#include "AIToolNames.h"
#endif

#ifndef __ADMTypes__
#include "ADMTypes.h"
#endif

#ifndef __ASHelp__
#include "ASHelp.h"
#endif

#ifndef FLT_MAX
#include <float.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AITool.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIToolSuite			"AI Tool Suite"
#define kAIToolSuiteVersion6	AIAPI_VERSION(6)
#define kAIToolSuiteVersion9	AIAPI_VERSION(9)
#define kAIToolSuiteVersion		kAIToolSuiteVersion9
#define kAIToolVersion			kAIToolSuiteVersion

/** @ingroup Notifiers
	Sent whenever some property of a plugin tool is changed. For example
	its options, title, icon, tooltip etc. */
#define kAIUserToolChangedNotifier		"AI User Tool Changed Notifier"
/** @ingroup Notifiers
	Sent when a tool is selected. The data is the AIToolHandle. A reselection
	of an already selected tool sends both #kAIToolDeselectedNotifier
	and #kAIToolSelectedNotifier notifications but the tool itself receives
	just the #kSelectorAIReselectTool message. */
#define kAIToolSelectedNotifier			"AI Tool Selected Notifier"
/** @ingroup Notifiers
	Sent when a tool is deselected. The data is the AIToolHandle. */
#define kAIToolDeselectedNotifier		"AI Tool Deselected Notifier"
/** @ingroup Notifiers
	Sent to notify tools that they should clear any editing state that
	they maintain. For example, the pen tool remembers the path to which
	it is adding points. This is sent as a notifier since tools other
	than the current tool may maintain editing state. For example,
	selecting the zoom tool does not clear the editing state of the
	pen tool. */
#define kAIToolClearStateNotifier		"AI Tool Clear State Notifier"
/** @ingroup Notifiers
	Sent whenever the title string of a tool is changed e.g by
	AIToolSuite::SetToolTitle() */
#define kAIToolTitleChangedNotifier		"AI Tool Title Changed Notifier"
/** @ingroup Notifiers
	Sent whenever a tool's icon is changed e.g by AIToolSuite::SetToolIcon()
	or AIToolSuite::SetToolRolloverIcon() */
#define kAIToolIconChangedNotifier		"AI Tool Icon Changed Notifier"
/** @ingroup Notifiers
	Sent whenever a tool's tooltip is changed e.g by AIToolSuite::SetToolTip() */
#define kAIToolTooltipChangedNotifier	"AI Tool Tooltip Changed Notifier"
/** @ingroup Notifiers
	Sent whenever a tool's help id is changed e.g by AIToolSuite::SetToolHelpID() */
#define kAIToolHelpIDChangedNotifier	"AI Tool Help ID Changed Notifier"
/** @ingroup Notifiers
	Notifier that is sent when the current tool is changed to a different
	tool. */
#define kAIToolChangedNotifier			"AI Tool Changed Notifier"
/** @ingroup Notifiers */
#define kAIToolWindowChangedNotifier	"AI Tool Window Changed Notifier"
/** @ingroup Notifiers
	Sent when the mouse leaves a document window.
	See also #kAIToolResumeNotifier. */
#define kAIToolSuspendNotifier			"AI Tool Suspend Notifier"
/** @ingroup Notifiers
	Sent when the mouse enters a document window.
	See also #kAIToolSuspendNotifier. */
#define kAIToolResumeNotifier			"AI Tool Resume Notifier"

// Notifiers sent by built-in tools

/** @ingroup Notifiers
	Sent by the eyedropper when it is selected and dragged. The data is
	an AIEyedropperDragNotifyData specifying the event and whether the
	path style has changed as a result. */
#define kAIEyedropperDragNotifier		"AI Eyedropper Drag Notifier"



/** @ingroup Callers
	This is the tool caller. */
#define kCallerAITool					"AI Tool"

/** @ingroup Selectors
	This selector is sent to request that the plugin display its tool options dialog. It
	will most likely be received in response to a user double clicking a tool's icon in
	the tool palette, though other interfaces are possible. The message data is defined
	by AIToolMessage. */
#define kSelectorAIEditToolOptions		"AI Edit Options"
/** @ingroup Selectors
	This selector is sent when the tool is selected and as the mouse is moved over the
	artboard with the button up. It is an opportunity for the plug-in to set the its
	cursor based on location or some other factor. The message data is defined
	by AIToolMessage. */
#define kSelectorAITrackToolCursor		"AI Track Cursor"
/** @ingroup Selectors
	This selector is received when the user presses the mouse button down. The message
	data is defined by AIToolMessage. Among other things it contains the mouse location
	and modifier key information. */
#define kSelectorAIToolMouseDown		"AI Mouse Down"
/** @ingroup Selectors
	If the user moves the mouse with the button held down, the plug-in receives a series
	of these selectors. The message data is defined by AIToolMessage. Among other things
	it contains the mouse location and modifier key information. */
#define kSelectorAIToolMouseDrag		"AI Mouse Drag"
/** @ingroup Selectors
	The mouse up selector either follows the mouse down selector immediately or ends
	the mouse drag selectors. The message data is defined by AIToolMessage. Among other
	things it contains the mouse location and modifier key information. */
#define kSelectorAIToolMouseUp			"AI Mouse Up"
/** @ingroup Selectors
	This selector tells the plug-in that its tool has been selected. It is an opportunity
	for the plug-in to do any need initialization. The message data is defined by
	AIToolMessage. */
#define kSelectorAISelectTool			"AI Select"
/** @ingroup Selectors
	This selector tells the plug-in that a different tool is selected. It is an opportunity
	for the plugin to clean up any run-time variables. The message data is defined by
	AIToolMessage. */
#define kSelectorAIDeselectTool			"AI Deselect"
/** @ingroup Selectors
	The message data is defined by AIToolMessage. A reselection of an already selected
	tool sends both #kAIToolDeselectedNotifier and #kAIToolSelectedNotifier notifications
	but the tool itself receives just the #kSelectorAIReselectTool message. */
#define kSelectorAIReselectTool			"AI Reselect"


/** These are the options that can be set by all plug-in tools when the plug-in
	tool is installed with the AIToolSuite::AddTool() function or at a later time
	with the AIToolSuite::SetToolOptions() function.

	(Note that Brush Tools share these options, so if you add new ones, make sure they
	don't overlap with the brush tool options defined in AIBrushMgr.h. Currently they
	start at 1L<<16 and go up.)
*/
enum AIToolOptions {
	/** Controls whether the tool wants to receive or disable the track cursor selector,
		#kSelectorAIrackToolCursor. It is turned off by default, and a standard arrow
		cursor will be used for the tool. If the plug-in tool wants to control its
		appearance, it should turn this option on. */
	kToolWantsToTrackCursorOption		= (1L<<0),
	/** By default, if a tool gets to the edge of the Illustrator window, the window
		will scroll. If the disable autoscroll option is set, this behavior will not occur.
		If the tool uses API calls to manipulate artwork objects, the autoscroll
		behavior will probably be desirable. If the plug-in tool draws to the screen
		directly, the autoscroll behavior should be turned off. The built-in brush tool
		exhibits the non-autoscrolling behavior. */
	kToolDoesntWantAutoScrollOption		= (1L<<1),
	/** Useful if a tool is calculation intensive. Normally the tool processes a drag
		selector and returns with a high frequency, resulting in near real-time feedback.
		In the case of more intensive calculations during the drag selector, it is
		possible that the plug-in would miss drag notifications resulting in rougher
		tracking. By setting this option Illustrator will buffer the drag selectors and
		messages and send all of them to the tool plug-in. The effect may no longer be
		real-time, but it would have a smoother final output. */
	kToolWantsBufferedDraggingOption	= (1L<<2),
	/** Causes Illustrator to maintain the edit context when this tool is selected. With
		art objects, this means keeping all current points and handles selected. With text,
		this means keeping the insertion point in the current location. This option would be
		set for navigational tools. For example the zoom tool and scroll tool set this
		option. */
	kToolMaintainEditContextOption 		= (1L<<3),
	/** Tells Illustrator that the tool is a Text Tool. This option tells Illustrator to
		maintain the text edit context when a tool is selected if in addition the previous
		tool set the #kToolMaintainEditContextOption option. */
	kToolIsTextToolOption 				= (1L<<4)
};


/** Info variables. See AIToolSuite::SetInfoVars().  The associated value types for all
	are real, except for kInfoFontAndSize, kInfoTrackOrKern, kInfoText1-6, and kInfoLongText1-3,
	which are platform encoded strings passed as char*. 
	kInfoTextUnicode1-6 and kInfoLongText1-3 refer to the same info palette slots as
	kInfoText1-6 and kInfoLongText1-3 respectively, but the associated value type is 
	assumed to be an ai::UnicodeString* which is treated as const.*/
enum AIToolInfoVariable {
	kInfoPageX = 0,
	kInfoPageY,
	kInfoSizeX,
	kInfoSizeY,
	kInfoDistance,
	kInfoVectorAngle,
	kInfoScaleX,
	kInfoScaleY,
	kInfoZoom,
	kInfoCornerRoundness,
	kInfoRotAngle,
	kInfoShearAngle,
	kInfoFontAndSize,
	kInfoTrackOrKern,
	kInfoRawRotAngle,
	kInfoDocX,
	kInfoDocY,
	kInfoText1,
	kInfoText2,
	kInfoText3,
	kInfoText4,
	kInfoText5,
	kInfoText6,
	kInfoLongText3,
	kInfoNumVars,
	kInfoBlank,

	// new in AI 12
	kInfoTextUnicode1,
	kInfoTextUnicode2,
	kInfoTextUnicode3,
	kInfoTextUnicode4,
	kInfoTextUnicode5,
	kInfoTextUnicode6,
	kInfoLongTextUnicode1,
	kInfoLongTextUnicode2,
	kInfoLongTextUnicode3,

	// overlayed items.
	kInfoLongText1 = kInfoFontAndSize,
	kInfoLongText2 = kInfoTrackOrKern,

	/** Use this to terminate the infoVars list. */
	kInfoEndOfList = -1
};


#define kNoTool -2


typedef short AIToolType;

#define kPluginToolOffset 1000

#define kToolCantTrackCursorErr		'TLTK'

//ToolTime for GetToolNullEventInterval. This type is to be deprecated ASAP.

typedef double AIToolTime; //Time in Seconds

#define kToolTimeDurationSecond            ((AIToolTime)1.0)
#define kToolTimeDurationMillisecond       ((AIToolTime)(kToolTimeDurationSecond/1000))
#define kToolTimeDurationNoWait            ((AIToolTime)0.0)
#define kToolTimeDurationForever           ((AIToolTime)(-1.0))


/*******************************************************************************
 **
 ** Types
 **
 **/

/** This is a reference to a tool. It is never dereferenced. */
typedef struct _t_AIToolOpaque *AIToolHandle;

/** The information for adding a tool.

	The title is a C string that will be displayed to the user as the current tool.

	The tooltip is a C string which will be displayed to the user if tooltips are
	activiated.
	
	The icon is an ADMIconRef. See the AMDIconSuite documentation for an explaination
	of the ADMIconRef.
	
	The sameGroupAs and sameToolsetAs records determine where on Illustrator's Tool
	Palette the plug-in tool will be added. Groups are collections of toolsets.
	
	On Illustrator's tool palette, groups are separated by vertical space. Toolsets
	are collections of individual tools. When more than one tool is added to a
	toolset, the additional tools are hidden "behind" the frontmost tool and
	Illustrator will draw a triangle in the lower left corner of the icon. The
	toolset is revealed when the user clicks and holds the mouse on the topmost
	tool.

	When creating a new group or toolset, pass kNoTool to the sameGroupAs or
	sameToolsetAs record.

	For example, when initially creating a new Group use these parameters:

@code
	toolData.title = "MyTool1";
	toolData.tooltip = "MyTool1 Tooltip";
	toolData.sameGroupAs = kNoTool;
	toolData.sameToolsetAs = kNoTool;
@endcode

	When adding Toolsets to the Group you just created, use these parameters:

@code
	toolData.title = "MyTool2";
	toolData.tooltip = "MyTool2 Tooltip";
	error = sTool->GetToolNumberFromName("MyTool1", &toolData.sameGroupAs);
	toolData.sameToolsetAs = kNoTool;
@endcode

When adding tools to the above Toolset, use these parameters:

@code
	toolData.title = "MyTool2a";
	toolData.tooltip = "Tool2a Tooltip";
	error = sTool->GetToolNumberFromName("MyTool1",	&toolData.sameGroupAs);
	error = sTool->GetToolNumberFromName("MyTool2", &toolData.sameToolsetAs);
@endcode
 */
typedef struct {
	char *title;
	char *tooltip;
	ASHelpID helpID;
	ADMIconRef icon;
	AIToolType sameGroupAs;
	AIToolType sameToolsetAs;
} AIAddToolData;

/** AIToolPressure is a value from #kAIMinToolPressure to #kAIMaxToolPressure indicating 
	the amount of force being applied with a pressure-sensitive input device, 
	such as a graphic tablet. See #AIToolPressureValue for values.
*/
typedef short AIToolPressure;
enum AIToolPressureValue
{
	/** The maximum pressure applied by a tool */
	kAIMaxToolPressure = 255,
	/** The minimal pressure applied by a tool */
	kAIMinToolPressure = 0,
	/** The amount of pressure applied by a tool that doesn't give "real" pressure data */
	kAINormalToolPressure = 127
};

/** AIToolAngle is used for describing values of an input device such tilt, rotation and bearing of a 
	pen on a graphic tablet. 
	See #AIToolAngleValue for values. 
*/
typedef short AIToolAngle;
enum AIToolAngleValue
{
	kAIToolAngle0 = 0,
	kAIToolAngle90 = 90,
	kAIToolAngleNegative179 = -179,
	kAIToolAngle180 = 180,
	kAIToolAngle360 = 360,
	
	/** Bearing ranges from -179 to 180 where 0 is up */
	kAIToolMinBearing = kAIToolAngleNegative179,
	kAIToolMaxBearing = kAIToolAngle180,
	kAIToolNormalBearing = kAIToolAngle0,
	
	/** Rotation ranges from -179 to 180 where 0 is up */
	kAIToolMinRotation = kAIToolAngleNegative179,
	kAIToolMaxRotation = kAIToolAngle180,
	kAIToolNormalRotation = kAIToolAngle0,
	
	/** Tilt ranges from 0 to 90 where 0 is the pen barrel being perpendicular to the tablet's plane
		and 90 is the pen barrel being parallel to the tablet's plane. */
	kAIToolMinTilt = kAIToolAngle0,
	kAIToolMaxTilt = kAIToolAngle90,
	kAIToolNormalTilt = kAIToolMinTilt
};

/** The contents of a tool message. The SPMessageData is that which is received by
	all calls to a plug-in. The AIToolHandle is a reference to the tool plug-in
	receiving the message. If more than one tool plug-in is installed it is used to
	determine the tool instance that should handle the message.

	The cursor and pressure fields provide direct information about the
	mouse. cursor is the location of the mouse event in Illustrator coordinates.
	pressure indicates the amount of force being applied with a pressure-sensitive input device, 
	such as a graphic tablet.
	
	The AIEvent event is an ADM event record providing information about the mouse
	event. It can be used to find out additional information, such as the
	native cursor coordinates or keyboard keys being held down. It is valid when
	a mouse selector is received. It is abstracted since the definition of an
	event may vary with the platform on which Illustrator is running.
	
	New For AI 12:
	More data from graphic tablets.
	tangential pressure: it is a measurement of the finger wheel on the airbrush tool.
	tilt: The tilt on the tool (how angled is the tilt) aka altitude aka elevation
	bearing: Bearing of the tilt (which direction are we tilting in) measured clockwise around the z axis aka azimuth
	rotation: Rotation of the tool measured clockwise around the tools barrel
	See the description of the individual typedefs for more information on how to interpret these parameters.
	
*/
typedef struct {
	SPMessageData d;
	AIToolHandle tool;
	AIRealPoint cursor;
	AIToolPressure pressure;
	AIEvent *event;
	AIToolPressure stylusWheel;
	AIToolAngle tilt;
	AIToolAngle bearing;
	AIToolAngle rotation;
} AIToolMessage;


/** The contents of a tool notification. */
typedef struct {
	AIToolHandle tool;
} AIToolNotifyData;

/** The contents of an eyedropper drag notification */
typedef struct {
	AIEvent event;
	AIBoolean pathStyleChanged;
} AIEyedropperDragNotifyData;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	Plug-in Tools provide the same level of interaction with the Illustrator
	artwork that native Illustrator Tools provide. They can work on existing
	artwork or create new objects. Plug-in Tools are added to the Illustrator Tool
	Palette in their own set or as part of an existing ToolSet. Feedback can be
	implemented using Illustrator API calls or at a lower level. Tools work fine by
	themselves but can be used in conjunction with other plug-in types.

	This section describes how to install and use plug-in tools. It describes the
	caller/selector pairs and messages that plug-in tools receive, as well as the
	functions to add tools, set options, and control the tool�s appearance in a
	tool palette.
 
	The caller for Tool plug-ins is #kCallerAITool. The following selectors
	may be sent. The mouse selectors are where the main work of a plug-in tool
	is done. The selectors would be checked for in the plug-in's main function.

	- #kSelectorAIEditToolOptions
	- #kSelectorAITrackToolCursor
	- #kSelectorAIToolMouseDown
	- #kSelectorAIToolMouseDrag
	- #kSelectorAIToolMouseUp
	- #kSelectorAISelectTool
	- #kSelectorAIDeselectTool
	- #kSelectorAIReselectTool

	A number of options control the behaviour of plug-in tools. These options are
	typically set when the tool is installed by AddTool() but they may be modified
	using SetToolOptions(). The options are defined by #AIToolOptions.

	The following notifiers are related to tools:

	- #kAIUserToolChangedNotifier
	- #kAIToolSelectedNotifier
	- #kAIToolDeselectedNotifier
	- #kAIToolClearStateNotifier
	- #kAIToolTitleChangedNotifier
	- #kAIToolIconChangedNotifier
	- #kAIToolTooltipChangedNotifier
	- #kAIToolHelpIDChangedNotifier
	- #kAIToolChangedNotifier
	- #kAIToolWindowChangedNotifier
	- #kAIToolSuspendNotifier
	- #kAIToolResumeNotifier
	- #kAIEyedropperDragNotifier

	A plug-in tool uses the Illustrator API calls and creates or modifies Illustrator
	art objects directly when the mouse selectors are received. The application
	updates the window between calls to the plug-in, so the new or changed
	artwork appears as the mouse moves. The AIUndoSuite::UndoChanges() function
	can be used before the tool drag is processed, allowing the plugin a known artwork
	state from which it can redraw its changes.

	Some tools may work on several art objects that they select. Since multiple
	plug-ins can be running at a given time, it is possible that the artwork state
	will change in the course of using a tool. If the tool needs an opportunity to
	verify that a specific artwork state exists, it can be combined with a plug-in
	notifier. The notifier would indicate that the artwork selection or properties
	have been changed. The tool would be aware of this before it selects or
	processes artwork further.
 */
typedef struct {

	/** Pass a reference to your tool plug-in in self and the tool�s name as a C
		string.

		The AIAddToolData structure contains information used in the tool
		palette.

		The options flags controls the tool's behavior, and can be set to
		any combination of the options from #AIToolOptions.

		The AIToolHandle returned is the installed plug-in tool. If you are installing
		multiple tools, this reference must be stored in the plug-in�s globals record,
		as your plug-in will need it to determine which tool will be is being called.
		
		AddTool is generally used at startup.
	*/
	AIAPI AIErr (*AddTool) ( SPPluginRef self, char *name,
				AIAddToolData *data, long options, 
				AIToolHandle *tool );
	
	/** Returns a pointer to the name of the tool. This is the name value originally
		passed to the AddTool call. It should not be modified. */
	AIAPI AIErr (*GetToolName) ( AIToolHandle tool, char **name );
	/** Use this to get the current options of a tool. Pass the AIToolHandle from the
		tool message to get options for the current tool being called or use GetNthTool()
		to get a tool by index. The options returned are the current options flags as
		defined by #AIToolOptions. */
	AIAPI AIErr (*GetToolOptions) ( AIToolHandle tool, long *options );
	/** Use this to set the current options of a tool. Pass the AIToolHandle from the
		tool message to set options for the tool being called or use GetNthTool() to
		set a tool by index. The options variable must reflect all tool options. The
		options flags are as defined by #AIToolOptions.

		Because the options variable must reflect all tool options, this call should be
		used in conjunction with the GetToolOptions call. This will avoid destroying
		any of the plug-ins existing options. */
	AIAPI AIErr (*SetToolOptions) ( AIToolHandle tool, long options );
	/** This call will return a reference to the plug-in that installed the tool. The
		AIPluginHandle can then be used with the SPPluginSuite functions. */
	AIAPI AIErr (*GetToolPlugin) ( AIToolHandle tool, SPPluginRef *plugin );

	/** This function gets the selected plug-in tool. It can be used to implement a
		user interface for setting a tool to use. The tool palette is actually a
		plug-in that uses this call. */
	AIAPI AIErr (*GetSelectedTool) ( AIToolHandle *tool );
	/** This function sets the selected plug-in tool. It can be used to implement a
		user interface for setting a tool to use. The tool palette is actually a
		plug-in that uses this call. */
	AIAPI AIErr (*SetSelectedTool) ( AIToolHandle tool );

	/** Use CountTools() with GetNthTool() to iterate through the plug-in tools
		installed. */
	AIAPI AIErr (*CountTools) ( long *count );
	/** This call returns the AIToolHandle at the specified index. It can be used with
		CountTools() to iterate through the plug-in tools installed. */
	AIAPI AIErr (*GetNthTool) ( long n, AIToolHandle *tool );

	/** This call returns the AIToolHandle from the specified Tool number. Illustrator
		assigns a number to a plug-in tool when it is created using AddTool(). */
	AIAPI AIErr (*GetToolHandleFromNumber) ( AIToolType toolNum,  AIToolHandle *tool );
	/** This call returns the tool number from the specified tool name. The names of
		Illustrator's built in tools can be found in AIToolNames.h. The name of a plugin
		tool is the same name passed into the AddTool() function. This function can be used
		when filling the AIAddToolData structure in preparation for adding your plugin
		tool. */
	AIAPI AIErr (*GetToolNumberFromName) ( char *name, AIToolType *toolNum );
	/** This call returns the tool number from the specified AIToolHandle. */
	AIAPI AIErr (*GetToolNumberFromHandle) ( AIToolHandle tool, AIToolType *toolNum );
	/** Get the name of a tool from its number. Caller must copy result immediately. */
	AIAPI AIErr (*GetToolNameFromNumber) (AIToolType toolNum, char **name);

	/** This function gets the indicated plug-in tool's title, as specified when it was
		added or modified with SetToolTitle(). The title is a C string that may be
		displayed to the user as the current tool. It can be used to implement a user
		interface for setting a tool to use. The plug-in tool palette is actually a
		plug-in that uses this call. */
	AIAPI AIErr (*GetToolTitle) ( AIToolHandle tool, char **title );
	/** This function changes the title of the plug-in tool. It might be used in
		dynamically creating variants of a standard tool. title is a C style string. */
	AIAPI AIErr (*SetToolTitle) ( AIToolHandle tool, char *title );
	/** This function gets the icon of the indicated plug-in tool. It can be used to
		implement a user interface for setting a tool to use. The icon is an ADMIconRef.
		See the ADMIconSuite documentation for a description of an ADMIconRef. The plug-in
		tool palette is actually a plug-in that uses this call. */
	AIAPI AIErr (*GetToolIcon) ( AIToolHandle tool, ADMIconRef *icon );
	/** This function changes the icon of the plug-in tool. The icon is an ADMIconRef.
		See the ADMIconSuite documentation for a description of an ADMIconRef. */
	AIAPI AIErr (*SetToolIcon) ( AIToolHandle tool, ADMIconRef icon );
	/** This function returns a plug-in�s tooltip from the specified AIToolHandle.
		The tooltip is the same as that which was passed to the AddTool() function. */
	AIAPI AIErr (*GetTooltip) ( AIToolHandle tool, char **tooltip );
	/** This function sets the tooltip of the the specified AIToolHanlde. */
	AIAPI AIErr (*SetTooltip) ( AIToolHandle tool, char *tooltip );
	/** This function returns the help id for the tool which can be an integer or
		a pooled string. */
	AIAPI AIErr (*GetToolHelpID) ( AIToolHandle tool, ASHelpID *helpID );
	/** Sets the tool's help id. */
	AIAPI AIErr (*SetToolHelpID) ( AIToolHandle tool, ASHelpID helpID );

	/** This API is typically called once, at startup, to initialize which fields in the
		info palette the tool will use. The infoVars parameter is a pointer to the first
		member of an array of information variables terminated by the special variable
		#kInfoEndOfList. Each variable that is displayed on Illustrator's Info Palette has
		a label. The information variable you pass into this function will determine which
		label is displayed with your info value. The variables are defined in
		#AIToolInfoVariable. */
	AIAPI AIErr (*SetToolInfoVars) ( AIToolHandle tool, const long * infoVars );
	/** Unlike SetToolInfoVars(), this call can be used anytime to set the tool's info
		values displayed in the Info Palette. infoVars is a pointer to an array of info
		variables. This list corresponds to the list passed to SetToolInfoVars(). The
		code below illustrates it's use.

@code
	AIErr UpdateInfoPalette( AIToolHandle tool, AIRealPoint origin, AIArtHandle art )
	{
		ASErr error = kNoErr;
		if (art)
		{
			static const long infoVars = { kInfoDocX, kInfoDocY, kInfoSizeX, kInfoSizeY, kInfoEndOfList };
			AIReal *infoValues[4];
			AIReal temp[2];
			AIRealRect artBounds;
			error = sAIArt->GetArtTransformBounds( art,0, kStroke, &artBounds);
			if ( error )
				return error;
			temp[0] = artBounds.right - artBounds.left;
			temp[1] = artBounds.bottom - artBounds.top;
			infoValues[0] = &origin.h;
			infoValues[1] = &origin.v;
			infoValues[2] = temp;
			infoValues[3] = temp +1;
			error = sAITool->SetToolInfoVarValues( infoVars, (void**)infoValues);
			if ( error )
				return error;
		}
	}
@endcode
	*/
	AIAPI AIErr (*SetToolInfoVarValues) ( const long *infoVars,  void **values );
	
	/** Check whether a pressure sensitive tablet is being used. */
	AIAPI AIErr (*SystemHasPressure) ( ASBoolean *hasPressure );
	
	// New for AI10
	
	/** Gets the rollover icon for the given tool. If the plugin tool never sets
		a rollover icon, then the regular icon is also used as the rollover icon. */
	AIAPI AIErr (*GetToolRolloverIcon) ( AIToolHandle tool, ADMIconRef *icon );
	/** Sets the rollover icon for the given tool. If the plugin tool never sets
		a rollover icon, then the regular icon is also used as the rollover icon. */
	AIAPI AIErr (*SetToolRolloverIcon) ( AIToolHandle tool, ADMIconRef icon );

	/** Deprecated in AI11. Please do not use. */
	AIAPI AIErr (*GetToolNullEventInterval) (AIToolHandle tool, AIToolTime *outTime);
	/** Deprecated in AI11. Please do not use. */
	AIAPI AIErr (*SetToolNullEventInterval) (AIToolHandle tool, AIToolTime inTime);
	
} AIToolSuite;




#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
