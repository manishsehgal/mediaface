#ifndef __AI80DrawArt__
#define __AI80DrawArt__

/*
 *        Name:	AI80DrawArt.h
 *   $Revision: 17 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 8.0 Draw Art Suite.
 *
 * Copyright (c) 1986-1998 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AIColor__
#include "AIColor.h"
#endif

#ifndef __AIRaster__
#include "AIRaster.h"
#endif

#ifndef __AIDrawArt__
#include "AIDrawArt.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIDrawArtSuite				"AI Draw Art Suite"
#define kAIDrawArtSuiteVersion4		AIAPI_VERSION(4)
#define kAI80DrawArtSuiteVersion	kAIDrawArtSuiteVersion4




/*******************************************************************************
 **
 **	Types
 **
 **/



typedef struct {
	short version;
	
	AIDrawArtFlags flags;
	AIDrawArtOutputType type;
	AIRealPoint origin;
	AIRealMatrix matrix;
	AIArtHandle art;
	AIRealRect destClipRect;
	AIBoolean eraseDestClipRect; // erase destClipRect before arts are drawn.


	AIReal greekThreshold;
	AIExtendedRGBColorRec selectionColor;
	
	union {
		AIDrawArtGWorld gWorld;
		AIDrawArtAGMPort port;
	} output;
} AI80DrawArtData;

typedef struct AI80DrawColorData {
	AIDrawArtOutputType type;

	AIDrawColorStyle style;
	AIDrawColorOptions options;
	AIColor color;
	AIRealRect rect;
	AIReal width;							// when style is frame
	
	union {
		AIDrawArtGWorld gWorld;
		AIDrawArtAGMPort port;
	} output;
} AI80DrawColorData;

/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	AIAPI AIErr (*DrawArt)( AI80DrawArtData *data );
	AIAPI AIErr (*DrawColorSwatch)( AI80DrawColorData *data );
	AIAPI AIErr (*DrawHilite)( AIArtHandle art, AIRGBColor* color );
	AIAPI AIErr (*DrawThumbnail) ( AIArtHandle art, void* port, AIRealRect* dstrect );

} AI80DrawArtSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
