/*
 *        Name:	AI90Tool.h
 *   $Revision: #1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 9.0 Tool Suite.
 *
 * Copyright (c) 1986-1998 Adobe Systems Incorporated, All Rights Reserved.
 *
 */
#ifndef __AI90Tool__
#define __AI90Tool__



/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AITool.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAI90ToolSuite			kAIToolSuite
#define kAIToolSuiteVersion8	AIAPI_VERSION(8)
#define kAI90ToolSuiteVersion	kAIToolSuiteVersion8

/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	AIAPI AIErr (*AddTool) ( SPPluginRef self, char *name,
				AIAddToolData *data, long options, 
				AIToolHandle *tool );
	
	AIAPI AIErr (*GetToolName) ( AIToolHandle tool, char **name );
	AIAPI AIErr (*GetToolOptions) ( AIToolHandle tool, long *options );
	AIAPI AIErr (*SetToolOptions) ( AIToolHandle tool, long options );
	AIAPI AIErr (*GetToolPlugin) ( AIToolHandle tool, SPPluginRef *plugin );

	AIAPI AIErr (*GetSelectedTool) ( AIToolHandle *tool );
	AIAPI AIErr (*SetSelectedTool) ( AIToolHandle tool );

	AIAPI AIErr (*CountTools) ( long *count );
	AIAPI AIErr (*GetNthTool) ( long n, AIToolHandle *tool );

	AIAPI AIErr (*GetToolHandleFromNumber) ( AIToolType toolNum,  AIToolHandle *tool );
	AIAPI AIErr (*GetToolNumberFromName) ( char *name, AIToolType *toolNum );
	AIAPI AIErr (*GetToolNumberFromHandle) ( AIToolHandle tool, AIToolType *toolNum );
	AIAPI AIErr (*GetToolNameFromNumber) (AIToolType toolNum, char **name);   // Caller must copy result immediately

	AIAPI AIErr (*GetToolTitle) ( AIToolHandle tool, char **title );
	AIAPI AIErr (*SetToolTitle) ( AIToolHandle tool, char *title );
	AIAPI AIErr (*GetToolIcon) ( AIToolHandle tool, ADMIconRef *icon );
	AIAPI AIErr (*SetToolIcon) ( AIToolHandle tool, ADMIconRef icon );
	AIAPI AIErr (*GetTooltip) ( AIToolHandle tool, char **tooltip );
	AIAPI AIErr (*SetTooltip) ( AIToolHandle tool, char *tooltip );
	AIAPI AIErr (*GetToolHelpID) ( AIToolHandle tool, ASHelpID *helpID );
	AIAPI AIErr (*SetToolHelpID) ( AIToolHandle tool, ASHelpID helpID );

	AIAPI AIErr (*SetToolInfoVars) ( AIToolHandle tool, const long * infoVars );
	AIAPI AIErr (*SetToolInfoVarValues) ( const long *infoVars,  void **values );
	
	AIAPI AIErr (*SystemHasPressure) ( ASBoolean *hasPressure );
	
} AI90ToolSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
