#ifndef __AI100CharacterEncoding__
#define __AI100CharacterEncoding__

/*
 *        Name:	AI100CharacterEncoding.h
 *		$Id $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Character Encoding Suite.
 *
 * Copyright (c) 2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AICharacterEncoding.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAI100CharacterEncodingSuite				kAICharacterEncodingSuite
#define kAICharacterEncodingSuiteVersion1			AIAPI_VERSION(1)
#define kAI100CharacterEncodingSuiteVersion			kAICharacterEncodingSuiteVersion1


/*******************************************************************************
 **
 **	Suite
 **
 **/


typedef struct _AI100CharacterEncodingSuite {

	// Converts the contents of the source buffer from the source encoding to
	// the destination encoding. srcBuffer is assumed to contain srcLength bytes
	// of data. dstBuffer is assumed to be large enough to contain dstSize bytes
	// and dstLength returns the actual number of bytes of converted data.
	AIAPI AIErr (*ConvertBuffer) (
			const void* srcBuffer, long srcLength, AICharacterEncoding srcEncoding,
			void* dstBuffer, long dstSize, AICharacterEncoding dstEncoding,
			long* dstLength);

} AI100CharacterEncodingSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
