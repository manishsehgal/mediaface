#ifndef __AI100HitTest__
#define __AI100HitTest__

/*
 *        Name:	AI100HitTest.h
 *   $Revision: 3 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Hit Testing Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AIHitTest.h"


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAI100HitTestSuite			kAIHitTestSuite
#define kAIHitTestSuiteVersion8		AIAPI_VERSION(1)
#define kAI100HitTestSuiteVersion	kAIHitTestSuiteVersion8


/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {
	AIAPI AIErr (*HitTest) ( AIArtHandle art, AIRealPoint *point, long option, AIHitRef *hit );

	AIAPI long (*AddRef) ( AIHitRef hit );
	AIAPI long (*Release) ( AIHitRef hit );

	AIAPI AIErr (*GetHitData) ( AIHitRef hit, AIToolHitData *toolHit );

	AIAPI AIBoolean (*IsHit) ( AIHitRef hit );
	AIAPI long (*GetType) ( AIHitRef hit );
	AIAPI AIArtHandle (*GetArt) ( AIHitRef hit );
	AIAPI AIArtHandle (*GetGroup) ( AIHitRef hit );
	AIAPI AIRealPoint (*GetPoint) ( AIHitRef hit );

	/* Path specific */
	AIAPI short (*GetPathSegment) ( AIHitRef hit );
	AIAPI AIReal (*GetPathParameter) ( AIHitRef hit );

} AI100HitTestSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
