#ifndef __AI80Undo__
#define __AI80Undo__

/*
 *        Name:	AIUndo.h
 *   $Revision: 6 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Undo Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AIUndo.h"


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAI80UndoSuite			"AI Undo Suite"
#define kAIUndoSuiteVersion3	AIAPI_VERSION(3)
#define kAI80UndoVersion		kAIUndoSuiteVersion3


/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	AIAPI AIErr (*SetUndoText) ( const char *undoText, const char *redoText );
	AIAPI AIErr (*UndoChanges) ( void );

	AIAPI AIErr (*SetActionPaletteUndo) ( void );
	AIAPI AIErr (*PurgeAllUndos) ( void );

} AI80UndoSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
