#ifndef __AI80DataFilter__
#define __AI80DataFilter__

/*
 *        Name:	AIDataFilter.h
 *   $Revision: 6 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Data Filter Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AIDataFilter.h"


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIDataFilterSuite				"AI Data Filter Suite"
#define kAIDataFilterSuiteVersion2		AIAPI_VERSION(2)
#define kAI80DataFilterVersion			kAIDataFilterSuiteVersion2



/*******************************************************************************
 **
 **	Types
 **
 **/


/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	AIAPI AIErr (*LinkDataFilter) ( AIDataFilter *prev, AIDataFilter *next );
	AIAPI AIErr (*UnlinkDataFilter) ( AIDataFilter *next, AIDataFilter **prev );
	AIAPI AIErr (*ReadDataFilter) ( AIDataFilter *filter, char *store, long *count );
	AIAPI AIErr (*WriteDataFilter) ( AIDataFilter *filter, char *store, long *count );
	AIAPI AIErr (*SeekDataFilter) ( AIDataFilter *filter, long *count );
	AIAPI AIErr (*MarkDataFilter) ( AIDataFilter *filter, long *count );
	AIAPI AIErr (*NewFileDataFilter) ( SPPlatformFileSpecification spec, char *mode, long creator, long type, AIDataFilter **filter );
	AIAPI AIErr (*NewBufferDataFilter) ( long size, AIDataFilter **filter );
	AIAPI AIErr (*NewHexdecDataFilter) ( char *state, long shift, AIDataFilter **filter );
	AIAPI AIErr (*NewBlockDataFilter) ( void *address, long size, AIDataFilter **filter );

} AI80DataFilterSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
