#ifndef __AI90Context__
#define __AI90Context__

/*
 *        Name:	AI90Context.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0-9.0 Runtime Context Environment.
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AIContext__
#include "AIContext.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAI90AppContextSuite		"AI Context Suite"
#define kAIAppContextSuiteVersion2	AIAPI_VERSION(2)
#define kAI90AppContextVersion		kAIAppContextSuiteVersion2


/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	AIAPI AIErr (*GetPlatformAppWindow) ( AIWindowRef *appWindow );

	AIAPI AIErr (*PushAppContext) ( SPPluginRef plugin, AIAppContextHandle *appContext );
	AIAPI AIErr (*PopAppContext) ( AIAppContextHandle appContext );

	AIAPI AIErr (*MacGetAppQDGlobal) ( void **appQD );

} AI90AppContextSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
