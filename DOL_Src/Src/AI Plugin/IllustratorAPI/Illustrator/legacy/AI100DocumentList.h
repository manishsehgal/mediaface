#ifndef __AI100DocumentList__
#define __AI100DocumentList__

/*
 *        Name:	AI100DocumentList.h
 *     Purpose:	Adobe Illustrator 10.0 Document Suite.
 *
 * Copyright (c) 1986-2003 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AIDocumentList.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAI100DocumentListSuite			kAIDocumentListSuite
#define kAIDocumentListSuiteVersion1	AIAPI_VERSION(1)
#define kAI100DocumentListSuiteVersion	kAIDocumentListSuiteVersion1


/*******************************************************************************
 **
 **	Suite
 **
 **/


// Illustrator 10.0 Document List Suite
typedef struct {

	AIAPI AIErr	(*Count)(long* count);
	AIAPI AIErr	(*GetNthDocument)(AIDocumentHandle* document , long lIndex);
	AIAPI AIErr	(*New)(char* title, AIColorModel* colorMode, 
						 AIReal* artboardWidth, AIReal* artboardHeight , ActionDialogStatus dialogStatus , AIDocumentHandle* document);
	AIAPI AIErr	(*Open)(SPPlatformFileSpecification *fileSpec , AIColorModel colorMode, 
							ActionDialogStatus dialogStatus , AIDocumentHandle* document);
	AIAPI AIErr	(*Save)(AIDocumentHandle document);
	AIAPI AIErr	(*Close)(AIDocumentHandle document);
	AIAPI AIErr	(*CloseAll)();
	AIAPI AIErr	(*Activate)(AIDocumentHandle document , ASBoolean bSetFocus);
	AIAPI AIErr	(*Print)(AIDocumentHandle document , ActionDialogStatus dialogStatus);
} AI100DocumentListSuite;



#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
