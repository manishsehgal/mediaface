#ifndef __AIDocumentView90__
#define __AIDocumentView90__

/*
 *        Name:	AIDocumentView90.h
 *   $Revision: 20 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 9.0 Document View Suite.
 *
 * Copyright 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIDocumentViewSuiteVersion6	AIAPI_VERSION(6)


/*******************************************************************************
 **
 **	Suite
 **
 **/


// version 6
typedef struct {

	AIAPI AIErr (*GetDocumentViewBounds) ( AIDocumentViewHandle view, AIRealRect *bounds );
	AIAPI AIErr (*GetDocumentViewCenter) ( AIDocumentViewHandle view, AIRealPoint *center );
	AIAPI AIErr (*SetDocumentViewCenter) ( AIDocumentViewHandle view, AIRealPoint *center );
	AIAPI AIErr (*GetDocumentViewZoom) ( AIDocumentViewHandle view, AIReal *zoom );
	AIAPI AIErr (*SetDocumentViewZoom) ( AIDocumentViewHandle view, AIReal zoom );

	AIAPI AIErr (*ArtworkPointToViewPoint) ( AIDocumentViewHandle view, AIRealPoint *artworkPoint, AIPoint *viewPoint );
	AIAPI AIErr (*ViewPointToArtworkPoint) ( AIDocumentViewHandle view, AIPoint *viewPoint, AIRealPoint *artworkPoint );

	AIAPI AIErr (*CountDocumentViews)( long *count );
	AIAPI AIErr (*GetNthDocumentView)( long n, AIDocumentViewHandle *view );

	AIAPI AIErr (*FixedArtworkPointToViewPoint) ( AIDocumentViewHandle view, AIRealPoint *artworkPoint, AIRealPoint *viewPoint );
	AIAPI AIErr (*FixedViewPointToArtworkPoint) ( AIDocumentViewHandle view, AIRealPoint *viewPoint, AIRealPoint *artworkPoint );

	AIAPI AIErr (*SetScreenMode) ( AIDocumentViewHandle view, AIScreenMode mode );
	AIAPI AIErr (*GetScreenMode) ( AIDocumentViewHandle view, AIScreenMode *mode );

	AIAPI AIErr (*GetPageTiling) ( AIPageTiling *pageTiling );

	AIAPI AIErr (*GetTemplateVisible) ( AIDocumentViewHandle view, AIBoolean *visible );

	AIAPI AIErr (*DocumentViewScrollDelta)( AIDocumentViewHandle view, AIRealPoint *delta);
	AIAPI AIErr (*GetDocumentViewInvalidRect)( AIDocumentViewHandle view, AIRealRect *invalidRect );
	AIAPI AIErr (*SetDocumentViewInvalidRect)( AIDocumentViewHandle view, AIRealRect *invalidRect );

	AIAPI AIErr (*GetDocumentViewStyle)( AIDocumentViewHandle view,  short *style );

	AIAPI AIErr (*SetDocumentViewInvalidDocumentRect)( AIDocumentViewHandle view, AIRealRect *invalidRect );

	AIAPI AIErr (*GetShowPageTiling) ( ASBoolean *show );
	AIAPI AIErr (*SetShowPageTiling) ( ASBoolean show );

	AIAPI AIErr (*GetGridOptions) ( AIDocumentViewHandle view, ASBoolean* show, ASBoolean* snap );
	AIAPI AIErr (*SetGridOptions) ( AIDocumentViewHandle view, ASBoolean show, ASBoolean snap );

	AIAPI AIErr (*GetShowTransparencyGrid) ( AIDocumentViewHandle view, ASBoolean* show );
	AIAPI AIErr (*SetShowTransparencyGrid) ( AIDocumentViewHandle view, ASBoolean show );

} AI90DocumentViewSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
