/**

	AI90Geometry.h
	Copyright (c) 1986-2001 Adobe Systems Incorporated.

	Adobe Illustrator 9.0 Geometry Suite.

 **/

#ifndef __AI90Geometry__
#define __AI90Geometry__


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AIGeometry.h"


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAI90GeometrySuite			kAIGeometrySuite
#define kAIGeometryVersion2			AIAPI_VERSION(2)
#define kAI90GeometrySuiteVersion	kAIGeometryVersion2



/*******************************************************************************
 **
 **	Types
 **
 **/


typedef struct {

	AIGeometryBeginProc beginPath;
	AIGeometryEndProc endPath;
	AIGeometryBeginProc beginGroup;
	AIGeometryEndProc endGroup;
	AIGeometryBeginProc beginCompoundPath;
	AIGeometryEndProc endCompoundPath;
	AIGeometryBeginProc beginPlacedImage;
	AIGeometryEndProc endPlacedImage;
	AIGeometryBeginProc beginRaster;
	AIGeometryEndProc endRaster;
	AIGeometryBeginProc beginText;
	AIGeometryEndProc endText;
	AIGeometryBeginProc beginTextPath;
	AIGeometryEndProc endTextPath;
	AIGeometryBeginProc beginTextLine;
	AIGeometryEndProc endTextLine;
	AIGeometryBeginProc beginClipGroup;
	AIGeometryEndProc endClipGroup;
	AIGeometryBeginProc beginMask;
	AIGeometryEndProc endMask;
	AIGeometryBeginProc beginMesh;
	AIGeometryEndProc endMesh;

} AI90GeometryOrganizationProcs;


/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	ASAPI AIErr (*GeometryIterate) ( AIArtHandle art, 
									 AI90GeometryOrganizationProcs *organizationProcs, 
									 AIGeometryConstructionProcs *constructionProcs, 
									 AIGeometryPaintingProcs *paintingProcs, 
									 AIGeometryStateProcs *stateProcs, 
									 AIGeometryUserData userData );

} AI90GeometrySuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
