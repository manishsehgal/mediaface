#ifndef __AI80Online__
#define __AI80Online__

/*
 *        Name:	AI80Online.h
 *   $Revision: 1 $
 *      Author:	I'lam Mougy
 *        Date:	23 June 1998
 *     Purpose:	AI Online suit.
 *
 * Copyright (c) 1986-1998 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

// AIOnline Suite
#define kAI80OnlineSuite							"AI Online Suite"
#define kAIOnlineSuiteVersion1				AIAPI_VERSION(1)
#define kAI80OnlineSuiteVersion				kAIOnlineSuiteVersion1


typedef struct 
{	
	AIAPI AIErr			(*ShowOnlineDialog)();
} AI80OnlineSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif