#ifndef __AI80ArtSet__
#define __AI80ArtSet__

/*
 *        Name:	AI80ArtSet.h
 *   $Revision: 5 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Art Set Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AIArtSet.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAI80ArtSetSuite		"AI Art Set Suite"
#define kAI80ArtSetSuiteVersion	AIAPI_VERSION(5)
#define kAI80ArtSetVersion		kAI80ArtSetSuiteVersion


/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	AIAPI AIErr (*NewArtSet) ( AIArtSet *artSet );
	AIAPI AIErr (*DisposeArtSet) ( AIArtSet *artSet );
	AIAPI AIErr (*CountArtSet) ( AIArtSet artSet, long *count );
	AIAPI AIErr (*IndexArtSet) ( AIArtSet artSet, long index, AIArtHandle *art );

	AIAPI AIErr (*ArrayArtSet) ( AIArtSet artSet, AIArtHandle *artArray, long count );
	AIAPI AIErr (*SelectedArtSet) ( AIArtSet artSet );
	AIAPI AIErr (*MatchingArtSet) ( AIArtSpec *specs, short numSpecs, AIArtSet artSet );
	AIAPI AIErr (*LayerArtSet) ( AILayerHandle layer, AIArtSet artSet );
	
	AIAPI AIErr (*NotArtSet) ( AIArtSet src, AIArtSet dst );
	AIAPI AIErr (*UnionArtSet) ( AIArtSet src0, AIArtSet src1, AIArtSet dst );
	AIAPI AIErr (*IntersectArtSet) ( AIArtSet src0, AIArtSet src1, AIArtSet dst );

	AIAPI AIErr (*NextInArtSet) ( AIArtSet artSet, AIArtHandle prevArt, AIArtHandle *nextArt );

} AI80ArtSetSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
