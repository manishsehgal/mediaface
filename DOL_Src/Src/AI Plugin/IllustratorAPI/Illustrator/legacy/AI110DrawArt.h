#ifndef __AI110DrawArt__
#define __AI110DrawArt__

/*
 *        Name:	AI110DrawArt.h
 *   $Revision: $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 11.0 Draw Art Suite.
 *
 * Copyright (c) 1986-1998 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AIDrawArt__
#include "AIDrawArt.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AI110DrawArt.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAI110DrawArtSuite			"AI Draw Art Suite"
#define kAIDrawArtSuiteVersion6		AIAPI_VERSION(6)
#define kAI110DrawArtSuiteVersion	kAIDrawArtSuiteVersion6
#define kAI110DrawArtVersion		kAIDrawArtSuiteVersion

/*******************************************************************************
 **
 **	Types
 **
 **/

/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	The draw art suite APIs can be used to draw Illustrator artwork to an
	output port (drawing surface). The port can either be contained within
	a window in the UI or it can be an offscreen drawing surface being used
	to rasterize artwork. There are a considerable number of options for
	controlling the drawing process.
 */
typedef struct {

	/** Draw artwork as specified by the AIDrawArtData parameter. */
	AIAPI AIErr (*DrawArt)( AIDrawArtData *data );
	/** Draw a color swatch as specified by the AIDrawColorData parameter. */
	AIAPI AIErr (*DrawColorSwatch)( AIDrawColorData *data );
	/** This method can only be called from the code that responds to an annotator's draw
		annotations message. It causes the highlighting annotations for the specified art
		object to be drawn using the specified color. */
	AIAPI AIErr (*DrawHilite)( AIArtHandle art, AIRGBColor* color );
	/** A convenience API for drawing a thumbnail of the art object to the AGM port with
		the specified bounds. */
	AIAPI AIErr (*DrawThumbnail) ( AIArtHandle art, void* port, AIRealRect* dstrect );

} AI110DrawArtSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
