#ifndef __AI90Placed__
#define __AI90Placed__

/*
 *        Name:	AI90Placed.h
 *   $Revision: 24 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 9.0 Placed Object Suite.
 *
 * Copyright (c) 1986-1999 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AI110Placed.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAI90PlacedSuite			"AI Placed Suite"
#define kAIPlacedSuiteVersion4	AIAPI_VERSION(4)
#define kAI90PlacedSuiteVersion	kAIPlacedSuiteVersion4
#define kAI90PlacedVersion		kAI90PlacedSuiteVersion

/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct AI90PlacedSuite {

	AIAPI AIErr (*GetPlacedFileSpecification) ( AIArtHandle placed, SPPlatformFileSpecification *file );
	AIAPI AIErr (*SetPlacedFileSpecification) ( AIArtHandle placed, SPPlatformFileSpecification *file );
	AIAPI AIErr (*GetPlacedMatrix) ( AIArtHandle placed, AIRealMatrix *matrix );
	AIAPI AIErr (*SetPlacedMatrix) ( AIArtHandle placed, AIRealMatrix *matrix );
	AIAPI AIErr (*GetPlacedBoundingBox) ( AIArtHandle placed, AIRealRect *bbox );
	AIAPI AIErr (*SetPlacedObject) ( AIArtHandle placed, AIArtHandle *group );
	AIAPI AIErr (*CountPlacedCustomColors) ( AIArtHandle art, long *count );
	AIAPI AIErr (*GetNthPlacedCustomColorName) ( AIArtHandle art, long num, char *name, int maxlen );
	AIAPI AIErr (*MakePlacedObjectNative) ( AIArtHandle placed, AIArtHandle *native );
	AIAPI AIErr (*GetPlacedType) ( AIArtHandle placed, short *pPlacedType );
	AIAPI AIErr (*GetPlacedChild) ( AIArtHandle placed, AIArtHandle *group );
	AIAPI AIErr (*ExecPlaceRequest)( AI110PlaceRequestData *pPlaceRequestData );
	AIAPI AIErr (*GetPlacedFileInfoFromArt)( AIArtHandle placed, SPPlatformFileInfo *spFileInfo );
	AIAPI AIErr (*GetPlacedFileInfoFromFile)( AIArtHandle placed, SPPlatformFileInfo *spFileInfo );
	AIAPI AIErr (*GetPlacedFilePathFromArt)( AIArtHandle placed, char *pszPath, int iMaxLen );
	AIAPI AIErr (*ConcatPlacedMatrix) ( AIArtHandle placed, AIRealMatrix *concat );

} AI90PlacedSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif

