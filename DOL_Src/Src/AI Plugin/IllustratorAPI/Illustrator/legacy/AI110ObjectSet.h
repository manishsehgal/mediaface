#ifndef __AI110ObjectSet__
#define __AI110ObjectSet__

/*
 *        Name:	AI110ObjectSet.h
 *   $Revision: 17 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Pattern Fill Suite.
 *
 * Copyright (c) 1986-1998 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AIObjectSet.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AI110ObjectSet.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAI110ObjectSetSuite			kAIObjectSetSuite
#define kAIObjectSetSuiteVersion1		AIAPI_VERSION(1)
#define kAI110ObjectSetSuiteVersion		kAIObjectSetSuiteVersion1
#define kAI110ObjectSetVersion			kAI110ObjectSetSuiteVersion


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	Object sets provide a mechanism for attaching global information to a document. Their use is
	deprecated in favor of dictionaries. The document contains a dictionary where arbitrary information
	can be stored. See AIDocumentSuite::GetDictionary() and AIDocumentSuite::GetNonRecordedDictionary().
 */
typedef struct {

	AIAPI AIErr (*NewObjectSet) ( SPPluginRef self, unsigned char *name, AIObjectMap *map, AIObjectSetHandle *set );
	AIAPI AIErr (*CountObjectSets) ( long *count );
	AIAPI AIErr (*GetNthObjectSet) ( long n, AIObjectSetHandle *set );
	AIAPI AIErr (*GetObjectSetName) ( AIObjectSetHandle set, unsigned char *name );
	AIAPI AIErr (*GetObjectSetByName) ( unsigned char *name, AIObjectSetHandle *set );
	AIAPI AIErr (*GetObjectSetMap) ( AIObjectSetHandle set, AIObjectMap **map );
	AIAPI AIErr (*NewObject) ( AIObjectSetHandle setHandle, unsigned char *name, void *def, int defLen, AIObjectHandle *object);
	AIAPI AIErr (*DeleteObject) ( AIObjectSetHandle setHandle, AIObjectHandle object);
	AIAPI AIErr (*SetObjectInternal) ( AIObjectSetHandle setHandle, AIObjectHandle object, void *data, long dataLen);
	AIAPI AIErr (*SetObjectExternal) ( AIObjectSetHandle setHandle, AIObjectHandle object, char *data);
	AIAPI AIErr (*GetObjectInternal) ( AIObjectSetHandle setHandle, AIObjectHandle object, void **data, long *dataLen);
	AIAPI AIErr (*GetObjectExternal) ( AIObjectSetHandle setHandle, AIObjectHandle object, char **data);
	AIAPI AIErr (*CountObjects) ( AIObjectSetHandle setHandle, long *count);
	AIAPI AIErr (*GetNthObject) ( AIObjectSetHandle setHandle, long n, AIObjectHandle *object);
	AIAPI AIErr (*GetObjectName) ( AIObjectSetHandle setHandle, AIObjectHandle object, unsigned char *name);
	AIAPI AIErr (*SetObjectName) ( AIObjectSetHandle setHandle, AIObjectHandle object, unsigned char *name);
	AIAPI AIErr (*GetObjectByName) ( AIObjectSetHandle setHandle, unsigned char *name, AIObjectHandle *object);
	AIAPI AIErr (*NewObjectName) ( AIObjectSetHandle setHandle, char *name, int maxLen );
	AIAPI AIErr (*GetObjectDisplayName) ( char *name );
	AIAPI AIErr (*MarkObjectInUse) ( AIObjectSetHandle setHandle, AIObjectHandle object);
	AIAPI AIErr (*RetargetForCurrentDocument) ( AIObjectHandle object, AIObjectHandle *newObj );

} AI110ObjectSetSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
