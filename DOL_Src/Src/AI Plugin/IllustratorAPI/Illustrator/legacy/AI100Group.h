#ifndef __AI100Group__
#define __AI100Group__

/*
 *        Name:	AIGroup.h
 *   $Revision: 6 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Group Object Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/


#ifndef __AIGroup__
#include "AIGroup.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN



/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAI100GroupSuite			"AI Group Suite"
#define kAIGroupSuiteVersion2		AIAPI_VERSION(2)
#define kAI100GroupSuiteVersion		kAIGroupSuiteVersion2


/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	AIAPI AIErr (*GetGroupClipped) ( AIArtHandle group, AIBoolean *clipped );
	AIAPI AIErr (*SetGroupClipped) ( AIArtHandle group, AIBoolean clipped );

	AIAPI AIErr (*GetGroupMaskLock) ( AIArtHandle group, AIBoolean *maskLocked );
	AIAPI AIErr (*SetGroupMaskLock) ( AIArtHandle group, AIBoolean maskLocked );

} AI100GroupSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
