#ifndef __AI100Gradient__
#define __AI100Gradient__

/*
 *        Name:	AI100Gradient.h
 *   $Revision: 10 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Gradient Fill Suite.
 *
 * Copyright (c) 1986-2002 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AI80Gradient.h"
#include "AI110PathStyle.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAI100GradientSuite			kAIGradientSuite
#define kAIGradientSuiteVersion4	AIAPI_VERSION(4)
#define kAI100GradientSuiteVersion	kAIGradientSuiteVersion4


/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	AIAPI AIErr (*NewGradient) ( AIGradientHandle *gradient );
	AIAPI AIErr (*DeleteGradient) ( AIGradientHandle gradient );
	AIAPI AIErr (*CountGradients) ( long *count );
	AIAPI AIErr (*GetNthGradient) ( long n, AIGradientHandle *gradient );
	AIAPI AIErr (*GetGradientByName) ( unsigned char *name, AIGradientHandle *gradient );
	AIAPI AIErr (*GetGradientName) ( AIGradientHandle gradient, unsigned char *name );
	AIAPI AIErr (*SetGradientName) ( AIGradientHandle gradient, unsigned char *name );
	AIAPI AIErr (*GetGradientType) ( AIGradientHandle gradient, short *type );
	AIAPI AIErr (*SetGradientType) ( AIGradientHandle gradient, short type );
	AIAPI AIErr (*GetGradientStopCount) ( AIGradientHandle gradient, short *count );
	AIAPI AIErr (*GetNthGradientStop) ( AIGradientHandle gradient, short n, AIGradientStop *stop );
	AIAPI AIErr (*SetNthGradientStop) ( AIGradientHandle gradient, short n, AIGradientStop *stop );
	AIAPI AIErr (*InsertGradientStop) ( AIGradientHandle gradient, short n, AIGradientStop *stop );
	AIAPI AIErr (*DeleteGradientStop) ( AIGradientHandle gradient, short n, AIGradientStop *stop );
	AIAPI AIErr (*IterateGradient) ( AIArtHandle art, AI110PathStyle *style, AI80GradientProcs *procs, short iterations );
	AIAPI AIErr (*NewGradientName) ( char *name, int maxlen ); //name is modified in place
	AIAPI AIErr (*GetGradientDisplayName) ( char *name ); //name is modified in place

	// New for AI8.0: report whether some AIGradientStop uses the given color
	// If matchTint is false and it is a custom color, any tint of that color counts
	AIAPI AIErr (*GradientUsesAIColor) (AIGradientHandle gradient, AIColor *color,
										AIBoolean matchTint, AIBoolean *usesColor);
										
	AIAPI AIBoolean (*ValidateGradient) ( AIGradientHandle gradient );


} AI100GradientSuite;



#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
