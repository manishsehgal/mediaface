/* -------------------------------------------------------------------------------

                                    ADOBE CONFIDENTIAL
                                _________________________

    Copyright 2000-2002 Adobe Systems Incorporated
    All Rights Reserved.

    NOTICE:  All information contained herein is, and remains
     the property of Adobe Systems Incorporated and its suppliers,
     if any.  The intellectual and technical concepts  contained
     herein are proprietary to Adobe Systems Incorporated and its
     suppliers and may be covered by U.S. and Foreign Patents,
     patents in process, and are protected by trade secret or copyright law.
     Dissemination of this information or reproduction of this material
     is strictly forbidden unless prior written permission is obtained
     from Adobe Systems Incorporated.

 ----------------------------------------------------------------------------------

	File:	IThrowException.cpp
		
	Notes:	Include this file in your plugin project, or write your own custom file.
	
 ---------------------------------------------------------------------------------- */
#include "IThrowException.h"
#include "ATEException.h"

namespace ATE
{

void Throw_ATE_Exception (ATEErr err)
{
    throw Exception(err);
}


}// namespace ATE
