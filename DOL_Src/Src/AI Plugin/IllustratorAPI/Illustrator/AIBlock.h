#ifndef __AIBlock__
#define __AIBlock__

/*
 *        Name:	AIBlock.h
 *   $Revision: 5 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Memory Management Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIBlock.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIBlockSuite			"AI Block Suite"
#define kAIBlockSuiteVersion	AIAPI_VERSION(2)
#define kAIBlockVersion			kAIBlockSuiteVersion

/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	Use the Block Suite to request, release, and resize blocks of memory.

	They are analagous to the standard C library memory allocation routines or
	the Macintosh pointer routines. The Block Suite provides a cross-platform
	way to manage memory. In some situations, Illustrator expects memory allocated
	to use this suite.
*/
typedef struct {

	AIAPI AIErr (*AllocateBlock) ( long size, void **block );
	AIAPI AIErr (*DisposeBlock) ( void *block );
	AIAPI AIErr (*ReallocateBlock) ( void *block, long newSize, void **newBlock );
	AIAPI AIErr (*SizeOfMemoryBlock) ( void *block, long *size );

} AIBlockSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif