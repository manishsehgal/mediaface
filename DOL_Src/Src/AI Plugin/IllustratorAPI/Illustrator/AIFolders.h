#ifndef __AIFolders__
#define __AIFolders__

/*
 *        Name:	AIFolders.h
 *   $Revision: 1 $
 *      Author:	 dmaclach
 *        Date:	   
 *     Purpose:	Adobe Illustrator Folders Suite.
 *
 * Copyright (c) 1986-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#include "AIFilePath.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIFolders.h */

/*******************************************************************************
 **
 **	Suite name and version
 **
 **/

#define kAIFoldersSuite					"AI Folders Suite"
#define kAIFoldersSuiteVersion3			AIAPI_VERSION(3)
#define kAIFoldersSuiteVersion			kAIFoldersSuiteVersion3
#define kAIFoldersVersion				kAIFoldersSuiteVersion

/*******************************************************************************
 **
 **	Constants
 **
 **/

/** @ingroup Errors */
#define kFolderNotFoundErr		'D!FD'


/*******************************************************************************
 **
 **	Types
 **
 **/

/**
	Identifiers for the different folders. See AIFoldersSuite for information.
*/
typedef enum AIFolderType
{
		kAIApplicationFolderType = 0,
		kAIPluginsFolderType = 1,
		kAIPrimaryScratchFolderType = 2,
		kAISecondaryScratchFolderType = 3,
		kAIPreferencesFolderType = 4,
		kAIUserSupportFolderType = 5,
		kAIUserSupportAIFolderType = 6,
		kAIUserSupportAIPluginsFolderType = 7,
		kAIApplicationSupportCommonFolderType = 8,
		kAIApplicationSupportCommonColorFolderType = 9,
		kAIApplicationSupportCommonTypeSupportFolderType = 10,
		kAIApplicationSupportCommonFontsFolderType = 11,
		kAIApplicationSupportCommonFontsReqrdFolderType = 12,
		kAIApplicationSupportCommonFontsReqrdCMapsFolderType = 13,
	    kAIRequiredFontsFolderType = 14,
		kAIFontsFolderType = 15,
		kAIMyDocumentsFolderType = 16,
		kAIApplicationSupportCommonWorkflowFolderType = 17,
		kAIPrinterDescriptionsFolderType = 18,
		kAIRequiredPluginsFolderType = 19,
		kAISettingsFolderType = 20,
		kAIColorTableSettingsFolderType = 21,
		kAIOptimizeSettingsFolderType = 22,
		kAIHelpFolderType = 23,
		kAIRootFolderType = 24,
		kAIPresetsFolderType = 25,
		kAIPresetActionsFolderType = 26,
		kAIPresetBrushesFolderType = 27,
		kAIPresetGradientsFolderType = 28,
		kAIPresetKeyboardShortcutsFolderType = 29,
		kAIPresetPatternsFolderType = 30,
		kAIPresetScriptsFolderType = 31,
		kAIPresetStylesFolderType = 32,
		kAIPresetSwatchesFolderType = 33,
		kAIDictionariesFolderType = 34,
		kAILegalFolderType = 35,
		kAISampleFilesFolderType = 36,
		kAIUtilitiesFolderType = 37,
		kAIPackageFolderType = 38,
		kAIApplicationSupportCommonFontsReqrdBaseFolderType = 39,
		kAIHelpersFolderType = 40,
		kAIPreviewInFolderType = 41,
		kAIStartupFileFolderType = 42,
		kAIRidersFileFolderType = 43,
		kAIHyphenationDictFolderType = 44,
		kAIApplicationSupportCommonPDFLFolderType = 45,
		kAIApplicationSupportCommonPDFL5FolderType = 46,
		kAIApplicationSupportCommonPDFL5CMapsFolderType = 47,
		kAIApplicationSupportCommonPDFL5FontsFolderType = 48,
		kAIApplicationSupportCommonPrintSupportFolderType = 49,
		kAIApplicationSupportCommonColorProfilesFolderType = 50,
		kAIApplicationSupportCommonColorSettingsFolderType = 51,
		kAIContentsFolderType = 52,
		kAIHelpImagesFolderType = 53,
		kAIFontsCMapsFolderType = 54,
		kAIPresetSymbolsFolderType = 55,
		kAITsumeFolderType = 56,
		kAISpellingDictFolderType = 57,
		kAIPresetTemplatesFolderType = 58,
		kAIFontsCFFolderType = 59,
		kAIApplicationSupportCommonKinsokuSetFolderType = 60,
		kAIApplicationSupportCommonMojikumeSetFolderType = 61,
		kAIPresetBlankDocumentsFolderType = 62,
		kAIUserSupportCommonFontsFolderType = 63,
		kAIFontsCFTempFolderType = 64,
		kAILogsFolderType = 65,
		kAISampleArtFolderType = 66,
		kAISampleSVGFolderType = 67,
		kAISampleGraphDesignsFolderType = 68,
		kAISampleDataDrivenGraphicsFolderType = 69,
		kAIWorkspacesFolderType = 70,
		kAIPresetColorBooksFolderType = 71,
		kAIPresetLegacyColorBooksFolderType = 72,
		kAIPresetSwatchExchangeFolderType = 73,
		kAIApplicationSupportCommonLinguisticsFolderType = 74,
		kAIApplicationSupportCommonLinguisticsProvidersFolderType = 75,
		kAIVersionCueFolderType = 76,
		kAIDemonstratorFolderType = 77,
		kAIResourcesFolderType = 78,
		kAICoolExtrasFolderType = 79,
		kAIOutputSettingsFolderType = 80,
		kAIStartupScriptsFolderType = 81,
		kAIDesktopFolderType = 82

} AIFolderType;

/** Types of files that can be acquired by AIFoldersSuite::GetFileName() */
typedef enum
{
	/** Generates a temporary file name. An attempt is made to generate a
		name for a file that does not exist. No check is made to ensure the
		file does not in fact exist. Generating names repeatedly will result
		in different names. */
	kAITemporaryFileType = 1
	
} AIFileType;


/*******************************************************************************
 **
 **	Suite
 **
 **/


/**
	The AIFoldersSuite provides APIs that return the locations of the standard
	folders used by Illustrator. The diagram below shows the layout of these folders
	and the corresponding identifiers.

@verbatim

  Local Hierarchy:
  
  	Adobe Illustrator X:						<- kAIRootFolderType
  		Adobe Illustrator X:					<- kAIPackageFolderType
  			Contents:							<- kAIContentsFolderType
  				Windows/MacOSClassic:			<- kAIApplicationFolderType
  					+The Executable
  					+Shared Libraries
  			Help:								<- kAIHelpFolderType
  				+HTML Help Files
  				images:							<- kAIHelpImagesFolderType
  					+Help Graphics
  			Helpers:							<- kAIHelpersFolderType
  				Preview In:						<- kAIPreviewInFolderType
  			Required:							<- kAIRequiredPluginsFolderType
  				+Required Plugins
  				Fonts:							<- kAIRequiredFontsFolderType
  					+Required Fonts
  		Legal:									<- kAILegalFolderType
  			+EULA
  		Plug-ins:								<- kAIPluginsFolderType - Can be changed By User   kAIStartupFileFolderType/kAIRidersFileFolderType both point here as well currently. This may change in the future.
  			Extensions:
  			Illustrator Filters:
  			Illustrator Formats - Other:
  			Illustrator Formats - Standard:
  			Photoshop Effects:					
  			Photoshop Filters:					
  			Photoshop Formats:					
  			Text Filters:						
  			Tools:								
  			Tsume:								<- kAITsumeFolderType
  		Presets:								<- kAIPresetsFolderType
  			Actions:							<- kAIPresetActionsFolderType
  			Brushes:							<- kAIPresetBrushesFolderType
			Color Books:						<- kAIPresetColorBooksFolderType
				Legacy:							<- kAIPresetLegacyColorBooksFolderType
  			Gradients:							<- kAIPresetGradientsFolderType
  			Keyboard Shortcuts:					<- kAIPresetKeyboardShortcutsFolderType
  			Patterns:							<- kAIPresetPatternsFolderType
  			Save for Web Settings:				<- kAISettingsFolderType
  				Color Tables:					<- kAIColorTableSettingsFolderType
  					+Color tables
  				Optimize:						<- kAIOptimizeSettingsFolderType
  					+Optimize Settings
				Output Settings:
					+Output Settings			<- kAIOutputSettingsFolderType
  			Scripts:							<- kAIPresetScriptsFolderType
  			Styles:								<- kAIPresetStylesFolderType
  			Swatches:							<- kAIPresetSwatchesFolderType + kAIPresetSwatchExchangeFolderType
  				Color Systems:					<- kAIPresetColorSystemsFolderType
  			Symbols:							<- kAIPresetSymbolsFolderType
  			Templates:							<- kAIPresetTemplatesFolderType
  				Blank Documents:				<- kAIPresetBlankDocumentsFolderType
  		Fonts:									<- kAIFontsFolderType
	  		Composite Fonts 					<- kAIFontsCFFolderType
				Temp							<- kAIFontsCFTempFolderType
  			CMaps:								<- kAIFontsCMapsFolderType
  		Sample Files:							<- kAISampleFilesFolderType
  			Graph Designs:						<- kAISampleGraphDesignsFolderType
  			Sample Art:							<- kAISampleArtFolderType
  				SVG:							<- kAISampleSVGFolderType
  				Data-Driven Graphics:			<- kAIDataDrivenGraphicsFolderType
  		StartupScripts:							<- kAIStartupScriptsFolderType
  		Utilities:								<- kAIUtilitiesFolderType
  		
  
  
  System Hierarchies:
  		
  	The diagram below tries to describe the parts of the directory hierarchy of
  	Illustrator 10 that are located under system folders. The locations of such
  	directories are necessarily dependent on the OS. In the diagram a name
  	enclosed in square brackets [...] indicates a system dependent directory.
  	The values for these directories for each system are defined following the
  	diagram.
  
  	[PrimaryScratch]							<- kAIPrimaryScratchFolderType
  		+ primary scratch file location
  	[SecondaryScratch] 							<- kAISecondaryScratchFolderType
  		+ secondary scratch file location
  	[AIPreferences]								<- kAIPreferencesFolderType
  		+ preferences file location
  		+ plugin cache file location
  	[UserSupport]								<- kAIUserSupportFolderType
  		Adobe Illustrator X						<- kAIUserSupportAIFolderType
  			plugins	 							<- kAIUserSupportAIPluginsFolderType
  				+ plugins that are usable by the user who is logged in
  		Fonts									<- kAIUserSupportCommonFontsFolderType
  	[ApplicationSupportCommon]					<- kAIApplicationSupportCommonFolderType
  		Color  									<- kAIApplicationSupportCommonColorFolderType
  			Profiles 							<- kAIApplicationSupportCommonColorProfilesFolderType
  			Settings							<- kAIApplicationSupportCommonColorSettingsFolderType
  		typeSpt									<- kAIApplicationSupportCommonTypeSupportFolderType
  			Kinsoku Sets						<- kAIApplicationSupportCommonKinsokuSetFolderType
  			Mojikume Sets						<- kAIApplicationSupportCommonMojikumeSetFolderType
  		Fonts									<- kAIApplicationSupportCommonFontsFolderType
  			Reqrd								<- kAIApplicationSupportCommonFontsReqrdFolderType
  				Base							<- kAIApplicationSupportCommonFontsReqrdBaseFolderType
  				cmaps							<- kAIApplicationSupportCommonFontsReqrdCMapsFolderType
  		PDFL									<- kAIApplicationSupportCommonPDFLFolderType
  			PDFL5.0 							<- kAIApplicationSupportCommonPDFL5FolderType
  				cmaps 							<- kAIApplicationSupportCommonPDFL5CMapsFolderType
  				fonts							<- kAIApplicationSupportCommonPDFL5FontsFolderType
  		Printspt 								<- kAIApplicationSupportCommonPrintSupportFolderType
  		Workflow 								<- kAIApplicationSupportCommonWorkflowFolderType
  		Linguistics								<- kAIApplicationSupportCommonLinguisticsFolderType
  			Providers							<- kAIApplicationSupportCommonLinguisticsProvidersFolderType
  				Proximity						<- kAIDictionariesFolderType/kAIHyphenationDictFolderType/kAISpellingDictFolderType (all three of these currently point at the same folder. This may change in the future. Please choose the one that is most appropriate for your usage.)
  	[MyDocuments]								<- kAIMyDocumentsFolderType
  		 +default documents file location
  	[PPDs]										<- kAIPrinterDescriptionsFolderType
  		+ ppd location
  	[Logs]										<- kAILogsFolderType
  System Dependant Directories:
   
  	W2k
  	-------
  	PrimaryScratch = System definition of temporary but can be changed by user
  	SecondaryScratch = System definition of temporary but can be changed by user
  	AIPreferences = Documents and Settings\username\Application Data\Adobe\Adobe Illustrator X
  	UserSupport = Documents and Settings\username\Application Data\Adobe
  	ApplicationSupportCommon = Program Files\Common Files\Adobe
  	MyDocuments = Documents and Settings\username\My Documents
  	PPDs = unknown
  	Logs = Documents and Settings\username\Application Data\Adobe\Logs
  	kAIResourcesFolderType = kAIApplicationFolderType
  	kAIDesktopFolderType = 
  	    
  	OSX
  	---
  	PrimaryScratch = System definition of temporary but can be changed by user
  	SecondaryScratch = System definition of temporary but can be changed by user
  	AIPreferences = ~/Library/Preferences/Adobe Illustrator X
  	UserSupport = ~/Library/Application Support/Adobe
  	ApplicationSupportCommon = /Library/Application Support/Adobe
  	MyDocuments = ~/Documents
  	PPDs = Libraries/Printers/PPDs
	Logs = ~/Library/Logs/Adobe Illustrator CS
	kAIResourcesFolderType = kAIApplicationFolderType/Resources
	kAIDesktopFolderType = ~/Desktop
	
@endverbatim
*/
struct AIFoldersSuite {

	/** Get the location of the folder. If createFolder is true it will be created if it
		does not already exist. */
	AIAPI AIErr (*FindFolder)(AIFolderType type, AIBoolean createFolder, ai::FilePath &folder);
	/** Get the name of the folder. The name buffer must be of size #kMaxPathLength. Only
		the name of the folder is returned, not the full path.*/
	AIAPI AIErr (*GetFolderName)(AIFolderType type, ai::UnicodeString &name);

	/** Get the file of the specified type. */
	AIAPI AIErr (*GetFileName)(AIFileType type, ai::FilePath &file);

};


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
