/**

	AIPluginGroup.h
	Copyright (c) 1995-2004 Adobe Systems Incorporated.

	Adobe Illustrator 12.0 Plugin Group Suite.

 **/

#ifndef __AIPluginGroup__
#define __AIPluginGroup__

/** @file AIPluginGroup.h */

/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif
#ifndef __AIPlugin__
#include "AIPlugin.h"
#endif
#ifndef __AIArt__
#include "AIArt.h"
#endif
#ifndef __AIPathStyle__
#include "AIPathStyle.h"
#endif
#ifndef __AIArtStyle__
#include "AIArtStyle.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIPluginGroupSuite				"AI Plugin Group Suite"
#define kAIPluginGroupSuiteVersion4		AIAPI_VERSION(4)
#define kAIPluginGroupSuiteVersion		kAIPluginGroupSuiteVersion4
#define kAIPluginGroupVersion			kAIPluginGroupSuiteVersion


// These are the AI 6 style plugin class messages.
// They are only needed temporarily until Joe T's prototype plugins are updated
#define kSelectorAINotifyObject		"AI Notify Plugin Object"
#define kSelectorAIUpdateObject		"AI Update Plugin Object"


// These are the AI 7 style plugin group messages (split into caller and request)

/** @ingroup Callers */
#define kCallerAIPluginGroup		"AI Plugin Group"

/** @ingroup Selectors
	Notifies of edits to the edit group */
#define kSelectorAINotifyEdits		"AI Notify Edits"
/** @ingroup Selectors
	Notifies that the resultGroup needs to be rebuilt */
#define kSelectorAIUpdateArt		"AI Update"
/** @ingroup Selectors
	Can layers palette show edit group? */
#define kSelectorAICanShowContents	"AI Show Contents"
/** @ingroup Selectors
	Interpolate plugin data and dictionary contents
	For details, see comment on #kPluginGroupBlendAsPluginGroup option */
#define kSelectorAIInterpolateData	"AI Interpolate Data"
/** @ingroup Selectors
	Asks a plugin group to replace symbols used in its dictionary.
	For details, see AIReplaceSymbolMessageData typedef */
#define kSelectorAIReplaceSymbol	"AI Art Replace Symbol"
/** @ingroup Selectors
	Asks a plugin group whether it allows art objects of a given type in its edit group.
	For details, see AIPluginArtTypeMessageData typedef */
#define kSelectorAIDisallowsArtTypeQuery	"AI Plugin Group Disallow Art Type"
/** @ingroup Selectors
	Asks plugin to perform a custom hit test.
	For details, see AIPluginCustomHitTestMessageData typedef */
#define kSelectorAIPerformCustomHitTest		"AI Plugin Group Perform Custom Hit Test"
/** @ingroup Selectors
	Asks plugin to collect the paint styles of all objects it privately manages the styles for.
	Will only be sent if the option kPluginGroupManagesPaintStyles is on.
	For details, see #AIPluginGroupCollectPaintStylesData typedef */
#define kSelectorAICollectPaintStyles		"AI Plugin Group Collect Paint Styles"
/** @ingroup Selectors
	Asks plugin to apply the paint styles to all [selected] objects it privately manages the styles for.
	Includes requests to merge partially specified styles or to transform style attributes by a matrix.
	Will only be sent if the option kPluginGroupManagesPaintStyles is on.
	For details, see #AIPluginGroupApplyPaintStylesData typedef */
#define kSelectorAIApplyPaintStyles		"AI Plugin Group Apply Paint Styles"
/** @ingroup Selectors
	Asks plugin to apply a client-specified color mapping callback function to all [optionally selected]
	objects it privately manages the styles for. For details, see #AIPluginGroupAdjustColorsData typedef.

	Will only be sent if the option kPluginGroupManagesPaintStyles is on, and either the selection is
	irrelevant or the plugin group is at least partially selected in object selection mode. See
	also the kAIPluginSelectionApplyAttributes selector in (Adobe internal suite) AIPluginSelection
	for when the plugin group manages its own selection mode. */
#define kSelectorAIAdjustColors		"AI Plugin Group Adjust Colors"


/** These are the options for AddAIPluginGroup (they can't be changed after the class is defined). */
enum AIPluginGroupOptions {
	/** If on, an AI Update message will be triggered by edits to non-descendant objects
		that overlap the bounds of the editArt. This is intended for objects that
		perform lens-like effects, distorting whatever they are moved on top of, or that
		exhibit avoidance behavior, moving away from whatever is moved on top of them. */
	kPluginGroupWantsOverlapUpdateOption	= (1L<<1),
	/** If on, and the entire pluginGroup is translated as a whole, the resultGroup will
		be transformed as well, and an AI Update message will not be sent. */
	kPluginGroupWantsAutoTranslateOption	= (1L<<2),
	/** If on, and the entire pluginGroup is scaled uniformly, the resultGroup will
		be transformed as well, and an AI Update message will not be sent. */
	kPluginGroupWantsAutoUScaleOption		= (1L<<3),
	/** If on, and the entire pluginGroup is transformed (by any matrix) as a whole, the
		resultGroup will be transformed as well, and an AI Update message will not be sent.
		
		The #kTransformOperationCode "AI Notify Edits" message will still be sent,
		just to let the client know what's been done, but a zero return from the
		auto-handled operation types will be treated as if #kDontCarePluginGroupReply
		were returned, so it is not necessary to catch the AI Notify Edits message
		if you don't want to do anything with it. */
	kPluginGroupWantsAutoTransformOption	= (1L<<4),
	/** If on, hit testing (by other tools than the plugin's own tool, which can control
		its own hit testing in detail) will do fill hit testing on the edit group. By
		default, the edit group is only outline-hit, since objects in the edit group
		are not previewed. But many plugin groups will copy the edit group contents
		into the result group and then add more stuff, making it appear that the edit
		group paths are previewed. Those plugins will want to do fill hit testing on
		the edit group in order to enable direct selecting individual editable paths
		with a fill click. (Note that if you turn this on, and there are some edit group
		objects whose fills or strokes are NOT copied into the result group, you must
		catch the style change notifications and make sure that you turn off any fill
		or stroke paint that the user has given to those objects. Otherwise you will
		allow hitting on elements that aren't being painted.)
		See also kPluginGroupForceFillHitOnClosedPaths. */
	kPluginGroupDoEditFillHitOption			= (1L<<5),
	/** If off (the default), when a user edit removes all objects from the edit group,
		the whole plugin group object will be deleted. (This is the same way that
		normal groups behave.) If on, the plugin group will remain in the artwork
		even after its edit group has been emptied. This could be used for objects
		that build their resultArt entirely based on data and external context. */
	kPluginGroupKeepWhenEmptyOption			= (1L<<6),
	
	/** If on, the objects in the result group act much like a fill of the
		object(s) in the edit group. (E.g., ink hatching could be implemented
		this way.) This flag is intended mainly for informing the behavior of
		other plugins besides the one managing the plugin group class. For
		example, an Expand Fill filter could expand plugin groups if this flag
		is on. Or a Fill Color could refuse to assign a pattern or gradient fill
		to the edit group objects, since the result group is acting as the
		fill. (Assigning solid color should be permitted, since fill-like
		plugin groups will often colorize their fills.) Default is off. See
		also kPluginGroup*/
	kPluginGroupIsFillLike					= (1L<<7),
	/** If on, the objects in the result group act much like a stroke of the
		object(s) in the edit group. (E.g., brushes would turn on this flag.)
		This flag is intended mainly for informing the behavior of other plugins
		besides the one managing the plugin group class. For example, an Expand
		Stroke filter could expand plugin groups if this flag is on. Or the
		Stroke palette could ignore stroke attributes, since the result group
		is acting as the stroke. Default is off.

		(If both #kPluginGroupIsFillLike and #kPluginGroupIsStrokeLike are off,
		the result group is treated as being discrete objects which are related
		to the edit group in some way other than as a fill or stroke effect.
		An example would be path interpolation Blends.) */
	kPluginGroupIsStrokeLike				= (1L<<8),

	/**	If on, then the plugin group object can never be targeted. The app always recurses
		into the contents of the edit group to find targeted objects regardless if the plugin
		group object is fully selected. Effectively, this prevents users from targeting the
		plugin group and applying styles, transparency or effects to that level. */
	kPluginGroupDoNotTarget					= (1L<<9),
	/**	If on, then selecting any piece inside the edit group will target the plugin group level
		automatically. Targeting behavior for this plugin group will be similar to that of
		compound paths. Fill/stroke changes will apply to the plugin group level, and the "Contents"
		field will not appear in the appearance palette. (Compound shapes plugin group uses this
		option.) Users will still be able to target the objects inside the edit group by
		manually targeting them in the layers palette. */
	kPluginGroupAlwaysSmartTarget			= (1L<<10),
	/**	If on, then the plugin group will not be targeted when fully selected. Users will have to
		manually target the plugin group through the layers palette.
		Clients should avoid setting both this option and the #kPluginGroupAlwaysSmartTarget on the
		same plugin group, otherwise undefined behaviors will result. */
	kPluginGroupDoNotSmartTarget			= (1L<<11),

	/**	If this option is on, the application will allow plug-in objects
		of the given plug-in group to be made clipping.  What it means for
		a plug-in object to be clipping is that its result art is used as
		a clip (provided the type of the result art makes this possible). */
	kPluginGroupCanBeClipping				= (1L<<12),

	/**	If on, layers palette will always show the contents of the edit group. */
	kPluginGroupAlwaysShowContents			= (1L<<13),
	/**	If on, layers palette will query the plugin group with the #kSelectorAICanShowContents
		message, asking if it is okay to expand the plugin group. Plugin groups should
		reply with #kRefusePluginGroupReply if it is _not_ okay to expand. */
	kPluginGroupAskToShowContents			= (1L<<14),

	/** This option together with #kPluginGroupBlendAsPluginGroup defines how plugin groups other than
		blends will act when they appear inside a blend. This one is tested first.

		If the selection contains no plugin groups with the #kPluginGroupDisableBlends
		flag on, then plugin groups that are blended to anything other than another
		plugin group of the same class with the #kPluginGroupBlendEditGroups flag on
		will be treated as if they were their result group. The interpolated objects
		will be ordinary groups.

		If on and the selection includes any plugin group with this flag on, the Make Blend
		command is disabled. Also, plugin groups with this option on will not respond to
		clicks of the Blend tool. */
	kPluginGroupDisableBlends				= (1L<<15),
	/** If the blend includes two adjacent plugin groups of the same plugin class,
		and it has this option on, then the interpolated objects will also be
		objects of the same plugin class. The contents of the edit groups of
		the two plugin objects are blended as if they were ordinary groups,
		and then the plugin is given a chance to interpolate the plugin group
		data and dictionary contents, and finally the interpolated plugin groups
		are sent the AIUpdateArt message.

		Details on the #kSelectorAIInterpolateData message:

		This message uses the AIOperationTime field of the AIPluginGroupMessage.
		It is not sent at #kCheckOperationTime; the #kBeforeOperationTime message
		can do double-duty as a check operation.

		It also uses the 'preEditArt' and 'postEditArt' fields, with a different
		meaning than is used by the AI Notify Edits message. At all three times,
		the preEditArt is the plugin group being blended 'from' (the one closest
		to t=0) and the postEditArt is the plugin group being blended 'to' (the
		one closest to t=1.) The 'art' field is only used at #kDuringOperationTime.

		#kBeforeOperationTime:
			If the plugin returns #kRefusePluginGroupReply, the interpolated steps
			will be ordinary groups blending the result groups, just as if the
			#kPluginGroupBlendAsPluginGroup option was off or the plugin groups
			were of different classes. No further messages will be sent. (This
			lets the plugin decide whether the two groups are blendable as plugin
			groups based on their data or contents.)

			If the plugin returns #kDontCarePluginGroupReply, the edit groups
			will be blended, and then the AIUpdateArt message will be sent.
			There will be no #kDuringOperationTime or #kAfterOperationTime messages.
			The intermediate objects will be of the same plugin group class.
			Their plugin data and dictionary contents will be duplicated from
			the 'postEditArt' object. This reply is mainly for plugin groups
			that do everything identically for all of their objects and do not
			any extra data. It could also be returned when the plugin determines
			that the two objects have the same data so that copying the data
			for the postEditArt will work for all of the intermediate objects.

			If the two plugin groups are deemed blendable as plugin groups,
			and the plugin wants a chance to interpolate their data, it should
			return either kNoErr or #kWantsAfterMsgPluginGroupReply, after
			optionally setting up any data it will need for the #kDuringOperationTime
			messages.

			The #kBeforeOperationTime message is the time to do anything
			that you want to do only once per blend, instead of once per step,
			such as finding and recording the commonalities, differences, and
			blendable aspects of the two objects. This temporary data can be
			stored in the message's AIInterruptData field, or attached to the
			preEditArt and postEditArt objects.

		#kDuringOperationTime:
			This is sent only when the #kBeforeOperationTime message returns
			either kNoErr or #kWantsAfterMsgPluginGroupReply. The blend will,
			for each step, create a plugin group of the same class, blend
			the edit group contents as if they were ordinary groups, and
			pass that interpolated object as the 'art' field in the message
			data. The 't' field of the message indicates what step the
			interpolation is at. The interpolated objects will be initially created with empty dictionaries
			and no plugin data. This is the time for the plugin to attached
			to them any interpolated data.

		#kAfterOperationTime:
			This is sent after all the interpolated steps have been created,
			if the #kWantsAfterMsgPluginGroupReply was returned from any of the
			#kBeforeOperationTime or #kDuringOperationTime notifications. It
			gives the plugin a chance to release any data it may have set up.

		Any reply other than those mentioned above will be treated as an error
		and cause the blend not to be created at all. */
	kPluginGroupBlendAsPluginGroup			= (1L<<16),

	/** If on, the plugin group restricts which types of objects can be inserted into its edit group, and
		should handle the #kSelectorAIDisallowsArtTypeQuery message. (Note that this differs from the doing
		a kBeforeOperationTime NotifyEdits check, in that the latter passes in a specific prospective contents
		object to a specific plugin group, and asks whether that object can be inserted. The #AIPluginArtTypeMessageData
		does not have a specific art object, only an art type, and can therefore be used for such purposes
		as disabling tools  or commands that might otherwise create a new object of that type, or giving cancel
		feedback on mouse over, etc.) */
	kPluginGroupRestrictsArtTypes			= (1L<<17),

	/** The following two options provide a lighter-weight less flexible version of customizing fill hit
		testing. If on, the plugin group will be treated as if it supplied fill paint on either all paths
		or all closed paths, so that selection tools will select those paths in the edit group when clicking
		in their interiors, **even if they have no fill paint of their own and no ancestor art style supplies
		fill paint.**  Edit fill hit testing is done only after outline testing fails to hit anything in
		the whole edit group, so front contents do not obscure outlines of back contents. It is also done
		only if something is hit in the result group, so that paths that are unfilled and that do not
		overlap any filled region will not be selected. 

		If on, this option takes precedence over #kPluginGroupDoEditFillHitOption. The latter option
		means that edit group objects are copied to the result group, possibly with different colors
		but preserving none/some status. Whereas this option means that the plugin group has private
		data that it uses to supply fills. (Depending on which option is turned on, either for all paths
		or for closed paths only.)

		Note that this option will not give ideal behavior if the plugin group supplies private fills
		on only some parts of the edit group, or if the fill geometry is transformed. In that case,
		the custom hit testing can allow more customization, at the cost of requiring more work on the
		part of the plugin. */
	kPluginGroupForceFillHitOnClosedPaths			= (1L<<18),
	kPluginGroupForceFillHitOnAllPaths				= (1L<<19),

	/** Indicates that paint styles applied to content of this plugin group should not be obtained or set
		by accessing the art styles of objects in the edit group, but instead by asking the plugin group to
		get or set the paint styles attached to objects that it considers selected. This should be used if the
		art styles attached to selected objects in the edit group do not reflect what the user would think of
		as the selected styles. (Recall that the contents of the edit group	are not previewed, so the styles 
		in the result group are what the user sees, and they may not correspond	directly to the styles used 
		in the edit group.) It is also useful for plugin groups that manage selection state in a way that does
		not map directly onto object selection within the edit group.

		Note that this flag is not compatible with any of the target flags (eg. kPluginGroupDoNotTarget,
		kPluginGroupAlwaysSmartTarget, and kPluginGroupDoNotSmartTarget).

		When enabled, the plugin group will receive the kSelectorAICollectPaintStyles message when
		the app would normally iterate the art styles attached to children of the edit group. The plugin group
		will also receive the kSelectorAIApplyPaintStyles message when the app would normally set the art
		styles attached to children of the edit group.

		For further details see the #AIPluginGroupCollectPaintStylesData and #AIPluginGroupApplyPaintStylesData
		structs. */
	kPluginGroupManagesPaintStyles	= (1L<<20)
};


// These are the errors returned by calls to AIPluginGroup suite functions (other than the AIArt standards).

/** @ingroup Errors 
	Plugin Group not found (looked up by name). */
#define kUnknownPluginGroupErr			'GRP?'
/** @ingroup Errors 
	Plugin Group already attached to this object. */
#define kAttachedPluginGroupErr			'GRP!'
/** @ingroup Errors 
	SetDataCount too high (just to be more helpful than #kBadParameterErr). */
#define kTooMuchDataPluginGroupErr		'FULL'

/** The limit on the size of the data store. */
#define kPluginStoreLimit			( 32 * 1024L )



/** @ingroup Errors
	For #kCheckOperationTime or #kSelectorAIDisallowsArtTypeQuery: Plugin Group says the edit shouldn't happen */
#define kRefusePluginGroupReply			'RFSE'
/** @ingroup Errors
	This code should be returned if the plugin group wants to get a kSelectorAINotifyEdits message with
	#kCheckOperationTime to refine the answer to a query sent by some lighter-weight message.
	(As of AI 12, the only message that checks for this reply is #kSelectorAIDisallowsArtTypeQuery,
	but the reply code is potentially more general.) */
#define kCheckPluginGroupReply			'CHEK'
/** @ingroup Errors
	For #kBeforeOperationTime: Plugin Group wants to get a #kAfterOperationTime message (it will be
	sent before the #kSelectorAIUpdateArt message) */
#define kWantsAfterMsgPluginGroupReply	'AFTR'
/** @ingroup Errors
	For #kAfterOperationTime: Plugin Group has already updated the object. Note about the difference
	between this reply and AIPluginGroupSuite::MarkPluginArtClean():

	MarkPluginArtClean() is intended for use within a GoMenu or AITool handler, or a notification that
	comes from some other suite. The default behavior, when an #kSelectorAINotifyEdits returns kNoErr,
	is to mark the plugin object as "dirty" (in need of #kSelectorAIUpdateArt.) So if MarkPluginArtClean()
	were called from within an #kSelectorAINotifyEdits handler, it would get marked dirty again as soon
	as the handler returns, without a reply code to prevent that. */
#define kMarkValidPluginGroupReply		'VALD'
/** @ingroup Errors
	For #kAfterOperationTime: the edit is irrelevant to this plugin (e.g., changing the "show center"
	attribute or locking editArt contents rarely affects the resultArt) Keeps prior state, doesn't
	mark it either clean or dirty. */
#define kDontCarePluginGroupReply		'MOOT'
/** @ingroup Errors
	Only meaningful as a return for #kAfterOperationTime message with #kReadOperationCode, it means
	to destroy the plugin group. New for AI9 */
#define kDestroyPluginGroupReply		'TRSH'
/** @ingroup Errors
	Only meaningful as a return for #kSelectorAIPerformCustomHitTest, it means the custom
	hit test wants to report a hit, returning kNoErr means a hit will not be reported */
#define kCustomHitPluginGroupReply		'CHIT'

/** @ingroup Errors
	This code should be returned from an #kSelectorAIReplaceSymbol message if the plugin group really
	performed the requested replacement. */
#define kDidSymbolReplacement			'dsRP'


/** The #kSelectorAINotifyEdits message specifies an operation time that indicates the
	relationship of the notification to the stage of the edit operation. The times are:

	- #kCheckOperationTime
	- #kBeforeOperationTime
	- #kAfterOperationTime
	- #kDuringOperationTime
*/
typedef char *AIOperationTime;

// the times themselves

/** Does the plugin allow this operation? */
#define kCheckOperationTime			"AI Check Operation Time"
/** Prepare for the operation to occur */
#define kBeforeOperationTime		"AI Before Operation Time"
/** The operation has completed */
#define kAfterOperationTime			"AI After Operation Time"
/** The operation is ongoing - used only by AI Interpolate Data */
#define kDuringOperationTime		"AI During Operation Time"


/** The #kSelectorAINotifyEdits message specifies an operation code that indicates the type of
	editing operation being performed.

	These are code values which refer only to the entire pluginGroup object:
	
	- #kReadOperationCode
	- #kWriteOperationCode
	- #kAttachOperationCode
	- #kExpandOperationCode

	These are AIOperationCode values for which the editing parameters are valid
	(the edit group or one of its descendants has been or will be edited.)

	The editing parameters are valid only during #kCheckOperationTime and
	#kBeforeOperationTime, where one AI Notify Edits message is sent for
	each top-level affected/selected descendant of the plugin object. E.g.,
	if an entire group is selected, but its parent is not fully selected,
	then that group will be passed in as the preEditArt, but not its children.

	If the plugin returns #kWantsAfterNotifyPluginGroupReply from a #kBeforeOperationTime
	AI Notify Edits message, then a single #kAfterOperationTime message will be sent
	at the end of the transaction, just before the Undo recording is closed, and
	before the AI Update message is sent. (Its code will always be #kChangeOperationCode,
	regardless of whether a more specific code was sent during the #kBeforeOperationTime
	phase.) This is the appropriate place to make any changes to the editArt.

	Important note: NOT EVERY EDIT to children of the editArt triggers an
	AI Notify Edits message! (Every edit will cause an AI Update message, though.)
	This mechanism is not intended to describe everything that can be done to
	an art object. It is intended mainly for highly structured objects where
	the different descendants have specific "roles" to play in the plugin.
	Graphs are a good example, even though they are not currently Plugin Group
	objects. (In fact, the choice of edit message parameters includes all those
	which would be necessary to implement the existing Graph behavior as a
	a Plugin Group.)

	Another example is Blend objects, where moving a spine point should cause
	the objects to move onto the spine, but moving a blend component should cause
	the spine point to move with the art object. By just looking at the "post edit"
	state of the editArt, it would be hard to detect the difference between those
	two edits, since in both of them the only easily detected discrepancy, without
	knowing which paths were edited, is that the art elements are off the spine.
	Thus, those kinds of basic edits which are classifiable enough that plugins
	have a likelihood of wanting to respond to them in specific ways will trigger
	an AI Notify Edits message. Changes which are harder to generalize are left
	to AI Update to handle.

	- #kOrderingOperationCode
	- #kTransformOperationCode
	- #kGeometryOperationCode
	- #kTopologyOperationCode
	- #kDeleteOperationCode
	- #kAttributeOperationCode
	- #kStyleOperationCode
	- #kChangeOperationCode
*/
typedef char *AIOperationCode;

// the codes themselves

/** Mainly intended for translating data from external to internal representation at
	#kAfterOperationTime. */
#define kReadOperationCode			"AI Read Operation Code"
/** Mainly intended for translating data from internal to external representation at
	#kBeforeOperationTime, and back to internal at #kAfterOperationTime. */
#define kWriteOperationCode			"AI Write Operation Code"
#define kAttachOperationCode		"AI Attach Operation Code"
/** Only sent at #kBeforeOperationTime (since after an Expand the object no longer exists.)
	The plugin art object is about to be expanded, that is, replaced by its resultArt
	contents. The intention is not for the plugin to DO the Expand, but to remove from
	the resultArt descendants any AITags or other state specific to the plugin, or to
	remove from the plugin's global data anything that "tracks" this art object. */
#define kExpandOperationCode		"AI Expand Operation Code"
/** Send to front, back, forward, backward. See #AIOrderingOperationSubcodes for
	subcode values. */
#define kOrderingOperationCode		"AI Ordering Operation Code"
/** Matrix operations on whole objects. See #AITransformOperationSubcodes for subcode
	values. */
#define kTransformOperationCode		"AI Transform Operation Code"
/** Segments of a path have been modified. See #AIGeometryOperationSubcodes for subcode
	values. */
#define kGeometryOperationCode		"AI Geometry Operation Code"
/** A single object is being replaced by multiple. Also covers paste into editArt.
	See #AITopologyOperationSubcodes for subcode values.

	(Note: A join of two paths is indicated by a Geometry of one followed by a Delete
	of the other, not a Topology. The reason is that the preEditArt must always be a
	single existing descendant of the editArt group.) */
#define kTopologyOperationCode		"AI Topology Operation Code"
/** An element of the editArt group is being deleted */
#define kDeleteOperationCode		"AI Delete Operation Code"
/** Lock, hide, unlock, unhide. See #AIAttributeOperationSubcodes for subcode values. */
#define kAttributeOperationCode		"AI Attribute Operation Code"
/** Path style or text style modifications. See #AIStyleOperationSubcodes for subcode
	values. */
#define kStyleOperationCode			"AI Style Operation Code"
/** Generic or mixed (mainly for #kAfterOperationTime) */
#define kChangeOperationCode		"AI Change Operation Code"


// Subcodes are four-byte chars like AIErr, to facilitate debugging while still allowing switch statements

/** Subcode values for #kChangeOperationCode (only at #kAfterOperationTime) */
enum AIChangeOperationSubcodes {
	/** Something in the edit group has been modified by plugin calls, and we are about
		to pop the API context. There will have been no Before notification. Do not
		modify anything outside of the plugin object itself when handling this message,
		because it is called inside a loop that is processing the artwork tree. */
	kPluginSyncArtOpCode	= 'PLUG'
};

/** Subcode values for #kOrderingOperationCode */
enum AIOrderingOperationSubcodes {
	kSendToBackOpCode		= 'BACK',
	kSendToFrontOpCode		= 'FRNT',
	kSendBackwardOpCode		= 'DOWN',
	kSendForwardOpCode		= 'UPWD',
	kReverseGroupOpCode		= 'RVRS',

	/** preEditArt is in the plugin group, and is being moved to another location which is also inside the plugin group.
		See message.paintOrder and message.destinationArt for the destination specification.
		This opcode is used only if the movement is not equivalent to one of the more specific commands above. */ 
	kSendInternalOpCode		= 'SINT',

	/** preEditArt is in the plugin group, and is about to be reordered to a location outside the plugin group.
		See message.paintOrder and message.destinationArt for the destination specification.
		(This is a good time to remove any private dictionary entries from preEditArt.) */
	kReorderOutCode			= 'MVOT',

	/** preEditArt is OUTSIDE the plugin group, and is about to be reordered to a location inside the plugin group,
		as specified by message.destinationArt and message.paintOrder.
		(This can usually be processed the same as a kPasteIntoOpCode kTopologyOperationCode.) */
	kReorderIntoOpCode		= 'MVIN'
};

/** Subcode values for #kTransformOperationCode */
enum AITransformOperationSubcodes {
	kTranslationOpCode		= 'MOVE',
	kRotationOpCode			= 'ROTA',
	kUniformScaleOpCode		= 'USCA',
	kNonUniformScaleOpCode	= 'NSCA',
	kShearOpCode			= 'SHER',
	kReflectionOpCode		= 'REFL',
	kMixedTransformOpCode	= 'MTFM'
};

/** Subcode values for #kGeometryOperationCode */
enum AIGeometryOperationSubcodes {
	kMiscPathChangeOpCode	= 'GEOM',
	/** currently unused */
	kMovePointsOpCode		= 'MPTS',
	/** currently unused */
	kAdjustControlsOpCode	= 'ADJC',
	/** currently unused */
	kAddPointsOpCode		= '+PTS',
	/** currently unused */
	kRemovePointsOpCode		= '-PTS',
	/** currently unused */
	kReverseSegmentsOpCode	= 'RVRS',
	/** currently unused */
	kOpenCloseOpCode		= 'OPCL',
	/** currently unused */
	kSwapSymbolsOpCode      = 'SWSM'
};

/** Subcode values for #kTopologyOperationCode */
enum AITopologyOperationSubcodes {
	/** one path is replaced by two */
	kScissorOpCode			= 'SCIZ',
	/** The postEditArt or its children will be inserted into the editArt below preEditArt (which will equal destinationArt.)
		(If postEditArt is a group, its children will be inserted. If postEditArt is not a group, that object will be inserted.) */
	kPasteBelowOpCode		= 'PBEL',
	/** The postEditArt or its children will be inserted into the top of preEditArt (which will equal destinationArt.) */
	kPasteIntoOpCode		= 'PINT',
	/** The postEditArt or its children will be inserted into the editArt below preEditArt (which will equal destinationArt.) */
	kPasteAboveOpCode		= 'PABV',
	/** postEditArt is a transformed copy of preEditArt. Generally it will be inserted above preEditArt. The destinationArt
		and insertionOrder fields can be checked for more details on the insertion location. */
	kTransformCopyOpCode	= 'TCOP',
	/** preEditArt will be changed into a type path */
	kTypePathChangeOpCode	= 'TYCH',
	/** currently unused */
	kReplaceOneOpCode		= 'REP1',
	/** currently unused */
	kReplaceByManyOpCode	= 'REPM',
	/** postEditArt is a non-transformed copy of preEditArt. It will be inserted according to destinationArt and insertionOrder.
		For this operation code, the destinationArt is either the editArt group or a descendant. */
	kDuplicateInsideOpCode		= 'DUPi',
	/** postEditArt is a copy of preEditArt. It will be inserted according to destinationArt and insertionOrder.
		For this operation code, destinationArt is outside the plugin group. This is sent only at kAfterOperation time,
		and the main intent of this notification is to allow the plugin to remove any private tags or dictionary entries
		from the postEditArt. */
	kDuplicateOutsideOpCode		= 'DUPo'
};

/** Subcode values for #kStyleOperationCode */
enum AIStyleOperationSubcodes {
	kPathStyleOpCode		= 'PATH',
	/** currently unused */
	kTextStyleOpCode		= 'ATET',
	/** currently unused */
	kArtStyleOpCode			= 'ARTS'
};

/** Subcode values for #kAttributeOperationCode */
enum AIAttributeOperationSubcodes {
	kHideOpCode				= 'HIDE',
	kShowOpCode				= 'SHOW',
	kLockOpCode				= 'LOCK',
	kUnlockOpCode			= 'UNLK'
};

/*******************************************************************************
 **
 ** Types
 **
 **/

// This is a reference to a plugin group. It is never dereferenced.
typedef struct _t_AIClassOpaque *AIPluginGroupHandle;



typedef void *AIInterruptData;
typedef ASErr (*AIInterruptProc)( AIArtHandle art, AIInterruptData data );

typedef union {
	AIPathStyle			*path_s;
	AIArtStyleHandle	art_s;			// currently unused
} StyleUnion;

typedef union {
	AIPathStyleMap	*path_s_map;
} StyleMapUnion;


/** 
	This is the message structure sent with the #kSelectorAINotifyEdits, #kSelectorAIUpdateArt,
	#kSelectorAICanShowContents and #kSelectorAIInterpolateData selectors.

	The #kSelectorAINotifyEdits notifies of edits to the edit group. In this case the time
	and code members of the message provide more information about the stage of the editing
	operation and the operation being performed. Additional members provide information
	about what is being edited, how it is being edited and the replacement art that will
	be the edited result. Plugin groups can process this information and return one of the
	following special error codes to control the operation:

	- #kRefusePluginGroupReply
	- #kWantsAfterMsgPluginGroupReply
	- #kMarkValidPluginGroupReply
	- #kDontCarePluginGroupReply
	- #kDestroyPluginGroupReply

	The #kSelectorAIUpdateArt message is sent to request that the plugin group manager
	rebuild the result group in response to edits to the edit group. In this case the
	only 
*/
typedef struct {
	SPMessageData		d;
	AIPluginGroupHandle	entry;
	/** The pluginGroup art being notified */
	AIArtHandle			art;

	/** Used only by #kSelectorAINotifyEdits and #kSelectorAIInterpolateData messages */
	AIOperationTime		time;
	/** Used only by #kSelectorAINotifyEdits message. A code defining the operation
		being performed. */
	AIOperationCode		code;

	// Editing parameters

	/** When being notified of an edit preEditArt is the descendant of the editArt group that's
		about to be edited. */
	AIArtHandle			preEditArt;

	/** When being notified of an edit, postEditArt is what will be in the edit group when the
		operation completes. For edits that change attributes but keep the same AIArtHandle (e.g.,
		most style operations), postEditArt will equal preEditArt, and the attributes will not
		have been modified yet at #kBeforeOperationTime. If postEditArt is NOT equal to preEditArt,
		then postEditArt is generally not in the editArt group until #kAfterOperationTime, but any
		transformation, geometry changes or attribute changes will generally have already been
		performed on the postEditArt, it just won't have been put into the artwork..

		WARNING: it is highly illegal to dispose or reorder any artwork objects during a
		#kCheckOperationTime or #kBeforeOperationTime notification. Any editing must wait
		until #kAfterOperationTime. (I say "highly illegal" because we don't have the mechanism
		to keep it from happening should an editing API be called. It would happen, and then
		the engine would go ahead and try to complete the notified operation on the original
		objects, not knowing they had been destroyed or moved, and probably crash.) */
	AIArtHandle			postEditArt;

	/** Many of the edit notification operation codes define subcodes that further refine
		the operation being performed. If so the subcode is supplied in this member. See
		the specific operation codes for their associated subcodes. */
	AIErr				subCode;
	/** Supplied if code == #kTransformOperationCode or subCode == #kTransformCopyOpCode */
	AIRealMatrix		matrix;
	/** Supplied if code == #kStyleOperationCode */
	StyleUnion			style;
	/** Supplied if code == #kStyleOperationCode (if null all style fields are known) */
	StyleMapUnion		styleMap;

	/** Used only by #kSelectorAIUpdateArt and #kSelectorAIInterpolateData */
	AIInterruptProc		proc;
	/** Used only by #kSelectorAIUpdateArt and #kSelectorAIInterpolateData */
	AIInterruptData		data;

	// ----New fields added in AI10-----

	/** Used only by #kSelectorAIInterpolateData. At #kBeforeOperationTime or #kAfterOperationTime,
		the number of steps in the blend. At #kDuringOperationTime, the number of this step (first
		step will be 1, not 0) */
	int					steps;
	/** Used only by #kSelectorAIInterpolateData. A value between 0 and 1. 0 = at preEditArt, 1 = at
		postEditArt */
	AIReal				t;

	// Editing parameter
	
	/** If code == #kGeometryOperationCode & subcode == #kSwapSymbolsOpCode 'art' is a symbol
		instance inside the edit group which is currently an instance of some other symbol, and
		is about to become an instance of toSymbol. (If your plugin group uses symbols in its
		art dictionary, see also the #kSelectorAIReplaceSymbol message.) */
	AIPatternHandle		toSymbol;

	// ----New fields added in AI12----

	/** Supplied if code == #kTransformOperationCode or subCode == #kTransformCopyOpCode.
		The values are AITransformArtOptions from the AITransformArt.h file. */
	long transformFlags;
	/** Supplied if code == #kTransformOperationCode or subCode == #kTransformCopyOpCode.
		If transformFlags contains kScaleLines, then this is the factor by which the strokes
		are going to be scaled. */
	AIReal transformLineScale;

	/** For reordering, duplicating, and pasting operations, this is the object that the new object will be
		inserted relative to. Interpreted along with insertionOrder the same as in AIArt's NewArt, DuplicateArt
		and ReorderArt methods.
	*/
	AIArtHandle			destinationArt;

	/** For reordering, duplicating and pasting operations. Note that insertionOrder will never be kPlaceAboveAll
		or kPlaceBelowAll, since those go above or below all other objects, and hence will never go inside a
		plugin group. It will also never be kPlaceDefault, since that is already translated into a more specific
		paint order relative to destinationArt before notification.
	*/
	AIPaintOrder		insertionOrder;

	/** For read and write messages (code == #kReadOperationCode or code == #kWriteOperationCode): supplies the version 
	    of the Illustrator file being read or written to. See #AIVersion.  For other codes, the value is 0. */
	AIVersion			aiVersion;

} AIPluginGroupMessage;


/** Data for the #kSelectorAIReplaceSymbol
	
	If targetSymbol is non-null, it has been detected that the plugin group includes
	at least one reference to that symbol in its dictionary, and it is being asked
	to replace the usage of that symbol with the replacementSymbol.

	If targetSymbol is null, it has been detected that the plugin group includes
	at least one reference to some symbol (perhaps multiple symbols) in its
	dictionary, and it is being asked to replace *all* symbols that it uses
	with usages of the replacementSymbol.

	It is not mandatory for objects with symbols in their dictionaries to handle
	this message. The sender of this message does not do anything to the dictionary,
	either before or after. So if the message is ignored, nothing will change.

	At the present time, this message is sent only to plugin groups that are
	selected when the "Replace Symbol" command is invoked in the Symbol palette.

	If the plugin group is dirty upon return from this message, then an
	AIUpdateArt message will be sent. So if you rebuild the result group
	while processing this message, be sure to call MarkPluginArtClean;
	conversely, if you modify only the dictionary and want an AIUpdateArt
	message, be sure to call MarkPluginArtDirty, because dictionary changes
	do not automatically cause a plugin group to be marked dirty.

	If the symbol replacement is really performed, then #kDidSymbolReplacement
	should be returned. (This can be used by the caller to decide whether to
	set the Undo/Redo strings, etc.)
*/
typedef struct {
	SPMessageData		d;
	AIPluginGroupHandle	entry;
	/** the pluginGroup art being notified */
	AIArtHandle			art;

	/** can be null */
	AIPatternHandle		targetSymbol;
	AIPatternHandle		replacementSymbol;
} AIReplaceSymbolMessageData;


/** Data for the #kSelectorAIDisallowsArtTypeQuery message

	This message will only be sent if the AIPluginGroupHandle entry has the option #kPluginGroupRestrictsArtTypes on.
	It is sent before an insertion is attempted into the edit group, either from the core or from a plugin API
	such as NewArt or ReorderArt. It will not, however, be sent if the plugin group has been marked during the
	current API context as being under construction by the managing plugin.

	Valid replies are #kRefusePluginGroupReply, #kCheckPluginGroupReply and #kNoErr.

	kRefusePluginGroupReply means that no plugin group managed by 'entry' allows art objects of artType.

	kNoErr means that all art objects of artType are allowed inside any plugin group managed by 'entry'.

	kCheckPluginGroupReply means that the client should send a #kSelectorAINotifyEdits message with code 
	#kTopologyOperationCode and subCode #kPasteIntoOpCode because the plugin wants to make the decision taking 
	other factors into account, such as the options attached to the specific plugin group being inserted into.
*/
typedef struct {
	SPMessageData		d;
	AIPluginGroupHandle	entry;

	short				artType;
} AIPluginArtTypeMessageData;


// Function pointers used for callbacks when storing custom hit data
/** This function pointer allows the client to provide a function that will be called by 
    the core when it is necessary to destruct/delete customHitData. This function should 
	cast the void* data to the appropriate type and then appropriately handle the object's 
	destruction. */
typedef AIAPI void (*AICustomHitDataDeleteFunc)(void*);
/** Data for the #kSelectorAIPerformCustomHitTest message
	This message will be sent if AIHitTest::CustomHitTest is called with this plugin group type.
	The message is sent while performing a hit test when the hit occurs within
	the bounds of the plugin object.

	If the custom hit code finds a hit, the plugin should respond with #kCustomHitPluginGroupReply and store the
	custom hit information in the CustomHitData struct (more details about this structure below).

	If the custom hit code does not hit anything, the plugin should respond with kNoErr;
*/
typedef struct {
	SPMessageData		d;						// IN
	AIPluginGroupHandle	entry;					// IN
	/** reference to the plugin object that was hit */
	AIArtHandle			art;					// IN
	/** mouse location */	
	AIRealPoint			location;				// IN

	/** If the plugin wants to record custom hit information, the plug-in should fill in the CustomHitData struct below.
	   This data can be access after the hit test is performed via AIHitTestSuite::GetCustomHitData()
		- inData will point to the data that was provided to the AIHitTest::CustomHitTest function
		- outData should point to heap-based data allocated by the plug-in.
		- deleteFunc is a call back function clients can use to properly delete
		  the information stored in data. deleteFunc is required and can NOT be NULL
	*/
	struct
	{
		void*						inData;		// IN
		void*						outData;	// OUT
		AICustomHitDataDeleteFunc	deleteFunc;	// OUT
	} CustomHitData;
	
} AIPluginGroupCustomHitTestData;


/** See description under #AIPluginProcessStylesMessageData. */
typedef AIAPI ASErr (*AIProcessArtStyleFunc)(const AIArtStyleHandle artStyle, 
					AIArtHandle gradientContext, long usage, void *callbackData );
/** See description under #AIPluginProcessStylesMessageData. */
typedef AIAPI ASErr (*AIProcessPathStyleFunc)(const AIPathStyle *pathStyle, const AIPathStyleMap *psMap, 
					AIArtHandle gradientContext, long usage, void *callbackData );

/** @ingroup Errors
	Returned by an AIProcessArtStyleFunc or AIProcessPathStyleFunc in non-error situations that nonetheless mean the
	iteration should be terminated without processing any remaining styles. This could be used, for instance, if the
	callback functions were searching for a particular global object and could quit as soon as it appeared. */
#define kIterationCanQuitReply			'DONE'

/** Data for the #kSelectorAICollectPaintStyles message.
	This message will only be sent if the #kPluginGroupManagesPaintStyles option flag is on.

	The plugin group is being requested to iterate the art styles or path styles managed by its plugin,
	passing each one to a "black box" callback function provided in the message data. (It has a choice
	of which to do, whichever is most appropriate for the kind of plugin.)

	The primary purpose is for UI elements which report information about style-related attributes of
	selected objects, such as fill or stroke attributes, under conditions in which the styles to be reported
	do not map directly onto the styles of selected objects within the edit group. The plugin group may,
	for example, keep styles in its dictionary, along with information about "pieces" of the edit group
	and/or result group to which those styles apply. A stroke style might apply only to certain bezier
	segments, for example, and not to an entire path.

	If the plugin deals primarily in AIArtStyles, it should call the artStyleCallback on each relevant
	art style that it manages. (Relevance being determined by the "selectedOnly" and "usageOfInterest"
	message data fields.)

	If the plugin deals primarly in AIPathStyles, it should call the pathStyleCallback.

	The AIPathStyleMap parameter of the pathStyleCallback, which may be null, can be used when the plugin
	wants to privately accumulate the report for multiple objects. If non-null, fields that are "false" in
	the psMap will be interpreted by the pathStyleCallback as being **MIXED** in the plugin group. For example,
	if the fillPaint field of the psMap is false, it means that some potentially-fillable contents are filled,
	and some are not filled. Plugins for which it is difficult or time-consuming to determine which styles
	correspond to selected objects may wish to privately cache the results of an accumulated PathStyle and
	PathStyleMap, and call the pathStyleCallback only once with those cached results, clearing the cache
	on a SelectionChanged or PropertiesChanged notifier.
 
	Both varieties of callback function will not modify the art styles and/or art objects. They will only
	examine them.

	Both varieties of callback take an AIArtHandle gradientContext. This art handle may be null. If non-null,
	it asserts that any gradient vectors appearing in the artStyle or pathStyle are relative to that art object.
	If null, it asserts that any gradient vectors appearing in the artStyle or pathStyle are relative to the
	whole plugin group.

	See definition of the AIArtStyleUsage enum for the interpretation of the usage parameter to the callbacks.

	The callback functions should be called repeatedly until either all the relevant styles have been iterated,
	or until the callback function returns an error or kIterationCanQuitReply. In the latter case, the error
	returned by the callback function should be returned as the result of the GoMessage.
*/
typedef struct {
	SPMessageData		d;						// IN
	AIPluginGroupHandle	entry;					// IN
	/** reference to the plugin group object whose style contents are being queried */
	AIArtHandle			pluginArt;				// IN
	/** if selected only is true, only styles considered selected should be iterated.
		(As of AI 12, it will always be sent true, but potentially this message type could
		be used for a kind of callback function that wanted to peek at all the styles used
		by the plugin, not just the selected ones.) */	
	AIBoolean			selectedOnly;			// IN

	void* callbackData;							// IN, plugin should pass back to artStyleCallback or pathStyleCallback
	AIProcessArtStyleFunc artStyleCallback;		// IN
	AIProcessPathStyleFunc pathStyleCallback;	// IN

} AIPluginGroupCollectPaintStylesData;

/** Data for the #kSelectorAIApplyPaintStyles message.
	This message will only be sent if the #kPluginGroupManagesPaintStyles option flag is on.

	The plugin group is being requested to merge the path style into its selected descendants.
	The pathStyle should be used together with the pathStyleMap to merge the appropriate pieces of fill
	and stroke attributes to the plugin group's selection.
*/
typedef struct {
	SPMessageData		d;						// IN
	AIPluginGroupHandle	entry;					// IN

	/** reference to the plugin group object whose style contents are being modified */
	AIArtHandle			pluginArt;				// IN

	/** If applyStyleTransform is true, then the matrix, transformFlags and transformLineScale parameters
		will be used, and the pathStyle and pathStyleMap parameters will be ignored, since they have no
		way to specify a transformation request.
	*/
	AIBoolean			applyStyleTransform;

	/** fill and stroke information to be merged in. */
	AIPathStyle			pathStyle;
	/** indicates what fields of the pathStyle should be merged in. */
	AIPathStyleMap		pathStyleMap;

	/** The following three parameters have the same meanings that they do for a #kSelectorAINotifyEdits message of
		with #kTransformOperationCode. The difference is that an AINotifyEdits message informs the plugin group that
		the styles are being transformed externally in the indicated way, while an AIApplyPaintStyles message asks
		the plugin group to do the transformation.
	*/
	AIRealMatrix		matrix;
	/** The kTransformObjects and kTransformChildren flags will always be off, and at least one of
		kTranformFillGradients, kTransformFillPatterns, kTransformStrokePatterns and/or kScaleLines will be on.
	*/
	long				transformFlags;
	/** If transformFlags contains kScaleLines, then this is the factor by which the strokes
		are going to be scaled. */
	AIReal				transformLineScale;

} AIPluginGroupApplyPaintStylesData;


/** Data for the kSelectorAIAdjustColors message.
	This message will only be sent if the #kPluginGroupManagesPaintStyles option flag is on.

	The data looks very similar to #AIPluginGroupCollectPaintStylesData, but the expected
	handling of the message is more similar to that for #kSelectorAIApplyPaintStyles, since
	this is an editing function, not a query.

	This request will be initiated by plugins or other system components that want to alter
	all the colors in a collection of objects. The expected response is that the plugin will
	iterate all the AIColors within the object that fit the request (according to the selectedOnly,
	adjustFills and adjustStrokes message fields), passing them to the adjustColorCallback along
	with the callbackData (which may contain extra parameters needed by the color mapping.)

	The typedef of AIAdjustColorFunc is in AIPluginSelection because it is used by other suites
	besides this one. For convenience, the definition and description is repeated here:
	
	typedef void (*AIadjustColorFunc) (AIColor *color, void *userData, AIErr *result, AIBoolean *altered);

	The AIColor* passed to the callback is both input and output. Only "top level" colors should be
	iterated. For example, if an object is painted with a gradient or pattern, the colors used
	within the gradient or pattern should not be iterated.

	After calling the adjustColorCallback, if *altered is true and *result is kNoErr, the plugin should
	apply the modified color to the same objects that it was acquired from. The process of applying
	the modified color is expected to be very similar to that of handling #kSelectorAIApplyPaintStyles.

	If *result is an error code, it should stop. (The result is a parameter rather than making
	AIadjustColorFunc return an AIErr so that the communication can go both ways. If the plugin handling
	the message wants the component making the adjust colors request to stop and report an error back
	to the user, it should set *result to that error code BEFORE calling the adjustColorCallback.)

	If *altered is false and *result is kNoErr, it can continue since the color was not modified.

	If another plugin wants to initiate this request to a plugin group that it does not
	manage, the most convenient way is to call the PluginArtAdjustColors API from this suite.
*/
typedef struct {
	SPMessageData		d;						// IN
	AIPluginGroupHandle	entry;					// IN
	/** reference to the plugin group object whose style contents are being modified */
	AIArtHandle			pluginArt;				// IN

	/** if selectedOnly is true, only colors considered selected should be iterated. */
	AIBoolean			selectedOnly;			// IN
	AIBoolean			adjustFills;			// IN, request to iterate colors used in fills
	AIBoolean			adjustStrokes;			// IN, request to iterate colors used in strokes

	void*				callbackData;			// IN, plugin should pass back to adjustColorCallback
	AIAdjustColorFunc	adjustColorCallback;	// IN, typedef is in AIColor.h

	AIBoolean			modifiedSomething;		// OUT, should be set if the adjustColorCallback modified anything

} AIPluginGroupAdjustColorsData;


typedef struct {
	long				major;
	long				minor;
	/** Localized description string for error messages */
	char				*desc;
} AIAddPluginGroupData;

/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	A Plugin Group Art Object is a new object type added in version 8.0 of Illustrator.
	A handle to a Plugin Group Art Object is obtained using the standard
	AIArtSuite::NewArt() function with art type #kPluginArt. Once created,
	every Plugin Group Art Object consists of 2 components: an Edit Group Art
	Object and a Result Group Art Object. The Edit Group Art Object and Result
	Group Art Object are contained within the Plugin Group Art object and can
	only be obtained using Plugin Group functions.

	The Edit Group and Result Group Art Objects are standard AIArtHandles and
	can be obtained using the following functions from the Plugin Group suite:
	GetPluginArtEditArt() and GetPluginArtResultArt(). Once the AIArtHandles
	have been obtained, they can be treated as normal art objects. By definition,
	all art objects that are descendents of the Edit Group become invisible but
	editable and selectable, and all art objects that are descendents of the Result
	Group become visible but not selectable.

	In addition to Plugin Group Art Objects there exists Plugin Groups. A Plugin
	Group is the entity that a plugin registers to manage the plugin group objects
	of a particular type. All Plugin Group Art Objects must associate themselves
	with a Plugin Group by calling UseAIPluginGroup().

	Live Blends is an example of how the plugin Group API is utilized within Illustrator.
	A live blend is created by drawing 2 objects and then separately clicking on them
	with Illustratorís blend tool. After clicking on the second object a series of
	intermediate objects is drawn between the original source and destination objects.
	The intermediate objects create a smooth blend between the original two objects. You
	will notice that only the source and destination objects are selectable or editable.
	You will also notice that when you edit or move the source or destination objects,
	the intermediate objects are regenerated. If you go into artwork mode you will notice
	that the document only contains the original two objects. This is by design. A live
	blend is a Plugin Group. The source and destination art objects (as well as the spine
	which links the two objects) are children of the Edit Group, and the intermediate
	objects (as well as a copy of the original source and destination objects) are
	children of the Result Group. This is why only the original two shapes (and the
	spine) are editable and selectable.

	Plugin Groups allow a plug-in to maintain 2 groups of art. One group which
	the user interacts with, and another group which your plug-in is responsible
	for regenerating.

	To create plugin group art objects the first step is to register a Plugin Group. A Plugin
	Group is just an opaque	reference which acts as a manager for one or more Plugin Group Art
	objects. During startup, create your Plugin Group. A Plugin Group is obtained by calling
	AddAIPluginGroup(). This function will return a pointer to an AIPluginGroupHandle. Since
	you will need this whenever you create a new Plugin Group Art object, you should store
	this in globals.

@code
	sAIPluginGroup->AddAIPluginGroup (
			message->d.self,
			"My Plugin Group Plugin",
			&pluginGroupData,
			kPluginGroupWantsAutoTransformOption,
			&g->myPluginGroupHandle
			);
@endcode

	The next step is to create the Plugin Group Art. Your plug-in will most likely do this
	in response to a menu item or click inside your dialog. As mentioned earlier, Plugin
	Group Art is created using the standard AIArtSuite::NewArt() function as follows:

@code
	sAIArt->NewArt( kPluginArt, kPlaceAboveAll, NULL, *pluginGroupArt );
@endcode

	Next, you need to associate the Plugin Group Art with the Plugin Group. Do
	this by calling UseAIPluginGroup().

@code
	sAIPluginGroup->UseAIPluginGroup (
			pluginGroupArt,
			g->myPluginGroupHandle );
@endcode

	Once you call this function, you activate the Plugin Group Art and can access
	itís Edit Group and Result Group.

	Once your Plugin Group has been created and Plugin Group Art has been
	activated, your plug-in will start to receive notifications. Since your plug-in is
	responsible for regenerating all the objects in the result group, naturally you
	need to know when and how often to do so. The Plugin Group suite has 2
	messages which your plug-in can use to commence regeneration of the result
	group objects: #kSelectorAIUpdateArt and #kSelectorAINotifyEdits.
	For the purpose of regenerating the result group objects the #kSelectorAIUpdateArt
	message is the simplest and most effective notifier to use. This
	message will be sent whenever any of the Edit Group objects are modified.
	Your plug-in should respond to this message by regenerating the Result
	Group objects according to the new state of the Edit Group objects. In your
	main loop your plug-in should listen for the #kCallerAIPluginGroup caller
	and #kSelectorAIUpdateArt selector and respond appropriately.

	In general plugins that manage plugin groups must be ready to receive the following
	selectors. Each one will have #kCallerAIPluginGroup as the caller.

	- #kSelectorAINotifyEdits. See the documentation for the AIPluginGroupMessage
		message data for details.
	- #kSelectorAIUpdateArt. See the documentation for the AIPluginGroupMessage
		message data for details.
	- #kSelectorAICanShowContents. See the documentation for the AIPluginGroupMessage
		message data for details.
	- #kSelectorAIInterpolateData. See the documentation for the AIPluginGroupMessage
		message data for details.
	- #kSelectorAIReplaceSymbol. See the documentation for the AIReplaceSymbolMessageData
		message data for details.
 */
typedef struct AIPluginGroupSuite {

	/** This function returns a handle to a Plugin Group. A Plugin Group is a
		manager for one or more Plugin Group Art Objects. A Plugin Group determines
		the behavior of all Plugin Group Art Objects who associate themselves
		with it.

		Your plug-in should pass a reference to its self to the self parameter and the
		name of the plug-in to the name parameter. The data parameter is your
		plug-in's opportunity to specify major and minor versions and a localized
		description string for error reporting. There are several flags you could
		potentially "or" together and pass into the options parameter as defined by
		#AIPluginGroupOptions. Note that you cannot change options once they are set. A
		pointer to a Plugin Group Handle is returned as entry.

		Normally, your plug-in would call this function during its startup procedure
		and keep a reference to the Plugin Group in globals. At a later time, perhaps
		when a menu item is selected or a button pressed, your plug-in would call
		UseAIPluginGroup() to associate a Plugin Group Art Object to the Plugin
		Group.
	*/
	AIAPI AIErr (*AddAIPluginGroup) ( SPPluginRef self, char *name, AIAddPluginGroupData *data, long options, AIPluginGroupHandle *entry );
	/** This function associates a Plugin Group Art Object art, with a Plugin Group
		entry. The Plugin Group Art Object is assumed to already have been created
		using the standard AIArtSuite::NewArt() function. The Plugin Group is assumed to
		already have been created using AddAIPluginGroup().
		
		As soon as your plug-in calls this function, it will start to receive Plugin
		Group selectors.
	*/
	AIAPI AIErr (*UseAIPluginGroup) ( AIArtHandle art, AIPluginGroupHandle entry );

	/** Returns the name that was originally specified when AddAIPluginGroup() was called. */
	AIAPI AIErr (*GetAIPluginGroupName) ( AIPluginGroupHandle entry, char **name );
	/** Returns the major and minor versions that were originally specified in the
		AIAddPluginGroupData struct when AddAIPluginGroup() was called. */
	AIAPI AIErr (*GetAIPluginGroupVersion) ( AIPluginGroupHandle entry, long *major, long *minor );
	/** The description is a localized string which Illustrator uses for error reporting
		and it was originally specified in the AIAddPluginGroupData struct when
		AddAIPluginGroup() was called. */
	AIAPI AIErr (*GetAIPluginGroupDescription) ( AIPluginGroupHandle entry, char **desc );
	/** Returns the options flags that were originally specified when AddAIPluginGroup() was
		called. */
	AIAPI AIErr (*GetAIPluginGroupOptions) ( AIPluginGroupHandle entry, long *options );
	/** Returns the plug-in reference that was originally specified when AddAIPluginGroup()
		was called. */
	AIAPI AIErr (*GetAIPluginGroupPlugin) ( AIPluginGroupHandle entry, SPPluginRef *plugin );


	/** This function returns the number of registered Plugin Group managers. */
	AIAPI AIErr (*CountAIPluginGroups) ( long *count );
	/** Returns the Nth registered plugin group manager. */
	AIAPI AIErr (*GetNthAIPluginGroup) ( long index, AIPluginGroupHandle *entry );


	/** Given a plugin group art object returns the name of its plugin group manager.
		This is the name passed as the name parameter to the AddAIPluginGroup() API
		not the descriptive name.  */
	AIAPI AIErr (*GetPluginArtName) ( AIArtHandle art, char **name );
	/** Givena plugin group art object sets the name of the plugin group manager. */
	AIAPI AIErr (*SetPluginArtName) ( AIArtHandle art, char *name );
	/** Given a plugin group art object gets the major and minor version numbers of
		its plugin group manager. */
	AIAPI AIErr (*GetPluginArtVersion) ( AIArtHandle art, long *major, long *minor );
	/** Given a plugin group art object sets the major and minor version numbers of
		its plugin group manager. */
	AIAPI AIErr (*SetPluginArtVersion) ( AIArtHandle art, long major, long minor );
	/** Given a plugin group art object gets its associated manager. */
	AIAPI AIErr (*GetPluginArtPluginGroup) ( AIArtHandle art,  AIPluginGroupHandle *entry );
	/** Given a plugin group art object sets its associated manager. */
	AIAPI AIErr (*SetPluginArtPluginGroup) ( AIArtHandle art );

	/** When the Plugin Group Art Object was initially created, the Edit Group Art
		Object and the Result Group Art Objects were automatically created. The
		Edit and Result Art Objects are not children of the Plugin Group Art Object
		and are only accessible through this function and GetPluginArtResultArt().
		However, once an Edit Group Art Object is obtained, it can be treated like
		any group art object.
		
		Note that you cannot obtain an Edit Group Art Object or a Result Group Art
		Object by calling AIArtSuite::GetFirstChild() on a Plugin Group Art
		Object. Nor can you call AIArtSuite::ReorderArt() or AIArtSuite::DisposeArt()
		on Edit Group or Result Group Art Objects.

		Also, any objects inside an Edit Group Art Object will be returned when
		calling AIArtSuite::GetSelectedArt() or AIArtSuite::GetMatchingArt().
		GetSelectedArt() may even return the Plugin Group Art Object and the Edit
		Group Art Object.
	*/
	AIAPI AIErr (*GetPluginArtEditArt) ( AIArtHandle art, AIArtHandle *editArt );
	/** Replaces the edit group with the supplied art object which must be a group.
	*/
	AIAPI AIErr (*SetPluginArtEditArt) ( AIArtHandle art, AIArtHandle editArt );
	/** When the Plugin Group Art Object was initially created, the Edit Group Art
		Object and the Result Group Art Objects were automatically created. The
		Edit and Result Art Objects are not children of the Plugin Group Art Object
		and are only accessible through this function and GetPluginArtEditArt().
		However, once a Result Group Art Object is obtained, it can be treated like
		any group art object.

		Note that you cannot obtain an Edit Group Art Object or a Result Group Art
		Object by calling AIArtSuite::GetFirstChild() on a Plugin Group Art
		Object. Nor can you call AIArtSuite::ReorderArt() or AIArtSuite::DisposeArt()
		on Edit Group or Result Group Art Objects.
	*/
	AIAPI AIErr (*GetPluginArtResultArt) ( AIArtHandle art, AIArtHandle *resultArt  );
	/** Replaces the result group with the supplied art object which must be a group.
	*/
	AIAPI AIErr (*SetPluginArtResultArt) ( AIArtHandle art, AIArtHandle resultArt  );

	/** Returns the number of bytes of data stored in the plugin group data store. */
	AIAPI AIErr (*GetPluginArtDataCount) ( AIArtHandle art, unsigned long *count );
	/** Sets the data store size in bytes. */
	AIAPI AIErr (*SetPluginArtDataCount) ( AIArtHandle art, unsigned long count );
	/** The index parameter specifies where to start and the count parameter specifies
		how many bytes to read. Note that the data store information is written out to
		file as a hex encoded string of bytes. */
	AIAPI AIErr (*GetPluginArtDataRange) ( AIArtHandle art, void *data, unsigned long index, unsigned long count );
	/** The index parameter specifies where to start and the count parameter specifies
		how many bytes to write. Note that the data store information is written out
		to file as a hex encoded string of bytes. */
	AIAPI AIErr (*SetPluginArtDataRange) ( AIArtHandle art, void *data, unsigned long index, unsigned long count );


	/** Ordinarily, AIPluginGroup takes care of detecting when a PluginGroup object needs
		to have the resultArt rebuilt. (A #kSelectorAIUpdateArt message is sent once when
		the object is first created, and thereafter whenever the group's data or any object
		in the editGroup has been edited outside of the update message handling itself.)
		But occasionally the default rules are inadequate.

		Some objects may have external dependencies not detectable by the suite support,
		such as referencing a pattern or other global object, an artwork attribute such
		as the page size, etc. Such plugins will usually detect relevant changes via other
		notifications, and for ease of implementation, may wish to trigger the update by
		calling MarkPluginArtDirty().

		Conversely, it is often the case that in processing a menu command, tool action,
		etc., a PluginGroup object will first modify the editArt contents and/or the
		object's data, and then rebuild the resultArt. Ordinarily, the modifications to
		the editArt or data will (A) cause NotifyEdit messages to be sent, and (B) mark
		the plugin object dirty, causing a redundant update if the resultArt has already
		been synced. Both of these default actions can be forestalled.

		Calling MarkPluginArtSilent() when the managing plugin is about to modify the edit
		group will prevent NotifyEdit messages from being sent informing the plugin of changes.
		It is not necessary to set this state, since most plugins simply ignore NotifyEdits messages.
		But if your plugin does care about NotifyEdit messages, then it will probably want to mark
		the plugin object silent when it wants to easily ignore NotifyEdits for a while. (This
		will not prevent the result group from getting marked dirty.) The silent status automatically
		gets cleared whenever the API context gets popped.

		Calling MarkPluginArtClean() after internally updating the result group from somewhere
		other than within the AIUpdateArt message will prevent an UpdateArt message from being sent.
		
	*/
	AIAPI AIErr (*MarkPluginArtDirty) ( AIArtHandle art );
	/** See description of MarkPluginArtDirty() */
	AIAPI AIErr (*MarkPluginArtClean) ( AIArtHandle art );

	// New for AI10: clipping attribute for plug-in group objects

	/** Returns true if the plugin group object is supposed to clip. If this is the
		case then the result group constructed by the plugin group manager should
		contain one or more paths that are set to clip. */
	AIAPI AIErr (*GetPluginArtClipping) ( AIArtHandle art, AIBoolean *clipping );
	/** Returns #kBadParamterErr if the associated group does not specify the option
		#kPluginGroupCanBeClipping. Otherwise sets whether the plugin group object
		is supposed clip. */
	AIAPI AIErr (*SetPluginArtClipping) ( AIArtHandle art, AIBoolean clipping );

	/** Get the default name for these plugin groups to be returned by AIArtSuite::GetArtName().
		The default name here can be NULL, in which case the default art name returned by
		AIArt::GetArtName() will be the desc string that the plugin group registered with. */
	AIAPI AIErr (*GetAIPluginGroupDefaultName) ( AIPluginGroupHandle entry, char** name );
	/** Set the default name for these plugin groups. See GetAIPluginGroupDefaultName() for
		more information. */
	AIAPI AIErr (*SetAIPluginGroupDefaultName) ( AIPluginGroupHandle entry, const char* name );

	/** Get and the minimum AI version that this plugin group supports. Values for the appVersion
		are defined in #AIVersion. Plugin groups that do not specifically use this api will default to
		version 8. Setting the version prior to version 8 will return an error, since plugin groups
		are not supported in AI versions prior to 8.

		When Illustrator writes out this plugin group, it will check this app version, and if it is
		writing out to a prior file format, it will only write out the result group. */
	AIAPI AIErr (*GetAIPluginGroupAppVersion) ( AIPluginGroupHandle entry, AIVersion* appVersion );
	/** Set and the minimum AI version that this plugin group supports. See GetAIPluginGroupAppVersion()
		for more information. */
	AIAPI AIErr (*SetAIPluginGroupAppVersion) ( AIPluginGroupHandle entry, AIVersion appVersion );

	// New for AI12:

	/** See description of MarkPluginArtDirty() */
	AIAPI AIErr (*MarkPluginArtSilent) ( AIArtHandle art );

	/** This function is intended for the use of plugins OTHER THAN the one implementing the plugin group.
		Asks the plugin group to modify all the colors inside the art object that it manages the styles for,
		by passing it a #kSelectorAIAdjustColors message (see above) packaging up the input parameters.

		If the plugin group managing the art object does not carry the #kPluginGroupManagesPaintStyles option,
		this function will return kNoErr but do nothing. If the adjustColorCallback reports an error, it will
		be returned as the result of PluginArtAdjustColors.
	*/ 
	AIAPI AIErr (*PluginArtAdjustColors) ( AIArtHandle art, AIAdjustColorFunc adjustColorCallback,
											void *callbackData, ASBoolean *modifiedSomething,
											AIBoolean adjustFills, AIBoolean adjustStrokes,
											AIBoolean selectionOnly );

} AIPluginGroupSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif

