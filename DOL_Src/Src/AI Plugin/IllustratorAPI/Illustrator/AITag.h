#ifndef __AITag__
#define __AITag__

/*
 *        Name:	AITag.h
 *   $Revision: 4 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Tag Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AITag.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAITagSuite			"AI Tag Suite"
#define kAITagSuiteVersion		AIAPI_VERSION(2)
#define kAITagVersion			kAITagSuiteVersion

/** @ingroup Notifiers */
#define kAITagChangedNotifier	"AI Tag Changed Notifier"

/** @ingroup Errors */
#define kBadTagTypeErr		'TGTY'
/** @ingroup Errors */
#define kBadTagNameErr		'TGNM'
/** @ingroup Errors */
#define kBadTagDataErr		'TGDT'
/** @ingroup Errors */
#define kTagNotFoundErr		'TGNF'


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** The Tag Suite allows you to assign arbitrary data to Illustrator artwork
	objects. Its use is generally deprecated in favor of the more powerful
	facilities provided by the AIDictionarySuite. Tags are actually stored
	as string entries in the art object dictionary. Use AIArtSuite::GetDictionary()
	to get the art object dictionary.

	A tag is data attached to an artwork object. The tag is made up of a descriptive
	name, a tag type, a change count, and the tag data itself.

	The tag name can contain up to 30 characters (alphanumeric characters and
	'_'). Illustrator does not check for duplicate names, so you should make sure
	you choose unique tag names. Note: Do not use spaces in tag names.
	
	A tag's type indicates the type of tag information. Currently the only tag
	type supported by Illustrator is "string", which represents a null terminated
	C string up to 200 characters.

	Each time an artwork tag is changed, Illustrator increments the change
	count.
 
	The following notifiers are associated with the tag suite:

	- #kAITagChangedNotifier
 */
typedef struct AITagSuite {

	/** The AIArtHandle to receive the tag is specified in object. The name argument is
		a unique identifier for the tag. The type specifies the type of data the tag
		contains; currently it can only be the literal "string". The arguments that
		follow depend on the type of data. For string types, pass a pointer to the
		C string to be associated with the object. */
	AIErr (*SetTag) ( void *object, const char *name, const char *type, ... );
	/** Gets the information tagged to the object. The AIArtHandle with the tag is
		specified in object. The name argument is a unique identifier for the tag you
		wish to retrieve. The type specifies the type of data the tag contains. The
		arguments that follow depend on the type of data. For "string" types pass a
		pointer to a char *. You do not need to allocate space for the tag string since
		a pointer to the existing string will be returned. The plug-in should not
		directly modify the string, but should use SetTag() instead. */
	AIErr (*GetTag) ( void *object, const char *name, const char *type, ... );

	/** Since only the string type is implemented in Illustrator, it is the only type
		that can be returned. GetTagType() can also be used to determine the existence
		of a tag of a given name.

		The AIArtHandle with the tag is specified in object. The name argument is the
		unique identifier for the tag whose type you want . A pointer to the type of
		data tag is returned in type. */
	AIErr (*GetTagType) ( void *object, const char *name, char **type );
	/** Illustrator tracks the number of times an object tag has changed. This function
		retrieves that count. The AIArtHandle with the tag is specified in object. The
		name argument is the unique identifier of the tag for which you want the change
		count. The number of changes is returned in count. */
	AIErr (*GetTagChangeCount) ( void *object, const char *name, long *count );
	/** When an object no longer needs to be tagged, use the RemoveTag function
		to clear it. Use this function with care, you should not remove tags that your
		plug-in did not create. */
	AIErr (*RemoveTag) ( void *object, const char *name );
	/** Use this function to determine how many tags are on an object. */
	AIErr (*CountTags) ( void *object, long *count );
	/** This function returns the tag at index n of the art object. A pointer to the
		tag name is returned in name. The first tag is index zero (0). */
	AIErr (*GetNthTag) ( void *object, long n, char **name );

} AITagSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
