/*
	Include in your plugin file. Requires the following to be defined:

		sAIMenu
		sADMBasic

	Wrapper for some common menu functions. See "Interface"
	section below for usage.

	cquartet 3/2001

*/

#include <string.h>
#include <stdio.h>

namespace AIMenu {


////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////

typedef unsigned char PString[255];		// Pascal string

	// Set only desired options, others will be initialized & ignored.
struct MenuItemParams
	{
	MenuItemParams();

	char *name;
	long options;
	ASHelpID helpID;
	char *inSubmenuNamed;
	ASInt32 stringTable;
	ASInt32 titleIndex;
	AIMenuItemHandle *itemPtr;

		// Auto-update flags
	long ifIsTrue;
	long ifIsNotTrue;
	};

	// Fetch string resource as Pascal string. Return zero length string on failure.
static void GetPascalString(
	SPPluginRef piRef,
	ASInt32 stringTable,
	ASInt32 stringIndex,
	PString string);

static ASErr CreateMenuItem(
	SPPluginRef piRef,
	MenuItemParams &itemParams,
	PString itemTitle);

static ASErr CreateSubMenu(
	SPPluginRef piRef,
	char *submenuName,
	PString subMenuTitle,
	int options,
	char *inMenuNamed);				// parent menu

	// Create item in a submenu. If the submenu doesn't exist, create it.
static ASErr CreateItemInSubMenu(
	SPPluginRef piRef,
	MenuItemParams &itemParams,		// menu item parameters
	ASInt32 submenuTitleIndex,		// assume same string table as itemParams.stringTable 
	long submenuOptions,
	char *inMenuNamed);				// parent menu of submenu

////////////////////////////////////////////////////////////////////////////////
// Implementation
////////////////////////////////////////////////////////////////////////////////
MenuItemParams::MenuItemParams():
	name(0),
	options(0),
	helpID(0),
	inSubmenuNamed(0),
	stringTable(0),
	titleIndex(0),
	itemPtr(0),
	ifIsTrue(kIfOpenDocument),
	ifIsNotTrue(0)
{
}

////////////////////////////////////////////////////////////////////////////////
static char *P2CString(const unsigned char *inString, char *outString)
{
	unsigned char cb = inString[0];

	memmove(outString, inString + 1, cb);
	*(outString + cb) = '\0';

	return outString;
}

////////////////////////////////////////////////////////////////////////////////
static ASErr CreateMenuItem(
	SPPluginRef piRef,
	MenuItemParams &itemParams,
	PString itemTitle)
{
	char *name= itemParams.name,
		 tempName[257];
	ASErr result= kNoErr;
	AIPlatformAddMenuItemData amd;


	amd.itemText= itemTitle;
	amd.groupName= itemParams.inSubmenuNamed;

	if (!name)
		{
		char itemAsCStr[257];

		P2CString(itemTitle, itemAsCStr);
		sprintf(tempName, "%s menu item", itemAsCStr);
		name= tempName;
		}
	
	AIMenuItemHandle item;
	result= sAIMenu->AddMenuItem(piRef, name, &amd, itemParams.options, &item);

	if (itemParams.itemPtr)
		*itemParams.itemPtr= item;

	if (!result && itemParams.helpID)
		result= sAIMenu->SetItemHelpID(item, itemParams.helpID);

	if (!result && (itemParams.ifIsTrue || itemParams.ifIsNotTrue))
		result= sAIMenu->UpdateMenuItemAutomatically(item,
			kAutoEnableMenuItemAction, 0, 0,
			0, 0, itemParams.ifIsTrue, itemParams.ifIsNotTrue);

	return result;
}


////////////////////////////////////////////////////////////////////////////////
static void GetPascalString(
	SPPluginRef piRef,
	ASInt32 stringTable,
	ASInt32 stringIndex,
	PString string)
{
char cStr[256];
ASErr result= sADMBasic->GetIndexString(piRef, stringTable, stringIndex, cStr, 256);

if (!result)
	{
	int length= strlen(cStr);
	string[0]= (length>255)?255:length;
	strncpy(((char *)string)+1, cStr, string[0]);
	}
else
	string[0]= 0;
}


////////////////////////////////////////////////////////////////////////////////
static ASErr CreateSubMenu(
	SPPluginRef piRef,
	char *submenuName,
	PString subMenuTitle,
	int options,
	char *inMenuNamed)
{
AIMenuItemHandle item;
MenuItemParams sub;

sub.name= submenuName;
sub.inSubmenuNamed= inMenuNamed;
sub.options= options;
sub.itemPtr= &item;
sub.ifIsTrue= 0;

ASErr result= CreateMenuItem(piRef, sub, subMenuTitle);

if (!result)
	{
	AIMenuGroup group;
	result= sAIMenu->AddMenuGroupAsSubMenu(submenuName, options, *sub.itemPtr, &group);
	}

return result;
}


////////////////////////////////////////////////////////////////////////////////
// Create 'itemName' in the submenu 'inSubmenuNamed'. If 'inSubmenuNamed' does
// not exist, create it.
////////////////////////////////////////////////////////////////////////////////
static ASErr CreateItemInSubMenu(
	SPPluginRef piRef,
	MenuItemParams &itemParams,
	ASInt32 submenuTitleIndex,
	long submenuOptions,
	char *inMenuNamed)
{
ASErr result= kNoErr;
PString itemTitle;


GetPascalString(piRef, itemParams.stringTable, itemParams.titleIndex, itemTitle);
result= CreateMenuItem(piRef, itemParams, itemTitle);

if (result)
	{
	PString submenuTitle;
	GetPascalString(piRef, itemParams.stringTable, submenuTitleIndex, submenuTitle);
	result= CreateSubMenu(piRef, itemParams.inSubmenuNamed, submenuTitle, submenuOptions, inMenuNamed);
	if (!result)
		result= CreateMenuItem(piRef, itemParams, itemTitle);
	}

return result;
}



}	// namespace