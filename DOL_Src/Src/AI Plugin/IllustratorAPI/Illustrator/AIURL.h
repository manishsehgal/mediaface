#ifndef _AIURL_H_
#define _AIURL_H_

/*
 *        Name:	AIDragDrop.h
 *   $Revision: 1 $
 *      Author:	Ron Fischer 
 *        Date:	June 27, 1996   
 *     Purpose:	Adobe Illustrator 7.0 Drag and Drop Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifndef __ASTypes__
#include "ASTypes.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIURL.h */


#define kAIURLSuite		"AI URL Suite"
#define kAIURLSuiteVersion		AIAPI_VERSION(1)

/** @ingroup Errors */
#define kFailureErr		'FAIL'
	


/**
	The URL Suite provides a simple interface to go to a URL through a web
	browser. You can use this to lead users to your web site for updates or other
	information.
 */
typedef struct {
	/** true if a web browser is available. */
	ASAPI ASBoolean (*IsBrowserAvailable)(void);
	/** Opens a web browser to the specified URL. */
	ASAPI ASBoolean (*OpenURL)(char *url);
} AIURLSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif // _AIURL_H_