#ifndef __AIUser__
#define __AIUser__

/*
 *        Name: AIUser.h
 *   $Revision: 15 $
 *      Author:	 
 *        Date:	   
 *     Purpose: Adobe Illustrator User Utilities Suite.	
 *
 * Copyright (c) 1986-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIColor__
#include "AIColor.h"
#endif

#ifndef __AIMenu__
#include "AIMenu.h"
#endif

#ifndef __ADMTypes__
#include "ADMTypes.h"
#endif

#include "IAIFilePath.hpp"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIUser.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

// v.11.0
#define kAIUserSuite				"AI User Suite"
#define kAIUserSuiteVersion7		AIAPI_VERSION(7)
#define kAIUserSuiteVersion			kAIUserSuiteVersion7
#define kAIUserVersion				kAIUserSuiteVersion

#if Macintosh || MSWindows
#define kAIMacUserSuite				"AI Mac User Suite"
#define kAIMacUserSuiteVersion3		AIAPI_VERSION(3)
#define kAIMacUserSuiteVersion		kAIMacUserSuiteVersion3
#define kAIMacUserVersion			kAIMacUserSuiteVersion
#endif


enum {
	kShortUnits,			// "in"
	kLongSingularUnits,		// "inch"
	kLongPluralUnits		// "inches"
};


#define kUnknownUnitsErr		'?UNT'
#define kApplicationNotFoundErr	'App?' //can be returned by EditInOriginalApp
#define kObjectNotLinkedErr		'!Lnk' //can be returned by EditInOriginalApp


typedef unsigned long AIUserDateTime;

#define kAIUniversalLocaleFormat		0x0000
#define kAIResourceLocaleFormat			0x0001
#define kAISystemLocaleFormat			0x0002

#define kAIShortDateFormat				0x0000
#define kAILongDateFormat				0x0100
#define kAIAbbrevDateFormat				0x0200
#define kAIShortTimeFormat				0x0000
#define kAILongTimeFormat				0x0100


/*******************************************************************************
 **
 **	Types
 **
 **/

/** There are many collections of named entities. Examples are layers, swatches
	and styles. When a new object is created or an object is copied a name
	needs to be manufactured. There are a set of rules that govern how those
	names are manufactured. The APIs AIUserSuite::NextName() and AIUserSuite::CopyOfName()
	implement these rules. In order to do so they need additional information
	about the collection. The AIAutoNameGenerator provides this information.

@code
	class AutoNameGenerator : public AIAutoNameGenerator {
	public:
		// Constructor for a name generator. Its supplied with the pluginref in
		// case the name generator needs to look in the plugin's resources e.g for
		// the default name. The next new name index is supplied since the plugin
		// probably needs to store this in its globals so that it can be preserved
		// across unload and reload.
		AutoNameGenerator (SPPluginRef pluginref, int nextNameIndex)
		{
			// fill in fDefaultName
			GetDefaultName(pluginref);

			// initialize the base members
			maximumLength = kMaxCollectionStringLength;
			uniqueNames = true;
			baseName = fDefaultName;
			nextNewNameIndex = nextNameIndex;
			HasName = sHasName;
		}

	private:
		void GetDefaultName (SPPluginRef pluginref)
		{
			// Somehow initialize fDefaultName with the default name for the
			// collection. The pluginref is probably needed in order to look
			// up the name in the plugin resources.
		}

		AIErr hasName (const ASUnicode* name, AIBoolean* hasit)
		{
			// Code to answer the question. When comparing 'name' against
			// names of objects in the collection it's important to use the
			// AIUserSuite::SameName() API. The name generation process does
			// not use a simple string comparison.
		}

		static AIAPI AIErr sHasName (_t_AIAutoNameGenerator* self, const ASUnicode*name, AIBoolean* hasit)
		{
			return static_cast<AutoNameGenerator*>(self)->hasName(name, hasit);
		}

		ASUnicode fDefaultName[128];
	};
@endcode
*/
typedef struct _t_AIAutoNameGenerator {

	/** The maximum length for names in the collection. This is a count of
		ASUnicode elements. It excludes the null terminator. */
    ASInt32 maximumLength;

	/** Indicates whether names in the collection must be unique. If an algorithm
		for name generation would otherwise produce non-unique names 
		setting this to true will enforce uniqueness. */
	AIBoolean uniqueNames;

	/** Unicode encoded string that is the base for new names generated. */
    const ASUnicode* baseName;

	/** The index to use for the next new name generated. The API for
		generating names will automatically increment this. */
	ASInt32 nextNewNameIndex;

	/** A callback function to determine whether a given name is already in
		the set. If an error is returned name generation is aborted and
		the error code is returned to the caller. Note that the AIUserSuite::SameName()
		API should be used for name comparisons since the rules for
		comparing names are not simple string equality. */
    AIAPI AIErr (*HasName) (_t_AIAutoNameGenerator* self, const ASUnicode* name, AIBoolean* hasit);

} AIAutoNameGenerator;


/*******************************************************************************
 **
 **	Suite
 **
 **/



/** 
	AICustomProgressSuite
	
	A custom progress reporting mechanism.  For use with the PushCustomProgress/PopCustomProgress
	suite functions.
	
*/
struct AICustomProgressSuite {

	AIAPI AIBoolean (*Cancel) ( void *data );
	AIAPI void (*UpdateProgress) ( void *data, long current, long max );
	AIAPI void (*SetProgressText) ( void *data, const char *text );
	AIAPI void (*CloseProgress) ( void *data );
	AIAPI void (*DisableProgressCancel) ( void *data );
	void *data; // user data.
};

/**	Manages collection of platform-dependent file type specifiers for get/put
	dialog suite functions. These are used to present users with drop-down
	lists, one for each file type.

	The AddFilter methods add Windows file types:

	@param desc		Description of file type.
	@param pattern	Sequence of Windows wildcards to filter given file type. Multiple wildcards are separated with semicolons ";"

	Sample use (pseudocode):

		Addfilter("JPEG Files", "*.jpg;*.jpeg");		<br>
		Addfilter("PNG Files", "*.png");

  */
struct AIPlatformFileDlgOpts
{
#ifdef WIN_ENV

		/// Add a Windows file type filter with default pattern (*.*)
	void AddFilter(const ai::UnicodeString &desc);
		/// Add a Windows file type filter, with Unicode* pattern
	void AddFilter(const ai::UnicodeString &desc, const ASUnicode *pattern);
		/// Add a Windows file type filter, with UnicodeString pattern
	void AddFilter(const ai::UnicodeString &desc, const ai::UnicodeString &pattern);

		/** Return a single string of type descriptions and patterns suitable for
			Windows dialog APIs (see OPENFILENAME::lpstrFilter for reference). It
			consists of a series of null-delimited descriptions and patterns
			terminated by a double-null:

			"first description\0first pattern\0 ... last description\0last pattern\0\0"
		 */
		/// Return a Windows OPENFILENAME::lpstrFilter string
	const ai::UnicodeString &GetFilter() const;

	private:
	ai::UnicodeString filterStr;
#else
	// not used for MacOS
#endif
};

/** 
	AIUserSuite
*/
struct AIUserSuite {

	AIAPI AIBoolean (*Cancel) ( void );
	AIAPI void (*UpdateProgress) ( long current, long max );
	AIAPI void (*SetProgressText) ( const ai::UnicodeString& text );
	AIAPI void (*CloseProgress) ( void );
	AIAPI void (*AIRealToString) ( AIReal value, short precision, ai::UnicodeString& string );
	AIAPI void (*StringToAIReal) ( const ai::UnicodeString& string, AIReal *value );
	AIAPI void (*IUAIRealToString) ( AIReal value, short precision, unsigned char *string );
	AIAPI void (*IUStringToAIReal) ( unsigned char *string, AIReal *value );

	/** This API converts a real value in points to a formatted string. The user's 
	    preferences for precision are used. The current ruler units for the artwork are used 
		to scale the value to these units before it is converted. 
	  */
	AIAPI AIErr (*IUAIRealToStringUnits) ( AIReal value, ai::UnicodeString& string );
	
	/** This API converts a formatted string to a real value in points. The current ruler units 
		for the artwork are used as the original units to scale the result and the user's 
		preferences for precision are applied.
	  */
	AIAPI void (*IUStringUnitsToAIReal) ( const ai::UnicodeString& string, AIReal *value );
	
	AIAPI AIErr (*GetUnitsString) ( short format, ai::UnicodeString& string );
	AIAPI AIErr (*AIUnitsToADMUnits) ( short aiUnits, ADMUnits *admUnits);
	AIAPI AIErr (*ADMUnitsToAIUnits) ( ADMUnits admUnits, short *aiUnits);
	
	AIAPI AIErr (*GetGlobalObjectDisplayName)( ai::UnicodeString& name );
	
	/** Open the appropriate app to edit the object.  This is only meaningful for
	  Placed and Raster objects, and is equivalent to the user option-double-clicking
	  on the object with the selection tool.  See errors above. */
	AIAPI AIErr (*EditInOriginalApp) ( AIArtHandle art );

	AIAPI AIErr (*AppIdle) ( void );

	AIAPI AIErr (*BuildDirectoryMenu) ( AIPlatformMenuHandle menu, const ai::FilePath &fileSpec);
	AIAPI AIErr (*GetIndexedDirectorySpec) ( ai::FilePath &fileSpec, int index);
	AIAPI AIErr (*RevealTheFile) ( const ai::FilePath &fileSpec);
	
	AIAPI void (*DisableProgressCancel) ( void );

	AIAPI AIErr (*SPPlatformFileSpecification2Path) ( const SPPlatformFileSpecification *fileSpec, char *path );
	AIAPI AIErr (*Path2SPPlatformFileSpecification) ( const char *path, SPPlatformFileSpecification *fileSpec );

	/** This method should be called by plugins when a timer or other asynchronous event
		like updating triggers some action that may  allocate memory.  If it returns false,
		the plugin should reschedule the action and try again later.  It is not necessary to
		call this when handling notifications or user events; the plugin should never receive
		these when it's not ok to allocate memory. */
	AIAPI AIBoolean (*OKToAllocateMemory) ( void );

	// New for AI 11 
	AIAPI AIErr (*GetDateAndTime)(AIUserDateTime *ioValue);
	/* if ioValue is NULL, it will get current time. */
	AIAPI AIErr (*GetDateString)(const AIUserDateTime *ioValue, const int formatFlag, ai::UnicodeString& dateStringUS);
	AIAPI AIErr (*GetTimeString)(const AIUserDateTime *ioValue, const int formatFlag, ai::UnicodeString& dateStringUS);
	AIAPI AIErr (*GetYear)(const AIUserDateTime *ioValue, int *year);
	AIAPI AIErr (*GetMonth)(const AIUserDateTime *ioValue, int *month);
	AIAPI AIErr (*GetDay)(const AIUserDateTime *ioValue, int *day);
	AIAPI AIErr (*GetHour)(const AIUserDateTime *ioValue, int *hour);
	AIAPI AIErr (*GetMinute)(const AIUserDateTime *ioValue, int *minute);
	AIAPI AIErr (*GetSecond)(const AIUserDateTime *ioValue, int *second);
	AIAPI AIErr (*GetDayOfWeek)(const AIUserDateTime *ioValue, int *dayOfWeek);
	AIAPI AIErr (*LaunchApp)(const ai::FilePath &spFileSpec, ASBoolean openDoc);


	/** There are many collections of named entities. Examples are layers, swatches
		and styles. Illustrator has a standard set of rules governing automatic
		generation of names for such collections. This API can be used to manufacture
		the next name to use for a newly created object. The name  buffer must be at
		least AIAutoNameGenerator::maximumLength + 1 ASUnicode elements in size. Note
		that the AIAutoNameGenerator::nextNewNameIndex member of the is incremented. */
	AIAPI AIErr (*NextName) (AIAutoNameGenerator* set, ASUnicode* name);

	/** There are many collections of named entities. Examples are layers, swatches
		and styles. Illustrator has a standard set of rules governing automatic
		generation of names for such collections. This API can be used to manufacture
		a name for a copy of an existing object. The name  buffer must be at
		least AIAutoNameGenerator::maximumLength + 1 ASUnicode elements in size. */
    AIAPI AIErr (*CopyOfName) (AIAutoNameGenerator* set, const ASUnicode* original, ASUnicode* copy);

	/** There are many collections of named entities. Examples are layers, swatches
		and styles. Illustrator has a standard set of rules governing automatic
		generation of names for such collections. This API is to be used by the
		AIAutoNameGenerator::HasName() callback function to determine whether an automatically
		generated name matches a name in the collection. It may be, but does not need to be
		used to test for equality of user entered names. */
	AIAPI AIErr (*SameName) (const ASUnicode* name1, const ASUnicode* name2, AIBoolean* same);

	/** PushCustomProgress allows the plugin client to install a custom progress reporting
	    mechanism.  This is currently not implemented and will return a kNotImplementedErr.
	  */
	AIAPI AIErr (*PushCustomProgress) (AICustomProgressSuite *suite);

	/**  PopCustomProgress removes the top-most custom AICustomProgressSuite suite from the
	     reporting stack. This is currently not implemented and will return a kNotImplementedErr.
	  */
	AIAPI AIErr (*PopCustomProgress) ();

	/** Invoke a dialog to let the user save a file.

		@param title					(in)  Dialog title.
		@param AIPlatformFileDlgOpts	(in)  File types to save, may be null. See AIPlatformFileDlgOpts for details.
		@param forceLocalFileView		(in)  Set to true to inhibit Version Cue view.
		@param defaultName				(in)  Default file name to appear in dialog, may be empty.
		@param ioFilePath				(in)  The directory to browse, may be empty.
										(out) The full path of the file file chosen by the user.
	  */
	AIAPI AIErr (*PutFileDialog)(const ai::UnicodeString &title, const AIPlatformFileDlgOpts*, bool forceLocalFileView, const ai::UnicodeString &defaultName, ai::FilePath &ioFilePath);

	/** Invoke a dialog to let the user open a file.

		@param title					(in)  Dialog title.
		@param AIPlatformFileDlgOpts	(in)  File types to open, may be null. See AIPlatformFileDlgOpts for details.
		@param forceLocalFileView		(in)  Set to true to inhibit Version Cue view.
		@param ioFilePath				(in)  The directory to browse, may be empty.
										(out) The full path of the file file chosen by the user.
	  */
	AIAPI AIErr (*GetFileDialog)(const ai::UnicodeString &title, const AIPlatformFileDlgOpts*, bool forceLocalFileView, ai::FilePath &ioFilePath);

	/** Invoke a dialog to let the user choose a directory.

		@param title					(in)  Dialog title.
		@param forceLocalFileView		(in)  Set to true to inhibit Version Cue view.
		@param ioFilePath				(in)  The directory to browse, may be empty.
										(out) The full path to the directory chosen by the user.
	  */
	AIAPI AIErr (*GetDirectoryDialog)(const ai::UnicodeString &title, bool forceLocalFileView, ai::FilePath &ioFilePath);

	/** Displays an alert with a warning icon and OK and cancel buttons. The message displayed
		in the alert is supplied by the msg parameter. The defaultOk parameter indicates whether
		the default is the Ok button or the cancel button. If dontShowKey is not NULL then a don't
		show again check box is included and the key is used to determine whether the dialog
		should be shown and to store the dont show again setting in the preferences file. */
	AIAPI AIBoolean (*OKCancelAlert) (const ai::UnicodeString& msg, AIBoolean defaultOk, const char* dontShowKey);
};



#if defined(MAC_ENV) || defined(WIN_ENV)
/** 
	AIMacUserSuite
*/
typedef struct {

	AIAPI AIBoolean (*ModalFilterProc) ( AIWindowRef theDialog, AIEvent *theEvent, short *itemHit );
	AIAPI AIErr (*PreviewColorSwatch) ( AIWindowRef window, AIRect *swatchRect, AIColor *colorSpec );

} AIMacUserSuite;
#endif

/*
	Inline members
*/
#ifdef WIN_ENV
	inline void AIPlatformFileDlgOpts::AddFilter(const ai::UnicodeString &desc)
	{
		const ASUnicode allFiles[] = {'*', '.', '*', 0};
		AddFilter(desc, ai::UnicodeString(allFiles));
	}

	inline void AIPlatformFileDlgOpts::AddFilter(const ai::UnicodeString &desc, const ai::UnicodeString &pattern)
	{
		if (!desc.empty() && !pattern.empty())
		{
			const ai::UnicodeString::size_type len = filterStr.length();
			if (len)
				filterStr.erase(len-1);	// erase one null of the previous double null terminators
			filterStr.append(desc);
			filterStr.append(1, 0);
			filterStr.append(pattern);
			filterStr.append(2, 0);
		}
	}

	inline void AIPlatformFileDlgOpts::AddFilter(const ai::UnicodeString &desc, const ASUnicode *pattern)
	{
		AddFilter(desc, ai::UnicodeString(pattern));	
	}

	inline const ai::UnicodeString &AIPlatformFileDlgOpts::GetFilter() const
	{
		return filterStr;
	}
#endif

#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
