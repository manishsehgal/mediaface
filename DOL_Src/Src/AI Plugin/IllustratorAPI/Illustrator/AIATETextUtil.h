#ifndef __ATETextUtil__
#define __ATETextUtil__

/*
 *        Name:	AIATETextUtil.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 12.0 Text Frame Object Suite.
 *
 *				REQUIREMENT: This won't compile without a C++ compiler!
 *
 * Copyright (c) 1986-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __ATESuites__
#include "ATESuites.h"
#endif

#ifndef __AIDocument__
#include "AIDocument.h"
#endif

#ifndef __AIFont__
#include "AIFont.h"
#endif

#ifndef __SPFiles__
#include "SPFiles.h"
#endif


extern "C" {

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIATETextUtil.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIATETextUtilSuite			"AI ATE Text Util Suite"
#define kAIATETextUtilSuiteVersion2	AIAPI_VERSION(2)
#define kAIATETextUtilSuiteVersion	kAIATETextUtilSuiteVersion2
#define kAIATETextUtilVersion		kAIATETextUtilSuiteVersion


/** @ingroup Notifiers */
#define kAICurrentFontChangedNotifier	"AI Current Font Changed Notifier"
/** @ingroup Notifiers */
#define kAICurrentFontSizeChangedNotifier	"AI Current Font Size Changed Notifier"
/** @ingroup Notifiers */
#define kAIFontListChangedNotifier		"AI Font List Changed Notifier"
/** @ingroup Notifiers
	The kATEDocumentResourceChangedNotifier notifier is sent when ATE document
	resources such as named style changed.*/
#define kATEDocumentResourceChangedNotifier	"AI ATE Document Resource Changed Notifier"


/** @ingroup Errors
	This could be the return of AIATETextUtilSuite::GetBoundsFromTextRange() if the range is
	hidden due to smaller frame size (overset).
*/
#define kAIATEInvalidBounds 'INVB'		

/*******************************************************************************
 **
 **	Suite
 **
 **/

/** Utilities for working with text.
 */
struct AIATETextUtilSuite {
	/** Return the bounds of the text range (tr).  If the text range is hidden due to smaller
		frame size then an error #kAIATEInvalidBounds returned. */
	AIAPI AIErr (*GetBoundsFromTextRange)( ATE::TextRangeRef tr, ASRealRect* bounds );

	/** The application text resources object contains application global resources related
		to text. See the ATE APIs for more information. */
	AIAPI AIErr (*GetApplicationTextResources)( ATE::ApplicationTextResourcesRef *result );
	/** The document text resources object contains per document global resources related
		to text. See the ATE APIs for more information. */
	AIAPI AIErr (*GetTextResourcesFromDocument)( AIDocumentHandle pDocument, ATE::TextResourcesRef* pResources );

	// Comp Font
	AIAPI AIErr (*GetCompositeFont)( const ai::FilePath&, ATE::CompFontRef * compFont );
	AIAPI AIErr (*WriteCompositeFont)( const ai::FilePath&, const ATE::CompFontRef compFontRef);

	// Kinsoku
	AIAPI AIErr (*GetKinsokuFile)( const ai::FilePath&, ATE::KinsokuRef *kinsokuRef );
	AIAPI AIErr (*WriteKinsokuFile)( const ai::FilePath&, const ATE::KinsokuRef kinsokuRef );
	
	// Mojukumi
	AIAPI AIErr (*GetMojiKumiFile) ( const ai::FilePath&, ATE::MojiKumiRef *mojikumeRef );
	AIAPI AIErr (*WriteMojiKumiFile)( const ai::FilePath&, const ATE::MojiKumiRef mojikumeRef);

	AIAPI AIErr (*UpdateParagraphPaletteKinsoku) (int index);
	AIAPI AIErr (*UpdateParagraphPaletteMojiKumi)(int index);

	AIAPI AIErr (*GetSpellFile)( const ai::FilePath& pSrcFileSpec, ATE::SpellRef* pDst );

	// Getting and setting the current font
	AIAPI AIErr (*SetCurrentFont)(AIFontKey theFont);
	AIAPI AIErr (*GetCurrentFont)(AIFontKey *result);

	AIAPI AIErr (*SetCurrentFontSize)(AIReal theFontSize);
	AIAPI AIErr (*GetCurrentFontSize)(AIReal *result);
	/**
	API that should be in ATE::IGlyphRun.
	Returns array of points representing the visual, flattened, representation of the underline for a given ATE::IGlyphRun.
	Those points represent polygon, not bezier, and is always closed, the last point is not equal to the first one.
	The coordinate are "hard", so use AIHardSoft suite to convert them into "soft" coordinate before working.
	*/
	AIAPI AIErr (*GetUnderlinePoints)(const ATE::GlyphRunRef gr , ATE::ArrayRealPointRef* pPoints);
	/**
	API that should be in ATE::IGlyphRun.
	Returns array of points representing the visual, flattened, representation of the strikethrough for a given ATE::IGlyphRun.  
	Those points represent polygon, not bezier, and is always closed, the last point is not equal to the first one.
	The coordinate are "hard", so use AIHardSoft suite to convert them into "soft" coordinate before working.
	*/
	AIAPI AIErr (*GetStrikethroughPoints)(const ATE::GlyphRunRef gr , ATE::ArrayRealPointRef* pPoints);
};


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

}


#endif
