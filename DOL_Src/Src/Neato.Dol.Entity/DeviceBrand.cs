using System;

namespace Neato.Dol.Entity {
    [Serializable]
    public class DeviceBrand : DeviceBrandBase {
        private string nameValue = string.Empty;

        public const string NameField = "Name";
        public string Name {
            get { return nameValue; }
            set { nameValue = value; }
        }

		public DeviceBrand() : base() {}

        public DeviceBrand(int id, string name) : base(id) {
            this.nameValue = name;
        }
    }
}