using System;

namespace Neato.Dol.Entity {
    [Serializable]
    public class Category : CategoryBase {
        private string nameValue = string.Empty;
        private bool visibleValue = false;
        private int targetPageValue = 2;
        private int sortOrder = 1;
        public const string NameField = "Name";
        public string Name {
            get { return nameValue; }
            set { nameValue = value; }
        }

        public bool Visible {
            get { return visibleValue; }
            set { visibleValue = value; }
        }

        public int TargetPage {
            get { return targetPageValue; }
            set { targetPageValue = value; }
        }

        public int SortOrder {
            get { return sortOrder; }
            set { sortOrder = value; }
        }

        public Category() : base() {}

        public Category(int id) : base(id) {}

        public Category(int id, bool visible) : base(id) {
            Visible = visible;
        }
    }
}