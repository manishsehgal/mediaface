namespace Neato.Dol.Entity {
    public class Authentication {
        public const string AuthenticationPage = "Authentication.aspx";
        public const string RegisteredUser = "RegisteredUser";
        public const string UnregisteredUser = "UnRegisteredUser";
        public const string GuestLogin = "Guest";
    }
}