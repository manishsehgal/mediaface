using System;

namespace Neato.Dol.Entity {
    [Serializable]
    public class IPAddress : IComparable {
        private byte field1Value;
        private byte field2Value;
        private byte field3Value;
        private byte field4Value;

        public const string IPMatchExpression = @"^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$";

        public IPAddress(byte field1, byte field2, byte field3, byte field4) {
            this.field1Value = field1;
            this.field2Value = field2;
            this.field3Value = field3;
            this.field4Value = field4;
        }

        public byte Field1 {
            get { return this.field1Value; }
        }

        public byte Field2 {
            get { return this.field2Value; }
        }

        public byte Field3 {
            get { return this.field3Value; }
        }

        public byte Field4 {
            get { return this.field4Value; }
        }

        public override string ToString() {
            return string.Format("{0}.{1}.{2}.{3}", Field1, Field2, Field3, Field4);
        }

        public static uint ToUInt32(IPAddress ipAddress) {
            return ipAddress.Field1 * (uint)Math.Pow(2, 24) + ipAddress.Field2 * (uint)Math.Pow(2, 16) + ipAddress.Field3 * (uint)Math.Pow(2, 8) + ipAddress.Field4;
        }

        public static IPAddress ToIPAddress(uint address) {
            byte field1 = (byte)((address & 0xff000000) >> 24);
            byte field2 = (byte)((address & 0xff000000) >> 16);
            byte field3 = (byte)((address & 0xff000000) >> 8);
            byte field4 = (byte)(address & 0xff000000);
            return new IPAddress(field1, field2, field3, field4);
        }

        public static IPAddress ToIPAddress(string address) {
            string[] fields = address.Split(new char[] {'.'});

            byte field1 = byte.Parse(fields[0]);
            byte field2 = byte.Parse(fields[1]);
            byte field3 = byte.Parse(fields[2]);
            byte field4 = byte.Parse(fields[3]);

            return new IPAddress(field1, field2, field3, field4);
        }

        public int CompareTo(object obj) {
            if(obj is IPAddress) {
                IPAddress ipAddress = (IPAddress) obj;
                return ToUInt32(this).CompareTo(ToUInt32(ipAddress));
            }
            throw new ArgumentException("object is not a Neato.Dol.Entity.IPAddress");    
        }

        public static IPAddress Previous(IPAddress ipAddress) {
            return ToIPAddress(ToUInt32(ipAddress) - 1);
        }

        public static IPAddress Next(IPAddress ipAddress) {
            return ToIPAddress(ToUInt32(ipAddress) + 1);
        }
    }
}