using System;

namespace Neato.Dol.Entity {
    [Serializable]
    public class PaperBase {
        private int idValue = 0;
        public const string IdField = "Id";

        public int Id {
            get { return idValue; }
            set { idValue = value; }
        }

        public bool IsNew {
            get { return idValue <= 0; }
        }

        public PaperBase() {}

        public PaperBase(int id) {
            this.idValue = id;
        }

        #region Equals, GetHashCode, ==, !=
        public override bool Equals(object obj) {
            PaperBase other = obj as PaperBase;
            if (other == null) {
                return false;
            } else {
                return this.Id == other.Id;
            }
        }

        public override int GetHashCode() {
            return idValue.GetHashCode();
        }

        public static bool operator ==(PaperBase first, PaperBase second) {
            if (object.Equals(first, null)) {
                return object.Equals(second, null);
            }
            return first.Equals(second);
        }

        public static bool operator !=(PaperBase first, PaperBase second) {
            if (object.Equals(first, null)) {
                return !object.Equals(second, null);
            }
            return !first.Equals(second);
        }
        #endregion
    }
}