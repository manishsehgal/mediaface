namespace Neato.Dol.Entity {
    public sealed class Constants {
        private Constants() {
        }

        public static string ClosePageFileName = "Close.html";
        public static string MaintenanceClosingDirectory = "Neato.Dol.MaintenanceClosing";
    
    }
}