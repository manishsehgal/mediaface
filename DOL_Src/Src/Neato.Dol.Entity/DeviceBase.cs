using System;

namespace Neato.Dol.Entity {
    [Serializable]
    public class DeviceBase {
        private int idValue = 0;

        public const string IdField = "Id";

        public int Id {
            get { return idValue; }
            set { idValue = value; }
        }

        public bool IsNew {
            get { return idValue <= 0; }
        }

        public DeviceBase() {}

        public DeviceBase(int id) {
            this.idValue = id;
        }

        #region Equals, GetHashCode, ==, !=
        public override bool Equals(object obj) {
            DeviceBase other = obj as DeviceBase;
            if (other == null) {
                return false;
            } else {
                return this.Id == other.Id;
            }
        }

        public override int GetHashCode() {
            return idValue.GetHashCode();
        }

        public static bool operator ==(DeviceBase first, DeviceBase second) {
            if (object.Equals(first, null)) {
                return object.Equals(second, null);
            }
            return first.Equals(second);
        }

        public static bool operator !=(DeviceBase first, DeviceBase second) {
            if (object.Equals(first, null)) {
                return !object.Equals(second, null);
            }
            return !first.Equals(second);
        }
        #endregion
    }
}