using System;

namespace Neato.Dol.Entity {
    [Serializable]
    public class Carrier : CarrierBase {
        private string nameValue = string.Empty;

        public const string NameField = "Name";
        public string Name {
            get { return nameValue; }
            set { nameValue = value; }
        }

        public Carrier() : base() {}

        public Carrier(int id, string name) : base(id) {
            this.nameValue = name;
        }
    }
}