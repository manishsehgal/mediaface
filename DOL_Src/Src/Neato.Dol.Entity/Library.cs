using System;
using System.Runtime.Serialization;
using System.Xml;

namespace Neato.Dol.Entity {
    [Serializable]
    public class Library : ISerializable {
        public Library() {}

        private XmlDocument xmlDataValue = new XmlDocument();

        public XmlDocument XmlData {
            get { return xmlDataValue; }
            set { xmlDataValue = value; }
        }

        public static string FolderName = "folder";

        #region ISerializable
        protected Library(SerializationInfo info, StreamingContext context) {
            this.xmlDataValue = new XmlDocument();
            this.xmlDataValue.InnerXml = info.GetString("xmlDataValueInnerXml");
        }

        void ISerializable.GetObjectData(
            SerializationInfo info, StreamingContext context) {
            info.AddValue("xmlDataValueInnerXml", this.xmlDataValue.InnerXml);
        }
        #endregion

        public XmlNode SelectFolderById(int folderId) {
            return xmlDataValue.SelectSingleNode(
                string.Format("//folder[@id=\"{0}\"]", folderId));
        }

        public XmlNode SelectItemById(int itemId) {
            return xmlDataValue.SelectSingleNode(
                string.Format("//item[@id=\"{0}\"]", itemId));
        }

        public string GetPathToCurrentLink
            (XmlNode currentFolder, string ctlPrefix) {
            string currentLinkContent = string.Format("'{0}{1}'",
                ctlPrefix, currentFolder.Attributes["id"].Value);

            return (currentFolder.ParentNode == null
                || currentFolder.ParentNode.Name == "Library")
                ? currentLinkContent
                : currentLinkContent + ", " +
                GetPathToCurrentLink(currentFolder.ParentNode, ctlPrefix);
        }

        public int DeleteFolder(int folderId) {
            XmlNode folder = SelectFolderById(folderId);
            XmlNode parentFolder = folder.ParentNode;
            parentFolder.RemoveChild(folder);
            return (parentFolder.Name == "Library")
                ? int.MinValue :
                Convert.ToInt32(parentFolder.Attributes["id"].Value);
        }

        public void EditFolder(int folderId, string newFolderName) {
            XmlNode folder = SelectFolderById(folderId);
            folder.Attributes["name"].Value = newFolderName;
        }
		
		public void EditFolderLibrary(int folderId, int newFolderLibrary) {
			XmlNode folder = SelectFolderById(folderId);
			folder.Attributes["lib"].Value = newFolderLibrary.ToString(System.Globalization.CultureInfo.InvariantCulture);
		}

        public void AddFolder
            (object parentFolderId, int newFolderId, string newFolderName) {
            XmlNode parentFolder = (parentFolderId == null)
                ? null: SelectFolderById((int)parentFolderId);
            XmlNode newFolder = xmlDataValue.CreateElement("folder");

            XmlAttribute folderIdAttr = xmlDataValue.CreateAttribute("id");
            folderIdAttr.Value = newFolderId.ToString();
            newFolder.Attributes.Append(folderIdAttr);

            XmlAttribute folderNameAttr = xmlDataValue.CreateAttribute("name");
            folderNameAttr.Value = newFolderName;
            newFolder.Attributes.Append(folderNameAttr);

			XmlAttribute folderLibAttr = xmlDataValue.CreateAttribute("lib");
			folderLibAttr.Value = ((int)ImageLibraries.Expanded).ToString(System.Globalization.CultureInfo.InvariantCulture);
			newFolder.Attributes.Append(folderLibAttr);

            if (parentFolderId == null) {
                xmlDataValue.FirstChild.AppendChild(newFolder);
            } else {
                parentFolder.AppendChild(newFolder);
            }
        }

        public bool IsFolderHasChildren(int parentFolderId) {
            XmlNode parentFolder = SelectFolderById(parentFolderId);
            return parentFolder.ChildNodes.Count > 0;
        }

        public void AddImage(int parentFolderId, int imageId) {
            XmlNode parentFolder = SelectFolderById(parentFolderId);
            XmlNode newFolder = xmlDataValue.CreateElement("item");

            XmlAttribute folderIdAttr = xmlDataValue.CreateAttribute("id");
            folderIdAttr.Value = imageId.ToString();
            newFolder.Attributes.Append(folderIdAttr);

            parentFolder.AppendChild(newFolder);
        }

        public void DeleteImage(int imageId) {
            XmlNode folder = SelectItemById(imageId);
            XmlNode parentFolder = folder.ParentNode;
            parentFolder.RemoveChild(folder);
        }
        public void DeleteImages(ImageLibItem[] items) {
            foreach(ImageLibItem item in items) {
                XmlNode folder = SelectItemById(item.Id);
                XmlNode parentFolder = folder.ParentNode;
                parentFolder.RemoveChild(folder);
            }
        }
        public int GetPrevFolderId(int folderId) {
            if (folderId <= 0) return int.MinValue;
            XmlNode node = SelectFolderById(folderId).PreviousSibling;
            return (node != null)
                ? Convert.ToInt32(node.Attributes["id"].Value)
                : int.MinValue;
        }
 
        public int GetNextFolderId(int folderId) {
            if (folderId <= 0) return int.MinValue;
            XmlNode node = SelectFolderById(folderId).NextSibling;
            return (node != null && node.Name == "folder")
                ? Convert.ToInt32(node.Attributes["id"].Value)
                : int.MinValue;
        }
 
        public void FolderSortOrderSwap(int folderId1, int folderId2) {
            XmlNode node1 = SelectFolderById(folderId1);
            XmlNode node2 = SelectFolderById(folderId2);
            node2.ParentNode.InsertAfter(node1, node2);
        }
    }
}