using System;

namespace Neato.Dol.Entity
{
    public enum ClientOS
    {
        Windows,
        PowerMac,
        IntelMac
    }
}
