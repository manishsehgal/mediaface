using System;

namespace Neato.Dol.Entity {
    [Serializable]
    public class DeviceRequest {
        private int requestDeviceIdValue;
        private string firstNameValue;
        private string lastNameValue;
        private string emailAddressValue;
        private string carrierValue;
        private string deviceBrandValue;
        private string deviceValue;
        private string commentValue;
        private DateTime createdValue;

        public DeviceRequest() {}

        public int RequestDeviceId {
            get { return requestDeviceIdValue;}
            set { requestDeviceIdValue = value;}
        }

        public string FirstName {
            get { return firstNameValue;}
            set { firstNameValue = value;}
        }

        public string LastName {
            get { return lastNameValue;}
            set { lastNameValue = value;}
        }

        public string EmailAddress {
            get { return emailAddressValue;}
            set { emailAddressValue = value;}
        }

        public string Carrier {
            get { return carrierValue; }
            set { carrierValue = value; }
        }

        public string DeviceBrand {
            get { return deviceBrandValue;}
            set { deviceBrandValue = value;}
        }

        public string Device {
            get { return deviceValue;}
            set { deviceValue = value;}
        }

        public string Comment {
            get { return commentValue;}
            set { commentValue = value;}
        }

        public DateTime Created {
            get { return createdValue; }
            set { createdValue = value; }
        }
    }
}