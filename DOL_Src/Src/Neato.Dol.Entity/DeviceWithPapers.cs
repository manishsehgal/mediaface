using System.Collections;

namespace Neato.Dol.Entity {
    public class DeviceWithPapers : Device {
        private PaperBase[] papersValue;

        public PaperBase[] Papers {
            get { return papersValue; }
        }

        public DeviceWithPapers(Device device) {
            Id = device.Id;
            Model = device.Model;
        }

        public void AddPapers(PaperBase[] papers) {
            ArrayList list = new ArrayList();
            foreach (PaperBase paper in papers) {
                list.Add(paper);
            }
            papersValue = (PaperBase[])list.ToArray(typeof(PaperBase));
        }
    }
}