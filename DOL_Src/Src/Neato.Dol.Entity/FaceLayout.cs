using System;
using System.Collections;
using System.Xml;

namespace Neato.Dol.Entity {
	public class FaceLayout {
		public FaceLayout() {
		}

        public FaceLayout(int id) {
            Id = id;
        }

        public void Parse(XmlNode faceNode) {
            Face.Id = int.Parse(faceNode.SelectSingleNode("@dbId").Value);
            try {
                Face.Guid = new Guid(faceNode.SelectSingleNode("@PUID").Value);
            } catch {}

            byte position = 0;
            foreach(XmlNode node in faceNode.SelectNodes("Text|TextEffect|Playlist")) {
                FaceLayoutItem faceLayoutItem = new FaceLayoutItem();
                faceLayoutItem.Parse(node, position ++);
                Add(faceLayoutItem);
            }
        }

        private int idValue = 0;
        public const string IdField = "Id";
        
        public int Id {
            get { return idValue; }
            set { idValue = value; }
        }

        public bool IsNew {
            get { return idValue <= 0; }
        }

        private string nameValue = string.Empty;
        public string Name {
            get { return nameValue; }
            set { nameValue = value; }
        }

        private Face faceValue = new Face();
        public Face Face {
            get { return faceValue; }
            set { faceValue = value; }
        }

        private ImageLibItem imageLibItemValue = new ImageLibItem(0);
        public ImageLibItem ImageLibItem {
            get { return imageLibItemValue; }
            set { imageLibItemValue = value; }
        }

        private ArrayList faceLayoutItemsValue = new ArrayList();
        public FaceLayoutItem[] FaceLayoutItems {
            get { return (FaceLayoutItem[])faceLayoutItemsValue.ToArray(typeof(FaceLayoutItem)); }
            //set { faceLayoutItemsValue = value; }
        }

        public void Add(FaceLayoutItem faceLayoutItem) {
            faceLayoutItemsValue.Add(faceLayoutItem);
        }

        #region Equals, GetHashCode, ==, !=
        public override bool Equals(object obj) 
        {
            FaceLayout other = obj as FaceLayout;
            if (other == null) 
            {
                return false;
            } 
            else 
            {
                return this.Id == other.Id;
            }
        }

        public override int GetHashCode() 
        {
            return idValue.GetHashCode();
        }

        public static bool operator ==(FaceLayout first, FaceLayout second) 
        {
            if (object.Equals(first, null)) 
            {
                return object.Equals(second, null);
            }
            return first.Equals(second);
        }

        public static bool operator !=(FaceLayout first, FaceLayout second) 
        {
            if (object.Equals(first, null)) 
            {
                return !object.Equals(second, null);
            }
            return !first.Equals(second);
        }
        #endregion
    }
}
