using System;

namespace Neato.Dol.Entity {
    [Serializable]
    public class BannerPage {
        private string idValue = string.Empty;
        private string nameValue = string.Empty;
        private string imgSrcValue = string.Empty;
        private int[] bannerIdsValue = new int[0];

        public string Id {
            set { idValue = value; }
            get { return idValue; }
        }

        public string Name {
            get { return nameValue; }
            set { nameValue = value; }
        }

        public string ImgSrc {
            get { return imgSrcValue; }
            set { imgSrcValue = value; }
        }

        public int[] BannerIds {
            get { return bannerIdsValue; }
            set { bannerIdsValue = value; }
        }

        public BannerPage() {}

        public BannerPage(string id, string name, string imgSrc, int[] bannerIds) {
            idValue = id;
            nameValue = name;
            imgSrcValue = imgSrc;
            bannerIdsValue = bannerIds;
        }
    }
}