using System;
namespace Neato.Dol.Entity
{
    //====================================
    // used to fill ImageLibrary control
    [Serializable]
	public class  ListSelectItem {
		public ListSelectItem() {
			Text="1";
			ImageUrl=null;
			NavigateUrl=null;
		    SkuNumber = null;
            Description = null;
		}
		public string Text;
		public string ImageUrl;
		public string NavigateUrl;
        public string SkuNumber;
        public string Description;
	}

    //====================================
    // used to fill ChoiceArea control
    public class ChoiceAreaItem {
        public class DropDownItem {
            public DropDownItem() {
                Text = "";
                Url = "";
            }
            public DropDownItem(string text, string url) {
                Text = text;
                Url = url;
            }
            public string Text;
            public string Url;
        }
        public ChoiceAreaItem() {
        }
        public DropDownItem[] DropDownItems; 
    }
}