namespace Neato.Dol.Entity {
    public enum PaperType {
        DieCut = 1,
        Universal = 2
    }
}