using System;

namespace Neato.Dol.Entity {
    [Serializable]
    public class IPFilter {
        private int idValue = 0;
        private IPAddress beginIPAddressValue;
        private IPAddress endIPAddressValue;
        private string descriptionValue;

        public const string IdField = "Id";

        #region Equals, GetHashCode, ==, !=
        public override bool Equals(object obj) 
        {
            IPFilter other = obj as IPFilter;
            if (other == null) 
            {
                return false;
            } 
            else 
            {
                return this.Id == other.Id;
            }
        }

        public override int GetHashCode() 
        {
            return idValue.GetHashCode();
        }

        public static bool operator ==(IPFilter first, IPFilter second) 
        {
            if (object.Equals(first, null)) 
            {
                return object.Equals(second, null);
            }
            return first.Equals(second);
        }

        public static bool operator !=(IPFilter first, IPFilter second) 
        {
            if (object.Equals(first, null)) 
            {
                return !object.Equals(second, null);
            }
            return !first.Equals(second);
        }
        #endregion

        public IPAddress BeginIPAddress {
            get { return beginIPAddressValue; }
            set { beginIPAddressValue = value; }
        }

        public IPAddress EndIPAddress {
            get { return endIPAddressValue; }
            set { endIPAddressValue = value; }
        }

        public int Id {
            get { return idValue; }
            set { idValue = value; }
        }

        public string Description {
            get { return descriptionValue; }
            set { descriptionValue = value; }
        }

        public IPFilter(string beginIPAddress, string endIPAddress) {
            this.BeginIPAddress = IPAddress.ToIPAddress(beginIPAddress);
            this.EndIPAddress = IPAddress.ToIPAddress(endIPAddress);
        }

        public IPFilter() {
            this.BeginIPAddress = new IPAddress(0, 0, 0, 0);
            this.EndIPAddress = new IPAddress(0, 0, 0, 0);
        }

        public IPFilter(IPAddress beginIPAddress, IPAddress endIPAddress, string description) {
            this.BeginIPAddress = beginIPAddress;
            this.EndIPAddress = endIPAddress;
            this.Description = description;
        }
    }
}