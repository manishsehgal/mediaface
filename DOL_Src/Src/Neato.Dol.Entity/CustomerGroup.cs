namespace Neato.Dol.Entity {
    public enum CustomerGroup {
        Trial = 0,
        MFO,
        MFOPE
    }
}