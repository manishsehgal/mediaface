using System;

namespace Neato.Dol.Entity {
    [Serializable]
    public class ImageLibItem {
        private int idValue;

        public int Id {
            get { return idValue; }
            set { idValue = value; }
        }

        public bool IsNew {
            get { return idValue <= 0; }
        }

        public ImageLibItem(int id) {
            this.idValue = id;
        }
    }
}