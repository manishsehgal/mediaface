namespace Neato.Dol.Entity {
    public enum MailType {
        None = 0,
        ForgotPassword,
        AccountCreated,

        //This type of messages now not used
        //TODO: These mail types deleted from DB, need delete from code
        TellAFriend,
        AddNewPhone,
        UserFeedback,
        PhoneRequestNotification,
    }
}