using System.Drawing;
using System.Globalization;
using System.Xml;

namespace Neato.Dol.Entity {
	public class FaceLayoutItem {
        private int idValue = 0;
        private float xValue;
        private float yValue;
        private float scaleXValue;
        private float scaleYValue;
        private byte positionValue;
        private string textValue;
        private string fontValue;
        private byte sizeValue;
        private bool isBoldValue;
        private bool isItalicValue;
        private System.Drawing.Color colorValue;
        private float angleValue;
        private float widthValue;
        private float heightValue;
        private string alignValue;
        private string unitTypeValue;
        private XmlNode effectNodeValue = null;

        private string TextUnit = "TextUnit";
        private string MultilineTextUnit = "MultilineTextUnit";
        private string TableUnit = "TableUnit";
        private string PlaylistInitText = "Playlist";

        public const string IdField = "Id";

		public FaceLayoutItem() 
        {
		}

        public void Parse(XmlNode xmlNode, byte position) {
            X = float.Parse(xmlNode.SelectSingleNode("@x").Value, CultureInfo.InvariantCulture);
            Y = float.Parse(xmlNode.SelectSingleNode("@y").Value, CultureInfo.InvariantCulture);
            scaleXValue = float.Parse(xmlNode.SelectSingleNode("@scaleX").Value, CultureInfo.InvariantCulture);
            scaleYValue = float.Parse(xmlNode.SelectSingleNode("@scaleY").Value, CultureInfo.InvariantCulture);
            Position = position;
            Angle = float.Parse(xmlNode.SelectSingleNode("@rotation").Value, CultureInfo.InvariantCulture);

            switch(xmlNode.Name) {
                case "Text":
                case "TextEffect":
                    Text = xmlNode.SelectSingleNode("@text").Value;
                    Align = xmlNode.SelectSingleNode("@align").Value;
                    Font = xmlNode.SelectSingleNode("Style/@font").Value;
                    Size = byte.Parse(xmlNode.SelectSingleNode("Style/@size").Value, CultureInfo.InvariantCulture);
                    IsBold = bool.Parse(xmlNode.SelectSingleNode("Style/@bold").Value);
                    IsItalic = bool.Parse(xmlNode.SelectSingleNode("Style/@italic").Value);
                    Color = ColorTranslator.FromHtml(xmlNode.SelectSingleNode("Style/@color").Value);
                    XmlNode widthNode = xmlNode.SelectSingleNode("@width|@multilineWidth");
                    Width = widthNode == null ? 0 : float.Parse(widthNode.Value, CultureInfo.InvariantCulture);
                    Height = 0;
                    EffectNode = xmlNode.SelectSingleNode("TextEffect");
                    XmlNode isMultilineNode = xmlNode.SelectSingleNode("@isMultiline");
                    UnitType = isMultilineNode == null ? TextUnit : (bool.Parse(isMultilineNode.Value) ? MultilineTextUnit : TextUnit);
                    break;
                case "Playlist":
                    Text = PlaylistInitText;
                    Align = xmlNode.SelectSingleNode("Columns/Column/@align").Value;
                    Font = xmlNode.SelectSingleNode("@font").Value;
                    Size = byte.Parse(xmlNode.SelectSingleNode("@fontSize").Value, CultureInfo.InvariantCulture);
                    IsBold = bool.Parse(xmlNode.SelectSingleNode("@bold").Value);
                    IsItalic = bool.Parse(xmlNode.SelectSingleNode("@italic").Value);
                    Color = ColorTranslator.FromHtml(xmlNode.SelectSingleNode("Columns/Column/@color").Value);
                    Width = float.Parse(xmlNode.SelectSingleNode("@width|@multilineWidth").Value, CultureInfo.InvariantCulture);
                    Height = float.Parse(xmlNode.SelectSingleNode("@height").Value, CultureInfo.InvariantCulture);
                    EffectNode = null;
                    UnitType = TableUnit;
                    break;
            }
        }

        public int Id {
			get { return idValue;}
			set { idValue = value; }
		}
        public bool IsNew {
            get { return idValue <= 0; }
        }
        public float X {
			get { return xValue;}
			set { xValue = value; }
		}
        public float Y {
			get { return yValue;}
			set { yValue = value; }
		}
        public float ScaleX {
            get { return scaleXValue;}
            set { scaleXValue = value; }
        }
        public float ScaleY {
            get { return scaleYValue;}
            set { scaleYValue = value; }
        }
        public byte Position {
			get { return positionValue;}
			set { positionValue = value; }
		}
        public string Text {
			get { return textValue;}
			set { textValue = value; }
		}
        public string Font {
			get { return fontValue;}
			set { fontValue = value; }
		}
        public byte Size {
			get { return sizeValue;}
			set { sizeValue = value; }
		}
        public bool IsBold {
			get { return isBoldValue;}
			set { isBoldValue = value; }
		}
        public bool IsItalic {
			get { return isItalicValue;}
            set { isItalicValue = value; }
		}
        public System.Drawing.Color Color {
			get { return colorValue;}
			set { colorValue = value; }
		}
        public float Angle {
			get { return angleValue;}
			set { angleValue = value; }
		}
        public float Width {
			get { return widthValue;}
			set { widthValue = value; }
		}
        public float Height {
			get { return heightValue;}
			set { heightValue = value; }
		}
        public string Align {
			get { return alignValue;}
			set { alignValue = value; }
		}
        public string UnitType {
			get { return unitTypeValue;}
			set { unitTypeValue = value; }
		}
        public XmlNode EffectNode {
			get { return effectNodeValue;}
			set { effectNodeValue = value; }
		}

        #region Equals, GetHashCode, ==, !=
        public override bool Equals(object obj) {
            FaceLayoutItem other = obj as FaceLayoutItem;
            if (other == null) {
                return false;
            } 
            else {
                return this.Id == other.Id;
            }
        }

        public override int GetHashCode() {
            return idValue.GetHashCode();
        }

        public static bool operator ==(FaceLayoutItem first, FaceLayoutItem second) {
            if (object.Equals(first, null)) {
                return object.Equals(second, null);
            }
            return first.Equals(second);
        }

        public static bool operator !=(FaceLayoutItem first, FaceLayoutItem second) {
            if (object.Equals(first, null)) {
                return !object.Equals(second, null);
            }
            return !first.Equals(second);
        }
        #endregion
    }
}
