namespace Neato.Dol.Entity {
    public sealed class SelectPage {
        public static string GetPageNameById(int id) {
            switch (id) {
                case 1:
                    return "PaperSelect.aspx";
                case 2:
                    return "MP3PlayerSelect.aspx";
                case 3:
                    return "DiscSelect.aspx";
            }

            return "";
        }

        public static string GetReadableNameById(int id) {
            switch (id) {
                case 1:
                    return "Choose Paper";
                case 2:
                    return "Choose MP3 player";
                case 3:
                    return "Choose Disc";
            }

            return "";
        }

        public static string[] GetPageNames() {
            string[] result = new string[3];
            result[0] = "Choose Paper";
            result[1] = "Choose MP3 player";
            result[2] = "Choose Disc";
            return result;
        }
    }

}