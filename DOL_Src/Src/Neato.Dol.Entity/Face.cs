using System;
using System.Collections;
using System.Drawing;
using System.Globalization;
using System.Xml;

using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Entity {
    [Serializable]
    public class Face: FaceBase {
        public Face(): base()  {}

        public Face(int id, XmlNode contour) : base(id) {
            this.contourValue = contour;
        }

        private Hashtable namesValue = new Hashtable();

        public void AddName(string culture, string name) {
            this.namesValue[culture] = name;
        }

        public string GetName(string culture) {
            return LocalizationHelper.GetString(culture, namesValue, "Key", "Value");
        }

        public IDictionaryEnumerator GetNames() {
            return this.namesValue.GetEnumerator();
        }

        private XmlNode contourValue = null;
        private Guid guidValue = Guid.Empty;//Guid.NewGuid();
        private bool isGuidValid = true;

        public XmlNode Contour {
            get { return contourValue; }
            set { contourValue = value; }
        }

        private PointF positionValue;

        public PointF Position {
            get { return positionValue; }
            set { positionValue = value; }
        }

        public Guid Guid {
            get { return guidValue; }
            set { guidValue = value; }
        }

        public bool IsGuidValid {
            get { return isGuidValid; }
            set { isGuidValid = value; }
        }
        public XmlNode XmlContent {
            get {
                XmlDocument doc = new XmlDocument();
                XmlNode node = doc.CreateElement("Face");
                XmlNode contour = doc.CreateElement("Contour");
                contour.InnerXml = Contour.InnerXml;

                node.AppendChild(contour);

                XmlAttribute idAttr = doc.CreateAttribute("id");
                idAttr.Value = Guid.ToString();
                node.Attributes.Append(idAttr);

                XmlAttribute dbIdAttr = doc.CreateAttribute("dbId");
                dbIdAttr.Value = Id.ToString(CultureInfo.InvariantCulture);
                node.Attributes.Append(dbIdAttr);

                XmlAttribute xAttr = doc.CreateAttribute("x");
                xAttr.Value = Position.X.ToString(CultureInfo.InvariantCulture);
                node.Attributes.Append(xAttr);

                XmlAttribute yAttr = doc.CreateAttribute("y");
                yAttr.Value = Position.Y.ToString(CultureInfo.InvariantCulture);
                node.Attributes.Append(yAttr);

                return node;
            }
        }
    }
}