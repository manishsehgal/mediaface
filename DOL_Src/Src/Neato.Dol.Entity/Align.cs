namespace Neato.Dol.Entity {
    public enum Align {
        None = 0,
        Left,
        Right
    }
}