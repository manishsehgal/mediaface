using System;

namespace Neato.Dol.Entity {
    [Serializable]
	public class HiddenBanner {
        private int bannerId = 0;
        private string pageName = string.Empty;

        public int BannerId {
            get { return bannerId; }
            set { bannerId = value; }
        }

        public string PageName {
            get { return pageName; }
            set { pageName = value; }
        }

		public HiddenBanner() {}

        public HiddenBanner(int bannerId, string pageName) {
            this.bannerId = bannerId;
            this.pageName = pageName;
        }
	}
}
