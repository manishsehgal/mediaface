using System;
using System.Collections;
using System.Globalization;

namespace Neato.Dol.Entity {
    [Serializable]
    public class Device : DeviceBase {
        private string modelValue = string.Empty;
        private DeviceBrand brandValue = null;
        private int deviceTypeIdValue = 0;
        private int ratingValue = 0;
        private string categoryValue = string.Empty;

        public Device() : base() {}

        public Device(int id, string model, DeviceBrand brand, int deviceTypeId) : base(id) {
            this.modelValue = model;
            this.brandValue = brand;
            this.deviceTypeIdValue = deviceTypeId;
        }

        public const string ModelField = "Model";
        public string Model {
            get { return modelValue; }
            set { modelValue = value; }
        }

        public DeviceBrand Brand {
            get { return brandValue; }
            set { brandValue = value; }
        }

        public int DeviceTypeId {
            get { return deviceTypeIdValue; }
            set { deviceTypeIdValue = value; }
        }

        public int Rating {
            get { return ratingValue; }
            set {
                if (value < 0 || value > 9) throw new ArgumentOutOfRangeException("Rating", value, "Rating must be in a range from 0 to 9.");
                ratingValue = value;
            }
        }

        public string Category {
            get { return categoryValue; }
            set { categoryValue = value; }
        }

        public const string FullModelField = "FullModel";
        public string FullModel {
            get { return string.Format("{0} {1}", brandValue.Name, modelValue); }
        }

        private class FullModelComparer : IComparer {
            IComparer comparer = null;
            public FullModelComparer(CultureInfo culture) {
                this.comparer = new CaseInsensitiveComparer(culture);
            }
            #region IComparer Members

            public int Compare(Device x, Device y) {
                if (x == null && y == null) return 0;
                if (x == null) return -1;
                if (y == null) return 1;
                return comparer.Compare(x.FullModel, y.FullModel);
            }

            public int Compare(object x, object y) {
                return Compare(x as Device, y as Device);
            }

            #endregion
        }
        public static IComparer GetFullModelComparer(CultureInfo culture) {
            return new FullModelComparer(culture);
        }

    }
}