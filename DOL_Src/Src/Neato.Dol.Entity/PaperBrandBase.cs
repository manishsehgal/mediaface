using System;

namespace Neato.Dol.Entity {
    [Serializable]
	public class PaperBrandBase {
        public PaperBrandBase() {}

        public PaperBrandBase(int id) {
            this.idValue = id;
        }

        public const string IdField = "Id";
        private int idValue = 0;

        public int Id {
            get { return idValue; }
            set { idValue = value; }
        }

        public bool IsNew {
            get { return idValue <= 0; }
        }

        #region Equals, GetHashCode, ==, !=
        public override bool Equals(object obj) {
            PaperBrandBase other = obj as PaperBrandBase;
            if (other == null) {
                return false;
            } else {
                return this.Id == other.Id;
            }
        }

        public override int GetHashCode() {
            return idValue.GetHashCode();
        }

        public static bool operator ==(PaperBrandBase first, PaperBrandBase second) {
            if (object.Equals(first, null)) {
                return object.Equals(second, null);
            }
            return first.Equals(second);
        }

        public static bool operator !=(PaperBrandBase first, PaperBrandBase second) {
            if (object.Equals(first, null)) {
                return !object.Equals(second, null);
            }
            return !first.Equals(second);
        }
        #endregion
	}
}
