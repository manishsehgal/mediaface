using System;

namespace Neato.Dol.Entity {
    [Serializable]
    public class PdfCalibration {
        private Single x;
        private Single y;
        private Single diamInnerL;
        private Single diamOuterL;
        private Single diamInnerB;
        private Single diamOuterB;
        private bool isSpecified;

        public Single X {
            get { return x; }
            set { x = value; isSpecified = true; }
        }

        public Single Y {
            get { return y; }
            set { y = value; isSpecified = true; }
        }

        public Single DiameterInnerL {
            get { return diamInnerL; }
            set { diamInnerL = value; isSpecified = true; }
        }

        public Single DiameterOuterL {
            get { return diamOuterL; }
            set { diamOuterL = value; isSpecified = true; }
        }

        public Single DiameterInnerB {
            get { return diamInnerB; }
            set { diamInnerB = value; isSpecified = true; }
        }

        public Single DiameterOuterB {
            get { return diamOuterB; }
            set { diamOuterB = value; isSpecified = true; }
        }

        public bool IsSpecified {
            get { return isSpecified; }
            set { isSpecified = value; }
        }

        public PdfCalibration() {
            this.x = 0;
            this.y = 0;
            this.diamInnerL = -1;
            this.diamOuterL = -1;
            this.diamInnerB = -1;
            this.diamOuterB = -1;
            this.isSpecified = false;
        }
    }
}