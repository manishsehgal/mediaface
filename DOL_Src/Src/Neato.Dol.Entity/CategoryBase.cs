using System;

namespace Neato.Dol.Entity {
    [Serializable]
    public class CategoryBase {
        public CategoryBase() {}

        public CategoryBase(int id) {
            this.idValue = id;
        }

        public const string IdField = "Id";
        private int idValue = 0;

        public int Id {
            get { return idValue; }
            set { idValue = value; }
        }

        public bool IsNew {
            get { return idValue <= 0; }
        }

        #region Equals, GetHashCode, ==, !=
        public override bool Equals(object obj) {
            CategoryBase other = obj as CategoryBase;
            if (other == null) {
                return false;
            } else {
                return this.Id == other.Id;
            }
        }

        public override int GetHashCode() {
            return idValue.GetHashCode();
        }

        public static bool operator ==(CategoryBase first, CategoryBase second) {
            if (object.Equals(first, null)) {
                return object.Equals(second, null);
            }
            return first.Equals(second);
        }

        public static bool operator !=(CategoryBase first, CategoryBase second) {
            if (object.Equals(first, null)) {
                return !object.Equals(second, null);
            }
            return !first.Equals(second);
        }
        #endregion
    }
}