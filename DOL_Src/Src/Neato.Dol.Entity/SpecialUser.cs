using System;

namespace Neato.Dol.Entity {
    [Serializable]
	public class SpecialUser : SpecialUserBase {

        public SpecialUser() : base() {}

        public SpecialUser(int id, string email, bool showPaper, bool notifyPaperState, bool notifyBanner) : base(id) {
            this.emailValue = email;
            this.showPaperValue = showPaper;
            this.notifyPaperStateValue = notifyPaperState;
            this.notifyBannerValue = notifyBanner;
        }

        private string emailValue = string.Empty;
        public  string Email {
            get { return emailValue; }
            set { emailValue = value; }
        }

        private bool showPaperValue = false;
        public  bool ShowPaper {
            get { return showPaperValue; }
            set { showPaperValue = value; }
        }

        private bool notifyPaperStateValue = false;
        public  bool NotifyPaperState {
            get { return notifyPaperStateValue; }
            set { notifyPaperStateValue = value; }
        }

        private bool notifyBannerValue = false;
        public  bool NotifyBanner {
            get { return notifyBannerValue; }
            set { notifyBannerValue = value; }
        }

	}
}
