using System;
using System.Threading;
using System.Collections;
using System.Globalization;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace Neato.Dol.Entity {
    [Serializable]
    public class Project : ISerializable {
        private const int LastVersion = 1;
        private int version = 1;
        
        public Project() {}

        public Project(Paper paper, DeviceType deviceType) : this() {
            this.projectXmlValue = new XmlDocument();

            XmlElement projectNode = this.projectXmlValue.CreateElement("Project");
            projectNode.SetAttribute("appName", Configuration.AppName);
            projectNode.SetAttribute("user", Thread.CurrentPrincipal.Identity.Name);
            projectNode.SetAttribute("paperDbId", paper.Id.ToString());
            projectNode.SetAttribute("minPaperAccess", ((int)paper.MinPaperAccess).ToString());
            this.projectXmlValue.AppendChild(projectNode);

            XmlElement facesNode = this.projectXmlValue.CreateElement("Faces");
            projectNode.AppendChild(facesNode);

            foreach (Face face in paper.Faces) {
                XmlElement faceNode = this.projectXmlValue.CreateElement("Face");
                faceNode.SetAttribute("id", Guid.NewGuid().ToString("N"));
                faceNode.SetAttribute("dbId", face.Id.ToString(CultureInfo.InvariantCulture));
                faceNode.SetAttribute("PUID", face.Guid.ToString("N"));
                faceNode.SetAttribute("x", face.Position.X.ToString(CultureInfo.InvariantCulture));
                faceNode.SetAttribute("y", face.Position.Y.ToString(CultureInfo.InvariantCulture));
                facesNode.AppendChild(faceNode);

                XmlElement contourNode = this.projectXmlValue.CreateElement("Contour");
                contourNode.InnerXml = face.Contour.InnerXml;
                contourNode.SetAttribute("x", 0.ToString(CultureInfo.InvariantCulture));
                contourNode.SetAttribute("y", 0.ToString(CultureInfo.InvariantCulture));
                faceNode.AppendChild(contourNode);

                XmlElement localizationNode = this.projectXmlValue.CreateElement("Localization");
                faceNode.AppendChild(localizationNode);

                XmlElement captionNode = this.projectXmlValue.CreateElement("Caption");
                localizationNode.AppendChild(captionNode);

                IEnumerator captions = face.GetNames();
                captions.Reset();
                while (captions.MoveNext()) {
                    DictionaryEntry caption = (DictionaryEntry)captions.Current;
                    XmlElement cultureNode = this.projectXmlValue.CreateElement("Culture");
                    cultureNode.SetAttribute("key", caption.Key.ToString());
                    cultureNode.SetAttribute("value", caption.Value.ToString());
                    captionNode.AppendChild(cultureNode);
                }
            }

            XmlElement paperNode = this.projectXmlValue.CreateElement("Paper");
            paperNode.SetAttribute("w", paper.Size.Width.ToString(CultureInfo.InvariantCulture));
            paperNode.SetAttribute("h", paper.Size.Height.ToString(CultureInfo.InvariantCulture));
            paperNode.SetAttribute("PaperType", paper.PaperType.ToString());
            paperNode.SetAttribute("Orientation", paper.Orientation);
            paperNode.SetAttribute("PaperBrand", paper.Brand.Name.ToLower());
            projectNode.AppendChild(paperNode);

            XmlElement propertiesNode = this.projectXmlValue.CreateElement("Properties");
            propertiesNode.SetAttribute("name", paper.Name);
            propertiesNode.SetAttribute("DeviceType", deviceType.ToString());
            projectNode.AppendChild(propertiesNode);

            this.projectPaperValue = new PaperBase(paper.Id);
            paperTypeValue = paper.PaperType;
            deviceTypeValue = deviceType;
        }

        #region ISerializable
        protected Project(SerializationInfo info, StreamingContext context) {
            // do not remove try - catch
            try {
                this.version = (int)info.GetValue("version", typeof(int));
            } catch (SerializationException) {
                this.version = 0;
            }

            this.projectImagesValue = (Hashtable)info.GetValue("projectImagesValue", typeof(Hashtable));
            
            //do not remove try-catch
            try {
                this.projectProcessedImagesValue =  (Hashtable)info.GetValue("projectProcessedImagesValue", typeof(Hashtable));
            } catch (SerializationException) {
                this.projectProcessedImagesValue =  new Hashtable();
            }

            this.projectXmlValue = new XmlDocument();
            this.projectXmlValue.InnerXml = info.GetString("projectXmlValueInnerXml");

            // do not remove try - catch
            try {
                this.projectPaperValue = (PaperBase)info.GetValue("projectPaperValue", typeof(PaperBase));
            } catch (SerializationException) {
                this.projectPaperValue = null;
            }

            try {
                this.paperTypeValue = (PaperType)info.GetValue("paperTypeValue", typeof(PaperType));
            } catch (SerializationException) {
                this.paperTypeValue = PaperType.DieCut;
            }

            this.version = LastVersion;
        }

        void ISerializable.GetObjectData(
            SerializationInfo info, StreamingContext context) {
            info.AddValue("version", this.version);
            info.AddValue("projectXmlValueInnerXml", this.projectXmlValue.InnerXml);
            info.AddValue("projectImagesValue", this.projectImagesValue);
            info.AddValue("projectProcessedImagesValue", this.projectProcessedImagesValue);
            info.AddValue("projectPaperValue", this.projectPaperValue);
            info.AddValue("paperTypeValue", this.paperTypeValue);
        }
        #endregion

        public Project Clone() {
            Project project = new Project();

            project.version = this.version;
            project.ProjectPaper = this.ProjectPaper;
            project.ProjectXml = (XmlDocument)this.ProjectXml.Clone();
            project.paperTypeValue = this.paperTypeValue;
            project.deviceTypeValue = this.deviceTypeValue;
            project.projectImagesValue = (Hashtable)this.projectImagesValue.Clone();
            project.projectProcessedImagesValue = (Hashtable)this.projectProcessedImagesValue.Clone();
            
            return project;
        }

        private Hashtable projectImagesValue = new Hashtable();
        private Hashtable projectProcessedImagesValue = new Hashtable();

        private XmlDocument projectXmlValue = null;

        public XmlDocument ProjectXml {
            get { return projectXmlValue; }
            set { PutProjectXml(value); }
        }

        public XmlDocument Xml {
            get { return projectXmlValue; }
            set { PutProjectXmlWithoutImageSync(value); }
        }

        private void PutProjectXml(XmlDocument project) {
            if (project.FirstChild.NodeType != XmlNodeType.XmlDeclaration) {
                XmlDeclaration declaration = project.CreateXmlDeclaration("1.0", Encoding.UTF8.WebName, null);
                XmlElement root = project.DocumentElement;
                project.InsertBefore(declaration, root);
            }
            projectXmlValue = project;
            CheckPaperIdCompliance();
            SynchronizeImageContent();
        }
        
        private void PutProjectXmlWithoutImageSync(XmlDocument project) {
            if (project.FirstChild.NodeType != XmlNodeType.XmlDeclaration) {
                XmlDeclaration declaration = project.CreateXmlDeclaration("1.0", Encoding.UTF8.WebName, null);
                XmlElement root = project.DocumentElement;
                project.InsertBefore(declaration, root);
            }
            projectXmlValue = project;
            CheckPaperIdCompliance();
        }
        
        private void CheckPaperIdCompliance() {
            XmlDocument project = projectXmlValue;
            XmlElement projectNode = project.DocumentElement;
            if(projectNode == null) 
                return;
            string paperDbId = projectNode.GetAttribute("paperDbId");
            if(paperDbId == null)
                return;
            int paperId = 0;
            try 
            {
                paperId = int.Parse(paperDbId);
                if(ProjectPaper.Id != paperId && paperId != 0)
                    ProjectPaper.Id = paperId;

            } 
            catch {}

            
            
        }
        public void SynchronizeImageContent() {
            ArrayList deleteImageIds = new ArrayList();
            ArrayList deleteUnitIds = new ArrayList();
            foreach (Guid imageId in projectImagesValue.Keys) {
                string id = imageId.ToString("N");
                if (projectXmlValue.InnerXml.IndexOf(id) == -1) {
                    deleteImageIds.Add(imageId);
                    continue;
                }
                if (projectProcessedImagesValue[imageId] != null) {
                    foreach(long uid in ((Hashtable)projectProcessedImagesValue[imageId]).Keys ) {
                        string uidmatch = "unitId=\""+uid.ToString();
                        if(projectXmlValue.InnerXml.IndexOf(uidmatch) == -1) {
                            deleteUnitIds.Add(uid);
                        }
                    }
                }
            }

            foreach (Guid imageId in deleteImageIds) {
                projectImagesValue.Remove(imageId);
                projectProcessedImagesValue.Remove(imageId);
            }
            foreach(Guid imkey in projectProcessedImagesValue.Keys) {
                Hashtable imtable = (Hashtable)projectProcessedImagesValue[imkey];
                foreach(long uidtodel in deleteUnitIds) {
                    if(imtable[uidtodel]!=null)
                        imtable.Remove(uidtodel);
                    if(imtable.Count<1)
                        projectProcessedImagesValue.Remove(imkey);
                }
            }
        }

        private PaperBase projectPaperValue = null;

        public PaperBase ProjectPaper {
            get { return projectPaperValue; }
            set { projectPaperValue = value; }
        }

        private PaperType paperTypeValue = PaperType.DieCut;
        private DeviceType deviceTypeValue = DeviceType.Undefined;

        public DeviceType DeviceType {
            get { return deviceTypeValue; }
        }

        public int Size {
            get {
                int projectXmlSize = ProjectXml.OuterXml.Length;
                int projectImagesSize = 0;
                foreach (byte[] projectImage in projectImagesValue.Values) {
                    projectImagesSize += projectImage.Length;
                }
                foreach(Hashtable tb in projectProcessedImagesValue.Values) {
                   foreach (ImageStorage projectImageStg in tb.Values) 
                    {
                        projectImagesSize += projectImageStg.image.Length;
                    } 
                }

                //foreach (ImageStorage projectImageStg in projectProcessedImagesValue.Values) 
                //{
                  //  projectImagesSize += projectImageStg.image.Length;
                //}

                return projectXmlSize + projectImagesSize;
            }
        }

        public void RemoveImage(Guid id) {
            projectImagesValue.Remove(id);
        }

        public XmlNode AddImage(byte[] fileData, int width, int height) {
            Guid id = Guid.NewGuid();
            projectImagesValue.Add(id, fileData);

            XmlElement imageNode = projectXmlValue.CreateElement("Image");

            Random random = new Random();

            const float WorkspaceWidth = 410f;
            const float WorkspaceHeight = 340f;

            float scale = CalculateImageScale(WorkspaceWidth, width, WorkspaceHeight, height);
            float rotation = 0f;
            bool isCurrentUnit = true;
            float x = random.Next(10);
            float y = random.Next(10);

            imageNode.SetAttribute("x", x.ToString(CultureInfo.InvariantCulture));
            imageNode.SetAttribute("y", y.ToString(CultureInfo.InvariantCulture));
            imageNode.SetAttribute("scaleX", scale.ToString(CultureInfo.InvariantCulture));
            imageNode.SetAttribute("scaleY", scale.ToString(CultureInfo.InvariantCulture));
            imageNode.SetAttribute("initScale", scale.ToString(CultureInfo.InvariantCulture));
            imageNode.SetAttribute("id", id.ToString("N"));
            imageNode.SetAttribute("isCurrentUnit", isCurrentUnit.ToString(CultureInfo.InvariantCulture));
            imageNode.SetAttribute("rotation", rotation.ToString(CultureInfo.InvariantCulture));
            imageNode.SetAttribute("isNewImage", bool.TrueString);

            return imageNode;
        }

        public static float CalculateImageScale(float workspaceWidth, int width, float workspaceHeight, int height) {
            float scaleWidth = workspaceWidth / width;
            float scaleHeight = workspaceHeight / height;
            int scaleFactor = 3;
            float scale = ((scaleWidth < scaleHeight) ? scaleWidth : scaleHeight) * 100 / scaleFactor;
            return (scale > 100) ? 100 : scale;
        }

        public byte[] GetImage(Guid id) {
            byte[] imageArray = null;
            /*if(projectProcessedImagesValue[id] != null)
                imageArray = ((ImageStorage)projectProcessedImagesValue[id]).image;
            else */
                imageArray = (byte[])projectImagesValue[id];
    
            if (imageArray == null) {
                imageArray = new byte[0];
            }
            return imageArray;
        }
        public byte[] GetImage(Guid id,long imgId) 
        {
            byte[] imageArray = null;
            if(projectProcessedImagesValue[id]!= null && ((Hashtable)projectProcessedImagesValue[id])[imgId] != null)
                imageArray = ((ImageStorage)((Hashtable)projectProcessedImagesValue[id])[imgId]).image;
            else {
                imageArray = (byte[])projectImagesValue[id];
            }
    
            if (imageArray == null) 
            {
                imageArray = new byte[0];
            }
            return imageArray;
        }
        public byte[] GetOriginalImage(Guid id) 
        {
            byte[] imageArray = null;
           imageArray = (byte[])projectImagesValue[id];
    
            if (imageArray == null) 
            {
                imageArray = new byte[0];
            }
            return imageArray;
        }
        
        public byte[] GetProcessedImage(Guid id, ImageEffectProperties imgEffectProps,long imgId) {
            if(projectProcessedImagesValue[id]==null )
                return null;
            if(((Hashtable)projectProcessedImagesValue[id])[imgId]==null) 
            {
                foreach(long imgkey in ((Hashtable)projectProcessedImagesValue[id]).Keys) 
                {
                    ImageStorage stg = (ImageStorage)((Hashtable)projectProcessedImagesValue[id])[imgkey];
                    if(stg.ImageEffProperties.IsEq(imgEffectProps)) 
                    {
                        ((Hashtable)projectProcessedImagesValue[id])[imgId] = stg;
                        break;
                    }
                }
            }
            
            ImageStorage imgStg = (ImageStorage)((Hashtable)projectProcessedImagesValue[id])[imgId];
            if(imgStg.ImageEffProperties.IsEq(imgEffectProps)) {
                return imgStg.image;
            }
            return null;

        }
        public void AddProcessedImage(Guid id,ImageEffectProperties props,byte [] image,long imgId) {
            if(props.IsNoEffect()) {
                if(projectProcessedImagesValue[id]!=null)
                    ((Hashtable)projectProcessedImagesValue[id]).Remove(imgId);
                return;
            }
            Hashtable img;
            if((img = (Hashtable)projectProcessedImagesValue[id])==null) 
                img = new Hashtable();
            img[imgId] = new ImageStorage(props,image);
            projectProcessedImagesValue[id] = img;
            //}else {
              //  Ha
            //}
            //if(projectProcessedImagesValue[id] != null)
            //{
              //  projectProcessedImagesValue[id] = new ImageStorage(props,image);
            //}
            //else 
              //  projectProcessedImagesValue.Add(id,new );
            
        }
        public void AddTemplateImageToProject(byte[] image,Guid id){
            if(projectImagesValue[id] == null) {
                projectImagesValue[id] = image;
            }
        }

    }
}
