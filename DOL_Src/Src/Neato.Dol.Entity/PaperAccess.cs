namespace Neato.Dol.Entity {
    public enum PaperAccess {
        Unregistered = 1,
        MFO,
        MFOPE,
    }
}