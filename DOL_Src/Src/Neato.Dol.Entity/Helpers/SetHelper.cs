using System;
using System.Diagnostics;

namespace Neato.Dol.Entity.Helpers {
    public sealed class SetHelper {

        public static bool IsIntersect(IComparable from1, IComparable to1, IComparable from2, IComparable to2) {
            Debug.Assert(from1.CompareTo(to1) <= 0, "First interval is incorrect");
            Debug.Assert(from2.CompareTo(to2) <= 0, "Second interval is incorrect");

            return from1.CompareTo(to2) <= 0 && from2.CompareTo(to1) <= 0;
        }

        public static bool IsInInterval(IComparable point, IComparable from, IComparable to) {
            Debug.Assert(from.CompareTo(to) <= 0, "Interval is incorrect");

            return from.CompareTo(point) <= 0 && to.CompareTo(point) >= 0;
        }

        public static bool IsSubInterval(IComparable innerFrom, IComparable innerTo, IComparable outerFrom, IComparable outerTo) {
            Debug.Assert(innerFrom.CompareTo(innerTo) <= 0, "First interval is incorrect");
            Debug.Assert(outerFrom.CompareTo(outerTo) <= 0, "Second interval is incorrect");

            return outerFrom.CompareTo(innerFrom) <= 0 && innerTo.CompareTo(outerTo) <= 0;
        }

        public static void GetIntervalIntersection(IComparable firstFrom, IComparable firstTo, IComparable secondFrom, IComparable secondTo, out IComparable resultFrom, out IComparable resultTo) {
            if (IsIntersect(firstFrom, firstTo, secondFrom, secondTo)) {
                resultFrom = firstFrom.CompareTo(secondFrom) <= 0 ? secondFrom : firstFrom;
                resultTo = firstTo.CompareTo(secondTo) <= 0 ? firstTo : secondTo;
            } else {
                resultFrom = null;
                resultTo = null;
            }
        }

        public static IComparable GetIntersectionStartPoint(IComparable firstFrom, IComparable firstTo, IComparable secondFrom, IComparable secondTo) {
            IComparable resultFrom = null;
            if (IsIntersect(firstFrom, firstTo, secondFrom, secondTo)) {
                resultFrom = firstFrom.CompareTo(secondFrom) <= 0 ? secondFrom : firstFrom;
            }
            return resultFrom;
        }

        public static IComparable GetIntersectionEndPoint(IComparable firstFrom, IComparable firstTo, IComparable secondFrom, IComparable secondTo) {
            IComparable resultTo = null;
            if (IsIntersect(firstFrom, firstTo, secondFrom, secondTo)) {
                resultTo = firstTo.CompareTo(secondTo) <= 0 ? firstTo : secondTo;
            }
            return resultTo;
        }

        private SetHelper() {}
    }
}