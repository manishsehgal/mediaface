using System;

namespace Neato.Dol.Entity.Helpers {
    public sealed class KeyValidationHelper {
        private KeyValidationHelper() {}

        //private const string SerialNumberMediaFace2 =       "26rw95bc96n";
        private const string SerialNumberMediaFace4Sample = "7135a11343691a16d0441";
        private const string SerialNumberMediaFace5Sample = "15XWE9Q3YH";
        private const string GroupSeparator = "-";

        public static bool IsSerialNumberValid(string number) {
            try {
                //if (number == SerialNumberMediaFace2) return true;

                string[] pureNumberArray = GetPureNumberArray(number);
                return IsSerialNumberMF4Or5Valid (pureNumberArray);
            }
            catch {
                return false;
            }
        }

        public static string[] GetPureNumberArray(string number) {
            string pureNumber = string.Empty;
            string[] pureNumberArray = new string[4];

            for(int i = 0; i < number.Length; i++) {
                if (Char.IsLetterOrDigit(number[i])) {
                    pureNumber += number[i];
                }
            }

            if (pureNumber.Length == SerialNumberMediaFace4Sample.Length) {
                pureNumberArray[0] = pureNumber.Substring(0, 6);
                pureNumberArray[1] = pureNumber.Substring(6, 6);
                pureNumberArray[2] = pureNumber.Substring(12, 4);
                pureNumberArray[3] = pureNumber.Substring(16, 5);
            } else if (pureNumber.Length == SerialNumberMediaFace5Sample.Length) {
                pureNumberArray[0] = pureNumber.Substring(0, 3);
                pureNumberArray[1] = pureNumber.Substring(3, 3);
                pureNumberArray[2] = pureNumber.Substring(6, 2);
                pureNumberArray[3] = pureNumber.Substring(8, 2);
            }

            return pureNumberArray;
        }

        public static string GetNumberInCorrectFormat(string number) {
            /*if (number == SerialNumberMediaFace2) {
                return number;
            } else*/ {
                string[] pureNumberArray = GetPureNumberArray(number);
                bool isMF5Number = (pureNumberArray[0].Length == 3);
                string result = (isMF5Number)
                    ? string.Format("{0}{1}{2}{3}{4}{5}{6}",
                    pureNumberArray[0].ToUpper(),
                    GroupSeparator,
                    pureNumberArray[1].ToUpper(),
                    GroupSeparator,
                    pureNumberArray[2].ToUpper(),
                    GroupSeparator,
                    pureNumberArray[3].ToUpper())
                    : string.Format("{0}{1}{2}{3}{4}{5}{6}",
                    pureNumberArray[0],
                    GroupSeparator,
                    pureNumberArray[1],
                    GroupSeparator,
                    pureNumberArray[2],
                    GroupSeparator,
                    pureNumberArray[3]);
                return result;
            }
        }
        
        public static bool IsSerialNumberMF4Or5Valid(string[] numberArray) {
            return NeatoDolCRC32.ValidateUtil.CheckSerialNumber(
                numberArray[0],
                numberArray[1],
                numberArray[2],
                numberArray[3]);
        }
    }
}