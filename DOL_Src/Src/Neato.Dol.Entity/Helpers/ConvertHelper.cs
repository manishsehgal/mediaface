using System;
using System.IO;
using System.Text;
using System.Xml;

using ICSharpCode.SharpZipLib.Zip.Compression.Streams;

namespace Neato.Dol.Entity.Helpers {
    public class ConvertHelper {
        private ConvertHelper() {}

        public static byte[] StreamToByteArray(Stream stream) {
            byte[] resultArray = new byte[stream.Length];
            stream.Position = 0;
            stream.Read(resultArray, 0, resultArray.Length);
            return resultArray;
        }

        public static byte[] XmlNodeToByteArray(XmlNode node) {
            MemoryStream stream = new MemoryStream();
            BinaryWriter wr = new BinaryWriter(stream);
            wr.Write(node.OuterXml.ToCharArray());
            return stream.ToArray();
        }

        public static XmlNode ByteArrayToXmlNode(byte[] array) {
            if (array.Length == 0) return null;
            MemoryStream stream = new MemoryStream(array);
            BinaryReader reader = new BinaryReader(stream);
            
            string nodeString = new string(reader.ReadChars((int)stream.Length));
            XmlDocument doc = new XmlDocument();
            doc.InnerXml = nodeString;
            return doc.FirstChild;
        }

        public static byte[] StringToByteArray(string str) {
            char[] chars = str.ToCharArray();
            Encoder uniEncoder = Encoding.UTF8.GetEncoder();
            int byteCount = uniEncoder.GetByteCount(chars, 0, chars.Length, true);
            byte[] bytes = new byte[byteCount];
            uniEncoder.GetBytes(chars, 0, chars.Length, bytes, 0, true);
            return bytes;
        }

        public static string ByteArrayToString(byte[] array) {
            if (array.Length == 0) return null;
            MemoryStream stream = new MemoryStream(array);
            BinaryReader reader = new BinaryReader(stream);
            return new string(reader.ReadChars((int)stream.Length));
        }

        public static string ZipAndBase64String(byte[] input) {
            MemoryStream memStream = new MemoryStream();
            DeflaterOutputStream deflateStream = new DeflaterOutputStream(memStream);
            deflateStream.Write(input, 0, input.Length);
            deflateStream.Finish();
            deflateStream.Close();
            return Convert.ToBase64String(memStream.ToArray());
        }

        public static byte[] FromBase64AndUnzip(string inputString) {
            byte[] input = Convert.FromBase64String(inputString);
            MemoryStream memStream = new MemoryStream();
            InflaterInputStream deflateStream = new InflaterInputStream(memStream);
            deflateStream.Write(input, 0, input.Length);
            deflateStream.Close();
            return memStream.ToArray();
        }
    }
}