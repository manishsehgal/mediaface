using System;
using System.Collections;
using System.Diagnostics;
using System.DirectoryServices;

namespace Neato.Dol.Entity.Helpers {

    #region class WebSiteInfo

    public class WebSiteInfo {
        private string _wsName = string.Empty;

        public string Name {
            get { return _wsName; }
        }

        private int _wsNum = 0;

        public int Number {
            get { return _wsNum; }
        }

        private string _url = string.Empty;

        public string URL {
            get { return _url; }
        }

        public DirectoryEntry DirectoryEntry = null;

        private string extractURL(string extractFrom) {
            if (extractFrom.IndexOf(".") <= 0) {
                string[] tokens = extractFrom.Split(':');
                if (tokens[1] != "80") {
                    return "localhost" + ":" + tokens[1];
                }
                else {
                    return "localhost";
                }
            }
            else if (extractFrom.Length > 8) {
                string t = extractFrom.Substring(0, extractFrom.LastIndexOf(':'));

                if (t.EndsWith(":80"))
                    return t.Substring(0, t.Length - 3);
                else
                    return t;
            }
            else
                return string.Empty;
        }

        public WebSiteInfo(DirectoryEntry createFrom) {
            string siteName = (string) createFrom.Properties["ServerComment"].Value;
            int number = int.Parse(createFrom.Name);
            _wsName = siteName;
            _wsNum = number;
            string urlVal = string.Empty;
            if (createFrom.Properties["ServerBindings"].Value.GetType().IsArray)
                urlVal = (string) ((object[]) createFrom.Properties["ServerBindings"].Value)[0];
            else
                urlVal = (string) createFrom.Properties["ServerBindings"].Value;
            _url = extractURL(urlVal);
            DirectoryEntry = createFrom;
        }

        public override string ToString() {
            return Name;
        }
    }

    public class WebSiteInfoCollection : Hashtable {
        public void Add(WebSiteInfo siteInfo) {
            base.Add(siteInfo.Number, siteInfo);
        }
    }

    #endregion

    #region class VirtualDirectoryInfo

    public class VirtualDirectoryInfo {
        private string _name = string.Empty;
        private const string PropPath = "Path";

        public string Name {
            get {
                if (Parent == null)
                    return @"/";
                else
                    return _name;
            }
        }

        private VirtualDirectoryInfo _parent = null;

        public VirtualDirectoryInfo Parent {
            get { return _parent; }
        }

        private VirtualDirectoryInfoCollection _children = new VirtualDirectoryInfoCollection();

        public VirtualDirectoryInfoCollection Children {
            get { return _children; }
        }

        public string FullName {
            get {
                if (Parent != null)
                    return Parent.FullName + Name + "/";
                else
                    return "/";
            }
        }

        public DirectoryEntry DirectoryEntry = null;

        private string _path = string.Empty;

        public string Path {
            get { return _path; }
            set {
                DirectoryEntry.Properties[PropPath][0] = value;
                DirectoryEntry.CommitChanges();
            }
        }

        public bool IsApplication {
            get {
                try {
                    return ((string) DirectoryEntry.Properties["AppRoot"].Value).ToLower() != IISHelper.NAME_DEFAULT_APP_ROOT.ToLower();
                }
                catch (Exception ex) {
                    Trace.WriteLine(ex.ToString(), "IsApplication");
                    return false;
                }
            }
        }

        private WebSiteInfo _ws = null;

        public WebSiteInfo WebSite {
            get { return _ws; }
        }

        public VirtualDirectoryInfo() {
        }

        public VirtualDirectoryInfo(DirectoryEntry createFrom, VirtualDirectoryInfo parentDir, WebSiteInfo site) {
            _parent = parentDir;
            _name = createFrom.Name;
            DirectoryEntry = createFrom;
            _ws = site;
            if (((string) createFrom.Properties["KeyType"].Value) == IISHelper.SCHEMA_VIRTUAL_DIR)
                _path = (string) createFrom.Properties[PropPath].Value;
        }

        public override string ToString() {
            return Name;
        }
    }

    public class VirtualDirectoryInfoCollection : ArrayList {
        public override void Sort(IComparer comparer) {
            base.Sort(comparer);
            foreach (VirtualDirectoryInfo vdir in this) {
                if (vdir.Children.Count > 0)
                    vdir.Children.Sort(comparer);
            }
        }
    }

    #endregion

    public class IISHelper {
        internal class VirtualDirectoryInfoComparer : IComparer {
            #region IComparer Members

            public int Compare(object x, object y) {
                VirtualDirectoryInfo vx = (VirtualDirectoryInfo) x;
                VirtualDirectoryInfo vy = (VirtualDirectoryInfo) y;
                return vx.Name.CompareTo(vy.Name);
            }

            #endregion
        }

        private const string IisPathFormat = "IIS://{0}/W3SVC";
        internal const string
            SCHEMA_WEBSERVER = "IIsWebServer",
            SCHEMA_VIRTUAL_DIR = "IIsWebVirtualDir",
            SCHEMA_WEB_DIR = "IIsWebDirectory",
            NAME_ROOT_DIR = "ROOT",
            NAME_DEFAULT_APP_ROOT = "/LM/W3SVC/1/ROOT";
        private static Exception _lastEx = null;

        public static Exception LastException {
            get { return _lastEx; }
        }

        #region ��������� ������ ���-������

        public static WebSiteInfoCollection GetWebSiteList(string serverName) {
            WebSiteInfoCollection result = new WebSiteInfoCollection();
            
            string iisPath = string.Format(IisPathFormat, serverName);
            DirectoryEntry root = new DirectoryEntry(iisPath);
            IEnumerator en = root.Children.GetEnumerator();
            while (en.MoveNext()) {
                DirectoryEntry current = (DirectoryEntry) en.Current;
                if (current.SchemaClassName == SCHEMA_WEBSERVER) {
                    result.Add(new WebSiteInfo(current));
                }
            }
            return result;
        }

        #endregion

        public static VirtualDirectoryInfo GetVDir(WebSiteInfo webSite, string vdirName) {
            VirtualDirectoryInfo vdir = new VirtualDirectoryInfo();
            IEnumerator en = webSite.DirectoryEntry.Children.GetEnumerator();
            while (en.MoveNext()) {
                DirectoryEntry current = (DirectoryEntry) en.Current;
                if (current.SchemaClassName == SCHEMA_VIRTUAL_DIR || current.SchemaClassName == SCHEMA_WEB_DIR) {
                    if (current.Name == vdirName)
                        return new VirtualDirectoryInfo(current, null, webSite);
                    else {
                        vdir = processToFindVDir(current, vdirName, webSite);
                        if (vdir != null)
                            return vdir;
                    }
                }
            }
            return null;
        }

        private static VirtualDirectoryInfo processToFindVDir(DirectoryEntry parent, string vdirName, WebSiteInfo site) {
            IEnumerator en = parent.Children.GetEnumerator();
            while (en.MoveNext()) {
                DirectoryEntry current = (DirectoryEntry) en.Current;
                if (current.SchemaClassName == SCHEMA_VIRTUAL_DIR || current.SchemaClassName == SCHEMA_WEB_DIR) {
                    PropertyValueCollection val = current.Properties["KeyType"];
                    if (val != null && ((string) val.Value == SCHEMA_WEB_DIR ||
                        current.SchemaClassName == SCHEMA_VIRTUAL_DIR)) {
                        if (current.Name.ToUpper() == vdirName.ToUpper())
                            return new VirtualDirectoryInfo(current, null, site);
                    }
                }
                VirtualDirectoryInfo ret = processToFindVDir(current, vdirName, site);
                if (ret != null)
                    return ret;
            }
            return null;
        }

        public static VirtualDirectoryInfo GetRootVDir(WebSiteInfo webSite) {
            VirtualDirectoryInfo root = new VirtualDirectoryInfo();
            IEnumerator en = webSite.DirectoryEntry.Children.GetEnumerator();
            while (en.MoveNext()) {
                DirectoryEntry current = (DirectoryEntry)en.Current;
                if (current.SchemaClassName == SCHEMA_VIRTUAL_DIR || current.SchemaClassName == SCHEMA_WEB_DIR) {
                    if (current.Name.ToUpper() == NAME_ROOT_DIR)
                        root = new VirtualDirectoryInfo(current, null, webSite);
                    root.Children.AddRange(processVDirChildren(current, root, webSite));
                    root.Children.Sort(new VirtualDirectoryInfoComparer());
                }
            }
            return root;
        }

        private static VirtualDirectoryInfoCollection processVDirChildren(DirectoryEntry parent, VirtualDirectoryInfo parentVDir, WebSiteInfo site) {
            IEnumerator en = parent.Children.GetEnumerator();
            VirtualDirectoryInfoCollection result = new VirtualDirectoryInfoCollection();
            while (en.MoveNext()) {
                DirectoryEntry current = (DirectoryEntry)en.Current;
                if (current.SchemaClassName == SCHEMA_VIRTUAL_DIR || current.SchemaClassName == SCHEMA_WEB_DIR) {
                    PropertyValueCollection val = current.Properties["KeyType"];
                    if (val != null && ((string)val.Value == SCHEMA_WEB_DIR ||
                        current.SchemaClassName == SCHEMA_VIRTUAL_DIR)) {
                        VirtualDirectoryInfo vdir = new VirtualDirectoryInfo(current, parentVDir, site);
                        vdir.Children.AddRange(processVDirChildren(current, vdir, site));
                        result.Add(vdir);
                    }
                }
            }
            return result;
        }
    }
}