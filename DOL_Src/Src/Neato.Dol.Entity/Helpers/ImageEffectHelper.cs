using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Neato.Dol.Entity.Helpers {
    public class ImageEffectHelper {
        public ImageEffectHelper() {}

        public class ConvMatrix {
            public int TopLeft = 0, TopMid = 0, TopRight = 0;
            public int MidLeft = 0, Pixel = 1, MidRight = 0;
            public int BottomLeft = 0, BottomMid = 0, BottomRight = 0;
            public int Factor = 1;
            public int Offset = 0;

            public void SetAll(int nVal) {
                TopLeft = TopMid = TopRight = MidLeft = Pixel = MidRight = BottomLeft = BottomMid = BottomRight = nVal;
            }
        }


        public static byte[] ProcessImage(byte[] image, ImageEffectProperties props) {
            try {
                Image img = ImageHelper.GetImage(image);
                if (img.RawFormat.Equals(ImageFormat.Gif)) {
                    img = ImageHelper.GetImage(ImageHelper.ConvertImageToPng(image));
                }

                ImageFormat format = ImageFormat.Png;
                if (img.RawFormat.Equals(ImageFormat.Bmp) || img.RawFormat.Equals(ImageFormat.Jpeg)) {
                    format = ImageFormat.Jpeg;
                }

                Bitmap bmp = new Bitmap(img);
                if (props.Contrast != 0 && props.Brightness != 0) {
                    ApplyEffect(bmp, CBMatrix(props.Brightness/10f, (props.Contrast + 10.0001f)*1f/10f));
                }
                else if (props.Contrast != 0)
                    ApplyEffect(bmp, ContrastMatrix((props.Contrast + 10.0001f)*1f/10f));
                else if (props.Brightness != 0)
                    ApplyEffect(bmp, BrightnessMatrix(props.Brightness/10f));

                if (props.BlurSharp < 0)
                    GaussianBlur(bmp, (int) Math.Round((Decimal) Math.Abs(props.BlurSharp/2)));
                else if (props.BlurSharp > 0)
                    Sharpen(bmp, 19 - props.BlurSharp); //Magic number 19: to get good picture if |props.BlurSharp|<=10 && !=0;

                switch (props.Effect) {
                    case ImageEffect.Sepia:
                        ApplyEffect(bmp, SepiaMatrix);
                        break;
                    case ImageEffect.Grey:
                        ApplyEffect(bmp, GrayscaleMatrix);
                        break;
                    default:
                        break;
                }

                return ImageHelper.ImageToByteArray(bmp, format);
            }
            catch (Exception ex) {
                return image;
            }

        }

        public static bool Invert(Bitmap b) {
            // GDI+ still lies to us - the return format is BGR, NOT RGB.
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            IntPtr Scan0 = bmData.Scan0;

            unsafe {
                byte* p = (byte*) (void*) Scan0;

                int nOffset = stride - b.Width*3;
                int nWidth = b.Width*3;

                for (int y = 0; y < b.Height; ++y) {
                    for (int x = 0; x < nWidth; ++x) {
                        p[0] = (byte) (255 - p[0]);
                        ++p;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return true;
        }

        public static bool ApplyEffect(Bitmap b, ColorMatrix matrix) {
            ImageAttributes imageAttributes = new ImageAttributes();
            imageAttributes.SetColorMatrix(matrix);

            Graphics gr = Graphics.FromImage(b);
            Rectangle rect = new Rectangle(0, 0, b.Width, b.Height);
            gr.DrawImage(b, rect, 0, 0, b.Width, b.Height, GraphicsUnit.Pixel, imageAttributes);

            return true;
        }

        public static bool GrayScale(Bitmap b) {
            ImageAttributes imageAttributes = new ImageAttributes();
            imageAttributes.SetColorMatrix(GrayscaleMatrix);

            Graphics gr = Graphics.FromImage(b);
            Rectangle rect = new Rectangle(0, 0, b.Width, b.Height);
            gr.DrawImage(b, rect, 0, 0, b.Width, b.Height, GraphicsUnit.Pixel, imageAttributes);

            return true;
        }

        public static bool Sepia(Bitmap b) {
            ImageAttributes imageAttributes = new ImageAttributes();
            imageAttributes.SetColorMatrix(SepiaMatrix);

            Graphics gr = Graphics.FromImage(b);
            Rectangle rect = new Rectangle(0, 0, b.Width, b.Height);
            gr.DrawImage(b, rect, 0, 0, b.Width, b.Height, GraphicsUnit.Pixel, imageAttributes);

            return true;
        }

        public static bool Brightness(Bitmap b, int nBrightness) {
            if (nBrightness < -255 || nBrightness > 255)
                return false;

            // GDI+ still lies to us - the return format is BGR, NOT RGB.
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            IntPtr Scan0 = bmData.Scan0;

            int nVal = 0;

            unsafe {
                byte* p = (byte*) (void*) Scan0;

                int nOffset = stride - b.Width*3;
                int nWidth = b.Width*3;

                for (int y = 0; y < b.Height; ++y) {
                    for (int x = 0; x < nWidth; ++x) {
                        nVal = (int) (p[0] + nBrightness);

                        if (nVal < 0) nVal = 0;
                        if (nVal > 255) nVal = 255;

                        p[0] = (byte) nVal;

                        ++p;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return true;
        }

        public static bool Contrast(Bitmap b, sbyte nContrast) {
            if (nContrast < -100) return false;
            if (nContrast > 100) return false;

            double pixel = 0, contrast = (100.0 + nContrast)/100.0;

            contrast *= contrast;

            int red, green, blue;

            // GDI+ still lies to us - the return format is BGR, NOT RGB.
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            IntPtr Scan0 = bmData.Scan0;

            unsafe {
                byte* p = (byte*) (void*) Scan0;

                int nOffset = stride - b.Width*3;

                for (int y = 0; y < b.Height; ++y) {
                    for (int x = 0; x < b.Width; ++x) {
                        blue = p[0];
                        green = p[1];
                        red = p[2];

                        pixel = red/255.0;
                        pixel -= 0.5;
                        pixel *= contrast;
                        pixel += 0.5;
                        pixel *= 255;
                        if (pixel < 0) pixel = 0;
                        if (pixel > 255) pixel = 255;
                        p[2] = (byte) pixel;

                        pixel = green/255.0;
                        pixel -= 0.5;
                        pixel *= contrast;
                        pixel += 0.5;
                        pixel *= 255;
                        if (pixel < 0) pixel = 0;
                        if (pixel > 255) pixel = 255;
                        p[1] = (byte) pixel;

                        pixel = blue/255.0;
                        pixel -= 0.5;
                        pixel *= contrast;
                        pixel += 0.5;
                        pixel *= 255;
                        if (pixel < 0) pixel = 0;
                        if (pixel > 255) pixel = 255;
                        p[0] = (byte) pixel;

                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return true;
        }

        public static bool Gamma(Bitmap b, double red, double green, double blue) {
            if (red < .2 || red > 5) return false;
            if (green < .2 || green > 5) return false;
            if (blue < .2 || blue > 5) return false;

            byte[] redGamma = new byte[256];
            byte[] greenGamma = new byte[256];
            byte[] blueGamma = new byte[256];

            for (int i = 0; i < 256; ++i) {
                redGamma[i] = (byte) Math.Min(255, (int) ((255.0*Math.Pow(i/255.0, 1.0/red)) + 0.5));
                greenGamma[i] = (byte) Math.Min(255, (int) ((255.0*Math.Pow(i/255.0, 1.0/green)) + 0.5));
                blueGamma[i] = (byte) Math.Min(255, (int) ((255.0*Math.Pow(i/255.0, 1.0/blue)) + 0.5));
            }

            // GDI+ still lies to us - the return format is BGR, NOT RGB.
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            IntPtr Scan0 = bmData.Scan0;

            unsafe {
                byte* p = (byte*) (void*) Scan0;

                int nOffset = stride - b.Width*3;

                for (int y = 0; y < b.Height; ++y) {
                    for (int x = 0; x < b.Width; ++x) {
                        p[2] = redGamma[p[2]];
                        p[1] = greenGamma[p[1]];
                        p[0] = blueGamma[p[0]];

                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return true;
        }

        public static bool Color(Bitmap b, int red, int green, int blue) {
            if (red < -255 || red > 255) return false;
            if (green < -255 || green > 255) return false;
            if (blue < -255 || blue > 255) return false;

            // GDI+ still lies to us - the return format is BGR, NOT RGB.
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;
            IntPtr Scan0 = bmData.Scan0;

            unsafe {
                byte* p = (byte*) (void*) Scan0;

                int nOffset = stride - b.Width*3;
                int nPixel;

                for (int y = 0; y < b.Height; ++y) {
                    for (int x = 0; x < b.Width; ++x) {
                        nPixel = p[2] + red;
                        nPixel = Math.Max(nPixel, 0);
                        p[2] = (byte) Math.Min(255, nPixel);

                        nPixel = p[1] + green;
                        nPixel = Math.Max(nPixel, 0);
                        p[1] = (byte) Math.Min(255, nPixel);

                        nPixel = p[0] + blue;
                        nPixel = Math.Max(nPixel, 0);
                        p[0] = (byte) Math.Min(255, nPixel);

                        p += 3;
                    }
                    p += nOffset;
                }
            }

            b.UnlockBits(bmData);

            return true;
        }

        public static bool Conv3x3(Bitmap b, ConvMatrix m) {
            // Avoid divide by zero errors
            if (0 == m.Factor) return false;

            Bitmap bSrc = (Bitmap) b.Clone();

            // GDI+ still lies to us - the return format is BGR, NOT RGB.
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            BitmapData bmSrc = bSrc.LockBits(new Rectangle(0, 0, bSrc.Width, bSrc.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);

            int stride = bmData.Stride;
            int stride2 = stride*2;
            IntPtr Scan0 = bmData.Scan0;
            IntPtr SrcScan0 = bmSrc.Scan0;

            unsafe {
                byte* p = (byte*) (void*) Scan0;
                byte* pSrc = (byte*) (void*) SrcScan0;

                int nOffset = stride + 6 - b.Width*4;
                int nWidth = b.Width - 2;
                int nHeight = b.Height - 2;

                int nPixel;

                for (int y = 0; y < nHeight; ++y) {
                    for (int x = 0; x < nWidth; ++x) {
                        nPixel = ((((pSrc[2]*m.TopLeft) + (pSrc[6]*m.TopMid) + (pSrc[10]*m.TopRight) +
                            (pSrc[2 + stride]*m.MidLeft) + (pSrc[6 + stride]*m.Pixel) + (pSrc[10 + stride]*m.MidRight) +
                            (pSrc[2 + stride2]*m.BottomLeft) + (pSrc[6 + stride2]*m.BottomMid) + (pSrc[10 + stride2]*m.BottomRight))/m.Factor) + m.Offset);

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[6 + stride] = (byte) nPixel;

                        nPixel = ((((pSrc[1]*m.TopLeft) + (pSrc[5]*m.TopMid) + (pSrc[9]*m.TopRight) +
                            (pSrc[1 + stride]*m.MidLeft) + (pSrc[5 + stride]*m.Pixel) + (pSrc[9 + stride]*m.MidRight) +
                            (pSrc[1 + stride2]*m.BottomLeft) + (pSrc[5 + stride2]*m.BottomMid) + (pSrc[9 + stride2]*m.BottomRight))/m.Factor) + m.Offset);

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[5 + stride] = (byte) nPixel;

                        nPixel = ((((pSrc[0]*m.TopLeft) + (pSrc[4]*m.TopMid) + (pSrc[8]*m.TopRight) +
                            (pSrc[0 + stride]*m.MidLeft) + (pSrc[4 + stride]*m.Pixel) + (pSrc[8 + stride]*m.MidRight) +
                            (pSrc[0 + stride2]*m.BottomLeft) + (pSrc[4 + stride2]*m.BottomMid) + (pSrc[8 + stride2]*m.BottomRight))/m.Factor) + m.Offset);

                        if (nPixel < 0) nPixel = 0;
                        if (nPixel > 255) nPixel = 255;

                        p[4 + stride] = (byte) nPixel;

                        p += 4;
                        pSrc += 4;
                    }

                    p += nOffset;
                    pSrc += nOffset;
                }
            }

            b.UnlockBits(bmData);
            bSrc.UnlockBits(bmSrc);

            return true;
        }

        public static bool Smooth(Bitmap b, int nWeight /* default to 1 */) {
            ConvMatrix m = new ConvMatrix();
            m.SetAll(1);
            m.Pixel = nWeight;
            m.Factor = nWeight + 8;

            return Conv3x3(b, m);
        }

        public static bool GaussianBlur(Bitmap b, int nWeight /* default to 4*/) {
            ConvMatrix m = new ConvMatrix();
            m.SetAll(1);
            m.Pixel = nWeight;
            m.TopMid = m.MidLeft = m.MidRight = m.BottomMid = 2;
            m.Factor = nWeight + 12;

            return Conv3x3(b, m);
        }

        public static bool MeanRemoval(Bitmap b, int nWeight /* default to 9*/) {
            ConvMatrix m = new ConvMatrix();
            m.SetAll(-1);
            m.Pixel = nWeight;
            m.Factor = nWeight - 8;

            return Conv3x3(b, m);
        }

        public static bool Sharpen(Bitmap b, int nWeight /* default to 11*/) {
            ConvMatrix m = new ConvMatrix();
            m.SetAll(0);
            m.Pixel = nWeight;
            m.TopMid = m.MidLeft = m.MidRight = m.BottomMid = -2;
            m.Factor = nWeight - 8;

            return Conv3x3(b, m);
        }

        public static bool EmbossLaplacian(Bitmap b) {
            ConvMatrix m = new ConvMatrix();
            m.SetAll(-1);
            m.TopMid = m.MidLeft = m.MidRight = m.BottomMid = 0;
            m.Pixel = 4;
            m.Offset = 127;

            return Conv3x3(b, m);
        }

        public static bool EdgeDetectQuick(Bitmap b) {
            ConvMatrix m = new ConvMatrix();
            m.TopLeft = m.TopMid = m.TopRight = -1;
            m.MidLeft = m.Pixel = m.MidRight = 0;
            m.BottomLeft = m.BottomMid = m.BottomRight = 1;

            m.Offset = 127;

            return Conv3x3(b, m);
        }

        public static ColorMatrix SepiaMatrix {
            get {
                return new ColorMatrix(new float[][] {
                    new float[] {0.393f, 0.349f, 0.272f, 0, 0},
                    new float[] {0.769f, 0.686f, 0.534f, 0, 0},
                    new float[] {0.189f, 0.168f, 0.131f, 0, 0},
                    new float[] {0, 0, 0, 1, 0},
                    new float[] {0, 0, 0, 0, 1}
                });
            }
        }

        public static ColorMatrix GrayscaleMatrix {
            get {
                return new ColorMatrix(new float[][] {
                    new float[] {0.3086f, 0.3086f, 0.3086f, 0f, 0f},
                    new float[] {0.6094f, 0.6094f, 0.6094f, 0f, 0f},
                    new float[] {0.0820f, 0.0820f, 0.0820f, 0f, 0f},
                    new float[] {0f, 0f, 0f, 1f, 0f},
                    new float[] {0f, 0f, 0f, 0f, 1f}
                });
            }
        }

        public static ColorMatrix NormalMatrix {
            get { return new ColorMatrix(); }
        }

        public static ColorMatrix BrightnessMatrix(float brightness) {
            if (brightness < -1 || brightness > 1)
                throw new ArgumentOutOfRangeException("Brightness value is out of range");

            return new ColorMatrix(new float[][] {
                new float[] {1f, 0f, 0f, 0f, 0f},
                new float[] {0f, 1f, 0f, 0f, 0f},
                new float[] {0f, 0f, 1f, 0f, 0f},
                new float[] {0f, 0f, 0f, 1f, 0f},
                new float[] {brightness, brightness, brightness, 0f, 1f}
            });
        }

        public static ColorMatrix ContrastMatrix(float contrast) {
            if (contrast < 0.0f || contrast > 2.0001f)
                throw new ArgumentOutOfRangeException("Contrast value is out of range");

            float trans = (1f - contrast)/10f;

            return new ColorMatrix(new float[][] {
                new float[] {contrast, 0f, 0f, 0f, 0f},
                new float[] {0f, contrast, 0f, 0f, 0f},
                new float[] {0f, 0f, contrast, 0f, 0f},
                new float[] {0f, 0f, 0f, 1f, 0f},
                new float[] {0.0001f, 0.0001f, 0.0001f, 0f, 1f}
            });
        }

        public static ColorMatrix CBMatrix(float brightness, float contrast) {
            return new ColorMatrix(new float[][] {
                new float[] {contrast, 0f, 0f, 0f, 0f},
                new float[] {0f, contrast, 0f, 0f, 0f},
                new float[] {0f, 0f, contrast, 0f, 0f},
                new float[] {0f, 0f, 0f, 1f, 0f},
                new float[] {brightness, brightness, brightness, 0f, 1f}
            });
        }
    }
}