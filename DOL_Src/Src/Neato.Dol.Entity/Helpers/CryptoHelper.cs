using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Neato.Dol.Entity.Helpers {
    public sealed class CryptoHelper {
        private CryptoHelper() {}

        private static readonly byte[] criptoKey = {220, 214, 181, 89, 125, 194, 128, 103};
        private static readonly byte[] criptoIV = {175, 23, 187, 118, 86, 226, 149, 246}; 
        

        public static MemoryStream Encrypt(Stream data) {
            DES des = new DESCryptoServiceProvider();
            return Cryptographer(data, des.CreateEncryptor(criptoKey, criptoIV));
        }

        public static MemoryStream Decrypt(Stream data) {
            DES des = new DESCryptoServiceProvider();
            return Cryptographer(data, des.CreateDecryptor(criptoKey, criptoIV));
        }

        public static string Encrypt(string data) {
            MemoryStream encryptStream = Encrypt(new MemoryStream(Encoding.UTF8.GetBytes(data)));
            return Convert.ToBase64String(encryptStream.ToArray());
        }

        public static string Decrypt(string data) {
            MemoryStream decryptStream = Decrypt(new MemoryStream(Convert.FromBase64String(data)));
            return new StreamReader(decryptStream).ReadToEnd();
        }

        private static MemoryStream Cryptographer(Stream data, ICryptoTransform transform) {
            MemoryStream result = new MemoryStream();
            data.Seek(0, SeekOrigin.Begin);
            byte[] bin = new byte[100]; //This is intermediate storage for the encryption.
            long rdlen = 0; //This is the total number of bytes written.
            long totlen = data.Length; //This is the total length of the input file.
            int len; //This is the number of bytes to be written at a time.

            CryptoStream encStream = new CryptoStream(result, transform, CryptoStreamMode.Write);
            while (rdlen < totlen) {
                len = data.Read(bin, 0, 100);
                encStream.Write(bin, 0, len);
                rdlen = rdlen + len;
            }
            encStream.Flush();
            encStream.FlushFinalBlock();
            result.Seek(0, SeekOrigin.Begin);
            return result;
        }
    }
}