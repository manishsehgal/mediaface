using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;

namespace Neato.Dol.Entity.Helpers {
    public class ImageHelper {
        private ImageHelper() {}

        private static ImageCodecInfo jpegCodecInfo;

        static ImageHelper() {
            ImageCodecInfo[] encoders = ImageCodecInfo.GetImageEncoders();
            for(int j = 0; j < encoders.Length; ++j) {
                if(encoders[j].MimeType == "image/jpeg") {
                    jpegCodecInfo = encoders[j];
                    break;
                }
            }
        }

        public static byte[] ConvertImageToJpeg(byte[] imageArray) {
            Image image = null;
            try {
                image = GetImage(imageArray);
                return ConvertImageToJpeg(image);
            } catch {
                return new byte[0];
            } finally {
                if (image != null) {
                    image.Dispose();
                }
            }
        }

        public static byte[] ConvertImageToJpeg(Image image) {
            Graphics gr = null;
            try {
                Bitmap bitmap = new Bitmap(image.Width, image.Height);
                bitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);
                gr = Graphics.FromImage(bitmap);
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.CompositingQuality = CompositingQuality.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.Clear(Color.FromArgb(255, 255, 255));
                gr.DrawImage(image, 0, 0);
                return SaveToJpeg(bitmap);
            } catch {
                return new byte[0];
            } finally {
                if (gr != null) {
                    gr.Dispose();
                }
            }
        }


        public static byte[] ConvertImageToPng(byte[] imageArray) {
            Image image = null;
            try {
                image = GetImage(imageArray);
                return ConvertImageToPng(image);
            } catch {
                return new byte[0];
            } finally {
                if (image != null) {
                    image.Dispose();
                }
            }
        }

        public static byte[] ConvertImageToPng(Image image) {
            Graphics gr = null;
            try {
                Bitmap bitmap = new Bitmap(image.Width, image.Height);
                bitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);
                gr = Graphics.FromImage(bitmap);
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.CompositingQuality = CompositingQuality.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.Clear(Color.Transparent);
                gr.DrawImage(image, 0, 0);
                return SaveToPng(bitmap);
            } catch {
                return new byte[0];
            } finally {
                if (gr != null) {
                    gr.Dispose();
                }
            }
        }

        public static byte[] ConvertImageToJpegCond(byte[] imageArray) {
            Image image = null;
            try {
                image = GetImage(imageArray);
                ImageFormat format = image.RawFormat;
                if (format.Equals(ImageFormat.Jpeg)==true || format.Equals(ImageFormat.Png)==true) {
                    return imageArray;
                } else if (format.Equals(ImageFormat.Gif)) {
                    return ConvertImageToPng(imageArray);
                } else {
                    return ConvertImageToJpeg(imageArray);
                }
            } catch {
                return new byte[0];
            } finally {
                if (image != null) {
                    image.Dispose();
                }
            }
        }

        private static byte[] SaveToJpeg(Image image) {
            using (MemoryStream chartStream = new MemoryStream()) {
                Encoder qualityEncoder = Encoder.Quality;
                EncoderParameter ratio = new EncoderParameter(qualityEncoder, 100L);
                EncoderParameters codecParams = new EncoderParameters(1);
                codecParams.Param[0] = ratio;
                image.Save(chartStream, jpegCodecInfo, codecParams);
                //image.Save(chartStream, ImageFormat.Jpeg);
                return ConvertHelper.StreamToByteArray(chartStream);
            }
        }

        public static byte[] SaveToPng(Image image) {
            using (MemoryStream chartStream = new MemoryStream()) {
                image.Save(chartStream, ImageFormat.Png);
                return ConvertHelper.StreamToByteArray(chartStream);
            }
        }

        /*
        public static void SaveToPngFile(Image image) {
            FileStream fileStream = new FileStream("E:\\tmp\\CPT\\asp\\test2.png", FileMode.Create);
            image.Save(fileStream, ImageFormat.Png);
            fileStream.Close();
        }
        */

        public static Image GetImage(byte[] imageArray) {
            using (MemoryStream chartStream = new MemoryStream(imageArray)) {
                Image image = Image.FromStream(chartStream);
                return image;
            }
        }

        public static byte[] ImageToByteArray(Image image, ImageFormat format) {
            if (format.Equals(ImageFormat.Png) || format.Equals(ImageFormat.Gif)) {
                return ConvertImageToPng(image);
            }
            else {
                return ConvertImageToJpeg(image);
            }
        }

        private static bool ThumbnailCallback() {
            return false;
        }

        public static byte[] GetIcon(byte[] imageArray, int iconWidth, int iconHeight) {
            Image.GetThumbnailImageAbort myCallback = new Image.GetThumbnailImageAbort(ThumbnailCallback);

            Image image = GetImage(imageArray);
            try {
                double scale = Math.Min((double)iconWidth/image.Width, (double)iconHeight/image.Height);
                iconWidth  = (int)Math.Round(scale * image.Width);
                iconHeight = (int)Math.Round(scale * image.Height);
                Image icon = new Bitmap(image).GetThumbnailImage(iconWidth, iconHeight, myCallback, IntPtr.Zero);
                return SaveToJpeg(icon);
            } catch {
                Debug.Assert(false, "GetIcon failed!");
                return null;
            }
        }

        public static byte[] TruncateImageTo2880Cond(byte[] imageArray) {
            Image image = null;
            int maxSize = 2880;
            try {
                image = GetImage(imageArray);
                ImageFormat format = image.RawFormat;
                double scale = Math.Max((double)image.Size.Width/maxSize, (double)image.Size.Height/maxSize);

                if (scale > 1) {
                    int newWidth  = (int)Math.Round(image.Width / scale);
                    int newHeight = (int)Math.Round(image.Height / scale);
                    Bitmap bitmap = new Bitmap(image, new Size(newWidth, newHeight));
                    bitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);
                    return ImageToByteArray(bitmap, format);
                }

                return imageArray;
            } catch (Exception ex) {
                return new byte[0];
            } finally {
                if (image != null) {
                    image.Dispose();
                }
            }        
        }

    }
}