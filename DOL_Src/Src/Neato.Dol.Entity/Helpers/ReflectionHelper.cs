using System.Reflection;

namespace Neato.Dol.Entity.Helpers {
    public sealed class ReflectionHelper {
        private ReflectionHelper() {}

        public static object GetFieldOrPropertyValue(object obj, string memberName, params object[] index) {
            MemberInfo[] infos = obj.GetType().GetMember(memberName);

            foreach (MemberInfo info in infos) {
                switch (info.MemberType) {
                    case MemberTypes.Property:
                        return ((PropertyInfo)info).GetValue(obj, index);
                    case MemberTypes.Field:
                        return ((FieldInfo)info).GetValue(obj);
                }
            }
            return null;
        }
    }
}