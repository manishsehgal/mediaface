using System;
using System.Runtime.Serialization;

namespace Neato.Dol.Entity.Exceptions {
    [Serializable]
    public abstract class BaseBusinessException : ApplicationException {
        public BaseBusinessException() {}

        public BaseBusinessException(string message) : base(message) {}

        public BaseBusinessException(string message, Exception innerException) : base(message, innerException) {}

        protected BaseBusinessException(SerializationInfo info, StreamingContext context) : base(info, context) {}
    }

}