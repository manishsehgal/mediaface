using System;
using System.Runtime.Serialization;

namespace Neato.Dol.Entity.Exceptions {
    [Serializable]
    public abstract class BaseNonRecoverableException : ApplicationException {
        public BaseNonRecoverableException() {}

        public BaseNonRecoverableException(string message) : base(message) {}

        public BaseNonRecoverableException(string message, Exception innerException) : base(message, innerException) {}

        protected BaseNonRecoverableException(SerializationInfo info, StreamingContext context) : base(info, context) {}
    }
}