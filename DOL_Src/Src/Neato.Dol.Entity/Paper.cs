using System;
using System.Collections;
using System.Drawing;
using System.Xml;

namespace Neato.Dol.Entity {
    [Serializable]
    public class Paper : PaperBase {
        private string nameValue = string.Empty;

        private string skuValue = string.Empty;
        public string Sku {
            get { return skuValue; }
            set { skuValue = value; }
        }

        public string Name {
            get { return nameValue; }
            set { nameValue = value; }
        }

        private SizeF sizeValue = new SizeF(0, 0);

        public SizeF Size {
            get { return sizeValue; }
            set { sizeValue = value; }
        }

        private PaperBrand brandValue = null;

        public PaperBrand Brand {
            get { return brandValue; }
            set { brandValue = value; }
        }

        private PaperType paperTypeValue = PaperType.DieCut;

        public PaperType PaperType {
            get { return paperTypeValue; }
            set { paperTypeValue = value; }
        }

        private string paperStateValue = "t";

        public string PaperState {
            get { return paperStateValue; }
            set { paperStateValue = value; }
        }
        
        private string orientationValue = "P";

        public string Orientation {
            get { return orientationValue; }
            set { orientationValue = value; }
        }

        private int metricIdValue = 0;
        private int orderValue = 1;

        private PaperAccess minPaperAccessValue = PaperAccess.MFOPE;
        public PaperAccess MinPaperAccess {
            get { return minPaperAccessValue; }
            set { minPaperAccessValue = value; }
        }

        public Paper() : base() {}

        public Paper(int id, string name, PaperBrand brand, float width, float height, PaperType paperType, string state, string orientation, int metricId) : base(id) {
            this.nameValue = name;
            this.sizeValue.Width = width;
            this.sizeValue.Height = height;
            this.brandValue = brand;
            this.paperTypeValue = paperType;
            this.paperStateValue = state;
            this.orientationValue = orientation;
            this.metricIdValue = metricId;
        }

        #region Faces
        private ArrayList facesValue = new ArrayList();

        public virtual void AddFace(Face face, float x, float y) {
            face.Position = new PointF(x, y);
            facesValue.Add(face);
        }

        public virtual Face[] Faces {
            get { return (Face[])this.facesValue.ToArray(typeof(Face)); }
        }

        public int MetricId {
            get { return metricIdValue; }
            set { metricIdValue = value; }
        }

        public int Order {
            get { return orderValue; }
            set { orderValue = value; }
        }

        public virtual string[] GetFaceNames(string culture) {
            ArrayList names = new ArrayList(this.facesValue.Count);
            foreach (Face face in this.facesValue) {
                names.Add(face.GetName(culture));
            }
            return (string[])names.ToArray(typeof(string));
        }
        #endregion

        public XmlDocument GetXmlContent(DeviceType deviceType) {
            return new Project(this, deviceType).ProjectXml;
        }
    }
}