using System;

namespace Neato.Dol.Entity
{
	public class Template
	{
        private int idField;
        private int faceIdField;
        private string nameField;
        private string xmlField;

        public const string IdField = "Id";
        public const string FaceIdField = "FaceId";

		public Template(){}
        public Template(int id, string text, string xml) {
            Id = id;
            Name = text;
            XmlValue = xml;
        }
        public Template(int id) {
            Id = id;
        }
        public string XmlValue {
            get { return xmlField;}
            set { xmlField = value;}
        }

        public string Name {
            get { return nameField;}
            set { nameField = value;}
        }

        public int Id {
            get { return idField; }
            set { idField = value; }
        }
        public int FaceId 
        {
            get { return faceIdField; }
            set { faceIdField = value; }
        }
	}
}
