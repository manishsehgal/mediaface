using System;

namespace Neato.Dol.Entity {
    [Serializable]
    public class Retailer {
        public static string IdField = "ID";
        public const string NameField = "Name";

        private int retailerIdValue = 0;
        private string nameValue = string.Empty;
        private string displayNameValue = string.Empty;
        private byte[] iconValue = new byte[0];
        private string retailerUrlValue = string.Empty;
        private bool hasExitPageValue = false;

        public Retailer() {}

        public Retailer(int id, string name, string displayName, byte[] icon, string url, bool hasExitPage) {
            retailerIdValue = id;
            nameValue = name;
            displayNameValue = displayName;
            iconValue = icon;
            retailerUrlValue = url;
            hasExitPageValue = hasExitPage;
        }

        public Retailer(int id) {
            this.retailerIdValue = id;
        }

        public Retailer(string name) {
            this.nameValue = name;
        }

        public int Id {
            set { retailerIdValue = value; }
            get { return retailerIdValue; }
        }

        public string Name {
            set { nameValue = value; }
            get { return nameValue; }
        }

        public string DisplayName {
            set { displayNameValue = value != null ? value : string.Empty; }
            get { return displayNameValue; }
        }

        public string Url {
            set { retailerUrlValue = value; }
            get { return retailerUrlValue; }
        }

        public byte[] Icon {
            get { return iconValue; }
            set { iconValue = value; }
        }

        public bool HasExitPage {
            get { return hasExitPageValue; }
            set { hasExitPageValue = value; }
        }

        #region Equals, GetHashCode, ==, !=
        public override bool Equals(object obj) {
            Retailer other = obj as Retailer;
            if (other == null) {
                return false;
            } else {
                return this.retailerIdValue == other.retailerIdValue;
            }
        }

        public override int GetHashCode() {
            return retailerIdValue.GetHashCode();
        }

        public static bool operator ==(Retailer first, Retailer second) {
            if (object.Equals(first, null)) {
                return object.Equals(second, null);
            }
            return first.Equals(second);
        }

        public static bool operator !=(Retailer first, Retailer second) {
            if (object.Equals(first, null)) {
                return !object.Equals(second, null);
            }
            return !first.Equals(second);
        }
        #endregion
    }
}