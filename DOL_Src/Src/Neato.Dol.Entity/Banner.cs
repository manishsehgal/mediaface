using System;

namespace Neato.Dol.Entity {
    [Serializable]
    public class Banner {
        private int idValue = 0;
        private bool isVisibleValue = false;
        private string srcValue = string.Empty;
        private bool isHiddenValue = false;

        public int Id {
            set { idValue = value; }
            get { return idValue; }
        }

        public bool IsVisible {
            set { isVisibleValue = value; }
            get { return isVisibleValue; }
        }

        public string Src {
            get { return srcValue; }
            set { srcValue = value; }
        }

        public bool IsHidden {
            get { return isHiddenValue; }
            set { isHiddenValue = value; }
        
        }

        public Banner() {}

        public Banner(int id, bool isVisible, string src, bool isHidden) {
            idValue = id;
            isVisibleValue = isVisible;
            srcValue = src;
            isHiddenValue = isHidden;
        }
    }
}