namespace Neato.Dol.Entity.Tracking {
    public enum UserAction {
        None = 0,
        Registration,
        Login,
        Logout,
        Designer
    }
}