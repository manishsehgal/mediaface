using System;
using System.Xml;

namespace Neato.Dol.Entity {
    [Serializable]
    public class Customer {
        public const string FieldId = "CustomerId";

        private int customerIdValue = 0;
        private bool activeValue = true;
        private string firstNameValue = string.Empty;
        private string lastNameValue = string.Empty;
        private string emailValue = string.Empty;
        private string passwordValue = string.Empty;
        private string passwordUidValue = string.Empty;
        private EmailOptions emailOptionsValue = EmailOptions.Text;
        private bool receiveInfoValue = false;
        private PaperBase[] lastEditedPapersValue;
        private DateTime createdValue;
        private int retailerIdValue;
        private CustomerGroup customerGroupValue = CustomerGroup.MFO;

        public int CustomerId {
            get { return customerIdValue; }
            set { customerIdValue = value; }
        }

        public bool Active {
            get { return activeValue; }
            set { activeValue = value; }
        }

        public string FirstName {
            get { return firstNameValue; }
            set { firstNameValue = value; }
        }

        public string LastName {
            get { return lastNameValue; }
            set { lastNameValue = value; }
        }

        public string FullName {
            get { return string.Format("{0} {1}", firstNameValue, lastNameValue); }
        }

        public string Email {
            get { return emailValue; }
            set { emailValue = value; }
        }

        public string Password {
            get { return passwordValue; }
            set { passwordValue = value; }
        }

        public string PasswordUid {
            get { return passwordUidValue; }
            set { passwordUidValue = Convert.ToString(value); }
        }

        public EmailOptions EmailOptions {
            get { return emailOptionsValue; }
            set { emailOptionsValue = value; }
        }

        public bool ReceiveInfo {
            get { return receiveInfoValue; }
            set { receiveInfoValue = value; }
        }

        public PaperBase LastEditedPaper {
            get {
                return (LastEditedPapers != null && LastEditedPapers.Length > 0)
                    ? LastEditedPapers[0] : null;
            }
        }

        public PaperBase[] LastEditedPapers {
            get { return lastEditedPapersValue; }
            set { lastEditedPapersValue = value; }
        }

        public DateTime Created {
            get { return createdValue; }
            set { createdValue = value; }
        }

        public bool IsNew {
            get { return customerIdValue == 0; }
        }

        public int RetailerId {
            get { return retailerIdValue; }
            set { retailerIdValue = value; }
        }
        
        public CustomerGroup Group {
            get { return customerGroupValue; }
            set { customerGroupValue = value; }
        }

        public Customer(){}

        public Customer(int customerId, string firstName, string lastName, string email, string password,
                        EmailOptions emailOptions, bool receiveInfo, int retailerId, CustomerGroup customerGroup) {
            this.customerIdValue = customerId;
            this.firstNameValue = firstName;
            this.lastNameValue = lastName;
            this.emailValue = email;
            this.passwordValue = password;
            this.emailOptionsValue = emailOptions;
            this.receiveInfoValue = receiveInfo;
            this.RetailerId = retailerId;
            this.customerGroupValue = customerGroup;
        }

        #region Equals, GetHashCode, ==, !=
        public override bool Equals(object obj) {
            Customer other = obj as Customer;
            if (other == null) {
                return false;
            } else {
                return this.customerIdValue == other.customerIdValue;
            }
        }

        public override int GetHashCode() {
            return customerIdValue.GetHashCode();
        }

        public static bool operator ==(Customer first, Customer second) {
            if (object.Equals(first, null)) {
                return object.Equals(second, null);
            }
            return first.Equals(second);
        }

        public static bool operator !=(Customer first, Customer second) {
            if (object.Equals(first, null)) {
                return !object.Equals(second, null);
            }
            return !first.Equals(second);
        }
        #endregion

        public static XmlDocument GetXmlForBuyAccountWithSerialNumber(
            string customerEmail, string customerSerialNumber) {
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("Data");

            XmlNode nodeType = doc.CreateElement("Type");
            nodeType.InnerText = "light";
            root.AppendChild(nodeType);

            XmlNode nodeEmail = doc.CreateElement("Email");
            nodeEmail.InnerText = customerEmail;
            root.AppendChild(nodeEmail);

            XmlNode nodeSerial = doc.CreateElement("Serial");
            nodeSerial.InnerText = customerSerialNumber;
            root.AppendChild(nodeSerial);

            doc.AppendChild(root);

            return doc;
        }


        public static XmlDocument GetXmlForBuyAccountWithoutSerialNumber(
            string customerEmail) {
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("Data");

            XmlNode nodeType = doc.CreateElement("Type");
            nodeType.InnerText = "full";
            root.AppendChild(nodeType);

            XmlNode nodeEmail = doc.CreateElement("Email");
            nodeEmail.InnerText = customerEmail;
            root.AppendChild(nodeEmail);

            doc.AppendChild(root);

            return doc;
        }

    }
}