using System;
using System.Runtime.Serialization;

namespace Neato.Dol.Entity
{
	public enum ImageEffect {
	    Normal = 0,
        Grey,
        Sepia
	}
   [Serializable]
	public class ImageEffectProperties
	{
        public int Contrast;
        public int Brightness;
        public ImageEffect Effect;
        public int BlurSharp;
        public bool IsNoEffect() {
            return Contrast == 0 &&
                   Brightness == 0 &&
                    BlurSharp == 0 &&
                    Effect == 0;
        }
       public bool IsEq(ImageEffectProperties c) {
           return c.Effect == this.Effect && 
                  c.Contrast == this.Contrast &&
                  c.Brightness == this.Brightness &&
                  c.BlurSharp == this.BlurSharp;
                    
       }
	}
    [Serializable]
    public class ImageStorage: ISerializable {
        public ImageEffectProperties ImageEffProperties;
        public byte[] image;
        public ImageStorage(ImageEffectProperties props,byte[] image) {
            ImageEffProperties = props;
            this.image = image;
        }
        #region ISerializable
        protected ImageStorage(SerializationInfo info, StreamingContext context) {
            this.ImageEffProperties =(ImageEffectProperties) info.GetValue("effprops",typeof(ImageEffectProperties));
            image =(byte[]) info.GetValue("image",typeof(byte[]));
        }
        void ISerializable.GetObjectData(
            SerializationInfo info, StreamingContext context) 
        {
            info.AddValue("effprops", this.ImageEffProperties);
            info.AddValue("image", this.image);
            
        }
        #endregion
    }
}

