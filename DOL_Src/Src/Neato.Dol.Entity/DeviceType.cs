namespace Neato.Dol.Entity {
    public enum DeviceType {
        Undefined = 0,
        CdDvdLabel,
        Insert,
        MP3Player,
        DirectToCD,
        Lightscribe,
        OtherLabels
    }
}