namespace Neato.Dol.Entity {
    public class PaperMetric {
        private int idValue = 0;
        private string nameValue;

        public const string IdField = "Id";
        public const string NameField = "Name";

        public PaperMetric() {
        }

        public PaperMetric(int id) {
            Id = id;
        }

        public PaperMetric(int id, string name) {
            Id = id;
            Name = name;
        }

        public int Id {
            get { return idValue; }
            set { idValue = value; }
        }

        public string Name {
            get { return nameValue; }
            set { nameValue = value; }
        }

        #region Equals, GetHashCode, ==, !=

        public override bool Equals(object obj) {
            PaperMetric other = obj as PaperMetric;
            if (other == null) {
                return false;
            }
            else {
                return Id == other.Id;
            }
        }

        public override int GetHashCode() {
            return idValue.GetHashCode();
        }

        public static bool operator ==(PaperMetric first, PaperMetric second) {
            if (Equals(first, null)) {
                return Equals(second, null);
            }
            return first.Equals(second);
        }

        public static bool operator !=(PaperMetric first, PaperMetric second) {
            if (Equals(first, null)) {
                return !Equals(second, null);
            }
            return !first.Equals(second);
        }

        #endregion
    }
}