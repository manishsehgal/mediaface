using System;

namespace Neato.Dol.Entity {
    [Serializable]
    public class Upc {
        public const string IdField = "Id";

        private int idValue = 0;
        private string codeValue = string.Empty;

        public int Id {
            get { return idValue; }
            set { idValue = value; }
        }

        public string Code {
            get { return codeValue; }
            set { codeValue = value; }
        }

        public Upc() {}

        public Upc(int id, string code) {
            idValue = id;
            codeValue = code;
        }
    }
}