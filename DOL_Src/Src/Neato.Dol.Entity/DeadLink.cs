using System;

namespace Neato.Dol.Entity {
    public class DeadLink {
        public DeadLink() {}

        public DeadLink(int id) {
            Id = id;
        }

        public DeadLink(string url, string ip, string referrer) {
            Url = url;
            IP = ip;
            Referrer = referrer;
            Time = DateTime.Now;
        }

        public DeadLink(int id, string url, string ip, string referrer, DateTime time) {
            Id = id;
            Url = url;
            IP = ip;
            Time = time;
        }

        private int idValue;
        public const string IdField = "Id";

        public int Id {
            get { return idValue; }
            set { idValue = value; }
        }

        public bool IsNew {
            get { return idValue <= 0; }
        }

        private string urlValue;

        public string Url {
            get { return urlValue; }
            set { urlValue = value; }
        }

        private string ipValue;

        public string IP {
            get { return ipValue; }
            set { ipValue = value; }
        }

        private string referrerValue;

        public string Referrer {
            get { return referrerValue; }
            set { referrerValue = value; }
        }

        private DateTime timeValue;

        public DateTime Time {
            get { return timeValue; }
            set { timeValue = value; }
        }

        #region Equals, GetHashCode, ==, !=

        public override bool Equals(object obj) {
            DeadLink other = obj as DeadLink;
            if (other == null) {
                return false;
            }
            else {
                return this.Id == other.Id;
            }
        }

        public override int GetHashCode() {
            return idValue.GetHashCode();
        }

        public static bool operator ==(DeadLink first, DeadLink second) {
            if (object.Equals(first, null)) {
                return object.Equals(second, null);
            }
            return first.Equals(second);
        }

        public static bool operator !=(DeadLink first, DeadLink second) {
            if (object.Equals(first, null)) {
                return !object.Equals(second, null);
            }
            return !first.Equals(second);
        }

        #endregion
    }
}