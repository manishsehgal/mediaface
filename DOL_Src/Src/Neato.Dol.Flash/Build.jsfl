var swfList = new Array (
    {src:"WorkspaceTab"},
    {src:"PreviewTool"},
    {src:"Preloader"},
    {src:"SetVar"},
    {src:"Starter"},
    {src:"CtrlLib"},
    {src:"MainMenu"},
    {src:"FloatMenu"},
    {src:"PluginsConfig"},
    {src:"Navigator"},
    {src:"LabelSwitcher"},
    {src:"Tools"},
    {src:"FillTool"},
    {src:"TextTool"},
    {src:"AddTextTool"},
    {src:"ImageTool"},
    {src:"AddImageTool"},
    {src:"ChangePaperPanel"},
    {src:"ChangePaperTool"},
    {src:"ShapeTool"},
    {src:"AddShapeTool"},
    {src:"GradientTool"},
    {src:"AddGradientTool"},
    {src:"AddPaintTool"},
    {src:"EditPaintTool"},
    {src:"AddPlaylistTool"},
    {src:"PlaylistTool"},
    {src:"TemplateTool"},
    {src:"Designer"},
    {src:"Debug"}
);

var outDir = "../Neato.Dol.WebDesigner/Flash/";

for(var info in swfList){
    fl.openDocument("file:///" + swfList[info].src + ".fla");
    var doc = fl.documents[0];
    fl.trace(doc.name);
    doc.currentPublishProfile = "Release";
    doc.exportSWF("file:///" + outDir + swfList[info].src + ".swf", true);
    doc.close(false);
}

swfList = new Array (
    {src:"WatermarkEditor"}
);

outDir = "../Neato.Dol.WebAdmin/Flash/";

for(var info in swfList){
    fl.openDocument("file:///" + swfList[info].src + ".fla");
    var doc = fl.documents[0];
    fl.trace(doc.name);
    doc.currentPublishProfile = "Release";
    doc.exportSWF("file:///" + outDir + swfList[info].src + ".swf", true);
    doc.close(false);
}
