﻿
s = new LocalConnection();
if (fq != null) {
	// ----------------------------------------------------------------------------
	// LocalConnection Send for setvariable method
	// ----------------------------------------------------------------------------
	s.send(lc, "setVariables", fq);
} else if (rq != null) {
	// ----------------------------------------------------------------------------
	// LocalConnection Send for remoting method
	// ----------------------------------------------------------------------------
	s.send(lc, "callResult", rq);
}
delete s;

// support for Plugin Service dicovery
if(chkPSPort != null) {
	var portN:Number = Number(chkPSPort);
	var sock:XMLSocket = new XMLSocket();
	sock.onConnect = function(res) {
		sock.send("<Discovery />");
		s = new LocalConnection();
		var chkRes:Object = new Object();
		chkRes.port = portN;
		chkRes.result = res;
		s.send(lc, "chkPSPortResult", chkRes);
		delete s;		
		sock.close();
	}

	System.security.loadPolicyFile("xmlsocket://localhost:"+chkPSPort);
	var res:Boolean = sock.connect("localhost", portN);
	if(!res) {
		sock.onConnect(false);
	}
}
