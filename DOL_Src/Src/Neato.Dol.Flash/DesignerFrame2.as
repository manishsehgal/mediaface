﻿#include "MainLogic.as"

_global.MainWindow = new MainWindow();

_global.Mode;
_global.TextMode="Text", _global.FillMode="Fill",
_global.PlaylistMode="Playlist", _global.PlaylistEditMode="PlaylistEdit",
_global.GradientMode="Gradient", _global.GradientEditMode="GradientEdit",
_global.TransferMode="Transfer", _global.ClearFaceMode="ClearFace",
_global.ImageMode="Image", _global.ImageEditMode="ImageEdit",
_global.ShapeMode="Shape", _global.ShapeEditMode="ShapeEdit", 
_global.PaintMode="Paint", _global.PaintEditMode="PaintEdit", 
_global.ChooseLayoutMode="ChooseLayoutMode",_global.InactiveMode="InactiveMode",
_global.TextEffectMode = "TextEffect",_global.ImageEffectMode = "ImageEffectMode",
_global.ChangePaperMode = "ChangePaper";

_global.ThumbailAutofit = "Thubail";
_global.HorizontalAutofit = "Horizontal";
_global.VerticalAutofit = "Vertical";
_global.FitToFaceAutofit = "Fill";
_global.NoneAutofit = "None";

this.pnlPreviewLayer._visible = false;
this.pnlMainLayer.content.pnlChangePaperLayer._visible = false;
this.pnlMainLayer.content.pnlPreviewDesc._visible = false;
this.pnlMainLayer.content.pnlPluginsConfig._visible = false;

_global.Watermark = Watermark.WatermarkHelper.ParseWatermark(_global.watermarkXml);
_global.Fonts = new FontHelper(_global.fontsXml);
_global.PaperHelper.DrawWaterMark(_global.Fonts.GetFontsMC(), 10, 10);
//_global.Fonts.LoadSmallFont();

_global.Brushes = new PaintHelper(_global.brushesXml);
_global.Shapes = new ShapeHelper(_global.shapesXml);
_global.Gradients = new GradientHelper(_global.gradientsXml);
_global.Effects = new TextEffectHelper(_global.effectsXml);
_global.Tools = new ToolsHelper(_global.toolsXml);

_global.EditLeft   = 0;//_root.pnlMainLayer.content.pnlWorkSpace._x;
_global.EditTop    = 0;//_root.pnlMainLayer.content.pnlWorkSpace._y;
_global.ZoomPanelWidth = 100;
_global.EditWidth  = _root.pnlMainLayer.content.pnlDesignArea._width - _global.ZoomPanelWidth;
_global.EditHeight = _root.pnlMainLayer.content.pnlDesignArea._height;

_global.Project.Load();

SASession.Initialize();

_global.keyListener = new Object();
_global.keyListener.onKeyDown = function() {
	trace("_global.keyListener.onKeyDown");
	trace("_global.Project.CurrentUnit.Text = "+_global.Project.CurrentUnit.Text);
	trace("Key.isDown(Key.DELETEKEY) = "+Key.isDown(Key.DELETEKEY));
	
	var unit = _global.Project.CurrentUnit; 
	if(
	   Key.isDown(Key.DELETEKEY) && 
	   		((unit.Text == "" &&
			 unit.layoutPosition == undefined || 
			 unit.Text == undefined
			 ) ||
			 !(eval(Selection.getFocus()) instanceof TextField))
		)
	{
		_global.UIController.ToolsController.OnDeleteUnit();
		return;
	}
	
	if (unit instanceof TextUnit || unit instanceof TableUnit || unit instanceof TextEffectUnit) {
		if (eval(Selection.getFocus()) instanceof TextField ||
			eval(Selection.getFocus()) instanceof mx.controls.List ||
			eval(Selection.getFocus()) instanceof mx.controls.ComboBox ||			
			eval(Selection.getFocus()) instanceof mx.controls.RadioButton)
			return;
	}

	switch (Key.getCode()) {
		case Key.LEFT :
			_global.SelectionFrame.X -= 1;
			break;
		case Key.UP :
			_global.SelectionFrame.Y -= 1;
			break;
		case Key.RIGHT :
			_global.SelectionFrame.X += 1;
			break;
		case Key.DOWN :
			_global.SelectionFrame.Y += 1;
			break;
	};
};
Key.addListener(_global.keyListener);

var intervalObject:Object = new Object;
intervalObject.intervalId = setInterval(OnPSChecked,1000,intervalObject);

function OnPSChecked(intervalObject:Object) {
	if(SAPlugins.IsDiscoveryRunning())
		return;
	clearInterval(intervalObject.intervalId);		
	if(SAPlugins.IsAvailable())
		return;	
	if(_global.Project.CurrentPaper.IsLightscribeDevice)
		_global.MessageBox.Alert(
			" You should have MediaFACE Online Plugins with LightScribe plugin\n"+
			"and Text Effects plugin installed and running to work with " +
			"LightScribe labels.\n" +
			" Please go to the \"Plugins\" menu to download and install required plugins.", 
			" Warning                                                               ", null);
}	
		
 