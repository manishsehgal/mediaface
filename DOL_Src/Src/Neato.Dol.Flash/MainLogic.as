﻿#include "Utils.as"

BrowserHelper.InvokePageScript("log", "MainLogic.as");

_global.Fonts;
_global.SelectionFrame;
_global.ModelName;
_global.Frames;
_global.DesignMode="Designer";
_global.MessageBox = MsgBox.MessageBox;
_global.MessageBox.init();

_global.ResurrectEmptyLayoutItem = function ():Void {
	trace("ResurrectEmptyLayoutItem");
	trace("_global.CurrentUnit.Text.length"+_global.CurrentUnit.Text.length);
	trace("unit instanceof TextUnit "+(unit instanceof TextUnit));
	
	if ((_global.Project.CurrentUnit instanceof TextUnit ||
		 _global.Project.CurrentUnit instanceof TextEffectUnit) &&
		 _global.Project.CurrentUnit.Text.length == 0 &&
		 _global.Project.CurrentUnit.EmptyLayoutItem == false) {
		
		trace("Unit is instance of TextUnit or TextEffectUnit");
		_global.Project.CurrentUnit.EmptyLayoutItem = true;
		var fmt = _global.Project.CurrentUnit.GetFontFormat(0);
		var len = _global.Project.CurrentUnit.Text.length;
		var defaultText = _global.CurrentUnit.predefText;
		_global.Project.CurrentUnit.ReplaceText(0, 0, defaultText, fmt);
		if(_global.Project.CurrentUnit instanceof TextEffectUnit)
			_global.Project.CurrentUnit.Draw();
			
		trace("Predefined text "+_global.CurrentUnit.predefText);
		
	}
	trace(_global.Project.CurrentUnit.GetAllText().length + " = _global.CurrentUnit.GetAllText() " +_global.CurrentUnit.EmptyLayoutItem);
	if (_global.Project.CurrentUnit instanceof TableUnit &&
		_global.Project.CurrentUnit.GetAllText().length < 1 &&
		_global.Project.CurrentUnit.EmptyLayoutItem == false) {
			trace("In resurect table Unit");
		
		_global.Project.CurrentUnit.EmptyLayoutItem = true;
		_global.Project.CurrentUnit.ReinitTable(null,true,false);
	}
}

_global.RotateToRightAngle = function(antiClockwise:Boolean):Void {
		var currentAngle = _global.SelectionFrame.Angle >= 0
        ? _global.SelectionFrame.Angle
        : _global.SelectionFrame.Angle + 360;
		var angle = currentAngle % 90;
		angle = antiClockwise
        ? - (angle + 90 * (angle == 0))
        : (90 - angle);
		_global.SelectionFrame.Angle = currentAngle + angle;
	}

if (SALocalWork.IsLocalWork) {
	_global.ProjectImageUrlFormat = "../Neato.Dol.WebDesigner/Images/P800.jpg";
	_global.LibraryImageIconUrlFormat = "../Neato.Dol.WebDesigner/Images/P800.jpg";
	_global.PaperIconUrlFormat = "../Neato.Dol.WebDesigner/Images/P800.jpg";
} else {
    _global.ProjectImageUrlFormat = "ProjectImage.aspx?mode=image&id=";
	_global.LibraryImageIconUrlFormat = "ImageLibrary.aspx?mode=icon&id=";
	_global.PaperIconUrlFormat = "Paper.aspx?mode=icon&id=";
}

function onReloadProject(eventObject) {
	_global.Project.Load();
	HintBox.getInstance(0).Update();
	HintBox.getInstance(1).Update();
}

function onLoadSettings(eventObject) {
	// Redraw WaterMark
	_global.UIController.PreviewAreaController.DataBind();
	
	switch (Tools.ToolContainer.getInstance().CurrentToolName) {
		case "Text" :
			
			break;
		case "Template" :
			
			break;
		case "Images":
			// Reload Image library		
			_global.UIController.ToolsController.imageToolController.LoadImageLibrary();
			_global.UIController.ToolsController.imageToolController.DataBind();
			break;
		case "Drawing":
			
			break;
		case "Shapes":
			// Reload Image library		
			_global.UIController.ToolsController.imageToolController.LoadImageLibrary();
			_global.UIController.ToolsController.imageToolController.DataBind();
			break;
		case "ChangePaper":
			
			break;
		}
}

if (_global.jslisten_init == undefined) {
	_global.jsvarlistener = new LocalConnection();
	_global.jsvarlistener.setVariables = function(query) {
		var i, values;
		var chunk = query.split("&");
		for (i in chunk) {
			values = chunk[i].split("=");
			if (values[0] == "cmd") {
				if(values[1] == "reload") {
					onReloadProject();
				} else if(values[1] == "save") {
					_global.Project.Save();
				} else if(values[1] == "update_settings") {
					_global.tr("update_settings");
					var settingsService:SASettings = SASettings.GetInstance();
					settingsService.RegisterOnLoadedHandler(this, onLoadSettings);
					settingsService.BeginLoad();
				}
			}
		}
	}
	_global.jsvarlistener.chkPSPortResult = function(chkRes) {
		SAPlugins.OnChkPSPortResult(chkRes.port,chkRes.result);
	}
	_global.jsvarlistener.connect("Designer");
	_global.jslisten_init = true;
}

_global.WatermarkHelper = Watermark.WatermarkHelper;
_global.PaperHelper = Helpers.PaperHelper;
_global.FaceHelper = new Helpers.FaceHelper();
