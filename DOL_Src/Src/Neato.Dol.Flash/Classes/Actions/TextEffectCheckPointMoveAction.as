import Actions.Action;
import Actions.ICommand;
import Actions.Property;

class Actions.TextEffectCheckPointMoveAction extends Action implements ICommand {
	private var checkPointId:Number;
	private var x:Property;
	private var y:Property;
	
	public function TextEffectCheckPointMoveAction(unit : Unit, checkPointId:Number, oldX:Number, oldY:Number, newX:Number, newY:Number) {
		super("Check Point Move", unit);
		this.checkPointId = checkPointId;
		this.x = new Property("X", oldX, newX);
		this.y = new Property("Y", oldY, newY);
	}

	public function Undo() : Void {
		super.Undo();
		MoveCheckPoint(x.Old, y.Old);
	}

	public function Redo() : Void {
		super.Redo();
		MoveCheckPoint(x.New, y.New);
	}
	
	private function MoveCheckPoint(X:Number, Y:Number) : Void {
		var obj:Object = {id:this.checkPointId, X:X, Y:Y};
		_global.SelectionFrame.OnHandlerUpdate(obj);
		_global.Project.CurrentUnit.UpdateTextEffect();
	}

	public function Update(action : Action) : Void {
		if (action instanceof TextEffectCheckPointMoveAction) {
			super.Update(action);
			var textEffectCheckPointMoveAction:TextEffectCheckPointMoveAction = TextEffectCheckPointMoveAction(action);
			this.x.New = textEffectCheckPointMoveAction.x.New;
			this.y.New = textEffectCheckPointMoveAction.y.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return x.IsIdentical() && y.IsIdentical();
	}

}