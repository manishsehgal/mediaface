import Actions.Action;
import Actions.ICommand;

class Actions.MoveableUnitAction extends Action implements ICommand {
	public function MoveableUnitAction(name : String, unit : MoveableUnit) {
		super(name, unit);
	}

	public function get Target():MoveableUnit {
		return MoveableUnit(super.Target);
	}
	
	public function set Target(value:MoveableUnit):Void {
		super.Target = value;
	}
	
	public function IsIdentical():Boolean {
		return true;
	}
}