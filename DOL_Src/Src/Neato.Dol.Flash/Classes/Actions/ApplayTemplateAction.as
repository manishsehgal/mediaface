import Actions.Action;
import Actions.ICommand;
import Actions.Property;

class Actions.ApplayTemplateAction extends Action implements ICommand {
	private var faceXml:Property;
	
	public function ApplayTemplateAction(unit : Unit, oldXml:XMLNode, newXml:XMLNode) {
		super("Apply Template", unit);
		unitId = "CurrentFace";
		faceXml = new Property("Face Xml", oldXml, newXml);
	}

	public function Undo() : Void {
		super.Undo();
		ParseXml(faceXml.Old);
	}

	public function Redo() : Void {
		super.Redo();
		ParseXml(faceXml.New);
	}
	
	private function ParseXml(xml:XMLNode) : Void { 
		var unit = Target;
		unit.Clear();
		unit.ParseXMLUnits(xml);
		unit.Draw();
	}

	public function Update(action : Action) : Void {
		super.Update();
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return false;
	}

}