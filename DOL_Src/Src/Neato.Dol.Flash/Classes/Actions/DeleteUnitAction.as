import Actions.Action;
import Actions.ICommand;
import Actions.ZOrderAction;

class Actions.DeleteUnitAction extends Action implements ICommand {
	private var unitXml:XMLNode;
	private var depth:Number;
	private var imageId:String = null;
	private var imageUrl:String = null;
	
	public function DeleteUnitAction(unit : Unit, xml:XMLNode, depth:Number) {
		super("Delete", unit);
		this.unitXml = xml;
		this.depth = depth;
		
		if (unit instanceof ImageUnit) {
			var imageUnit:ImageUnit = ImageUnit(unit);
			imageId = unit.id;
			imageUrl = imageUnit.Url;
		}
		else if (unit instanceof ShapeUnit) {
			var shapeUnit:ShapeUnit = ShapeUnit(unit);
			if (shapeUnit.FillType == "image") {
				imageId = shapeUnit.FillImageId; 
				imageUrl = shapeUnit.FillImageUrl;
			}
		}
	}

	public function Undo() : Void {
		super.Undo();
		var unit = _global.Project.CurrentPaper.CurrentFace.ParseXMLUnit(unitXml);
		unit.Draw();
		_global.Project.CurrentPaper.CurrentFace.ChangeCurrentUnit(unit);
		Target = unit;
		var newDepth:Number = _global.Project.CurrentPaper.CurrentFace.GetUnitIndex(unit.id);
		var zOrderAction:ZOrderAction = new ZOrderAction(unit, depth, newDepth);
		zOrderAction.Undo();
	}

	public function Redo() : Void {
		super.Redo();
		_global.Mode = _global.InactiveMode;
		_global.Project.CurrentPaper.CurrentFace.DeleteCurrentUnit();
	}

	public function Update(action : Action) : Void {
		super.Update(action);
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return false;
	}

	public function Clear() : Void {
		super.Clear();
		if (imageId != null && imageUrl != null) {
			_global.Project.CurrentPaper.UnloadImage(imageId, imageUrl);
		}
	}
}