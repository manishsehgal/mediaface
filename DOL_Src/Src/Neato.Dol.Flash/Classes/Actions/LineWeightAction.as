import Actions.Action;
import Actions.ICommand;
import Actions.Property;
import Actions.MoveableUnitAction;

class Actions.LineWeightAction extends MoveableUnitAction implements ICommand {
	private var lineWeight:Property;
	
	public function LineWeightAction(unit : MoveableUnit, oldLineWeight:Number, newLineWeight:Number) {
		super("Line weight", unit);
		lineWeight = new Property("LineWeight", oldLineWeight, newLineWeight);
	}

	public function Undo() : Void {
		super.Undo();
		BorderWidth(lineWeight.Old);
	}

	public function Redo() : Void {
		super.Redo();
		BorderWidth(lineWeight.New);
	}
	
	private function BorderWidth(LineWeight:Number) : Void {
		var unit = Target;
		unit.BorderWidth = LineWeight;
	}

	public function Update(action : Action) : Void {
		if (action instanceof LineWeightAction) {
			super.Update(action);
			var lineWeightAction:LineWeightAction = LineWeightAction(action);
			this.lineWeight.New = lineWeightAction.lineWeight.New;
		}
	}

	public function SetFocus() : Boolean {
		return true;
	}

	public function IsIdentical() : Boolean {
		return lineWeight.IsIdentical();
	}

}