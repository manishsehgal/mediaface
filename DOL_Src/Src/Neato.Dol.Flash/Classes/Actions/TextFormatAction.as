import Actions.Action;
import Actions.ICommand;
import Actions.Property;

class Actions.TextFormatAction extends Action implements ICommand {
	private var beginIndex:Number;
	private var endIndex:Number;
	private var formats:Property; 
	
	public function TextFormatAction(unit : Unit, beginIndex:Number, endIndex:Number, oldFormats:Array, newFormats:Array) {
		super("Font Format", unit);
		this.beginIndex = beginIndex;
		this.endIndex = endIndex; 
		formats = new Property("FontFormat", oldFormats, newFormats);
	}

	public function Undo() : Void {
		super.Undo();
		ChangeFontFormat(beginIndex, endIndex, formats.Old);
	}

	public function Redo() : Void {
		super.Redo();
		ChangeFontFormat(beginIndex, endIndex, formats.New);
	}
	
	private function ChangeFontFormat(begin:Number, end:Number, Formats:Array) : Void {
		var unit = Target;
		for(var index:Number = begin; index < end; ++index)
			unit.SetFontFormat(index, Formats[index-begin]);

		if (begin == end)
			unit.Draw();
		if (unit instanceof TextEffectUnit)
			unit.UpdateTextEffect();
		_global.SelectionFrame.RedrawUnit();
	}

	public function Update(action : Action) : Void {
		if (action instanceof TextFormatAction) {
			super.Update(action);
			var textFormatAction:TextFormatAction = TextFormatAction(action);
			this.formats.New = textFormatAction.formats.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return formats.IsIdentical();
	}

}