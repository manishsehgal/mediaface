import Actions.Action;
import Actions.ICommand;
import Actions.Property;

class Actions.TableFormatAction extends Action implements ICommand {
	private var format:Property; 
	
	public function TableFormatAction(unit : Unit, oldFormat:TextFormat, newFormat:TextFormat) {
		super("Font Format", unit);
		format = new Property("FontFormat", oldFormat, newFormat);
	}

	public function Undo() : Void {
		super.Undo();
		ChangeFontFormat(format.Old);
	}

	public function Redo() : Void {
		super.Redo();
		ChangeFontFormat(format.New);
	}
	
	private function ChangeFontFormat(Format:TextFormat) : Void {
		var unit = Target;
		unit.FontFormat = Format;
	}

	public function Update(action : Action) : Void {
		if (action instanceof TableFormatAction) {
			super.Update(action);
			var tableFormatAction:TableFormatAction = TableFormatAction(action);
			format.New = tableFormatAction.format.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return format.IsIdentical();
	}

}