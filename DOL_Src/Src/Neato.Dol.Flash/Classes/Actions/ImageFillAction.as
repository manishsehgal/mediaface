import Actions.Action;
import Actions.GradientFillAction;
import Actions.ICommand;
import Actions.Property;

class Actions.ImageFillAction extends GradientFillAction implements ICommand {
	private var imageId:Property;
	private var fillType:Property;
	private var imageUrl:String;
	
	public function ImageFillAction(unit : Unit, oldColor:Number, oldColor2:Number, oldGradientType:String, oldIsFilled:Boolean, oldImageId:String, oldFillType:String, newColor:Number, newColor2:Number, newGradientType:String, newIsFilled:Boolean, newImageId:String, newFillType:String, oldImageUrl:String) {
		super(unit, oldColor, oldColor2, oldGradientType, oldIsFilled, newColor, newColor2, newGradientType, newIsFilled);
		Name = "Image Fill";
		fillType = new Property("Fill type", oldFillType, newFillType); 
		imageId = new Property("Image Id", oldImageId, newImageId);
		this.imageUrl = oldImageUrl; 
	}

	public function Undo() : Void {
		if (fillType.Old == "image") {
			ImageFill(imageId.Old);
		}
		else if (fillType.Old == "gradient") {
			GradientFill(color.Old, color2.Old, gradientType.Old, isFilled.Old);
		}
	}

	public function Redo() : Void {
		if (fillType.New == "image") {
			ImageFill(imageId.New);
		}
		else if (fillType.New == "gradient") {
			GradientFill(color.New, color2.New, gradientType.New, isFilled.New);
		}
	}

	private function ImageFill(ImageId:String, Transparency:Boolean) : Void {
//		_global.tr("### ImageFill id = " + ImageId);
		var unit = Target;
		unit.FillImageId = ImageId;
		unit.FillType = "image";
		unit.Transparency = false;
	}

	public function Update(action : Action) : Void {
		if (action instanceof ImageFillAction) {
			super.Update(action);
			var imageFillAction:ImageFillAction = ImageFillAction(action);
			this.color.New = imageFillAction.color.New;
			this.color2.New = imageFillAction.color2.New;
			this.gradientType.New = imageFillAction.gradientType.New;
			this.isFilled.New = imageFillAction.isFilled.New;
			this.imageId.New = imageFillAction.imageId.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return super.IsIdentical() && imageId.IsIdentical();
	}

	public function Clear() : Void {
		super.Clear();
//		_global.tr("### ImageFillAction.Clear() oldImageUrl = " + imageUrl);
		if (imageId.Old != null && imageUrl != null) {
			_global.Project.CurrentPaper.UnloadImage(imageId.Old, imageUrl);
		}
	}
}