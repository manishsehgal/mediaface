import Actions.Action;

interface Actions.ICommand {
	public function Undo():Void;
	public function Redo():Void;
	public function Update(action:Action):Void;
	public function SetFocus():Boolean;
	public function IsIdentical():Boolean;
}