﻿import Actions.Action;
import Actions.ICommand;
import Actions.Property;

class Actions.MoveAction extends Action implements ICommand {
	private var x:Property;
	private var y:Property;
	
	public function MoveAction(unit : Unit, oldX : Number, oldY : Number, newX : Number, newY : Number) {
		super("Move", unit);
		this.x = new Property("X", oldX, newX);
		this.y = new Property("Y", oldY, newY); 
	}

	public function Undo() : Void {
		super.Undo();
		Move(x.Old, y.Old);
	}

	public function Redo() : Void {
		super.Redo();
		Move(x.New, y.New);
	}
	
	private function Move(X:Number, Y:Number):Void {
		_global.SelectionFrame.X = X;
		_global.SelectionFrame.Y = Y;
	}
	
	public function Update(action:Action):Void {
		if(action instanceof MoveAction) {
			super.Update(action);
			var moveAction:MoveAction = MoveAction(action);
			this.x.New = moveAction.x.New;
			this.y.New = moveAction.y.New;
		}
	}
	
	public function SetFocus():Boolean {
		return true;
	}
	
	public function IsIdentical():Boolean {
		return x.IsIdentical() && y.IsIdentical();
	}
}