import Actions.Action;
import Actions.ICommand;
import Actions.Property;

class Actions.ColorAction extends Action implements ICommand {
	private var color:Property;
	
	public function ColorAction(name : String, unit : Unit, oldColor:Number, newColor:Number) {
		super(name, unit);
		color = new Property("Color", oldColor, newColor);
	}

	public function Undo() : Void {
		super.Undo();
	}

	public function Redo() : Void {
		super.Redo();
	}

	public function Update(action : Action) : Void {
		if (action instanceof ColorAction) {
			super.Update(action);
			var colorAction:ColorAction = ColorAction(action);
			this.color.New = colorAction.color.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return color.IsIdentical();
	}

}