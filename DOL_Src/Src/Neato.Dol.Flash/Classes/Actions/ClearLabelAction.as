import Actions.Action;
import Actions.ICommand;

class Actions.ClearLabelAction extends Action implements ICommand {
	private var faceXml:XMLNode;
	
	public function ClearLabelAction(unit : Unit, xml:XMLNode) {
		super("Clear label", unit);
		unitId = "CurrentFace";
		this.faceXml = xml;
	}

	public function Undo() : Void {
		super.Undo();
		var unit = Target;
		unit.ParseXMLUnits(faceXml);
		unit.Draw();
	}

	public function Redo() : Void {
		super.Redo();
		var unit = Target;
		unit.Clear();
	}

	public function Update(action : Action) : Void {
		super.Update(action);
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return false;
	}

	public function Clear() : Void {
		super.Clear();
		for (var node:XMLNode = faceXml.firstChild; node != null; node = node.nextSibling) {
			var imageId:String  = null;
			var imageUrl:String = null;
			switch(node.nodeName) {
				case "Image":
					var n = node;
					var imageUnit = new ImageUnit(null, n, _global.ProjectImageUrlFormat);
					imageId = node.attributes.id;
					imageUrl = imageUnit.buildUrl();
					break;
				case "Shape":
					var fillNode:XMLNode = Utils.XMLGetChild(node,"Fill");
					if (fillNode.attributes.type == "image") {
						imageId = fillNode.attributes.fillImageId;
						imageUrl =_global.ProjectImageUrlFormat + imageId;
					}
					break;
			}
//			_global.tr("### imageId " + imageId + " imageUrl = " + imageUrl);
			_global.Project.CurrentPaper.UnloadImage(imageId, imageUrl);
		}
	}
}