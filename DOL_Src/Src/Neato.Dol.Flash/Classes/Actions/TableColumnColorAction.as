import Actions.Action;
import Actions.ColorAction;
import Actions.ICommand;

class Actions.TableColumnColorAction extends ColorAction implements ICommand {
	private var id;
	
	public function TableColumnColorAction(unit : Unit, id, oldColor : Number, newColor : Number) {
		super("Table Color", unit, oldColor, newColor);
		this.id = id;
	}

	public function Undo() : Void {
		super.Undo();
		SetColumnColor(id, color.Old);
	}

	public function Redo() : Void {
		super.Redo();
		SetColumnColor(id, color.New);
	}
	
	private function SetColumnColor(id, Color:Number) : Void {
		var unit = Target;
		unit.SetColumnColor(id, Color);
	}

	public function Update(action : Action) : Void {
		if (action instanceof TableColumnColorAction) {
			super.Update(action);
			var colorAction:TableColumnColorAction = TableColumnColorAction(action);
			this.color.New = colorAction.color.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return super.IsIdentical();
	}

}