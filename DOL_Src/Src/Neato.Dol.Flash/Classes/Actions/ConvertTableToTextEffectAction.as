import Actions.Action;
import Actions.ICommand;
import Actions.ZOrderAction;
import Actions.TextEffectChangeAction;

class Actions.ConvertTableToTextEffectAction extends TextEffectChangeAction implements ICommand {
	
	public function ConvertTableToTextEffectAction(unit : Unit, oldEffect : String, newEffect : String, xml:XMLNode) {
		super(unit, oldEffect, newEffect, xml);
	}

	public function Undo() : Void {
		var unit = Target;
		var oldDepth:Number = _global.Project.CurrentPaper.CurrentFace.GetUnitIndex(unit.id);
		_global.Project.CurrentPaper.CurrentFace.DeleteCurrentUnit();
		
		unit = _global.Project.CurrentPaper.CurrentFace.ParseXMLUnit(xmlNode);
		_global.Project.CurrentPaper.CurrentFace.ChangeCurrentUnit(unit);
		Target = unit;

		var newDepth:Number = _global.Project.CurrentPaper.CurrentFace.GetUnitIndex(unit.id);
		var zOrderAction:ZOrderAction = new ZOrderAction(unit, oldDepth, newDepth);
		zOrderAction.Undo();
	}

	public function Redo() : Void {
		_global.Project.CurrentPaper.CurrentFace.ConvertPlaylistToEffect(effect.New);
		Target = _global.Project.CurrentUnit;
	}

	public function Update(action : Action) : Void {
		super.Update(action);
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return false;
	}

}