import Actions.Action;
import Actions.DeleteUnitAction;
import Actions.AddUnitAction; 
import Actions.ImageFillAction;
import Actions.ClearLabelAction;
import mx.events.EventDispatcher;
import mx.utils.Delegate;

class Actions.ActionManager {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(ActionManager.prototype);

	private var actions:Array;
	private var capacity:Number = 10;
	private var index:Number = -1;
	private var focused:Boolean = false;
	
	public function ActionManager(c:Number) {
		//_global.tr("### ActionManager.ActionManager() c = " + c);
		actions = new Array();
		this.capacity = c;
	}
	
	public function get Focused():Boolean {
		return focused;
	}
	
	public function set Focused(value:Boolean):Void {
		focused = value;
	}
	
	public function Insert(action:Action):Void {
//		_global.tr("### ActionManager.Insert() index = " + index);
		if (!action.Equal(actions[index])) {
//			_global.tr("### !action.Equal(actions[index])");
			Focused = false;
		}
		if (Focused) {
//			_global.tr("### return");
			actions[index].Update(action);
			return;
		}
		if (action.IsIdentical())	// ???
			return;					// ???
		if (++ index != actions.length) {
			RemoveActions(index);
		}
//		_global.tr("### index = " + index);
		actions.push(action);
		Focused = action.SetFocus();
		
		if (actions.length > capacity) {
//			_global.tr("### if (actions.length > capacity)");
			RemoveFirstAction();
			-- index;
		}
		OnChange();
//		_global.tr("### _______________");
	}
	
	private function RemoveFirstAction() : Void {
		var action = actions[0]; 
		if (CheckClearAction(1) == false) { 
			if (action instanceof DeleteUnitAction) {
				action.Clear();
			}
			else if (action instanceof ImageFillAction) {
				var unit = action.Target;
				if (unit != null) {
					if (action.imageUrl != unit.FillImageUrl) {
						action.Clear();
					}
				}
				else {
					action.Clear();
				}
			}
			else if (action instanceof ClearLabelAction) {
				action.Clear();
			}
		}
		actions.splice(0, 1);
	}
	
	private function RemoveActions(beginIndex:Number) : Void {
		for (var i:Number = beginIndex; i < actions.length; ++ i) {
			var action = actions[i];
			if (!(action instanceof Actions.ClearLabelAction))
				action.Clear();
		}
		actions.splice(beginIndex);
	}
	
	private function CheckClearAction(beginIndex:Number) : Boolean {
		for (var i:Number = beginIndex; i < actions.length; ++ i) {
			var action = actions[i];
			if (action instanceof ClearLabelAction)
				return true;
		}
		return false;
	}
	
	public function Clear() : Void {
//		_global.tr("### ActionManager.Clear()");
		actions = new Array();
		index = -1;
		OnChange();
	}
	
	public function get Index():Number {
		return index; 
	}
	
	public function set Index(value:Number):Void {
		index = value;
	}
	
	public function get CanUndo():Boolean {
		return index >= 0 && actions.length > 0; 
	}
	
	public function get CanRedo():Boolean {
		return index < (actions.length - 1) && actions.length > 0; 
	}
	
	public function Undo():Void {
		//OnChange(true);
		Focused = false;
//		_global.tr("### ActionManager.Undo() index = " + index + " actions.length = " + actions.length + " CanUndo = " + CanUndo);
		if (CanUndo) {
			var action:Action = actions[index --];
			UndoRedo(action, true);
		}
		OnChange();
	}
	
	public function Redo():Void {
		//OnChange(true);
		Focused = false;
//		_global.tr("### ActionManager.Redo() index = " + index + " actions.length = " + actions.length + " CanRedo = " + CanRedo);
		if (CanRedo) {
			var action:Action = actions[++ index];
			UndoRedo(action, false);
		}
		OnChange();
	}
	
	private function UndoRedo(action:Action, doUndo:Boolean):Void {
//		_global.tr("### action.Name = " + action.Name);

		var isFaceUnit:Boolean = action.Target instanceof FaceUnit;
		if (!isFaceUnit) {
			_global.Project.CurrentPaper.CurrentFace.ChangeCurrentUnit(action.Target);
		}

		if (doUndo)
			action.Undo();
		else
			action.Redo();
		
		if (!isFaceUnit) {
			if (_global.Project.CurrentUnit != null)
				_global.SelectionFrame.AttachUnit(action.Target);
			else
				_global.UIController.FramesController.detach();
			_global.UIController.ToolsController.OnUnitChanged();
		}
		else {
			_global.UIController.UnselectCurrentUnit();
		}

//		_global.tr("### _______________");
	}
	
	public function RegisterOnChangeHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("change", Delegate.create(scopeObject, callBackFunction));
    }

	public function OnChange(disable:Boolean) : Void {
		disable = disable == true;
		var eventObject:Object = new Object();
		eventObject.type = "change";
		eventObject.target = this;
		if (disable) {
			eventObject.CanUndo = false;
			eventObject.CanRedo = false;
		}
		else {
			eventObject.CanUndo = CanUndo;
			eventObject.CanRedo = CanRedo;
			if (CanUndo) {
				var action:Action = actions[index];
				eventObject.UndoAction = action.Name;
			} 
			if (CanRedo) {
				var action:Action = actions[index + 1];
				eventObject.RedoAction = action.Name;
			}
		}
		dispatchEvent(eventObject);
	}
}