import Actions.Action;
import Actions.ICommand;
import Actions.Property;
import Actions.MoveableUnitAction;

class Actions.TransparentFillAction extends MoveableUnitAction implements ICommand {
	private var transparency:Property;
	
	public function TransparentFillAction(unit : MoveableUnit, oldTransparency:Boolean, newTransparency:Boolean) {
		super(name, unit);
		transparency = new Property("Transparency", oldTransparency, newTransparency);
	}

	public function Undo() : Void {
		super.Undo();
		SetTransparency(transparency.Old);
	}

	public function Redo() : Void {
		super.Redo();
		SetTransparency(transparency.New);
	}
	
	public function SetTransparency(Transparency:Boolean):Void {
		var unit = Target;
		unit.Transparency = Transparency;
	}

	public function Update(action : Action) : Void {
		if (action instanceof TransparentFillAction) {
			super.Update();
			var transparentFillAction:TransparentFillAction = TransparentFillAction(action);
			this.transparency.New = transparentFillAction.transparency.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return transparency.IsIdentical();
	}

}