import Actions.Action;
import Actions.ICommand;
import Actions.Property;

class Actions.TableColumnVisibleAction extends Action implements ICommand {
	private var visible:Property;
	private var id;
	
	public function TableColumnVisibleAction(unit : Unit, id, oldVisible:Boolean, newVisible:Boolean) {
		super("Hide column", unit);
		this.id = id;
		visible = new Property("Visible", oldVisible, newVisible);
	}

	public function Undo() : Void {
		super.Undo();
		HideColumn(id, visible.Old);
	}

	public function Redo() : Void {
		super.Redo();
		HideColumn(id, visible.New);
	}
	
	private function HideColumn(id, Visible:Boolean) : Void {
		var unit = Target;
		unit.SetColumnVisible(id, Visible);
	}

	public function Update(action : Action) : Void {
		if (action instanceof TableColumnVisibleAction) {
			super.Update(action);
			var tableColumnVisibleAction:TableColumnVisibleAction = TableColumnVisibleAction(action);
			visible.New = tableColumnVisibleAction.visible.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return visible.IsIdentical();
	}

}