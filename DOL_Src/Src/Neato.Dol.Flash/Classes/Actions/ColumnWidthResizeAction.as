import Actions.Action;
import Actions.ICommand;
import Actions.Property;

class Actions.ColumnWidthResizeAction extends Action implements ICommand {
	private var width:Property;
	private var columnNumber:Number;
	
	public function ColumnWidthResizeAction(unit : Unit, columnNumber:Number, oldWidth:Number, newWidth:Number) {
		super("Column resize", unit);
		width = new Property("Width", oldWidth, newWidth);
		this.columnNumber = columnNumber;
	}

	public function Undo() : Void {
		super.Undo();
		ResizeColumn(columnNumber, width.Old - width.New);
	}

	public function Redo() : Void {
		super.Redo();
		ResizeColumn(columnNumber, width.New - width.Old);
	}
	
	private function ResizeColumn(ColumnNumber:Number, Width:Number) : Void {
		var unit = Target;
		unit.ResizeColumnWidth(ColumnNumber, Width);
		_global.SelectionFrame.AttachUnit(unit);
	}

	public function Update(action : Action) : Void {
		if (action instanceof ColumnWidthResizeAction) {
			super.Update(action);
			var columnWidthResizeAction:ColumnWidthResizeAction = ColumnWidthResizeAction(action);
			width.New = columnWidthResizeAction.width.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return width.IsIdentical();
	}

}