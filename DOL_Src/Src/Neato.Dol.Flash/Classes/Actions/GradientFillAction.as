import Actions.Action;
import Actions.ICommand;
import Actions.Property;

class Actions.GradientFillAction extends Action implements ICommand {
	private var color:Property;
	private var color2:Property;
	private var gradientType:Property;
	private var isFilled:Property;
	
	public function GradientFillAction(unit : Unit, oldColor:Number, oldColor2:Number, oldGradientType:String, oldIsFilled:Boolean, newColor:Number, newColor2:Number, newGradientType:String, newIsFilled:Boolean) {
		super("Gradient fill", unit);
		if (unit instanceof FaceUnit)
			unitId = "CurrentFace";
		color = new Property("Color", oldColor, newColor);
		color2 = new Property("Color2", oldColor2, newColor2);
		gradientType = new Property("GradientType", oldGradientType, newGradientType);
		isFilled = new Property("IsFilled", oldIsFilled, newIsFilled);
	}

	public function Undo() : Void {
		super.Undo();
		GradientFill(color.Old, color2.Old, gradientType.Old, isFilled.Old);
	}

	public function Redo() : Void {
		super.Redo();
		GradientFill(color.New, color2.New, gradientType.New, isFilled.New);
	}
	
	private function GradientFill(Color:Number, Color2:Number, GradientType:String, IsFilled:Boolean) : Void {
//		_global.tr("### Gradientfill Color = " + Color + " Color2 = " + Color2 + " GradientType = " + GradientType);
		var unit = Target;
		unit.Fill(Color, Color2, GradientType, IsFilled);
	}

	public function Update(action : Action) : Void {
		if (action instanceof GradientFillAction) {
			super.Update(action);
			var gradientFillAction:GradientFillAction = GradientFillAction(action);
			this.color.New = gradientFillAction.color.New;
			this.color2.New = gradientFillAction.color2.New;
			this.gradientType.New = gradientFillAction.gradientType.New;
			this.isFilled.New = gradientFillAction.isFilled.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return color.IsIdentical() && color2.IsIdentical() && gradientType.IsIdentical() && isFilled.IsIdentical();
	}

}