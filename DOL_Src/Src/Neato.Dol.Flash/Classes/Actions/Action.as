import Actions.ICommand;

class Actions.Action implements ICommand {
	private var name:String;
	private var unitId:String;
	
	public function get Name():String {
		return name;
	}
	
	public function set Name(value:String):Void {
		name = value;
	}
	
	public function Action(name:String, unit : Unit) {
//		_global.tr("### Action.Action() name = " + name + " unit.id = " + unit.id);
		this.name = name;
		this.unitId = unit.id;
	}
	
	public function Clear() : Void {
//		_global.tr("### Action.Clear() name = " + name);
	}

	public function Equal(action:Action):Boolean {
		var e:Boolean = Name == action.Name && Target == action.Target && action != null && action != undefined;
		if (!e) {
//			_global.tr("### Equal action = " + action);
//			_global.tr("### Equal Name = " + Name + " action.Name = " + action.Name + " == " + (Name == action.Name));
//			_global.tr("### Equal Target = " + Target + " action.Target = " + action.Target + " == " + (Target == action.Target));
		}
		return e;
	}

	private function GetUnitById(id:String) : Unit {
		var face = _global.Project.CurrentPaper.CurrentFace;
		if (id == "CurrentFace")
			return face;
		return face.FindUnitById(id);
	}

	public function get Target():Unit {
		return GetUnitById(unitId);
	}
	
	public function set Target(value:Unit):Void {
		unitId = value.id;
	}

	public function Undo():Void {
//		_global.tr("### Action.Undo() " + Name + " unitId = " + unitId);
	}
	
	public function Redo():Void {
//		_global.tr("### Action.Redo() " + Name + " unitId = " + unitId);
	}
	
	public function Update(action:Action):Void {
//		_global.tr("### Action.Update() " + Name);
	}
	
	public function SetFocus():Boolean {
		return false;
	}
	
	public function IsIdentical():Boolean {
		return true;
	}
}