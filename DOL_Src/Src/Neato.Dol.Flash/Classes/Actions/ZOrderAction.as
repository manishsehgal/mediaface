import Actions.Action;
import Actions.ICommand;
import Actions.Property;

class Actions.ZOrderAction extends Action implements ICommand {
	private var depth:Property;  
	
	public function ZOrderAction(unit : Unit, oldDepth:Number, newDepth:Number) {
		super("Depth", unit);
		depth = new Property("Depth", oldDepth, newDepth);
	}

	public function Undo() : Void {
		super.Undo();
		
		if (depth.Old > depth.New) {
			ToFrontCurrentUnit(depth.Old - depth.New);
		}
		else {
			ToBackCurrentUnit(depth.New - depth.Old);
		}
	}

	public function Redo() : Void {
		super.Redo();

		if (depth.Old > depth.New) {
			ToBackCurrentUnit(depth.Old - depth.New);
		}
		else {
			ToFrontCurrentUnit(depth.New - depth.Old);
		}
	}

	public function Update(action : Action) : Void {
		if (action instanceof ZOrderAction) {
			super.Update(action);
			var zOrderAction:ZOrderAction = ZOrderAction(action);
			this.depth.New = zOrderAction.depth.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function ToBackCurrentUnit(count:Number) : Void {
		for (var index:Number = 0; index < count; ++ index) {
			_global.Project.CurrentPaper.CurrentFace.ToBackCurrentUnit();
		}
	} 
	
	public function ToFrontCurrentUnit(count:Number) : Void {
		for (var index:Number = 0; index < count; ++ index) {
			_global.Project.CurrentPaper.CurrentFace.ToFrontCurrentUnit();
		}
	} 
	
	public function IsIdentical():Boolean {
		return depth.IsIdentical();
	}
}