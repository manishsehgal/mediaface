import Actions.Action;
import Actions.ColorAction;
import Actions.ICommand;

class Actions.LineColorAction extends ColorAction implements ICommand {
	
	public function LineColorAction(unit : Unit, oldColor : Number, newColor : Number) {
		super("Line color", unit, oldColor, newColor);
	}

	public function Undo() : Void {
		super.Undo();
		BorderColor(color.Old); 
	}

	public function Redo() : Void {
		super.Redo();
		BorderColor(color.New);
	}

	private function BorderColor(Color:Number) : Void {
		var unit = Target;
		unit.BorderColor = Color;
	}

	public function Update(action : Action) : Void {
		if (action instanceof LineColorAction) {
			super.Update();
			var lineColorAction:LineColorAction = LineColorAction(action);
			this.color.New = lineColorAction.color.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return super.IsIdentical();
	}

}