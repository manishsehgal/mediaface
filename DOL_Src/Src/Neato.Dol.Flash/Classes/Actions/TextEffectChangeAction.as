import Actions.Action;
import Actions.ICommand;
import Actions.Property;
import Actions.ZOrderAction;

class Actions.TextEffectChangeAction extends Action implements ICommand {
	private var effect:Property;
	private var xmlNode:XMLNode;
	
	public function TextEffectChangeAction(unit : Unit, oldEffect:String, newEffect:String, xml:XMLNode) {
		super("Text effect", unit);
		effect = new Property("Effect", oldEffect, newEffect);
		xmlNode = xml;
	}

	public function Undo() : Void {
		var unit = Target;
		var oldDepth:Number = _global.Project.CurrentPaper.CurrentFace.GetUnitIndex(unit.id);
		_global.Project.CurrentPaper.CurrentFace.DeleteCurrentUnit();
		
		unit = _global.Project.CurrentPaper.CurrentFace.ParseXMLUnit(xmlNode);
		unit.Draw();
		_global.Project.CurrentPaper.CurrentFace.ChangeCurrentUnit(unit);
		Target = unit;

		var newDepth:Number = _global.Project.CurrentPaper.CurrentFace.GetUnitIndex(unit.id);
		var zOrderAction:ZOrderAction = new ZOrderAction(unit, oldDepth, newDepth);
		zOrderAction.Undo();
	}

	public function Redo() : Void {
		super.Redo();
		ChangeTextEffect(effect.New);
	}
	
	private function ChangeTextEffect(Effect:String) : Void {
		var unit = Target;
//		_global.tr("### ChangeTextEffect Effect = " + Effect + " unit = " + unit);
  		if(Effect == "IsTextUnit"){
  			Logic.TextLogic.ConvertEffectToText(unit);
  		}
  		else {
  			Logic.TextLogic.ConvertTextToEffect(unit, Effect);
  		}
//  		_global.tr("### ChangeTextEffect _global.Project.CurrentUnit = " + _global.Project.CurrentUnit);
  		Target = _global.Project.CurrentUnit;
  	    //SB
		_global.SelectionFrame.RedrawUnit();
	}

	public function Update(action : Action) : Void {
		if (action instanceof TextEffectChangeAction) {
			super.Update(action);
			var textEffectChangeAction:TextEffectChangeAction = TextEffectChangeAction(action);
			effect.New = textEffectChangeAction.effect.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return effect.IsIdentical();
	}

}