import Actions.Action;
import Actions.ICommand;
import Actions.Property;

class Actions.TableHeightResizeAction extends Action implements ICommand {
	private var height:Property;
	private var position:String;
	
	public function TableHeightResizeAction(unit : Unit, position:String, oldHeight:Number, newHeight:Number) {
		super("Table height", unit);
		this.position = position;
		height = new Property("Height", oldHeight, newHeight);
	}

	public function Undo() : Void {
		super.Undo();
		
		var h:Number = height.Old - height.New;
		if(position == "topright")
			h *= - 1;
		
		ResizeHeight(position, h);
	}

	public function Redo() : Void {
		super.Redo();
		
		var h:Number = height.New - height.Old;
		if(position == "topright")
			h *= - 1;
		
		ResizeHeight(position, h);
	}
	
	private function ResizeHeight(Position:String, Height:Number) : Void {
		var unit = Target;
		unit.SetLeading(Position, Height);
		_global.UIController.FramesController.attach(unit.frameLinkageName, false);
	}

	public function Update(action : Action) : Void {
		if (action instanceof TableHeightResizeAction) {
			super.Update(action);
			var tableHeightResizeAction:TableHeightResizeAction = TableHeightResizeAction(action);
			height.New = tableHeightResizeAction.height.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return height.IsIdentical();
	}

}