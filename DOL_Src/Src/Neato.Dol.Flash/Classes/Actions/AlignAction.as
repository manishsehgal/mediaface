import Actions.Action;
import Actions.ICommand;
import Actions.Property;
import Actions.MoveableUnitAction;

class Actions.AlignAction extends MoveableUnitAction implements ICommand {
	private var align:Property;
	
	public function AlignAction(unit : MoveableUnit, oldAlign:String, newAlign:String) {
		super("Align", unit);
		align = new Property("Align", oldAlign, newAlign);
	}

	public function Undo() : Void {
		super.Undo();
		AlignUnit(align.Old);
	}

	public function Redo() : Void {
		super.Redo();
		AlignUnit(align.New);
	}
	
	private function AlignUnit(Align:String) : Void {
		var unit = Target;
		unit.Align = Align;
	}

	public function Update(action : Action) : Void {
		if (action instanceof AlignAction) {
			super.Update(action);
			var alignAction:AlignAction = AlignAction(action);
			this.align.New = alignAction.align.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return align.IsIdentical();
	}

}