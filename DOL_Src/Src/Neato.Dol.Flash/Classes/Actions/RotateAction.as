import Actions.Action;
import Actions.ICommand;
import Actions.Property;
import Actions.MoveableUnitAction;

class Actions.RotateAction extends MoveableUnitAction implements ICommand {
	private var angle:Property;
	
	public function RotateAction(unit : MoveableUnit, oldAngle:Number, newAngle:Number) {
		super("Rotate", unit);
		angle = new Property("Angle", oldAngle, newAngle);
	}

	public function Undo() : Void {
		super.Undo();
		Rotate(angle.Old);
	}

	public function Redo() : Void {
		super.Undo();
		Rotate(angle.New);
	}
	
	public function Rotate(Angle:Number) : Void {
		_global.SelectionFrame.Angle = Angle;
	}

	public function Update(action : Action) : Void {
		if(action instanceof RotateAction) {
			super.Update(action);
			var moveAction:RotateAction = RotateAction(action);
			this.angle.New = moveAction.angle.New;
		}
	}

	public function SetFocus() : Boolean {
		return true;
	}
	
	public function IsIdentical():Boolean {
		return angle.IsIdentical();
	}
}