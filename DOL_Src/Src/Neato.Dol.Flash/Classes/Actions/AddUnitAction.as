import Actions.Action;
import Actions.ICommand;
import Actions.Property;

class Actions.AddUnitAction extends Action implements ICommand {
	private var unitXml:XMLNode;
	private var imageId:String = null;
	private var imageUrl:String = null;
	
	public function AddUnitAction(unit : Unit, xml:XMLNode) {
		super("Add", unit);
		this.unitXml = xml;

		if (unit instanceof ImageUnit) {
			var imageUnit:ImageUnit = ImageUnit(unit);
			imageId = unit.id;
			imageUrl = imageUnit.Url;
		}
		else if (unit instanceof ShapeUnit) {
			var shapeUnit:ShapeUnit = ShapeUnit(unit);
			if (shapeUnit.FillType == "image") {
				imageId = shapeUnit.FillImageId; 
				imageUrl = shapeUnit.FillImageUrl;
			}
		}
	}

	public function Undo() : Void {
		super.Undo();
		_global.Mode = _global.InactiveMode;
		_global.Project.CurrentPaper.CurrentFace.DeleteCurrentUnit();
	}

	public function Redo() : Void {
		super.Redo();
		var unit = _global.Project.CurrentPaper.CurrentFace.ParseXMLUnit(unitXml);
		unit.Draw();
		_global.Project.CurrentPaper.CurrentFace.ChangeCurrentUnit(unit);
		Target = unit;
	}

	public function Update(action : Action) : Void {
		super.Update(action);
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return false;
	}

	public function Clear() : Void {
		super.Clear();
		if (imageId != null && imageUrl != null) {
			_global.Project.CurrentPaper.UnloadImage(imageId, imageUrl);
		}
	}
}