import Actions.Action;
import Actions.ICommand;
import Actions.Property;
import Actions.TextFormatAction;

class Actions.TextAction extends TextFormatAction implements ICommand {
	private var text:Property;
	
	public function TextAction(unit : Unit, oldFormats : Array, oldText:String, newFormats : Array, newText:String) {
		super(unit, 0, oldText.length, oldFormats, newFormats);
		Name = "Text typing";
		text = new Property("Text", oldText, newText);
	}

	public function Undo() : Void {
		ChangeText(text.Old, formats.Old);
	}

	public function Redo() : Void {
		ChangeText(text.New, formats.New);
	}
	
	private function ChangeText(Text:String, Formats:Array) : Void {
		var unit = Target;
		unit.SetText(Text);
		ChangeFontFormat(0, Text.length, Formats);
	}

	public function Update(action : Action) : Void {
		if (action instanceof TextAction) {
			var textAction:TextAction = TextAction(action);
			this.text.New = textAction.text.New;
			this.formats.New = textAction.formats.New;
		}
	}

	public function SetFocus() : Boolean {
		return true;
	}

	public function IsIdentical() : Boolean {
		return super.IsIdentical() && text.IsIdentical();
	}

}