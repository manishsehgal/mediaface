import Actions.Action;
import Actions.ICommand;
import Actions.Property;
import Actions.MoveAction;
import Actions.RotateAction;
import Actions.ScaleAction;
import Actions.MoveableUnitAction;

class Actions.AutofitAction extends MoveableUnitAction implements ICommand {
	private var moveAction:MoveAction;
	private var scaleAction:ScaleAction;
	private var rotateAction:RotateAction;
	private var autofitType:Property; 
	
	public function AutofitAction(unit : MoveableUnit, oldX:Number, oldY:Number, oldAngle:Number, oldScaleX:Number, oldScaleY:Number, oldAutofitType:String, newX:Number, newY:Number, newAngle:Number, newScaleX:Number, newScaleY:Number, newAutofitType:String) {
		super("Autofit", unit);
		autofitType = new Property("AutofitType", oldAutofitType, newAutofitType);
		moveAction = new MoveAction(unit, oldX, oldY, newX, newY);
		scaleAction = new ScaleAction(unit, oldScaleX, oldScaleY, newScaleX, newScaleY);
		rotateAction = new RotateAction(unit, oldAngle, newAngle);
	}

	public function Undo() : Void {
		super.Undo();
		scaleAction.Undo();
		rotateAction.Undo();
		moveAction.Undo();
		Autofit(autofitType.Old);
	}

	public function Redo() : Void {
		super.Redo();
		scaleAction.Redo();
		rotateAction.Redo();
		moveAction.Redo();
		Autofit(autofitType.New);
	}
	
	private function Autofit(AutofitType:String) : Void {
		Target.AutofitType = AutofitType;
	}

	public function Update(action : Action) : Void {
		if (action instanceof AutofitAction) {
			var autofitAction:AutofitAction = AutofitAction(action);
			this.moveAction.Update(autofitAction.moveAction);
			this.scaleAction.Update(autofitAction.scaleAction);
			this.rotateAction.Update(autofitAction.rotateAction);
			this.autofitType.New = autofitAction.autofitType.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return autofitType.IsIdentical() && moveAction.IsIdentical() && scaleAction.IsIdentical() && rotateAction.IsIdentical();
	}

}