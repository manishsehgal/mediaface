import Actions.Action;
import Actions.ICommand;
import Actions.Property;

class Actions.TableTextAction extends Action implements ICommand {
	private var columnIndex:Number = null;
	private var text:Property;
	
	public function TableTextAction(unit : Unit, columnIndex:Number, oldText:String, newText:String) {
		super("Text typing", unit);
		this.columnIndex = columnIndex;
		text = new Property("Text", oldText, newText);
	}

	public function Undo() : Void {
		super.Undo();
		ChangeText(text.Old);
	}

	public function Redo() : Void {
		super.Redo();
		ChangeText(text.New);
	}
	
	private function ChangeText(Text:String) : Void {
		var unit = Target;
		unit.SetColumnText(columnIndex, Text);
	}

	public function Update(action : Action) : Void {
		if (action instanceof TableTextAction) {
			super.Update(action);
			var tableTextAction:TableTextAction = TableTextAction(action);
			text.New = tableTextAction.text.New;
		}
	}

	public function SetFocus() : Boolean {
		return true;
	}

	public function IsIdentical() : Boolean {
		return text.IsIdentical();
	}

}