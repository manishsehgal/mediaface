import Actions.Action;
import Actions.AlignAction;
import Actions.ICommand;

class Actions.TableColumnAlignAction extends AlignAction implements ICommand {
	private var id;
	
	public function TableColumnAlignAction(unit : MoveableUnit, id, oldAlign : String, newAlign : String) {
		super(unit, oldAlign, newAlign);
		this.id = id;
	}

	public function Undo() : Void {
		super.Undo();
		SetColumnAlign(id, align.Old);
	}

	public function Redo() : Void {
		super.Undo();
		SetColumnAlign(id, align.New);
	}
	
	private function SetColumnAlign(id, Align) : Void {
		var unit = Target;
		unit.SetColumnAlign(id, Align);
	}

	public function Update(action : Action) : Void {
		if (action instanceof TableColumnAlignAction) {
			super.Update(action);
			var alignAction:TableColumnAlignAction = TableColumnAlignAction(action);
			this.align.New = alignAction.align.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return super.IsIdentical();
	}

}