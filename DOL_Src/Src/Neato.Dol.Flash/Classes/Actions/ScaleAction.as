import Actions.Action;
import Actions.ICommand;
import Actions.Property;
import Actions.MoveableUnitAction;

class Actions.ScaleAction extends MoveableUnitAction implements ICommand {
	private var scaleX:Property;
	private var scaleY:Property;
	
	public function ScaleAction(unit : MoveableUnit, oldScaleX:Number, oldScaleY:Number, newScaleX:Number, newScaleY:Number) {
		super("Scale", unit);
		this.scaleX = new Property("ScaleX", oldScaleX, newScaleX);
		this.scaleY = new Property("ScaleY", oldScaleY, newScaleY);
	}

	public function Undo() : Void {
		super.Undo();
		Scale(scaleX.Old, scaleY.Old);
	}

	public function Redo() : Void {
		super.Undo();
		Scale(scaleX.New, scaleY.New);
	}
	
	private function Scale(ScaleX:Number, ScaleY:Number) : Void {
		var scaleXString:String = ScaleXProperty(Target);
		var scaleYString:String = ScaleYProperty(Target);
		var kx:Number = ScaleX / Target[scaleXString];
		var ky:Number = ScaleY / Target[scaleYString];
		Target.Resize(kx, ky);
	}

	public function Update(action : Action) : Void {
		if(action instanceof ScaleAction) {
			super.Update(action);
			var moveAction:ScaleAction = ScaleAction(action);
			this.scaleX.New = moveAction.scaleX.New;
			this.scaleY.New = moveAction.scaleY.New;
		}
	}

	public function SetFocus() : Boolean {
		return true;
	}

	public static function ScaleXProperty(unit : MoveableUnit):String {
		if (unit instanceof ShapeUnit)
			return "currentWidth";
		else if (unit instanceof TextEffectUnit)
			return "ScaleX";
		else if (unit instanceof TextUnit)
			return "multilineWidth";
		else if (unit instanceof TableUnit) {
			return "UnitWidth";
		}
		return "ScaleX";
	}

	public static function ScaleYProperty(unit : MoveableUnit):String {
		if (unit instanceof ShapeUnit)
			return "currentHeight";
//		else if (unit instanceof TextUnit)
//			return "multilineHeight";
		else if (unit instanceof TableUnit) {
			return "fontSize";
		}
		return "ScaleY";
	}
	
	public function IsIdentical():Boolean {
		return scaleX.IsIdentical() && scaleY.IsIdentical();
	}
}