import Actions.Action;
import Actions.ICommand;
import Actions.Property;

class Actions.TableColumnOrderAction extends Action implements ICommand {
	private var order:Property;
	private var item;
	
	public function TableColumnOrderAction(unit : Unit, item, oldOrder:Number, newOrder:Number) {
		super("Column Order", unit);
		this.item = item;
		order = new Property("Order", oldOrder, newOrder); 
	}

	public function Undo() : Void {
		super.Undo();
		SetColumnOrder(order.Old);
	}

	public function Redo() : Void {
		super.Redo();
		SetColumnOrder(order.New);
	}
	
	private function SetColumnOrder(Order:Number) : Void {
		var unit = Target;
		unit.SetColumnOrder(item, Order);
	}

	public function Update(action : Action) : Void {
		if (action instanceof TableColumnOrderAction) {
			super.Update(action);
			var tableColumnOrderAction:TableColumnOrderAction = TableColumnOrderAction(action);
			order.New = tableColumnOrderAction.order.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return order.IsIdentical();
	}

}