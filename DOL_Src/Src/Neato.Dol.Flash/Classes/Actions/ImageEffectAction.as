import Actions.Action;
import Actions.ICommand;
import Actions.Property;
import Actions.MoveableUnitAction;

class Actions.ImageEffectAction extends MoveableUnitAction implements ICommand {
	private var brightness:Property;
	private var contrast:Property;
	private var blursharp:Property;
	private var effect:Property;
	
	public function ImageEffectAction(unit : MoveableUnit, oldBrightness:Number, oldContrast:Number, oldBlursharp:Number, oldEffect:String, newBrightness:Number, newContrast:Number, newBlursharp:Number, newEffect:String) {
		super("Image effects", unit);
		brightness	= new Property("Brightness", oldBrightness, newBrightness); 
		contrast	= new Property("Contrast", oldContrast, newContrast); 
		blursharp	= new Property("Blursharp", oldBlursharp, newBlursharp); 
		effect		= new Property("Effect", oldEffect, newEffect); 
	}

	public function Undo() : Void {
		super.Undo();
		ApplyEffect(brightness.Old, contrast.Old, blursharp.Old, effect.Old);
	}

	public function Redo() : Void {
		super.Redo();
		ApplyEffect(brightness.New, contrast.New, blursharp.New, effect.New);
	}
	
	private function ApplyEffect(Brightness:Number, Contrast:Number, Blursharp:Number, Effect:String) : Void {
		_global.Project.CurrentUnit.ApplyEffect(Contrast, Brightness, Blursharp, Effect);
	}

	public function Update(action : Action) : Void {
		if (action instanceof ImageEffectAction) {
			super.Update();
			var imageEffectAction:ImageEffectAction = ImageEffectAction(action);
			this.brightness.New = imageEffectAction.brightness.New;
			this.contrast.New = imageEffectAction.contrast.New;
			this.blursharp.New = imageEffectAction.blursharp.New;
			this.effect.New = imageEffectAction.effect.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return brightness.IsIdentical() && contrast.IsIdentical() && blursharp.IsIdentical() && effect.IsIdentical();
	}

}