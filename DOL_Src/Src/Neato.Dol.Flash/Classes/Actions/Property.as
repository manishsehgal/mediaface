class Actions.Property {
	private var name:String;
	private var oldValue;
	private var newValue;
	
	public function Property(name:String, oldValue, newValue) {
		this.name = name;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}
	
	public function get Name():String {
		return name;
	}
	
	public function set Name(value:String):Void {
		name = value;
	}
	
	public function get Old() {
		return oldValue;
	}
	
	public function set Old(value):Void {
		oldValue = value;
	}
	
	public function get New() {
		return newValue;
	}
	
	public function set New(value):Void {
		newValue = value;
	}
	
	public function toString():String {
		return new String(name + " old = '" + Old + "' new = '" + New + "'");
	}
	
	public function Equal(property:Property):Boolean {
		return Name == property.Name && Old == property.Old && New == property.New;
	}
	
	public function IsIdentical() : Boolean {
		return New == Old;
	}
	
	public function Clone():Property {
		return new Property(Name, Old, New);
	}
}