import Actions.Action;
import Actions.MoveableUnitAction;
import Actions.ICommand;
import Actions.Property;

class Actions.KeepProportionsAction extends MoveableUnitAction implements ICommand {
	private var keepProportions:Property;
	
	public function KeepProportionsAction(unit : MoveableUnit, oldKeepProportions:Boolean, newKeepProportions:Boolean) {
		super("Keep proportions", unit);
		keepProportions = new Property("KeepProportions", oldKeepProportions, newKeepProportions);
	}

	public function Undo() : Void {
		super.Undo();
		KeepProportions(keepProportions.Old);
	}

	public function Redo() : Void {
		super.Redo();
		KeepProportions(keepProportions.New);
	}
	
	private function KeepProportions(kp:Boolean) : Void {
		Target.KeepProportions = kp;
	}

	public function Update(action : Action) : Void {
		if (action instanceof KeepProportionsAction) {
			super.Update();
			var keepProportionsAction:KeepProportionsAction = KeepProportionsAction(action);
			this.keepProportions.New = keepProportionsAction.keepProportions.New;
		}
	}

	public function SetFocus() : Boolean {
		return false;
	}

	public function IsIdentical() : Boolean {
		return keepProportions.IsIdentical();
	}

}