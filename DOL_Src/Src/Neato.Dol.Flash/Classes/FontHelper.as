﻿class FontHelper {
	private var names:Array;
	private var urls:Object;
	private var ttfnames:Array;
	private var fontRanges:Array;
	private var defaultFontKey:String;
	private var fontTargets:Object;
	//private var smallFontUrl:String;
	public function FontHelper(fontsXml:XML) {
		trace("FontHelper");
		defaultFontKey = CreateKey("Arial", false, false);
		 
		names = new Array();
		fontRanges = new Array();
		ttfnames = new Array();
		urls = new Object();
		fontTargets = new Object();
		var fontName:String;
		var fontDesc:String;
		var rangeFontName:String;
		//smallFontUrl = fontsXml.firstChild.attributes.smallFontUrl;
		for (var itemNode:XMLNode = fontsXml.firstChild.firstChild; itemNode != null; itemNode = itemNode.nextSibling) {
			fontName = itemNode.attributes.name;
			fontDesc = itemNode.attributes.desc;
			names.push({data:String(fontName), label:String(fontDesc)});
			//////////////////////////////////////////////////////////
			if (fontName.lastIndexOf("_") != -1) {
				rangeFontName = fontName.slice(0,fontName.lastIndexOf("_"));
			} else {
				rangeFontName = fontName;
			}
			var codeRanges:Array = new Array();
			codeRanges = itemNode.attributes.codes.split(",");			
			fontRanges.push([rangeFontName, [itemNode.attributes.range, codeRanges]]);
			//////////////////////////////////////////////////////////
			for (var styleNode:XMLNode = itemNode.firstChild; styleNode != null; styleNode = styleNode.nextSibling) {
				var italic:Boolean = false;
				var bold:Boolean = false;
				switch (styleNode.nodeName) {
					case "italic" :
						italic = true;
						break;
					case "bold" :
						bold = true;
						break;
					case "bold-italic" :
						italic = true;
						bold = true;
						break;
					case "normal" :
					default :
						italic = false;
						bold = false;
						break;
				}
				urls[CreateKey(fontName, italic, bold)] = styleNode.attributes.url;
				ttfnames[CreateKey(fontName,italic,bold)] = styleNode.attributes.ttf;
			}
		}
	}
	
	private function GetFontsMC():MovieClip {
		if (_root.fonts_mc == undefined) {
			_root.createEmptyMovieClip("fonts_mc", _root.getNextHighestDepth());
			_root.fonts_mc._y = 200;
			_root.fonts_mc._visible = false;
		}
		
		return _root.fonts_mc;
	}
	
	private function CreateKey(fontId:String, italic:Boolean, bold:Boolean):String {
		return fontId.toLowerCase()+"_"+italic.toString()+"_"+bold.toString();
	}
	public function GetFontsArray():Array {
		return names;
	}
	public function GetRangesArray() : Array {
		return fontRanges;	
	}
	public function GetTTFFileName(key:String):String {
		var res:String = ttfnames[key];
		var inx:Number = res.lastIndexOf("/");
		if(inx==-1)
			return res;
		else
			return res.substring(inx+1);
			
	}
	public function TestFont(fontId:String, italic:Boolean, bold:Boolean):Boolean {
		var key:String = CreateKey(fontId, italic, bold);
		if (key == defaultFontKey)
			return true;
		return urls[key] != undefined;
	}
	public function TestLoadedFont(fontFormat:TextFormat):Boolean {
		var key:String = CreateKey(fontFormat.font, fontFormat.italic, fontFormat.bold);
		if (key == defaultFontKey)
			return true;
		return GetFontsMC()[key]._width > 0;
	}
	/*public function TestLoadedSmallFont(){
		return (GetFontsMC()["smallFont"]._width > 100 && GetFontsMC()["smallFont"]._height > 10);
	}
	public function LoadSmallFont()
	{
		var key:String = "smallFont";
		urls[key]=smallFontUrl;
		if (urls[key] == undefined)
			return;
		if (GetFontsMC()[key]._width > 0)
			return;
			
		var mcFont:MovieClip = GetFontsMC()[key];
		if (!mcFont) {
			Preloader.Show();
			var i:Number = GetFontsMC().getNextHighestDepth();
			mcFont = GetFontsMC().createEmptyMovieClip(key, i);
			var url:String = urls[key];
			mcFont.loadMovie(url);
		}
		var intervalObject = new Object();
		var interval:Number = setInterval(checkLoaded, 500, mcFont, key, intervalObject);
		intervalObject["interval"] = interval;
	}*/
	public function LoadFont(fontFormat:TextFormat, target:Object):Void {
		var key:String = CreateKey(fontFormat.font, fontFormat.italic, fontFormat.bold);
		if (urls[key] == undefined)
			return;
		if (GetFontsMC()[key]._width > 0)
			return;
		if (key == defaultFontKey)
			return;
		
		if (fontTargets[key] == undefined)
			fontTargets[key] = new Array();
		var targets:Array = fontTargets[key];
		var found:Boolean = false;
		for(var k:String in targets) {
			if(targets[k] == target) {
				found = true;
				break;
			};
		};
		
		if (!found)
			targets.push(target);
		
		var mcFont:MovieClip = GetFontsMC()[key];
		if (!mcFont) {
			//_global.tr("!!! LoadFont key="+key);
			Preloader.Show();
			var i:Number = GetFontsMC().getNextHighestDepth();
			mcFont = GetFontsMC().createEmptyMovieClip(key, i);
			var url:String = urls[key];
			mcFont.loadMovie(url);
			var intervalObject:Object = new Object();
			var interval:Number = setInterval(this, "checkLoaded", 500, mcFont, key, intervalObject);
			intervalObject["interval"] = interval;
		}
	}
	
	private function checkLoaded(mcFont:MovieClip, key:String, intervalObject:Object) :Void {
		//_global.tr("!!! checkLoaded key="+key);
		if (mcFont._width > 0) {
			Preloader.Hide();
			clearInterval(intervalObject.interval);
			//trace("loaded font key="+key);
			//_global.tr("### checkLoaded loaded key="+key);
			for(var i:String in fontTargets[key]) {
				var target:Object = fontTargets[key][i];
				target.Draw();
			}
			fontTargets[key].length = 0;
			delete fontTargets[key];
		} else 
			trace("loading font key="+key+", intervalObject="+intervalObject.interval);
	}
	
	public function RangeFromName ( RangeName:String ) :String {
		var RangeConvert:String = "";		
		if (RangeName == "Basic Latin") {
			RangeConvert = "";
		} else if (RangeName == "Currency") {
			RangeConvert = "_Curr";
		} else if (RangeName == "Punctuation") {
			RangeConvert = "_Punct";
		}		
		return RangeConvert;
	}
	public function NameFromRange ( RangeName:String ) :String {
		var RangeConvert:String = "Basic Latin";		
		if (RangeName == "") {
			RangeConvert = "Basic Latin";
		} else if (RangeName == "_Curr") {
			RangeConvert = "Currency";
		} else if (RangeName == "_Punct") {
			RangeConvert = "Punctuation";
		}		
		return RangeConvert;
	}
	
	public function FontRangeDetect ( charCode:Number, fontName:String ) :String {
		var strFontNameReturn:String = fontName;
		if (fontName.lastIndexOf("_") != -1) {
			fontName = fontName.slice(0,fontName.lastIndexOf("_"));				
		}
		for (var i:Number = 0; i<fontRanges.length; i++ ) {
			var strFontBaseName:String = fontRanges[i][0];
			if (strFontBaseName.lastIndexOf("_") != -1) {
				strFontBaseName = strFontBaseName.slice(0,strFontBaseName.lastIndexOf("_"));				
			}			
			if (strFontBaseName == fontName) {	
				for (var j:Number = 0; j<fontRanges[i][1][1].length; j++) {
					if (fontRanges[i][1][1][j] == charCode.toString()) {
						strFontNameReturn = fontRanges[i][0] + RangeFromName(fontRanges[i][1][0]);
					}
				}					
			}
		}
		return strFontNameReturn;
	}
	
	public function TestFontFormat(fontFormat:TextFormat, target:Object):Void {
		if (!TestFont(fontFormat.font, false, true))
			fontFormat.bold = false;
		if (!TestFont(fontFormat.font, true, false))
			fontFormat.italic = false;
		if (!TestLoadedFont(fontFormat))
			LoadFont(fontFormat, target);
	}
}
