﻿import mx.core.UIObject;
import mx.controls.CheckBox;
import mx.controls.ComboBox;
import mx.controls.Label;
import mx.controls.RadioButton;
import mx.utils.Delegate;
import CtrlLib.ButtonEx;
import CtrlLib.SliderEx;
[Event("exit")]
class ImageEffectProperties extends UIObject {
	private var lblImageEffects:Label;
	private var btnDelete:ButtonEx;
	private var btnApply:ButtonEx;
	private var btnBack:ButtonEx;
	private var sldBlurSharp:SliderEx;
	private var sldContrast:SliderEx;
	private var sldBrightness:SliderEx;
	private var btnNormal,btnGrey,btnSepia:RadioButton;
	function ImageEffectProperties() {
		setStyle("styleName", "ToolPropertiesPanel");
		this.btnDelete.setStyle("styleName", "ToolPropertiesActiveButton");
		this.lblImageEffects.setStyle("styleName", "ToolPropertiesCaption");
		/*this.cbFont.setStyle("styleName", "ToolPropertiesCombo");
		this.cbFontSize.setStyle("styleName", "ToolPropertiesCombo");
		this.txtText.setStyle("styleName", "ToolPropertiesInputText");
		this.chkBold.setStyle("styleName", "ToolPropertiesCheckBoxBold");
		this.chkItalic.setStyle("styleName", "ToolPropertiesCheckBox");
		/*this.btnAlignLeft.setStyle("styleName", "ToolPropertiesCheckBox");
		this.btnAlignCenter.setStyle("styleName", "ToolPropertiesCheckBox");
		this.btnAlignRight.setStyle("styleName", "ToolPropertiesCheckBox");*/
/*		this.btnDelete.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnTextEffect.setStyle("styleName", "ToolPropertiesActiveButton");
		this.lblEditText.setStyle("styleName", "ToolPropertiesCaption");*/
	}
	
	function onLoad() {
		btnApply.RegisterOnClickHandler(this,OnApply);
		btnBack.RegisterOnClickHandler(this,OnBack);
		btnDelete.RegisterOnClickHandler(this, _global.DeleteCurrentUnit);
		_global.LocalHelper.LocalizeInstance(sldBlurSharp,"ToolProperties","IDS_LBLBLURSHARP");
		_global.LocalHelper.LocalizeInstance(sldContrast,"ToolProperties","IDS_LBLCONTRAST");
		_global.LocalHelper.LocalizeInstance(sldBrightness,"ToolProperties","IDS_LBLBRIGHTNESS");
		_global.LocalHelper.LocalizeInstance(lblImageEffects,"ToolProperties","IDS_LBLIMAGEEFFECTS");
		_global.LocalHelper.LocalizeInstance(btnNormal,"ToolProperties","IDS_LBLIMAGENORMAL");
		_global.LocalHelper.LocalizeInstance(btnGrey,"ToolProperties","IDS_LBLIMAGEGREY");
		_global.LocalHelper.LocalizeInstance(btnSepia,"ToolProperties","IDS_LBLIMAGESEPIA");
		_global.LocalHelper.LocalizeInstance(btnApply,"ToolProperties","IDS_BTNAPPLY");
		_global.LocalHelper.LocalizeInstance(btnBack,"ToolProperties","IDS_BTNBACK");
		
		TooltipHelper.SetTooltip2(btnApply, "IDS_TOOLTIP_APPLYIMAGEEFFECT");

		trace("sldBlurSharp.text "+sldBlurSharp.text);
		
	/*	sldBlurSharp.text = "Blur <-> Sharp";
		sldContrast.text = "Contrast";
		sldBrightness.text = "Brightness";*/
		sldBrightness.Max = 10;
		sldContrast.Max = 10;
		sldBlurSharp.Max = 10;
		sldBrightness.Min = -10;
		sldContrast.Min = -10;
		sldBlurSharp.Min = -10;
		sldBrightness.Step = 1;
		sldContrast.Step = 1;
		sldBlurSharp.Step = 1;
	}

	//region back event
	private function OnBack() {
		var eventObject = {type:"back", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnBackHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("back", Delegate.create(scopeObject, callBackFunction));
    }
	function OnApply(){
		var brightness:Number = sldBrightness.Points; 
		var contrast:Number = sldContrast.Points;
		var blursharp:Number = sldBlurSharp.Points;
		var effect:String = "Normal";
		if(btnGrey.selected)
			effect = "Grey";
		else if(btnSepia.selected)
			effect = "Sepia";
		else
			effect = "Normal";
		_global.CurrentUnit.ApplyEffect(contrast,brightness,blursharp,effect);
	}
	function DataBind(){
		var settings:Object = _global.CurrentUnit.GetEffect();
		sldBlurSharp.Points = parseInt(settings.blursharp);
		sldContrast.Points = parseInt(settings.contrast);
		sldBrightness.Points = parseInt(settings.brightness);
		if(settings.effect == "Normal")
			btnNormal.selected = true;
		else if(settings.effect == "Grey")
			btnGrey.selected = true;
		else if(settings.effect == "Sepia")
			btnSepia.selected = true;
	}
	//endregion
	
}
