﻿class PaintHelper {
	private var brushes:Array;
	private var defaultSize:Number = 5;
	
	public function PaintHelper(brushesXml:XML) {
		brushes = new Array();
		
		for (var itemNode:XMLNode = brushesXml.firstChild.firstChild; itemNode != null; itemNode = itemNode.nextSibling) {
			
			var brush = CreateBrushFromXml(itemNode);
			brushes.push(brush);
		}
	}
	
	public static function CreateBrushFromXml(itemNode:XMLNode):Object {
		var brush = new Object(); //replace with Brush object?
		brush.type = "brush";
		brush.group = itemNode.attributes.group;
		brush.name = itemNode.attributes.name; //.toLowerCase();
		brush.step = Number(itemNode.attributes.step);
		brush.isDefault = Boolean(itemNode.attributes.isDefault); //.toLowerCase();
		brush.mode = itemNode.attributes.mode; //optional
		brush.filled = Boolean(itemNode.attributes.filled); //optional
		brush.tipID = "IDS_BRUSH_" + itemNode.attributes.group;
		brush.originalXmlNode = itemNode;
		if (brush.step<=0) brush.step = 1;
		var drawSequence = new Array();
		for (var i = 0; i < itemNode.childNodes.length; ++i) {
			var drawNode:XML = itemNode.childNodes[i];
			drawSequence.push(Logic.DrawingHelper.CreatePrimitive(drawNode));
		}
		brush.drawSequence = drawSequence;
		return brush;
	}
	
	private function GetBrush(paintType:String):Object {
		if (brushes.length==0) {
			trace("ERROR no brushes loaded");
			var drawSequence = new Array();
			var xml = new XML();
			xml.parseXML('<Circle x="0" y="0" r="3"/>');
			drawSequence.push(new CirclePrimitive(xml));
			var brush = new Object({drawSequence:drawSequence, group:"Default", name:"Default", step:3});
		}
		for (var i = 0; i < brushes.length; ++i) {
			if (brushes[i].name == paintType) return brushes[i];
		}
		trace("ERROR: cannot find brush of type " + paintType);
		return brushes[1];
	}
	public function GetBrushesArray():Array {
		return brushes;
	}
	public function GetBrushDefaultsArray():Array {
		var defaultBrushes = new Array();
		for (var b = 0; b < brushes.length; ++b) {
			if (!BrushArrayHasGroup(defaultBrushes, brushes[b].group)) {
				var groupBrashes:Array = GetBrushesArrayByGroup(brushes[b].group);
				var defaultBrash:Object = groupBrashes[0];
				trace("defaultBrash group = " + defaultBrash.group);
				trace("defaultBrash isDefault = " + defaultBrash.isDefault);
				trace("defaultBrash step = " + defaultBrash.step);
				for (var index = 0; index < groupBrashes.length; index++) {
					if (groupBrashes[index].isDefault == true) {
						defaultBrash = groupBrashes[index];
						trace("defaultBrash isDefault = " + defaultBrash.isDefault);
						trace("defaultBrash step = " + defaultBrash.step);
						break;
					}
				}
				defaultBrushes.push(defaultBrash);
			}
		}
		return defaultBrushes;
	}
	private function BrushArrayHasGroup(brushArray:Array, group:String):Boolean {
		for (var b = 0; b < brushArray.length; ++b) {
			if (brushArray[b].group == group) return true;
		}
		return false;
	}
	
	public function GetBrushesArrayByGroup(group:String):Array {
		var brushesByGroup = new Array();
		for (var b = 0; b < brushes.length; ++b) {
			if (brushes[b].group == group) brushesByGroup.push(brushes[b]);
		}
		return brushesByGroup;
	}
}
