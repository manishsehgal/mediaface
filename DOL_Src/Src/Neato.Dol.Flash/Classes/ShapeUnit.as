﻿class ShapeUnit extends MoveableUnit {
	private var shapeType:String;
	private var borderWidth:Number = 1;
	private var borderColor:Number = 0;
	private var fillColor:Number = 0x0066FF;
	private var fillColor2:Number = 0x660033;
	private var transparency:Boolean = false;

	private var fillType:String = "gradient";
	private var gradientType:String = "00";

	public var currentWidth:Number=100;
	public var currentHeight:Number=100;
	
	private var isNonScalable:Boolean=false;
	private var width:Number=100;
	private var height:Number=100;
	private var maxSize:Number=100;
	
	private var shape:Object;
	
	private var fillImageId:String = null;
	private var fillScaleX:Number = 0;
	private var fillScaleY:Number = 0;
	
	public function get FillImageId():String {
		return fillImageId;
	}
	
	public function get FillImageUrl():String {
		return fillImageId == null ? null : _global.ProjectImageUrlFormat + fillImageId;
	}
	
	public function set FillImageId(value:String):Void {
		fillImageId = value;
	}
	
	function ShapeUnit(mc:MovieClip, node:XML, shapeType:String, size:Number) {
		super(mc, node);
		this.shapeType = shapeType;
		shape = _global.Shapes.GetShape(this.shapeType);
		transparency = shape.transparency;
		
		if (shape.width > shape.height) {
			currentWidth = size;
			currentHeight = shape.height / (shape.width / currentWidth);
		} else {
			currentHeight = size;
			currentWidth = shape.width / (shape.height / currentHeight);
		}
		maxSize = size;
		
		if(!isNaN(shape.fillColor))
			fillColor = shape.fillColor;
		if(!isNaN(shape.fillColor2))
			fillColor2 = shape.fillColor2;
		if(shape.gradientType != undefined)
			gradientType = shape.gradientType;
		if(!isNaN(shape.borderColor))
			borderColor = shape.borderColor;
		if(!isNaN(shape.borderWidth))
			borderWidth = shape.borderWidth;
			
		if (node != null) {
			this.shapeType = node.attributes.type;
			currentWidth = Number(node.attributes.currentWidth);
			currentHeight = Number(node.attributes.currentHeight);
			if (node.attributes.maxSize != undefined) {
				maxSize = Number(node.attributes.maxSize);
			}
			for (var i:Number = 0; i < node.childNodes.length; ++i) {
				var childNode:XML = node.childNodes[i];
				switch(childNode.nodeName) {
					case "Fill":
						fillType = new String(childNode.attributes.type);
						fillColor = parseInt(childNode.attributes.color.substr(1), 16);
						fillColor2 = parseInt(childNode.attributes.color2.substr(1), 16);
						gradientType = childNode.attributes.gradientType == undefined ? "00" : childNode.attributes.gradientType;
						transparency = childNode.attributes.transparency.toString() == "true" ? true : false;						
						if (childNode.attributes.fillImageId != undefined)
							fillImageId = childNode.attributes.fillImageId;
					break;
					case "Border":
						borderWidth=Number(childNode.attributes.width);
						borderColor=parseInt(childNode.attributes.color.substr(1), 16);
					break;
					case "Type":
						shape = _global.Shapes.CreateShapeFromXml(childNode);
					default:
					break;
				}
			}
		}
		width = Number(shape.width);
		height = Number(shape.height);
		isNonScalable = shape.isNonScalable;
		KeepProportions = true;
		mc.cacheAsBitmap = true;
	}

	function GetXmlNode():XMLNode {
		var node:XMLNode = super.GetXmlNode();
		node.nodeName = "Shape";
		node.attributes.type=shapeType;
		node.attributes.currentWidth=currentWidth;
		node.attributes.currentHeight=currentHeight;
		node.attributes.maxSize=maxSize;

		node.appendChild(shape.originalXmlNode.cloneNode(true));
		
		var fillNode:XMLNode = new XMLNode(1, "Fill");
		fillNode.attributes.type=fillType;
		fillNode.attributes.color=_global.GetColorForXML(fillColor);
		fillNode.attributes.color2=_global.GetColorForXML(fillColor2);
		fillNode.attributes.gradientType = gradientType;
		fillNode.attributes.transparency=transparency;
		fillNode.attributes.fillImageId = fillImageId;
		fillNode.attributes.fillScaleX = fillScaleX;
		fillNode.attributes.fillScaleY = fillScaleY;
		
		node.appendChild(fillNode);
		
		var borderNode:XMLNode = new XMLNode(1, "Border");
		borderNode.attributes.width=borderWidth;
		borderNode.attributes.color=_global.GetColorForXML(borderColor);
		node.appendChild(borderNode);
		
		node.appendChild(super.GetXmlNodeRotate());
		return node;
	}
	function Draw(eventObject:Object):Void {
		eventObject.target.UnregisterOnImageAttachedHandler(this);
		trace ("ShapeUnit:Draw");
		DrawMC(mc);
	}
	
	function DrawMC(mc:MovieClip):Void {
		mc.clear();
		if (transparency)
			mc.beginFill(0, 0);
		else
			mc.beginFill(fillColor, 100);
		
		mc.lineStyle(borderWidth > 0 ? borderWidth : 0.01, borderColor, borderWidth > 0 ? 100 : 0);
		for (var i:Number = 0; i < shape.drawSequence.length; ++i) {
			var primitive:IDraw = shape.drawSequence[i];
			primitive.Draw(mc, 0, 0, 0, currentWidth / width, currentHeight / height);
		}
		mc.endFill();
		
		var bounds:Object = mc.getBounds();
		var dX:Number = -bounds.xMin / (currentWidth  / width);
		var dY:Number = -bounds.yMin / (currentHeight / height);
		var cW:Number = bounds.xMax - bounds.xMin;
		var cH:Number = bounds.yMax - bounds.yMin;
		
		mc.clear();
		if (transparency) {
//			_global.tr("### ShapeUnit.Draw() trans = " + transparency);
			mc.beginFill(0, 0);
			mc.lineStyle(borderWidth > 0 ? borderWidth : 0.01, borderColor, borderWidth > 0 ? 100 : 0);
			for (var i:Number = 0; i < shape.drawSequence.length; ++i) {
				var primitive:IDraw = shape.drawSequence[i];
				primitive.Draw(mc, dX, dY, 0, currentWidth / width, currentHeight / height);
			}
			mc.endFill();
		} else {
//			_global.tr("### ShapeUnit.Draw() FillType = '" + FillType + "'" + " GradientType = '" + GradientType + "'");
			if (FillType == "gradient") {
				var gradient:Object = _global.Gradients.GetGradient(GradientType);
//				_global.tr("### gradient = " + gradient + " n = " + gradient.name);
				if (gradient != null && gradient.name != "00") {
//					_global.tr("### ShapeUnit.Draw() gr != null");
					gradient.color1 = FillColor;
					gradient.color2 = FillColor2;
					mc.color  = gradient.color1;
					mc.color2 = gradient.color2;

					var pr:GradientRectPrimitive = new GradientRectPrimitive(gradient.drawSequence[0]);
					pr.Primitives = shape.drawSequence;

					fillScaleX = cW / pr.Width;
					fillScaleY = cH / pr.Height;
					pr.ScaleX = fillScaleX;
					pr.ScaleY = fillScaleY;
					
					mc.lineStyle(borderWidth > 0 ? borderWidth : 0.01, borderColor, borderWidth > 0 ? 100 : 0);
					pr.Draw(mc, dX, dY, 0, currentWidth / width, currentHeight / height);
				}
				else {
					mc.beginFill(fillColor, 100);
					mc.lineStyle(borderWidth > 0 ? borderWidth : 0.01, borderColor, borderWidth > 0 ? 100 : 0);
					for (var i:Number = 0; i < shape.drawSequence.length; ++i) {
						var primitive:IDraw = shape.drawSequence[i];
						primitive.Draw(mc, dX, dY, 0, currentWidth / width, currentHeight / height);
					}
					mc.endFill();
				}
			}
			else if (FillType == "image") {
				var image = _global.UIController.ImageStorage.FindImageByUrl(FillImageUrl);
				if (image != null && image.IsLoaded) {
					var bmp:flash.display.BitmapData= image.ImageBitmap;
//					_global.tr("### ShapeUnit.Draw() FillType == image = " + image + " : " + bmp);
	
					var m:flash.geom.Matrix = new flash.geom.Matrix();
					fillScaleX = cW / bmp.width;
					fillScaleY = cH / bmp.height;
					m.scale(fillScaleX, fillScaleY);
					mc.beginBitmapFill(bmp, m, false);
					mc.lineStyle(borderWidth > 0 ? borderWidth : 0.01, borderColor, borderWidth > 0 ? 100 : 0);
					for (var i:Number = 0; i < shape.drawSequence.length; ++i) {
						var primitive:IDraw = shape.drawSequence[i];
						primitive.Draw(mc, dX, dY, 0, currentWidth / width, currentHeight / height);
					}
					mc.endFill();
				}
				else {
					_global.UIController.ImageStorage.LoadImage(mc, FillImageUrl, false, true, this, Draw, true);
				}
			}
		}
	}
	
	function Fill(color:Number, color2:Number, gradType:String):Void {
//		_global.tr("### ShapeUnit.Fill() color = " + color + " color2 = " + color2 + " gradType = " + gradType);
		FillColor = color;
		FillColor2 = color2;
		FillType = "gradient";
		GradientType = gradType;
		transparency = false;
		Draw();
	}

	function getMode():String {
		trace ("ShapeUnit:GetMode");
		return _global.ShapeEditMode;
	}
	
	function set BorderColor(value:Number):Void {
		borderColor = value;
		Draw();
	}
	function get BorderColor():Number {
		return borderColor;
	}
	function set BorderWidth(value:Number):Void {
		borderWidth = value;
		Draw();
	}
	function get BorderWidth():Number {
		return Math.min(borderWidth, MaxBorderWidth);
	}
	function get FillColor():Number {
		return fillColor;
	}
	function set FillColor(value:Number):Void {
		fillColor = value;
	}
	function get FillColor2():Number {
		return fillColor2;
	}
	function set FillColor2(value:Number):Void {
		fillColor2 = value;
	}
	function get GradientType():String {
		if (fillType != "gradient")
			gradientType = "00";
		return gradientType;
	}
	function set GradientType(value:String):Void {
		gradientType = value;
	}
	function get FillType():String {
		return fillType;
	}
	function set FillType(value:String):Void {
		fillType = value;
	}
	function get Transparency():Boolean{
		return transparency;
	}
	function set Transparency(value:Boolean):Void{
		transparency = value;
		Draw();
	}
	function set IsNonScalable(value:Boolean):Void {
		isNonScalable = value;
	}
	function get IsNonScalable():Boolean {
		return isNonScalable;
	}
	function RestoreProportions():Void	{
		//currentWidth = width;
		//currentHeight = height;
		if (width > height) {
			currentWidth = maxSize;
			currentHeight = height / (width / currentWidth);
		} else {
			currentHeight = maxSize;
			currentWidth = width / (height / currentHeight);
		}
		Draw();
	}
	function get MaxBorderWidth():Number {
		return Math.floor(Math.min(currentWidth,currentHeight)/2);
	}

	private function Resize(kx:Number, ky:Number) : Void {
		var borderWidth:Number = BorderWidth;

		var newWidth:Number = currentWidth * kx;
		var newHeight:Number = currentHeight * ky;

		if (newHeight <= borderWidth * 2 || newWidth <= borderWidth * 2) {
			newWidth = currentWidth;
			newHeight = currentHeight;
		}

		this.ScaleX = 100;
		this.ScaleY = 100;
	
		this.currentWidth = newWidth;
		this.currentHeight = newHeight;
		this.Draw();
	}
}
