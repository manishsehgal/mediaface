﻿import mx.controls.Alert;
class MsgBox.MessageBox {
	private static var alertCallbackOk:Function;
	private static var alertCallbackCancel:Function;
	private static var bInited:Boolean = false;
	public static function init() {
		if(!bInited){
			Alert.messageStyleDeclaration = _global.styles.MessageBoxText;
			Alert.titleStyleDeclaration = _global.styles.MessageBoxTitle;
			Alert.buttonStyleDeclaration = _global.styles.MessageBoxButton;
			_global.styles.Alert.setStyle("styleName", "MessageBox");
			_global.styles.Alert.setStyle("borderStyle", "default");
			bInited = true;
		}	
		

	}
	private static function Confirm(message:String, title:String, callbckOk:Function, callbckCancel:Function ) {
		init();
		alertCallbackOk = callbckOk;
		alertCallbackCancel = callbckCancel;
		Alert.show(message, title, Alert.OK | Alert.CANCEL, _root,alertEvent);
	}
	private static function alertEvent(eventObject) {
		if(eventObject.detail == Alert.OK || eventObject.detail == Alert.YES) {
			alertCallbackOk();
		} else {
			alertCallbackCancel();
		}
		alertCallbackOk = alertCallbackCancel = null;
	}

	private static function Alert(message:String, title:String, callbckOk:Function ) {
		init();
		alertCallbackOk = callbckOk;
		Alert.show(message, " "+title, Alert.OK, _root, alertEvent);
	}
}