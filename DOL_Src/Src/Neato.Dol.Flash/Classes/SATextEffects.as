﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;
[Event("onLoaded")]
class SATextEffects {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SATextEffects.prototype);

	private var effects:XML;
	
	public static function GetInstance(effects:XML):SATextEffects {
		return new SATextEffects(effects);
	}
	
	public function get IsLoaded():Boolean {
		return effects.loaded;
	}
	
	private function SATextEffects(effects:XML) {
		var parent = this;
		
		this.effects = effects;
		this.effects.onLoad = function(success) {
			var eventObject = {type:"onLoaded", target:parent};
			parent.dispatchEvent(eventObject);
		};
	}

	public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }

	public function BeginLoad() {
		var currentDate:Date = new Date();
		var url:String = "TextEffects.xml";
		if (SALocalWork.IsLocalWork)
			url = "../Neato.Dol.WebDesigner/TextEffects.xml";
		BrowserHelper.InvokePageScript("log", url);
		this.effects.load(url);
		BrowserHelper.InvokePageScript("log", "SATextEffects.as: types loaded");
    }
}