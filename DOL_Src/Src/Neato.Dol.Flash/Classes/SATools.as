﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;
[Event("onLoaded")]
class SATools {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SATools.prototype);

	private var tools:XML;
	
	public static function GetInstance(tools:XML):SATools {
		return new SATools(tools);
	}
	
	public function get IsLoaded():Boolean {
		return tools.loaded;
	}
	
	private function SATools(tools:XML) {
		var parent = this;
		
		this.tools = tools;
		this.tools.onLoad = function(success) {
			var eventObject = {type:"onLoaded", target:parent};
			trace("SAtools: dispatch tools onLoaded");
			parent.dispatchEvent(eventObject);
		};
	}

	public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }

	public function BeginLoad() {
		var currentDate:Date = new Date();
		var url:String = "ToolItems.xml";
		if (SALocalWork.IsLocalWork)
			url = "../Neato.Dol.WebDesigner/ToolItems.xml";
		trace("SAtools before load:"+url);
		BrowserHelper.InvokePageScript("log", url);
		
		this.tools.load(url);
		trace("SAtools after load:"+url);
		BrowserHelper.InvokePageScript("log", "SATools.as: types loaded");
    }
}