﻿
import mx.utils.Delegate;
import mx.events.EventDispatcher;
import mx.core.UIObject;
import Geometry.GeometryHelper;

[event("onRedraw")]
class TextEffect extends UIObject {
	
	private var pluginsService;
	public var effect;
	public var mc:MovieClip;
	public var text:String;
	public var color:Number;
	public var bold:Boolean;
	public var italic:Boolean;
	public var underline:Boolean;
	public var align:String;
	public var contourNode:XMLNode;
	public var bChanged:Boolean;
	public var fontTTF:String;
	public var size:Number;
	private var owner;
	private var needFontName:String;
	private var saFont:SAFont;
	private var fontFile:XML;

	public static var asyncRunningCount:Number = 0;
	private var asyncCounterRegistered:Boolean;
	private static var eventDispatcher:EventDispatcher = new EventDispatcher();
	
	function TextEffect(mc:MovieClip,owner){
		
		pluginsService = SAPlugins.GetInstance();
		pluginsService.RegisterOnInvokedParsedHandler(this, onResponse);
		pluginsService.RegisterOnModuleAvaliableHandler(this, IsTextEffectModuleAvaliable);
		needFontName = "";
		this.mc = mc;
		this.owner = owner;
		bold = false;
		italic = false;
		underline = false;
		textString = " ";
		contourNode = null;
		bChanged = true;
		fontTTF = "SCHLBKB.TTF";
		size = 12;
	}
	
	function Draw():Void {
		trace("====TextEffect.draw begin");

		IncAcyncCount();
		if ((owner.bChanged == false && owner.contour != null) || SAPlugins.IsAvailable() == false)
		{			
			mc.clear();
			DrawByNode(owner.contour);
			//_global.SelectionFrame.RedrawUnit();		
			//_global.SelectionFrame.AttachUnit(_global.Project.CurrentUnit);
			return;
		}
		//pluginsService.IsModuleAvailable("TextEffectModule");
 		var intervalObject:Object = new Object();
 		var interval:Number = setInterval(Delegate.create(this, IsTextEffectModuleAvaliable), 0, {isAvaliable:true}, intervalObject);
 		intervalObject["interval"] = interval;
	}

	function RegisterOnConvertHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("onConvert", Delegate.create(scopeObject, callBackFunction));
	}
	
	function Convert() : Void {
		_global.tr("TextEffect.Convert()");
		var eventObject:Object = {type:"onConvert", target:this};
		this.dispatchEvent(eventObject);
	}
	
	function IsTextEffectModuleAvaliable(eventObject:Object, intervalObject:Object) : Void {
 		if (intervalObject != undefined)
 			clearInterval(intervalObject.interval);
		/*if ((owner.bChanged == false && owner.contour != null )
			|| (eventObject.isAvaliable.toString() == 'false' && owner.contour != null))
		{
			mc.clear();
			DrawByNode(owner.contour);
			return;
		}*/
		
		mc.clear();
		if (!eventObject.isAvaliable)
		{
			Convert();
			DecAcyncCount();
			return;
		}
		
		/*trace("====Plugin avaible");
		mc.clear();
		
		var cmd:XML = new XML();
		var CmdInfo:XMLNode = cmd.createElement("CmdInfo");
		CmdInfo.attributes.Type = "Request";
		cmd.appendChild(CmdInfo);
		
		var Request:XMLNode = cmd.createElement("Request");
		Request.attributes.Name = "TEXTEFFECT_GET_CONTOUR";
		CmdInfo.appendChild(Request);
		
		var Params:XMLNode = cmd.createElement("Params");
		Request.appendChild(Params);
		
		var Text:XMLNode = cmd.createElement("Text");
		var TextValue:XMLNode = cmd.createTextNode(text);
		Text.appendChild(TextValue);
		//Text.attributes.text = text;
		trace("====TextEffect.as:"+text);
		var Style:XMLNode = cmd.createElement("Style");
		Style.attributes.font = "SCHLBKB.TTF";
		Style.attributes.bold = bold == true ? 1 : 0;
		Style.attributes.italic = italic == true ? 1 : 0;
		Text.appendChild(Style);
		
		Params.appendChild(Text.cloneNode(true));
		
		var Desc:XMLNode = cmd.createElement("EffectDesc");
		Desc.attributes.Type = "Curved";
		var guides:String = effect.EncodeGuideLines(effect);
		trace("====Encoded guides:"+guides);
		var DescText:XMLNode = cmd.createTextNode(guides);//"A:0:50:300:200.57:600:50:A:0:300:300:360:600:300:");
		Desc.appendChild(DescText);
		Params.appendChild(Desc);
		
		//if(contourNode==null || guides!=lastGuides || guides == null || lastText != text)

		//if(owner.bChanged)
		//{
			//lastGuides = guides;
			//lastText = text;
			pluginsService.InvokeCommand(cmd);
		//}
		//else 
			*/
		
		var TextEl:XMLNode = new XMLNode(1,"Text");
		var Text:XMLNode = new XMLNode(3,text);
		TextEl.appendChild(Text);
		
		var Style:XMLNode = new XMLNode(1,"Style");
		Style.attributes.font = SALocalWork.IsLocalWork?"SCHLBKB.TTF":fontTTF;
		//Style.attributes.font = "arial.ttf";
		//Style.attributes.font = "tahomabd.ttf";
		
		Style.attributes.bold = bold == true ? 1 : 0;
		Style.attributes.italic = italic == true ? 1 : 0;
		Style.attributes.underline = underline == true ? 1 : 0;
		Style.attributes.size = size;
		Style.attributes.align = align;
		trace('TEXTEFFECT:ALIGN '+align);
		TextEl.appendChild(Style);
		
		
		var Desc:XMLNode = new XMLNode(1,"EffectDesc");
		Desc.attributes.Type = effect.type;
		var bounds:Object = mc.getBounds();
		Desc.attributes.Width = bounds.xMax - bounds.xMin;
		Desc.attributes.Height = bounds.yMax - bounds.yMin;
		
		var guides:String = effect.EncodeGuideLines(effect);
		//trace("====Encoded guides:"+guides);
		var DescText:XMLNode = new XMLNode(3,guides);//"L:0:50:600:50:B:0:300:200:660:400:-40:600:300:");
		
		Desc.appendChild(DescText);
		
		//_global.tr("BSM - InvokeCommandExArr" + Desc);

		pluginsService.InvokeCommandExArr("TEXTEFFECT_GET_CONTOUR",new Array(TextEl,Desc));
		Preloader.Show();
		owner.preloaderCount++;
		
	}

	private function onResponse(parsedResponse:Object) : Void {
				
		//_global.tr("BSM - onResponse - "+parsedResponse.errorCode);
		
		if(parsedResponse.errorCode > 0) {
			DrawByNode(owner.contour);
			_global.Project.CurrentPaper.CurrentFace.ActManager.Undo();
			OnRedraw(false);
			return;
		}

		var Command = parsedResponse.command;

		if (Command == "TEXTEFFECT_GET_CONTOUR") {
			
			var Status:XMLNode = Utils.XMLGetChild(parsedResponse.params, "Status");
			if (Status == undefined) {
				OnRedraw(false);
				return;
			}
			
			//==================================================
			if (Status.attributes.status == "Ok") {
				
				var Contour:XMLNode = Utils.XMLGetChild(parsedResponse.params, "Contour");
				if (Contour == undefined) {
					OnRedraw(false);
					return;
				}
				
				
				
				Preloader.Hide();
				if (owner.preloaderCount > 0) {
					owner.preloaderCount--;
				}
				trace("====status ok owner.preloaderCount--"+owner.preloaderCount);
				
				
				var Geometry:XMLNode = Utils.XMLGetChild(parsedResponse.params, "Geometry");
				if (Geometry != undefined) {
					//var textWidth:Number = Geometry.attributes.Width;
					//var textHeight:Number = Geometry.attributes.Height;
					var textRadius:Number = Geometry.attributes.Radius;
					var textAlpha:Number = Geometry.attributes.Alpha;
					var textBeta:Number = Geometry.attributes.Beta;
					//_global.tr("BSM - textAlpha - "+textAlpha);
					if (textAlpha < 0) {
						textAlpha = Math.abs(textAlpha);
					} else {
						textAlpha = textAlpha * -1;
					}
					
					owner.effect.refPoints[0].x = textRadius*Math.cos(textAlpha-(textBeta/2));
					owner.effect.refPoints[0].y = textRadius*Math.sin(textAlpha-(textBeta/2));
					
					delete owner.frame;
					owner.frame = Geometry;
				}
				
				//_global.tr("BSM - " + Contour);
				DrawByNode(Contour);
				delete owner.contour;
				owner.contour= Contour;
				owner.bChanged = false;
				
			}
			else if (Status.attributes.status == "NeedFont") {
				Preloader.Hide();
				if (owner.preloaderCount > 0) {
					owner.preloaderCount--;
				}
				trace("====status NeedFont owner.preloaderCount--"+owner.preloaderCount);
				if (this.needFontName == Status.attributes.font) return;
				this.needFontName = Status.attributes.font;
				this.fontFile = new XML();
				this.saFont = SAFont.GetInstance(this.fontFile);
				this.saFont.RegisterOnLoadedHandler(this, onFontFileLoaded);
				this.saFont.BeginLoadFontFile(Status.attributes.font);
				Preloader.Show();
				
				owner.preloaderCount++;
				trace("====status +NeedFont+ owner.preloaderCount++"+owner.preloaderCount);
			}
		}
		else if (Command == "TEXTEFFECT_SET_FONT") {
			Preloader.Hide();
			if (owner.preloaderCount > 0) {
					owner.preloaderCount--;
				}
			trace("====status TEXTEFFECT_SET_FONT owner.preloaderCount--"+owner.preloaderCount);
			this.needFontName = "";
			Draw();
		}		
	}


	function DrawByNode(Contour:XMLNode) : Void {
		IncAcyncCount();

		var cont:String = Contour.firstChild.nodeValue;
		
		mc.lineStyle(0,color,0);
		mc.beginFill(color);

		if (cont.length<1) {
			OnRedraw(true);
			return;
		}

		var cArr:Array=cont.split(":");
		var DrawObject:Object = {cArr:cArr,blockSize:10000,timeStart:getTimer()};


		if(cArr[0] == "Ver") {	// new format
			var formatVer:Number = parseFloat(cArr[1]);
			// if formatVer > 1.00 .... - version is not supported

			DrawObject.radix = 10;

/*  not used now; will be neccessary if we ever return to old version of DrawStepe 
			// internal coefficients to transfer integral coordinates into 
			// coordinates of moovieclip; optimal value is 1/20 (because flash
			// rounds all coordinates to 1/20 pt) but a) additional points with
			// fractional coordinated appear when we approximate besier curves by 
			// quadratic curves - so 1/10 is bette and b) we are limited by minimal
			// values of _xscale and _yscale for movieclip: they should be not less
			// than 100/50 (otherwise scaling is not precise enough and PDF differs 
			// from screen and direct print) so we may be 
			// forced to make these coefficients smaller than optimal
			DrawObject.coeX = 0.1; 
			DrawObject.coeY = 0.1;
*/
			DrawObject.X = 0;
			DrawObject.Y = 0;

			DrawObject.pos = 1;

			DrawStep(DrawObject);
		} else {
			mc._xscale = 100;
			mc._yscale = 100;

			DrawObject.pos = -1;
			DrawStepOld(DrawObject);
		}
	}

	private function DrawStep(DrawObject:Object) : Void {
		if(DrawObject.intervalId != undefined) 
			clearInterval(DrawObject.intervalId);

		var radix:Number = DrawObject.radix;

		var cArr:Array = DrawObject.cArr;

		//var coeX:Number = DrawObject.coeX; 
		//var coeY:Number = DrawObject.coeY;

		var X:Number = DrawObject.X;
		var Y:Number = DrawObject.Y;
		var dX:Number;
		var dY:Number;

		var pos:Number = DrawObject.pos;
		var lastPos:Number = Math.min(cArr.length,pos+DrawObject.blockSize);
		var contIsOK:Boolean = true;
		var stopDraw:Boolean = false;
					
		while ( pos < lastPos ) {
			var code:String = cArr[++pos];
					
			switch(code) {
				case "B": {
					// optimized for performance: besier curve is approximated 
					//   by 2 quadratic qurves (quite enough for font glyphs),
					//   all calculations are optimized

					//var x0:Number = X;
					//var y0:Number = Y;
					dX = parseInt(cArr[++pos],radix);
					if(dX%2==1)
					        dX=1-dX;
					dY = parseInt(cArr[++pos],radix);
					if(dY%2==1)
					        dY=1-dY;

					var xC1:Number = dX*(3/4)+X;
					var yC1:Number = dY*(3/4)+Y;
					//var x1:Number = (X+=dX);
					//var y1:Number = (Y+=dY);
					X+=dX;
					Y+=dY;							

					dX = parseInt(cArr[++pos],radix);
					if(dX%2==1)
					        dX=1-dX;
					dY = parseInt(cArr[++pos],radix);
					if(dY%2==1)
					        dY=1-dY;
					//var x2:Number = (X+=dX);
					//var y2:Number = (Y+=dY);							
					X+=dX;
					Y+=dY;							

					dX = parseInt(cArr[++pos],radix);
					if(dX%2==1)
					        dX=1-dX;
					dY = parseInt(cArr[++pos],radix);
					if(dY%2==1)
					        dY=1-dY;

					var xC2:Number = dX/4+X;
					var yC2:Number = dY/4+Y;

					//var x3:Number = (X+=dX);
					//var y3:Number = (Y+=dY);							
					X+=dX;
					Y+=dY;							

					//GeometryHelper.DrawCubicBezier2(mc,{x:x0, y:y0}, {x:x1, y:y1}, {x:x2, y:y2}, {x:x3, y:y3});
					// use optimized for speed version of DrawCubicBezier2:
					
					//var xC1:Number = (x1*3+x0)/4;
					//var yC1:Number = (y1*3+y0)/4;

					//var xC2:Number = (x2*3+x3)/4;
					//var yC2:Number = (y2*3+y3)/4;

					mc.curveTo(xC1, yC1, (xC1+xC2)/2, (yC1+yC2)/2);
					mc.curveTo(xC2, yC2, X, Y);

					break;
				}
				case "M": {
					dX = parseInt(cArr[++pos],radix);
					if(dX%2==1)
					        dX=1-dX;
					dY = parseInt(cArr[++pos],radix);
					if(dY%2==1)
					        dY=1-dY;
					//var x:Number = (X+=dX);
					//var y:Number = (Y+=dY);							

					mc.moveTo( X+=dX, Y+=dY );
					break;
				}
				case "L": {
					dX = parseInt(cArr[++pos],radix);
					if(dX%2==1)
					        dX=1-dX;
					dY = parseInt(cArr[++pos],radix);
					if(dY%2==1)
					        dY=1-dY;
					//var x:Number = (X+=dX);
					//var y:Number = (Y+=dY);							

					mc.lineTo( X+=dX, Y+=dY );
					break;
				}
				case "C": {
					var dXC:Number = parseInt(cArr[++pos],radix);
					if(dXC%2==1)
					    dXC=1-dXC;
					var dYC = parseInt(cArr[++pos],radix);
					if(dYC%2==1)
					    dYC=1-dYC;
					X+=dXC; Y+=dYC;
					var cx:Number = X;
					var cy:Number = Y;

					dX = parseInt(cArr[++pos],radix);
					if(dX%2==1)
					    dX=1-dX;
					dY = parseInt(cArr[++pos],radix);
					if(dY%2==1)
					    dY=1-dY;
					X+=dX; Y+=dY;
					var x:Number = X;
					var y:Number = Y;
					//var x:Number = (X+=dX);
					//var y:Number = (Y+=dY);							

					mc.curveTo(cx, cy, x, y);
					break;
				}
/* avoid to use Arcs in path: they are not invariant relative scaling 
				case "A": {
					var x1:Number = X;
					var y1:Number = Y;					

					dX = parseInt(cArr[++pos],radix);
					if(dX%2==1)
					        dX=1-dX;
					dY = parseInt(cArr[++pos],radix);
					if(dY%2==1)
					        dY=1-dY;
					var x2:Number = (X+=dX);
					var y2:Number = (Y+=dY);							

					dX = parseInt(cArr[++pos],radix);
					if(dX%2==1)
					        dX=1-dX;
					dY = parseInt(cArr[++pos],radix);
					if(dY%2==1)
					        dY=1-dY;
					var x3:Number = (X+=dX);
					var y3:Number = (Y+=dY);
					
					GeometryHelper.DrawArcFrom3Points(mc,x3*coeX,y3*coeY,x2*coeX,y2*coeY,x1*coeX,y1*coeY);
					mc.moveTo(x3*coeX, y3*coeY);
					//mc.curveTo(x2*coeX, y2*coeY, x3*coeX, y3*coeY);
					break;
				}
*/
				case "R": {
					radix = parseInt(cArr[++pos],10);
					DrawObject.radix = radix;
					break;
				}
				case "S": {
					var scaleX:Number = parseInt(cArr[++pos],radix)*2;
					var scaleY:Number = parseInt(cArr[++pos],radix)*2;

					mc._xscale = 100/scaleX;
					mc._yscale = 100/scaleY;

					break;
				}
				case "E": {
					stopDraw = true;
					pos = lastPos;
					break;
				}
				default: {
					contIsOK = false;
					stopDraw = true;
					pos = lastPos;
					break;
				}
			}
		}
		if(!stopDraw && pos >= cArr.length) { // no 'E' marker found
			stopDraw = true;
			contIsOK = false;
		}			
		if(stopDraw) 
			DrawEnd(contIsOK);
		else {
			DrawObject.X = X;
			DrawObject.Y = Y;
			DrawObject.pos = pos;

			var timeUsed:Number = getTimer() - DrawObject.timeStart;
			if(timeUsed>1000) 
				DrawObject.blockSize = Math.round(pos*3000/timeUsed);

			DrawObject.intervalId = setInterval(this, "DrawStep", 5, DrawObject);
		}
	}

/*  //  old version: it should be used if plugin (for any reason) will not adjust scales to 
    //  a power of 2

	private function DrawStep(DrawObject:Object) : Void {
		if(DrawObject.intervalId != undefined) 
			clearInterval(DrawObject.intervalId);

		var radix:Number = DrawObject.radix;

		var cArr:Array = DrawObject.cArr;

		var coeX:Number = DrawObject.coeX; 
		var coeY:Number = DrawObject.coeY;

		var X:Number = DrawObject.X;
		var Y:Number = DrawObject.Y;
		var dX:Number;
		var dY:Number;

		var pos:Number = DrawObject.pos;
		var lastPos:Number = Math.min(cArr.length,pos+DrawObject.blockSize);
		var contIsOK:Boolean = true;
		var stopDraw:Boolean = false;
					
		while ( pos < lastPos ) {
			var code:String = cArr[++pos];
					
			switch(code) {
				case "B": {
					var x0:Number = X;
					var y0:Number = Y;

					dX = parseInt(cArr[++pos],radix);
					if(dX%2==1)
					        dX=1-dX;
					dY = parseInt(cArr[++pos],radix);
					if(dY%2==1)
					        dY=1-dY;
					var x1:Number = (X+=dX);
					var y1:Number = (Y+=dY);							

					dX = parseInt(cArr[++pos],radix);
					if(dX%2==1)
					        dX=1-dX;
					dY = parseInt(cArr[++pos],radix);
					if(dY%2==1)
					        dY=1-dY;
					var x2:Number = (X+=dX);
					var y2:Number = (Y+=dY);							

					dX = parseInt(cArr[++pos],radix);
					if(dX%2==1)
					        dX=1-dX;
					dY = parseInt(cArr[++pos],radix);
					if(dY%2==1)
					        dY=1-dY;
					var x3:Number = (X+=dX);
					var y3:Number = (Y+=dY);							

					GeometryHelper.DrawCubicBezier2(mc,{x:x0*coeX, y:y0*coeY}, {x:x1*coeX, y:y1*coeY}, {x:x2*coeX, y:y2*coeY}, {x:x3*coeX, y:y3*coeY});
					break;
				}
				case "M": {
					dX = parseInt(cArr[++pos],radix);
					if(dX%2==1)
					        dX=1-dX;
					dY = parseInt(cArr[++pos],radix);
					if(dY%2==1)
					        dY=1-dY;
					var x:Number = (X+=dX);
					var y:Number = (Y+=dY);							

					mc.moveTo(x*coeX, y*coeY);
					break;
				}
				case "L": {
					dX = parseInt(cArr[++pos],radix);
					if(dX%2==1)
					        dX=1-dX;
					dY = parseInt(cArr[++pos],radix);
					if(dY%2==1)
					        dY=1-dY;
					var x:Number = (X+=dX);
					var y:Number = (Y+=dY);							

					mc.lineTo(x*coeX, y*coeY);
					break;
				}
				case "C": {
					dX = parseInt(cArr[++pos],radix);
					if(dX%2==1)
					        dX=1-dX;
					dY = parseInt(cArr[++pos],radix);
					if(dY%2==1)
					        dY=1-dY;
					var cx:Number = (X+=dX);
					var cy:Number = (Y+=dY);							

					dX = parseInt(cArr[++pos],radix);
					if(dX%2==1)
					        dX=1-dX;
					dY = parseInt(cArr[++pos],radix);
					if(dY%2==1)
					        dY=1-dY;
					var x:Number = (X+=dX);
					var y:Number = (Y+=dY);							

					mc.curveTo(cx*coeX, cy*coeY, x*coeX, y*coeY);
					break;
				}
				case "R": {
					radix = parseInt(cArr[++pos],10);
					DrawObject.radix = radix;
					break;
				}
				case "S": {
					var scaleX:Number = parseInt(cArr[++pos],radix)*2;
					var scaleY:Number = parseInt(cArr[++pos],radix)*2;
		// 50 is a variable coefficient. Affects rendering of large text effects 
		// on screen: further increasing slightly improves quality of letters 
		// on screen but a gap between letters and effect guides appears 
		// at screen and Print to Local Printer
					coeX = Math.min(coeX,50/scaleX);
					coeY = Math.min(coeY,50/scaleY);
					DrawObject.coeX = coeX; 
					DrawObject.coeY = coeY;

					mc._xscale = 100/(coeX*scaleX);
					mc._yscale = 100/(coeY*scaleY);

					break;
				}
				case "E": {
					stopDraw = true;
					pos = lastPos;
					break;
				}
				default: {
					contIsOK = false;
					stopDraw = true;
					pos = lastPos;
					break;
				}
			}
		}
		if(!stopDraw && pos >= cArr.length) { // no 'E' marker found
			stopDraw = true;
			contIsOK = false;
		}			
		if(stopDraw) 
			DrawEnd(contIsOK);
		else {
			DrawObject.X = X;
			DrawObject.Y = Y;
			DrawObject.pos = pos;

			var timeUsed:Number = getTimer() - DrawObject.timeStart;
			if(timeUsed>500) 
				DrawObject.blockSize = Math.round(pos*300/timeUsed);

			DrawObject.intervalId = setInterval(this, "DrawStep", 5, DrawObject);
		}
	}
*/

	private function DrawStepOld(DrawObject:Object) : Void {
		//old format - for support of saved projects

		if(DrawObject.intervalId != undefined)
			clearInterval(DrawObject.intervalId);

		var cArr:Array = DrawObject.cArr;
		var pos:Number = DrawObject.pos;
		var lastPos:Number = Math.min(cArr.length,pos+DrawObject.blockSize);
		var contIsOK:Boolean = true;
		var stopDraw:Boolean = false;

		while (pos < lastPos) {
			var code:String = cArr[++pos];
					
			switch(code) {
				case "B": {
					var x0:Number = parseFloat(cArr[++pos]);
					var y0:Number = parseFloat(cArr[++pos]);							
					var x1:Number = parseFloat(cArr[++pos]);
					var y1:Number = parseFloat(cArr[++pos]);							
					var x2:Number = parseFloat(cArr[++pos]);
					var y2:Number = parseFloat(cArr[++pos]);							
					var x3:Number = parseFloat(cArr[++pos]);
					var y3:Number = parseFloat(cArr[++pos]);							
				
					GeometryHelper.DrawCubicBezier2(mc,{x:x0, y:y0}, {x:x1, y:y1}, {x:x2, y:y2}, {x:x3, y:y3});
					break;
				}
				case "M": {
					var x:Number = parseFloat(cArr[++pos]);
					var y:Number = parseFloat(cArr[++pos]);

					mc.moveTo(x, y);
					break;
				}
				case "L": {
					var x:Number = parseFloat(cArr[++pos]);
					var y:Number = parseFloat(cArr[++pos]);
				
					mc.lineTo(x, y);
					break;
				}
				case "C": {
					var cx:Number = parseFloat(cArr[++pos]);
					var cy:Number = parseFloat(cArr[++pos]);							
					var x:Number = parseFloat(cArr[++pos]);
					var y:Number = parseFloat(cArr[++pos]);							

					mc.curveTo(cx, cy, x, y);
					break;
				}
				case "E": {
					stopDraw = true;
					pos = lastPos;
					break;
				}
				default: {
					contIsOK = false;
					stopDraw = true;
					pos = lastPos;
					break;
				}
			}
		}

		if(!stopDraw && pos >= cArr.length) {
			stopDraw = true;
			contIsOK = false;
		}			
		if(stopDraw)
			DrawEnd(contIsOK);
		else {
			DrawObject.pos = pos;

			var timeUsed:Number = getTimer() - DrawObject.timeStart;
			if(timeUsed>500) 
				DrawObject.blockSize = Math.round(pos*300/timeUsed);

			DrawObject.intervalId = setInterval(this, "DrawStepOld", 5, DrawObject);
		}
	}

	private function DrawEnd(result:Boolean) : Void {
		mc.endFill();
		OnRedraw(result);
	}


	public function set textString(value) {
		//lastGuides == null;
		text = value;
		//owner.bChanged = true;
	}

	function onFontFileLoaded(eventObject:Object) : Void {
		if (this.fontFile.firstChild.attributes.status != "Ok") return;
		
		var bin:String = this.fontFile.firstChild.firstChild.toString();
		if (bin == undefined || bin.length == 0) return;
		
		var Font:XMLNode = new XMLNode(1,"Font");
		Font.attributes.Name = this.needFontName;
		
		var Bin:XMLNode = new XMLNode(3,bin);
		Font.appendChild(Bin);
		
		pluginsService.InvokeCommandEx("TEXTEFFECT_SET_FONT",Font);
	}

	private function OnRedraw(isOK:Boolean) : Void {
		HideOwnerPreloader();
		var eventObject:Object = {type:"onRedraw", target:this, isOK:isOK};
		this.dispatchEvent(eventObject);
		DecAcyncCount();
	};

	public function RegisterOnRedrawHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("onRedraw", Delegate.create(scopeObject, callBackFunction));
	}

	private function HideOwnerPreloader() : Void {
		while(owner.preloaderCount > 0){
			Preloader.Hide();
			owner.preloaderCount--;
			trace("====while owner.preloaderCount--"+owner.preloaderCount);
		}
	}

	public static function get AsyncRunningCount():Number {
		return asyncRunningCount;
	}
	
	private function IncAcyncCount() {
		if(asyncCounterRegistered)
			return;
		asyncRunningCount++;
		asyncCounterRegistered = true;
	}

	private function DecAcyncCount() {
		if(!asyncCounterRegistered)
			return;
		asyncCounterRegistered = false;
		if(asyncRunningCount > 0)
			asyncRunningCount--;
		var eventObject = {type:"onEffRedrawFinished", target:this, running:asyncRunningCount};
		eventDispatcher.dispatchEvent(eventObject);
	}

	public static function RegisterOnEffRedrawFinishedHandler(scopeObject:Object, callBackFunction:Function) {
		eventDispatcher.addEventListener("onEffRedrawFinished", Delegate.create(scopeObject, callBackFunction));
	}

	public static function UnRegisterOnEffRedrawFinishedHandler(scopeObject:Object, callBackFunction:Function) {
		eventDispatcher.removeEventListener("onEffRedrawFinished", Delegate.create(scopeObject, callBackFunction));
	}

}
