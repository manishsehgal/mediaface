﻿class GradientUnit extends MoveableUnit {
	private var saveSize:Number;
	private var gradient:Object;
	
	function GradientUnit(mc:MovieClip, node:XML, gradientType:String, size:Number) {
		super(mc, node);
			
		if (node != null) {
			this.gradient = _global.Gradients.CreateGradientFromXml(node);
			this.saveSize = Number(node.attributes.saveSize);
		} else {
			this.gradient = _global.Gradients.GetGradient(gradientType);
			this.saveSize = size;
			
		}
		
		KeepProportions = false;
	}

	function GetXmlNode():XMLNode {
		var node:XMLNode = super.GetXmlNode();
		
		node.nodeName            = "Gradient";
		node.attributes.saveSize = this.saveSize;
		
		node.attributes.name   = this.gradient.name;
		node.attributes.dir    = this.gradient.dir;
		node.attributes.points = this.gradient.points;
		node.attributes.color1 = _global.GetColorForXML(this.gradient.color1);
		node.attributes.color2 = _global.GetColorForXML(this.gradient.color2);

		node.appendChild(super.GetXmlNodeRotate());
		return node;
	}
	
	function Draw():Void {
		trace ("ShapeUnit:Draw");
		DrawMC(mc);
	}
	function DrawMC(mc:MovieClip):Void {
		mc.clear();
		mc.color  = this.gradient.color1;
		mc.color2 = this.gradient.color2;
		var primitive:IDraw = this.gradient.drawSequence[0];
		primitive.Draw(mc, 0, 0, 0, this.saveSize/100, this.saveSize/100);
	}
	
	function getMode():String {
		trace ("ShapeUnit:GetMode");
		return _global.GradientEditMode;
	}
	
	function set Color1(value:Number):Void {
		this.gradient.color1 = value;
		this.Draw();
	}
	function get Color1():Number {
		return this.gradient.color1;
	}
	function set Color2(value:Number):Void {
		this.gradient.color2 = value;
		this.Draw();
	}
	function get Color2():Number {
		return this.gradient.color2;
	}
	
	private function Resize(kx:Number, ky:Number) {
		this.ScaleX = this.ScaleX * kx;
		this.ScaleY = this.ScaleY * ky;
	}
}
