﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;


[Event("onLoaded")]
class SASettings {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SASettings.prototype);
	private static var instance:SASettings = undefined;

	private var settings:LoadVars;
	private var isLoaded:Boolean;

	public static function GetInstance():SASettings {
		//if (instance == undefined)
			//instance = new SASettings();			
		//return instance;
		return new SASettings();
	}
	
	public function get IsLoaded():Boolean {
		return isLoaded;
	}
	
	private function SASettings() {
		isLoaded = false;
		var parent = this;
		settings = new LoadVars();
		
		settings.onLoad = function(success) {
			parent.ApplyVars();
			var eventObject:Object = {type:"onLoaded", target:parent, status:success};
			trace("SASettings: dispatch styles onLoaded");
			parent.dispatchEvent(eventObject);
		};
	}

	public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }

	public function BeginLoad() : Void {
		var currentDate:Date = new Date();
		var url:String = "Settings.aspx?time=" + currentDate.getTime();
		settings.load(url);
		BrowserHelper.InvokePageScript("log", "SASettings.as: css loaded");
    }

	public function ApplyVars() : Void {
		isLoaded = true;

		_global.Project.CurrentPaper.Calibration.X = parseFloat(settings.xcalibration);
		_global.Project.CurrentPaper.Calibration.Y = parseFloat(settings.ycalibration);
		_global.Project.CurrentPaper.Calibration.XDirectToCD = parseFloat(settings.xcalibrationDirectToCD);
		_global.Project.CurrentPaper.Calibration.YDirectToCD = parseFloat(settings.ycalibrationDirectToCD);
		_global.Project.CurrentPaper.Calibration.DiameterInnerL = parseFloat(settings.diamInnerL);
		_global.Project.CurrentPaper.Calibration.DiameterOuterL = parseFloat(settings.diamOuterL);
		_global.Project.CurrentPaper.Calibration.DiameterInnerB = parseFloat(settings.diamInnerB);
		_global.Project.CurrentPaper.Calibration.DiameterOuterB = parseFloat(settings.diamOuterB);
		_global.Project.CurrentPaper.Calibration.IsCalibrationNonDTCDSpecified = (!isNaN(settings.xcalibration));
		_global.Project.CurrentPaper.Calibration.IsCalibrationDTCDSpecified = (!isNaN(settings.xcalibrationDirectToCD));
		_global.Project.Extension = settings.extension;
		_global.culture = settings.culture;
		_global.browser = settings.browser;
		_global.platform = settings.platform;
		_global.basePlatform = settings.basePlatform;
		_global.sessionId = settings.sessionId;
		_global.user = settings.user;
		_global.userLevel = settings.userlevel; 
		_global.userLevelIndex = settings.userLevelIndex; 
		_global.freeSize = parseInt(settings.freeSize);
		_global.previewHtmlText = settings.previewHtmlText == null || settings.previewHtmlText == undefined ? "" : settings.previewHtmlText;
		
		if (_global.culture == undefined)
			_global.culture = "en";
		
		_global.tr("BSM - UserLevel = " + _global.userLevel);
	}
	
	
	
	public static function SaveCalibration(calibration:Object, isDTCD):Void {
		var currentDate:Date = new Date();
		var url:String = "Settings.aspx?type=SaveCalibration&isDTCD=" + isDTCD + 
			"&x=" + calibration.XSave + 
			"&y=" + calibration.YSave + 
//			"&diamInnerL=" + calibration.DiameterInnerL + 
//			"&diamOuterL=" + calibration.DiameterOuterL + 
//			"&diamInnerB=" + calibration.DiameterInnerB + 
//			"&diamOuterB=" + calibration.DiameterOuterB + 
			"&time=" + currentDate.getTime();
			
		var xml:XML = new XML();
		xml.load(url);
		xml.onLoad = function(success) {};
	}
}