﻿import mx.controls.Label;
import mx.controls.TextInput;
import mx.core.UIObject;
import mx.utils.Delegate;

[Event("change")]

class Slider extends UIObject{
	var title:String;
	var minValue:Number;
	var maxValue:Number;
	var shiftMM:Number = 0;
	var step:Number;
	var coeff:Number;
	
	var lblMaxValue:Label;
	var lblMinValue:Label;
	var lblLeft:Label;
	var lblRight:Label;
	var sliderHandle:MovieClip;
	var scale:MovieClip;
	var lblValue:TextInput;
	var lblTitle:Label;
	
	function Slider() {
		sliderHandle.onPress = press;
		sliderHandle.onMouseUp = mouseUp;
		sliderHandle.onMouseMove = null;
		
		lblTitle.setStyle("styleName", "SliderTitleText");
		lblValue.setStyle("styleName", "SliderInputText");
		lblLeft.setStyle("styleName", "CalibrationLabel");
		lblRight.setStyle("styleName", "CalibrationLabel");
		
		coeff = 200 /(maxValue - minValue);
	}
	
	public function set Points(value:Number):Void {
		shiftMM = Math.round(value/72*25.4*10)/10;
		sliderHandle._x = (shiftMM - minValue)*coeff;
		lblValue.text = shiftMM.toString();
		lblValue.text += " mm";
	}
	
	public function get Points():Number {
		return shiftMM*72/25.4;
	}
	
	function onLoad() {
		var x:Number = Math.round(maxValue*10)/10;
		lblMaxValue.text = x.toString();
		lblMaxValue.text += " mm";
		x = Math.round(minValue*10)/10;
		lblMinValue.text = x.toString();
		lblMinValue.text += " mm";
		lblTitle.text = title;
		
		scale.lineStyle(1,0xFFFFFF);
		for (var i:Number = minValue; i < maxValue; i += 2*step) {
			x = (i-minValue)*coeff;
			scale.moveTo(x, 0);
			scale.lineTo(x, -4);
		}
	}
	
	function mouseMove() {
		var i:Number = Math.round((_parent.minValue + _parent._xmouse / _parent.coeff) / _parent.step);
		_x = (i * _parent.step - _parent.minValue) * _parent.coeff;
		if (_x < 0) _x = 0;
		if (_x > 200) _x = 200; 
		_parent.adjustValue();
	}
	
	public function RegisterOnChangeHandler(scopeObject:Object, callBackFunction:Function) {
		addEventListener("change", Delegate.create(scopeObject, callBackFunction));
    }
	
	function adjustValue() {
		var i:Number = Math.round((minValue + sliderHandle._x / coeff) / step);
		sliderHandle._x = (i * step - minValue) * coeff;
		
		var newShift = Math.round((minValue + sliderHandle._x / coeff)*10)/10;
		
		if (shiftMM == newShift)
			return;
			
		shiftMM = newShift;
		
		lblValue.text = shiftMM.toString();
		lblValue.text += " mm";

		var eventObject = {type:"change", target:this};
		this.dispatchEvent(eventObject);
	}
	
	function press() {
		this.onMouseMove = _parent.mouseMove;
	}
	
	function mouseUp() {
		if(!_root.pnlPreviewLayer._visible)
			return;

		//_parent.adjustValue();
		this.onMouseMove = null;
	}
	
	function onMouseDown() {
		if(!_root.pnlPreviewLayer._visible)
			return;

		if (_xmouse >=0 && _xmouse <= 200 &&
			_ymouse >=-4 && _ymouse <= 10)
		{
			sliderHandle._x = _xmouse;
			adjustValue();
		}
	}
}