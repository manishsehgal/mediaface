﻿import mx.core.UIObject;
import mx.controls.HScrollBar;
import mx.controls.VScrollBar;
import mx.utils.Delegate;

class Workspace.DesignArea  extends UIObject {
	
	private var mcFaceContainer : MovieClip;
	private var hSB : HScrollBar;
	private var vSB : VScrollBar;
	
	function DesignArea() {
		instance = this;
	}

    function onLoad() {
        this.hSB.lineScrollSize = 10;
        this.hSB.pageScrollSize = 100;
		this.hSB.addEventListener("scrollChanged", Delegate.create(this, onHScrollChanged));
        this.vSB.lineScrollSize = 10;
        this.vSB.pageScrollSize = 100;
		this.vSB.addEventListener("scrollChanged", Delegate.create(this, onVScrollChanged));
    }

	static private var instance : Workspace.DesignArea;
	static function getInstance() : Workspace.DesignArea {
		return instance;
	}
	
	function get Visible() : Boolean {
		return this._parent._visible;
	}
	function set Visible(vis:Boolean) {
		this._visible = vis;
		this._parent._visible = vis;
	}
	
	function GetScrollPosition() : Object {
		var pos:Object = new Object();
		pos.x = hSB.scrollPosition;
		pos.y = vSB.scrollPosition;
		pos.maxX = hSB.maxPos;
		pos.minX = hSB.minPos;
		pos.maxY = vSB.maxPos;
		pos.minY = vSB.minPos;
		return pos;
	}
	
	function SetScrollPosition(posX:Number, posY:Number) {
		 hSB.scrollPosition = posX;
		 vSB.scrollPosition = posY;
	}
	
	function SetScrollProperties(sd:Object) {
		if (sd.hMaxSize > sd.hPageSize) {
			this.hSB._visible = true;
	        this.hSB.setScrollProperties(sd.hPageSize-this.vSB._width, 0, sd.hMaxSize-sd.hPageSize+this.vSB._width);
	        //this.hSB.scrollPosition = sd.hPos;
		} else {
			this.hSB._visible = false;
	        this.hSB.setScrollProperties(0, 0, 0);
	        //this.hSB.scrollPosition = 0;
		}
		
		if (sd.vMaxSize > sd.vPageSize) {
			this.vSB._visible = true;
	        this.vSB.setScrollProperties(sd.vPageSize-this.hSB._height, 0, sd.vMaxSize-sd.vPageSize+this.hSB._height);
	        //this.vSB.scrollPosition = sd.vPos;
		} else {
			this.vSB._visible = false;
	        this.vSB.setScrollProperties(0, 0, 0);
	        //this.vSB.scrollPosition = 0;
		}
	}
	
	function onHScrollChanged(eventObject) {
		_global.Project.CurrentPaper.CurrentFace.AdjustPosition();
	}
	function onVScrollChanged(eventObject) {
		_global.Project.CurrentPaper.CurrentFace.AdjustPosition();
	}
}