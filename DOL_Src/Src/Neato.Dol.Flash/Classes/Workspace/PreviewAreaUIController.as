﻿
import mx.controls.Alert;
class Workspace.PreviewAreaUIController {
	private var ctlPrintArea:Workspace.PreviewArea;
	private var ctlPrint:Preview.Print;
	private var ctlLSPrint:Preview.LSPrint;
	private var ctlPreviewTool:Preview.PreviewTool;
	private var ctlCalibrate:Preview.Calibrate;
	private var ctlLSPanel:Preview.LSPanel;
	private var previewXml:XML = null;
	private var lightscribeImage = new Object();
	private var printPaper;
	private var pxToMmCoeff:Number = 0.35277777777777;

	private var pluginsService : SAPlugins;
	
	function PreviewAreaUIController() {
	}

	public function Clear() {
		lightscribeImage.doScan = false;
		var td:Object = lightscribeImage.timerData;
		td.bmp.dispose();
		delete lightscribeImage.timerData;
		delete lightscribeImage.xml;
		lightscribeImage = new Object();
		printPaper = null;
	}
	
	function OnComponentLoaded(compName:String, compObj:Object, param) {
		_global.tr("PreviewArea OnComponentLoaded: "+compName);
		if (compName == "PreviewArea") {
			ctlPrintArea = Workspace.PreviewArea(compObj);
			//ctlPrintArea.RegisterOnBmpDataHandler(this, onLightscribeDataReady);
			ctlPrintArea.RegisterOnFaceClickHandler(this, SwitchLabel);
			compObj.Visible = false;
			compObj.RegisterOnSetVisibleHandler(this, onSetVisible);
			_global.Project.RegisterOnLoadedHandler(this, onProjectLoaded);
			
		}

		switch(compName.split(".")[1]) {
			case "Print" :
				ctlPrint = Preview.Print(compObj);
				ctlPrint.RegisterPrintHandler(this, Print);
				ctlPrint.RegisterPrintToPdfHandler(this, PrintToPdf);
				break;
			case "LSPrint" :
				ctlLSPrint = Preview.LSPrint(compObj);
				ctlLSPrint.RegisterLightscribeHandler(this, onLightscribeClicked);

				pluginsService = SAPlugins.GetInstance();
				pluginsService.RegisterOnInvokedParsedHandler(this, onLightScribeInvoked);
				pluginsService.RegisterOnModuleAvaliableHandler(this, IsLightscribeModuleAvaliable);
				break;
			case "Calibrate" :
				ctlCalibrate = Preview.Calibrate(compObj);
				ctlCalibrate.RegisterOnCalibrateHandler(this, OnCalibrate);
				ctlCalibrate.RegisterOnCalibrateDTCDHandler(this, OnCalibrateDTCD);
				ctlCalibrate.RegisterOnNeedRefreshHandler(this, OnNeedRefresh);
				ctlCalibrate.Visibility = false;
				break;
			case "PreviewTool" :
				ctlPreviewTool = Preview.PreviewTool(compObj);
				break;
			case "LSPanel":
				ctlLSPanel = Preview.LSPanel(compObj);
		}
	}
	
	function onProjectLoaded(eventObject) {
		if (ctlPrintArea._visible == true)
			DataBind();
	}
	
	function onSetVisible(eventObject) {
		if (eventObject.visiblility) {
			DataBind();
		}
		ctlCalibrate.Visibility = eventObject.visiblility;
	}
	
	function OnCalibrate(eventObject) {
		_global.Project.CurrentPaper.Calibration.X = eventObject.x;
		_global.Project.CurrentPaper.Calibration.Y = eventObject.y;
		_global.Project.CurrentPaper.Calibration.IsCalibrationNonDTCDSpecified = true;
		
		printPaper.Calibration.X = eventObject.x;
		printPaper.Calibration.Y = eventObject.y;
		printPaper.Calibration.IsCalibrationNonDTCDSpecified = true;

		_global.Project.CurrentPaper.Calibration.Save();
		PrintAreaDataBind(true);
	}
	
	function OnCalibrateDTCD(eventObject) {
		_global.Project.CurrentPaper.Calibration.XCopy = eventObject.x;
		_global.Project.CurrentPaper.Calibration.YCopy = eventObject.y;
		_global.Project.CurrentPaper.Calibration.XDirectToCD = eventObject.xCenter;
		_global.Project.CurrentPaper.Calibration.YDirectToCD = eventObject.yCenter;
		_global.Project.CurrentPaper.Calibration.DiameterInner = eventObject.diamInner;
		_global.Project.CurrentPaper.Calibration.DiameterOuter = eventObject.diamOuter;
		_global.Project.CurrentPaper.Calibration.IsCalibrationDTCDSpecified = true;
		
		printPaper.Calibration.XCopy = eventObject.x;
		printPaper.Calibration.YCopy = eventObject.y;
		printPaper.Calibration.XDirectToCD = eventObject.xCenter;
		printPaper.Calibration.YDirectToCD = eventObject.yCenter;
		printPaper.Calibration.DiameterInner = eventObject.diamInner;
		printPaper.Calibration.DiameterOuter = eventObject.diamOuter;
		printPaper.Calibration.IsCalibrationDTCDSpecified = true;

		_global.Project.CurrentPaper.Calibration.Save();
		
		
		var mc = ctlPrintArea["printPaper"];
		previewXml = _global.Project.GenerateProjectXml();
		AdjustProjectForDTCD(previewXml);
		printPaper.ReadFaces(mc, previewXml);
		AutofitUnits(printPaper);
		previewXml = printPaper.GenerateProjectXml(previewXml);
		AdjustFillNodeForDTCD(previewXml);
		
		PrintAreaDataBind(true);
		CalibrateDataBind();
		PrintDataBind();
		
		//this.DataBind();
	}
	
	function OnNeedRefresh(eventObject) {
		_global.Project.CurrentPaper.Calibration.XReal = eventObject.x;
		_global.Project.CurrentPaper.Calibration.YReal = eventObject.y;
		
		printPaper.Calibration.XReal = eventObject.x;
		printPaper.Calibration.YReal = eventObject.y;

		PrintAreaDataBind();
	}
	
	public function DataBind():Void {
		var mc = ctlPrintArea["printPaper"];
		if (mc == null || mc == undefined)
			mc = ctlPrintArea.createEmptyMovieClip("printPaper", ctlPrintArea.getNextHighestDepth());
		else
			mc = ctlPrintArea.createEmptyMovieClip("printPaper", mc.getDepth());
		mc._visible = false;
		
		printPaper = _global.PaperHelper.CreatePaper();
		printPaper.Calibration.X =_global.Project.CurrentPaper.Calibration.X;
		printPaper.Calibration.Y =_global.Project.CurrentPaper.Calibration.Y;
		printPaper.Calibration.XDirectToCD =_global.Project.CurrentPaper.Calibration.XDirectToCD;
		printPaper.Calibration.YDirectToCD =_global.Project.CurrentPaper.Calibration.YDirectToCD;
		printPaper.Calibration.DiameterInner =_global.Project.CurrentPaper.Calibration.DiameterInner;
		printPaper.Calibration.DiameterOuter =_global.Project.CurrentPaper.Calibration.DiameterOuter;
		printPaper.Calibration.IsCalibrationNonDTCDSpecified =_global.Project.CurrentPaper.Calibration.IsCalibrationNonDTCDSpecified;
		printPaper.Calibration.IsCalibrationDTCDSpecified = _global.Project.CurrentPaper.Calibration.IsCalibrationDTCDSpecified;

		previewXml = _global.Project.GenerateProjectXml();
		AdjustProjectForDTCD(previewXml);
		printPaper.ReadFaces(mc, previewXml);
		AutofitUnits(printPaper);
		previewXml = printPaper.GenerateProjectXml(previewXml);
		AdjustFillNodeForDTCD(previewXml);

		if (printPaper.IsLightscribeDevice) {
			printPaper.Calibration.X = 0;
			printPaper.Calibration.Y = 0;
		}

		var data:Object = new Object();
		data.isLightScribeDevice = printPaper.IsLightscribeDevice;
		ctlPreviewTool.DataBind(data);
		CalibrateDataBind();
		PrintDataBind();
		//ctlPrint.doLater(this, "PrintAreaDataBind");
		if(printPaper.IsLightscribeDevice)
			LightscribePrepare();
	}
	
	private function AdjustProjectForDTCD(projectXml:XML) {
		if (_global.Project.CurrentPaper.IsDirectToCDDevice == false) 
			return;
			
		var projXml:XMLNode = projectXml.firstChild;
		var facesXml:XMLNode = Utils.XMLGetChild(projXml,"Faces");	
		var faceNode:XMLNode = facesXml.firstChild;
		var contourNode:XMLNode = Utils.XMLGetChild(faceNode,"Contour");
		
		var maxRadius = -1;
		var minRadius = 1000;
		var outerCircleNode:XMLNode = undefined;
		var innerCircleNode:XMLNode = undefined;		 		
		
		while (contourNode != undefined) {
			
			for(var circleNode:XMLNode = contourNode.firstChild; 
					circleNode != undefined; 
					circleNode = circleNode.nextSibling) { 
				var r:Number = Number(circleNode.attributes.r);
		
				if(r > maxRadius) {
					maxRadius = r;
					outerCircleNode = circleNode;
				} 
				if(r < minRadius) {
					minRadius = r;
					innerCircleNode = circleNode;
				} 
			}

			// paper may be described as Universal with RealContour
			contourNode = Utils.XMLGetChild(contourNode,"RealContour");
		}

		if (maxRadius < 150) {
			printPaper.Calibration.DiameterType = "L";
			_global.Project.CurrentPaper.Calibration.DiameterType = "L";
		}
		printPaper.Calibration.DiameterInner = _global.Project.CurrentPaper.Calibration.DiameterInner;
		printPaper.Calibration.DiameterOuter = _global.Project.CurrentPaper.Calibration.DiameterOuter;
			
		if(outerCircleNode != undefined) {
			var outer:Number = printPaper.Calibration.DiameterOuter / (2*pxToMmCoeff);
			outerCircleNode.attributes.r = outer;
		}

		if (innerCircleNode != undefined) {
			var inner:Number = printPaper.Calibration.DiameterInner / (2*pxToMmCoeff);
			innerCircleNode.attributes.r = inner;
		}
	}
	
	private function AdjustFillNodeForDTCD(projectXml:XML) : Void {
		if (_global.Project.CurrentPaper.IsDirectToCDDevice == false) 
			return;
			
		var projXml:XMLNode = projectXml.firstChild;
		var facesXml:XMLNode = Utils.XMLGetChild(projXml,"Faces");	
		var faceNode:XMLNode = facesXml.firstChild;
		var fillNode:XMLNode = Utils.XMLGetChild(faceNode,"Fill");
			
		if (fillNode != undefined && fillNode != null) {
			var gradientType:String = fillNode.attributes.gradientType;
			if (gradientType != undefined && gradientType != null && gradientType != "00") {
				fillNode.attributes.width  = printPaper.Calibration.DiameterOuter / pxToMmCoeff;
				fillNode.attributes.height = printPaper.Calibration.DiameterOuter / pxToMmCoeff;
				var contour/*:ContourUnit*/ = printPaper.Faces[0].contours[0];
				if (contour != undefined) {
					fillNode.attributes.x = contour.contourX;
					fillNode.attributes.y = contour.contourY;
				}
			}
		}
	}
	
	private function AutofitUnits(paper) : Void {
		if (_global.Project.CurrentPaper.IsDirectToCDDevice == false) 
			return;
		
		for (var i:Number = 0; i < paper.Faces.length; ++ i) {
			var face/*:FaceUnit*/ = paper.Faces[i];
			for (var j:Number = 0; j < face.units.length; ++ j) {
				var unit = face.units[j];
				if (unit.AutofitType != _global.NoneAutofit && unit.AutofitType != _global.ThumbailAutofit)
					face.AutofitUnit(unit, unit.AutofitType);
			}
		}
	}

	public function CalibrateDataBind() {
		var data = new Object();
		
		data.x = printPaper.Calibration.XSave;
		data.y = printPaper.Calibration.YSave;
		
		data.diamInnerMin = printPaper.Calibration.DiameterInnerMin;
		data.diamInnerMax = printPaper.Calibration.DiameterInnerMax;
		data.diamInner    = printPaper.Calibration.DiameterInner;
		data.diamOuterMin = printPaper.Calibration.DiameterOuterMin;
		data.diamOuterMax = printPaper.Calibration.DiameterOuterMax;
		data.diamOuter    = printPaper.Calibration.DiameterOuter;
		
		data.isLightscribeDevice = printPaper.IsLightscribeDevice;
		
		ctlCalibrate.DataBind(data);
	}
	
	public function PrintDataBind() {
		var data = new Object();
		data.isLightscribeDevice = printPaper.IsLightscribeDevice;
		ctlPrint.DataBind(data);
	}

	public function PrintAreaDataBind(calibrateOnly:Boolean) {
		var data = new Object();
		data.paperType = printPaper.PaperType;
		data.calibrateOnly = calibrateOnly == true ? true : false;
		data.faces = printPaper.Faces;
		data.width = printPaper.Width;
		data.height = printPaper.Height;
		data.x = printPaper.Calibration.XReal;
		data.y = printPaper.Calibration.YReal;
		data.isLightscribeDevice = printPaper.IsLightscribeDevice;
		data.htmlText = _global.previewHtmlText;
		ctlPrintArea.DataBind(data);
	}
	
	private var prevSrcIndex:Number = -1;
	function SwitchLabel(eventObject) {
		var srcArr:Array    = _global.Project.CurrentPaper.Faces;
		var dstArr:Array    = printPaper.Faces;
		
		if(dstArr.length < 2)
			return;			// switching off single label is senseless
					
		var srcIndex:Number = _global.LabelSwitcher.SelectedLabelIndex;
		var dstIndex:Number = eventObject.faceIndex;

		var srcFace = srcArr[srcIndex];
		var dstFace = dstArr[dstIndex];
		
		if (srcIndex != prevSrcIndex) {
			prevSrcIndex = srcIndex;
		}

		if (srcFace.puid != dstFace.puid) {
			srcIndex = dstIndex;
			if (dstFace.SrcIndex != undefined)
				srcIndex = dstFace.SrcIndex;
		}
		
		if (dstFace.IsEmpty) {
			_global.FaceHelper.CopyFace(srcArr, dstArr, srcIndex, dstIndex, true);
			dstFace.SrcIndex = srcIndex;
		}
		else {
			dstFace.Clear();
		}

		PrintAreaDataBind();
	}
	
	//-------- Print to local printer --------------------
	
	public function Print(eventObject) {
		switch(printPaper.PaperType) {
			case "DieCut" :
				_global.PaperHelper.ContourVisibility(PrintHelper.PrintPreview, false, printPaper.Faces);
				break;
			case "Universal" :
				_global.PaperHelper.ContourVisibility(PrintHelper.PrintPreview, true, printPaper.Faces);
				break;
		}
		
		if (printPaper.Calibration.IsCalibrationSpecified) {
			PrintHelper.PrintMC(PrintHelper.PrintPreview);
			ShowExitPage();
			SATracking.PrintLocal();
		} else {
			ShowCalibrationMessage(this.onAlertLocal);
		}
	}
	private function onAlertLocal(evt) {
		if (evt.detail == Alert.CANCEL) {
			PrintHelper.PrintMC(PrintHelper.PrintPreview);
			ShowExitPage();
			SATracking.PrintLocal();
		}
		else if (evt.detail == Alert.OK && !printPaper.IsLightscribeDevice) {
			// _global.UIController.PreviewAreaController == this
			_global.UIController.PreviewAreaController.ctlPreviewTool.Show(Preview.PreviewTool.CalibrateTab);
		}
	}
	
	//-------- Print to PDF --------------------
	
	public function PrintToPdf(eventObject) {
		if (printPaper.Calibration.IsCalibrationSpecified) {
			var x:XML = printPaper.GenerateProjectXml(previewXml);
			AdjustFillNodeForDTCD(x);
			PrintHelper.Print(x);
			
			ShowExitPage();
			SATracking.PrintPDF();
		} else {
			ShowCalibrationMessage(this.onAlertPdf);
		}
	}
	private function onAlertPdf(evt) {
		if (evt.detail == Alert.CANCEL) {
			var x:XML = _global.UIController.PreviewAreaController.printPaper.GenerateProjectXml(_global.UIController.PreviewAreaController.previewXml);
			_global.UIController.PreviewAreaController.AdjustFillNodeForDTCD(x);
			PrintHelper.Print(x);
			
			ShowExitPage();
			SATracking.PrintPDF();
		}
		else if (evt.detail == Alert.OK && !printPaper.IsLightscribeDevice) {
			// _global.UIController.PreviewAreaController == this
			_global.UIController.PreviewAreaController.ctlPreviewTool.Show(Preview.PreviewTool.CalibrateTab);
		}
	}
	
	//-------- Print to Lightscribe --------------------

	public function LightscribePrepare() {
		lightscribeImage.isLSbuttonClicked = false;
		ctlLSPrint.LightscribeEnabled = true;
		Preloader.Hide();
		MakeLightscribeImg();
	}
	
	public function onLightscribeClicked(eventObject) {
		lightscribeImage.isLSbuttonClicked = true;
		lightscribeImage.isCheckRunning = true;
		pluginsService.IsModuleAvailable("LightscribeModule");
	}

	private function IsLightscribeModuleAvaliable(eventObject) {
		if (!eventObject.isAvaliable) {
			lightscribeImage.isCheckRunning = true;
			return;
		}

		if(lightscribeImage.isLSbuttonClicked) {
			ctlLSPrint.LightscribeEnabled = false;
			Preloader.Show();
		}
			
		pluginsService.InvokeCommandEx("LS_DETECT_DEVICE");
	}

	public function MakeLightscribeImg() {
		lightscribeImage.Status = "Scan";
		lightscribeImage.data = null;

		lightscribeImage.passN = -1;
		lightscribeImage.scanDone  = 0;
		lightscribeImage.scanTotal = 100;
		UpdateLSProgressMeter();

		_global.PaperHelper.ContourVisibility(PrintHelper.PrintPreview, false, printPaper.Faces);
		var face = printPaper.CurrentFace;
		var k:Number = 2836/face.Width;	// keep 600x600 dpi resolution as strong as possible
		ctlPrintArea.GetPreviewBitmapData(face.PaperX, face.PaperY, face.Width, face.Height, printPaper.Calibration.XReal, printPaper.Calibration.YReal, k, lightscribeImage, this, onLightscribeDataReady);
		//ctlPrintArea.GetPreviewBitmapData(face.PaperX, face.PaperY, width, height, 0, 0, k);
	}

	
	private function showLSmsg(msg:String) {
		if(lightscribeImage.isLSbuttonClicked)
			_global.MessageBox.Alert(msg, "Warning", null);
	}

	private function onLightScribeInvoked(parsedResponse) { // response obtained from LS plugin

		switch(parsedResponse.command) {
			case "LS_DETECT_DEVICE": 
				var isLightscribeOK:Boolean = false;

				switch(parsedResponse.response.attributes.Error) {
					case "0":
					case "Ok":
						isLightscribeOK = true;
						break;
					case "NoDll":
						showLSmsg(" The Label Printing Library is not installed. Please reinstall the LightScribe host software.");
						break;
					case "NoDrive":
						showLSmsg(" No LightScribe drives were found in your system.");
						break;
					case "NoLSBurner":
						showLSmsg(" A part of LightScribe plugin is missing or corrupted.  Please update the LightScribe plugin.");
						break;
				}
				lightscribeImage.isCheckRunning = false;
				if(!isLightscribeOK) {
					lightscribeImage.isLSbuttonClicked = false;
					ctlLSPrint.LightscribeEnabled = true;
					Preloader.Hide();
					break;
				}

				if(lightscribeImage.isLSbuttonClicked) {
					SATracking.PrintLightscribe();
					SendLightscribeData();
				} /*else {
					MakeLightscribeImg();
				}*/
				break;
			case "LS_PRINT":
				ctlLSPrint.LightscribeEnabled = true;
				Preloader.Hide();
				ShowExitPage();
				break;
		}
	}

	private function onLightscribeDataReady(scanObj) {

		UpdateLSProgressMeter();

		SendLightscribeData();
	}

	private function UpdateLSProgressMeter() {
		ctlLSPanel.SetProgress(lightscribeImage.scanDone,lightscribeImage.scanTotal);
//		_global.tr("LS progress: "+lightscribeImage.scanDone+" from "+lightscribeImage.scanTotal);
	}

	private function ConvertLSImage() {
		if(lightscribeImage.Status == "Ready")
			return;

		var xmlDoc:XML = new XML("<CmdInfo/>");
		var rootNode:XMLNode = xmlDoc.firstChild;
		rootNode.attributes.Type = "Request";
		
		var requestNode:XMLNode = xmlDoc.createElement("Request");
		requestNode.attributes.Name = "LS_PRINT";
		rootNode.appendChild(requestNode);
		var paramsNode:XMLNode = xmlDoc.createElement("Params");
		requestNode.appendChild(paramsNode);

		var timerData = lightscribeImage.timerData;		
		var bmpDataNode:XMLNode = xmlDoc.createElement("BmpData");
		bmpDataNode.attributes.Encoding = timerData.encoding;
		bmpDataNode.attributes.Width = timerData.bmp.width;
		bmpDataNode.attributes.Height = timerData.bmp.height;
		bmpDataNode.attributes.LargeRadius = timerData.largeRadius;
		bmpDataNode.attributes.SmallRadius = timerData.smallRadius;
		
/*		var paperName:String = printPaper.PaperName.toLowerCase();
		if (paperName.indexOf("full") != -1)
			bmpDataNode.attributes.LabelMode = "full";
		else if (paperName.indexOf("title") != -1)
			bmpDataNode.attributes.LabelMode = "title";
		else if (paperName.indexOf("content") != -1)
			bmpDataNode.attributes.LabelMode = "content";
*/		
		var radFraction:Number = timerData.largeRadius/timerData.smallRadius;
		bmpDataNode.attributes.LabelMode = (radFraction>1.5 ? "full" : radFraction>1.2 ? "content" : "title");
			
		paramsNode.appendChild(bmpDataNode);

/*		var bmpDataTextNode:XMLNode = xmlDoc.createTextNode(timerData.result);
		lightscribeImage.timerData = null;
		bmpDataNode.appendChild(bmpDataTextNode);
*/
		lightscribeImage.xml = xmlDoc;

		lightscribeImage.Status = "Ready";
		lightscribeImage.doScan = false;
	}

	private function AddPrintParameters(xmlDoc:XML) {

		var guiDataNode:XMLNode = xmlDoc.createElement("GuiData");
		guiDataNode.attributes.LSquality = ctlLSPanel.LSQuality;
		guiDataNode.attributes.silent = ( ctlLSPanel.SilentMode ? "1" : "0" );

		var paramsNode:XMLNode = xmlDoc.firstChild.firstChild.firstChild;
		paramsNode.insertBefore(guiDataNode,paramsNode.firstChild);
	}

	private function SendLightscribeData() {
		if(lightscribeImage.Status == undefined) {
			MakeLightscribeImg();
			return;
		}
		var passN:Number = lightscribeImage.passN;
		var quality:Number = ctlLSPanel.LSQuality;
		if(passN >= 4) {
			UpdateLSProgressMeter();
			ConvertLSImage();
		}
		if(lightscribeImage.isCheckRunning || !(lightscribeImage.isLSbuttonClicked && (!lightscribeImage.doScan || passN >=4 || quality>0 && passN > 0 )))
			return;
		lightscribeImage.doScan = false;
		lightscribeImage.isLSbuttonClicked = false;
		ConvertLSImage();
		var xml:XML = lightscribeImage.xml;
		AddPrintParameters(xml);
		//pluginsService.InvokeCommand(xml);
		pluginsService.InvokeCommand(xml,lightscribeImage.timerData.result);
		//Preloader.Hide();

		//ShowExitPage();
	}
	
	//---------------------------------------
	
	private function ShowCalibrationMessage(onAlert) {
		var alertOkLabel     : String = Alert.okLabel;
		var alertCancelLabel : String = Alert.cancelLabel;
		var alertButtonWidth : Number = Alert.buttonWidth;
			
		var alertMessage:String = "It is strongly recommended you calibrate \n your printer before you print your design.";
		var alertCaption:String = "Warning";
		var alertButtonWidth:Number = Alert.buttonWidth;
		
		Alert.okLabel     = "Calibrate Printer";
		Alert.cancelLabel = "No Thanks, continue printing";
		Alert.buttonWidth = 150;
		Alert.show(alertMessage, alertCaption, Alert.OK | Alert.CANCEL, null, onAlert);
		
		Alert.okLabel = alertOkLabel;
		Alert.cancelLabel = alertCancelLabel;
		Alert.buttonWidth = alertButtonWidth;
	}
	
	static private function ShowExitPage():Void {
		BrowserHelper.InvokePageScript("ShowExitPage", "");
	}
}
