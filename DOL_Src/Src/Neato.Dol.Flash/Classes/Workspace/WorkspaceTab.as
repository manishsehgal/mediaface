﻿import mx.utils.Delegate;
import mx.controls.Label;
import mx.core.UIObject;
import mx.behaviors.DepthControl;

class Workspace.WorkspaceTab extends UIObject {
	
	private var mcTab1 : MovieClip;
	private var mcTab2 : MovieClip;
	private var lbl1   : Label;
	private var lbl2   : Label;
	private var curSel : String;
	
	function WorkspaceTab() {
		instance = this;
	}
	
	private static var instance : Workspace.WorkspaceTab;
	static function getInstance() : Workspace.WorkspaceTab {
		return instance;
	}
	
	function onLoad() {
		mcTab1.onRelease =  function() {
			this._parent.onSelect("design");
		};
		mcTab2.onRelease =  function() {
			this._parent.onSelect("preview");
		};
		
		SetSel("design");
		
		_global.GlobalNotificator.OnComponentLoaded("WorkspaceTab", this);
	}
	
	private function onSelect(tabName:String) {
		_global.tr("onSelect tabName = "+tabName);
		var eventObject = {type:"tabSelChange", target:this, tabName:tabName};
		this.dispatchEvent(eventObject);
	}
	
	public function SetSel(tabName:String) {
		//_global.tr("SetSel tabName = "+tabName+", curSel = "+curSel);
		if (tabName == curSel) return;
		
		if (tabName == "design") {
			mcTab1.face._visible = true;
			mcTab2.face._visible = false;
			DepthControl.bringForward(mcTab1);
			curSel = tabName;
		}
		else if (tabName == "preview") {
			mcTab1.face._visible = false;
			mcTab2.face._visible = true;
			DepthControl.sendBackward(mcTab1);
			curSel = tabName;
		}
	}
	
	public function RegisterOnSelChangeHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("tabSelChange", Delegate.create(scopeObject, callBackFunction));
    }
}