﻿class Workspace.WorkspaceTabUIController {
	private var savedCurrentUnit = null;
	
	function WorkspaceTabUIController() {
	}
	
	function OnComponentLoaded(compName:String, compObj:Object, param) {
		compObj.RegisterOnSelChangeHandler(this, onSelChange);
	}
	
	function onSelChange(eventObject) {
		Preloader.Hide();
		if (eventObject.tabName == "design") {
			_global.UIController.PreviewAreaController.Clear();
			_global.MainWindow.Mode = "design";
			if (savedCurrentUnit != null) {
				_global.Project.CurrentUnit = savedCurrentUnit;
				savedCurrentUnit = null;
			}
			
			var unit=_global.Project.CurrentUnit;
			
			_global.Project.CurrentPaper.CurrentFace.ChangeCurrentUnit(null);
			_global.Project.CurrentPaper.CurrentFace.ChangeCurrentUnit(unit);
			//for(var i in unit)
				//trace('unit i = '+i+ ' ==> '+unit[i]);
			if(unit.frameLinkageName == "FrameEx")
			{
				trace("Unit"+unit+ "unittexteffect");
				unit.UpdateTextEffect();
			}
			
			trace("Ontabselchage"+_global.Project.CurrentUnit);	
			_global.Project.CurrentPaper.CurrentFace.RedrawPaintUnits(); 
		}
		else if (eventObject.tabName == "preview") {
			if(_global.Mode == _global.PaintMode)
				_global.UIController.ToolsController.paintToolController.paintAddTool_onEnd();
			savedCurrentUnit = _global.Project.CurrentUnit == null || _global.Project.CurrentUnit == undefined ? null : _global.Project.CurrentUnit;
			_global.UIController.UnselectCurrentUnit();
			SATracking.Preview();
			
			ChangeMode("preview");
		}
	}
	
	function ChangeMode(mode:String):Void {
		var intervalObject = new Object();
		intervalObject.mode = mode;
		var id = setInterval(this, "SetMode", 100, intervalObject);
		intervalObject.id = id;
	}
	
	function SetMode(intervalObject) {
		clearInterval(intervalObject.id);
		_global.MainWindow.Mode = intervalObject.mode;
	}
}
