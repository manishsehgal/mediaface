﻿import mx.utils.Delegate;
import mx.core.UIObject;
import mx.controls.TextArea;
import flash.display.BitmapData;
import flash.filters.ColorMatrixFilter;
import flash.geom.ColorTransform;
import flash.geom.Rectangle;
import flash.geom.Matrix;
import flash.geom.Point;
			
class Workspace.PreviewArea  extends UIObject {
	private var maxWidth    : Number;
	private var maxHeight   : Number;
	private var mcPaper     : MovieClip;
	private var mcHtmltext  : MovieClip;

	
	private var HtmlTextWidth:Number = 210;
	
	function PreviewArea() {
		instance = this;
	}
	
	static private var instance   : Workspace.PreviewArea;
	static function getInstance() : Workspace.PreviewArea {
		return instance;
	}
	
	private function onLoad() : Void {
		maxWidth  = this._width - HtmlTextWidth - 4;
		maxHeight = this._height - 4;
		_global.GlobalNotificator.OnComponentLoaded("PreviewArea", this);
		CreateHtmlText();
	}

	function CreateHtmlText() : Void {
		mcHtmltext = this.createClassObject(TextArea, "Htmltext", this.getNextHighestDepth());
		
		if (mcHtmltext.getDepth() > PaperMC.getDepth())
			mcHtmltext.swapDepths(PaperMC);
		
		mcHtmltext.setStyle("borderStyle", "none");
		mcHtmltext.setStyle("backgroundColor", "0xf7f7f7");
		mcHtmltext.setStyle("fontSize", 8);
		mcHtmltext.html = false;
		mcHtmltext.wordWrap = true;
		mcHtmltext.move(maxWidth + 2 + 7, 210 + 2);
		mcHtmltext.setSize(HtmlTextWidth - 14, maxHeight);
		var styles = new TextField.StyleSheet();
		styles.setStyle("p",
			{fontSize: '11', display: 'block'}
		);
		styles.setStyle("div",
			{fontSize: '11'}
		);
		styles.setStyle("a",
			{color: '#0000ff',
			 textDecoration: 'underline'}
		);
		mcHtmltext.styleSheet=styles;
		mcHtmltext.html = true;
	}
	
	function set HtmlText(value:String):Void {
		mcHtmltext.visible = !(value == "" || value == null || value == undefined);
		mcHtmltext.text = mcHtmltext.visible ? value : "";
	}
	
	function get Visible() : Boolean {
		return this._parent._visible;
	}
	function set Visible(vis:Boolean) {
		var prevVis : Boolean = this._visible;
		this._visible = vis;
		this._parent._visible = vis;
		mcHtmltext.move(maxWidth + 2 + 7, _global.Project.CurrentPaper.Faces.length > 1 ? 212 : 7);
		mcHtmltext.setSize(mcHtmltext._width, _global.Project.CurrentPaper.Faces.length > 1 ? (maxHeight - 212) : maxHeight);
		if (prevVis != vis) {
			var eventObject:Object = {type:"setVisible", target:this, visiblility:vis};
			this.dispatchEvent(eventObject);
		}
	}
	
	function get PaperMC() : MovieClip {
		return mcPaper;
	}
	
	public function RegisterOnSetVisibleHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("setVisible", Delegate.create(scopeObject, callBackFunction));
    }
    
	function Center(width:Number, height:Number) : Void {
		var w:Number = mcHtmltext.visible ? 0 : HtmlTextWidth;
		var kx:Number = (maxWidth + w) / width  * 100;
		var ky:Number = maxHeight / height * 100;
		var k:Number = kx <= ky ? kx : ky;
		this.PaperMC._xscale = this.PaperMC._yscale = k;
		var dx:Number = (maxWidth + w) - width * k / 100;
		var dy:Number = maxHeight - height * k / 100;
		this.PaperMC._x = dx / 2;
		this.PaperMC._y = dy / 2;
	}
	
	private function OnFaceClick(index:Number) : Void {
		var eventObject:Object = {type:"faceClick", target:this._parent, faceIndex:index};
		this._parent.dispatchEvent(eventObject);
	}

	public function RegisterOnFaceClickHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("faceClick", Delegate.create(scopeObject, callBackFunction));
    }
    
	public function DataBind(data:Object) : Void {
		HtmlText = data.htmlText;
		trace('DataBind');
		
		var printRealContour:Boolean = false;
		var printMarkLines:Boolean = false;
		var printCutLines:Boolean = data.paperType == "Universal";
				
		if (data.calibrateOnly != true) {
			PrintHelper.NewPrintPreview();
			this.createEmptyMovieClip(PaperMC._name, PaperMC.getDepth());
			// watermark is printed differently on DirectToCD and at other papers
			if(_global.Project.CurrentPaper.IsDirectToCDDevice){
				_global.tr('Special printing for direct to cd');
				_global.PaperHelper.Draw(PrintHelper.PrintPreview, PrintHelper.PrintPreview, null, false, false, data.faces, data.width, data.height, true, printRealContour, printMarkLines, printCutLines);
			} else {
				_global.tr('Normal printing preview');
				_global.PaperHelper.Draw(PrintHelper.PrintPreview, PrintHelper.PrintPreview, null, false, false, data.faces, data.width, data.height, false, printRealContour, printMarkLines, printCutLines);
				_global.PaperHelper.DrawWaterMark(PrintHelper.PrintPreview, data.width, data.height);
			}
			//
			
			_global.PaperHelper.Draw(PaperMC, PaperMC, OnFaceClick, false, false, data.faces, data.width, data.height, false, printRealContour, printMarkLines, printCutLines);
			_global.PaperHelper.DrawWaterMark(PaperMC, data.width, data.height);
			this.Center(data.width, data.height);
		}
		var calibrateContours:Boolean = _global.Project.CurrentPaper.IsDirectToCDDevice || data.paperType == "Universal";
		_global.PaperHelper.Calibrate(PrintHelper.PrintPreview, data.x, data.y, data.faces, calibrateContours);
		calibrateContours = _global.Project.CurrentPaper.IsDirectToCDDevice;
		_global.PaperHelper.Calibrate(PaperMC, data.x, data.y, data.faces, calibrateContours);

		if (data.isLightscribeDevice == true) {
			//var filters:Array = PaperMC.filters;
			//filters.push(new ColorMatrixFilter(SepiaMatrix));
			//PaperMC.filters = filters;
		}
	}
	
	public function GetPreviewBitmapData(x:Number, y:Number, width:Number, height:Number, dx:Number, dy:Number, k:Number, scanObj:Object, callerObj:Object, onImageReady:Function):Void {
		//_global.tr("GetPreviewBitmapData(x="+x+",y="+y+",width="+width+",height="+height+",dx="+dx+",dy="+dy+",k="+k);		
		var bmp:BitmapData = new BitmapData(width * k, height * k);
		bmp.draw(PrintHelper.PrintPreview, new Matrix(k, 0, 0, k, (-x - dx) * k, (-y - dy) * k));//, new ColorTransform(), new Object(), new Rectangle(0, 0, width, height));
		bmp.applyFilter(bmp, bmp.rectangle, new Point(0, 0), new ColorMatrixFilter(Grayscale2BlueMatrix));

		var radiuses:Array = new Array();
		var contour:Array = _global.Project.CurrentPaper.CurrentFace.contours[0].Contour();
		for (var i:Number = 0; i < contour.length; ++i) {
			var primitive = contour[i];
			if (primitive instanceof CirclePrimitive) {
				radiuses.push(primitive.r);
			}
		}
		radiuses.sort(Array.NUMERIC | Array.DESCENDING);

		var smallRadius:Number = 0;
		var largeRadius:Number = 0;
		
		for (i = 0; i < radiuses.length; i += 2) {
			if (radiuses[i] != radiuses[i+1]) {
				largeRadius = radiuses[i];
				smallRadius = radiuses[i+1];
			}
		}

		var n2str:Array = new Array(256);
		for(var i:Number=0;i<16;i++)
			for(var j:Number=0;j<16;j++)
				n2str[i*16+j]=String.fromCharCode(i+65)+String.fromCharCode(j+65);
		
		//_global.tr("smallRadius = " + smallRadius + " largeRadius = " + largeRadius);
		
		var timerData:Object = new Object();
		timerData.bmp = bmp;
		timerData.largeRadius = largeRadius * k;
		timerData.smallRadius = smallRadius * k;
		timerData.centerX = bmp.width / 2;
		timerData.centerY = bmp.height / 2;
		timerData.startI = Math.floor(timerData.centerY - timerData.largeRadius);
		timerData.endY = Math.min(timerData.centerY + timerData.largeRadius,bmp.height);
		timerData.currentI = timerData.startI;
		timerData.result = "";
		timerData.n2str = n2str;
		timerData.delta = new Array(new Array(0,0),new Array(1,1),new Array(1,0),new Array(0,1));
		timerData.encoding = "SQRT_R2C2_PROGR";
		timerData.blockSize = 100000;

		scanObj.callerObj = callerObj;
		scanObj.onImageReady = onImageReady;
		scanObj.timerData = timerData;
		scanObj.doScan = true;
		scanObj.passN = 0;
		var lR:Number = timerData.largeRadius+1;
		var sR:Number = timerData.smallRadius;
		scanObj.scanTotal = Math.round(Math.PI*(lR*lR-sR*sR));
		scanObj.scanDone = 0;

		scanObj.timer = getTimer();			

		var intervalId:Number = setInterval(this, "GetBitmapData", 5, scanObj);
		scanObj.IntervalId = intervalId;
	}
	
	private function GetBitmapData(scanObj:Object):Void {
		//_global.tr("GetBitmapData, bmpWidth="+scanObj.timerData.bmp.width+" bmpHeight="+scanObj.timerData.bmp.height+" endY="+scanObj.timerData.endY+" currentI="+scanObj.timerData.currentI);

		clearInterval(scanObj.IntervalId);

		var time2:Number = getTimer();
		//_global.tr("time between loops "+(time2 - scanObj.timer2));
		scanObj.timer2 = time2;

		if( !(scanObj.doScan && scanObj.passN < 4) ) {
			traceScanEnd(scanObj);
			onBmpData(scanObj);
			return;
		}

		var timerData = scanObj.timerData;

		// to improve performance:
		//   a) avoid calling user-defined functions in the loop 
		//   b) avoid adding small pieces to long string in the loop
		//   c) use local (register) variables instead of named variables
		//   d) do not use any number<->string conversion functions (they work slow)

		var bmp:BitmapData = timerData.bmp;
		var bmpWidth:Number = bmp.width;
		var bmpHeight:Number = bmp.height;
		var centerX:Number = timerData.centerX;
		var centerY:Number = timerData.centerY;
		var endY:Number = timerData.endY;
		var largeRadius:Number = timerData.largeRadius;
		var smallRadius:Number = timerData.smallRadius;
		var currentI:Number = timerData.currentI;
		var n2str:Array = timerData.n2str;
		var blockSize = timerData.blockSize;

		var delta = timerData.delta;
		var deltaX:Number = delta[scanObj.passN][0];
		var deltaY:Number = delta[scanObj.passN][1];

		var largeR2:Number = largeRadius*largeRadius;
		var smallR2:Number = smallRadius*smallRadius;

		var blockResult:String = "";

		var largeB:Number;
		var largeE:Number;
		var smallB:Number;
		var smallE:Number;

		var color:Number;
		var dx:Number;
		var dy:Number;
		var dy2:Number;

		for ( ; blockResult.length < blockSize && currentI < endY; currentI+=2 ) {

			dy = centerY - currentI;
			dy2 = dy*dy;
			
			if (dy2 > largeR2) 
				continue;

			dx = Math.sqrt(largeR2 - dy2);
			largeB = Math.floor((centerX - dx - deltaX)/2)*2 + deltaX;
			largeE = Math.floor(centerX + dx);

			var lineResult:String = "";

			if (dy2 > smallR2) {
				for(var j:Number = largeB; j<=largeE; j+=2)
					lineResult += n2str[bmp.getPixel(j,currentI) /* & 0xFF */];
			} else {
				dx = Math.sqrt(smallR2 - dy2);
				smallB = Math.floor(centerX - dx);
				smallE = Math.floor((centerX + dx - deltaX)/2)*2 + deltaX;

				var j:Number = largeB;
				for(var j:Number = largeB; j<=smallB; j+=2)
					lineResult += n2str[bmp.getPixel(j,currentI) /* & 0xFF */];

				for(var j:Number = smallE; j<=largeE; j+=2)
					lineResult += n2str[bmp.getPixel(j,currentI) /* & 0xFF */];

			}

			blockResult += lineResult;
		}
		
		timerData.currentI = currentI;
		timerData.result += blockResult;

		scanObj.scanDone = timerData.result.length/2;

		onBmpData(scanObj);

		if (currentI >= endY) {
			if(++scanObj.passN < 4) {
				timerData.currentI = timerData.startI + delta[scanObj.passN][1];
			} else {
				scanObj.scanTotal = scanObj.scanDone;
			}
		}

		var time2:Number = getTimer();
		var timeUsed:Number = time2 - scanObj.timer;
		if(timeUsed>500) 
			timerData.blockSize = Math.round(scanObj.scanDone*500/timeUsed);
		//_global.tr("time in loop "+(time2 - scanObj.timer2)+" scanned="+blockResult.length+" i="+currentI+" end="+endY+" newBlock="+timerData.blockSize);
		scanObj.timer2 = time2;
		scanObj.timeUsed = timeUsed;

		var intervalId:Number = setInterval(this, "GetBitmapData", 5, scanObj);
		scanObj.IntervalId = intervalId;

	}

	private function traceScanEnd(scanObj:Object) : Void {
		//_global.tr("LS scan, algorithm: SQRT_R2C2 progressive");
		_global.tr("LS scanTime=" + (getTimer()-scanObj.timer) + " ms, scanned "+Math.round(scanObj.scanDone/scanObj.scanTotal*100)+"%="+scanObj.scanDone+"/"+scanObj.scanTotal);
	}

	private function onBmpData(scanObj:Object):Void {
		var eventObject:Object = {type:"bmpData", target:this};
		//this.dispatchEvent(eventObject);
		// user events are sometimes lost - so use more reliable method:
		scanObj.onImageReady.call(scanObj.callerObj,scanObj);
	}
	
//	public function RegisterOnBmpDataHandler(scopeObject:Object, callBackFunction:Function) {
//		this.addEventListener("bmpData", Delegate.create(scopeObject, callBackFunction));
//    }
	
	public static function get GrayscaleMatrix() {
		var rwgt:Number = 0.3086;
		var gwgt:Number = 0.6094;
		var bwgt:Number = 0.0820;
		
		var matrix:Array = [
		rwgt, gwgt, bwgt, 0, 0,
		rwgt, gwgt, bwgt, 0, 0,
		rwgt, gwgt, bwgt, 0, 0,
		0.0, 0.0, 0.0, 1.0, 0.0
		];
		return matrix;
	}

	public static function get Grayscale2BlueMatrix() {
		var rwgt:Number = 0.3086;
		var gwgt:Number = 0.6094;
		var bwgt:Number = 0.0820;
		
		var matrix:Array = [
		0.0,  0.0,  0.0,  0, 0,
		0.0,  0.0,  0.0,  0, 0,
		rwgt, gwgt, bwgt, 0, 0,
		0.0, 0.0, 0.0, 1.0, 0.0
		];
		return matrix;
	}

	public static function get SepiaMatrix() {
		var matrix:Array = [
		0.393, 0.769, 0.189, 0, 0,
		0.349, 0.686, 0.168, 0, 0,
		0.272, 0.534, 0.131, 0, 0,
		0.0, 0.0, 0.0, 1.0, 0.0
		];
		return matrix;
	}
	
	public static function get NormalMatrix() {
		var matrix:Array = [
		1, 0, 0, 0, 0,
		0, 1, 0, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 0, 1, 0
		];
		return matrix;
	}
}
