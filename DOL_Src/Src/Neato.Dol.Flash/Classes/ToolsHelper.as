﻿class ToolsHelper {
	private var items:Array;
	
	function ToolsHelper(toolXml:XML) {
		items = new Array();
		var rootNode:XMLNode = toolXml.firstChild;
		for (var itemNode:XMLNode = rootNode.firstChild; itemNode != null; itemNode = itemNode.nextSibling) {
			var obj:Object = new Object();
			for (var node:XMLNode = itemNode.firstChild; node != null; node = node.nextSibling) {
				switch(node.nodeName) {
					case "Label" :
						obj.label = node.firstChild.nodeValue;
						break;
					case "Name" :
						obj.name = node.firstChild.nodeValue;
						break;
					case "EditToolUrl" :
						obj.editUrl = node.firstChild.nodeValue;
						break;
					case "AddToolUrl" :
						obj.addUrl = node.firstChild.nodeValue;
						break;
					case "ShowTool":
						obj.visibility = node.firstChild.nodeValue == 'true' ? true: false;
						break;
				}
			}
			items.push(obj);
		}
	}
	
	public function get EditUrls():Array {
		var arr:Array = new Array();
		for(var i = 0; i < items.length; ++i) {
			arr.push(items[i].editUrl);
		}
		return arr;
	}
	
	public function get AddUrls():Array {
		var arr:Array = new Array();
		for(var i = 0; i < items.length; ++i) {
			arr.push(items[i].addUrl);
		}
		return arr;
	}
	
	public function get Names():Array {
		var arr:Array = new Array();
		for(var i = 0; i < items.length; ++i) {
			arr.push(items[i].name);
		}
		return arr;
	}
	
	public function get Labels():Array {
		var arr:Array = new Array();
		for(var i = 0; i < items.length; ++i) {
			arr.push(items[i].label);
		}
		return arr;
	}
	public function get Visibility():Array {
		var arr:Array = new Array();
		for(var i = 0; i < items.length; ++i) {
			arr.push(items[i].visibility);
		}
		return arr;
	}
	
	public function get Length():Number {
		return items.length;
	}
	
	public function GetEditUrlAt(index:Number):String {
		return items[index].editUrl;
	}
	
	public function GetAddUrlAt(index:Number):String {
		return items[index].addUrl;
	}
	
	public function GetNameAt(index:Number):String {
		return items[index].name;
	}
	
	public function GetLabelAt(index:Number):String {
		return items[index].label;
	}
	public function GetVisibilityAt(index:Number):Boolean {
		return items[index].visibility;
	}
	
	public function HasCommandsAt(index:Number):Boolean {
		var url:String = GetAddUrlAt(index);
		return (url != undefined && url != null && url != "");
	}
}