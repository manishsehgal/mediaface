﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;
[Event("onLoaded")]
class SAPrinter {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SAPrinter.prototype);

	private  var printers:XML;
	
	public static function GetInstance(printers:XML):SAPrinter {
		return new SAPrinter(printers);
	}

	public function get IsLoaded():Boolean {
		return printers.loaded;
	}
	
	private function SAPrinter(printers:XML) {
		var parent = this;
		
		this.printers = printers;
		this.printers.onLoad = function(success) {
			var eventObject = {type:"onLoaded", target:parent};
			trace("SAPrinter: dispatch printers onLoaded");
			parent.dispatchEvent(eventObject);
		};
	}

	public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }

	public function BeginLoad() {
		var currentDate:Date = new Date();
		var url:String = "Printers.xml";
		if (SALocalWork.IsLocalWork)
			url = "../Neato.Dol.WebDesigner/Printers.xml";
		
		printers.load(url);
    }
    
	public static function get Printers():Array {
		var arrPrinters:Array = new Array();
		
		for (var itemNode:XMLNode = _global.printersXml.firstChild.lastChild; itemNode != null; itemNode = itemNode.previousSibling) {
			var printerName:String = itemNode.attributes.name;
			arrPrinters.push(printerName);
		}
		
		return  arrPrinters;
    }
    
	public static function GetPrinterShifts(printer:String):Object {
		var printerShifts:Object = new Object({xshift:0,  yshift:0});
		var findPrinter:Boolean = false;
		
		for (var itemNode:XMLNode = _global.printersXml.firstChild.firstChild; itemNode != null; itemNode = itemNode.nextSibling) {
			var printerName:String = itemNode.attributes.name;
			var xshift:String = itemNode.attributes.xshift;
			var yshift:String = itemNode.attributes.yshift;
			
			if (printerName == printer) { 
				findPrinter = true; printerShifts.xshift = xshift; printerShifts.yshift = yshift; break;
				}
		}
		
		return  (findPrinter) ? printerShifts : null;
    }
}