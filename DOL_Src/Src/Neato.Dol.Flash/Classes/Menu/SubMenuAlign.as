﻿import mx.utils.Delegate;
import CtrlLib.ButtonEx;

class Menu.SubMenuAlign extends Menu.SubMenuBase {
	
	private var btnAlignHL : ButtonEx;
	private var btnAlignHC : ButtonEx;
	private var btnAlignHR : ButtonEx;
	private var btnAlignVT : ButtonEx;
	private var btnAlignVC : ButtonEx;
	private var btnAlignVB : ButtonEx;
	
	function SubMenuAlign() {
		super();
	}
	
	function onLoad() {
		btnAlignHL.addEventListener("click", Delegate.create(this, onButtonClick));
		btnAlignHC.addEventListener("click", Delegate.create(this, onButtonClick));
		btnAlignHR.addEventListener("click", Delegate.create(this, onButtonClick));
		btnAlignVT.addEventListener("click", Delegate.create(this, onButtonClick));
		btnAlignVC.addEventListener("click", Delegate.create(this, onButtonClick));
		btnAlignVB.addEventListener("click", Delegate.create(this, onButtonClick));
		
		TooltipHelper.SetTooltip2(btnAlignHL, "IDS_TOOLTIP_ALIGNHL");
		TooltipHelper.SetTooltip2(btnAlignHC, "IDS_TOOLTIP_ALIGNHC");
		TooltipHelper.SetTooltip2(btnAlignHR, "IDS_TOOLTIP_ALIGNHR");
		TooltipHelper.SetTooltip2(btnAlignVT, "IDS_TOOLTIP_ALIGNVT");
		TooltipHelper.SetTooltip2(btnAlignVC, "IDS_TOOLTIP_ALIGNVC");
		TooltipHelper.SetTooltip2(btnAlignVB, "IDS_TOOLTIP_ALIGNVB");
	}
}