﻿import mx.utils.Delegate;
import CtrlLib.ButtonEx;

class Menu.SubMenuMove extends Menu.SubMenuBase {
	
	private var btnMoveLeft  : ButtonEx;
	private var btnMoveDown  : ButtonEx;
	private var btnMoveUp    : ButtonEx;
	private var btnMoveRight : ButtonEx;
	
	function SubMenuMove() {
		super();
	}
	
	function onLoad() {
		btnMoveLeft.addEventListener("click", Delegate.create(this, onButtonClick));
		btnMoveDown.addEventListener("click", Delegate.create(this, onButtonClick));
		btnMoveUp.addEventListener("click", Delegate.create(this, onButtonClick));
		btnMoveRight.addEventListener("click", Delegate.create(this, onButtonClick));
		
		TooltipHelper.SetTooltip2(btnMoveLeft, "IDS_TOOLTIP_MOVELEFT");
		TooltipHelper.SetTooltip2(btnMoveDown, "IDS_TOOLTIP_MOVEDOWN");
		TooltipHelper.SetTooltip2(btnMoveUp, "IDS_TOOLTIP_MOVEUP");
		TooltipHelper.SetTooltip2(btnMoveRight, "IDS_TOOLTIP_MOVERIGHT");
	}
}