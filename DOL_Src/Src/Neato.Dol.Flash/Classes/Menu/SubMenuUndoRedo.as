import mx.utils.Delegate;
import CtrlLib.ButtonEx;

class Menu.SubMenuUndoRedo extends Menu.SubMenuBase {

	private var btnUndo   : ButtonEx;
	private var btnRedo   : ButtonEx;
	
	function SubMenuUndoRedo() {
		super();
	}

	function onLoad() {
		btnUndo.addEventListener("click", Delegate.create(this, onButtonClick));
		btnUndo.setStyle("color", 0x0);
		btnRedo.addEventListener("click", Delegate.create(this, onButtonClick));
		btnRedo.setStyle("color", 0x0);
		btnUndo.enabled = false;
		btnRedo.enabled = false;
		
		TooltipHelper.SetTooltip2(btnUndo, "IDS_TOOLTIP_MENUUNDO");
		TooltipHelper.SetTooltip2(btnRedo, "IDS_TOOLTIP_MENUREDO");
	}
	
	function DataBind(data:Object) : Void {
//		_global.tr("### SubMenuUndoRedo.DataBind");
//		for(var i in  data)
//			_global.tr("### " + i + " = " + data[i]);
//		_global.tr("### ________________________");
		btnUndo.enabled = data.CanUndo;
		btnRedo.enabled = data.CanRedo;
	}
}