﻿import mx.utils.Delegate;
import mx.core.UIObject;

class Menu.ConfigPlugins extends UIObject {
	
	private var lstModules : CtrlLib.ListEx;
	private var btnHide    : CtrlLib.ButtonEx;
	private var pluginsService;
	private var mcLoadingInfo: MovieClip;
	private var mcNoPlugins: MovieClip;
	private var showMessageForCorePlugin:Boolean = true;
	private var itemsToUpdate:Array = new Array();
	private var updatingFile:String = null;
	
	public function ConfigPlugins() {
		this.pluginsService = SAPlugins.GetInstance();
		mcNoPlugins._visible = false;
	}
	
	public function onLoad() {
		this.lstModules.setStyle("backgroundColor ", 0xd0d0d0);
		this.setStyle("styleName", "MainMenu");
		this.lstModules.rowHeight = 22;
		this.lstModules.selectable = false;
		this.lstModules.vScrollPolicy = "off";
		this.lstModules.cellRenderer = "ConfigCellRenderer";
		this.pluginsService.RegisterOnPluginUpdateInfoReady(this, OnPluginUpdateInfoReady);
		this.pluginsService.RegisterOnFileUpdated(this, OnFileUpdated);
		this.pluginsService.LoadServerPluginInfo(false);
		this.btnHide.RegisterOnClickHandler(this, btnHide_Click);
		
		TooltipHelper.SetTooltip2(btnHide, "IDS_TOOLTIP_HIDE");
	}

	public function Show_Hide() {
		//var pnlPluginsConfig = _root.pnlMainLayer.content.pnlPluginsConfig;
		var pnlPluginsConfig = this._parent._parent;
		pnlPluginsConfig._visible = !pnlPluginsConfig._visible;
		if(pnlPluginsConfig._visible) {
			SATracking.Plugins();
		}
		this.Show(pnlPluginsConfig._visible);
	}
	
	public function Show(vis:Boolean) {
		if (vis == true) {
			var isInstalled = SAPlugins.IsInstalled();
			var isAvailable = SAPlugins.IsAvailable();
			if(isAvailable) {
				mcLoadingInfo._visible = false;
				mcNoPlugins._visible = false;
			} else {
				mcLoadingInfo._visible = isInstalled;
				mcNoPlugins._visible = !isInstalled;
			}
			this.pluginsService.LoadServerPluginInfo(isInstalled);
		}
	}
    
	public function OnPluginUpdateInfoReady(eventObject) {
		var infoReady:Boolean = eventObject.status == true;
		mcLoadingInfo._visible = false;
		mcNoPlugins._visible = !infoReady;
		if(infoReady) {
			//this.btnHide._y = 224;
			this.doLater(this, "FillData");
		} 
	}
	
	private function FillData() {
		var pluginInfo:Array = pluginsService.GetPluginUpdateInfo();
		if (showMessageForCorePlugin) {
			pluginsService.IsModuleAvailable("CORE");
			showMessageForCorePlugin = false;
		}
   		this.lstModules.removeAll();
		this.lstModules.rowCount = 8;//pluginInfo.length;
		for (var i=0; i<pluginInfo.length; i++) {
			var item = new Object();
			//item.pluginName = pluginInfo[i].pluginName;
			item.label = pluginInfo[i].name;
			item.data  = pluginInfo[i].state;
			
			item.file  = pluginInfo[i].file;
			item.depends = pluginInfo[i].depends;
			item.dependsState = pluginInfo[i].dependsState;
			item.callback = Delegate.create(this, DoUpdate);
			
			if (updatingFile == item.file) {
				item.data = "Updating";
			}
			else {
				for(var j:Number = 0; j < itemsToUpdate.length; ++ j)
					if (itemsToUpdate[j].file == item.file) {
						item.data = "Updating";
						break;
					}
			}
			
			if (_global.Project.CurrentPaper.IsLightscribeDevice != false || pluginInfo[i].pluginName != "LightscribeModule") {
				this.lstModules.addItem(item);
			}
		}
	}
	
	public function DoUpdate(item) {
		item.data  = "Updating";
		lstModules.selectedIndex = 0;
		itemsToUpdate.push(item);
		if (updatingFile == null) {
			BeginUpdate();
		}
	}
	
	private function BeginUpdate() {
		if (itemsToUpdate.length > 0) {
			var item = itemsToUpdate[0];
			itemsToUpdate.splice(0, 1);
			updatingFile = item.file;
			pluginsService.DoUpdate(item.file, item.depends);
		}
	}
    
	public function OnFileUpdated(eventObject) {
		if (itemsToUpdate.length == 0) {
			updatingFile = null;
		}
		else {
			BeginUpdate();
		}
		this.pluginsService.LoadServerPluginInfo(false);
	}

	private function btnHide_Click(eventObject):Void {
		_root.pnlMainLayer.content.pnlPluginsConfig._visible = false;
	}
}