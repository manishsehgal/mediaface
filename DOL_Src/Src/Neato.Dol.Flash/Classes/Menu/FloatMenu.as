﻿import mx.utils.Delegate;
import mx.core.UIObject;

class Menu.FloatMenu extends UIObject {

	private var menuZoom   : Menu.SubMenuBase;
	private var menuClear  : Menu.SubMenuBase;
	
	private var isEnabled : Boolean;
	
	function FloatMenu() {
		instance = this;
		isEnabled = true;
	}
	
	static private var instance : Menu.FloatMenu;
	static function getInstance() : Menu.FloatMenu {
		return instance;
	}
	
	function set Enabled(val:Boolean) {
		isEnabled = val;
		menuZoom.enabled = val;
	}
	
	function onLoad() {
		//this.setStyle("styleName", "MainMenu");
		
		trace("menuZoom = "+menuZoom);
		menuZoom.RegisterOnCommandHandler(this, onCommand);
		menuClear.RegisterOnCommandHandler(this, onCommand);
		
		_global.GlobalNotificator.OnComponentLoaded("FloatMenu", this);
	}
	
	private function onCommand(eventObject) {
		trace("FloatMenu onCommand");
		if (isEnabled == false) return;
		var eventObject = {type:"menuCommand", target:this, cmdId:eventObject.cmdId};
		this.dispatchEvent(eventObject);
	}
	
	public function RegisterOnCommandHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("menuCommand", Delegate.create(scopeObject, callBackFunction));
    }
}