﻿import mx.utils.Delegate;
import CtrlLib.ButtonEx;

class Menu.SubMenuScale extends Menu.SubMenuBase {
	
	private var btnScaleInc : ButtonEx;
	private var btnScaleDec : ButtonEx;
	
	function SubMenuScale() {
		super();
	}
	
	function onLoad() {
		btnScaleInc.addEventListener("click", Delegate.create(this, onButtonClick));
		btnScaleDec.addEventListener("click", Delegate.create(this, onButtonClick));
		
		TooltipHelper.SetTooltip2(btnScaleInc, "IDS_TOOLTIP_SCALEINC");
		TooltipHelper.SetTooltip2(btnScaleDec, "IDS_TOOLTIP_SCALEDEC");
	}
}