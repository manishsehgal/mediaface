﻿import flash.net.FileReference;
//import mx.utils.Delegate;
class Menu.MainMenuUIController {
	private var mainMenu:Menu.MainMenu;
	private var fileRef:FileReference;
	private var allTypes:Array;
	private var savedCurrentUnit = null;
	private var needToReloadProject:Boolean = false;
	
	private function GetListener():Object {
		var listener:Object = new Object();
		var target = this;
		
		listener.onSelect = function(file:FileReference):Void {
			//_global.tr("onSelect: " + file.name);
			target.savedCurrentUnit = null;
			if (target.needToReloadProject == true) {
				var projectService:SAProject = SAProject.GetInstance(new XML());
				projectService.BeginUploadProject(file);
			}
		};
		
		listener.onCancel = function(file:FileReference):Void {
			if (target.savedCurrentUnit != null) {
				_global.Project.CurrentUnit = target.savedCurrentUnit;
				target.savedCurrentUnit = null;
			}
			
			var unit=_global.Project.CurrentUnit;
			_global.Project.CurrentPaper.CurrentFace.ChangeCurrentUnit(null);
			_global.Project.CurrentPaper.CurrentFace.ChangeCurrentUnit(unit);

			target.needToReloadProject = false;

			//_global.tr("onCancel");
		};
		
		listener.onOpen = function(file:FileReference):Void {
			//_global.tr("onOpen: " + file.name);
		};
		
		listener.onProgress = function(file:FileReference, bytesLoaded:Number, bytesTotal:Number):Void {
			//_global.tr("onProgress with bytesLoaded: " + bytesLoaded + " bytesTotal: " + bytesTotal);
			if (bytesLoaded == bytesTotal) {
//				file.cancel();
//				_global.Project.Load();
			}
		};
		
		listener.onComplete = function(file:FileReference):Void {
			//_global.tr("onComplete: " + file.name + " _global.Project = " + _global.Project);
			//_global.tr("target: " + target + " target.onProjectLoaded = " + target.onProjectLoaded);
			if (target.needToReloadProject == true) {
				BrowserHelper.InvokePageScript("RefreshHeader", "");
				_global.Project.RegisterOnLoadedHandler(target, target.onProjectLoaded);
				_global.Project.Load();
				target.needToReloadProject = false;
			}
		};
		
		listener.onHTTPError = function(file:FileReference, httpError:Number):Void {
			//_global.tr("onHTTPError: " + file.name + " http error = " + httpError);
		};
		
		listener.onIOError = function(file:FileReference):Void {
			BrowserHelper.InvokePageScript("RefreshHeader", "");
			//_global.tr("onIOError: " + file.name);
			//_global.tr("target: " + target + " target.onProjectLoaded = " + target.onProjectLoaded);
			if (target.needToReloadProject == true) {
				_global.Project.RegisterOnLoadedHandler(target, target.onProjectLoaded);
				_global.Project.Load();
				target.needToReloadProject = false;
			}
		};
				
		listener.onSecurityError = function(file:FileReference, errorString:String):Void {
			//_global.tr("onSecurityError: " + file.name + " errorString: " + errorString);
		};
		
		return listener;
	}
	
	function onProjectLoaded():Void {
		//_global.tr("onProjectLoaded");
		_global.UIController.UnselectCurrentUnit();
		_global.Mode = _global.FillMode;
		_global.UIController.ToolsController.OnUnitChanged();
	}
	
	function GetExtension():Array {
		var types:Array = new Array();
		var imageTypes:Object = new Object();
		var extensions:Array = new Array("*." + _global.Project.Extension);
		imageTypes.description = "Project (" + extensions.join(", ") + ")";
		imageTypes.extension = extensions.join("; ");
		types.push(imageTypes);
		return types;
	}
		
	function MainMenuUIController() {
		fileRef = new FileReference();
		fileRef.addListener(GetListener());
	}
	function OnComponentLoaded(compName:String, compObj:Object, param):Void {
		mainMenu = Menu.MainMenu(compObj);
		compObj.RegisterOnCommandHandler(this, onCommand);
	}
	private static function TrackQTCommand(cmdId:String) {
		SATracking.QuickTools(cmdId);
	}
	function onCommand(eventObject:Object):Void {
		if(_global.Mode == _global.PaintMode)
			_global.UIController.ToolsController.paintToolController.paintAddTool_onEnd();
		var cmdIdFull:String = eventObject.cmdId;
		//var cmdId:String = cmdIdFull.substring(4);

		//_global.tr("MainMenu onCommand() cmd="+cmdIdFull);

		switch (cmdIdFull) {
		case "CMD_NEW" :
			newProject();
			break;
		case "CMD_LOAD" :
			loadProject();
			break;
		case "CMD_SAVE" :
			saveProject(commandSave);
			break;
		case "CMD_CONFIG" :
			configPlugins();
			break;
		case "CMD_SCALE_INC" :
			TrackQTCommand("Scale");
			scaleInc();
			break;
		case "CMD_SCALE_DEC" :
			TrackQTCommand("Scale");
			scaleDec();
			break;
		case "CMD_ROTATE_CCW" :
			TrackQTCommand("Rotate");
			rotateCCW();
			break;
		case "CMD_ROTATE_90CCW" :
			TrackQTCommand("Rotate");
			rotate90CCW();
			break;
		case "CMD_ROTATE_90CW" :
			TrackQTCommand("Rotate");
			rotate90CW();
			break;
		case "CMD_ROTATE_CW" :
			TrackQTCommand("Rotate");
			rotateCW();
			break;
		case "CMD_MOVE_LEFT" :
			TrackQTCommand("Move");
			moveLeft();
			break;
		case "CMD_MOVE_DOWN" :
			TrackQTCommand("Move");
			moveDown();
			break;
		case "CMD_MOVE_UP" :
			TrackQTCommand("Move");
			moveUp();
			break;
		case "CMD_MOVE_RIGHT" :
			TrackQTCommand("Move");
			moveRight();
			break;
		case "CMD_ALIGN_HL" :
			TrackQTCommand("Align");
			alignHL();
			break;
		case "CMD_ALIGN_HC" :
			TrackQTCommand("Align");
			alignHC();
			break;
		case "CMD_ALIGN_HR" :
			TrackQTCommand("Align");
			alignHR();
			break;
		case "CMD_ALIGN_VT" :
			TrackQTCommand("Align");
			alignVT();
			break;
		case "CMD_ALIGN_VC" :
			TrackQTCommand("Align");
			alignVC();
			break;
		case "CMD_ALIGN_VB" :
			TrackQTCommand("Align");
			alignVB();
			break;
		case "CMD_LAYER_UP" :
			TrackQTCommand("Layers");
			layerUp();
			break;
		case "CMD_LAYER_DOWN" :
			TrackQTCommand("Layers");
			layerDown();
			break;
		case "CMD_LAYER_TO_FRONT" :
			TrackQTCommand("Layers");
			layerToFront();
			break;
		case "CMD_LAYER_TO_BACK" :
			TrackQTCommand("Layers");
			layerToBack();
			break;
		case "CMD_UNDO" :
			TrackQTCommand("UndoRedo");
			_global.Project.CurrentPaper.CurrentFace.ActManager.Undo();
			break;
		case "CMD_REDO" :
			TrackQTCommand("UndoRedo");
			_global.Project.CurrentPaper.CurrentFace.ActManager.Redo();
			break;
		}
	}
	//=== Command handlers ==================================================
	function newProject():Void {
		BrowserHelper.InvokePageScript("home", "");
	}
	function loadProject():Void {
		savedCurrentUnit = _global.Project.CurrentUnit == null || _global.Project.CurrentUnit == undefined ? null : _global.Project.CurrentUnit;
		_global.UIController.UnselectCurrentUnit();

		allTypes = GetExtension();
		fileRef.browse(allTypes);
		_global.MainWindow.Mode = "design";

		needToReloadProject = true;
	}
	
	private var fileAction:Boolean = false;
	
	function saveProject(callBackFunction:Function):Void {
		savedCurrentUnit = _global.Project.CurrentUnit == null || _global.Project.CurrentUnit == undefined ? null : _global.Project.CurrentUnit;
		_global.UIController.UnselectCurrentUnit();

		var intervalObject:Object = new Object();
		var id:Number = setInterval(this, "SaveProject", 100, intervalObject);
		intervalObject.id = id;
		
		_global.Project.RegisterOnSavedHandler(this, callBackFunction);
	}
	function SaveProject(intervalObject:Object):Void {
		clearInterval(intervalObject.id);
		_global.Project.Save();
		this.fileAction = true;
	}
	function commandSave(eventObject:Object):Void {
		if (fileAction == true) {
			SATracking.SaveDesign();
			BrowserHelper.InvokePageScript("save", "");
			/* There are some problems with project saving under FireFox.
			var projectService:SAProject = SAProject.GetInstance(new XML());
			projectService.BeginDownloadProject(fileRef);
			*/
			fileAction = false;
		}
	}
	function configPlugins():Void {
		var pnlPluginsConfig = _root.pnlMainLayer.content.pnlPluginsConfig;
		pnlPluginsConfig.content.mcConfig.Show_Hide();
	}
	
	public function UndoRedoMenuDataBind(eventObject:Object) : Void {
		mainMenu.MenuUndoRedo.DataBind(eventObject);
	}
	
	// Scaling
	private function ScaleUnitInc() : Void {
		_global.Project.CurrentUnit.ResizeByStep(true);
		_global.SelectionFrame.AttachUnit(_global.Project.CurrentUnit);
	}
	private function ScaleUnitDec() : Void {
		_global.Project.CurrentUnit.ResizeByStep(false);
		_global.SelectionFrame.AttachUnit(_global.Project.CurrentUnit);
	}
	private function ScaleUnit(scaleFunction:Function) : Void {
		var unit = _global.Project.CurrentUnit;
		var scaleXString:String = Actions.ScaleAction.ScaleXProperty(unit);
		var scaleYString:String = Actions.ScaleAction.ScaleYProperty(unit);
		var scaleX:Number = unit[scaleXString];
		var scaleY:Number = unit[scaleYString];
		
		scaleFunction.call(this);
		
		var scaleXNew:Number = unit[scaleXString];
		var scaleYNew:Number = unit[scaleYString];

		var action:Actions.ScaleAction = new Actions.ScaleAction(unit, scaleX, scaleY, scaleXNew, scaleYNew);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	function scaleInc():Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				ScaleUnit(ScaleUnitInc);
			}
		}
	}
	function scaleDec() : Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				ScaleUnit(ScaleUnitDec);
			}
		}
	}
	
	// Rotating
	private function RotateUnit90CCW() : Void {
		_global.RotateToRightAngle(true);
	}
	private function RotateUnit90CW() : Void {
		_global.RotateToRightAngle(false);
	}
	private function RotateUnitCCW() : Void {
		_global.SelectionFrame.Angle -= 1;
	}
	private function RotateUnitCW() : Void {
		_global.SelectionFrame.Angle += 1;
	}
	private function RotateUnit(rotateFunction:Function) : Void {
		var unit = _global.Project.CurrentUnit;
		var angle:Number = unit.Angle;

		rotateFunction.call(this);
		
		var angleNew:Number = unit.Angle;

		var action:Actions.RotateAction = new Actions.RotateAction(unit, angle, angleNew);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	function rotateCCW() : Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				RotateUnit(RotateUnitCCW);
			}
		} else {
			_global.Project.CurrentUnit.RotateCircularCCW();
		}
	}
	function rotate90CCW() : Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				RotateUnit(RotateUnit90CCW);
			}
		} else {
			_global.Project.CurrentUnit.RotateCircular90CCW();
		}
	}
	function rotate90CW() : Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				RotateUnit(RotateUnit90CW);
			}
		} else {
			_global.Project.CurrentUnit.RotateCircular90CW();
		}
	}
	function rotateCW() : Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				RotateUnit(RotateUnitCW);
			}
		} else {
			_global.Project.CurrentUnit.RotateCircularCW();
		}
	}
	
	// Moving
	private function MoveUnitLeft() : Void {
		_global.SelectionFrame.X -= 1;
	}
	private function MoveUnitRight() : Void {
		_global.SelectionFrame.X += 1;
	}
	private function MoveUnitUp() : Void {
		_global.SelectionFrame.Y -= 1;
	}
	private function MoveUnitDown() : Void {
		_global.SelectionFrame.Y += 1;
	}
	private function MoveUnit(moveFunction:Function) : Void {
		var unit = _global.Project.CurrentUnit;
		var x:Number = unit.X;
		var y:Number = unit.Y;

		moveFunction.call(this);

		var xNew:Number = unit.X;
		var yNew:Number = unit.Y;

		var action:Actions.MoveAction = new Actions.MoveAction(unit, x, y, xNew, yNew); 
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	function moveLeft() : Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				MoveUnit(MoveUnitLeft);
			}
		}
	}
	function moveDown() : Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				MoveUnit(MoveUnitDown);
			}
		}
	}
	function moveUp() : Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				MoveUnit(MoveUnitUp);
			}
		}
	}
	function moveRight() : Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				MoveUnit(MoveUnitRight);
			}
		}
	}
	
	// Aligning
	private function AlignUnitHL() : Void {
		_global.Project.CurrentPaper.CurrentFace.StickLeftCurrentObject();
	}
	private function AlignUnitHC() : Void {
		_global.Project.CurrentPaper.CurrentFace.AlignCurrentObjectHorizontally();
	}
	private function AlignUnitHR() : Void {
		_global.Project.CurrentPaper.CurrentFace.StickRightCurrentObject();
	}
	private function AlignUnitVT() : Void {
		_global.Project.CurrentPaper.CurrentFace.StickUpCurrentObject();
	}
	private function AlignUnitVC() : Void {
		_global.Project.CurrentPaper.CurrentFace.AlignCurrentObjectVertically();
	}
	private function AlignUnitVB() : Void {
		_global.Project.CurrentPaper.CurrentFace.StickDownCurrentObject();
	}
	function alignHL() : Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				_global.Project.CurrentPaper.CurrentFace.ActManager.Focused = false; 
				MoveUnit(AlignUnitHL);
			}
		}
	}
	function alignHC() : Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				_global.Project.CurrentPaper.CurrentFace.ActManager.Focused = false; 
				MoveUnit(AlignUnitHC);
			}
		}
	}
	function alignHR() : Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				_global.Project.CurrentPaper.CurrentFace.ActManager.Focused = false; 
				MoveUnit(AlignUnitHR);
			}
		}
	}
	function alignVT() : Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				_global.Project.CurrentPaper.CurrentFace.ActManager.Focused = false; 
				MoveUnit(AlignUnitVT);
			}
		}
	}
	function alignVC() : Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				_global.Project.CurrentPaper.CurrentFace.ActManager.Focused = false; 
				MoveUnit(AlignUnitVC);
			}
		}
	}
	function alignVB() : Void {
		if (_global.Project.CurrentUnit.getEffect().subtype != "Circular") {
			if (_global.Project.CurrentUnit != null) {
				_global.Project.CurrentPaper.CurrentFace.ActManager.Focused = false; 
				MoveUnit(AlignUnitVB);
			}
		}
	}
	
	// Z-Order
	private function LayerUnitUp() : Void {
		_global.Project.CurrentPaper.CurrentFace.ToFrontCurrentUnit();
	}
	private function LayerUnitDown() : Void {
		_global.Project.CurrentPaper.CurrentFace.ToBackCurrentUnit();
	}
	private function LayerUnitToFront() : Void {
		_global.Project.CurrentPaper.CurrentFace.ToFrontAllCurrentUnit();
	}
	private function LayerUnitToBack() : Void {
		_global.Project.CurrentPaper.CurrentFace.ToBackAllCurrentUnit();
	}
	private function LayerUnit(layerFunction:Function) : Void {
		var unit = _global.Project.CurrentUnit;
		var oldDepth:Number = unit.mc.getDepth();

		layerFunction.call(this);

		var newDepth:Number = unit.mc.getDepth();

		var action:Actions.ZOrderAction = new Actions.ZOrderAction(unit, oldDepth, newDepth); 
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	function layerUp() : Void {
		if (_global.Project.CurrentUnit != null) {
			LayerUnit(LayerUnitUp);
		}
	}
	function layerDown() : Void {
		if (_global.Project.CurrentUnit != null) {
			LayerUnit(LayerUnitDown);
		}
	}
	function layerToFront() : Void {
		if (_global.Project.CurrentUnit != null) {
			LayerUnit(LayerUnitToFront);
		}
	}
	function layerToBack() : Void {
		if (_global.Project.CurrentUnit != null) {
			LayerUnit(LayerUnitToBack);
		}
	}
}
