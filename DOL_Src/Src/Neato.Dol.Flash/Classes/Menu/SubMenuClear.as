﻿import mx.utils.Delegate;
import CtrlLib.ButtonEx;

class Menu.SubMenuClear extends Menu.SubMenuBase {
	
	private var btnClear : ButtonEx;
	
	function SubMenuClear() {
		super();
	}
	
	function onLoad() {
		btnClear.addEventListener("click", Delegate.create(this, onButtonClick));
		
		TooltipHelper.SetTooltip2(btnClear, "IDS_TOOLTIP_CLEAR");
	}
}