﻿import mx.utils.Delegate;
import CtrlLib.ButtonEx;

class Menu.SubMenuLayer extends Menu.SubMenuBase {
	
	private var btnLayerUp      : ButtonEx;
	private var btnLayerDown    : ButtonEx;
	private var btnLayerToFront : ButtonEx;
	private var btnLayerToBack  : ButtonEx;
	
	function SubMenuLayer() {
		super();
	}
	
	function onLoad() {
		btnLayerUp.addEventListener("click", Delegate.create(this, onButtonClick));
		btnLayerDown.addEventListener("click", Delegate.create(this, onButtonClick));
		btnLayerToFront.addEventListener("click", Delegate.create(this, onButtonClick));
		btnLayerToBack.addEventListener("click", Delegate.create(this, onButtonClick));
		
		TooltipHelper.SetTooltip2(btnLayerUp, "IDS_TOOLTIP_LAYERUP");
		TooltipHelper.SetTooltip2(btnLayerDown, "IDS_TOOLTIP_LAYERDOWN");
		TooltipHelper.SetTooltip2(btnLayerToFront, "IDS_TOOLTIP_LAYERTOFRONT");
		TooltipHelper.SetTooltip2(btnLayerToBack, "IDS_TOOLTIP_LAYERTOBACK");
	}
}