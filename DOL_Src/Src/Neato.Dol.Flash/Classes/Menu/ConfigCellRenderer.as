﻿import mx.core.UIComponent;
import mx.utils.Delegate;

class Menu.ConfigCellRenderer extends UIComponent
{
	static public var symbolName  : String = "ConfigCellRenderer";
	static public var symbolOwner : Object =  ConfigCellRenderer;
	public var        className   : String = "ConfigCellRenderer";
	
    //private var multiLineLabel; // The label to be used for text.
    private var owner; // The row that contains this cell.
    private var listOwner; // The List, data grid or tree containing this cell.

	private var lblName     : CtrlLib.LabelEx;
	private var btnLoad     : CtrlLib.ButtonEx;
	private var btnUpdate   : CtrlLib.ButtonEx;
	private var btnReinstall: CtrlLib.ButtonEx;
	private var lblOk       : CtrlLib.LabelEx;
	private var lblUpdating : CtrlLib.LabelEx;
	
	private var itemObj:Object;

    public function ConfigCellRenderer() {
    }
    
	public function onLoad() {
		this.setStyle("styleName", "MainMenu");
		this.btnUpdate.setStyle("color", 0xCC0000);
		this.btnUpdate.addEventListener("click", Delegate.create(this, OnUpdate));
		this.btnReinstall.setStyle("color", 0xCC0000);
		this.btnReinstall.addEventListener("click", Delegate.create(this, OnUpdate));
		this.btnLoad.setStyle("color", 0xFF0000);
		this.btnLoad.addEventListener("click", Delegate.create(this, OnUpdate));
		this.lblOk.setStyle("color", 0x00CC00);
		this.lblUpdating.setStyle("color", 0x0000CC);
	}

    public function createChildren():Void {
    }
    
	public function OnUpdate(eventObject) {
		if (itemObj != undefined) {
			itemObj.callback(itemObj);
		}
	}

	public function getPreferredHeight() : Number {
		return owner.height;
    }

    public function setValue(suggestedValue:String, item:Object, selected:String):Void
    {
		if(item == undefined) {
            _visible = false;
			return;
        }
        
        this.itemObj = item;
        if (item.data == undefined) {
        	item.data = "NoChange";
        }
		if(item.data == "NoChange" && item.dependsState != "") {
			var dependsStates:Array = item.dependsState.split(";");
			for(var i:Number = 0; i < dependsStates.length; ++i) {
				if(dependsStates[i] != "NoChange") {
					item.data = "Updated";
					break;
				}
			}
		}
		
        btnUpdate._visible    = (item.data == "Updated");
		btnReinstall._visible = (item.data == "Reinstall");
        btnLoad._visible      = (item.data == "New");
		lblOk._visible        = (item.data == "NoChange");
		lblUpdating._visible  = (item.data == "Updating");
		
		lblName.text = suggestedValue;
		_visible = true;
	
    }
    // function getPreferredWidth :: only for menus and DataGrid headers
    // function getCellIndex :: not used in this cell renderer

}

