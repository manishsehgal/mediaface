﻿import mx.utils.Delegate;
import mx.core.UIObject;
import CtrlLib.ButtonEx;

class Menu.SubMenuBase extends UIObject {
	
	function SubMenuBase() {
	}
	
	public function RegisterOnCommandHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("menuCommand", Delegate.create(scopeObject, callBackFunction));
		this.watch("enabled", enableWatcher);
    }
    
	private function onButtonClick(eventObject) {
		var eventObject = {type:"menuCommand", target:this, cmdId:eventObject.target.CmdId};
		this.dispatchEvent(eventObject);
	}
	
	private function enableWatcher(prop, oldVal, newVal) {
		if (oldVal == newVal)
			return;
			
		for(var i in this) {
			if (this[i] instanceof CtrlLib.ButtonEx)
				this[i].enabled = newVal;
		}
	}
}