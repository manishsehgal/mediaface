﻿import mx.utils.Delegate;
import CtrlLib.ButtonEx;

class Menu.SubMenuRotate extends Menu.SubMenuBase {
	
	private var btnRotateCCW   : ButtonEx;
	private var btnRotate90CCW : ButtonEx;
	private var btnRotate90CW  : ButtonEx;
	private var btnRotateCW    : ButtonEx;
	
	function SubMenuRotate() {
		super();
	}
	
	function onLoad() {
		btnRotateCCW.addEventListener("click", Delegate.create(this, onButtonClick));
		btnRotate90CCW.addEventListener("click", Delegate.create(this, onButtonClick));
		btnRotate90CW.addEventListener("click", Delegate.create(this, onButtonClick));
		btnRotateCW.addEventListener("click", Delegate.create(this, onButtonClick));
		
		TooltipHelper.SetTooltip2(btnRotateCCW, "IDS_TOOLTIP_ROTATECCW");
		TooltipHelper.SetTooltip2(btnRotate90CCW, "IDS_TOOLTIP_ROTATE90CCW");
		TooltipHelper.SetTooltip2(btnRotate90CW, "IDS_TOOLTIP_ROTATE90CW");
		TooltipHelper.SetTooltip2(btnRotateCW, "IDS_TOOLTIP_ROTATECW");
	}
}