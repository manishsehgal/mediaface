﻿import mx.utils.Delegate;
import CtrlLib.ButtonEx;

class Menu.SubMenuZoom extends Menu.SubMenuBase {
	
	private var btnZoomIn  : ButtonEx;
	private var btnZoomOut : ButtonEx;
	private var btnZoomRestore : ButtonEx;
	
	function SubMenuZoom() {
		super();
	}
	
	function onLoad() {
		btnZoomIn.addEventListener("click", Delegate.create(this, onButtonClick));
		btnZoomOut.addEventListener("click", Delegate.create(this, onButtonClick));
		btnZoomRestore.addEventListener("click", Delegate.create(this, onButtonClick));
		
		TooltipHelper.SetTooltip2(btnZoomIn, "IDS_TOOLTIP_ZOOMIN");
		TooltipHelper.SetTooltip2(btnZoomOut, "IDS_TOOLTIP_ZOOMOUT");
		TooltipHelper.SetTooltip2(btnZoomRestore, "IDS_TOOLTIP_ZOOMRESTORE");
	}
}