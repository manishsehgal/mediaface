﻿class Menu.FloatMenuUIController {
	
	function FloatMenuUIController() {
	}
	
	function OnComponentLoaded(compName:String, compObj:Object, param) {
		compObj.RegisterOnCommandHandler(this, onCommand);
	}
	
	function onCommand(eventObject) {
		
		trace("FloatMenuUIController onCommand: eventObject = "+eventObject.cmdId);


		switch (eventObject.cmdId) {
			case "CMD_ZOOM_IN": {
				zoomIn();
				break;
			}
			case "CMD_ZOOM_OUT": {
				zoomOut();
				break;
			}
			case "CMD_ZOOM_RESTORE": {
				zoomRestore();
				break;
			}
			case "CMD_CLEAR": {
				clearFace();
				break;
			}
		}
	}

    //=== Command handlers ==================================================    
	function zoomIn() {
		SATracking.Zoom();
		_global.Project.CurrentPaper.CurrentFace.zoomIn();
	}
	
	function zoomOut() {
		SATracking.Zoom();
		_global.Project.CurrentPaper.CurrentFace.zoomOut();
	}
	
	function zoomRestore() {
		SATracking.Zoom();
		_global.Project.CurrentPaper.CurrentFace.zoomRestore();
	}
	
	function clearFace() {
		_global.MessageBox.Confirm(
				"Are you sure you want to completely clear your design?",
				" Warning",
				clear,
				null);
	}
	
	function clear() {
		var face = _global.Project.CurrentPaper.CurrentFace;
		var faceNode:XML = new XML("<Face/>");
		face.AddUnitNodes(faceNode, true);
		face.Clear();
		
		SATracking.Clear();
		_global.UIController.UnselectCurrentUnit();
		
		var action:Actions.ClearLabelAction = new Actions.ClearLabelAction(face, faceNode); 
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}	
}
