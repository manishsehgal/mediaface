﻿import mx.utils.Delegate;
import mx.core.UIObject;

class Menu.MainMenu extends UIObject {
	
	private var menuFile   : Menu.SubMenuBase;
	private var menuConfig : Menu.SubMenuBase;
	private var menuScale  : Menu.SubMenuBase;
	private var menuRotate : Menu.SubMenuBase;
	private var menuMove   : Menu.SubMenuBase;
	private var menuAlign  : Menu.SubMenuBase;
	private var menuLayer  : Menu.SubMenuBase;
	private var menuUndoRedo: Menu.SubMenuBase;
	private var Bakground  : MovieClip;
	private var Bakground2 : MovieClip;
	//private var mcConfig   : Menu.ConfigPlugins;
		
	private var isEnabled : Boolean;
	
	public function get MenuUndoRedo() : Menu.SubMenuUndoRedo {
		return Menu.SubMenuUndoRedo(menuUndoRedo);
	}
	
	function MainMenu() {
		//this.setStyle("color", 0x00);
		//this.setStyle("fontSize", 11);
		instance = this;
		isEnabled = true;
		//mcConfig._visible = false;
	}
	
	static private var instance : Menu.MainMenu;
	static function getInstance() : Menu.MainMenu {
		return instance;
	}
	
	function set Enabled(val:Boolean) {
		isEnabled = val;
		menuScale.enabled = val;
		menuRotate.enabled = val;
		menuMove.enabled = val;
		menuAlign.enabled = val;
		menuLayer.enabled = val;
		menuUndoRedo.enabled = val;
	}
	
	function set FullMenu(val:Boolean) {
		Bakground._visible = val;
		Bakground2._visible = !Bakground._visible;
	}
		
	function onLoad() {
		this.setStyle("styleName", "MainMenu");
		
		menuFile.RegisterOnCommandHandler(this, onCommand);
		menuConfig.RegisterOnCommandHandler(this, onCommand);
		menuScale.RegisterOnCommandHandler(this, onCommand);
		menuRotate.RegisterOnCommandHandler(this, onCommand);
		menuMove.RegisterOnCommandHandler(this, onCommand);
		menuAlign.RegisterOnCommandHandler(this, onCommand);
		menuLayer.RegisterOnCommandHandler(this, onCommand);
		menuUndoRedo.RegisterOnCommandHandler(this, onCommand);
		
		_global.GlobalNotificator.OnComponentLoaded("MainMenu", this);
	}
	
	private function onCommand(eventObject) {
		var eventObject = {type:"menuCommand", target:this, cmdId:eventObject.cmdId};
		this.dispatchEvent(eventObject);
	}
	
	public function RegisterOnCommandHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("menuCommand", Delegate.create(scopeObject, callBackFunction));
    }
    
    //public function ShowConfig() {
//    	mcConfig.Show(!mcConfig._visible);
  //  }
}