﻿import mx.utils.Delegate;
import CtrlLib.ButtonEx;

class Menu.SubMenuFile extends Menu.SubMenuBase {
	
	private var btnNew   : ButtonEx;
	private var btnLoad  : ButtonEx;
	private var btnSave  : ButtonEx;
	
	function SubMenuFile() {
		super();
	}
	
	function onLoad() {
		btnNew.addEventListener("click", Delegate.create(this, onButtonClick));
		btnNew.setStyle("color", 0xFFFFFF);
		btnLoad.addEventListener("click", Delegate.create(this, onButtonClick));
		btnLoad.setStyle("color", 0xFFFFFF);
		btnSave.addEventListener("click", Delegate.create(this, onButtonClick));
		btnSave.setStyle("color", 0xFFFFFF);
		
		TooltipHelper.SetTooltip2(btnNew, "IDS_TOOLTIP_NEW");
		TooltipHelper.SetTooltip2(btnLoad, "IDS_TOOLTIP_LOAD");
		TooltipHelper.SetTooltip2(btnSave, "IDS_TOOLTIP_SAVE");
	}
}