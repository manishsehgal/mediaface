﻿import mx.utils.Delegate;
import CtrlLib.ButtonEx;

class Menu.SubMenuConfig extends Menu.SubMenuBase {
	
	private var btnConfig: ButtonEx;
	
	function SubMenuConfig() {
		super();
	}
	
	function onLoad() {
		btnConfig.addEventListener("click", Delegate.create(this, onButtonClick));
		btnConfig.setStyle("color", 0xFFFFFF);
		_root.pnlMainLayer.content.pnlPluginsConfig.content.mcConfig.btnHide.RegisterOnClickHandler(this, btnHide_Click);
		
		TooltipHelper.SetTooltip2(btnConfig, "IDS_TOOLTIP_CONFIG");
	}
	
	function btnHide_Click(eventObject) {
		btnConfig.selected = false;
		btnConfig.redraw();
	}
}