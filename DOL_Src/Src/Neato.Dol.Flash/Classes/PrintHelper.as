﻿
import mx.controls.Alert;

class PrintHelper {
	
	private static var instance : PrintHelper = undefined;
	
	static function Print(xml:XML):Void {
//		_global.tr("### $$$$$$ xml = " + xml + "\n\n\n");
				
		if (instance == undefined) {
			instance = new PrintHelper();
		}

		var projectService:SAProject = SAProject.GetInstance(xml);
		projectService.BeginSendToPrint();
	}
	
	static private function OnSaveProject(eventObject) {
		ShowPDF();
	}
	
	static private function ShowPDF():Void {
		SATracking.PrintPDF();
		var currentDate:Date = new Date();
		var url:String = "Print.aspx?time=" + currentDate.getTime();
		getURL(url, "_blank");
	}

	public static function get PrintPreview():MovieClip {
		return _root._parent.printPreview;
	}
	
	public static function NewPrintPreview():Void {
		_root._parent.createEmptyMovieClip(PrintPreview._name, PrintPreview.getDepth());
	}
	
	public static function PrintMC(mc:MovieClip):Void {
		var paper : Entity.Paper = _global.Project.CurrentPaper;
		var pj:PrintJob = new PrintJob();
		
		if(pj.start()) {
			var area:Object = new Object();
			var saveRotation = mc._rotation;
			
			if (pj.orientation == "portrait" && paper.Orientation == "P") {
				if(paper.Width <= paper.Height) {
					area.xMin = (pj.paperWidth-pj.pageWidth)/2;
				    area.xMax = paper.Width;
				    area.yMin = (pj.paperHeight-pj.pageHeight)/2;
			    	area.yMax = paper.Height;
				} 
				else if(paper.Width > paper.Height) {
					area.xMin = (pj.paperHeight-pj.pageHeight)/2;
				    area.xMax = paper.Width;
			    	area.yMin = 0;
				    area.yMax = paper.Height-(pj.paperWidth-pj.pageWidth)/2;
    				mc._rotation = 90;
				}
			}
			else if ((pj.orientation == "landscape" && paper.Orientation == "P") ||
			         (pj.orientation == "portrait" && paper.Orientation == "L")) {
				if(paper.Width > paper.Height) {
					area.xMin = (pj.paperWidth-pj.pageWidth)/2;
				    area.xMax = paper.Width;
				    area.yMin = (pj.paperHeight-pj.pageHeight)/2;
				    area.yMax = paper.Height;
				}
				else if(paper.Width <= paper.Height) {
					area.xMin = 0;
				    area.xMax = paper.Width-(pj.paperHeight-pj.pageHeight)/2;
				    area.yMin = (pj.paperWidth-pj.pageWidth)/2;
				    area.yMax = paper.Height;
    				mc._rotation = -90;
				}
			}
		    else if (pj.orientation == "landscape" && paper.Orientation == "L") {
				if(paper.Width > paper.Height) {
					area.xMin = 0;
				    area.xMax = paper.Width-(pj.paperHeight-pj.pageHeight)/2;
				    area.yMin = (pj.paperWidth-pj.pageWidth)/2;
				    area.yMax = paper.Height;
    				mc._rotation = -90;
				}
				else if(paper.Width <= paper.Height) {
					area.xMin = (pj.paperWidth-pj.pageWidth)/2;
				    area.xMax = paper.Width;
				    area.yMin = (pj.paperHeight-pj.pageHeight)/2;
				    area.yMax = paper.Height;
				}
		    }
			
			if(pj.addPage(mc, area, {printAsBitmap:true})) 
			{
				pj.send();
			}
			mc._rotation = saveRotation;
		}
		delete pj;
	}
}