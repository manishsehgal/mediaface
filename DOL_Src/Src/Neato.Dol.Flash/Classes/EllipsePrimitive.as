﻿class EllipsePrimitive implements IDraw {
	private var x:Number;
	private var y:Number;
	private var rH:Number;
	private var rV:Number;
	private static var exprTan:Number = Math.tan(Math.PI/8);
	private static var exprSin:Number = Math.sin(Math.PI/4);
	
	function EllipsePrimitive(node:XMLNode) {
		x = Number(node.attributes.x);
		y = Number(node.attributes.y);
		rH = Number(node.attributes.rH);
		rV = Number(node.attributes.rV);
	}
	
	function Draw(mc:MovieClip, originalX:Number, originalY:Number,rotationAngle:Number,scaleX:Number,scaleY:Number):Void {
		if(scaleX==undefined)scaleX=1.0;
		if(scaleY==undefined)scaleY=1.0;
		
		var points = new Object();
		points.a =  new Object({x:         rH+x, y:            y});
		points.ab = new Object({x:         rH+x, y: exprTan*rV+y}); points.b = new Object({x:  exprSin*rH+x, y: exprSin*rV+y});
		points.bc = new Object({x: exprTan*rH+x, y:         rV+y}); points.c = new Object({x:             x, y:         rV+y});
		points.cd = new Object({x:-exprTan*rH+x, y:         rV+y}); points.d = new Object({x: -exprSin*rH+x, y: exprSin*rV+y});
		points.de = new Object({x:        -rH+x, y: exprTan*rV+y}); points.e = new Object({x:         -rH+x, y:            y});
		points.ef = new Object({x:        -rH+x, y:-exprTan*rV+y}); points.f = new Object({x: -exprSin*rH+x, y:-exprSin*rV+y});
		points.fg = new Object({x:-exprTan*rH+x, y:        -rV+y}); points.g = new Object({x:             x, y:        -rV+y});
		points.gh = new Object({x: exprTan*rH+x, y:        -rV+y}); points.h = new Object({x:  exprSin*rH+x, y:-exprSin*rV+y});
		points.ha = new Object({x:         rH+x, y:-exprTan*rV+y}); 
		
		//move
		for (var i in points) {
			var point = points[i];
			point.x += originalX;
			point.y += originalY;
		}
		
		//rotate
		if (rotationAngle > 0 || rotationAngle < 0) {
			for (var i in points) {
				var point = points[i];
				var newpoint = _global.RotateAboutCenter(point.x, point.y, originalX, originalY, rotationAngle);
				point.x = newpoint.x;
				point.y = newpoint.y;
			}
		}
		
		//scale
		for (var i in points) {
			var point = points[i];
			point.x *= scaleX;
			point.y *= scaleY;
		}
		
		//draw
		drawEllipse(mc, points);
	}
	
	static private function drawEllipse(mc:MovieClip, points):Void {
		mc.moveTo(points.a.x, points.a.y);
		mc.curveTo(points.ab.x, points.ab.y, points.b.x, points.b.y);
		mc.curveTo(points.bc.x, points.bc.y, points.c.x, points.c.y);
		mc.curveTo(points.cd.x, points.cd.y, points.d.x, points.d.y);
		mc.curveTo(points.de.x, points.de.y, points.e.x, points.e.y);
		mc.curveTo(points.ef.x, points.ef.y, points.f.x, points.f.y);
		mc.curveTo(points.fg.x, points.fg.y, points.g.x, points.g.y);
		mc.curveTo(points.gh.x, points.gh.y, points.h.x, points.h.y);
		mc.curveTo(points.ha.x, points.ha.y, points.a.x, points.a.y);
	}
}