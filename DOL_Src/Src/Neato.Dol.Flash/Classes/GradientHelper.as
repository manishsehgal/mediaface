﻿class GradientHelper {
	private var gradients:Array;
	public function GradientHelper(gradientsXml:XML) {
		gradients = new Array();
		
		for (var itemNode:XMLNode = gradientsXml.firstChild.firstChild; itemNode != null; itemNode = itemNode.nextSibling) {
			var gradient = CreateGradientFromXml(itemNode);
			gradients.push(gradient);
		}
	}
	
	public function CreateGradientFromXml(itemNode:XMLNode):Object {
		var gradient = new Object(); //todo: replace with Gradient object
		
		gradient.name   = itemNode.attributes.name;
		gradient.dir    = itemNode.attributes.dir;
		gradient.points = itemNode.attributes.points;
		gradient.color1 = 0x000000;
		if (itemNode.attributes.color1 != undefined) {
			gradient.color1 = parseInt(itemNode.attributes.color1.substr(1), 16);
		}
		gradient.color2 = 0xffffff;
		if (itemNode.attributes.color2 != undefined) {
			gradient.color2 = parseInt(itemNode.attributes.color2.substr(1), 16);
		}
		
		gradient.drawSequence = new Array();
		gradient.drawSequence.push(new GradientRectPrimitive(itemNode));
		if (gradient.name == "00") {
			var xml:XML = new XML('<root x="10" y="10" w="80" h="80"/>');
			gradient.drawSequence[0] = new RectanglePrimitive(xml.firstChild);
		}
		
		gradient.originalXmlNode = itemNode;
		return gradient;
	}
	
	public function GetGradient(gradientType:String):Object {
		for (var i = 0; i < gradients.length; ++i) {
			if (gradients[i].name == gradientType) {
				var gradient = new Object();
				for(var j in gradients[i])
					gradient[j] = gradients[i][j];
				return gradient;
			}
		}
		return new Object();
	}

	public function GetGradientsArray():Array {
		return gradients;
	}
}
