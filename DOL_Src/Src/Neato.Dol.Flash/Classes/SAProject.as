﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;
[Event("onSaved")]
[Event("onLoaded")]
class SAProject {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SAProject.prototype);

	private var project:XML;
	private var saveResultXml:XML;

	public static function GetInstance(project:XML):SAProject {
		return new SAProject(project);
	}
	
	public function get IsLoaded():Boolean {
		return project.loaded;
	}

	private function SAProject(project:XML) {
		var parent = this;
		trace("SAProject ctr projectXml="+project);
		
		this.project = project;
		this.project.onLoad = function(success) {
			var eventObject = {type:"onLoaded", target:parent, result:this};
			trace("SAProject: dispatch project onLoaded");
			parent.dispatchEvent(eventObject);
		};
		
		this.saveResultXml = new XML();
		this.saveResultXml.onLoad = function(success) {
			var eventObject = {type:"onSaved", target:parent, result:this};
			trace("SAProject: dispatch project onSaved");
			parent.dispatchEvent(eventObject);
		};
	}

	public function BeginLoad() {
        trace("SAProject BeginLoad");
		var currentDate:Date = new Date();
		var url:String = "Project.aspx?type=XmlProject&time=" + currentDate.getTime();
		if (SALocalWork.IsLocalWork)
			url = "Motorola_V635.xml";
		BrowserHelper.InvokePageScript("log", url);
        this.project.load(url);
    }

    public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }

	public function SessionChecked() {
		var currentDate:Date = new Date();
		var url:String = "Project.aspx?type=SaveXmlProject&time=" + currentDate.getTime();
		project.contentType = "text/xml";
		project.sendAndLoad(url, saveResultXml);
	}
	
    public function BeginSave() {
		trace("SAProject BeginSave");
		SASession.WaitForSessionChecked(Delegate.create(this,SessionChecked));
    }

	public function RegisterOnSavedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onSaved", Delegate.create(scopeObject, callBackFunction));
    }

	public function BeginUploadProject(file:flash.net.FileReference) {
		var sessionId:String = "&ASP.NET_SessionId=" + _global.sessionId;
		var currentDate:Date = new Date();
		var url:String = "ProjectFile.aspx?mode=Upload&time=" + currentDate.getTime() + sessionId;
		_global.tr("BeginUploadProject url = " + url);
		if(!file.upload(url)) {
			_global.tr("Upload dialog failed to open.");
		}
		SATracking.UsePreviousDesign();
	}

	public function BeginDownloadProject(file:flash.net.FileReference) {
		var sessionId:String = "&ASP.NET_SessionId=" + _global.sessionId;
		var currentDate:Date = new Date();
		var url:String = "ProjectFile.aspx?time=" + currentDate.getTime() + sessionId;
		_global.tr("BeginDownloadProject url = " + url);
		if(!file.download(url, "Project." + _global.Project.Extension)) {
			_global.tr("Daonload dialog failed to open.");
		}
		SATracking.SaveDesign();
	}

	public function BeginSendToPrint() {
		SASession.WaitForSessionChecked(Delegate.create(this,SendToPrint));
	}
	
	private function SendToPrint() {
		var currentDate:Date = new Date();
		var url:String = "Print.aspx?time=" + currentDate.getTime();
		project.contentType = "text/xml";
		project.send(url, "_blank");
	}
}
