﻿class UIController {
	
	private var mainMenuController    : Menu.MainMenuUIController;
	private var floatMenuController   : Menu.FloatMenuUIController;
	private var wsTabController       : Workspace.WorkspaceTabUIController;
	private var toolsController       : Tools.ToolsUIController;
	private var previewAreaController : Workspace.PreviewAreaUIController;
	private var wsFramesController    : Frames.FramesUIController;
	private var imageStorage          ;//: ImageStorage;

	function UIController() {
		trace("UIConroler ctr");
		mainMenuController = new Menu.MainMenuUIController();
		floatMenuController = new Menu.FloatMenuUIController();
		wsTabController = new Workspace.WorkspaceTabUIController();
		toolsController = new Tools.ToolsUIController();
		previewAreaController = new Workspace.PreviewAreaUIController();
		wsFramesController = new Frames.FramesUIController();

		imageStorage = _root.createObject("ImageStorage", "ImageStorage", -100, new Object());
	}
	
	function get MainMenuController() {
		return mainMenuController;
	}
	
	function get FloatMenuController() {
		return floatMenuController;
	}
	
	function get WorkspaceTabController() {
		return wsTabController;
	}
	
	function get ToolsController() {
		return toolsController;
	}
	
	function get PreviewAreaController() {
		return previewAreaController;
	}
	function get FramesController() {
		return wsFramesController;
	}
	function get ImageStorage() {
		return imageStorage;
	}
	
	function OnComponentLoaded(compName:String, compObj:Object, param) {
		var firstName:String = compName.split(".")[0];
		switch (firstName) {
			case "MainMenu":
			{
				mainMenuController.OnComponentLoaded(compName, compObj, param);
				break;
			}
			case "FloatMenu":
			{
				floatMenuController.OnComponentLoaded(compName, compObj, param);
				break;
			}
			case "WorkspaceTab":
			{
				wsTabController.OnComponentLoaded(compName, compObj, param);
				break;
			}
			case "Tools":
			{
				toolsController.OnComponentLoaded(compName, compObj, param);
				break;
			}
			case "PreviewArea":
			{
				previewAreaController.OnComponentLoaded(compName, compObj, param);
				break;
			}
			case "Frames":
			{
				trace("UIController "+compName);
				wsFramesController.OnComponentLoaded(compName, compObj, param);
				break;
			}
		}
	}
	
	function OnUnitChanged() {
		toolsController.OnUnitChanged();
	}
	
	function UnselectCurrentUnit() {
		var currUnit = _global.Project.CurrentUnit;
		if (currUnit != null) {
			currUnit.IsCurrentUnit = false;
			_global.UIController.FramesController.detach();
			trace("currUnit.layoutPosition = "+currUnit.layoutPosition);
			if (currUnit.Text == "" && currUnit.layoutPosition == undefined || currUnit.Empty()) {
				_global.Project.CurrentPaper.CurrentFace.DeleteUnit(currUnit);
				trace("UnselectCurrentUnit: deleted empty text or paint");
			}
		}
		_global.Mode = _global.InactiveMode;
		toolsController.OnUnitChanged();
	}

}
