﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;
[Event("loaded")]
[Event("addedToProject")]

class SAImages {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SAImages.prototype);

	private var libraryXml:XML;
	private var addResult:LoadVars;

	public static function GetInstance(images:XML):SAImages {
		return new SAImages(images);
	}

	public function get IsLoaded():Boolean {
		return libraryXml.loaded;
	}

	private function SAImages(libraryXml:XML) {
		var parent = this;
		
		this.libraryXml = libraryXml;
		this.libraryXml.onLoad = function(success) {
			var eventObject = {type:"loaded", target:parent, result:this};
			trace("SAImages: dispatch images onLoaded");
			parent.dispatchEvent(eventObject);
		};
		
		addResult = new LoadVars();
		addResult.onLoad = function(success) {
			//_global.tr("addResult.onLoad this.added = " + this.added + " LastInsertedImage = " + this.LastInsertedImage);
			if (success && new Boolean(this.added)) {
				var eventObject = {type:"addedToProject", target:parent, LastInsertedImage:this.LastInsertedImage};
				trace("SAImages: dispatch images addedToProject");
				parent.dispatchEvent(eventObject);
			}
			if (success == false) {
 				_global.MessageBox.Alert("Can't upload image. It's possible that project size exceeds the limit.", " Warning", null);
 			}
		};		
	}
	
	public function BeginLoad() {
        trace("SAImages BeginLoad");
		var currentDate:Date = new Date();
		var url:String = "ImageLibrary.aspx?mode=xml" + "&time=" + currentDate.getTime();
		if (SALocalWork.IsLocalWork)
			url = "images.xml";
		//BrowserHelper.InvokePageScript("log", url);		
        this.libraryXml.load(url);
    }

    public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("loaded", Delegate.create(scopeObject, callBackFunction));
    }

	public function BeginAddImageToProject(id) {
        trace("SAImages BeginLoad");
		var currentDate:Date = new Date();
		var url:String = "ImageLibrary.aspx?mode=add&id=" + id.toString();// + "&time=" + currentDate.getTime();
		//BrowserHelper.InvokePageScript("log", url);
		addResult.load(url);
    }

    public function RegisterOnAddedToProjectHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("addedToProject", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function BeginUploadImage(file:flash.net.FileReference) {
		var sessionId:String = "&ASP.NET_SessionId=" + _global.sessionId;
		var currentDate:Date = new Date();
		var url:String = "ImageLibrary.aspx?mode=upload" + sessionId;// + "&time=" + currentDate.getTime();
		_global.tr("BeginUploadImage url = " + url);
		if(!file.upload(url)) {
			_global.tr("Upload dialog failed to open.");
		}
	}
	
	public function BeginDeleteImage(id) {
		var url:String = "ImageLibrary.aspx?mode=delete&id=" + id;
		addResult.load(url);
	}
	
	public function GetLastImageInfo() {
		var currentDate:Date = new Date();
		var url:String = "ImageLibrary.aspx?mode=GetLastImageInfo&time=" + currentDate.getTime();
		addResult.load(url);
	}
}
