﻿import mx.utils.Delegate;
class TextEffectUnit extends TextUnit {
	private var effect:Object;
	private var effmc:TextEffect;
	private var contour:XMLNode = null;
	public var frame:XMLNode = null;
	public var frameLinkageName:String = "FrameEx";
	public var bChanged:Boolean = true;
	public var preloaderCount:Number = 0;
	private var moveLock:Boolean = true;
	//private var contour:XMLNode = null;
	function TextEffectUnit(mc:MovieClip, node:XML) {
		super(mc,node);
		textField._visible = false;
		trace("TextEffectUnit constructor");
		effect = null;
		effmc = null;
		isMultiline = true;
		var fontFormat:TextFormat = textField.getNewTextFormat();
		
		if (node != null) {
			for (var i:Number = 0; i < node.childNodes.length; ++i) {
				var childNode:XML = node.childNodes[i];
				switch(childNode.nodeName) {
					case "TextEffect":
						bChanged = false;
						effect = _global.Effects.CreateTextEffectFromXml(childNode);
						break;
					case "Contour":
						contour = childNode.cloneNode(true);
						break;
					case "Geometry":
						frame = childNode.cloneNode(true);
					default:		
				}
			}
		}
		if(effect.subtype == "Circular"){
				_global.SelectionFrame.PrepareSpecialEffect(this,effect);
		}
		
	}
	function GetXmlNode():XMLNode {
		
		var node:XMLNode = super.GetXmlNode();
		if(node == null)
			return null;
		node.nodeName = "TextEffect";
		node.appendChild(_global.Effects.GetXML(effect));
		var cont:XMLNode = contour;
		contour.attributes.color = _global.GetColorForXML(textField.getTextFormat(0).color);
		contour.attributes.width = mc.getBounds(mc).xMax - mc.getBounds(mc).xMin;
		contour.attributes.height = mc.getBounds(mc).yMax - mc.getBounds(mc).yMin;
		contour.attributes.x = mc.getBounds(mc).xMin;
		contour.attributes.y = mc.getBounds(mc).yMin;
		node.appendChild(cont);
		var frameXml:XMLNode = frame;
		node.appendChild(frame);
		//_global.tr("BSM - Circular XML - " + node.toString());
		return node;		
	}	
	
	function SetTextEffect(effectName:String) : Void {
		//_global.tr("BSM - SetTextEffect");
			effect = _global.Effects.GetEffect(effectName);
			if(effect.subtype == "Circular"){
				effect.centerX = this.X;
				effect.centerY = this.Y;
				trace("SetTextEffect + "+effect.centerX);
				KeepProportions = true;
			} else {
				KeepProportions = false;
			}
			
			this.ScaleX = 100;
			this.ScaleY = 100;
			bChanged = true;			
			
			textField._visible = false;
			textField.autoSize = false;
			textField._width = 1;
			textField._height = 1;
		
			mc.textField._visible = false;
			mc.textField.autoSize = false;
			mc.textField._width = 1;
			mc.textField._height = 1;
			mc.textField._x = effect.refPoints[0].x;
			mc.textField._y = effect.refPoints[0].y;
			mc.txt.removeTextField();
			
			mc.textField._visible = false;
			mc.textField.autoSize = false;
			mc.textField._width = 1;
			mc.textField._height = 1;
			
			mc.textField._x = effect.refPoints[0].x;
			mc.textField._y = effect.refPoints[0].y;
			if(effect.subtype == "Circular"){
				
				_global.SelectionFrame.PrepareSpecialEffect(this,effect);
				
			}
			frameLinkageName = "FrameEx";
		
	}
	public function SetTextEffectByObject(obj:Object) : Void {
		effect = obj;
	}
	public function UpdateTextEffect() : Void {
	if (SAPlugins.IsAvailable() == false) return;
		bChanged = true;
		Draw();
	}

	function getEffect() :Object {
		return effect;
	}

	function RegisterOnConvertHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("onConvert", Delegate.create(scopeObject, callBackFunction));
	}
	
	function Convert() : Void {
		_global.tr("TextEffectUnit.Convert()");
		var eventObject:Object = {type:"onConvert", target:this};
		this.dispatchEvent(eventObject);
	}
	
	function Draw():Void {
		DrawMC(mc, true);
	}
	

	function DrawMC(mc:MovieClip, drawOnWorkArea:Boolean):Void {
		//_global.tr("BSM - TextEffectUnit Draw");
			while(preloaderCount > 0) {
				Preloader.Hide();
				preloaderCount--;
			}
			
			drawOnWorkArea = drawOnWorkArea == undefined || drawOnWorkArea == null ? false : drawOnWorkArea;
				
			mc.txt._visible = false;
			var mcEffect:MovieClip = mc["_effect"];
			if (mcEffect == undefined)
				mcEffect = mc.createEmptyMovieClip("_effect", 1);
			
			else 
				mcEffect.clear();
				
			if(effmc != null)
				delete effmc;
				
			mc.clear();
			effmc = new TextEffect(mcEffect, this);
			effmc.RegisterOnRedrawHandler(this,OnEffectRedraw);
			effmc.RegisterOnConvertHandler(this, Convert);
			effmc.effect = effect;
			effmc.textString = textField.text;
			effmc.color = textField.getTextFormat(0).color;
			effmc.bold = textField.getTextFormat(0).bold;
			effmc.italic = textField.getTextFormat(0).italic;
			effmc.underline = false;
			effmc.size = textField.getTextFormat(0).size;
			effmc.align = Align;
			trace('effmc.bold '+effmc.bold );
			trace('effmc.italic '+effmc.italic );
			trace('effmc.underline '+effmc.underline );
			trace('effmc.size'+effmc.size );
			effmc.fontTTF = _global.Fonts.GetTTFFileName(textField.getTextFormat(0).font.toString().toLowerCase()+"_"+effmc.italic.toString()+"_"+effmc.bold.toString());
			if(effmc.fontTTF == undefined)
				effmc.fontTTF = _global.Fonts.GetTTFFileName(textField.getNewTextFormat().font.toString().toLowerCase()+"_"+effmc.italic.toString()+"_"+effmc.bold.toString());
			if(effmc.fontTTF == undefined)
				effmc.fontTTF = _global.Fonts.GetTTFFileName(textField.getNewTextFormat().font.toString().toLowerCase()+"_false_false");
			trace('effmc.font'+textField.getTextFormat(0).font);
			mc.textField._visible = false;
			mc.textField.autoSize = false;
			mc.textField._width = 1;
			mc.textField._height = 1;
			mc.textField._x = effect.refPoints[0].x;
			mc.textField._y = effect.refPoints[0].y;
			
			effmc.Draw();
			mc._visible = this.Visible && (drawOnWorkArea || !EmptyLayoutItem);
			
			if (_global.Project.CurrentUnit == this &&  _global.Project.CurrentUnit!= null)
				_global.CurrentFace.UpdateSelection(this);
	}
	public function ResizeByStep(isPositive:Boolean) : Void {
		ResizeByStep2(isPositive);
	}
	
	public function ResizeToPosition(globalPositionX:Number, globalPositionY:Number) : Void {
		var resizer:UnitResizer = new UnitResizer(_global.SelectionFrame.frameBorder);

		var kx:Number = 1;
		var ky:Number = 1;
		if (this.KeepProportions == true) {
			var k:Number = resizer.GetProportionalFactor(globalPositionX, globalPositionY);
			kx = k;
			ky = k;
		} else {
			kx = resizer.GetFreeXFactor(globalPositionX, globalPositionY);
			ky = resizer.GetFreeYFactor(globalPositionX, globalPositionY);
		}

		var newWidth:Number = 	resizer.CurrentWidth * kx;
		var newHeight:Number = resizer.CurrentHeight * ky;
		
		if (newHeight <= 4 || newWidth <= 4) {
			kx = ky = 1;
		}

		Resize(kx, ky);
	}

	private function Resize(kx:Number, ky:Number) : Void {
		var oldBounds:Object = _global.SelectionFrame.frameBorder.getBounds(_global.SelectionFrame.frameBorder);
		var oldLeftUpper:Object = {x:oldBounds.xMin, y:oldBounds.yMin};
		this.mc.localToGlobal(oldLeftUpper);
		this.mc._parent.globalToLocal(oldLeftUpper);

		this.ScaleX = this.ScaleX * kx;
		this.ScaleY = this.ScaleY * ky;

		var newBounds:Object = _global.SelectionFrame.frameBorder.getBounds(_global.SelectionFrame.frameBorder);
		var newLeftUpper:Object = {x:newBounds.xMin, y:newBounds.yMin};
		this.mc.localToGlobal(newLeftUpper);
		this.mc._parent.globalToLocal(newLeftUpper);

		var originPoint:Object = {x:this.X, y:this.Y};

		var dx:Number = newLeftUpper.x - oldLeftUpper.x;
		var dy:Number = newLeftUpper.y - oldLeftUpper.y;

		originPoint.x = originPoint.x - dx;
		originPoint.y = originPoint.y - dy;
		
		if(effect.subtype != 'Circular'){
			this.X = originPoint.x;
			this.Y = originPoint.y;
		}
	}
	public function ChangeFontFormat(indexBegin:Number, indexEnd:Number, format:TextFormat):Void {
		if(effect.subtype == "Circular" && format.size != undefined) {
			var oldSize:Number = textField.getTextFormat(0).size;
			/*
			_global.tr("ChangeFontFormat format.size = "+format.size);
			var p0:Object = new Object({x:effect.refPoints[0].x, y:effect.refPoints[0].y});
			var p1:Object = new Object({x:effect.refPoints[1].x, y:effect.refPoints[1].y});
			var p2:Object = new Object({x:effect.refPoints[2].x, y:effect.refPoints[2].y});
			var radius:Number = Math.sqrt(p0.x*p0.x+p0.y*p0.y);
			var a1:Number= Math.atan2(p0.y,p0.x);
			var a2:Number= Math.atan2(p2.y,p2.x);
			if (a1 < 0)
				a1 += 2*Math.PI;
			if (a2 < 0)
				a2 += 2*Math.PI;
				
			var arc:Number = 0;
			if (a1 >= a2)
				arc = a1 - a2;
			else
				arc = a1 - a2 + 2*Math.PI;
				
			_global.tr("a1="+a1+", a2="+a2+",arc = "+arc);
			
			var delta:Number = 0;
			if (format.size > oldSize)
				delta = arc * (format.size - oldSize) / oldSize;
			else
				delta = arc * (format.size - oldSize) / oldSize;
				
			delta *= 0.5;
			
			if (a1 + delta > 1.5 * Math.PI)
				delta = 1.5 * Math.PI - a1;
			if (a1 + delta < 0.51 * Math.PI)
				delta = 0.51 * Math.PI - a1;
			
			_global.tr("delta = "+delta);
			
			effect.refPoints[0].x = radius * Math.cos(a1 + delta);
			effect.refPoints[0].y = radius * Math.sin(a1 + delta);
			effect.refPoints[2].x = radius * Math.cos(a2 - delta);
			effect.refPoints[2].y = radius * Math.sin(a2 - delta);
			*/
		}
		textField.setTextFormat(format);
		textField.setNewTextFormat(format);
		textField.setTextFormat(format);
		trace("ChangeFontFormat color" + format.color);
		bChanged = true;
	}
	public function SetText(txt:String) : Void {
		textField.text = txt;
		effmc.textString = txt;
		bChanged = true;
	}
	public function ReplaceText(indexBegin:Number, indexEnd:Number, newSubsrting:String, format:TextFormat):Void {
		textField.setNewTextFormat(format);
		newSubsrting = newSubsrting.split("\r").join("\n");
		if (newSubsrting.length > 0) {
			textField.replaceText(indexBegin, indexEnd, newSubsrting);
			textField.setTextFormat(indexBegin, indexBegin+newSubsrting.length, format);
			_global.Fonts.TestFontFormat(format, this);
			trace('Else1');
		} else if (indexBegin == 0 && indexEnd == textField.text.length) { 
			textField.text = newSubsrting;
			trace('Else2');
		} else { 
			deleteTextRange(textField, indexBegin, indexEnd);			
			trace('Else3');
		}
		effmc.textString = textField.text;
		bChanged = true;
		
	
	}
	public function ApplyLayoutFormat(layout:FaceLayoutItem, putNewText:Boolean):Void {		
		trace("TextEffectUnit:ApplyLayoutFormat begin");
		super.ApplyLayoutFormat(layout,putNewText);
		this.predefText = layout.Text;
		trace("TextEffectUnit:ApplyLayoutFormat predefText = "+predefText);
	}
	public function OnEffectRedraw(eventObject:Object) : Void {
		while(preloaderCount > 0)
		{
			Preloader.Hide();
			preloaderCount--;
		}
		//hiding text field
		textField._visible = false;
		textField.autoSize = false;
		textField._width = 1;
		textField._height = 1;
	
		mc.textField._visible = false;
		mc.textField.autoSize = false;
		mc.textField._width = 1;
		mc.textField._height = 1;
		mc.textField._x = effect.refPoints[0].x;
		mc.textField._y = effect.refPoints[0].y;
		mc.txt.removeTextField();
		//filling contour
		mc.beginFill(0, 0);
		var b:Object = mc.getBounds();
		mc.moveTo(b.xMin, b.yMin);
		mc.lineTo(b.xMin, b.yMax);
		mc.lineTo(b.xMax, b.yMax);
		mc.lineTo(b.xMax, b.yMin);
		mc.lineTo(b.xMin, b.yMin);
		mc.endFill();
		bChanged = false;
		if(_global.Project.CurrentUnit != null && _global.Project.CurrentUnit == this) {
			trace('Inn');
			_global.SelectionFrame.AttachUnit(this);
				
		}
	}

	function getMode():String {
		return _global.TextMode;
	}
	
	function Detach() : Void {
		if(Text.length == 0 && EmptyLayoutItem == false) {
			EmptyLayoutItem = true;
			var fmt:TextFormat = GetFontFormat(0);
			var len:Number = Text.length;
			var defaultText:String = predefText;
			ReplaceText(0, 0, defaultText, fmt);
			bChanged = true;
			trace('TEDETACH');
		
		}
		if(bChanged == true ){
			UpdateTextEffect();
		}
		
	}
	public function StepResize(kx:Number, ky:Number) : Void {
		if(effect.subtype == 'Circular'){
			_global.SelectionFrame.SpecialStepResize(kx,ky);
			return;
		}
		trace('StepResize texteffectUnit');
	  	var oldBounds:Object = this.mc.getBounds(this.mc);
   		var oldLeftUpper:Object = {x:oldBounds.xMin, y:oldBounds.yMin};
   		this.mc.localToGlobal(oldLeftUpper);
   		this.mc._parent.globalToLocal(oldLeftUpper);

		this.ScaleX = this.ScaleX * kx;
		this.ScaleY = this.ScaleY * ky;
		
   		var newBounds:Object = this.mc.getBounds(this.mc);
   		var newLeftUpper:Object = {x:newBounds.xMin, y:newBounds.yMin};
   		this.mc.localToGlobal(newLeftUpper);
   		this.mc._parent.globalToLocal(newLeftUpper);
   
   		var originPoint:Object = {x:this.X, y:this.Y};
   
   		var dx:Number = newLeftUpper.x - oldLeftUpper.x;
   		var dy:Number = newLeftUpper.y - oldLeftUpper.y;
   
   		originPoint.x = originPoint.x - dx;
   		originPoint.y = originPoint.y - dy;
   		
   		this.X = originPoint.x;
   		this.Y = originPoint.y;
	}
	public function set X(value:Number) {
		super.X = value;return;
		if(effect.subtype != "Circular" || !moveLock) {
			
		} else {
			_global.SelectionFrame._x = X;
		}
		
	}
	public function set Y(value:Number) {
		super.Y = value;return;
		if(effect.subtype != "Circular" || !moveLock) {
			super.Y = value;
		} else {
			_global.SelectionFrame._y = Y;
		}
	}
	public function get X() {
			return super.X; 
	}
	public function get Y() {
			return super.Y ;
	}

	public function LockMove() : Void {
		moveLock = true;
	}	
	
	public function UnlockMove() : Void {
		moveLock = true;
	}	
	
	public function get LineNumbers():Number {
		var tokens:Array = this.Text.split("\r");
		return tokens.length;
	}
	
	public function get MaxLineLength():Number {
		var tokens:Array = this.Text.split("\r");
		var maxTokenLength:Number = 0;
		for (var i:Number = 0; i < tokens.length; ++i)
			maxTokenLength = Math.max(maxTokenLength, tokens[i].length);
		return maxTokenLength;
	}

	// BSM Special rotate method for Circular effect
	//////////////////////////////////////////////////////////////////
	public function RotateCircularCCW():Void {		
		var textRadius:Number = this.frame.attributes.Radius;
		var textAlpha:Number = this.frame.attributes.Alpha;
		var textBeta:Number = this.frame.attributes.Beta;
		if (textAlpha < 0) {
			textAlpha = Math.abs(textAlpha);
			
		} else {
			textAlpha = textAlpha * -1;
		}
		textAlpha -= 0.2;		
		this.effect.refPoints[0].x = textRadius*Math.cos(textAlpha-(textBeta/2));
		this.effect.refPoints[0].y = textRadius*Math.sin(textAlpha-(textBeta/2));
		this.UpdateTextEffect();
	}
	public function RotateCircular90CCW():Void {
		var textRadius:Number = this.frame.attributes.Radius;
		var textAlpha:Number = this.frame.attributes.Alpha;
		var textBeta:Number = this.frame.attributes.Beta;
		if (textAlpha < 0) {
			textAlpha = Math.abs(textAlpha) - 0.1;
			
		} else {
			textAlpha = (textAlpha * -1) - 0.1;
		}
		var Alpha:Number = textAlpha-(textBeta/2);
		//_global.tr("BSM - Rad  "+textAlpha);
		if(Alpha >= 0 && Alpha < 1.57) {
			Alpha = 0;
		}
		else if(Alpha >= 1.57 && Alpha < 3.14) {
			Alpha = 1.57;
		}
		else if (Alpha >= -3.14 && Alpha < -1.57) {
			Alpha = 3.14;
		}
		else if(Alpha >= -1.57 && Alpha < 0) {
			Alpha = -1.57;
		}
		//_global.tr("BSM - Rad  "+textAlpha);				
		this.effect.refPoints[0].x = textRadius*Math.cos(Alpha);
		this.effect.refPoints[0].y = textRadius*Math.sin(Alpha);
		this.UpdateTextEffect();
	}
	public function RotateCircularCW():Void {
		var textRadius:Number = this.frame.attributes.Radius;
		var textAlpha:Number = this.frame.attributes.Alpha;
		var textBeta:Number = this.frame.attributes.Beta;
		if (textAlpha < 0) {
			textAlpha = Math.abs(textAlpha);
			
		} else {
			textAlpha = textAlpha * -1;
		}
		textAlpha += 0.2;	
		this.effect.refPoints[0].x = textRadius*Math.cos(textAlpha-(textBeta/2));
		this.effect.refPoints[0].y = textRadius*Math.sin(textAlpha-(textBeta/2));
		this.UpdateTextEffect();
	}
	public function RotateCircular90CW():Void {
		var textRadius:Number = this.frame.attributes.Radius;
		var textAlpha:Number = this.frame.attributes.Alpha;
		var textBeta:Number = this.frame.attributes.Beta;
		if (textAlpha < 0) {
			textAlpha = Math.abs(textAlpha) + 0.1;
			
		} else {
			textAlpha = (textAlpha * -1) + 0.1;
		}
		var Alpha:Number = textAlpha-(textBeta/2);
		//_global.tr("BSM - Rad  "+textAlpha);
		if(Alpha >= 0 && Alpha < 1.57) {
			Alpha = 1.57;
		}
		else if(Alpha >= 1.57 && Alpha < 3.14) {
			Alpha = -3.14;
		}
		else if (Alpha >= -3.14 && Alpha < -1.57) {
			Alpha = -1.57;
		}
		else if(Alpha >= -1.57 && Alpha < 0) {
			Alpha = 0;
		}
		//_global.tr("BSM - Rad  "+textAlpha);				
		this.effect.refPoints[0].x = textRadius*Math.cos(Alpha);
		this.effect.refPoints[0].y = textRadius*Math.sin(Alpha);
		this.UpdateTextEffect();
	}
	//////////////////////////////////////////////////////////////////
}
