﻿class TextUnit extends MoveableUnit {
	private var textField:TextField;
	public var margin2px:Number = 2;
	public var margin4px:Number = 4;
	public var letterCount:Number = 0;
	private var isMultiline:Boolean = false;
	private var multilineWidth:Number = 100;
	private var align:String = "left";
	//private var layoutPosition:Number; 
	private var emptyLayoutItem:Boolean;
	public var predefText:String = "";

	public var frameLinkageName:String = "Frame";
	
	function TextUnit(mc:MovieClip, node:XML) {
		super(mc, node);
		unitClassName = "MultilineTextUnit";
		if(mc.textField == undefined)
			mc.createTextField("textField", 0, 0, 0, 1, 1);
		
		textField = mc.textField;
		textField.embedFonts = true;
		textField.autoSize = false;
		textField._visible = false;
		textField._height = margin4px;
		textField._width = margin4px;
		
		var fontFormat:TextFormat = textField.getNewTextFormat();
		fontFormat.font = "Arial";
		fontFormat.size = 16;
		fontFormat.bold = false;
		fontFormat.italic = false;
		fontFormat.underline = false;
		fontFormat.color = 0x000000;
		if (!_global.Fonts.TestLoadedFont(fontFormat))
			_global.Fonts.LoadFont(fontFormat, this);
			
		textField.setTextFormat(fontFormat);
		textField.setNewTextFormat(fontFormat);
		
		if (node != null) {
			trace("NODE !+ NULL");
			textField.text = node.attributes.text;
			layoutPosition = node.attributes.layoutPosition;
			align = node.attributes.align;
			predefText = node.attributes.predefText;
			this.Visible = node.attributes.visible.toString() == "true" ? true : false;
			this.EmptyLayoutItem = node.attributes.emptyLayoutItem.toString() == "true" ? true : false;
			isMultiline = node.attributes.isMultiline.toString() == "true" ? true : false;
			if (isMultiline) {
				multilineWidth = Number(node.attributes.multilineWidth);
				keepProportions = false;
			}
				
			for (var i:Number = 0; i < node.childNodes.length; ++i) {
				var childNode:XML = node.childNodes[i];
				trace("childNode.nodeName"+childNode.nodeName);
				switch(childNode.nodeName) {
					case "Style":
						//fontFormat = new TextFormat();
						fontFormat.font = childNode.attributes.font;
						trace("childNode.attributes.font"+childNode.attributes.font);
						trace("childNode.attributes.size"+childNode.attributes.size);
						fontFormat.size = Number(childNode.attributes.size);
						
						fontFormat.bold = childNode.attributes.bold.toString() == "true" ? true : false;
						fontFormat.italic = childNode.attributes.italic.toString() == "true" ? true : false;
						fontFormat.underline = childNode.attributes.underline.toString() == "true" ? true : false;
						fontFormat.color = parseInt(childNode.attributes.color.substr(1), 16);
						if (!_global.Fonts.TestLoadedFont(fontFormat))
							_global.Fonts.LoadFont(fontFormat, this);
						textField.setTextFormat(i,fontFormat);
						textField.newTextFormat(fontFormat);
						break;
					
					default:
						//trace("Unexpected tag name! " + childNode.nodeName);
				}
			}
		} else {
			textField.text = "";
			this.X = 0 + Math.random()*100; 
			this.Y = 0 + Math.random()*200;
			width  = 31;
			height = 31;
		}		
	}
	
	function GetXmlNode(generate:Boolean):XMLNode {
		generate = generate == true;
		if (textField.text == "" && layoutPosition == undefined && generate == false) {
			return null;
		}
		var node:XMLNode = super.GetXmlNode();
		node.nodeName = "Text";
		node.attributes.text = textField.text;
		node.attributes.layoutPosition = layoutPosition;
		node.attributes.align = align;
		node.attributes.multilineWidth = multilineWidth;
		node.attributes.isMultiline = isMultiline;
		node.attributes.predefText = predefText;
		node.attributes.visible = this.Visible;
		node.attributes.emptyLayoutItem = this.EmptyLayoutItem;
		for (var index:String in node.attributes) {
			if (node.attributes[index] == "scaleX" ||
				node.attributes[index] == "scaleY" ||
				node.attributes[index] == "initScale")
				node.attributes.splice(index, 1);
		}
		
		for (var i:Number = 0; i < textField.length; ++i) {
			var styleNode:XMLNode = new XMLNode(1, "Style");
			var fontFormat:TextFormat = textField.getTextFormat(i);
			//////////////////////////////////////////////////
			fontFormat.font = _global.Fonts.FontRangeDetect(textField.text.charCodeAt(i), fontFormat.font);
			
			//////////////////////////////////////////////////
			styleNode.attributes.font = fontFormat.font;
			styleNode.attributes.size = fontFormat.size;
			styleNode.attributes.bold = fontFormat.bold;
			styleNode.attributes.italic = fontFormat.italic;
			styleNode.attributes.underline = fontFormat.underline;
			styleNode.attributes.color = _global.GetColorForXML(fontFormat.color);
			node.appendChild(styleNode);
		}

		node.appendChild(super.GetXmlNodeRotate());
		
		return node;
	}
	
	public function ChangeFontFormat(indexBegin:Number, indexEnd:Number, format:TextFormat):Void {
//		_global.tr("### ChangeFontFormat ib = " + indexBegin + " ie = " + indexEnd);
		// Any property of textFormat that is set to null will not be applied
		if (!_global.Fonts.TestLoadedFont(format)) {
			_global.Fonts.LoadFont(format, this);
		}		
		var newFontName:String = format.font;
		if (newFontName == undefined) {
			textField.setNewTextFormat(format);
			for(var index:Number = indexBegin; index <= indexEnd; ++index) {
				var begin:Number = (index == indexBegin) ? index : index - 1;
				var end:Number =  index;
				if (begin != end) {
					textField.setTextFormat(begin, end, format);
				}
			}
		} else {
			if (newFontName.lastIndexOf("_") != -1) {
				var newFontNameWithoutRange:String = newFontName.slice(0,newFontName.lastIndexOf("_"));
			} else {
				var newFontNameWithoutRange:String = newFontName;
			}
			
			for(var index:Number = indexBegin; index <= indexEnd; ++index) {
				var begin:Number = (index == indexBegin) ? index : index - 1;
				var end:Number =  index;
				if (begin != end) {
					///////////////////////////////////////////////
					var curFormat:TextFormat = new TextFormat();			
					curFormat = textField.getTextFormat(begin, end);			
					var curFontName:String = curFormat.font;
					if (curFontName.lastIndexOf("_") != -1) {			
						var curFontRange:String = curFontName.slice(curFontName.lastIndexOf("_"));
						format.font = String(newFontNameWithoutRange+curFontRange);
					} else {
						format.font = String(newFontNameWithoutRange);
					}
					
					///////////////////////////////////////////////
					textField.setTextFormat(begin, end, format);
				}
			}
			format.font = newFontName;
			textField.setNewTextFormat(format);
		}
		CalcSize(mc, true, true);
		
		Draw();
	}
	
	public function SetFontFormat(index:Number, format:TextFormat) : Void {
		if (!_global.Fonts.TestLoadedFont(format)) {
			_global.Fonts.LoadFont(format, this);
		}
		textField.setNewTextFormat(format);
		textField.setTextFormat(index, format);

		CalcSize(mc, true, true);
		Draw();
	}
	
	public function ReplaceText(indexBegin:Number, indexEnd:Number, newSubsrting:String, format:TextFormat):Void {
		// Any property of textFormat that is set to null will not be applied
		textField.setNewTextFormat(format);
		
		newSubsrting = newSubsrting.split("\r").join("\n");
		if (newSubsrting.length > 0) {
			textField.replaceText(indexBegin, indexEnd, newSubsrting);
			var baseFont:String = format.font;
			for (var i:Number = 0; i<newSubsrting.length; i++) {
				//_global.tr("BSM - setTextFormat - "+textField.text.charAt(indexBegin+i)+" - Format - "+format.font);
				format.font = _global.Fonts.FontRangeDetect(textField.text.charCodeAt(indexBegin+i), baseFont);
				textField.setTextFormat(indexBegin+i, indexBegin+i+1, format);
				_global.Fonts.TestFontFormat(format, this);
			}
		} else if (indexBegin == 0 && indexEnd == textField.text.length) { 
			textField.text = newSubsrting;
		} else { 
			deleteTextRange(textField, indexBegin, indexEnd);			
		}
			
		CalcSize(mc, true, true);
		
		Draw();		
	}
	
	private static function deleteTextRange(txtField:TextField, indexBegin:Number, indexEnd:Number):Void {
		//removing range
		var txt:String = txtField.text.substr(0, indexBegin);
		txt += txtField.text.substr(indexEnd, txtField.length);
		
		var fontFormats:Array = new Array();
		
		for (var i:Number = 0; i < indexBegin; ++i) {
			fontFormats.push(txtField.getTextFormat(i));
		}
		
		for (var i:Number = indexEnd; i < txtField.length; ++i) {
			fontFormats.push(txtField.getTextFormat(i));
		}
		
		txtField.text = txt;
		for (var i:Number = 0; (i < fontFormats.length) || (i == 0 && txtField.length == 0); ++i) {
			txtField.setTextFormat(i, fontFormats[i]);
		}
	}

	public function GetFontFormat(index:Number):TextFormat {
		if (textField.length == 0) 
			return textField.getNewTextFormat();
		return textField.getTextFormat(index);
	}
	
	public function GetFontFormats(beginIndex:Number, endIndex:Number):Array {
		beginIndex = beginIndex == null || beginIndex == undefined ? 0 : beginIndex;
		endIndex = endIndex == null || endIndex == undefined ? textField.text.length : endIndex;
		var formats:Array = new Array();
		for(var i:Number = beginIndex; i < endIndex; ++ i)
			formats.push(GetFontFormat(i));
		return formats;
	}

	public function get Text():String {
		return textField.text;
	}
	public function SetText(txt:String) : Void {
		textField.text = txt;
	}

	function RestoreText() : Void {
		trace("simple text");
			this.ScaleX = 100;
			this.ScaleY = 100;
			mc.textField._x = 0;
			mc.textField._y = 0;
			textField.autoSize = false;
			textField._visible = true;		
		
		Draw();		
	}	
	
	function Draw():Void {
		DrawMC(mc, true);
		//SB - Multiline text editing fix
		//AttachInternal();
	}
	//SB - Multiline text editing fix
	function AttachInternal() : Void {
		_global.UIController.FramesController.attach(frameLinkageName, false);
	}

	function DrawMC(mc:MovieClip, drawOnWorkArea:Boolean):Void {
		//_global.tr("BSM - TextUnit Draw");
		CalcSize(mc, false, drawOnWorkArea);
		mc.beginFill(0, 0);
		mc.moveTo(0, 0);
		mc.lineTo(width, 0);
		mc.lineTo(width, height);
		mc.lineTo(0, height);
		mc.endFill();
		_global.SelectionFrame.RedrawUnit();		
	}
	
	function CalcSize(mc:MovieClip, doOffset:Boolean, drawOnWorkArea:Boolean):Void {
		mc.clear();
		mc.txt.removeTextField();
		mc.createTextField("txt", 1, -margin2px, -margin2px, 100, 100);
		var txt:TextField = mc.txt;
		txt._visible = this.Visible && (drawOnWorkArea || !EmptyLayoutItem);
		txt.embedFonts = true;
		txt.autoSize = true;
		txt.selectable = false;
		txt.multiline = isMultiline;
		txt.wordWrap = isMultiline;
				
		var oldW:Number = width;
		if (textField.length == 0) {
			height = 31;
			width = isMultiline ? multilineWidth : 31;				
		} else {
			if (isMultiline)
				RenderMultilineText(txt);
			else
				RenderSimpleText(txt);
				
			txt.autoSize = true;
			
			width = txt._width - margin4px;
			height = txt._height - margin4px;
		}
		
		if (isMultiline) {
			var fmt:TextFormat = new TextFormat();
			fmt.align = align;
			txt.setTextFormat(fmt);
		} else {
			var deltaW:Number = oldW - width;
			if (deltaW != 0 && doOffset == true) {
				if (align == "center") {
					Offset(deltaW/2, 0);
				}
				else if (align == "right") {
					Offset(deltaW, 0);
				}
			}
		}
	}
	
	private function RenderSimpleText(txt:TextField) : Void {
		var fontFormat:TextFormat = new TextFormat();
		fontFormat.leftMargin  = 0;
		fontFormat.rightMargin = 0;
		textField.setTextFormat(fontFormat);
		
		txt.text = " " + textField.text + " ";
		fontFormat = textField.getTextFormat(0);
		_global.Fonts.TestFontFormat(fontFormat, this);
		txt.setTextFormat(0, fontFormat);
		fontFormat = textField.getTextFormat(textField.length - 1);
		_global.Fonts.TestFontFormat(fontFormat, this);
		txt.setTextFormat(textField.length + 1, fontFormat);
		for (var i:Number = 0; i < textField.length; ++i) {
			fontFormat = textField.getTextFormat(i);
			_global.Fonts.TestFontFormat(fontFormat, this);
			txt.setTextFormat(i+1, fontFormat);
		}
	}
	
	private function RenderMultilineText(txt:TextField) : Void {
		multilineWidth = Math.max(this.Size*2.6, multilineWidth);
		txt._width = multilineWidth + margin4px;		
		var fontFormat:TextFormat = new TextFormat();
		var margin:Number = Size/8;
		fontFormat.leftMargin  = margin;
		fontFormat.rightMargin = margin;
		textField.setTextFormat(fontFormat);
		
		txt.text = textField.text;
		for (var i:Number = 0; i < textField.length; ++i) {
			fontFormat = textField.getTextFormat(i);
			_global.Fonts.TestFontFormat(fontFormat, this);
			txt.setTextFormat(i, fontFormat);
		}
	}
	
	function getMode():String {
		return _global.TextMode;
	}
	
	function get Size():Number {
		var size:Number = 0;
		var fontFormat:TextFormat;
		for (var i:Number = 0; i < textField.length; ++i) {
			fontFormat = textField.getTextFormat(i);
			size = Math.max(size, fontFormat.size);
		}
  		return size;
	}

	function set Size(value:Number):Void {
		var oldSize:Number = Size;
		if (oldSize == value)
			return;
			
		var fontFormat:TextFormat;
		var minSize:Number = 127;
		for (var i:Number = 0; i < textField.length; ++i) {
			fontFormat = textField.getTextFormat(i);
			minSize = Math.min(minSize, fontFormat .size);
		}

		var delta:Number = value - oldSize;
		if (delta <= 4 - minSize)
			delta = 4 - minSize;
		if (delta >= 127 - oldSize)
			delta = 127 - oldSize;
			
		for (var i:Number = 0; i < textField.length; ++i) {
			fontFormat = textField.getTextFormat(i);
			fontFormat.size += delta;
			fontFormat.size = Math.max(fontFormat.size, 4);
			fontFormat.size = Math.min(fontFormat.size, 127);
			textField.setTextFormat(i, fontFormat);
		}

		Draw(mc);
	}
	
	public function ApplyLayoutFormat(layout:FaceLayoutItem, putNewText:Boolean):Void {		
		var format:TextFormat = new TextFormat();
		format.font = layout.Font;
		format.size = layout.Size;
		format.bold = layout.Bold;
		format.italic = layout.Italic;
		format.underline = layout.Underline;
		format.color = layout.Color;
		this.X = layout.X;
		this.Y = layout.Y;
		this.Angle = layout.Angle;
		this.Align = layout.Align;
		var actualText:String = (putNewText) ? layout.Text : Text;
		ReplaceText(0, actualText.length, actualText, format);
		
		
		this.EmptyLayoutItem = layout.EmptyLayoutItem;
		if(layout.UnitType == "MultilineTextUnit"){
			isMultiline = true;
			multilineWidth = layout.Width;
		}
		this.predefText = layout.Text;
		trace("ApplyLayoutFormat predefText = "+predefText);
		this.ScaleX = layout.ScaleX;
		this.ScaleY = layout.ScaleY;
	}

	public function ResizeByStep(isPositive:Boolean) : Void {
		this.ScaleX = 100;
		this.ScaleY = 100;
		if (isMultiline) {
			multilineWidth += isPositive ? 10 : -10;;
			DrawMC(mc);
		} else {
			var step:Number = isPositive ? 1 : -1;
			var k:Number = 1 + step / this.Size; 
			Resize(1, k);
		}		
	}

	private function Resize(kx:Number, ky:Number) : Void {
		this.ScaleX = 100;
		this.ScaleY = 100;
			
		if (isMultiline) {
			multilineWidth *= kx;
			DrawMC(mc);
		} else {
			var newSize:Number  = Math.round(this.Size * ky);
			if (this.Text.length > 0 &&	newSize >=4 && newSize <= 127 && newSize != this.Size) {
				this.Size = newSize;
 				Tools.Text.TextToolUIController.getInstance().RefreshFontSize();
			}
		}
	}

	public function get Align():String {
		return align;
	}
	public function set Align(value:String):Void {
		
			align = value;
			trace("align = "+align);
			Draw();
		
	}
	
	public function get Visible():Boolean {
		return mc._visible;
	}
	public function set Visible(value:Boolean):Void {
		trace("TextUnit:SetVisible: value = "+value);
		mc._visible = value;
	}
	
	public function get EmptyLayoutItem():Boolean {
		return emptyLayoutItem;
	}
	public function set EmptyLayoutItem(value:Boolean):Void {
		emptyLayoutItem = value;
	}
	/*
	public function get LayoutPosition():Number {
		return layoutPosition;
	}
	public function set LayoutPosition(value:Number):Void {
		layoutPosition = value;
	}
	*/
	public function get Multiline():Boolean {
		return isMultiline;
	}
	public function set Multiline(value:Boolean):Void {
		isMultiline = value;
		keepProportions = !isMultiline;
		trace("function set Multiline = "+isMultiline);
		if (!isMultiline) {
			var i:Number = textField.text.indexOf("\r");
			while(i != -1) {
				textField.replaceText(i, i+1, " ");
				i = textField.text.indexOf("\r");
			}
			CalcSize(mc, true, true);
		} else {
			var deltaW:Number = width - multilineWidth;
			if (align == "center") {
				Offset(deltaW/2, 0);
			}
			else if (align == "right") {
				Offset(deltaW, 0);
			}
		}
		DrawMC(mc, true);
	}
	
	public function Detach() : Boolean {
  	    if(Text.length == 0 && EmptyLayoutItem == false) {
			EmptyLayoutItem = true;
			var fmt:TextFormat = GetFontFormat(0);
			var len:Number = Text.length;
			var defaultText:String = predefText;
			ReplaceText(0, 0, defaultText, fmt);
			trace('TextUnitDetach layout case');
			return true;
		}
	}
	public function set MultilineWidth(value:Number) {
		multilineWidth = value;
	}
	
}
