﻿import mx.utils.Delegate;
import mx.core.UIObject;
import mx.controls.*;
import CtrlLib.ButtonEx;
class ImageEditProperties extends UIObject {
	private var chkKeepProportions:CheckBox;
	private var btnDelete:ButtonEx;
	private var btnImageEffects:ButtonEx;
	private var btnRestoreProportions:ButtonEx;
	private var lblImageEdit:Label;
	private var btnContinue:ButtonEx;
	
	private var optFitToFace:RadioButton;
	private var optFillVertically:RadioButton;
	private var optFillHorizontally:RadioButton;
	private var optThumbnail:RadioButton;
	
	function ImageEditProperties() {
		this.btnDelete.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnImageEffects.setStyle("styleName", "ToolPropertiesActiveButton");
		this.lblImageEdit.setStyle("styleName", "ToolPropertiesCaption");
		this.btnContinue.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnRestoreProportions.setStyle("styleName", "ToolPropertiesActiveButtonSmall");
	}
	
	function onLoad() {
		btnDelete.RegisterOnClickHandler(this, _global.DeleteCurrentUnit);
		btnImageEffects.RegisterOnClickHandler(this, OnImageEffect);
		
		chkKeepProportions.addEventListener("click", Delegate.create(this, chkKeepProportions_OnClick));

		optFitToFace.addEventListener("click", Delegate.create(this, optFitToFace_OnClick));
		optFillVertically.addEventListener("click", Delegate.create(this, optFillVertically_OnClick));
		optFillHorizontally.addEventListener("click", Delegate.create(this, optFillHorizontally_OnClick));
		optThumbnail.addEventListener("click", Delegate.create(this, optThumbnail_OnClick));

		btnRestoreProportions.RegisterOnClickHandler(this, btnRestoreProportions_OnClick);
		btnContinue.RegisterOnClickHandler(this, btnContinue_OnClick);
		btnRestoreProportions._visible = false;
		DataBind();
		InitLocale();
	}
	public function chkKeepProportions_OnClick(eventObject)	{
		_global.CurrentUnit.KeepProportions = chkKeepProportions.selected;
	}
	public function btnRestoreProportions_OnClick(eventObject) {
		_global.CurrentUnit.ScaleX = _global.CurrentUnit.InitialScale;
		_global.CurrentUnit.ScaleY = _global.CurrentUnit.InitialScale;
		_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
	}
	public function DataBind():Void {
		chkKeepProportions.selected = _global.CurrentUnit.KeepProportions;
		SelectAutofitButton(_global.CurrentUnit.AutofitType);
	}
    private function InitLocale() {
		//_global.LocalHelper.LocalizeInstance(btnDelete, "ToolProperties", "IDS_BTNDELETE");
		_global.LocalHelper.LocalizeInstance(lblImageEdit, "ToolProperties", "IDS_LBLIMAGEEDIT");
		_global.LocalHelper.LocalizeInstance(btnImageEffects, "ToolProperties", "IDS_BTNIMAGEEFFECTS");
		_global.LocalHelper.LocalizeInstance(chkKeepProportions, "ToolProperties", "IDS_CHKKEEPPROPORTIONS");
		//_global.LocalHelper.LocalizeInstance(btnContinue, "ToolProperties", "IDS_BTNCONTINUE");
		//_global.LocalHelper.LocalizeInstance(btnRestoreProportions, "ToolProperties", "IDS_BTNRESTOREPROPORTIONS");
		_global.LocalHelper.LocalizeInstance(optFillHorizontally, "ToolProperties", "IDS_OPTFILLHORIZONTALLY");
		_global.LocalHelper.LocalizeInstance(optFillVertically, "ToolProperties", "IDS_OPTFILLVERTICALLY");
		_global.LocalHelper.LocalizeInstance(optFitToFace, "ToolProperties", "IDS_OPTFITTOFACE");
		_global.LocalHelper.LocalizeInstance(optThumbnail, "ToolProperties", "IDS_OPTTHUMBNAIL");
	
		TooltipHelper.SetTooltip(btnContinue, "ToolProperties", "IDS_TOOLTIP_CONTINUE");
		TooltipHelper.SetTooltip(btnDelete, "ToolProperties", "IDS_TOOLTIP_DELETE");
		TooltipHelper.SetTooltip(chkKeepProportions, "ToolProperties", "IDS_TOOLTIP_KEEPPROPORTIONS");
		TooltipHelper.SetTooltip(btnRestoreProportions, "ToolProperties", "IDS_TOOLTIP_RESTOREPROPORTIONS");
		TooltipHelper.SetTooltip(optFitToFace, "ToolProperties", "IDS_TOOLTIP_FITTOFACE");
		TooltipHelper.SetTooltip(optFillHorizontally, "ToolProperties", "IDS_TOOLTIP_FITHORIZONTALLY");
		TooltipHelper.SetTooltip(optFillVertically, "ToolProperties", "IDS_TOOLTIP_FITVERTICALLY");
		TooltipHelper.SetTooltip(optThumbnail, "ToolProperties", "IDS_TOOLTIP_FITTHUMBNAIL");
    }
	private function btnContinue_OnClick() {
		OnExit();
	}
	
	private function SelectAutofitButton(autofitType:String) {
		optFitToFace.selected = autofitType == _global.FitToFaceAutofit;
		optFillVertically.selected = autofitType == _global.VerticalAutofit;
		optFillHorizontally.selected = autofitType == _global.HorizontalAutofit;
		optThumbnail.selected = autofitType == _global.ThumbailAutofit;
	}
	
	private function optFitToFace_OnClick(eventObject) {
		_global.CurrentFace.AutofitCurrentUnit(_global.FitToFaceAutofit);
	}
	
	private function optFillVertically_OnClick(eventObject) {
		_global.CurrentFace.AutofitCurrentUnit(_global.VerticalAutofit);
	}
	
	private function optFillHorizontally_OnClick(eventObject) {
		_global.CurrentFace.AutofitCurrentUnit(_global.HorizontalAutofit);
	}
	
	private function optThumbnail_OnClick(eventObject) {
		_global.CurrentFace.AutofitCurrentUnit(_global.ThumbailAutofit);
	}

	//region Exit event
	private function OnExit() {
		var eventObject = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
	//region ImageEffect event
	private function OnImageEffect() {
		var eventObject = {type:"imageeffect", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnImageEffectHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("imageeffect", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
}
