class Watermark.WatermarkHelper {
	public function WatermarkHelper() {
	}
	
	public static function GenerateXml(watermark:Entity.Watermark) : XML {
		var xml:XML = new XML("<Watermark />");
		var root:XMLNode = xml.firstChild;
		root.attributes.Width = watermark.Width;
		root.attributes.Height = watermark.Height;
		
		var textNode:XML = new XML(watermark.XmlText);
		textNode.ignoreWhite = true;
		root.appendChild(textNode);
		return xml;
	}
	
	public static function ParseWatermark(xml:XML) : Entity.Watermark {
		var root:XMLNode = xml.firstChild;
		var watermark:Entity.Watermark = Entity.Watermark.GetInstance();
		watermark.Width  = parseInt(root.attributes.Width); 
		watermark.Height = parseInt(root.attributes.Height);
		for (var i:Number = 0; i < root.childNodes.length; ++ i) {
			var node:XMLNode = root.childNodes[i];
			switch(node.nodeName) {
				case "XmlText":
					var text:String = node.toString();
					watermark.XmlText = text == undefined ? "" : text;
					break;
			}
		}
		return watermark;
	}
	
	public static function ParseFonts(xml:XML) : Array {
		var fonts: Array = undefined;
		var root:XMLNode = xml.firstChild;
		for (var i:Number = 0; i < root.childNodes.length; ++ i) {
			var node:XMLNode = root.childNodes[i];
			switch(node.nodeName) {
				case "Fonts":
					fonts = new Array();
					for(var j:Number = 0; j < node.childNodes.length; ++ j) {
						var fontNode:XMLNode = node.childNodes[j];
						fonts.push(fontNode.firstChild.toString());
					}
					break;
			}
		}
		return fonts;
	}
	
	private static function GetColorForXML (colorNumber:Number):String {
		if (colorNumber == null || colorNumber == undefined) {
			return "#000000";
		}
		
		var resultColor:String = "";
		var maxColorLength:Number = 6;
	
		var colorString:String = colorNumber.toString(16);
		
		if (colorString.indexOf("0x") != -1)
		colorString = colorString.substr(2);
		
		for(var i:Number=1; i<=(maxColorLength-colorString.length); i++) {
			resultColor += "0";
		}
		
		resultColor += colorString;
		
		return "#" + resultColor;
	}
	
	public static function GetTextFieldXml(textField:TextField) : String {
    	var node:XML = new XML("");
		node.ignoreWhite = true;
		node.nodeName = "XmlText";
		node.attributes.text = textField.text;
    	for (var i:Number = 0; i < textField.length; ++i) {
			var styleNode:XMLNode = new XMLNode(1, "Style");
			var fontFormat:TextFormat = textField.getTextFormat(i);
			//////////////////////////////////////////////////
			styleNode.attributes.font = fontFormat.font;
			styleNode.attributes.size = fontFormat.size;
			styleNode.attributes.bold = fontFormat.bold;
			styleNode.attributes.italic = fontFormat.italic;
			styleNode.attributes.underline = fontFormat.underline;
			styleNode.attributes.color = GetColorForXML(fontFormat.color);
			node.appendChild(styleNode);
		}
		return node.toString();
	}
	
	public static function SetTextFieldXml(textField:TextField, text:String) : Void {
		var node:XML = new XML(text);
		node.ignoreWhite = true;
		var root:XMLNode = node.firstChild;
		
		var fontFormat:TextFormat = textField.getNewTextFormat();
		fontFormat.align = "center";
		textField.text = root.attributes.text;
		
		for (var i:Number = 0; i < root.childNodes.length; ++i) {
			var childNode:XML = root.childNodes[i];
			switch(childNode.nodeName) {
				case "Style":
					fontFormat.font = childNode.attributes.font;
					fontFormat.size = Number(childNode.attributes.size);
					
					fontFormat.bold = childNode.attributes.bold.toString() == "true" ? true : false;
					fontFormat.italic = childNode.attributes.italic.toString() == "true" ? true : false;
					fontFormat.underline = childNode.attributes.underline.toString() == "true" ? true : false;
					fontFormat.color = parseInt(childNode.attributes.color.substr(1), 16);
					textField.setTextFormat(i,fontFormat);
					textField.newTextFormat(fontFormat);
					break;
				
				default:
			}
		}
	}
}