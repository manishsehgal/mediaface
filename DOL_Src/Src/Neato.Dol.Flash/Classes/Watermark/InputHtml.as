import mx.core.UIObject;
import mx.utils.Delegate;
import Watermark.WatermarkHelper;

class Watermark.InputHtml extends UIObject {
	private var txtInput:TextField;
	
	private var startIndex:Number = - 1;
	private var endIndex:Number = - 1;
	
	private static var ChangeEventText:String	= "change";
	private static var TextChangeEventText:String	= "TextChange";
	private static var KeyUpEventText:String	= "keyUp";
	private static var FocusInEventText:String	= "focusIn";
	private static var FocusOutEventText:String	= "focusOut";
	
	function InputHtml() {
		super();
	}
	
	function onLoad(Void) : Void {
		txtInput.autoSize  = false;
		txtInput.wordWrap  = false;
		txtInput.multiline = true;
		var parent = this;
		
		var OnChanged:Function = function(textfield:TextField) {
			if(Selection.getBeginIndex() != -1 && Selection.getEndIndex() != -1){
				parent.startIndex = Selection.getBeginIndex();
				parent.endIndex = Selection.getEndIndex();
				parent.OnTextChange();
			}
		};
		
		var mouseListener:Object = new Object();
		mouseListener.onMouseUp = OnChanged;
		Mouse.addListener(mouseListener);
		
		var keyListener:Object = new Object();
		keyListener.onKeyUp = OnChanged;
		Key.addListener(keyListener);
	}

	public function DataBind(data:Object) : Void {
		if (data.Format != undefined && startIndex != -1 && endIndex != -1) {
			var format:TextFormat = data.Format;
			CurrentFormat = format;
		}
		if (data.Width != undefined && data.Height != undefined) {
			var pxToMmCoeff:Number = 0.35277777777777;
			txtInput._width = data.Width   / pxToMmCoeff;
			txtInput._height = data.Height / pxToMmCoeff;
		}
		if (data.ChangeFocus) {
			var intervalObject:Object = new Object();
			var interval:Number = setInterval(this, "Select", 10, intervalObject);
			intervalObject["interval"] = interval;
		}
	}
	
	private function Select(intervalObject:Object) : Void {
		clearInterval(intervalObject.interval);
		trace(Selection.getFocus());
		Selection.setFocus(txtInput);
		Selection.setSelection(startIndex, endIndex);
	}
	
	public function set CurrentFormat(value:TextFormat) : Void {
		if (value != undefined && value != null) {
			if(startIndex == -1 && endIndex == -1)
				txtInput.setTextFormat(value);
			else if (startIndex == endIndex && startIndex == txtInput.length)
				txtInput.setTextFormat(startIndex, value);
			else
				txtInput.setTextFormat(startIndex, endIndex, value);
			
			var index:Number = 0;
			if (startIndex == endIndex)
				index = Math.max(0, startIndex - 1);
			else
				index = endIndex - 1;
				
			var newTextFormat:TextFormat = txtInput.getTextFormat(index);
			for(var key:String in value)
				if (value[key] != null)
					newTextFormat[key] = value[key];
			txtInput.setNewTextFormat(newTextFormat);
		}
	}
	
	public function get CurrentFormat() : TextFormat {
		if(startIndex == -1 && endIndex == -1)
			return null;
		else if (startIndex == endIndex) {
			var index:Number = startIndex;
			if (index == txtInput.length)
				-- index;
			return txtInput.getTextFormat(index);
		}
		else
			return txtInput.getTextFormat(startIndex, endIndex);
	}
	
	public function RegisterOnTextChangeHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener(TextChangeEventText, Delegate.create(scopeObject, callBackFunction));
    }

	public function set XmlText(text:String) : Void {
		WatermarkHelper.SetTextFieldXml(txtInput, text);
	}
    
    public function get XmlText() : String {
    	return WatermarkHelper.GetTextFieldXml(txtInput);
    }
    
    private function OnTextChange() : Void {
    	var format:TextFormat = CurrentFormat;
  		var eventObject:Object = {type:TextChangeEventText, target:this, format:format};
  		this.dispatchEvent(eventObject);
    } 
}