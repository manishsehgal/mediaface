import mx.core.UIObject;
import mx.utils.Delegate;


class Watermark.Menu extends UIObject {
	private var btnSave:mx.controls.Button;
	private var btnCancel:mx.controls.Button;
	
	private static var ClickEventText:String	= "click";
	private static var SaveEventText:String		= "Save";
	private static var CancelEventText:String	= "Cancel";
	
	function Menu() {
		super();
	}
	
	function onLoad(Void) : Void {
		btnSave.addEventListener(ClickEventText, Delegate.create(this, OnSaveClick));
		btnCancel.addEventListener(ClickEventText, Delegate.create(this, OnCancelClick));
	}

	public function RegisterOnSaveHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener(SaveEventText, Delegate.create(scopeObject, callBackFunction));
    }

	public function RegisterOnCancelHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener(CancelEventText, Delegate.create(scopeObject, callBackFunction));
    }

	private function OnSaveClick(eventObject:Object) : Void {
		var eventObject:Object = {type:SaveEventText, target:this};
		this.dispatchEvent(eventObject);
	}

	private function OnCancelClick(eventObject:Object) : Void {
		var eventObject:Object = {type:CancelEventText, target:this};
		this.dispatchEvent(eventObject);
	}
}