import mx.core.UIObject;
import mx.utils.Delegate;

class Watermark.Properties extends UIObject {
	private var btnLeft		: mx.controls.Button;
	private var btnCenter	: mx.controls.Button;
	private var btnRight	: mx.controls.Button;
	private var btnBold		: mx.controls.Button;
	private var btnItalic	: mx.controls.Button;
	private var btnUnderline: mx.controls.Button;
	
	private var cboFont		: mx.controls.ComboBox;
	private var cboFontSize	: mx.controls.ComboBox;
	
	private var txtWidth	: mx.controls.TextInput;
	private var txtHeight	: mx.controls.TextInput;
	
	private var lblAlignment:mx.controls.Label;
	
	private var ctlColorPicker : ColPicker.ColorPicker;
	
	public static var AlignLeft:String		= "left";
	public static var AlignCenter:String	= "center";
	public static var AlignRight:String		= "right";
	
	private static var ClickEventText:String		= "click";
	private static var ChangeEventText:String		= "change";
	private static var FontChangeEventText:String	= "FontChange";
	private static var WidthChangeEventText:String	= "WidthChange";
	private static var HeightChangeEventText:String	= "HeightChange";
	
	public static var FontSizes : Array = [4,5,6,7,8,9,10,12,14,16,18,20,24,28,32,36,40,44,48,54,60,66,72,80,88,96,106,117,127];
	public static var FontNames : Array = ["Arial", "Arial Black"];

	function Properties() {
		super();
	}
	
	function onLoad(Void) : Void {
		super.init();
		txtWidth.restrict  = txtHeight.restrict = "0-9";
		txtWidth.addEventListener(ChangeEventText, Delegate.create(this, OnWidthChange));
		txtHeight.addEventListener(ChangeEventText, Delegate.create(this, OnHeightChange));
		
		btnLeft.addEventListener(ClickEventText, Delegate.create(this, OnAlignLeft));
		btnCenter.addEventListener(ClickEventText, Delegate.create(this, OnAlignCenter));
		btnRight.addEventListener(ClickEventText, Delegate.create(this, OnAlignRight));
		
		btnBold.addEventListener(ClickEventText, Delegate.create(this, OnFontBold));
		btnItalic.addEventListener(ClickEventText, Delegate.create(this, OnFontItalic));
		btnUnderline.addEventListener(ClickEventText, Delegate.create(this, OnFontUnderline));

        cboFontSize.addEventListener(ChangeEventText, Delegate.create(this, OnFontSizeChange));
		cboFont.addEventListener(ChangeEventText, Delegate.create(this, OnFontNameChange));
		
		cboFontSize.setStyle("openDuration", 0);
		cboFontSize.setStyle("openEasing", 0);
		cboFont.setStyle("openDuration", 0);
		cboFont.setStyle("openEasing", 0);
		
		var parent = this;
		var listener:Object = new Object();
		listener.change = function(eventObject:Object) {
			var color:Number = eventObject.color;
			parent.OnColorChange(color);
		};
		ctlColorPicker.addListener(listener);
		
		lblAlignment.visible = false;
		btnLeft.visible = false;
		btnCenter.visible = false;
		btnRight.visible = false;
	}

	function DataBind(data : Object) : Void {
		if (data.Bind == true || cboFont.length == undefined || cboFont.length == null || cboFont.length == 0) {
			cboFont.removeAll();
			for (var i:Number = 0; i < FontNames.length; ++ i)
				cboFont.addItem(FontNames[i]);
		}
		if (data.Bind == true || cboFontSize.length == undefined || cboFontSize.length == null || cboFontSize.length == 0) {
			cboFontSize.removeAll();
	        for (var i:Number = 0; i < FontSizes.length; ++ i)
	            this.cboFontSize.addItem(FontSizes[i]);
		}
		
		if (data.Width != undefined && data.Width != null) {
			txtWidth.text  = data.Width;
		}
		if (data.Height != undefined && data.Height != null) {
			txtHeight.text = data.Height;
		}
		
		if (data.Format != undefined && data.Format != null) {
			var format:TextFormat = data.Format;
			
			btnLeft.selected = format.align == AlignLeft;
			btnRight.selected = format.align == AlignRight;
			btnCenter.selected = format.align == AlignCenter;

			ctlColorPicker.color = format.color;
			
			btnBold.selected = format.bold == true;
			btnItalic.selected = format.italic == true;
			btnUnderline.selected = format.underline == true;
			
			if (format.font != undefined) {
				for (var i:Number = 0; i < cboFont.length; ++ i)
					if (cboFont.getItemAt(i).label == format.font)
						cboFont.selectedIndex = i;
			} 
			
			if (format.size != undefined) {
				for (var i:Number = 0; i < cboFontSize.length; ++ i)
					if (cboFontSize.getItemAt(i).label == format.size)
						cboFontSize.selectedIndex = i;
			} 
		}
	}
	
	public function RegisterOnFontChangeHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener(FontChangeEventText, Delegate.create(scopeObject, callBackFunction));
    }
	
	public function RegisterOnWidthChangeHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener(WidthChangeEventText, Delegate.create(scopeObject, callBackFunction));
    }
	
	public function RegisterOnHeightChangeHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener(HeightChangeEventText, Delegate.create(scopeObject, callBackFunction));
    }

	private function OnWidthChange(eventObject:Object) : Void {
		var width:Number = parseInt(txtWidth.text, 10);
		var eventObject:Object = {type:WidthChangeEventText, target:this, width:width};
		this.dispatchEvent(eventObject);
	}

	private function OnHeightChange(eventObject:Object) : Void {
		var height:Number = parseInt(txtHeight.text, 10);
		var eventObject:Object = {type:HeightChangeEventText, target:this, height:height};
		this.dispatchEvent(eventObject);
	}
	
	private function OnAlignLeft(eventObject:Object) : Void {
		var format:TextFormat = new TextFormat();
		format.align = AlignLeft;
		btnLeft.selected = true;
		btnCenter.selected = false;
		btnRight.selected = false;
		OnFontChange(format);
	}
	
	private function OnAlignCenter(eventObject:Object) : Void {
		var format:TextFormat = new TextFormat();
		format.align = AlignCenter;
		btnLeft.selected = false;
		btnCenter.selected = true;
		btnRight.selected = false;
		OnFontChange(format);
	}
	
	private function OnAlignRight(eventObject:Object) : Void {
		var format:TextFormat = new TextFormat();
		format.align = AlignRight;
		btnLeft.selected = false;
		btnCenter.selected = false;
		btnRight.selected = true;
		OnFontChange(format);
	}
	
	private function OnFontBold(eventObject:Object) : Void {
		var format:TextFormat = new TextFormat();
		format.bold = eventObject.target.selected;
		OnFontChange(format);
	}
	
	private function OnFontItalic(eventObject:Object) : Void {
		var format:TextFormat = new TextFormat();
		format.italic = eventObject.target.selected;
		OnFontChange(format);
	}
	
	private function OnFontUnderline(eventObject:Object) : Void {
		var format:TextFormat = new TextFormat();
		format.underline = eventObject.target.selected;
		OnFontChange(format);
	}
	
	private function OnFontSizeChange(eventObject:Object) : Void {
		var format:TextFormat = new TextFormat();
		format.size = parseInt(eventObject.target.text_mc.label.text);
//		eventObject.target.doLater(btnBold, "setFocus");
		OnFontChange(format);
	}
	
	private function OnFontNameChange(eventObject:Object) : Void {
		var format:TextFormat = new TextFormat();
//		format.font = eventObject.target.selectedItem.data;
		format.font = eventObject.target.selectedItem.label;
//		eventObject.target.doLater(btnBold, "setFocus");
		OnFontChange(format);
	}
	
	private function OnColorChange(color:Number) : Void {
		var format:TextFormat = new TextFormat();
		format.color = color;
		OnFontChange(format);
	}
	
	private function OnFontChange(format:TextFormat) : Void {
		var eventObject:Object = {type:FontChangeEventText, target:this, format:format};
		this.dispatchEvent(eventObject);
	}
}