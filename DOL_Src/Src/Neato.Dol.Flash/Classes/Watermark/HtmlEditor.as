import mx.core.UIObject;
import Watermark.*;


class Watermark.HtmlEditor extends UIObject {
	private var inputHtml:InputHtml;
	private var menu:Menu;
	private var properties:Properties;
	
	private var watermark:Entity.Watermark;
	private var saWatermark:SAWatermark;
	
	function HtmlEditor() {
		super();
		saWatermark = SAWatermark.GetInstance(new XML());
	}

	function onLoad(Void) : Void {
		menu.RegisterOnSaveHandler(this, OnSave);
		menu.RegisterOnCancelHandler(this, OnCancel);
		
		properties.RegisterOnWidthChangeHandler(this, OnWidthChange);
		properties.RegisterOnHeightChangeHandler(this, OnHeightChange);
		properties.RegisterOnFontChangeHandler(this, OnFontChange);
		
		inputHtml.RegisterOnTextChangeHandler(this, OnTextChange);

		saWatermark.RegisterOnLoadedHandler(this, OnXmlLoaded);
		saWatermark.RegisterOnSavedHandler(this, OnXmlSaved);
		saWatermark.BeginLoad(true);
		
		visible = false;
	}
	
	private function PropertiesDataBind(format:TextFormat, bind:Boolean) : Void {
		var data:Object = new Object();
		data.Width = watermark.Width;
		data.Height = watermark.Height;
		data.Format = format;
		data.Bind = bind == true;
		properties.DataBind(data);
	}
	
	private function InputHtmlDataBind(format:TextFormat, changeFocus:Boolean) : Void {
		var data:Object = new Object();
		data.Format = format;
		data.Width = watermark.Width;
		data.Height = watermark.Height;
		data.ChangeFocus = changeFocus == true;
		inputHtml.DataBind(data);
	}
	
	private function OnSave(eventObject:Object) : Void {
		watermark.XmlText = inputHtml.XmlText;
		var xml:XML = WatermarkHelper.GenerateXml(watermark);
		trace("OnSave = "+xml.toString());
		saWatermark.BeginSave(xml);
	}

	private function OnCancel(eventObject:Object) : Void {
		visible = false;
		saWatermark.BeginLoad();
	}

	private function OnWidthChange(eventObject:Object) : Void {
		watermark.Width = eventObject.width;
		InputHtmlDataBind(null, false);
	}

	private function OnHeightChange(eventObject:Object) : Void {
		watermark.Height = eventObject.height;
		InputHtmlDataBind(null, false);
	}

	private function OnFontChange(eventObject:Object) : Void {
		var format:TextFormat = eventObject.format;
		InputHtmlDataBind(format, true);
	}
	
	private function OnTextChange(eventObject:Object) : Void {
		var format:TextFormat = eventObject.format;
		PropertiesDataBind(format);
	}
	
	private function OnXmlLoaded(eventObject:Object) : Void {
		visible = true;
		trace("OnLoad = "+eventObject.xml.toString());
		//getURL("javascript:alert('OnLoad"+eventObject.xml.toString()+"')");
		watermark = WatermarkHelper.ParseWatermark(eventObject.xml);
		var fonts:Array = WatermarkHelper.ParseFonts(eventObject.xml);

		var rebind:Boolean = false;
		if (fonts != null && fonts != undefined) {
			rebind = true;
			Properties.FontNames = fonts; 
		}
		//getURL("javascript:alert('OnXmlLoaded = "+watermark.XmlText+"')");
		inputHtml.XmlText = watermark.XmlText;
		PropertiesDataBind(null, rebind);
		InputHtmlDataBind(null, false);
	}
	
	private function OnXmlSaved(eventObject:Object) : Void {
	}
}