﻿class GradientCirclePrimitive implements IDraw {
	private var x:Number;
	private var y:Number;
	private var r:Number;
	private var colors:Array;
	private static var alphas:Array = [100, 0];
	private static var ratios:Array = [0, 0xFF];
	
	function GradientCirclePrimitive(node:XMLNode) {
		x = Number(node.attributes.x);
		y = Number(node.attributes.y);
		r = Number(node.attributes.r);
	}
	
	function Draw(mc:MovieClip, originalX:Number, originalY:Number, rotationAngle:Number, scaleX:Number, scaleY:Number):Void {
		if(scaleX==undefined)scaleX=1.0;
		if(scaleY==undefined)scaleY=1.0;
		var c = new Object({x:originalX+x,  y:originalY+y});
		if (rotationAngle > 0 || rotationAngle < 0) {
			c = _global.RotateAboutCenter(c.x, c.y, originalX, originalY, rotationAngle);
		}
		var absX = c.x*scaleX;
		var absY = c.y*scaleY;
		var absR = r*(scaleX<=scaleY?scaleX:scaleY);
		colors = [mc.color, mc.color];
				
		var matrix:Object = {matrixType:"box", x:absX - absR, y:absY - absR, w:2*absR, h:2*absR, r:0};
		mc.beginGradientFill("radial", colors, alphas, ratios, matrix);
		mc.lineStyle(undefined);
		CirclePrimitive.drawCircle(mc, absX, absY, absR);
		mc.endFill();
	}	
}
