﻿
class ContourUnit extends Unit {
	private var owner:FaceUnit;			// reference to owning FaceUnit
	private var outlineContour:Array;
	private var realContour:Array;
	private var markLines:Array;
	private var cutLines:Array;
	private var bgColor:Number;
	private var bgColor2:Number;
	private var gradient:Object;
	private var isFilled:Boolean;
	private var contourWidth:Number = null;
	private var contourHeight:Number = null;
	private var contourX:Number = 0;
	private var contourY:Number = 0;
	
	private var realContourDashOn:Number = 5;
	private var realContourDashOff:Number = 5;
		
	function ContourUnit(mc:MovieClip, node:XML, owner:FaceUnit) {
		super(mc, node);
		this.owner = owner;
		outlineContour = new Array();
		realContour = new Array(); 		
		markLines = new Array();
		cutLines = new Array();
		for (var i:Number = 0; i < node.childNodes.length; ++i) {
			var childNode:XML = node.childNodes[i];
			switch (childNode.nodeName) {
				case "CutLine":
					cutLines.push(Logic.DrawingHelper.CreatePrimitive(childNode));
					break;
				case "RealContour":	
					var contourNodes:Array = childNode.childNodes;					
					for (var j:Number = 0; j < contourNodes.length; ++j) {
						realContour.push(Logic.DrawingHelper.CreatePrimitive(contourNodes[j]));						
					}
					break;		
				case "MarkLines":
					var markLinesNodes:Array = childNode.childNodes;
					for (var j:Number = 0; j < markLinesNodes.length; ++j) {
						markLines.push(Logic.DrawingHelper.CreatePrimitive(markLinesNodes[j]));
					}				
					break;
				default:
					outlineContour.push(Logic.DrawingHelper.CreatePrimitive(childNode));
					break;
			}
		}
	}
	
	function CreateXmlNode():XML {
		return new XML();
	}

	private function Contour():Array {
		if(owner.UseRealContour && realContour.length > 0)
			return realContour;
		else
			return outlineContour.length > 0 ? outlineContour : realContour; 			
	}

	private function RealContour():Array {
		if(owner.UseRealContour)
			return new Array();
		else
			return realContour; 			
	}
	
	public function HasOutline() {
		var realContour:Array = RealContour(); 
		return Contour() != RealContour() && realContour.length > 0 ;
	}

	private function DrawContour(mc:MovieClip, contour:Array) {
		for (var i:Number = 0; i < contour.length; ++i) {
			var primitive:IDraw = contour[i];
			primitive.Draw(mc, 0, 0);
		}		
	}
		
	function SetBgColor(IsFilled:Boolean, BgColor:Number):Void {
		gradient = null;
		isFilled = IsFilled;
		bgColor = BgColor;
		if (!isFilled)
			bgColor = 0xFFFFFF;
	}
	
	function SetGradientColor(IsFilled:Boolean, BgColor:Number, BgColor2:Number, GradientType:String):Void {
		SetBgColor(IsFilled, BgColor);
		if (BgColor2 != null && BgColor2 != undefined && GradientType != null && GradientType != undefined && GradientType != "") {
			gradient = _global.Gradients.GetGradient(GradientType);
			gradient.color1 = BgColor;
			gradient.color2 = BgColor2;
			bgColor2 = BgColor2;
		}
	}
	
	function SetBgAndDraw(IsFilled:Boolean, BgColor:Number, BgColor2:Number, GradientType:String):Void {
		mc.clear();
		SetGradientColor(IsFilled, BgColor, BgColor2, GradientType);
		Draw();
	}
	
	function Draw():Void {
		trace("Contour Draw");
		trace(mc);
		DrawMC(mc);
	}
	
	function DrawMC(mc:MovieClip, transparent:Boolean):Void {
		//_global.tr("ContourUnit.DrawMC tr = " + transparent);
		//_global.tr("### ContourUnit.DrawMC() contourWidth = " + contourWidth + " width = " + Width + " mc._xscale = " + mc._xscale + " x = " + X + " y = " + Y);

		var contour:Array = Contour();		
		
		if(transparent)
			DrawContour(mc,contour);
		else {					
			if (gradient != null && gradient.name != "00") {
				mc.color  = this.gradient.color1;
				mc.color2 = this.gradient.color2;
				var primitive:GradientRectPrimitive/*:IDraw*/ = new GradientRectPrimitive(this.gradient.drawSequence[0]);
				
				primitive.Primitives = contour;
				primitive.ScaleX = contourWidth / primitive.Width;
				primitive.ScaleY = contourHeight / primitive.Height;
				primitive.Draw(mc, contourX, contourY, 0, 1, 1);
			}
			else {
				if (!isFilled)
					bgColor = 0xFFFFFF;

				mc.beginFill(bgColor, 100);
	
				DrawContour(mc,contour);			

				mc.endFill();
			}
		}
		
		if (contourWidth == null || contourWidth == undefined || contourHeight == null || contourHeight == undefined) {
			contourWidth = mc._width * 100 / mc._xscale;
			contourHeight = mc._height * 100 / mc._yscale;
			var q:Object = mc.getBounds(mc);
			contourX = q.xMin;
			contourY = q.yMin;
			DrawMC(mc, transparent);
		}
	}
	
	function DrawMask(mc:MovieClip, rc:Object):Void {
		mc.clear();
		
		mc.beginFill(_global.styles.MainLayerPanel.backgroundColor, 90);
		//mc.beginFill(0x00FF00, 50);
		
		mc.moveTo(rc.x,      rc.y);
		mc.lineTo(rc.x+rc.w, rc.y);
		mc.lineTo(rc.x+rc.w, rc.y+rc.h);
		mc.lineTo(rc.x,      rc.y+rc.h);
		mc.lineTo(rc.x,      rc.y);

		DrawContour(mc,Contour());		

		mc.endFill();
	}
	
	function DrawHoles(mc:MovieClip):Void {
		var bounds:Object = mc.getBounds(mc);
		var mcHoles:MovieClip = mc.createEmptyMovieClip("Holes", mc.getNextHighestDepth());
		
		mcHoles.beginFill(0xFFFFFF, 100);
		
		var xMin:Number = bounds.xMin - 1;
		var yMin:Number = bounds.yMin - 1;
		var xMax:Number = bounds.xMax + 1;
		var yMax:Number = bounds.yMax + 1;

		mcHoles.moveTo(xMin, yMin);
		mcHoles.lineTo(xMin, yMax);
		mcHoles.lineTo(xMax, yMax);
		mcHoles.lineTo(xMax, yMin);
		mcHoles.lineTo(xMin, yMin);
		
		DrawContour(mcHoles,Contour());
		mcHoles.endFill();
		mc.setMask(mcHoles);
	}

	function DrawRealContour(mc:MovieClip):Void {
		trace("DrawRealContour");
//		var contourDrawer:DashedLineDrawer = 
//				new DashedLineDrawer(mc /*mcMarkLines*/,realContourDashOn,realContourDashOff);
//		DrawContour(contourDrawer,RealContour());
		DrawContour(mc,RealContour());
	}

	function DrawMarkLines(mc:MovieClip):Void {
		trace("DrawMarkLines");
		//var mcMarkLines:MovieClip = mc.createEmptyMovieClip("MarkLines", mc.getNextHighestDepth());
		DrawContour(mc /*mcMarkLines*/,markLines);		
	}
	
	function DrawCutLines(mc:MovieClip):Void {
		trace("DrawCutLines");
		var mcCutLines:MovieClip = mc.createEmptyMovieClip("CutLines", mc.getNextHighestDepth());
		mcCutLines.lineStyle(0);
		DrawContour(mcCutLines,cutLines);		
	}
}