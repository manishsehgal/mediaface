﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;
[Event("onLoaded")]
class SAGradients {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SAGradients.prototype);

	private var types:XML;
	
	public static function GetInstance(types:XML):SAGradients {
		return new SAGradients(types);
	}
	
	public function get IsLoaded():Boolean {
		return types.loaded;
	}
	
	private function SAGradients(types:XML) {
		var parent = this;
		
		this.types = types;
		this.types.onLoad = function(success) {
			var eventObject = {type:"onLoaded", target:parent};
			trace("SAGradients: dispatch types onLoaded");
			parent.dispatchEvent(eventObject);
		};
	}

	public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }

	public function BeginLoad() {
		var currentDate:Date = new Date();
		var url:String = "GradientTypes.xml";
		if (SALocalWork.IsLocalWork)
			url = "../Neato.Dol.WebDesigner/GradientTypes.xml";
		trace("SAGradients before load:"+url);
		BrowserHelper.InvokePageScript("log", url);
		
		this.types.load(url);
		trace("SAGradients after load:"+url);
		BrowserHelper.InvokePageScript("log", "SAGradients.as: types loaded");
    }
}