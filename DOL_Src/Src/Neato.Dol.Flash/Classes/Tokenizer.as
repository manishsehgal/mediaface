﻿class Tokenizer {
	private var list:Array;
	private var listIndex:Number;

	public function Tokenizer(txt:String) {
		list = new Array();
		var i:Number = 0;
		var len:Number = txt.length;
		while (i < len) {
			if (txt.charAt(i) != " ") {
				list.push(txt.charAt(i));
				++i;
			} else {
				var begin:Number = i;
				var end:Number = i + 1;
				while(txt.charAt(end) == " ")
					++end;
				list.push(txt.substring(begin, end));
				i = end;
			}
		}
		listIndex = 0;
	}
	
	public function IsFirstToken():Boolean {
		return listIndex == 0;
	}
	
	public function IsSpaceToken():Boolean {
		var str:String = list[listIndex];
		return str.charAt(0) == " ";
	}
	
	public function IsPrevTokenSpace():Boolean {
		var str:String = list[listIndex-1];
		return str.charAt(0) == " ";
	}
	
	public function IsNextTokenCR():Boolean {
		var str:String = list[listIndex+1];
		if (str == undefined || str.length == 0) {
			return true;
		}
		return str.charAt(0) == "\r";
	}
	
	public function IsPrevTokenCR():Boolean {
		var str:String = list[listIndex-1];
		if (str == undefined || str.length == 0) {
			return true;
		}
		return str.charAt(0) == "\r";
	}
	
	public function GetToken():String {
		return list[listIndex];
	}
	
	public function MoveNext() {
		++listIndex;
	}
}