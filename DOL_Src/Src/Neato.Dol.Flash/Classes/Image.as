﻿import mx.utils.Delegate;
import mx.core.UIObject;
import flash.display.BitmapData;
import flash.filters.ColorMatrixFilter;
import flash.geom.ColorTransform;
import flash.geom.Rectangle;
import flash.geom.Matrix;
import flash.geom.Point;

class Image extends mx.core.UIObject {
	private var url:String;
	private var status:String = "empty";
	private var queue:Array;
	private var showPreloader:Boolean = false;
	private var name:String;
	private var id:Number;
	private var noAttach:Boolean = false;
	
	public function Image() {
		super();
		this.queue = new Array();
		RegisterOnImageLoadedHandler(this, EndLoad);
	}

	public function get NoAttach():Boolean {
		return noAttach;
	}
	
	public function set NoAttach(value:Boolean):Void {
		noAttach = value;
	}
	
	public function get Id():Number {
		return id;
	}
	
	public function set Id(value:Number):Void {
		this.id = value;
	}
	
	public function get Url():String {
		return url;
	}
	
	public function set Url(value:String):Void {
		this.url = value;
		BeginLoad();
	}
	
	public function get Name():String {
		return name;
	}
	
	public function set Name(value:String):Void {
		name = value;
	}
	
	public function get ShowPreloader():Boolean {
		return showPreloader == true ? true : false;
	}
	
	public function set ShowPreloader(value:Boolean):Void {
		showPreloader = value == true ? true : false;
	}
	
	public function get ImageContent():UIObject {
		return this["content"];
	}
	
	public function get ImageBitmap():BitmapData {
		var bounds:Object = ImageContent.getBounds(ImageContent);
		var width:Number = bounds.xMax - bounds.xMin;
		var height:Number = bounds.yMax - bounds.yMin;
//		_global.tr("### +++ get ImageBitmap() status = " + status + " w = " + width + " h = " + height);

		var bmp:BitmapData = new BitmapData(width, height, true, 0x0);
		bmp.draw(ImageContent, new Matrix(1, 0, 0, 1, 0, 0), new ColorTransform(), new Object(), new Rectangle(0, 0, width, height));
		
		return bmp;
	}

	public function get IsEmpty():Boolean {
		return status == "empty";
	}

	public function get IsLoading():Boolean {
		return status == "loading";
	}

	public function get IsLoaded():Boolean {
		return status == "loaded";
	}

	private function BeginLoad():Void {
		if (IsLoading) return;
		
//		_global.tr("### Image.BeginLoad url = " + url);
		status = "loading";
		
		var mclListener:Object = new Object();
		var mcl:MovieClipLoader = new MovieClipLoader();
		var mcImage = this.createObject("UIObject", "content", 0, new Object());
		mcImage.url = url;

		var ref = this;
		mclListener.onLoadInit = function(target_mc) {
			ref.OnImageLoaded();
		};
		
		if (ShowPreloader == true)
			Preloader.Show();
		mcl.addListener(mclListener);
		mcl.loadClip(url, mcImage);
	}
	
	private function EndLoad(eventObject:Object):Void {
//		_global.tr("### Image.EndLoad");
		if (ShowPreloader == true)
			Preloader.Hide();
		status = "loaded";
		var bmp:BitmapData = ImageBitmap;
		var length:Number = queue.length;
		for (var i:Number = 0; i < length; ++ i) {
			AttachImage(queue.pop(), bmp, false);
		}
		OnImageAttached(bmp);
	}
	
	public function AttachImage(mc, bmp:BitmapData, sendEvent:Boolean /* == true */):Void {
//		_global.tr("### Image.AttachImage mc = " + mc._name + " status = " + status + " queue.length = " + queue.length);
		if (!IsLoaded) {
			queue.push(mc);
			return;
		}
		if (bmp == undefined || bmp == null) {
			bmp = ImageBitmap;
		}
//		_global.tr("### Image.AttachImage NoAttach = " + NoAttach);
		if (NoAttach == false)
			mc.attachBitmap(bmp, 0);
		if (sendEvent != false)
			OnImageAttached(bmp);
	}
	
	private function RegisterOnImageLoadedHandler(scopeObject:Object, callBackFunction:Function):Void {
		this.addEventListener("imageLoaded", Delegate.create(scopeObject, callBackFunction));
    }
	
	private function OnImageLoaded():Void {
		var eventObject:Object = {type:"imageLoaded", target:this};
		this.dispatchEvent(eventObject);
	}
	
	private function RegisterOnImageAttachedHandler(scopeObject:Object, callBackFunction:Function):Void {
		if (scopeObject.RegisterOnImageAttachedHandler != undefined) { 
//			_global.tr("### RETURN");
			return;
		}
		var delegate:Function = Delegate.create(scopeObject, callBackFunction);
		scopeObject.RegisterOnImageAttachedHandler = delegate;
		this.addEventListener("imageAttached", delegate);
    }

	private function UnregisterOnImageAttachedHandler(scopeObject:Object):Void {
//		_global.tr("UnregisterOnImageAttachedHandler scopeObject = " + scopeObject);
		if (scopeObject.RegisterOnImageAttachedHandler != undefined) { 
			var delegate:Function = scopeObject.RegisterOnImageAttachedHandler;
			this.removeEventListener("imageAttached", delegate);
			scopeObject.RegisterOnImageAttachedHandler = undefined;
		}	
	}
	
	private function OnImageAttached(bmp:BitmapData):Void {
//		_global.tr("### OnImageAttached");
		var eventObject:Object = {type:"imageAttached", target:this, bmp:bmp};
		this.dispatchEvent(eventObject);
	}
}