﻿class LabelSwitcher extends mx.core.UIObject {
	private var mcNavigator:MovieClip;
	private var maxWidth:Number;
	private var maxHeight:Number;
	private var multiLabelModeValue:Boolean;
	private var selectedLabelIndex:Number = 0;
	
	public function get SelectedLabelIndex():Number {
		return selectedLabelIndex;
	}
	
	function LabelSwitcher() {
		if (this instanceof LabelSwitcher) {
			//_global.tr("LabelSwitcher() this instanceof LabelSwitcher");
			_global.LabelSwitcher = this;
		}
	}
	
	public function set Visible(value:Boolean):Void {
		this._visible = multiLabelModeValue && value;
	}
	
	public function DataBind():Void {
		multiLabelModeValue = _global.Project.CurrentPaper.Faces.length > 1;
		if (multiLabelModeValue) {
			Draw();
			this._visible = true;
		} else
			this._visible = false;
	}

	private function onLoad() {
		maxWidth = 186;
		maxHeight = 200;

		this._visible = false;
	}
	
	function SelectFace(index:Number){
		//_global.tr("____ SelectFace index = " + index);
		selectedLabelIndex = index;
	}
	
	function Draw():Void {
		selectedLabelIndex = _global.FaceHelper.GetFaceIndex(_global.Project.CurrentPaper.Faces, _global.Project.CurrentPaper.CurrentFace);
		mcNavigator.clear();
		var w:Number = _global.Project.CurrentPaper.Width;
		var h:Number = _global.Project.CurrentPaper.Height;
		
		_global.PaperHelper.Draw(mcNavigator, this, SelectFace, true, false, 
								_global.Project.CurrentPaper.Faces, w, h, false, false, true, false);
		
		var kx:Number = maxWidth / mcNavigator._width;
		var ky:Number = maxHeight / mcNavigator._height;
		var k = kx <= ky ? kx : ky;
		mcNavigator._xscale = mcNavigator._yscale = k * 100;

		mcNavigator.lineStyle(2 / k, 0x999999);
		mcNavigator.moveTo(0, 0);
		mcNavigator.lineTo(0, h);
		mcNavigator.lineTo(w, h);
		mcNavigator.lineTo(w, 0);
		mcNavigator.lineTo(0, 0);
		
		this.cacheAsBitmap = true;
		//mcNavigator._x = (maxWidth - mcNavigator._width + 4) / 2;
		//_global.tr("#### mcNavigator._x = " + mcNavigator._x);
	}
}