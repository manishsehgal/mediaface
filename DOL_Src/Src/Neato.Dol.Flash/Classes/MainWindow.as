﻿class MainWindow {
	
	function MainWindow() {
	}
	
	public function get MainMenu() : Menu.MainMenu {
		return Menu.MainMenu.getInstance();
	}
	
	public function get WorkspaceTab() : Workspace.WorkspaceTab {
		return Workspace.WorkspaceTab.getInstance();
	}
	
	public function get DesignArea() : Workspace.DesignArea {
		return Workspace.DesignArea.getInstance();
	}
	
	public function get PreviewArea() : Workspace.PreviewArea {
		return Workspace.PreviewArea.getInstance();
	}
	
	public function set Mode(mode:String) {
		if (mode == "design") {
			_global.UIController.ToolsController.changePaperToolController.Hide();
			_root.pnlMainLayer.content.pnlTools._visible = true;
			this.DesignArea.Visible = true;
			_global.Navigator.Visible = true;
			_global.LabelSwitcher.Visible = false;
			_root.pnlMainLayer.content.pnlPreviewDesc._visible = false;
			this.PreviewArea.Visible = false;
			this.MainMenu.Enabled = true;
			this.MainMenu.FullMenu = true;
			_root.pnlMainLayer.content.pnlFloatMenu._visible = true;
			_root.pnlMainLayer.content.pnlDesignArea._visible = true;
			WorkspaceTab.SetSel("design");
		}
		else if (mode == "preview") {
			_global.UIController.ToolsController.changePaperToolController.Hide();
			_root.pnlMainLayer.content.pnlPreviewDesc._visible = true;
			this.PreviewArea.Visible = true;
			//this.PreviewArea.Draw();
			_root.pnlMainLayer.content.pnlTools._visible = false;
			this.DesignArea.Visible = false;
			_global.Navigator.Visible = false;
			_global.LabelSwitcher.Visible = true;
			_global.LabelSwitcher.DataBind();
			_root.pnlMainLayer.content.pnlPreviewDesc.Init();
			this.MainMenu.Enabled = false;
			this.MainMenu.FullMenu = false;
			_root.pnlMainLayer.content.pnlFloatMenu._visible = false;
			_root.pnlMainLayer.content.pnlDesignArea._visible = true;
			WorkspaceTab.SetSel("preview");
		}
		else if (mode == "changePaper") {
			_global.UIController.ToolsController.changePaperToolController.Show();
			this.MainMenu.Enabled = false;
			this.MainMenu.FullMenu = true;
			_global.Navigator.Visible = false;
			_global.LabelSwitcher.Visible = false;
			_root.pnlMainLayer.content.pnlFloatMenu._visible = false;
			_root.pnlMainLayer.content.pnlDesignArea._visible = false;
		}
	}
}