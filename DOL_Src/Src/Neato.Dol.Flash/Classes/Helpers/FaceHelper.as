﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;

[Event("onFaceChanged")]
class Helpers.FaceHelper {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(FaceHelper.prototype);
	
	public function RegisterOnFaceChangedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onFaceChanged", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function ChangeFace(index:Number):Void {
		if(_global.Mode == _global.PaintMode)
			_global.UIController.ToolsController.paintToolController.paintAddTool_onEnd();

		_global.Mode = _global.InactiveMode;
		_global.Project.CurrentPaper.CurrentFace.Hide();
		_global.Project.CurrentPaper.CurrentFace = _global.Project.CurrentPaper.Faces[index];
		_global.Project.CurrentPaper.CurrentFace.Show();
		_global.UIController.UnselectCurrentUnit();
		
		var eventObject = {type:"onFaceChanged", index:index};
		trace("ChangeFace: dispatch OnFaceChanged");
		_global.tr("ChangeFace: dispatch OnFaceChanged");
		dispatchEvent(eventObject);
	}
	
	public function CopyCurrentFace(destinationIndex:Number):Void {
		var sourceFaces:Array = _global.Project.CurrentPaper.Faces;
		var destinationFaces:Array = _global.Project.CurrentPaper.Faces;
		var sourceIndex = GetFaceIndex(_global.Project.CurrentPaper.Faces, _global.Project.CurrentPaper.CurrentFace);
		CopyFace(sourceFaces, destinationFaces, sourceIndex, destinationIndex);
	}

	public function GetFaceIndex(faces:Array, face:FaceUnit):Number {
		for(var i:Number = 0; i < faces.length; ++i)
			if(face == faces[i])
				return i;
		return -1;
	}
	
	public function CopyFace(sourceFaces:Array, destinationFaces:Array, sourceFaceIndex:Number, destinationFaceIndex:Number, noChangeUnitCoordinates:Boolean):Void {
		noChangeUnitCoordinates = noChangeUnitCoordinates == true;
		var sourceFace = sourceFaces[sourceFaceIndex];
		var destinationFace = destinationFaces[destinationFaceIndex];

		if(sourceFace == destinationFace)
			return;
		
		//SATracking.CopyDesign();
	
		var faceXml:XMLNode = new XMLNode(1, "Face");
		sourceFace.AddUnitNodes(faceXml, false);

		destinationFace.Clear();
		destinationFace.ParseXMLUnits(faceXml);
		if (!noChangeUnitCoordinates)
			destinationFace.ChangeUnitCoordinatesUnderTransfer(sourceFace.Width, sourceFace.Height);
	}	
}
