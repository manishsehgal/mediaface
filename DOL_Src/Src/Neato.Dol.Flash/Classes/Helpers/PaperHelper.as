﻿class Helpers.PaperHelper {    
    private static var mcFrame:MovieClip;
    
	public static function Clear(mc:MovieClip):Void {
		for(var i:String in mc) {
			if (mc[i] instanceof MovieClip)
				mc[i].removeMovieClip();
		}
		mc.clear();
	}

	public static function DrawBackground(mc:MovieClip, width:Number,
        height:Number, color:Number, alpha:Number):Void {
		mc.lineStyle();
		mc.beginFill(color, alpha);
		mc.moveTo(0, 0);
		mc.lineTo(width, 0);
		mc.lineTo(width, height);
		mc.lineTo(0, height);
		mc.lineTo(0, 0);
		mc.endFill();
	}
	
	public static function DrawBorder(mc:MovieClip, width:Number,
        height:Number, lineWidth:Number, color:Number, alpha:Number):Void {
  		var mcBorder:MovieClip = mc.createEmptyMovieClip("mcBorder", mc.getNextHighestDepth());
  		mcBorder.lineStyle(lineWidth, color, alpha);
  		mcBorder.moveTo(0, 0);
  		mcBorder.lineTo(width, 0);
  		mcBorder.lineTo(width, height);
  		mcBorder.lineTo(0, height);
  		mcBorder.lineTo(0, 0);
	}
	
	public static function DrawOneFace(mc:MovieClip, faces:Array, index:Number, bDrawMark:Boolean):Void {
		//_global.tr("### DrawOneFace " + mc["Face"+index]);
		if(bDrawMark == undefined)
			bDrawMark = false;
		var face:FaceUnit = faces[index];
		var depth:Number = 2 * index;
		
		var mcFace:MovieClip = mc.createEmptyMovieClip("Face" + index, depth);
		//_global.tr("### DrawOneFace " + mcFace);
		face.DrawMC(mcFace);
		if(bDrawMark) {
			DrawWaterMark(mcFace, mcFace._width, mcFace._height);
			_global.tr('Draw mask on watermark');			
			face.DrawContourHoles(mcFace,false);			
		}
		mcFace._x = face.PaperX;
		mcFace._y = face.PaperY;
		
		var mcFaceMask:MovieClip = mc.createEmptyMovieClip("FaceMask" + index, depth + 1);		
		face.DrawContour(mcFaceMask);
		mcFaceMask._x = face.PaperX;
		mcFaceMask._y = face.PaperY;
		mcFace.setMask(mcFaceMask);
//		if (!_global.Project.CurrentPaper.IsLightscribeDevice)
//			mcFace.cacheAsBitmap = true;
	}
	
	public static function DrawFace(mc:MovieClip, faces:Array, bDrawMark:Boolean):Void {
		if(bDrawMark == undefined)
			bDrawMark = false;
		var count:Number = faces.length;
		for (var i:Number = 0; i < count; ++i) {
			DrawOneFace(mc, faces, i, bDrawMark);
		}
	}
        
	public static function DrawFaceContour(mc:MovieClip, obj:Object,
				callBackFunction:Function, showFrameAroundSelectedFace:Boolean,
				selectCurrentFace:Boolean, faces:Array,
				drawRealContour:Boolean, drawMarkLines:Boolean, drawCutLines:Boolean):Void {

		if(drawCutLines ==  undefined)
			drawCutLines = true;	
		
		var count:Number = faces.length;
		var depth:Number = 2 * count;
		for (var i:Number = 0; i < count; ++i) {
			var face:FaceUnit = faces[i];
						
			var mcFaceContour:MovieClip = mc.createEmptyMovieClip("FaceContour" + i, depth + i);

			if(obj != null && obj != undefined ) {
				DrawBackground(mcFaceContour, mcFaceContour._width, mcFaceContour._height, 0x0, 0);
				mcFaceContour.index = i;
				if (callBackFunction != null && callBackFunction != undefined) {
					mcFaceContour.onPress = function() {
						callBackFunction.call(obj, this.index);
						if (showFrameAroundSelectedFace) DrawFaceAroundFrame(mc, faces[this.index]);
					};
				}
				if(face == _global.Project.CurrentPaper.CurrentFace && selectCurrentFace)
					mcFaceContour.beginFill(0xCCCCCC, 100);
				else
					mcFaceContour.beginFill(0xffffff, 0);
		
				if (face == _global.Project.CurrentPaper.CurrentFace && showFrameAroundSelectedFace) {
					DrawFaceAroundFrame(mc, face);
				}
			}
			mcFaceContour.lineStyle(0, 0xcccccc);
			face.DrawContour(mcFaceContour, true);
			mcFaceContour.endFill();
			if(drawRealContour)
				face.DrawRealContour(mcFaceContour);
			if(drawMarkLines)
				face.DrawMarkLines(mcFaceContour);
			if(drawCutLines)				
				face.DrawCutLines(mcFaceContour);
			mcFaceContour._x = face.PaperX;
			mcFaceContour._y = face.PaperY;
            
			var bounds:Object = mcFaceContour.getBounds(mcFaceContour);
			face["width"] = bounds.xMax - bounds.xMin;
			face["height"] = bounds.yMax - bounds.yMin;
		}
	}
	
	public static function DrawMask(mc:MovieClip, width:Number, height:Number):Void {
		var mcFaceMask:MovieClip = mc.createEmptyMovieClip("FaceMask", mc.getNextHighestDepth());
		mcFaceMask.lineStyle(0);
		mcFaceMask.beginFill(0xFFFFFF);
		mcFaceMask.moveTo(-1, -1);
		mcFaceMask.lineTo(width + 1, -1);
		mcFaceMask.lineTo(width + 1, height + 1);
		mcFaceMask.lineTo(-1, height + 1);
		mcFaceMask.lineTo(-1, -1);
		mcFaceMask.endFill();
		mc.setMask(mcFaceMask);
	}
	
	public static function Draw(mc:MovieClip, obj:Object, callBackFunction:Function,
					showFrameAroundSelectedFace:Boolean,
					selectCurrentFace:Boolean, faces:Array, 
					width:Number, height:Number,
					drawWaterMark:Boolean, 
					drawRealContour:Boolean, drawMarkLines:Boolean, drawCutLines:Boolean):Void {
	    if(drawWaterMark == undefined)	
			drawWaterMark = false;
		mc._xscale = mc._yscale = 100;
		Clear(mc);
		
		DrawBackground(mc, width, height, 0xFFFFFF, 100);
		
		DrawFace(mc, faces, drawWaterMark);
		
		DrawFaceContour(mc, obj, callBackFunction,
			showFrameAroundSelectedFace, selectCurrentFace, faces, 
			drawRealContour, drawMarkLines, drawCutLines);
		
		DrawMask(mc, width, height);
		
		//_global.tr('bDrawMark = '+bDrawMark);

		//draw border again
		//DrawBorder(mc, width, height, 1, 0x0, 100);
	}
	
	private static var WatermarkFirstDraw:Boolean = true;
	public static function DrawWaterMark(mc:MovieClip, width:Number, height:Number):Void {
		//_global.tr("### DrawWaterMark: _global.userLevelIndex = " + _global.userLevelIndex + " _global.Project.CurrentPaper.minPaperAccess = " + _global.Project.CurrentPaper.minPaperAccess);
		if (_global.userLevelIndex >= _global.Project.CurrentPaper.minPaperAccess) return;
		
		var watermark:MovieClip = mc.createEmptyMovieClip("Watermark", 50);
		
		var m:Number = 72;
		var pxToMmCoeff:Number = 0.35277777777777;
		var tfw:Number = _global.Watermark.Width / pxToMmCoeff;
		var tfh:Number = _global.Watermark.Height/ pxToMmCoeff;
		var w:Number = (tfw + tfh) / Math.sqrt(2) + m;
		var h:Number = w;
		var wl:Number = width  / w + 1;
		var hl:Number = height / h + 1;
		var tfx:Number = tfh / Math.sqrt(2) + m / 2;
		var tfy:Number = m / 2;
		
		for (var k:Number = 0; k < wl; ++ k) {
			for(var l:Number = 0; l < hl; ++ l) {
				var wm:MovieClip = watermark.createEmptyMovieClip("k"+k+"l"+l, watermark.getNextHighestDepth());
//				wm.lineStyle(2, 0xff0000, 100);
				wm.moveTo(0, 0);
				wm.lineTo(w, 0);
				wm.lineTo(w, h);
				wm.lineTo(0, h);
				wm.lineTo(0, 0);
				
				var tf:TextField = wm.createTextField("txtMark", 0, 0, 0, tfw, tfh);
				tf.embedFonts = true;
				tf.autoSize   = false;
				tf.selectable = false;
				tf._alpha     = 50;
				tf.multiline  = true;
				tf.html       = true;
				tf.wordWrap   = false;
//				tf.background = true;
//				tf.backgroundColor = 0x008000;

				tf._rotation = 45;
				tf._x = tfx;
				tf._y = tfy;
				
				wm._x = w * k;
				wm._y = h * l;
		
				if (WatermarkFirstDraw) {
					WatermarkFirstDraw = false;

					_global.WatermarkHelper.SetTextFieldXml(tf, _global.Watermark.XmlText);
					_global.Watermark.XmlText = tf.htmlText;

					var fontLoadedObject:Object = new Object();
					fontLoadedObject.Draw = function() {
						_global.UIController.PreviewAreaController.PrintAreaDataBind(false);
					};
					
					var formats:Array = new Array();
					for (var i:Number = 0; i < tf.text.length; ++ i) {
						formats.push(tf.getTextFormat(i));
					}
					for (var i:Number = 0; i < formats.length; ++ i) {
						_global.Fonts.TestFontFormat(formats[i], fontLoadedObject);
					}
				}
				else {
					tf.htmlText = _global.Watermark.XmlText;
				}
			}
		}
	}
	
	public static function DrawContours
		(mc:MovieClip, obj:Object, callBackFunction:Function,
		showFrameAroundSelectedFace:Boolean,
		selectCurrentFace:Boolean, faces:Array, 
		width:Number, height:Number,
		drawRealContour:Boolean, drawMarkLines:Boolean, drawCutLines:Boolean):Void {
		mc._xscale = mc._yscale = 100;
		Clear(mc);
		
		DrawBackground(mc, width, height, 0xffffff, 100);

		DrawFaceContour(mc, obj, callBackFunction,
				showFrameAroundSelectedFace, selectCurrentFace, faces, 
				drawRealContour, drawMarkLines, drawCutLines);
		DrawMask(mc, width, height);

		//draw border again
		//DrawBorder(mc, width, height, 5, 0xCCCCCC, 100);
	}
	
	public static function Calibrate(mc:MovieClip, x:Number, y:Number,
        faces:Array, calibrateContours:Boolean):Void {
		var count:Number = faces.length;
		for (var i:Number = 0; i < count; ++i) {
			var face = faces[i];
			var mcFace:MovieClip = mc["Face" + i];
			mcFace._x = face.PaperX + x;
			mcFace._y = face.PaperY + y;
			var mcFaceMask:MovieClip = mc["FaceMask" + i];
			mcFaceMask._x = face.PaperX + x;
			mcFaceMask._y = face.PaperY + y;
			//Nice feature -> available only for [NON] DirectToCD devices
			if (calibrateContours) {
				var mcFaceContour:MovieClip = mc["FaceContour" + i];
				mcFaceContour._x = face.PaperX + x;
				mcFaceContour._y = face.PaperY + y;
			}
		}
	}

	public static function ContourVisibility(mc:MovieClip,
        value:Boolean, faces:Array):Void {
		var count:Number = faces.length;
		var depth:Number = 2 * count;
		for (var i:Number = 0; i < faces.length; ++i) {
			var mcFaceContour:MovieClip = mc.getInstanceAtDepth(depth + i);
			mcFaceContour._visible = value;
		}
	}


	public static function DrawFaceAroundFrame(mc:MovieClip, face:FaceUnit):Void {
        var mcFrame:MovieClip = mc.createEmptyMovieClip("mcFrame", 100);		
        mcFrame._x = face.PaperX;
        mcFrame._y = face.PaperY;
        mcFrame.clear();
		
		var bounds:Object = face.GetBounds();

		var xMin:Number = bounds.xMin;
		var yMin:Number = bounds.yMin;
		var xMax:Number = bounds.xMax;
  		var yMax:Number = bounds.yMax;
		var frameColor:Number = 0xFF0000;
		var addColor:Number = 0x00FFFF;

		var dashedLineDrawer:DashedLineDrawer = new DashedLineDrawer(mcFrame,20,20);
		mcFrame.lineStyle(0, frameColor);		
 		dashedLineDrawer.moveTo(xMin,yMin);
 		dashedLineDrawer.lineTo(xMin,yMax);
 		dashedLineDrawer.lineTo(xMax,yMax);
 		dashedLineDrawer.lineTo(xMax,yMin);
		dashedLineDrawer.lineTo(xMin,yMin); 		
/*		mcFrame.moveTo(xMin, yMin);
		var count:Number = 0;
		var step:Number = 20;
		mcFrame.lineStyle(0, frameColor + addColor*(count%2));
		for(var i:Number = yMin; i < yMax; i += step, ++count) {
			mcFrame.lineTo(xMin, i);
			mcFrame.lineStyle(0, frameColor + addColor*(count%2));
		}
		for(var i:Number = xMin; i < xMax; i += step, ++count) {
			mcFrame.lineTo(i, yMax);
			mcFrame.lineStyle(0, frameColor + addColor*(count%2));
		}
		for(var i:Number = yMax; i > yMin; i -= step, ++count) {
			mcFrame.lineTo(xMax, i);
			mcFrame.lineStyle(0, frameColor + addColor*(count%2));
		}
		for(var i:Number = xMax; i > xMin; i -= step, ++count) {
			mcFrame.lineTo(i, yMin);
			mcFrame.lineStyle(0, frameColor + addColor*(count%2));
		}
		mcFrame.lineTo(xMin, yMin);
*/		
	}

	
	public static function HideCurrentFace(mc:MovieClip):Void {
		CurrentFaceVisibility(mc, false);
	}
	
	private static function CurrentFaceVisibility(mc:MovieClip, value:Boolean):Void {
		var count:Number = _global.Project.CurrentPaper.Faces.length;
		var index:Number = 0;
		for (var i:Number = 0; i < count; ++i) {
			var face = _global.Project.CurrentPaper.Faces[i];
			if(face == _global.Project.CurrentPaper.CurrentFace) {
				index = i;
				break;
			}
		}
		var mcFace:MovieClip = mc.getInstanceAtDepth(3 * index);
		var mcFaceMask:MovieClip = mc.getInstanceAtDepth(3 * index + 1);
		var mcFaceContour:MovieClip = mc.getInstanceAtDepth(3 * index + 2);
		mcFace._visible = value;
		mcFaceMask._visible = value;
		mcFaceContour._visible = value;
	}
	
	public static function CreatePaper() :Entity.Paper {
		return new Entity.Paper();
	}
}
