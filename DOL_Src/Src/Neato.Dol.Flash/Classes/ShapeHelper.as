﻿class ShapeHelper {
	private var shapes:Array;
	public function ShapeHelper(shapesXml:XML) {
		shapes = new Array();
		
		for (var itemNode:XMLNode = shapesXml.firstChild.firstChild; itemNode != null; itemNode = itemNode.nextSibling) {
			var shape = CreateShapeFromXml(itemNode);
			shapes.push(shape);
		}
	}
	
	public function CreateShapeFromXml(itemNode:XMLNode):Object {
		var shape = new Object(); //todo: replace with Shape object
		shape.name = itemNode.attributes.name; //.toLowerCase();
		shape.width=itemNode.attributes.width;
		shape.height=itemNode.attributes.height;
		shape.isNonScalable = itemNode.attributes.isNonScalable ? Boolean(true) : Boolean(false);
		shape.transparency = itemNode.attributes.transparency == "true" ? Boolean(true) : Boolean(false);
		shape.fillColor = parseInt(itemNode.attributes.fillColor.substr(1), 16);
		shape.fillColor2 = parseInt(itemNode.attributes.fillColor2.substr(1), 16);
		shape.gradientType = itemNode.attributes.gradientType;
		shape.borderColor = parseInt(itemNode.attributes.borderColor.substr(1), 16);
		shape.borderWidth = itemNode.attributes.borderWidth;
		var drawSequence = new Array();
		for (var i = 0; i < itemNode.childNodes.length; ++i) {
			var drawNode:XML = itemNode.childNodes[i];
			drawSequence.push(Logic.DrawingHelper.CreatePrimitive(drawNode));
		}
		shape.drawSequence = drawSequence;
		shape.originalXmlNode = itemNode;
		return shape;
	}
	
	public function GetShape(shapeType:String):Object {
		for (var i = 0; i < shapes.length; ++i) {
			if (shapes[i].name == shapeType) return shapes[i];
		}
		trace("ERROR: cannot find shape of type " + shapeType);
		return new Object();
	}

	public function GetShapesArray():Array {
		return shapes;
	}
}
