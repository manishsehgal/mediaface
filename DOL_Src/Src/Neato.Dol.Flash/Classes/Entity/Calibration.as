﻿class Entity.Calibration {
	private var x:Number;
	private var y:Number;
	private var xCopy:Number;
	private var yCopy:Number;
	private var xDirectToCD:Number;
	private var yDirectToCD:Number;
	
	private var isCalibrationNonDTCDSpecified:Boolean = false;
	private var isCalibrationDTCDSpecified:Boolean = false;
	
	public function Calibration () {
	}
	
	public function set X (value:Number):Void {
		x = value;
	}
	
	public function get X ():Number {
		return x;
	}
	
	public function set Y (value:Number):Void {
		y = value;
	}
	
	public function get Y ():Number {
		return y;
	}
	
	public function set XCopy (value:Number):Void {
		xCopy = value;
	}
	
	public function get XCopy ():Number {
		return xCopy;
	}
	
	public function set YCopy (value:Number):Void {
		yCopy = value;
	}
	
	public function get YCopy ():Number {
		return yCopy;
	}
	
	public function set XDirectToCD (value:Number):Void {
		xDirectToCD = value;
	}
	
	public function get XDirectToCD ():Number {
		return xDirectToCD;
	}
	
	public function set YDirectToCD (value:Number):Void {
		yDirectToCD = value;
	}
	
	public function get YDirectToCD ():Number {
		return yDirectToCD;
	}
	
	public function get XReal ():Number {
		return (_global.Project.CurrentPaper.IsDirectToCDDevice) ? xCopy : x;
	}
	
	public function set XReal (value:Number):Void {
		if (_global.Project.CurrentPaper.IsDirectToCDDevice)
			xCopy = value; else x = value;
	}
	
	public function get YReal ():Number {
		return (_global.Project.CurrentPaper.IsDirectToCDDevice) ? yCopy : y;
	}
	
	public function set YReal (value:Number):Void {
		if (_global.Project.CurrentPaper.IsDirectToCDDevice)
			yCopy = value; else y = value;
	}
	
	public function get XSave ():Number {
		return (_global.Project.CurrentPaper.IsDirectToCDDevice) ? xDirectToCD : x;
	}
	
	public function get YSave ():Number {
		return (_global.Project.CurrentPaper.IsDirectToCDDevice) ? yDirectToCD : y;
	}
	
	public function get IsCalibrationSpecified ():Boolean {
		var flag:Boolean = 
		((_global.Project.CurrentPaper.IsDirectToCDDevice && isCalibrationDTCDSpecified) ||
		(!_global.Project.CurrentPaper.IsDirectToCDDevice && isCalibrationNonDTCDSpecified));
		
		return (flag != undefined) ? flag : false;
	}
	
	public function set IsCalibrationNonDTCDSpecified (value:Boolean):Void {
		isCalibrationNonDTCDSpecified = value;
	}
	
	public function get IsCalibrationNonDTCDSpecified ():Boolean {
		return isCalibrationNonDTCDSpecified;
	}
	
	public function set IsCalibrationDTCDSpecified (value:Boolean):Void {
		isCalibrationDTCDSpecified = value;
	}
	
	public function get IsCalibrationDTCDSpecified ():Boolean {
		return isCalibrationDTCDSpecified;
	}
	
	public function Save ():Void {
		var vars:String = "x=" + XSave + "&y=" + YSave;
		var commandName:String = (_global.Project.CurrentPaper.IsDirectToCDDevice)
		? "calibrationDirectToCD": "calibration";
		//BrowserHelper.InvokePageScript (commandName, vars);
		
		SASettings.SaveCalibration(this, _global.Project.CurrentPaper.IsDirectToCDDevice);
	}
	
	//========= DirectToCd diameters handling ==========================
	
	private var diamType   : String = "B";
	private var diamInnerL : Number = -1;
	private var diamOuterL : Number = -1;
	private var diamInnerB : Number = -1;
	private var diamOuterB : Number = -1;
	
	public function set DiameterType(value:String):Void {
		diamType = value;
	}
	
	public function set DiameterInner(value:Number):Void {
		if (diamType == "L") {
			this.DiameterInnerL = value;
		} else {
			this.DiameterInnerB = value;
		}
	}
	public function get DiameterInner():Number {
		if (diamType == "L") {
			return this.DiameterInnerL;
		} else {
			return this.DiameterInnerB;
		}
	}
	public function get DiameterInnerMin():Number {
		if (diamType == "L") return 1;
		else return 1;
	}
	public function get DiameterInnerMax():Number {
		if (diamType == "L") return 39;
		else return 60;
	}
	
	public function set DiameterOuter(value:Number):Void {
		if (diamType == "L") {
			this.DiameterOuterL = value;
		} else {
			this.DiameterOuterB = value;
		}
	}
	public function get DiameterOuter():Number {
		if (diamType == "L") {
			return this.DiameterOuterL;
		} else {
			return this.DiameterOuterB;
		}
	}
	public function get DiameterOuterMin():Number {
		if (diamType == "L") return 40;
		else return 61;
	}
	public function get DiameterOuterMax():Number {
		if (diamType == "L") return 92;
		else return 140;
	}
	
	public function set DiameterInnerL (value:Number):Void {
		if      (value == NaN) diamInnerL = -1;
		else if (value == -1)  diamInnerL = -1;
		else if (value < 1)    diamInnerL = 1;
		else if (value > 39)   diamInnerL = 39;
		else                   diamInnerL = value;
	}
	public function get DiameterInnerL ():Number {
		return (diamInnerL > 0) ? diamInnerL : 13;
	}
	
	public function set DiameterOuterL (value:Number):Void {
		if      (value == NaN) diamOuterL = -1;
		else if (value == -1)  diamOuterL = -1;
		else if (value < 40)   diamOuterL = 40;
		else if (value > 92)   diamOuterL = 92;
		else                   diamOuterL = value;
	}
	public function get DiameterOuterL ():Number {
		return (diamOuterL > 0) ? diamOuterL : 80;
	}
	
	public function set DiameterInnerB (value:Number):Void {
		if      (value == NaN) diamInnerB = -1;
		else if (value == -1)  diamInnerB = -1;
		else if (value < 1)    diamInnerB = 1;
		else if (value > 60)   diamInnerB = 60;
		else                   diamInnerB = value;
	}
	public function get DiameterInnerB ():Number {
		return (diamInnerB > 0) ? diamInnerB : 18.5;
	}
	
	public function set DiameterOuterB (value:Number):Void {
		if      (value == NaN) diamOuterB = -1;
		else if (value == -1)  diamOuterB = -1;
		else if (value < 61)   diamOuterB = 61;
		else if (value > 140)  diamOuterB = 140;
		else                   diamOuterB = value;
	}
	public function get DiameterOuterB ():Number {
		return (diamOuterB > 0) ? diamOuterB : 120;
	}
	
	//========================================================
	
}

