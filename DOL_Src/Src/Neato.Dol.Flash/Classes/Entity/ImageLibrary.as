﻿class Entity.ImageLibrary {
	function ImageLibrary () {
	}
	
	public static function CreateTreeData (targetNode:XMLNode, sourceNode:XMLNode, tree:CtrlLib.TreeEx) {
		for (var node:XMLNode = sourceNode.firstChild; node != null; node = node.nextSibling) {
			if (node.nodeName == "folder") {
				var child:XMLNode = targetNode.addTreeNode (node.attributes["name"], node.attributes["id"]);
				tree.setIsBranch (child, true);
				CreateTreeData (child, node, tree);
			}
		}
	}
	
	public static function FindFolder (parentNode:XMLNode, id):XMLNode {
		for (var node:XMLNode = parentNode.firstChild; node != null; node = node.nextSibling) {
			if (node.nodeName == "folder" && node.attributes["id"] == id) {
				return node;
			}
			else {
				var child:XMLNode = FindFolder (node, id);
				if (child != undefined) {
					return child;
				}
			}
		}
	}
	
	public static function CreateImageListData (node:XMLNode, imageIconUrlFormat:String):Array {
		var itemData:Array = new Array ();
		for (var child:XMLNode = node.firstChild; child != null; child = child.nextSibling) {
			if (child.nodeName == "item") {
				itemData.push ({name:child.attributes["name"], id:child.attributes["id"], url:imageIconUrlFormat + child.attributes["id"]});
			}
		}
		return itemData;
	}
}
