﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;

[Event("onSaved")]
[Event("onLoaded")]
class Entity.Project {
	
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(Project.prototype);
	
	private var CurrentPaper:Entity.Paper;	
	private var projectXml:XML;	
	private var projectService:SAProject;
	
	function Project() {
		//_global.tr("Entity.Project ctr");
		CurrentPaper = new Entity.Paper();
		CurrentPaper.RegisterOnChangedHandler(this, OnChange);
		trace("CurrentPaper = "+CurrentPaper);
		
		projectXml = new XML();
		projectXml.ignoreWhite = true;
	}

	function GenerateProjectXml() : XML {
		return CurrentPaper.GenerateProjectXml(projectXml);
	}
	
	function Load() {
		projectService = SAProject.GetInstance(projectXml);
		projectService.RegisterOnLoadedHandler(this, onLoaded);
		projectService.BeginLoad();
	}
	
	function Save() {
		var xml:XML = CurrentPaper.GenerateProjectXml(projectXml);
		projectXml = xml;
		projectService = SAProject.GetInstance(projectXml);
		projectService.RegisterOnSavedHandler(this, onSaved);
		projectService.BeginSave();
	}
	
	public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function UnregisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.removeEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function RegisterOnSavedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onSaved", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function UnregisterOnSavedHandler(scopeObject:Object, callBackFunction:Function) {
		this.removeEventListener("onSaved", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function RegisterOnChangedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onChanged", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function UnregisterOnChangedHandler(scopeObject:Object, callBackFunction:Function) {
		this.removeEventListener("onChanged", Delegate.create(scopeObject, callBackFunction));
    }

	private function OnChange(eventObject:Object) : Void {
		eventObject.type = "onChanged";
		eventObject.target = this;
		dispatchEvent(eventObject);
	}
	
	function onLoaded(eventObject) {
		var mc:MovieClip = _root.pnlMainLayer.content.pnlDesignArea.content.mcFaceContainer;
		CurrentPaper.UnloadImages();
		CurrentPaper.ReadFaces(mc, projectXml);
		if(_global.Frames != undefined) {
		   for(var i in _global.Frames)
				_global.Frames[i].removeMovieClip();
		}
		_global.Frames=new Array(3);
		_global.Frames["Frame"] = mc.attachMovie("Frame", "frame", mc.getNextHighestDepth());
		_global.Frames["TableFrame"] = mc.attachMovie("TableFrame", "tableframe", mc.getNextHighestDepth());
		_global.Frames["FrameEx"] = mc.attachMovie("FrameEx", "frameex", mc.getNextHighestDepth());

		if (CurrentPaper.CurrentUnit == undefined) {
			_global.Frames["Frame"].DetachUnit();
			_global.Frames["TableFrame"].DetachUnit();
			_global.Frames["FrameEx"].DetachUnit();			
		} else {
			_global.Frames["Frame"]._visible = false;
			_global.Frames["TableFrame"]._visible = false;	
			_global.Frames["FrameEx"]._visible = false;	
		}
		_global.SelectionFrame = _global.Frames["Frame"];
	
		CurrentPaper.CurrentFace.Show();
		if(_global.Navigator != undefined)
			_global.Navigator.DataBind();
		
		var rootNode:XMLNode = projectXml.firstChild;
		var propertiesNode:XMLNode = Utils.XMLGetChild(rootNode, "Properties");
		
		_global.ModelName = propertiesNode.attributes.name; 
		_global.Paper = _root.pnlPreviewLayer.content.paper;

		var eventObject = {type:"onLoaded", target:this};
		dispatchEvent(eventObject);
		BrowserHelper.InvokePageScript("RefreshHeader", "");
	}
	
	function onSaved(eventObject) {
		var eventObject = {type:"onSaved", target:this};
		dispatchEvent(eventObject);
	}
	function get CurrentUnit():Unit {
		return CurrentPaper.CurrentUnit;
	}
	function set CurrentUnit(value:Unit):Void {
		CurrentPaper.CurrentUnit = value;
	}

	private var extension:String;
	public function get Extension():String {
		return extension;
	}
	public function set Extension(value:String):Void {
		extension = value;
	}
}