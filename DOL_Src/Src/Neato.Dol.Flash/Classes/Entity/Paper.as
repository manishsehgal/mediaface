﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;

class Entity.Paper {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(Paper.prototype);
	
	public var Faces:Array;
	private var currentFace:FaceUnit;
	public var CurrentUnit:Unit;
	public var Calibration:Entity.Calibration;
	private var width:Number;
	private var height:Number;
	private var paperType:String;
	private var paperName:String;
	private var paperBrand:String;
	private var deviceType:String;
	private var paperId:Number = 0;
	private var minPaperAccess:Number = 0;
	private var orientation:String;
	
	public function get CurrentFace() : FaceUnit {
		return currentFace;
	}
	public function set CurrentFace(value:FaceUnit) {
		currentFace = value;
		currentFace.ActManager.OnChange();
	}
	
	function Paper() {
		trace("Paper ctr");
		Faces = new Array();
		width = 100;
		height = 100;
		orientation = "P";
		Calibration = new Entity.Calibration();
	}
	
	public function get Width():Number {
		return width;
	}

	public function get Height():Number {
		return height;
	}
	
	public function get PaperType():String {
		return paperType;
	}
	
	public function set PaperType(type:String):Void {
		paperType = type;
		var isUniversal = (type.toLowerCase() == "universal");
		for(var i:Number=0; i<Faces.length; i++) {
			var face:FaceUnit = Faces[i];
			face.IsUniversal = isUniversal;
		}	
	}	
	
	public function get PaperName():String {
		return paperName;
	}
	
	public function get Orientation():String {
		return orientation;
	}
	
	public function UnloadImages():Void {
		_global.UIController.ImageStorage.UnloadProjectImages();
	}
	
	public function ReadFaces(mc:MovieClip, projectXml:XML):Void {	
		for (var i:Number = 0; i < Faces.length; i++) {
			var inst:MovieClip = Faces[i]["mc"];
			if (inst != undefined) {
				inst.removeMovieClip();
			}
		}
		delete(Faces);
		Faces = new Array();
		var projXml:XMLNode = projectXml.firstChild;
		var facesXml:XMLNode = Utils.XMLGetChild(projXml,"Faces");
		var paperXml:XMLNode = Utils.XMLGetChild(projXml,"Paper");		
		var propertiesNode:XMLNode = Utils.XMLGetChild(projXml,"Properties");
		var currentFaceId:String = propertiesNode.attributes.currentFaceId;
		paperId = Number(projXml.attributes.paperDbId);
		minPaperAccess = Number(projXml.attributes.minPaperAccess);
		trace("Faces creation");
		trace("paperId = "+paperId);
		for (var i:Number = 0; i < facesXml.childNodes.length; ++i) {
			var mcFace:MovieClip = mc.createEmptyMovieClip("Face" + i, mc.getNextHighestDepth());
			var face:FaceUnit = new FaceUnit(mcFace, facesXml.childNodes[i]);
			face.ActManager.RegisterOnChangeHandler(this, OnChange);
			trace("face mc="+mcFace);
			Faces.push(face);
			if (face.Id == currentFaceId)
				CurrentFace = face;
		}
		
		trace("Faces="+Faces);
		if (currentFaceId == undefined)
			CurrentFace = Faces[0];
		
		if (paperXml != undefined) {
			width = Number(paperXml.attributes.w);
			height = Number(paperXml.attributes.h);
			PaperType = String(paperXml.attributes.PaperType);
			paperBrand = paperXml.attributes.PaperBrand;
			orientation = paperXml.attributes.Orientation;
		}
		
		deviceType = propertiesNode.attributes.DeviceType;
		paperName  = propertiesNode.attributes.name;
		
		
		var mu:MoveableUnit = CurrentFace.GetCurrentUnit();
		if (mu != null) {
			CurrentFace.ChangeCurrentUnit(mu);
			CurrentFace.UpdateSelection(mu);
		}
	}
	
	public function GenerateProjectXml(projectXml:XML):XML {
		//Create a copy of current XML file
		var i:Number;
		var j:Number;

		var xml:XML = new XML();
		xml.appendChild(projectXml.firstChild.cloneNode(true));

		var projXml:XMLNode = xml.firstChild;

		//remove CalibrationNode
		for (i=0; i<projXml.childNodes.length; i++) {
			var node:XMLNode = projXml.childNodes[i];
			if (node.nodeName == "Calibration")
				node.removeNode();
		}
		
		var calibrationNode:XMLNode = new XMLNode(1, "Calibration");
		calibrationNode.attributes.x = Calibration.XReal == undefined || Calibration.XReal == null || isNaN(Calibration.XReal) ? 0 : Calibration.XReal;
		calibrationNode.attributes.y = Calibration.YReal == undefined || Calibration.YReal == null || isNaN(Calibration.YReal) ? 0 : Calibration.YReal;
		projXml.appendChild(calibrationNode);		
		
		//Delete from XML all nodes from FACES, except CONTOURS, and replace them by nodes for current paper
		var facesXml:XMLNode = projXml.firstChild;
		for (i=0; i<facesXml.childNodes.length; i++) {
			var faceNode:XMLNode = facesXml.childNodes[i];
			for(j=faceNode.childNodes.length-1; j>=0; j--) {
				var faceSubNode:XMLNode = faceNode.childNodes[j];
				if (!(faceSubNode.nodeName == "Contour" || faceSubNode.nodeName == "Localization"))
					faceSubNode.removeNode();
			}
			//Add others units (texts, circles, rectangles, etc.) into XML document
			Faces[i].AddUnitNodes(faceNode, true);
		}
	
		var propertiesNode:XMLNode = Utils.XMLGetChild(projXml,"Properties");
		propertiesNode.attributes.currentFaceId = CurrentFace.Id;
	
		return xml;
	}
	
	public function get PaperId() {
		return paperId;
	}
	public function get IsLightscribeDevice():Boolean {
		return deviceType == "Lightscribe";
	}
	
	public function get IsDirectToCDDevice():Boolean {
		return deviceType == "DirectToCD";
	}
	
	function UnloadImage(imageId:String, imageUrl:String, forceUnload:Boolean) : Void {
		forceUnload = forceUnload == true;
//		_global.tr("### UnloadImage imageId = " + imageId + " imageUrl = " + imageUrl + " forceUnload = " + forceUnload);
		if (imageId != null && imageUrl != null && imageId != undefined && imageUrl != undefined) {
			var count:Number = 0;
			for (var i:Number = 0; i < Faces.length; ++ i) {
				var units = Faces[i].units;
				for(var j:Number = 0; j < units.length; ++ j) {
					var unit = Faces[i].units[j];
					if (unit instanceof ImageUnit) {
						if (unit.id == imageId) {
							++ count;
						}
					}
					else if (unit instanceof ShapeUnit) {
						if (unit.FillImageId == imageId) {
							++ count;
						}
					} 
				}
			}
//			_global.tr("### UnloadImage count = " + count);
			if (count == 0 || forceUnload) {
				_global.UIController.ImageStorage.UnloadImage(imageUrl);
				var imageLibariryService:SAImages = SAImages.GetInstance(new XML());
				imageLibariryService.BeginDeleteImage(imageId);
			}
		} 
	}

	public function RegisterOnChangedHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("onChanged", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function UnregisterOnChangedHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.removeEventListener("onChanged", Delegate.create(scopeObject, callBackFunction));
    }
	
	private function OnChange(eventObject:Object) : Void {
		eventObject.type = "onChanged";
		eventObject.target = this;
		dispatchEvent(eventObject);
	}
}