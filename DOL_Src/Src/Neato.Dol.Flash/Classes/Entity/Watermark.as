
class Entity.Watermark {
	private var width:Number = 0;
	private var height:Number = 0;
	private var xmlText:String = "";
	
	function Watermark() {
	}
	
	public static function GetInstance() : Watermark {
		return new Watermark();
	}
	
	public function get Width() : Number {
		return width;
	}
	
	public function get Height() : Number {
		return height;
	}
	
	public function set Width(value:Number) : Void {
		width = value;
	}
	
	public function set Height(value:Number) : Void {
		height = value;
	}
	
	public function get XmlText() : String {
		return xmlText;
	}
	
	public function set XmlText(value:String) : Void {
		xmlText = value;
	}
}