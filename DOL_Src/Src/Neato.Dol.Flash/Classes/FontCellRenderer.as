﻿import mx.core.UIComponent;
//import mx.controls.Loader;
import mx.controls.Label;

class FontCellRenderer extends UIComponent
{
	static public var symbolName:String = "FontCellRenderer";
	static public var symbolOwner:Object = FontCellRenderer;
	public var className:String = "FontCellRenderer";
	

    private var multiLineLabel; // The label to be used for text.
    private var owner; // The row that contains this cell.
    private var listOwner; // The List, data grid or tree containing this cell.

    // Cell height offset from the row height total and preferred cell width.
    private static var PREFERRED_HEIGHT_OFFSET = 0; 
    private static var PREFERRED_WIDTH = 140;

//	private var image_ldr:Loader;
	private var label_lbl;//:Label;
	
	
	public var getDataLabel:Function;
	

    public function FontCellRenderer()
    { }

    public function createChildren():Void
    {
		label_lbl = createObject("Label", "label_lbl", 1, {styleName:this, owner:this});
		label_lbl.text = " ";
    }

    public function doLayout():Void
	{
		label_lbl.move(0, 0);
		var w:Number = width - label_lbl.x;
		label_lbl.setSize(w, 22);
	}

	public function getPreferredHeight():Number
    {
		return owner.__height - PREFERRED_HEIGHT_OFFSET;
    }

    public function setValue(suggestedValue:String, item:Object, selected:Boolean):Void
    {
		
		if(item == undefined)
		{
            _visible = false;
			return;
        }
		var lbl:String = getDataLabel();
		var fontName:String = item[lbl];
		
		var labelField:TextField = label_lbl.labelField;	

		var textFormat:TextFormat=labelField.getTextFormat();		
		
		labelField.embedFonts=true;//_global.Fonts.TestLoadedSmallFont();
		labelField.antiAliasType = "advanced";
		textFormat.color = (selected == "selected" || selected == "highlighted") ? 0 : 0;
 
		label_lbl.text = fontName; 
		textFormat.size=14;
		textFormat.font = fontName;
		if(fontName == "Symbol") { 
			labelField.text = fontName+"        "+"\"$'^åÕ";
			labelField.setTextFormat(textFormat);
			textFormat.font = "Arial";			
			labelField.setTextFormat(0,fontName.length+8,textFormat);
		} else if (fontName.indexOf("dings",3) > 0) {	// all Windings, Webdings
			labelField.text = fontName+"    "+"!èx8m2";
			labelField.setTextFormat(textFormat);
			textFormat.font = "Arial";			
			labelField.setTextFormat(0,fontName.length+4,textFormat);		  
		} else if (fontName == "Monotype Corsiva") {
			textFormat.size=20;
			labelField.setTextFormat(textFormat);
		} else 
			labelField.setTextFormat(textFormat);
		
		doLayout();
		_visible = true;
	
    }
    // function getPreferredWidth :: only for menus and DataGrid headers
    // function getCellIndex :: not used in this cell renderer

}

