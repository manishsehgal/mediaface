﻿class TextEffectHelper {
	
	private var effects:Array;
	
	public function TextEffectHelper(effectsXml:XML) {
		effects = new Array();
		for (var itemNode:XMLNode = effectsXml.firstChild.firstChild; itemNode != null; itemNode = itemNode.nextSibling) {
			var effect:Object = CreateTextEffectFromXml(itemNode);
			effects.push(effect);
		}
	}
	
	public function CreateTextEffectFromXml(itemNode:XMLNode):Object {
		
		//_global.tr("BSM - EffectHelper - GetXML - "+itemNode);
		
		var effect:Object = new Object();		
		effect.name = itemNode.attributes.name;
		effect.type = itemNode.attributes.type;
		effect.subtype = itemNode.attributes.subtype;
		effect.refPoints = new Array();
		effect.drawSequence = new Array();
		for (var effNode:XMLNode = itemNode.firstChild; effNode != null; effNode = effNode.nextSibling) {
			if(effNode.nodeName == "Geometry") {
				for(var i:Number = 0; i < effNode.childNodes.length; i++) {
					var nodeGeometry:XMLNode = effNode.childNodes[i];
					var refPoint:Object = new Object({id:Number,x:Number,y:Number,checked:Boolean});
					refPoint.id = parseInt(nodeGeometry.attributes.id);
					refPoint.x = parseFloat(nodeGeometry.attributes.x);
					refPoint.y = parseFloat(nodeGeometry.attributes.y);
					refPoint.checked = nodeGeometry.attributes.checked == "true" ? true : false;
					effect.refPoints.push(refPoint);
				}
			}
			else if(effNode.nodeName == "Settings") {
				for(var settNode:XMLNode = effNode.firstChild; settNode != null; settNode = settNode.nextSibling) {
					if(settNode.nodeName == "Font") {
						effect.fontSize = parseInt(settNode.attributes.size);
						effect.fontAlign = settNode.attributes.align;
						effect.fontScaled = settNode.attributes.scaled == "true" ? true : false;
					} else if(settNode.nodeName == "Angle") {
						effect.Angle = parseFloat(settNode.attributes.value);
					}
				}
			}
			else if(effNode.nodeName == "Icon") {
				for(var iconNode:XMLNode = effNode.firstChild; iconNode != null; iconNode = iconNode.nextSibling) {
					effect.drawSequence.push(Logic.DrawingHelper.CreatePrimitive(iconNode));
				}
			}
			
		}
		effect.EncodeGuideLines = this.EncodeGuideLines;
		return effect;
	}
	
	public function GetEffect(effectName:String):Object {
		
		for (var i:Number = 0; i < effects.length; ++i) {
			if (effects[i].name == effectName) return Clone(effects[i]);
		}
		trace("ERROR: cannot find effect of type " + effectName);
		return new Object();
	}

	public function Clone(eff:Object):Object {
		var res:Object = new Object();
		
		
		res.name = new String(eff.name); 
		res.type = new String(eff.type);
		res.subtype = new String(eff.subtype);
		res.refPoints = new Array();
		for(var i:Number = 0; i < eff.refPoints.length; i++) {
					
					var refPoint:Object = new Object({id:Number,x:Number,y:Number,checked:Boolean});
					refPoint.id = new Number(eff.refPoints[i].id);
					refPoint.x = new Number(eff.refPoints[i].x);
					refPoint.y = new Number(eff.refPoints[i].y);
					refPoint.checked = new Boolean(eff.refPoints[i].checked);
					res.refPoints.push(refPoint);
				
		}
		
		res.fontSize = new Number(eff.fontSize) ;
		res.fontAlign = new String(eff.fontAlign);
		res.fontScaled = new Boolean(eff.fontScaled);
		res.Angle = new Number(eff.Angle);
		res.EncodeGuideLines = this.EncodeGuideLines;
		return res;
	}
	public function GetEffectsArray():Array {
		return effects;
	}
	
	public function GetEffectAt(index:Number) :Object {
		return effects[index];
	}
	
	public function GetXML(eff :Object):XMLNode {
		var res:XMLNode = new XMLNode(1,"TextEffect");
		res.attributes.name = new String(eff.name); 
		res.attributes.type = new String(eff.type);
		res.attributes.subtype = new String(eff.subtype);
		var Geometry:XMLNode = new XMLNode(1,"Geometry");
		var refPoints:XMLNode = new XMLNode(1,"RefPoints");
		for(var i:Number = 0; i < eff.refPoints.length; i++) {
					
					var refPoint:XMLNode = new XMLNode(1,"RefPoint");
					refPoint.attributes.id = new Number(eff.refPoints[i].id);
					refPoint.attributes.x = new Number(eff.refPoints[i].x);
					refPoint.attributes.y = new Number(eff.refPoints[i].y);
					refPoint.attributes.checked = new Boolean(eff.refPoints[i].checked);
					Geometry.appendChild(refPoint);
				
		}
		//Geometry.appendChild(refPoints);
		res.appendChild(Geometry);
		var Settings:XMLNode = new XMLNode(1,"Settings");
		var Angle:XMLNode = new XMLNode(1,"Angle");
		var Font:XMLNode = new XMLNode(1,"Font");
		Font.attributes.fontSize = new Number(eff.fontSize) ;
		Font.attributes.fontAlign = new String(eff.fontAlign);
		Font.attributes.fontScaled = new Boolean(eff.fontScaled);
		Angle.attributes.value = new Number(eff.Angle);
		Settings.appendChild(Angle);
		Settings.appendChild(Font);
		res.appendChild(Settings);
		
		
		
		return res;
	}
	
	function EncodeGuideLines(eff:Object):String {
		var res:String = "";
		var p :Array = eff.refPoints;
		if(eff.type == "Curved" && eff.subtype == "Balloon") {	
			res="A:"+
			p[0].x+":"+p[0].y+":"+
			p[1].x+":"+p[1].y+":"+
			p[2].x+":"+p[2].y+":"+
			"A"+":"+
			p[3].x+":"+p[3].y+":"+
			p[4].x+":"+p[4].y+":"+
			p[5].x+":"+p[5].y+":"+
			"E";
			return res;
		} else if(eff.type == "Curved" && eff.subtype == "BottomArc") {
			
			res="L:"+
			p[0].x+":"+p[0].y+":"+
			p[1].x+":"+p[1].y+":"+
			"A"+":"+
			p[2].x+":"+p[2].y+":"+
			p[3].x+":"+p[3].y+":"+
			p[4].x+":"+p[4].y+":"+
			"E";
			return res;
		} else if(eff.type == "Curved" && eff.subtype == "TopArc") {
			res="A:"+
			p[0].x+":"+p[0].y+":"+
			p[1].x+":"+p[1].y+":"+
			p[2].x+":"+p[2].y+":"+
			"L"+":"+
			p[3].x+":"+p[3].y+":"+
			p[4].x+":"+p[4].y+":"+
			"E";
			return res;
		} else if(eff.type == "Curved" && eff.subtype == "Perspective") {
			res="L:"+
			p[0].x+":"+p[0].y+":"+
			p[1].x+":"+p[1].y+":"+
			"L"+":"+
			p[2].x+":"+p[2].y+":"+
			p[3].x+":"+p[3].y+":"+
			"E";
			return res;
		} else if(eff.type == "Curved" && eff.subtype == "DoubleArch") {
			res="B:"+
			p[0].x+":"+p[0].y+":"+
			p[1].x+":"+p[1].y+":"+
			p[2].x+":"+p[2].y+":"+
			p[3].x+":"+p[3].y+":"+
			"B:"+
			p[4].x+":"+p[4].y+":"+
			p[5].x+":"+p[5].y+":"+
			p[6].x+":"+p[6].y+":"+
			p[7].x+":"+p[7].y+":"+
			"E";
			return res;
		} else if(eff.type == "Guided" && eff.subtype == "Curve4") {
			res="B:"+
			p[0].x+":"+p[0].y+":"+
			p[1].x+":"+p[1].y+":"+
			p[2].x+":"+p[2].y+":"+
			p[3].x+":"+p[3].y+":"+
			"E";
			return res;
		} else if(	(eff.type == "Guided" && (eff.subtype == "LineAngleDown" || eff.subtype == "LineAngleUp"))
					|| (eff.type == "GuidedFit" && eff.subtype == "fit" )) {
			res="L:"+
			p[0].x+":"+p[0].y+":"+
			p[1].x+":"+p[1].y+":"+
			"E";
			return res;
		}
		else if(eff.type == "Button" || eff.type == "Circle"	) {
			var w:Number = p[2].x-p[0].x;
			var h:Number = eff.subtype=="CircleDown" ? -1 * (p[3].y-p[1].y) : p[3].y-p[1].y;
			res=p[0].x+":"+p[1].y+":"+w+":"+h+":E";
			
			return res;
		} else if(eff.type == "GuidedFit" && eff.subtype == "CurveArc" ) {
			res="A:"+
			p[0].x+":"+p[0].y+":"+
			p[1].x+":"+p[1].y+":"+
			p[2].x+":"+p[2].y+":"+
			"E";
			return res;
		} else if(eff.subtype == "Circular") {
			
			res="L:"+
			0.0+":"+0.0+":"+
			p[0].x+":"+p[0].y+":"+
			"E";
			return res;
		}
		return "E";
	}
}
