﻿import mx.utils.Delegate;
import Logic.*;
import CtrlLib.ButtonEx;
[Event("add")]
class TextEffectControl extends mx.core.UIComponent {
	static var symbolName:String = "TextEffectControl";
	static var symbolOwner:Object = TextEffectControl;
	var className:String = "TextEffectControl";

	private var boundingBox_mc:MovieClip;
	private var ctlChooseEffect;
	private var lblTextEffects;
	private var lblError;
	private var btnEditText/*:MenuButton*/;
	private var btnDelete:ButtonEx;
	

	private var xGap:Number = 4;
	private var yGap:Number = 4;

	function TextEffectControl() {
		super();
	}

	function init():Void {
		super.init();

		if (boundingBox_mc != undefined) {
			boundingBox_mc._width = 0;
			boundingBox_mc._height = 0;
			boundingBox_mc._visible = false;
		}
	}

	function createChildren():Void {
		super.createChildren();
		if (ctlChooseEffect == undefined) {
			ctlChooseEffect = this.createObject("HList", "ctlChooseEffect", this.getNextHighestDepth());
		}
		/*if (btnCancel == undefined) {
			btnCancel = this.createObject("MenuButton", "btnCancel", this.getNextHighestDepth());
		}*/
	}

	private function onLoad() {
		lblTextEffects.setStyle("styleName", "ToolPropertiesCaption");
		lblError.setStyle("styleName", "ToolPropertiesNote");
		//lblAddShapeHintText.setStyle("styleName", "TransferPropertiesText");
		
		ctlChooseEffect.RegisterOnChangeHandler(this, ctlChooseEffect_OnChange);

		this.btnDelete.setStyle("styleName", "ToolPropertiesActiveButton");
		btnDelete.RegisterOnClickHandler(this, _global.DeleteCurrentUnit);
		
		this.btnEditText.setStyle("styleName", "ToolPropertiesActiveButton");
		btnEditText.RegisterOnClickHandler(this,	btnEditText_OnClick);

		DataBind();
		InitLocale();
	}

    private function InitLocale() {
		_global.LocalHelper.LocalizeInstance(lblTextEffects, "ToolProperties", "IDS_LBLTEXTEFFECTS");
		_global.LocalHelper.LocalizeInstance(btnEditText, "ToolProperties", "IDS_BTNEDITTEXT");
		//_global.LocalHelper.LocalizeInstance(btnDelete, "ToolProperties", "IDS_BTNDELETE");
		
	//	TooltipHelper.SetTooltip(btnCancel, "ToolProperties", "IDS_TOOLTIP_CANCEL");
		TooltipHelper.SetTooltip(btnDelete, "ToolProperties", "IDS_TOOLTIP_DELETE");
    }

	public var DataSource:Array;

	public function DataBind() {
		trace("TextEffects DataBind");
		if (SAPlugins.IsAvailable() == false){
			_global.LocalHelper.LocalizeInstance(lblError, "ToolProperties", "IDS_LBLNOPLUGIN");
			
			ctlChooseEffect.DataSource = DataSource.slice(0,1);
		}
		else
		{
			lblError.text = "";
			ctlChooseEffect.DataSource = DataSource;
		}
		ctlChooseEffect.ItemLinkageName = "DrawingHolder";
		ctlChooseEffect.DataBind();
	}
	//region Actions

	private function ctlChooseEffect_OnChange(eventObject) {
		if(eventObject.index == undefined)
			return;
		var effname = DataSource[eventObject.index].name;
		trace("ctlChooseEffect_OnChange +"+effname);
		if(eventObject.index == 0){
			TextLogic.ConvertEffectToText(_global.CurrentUnit);
		}
		else
			TextLogic.ConvertTextToEffect(_global.CurrentUnit, effname);
		
	}
	//endregion

	//region add Shape event
	private function OnApply(index) {
		var effectName = DataSource[index].name;
		var eventObject = {type:"apply", target:this, effectName:effectName};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnApplyHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("apply", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
	
	private function btnEditText_OnClick(eventObject) {
		OnEditText();
	}
	
	
	//region Exit event
	private function OnEditText() {
		var eventObject = {type:"edittext", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnEditTextHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("edittext", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
}
