﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;

[Event("onLoaded")]
class SAWatermark {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SAWatermark.prototype);

	private var watermarkXml:XML;
	private var saveResultXml:XML;
	
	public static function GetInstance(xml:XML):SAWatermark {
		return new SAWatermark(xml);
	}
	
	public function get IsLoaded():Boolean {
		return watermarkXml.loaded;
	}
	
	private function SAWatermark(xml:XML) {
		var parent = this;
		
		this.watermarkXml = xml;
		this.saveResultXml = new XML();
		this.watermarkXml.onLoad = function(success:Boolean) {
			parent.OnWatermarkLoaded();
		};
		
		this.saveResultXml.onLoad = function(success:Boolean) {
			parent.OnWatermarkSaved();
		};
	}

	public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }
    
	public function RegisterOnSavedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onSaved", Delegate.create(scopeObject, callBackFunction));
    }

    private function OnWatermarkLoaded() : Void {
		var eventObject:Object = {type:"onLoaded", target:this, xml:watermarkXml};
		this.dispatchEvent(eventObject);
    }

    private function OnWatermarkSaved() : Void {
		var eventObject:Object = {type:"onSaved", target:this};
		this.dispatchEvent(eventObject);
    }

	public function BeginLoad(loadFonts:Boolean) : Void {
		var currentDate:Date = new Date();
		var url:String = "watermark.aspx?time=" + currentDate.getTime();
		if (loadFonts)
			url += "&LoadFonts=True";
		watermarkXml.load(url);
    }
    
    public function BeginSave(xml:XML) : Void {
		var currentDate:Date = new Date();
		var url:String = "watermark.aspx?time=" + currentDate.getTime();
		xml.contentType = "text/xml";
		xml.sendAndLoad(url, saveResultXml);
    }
}