﻿class Frames.FrameBorder extends MovieClip {

	function Draw(mu/*:MoveableUnit*/):Void {
		//var bounds = mu.mc.getBounds(mu.mc);		
		var bounds:Object = mu.GetBounds();
        
        //var borderWidthDelta = (mu instanceof ShapeUnit) ? ((mu.GetBorderWidth()) / 2) : 0;
		var borderWidthDelta = 0;

		var kx = _parent.ScaleX/100;
		var ky = _parent.ScaleY/100;

		var xMin = (bounds.xMin - borderWidthDelta) * kx * 100 / this._xscale;
		var yMin = (bounds.yMin - borderWidthDelta) * ky * 100 / this._yscale;
		var xMax = (bounds.xMax + borderWidthDelta) * kx * 100 / this._xscale;
  		var yMax = (bounds.yMax + borderWidthDelta) * ky * 100 / this._yscale;
		
		this.clear();
			
		this.moveTo(xMin, yMin);
		var count:Number = 0;
		var step:Number = 3;
		lineStyle(1, 0xFFFFFF*(count%2));
		for(var i = yMin; i < yMax; i += step, ++count) {
			this.lineTo(xMin, i);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		for(var i = xMin; i < xMax; i += step, ++count) {
			this.lineTo(i, yMax);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		for(var i = yMax; i > yMin; i -= step, ++count) {
			this.lineTo(xMax, i);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		for(var i = xMax; i > xMin; i -= step, ++count) {
			this.lineTo(i, yMin);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		this.lineTo(xMin, yMin);
	}
}
