﻿class Frames.TableFrame extends Frames.Frame {

	private var rowResizeBottomLeft:MovieClip;
	private var rowResizeTopRight:MovieClip;
	private var frameBorder:Frames.TableFrameBorder;
	private var colResizeHandler:Frames.ColumnResizeHandler;
	private var columnHandlers:Array;
	private var handscale:Number = 100;
	function TableFrame() {
		columnHandlers = new Array();
		columnHandlers[0] = colResizeHandler;
		rowResizeBottomLeft.pos = "bottomleft";
		rowResizeTopRight.pos = "topright";
	}
	public function AttachUnit(mu:MoveableUnit) {
		super.AttachUnit(mu);
		//trace("FrameBorder: "+frameBorder);
		//_global.tr("AttachUnit - TableFrame");
		var fbUpperLeftX:Number = frameBorder.getBounds(this).xMin;
		var fbUpperLeftY:Number = frameBorder.getBounds(this).yMin;
		var fbCenter:Number = (frameBorder.getBounds(this).yMax - frameBorder.getBounds(this).yMin) / 2 ;//+ fbUpperLeftY;
		rowResizeBottomLeft._x = fbUpperLeftX;
		rowResizeBottomLeft._y = fbUpperLeftY + frameBorder._height;
		rowResizeTopRight._x = fbUpperLeftX + frameBorder._width;
		rowResizeTopRight._y = fbUpperLeftY ;
		
		var widthes:Array = _global.Project.CurrentUnit.GetColumnsWidth();
		var len:Number = widthes.length;
		var currWidth:Number = 0;
		columnHandlersCountUpdate(len);
		for(var j = 0; j < widthes.length;j++)
		{
			currWidth += widthes[j];
			columnHandlers[j].colNum = j;
			columnHandlers[j]._visible = true;
			columnHandlers[j]._x = fbUpperLeftX + currWidth;
			columnHandlers[j]._y = fbCenter;
		}
		
	}
	public function DetachUnit() {
		trace("TableFrame:detach");
		if(_global.Project.CurrentUnit.GetAllText().length <1 && _global.Project.CurrentUnit != null /*&& _global.Mode != _global.PlaylistEditMode*/)
		{
			_global.DeleteCurrentUnit();
			_global.Mode = _global.InactiveMode;
		}
		
		super.DetachUnit();
		_visible = false;
		unit = undefined;
		for(var j = 0; j < columnHandlers.length;j++)
		{
			columnHandlers[j]._visible = false;
		}
		
	}
	
	public function set HandlersScale(value:Number):Void {
		rowResizeBottomLeft._xscale = rowResizeBottomLeft._yscale = 
		rowResizeTopRight._xscale = rowResizeTopRight._yscale = 
		rotateHandler._xscale = rotateHandler._yscale = 
		resizeHandler._xscale = resizeHandler._yscale = 
		rotateLabel._xscale = rotateLabel._yscale =
		resizeLabel._xscale = resizeLabel._yscale =
		frameBorder._xscale = frameBorder._yscale = 
		handscale = value;
		// SB - Scaling Fix
		RedrawUnit();
		var widthes:Array = _global.Project.CurrentUnit.GetColumnsWidth();
		var len:Number = widthes.length;		
		for(var i = 0; i < len; i++) {
			columnHandlers[i]._xscale = handscale;
			columnHandlers[i]._yscale = handscale;		
		}
	}
	private function columnHandlersCountUpdate(num:Number) {
		var len:Number = columnHandlers.length;
		for(var i = 0; i < len; i++) {
			columnHandlers[i].visible = false;
			columnHandlers[i]._xscale = handscale;
			columnHandlers[i]._yscale = handscale;
		}
		if(num <= len)
			return;
		else {
			
			for( var i:Number = 0; i < num - len; i++) {
				var id:Number = len + i;
				var res:MovieClip = this.attachMovie("ColumnResizeHandler","colResizeHandler"+id, this.getNextHighestDepth());
				res._xscale = handscale;
				res._yscale = handscale;
				columnHandlers.push(res);
			}
		}
	}

}