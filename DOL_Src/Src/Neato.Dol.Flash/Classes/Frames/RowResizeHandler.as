﻿import mx.utils.Delegate;
class Frames.RowResizeHandler extends mx.core.UIObject {
	private var xMouseBegin:Number = 0;
	private var yMouseBegin:Number = 0;
	public var pos:String;
	private var oldHeight:Number = null;
	function RowResizeHandler() {
		this.onPress = drag;
		this.onMouseMove = null;
		this.onMouseUp = mouseUp;
		this.onRollOver = rollOver;
		this.onRollOut = rollOut;
		gotoAndStop(1);
	}
	
	function drag() {
		trace("resize drag");
		this.onMouseMove = mouseMove;
		xMouseBegin = _xmouse;
		yMouseBegin = _ymouse;
		gotoAndStop(3);
		oldHeight = _global.Project.CurrentUnit.GetUnitHeight();
	}
	
	function noDrag() {
		trace("resize noDrag");
		this.onMouseMove = null;
		gotoAndStop(1);
	}
	
	function mouseMove() {
		_global.Project.CurrentUnit.SetLeading(pos,_ymouse - yMouseBegin);
		//		var keepProportionOnly:Boolean = false;
		//		_global.CurrentUnit.Resize(_xmouse - xMouseBegin, _ymouse - yMouseBegin, keepProportionOnly);
//		_global.SelectionFrame.AttachUnit(_global.Project.CurrentUnit);
		_global.UIController.FramesController.attach(_global.Project.CurrentUnit.frameLinkageName, false);

	}
	
	function rollOver() {
		gotoAndStop(2);
	}
	
	function rollOut() {
		gotoAndStop(1);
	}
		
	function mouseUp() {
		if (onMouseMove != null) {
			noDrag();
			
			var unit = _global.Project.CurrentUnit;
			var newHeight = _global.Project.CurrentUnit.GetUnitHeight();
			
			var action:Actions.TableHeightResizeAction = new Actions.TableHeightResizeAction(unit, pos, oldHeight, newHeight);
			_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
			oldHeight = null;
		}
	}
}