﻿class Frames.FrameEx extends Frames.Frame {
	
	public var frameBorder:Frames.FrameBorderEx;
	private var checkPointHandlers:Array;
	private var effect:Object;
	private var checkHandler0:Frames.CheckPointHandler;
	private var coordFreese:Object;
	private var handScale:Number;
	private var tempObj:Object;
	function FrameEx() {
		super();
		coordFreese = new Object({x:0,y:0});
		checkPointHandlers = new Array();
		checkPointHandlers.push(checkHandler0);
		checkHandler0.id = 0;
		checkHandler0.RegisterOnUpdateHandler(this,OnHandlerUpdate);
		
	}
	function onLoad() : Void {
		checkHandler0.id = 0;
		checkHandler0.RegisterOnUpdateHandler(this,OnHandlerUpdate);
		checkHandler0.RegisterOnFinishDragHandler(this,OnUpdate);
	}
	function AttachUnit(mu:TextEffectUnit) : Void {
						
		effect = mu.getEffect();//_global.Project.CurrentUnit.getEffect();
		unit = MoveableUnit(mu);
		if(effect.name == null)
			return;
		
		super.AttachUnit(MoveableUnit(mu));
		
		_visible = true;
		
		_x = unit.X;
		_y = unit.Y;

		_rotation = unit.Angle;
			
		saveAngle = NaN;
		frameBorder._rotation = rotateHandler._rotation = resizeHandler._rotation = 0;
		frameBorder._x = 0;
		frameBorder._y = 0;
		var cnt:Number =0;
		checkPointsCountUpdate(effect.refPoints.length);
		for(var i:Number=0 ;i<effect.refPoints.length ;i++) {
			checkPointHandlers[i]._visible = true;
			checkPointHandlers[i]._x = effect.refPoints[i].x*ScaleX/100;
			checkPointHandlers[i]._y = effect.refPoints[i].y*ScaleY/100;
			checkPointHandlers[i].id = effect.refPoints[i].id;
		}		
		
		var fbUpperLeftX:Number = frameBorder.getBounds(this).xMin;
		var fbUpperLeftY:Number = frameBorder.getBounds(this).yMin;
		rotateHandler._x = fbUpperLeftX;
		rotateHandler._y = fbUpperLeftY;
		resizeHandler._x = (fbUpperLeftX + frameBorder._width);
		resizeHandler._y = (fbUpperLeftY + frameBorder._height);
		if(effect.subtype == "Circular"){
			if(tempObj != null){
				if(mu == tempObj.unit && tempObj.effect == effect)
				{
					//_global.tr("BSM - AttachUnit");
					SetDefaultCircularCoords();
					tempObj = null;
					mu.Draw();
				}
			}
		}
	}
	
	function DetachUnit() : Void {
		_visible = false;
		unit = undefined;
		tempObj = null;
	}
	private function OnUpdate() : Void {
		_global.Project.CurrentUnit.UpdateTextEffect();
	}
	private function OnHandlerUpdate(eventObject:Object) : Void {
		if(effect.type == "Circle" || effect.type == "Button") {
			 if(eventObject.id == 0){
				if((eventObject.X / ScaleX * 100 )< (effect.refPoints[2].x)  )
					effect.refPoints[eventObject.id].x = eventObject.X / ScaleX * 100;
				else 
					effect.refPoints[eventObject.id].x = effect.refPoints[2].x-1;
			 }else if(eventObject.id == 1 ){
				 if((eventObject.Y / ScaleX * 100 )< (effect.refPoints[3].y)  )
					effect.refPoints[eventObject.id].y = eventObject.Y / ScaleY * 100;
				 else 
					effect.refPoints[eventObject.id].y = effect.refPoints[3].y-1;
			 } else {
				 effect.refPoints[eventObject.id].x = eventObject.X / ScaleX * 100;
				 effect.refPoints[eventObject.id].y = eventObject.Y / ScaleY * 100;
			 }
		}
		if (effect.subtype == "Circular")  {
			SpecialCircularProcessing(eventObject);
		}else {
			effect.refPoints[eventObject.id].x = eventObject.X / ScaleX * 100;
			effect.refPoints[eventObject.id].y = eventObject.Y / ScaleY * 100;
		}
		AttachUnit(TextEffectUnit(unit));
		
	}
	public function set HandlersScale(value:Number):Void {
		rotateHandler._xscale = rotateHandler._yscale = 
		resizeHandler._xscale = resizeHandler._yscale = 
		rotateLabel._xscale = rotateLabel._yscale =
		resizeLabel._xscale = resizeLabel._yscale =
		frameBorder._xscale = frameBorder._yscale = 
		handScale = value;
		RedrawUnit();
		var len:Number = checkPointHandlers.length;
		for(var i:Number = 0; i < len; i++) {
			checkPointHandlers[i]._xscale = handScale;
			checkPointHandlers[i]._yscale = handScale; 
		}
	}
	private function checkPointsCountUpdate(num:Number) : Void {
		var len:Number = checkPointHandlers.length;
		for(var i:Number = 0; i < len; i++) {
			checkPointHandlers[i]._visible = false;
			checkPointHandlers[i]._xscale = checkPointHandlers[i]._yscale = handScale; 
		}
		if(num <= len)
			return;
		else {
			
			for( var i:Number = 0; i < num - len; i++) {
				var id:Number = checkPointHandlers.length;
				var res:MovieClip = this.attachMovie("CheckPointHandler","checkHand"+id, this.getNextHighestDepth());
				res._yscale = res._xscale = handScale;
				res.RegisterOnUpdateHandler(this,OnHandlerUpdate);
				res.RegisterOnFinishDragHandler(this,OnUpdate);
				checkPointHandlers.push(res);
			}
		}
	}
	public function Bounds(mu) {
		return frameBorder.Bounds(mu);
	}
	public function FreezeCoords() : Void {
		coordFreese.x = this.getBounds().xMin;
		coordFreese.y = this.getBounds().yMin;
	}
	public function UnFreezeCoords() : Void {
		//this._x = coordFreese.x;
		//this._y = coordFreese.y;
		
	}
	
	private function SpecialCircularProcessing(eventObject:Object) : Void {
		
		//_global.tr("BSM - SpecialCircularProcessing");
				
		effect.refPoints[0].x = eventObject.X;
		effect.refPoints[0].y = eventObject.Y;
		
		
		
		
		/*
		var faceCX:Number = _global.Project.CurrentPaper.CurrentFace.CenterX;
		var faceCY:Number = _global.Project.CurrentPaper.CurrentFace.CenterY;
		var p:Array = effect.refPoints;
		var faceCPt:Object = new Object({x:0, y:0});
		var nPoint:Number = parseInt(eventObject.id);
		var m_ptLast:Object = new Object({X:effect.refPoints[nPoint].x,Y:effect.refPoints[nPoint].y});
		var pt:Object = new Object({X:eventObject.X, Y:eventObject.Y});
				
		switch (nPoint)
		{
		case 0:
			// Calculate the current radius.
			var radius:Number = Math.sqrt((effect.refPoints[2].x)*(effect.refPoints[2].x)+(effect.refPoints[2].y)*(effect.refPoints[2].y));
			// Calculate the current point angle.
			var fCurAngle:Number = Math.atan2(pt.Y, pt.X);
			// Calculate the other end point angle.
			var fStartAngle:Number= Math.atan2(effect.refPoints[2].y,effect.refPoints[2].x);
			// Calculate the total angle
			var fTotalAngle:Number = 0.0; 
			if (fCurAngle >= fStartAngle)
				fTotalAngle = fCurAngle - fStartAngle;
			else
				fTotalAngle = (fCurAngle + 2.0 * Math.PI - fStartAngle);
			// Calculate new arc length.
			var fArcLength:Number = fTotalAngle * radius;

			// Calculate point 0 position.
			pt.X = faceCPt.x+radius * Math.cos(fCurAngle); 
			pt.Y = faceCPt.y+radius * Math.sin(fCurAngle);
			effect.refPoints[0].x = pt.X;
			effect.refPoints[0].y = pt.Y;
			effect.refPoints[2].x = 2 * effect.refPoints[1].x - pt.X;
			effect.refPoints[2].y = pt.Y;
			
			break;
		case 1:
			var middleCoords:Object = new Object({x:effect.refPoints[1].x, y:eventObject.Y});
			var p0:Object = new Object({x:effect.refPoints[0].x, y:effect.refPoints[0].y});
			var p1:Object = new Object({x:effect.refPoints[1].x, y:effect.refPoints[1].y});
			var p2:Object = new Object({x:effect.refPoints[2].x, y:effect.refPoints[2].y});
			var CX:Number = faceCPt.x;
			var CY:Number = faceCPt.y;

			var radius    : Number = Math.sqrt((middleCoords.x-CX)*(middleCoords.x-CX)+(middleCoords.y-CY)*(middleCoords.y-CY));
			var currAngle : Number = Math.atan2(middleCoords.x-CX, middleCoords.y-CY);
			var oldAngle  : Number = Math.atan2(p1.x-CX, p1.y-CY);
			var angle1    : Number = Math.atan2(p0.x-CX, p0.y-CY);			
			var angle2    : Number = Math.atan2(p2.x-CX, p2.y-CY);			
			var delta     : Number = currAngle - oldAngle;
			
			p0.x = CX + (radius*Math.sin(angle1+delta))/(this.ScaleX/100);
			p0.y = CY + (radius*Math.cos(angle1+delta))/(this.ScaleY/100);
			p1.x = effect.refPoints[1].x;
			p1.y = eventObject.Y / (this.ScaleY / 100);
			p2.x = CX + (radius*Math.sin(angle2+delta))/(this.ScaleX/100);
			p2.y = CY + (radius*Math.cos(angle2+delta))/(this.ScaleY/100);
			
			if (Math.abs(p0.y-p1.y) > 0.1/(this.ScaleX/100)) {
				effect.refPoints[0].x = p0.x;
				effect.refPoints[0].y = p0.y;
				effect.refPoints[1].x = p1.x;
				effect.refPoints[1].y = p1.y;
				effect.refPoints[2].x = p2.x;
				effect.refPoints[2].y = p2.y;
			}
			
			break;
		case 2:
			// Calculate the current radius.
			var radius:Number = Math.sqrt((effect.refPoints[0].x)*(effect.refPoints[0].x)+(effect.refPoints[0].y)*(effect.refPoints[0].y));
			// Calculate the current point angle.
			var fCurAngle:Number = Math.atan2(pt.Y, pt.X);
			// Calculate the other end point angle.
			var fEndAngle:Number= Math.atan2(effect.refPoints[0].y,effect.refPoints[0].x);
			// Calculate the total angle
			var fTotalAngle:Number = 0.0; 
			if (fEndAngle >= fCurAngle)
				fTotalAngle = fEndAngle - fCurAngle;
			else
				fTotalAngle = (fEndAngle + 2.0 * Math.PI - fCurAngle);
			// Calculate the new text length.
			var fArcLength:Number = fTotalAngle * radius;

			pt.X = radius * Math.cos(fCurAngle); 
			pt.Y = radius * Math.sin(fCurAngle); 
			effect.refPoints[2].x = pt.X;
			effect.refPoints[2].y = pt.Y;
			effect.refPoints[0].x = 2 * effect.refPoints[1].x - pt.X;
			effect.refPoints[0].y = pt.Y;
			
			break;
		default:
			break;
		}	*/		
	}
	public function PrepareSpecialEffect(unit, effect) : Void {
		tempObj = new Object();
		tempObj.unit = unit;
		tempObj.effect = effect;
		
	}
	public function SetDefaultCircularCoords() : Void {
			var faceCX:Number = _global.Project.CurrentPaper.CurrentFace.CenterX;
			var faceCY:Number = _global.Project.CurrentPaper.CurrentFace.CenterY;

			unit.UnlockMove();
			unit.ScaleX = unit.ScaleY = 100;
			unit.X = faceCX;
			unit.Y = faceCY;
			
			unit.LockMove();
			effect.refPoints[0].x = effect.refPoints[0].y = 0;
			var p1:Object = new Object({x:effect.refPoints[0].x, y:effect.refPoints[0].y});
			//var p2:Object = new Object({x:effect.refPoints[1].x, y:effect.refPoints[1].y});
			//var p3:Object = new Object({x:effect.refPoints[2].x, y:effect.refPoints[2].y});
			
			this._x = faceCX;
			this._y = faceCY;
			//var r:Number = Math.min(faceCX/2 ,faceCY/2);
			var r:Number = Math.min(faceCX ,faceCY);
			var format:TextFormat = new TextFormat();
			var textEffectUnit = unit;
			format.size = r / textEffectUnit.LineNumbers;
			//textEffectUnit.ChangeFontFormat(0, 0, format);
			//r *= 1.3;
			var maxTokenLength:Number = Math.max(1, textEffectUnit.MaxLineLength);
			var angle:Number = Math.min(maxTokenLength * Math.PI / 20, Math.PI /2);
			var rsin:Number = r * Math.sin(angle);
			var rcos:Number = r * Math.cos(angle);
						
			
			p1.x = faceCX;
			if(effect.type == "Rounding") {				
				p1.y = faceCY + r/2;
			} else {
				p1.y = faceCY - r/2;
			}
			
			/*p1.x = faceCX - rsin;
			p1.y = faceCY + rcos;*/
			/*p2.x = faceCX;
			p2.y = faceCY + r;			
			p3.x = faceCX + rsin;
			p3.y = faceCY + rcos;*/
			
			_parent.localToGlobal(p1);
			//_parent.localToGlobal(p2);
			//_parent.localToGlobal(p3);
			this.globalToLocal(p1);
			//this.globalToLocal(p2);
			//this.globalToLocal(p3);
			effect.refPoints[0].x = p1.x;
			effect.refPoints[0].y = p1.y;
			//effect.refPoints[1].x = p2.x;
			//effect.refPoints[1].y = p2.y;
			/*effect.refPoints[2].x = p3.x;
			effect.refPoints[2].y = p3.y;*/
			
			_global.Project.CurrentUnit.UpdateTextEffect();
	}

	public function Move() : Void {
		
		//if(effect.subtype != 'Circular') {
		// return;
		//}
			/*
			var faceCX:Number = _global.Project.CurrentPaper.CurrentFace.CenterX;
			var faceCY:Number = _global.Project.CurrentPaper.CurrentFace.CenterY;
			var newX:Number = this._x;
			var newY:Number = this._y;
			var p1:Object = new Object({x:effect.refPoints[0].x, y:effect.refPoints[0].y});
			this.localToGlobal(p1);
			_parent.globalToLocal(p1);
			var p2:Object = new Object({x:effect.refPoints[1].x, y:effect.refPoints[1].y});
			this.localToGlobal(p2);
			_parent.globalToLocal(p2);
			var p3:Object = new Object({x:effect.refPoints[2].x, y:effect.refPoints[2].y});
			this.localToGlobal(p3);
			_parent.globalToLocal(p3);

			var radius:Number = Math.sqrt((newX-faceCX)*(newX-faceCX)+(newY-faceCY)*(newY-faceCY));
			var currAngle:Number = Math.atan2(newX-faceCX, newY-faceCY);
			var oldAngle:Number = Math.atan2(unit.X-faceCX, unit.Y-faceCY);
			var angle1:Number = Math.atan2(p1.x-faceCX, p1.y-faceCY);			
			var angle2:Number = Math.atan2(p2.x-faceCX, p2.y-faceCY);			
			var angle3:Number = Math.atan2(p3.x-faceCX, p3.y-faceCY);			
			var delta:Number = currAngle - oldAngle;			
			
			p1.x = faceCX + radius*Math.sin(angle1+delta);
			p1.y = faceCY + radius*Math.cos(angle1+delta);
			p2.x = faceCX + radius*Math.sin(angle2+delta);
			p2.y = faceCY + radius*Math.cos(angle2+delta);
			p3.x = faceCX + radius*Math.sin(angle3+delta);
			p3.y = faceCY + radius*Math.cos(angle3+delta);
			
			_parent.localToGlobal(p1);
			_parent.localToGlobal(p2);
			_parent.localToGlobal(p3);
			this.globalToLocal(p1);
			this.globalToLocal(p2);
			this.globalToLocal(p3);
			effect.refPoints[0].x = p1.x;
			effect.refPoints[0].y = p1.y;
			effect.refPoints[1].x = p2.x;
			effect.refPoints[1].y = p2.y;
			effect.refPoints[2].x = p3.x;
			effect.refPoints[2].y = p3.y;
			AttachUnit(TextEffectUnit(unit));*/
	}
	public function StopDrag() : Void {
		//if(effect.subtype != 'Circular')
			return;
		var u:TextEffectUnit = TextEffectUnit(unit);
		u.UpdateTextEffect();
	}
	public function SpecialResize(kx:Number, ky:Number) : Void {
		kx = ky = Math.min(kx,ky);
		var bounds:Object = unit['mc'].getBounds(this);
		var boundsBorder:Object = frameBorder.getBounds(this);
		var boundsThis:Object = this.getBounds(this);
		
		var oldkx:Number = ScaleX/100;
		var oldky:Number = ScaleY/100;
		var xMin:Number = bounds.xMin/*oldkx ;//*/* 100 / this._xscale;
		var yMin:Number = bounds.yMin/*oldky ;//*/* 100 / this._yscale;
		var xMax:Number = bounds.xMax/*oldkx ;//*/* 100 / this._xscale;
		var yMax:Number = bounds.yMax/*oldky ;//*/* 100 / this._yscale;
		
		var xMin1:Number = boundsBorder.xMin;
		var yMin1:Number = boundsBorder.yMin;
		var xMax1:Number = boundsBorder.xMax;
		var yMax1:Number = boundsBorder.yMax;
		
		var xMin2:Number = boundsThis.xMin+_parent.rotateHandler._width;
		var yMin2:Number = boundsThis.yMin+_parent.rotateHandler._width;
		var xMax2:Number = boundsThis.xMax-_parent.resizeHandler._width;
		var yMax2:Number = boundsThis.yMax-_parent.resizeHandler._width;
		
		xMin = xMin<xMin1?xMin:xMin1;
		yMin = yMin<yMin1?yMin:yMin1;
		xMax = xMax>xMax1?xMax:xMax1;
		yMax = yMax>yMax1?yMax:yMax1;
		
		/****/
		
		var bnds:Object = _global.SelectionFrame.Bounds(this);
		var uWidth:Number = bounds.xMax - bounds.xMin;
		var uHeight:Number = bounds.yMax - bounds.yMin;
		var bWidth:Number = xMax - xMin;
		var bHeight:Number = yMax - yMin;
		kx /= (bWidth/uWidth);
		ky /= (bHeight/uHeight);
		kx = ky = Math.max(kx,ky);
		if(unit['mc']._width*kx > 1 && unit['mc']._height*ky > 1 ) {
			unit.ScaleY *=ky; 	
			unit.ScaleX = unit.ScaleY;
		}
		
		
	}
	public function SpecialStepResize(kx:Number, ky:Number) : Void {
		unit.ScaleX*=kx;
		unit.ScaleY*=kx;
	}
	public function set Angle(value:Number):Void {
		/*if(effect.subtype == 'Circular') {
			var faceCX = _global.Project.CurrentPaper.CurrentFace.CenterX;
			var faceCY = _global.Project.CurrentPaper.CurrentFace.CenterY;
			var newCoords:Object = _global.RotateAboutCenter(X, Y, faceCX, faceCY, value);
			this._x = newCoords.x;
			this._x = newCoords.y;		
		}*/
		
		super.Angle = value;


		/*var rot = _rotation;
		var xS = _x;
		var yS = _y;
		if (!isNaN(saveAngle)) {
			rot = saveAngle;
			xS = saveX;
			yS = saveY;
		}
			
		
		var w = frameBorder.getBounds(this).xMin + frameBorder._width/2-1;
		var h = frameBorder.getBounds(this).yMin + frameBorder._height/2-1;
		var r = Math.sqrt(w*w + h*h);
		
		var angle = Math.atan(h/Math.abs(w));
		if (w<0) angle = Math.PI - angle;
		angle += rot * Math.PI/180;
		var x0 = w - r * Math.cos(angle);
		var y0 = h - r * Math.sin(angle);
		
		angle = Math.atan(h/Math.abs(w));
		if (w<0) angle = Math.PI - angle;
		angle += value * Math.PI/180;
		var x = w - r * Math.cos(angle);
		var y = h - r * Math.sin(angle);
		
		if (isNaN(saveAngle)) {
			saveAngle = _rotation;
			saveX = _x;
			saveY = _y;
		}
		unit.UnlockMove();
		_x     = xS + (x - x0);
		unit.X = _x;
		_y     = yS + (y - y0);
		unit.Y = _y;
		unit.LockMove();

		_rotation = value;
		unit.Angle = value;
		
		MoveHandlerLabels();*/
	}
	
	public function get Angle():Number {
		return super.Angle;
	}
	
}