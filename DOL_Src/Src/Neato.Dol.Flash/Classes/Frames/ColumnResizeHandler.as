﻿import mx.utils.Delegate;
class Frames.ColumnResizeHandler extends mx.core.UIObject {
	private var xMouseBegin:Number = 0;
	private var yMouseBegin:Number = 0;
    var colNum:Number;
    
    private var oldWidth:Number = null; 
    
	function ColumnResizeHandler() {
		this.onPress = drag;
		this.onMouseMove = null;
		this.onMouseUp = mouseUp;
		this.onRollOver = rollOver;
		this.onRollOut = rollOut;
		gotoAndStop(1);
	}
	
	function drag() {
		trace("resize drag");
		this.onMouseMove = mouseMove;
		xMouseBegin = _xmouse;
		yMouseBegin = _ymouse;
		gotoAndStop(3);
		
		oldWidth = _global.Project.CurrentUnit.GetColumnsWidth()[this.colNum];
	}
	
	function noDrag() {
		trace("resize noDrag");
		this.onMouseMove = null;
		gotoAndStop(1);
	}
	
	function mouseMove() {
		var keepProportionOnly:Boolean = false;
		_global.Project.CurrentUnit.ResizeColumnWidth(this.colNum,(_xmouse - xMouseBegin)*this._xscale/100);
		_global.SelectionFrame.AttachUnit(_global.Project.CurrentUnit);
	}
	
	function rollOver() {
		gotoAndStop(2);
	}
	
	function rollOut() {
		gotoAndStop(1);
	}
		
	function mouseUp() {
		if (onMouseMove != null) {
			noDrag();

			var unit = _global.Project.CurrentUnit;
			var newWidth = unit.GetColumnsWidth()[this.colNum];
			
			var action:Actions.ColumnWidthResizeAction = new Actions.ColumnWidthResizeAction(unit, colNum, oldWidth, newWidth);
			_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
			oldWidth = null;
		}
	}
}