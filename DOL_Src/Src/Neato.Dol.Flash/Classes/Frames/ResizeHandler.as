class Frames.ResizeHandler extends mx.core.UIObject {
	private var mousePositionDelta:Object;
	private var oldScaleX:Number = null;
	private var oldScaleY:Number = null;
	
	function ResizeHandler() {
		this.onPress = drag;
		this.onMouseMove = null;
		this.onMouseUp = mouseUp;
		this.onRollOver = rollOver;
		this.onRollOut = rollOut;
		gotoAndStop(1);
	}
	
	function drag() {
		trace("resize drag");
		var unit = _global.Project.CurrentUnit;
		var scaleXString:String = Actions.ScaleAction.ScaleXProperty(unit);
		var scaleYString:String = Actions.ScaleAction.ScaleYProperty(unit);
		oldScaleX = unit[scaleXString];
		oldScaleY = unit[scaleYString];
		
		this.onMouseMove = mouseMove;
		
		var startPosition:Object = {x:this._x, y:this._y};
		this._parent.localToGlobal(startPosition);
		this.globalToLocal(startPosition);

		mousePositionDelta = {x:this._xmouse - startPosition.x, y:this._ymouse - startPosition.y};
		
		gotoAndStop(3);
	}
	
	function noDrag() {
		trace("resize noDrag");
		this.onMouseMove = null;
		gotoAndStop(1);
	}
	
	function mouseMove() {
		var newPosition:Object = {x:_xmouse - mousePositionDelta.x, y:_ymouse - mousePositionDelta.y};
		this.localToGlobal(newPosition);
		_global.Project.CurrentUnit.ResizeToPosition(newPosition.x, newPosition.y);
		_global.SelectionFrame.AttachUnit(_global.Project.CurrentUnit);
	}
	
	function rollOver() {
		gotoAndStop(2);
	}
	
	function rollOut() {
		gotoAndStop(1);
	}
		
	function mouseUp() {
		if (onMouseMove != null) {
			noDrag();

			var unit = _global.Project.CurrentUnit;
			var scaleXString:String = Actions.ScaleAction.ScaleXProperty(unit);
			var scaleYString:String = Actions.ScaleAction.ScaleYProperty(unit);
			var newScaleX:Number = unit[scaleXString];
			var newScaleY:Number = unit[scaleYString];
	
			var action:Actions.ScaleAction = new Actions.ScaleAction(unit, oldScaleX, oldScaleY, newScaleX, newScaleY);
			_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
			oldScaleX = oldScaleY = null;
		}
	}
}