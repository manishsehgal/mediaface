﻿import Geometry.*;
class Frames.FrameBorderEx extends MovieClip {

	function Draw(mu:MoveableUnit):Void {
		var kx:Number = 1;//_parent.ScaleX/100;
		var ky:Number = 1;//_parent.ScaleY/100;
		this._xscale = _parent.ScaleX;
		this._yscale = _parent.ScaleY;
		this.clear();
		
		lineStyle(0,0x007FFF);
		var effect:Object = _parent.effect;
		//trace("FrameBorderEx:Draw:effect.type="+effect.type+";effect.subtype="+effect.subtype);
		var fontSize:Number = effect.fontSize;
		if(effect.type == "Curved" && effect.subtype == "DoubleArch") {
			for(var i:Number=0 ;i<effect.refPoints.length ; i += 4) {
				moveTo(effect.refPoints[i].x*kx,effect.refPoints[i].y*ky);
				GeometryHelper.DrawCubicBezier(this,
											effect.refPoints[i].x*kx,   effect.refPoints[i].y*ky,
											effect.refPoints[i+1].x*kx, effect.refPoints[i+1].y*ky,
											effect.refPoints[i+2].x*kx, effect.refPoints[i+2].y*ky,
											effect.refPoints[i+3].x*kx, effect.refPoints[i+3].y*ky
											);
			}
			if(effect.refPoints.length == 8) { //refactor:update logic for diff. effects
				moveTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);
				lineTo(effect.refPoints[4].x*kx,effect.refPoints[4].y*ky);
				moveTo(effect.refPoints[3].x*kx,effect.refPoints[3].y*ky);
				lineTo(effect.refPoints[7].x*kx,effect.refPoints[7].y*ky);
			}
		} else if(effect.type == "Curved" && effect.subtype == "Perspective"){
				moveTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);
				lineTo(effect.refPoints[1].x*kx,effect.refPoints[1].y*ky);
				lineTo(effect.refPoints[3].x*kx,effect.refPoints[3].y*ky);
				lineTo(effect.refPoints[2].x*kx,effect.refPoints[2].y*ky);
				lineTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);
		} else if(effect.type == "Curved" && effect.subtype == "Balloon"){
			//trace("Here: curved + Balloon");
				moveTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);
				GeometryHelper.DrawArcFrom3Points(this,
					effect.refPoints[0].x*kx,effect.refPoints[0].y*ky,
					effect.refPoints[1].x*kx,effect.refPoints[1].y*ky,
					effect.refPoints[2].x*kx,effect.refPoints[2].y*ky
					);
				moveTo(effect.refPoints[2].x*kx,effect.refPoints[2].y*ky);	
				lineTo(effect.refPoints[5].x*kx,effect.refPoints[5].y*ky);
				GeometryHelper.DrawArcFrom3Points(this,
					effect.refPoints[3].x*kx,effect.refPoints[3].y*ky,
					effect.refPoints[4].x*kx,effect.refPoints[4].y*ky,
					effect.refPoints[5].x*kx,effect.refPoints[5].y*ky
					);
				//curveTo(effect.refPoints[2].x,effect.refPoints[2].y,effect.refPoints[1].x,effect.refPoints[1].y);
				//lineTo(effect.refPoints[2].x,effect.refPoints[2].y);
				moveTo(effect.refPoints[3].x*kx,effect.refPoints[3].y*ky);
				lineTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);
		} else if(effect.type == "Curved" && effect.subtype == "BottomArc"){
				moveTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);
				lineTo(effect.refPoints[1].x*kx,effect.refPoints[1].y*ky);
				lineTo(effect.refPoints[4].x*kx,effect.refPoints[4].y*ky);
				moveTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);	
				lineTo(effect.refPoints[2].x*kx,effect.refPoints[2].y*ky);	
				GeometryHelper.DrawArcFrom3Points(this,
					effect.refPoints[2].x*kx,effect.refPoints[2].y*ky,
					effect.refPoints[3].x*kx,effect.refPoints[3].y*ky,
					effect.refPoints[4].x*kx,effect.refPoints[4].y*ky
					);
		}else if(effect.type == "Curved" && effect.subtype == "TopArc"){
				moveTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);
				GeometryHelper.DrawArcFrom3Points(this,
					effect.refPoints[0].x*kx,effect.refPoints[0].y*ky,
					effect.refPoints[1].x*kx,effect.refPoints[1].y*ky,
					effect.refPoints[2].x*kx,effect.refPoints[2].y*ky
					);
				moveTo(effect.refPoints[2].x*kx,effect.refPoints[2].y*ky);
				lineTo(effect.refPoints[4].x*kx,effect.refPoints[4].y*ky);
				lineTo(effect.refPoints[3].x*kx,effect.refPoints[3].y*ky);
				lineTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);	
		} else if((effect.type == "Guided" && (effect.subtype == "LineAngleDown" || effect.subtype == "LineAngleUp"))
					||(effect.type == "GuidedFit" && effect.subtype == "fit")) {
			var x0:Number = effect.refPoints[0].x*kx;
			var y0:Number = effect.refPoints[0].y*ky;
			var x1:Number = effect.refPoints[1].x*kx;
			var y1:Number = effect.refPoints[1].y*ky;
			moveTo((effect.refPoints[0].x)*kx ,(effect.refPoints[0].y) *ky);
			lineTo((effect.refPoints[1].x)*kx ,(effect.refPoints[1].y)*ky);

		} else if(effect.type == "Guided" && effect.subtype == "Curve4" ) {
			
			var x0:Number = effect.refPoints[0].x*kx;
			var y0:Number = effect.refPoints[0].y*ky;
			var x1:Number = effect.refPoints[1].x*kx;
			var y1:Number = effect.refPoints[1].y*ky;
			var x2:Number = effect.refPoints[2].x*kx;
			var y2:Number = effect.refPoints[2].y*ky;
			var x3:Number = effect.refPoints[3].x*kx;
			var y3:Number = effect.refPoints[3].y*ky;
			
			var angl1:Number = Math.atan2((y1-y0),(x1-x0));///Math.sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0))) ;
			var angl2:Number = Math.atan2((y2-y1),(x2-x1));
			var angl3:Number = Math.atan2((y3-y2),(x3-x2));
			moveTo(x0,y0);
			GeometryHelper.DrawCubicBezier(this,
						(x0),(y0),
						(x1),(y1),
						(x2),(y2),
						(x3),(y3)
						);			
		} else if(effect.type == "Circle" || effect.type == "Button") {
			var x0:Number = effect.refPoints[0].x*kx;
			var y0:Number = effect.refPoints[0].y*ky;
			var x1:Number = effect.refPoints[1].x*kx;
			var y1:Number = effect.refPoints[1].y*ky;
			var x2:Number = effect.refPoints[2].x*kx;
			var y2:Number = effect.refPoints[2].y*ky;
			var x3:Number = effect.refPoints[3].x*kx;
			var y3:Number = effect.refPoints[3].y*ky;
			var tmpXml:XML = new XML("<Obj/>");
			
			if(x2<x0)
			{
				x2 = x0;
				effect.refPoints[2].x = effect.refPoints[0].x;
			}
			
			
			if(y3<y1){
				y3 = y1;
				effect.refPoints[3].y = effect.refPoints[1].y;
				
			}
			tmpXml.attributes.x = (x2+x0) / 2;
			tmpXml.attributes.y = (y3+y1) / 2;
			tmpXml.attributes.rH = (x2-x0) / 2;
			tmpXml.attributes.rV = (y3-y1) / 2;
			
			var circle:EllipsePrimitive = new EllipsePrimitive(tmpXml);
			x0 = effect.refPoints[0].x;
			y0 = effect.refPoints[0].y;
			x1 = effect.refPoints[1].x;
			y1 = effect.refPoints[1].y;
			x2 = effect.refPoints[2].x;
			y2 = effect.refPoints[2].y;
			x3 = effect.refPoints[3].x;
			y3 = effect.refPoints[3].y;

			effect.refPoints[0].y = (y3+y1) / 2;
			effect.refPoints[2].y = (y3+y1) / 2;
			effect.refPoints[1].x = (x2+x0) / 2;
			effect.refPoints[3].x = (x2+x0) / 2;
			circle.Draw(this,0,0,0);

		} else if(effect.type == "GuidedFit" && effect.subtype == "CurveArc" ) {
					GeometryHelper.DrawArcFrom3Points(this,
						effect.refPoints[0].x*kx,effect.refPoints[0].y*ky,
						effect.refPoints[1].x*kx,effect.refPoints[1].y*ky,
						effect.refPoints[2].x*kx,effect.refPoints[2].y*ky
					);
		} else if( effect.subtype == "Circular" ) {
			////////////////////////////////////////////////////////////////////
			// _global.tr("BSM - FrameBorderEx - Draw - "+ _global.Project.CurrentUnit.frame);
			
			var textWidth:Number = _global.Project.CurrentUnit.frame.attributes.Width;
			var textHeight:Number = _global.Project.CurrentUnit.frame.attributes.Height;
			var textRadius:Number = _global.Project.CurrentUnit.frame.attributes.Radius;
			var textAlpha:Number = _global.Project.CurrentUnit.frame.attributes.Alpha;
			var textBeta:Number = _global.Project.CurrentUnit.frame.attributes.Beta;
	
			//_global.tr("BSM - textHeigh - "+textHeight);
			if (textHeight == undefined) {
				return;	
			}
			//_global.tr("BSM - DrawBorder0 - effect.refPoints[0].x - "+effect.refPoints[0].x+" - effect.refPoints[0].y"+effect.refPoints[0].y);
					
			var handlerX:Number = effect.refPoints[0].x;
			var handlerY:Number = effect.refPoints[0].y;			
			var handlerRadius:Number = Math.sqrt((handlerX*handlerX)+(handlerY*handlerY));
			var handlerAlfa:Number = Math.atan2(handlerY, handlerX);
			
			//_global.tr("BSM - DrawBorder - handlerRadius - "+handlerRadius+" - textHeight/2"+textHeight/2);
			
			
			if (handlerRadius <= textHeight/2) {
				handlerRadius = textHeight/2 + 0.1;
				handlerX = handlerRadius*Math.cos(handlerAlfa);
				handlerY = handlerRadius*Math.sin(handlerAlfa);
				effect.refPoints[0].x = handlerX;
				effect.refPoints[0].y = handlerY;
			}
			
			//_global.tr("BSM - DrawBorder2 - effect.refPoints[0].x - "+effect.refPoints[0].x+" - effect.refPoints[0].y"+effect.refPoints[0].y);
			
			var pointLAlfa:Number = handlerAlfa - (textWidth/handlerRadius)/2;
			var pointRAlfa:Number = handlerAlfa + (textWidth/handlerRadius)/2;
			
			if (textWidth/handlerRadius >= 2 * Math.PI) {
				pointLAlfa = handlerAlfa + Math.PI + 0.01;
				pointRAlfa = handlerAlfa + Math.PI - 0.01;
			}
			
			var Arc1Point1X:Number = (handlerRadius-(textHeight/2))*Math.cos(pointLAlfa);
			var Arc1Point1Y:Number = (handlerRadius-(textHeight/2))*Math.sin(pointLAlfa);
			var Arc1Point2X:Number = (handlerRadius-(textHeight/2))*Math.cos(handlerAlfa);
			var Arc1Point2Y:Number = (handlerRadius-(textHeight/2))*Math.sin(handlerAlfa);
			var Arc1Point3X:Number = (handlerRadius-(textHeight/2))*Math.cos(pointRAlfa);
			var Arc1Point3Y:Number = (handlerRadius-(textHeight/2))*Math.sin(pointRAlfa);
			
			var Arc2Point1X:Number = (handlerRadius+(textHeight/2))*Math.cos(pointLAlfa);
			var Arc2Point1Y:Number = (handlerRadius+(textHeight/2))*Math.sin(pointLAlfa);
			var Arc2Point2X:Number = (handlerRadius+(textHeight/2))*Math.cos(handlerAlfa);
			var Arc2Point2Y:Number = (handlerRadius+(textHeight/2))*Math.sin(handlerAlfa);
			var Arc2Point3X:Number = (handlerRadius+(textHeight/2))*Math.cos(pointRAlfa);
			var Arc2Point3Y:Number = (handlerRadius+(textHeight/2))*Math.sin(pointRAlfa);
			
			this.lineStyle(0, 0x007FFF, 100);	
			this.moveTo(Arc1Point1X,Arc1Point1Y);
			GeometryHelper.DrawArcFrom3Points(this,Arc1Point1X,Arc1Point1Y,Arc1Point2X,Arc1Point2Y,Arc1Point3X,Arc1Point3Y);
			this.moveTo(Arc1Point3X,Arc1Point3Y);
			this.lineTo(Arc2Point3X,Arc2Point3Y);
			GeometryHelper.DrawArcFrom3Points(this,Arc2Point1X,Arc2Point1Y,Arc2Point2X,Arc2Point2Y,Arc2Point3X,Arc2Point3Y);
			this.moveTo(Arc2Point1X,Arc2Point1Y);
			this.lineTo(Arc1Point1X,Arc1Point1Y);
			
			
			
			var bounds:Object = this.getBounds(this);
			return;
		
		}
		
		var bounds:Object = mu.GetBounds();
		var boundsThis:Object = this.getBounds(this);
		var boundsParent:Object = _parent.getBounds(this);
		
		
		var xMin:Number = bounds.xMin*kx;// * 100 / this._xscale;
		var yMin:Number = bounds.yMin*ky;// * 100 / this._yscale;
		var xMax:Number = bounds.xMax*kx;// * 100 / this._xscale;
		var yMax:Number = bounds.yMax*ky;// * 100 / this._yscale;
				
		var xMin1:Number = boundsThis.xMin;
		var yMin1:Number = boundsThis.yMin;
		var xMax1:Number = boundsThis.xMax;
		var yMax1:Number = boundsThis.yMax;
		
		var xMin2:Number = boundsParent.xMin+_parent.rotateHandler._width;
		var yMin2:Number = boundsParent.yMin+_parent.rotateHandler._width;
		var xMax2:Number = boundsParent.xMax-_parent.resizeHandler._width;
		var yMax2:Number = boundsParent.yMax-_parent.resizeHandler._width;
		
		xMin = xMin<xMin1?xMin:xMin1;
		yMin = yMin<yMin1?yMin:yMin1;
		xMax = xMax>xMax1?xMax:xMax1;
		yMax = yMax>yMax1?yMax:yMax1;
		
		this.moveTo(xMin, yMin);
		var count:Number = 0;
		var stepx:Number = 3/(this._xscale/100)*(_parent.handScale/100);
		var stepy:Number = 3/(this._yscale/100)*(_parent.handScale/100);
		this.lineStyle(0, 0xFFFFFF*(count%2));
		for(var i:Number = yMin; i < yMax; i += stepy, ++count) {
			this.lineTo(xMin, i);
			this.lineStyle(0, 0xFFFFFF*(count%2));
		}
		for(var i:Number = xMin; i < xMax; i += stepx, ++count) {
			this.lineTo(i, yMax);
			this.lineStyle(0, 0xFFFFFF*(count%2));
		}
		for(var i:Number = yMax; i > yMin; i -= stepy, ++count) {
			this.lineTo(xMax, i);
			this.lineStyle(0, 0xFFFFFF*(count%2));
		}
		for(var i:Number = xMax; i > xMin; i -= stepx, ++count) {
			this.lineTo(i, yMin);
			this.lineStyle(0, 0xFFFFFF*(count%2));
		}
		this.lineTo(xMin, yMin);		
				
	}
	
	public function Bounds(mu:TextEffectUnit) :Object {
		if(mu.getEffect().subtype == "Circular"){
			var result:Object = new Object({xMin:0, yMin:0, xMax:0, yMax:0});		
			
			var bounds:Object = this.getBounds(this);
			var minPT:Object = new Object({x:bounds.xMin, y:bounds.yMin});
			var maxPT:Object = new Object({x:bounds.xMax, y:bounds.yMax});
			var faceCX:Number = _global.Project.CurrentPaper.CurrentFace.CenterX;
			var faceCY:Number = _global.Project.CurrentPaper.CurrentFace.CenterY;
			minPT.x -= faceCX;
			maxPT.x -= faceCX;
			minPT.y -= faceCY;
			maxPT.y -= faceCY;
			var rH:Number = Math.abs(minPT.x) > Math.abs(maxPT.x) ? Math.abs(minPT.x):Math.abs(maxPT.x);
			var rV:Number = Math.abs(minPT.y) > Math.abs(maxPT.y) ? Math.abs(minPT.y):Math.abs(maxPT.y);
			var r:Number = Math.max(rH,rV);
			
			minPT.x = faceCX - r;
			maxPT.x = faceCX + r;
			minPT.y = faceCY - r;
			maxPT.y = faceCY + r;
			
			result.xMin = result.xMinBorder = minPT.x;
			result.yMin = result.yMinBorder = minPT.y;
			result.xMax = result.xMaxBorder = maxPT.x;
			result.yMax = result.yMaxBorder = maxPT.y;
			return result;
		}
		
		var bounds:Object = mu.GetBounds();
		var boundsThis:Object = this.getBounds(this);
		var boundsParent:Object = _parent.getBounds(this);
		
		var kx:Number = _parent.ScaleX/100;
		var ky:Number = _parent.ScaleY/100;
		var xMin:Number = bounds.xMin*kx * 100 / this._xscale;
		var yMin:Number = bounds.yMin*ky * 100 / this._yscale;
		var xMax:Number = bounds.xMax*kx * 100 / this._xscale;
		var yMax:Number = bounds.yMax*ky * 100 / this._yscale;
		var result:Object = new Object({xMin:xMin, yMin:yMin, xMax:xMax, yMax:yMax});		
		var xMin1:Number = boundsThis.xMin;
		var yMin1:Number = boundsThis.yMin;
		var xMax1:Number = boundsThis.xMax;
		var yMax1:Number = boundsThis.yMax;
		
		var xMin2:Number = boundsParent.xMin+_parent.rotateHandler._width;
		var yMin2:Number = boundsParent.yMin+_parent.rotateHandler._width;
		var xMax2:Number = boundsParent.xMax-_parent.resizeHandler._width;
		var yMax2:Number = boundsParent.yMax-_parent.resizeHandler._width;
		
		xMin = xMin<xMin1?xMin:xMin1;
		yMin = yMin<yMin1?yMin:yMin1;
		xMax = xMax>xMax1?xMax:xMax1;
		yMax = yMax>yMax1?yMax:yMax1;
		result.xMinBorder = xMin ;
		result.yMinBorder = yMin ;
		result.xMaxBorder = xMax ;
		result.yMaxBorder = yMax ;

		
		return result;
	}
	
}
