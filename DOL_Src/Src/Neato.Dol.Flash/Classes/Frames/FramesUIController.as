﻿import mx.utils.Delegate;
import mx.events.EventDispatcher;
class Frames.FramesUIController{
	var frame;//:Frames.Frame;
	var tableFrame;//:Frames.TableFrame;
	var frameEx;//:Frames.FrameEx;
	var bRegisteredDetach = false;
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(Frames.FramesUIController.prototype);


	function FramesUIController() {
		
	}
	function OnComponentLoaded(compName:String, compObj:Object, param) {
		var secondName:String = compName.split(".")[1];
		trace("Frame:OnComponentLoaded:secondName = "+secondName+"; compobj = "+compObj);
		switch(secondName) {
			case "Frame":
				frame = compObj;//Frames.Frame(compObj);
				trace("Frame:OnComponentLoaded:Switch:frame = "+frame);
				frame.RegisterOnResizeHandler(this,onResizeHandlerEvent);
				frame.RegisterOnRotateHandler(this,onRotateHandlerEvent);
				break;
			case "FrameEx":
				frameEx = compObj;//Frames.FrameEx(compObj);
				frameEx.RegisterOnResizeHandler(this,onResizeHandlerEvent);
				frameEx.RegisterOnRotateHandler(this,onRotateHandlerEvent);
				break;
			case "TableFrame":
				tableFrame = compObj;//Frames.TableFrame(compObj);
				tableFrame.RegisterOnResizeHandler(this,onResizeHandlerEvent);
				tableFrame.RegisterOnRotateHandler(this,onRotateHandlerEvent);
				break;
			case "Attach":
				attach(param.frameName);
				break;
			case "Detach":
				detach();
				break;
				
		}
		//compObj.RegisterOnCommandHandler(this, onCommand);
	}
	
	  
    //=== Command handlers ==================================================
    function attach(frameName:String,bCallDetach:Boolean) {
		var currUnit = _global.Project.CurrentUnit;
		if(bRegisteredDetach == false)	{
			trace("bRegisteredDetach"+_global.FaceHelper);
			_global.FaceHelper.RegisterOnFaceChangedHandler(this, detach);
			bRegisteredDetach = true;
		}
		trace('attach');
		if(currUnit == undefined)
			return;
		if(bCallDetach == true)
			detach();
		if(frameName!=undefined)
		{
			onAttach(currUnit);
			_global.SelectionFrame = _global.Frames[frameName];
			currUnit.FirstAttach();
		}
		currUnit.Attach();
		_global.SelectionFrame.AttachUnit(currUnit);
		_global.Project.CurrentUnit = currUnit;
		_global.Project.CurrentUnit.IsCurrentUnit = true;
		
		
	}
	function detach(eventObject) {
		trace("Detach event");
		onDetach(_global.Project.CurrentUnit);
		
		_global.Project.CurrentUnit.Detach();
		_global.SelectionFrame.DetachUnit();
		frame.DetachUnit();
		frameEx.DetachUnit();
		tableFrame.DetachUnit();
		_global.Project.CurrentUnit.IsCurrentUnit = false;
		_global.Project.CurrentUnit = null;

		
		
	}
	

	public function RegisterAttachDetachHandler(scopeObject:Object, callBackAttach:Function, callBackDetach:Function) {
		this.addEventListener("onAttach", Delegate.create(scopeObject, callBackAttach));
		this.addEventListener("onDetach", Delegate.create(scopeObject, callBackDetach));
    }
	public function UnregisterAttachDetachHandler(scopeObject:Object, callBackAttach:Function, callBackDetach:Function) {
		this.removeEventListener("onAttach", Delegate.create(scopeObject, callBackAttach));
		this.removeEventListener("onDetach", Delegate.create(scopeObject, callBackDetach));
    }
	private function onAttach(unit) {
		if(unit == undefined)
			return;
		var eventObject = {type:"onAttach",unit:unit};
		this.dispatchEvent(eventObject);
	}
	private function onDetach(unit) {
		if(unit == undefined)
			return;
		var eventObject = {type:"onDetach",unit:unit};
		this.dispatchEvent(eventObject);
	}
	
	
	function onResizeHandlerEvent(eventObject) {
		trace("onResizeHandlerEvent");
		attach();
		_global.Project.CurrentUnit.ResizeToPosition(eventObject.x, eventObject.y);
		//_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
	}
	function onRotateHandlerEvent(eventObject) {
		trace("onRotateHandlerEvent");
		attach();
		_global.Project.CurrentUnit.X = eventObject.x;
		_global.Project.CurrentUnit.Y = eventObject.y;
		_global.Project.CurrentUnit.Angle = eventObject.angle;
	}
	
}
