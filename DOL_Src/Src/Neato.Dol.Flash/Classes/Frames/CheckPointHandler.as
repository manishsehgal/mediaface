﻿import mx.utils.Delegate;
class Frames.CheckPointHandler extends mx.core.UIObject {
	public var xMouseBegin:Number = 0;
	public var yMouseBegin:Number = 0;
	public var pos:String;
	public var id:Number;
	private var oldX:Number = null;
	private var oldY:Number = null;
	
	function CheckPointHandler() {
		this.onPress = drag;
		this.onMouseMove = null;
		this.onMouseUp = mouseUp;
		this.onRollOver = rollOver;
		this.onRollOut = rollOut;
		gotoAndStop(1);
	}
	
	function drag() {
		//trace("resize drag");
		if (SAPlugins.IsAvailable() == false) return;
		this.onMouseMove = mouseMove;
		oldX = _x;
		oldY = _y;
		xMouseBegin = _xmouse;
		yMouseBegin = _ymouse;
		gotoAndStop(3);
	}
	
	function noDrag() {
		//trace("resize noDrag");
		this.onMouseMove = null;
		gotoAndStop(1);
		OnFinishDrag();
	}
	
	function mouseMove() {
		this._x = _parent._xmouse;
		this._y = _parent._ymouse;
		//trace("id = "+id);
		OnUpdate( _parent._xmouse,_parent._ymouse);
	}
	
	function rollOver() {
		if (SAPlugins.IsAvailable() == false) return;
		gotoAndStop(2);
	}
	
	function rollOut() {
		gotoAndStop(1);
	}
		
	function mouseUp() {
		if (onMouseMove != null) {
			noDrag();
			
			var unit = _global.Project.CurrentUnit;
			var newX:Number = _x;
			var newY:Number = _y;
			
			var action:Actions.TextEffectCheckPointMoveAction = new Actions.TextEffectCheckPointMoveAction(unit, this.id, oldX, oldY, newX, newY); 
			_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
			oldX = oldY = null;
		}
	}
	//region Exit event
	private function OnUpdate(X:Number,Y:Number) {
		var eventObject = {type:"update", target:this, id:this.id, X:X, Y:Y};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnUpdateHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("update", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion	//region finishdrag event
	private function OnFinishDrag(X:Number,Y:Number) {
		var eventObject = {type:"finishdrag", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnFinishDragHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("finishdrag", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
}