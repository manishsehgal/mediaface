﻿import mx.utils.Delegate;
import mx.core.UIObject;

class ImageStorage extends mx.core.UIObject {
	private var images:Array;
	private var ImageId:Number = 0;
	
	public function ImageStorage() {
		//_global.tr("ImageStorage constructor");
		images = new Array();
		visible = false;
	}
	
	public function LoadImage(mc, url:String, reload:Boolean, showPreloader:Boolean, scopeObject:Object, callBackFunction:Function, noAttach:Boolean):Void {
		noAttach = noAttach == true;// || noAttach == null ? false : noAttach;
		var image = FindImageByUrl(url);
		if (image == null) {
//			_global.tr("### image == null url = " + url)
			image = FindImageByName(mc);
			reload = true;
		}
//		_global.tr("### LoadImage image = " + image);
		if (image == null) {
//			_global.tr("### +++ ImageId = " + ImageId + " url = " + url);
			image = this.createObject("Image", "image" + ImageId, ImageId, new Object());
			image.Id = ImageId ++ ;
			image.ShowPreloader = showPreloader;
			image.Name = mc;
			image.NoAttach = noAttach;
			images.push(image);
			reload = true;
		}
		if (reload == true) {
			image.Url = url;
		}
//		_global.tr("### +++ ImageStorage.LoadImage mc = " + mc + " url = " + url + " images.length = " + images.length);

		if (scopeObject != undefined && scopeObject != null && callBackFunction != undefined && callBackFunction != null) {
			image.RegisterOnImageAttachedHandler(scopeObject, callBackFunction);
		}

		image.AttachImage(mc);
	}

	public function UnloadImage(url:String) : Void {
//		_global.tr("### +++ ImageStarage.UnloadImage() url = " + url);
		for (var i:Number = 0; i < images.length; ++ i) {
			if (images[i].Url == url) {
				var image = images[i];
				this.destroyObject("image" + image.Id);
				images.splice(i, 1);
				break;
			}
		}
	}
	
	public function UnloadProjectImages() : Void {
		var urls:Array = new Array();
		for (var i:Number = 0; i < images.length; ++ i) {
			if (images[i].Url.indexOf(_global.ProjectImageUrlFormat) == 0) {
//				_global.tr("### UnloadProjectImages url = " + images[i].Url);
				urls.push(images[i].Url);
			}
		}
		var length:Number = urls.length;
		for (var i:Number = 0; i < length; ++ i) {
			var url:String = String(urls.pop());
			UnloadImage(url);
		}
	}
	
	public function FindImageByUrl(url:String) {
		for (var i:Number = 0; i < images.length; ++ i) {
//			_global.tr("### i = "  + i + " images[i].Url = " + images[i].Url);
			if (images[i].Url == url)
				return images[i];
		}
		return null;
	}
	
	public function FindImageByName(name:String) {
		for (var i:Number = 0; i < images.length; ++ i) {
			if (images[i].Name == name)
				return images[i];
		}
		return null;
	}
}