﻿class Geometry.GeometryHelper {

	public static var M_RAD_2_DEG:Number = 57.295779;
	public static var M_DEG_2_RAD:Number = 1.0 / M_RAD_2_DEG;
	/////////////////////////////////////////////////////////////////////////
	//
	// Add a drawCubicBezier2 to the movieClip prototype based on a MidPoint 
	// simplified version of the midPoint algorithm by Helen Triolo
	//
	/////////////////////////////////////////////////////////////////////////
	
	// This function will trace a cubic approximation of the cubic Bezier
	// It will calculate a serie of [control point/Destination point] which 
	// will be used to draw quadratic Bezier starting from P0
	public static function DrawCubicBezier(mc:MovieClip, x1, y1, x2, y2, x3, y3, x4, y4) {
		DrawCubicBezier4(mc, {x : x1, y : y1 },
							 {x : x2, y : y2 },
							 {x : x3, y : y3 },
							 {x : x4, y : y4 } );
	}
	public static function DrawCubicBezier2(mc:MovieClip,P0, P1, P2, P3) {

	// calculates the useful base points
		var PA = getPointOnSegment(P0, P1, 3/4);
		var PB = getPointOnSegment(P3, P2, 3/4);
		
		var Pa_2 = getMiddle(PA, PB);

		// draw two quadratic subsegments
		mc.curveTo(PA.x, PA.y, Pa_2.x, Pa_2.y);
		mc.curveTo(PB.x, PB.y, P3.x, P3.y);
	}

	public static function DrawCubicBezier4(mc:MovieClip,P0, P1, P2, P3) {

	// calculates the useful base points
		var PA = getPointOnSegment(P0, P1, 3/4);
		var PB = getPointOnSegment(P3, P2, 3/4);
		
		// get 1/16 of the [P3, P0] segment
		var dx = (P3.x - P0.x)/16;
		var dy = (P3.y - P0.y)/16;
	
		// calculates control point 1
		var Pc_1 = getPointOnSegment(P0, P1, 3/8);
	
		// calculates control point 2
		var Pc_2 = getPointOnSegment(PA, PB, 3/8);
		Pc_2.x -= dx;
		Pc_2.y -= dy;
		
		// calculates control point 3
		var Pc_3 = getPointOnSegment(PB, PA, 3/8);
		Pc_3.x += dx;
		Pc_3.y += dy;
	
		// calculates control point 4
		var Pc_4 = getPointOnSegment(P3, P2, 3/8);
	
		// calculates the 3 anchor points
		var Pa_1 = getMiddle(Pc_1, Pc_2);
		var Pa_2 = getMiddle(PA, PB);
		var Pa_3 = getMiddle(Pc_3, Pc_4);

		// draw the four quadratic subsegments
		mc.curveTo(Pc_1.x, Pc_1.y, Pa_1.x, Pa_1.y);
		mc.curveTo(Pc_2.x, Pc_2.y, Pa_2.x, Pa_2.y);
		mc.curveTo(Pc_3.x, Pc_3.y, Pa_3.x, Pa_3.y);
		mc.curveTo(Pc_4.x, Pc_4.y, P3.x, P3.y);
	}
	
	// return the distance between two points
	private static function distance(P0, P1):Number {
		var dx:Number = 0, dy:Number = 0;
		
		if(P0.x != undefined){
			dx = P0.x - P1.x;
			dy = P0.y - P1.y;
			return Math.sqrt(dx*dx + dy*dy);
		}
		else if(P0.X != undefined) {
			dx = P0.X - P1.X;
			dy = P0.Y - P1.Y;
			return Math.sqrt(dx*dx + dy*dy);
		}
	}

	// return the middle of a segment define by two points
	private static function getMiddle(P0, P1) {
		return {x: ((P0.x + P1.x) / 2), y: ((P0.y + P1.y) / 2)};
	}


	// return a point on a segment [P0, P1] which distance from P0
	// is ratio of the length [P0, P1]
	private static function getPointOnSegment(P0, P1, ratio) {
		return {x: (P0.x + ((P1.x - P0.x) * ratio)), y: (P0.y + ((P1.y - P0.y) * ratio))};
	}
	
	public static function DrawArcFrom3Points(mc, ax ,ay ,bx ,by ,cx ,cy) {

	var x0:Number = ax, x1:Number = bx, x2:Number = cx, X:Number;
	var y0:Number = ay, y1:Number = by, y2:Number = cy, Y:Number;
	var m_ptCenter = new Object({X:Number,Y:Number});
	var radius:Number = 0;
	if (x1 == x0)
	{
		// renumerating
		x1 = ax;
		x2 = bx;
		x0 = cx;
		y1 = ay;
		y2 = by;
		y0 = cy;
	}
	Y = 2 * ((y1 - y2) * (x1 - x0) - (y0 - y1) * (x2 - x1));

	Y = ((y2 + y1) * (y1 - y2) * (x1 - x0) - (y1 + y0) * (y0 - y1) * (x2 - x1) + (x0 - x2) * (x1 - x0) * (x2 - x1)) / Y;
	X = (Y - 0.5 * (y1 + y0)) * (y0 - y1) / (x1 - x0) + 0.5 * (x1 + x0);
	m_ptCenter.X = X;
	m_ptCenter.Y = Y;
	radius = distance(m_ptCenter, {X : ax, Y : ay});
	if(radius > 5000 || isNaN(radius)) {
		mc.moveTo(ax, ay);
		mc.lineTo(bx,by);
		mc.lineTo(cx,cy);
		return;
	}
	//VectorF vec0(m_pptArc[0] - m_ptCenter), vec1(m_pptArc[1] - m_ptCenter), vec2(m_pptArc[2] - m_ptCenter);
	var vec0=new Object();
	vec0.X = ax - m_ptCenter.X;
	vec0.Y = ay - m_ptCenter.Y;
	
	var vec1=new Object();
	vec1.X = bx - m_ptCenter.X;
	vec1.Y = by - m_ptCenter.Y;
	
	var vec2=new Object();
	vec2.X = cx - m_ptCenter.X;
	vec2.Y = cy - m_ptCenter.Y;
	
	var m_fStartAngle:Number = M_RAD_2_DEG * Math.atan2(vec0.Y, vec0.X);
	var m_fSafeStartAngle:Number = m_fStartAngle;
	var m_fSweepAngle:Number = M_RAD_2_DEG * Math.atan2(vec2.Y, vec2.X);
	var fMidAngle:Number = M_RAD_2_DEG * Math.atan2(vec1.Y, vec1.X);
	
	m_fSweepAngle -= m_fStartAngle;
	fMidAngle -= m_fStartAngle;
	m_fStartAngle = 0;
	while (m_fSweepAngle <0 || fMidAngle < 0  )	{
		m_fSweepAngle += 360;
		fMidAngle += 360;
		m_fStartAngle += 360;
	}
	while(m_fStartAngle > m_fSweepAngle)
	{
		m_fSweepAngle += 360;
		fMidAngle += 360;
	}
	while(m_fSweepAngle < fMidAngle){
		m_fSweepAngle += 360;
	}
	m_fSweepAngle -= m_fStartAngle;
	fMidAngle -= m_fStartAngle;
	m_fStartAngle = 0;
	while(m_fSweepAngle - m_fStartAngle > 360){
		m_fSweepAngle -=360;
	}
	while(fMidAngle - m_fStartAngle > 360){
		fMidAngle -=360;
	}
	if(fMidAngle > m_fSweepAngle)
		fMidAngle -= 360;
	m_fSweepAngle += m_fSafeStartAngle;
	fMidAngle += m_fSafeStartAngle;
	m_fStartAngle += m_fSafeStartAngle;
	
	
	if(Math.abs(m_fSweepAngle - fMidAngle) + Math.abs(fMidAngle-m_fStartAngle) >= 359 || 
	(fMidAngle < m_fStartAngle || fMidAngle > m_fSweepAngle)){
		trace('Achtunk!!!!!!!');
		
		var tmp:Number = m_fStartAngle;
		m_fStartAngle = m_fSweepAngle;
		m_fSweepAngle = tmp;
		
	}

			DrawArc(mc, m_ptCenter.X , m_ptCenter.Y, radius, m_fStartAngle, fMidAngle);
			DrawArc(mc, m_ptCenter.X , m_ptCenter.Y, radius, fMidAngle, m_fSweepAngle);

		
	}
	
	public static function DrawArc (mc, x, y, radius, bA, eA) {
		if (eA < bA) eA += 360;
		var r = radius;
		var degToRad = Math.PI / 180 ;
		var n = Math.ceil((eA - bA) / 45);
		var theta = ((eA - bA) / n) * degToRad;
		var cr = radius/Math.cos(theta/2);
		var angle = bA*degToRad;
		var cangle = angle-theta/2;
		mc.moveTo(x+r*Math.cos(angle), y+r*Math.sin(angle));
		for (var i=0;i < n;i++) 
		{
			angle += theta;
			cangle += theta;
			var endX = r*Math.cos (angle);
			var endY = r*Math.sin (angle);
			var cX = cr*Math.cos (cangle);
			var cY = cr*Math.sin (cangle);
			mc.curveTo(x+cX,y+cY, x+endX,y+endY);
		}
		trace("Exit");

	}	
	public static function GetCenterFor3Points( ax ,ay ,bx ,by ,cx ,cy):Object {
		_global.tr("GetCenterFor3Points ax="+ax+", ay="+ay+",bx="+bx+",by="+by+",cx="+cx+",cy="+cy);
		var x0:Number = ax, x1:Number = bx, x2:Number = cx, X:Number;
		var y0:Number = ay, y1:Number = by, y2:Number = cy, Y:Number;
		var m_ptCenter = new Object({x:Number,y	:Number});
		var radius:Number = 0;
		if (x1 == x0)
		{
			// renumerating
			x1 = ax;
			x2 = bx;
			x0 = cx;
			y1 = ay;
			y2 = by;
			y0 = cy;
		}
		Y = 2 * ((y1 - y2) * (x1 - x0) - (y0 - y1) * (x2 - x1));
		_global.tr("before Y="+Y);
		if(Y == 0)
			Y = 0.01;
		
		Y = ((y2 + y1) * (y1 - y2) * (x1 - x0) - (y1 + y0) * (y0 - y1) * (x2 - x1) + (x0 - x2) * (x1 - x0) * (x2 - x1)) / Y;
		X = (Y - 0.5 * (y1 + y0)) * (y0 - y1) / (x1 - x0) + 0.5 * (x1 + x0);
		_global.tr("after Y="+Y+", X="+X);
		m_ptCenter.x = X;
		m_ptCenter.y = Y;
		return m_ptCenter;
	}
}