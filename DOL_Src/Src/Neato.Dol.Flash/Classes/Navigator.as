﻿class Navigator extends mx.core.UIObject {
	private var btnShow:CtrlLib.ButtonEx;
	private var btnHide:CtrlLib.ButtonEx;
	private var mcNavigator:MovieClip;
	private var maxWidth:Number;
	private var maxHeight:Number;
	private var multiLabelModeValue:Boolean;
	
	function Navigator() {
		if (this instanceof Navigator) {
			//_global.tr("Navigator() this instanceof LabelSwitcher");
			_global.Navigator = this;
		}
		multiLabelModeValue = true;
	}
	
	public function set Visible(value:Boolean):Void {
		this._visible = multiLabelModeValue && value;
	}
	
	public function DataBind():Void {
		multiLabelModeValue = _global.Project.CurrentPaper.Faces.length > 1;
		if (multiLabelModeValue) {
			Draw();
			this._visible = true;
		} else
			this._visible = false;
	}

	private function InitLocale():Void {
		_global.LocalHelper.LocalizeInstance(btnShow, "Navigator", "IDS_BTNSHOW");
		_global.LocalHelper.LocalizeInstance(btnHide, "Navigator", "IDS_BTNHIDE");
	}
	
	private function onLoad() {
		maxWidth = 186;
		maxHeight = 200;
		
		btnShow.RegisterOnPressHandler(this, btnShow_OnClick);
		btnHide.RegisterOnPressHandler(this, btnHide_OnClick);
		
		_global.FaceHelper.RegisterOnFaceChangedHandler(this, Draw);

		Show();
		InitLocale();
		DataBind();
	}
	
	public function Hide():Void {
		SetMode(true);
	}
	
	public function Show():Void {
		SetMode(false);
	}
	
	private function SetMode(hidden:Boolean):Void {
		btnShow._visible = hidden;
		btnHide._visible = !hidden;
		mcNavigator._visible = !hidden;
	}
	
	private function btnShow_OnClick(eventObject):Void {
		Show();
	}
	
	private function btnHide_OnClick(eventObject):Void {
		Hide();
	}
	
	function ChangeFace(index:Number){
		_global.FaceHelper.ChangeFace(index);
	}
	
	function Draw():Void {
		mcNavigator.clear();
		var w:Number = _global.Project.CurrentPaper.Width;
		var h:Number = _global.Project.CurrentPaper.Height;
		_global.PaperHelper.DrawContours(mcNavigator, this, ChangeFace, false, true, 
									_global.Project.CurrentPaper.Faces, w, h, false, true, false);
						
		var kx:Number = maxWidth / mcNavigator._width;
		var ky:Number = maxHeight / mcNavigator._height;
		var k = kx <= ky ? kx : ky;
		mcNavigator._xscale = mcNavigator._yscale = k * 100;

		mcNavigator.lineStyle(2 / k, 0x999999);
		mcNavigator.moveTo(0, 0);
		mcNavigator.lineTo(0, h);
		mcNavigator.lineTo(w, h);
		mcNavigator.lineTo(w, 0);
		mcNavigator.lineTo(0, 0);
		
		mcNavigator._x = (maxWidth - mcNavigator._width + 4) / 2;
		
	}
}