﻿import flash.display.BitmapData;
import flash.filters.ColorMatrixFilter;
import flash.geom.ColorTransform;
import flash.geom.Rectangle;
import flash.geom.Matrix;
import flash.geom.Point;

class ImageUnit extends MoveableUnit {
	private var url:String;
	private var mclListener:Object;
	private var image_mcl:MovieClipLoader;
	
	private var id:String;
	private var unitId:Number;
	/*private var effectContrast:Number = 0;
	private var effectBrightness:Number = 0;
	private var effectBlurSharpen:Number = 0;
	private var effectImage:String = "Normal";*/
	private var effectContrast:Number = 0;
	private var effectBrightness:Number = 0;
	private var effectBlurSharpen:Number = 0;
	private var effectImage:String = "Normal";

    private static var ContrastEffectKey:String = "contrast";
    private static var BrightnessEffectKey:String = "brigntness";
    private static var BlurSharpnessEffectKey:String = "blursharp";
    private static var ImageEffectKey:String = "effect";
	
	private var urlFmt:String;
	private var templateId;
	private var isNewImage:Boolean = false;
	
	function ImageUnit(mc:MovieClip, node:XML, urlFormat:String) {
		super(mc, node);
		//_global.tr("+++ImageUnit");
		keepProportions = true;
		this.effectContrast = node.attributes.effectContrast;
		if(this.effectContrast==null)this.effectContrast=0;
		
		this.effectBrightness = node.attributes.effectBrightness;
		if(this.effectBrightness==null)this.effectBrightness=0;
		
		this.effectBlurSharpen = node.attributes.effectBlurSharpen;
		if(this.effectBlurSharpen==null)this.effectBlurSharpen=0;
		
		this.effectImage = node.attributes.effectImage;
		if(this.effectImage==null)this.effectImage="Normal";
		
		this.templateId = node.attributes.templateId;
		this.id = node.attributes.id;
		
		if (node.attributes.isNewImage.toLowerCase() == "true") {
			isNewImage = true;
		}
		
		urlFmt = urlFormat;
		url = buildUrl();//urlFormat+node.attributes.id;
		if(SALocalWork.IsLocalWork)
			url = urlFormat;
		
		trace("ImageUnit url="+ url);
	}
	
	public function get Url() : String {
		return url;
	}
	
	function Draw():Void {
		DrawMC(mc);
	}
	
	function DrawMC(mc:MovieClip):Void {
		_global.UIController.ImageStorage.LoadImage(mc, url, false, true, this, LoadComplete);
	}
	
	private function LoadComplete() {
		//_global.tr("ImageUnit.LoadComplete");
		if (isNewImage == true) {
			isNewImage = false;
			_global.Project.CurrentPaper.CurrentFace.SetStartPosition(this);

			var xml:XMLNode = this.GetXmlNode();
			var action:Actions.AddUnitAction = new Actions.AddUnitAction(this, xml); 
			_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
			mc.cacheAsBitmap = true; 
		}
		if (_global.Project.CurrentUnit == this) {
			_global.Project.CurrentPaper.CurrentFace.ChangeCurrentUnit(this);
			_global.Project.CurrentUnit = this;
			_global.UIController.FramesController.attach(this.frameLinkageName,true);
			_global.UIController.OnUnitChanged();
		}
	}

	function GetXmlNode():XMLNode {
		var node:XMLNode = super.GetXmlNode();
		node.nodeName = "Image";
		node.attributes.effectContrast = this.effectContrast;
		node.attributes.effectBrightness = this.effectBrightness;
		node.attributes.effectBlurSharpen = this.effectBlurSharpen;
		node.attributes.effectImage = this.effectImage;
		node.attributes.unitId = this.unitId;


		return node;
	}

	function getMode():String {
		return _global.ImageEditMode;
	}
	

	private function Resize(kx:Number, ky:Number) {
		this.ScaleX = this.ScaleX * kx;
		this.ScaleY = this.ScaleY * ky;
	}
	
	public function ApplyEffect(contrast:Number,brightness:Number,blursharp:Number,effect:String) {
		effectImage = effect;
		effectContrast = contrast;
		effectBrightness = brightness;
		effectBlurSharpen = blursharp;
		url = buildUrl();
		Draw();
	}
	public function GetEffect():Object
	{
		var res:Object = new Object({effect:effectImage,
									   contrast:effectContrast,
									   brightness:effectBrightness,
									   blursharp:effectBlurSharpen
										});
		return res;
	}
	private function buildUrl():String{
		if(urlFmt.length<1)
			return "";
		var res:String= urlFmt+id+
				"&"+ContrastEffectKey+"="+effectContrast+
				"&"+BrightnessEffectKey+"="+effectBrightness+
				"&"+BlurSharpnessEffectKey+"="+effectBlurSharpen+
				"&"+ImageEffectKey+"="+effectImage;
		if(templateId != undefined)
			res += '&TemplateId='+templateId;
		return res;
				
		
	}
}
