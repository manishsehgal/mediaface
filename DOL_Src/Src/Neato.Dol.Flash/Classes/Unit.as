class Unit {
	public var id:String;
	private var mc:MovieClip;
	private var unitClassName:String;
	private var layoutPosition:Number = undefined;
	
	function Unit(mc:MovieClip, node:XML) {
		this.mc = mc;
		if (node != null) {
			id = node.attributes.id == undefined ? mc._name : String(node.attributes.id);
			this.mc._x = Number(node.attributes.x);
			this.mc._y = Number(node.attributes.y);
		} else {
			id = mc._name;
			this.mc._x = 0;
			this.mc._y = 0;
		}
//		_global.tr("### UNIT ID = " + id);
		unitClassName = "";
		layoutPosition = undefined;
	}
	
	public function get UnitClassName():String {
		return unitClassName;
	}
	
	public function set LayoutPosition(value:Number):Void {
		layoutPosition = value;
	}

	public function get LayoutPosition():Number {
		return layoutPosition;
	}

	public function set X(value:Number):Void {
		mc._x = value;
	}
	
	public function get X():Number {
		return mc._x;
	}
	
	public function set Y(value:Number):Void {
		mc._y = value;
	}
	
	public function get Y():Number {
		return mc._y;
	}
	
	public function get Width():Number {
		return mc._width;
	}
	
	public function get Height():Number {
		return mc._height;
	}
	
	public function GetBounds():Object {
		return mc.getBounds(mc);
	}
	
	function GetXmlNodeBase():XMLNode {
		var node:XMLNode = new XMLNode(1, "BaseNode");
		node.attributes.id = id;
		node.attributes.x = Math.round(this.mc._x*1000)/1000;
		node.attributes.y = Math.round(this.mc._y*1000)/1000;
		return node;
	}
}