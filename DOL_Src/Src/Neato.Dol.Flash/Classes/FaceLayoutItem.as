﻿class FaceLayoutItem {
	public var Id:Number;
	public var FaceLayoutId:Number;
	public var X:Number;
	public var Y:Number;
	public var ScaleX:Number;
	public var ScaleY:Number;
	public var Position:Number; 
	public var Text:String; 
	public var Font:String;
	public var Size:Number; 
	public var Bold:Boolean; 
	public var Italic:Boolean; 
	public var Underline:Boolean; 
	public var Color:Number; 
	public var Angle:Number;
	public var Height:Number;
	public var Width:Number;
	public var Align:String;
	public var UnitType:String;
	public var Effect:String;
	public var EmptyLayoutItem:Boolean; 
	
	function FaceLayoutItem(){}
}