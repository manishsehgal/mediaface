﻿class SAPaperTracking {
	private static var url:String = "PaperTracking.aspx?paperId=";
	
	public static function TrackPaperChange(paperId:Number) {
		var xml:XML = new XML();
		var currentDate:Date = new Date();
		xml.load(url+paperId+"&time="+currentDate.getTime());
		xml.onLoad = function(success) {
			trace("Paper Change tracked ok");
		};
	}
}