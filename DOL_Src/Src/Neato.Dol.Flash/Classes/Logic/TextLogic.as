﻿class Logic.TextLogic {
	static function ConvertTextToEffect(unit,effect) :Void {
		trace("ConvertTextToEffect");
		
		if(unit instanceof TextEffectUnit )	{
			unit.SetTextEffect(effect);
			unit.Draw();
			return;
		} 
		trace("Unit = "+unit+" mc = "+unit.mc);
		var node:XML = unit.GetXmlNode();
		_global.Project.CurrentPaper.CurrentFace.CreateEffectUnitFromText(node,effect);
	
	}
	static function ConvertEffectToText(unit) : Void {
		if(!(unit instanceof TextEffectUnit))
		{
			trace("ConvertEffectToText: unit not of type TextEffectUnit");
			return;
		}		
		trace("ConvertEffectToText");
		var node:XML = unit.GetXmlNode();
		_global.Project.CurrentPaper.CurrentFace.CreateTextUnitFromEffect(node);
	}	
}