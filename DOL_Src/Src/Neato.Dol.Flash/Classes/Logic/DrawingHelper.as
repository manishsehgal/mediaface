﻿class Logic.DrawingHelper {
	public static function CreatePrimitive(node:XMLNode):IDraw {
		switch(node.nodeName) {
			case "Rectangle":
				return new RectanglePrimitive(node);
				break;
			case "Circle":
				return new CirclePrimitive(node);
				break;
			case "GradientCircle":
				return new GradientCirclePrimitive(node);
				break;
			case "GradientLine":
				return new GradientLinePrimitive(node);
				break;
			case "GradientRect":
				return new GradientRectPrimitive(node);
				break;
			case "MoveTo":
				return new MoveToCommand(node);
				break;
			case "LineTo":
				 return new LineToCommand(node);
				break;
			case "CurveTo":
				return new CurveToCommand(node);
				break;
			case "CubicCurveTo":
				return new CubicCurveToCommand(node);
				break;
			case "Ellipse":
				return new EllipsePrimitive(node);
				break;
			case "CutLine":
				return new CutLinePrimitive(node);
				break;
			default:
				trace("Unknown primitive name! " + node.nodeName);
				return null;
		}
	}

	public static function DrawSequence(iDrawSequence:Array, mc:MovieClip, x:Number, y:Number, rotation:Number, color:Number, color2:Number, thickness:Number, filled:Boolean):Void {
		mc.lineStyle(thickness, color, 100);
		mc.color = color;
		mc.color2 = color2;
		if (filled) mc.beginFill(color, 100);
		for (var i = 0; i < iDrawSequence.length; ++i) {
			var primitive:IDraw = iDrawSequence[i];
			primitive.Draw(mc, x, y, rotation);
		}
		if (filled) mc.endFill();
		mc.moveTo(0,0);
	}
}