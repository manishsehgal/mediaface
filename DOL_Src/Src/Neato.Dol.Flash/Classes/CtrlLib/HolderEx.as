﻿ import mx.utils.Delegate;
[TagName("ImageHolder")]
[Event("click")]
[Event("doubleclick")]
[Event("contentLoaded")]
class CtrlLib.HolderEx extends mx.core.UIComponent {
	static var symbolName:String = "HolderEx";
	static var symbolOwner:Object = Object(CtrlLib.HolderEx);
	var className:String = "HolderEx";
	static var serializeDataBinds:Boolean = false;

	private var boundingBox_mc:MovieClip;
	
	private var ctlSkin:MovieClip;

	[Inspectable(defaultValue="ImageHolderTrueUpSkin")]
	var trueUpSkin:String = "ImageHolderTrueUpSkin";

	[Inspectable(defaultValue="ImageHolderTrueOverSkin")]
	var trueOverSkin:String = "ImageHolderTrueOverSkin";

	[Inspectable(defaultValue="ImageHolderTrueDownSkin")]
	var trueDownSkin:String = "ImageHolderTrueDownSkin";

	[Inspectable(defaultValue="ImageHolderTrueDisabledSkin")]
	var trueDisabledSkin:String = "ImageHolderTrueDisabledSkin";

	[Inspectable(defaultValue="ImageHolderFalseUpSkin")]
	var falseUpSkin:String = "ImageHolderFalseUpSkin";

	[Inspectable(defaultValue="ImageHolderFalseOverSkin")]
	var falseOverSkin:String = "ImageHolderFalseOverSkin";

	[Inspectable(defaultValue="ImageHolderFalseDownSkin")]
	var falseDownSkin:String = "ImageHolderFalseDownSkin";

	[Inspectable(defaultValue="ImageHolderFalseDisabledSkin")]
	var falseDisabledSkin:String = "ImageHolderFalseDisabledSkin";

	private var selectedValue:Boolean;
    [Inspectable(defaultValue="false")]
	public function get Selected():Boolean {
		return selectedValue;
	}

	public function set Selected(selected:Boolean):Void {
		selectedValue = selected;
		ChangeSkin();
	}

	private var isOver:Boolean = false;
	private var isPress:Boolean = false;

	function HolderEx() {
	}

	function init():Void {
		super.init();

		isOver = false;
		isPress = false;
		ChangeSkin();

		if (boundingBox_mc != undefined) {
			boundingBox_mc._width = 0;
			boundingBox_mc._height = 0;
			boundingBox_mc._visible = false;
		}
	}
	
	//region Data Binding
	private var dataSourceValue:Object;
    [Inspectable(defaultValue="")]
	public function get DataSource():Object {
		return dataSourceValue;
	}
	public function set DataSource(data:Object):Void {
		dataSourceValue = data;
	}
	//endregion Data Binding

	function size():Void {
		super.size();
		var width = this.width;
		var height = this.height;

		ctlSkin._xscale = 100;
		ctlSkin._yscale = 100;

		var xscale = width / ctlSkin._width * 100;
		var yscale = height / ctlSkin._height * 100;

		ctlSkin._xscale = xscale;
		ctlSkin._yscale = yscale;

		ctlSkin._x = 0;
		ctlSkin._y = 0;

		var bounds = ctlSkin.getBounds(ctlSkin._parent);
		ctlSkin._x = (width - ctlSkin._width) / 2 - bounds.xMin;
		ctlSkin._y = (height - ctlSkin._height) / 2 - bounds.yMin;
	}

	private function onRollOver() {	
		isOver = true;
		isPress = false;
		ChangeSkin();
	}

	private function onRollOut() {	
		isOver = false;
		isPress = false;
		ChangeSkin();
	}

	private function onRelease() {
		StartDetectClicks();
		isOver = true;
		isPress = false;
		ChangeSkin();
	}

	private function onReleaseOutside() {	
		isOver = false;
		isPress = false;
		ChangeSkin();
	}

	private function onPress() {
		isOver = true;
		isPress = true;
		ChangeSkin();
	}

	private function onContentLoaded() {
		var eventObject = {type:"contentLoaded", target:this};
		this.dispatchEvent(eventObject);
	}

	private function onClick() {
		var eventObject = {type:"click", target:this};
		this.dispatchEvent(eventObject);
	}

	private function onDoubleClick() {
		var eventObject = {type:"doubleclick", target:this};
		this.dispatchEvent(eventObject);
	}

	private var clickCount:Number = 0;
	private var doubleClickInterval:Number = 0;
	private function StartDetectClicks() {
		clickCount++;
		if (doubleClickInterval == 0) {
			doubleClickInterval = setInterval(this, "DetectClicks", 250);
		}
	}	
	private function DetectClicks() {
		clearInterval(doubleClickInterval);
		doubleClickInterval = 0;
		if (clickCount > 1) {
			onDoubleClick();
		} else if (clickCount == 1) {
			onClick();
		}
		clickCount = 0;
	}	

	public function RegisterOnContentLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("contentLoaded", Delegate.create(scopeObject, callBackFunction));
    }

	public function UnregisterOnContentLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.removeEventListener("contentLoaded", Delegate.create(scopeObject, callBackFunction));
    }

	public function RegisterOnClickHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("click", Delegate.create(scopeObject, callBackFunction));
    }

	public function RegisterOnDoubleClickHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("doubleclick", Delegate.create(scopeObject, callBackFunction));
    }

	private function ChangeSkin() {
		if (!this.enabled) {
			if (!selectedValue) {
				ctlSkin = setSkin(1, trueDisabledSkin);
			} else {
				ctlSkin = setSkin(1, falseDisabledSkin);
			}
		} else if (isPress) {
			if (!selectedValue) {
				ctlSkin = setSkin(1, trueDownSkin);
			} else {
				ctlSkin = setSkin(1, falseDownSkin);
			}

		} else if (!isOver) {
			if (!selectedValue) {
				ctlSkin = setSkin(1, trueUpSkin);
			} else {
				ctlSkin = setSkin(1, falseUpSkin);
			}
		} else {
			if (!selectedValue) {
				ctlSkin = setSkin(1, trueOverSkin);
			} else {
				ctlSkin = setSkin(1, falseOverSkin);
			}
		}
		size();
	}
}