﻿import mx.utils.Delegate;
[Event("tabClick")]
[Event("itemDataBound")]
[Event("load")]
class CtrlLib.AccordionEx extends mx.containers.Accordion {
	
	[Inspectable(defaultValue="")]
	var falseUpSkin:String = "";
	
	[Inspectable(defaultValue="")]
	var falseDownSkin:String = "";
	
	[Inspectable(defaultValue="")]
	var falseOverSkin:String = "";
	
	[Inspectable(defaultValue="")]
	var falseDisabled:String = "";

	[Inspectable(defaultValue="")]
	var trueUpSkin:String = "";
	
	[Inspectable(defaultValue="")]
	var trueDownSkin:String = "";
	
	[Inspectable(defaultValue="")]
	var trueOverSkin:String = "";
	
	[Inspectable(defaultValue="")]
	var trueDisabled:String = "";
	
	[Inspectable(defaultValue=22)]
	var headerHeight:Number = 22;
	
	private var childNames:Array;
	public function get ChildNames():Array {
		return childNames;
	}
	public function set ChildNames(value:Array):Void {
		childNames = value;
	}
	
	private var childLabels:Array;
	public function get ChildLabels():Array {
		return childLabels;
	}
	public function set ChildLabels(value:Array):Void {
		childLabels = value;
	}
	
	private var childIcons:Array;
	public function get ChildIcons():Array {
		return childIcons;
	}
	public function set ChildIcons(value:Array):Void {
		childIcons = value;
	}
	private var childVisibility:Array;
	public function get ChildVisibility():Array {
		return childVisibility;
	}
	public function set ChildVisibility(value:Array):Void {
		childVisibility = value;
	}
	
	function AccordionEx() {
		super();
		trace("AccordionEx ctr");
		setStyle("color", 0x00);
		setStyle("backgroundColor", 0xFFFFFF);
		setStyle("headerHeight", headerHeight);
		//_global.styles.AccordionHeader.setStyle("fontSize", 14);
		//_global.styles.AccordionHeader.setStyle("fontStyle", "normal");
	}
	
	public function RegisterOnTabClickHandler(scopeObject:Object, callBackFunction:Function) : Void {
		trace("RegisterOnTabClickHandler");
		this.addEventListener("change", Delegate.create(scopeObject, callBackFunction));
    }

	public function RegisterOnItemDataBoundHandler(scopeObject:Object, callBackFunction:Function) : Void {
		trace("RegisterOnTabClickHandler");
		this.addEventListener("itemDataBound", Delegate.create(scopeObject, callBackFunction));
    }
	
	private function OnItemDataBound(mc:MovieClip, index:Number) : Void {
		var eventObject:Object = {type:"itemDataBound", target:mc, itemIndex:index};
		this.dispatchEvent(eventObject);
	}

	public function DataBind():Void {
		for (var i:Number = 0; i < ChildNames.length; ++i) {
			trace('childVisibility[i] ='+childVisibility[i]);
			var mc:MovieClip = null;
			if(childVisibility[i] == true || childVisibility == undefined) {
				mc = this.createSegment("View", ChildNames[i], ChildLabels[i], ChildIcons[i]);				
			}
			OnItemDataBound(mc, i);
			
		}
	}
	
	public function get AllHeadersHeight():Number {
		return (headerHeight -1) * numChildren; 
	}
}