﻿import mx.utils.Delegate;
[TagName("SignImageHolderEx")]
[Event("click")]
[Event("doubleclick")]
[Event("contentLoaded")]
class CtrlLib.SignImageHolderEx extends CtrlLib.HolderEx {
	static var symbolName:String = "SignImageHolderEx";
	static var symbolOwner:Object = Object(CtrlLib.SignImageHolderEx);
	var className:String = "SignImageHolderEx";
	var serializeDataBinds:Boolean = true;

	private var ctlImage;
	private var ctlLabel;
	
	function createChildren():Void {
		super.createChildren();
		if (ctlImage == undefined) {
			ctlImage =  this.createObject("UIObject", "ctlImage", this.getNextHighestDepth(), new Object());
		}
		if (ctlLabel == undefined) {
			this.createTextField("ctlLabel", 3, 0, 0, 100, 30);
			ctlLabel.selectable = false;
			ctlLabel.multiline = true;
			ctlLabel.wordWrap = true;
			ctlLabel.autoSize = true;
		}
	}

	//region Data Binding
	public function DataBind() {
		_global.UIController.ImageStorage.LoadImage(ctlImage, DataSource.url, false, false, this, ctlImage_OnComplete);
		ctlLabel.visible = false;
		ctlLabel.htmlText = DataSource.name;

		ctlLabel._width = this.width * 0.9;
		var textFormat:TextFormat = new TextFormat();
		textFormat.font = "_sans";
		textFormat.size = 10;
		textFormat.color = 0x0;
		textFormat.align = "center";
		ctlLabel.setTextFormat(textFormat);

		var iconSize:Number = 80;
		ctlLabel.move(this.width * 0.1 / 2, iconSize + (this.height - iconSize - ctlLabel._height)/2);
	}
	//endregion Data Binding

	function size():Void {
		super.size();
		if (ctlImage == undefined || ctlLabel == undefined) return;
		//_global.tr("SignImageHolderEx.Size() mc = " + this._name);

		ctlLabel._width = this.width * 0.9;
		var textFormat:TextFormat = new TextFormat();
		textFormat.font = "_sans";
		textFormat.size = 10;
		textFormat.color = 0x0;
		textFormat.align = "center";
		ctlLabel.setTextFormat(textFormat);

		var iconSize:Number = 80;
		ctlImage.move((this.width -  ctlImage.width) / 2, 8);
		ctlLabel.move(this.width * 0.1 / 2, iconSize + (this.height - iconSize - ctlLabel._height)/2);
		ctlLabel.visible = true;
	}

	private function ctlImage_OnComplete(eventObject) {
		//_global.tr("ctlImage_OnComplete mc = " + this._name);
		eventObject.target.UnregisterOnImageAttachedHandler(this);
		size();
		this.doLater(this, "onContentLoaded");
	}
}