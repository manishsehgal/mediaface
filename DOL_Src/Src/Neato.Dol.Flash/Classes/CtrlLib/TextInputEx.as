﻿import mx.utils.Delegate;
class CtrlLib.TextInputEx extends mx.controls.TextInput {
	public function RegisterOnChangeHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("change", Delegate.create(scopeObject, callBackFunction));
    }

	public function RegisterOnEnterHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("enter", Delegate.create(scopeObject, callBackFunction));
    }

	public function RegisterOnFocusInHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("focusIn", Delegate.create(scopeObject, callBackFunction));
    }

	public function RegisterOnFocusOutHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("focusOut", Delegate.create(scopeObject, callBackFunction));
    }
}