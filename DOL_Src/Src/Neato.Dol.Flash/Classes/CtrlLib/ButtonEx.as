﻿import mx.utils.Delegate;
[Event("press")]
[Event("rollOver")]
[Event("rollOut")]
class CtrlLib.ButtonEx extends mx.controls.Button {
	
	[Inspectable(defaultValue="")]
	var cmdId:String = "cmdId";
	
	[Inspectable(defaultValue="ButtonTrueUpSkin")]
	var trueUpSkin:String = "ButtonTrueUpSkin";

	[Inspectable(defaultValue="ButtonTrueOverSkin")]
	var trueOverSkin:String = "ButtonTrueOverSkin";

	[Inspectable(defaultValue="ButtonTrueDownSkin")]
	var trueDownSkin:String = "ButtonTrueDownSkin";

	[Inspectable(defaultValue="ButtonTrueDisabledSkin")]
	var trueDisabledSkin:String = "ButtonTrueDisabledSkin";

	[Inspectable(defaultValue="ButtonFalseUpSkin")]
	var falseUpSkin:String = "ButtonFalseUpSkin";

	[Inspectable(defaultValue="ButtonFalseOverSkin")]
	var falseOverSkin:String = "ButtonFalseOverSkin";

	[Inspectable(defaultValue="ButtonFalseDownSkin")]
	var falseDownSkin:String = "ButtonFalseDownSkin";

	[Inspectable(defaultValue="ButtonFalseDisabledSkin")]
	var falseDisabledSkin:String = "ButtonFalseDisabledSkin";

	[Inspectable(defaultValue=false)]
	var centerContent:Boolean = false;
	
	[Inspectable(defaultValue=true)]
	var useHandCursor:Boolean = true;
	
	[Inspectable(defaultValue=false)]
	var autoRepeat:Boolean = false;
	
	[Inspectable(defaultValue=50)]
	var speedRepeat:Number = 50;
	
	var intRepeat   : Number = 0;
	var countRepeat : Number = 0;
	
	public function get CmdId():String {
		return this.cmdId;
	}
	public function get CountRepeat():Number {
		return this.countRepeat;
	}
	public function set CountRepeat(val:Number) {
		this.countRepeat = val;
	}
	
	function ButtonEx() {
		super();
	}
	
	function init(Void):Void {
		var tempUseHandCursor:Boolean = useHandCursor;
		super.init();
		useHandCursor = (tempUseHandCursor == true);
	}
	
	public function RegisterOnRollOverHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("rollOver", Delegate.create(scopeObject, callBackFunction));
    }

	function onRollOver() : Void {
		super.onRollOver.call(this);
		var eventObject:Object = {type:"rollOver", target:this};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnRollOutHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("rollOut", Delegate.create(scopeObject, callBackFunction));
    }

	function onRollOut() : Void {
		super.onRollOut.call(this);
		var eventObject:Object = {type:"rollOut", target:this};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnPressHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("press", Delegate.create(scopeObject, callBackFunction));
    }

	function onPress() : Void {
		super.onPress.call(this);
		if (autoRepeat == true) {
			StartRepeat();
		} else {
			var eventObject:Object = {type:"press", target:this};
			this.dispatchEvent(eventObject);
		}
	}

	public function RegisterOnReleaseHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("release", Delegate.create(scopeObject, callBackFunction));
    }

	function onRelease() : Void {
		super.onRelease.call(this);
		if (autoRepeat == true) {
			StopRepeat();
		} else {
			var eventObject:Object = {type:"release", target:this};
			this.dispatchEvent(eventObject);
		}
	}

	public function RegisterOnDragOutHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("dragOut", Delegate.create(scopeObject, callBackFunction));
    }

	function onDragOut() : Void {
		super.onDragOut.call(this);
		if (autoRepeat == true) {
			StopRepeat();
		} else {
			var eventObject:Object = {type:"dragOut", target:this};
			this.dispatchEvent(eventObject);
		}
	}

	public function RegisterOnClickHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("click", Delegate.create(scopeObject, callBackFunction));
    }
    
    //--- Repeating ---
    
	function StartRepeat() : Void {
		countRepeat = 0;
		intRepeat = setInterval(Repeat, speedRepeat, this);
	}
	function Repeat(btn:ButtonEx) : Void {
		if (btn.CountRepeat > 4) {
			var eventObject:Object = {type:"click", target:btn};
			btn.dispatchEvent(eventObject);
		}
		btn.CountRepeat++;
	}
	function StopRepeat() : Void {
		clearInterval(intRepeat);
		countRepeat = 0;
	}
}