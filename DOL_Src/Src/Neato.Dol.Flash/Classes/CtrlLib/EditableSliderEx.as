﻿class CtrlLib.EditableSliderEx extends CtrlLib.SliderEx {
	private var txtValue:CtrlLib.TextInputEx;
	private var lblMM:CtrlLib.LabelEx;
	
	public function EditableSliderEx() {
		super();
	}
	
	function onLoad() {
		txtValue.RegisterOnFocusOutHandler(this, ValueChange);
		txtValue.RegisterOnEnterHandler(this, ValueChange);
		this.RegisterOnChangeHandler(this, SynchronizeValue);
		lblValue.visible = false;
		SynchronizeValue();
		txtValue.restrict = "0-9.\\-";
		this.setStyle("fontSize", "10");
	}
	
	private function ValueChange(eventObject) {
		var value = parseFloat(txtValue.label.text);
		if(!isNaN(value)) {
			var oldValue:Number = Points;
			if (value > this.Max) {
				value = this.Max;
			}
			else if(value < this.Min) {
				value = this.Min;
			}
			Points = value;
		}
		OnChange(oldValue, Points);
	}
	
	public function SetPoints(value:Number):Void {
		super.SetPoints(value);
		txtValue.text = shiftMM.toString();
		lblMM.text = " mm";
	}
	
	private function SynchronizeValue(eventObject) {
		txtValue.text = shiftMM.toString();
		lblMM.text = " mm";
	}
}