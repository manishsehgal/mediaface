﻿import mx.core.UIObject;
import mx.utils.Delegate;

class CtrlLib.TreeEx extends mx.controls.Tree {
    function TreeEx() {
        super();
        vScrollPolicy = "auto";
		setStyle("indentation", 10);
		setStyle("disclosureClosedIcon", "EmptyContent");
		setStyle("disclosureOpenIcon", "EmptyContent");
		setStyle("folderClosedIcon", "TreeFolderClosed");
		setStyle("folderOpenIcon", "TreeFolderOpen");
    }

	public function RegisterOnChangeHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("change", Delegate.create(scopeObject, callBackFunction));
    }

	public function RegisterOnNodeOpenHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("nodeOpen", Delegate.create(scopeObject, callBackFunction));
    }

	public function RegisterOnNodeCloseHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("nodeClose", Delegate.create(scopeObject, callBackFunction));
    }
}
