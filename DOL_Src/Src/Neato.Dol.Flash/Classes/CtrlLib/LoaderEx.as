﻿import mx.utils.Delegate;
[TagName("LoaderEx")]
class CtrlLib.LoaderEx extends mx.controls.Loader {
	function LoaderEx() {
	}

	public function RegisterOnCompleteHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("complete", Delegate.create(scopeObject, callBackFunction));
    }
}