﻿import mx.utils.Delegate;
[Event("click")]
[Event("rollOver")]
[Event("rollOut")]
class CtrlLib.CheckBoxEx extends mx.controls.CheckBox {
	[Inspectable(defaultValue="CheckBoxTrueUpSkin")]
	var trueUpSkin:String = "CheckBoxTrueUpSkin";

	[Inspectable(defaultValue="CheckBoxTrueOverSkin")]
	var trueOverSkin:String = "CheckBoxTrueOverSkin";

	[Inspectable(defaultValue="CheckBoxTrueDownSkin")]
	var trueDownSkin:String = "CheckBoxTrueDownSkin";

	[Inspectable(defaultValue="CheckBoxTrueDisabledSkin")]
	var trueDisabledSkin:String = "CheckBoxTrueDisabledSkin";

	[Inspectable(defaultValue="CheckBoxFalseUpSkin")]
	var falseUpSkin:String = "CheckBoxFalseUpSkin";

	[Inspectable(defaultValue="CheckBoxFalseOverSkin")]
	var falseOverSkin:String = "CheckBoxFalseOverSkin";

	[Inspectable(defaultValue="CheckBoxFalseDownSkin")]
	var falseDownSkin:String = "CheckBoxFalseDownSkin";

	[Inspectable(defaultValue="CheckBoxFalseDisabledSkin")]
	var falseDisabledSkin:String = "CheckBoxFalseDisabledSkin";

	public function CheckBoxEx () {
		super();
	}
	
	public function RegisterOnRollOverHandler (scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener ("rollOver", Delegate.create (scopeObject, callBackFunction));
	}
	
	function onRollOver () : Void {
		super.onRollOver.call (this);
		var eventObject:Object = {type:"rollOver", target:this};
		this.dispatchEvent (eventObject);
	}
	
	public function RegisterOnRollOutHandler (scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener ("rollOut", Delegate.create (scopeObject, callBackFunction));
	}
	
	function onRollOut () : Void {
		super.onRollOut.call (this);
		var eventObject:Object = {type:"rollOut", target:this};
		this.dispatchEvent (eventObject);
	}
	
	public function RegisterOnClickHandler (scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener ("click", Delegate.create (scopeObject, callBackFunction));
	}
}
