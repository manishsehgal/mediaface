﻿import mx.utils.Delegate;
import mx.skins.RectBorder;
import mx.managers.DepthManager;
[Event("apply")]
[Event("change")]
class CtrlLib.HListEx extends mx.core.UIComponent {
    static var symbolName:String = "HListEx";
    static var symbolOwner:Object = Object(CtrlLib.HListEx);
    var className:String = "HListEx";

	private var createClassChildAtDepth:Function;

    private var listItems:Array;
    private var ctlPane:CtrlLib.ScrollPaneEx;

    private var itemHeightValue = 55;
    [Inspectable(defaultValue=55, type="Number")]
	public function get ItemHeight():Number {
		return itemHeightValue;
	}
	public function set ItemHeight(data:Number):Void {
		itemHeightValue = data;
	}

    private var itemWidthValue = 55;
    [Inspectable(defaultValue=55, type="Number")]
	public function get ItemWidth():Number {
		return itemWidthValue;
	}
	public function set ItemWidth(data:Number):Void {
		itemWidthValue = data;
	}


    private var xGapValue:Number = 3;
    [Inspectable(defaultValue=3, type="Number")]
	public function get XGap():Number {
		return xGapValue;
	}
	public function set XGap(data:Number):Void {
		xGapValue = data;
	}

    private var yGapValue:Number = 3;
    [Inspectable(defaultValue=3, type="Number")]
	public function get YGap():Number {
		return yGapValue;
	}
	public function set YGap(data:Number):Void {
		yGapValue = data;
	}

    function HListEx() {
		super();
    }

    function init() {
        super.init();
		if (ctlPane != undefined) {
            ctlPane.setStyle("borderColor", 0xcccccc);
			ctlPane.setStyle("borderStyle", "solid");
			ctlPane.setStyle("backgroundColor", "0xf7f7f7");
        }
    }

    function createChildren():Void {
        super.createChildren();

		if (listItems == undefined) {
            listItems = new Array();
        }
    }

    function size():Void {
		super.size();
        ctlPane.setSize(width, height);
        ctlPane.move(0, 0);
		
        var columnNumber = Math.floor((ctlPane.width - xGapValue) / (itemWidthValue + xGapValue));
        var columnIndex:Number = 0;
        var rowIndex:Number = 0;
        for (var i:Number = 0; i < listItems.length; i++) {
            if (columnIndex >= columnNumber) {
                rowIndex++;
                columnIndex = 0;
            }

            var x:Number = xGapValue + columnIndex * (xGapValue + itemWidthValue);
            var y:Number = rowIndex * (yGapValue + itemHeightValue);
            listItems[i].move(x, y);
            listItems[i].setSize(itemWidthValue, itemHeightValue);
            
            columnIndex++;
        }
		
		var rowsNumber:Number = rowIndex + 1;
		var itemsHeight = rowsNumber * (yGapValue + itemHeightValue);
		if (itemsHeight > this.height) {
			ctlPane.vScrollPolicy = "on";
		} else {
			ctlPane.vScrollPolicy = "off";
		}
    }

	private function onLoad() {
		DataBind();
	}

	//region Data Binding
	private var dataSourceValue:Array;
	public function get DataSource():Array {
		return dataSourceValue;
	}
	public function set DataSource(data:Array):Void {
		dataSourceValue = data;
	}

	private var itemLinkageNameValue:String = "";
    [Inspectable(defaultValue="", type="String")]
	public function get ItemLinkageName():String {
		return itemLinkageNameValue;
	}
	public function set ItemLinkageName(data:String):Void {
		itemLinkageNameValue = data;
	}

	public function Clear() {
		for (var index = 0; index < listItems.length; index++) {
			ctlPane.content.destroyObject(listItems[index]._name);
		}
		listItems.length = 0;
	}
	
	public function DataBind() {
		trace("HListEx.DataBind begin: " + dataSourceValue.length);

		for (var index = 0; index < listItems.length; index++) {
			ctlPane.content.destroyObject(listItems[index]._name);
		}
		listItems.length = 0;
		var firstSerializedDataBindItem = undefined;
		for (var index = 0; index < dataSourceValue.length; index++) {
			var item = ctlPane.content.createObject(itemLinkageNameValue, "ctlItem" + index.toString(), ctlPane.content.getNextHighestDepth());

			item.DataSource = dataSourceValue[index];

			item.RegisterOnClickHandler(this, Item_OnClick);
			item.RegisterOnDoubleClickHandler(this, Item_OnDoubleClick);

			listItems[index] = item;
			
			if (item.serializeDataBinds) {
				item.RegisterOnContentLoadedHandler(this, Item_OnContentLoaded);
				if (firstSerializedDataBindItem==undefined) firstSerializedDataBindItem = item;
			} else {
				item.DataBind();
			}
		}
		
		if (firstSerializedDataBindItem!=undefined) firstSerializedDataBindItem.DataBind();

		SelectedItemIndex = undefined;
		size();
	} 
	
	public function RebindItem(itemNumber:Number) {
		listItems[itemNumber].DataSource = dataSourceValue[itemNumber];
		listItems[itemNumber].DataBind();
	}
	
	public function Item_OnContentLoaded(eventObject) {
		var item = eventObject.target;
		item.UnregisterOnContentLoadedHandler(this, Item_OnContentLoaded);
		for (var index = 0; index < listItems.length - 1 ; index++) {
			if (listItems[index] == item) {
				for (var index2 = index+1; index2 < listItems.length; index2++) {
					if (listItems[index2].serializeDataBinds) {
						listItems[index2].DataBind();
						listItems[index2].invalidate();
						return;
					}
				}
			}
		}		
	}
	//endregion Data Binding
    private var selectedItemIndexValue:Number = undefined;
    [Inspectable(defaultValue=undefined, type="Number")]
	public function get SelectedItemIndex():Number {
		return selectedItemIndexValue;
	}
	public function set SelectedItemIndex(index:Number):Void {
		ChangeSelectedItemIndex(index);
		OnChange();
	}

	private function ChangeSelectedItem(item) {
		selectedItemIndexValue = undefined;
		for (var index in listItems) {
			if (listItems[index] == item) {
				selectedItemIndexValue = Number(index);
				listItems[index].Selected = true;
			} else {
				listItems[index].Selected = false;
			}
		}
	}

	private function ChangeSelectedItemIndex(itemIndex:Number) {
		selectedItemIndexValue = undefined;
		for (var index in listItems) {
			if (index == itemIndex) {
				selectedItemIndexValue = Number(index);
				listItems[index].Selected = true;
			} else {
				listItems[index].Selected = false;
			}
		}
	}

    private function Item_OnClick(eventObject) {
        ChangeSelectedItem(eventObject.target);
		OnChange();
    }

    private function Item_OnDoubleClick(eventObject) {
        ChangeSelectedItem(eventObject.target);
		OnChange();
		OnApply();
    }

    private function OnChange() {
		var eventObject = {type:"change", target:this, index:selectedItemIndexValue };
		this.dispatchEvent(eventObject);
    }

	public function RegisterOnChangeHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("change", Delegate.create(scopeObject, callBackFunction));
    }
 
    private function OnApply() {
		var eventObject = {type:"apply", target:this, index:selectedItemIndexValue };
		this.dispatchEvent(eventObject);
    }

	public function RegisterOnApplyHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("apply", Delegate.create(scopeObject, callBackFunction));
    } 
}