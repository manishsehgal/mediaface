﻿import mx.utils.Delegate;
[TagName("VectorGraphicsHolder")]
[Event("click")]
[Event("doubleclick")]
[Event("contentLoaded")]
class CtrlLib.DrawingHolderEx extends CtrlLib.HolderEx {
	static var symbolName:String = "DrawingHolderEx";
	static var symbolOwner:Object = Object(CtrlLib.DrawingHolderEx);
	var className:String = "DrawingHolderEx";
	static var serializeDataBinds:Boolean = true;

	private var ctlContent:MovieClip;
	
	function DrawingHolderEx() {
	}

	function init():Void {
		super.init();
	}

	function createChildren():Void {
		super.createChildren();
		if (ctlContent == undefined) {
			ctlContent = this.createEmptyMovieClip("ctlContent", this.getNextHighestDepth());
		}
	}

	//region Data Binding
	public function DataBind() : Void {
		var x:Number = 0;
		var y:Number = 0;
		var rotation:Number = 0;
		var color:Number = 0x000000;
		var color2:Number = 0xffffff;
		var thickness:Number = 1;
		var filled:Boolean = false;
		if (DataSource.type == "brush") {
			trace("brushbrushbrushbrushbrushbrushbrushbrushbrushbrushbrushbrushbrushbrush");
			filled = DataSource.filled;
			TooltipHelper.SetTooltip2(this, DataSource.tipID);
		}
		ctlContent.clear();
		Logic.DrawingHelper.DrawSequence(DataSource.drawSequence, ctlContent, x, y, rotation, color, color2, thickness, filled);
		var layoutItemColor:Number = 0xFF6666;
  		Logic.DrawingHelper.DrawSequence(DataSource.textSequence, ctlContent, x, y, rotation, layoutItemColor, color2, thickness, true);
		size();
		onContentLoaded();
	}
	//endregion Data Binding
	
	function size():Void {
		super.size();
		var width:Number = this.width;
		var height:Number = this.height;

		ctlContent._xscale = 100;
		ctlContent._yscale = 100;

		var xscale:Number = (width * 0.9) / ctlContent._width * 100;
		var yscale:Number = (height * 0.9) / ctlContent._height * 100;

		var scale:Number = xscale < yscale ? xscale : yscale;
		if (scale > 100) {
			scale = 100;
		}
		ctlContent._xscale = scale;
		ctlContent._yscale = scale;

		ctlContent._x = 0;
		ctlContent._y = 0;
		var bounds:Object = ctlContent.getBounds(ctlContent._parent);

		ctlContent._x = (width - ctlContent._width) / 2 - bounds.xMin;
		ctlContent._y = (height - ctlContent._height) / 2 - bounds.yMin;
	}
}