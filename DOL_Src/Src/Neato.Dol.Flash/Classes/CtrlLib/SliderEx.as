﻿import mx.core.UIObject;
import mx.utils.Delegate;

[Event("change")]

class CtrlLib.SliderEx extends UIObject{
	var title:String="";
	var minValue:Number = -10;
	var maxValue:Number = 10;
	var shiftMM:Number = 0;
	var step:Number = 1;
	var coeff:Number = 1;
	
	var lblMaxValue:CtrlLib.LabelEx;
	var lblMinValue:CtrlLib.LabelEx;
	var lblZero:CtrlLib.LabelEx;
	var lblLeft:CtrlLib.LabelEx;
	var lblRight:CtrlLib.LabelEx;
	var sliderHandle:MovieClip;
	var scale:MovieClip;
	var lblValue;
	var lblTitle:CtrlLib.LabelEx;
	var scaleLength:Number = 200;
	
	public function get ScaleLength():Number {
		return scaleLength;
	}
	
	public function set ScaleLength(value:Number):Void {
		scaleLength = value;
		coeff = ScaleLength /(maxValue - minValue);
	}
	
	public function set IsZeroLabel(value:Boolean):Void {
		lblZero._visible = value;
	}
	
	function SliderEx() {
		sliderHandle.onPress = press;
		sliderHandle.onMouseUp = mouseUp;
		sliderHandle.onMouseMove = null;
		
		coeff = ScaleLength /(maxValue - minValue);
	}
	
	public function set Points(value:Number):Void {
		sliderHandle._x = (value - minValue)*coeff;
		adjustValue();
	}
	
	public function get Points():Number {
		return shiftMM;
	}
	public function SetPoints(value:Number):Void {
		sliderHandle._x = (value - minValue)*coeff;
		adjustValue(true);
	}
	public function get Max() {
		return this.maxValue;
	}
	public function set Max(value){
		this.maxValue = value;
		Refresh();
	}
	public function get Min() {
		return this.minValue;
	}
	public function set Min(value){
		this.minValue = value;
		Refresh();
		
	}
	public function set Step(value){
		this.step = value;
		Refresh();
	}
	public function set text(value){
		title=value;
		Refresh();
	}
	public function get text(){
		return title;
		
	}
	
	function Refresh(){
		lblTitle.text = title;
		lblMinValue.text = minValue.toString();
		lblMaxValue.text = maxValue.toString();
		scale.lineStyle(1,0xFFFFFF);
		var x:Number;
		for (var i:Number = minValue; i < maxValue; i += 2*step) {
			x = (i-minValue)*coeff;
			scale.moveTo(x, 0);
			scale.lineTo(x, -4);
		}
	}
	function onLoad() {
		Refresh();
	}
	
	function mouseMove() {
		var i:Number = Math.round((_parent.minValue + _parent._xmouse / _parent.coeff) / _parent.step);
		var xx = (i * _parent.step - _parent.minValue) * _parent.coeff;
		if (xx < 0)
			_x = 0;
		else if (xx > _parent.ScaleLength)
			_x = _parent.ScaleLength; 
		else
			_x = xx;
		_parent.adjustValue();
	}
	
	private function OnChange(oldValue:Number, newValue:Number) {
		var eventObject = {type:"change", target:this, oldValue:oldValue, newValue:newValue};
		this.dispatchEvent(eventObject);
	}
	
	public function RegisterOnChangeHandler(scopeObject:Object, callBackFunction:Function) {
		addEventListener("change", Delegate.create(scopeObject, callBackFunction));
    }
	
	function adjustValue(noEvent:Boolean) {
		var oldValue:Number = Points;
		var i:Number = Math.round((minValue + sliderHandle._x / coeff) / step);
		sliderHandle._x = (i * step - minValue) * coeff;
		
		var newShift = Math.round((minValue + sliderHandle._x / coeff)*10)/10;
		
		if (shiftMM == newShift)
			return;
			
		shiftMM = newShift;
		
		lblValue.text = shiftMM.toString();
		lblValue.text += " mm";

		if (noEvent != true)
			OnChange(oldValue, Points);
	}
	
	function press() {
		this.onMouseMove = _parent.mouseMove;
	}
	
	function mouseUp() {
		//_parent.adjustValue();
		this.onMouseMove = null;
	}
	
	function onMouseDown() {
		var offset = 6;
		if (((_xmouse >=0 && _xmouse <= (sliderHandle._x - offset))
			|| (_xmouse >= (sliderHandle._x + offset) && _xmouse <= ScaleLength))
			&& (_ymouse >= 40 && _ymouse <= 48)) {
			sliderHandle._x = _xmouse;
			adjustValue();
		}
	}
}