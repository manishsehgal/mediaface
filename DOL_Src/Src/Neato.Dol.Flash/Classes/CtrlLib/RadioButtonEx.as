﻿import mx.utils.Delegate;
[Event("click")]
[Event("rollOver")]
[Event("rollOut")]
class CtrlLib.RadioButtonEx extends mx.controls.RadioButton {
	
	[Inspectable(defaultValue="RadioFalseUp")]
	var falseUpIcon:String = "RadioFalseUp";

	[Inspectable(defaultValue="RadioFalseDown")]
	var falseDownIcon:String = "RadioFalseDown";

	[Inspectable(defaultValue="RadioFalseOver")]
	var falseOverIcon:String = "RadioFalseOver";

	[Inspectable(defaultValue="RadioFalseDisabled")]
	var falseDisabledIcon:String = "RadioFalseDisabled";

	[Inspectable(defaultValue="RadioTrueUp")]
	var trueUpIcon:String = "RadioTrueUp";

	[Inspectable(defaultValue="RadioTrueDisabled")]
	var trueDisabledIcon:String = "RadioTrueDisabled";
	
	function RadioButtonEx() {
		super();
	}

	public function RegisterOnRollOverHandler (scopeObject:Object, callBackFunction:Function) {
		this.addEventListener ("rollOver", Delegate.create (scopeObject, callBackFunction));
	}
	
	function onRollOver () {
		super.onRollOver.call (this);
		var eventObject = {type:"rollOver", target:this};
		this.dispatchEvent (eventObject);
	}
	
	public function RegisterOnRollOutHandler (scopeObject:Object, callBackFunction:Function) {
		this.addEventListener ("rollOut", Delegate.create (scopeObject, callBackFunction));
	}
	
	function onRollOut () {
		super.onRollOut.call (this);
		var eventObject = {type:"rollOut", target:this};
		this.dispatchEvent (eventObject);
	}
	
	public function RegisterOnClickHandler (scopeObject:Object, callBackFunction:Function) {
		this.addEventListener ("click", Delegate.create (scopeObject, callBackFunction));
	}
}