﻿import mx.utils.Delegate;
[TagName("ImageHolder")]
[Event("click")]
[Event("doubleclick")]
[Event("contentLoaded")]
class CtrlLib.SymbolHolderEx extends CtrlLib.HolderEx {
	static var symbolName:String = "SymbolHolderEx";
	static var symbolOwner:Object = Object(CtrlLib.SymbolHolderEx);
	var className:String = "SymbolHolderEx";
	var serializeDataBinds:Boolean = true;

	private var ctlContent;
	
	function SymbolHolderEx() {
	}

	function init():Void {
		super.init();
	}

	function createChildren():Void {
		super.createChildren();
	}

	//region Data Binding
	public function DataBind() {
		ctlContent =  this.createObject(DataSource.name, "ctlContent", this.getNextHighestDepth(), new Object());
		ctlContent_OnComplete();
		//_global.tr("SymbolHolderEx.DataBind DataSource.name = " + DataSource.name + " ctlSymbol = " + ctlSymbol);
	}
	//endregion Data Binding

	function size():Void {
		super.size();
		var width = this.width;
		var height = this.height;

		//ctlContent._xscale = 100;
		//ctlContent._yscale = 100;

		//var xscale = (width * 0.9) / ctlContent._width * 100;
		//var yscale = (height * 0.9) / ctlContent._height * 100;

		//var scale = xscale < yscale ? xscale : yscale;
		//if (scale > 100) {
		//	scale = 100;
		//}
		//ctlContent._xscale = scale;
		//ctlContent._yscale = scale;

		ctlContent._x = 0;
		ctlContent._y = 0;
		var bounds = ctlContent.getBounds(ctlContent._parent);

		ctlContent._x = (width - ctlContent._width) / 2 - bounds.xMin;
		ctlContent._y = (height - ctlContent._height) / 2 - bounds.yMin;
	}

	private function ctlContent_OnComplete(eventObject) {
		size();
		this.doLater(this, "onContentLoaded");
	}
}
