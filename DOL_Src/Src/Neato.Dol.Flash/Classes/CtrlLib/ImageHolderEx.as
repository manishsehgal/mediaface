﻿import mx.utils.Delegate;
[TagName("ImageHolder")]
[Event("click")]
[Event("doubleclick")]
[Event("contentLoaded")]
class CtrlLib.ImageHolderEx extends CtrlLib.HolderEx {
	static var symbolName:String = "ImageHolderEx";
	static var symbolOwner:Object = Object(CtrlLib.ImageHolderEx);
	var className:String = "ImageHolderEx";
	var serializeDataBinds:Boolean = true;

	private var ctlImage;
	
	function ImageHolderEx() {
	}

	function init():Void {
		super.init();
	}

	function createChildren():Void {
		super.createChildren();
		if (ctlImage == undefined) {
			ctlImage =  this.createObject("UIObject", "ctlImage", this.getNextHighestDepth(), new Object());
		}
	}

	//region Data Binding
	public function DataBind() {
		//_global.tr("_________DataBind mc = " + this._name);
		_global.UIController.ImageStorage.LoadImage(ctlImage, DataSource.url, false, false, this, ctlImage_OnComplete);
	}
	//endregion Data Binding

	function size():Void {
		super.size();
		ctlImage.move(
			(this.width -  ctlImage.width )/2,
			(this.height - ctlImage.height)/2);
	}

	private function ctlImage_OnComplete(eventObject) {
		//_global.tr("_________ctlImage_OnComplete mc = " + this._name);
		eventObject.target.UnregisterOnImageAttachedHandler(this);
		size();
		this.doLater(this, "onContentLoaded");
	}
}
