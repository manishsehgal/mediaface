﻿class LineToCommand implements IDraw {
	private var x:Number;
	private var y:Number;
	
	function LineToCommand(node:XMLNode) {
		x = Number(node.attributes.x);
		y = Number(node.attributes.y);
	}
	
	function Draw(mc:MovieClip, originalX:Number, originalY:Number,rotationAngle:Number,scaleX:Number,scaleY:Number):Void {
		if(scaleX==undefined)scaleX=1.0;
		if(scaleY==undefined)scaleY=1.0;
  		var a = new Object({x:originalX+x,   y:originalY+y});
  		if (rotationAngle > 0 || rotationAngle < 0) {
  			a = _global.RotateAboutCenter(a.x, a.y, originalX, originalY, rotationAngle);
  		}
  		mc.lineTo(a.x*scaleX, a.y*scaleY);

	}	
}
