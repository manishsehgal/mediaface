﻿
import mx.core.UIObject;
import mx.controls.*;
import mx.utils.Delegate;

class Tools.PlaylistTool.FileListTool extends UIObject {
	
	private var pluginsService;
	private var mode:String;//current tool mode: "folder" or "file"
	private var curFolder:String;
	
	private var lblPath:CtrlLib.TextAreaEx;
	private var btnUp:CtrlLib.ButtonEx;
	private var lstFolders:CtrlLib.ListEx;
	
	function FileListTool(){
		this.lblPath.setStyle("styleName", "ToolPropertiesInputText");
		this.btnUp.setStyle("styleName", "ToolPropertiesActiveButton");
		this.lstFolders.setStyle("styleName", "ToolPropertiesCombo");
		
		pluginsService = SAPlugins.GetInstance();
		pluginsService.RegisterOnInvokedParsedHandler(this, onResponse);
		mode = "file";
		curFolder = "";
	}
	
	public function set Mode(val:String):Void {
		this.mode = val;
	}
	
	function Show() {
		this._visible = true;
		DoFileList(curFolder);
		OnAvaible(false, "");
	}
	
    function onLoad() {
        this.btnUp.addEventListener("click", Delegate.create(this, onUp));
        this.lstFolders.addEventListener("change", Delegate.create(this, onFolderClick));
    }
    
	function DoFileList(folder:String):Void {
		//if (SAPlugins.IsAvailable() == false) return;
		
		var FileEl:XMLNode = new XMLNode(1,"Folder");
		FileEl.attributes.Path = folder;

		pluginsService.InvokeCommandEx("FILELIST_GET_FOLDER_CONTENT",FileEl);
	}
	
	function DoPlayList(file:String):Void {
		//if (SAPlugins.IsAvailable() == false) return;
		
		var FileEl:XMLNode = new XMLNode(1,"File");
		FileEl.attributes.Name = file;

		pluginsService.InvokeCommandEx("FILELIST_GET_PLAYLIST",FileEl);
	}
	
	function DoScanFolder(folder:String):Void {
		//if (SAPlugins.IsAvailable() == false) return;

		var FileEl:XMLNode = new XMLNode(1,"Folder");
		FileEl.attributes.Name = folder;
		FileEl.attributes.GetSubFolders = "false";

		pluginsService.InvokeCommandEx("FILELIST_GET_PLAYLIST",FileEl);
	}
	
	function onResponse(parsedResponse:Object) {
		try {

		if(parsedResponse.errorCode != 0)
			throw "";	// error is already shown
		
		var Response:XMLNode = parsedResponse.response;
		if (Response == undefined) throw "Not available";
		if (Response.attributes.Error != "0") throw Response.attributes.ErrorDesc;
		var Params:XMLNode = parsedResponse.params;
		if (Params == undefined) throw "Not available";
		
		if (parsedResponse.command == "FILELIST_GET_FOLDER_CONTENT") {
			var Cont:XMLNode = Utils.XMLGetChild(Params, "FolderContent");
			if (Cont == undefined) throw "Not available";
			
			curFolder = Cont.attributes.Path;
			lblPath.text = curFolder;
			
			lstFolders.removeAll();
			var Folders:XMLNode = Utils.XMLGetChild(Cont, "Folders");
			if (Folders != undefined) {
				for(var node:XMLNode=Folders.firstChild; node!=null; node=node.nextSibling){
					if (node.nodeName == "Folder") {
						lstFolders.addItem(node.attributes.Name, "d");
					}
				}
			}
			var Files:XMLNode = Utils.XMLGetChild(Cont, "Files");
			if (Files != undefined) {
				for(var node:XMLNode=Files.firstChild; node!=null; node=node.nextSibling){
					if (node.nodeName == "File") {
						lstFolders.addItem(node.attributes.Name, "f");
					}
				}
			}
			lstFolders.vPosition = 0;
			lstFolders.selectedIndex = 0;
			OnAvaible((mode == "file" ? false : true), "");
		}
		else if (parsedResponse.command == "FILELIST_GET_PLAYLIST") {
			var PL:XMLNode = Utils.XMLGetChild(Params, "PlayList");
			if (PL == undefined) throw "Not available";
			OnAdd(PL);
		}
		} catch(err:String) {
			OnAvaible(false, err);
		}
	}

/*	
	//Get first child node of 'parNode' with given name 'childName'
	function GetChild(parNode:XMLNode, childName:String) : XMLNode {
		for(var node:XMLNode=parNode.firstChild; node!=null; node=node.nextSibling){
			if (node.nodeName == childName) {
				return node;
			}
		}
		return undefined;
	}
*/	
    function onUp(eventObject) {
	if(curFolder.length > 1) {
	        DoFileList(curFolder + "..");
	}
    }
    
    function onFolderClick(eventObject) {
    	var item = this.lstFolders.selectedItem;
       	if (item == undefined) return;
       	if (item.data == "d") {
   			DoFileList(curFolder + item.label + "\\");
   		} else {
   			OnAvaible(true, "");
   		}
    }
    
	function Retrieve() {
		if (mode == "file") {
	    	var item = this.lstFolders.selectedItem;
	    	if (item != undefined && item.data == "f") {
        		DoPlayList(curFolder + item.label);
        	}
        } else if (mode == "folder") {
	        DoScanFolder(curFolder);
        }
    }
    
	private function OnAdd(node:XMLNode) {
		var playlistNode = node;
		var eventObject = {type:"add", target:this, playlistNode:playlistNode};
		this.dispatchEvent(eventObject);
	};
	
	public function RegisterOnAddHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("add", Delegate.create(scopeObject, callBackFunction));
    }
	
	private function OnAvaible(bAvaible:Boolean, err:String) {
		var avaible:Boolean = (bAvaible == false || bAvaible == undefined) ? false : true;
		var errorDesc:String = err;
		var eventObject = {type:"avaible", target:this, avaible:avaible, errorDesk:errorDesc};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnAvaibleHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("avaible", Delegate.create(scopeObject, callBackFunction));
    }
}
