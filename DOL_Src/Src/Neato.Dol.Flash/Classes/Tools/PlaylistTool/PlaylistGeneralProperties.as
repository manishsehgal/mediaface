import CtrlLib.LabelEx;
import CtrlLib.ButtonEx;
import CtrlLib.ComboBoxEx;
import CtrlLib.CheckBoxEx;
import mx.utils.Delegate;

[Event("exit")]
class Tools.PlaylistTool.PlaylistGeneralProperties extends mx.core.UIObject {
		/************************/
	    
    private var btnDelete:CtrlLib.ButtonEx;
    private var lblFont:CtrlLib.LabelEx;
    private var lblSize:CtrlLib.LabelEx;
    private var cbFont:CtrlLib.ComboBoxEx;
    private var cbFontSize:CtrlLib.ComboBoxEx;
    private static var fontSizes:Array = [4,5,6,7,8,9,10,12,14,16,18,20,24,28,32,36,40,44,48,54,60,66,72,80,88,96,106,117,127];
    private var oldFontSize:Number;
    private var btnBold:CtrlLib.ButtonEx;
    private var btnItalic:CtrlLib.ButtonEx;
	private var btnTextEffect:CtrlLib.ButtonEx;
    private var columnEditMode:String;
    
    
    function PlaylistGeneralProperties() {

        this.btnDelete.setStyle("styleName", "ToolPropertiesActiveButton");
        this.cbFont.setStyle("styleName", "ToolPropertiesCombo");
        this.cbFontSize.setStyle("styleName", "ToolPropertiesCombo");
    }
    
    function onLoad() : Void {
		TooltipHelper.SetTooltip2(btnDelete, "IDS_TOOLTIP_DELETE");

        var FontsArray:Array = _global.Fonts.GetFontsArray();
		cbFont.dropdown.cellRenderer = "FontCellRenderer";
        for (var i:Number = 0; i<FontsArray.length; i++) {
        	if (FontsArray[i].label.toString() != "_") {
				this.cbFont.addItem({data:FontsArray[i].data, label:FontsArray[i].label});
        	}
		}
        this.cbFont.sortItems(upperCaseFunc);
        this.cbFont.selectedIndex = 0;
        this.cbFont.addEventListener("change", Delegate.create(this, OnChangeFont));
        
        for (var i:String in fontSizes) {
            this.cbFontSize.addItem(fontSizes[i]);
        }
        this.cbFontSize.sortItems(numberFunc);
        this.cbFontSize.selectedIndex = 0;
        this.cbFontSize.textField.restrict="0-9";
        this.cbFontSize.textField.maxChars=3;
        this.oldFontSize = 4;
        this.cbFontSize.addEventListener("change", Delegate.create(this, OnChangeFontSize));
		this.cbFontSize.addEventListener("focusOut", Delegate.create(this, onFontSizeFocusOut));
        this.cbFontSize.textField.addEventListener("enter", Delegate.create(this, ChangeFontSize));
        
        this.btnBold.addEventListener("click", Delegate.create(this, OnBold));
        this.btnItalic.addEventListener("click", Delegate.create(this, OnItalic));
		this.btnTextEffect.addEventListener("click", Delegate.create(this, OnTextEffect));
        this.btnTextEffect.visible = false;
        columnEditMode = "Text";
		_global.GlobalNotificator.OnComponentLoaded("Tools.EditPlaylistTool.PlaylistGeneralProperties", this);
        
        //DataBind();
    }
	
	public function RegisterOnDeleteUnitHandler(scopeObject:Object, callBackFunction:Function) : Void {
        btnDelete.RegisterOnClickHandler(scopeObject, callBackFunction);
    }

    private function InitLocale() : Void {
        /*_global.LocalHelper.LocalizeInstance(btnDelete, "ToolProperties", "IDS_BTNDELETE");
        _global.LocalHelper.LocalizeInstance(lblFont, "ToolProperties", "IDS_LBLFONT");
        _global.LocalHelper.LocalizeInstance(lblSize, "ToolProperties", "IDS_LBLSIZE");
        _global.LocalHelper.LocalizeInstance(lblColumns, "ToolProperties", "IDS_LBL_COLUMNS");
        _global.LocalHelper.LocalizeInstance(btnUp, "ToolProperties", "IDS_BTN_COLUMN_UP");
        _global.LocalHelper.LocalizeInstance(btnDown, "ToolProperties", "IDS_BTN_COLUMN_DOWN");
        _global.LocalHelper.LocalizeInstance(btnHide, "ToolProperties", "IDS_BTN_HIDE");
        
        _global.LocalHelper.LocalizeInstance(this.pnlColumnText.btnEditColProps, "ToolProperties", "IDS_BTN_EDIT_COLUMN_PROPS");
        _global.LocalHelper.LocalizeInstance(this.pnlColumnProps.btnEditColText, "ToolProperties", "IDS_BTN_EDIT_COLUMN_TEXT");*/
    }
    
    static function upperCaseFunc(a, b) {
        return a.label.toUpperCase() > b.label.toUpperCase();
    }
    static function numberFunc(a, b) {
        return parseInt(a.label) > parseInt(b.label);
    }
    
    function DataBind(data:Object) : Void {
		
		var Font = _global.Project.CurrentUnit.Font;
  		var Bold = _global.Project.CurrentUnit.Bold;
  		var Italic = _global.Project.CurrentUnit.Italic;
        for (var i:Number = 0; i < this.cbFont.length; ++i) {
            if (this.cbFont.getItemAt(i).data.toLowerCase() == Font.toLowerCase()) {
                this.cbFont.selectedIndex = i;
                break;
            }
        }
        
        RefreshFontSize();
        
        this.btnBold.selected   = Bold;
        this.btnItalic.selected = Italic;
		if (_global.Project.CurrentUnit.EmptyLayoutItem) {
			_global.Project.CurrentUnit.EmptyLayoutItem = false;
		}
	}
    
    
    function RefreshFontSize() : Void {
        oldFontSize = _global.Project.CurrentUnit.Size;
        for (var i:Number = 0; i < this.cbFontSize.length; ++i) {
            if (this.cbFontSize.getItemAt(i).label == oldFontSize) {
                this.cbFontSize.selectedIndex = i;
                break;
            }
        }
        this.cbFontSize.text = oldFontSize.toString();
    }
    
    function OnChangeFont(eventObject) {
		var format:TextFormat = new TextFormat();
		format.font = eventObject.target.selectedItem.data;
		ChangeFontFormat(format);
//        _global.Project.CurrentUnit.Font = eventObject.target.selectedItem.data;
    }
    
	function onFontSizeFocusOut(eventObject:Object) : Void {
		ChangeFontSize(eventObject);
	}
	
    function OnChangeFontSize(eventObject:Object) : Void {
        if (cbFontSize.textField.getFocus() != null)
            return;
		trace('OnChangeFontSize');
        ChangeFontSize(eventObject);
    }
    
    function ChangeFontSize(eventObject:Object) : Void {
		var newSize:Number = parseInt(eventObject.target.text_mc.label.text);
		if (newSize > 3 && newSize < 128) {
			oldFontSize = newSize;
			var format:TextFormat = new TextFormat();
			format.size = newSize;
			ChangeFontFormat(format);
		} else {
			cbFontSize.text = oldFontSize.toString();
		}
//        var newSize:Number = parseInt(eventObject.target.text_mc.label.text);
//        if (newSize > 3 && newSize < 128) {
//            oldFontSize = newSize;
//            _global.Project.CurrentUnit.Size = newSize;
//        } else {
//            cbFontSize.text = oldFontSize.toString();
//        }
    }
    
    function OnBold(eventObject:Object) : Void {
		var format:TextFormat = new TextFormat();
		format.bold = eventObject.target.selected;
		ChangeFontFormat(format);
//        _global.Project.CurrentUnit.Bold = eventObject.target.selected;
    }
    
    function OnItalic(eventObject:Object) : Void {
		var format:TextFormat = new TextFormat();
		format.italic = eventObject.target.selected;
		ChangeFontFormat(format);
//        _global.Project.CurrentUnit.Italic = eventObject.target.selected;
    }
    
	public function RegisterChangeFontFormatHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("changeFontFormat", Delegate.create(scopeObject, callBackFunction));
    }
	
	function ChangeFontFormat(format:TextFormat) : Void {
  		var eventObject:Object = {type:"changeFontFormat", target:this, format:format};
  		this.dispatchEvent(eventObject);
	}
    
    private function OnReinit() : Void {
        var eventObject:Object = {type:"reinit", target:this};
        this.dispatchEvent(eventObject);
    };

    public function RegisterOnReinitHandler(scopeObject:Object, callBackFunction:Function) : Void {
        this.addEventListener("reinit", Delegate.create(scopeObject, callBackFunction));
    }
	
	private function OnTextEffect() : Void {
        var eventObject:Object = {type:"PlaylistTextEffect", target:this};
        this.dispatchEvent(eventObject);
    };

    public function RegisterOnTextEffectHandler(scopeObject:Object, callBackFunction:Function) : Void {
        this.addEventListener("PlaylistTextEffect", Delegate.create(scopeObject, callBackFunction));
    }
    
 

	
}
