﻿import mx.core.UIObject;
import mx.utils.Delegate;

class Tools.PlaylistTool.PlaylistToolContainer extends UIObject {
	
	private var accPlaylistTools: CtrlLib.AccordionEx;
	
	function PlaylistToolContainer() {
		instance = this;
	}
	
	static private var instance   : Tools.PlaylistTool.PlaylistToolContainer;
	static function getInstance() : Tools.PlaylistTool.PlaylistToolContainer {
		return instance;
	}
	
	function onLoad() {
		accPlaylistTools.setStyle("backgroundColor", "0xf7f7f7");
		accPlaylistTools.RegisterOnTabClickHandler(this, onToolClick);
		_global.tr("Tools.PlaylistTool.PlaylistToolContainer onLoad = "+this );
		_global.GlobalNotificator.OnComponentLoaded("Tools.PlaylistToolContainer", this);
	}
	
	private function onToolClick(eventObject) {
		var toolName = accPlaylistTools.getChildAt(eventObject.newValue)._name;
		var eventObject = {type:"toolClick", target:this, toolName: toolName};
		this.dispatchEvent(eventObject);
	}
	
	public function RegisterOnToolClickHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("toolClick", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function SelectTool(toolName:String) {
		var length:Number = accPlaylistTools.numChildren;
		for(var index:Number = 0; index < length; ++index) {
			if (toolName == accPlaylistTools.getChildAt(index)._name) {
				if (accPlaylistTools.selectedIndex != index)
					accPlaylistTools.selectedIndex = index;
				break;
			}
		}
	}
	
	public function AdjustHeight(height:Number) {
		this.accPlaylistTools.setSize(this.accPlaylistTools.width, height);
	}

}