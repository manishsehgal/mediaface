﻿import mx.core.UIObject;

import mx.utils.Delegate;
[Event("exit")]
class Tools.PlaylistTool.PlaylistColumnOrder extends UIObject {
		/************************/
	    
    private var lblColumns:CtrlLib.LabelEx;
    private var columnNames:Array = ["Number", "Artist", "Song", "Time"];
    private var lstColumns:CtrlLib.ListEx;
    private var btnUp:CtrlLib.ButtonEx;
    private var btnDown:CtrlLib.ButtonEx;
    private var btnHide:CtrlLib.ButtonEx;
	
    function PlaylistColumnOrder() {
        
        this.lstColumns.setStyle("styleName", "ToolPropertiesCombo");
        this.btnUp.setStyle("styleName", "ToolPropertiesActiveButton");
        this.btnDown.setStyle("styleName", "ToolPropertiesActiveButton");
        this.btnHide.setStyle("styleName", "ToolPropertiesActiveButton");
		
    }
    
    function onLoad() {
        this.lstColumns.addItem("Number", 0);
        this.lstColumns.addItem("Artist", 1);
        this.lstColumns.addItem("Song",   2);
        this.lstColumns.addItem("Time",   3);
        this.lstColumns.selectedIndex = 0;
        this.lstColumns.vScrollPolicy = "auto";
        this.lstColumns.addEventListener("change", Delegate.create(this, OnChangeColumn));
        
        this.btnUp.RegisterOnClickHandler(this, OnColumnUp);
        this.btnDown.RegisterOnClickHandler(this, OnColumnDown);
        this.btnHide.RegisterOnClickHandler(this, OnColumnHide);
        
		_global.GlobalNotificator.OnComponentLoaded("Tools.EditPlaylistTool.PlaylistColumnOrder", this);
        
        //DataBind();
    }
    
    
    static function upperCaseFunc(a, b) {
        return a.label.toUpperCase() > b.label.toUpperCase();
    }
    static function numberFunc(a, b) {
        return parseInt(a.label) > parseInt(b.label);
    }
    
    function DataBind(data) {

		for (var i=0; i<4; i++) {
			var order = _global.Project.CurrentUnit.GetColumnOrder(i);
			this.lstColumns.getItemAt(order).label = columnNames[i];
			this.lstColumns.getItemAt(order).data = i;
		}
		//this.lstColumns.redraw();
        var selInd = this.lstColumns.selectedIndex;
		this.lstColumns.selectedIndex = selInd;
        if (selInd == undefined) selInd = 0;
		OnChangeColumn();
    }
    
    function OnChangeColumn(eventObject) {
		var selInd = this.lstColumns.selectedIndex;
        var id = this.lstColumns.getItemAt(selInd).data;
		var vis = _global.Project.CurrentUnit.GetColumnVisible(id);
		btnHide.label = vis ? "Hide" : "Unhide";
        
    }
    
    function OnColumnUp(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined || selInd == 0) return;
        var tmpLabel = this.lstColumns.getItemAt(selInd).label;
        var tmpData = this.lstColumns.getItemAt(selInd).data;
        this.lstColumns.getItemAt(selInd).label = this.lstColumns.getItemAt(selInd-1).label;
        this.lstColumns.getItemAt(selInd).data = this.lstColumns.getItemAt(selInd-1).data;
        this.lstColumns.getItemAt(selInd-1).label = tmpLabel;
        this.lstColumns.getItemAt(selInd-1).data = tmpData;
        this.lstColumns.selectedIndex = selInd-1;
        OnMoveColumn(tmpData, selInd, selInd - 1);
//        _global.Project.CurrentUnit.SetColumnOrder(tmpData, selInd-1);
    }
    
    function OnColumnDown(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined || selInd == this.lstColumns.length-1) return;
        var tmpLabel = this.lstColumns.getItemAt(selInd).label;
        var tmpData = this.lstColumns.getItemAt(selInd).data;
        this.lstColumns.getItemAt(selInd).label = this.lstColumns.getItemAt(selInd+1).label;
        this.lstColumns.getItemAt(selInd).data = this.lstColumns.getItemAt(selInd+1).data;
        this.lstColumns.getItemAt(selInd+1).label = tmpLabel;
        this.lstColumns.getItemAt(selInd+1).data = tmpData;
        this.lstColumns.selectedIndex = selInd+1;
        OnMoveColumn(tmpData, selInd, selInd + 1);
//        _global.Project.CurrentUnit.SetColumnOrder(tmpData, selInd+1);
    }
    
    private function OnMoveColumn(item, oldIndex:Number, newIndex:Number) : Void {
		var eventObject = {type:"MoveColumn", target:this, item:item, oldIndex:oldIndex, newIndex:newIndex};
		this.dispatchEvent(eventObject);
    }

	public function RegisterOnMoveColumnHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("MoveColumn", Delegate.create(scopeObject, callBackFunction));
    }
    
    function OnColumnHide(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined) return;
        var id = this.lstColumns.getItemAt(selInd).data;
        var vis = _global.Project.CurrentUnit.GetColumnVisible(id);
//        _global.Project.CurrentUnit.SetColumnVisible(id, !vis);
		btnHide.label = !vis ? "Hide" : "Unhide";
        //_global.LocalHelper.LocalizeInstance(this.btnHide, "ToolProperties", );
        
		var eventObject = {type:"HideColumn", target:this, id:id, currentVisibility:vis};
		this.dispatchEvent(eventObject);
    }

	public function RegisterOnHideColumnHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("HideColumn", Delegate.create(scopeObject, callBackFunction));
    }

	/*************************/
}
