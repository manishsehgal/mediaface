﻿import mx.core.UIObject;
import mx.utils.Delegate;

class Tools.PlaylistTool.AudioCDTool extends UIObject  {
	private var cboDrive:CtrlLib.ComboBoxEx;
	private var lblDrive:CtrlLib.LabelEx;
	private var listenerRetrieve:Object;
	private var pluginsService;
	
	
	function AudioCDTool(){
		cboDrive.enabled=false;
		pluginsService = SAPlugins.GetInstance();
		pluginsService.RegisterOnInvokedHandler(this, onInvoked);
	}

	function onLoad()
	{
		setStyle("styleName", "ToolPropertiesPanel");
		this.cboDrive.setStyle("styleName", "ToolPropertiesCombo");
		_global.LocalHelper.LocalizeInstance(lblDrive,"ToolProperties","IDS_LBLSELECTDRIVETEXT");
	}
	
	function onInvoked(eventObject) {
			//BrowserHelper.InvokePageScript("log", eventObject.result.firstChild.nodeName);
			this._parent.txtResponse.text=eventObject.result;
			trace("Onivoked event!!!"+eventObject.result);
			var Reply:XML=new XML();
			Reply.parseXML(eventObject.result);
			trace("Reply - "+Reply);
			
			var CmdInfo:XMLNode=Reply.firstChild;
			var Request:XMLNode;
			var Response:XMLNode;
			for(var tmpNode:XMLNode=CmdInfo.firstChild;tmpNode!=null;tmpNode=tmpNode.nextSibling){
					switch(tmpNode.nodeName){
						case "Request":
							Request=tmpNode;
							break;
						case "Response":
							Response=tmpNode;
							break;
						default:
							break;
					}
			}
						
			if(Request.attributes.Name=="GET_AUDIOCD_DRIVES"){
				if(Response.attributes.Error==0){
					var Drives:XMLNode=Response.firstChild.firstChild;//Params//Drives
					trace("Drives - "+Drives);
					cboDrive.removeAll();
					if(Drives.childNodes.length==0){
				//		this._parent.txtResponse.text="No drives";
			//			btnRetrieve.enabled=false;
						cboDrive.enabled=false;
						OnAvaible(false,"No drives");
					}else{
						//btnRetrieve.enabled=true;
						cboDrive.enabled=true;
						OnAvaible(true,"");
						for(var Node:XMLNode=Drives.firstChild;Node!=null;Node=Node.nextSibling){
							cboDrive.addItem(String(Node.attributes.Name));
							
						}
					}
				}else{
					//this._parent.txtResponse.text=Response.attributes.ErrorDesc;
					//btnRetrieve.enabled=false;
					cboDrive.enabled=false;
				}
			}else if(Request.attributes.Name=="GET_AUDIOCD_PLAYLIST"){
				if(Response.attributes.Error==0){
					var Playlist:XMLNode=Response.firstChild.firstChild;//params//playlist
					OnAdd(Playlist);
			}else{
					//this._parent.txtResponse.text=Response.attributes.ErrorDesc;
					OnAvaible(true,Response.attributes.ErrorDesc);
				}
			
				
			}
	}
	
	function FillDrives() {
		var Drives:XML = new XML();
		var CmdInfo:XMLNode = Drives.createElement("CmdInfo");
		var Request:XMLNode = Drives.createElement("Request");
		Drives.appendChild(CmdInfo);
		CmdInfo.appendChild(Request);
		CmdInfo.attributes.Type="Request";
		Request.attributes.Name="GET_AUDIOCD_DRIVES";
		pluginsService.InvokeCommand(Drives);
	}
	function Show() {
		this._visible = true;
		FillDrives();
		OnAvaible(false,"");
	}
	function Retrieve() {
			if(cboDrive.enabled==false || cboDrive.length<1){
				return;
			}
			var Query:XML = new XML();
			var CmdInfo:XMLNode = Query.createElement("CmdInfo");
			var Request:XMLNode = Query.createElement("Request");
			var Params:XMLNode = Query.createElement("Params");
			var Drive:XMLNode = Query.createElement("Drive");
			Query.appendChild(CmdInfo);
			CmdInfo.appendChild(Request);
			Request.appendChild(Params);
			Params.appendChild(Drive);
			CmdInfo.attributes.Type="Request";
			Request.attributes.Name="GET_AUDIOCD_PLAYLIST";
			Drive.attributes.Name = cboDrive.text;
			pluginsService.InvokeCommand(Query);
	}
	//region add  event
	private function OnAdd(node:XMLNode) {
		var playlistNode = node;
		var eventObject = {type:"add", target:this, playlistNode:playlistNode};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnAddHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("add", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
	//region Avaible
	
	private function OnAvaible(bAvaible:Boolean,err:String) {
		var avaible:Boolean = (bAvaible == false || bAvaible == undefined) ? false : true;
		var errorDesk:String =  err ;
		var eventObject = {type:"avaible", target:this, avaible:avaible, errorDesk:errorDesk};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnAvaibleHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("avaible", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion

}