﻿import mx.core.UIObject;
import mx.utils.Delegate;

class Tools.PlaylistTool.PlaylistToolSubContainer extends UIObject {
	
	private var accPlaylistSubTools: CtrlLib.AccordionEx;
	
	function PlaylistToolSubContainer() {
		instance = this;
	}
	
	static private var instance   : Tools.PlaylistTool.PlaylistToolSubContainer;
	static function getInstance() : Tools.PlaylistTool.PlaylistToolSubContainer {
		return instance;
	}
	
	function onLoad() {
		accPlaylistSubTools.setStyle("backgroundColor", "0xf7f7f7");
		accPlaylistSubTools.RegisterOnTabClickHandler(this, onToolClick);
		_global.tr("Tools.PlaylistTool.PlaylistToolContainer onLoad = "+this );
		_global.GlobalNotificator.OnComponentLoaded("Tools.EditPlaylistTool.PlaylistToolSubContainer", this);
	}
	
	private function onToolClick(eventObject) {
		var toolName = accPlaylistSubTools.getChildAt(eventObject.newValue)._name;
		var eventObject = {type:"toolClick", target:this, toolName: toolName};
		this.dispatchEvent(eventObject);
	}
	
	public function RegisterOnToolClickHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("toolClick", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function SelectTool(toolName:String) {
		var length:Number = accPlaylistSubTools.numChildren;
		for(var index:Number = 0; index < length; ++index) {
			if (toolName == accPlaylistSubTools.getChildAt(index)._name) {
				if (accPlaylistSubTools.selectedIndex != index)
					accPlaylistSubTools.selectedIndex = index;
				break;
			}
		}
	}
	
	public function AdjustHeight(height:Number) {
		this.accPlaylistSubTools.setSize(this.accPlaylistSubTools.width, height);
	}

}