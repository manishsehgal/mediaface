import mx.core.UIComponent;
import mx.utils.Delegate;

class Tools.PlaylistTool.CheckListCellRenderer extends UIComponent
{
	static public var symbolName  : String = "CheckListCellRenderer";
	static public var symbolOwner : Object =  CheckListCellRenderer;
	public var        className   : String = "CheckListCellRenderer";
	

    //private var multiLineLabel; // The label to be used for text.
    private var owner; // The row that contains this cell.
    private var listOwner; // The List, data grid or tree containing this cell.

    // Cell height offset from the row height total and preferred cell width.
    //private static var PREFERRED_HEIGHT_OFFSET = 0; 
    //private static var PREFERRED_WIDTH = 140;

	private var chkCheck;
	private var lblLabel;
	private var itemObj:Object;
	
	
	public var getDataLabel:Function;
	

    public function CheckListCellRenderer() {
    }

    public function createChildren():Void
    {
		chkCheck = createObject("CheckBoxEx", "chkCheck", 1, {styleName:this, owner:this});
		chkCheck.addEventListener("click", Delegate.create(this, OnCheck));
		lblLabel = createObject("LabelEx",    "lblLabel", 2, {styleName:this, owner:this});
		lblLabel.text = " ";
    }
    
	public function OnCheck(eventObject) {
		if (itemObj != undefined) {
			itemObj.checked = chkCheck.selected;
		}
	}

    public function doLayout():Void
	{
		chkCheck.move(4,8);
		lblLabel.move(20, 4);
		var w:Number = width - lblLabel.x;
		lblLabel.setSize(w, 22);
	}

	public function getPreferredHeight():Number
    {
		return owner.__height;// - PREFERRED_HEIGHT_OFFSET;
    }

    public function setValue(suggestedValue:String, item:Object, selected:String):Void
    {
		if(item == undefined) {
            _visible = false;
			return;
        }
        
        this.itemObj = item;
        if (item.checked == undefined) {
        	item.checked = false;
        }
        chkCheck.selected = item.checked;
        
        //_global.tr(item.label + " : " + item.data);
        /*
        _visible = true;
		var lbl:String = getDataLabel();

		var textFormat:TextFormat=label_lbl.labelField.getTextFormat();
		//textFormat.color=Math.round(Math.random()*256*256*256-1);
		//var a:TextField;
		
		if(_global.Fonts.TestLoadedSmallFont()){
			label_lbl.labelField.embedFonts=true;
			textFormat.font=item[lbl];
			textFormat.size=14;
		}
		textFormat.color = (selected == "selected" || selected == "highlighted") ? 0 : 0;
		label_lbl.labelField.setTextFormat(textFormat);
		label_lbl.labelField.setNewTextFormat(textFormat);
		*/
		lblLabel.text = suggestedValue;//item[lbl];
		doLayout();
		_visible = true;
	
    }
    // function getPreferredWidth :: only for menus and DataGrid headers
    // function getCellIndex :: not used in this cell renderer

}

