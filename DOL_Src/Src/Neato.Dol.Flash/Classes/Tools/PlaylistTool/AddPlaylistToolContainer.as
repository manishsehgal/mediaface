﻿import mx.core.UIObject;
import mx.utils.Delegate;
import mx.controls.*;
class Tools.PlaylistTool.AddPlaylistToolContainer extends UIObject {
	private var listenerCboSource:Object;
	private var CDTextToolClip:Tools.PlaylistTool.CDTextTool;
	private var AudioCDToolClip:Tools.PlaylistTool.AudioCDTool;
	private var WMPToolClip:Tools.PlaylistTool.WMPTool;
	private var FileListToolClip:Tools.PlaylistTool.FileListTool;
	private var keyListener:Object;
	private var btnCDText:CtrlLib.RadioButtonEx;
	private var btnAudio:CtrlLib.RadioButtonEx;
	private var btnFolder:CtrlLib.RadioButtonEx;
	private var btnPlayer:CtrlLib.RadioButtonEx;
	private var btnFile:CtrlLib.RadioButtonEx;
	private var btnManual:CtrlLib.RadioButtonEx;
	private var btnCancel:CtrlLib.ButtonEx;
	private var btnCreate:CtrlLib.ButtonEx;
	private var mcFingerPrint:Tools.PlaylistTool.FingerPrintClip;
	private var currentClip;
	
	private var chkFP:CtrlLib.CheckBoxEx;
		
	private var pluginsService : SAPlugins;
	
	function AddPlaylistToolContainer() {
		currentClip = null;
		setStyle("styleName", "ToolPropertiesPanel");
		//this.cboSource.setStyle("styleName", "ToolPropertiesCombo");
		//this.txtResponse.setStyle("styleName", "ToolPropertiesInputText");
		this.btnCancel.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnCreate.setStyle("styleName", "ToolPropertiesActiveButton");
		//this.lblError.setStyle("styleName", "ToolPropertiesNote");
		btnCDText.setStyle("styleName", "ToolPropertiesRadioButton");
		btnAudio.setStyle("styleName", "ToolPropertiesRadioButton");
		btnFolder.setStyle("styleName", "ToolPropertiesRadioButton");
		btnPlayer.setStyle("styleName", "ToolPropertiesRadioButton");
		btnFile.setStyle("styleName", "ToolPropertiesRadioButton");
		btnManual.setStyle("styleName", "ToolPropertiesRadioButton");

		CDTextToolClip._visible=false;
		AudioCDToolClip._visible=false;
		WMPToolClip._visible=false;
		FileListToolClip._visible=false;
		mcFingerPrint._visible=false;
		chkFP._visible=false;
	}
	
	function onLoad() {
		//lblError.text = "";
		//btnManual.selected = true;
		
		CDTextToolClip.RegisterOnAvaibleHandler(this,OnAvaible);
		AudioCDToolClip.RegisterOnAvaibleHandler(this,OnAvaible);
		WMPToolClip.RegisterOnAvaibleHandler(this,OnAvaible);
		FileListToolClip.RegisterOnAvaibleHandler(this,OnAvaible);
		
		CDTextToolClip.RegisterOnAddHandler(this,OnPlaylistReceive);
		AudioCDToolClip.RegisterOnAddHandler(this,OnPlaylistReceive);
		WMPToolClip.RegisterOnAddHandler(this,OnPlaylistReceive);
		FileListToolClip.RegisterOnAddHandler(this,OnPlaylistReceive);
		mcFingerPrint.RegisterOnAddHandler(this,OnPlaylistReceive2);
		
		//_global.LocalHelper.LocalizeInstance(lblPlaylist,"ToolProperties","IDS_LBLADDPLAYLISTTEXT");
		_global.LocalHelper.LocalizeInstance(btnCancel, "ToolProperties", "IDS_BTNCANCEL");
		_global.LocalHelper.LocalizeInstance(btnAudio, "ToolProperties", "IDS_BTNAUDIO");
		_global.LocalHelper.LocalizeInstance(btnCDText, "ToolProperties", "IDS_BTNCDTEXT");
		_global.LocalHelper.LocalizeInstance(btnFolder, "ToolProperties", "IDS_BTNFOLDER");
		_global.LocalHelper.LocalizeInstance(btnPlayer, "ToolProperties", "IDS_BTNPLAYER");
		_global.LocalHelper.LocalizeInstance(btnFile, "ToolProperties", "IDS_BTNFILE");
		_global.LocalHelper.LocalizeInstance(btnManual, "ToolProperties", "IDS_BTNMANUAL");
		_global.LocalHelper.LocalizeInstance(btnCreate, "ToolProperties", "IDS_BTNCREATENEW");
		
		TooltipHelper.SetTooltip2(btnCancel, "IDS_TOOLTIP_CANCEL");
		TooltipHelper.SetTooltip2(btnAudio, "IDS_BTNAUDIO");
		TooltipHelper.SetTooltip2(btnCDText, "IDS_BTNCDTEXT");
		TooltipHelper.SetTooltip2(btnFolder, "IDS_BTNFOLDER");
		TooltipHelper.SetTooltip2(btnPlayer, "IDS_BTNPLAYER");
		TooltipHelper.SetTooltip2(btnFile, "IDS_BTNFILE");
		TooltipHelper.SetTooltip2(btnManual, "IDS_BTNMANUAL");
		TooltipHelper.SetTooltip2(btnCreate, "IDS_BTNCREATENEW");
		TooltipHelper.SetTooltip2(chkFP, "IDS_CHKFINGERPRINT");
		
		btnCancel.RegisterOnClickHandler(this,	btnCancel_OnClick);
		btnCreate.RegisterOnClickHandler(this, Retrieve);
		
		btnCDText.addEventListener("click", Delegate.create(this, btnCDText_OnClick));//.RegisterOnClickHandler(this,	btnCDText_OnClick);
		btnAudio.addEventListener("click", Delegate.create(this, btnAudio_OnClick));//.RegisterOnClickHandler(this,	btnAudio_OnClick);
		btnFolder.addEventListener("click", Delegate.create(this, btnFolder_OnClick));//.RegisterOnClickHandler(this,	btnFolder_OnClick);
		btnPlayer.addEventListener("click", Delegate.create(this, btnPlayer_OnClick));//.RegisterOnClickHandler(this,		btnPlayer_OnClick);
		btnFile.addEventListener("click", Delegate.create(this, btnFile_OnClick));//.RegisterOnClickHandler(this,	btnFile_OnClick);
		btnManual.addEventListener("click", Delegate.create(this, btnManual_OnClick));//.RegisterOnClickHandler(this,	btnManual_OnClick);
		chkFP.addEventListener("click", Delegate.create(this, chkFP_OnClick));
		
		mcFingerPrint.addEventListener("fpclose", Delegate.create(this, OnFPClose));
		
		_global.GlobalNotificator.OnComponentLoaded("Tools.AddPlaylistTool", this);
		
		pluginsService = SAPlugins.GetInstance();
		pluginsService.RegisterOnModuleAvaliableHandler(this, IsModuleAvaliable);
		//chkFP.selected = true;
		
		if(_global.basePlatform != "Windows")
		{
			btnManual._y = btnFile._y;
			btnFile.visible = false;
			btnFile.enabled = false;
			btnFile.label = "";
			btnFile._x = -20;
		}
	}
	function DataBind(){
		HideAllClips();
		btnManual.selected = true;
		currentClip = null;
		btnCreate.enabled = true; 
		SetFPClipVisible(false);
	}
	function ShowPlaylistProperties():Void {
	
		
	}
	
	private function Retrieve(eventObject) {
		if(currentClip != null) {
			if (currentClip == CDTextToolClip) {
				SATracking.AddPlaylistAudioCd();
			}
			else if (currentClip == AudioCDToolClip) {
				SATracking.AddPlaylistAudioScanTracks();
			}
			else if (currentClip == WMPToolClip) {
				if (currentClip.cboPlayer.selectedItem.data == "wmp") {
					SATracking.AddPlaylistPlayerWMP();
				}
				else if (currentClip.cboPlayer.selectedItem.data == "itunes") {
					SATracking.AddPlaylistPlayeriTunes();
				}
			}
			else if (currentClip == FileListToolClip) {
				if (currentClip.mode == "file") {
					SATracking.AddPlaylistFromFile();
				}
				else if (currentClip.mode == "folder") {
					SATracking.AddPlaylistScanFolders();
				}
			}
			currentClip.Retrieve();
		}
		else {
			SATracking.AddPlaylist();
			OnAdd(null,true);
		}
	}
	private function btnCDText_OnClick(eventObject) {
		HideAllClips();
		currentClip = CDTextToolClip;
		pluginsService.IsModuleAvailable("CDTextModule");
	}
	private function btnAudio_OnClick(eventObject) {
		HideAllClips();
		currentClip = AudioCDToolClip;
		pluginsService.IsModuleAvailable("AudioCDModule");
	}
	private function btnFolder_OnClick(eventObject) {
		HideAllClips();
		currentClip = FileListToolClip;
		currentClip.Mode = "folder";
		pluginsService.IsModuleAvailable("FileListModule");
	}
	private function btnPlayer_OnClick(eventObject) {
		HideAllClips();
		currentClip = WMPToolClip;
		currentClip.Show();
	}
	private function btnFile_OnClick(eventObject) {
		HideAllClips();
		currentClip = FileListToolClip;
		currentClip.Mode = "file";
		pluginsService.IsModuleAvailable("FileListModule");
	}
	private function btnManual_OnClick(eventObject) {
		trace("btnManual_OnClick");
		currentClip = null;
		HideAllClips();
		btnCreate.enabled = true; 
	}
	
	private function btnCancel_OnClick(eventObject) {
		OnExit();
	}
	
	
	private function HideAllClips() {
		CDTextToolClip._visible = false;
		AudioCDToolClip._visible = false;
		WMPToolClip._visible = false;
		FileListToolClip._visible = false;
		btnCreate.enabled = false; 
		//lblError.text = "";
	}
	private function OnPlaylistReceive(eventObject) {
		trace("OnPlaylistReceive))"+eventObject.playlistNode);
		var playlistNode:XMLNode = eventObject.playlistNode;
		OnAdd(playlistNode);
	}
	
	private function OnPlaylistReceive2(eventObject) {
		var eventObject2 = {type:"add", target:this, playlistNode:eventObject.playlistNode};
		this.dispatchEvent(eventObject2);
	}
	
	private function OnAvaible(eventObject) {
		trace("OnAvaible");
		var bAvaible:Boolean = eventObject.avaible;
		var err:String = eventObject.errorDesk;
		btnCreate.enabled = bAvaible; 
		//lblError.text = err;
		if (err.length > 0) {
			_global.MessageBox.Alert(err, "Warning", null);
		}
	}
	

	//region Exit event
	private function OnExit() {
		var eventObject = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
	
	
	private function OnAdd(node:XMLNode,bAddNew:Boolean) {
		if (chkFP.selected == true) {
			mcFingerPrint.SetPlayList(node);
			SetFPClipVisible(true);
		} else {
			var playlistNode = node;
			var bNew:Boolean = bAddNew;
			var eventObject = {type:"add", target:this, playlistNode:playlistNode,bNew:bNew};
			this.dispatchEvent(eventObject);
		}
	};

	public function RegisterOnAddHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("add", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
	
	private function SetFPClipVisible(vis:Boolean) {
		CDTextToolClip._visible = !vis;
		AudioCDToolClip._visible = !vis;
		WMPToolClip._visible = !vis;
		FileListToolClip._visible = !vis;
		btnCDText._visible = !vis;
		// Audio CD feature (scan tracks) off
		btnAudio._visible = false;
		btnFolder._visible = !vis;
		btnPlayer._visible = !vis;
		btnFile._visible = !vis;
		btnManual._visible = !vis;
		btnCancel._visible = !vis;
		btnCreate._visible = !vis;
		//chkFP._visible = !vis;
		mcFingerPrint._visible = vis;
		if (vis == false) {
			HideAllClips();
			currentClip.Show();
			btnCreate.enabled = true; 
		}
	}
	
	public function OnFPClose(eventObject) {
		SetFPClipVisible(false);
	}
	
	private function chkFP_OnClick(eventObject) {
		pluginsService.IsModuleAvailable("FingerPrintModule");
	}
	
	private function IsModuleAvaliable(eventObject) {
		switch(eventObject.moduleName) {
			case "FingerPrintModule":
				if (eventObject.isAvaliable == false) {
					chkFP.selected = false;
				}
				break;
			case "FileListModule":
			case "CDTextModule":
			case "AudioCDModule":
				if (eventObject.isAvaliable == true) {
					currentClip.Show();
				}
				break;
			case "WMPModule":
				break;
		}
	}
}