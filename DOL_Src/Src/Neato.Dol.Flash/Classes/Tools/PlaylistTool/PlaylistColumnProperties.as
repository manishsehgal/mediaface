﻿import mx.core.UIObject;
import mx.utils.Delegate;

[Event("exit")]
class Tools.PlaylistTool.PlaylistColumnProperties extends UIObject {
	var btnEditColText:CtrlLib.ButtonEx;
	var colorTool:Tools.ColorSelectionTool.ColorTool;
	var btnLeft:CtrlLib.RadioButtonEx;
	var btnCenter:CtrlLib.RadioButtonEx;
	var btnRight:CtrlLib.RadioButtonEx;
		/************************/
	    
    private var lblColumns:CtrlLib.LabelEx;
    
    private var cboColumns:CtrlLib.ComboBoxEx;
    private var fp_curTrack:Number;
    
    function PlaylistColumnProperties() {
        
        this.btnLeft.setStyle("styleName", "ToolPropertiesCheckBox");
        this.btnCenter.setStyle("styleName", "ToolPropertiesCheckBox");
        this.btnRight.setStyle("styleName", "ToolPropertiesCheckBox");
   }
    
    function onLoad() {
        
        this.colorTool.RegisterOnSelectHandler(this, OnColor);
        this.btnLeft.addEventListener("click", Delegate.create(this, OnAlign));
        this.btnCenter.addEventListener("click", Delegate.create(this, OnAlign));
        this.btnRight.addEventListener("click", Delegate.create(this, OnAlign));
        this.cboColumns.addEventListener("change", Delegate.create(this, OnChangeColumn));
		_global.GlobalNotificator.OnComponentLoaded("Tools.EditPlaylistTool.PlaylistColumnProperties", this);
		this.cboColumns.selectedIndex = 0;
        
        //DataBind();
    }
    
    
    static function upperCaseFunc(a, b) {
        return a.label.toUpperCase() > b.label.toUpperCase();
    }
    static function numberFunc(a, b) {
        return parseInt(a.label) > parseInt(b.label);
    }
    
    function DataBind(data) {
        /***TODO*/
		var selInd = this.cboColumns.selectedIndex;
		if(data != undefined)
		{
			selInd = 0;
			this.cboColumns.selectedIndex = 0;
		}
		var selectedColor = _global.Project.CurrentUnit.GetColumnColor(selInd);
        if (selectedColor == undefined) selectedColor = 0;
        this.colorTool.SetColorState(selectedColor);
		if (_global.Project.CurrentUnit.GetColumnAlign(selInd) == "right") {
            this.btnRight.selected = true;
        } else if (_global.Project.CurrentUnit.GetColumnAlign(selInd)== "center") {
            this.btnCenter.selected = true;
        } else {
            this.btnLeft.selected = true;
        }
        
    }
    
  
    
    function OnColor(eventObject) {
        var selInd = this.cboColumns.selectedIndex;
        if (selInd == undefined) return;
        var id = this.cboColumns.getItemAt(selInd).data;
        
		var eventObject = {type:"ColumnColor", target:this, id:id, color:eventObject.color};
		this.dispatchEvent(eventObject);

//        _global.Project.CurrentUnit.SetColumnColor(id, eventObject.color);
    }
    
    function OnAlign(eventObject) {
		Selection.setFocus(cboColumns);
        var selInd = this.cboColumns.selectedIndex;
        if (selInd == undefined) return;
        var id = this.cboColumns.getItemAt(selInd).data;

		var eventObject = {type:"ColumnAlign", target:this, id:id, align:eventObject.target.data};
		this.dispatchEvent(eventObject);

//        _global.Project.CurrentUnit.SetColumnAlign(id, eventObject.target.data);
    }

	public function RegisterOnColumnColorHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("ColumnColor", Delegate.create(scopeObject, callBackFunction));
    }

	public function RegisterOnColumnAlignHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("ColumnAlign", Delegate.create(scopeObject, callBackFunction));
    }

    function OnChangeColumn(eventObj) {
		DataBind();
	}
    
    
 

	/*************************/
}
