﻿import mx.core.UIObject;
import mx.utils.Delegate;
import CtrlLib.ButtonEx;

[Event("exit")]
class Tools.PlaylistTool.PlaylistColumnText extends UIObject {
	
	var txtText					:Tools.Text.SelectionTextArea;
	/************************/
	private var lblColumns:CtrlLib.LabelEx;
    private var cboColumns:CtrlLib.ComboBoxEx;
    
    private var btnSpecchar          : ButtonEx;
	private var SpeccharTool         : MovieClip;
	private var boolSpeccharToolMode : Boolean;
	private var arrayCurentRange     : Array;
	
	private var intervalId			: Number;
	
    function PlaylistColumnText() {
        this.txtText.setStyle("styleName", "ToolPropertiesInputText");
    }
    
    function onLoad() :Void {

        this.txtText.addEventListener("change", Delegate.create(this, OnChangeText));       
		this.cboColumns.addEventListener("change", Delegate.create(this, OnChangeColumn));
		_global.GlobalNotificator.OnComponentLoaded("Tools.EditPlaylistTool.PlaylistColumnText", this);
        this.cboColumns.selectedIndex = 0;
        //------ special char button
		this.btnSpecchar.addEventListener("click", Delegate.create(this, onSpecchar));
		
        //------ special char tool		
		fnSpeccharToolPanelInit();
		//------------------------
    }
    
    
    static function upperCaseFunc(a, b) {
        return a.label.toUpperCase() > b.label.toUpperCase();
    }
    static function numberFunc(a, b) {
        return parseInt(a.label) > parseInt(b.label);
    }
    
    function DataBind(data) :Void {    	
    	// Prepare Special Char Tool (Default)
    	if (boolSpeccharToolMode) {
			boolSpeccharToolMode = false;
			fnSpeccharToolRedraw();
    	}
		///////////////////////////////////////
		
		var selInd:Number = this.cboColumns.selectedIndex;
		if(data != undefined)
		{
			selInd = 0;
			this.cboColumns.selectedIndex = 0;
		}
		this.txtText.text = _global.Project.CurrentUnit.GetColumnText(selInd);
    }
    
	function OnChangeText(eventObject:Object) :Void {
		ChangeColumnText(this.cboColumns.selectedIndex, eventObject.target.text);
    }
    
    public function GetEventObject() :Object {
    	return {index:this.cboColumns.selectedIndex, text:txtText.text};	
    }
    
    public function ChangeColumnText(index:Number, text:String) : Void  {
  		var eventObject:Object = {type:"changeColumnText", target:this, index:index, text:text};
  		this.dispatchEvent(eventObject);
    }

	public function RegisterChangeFontFormatHandler(scopeObject:Object, callBackFunction:Function) :Void {
		this.addEventListener("changeColumnText", Delegate.create(scopeObject, callBackFunction));
    }
    
    function OnChangeColumn(eventObj:Object) :Void {
		DataBind();
		txtText.RestoreSelection();
		_global.Project.CurrentPaper.CurrentFace.ActManager.Focused = false;
	}
 
	/////////////////////////////////////////////////////////////////////////////
	// Special Character Tool Events and Methods
	/////////////////////////////////////////////////////////////////////////////
	// Event from Range comboBox
	function onRangeChanged(eventObject:Object) : Void {
		fnSpeccharButtonPanelInit();
	}
	
	// Event from Special Char Button
	function onSpecchar(eventObject:Object) : Void {
		if (boolSpeccharToolMode) {
			boolSpeccharToolMode = false;			
		} else {
			boolSpeccharToolMode = true;
		}	
		fnSpeccharToolRedraw();	
	}
	
	// Initialisation of Special charactert Tool
	function fnSpeccharToolPanelInit() : Void {
		this.SpeccharTool = createObject("mcSpeccharTool", "mcSpeccharTool", this.getNextHighestDepth());		
		this.boolSpeccharToolMode = false;
		fnSpeccharToolRedraw();
	}
	// Initialisation of Special characters tool content
	function fnSpeccharToolContentInit(strRange:String) : Void {
		if (strRange == undefined) {
			strRange = "";
		}
		// Range ComboBox Init
		var RangeIndex:Number = 0;
		var RangeArray:Array = new Array();
		this.SpeccharTool.cbxRangeSelect.removeAll();
		RangeArray = _global.Fonts.GetRangesArray();
		var currentFont:String = _global.Project.CurrentUnit.font;
		for (var i:Number = 0; i<RangeArray.length; i++ ) {
			if (RangeArray[i][0] == currentFont) {				
				this.SpeccharTool.cbxRangeSelect.addItem(RangeArray[i][1][0]);			
				if (_global.Fonts.NameFromRange(strRange) == RangeArray[i][1][0]) {
					RangeIndex = this.SpeccharTool.cbxRangeSelect.length-1;
				}
			}
		}
		
		//this.SpeccharTool.cbxRangeSelect.sortItems(upperCaseFunc);
		this.SpeccharTool.cbxRangeSelect.selectedIndex = RangeIndex;
		this.SpeccharTool.cbxRangeSelect.addEventListener("change", Delegate.create(this, onRangeChanged));
		// Special character buttons panel Init
		fnSpeccharButtonPanelInit();
		this.SpeccharTool.scrpSpeccharTable.invalidate();
		clearInterval(intervalId);
		/////////////////////////////////////////	
	}
	function Draw() : Void{
		fnSpeccharButtonPanelInit();
	}
	// Special character button panel Init
	function fnSpeccharButtonPanelInit() : Void {
		
		var nmPanelLineButtonCounter:Number = 0;
		var arrayCodeRanges:Array = new Array();
		arrayCurentRange = new Array();
		arrayCodeRanges = _global.Fonts.GetRangesArray();
		var currentFont:String = _global.Project.CurrentUnit.font;
		for (var i:Number = 0; i<arrayCodeRanges.length; i++ ) {
			if ((arrayCodeRanges[i][0] == currentFont)&&(arrayCodeRanges[i][1][0] == this.SpeccharTool.cbxRangeSelect.selectedItem.label)) {	
							
				nmPanelLineButtonCounter = arrayCodeRanges[i][1][1].length;
				arrayCurentRange = arrayCodeRanges[i][1][1];
				
			}
		}
		//////////////////////
		var formatSpecchar:TextFormat = new TextFormat();
		formatSpecchar = getFormatSpeccharTool();
		
		formatSpecchar.size			= 16;
		formatSpecchar.align 		= "center";
		formatSpecchar.color		= 0x000000;
		formatSpecchar.bold 		= false;
		formatSpecchar.italic 		= false;
		formatSpecchar.underline 	= false;
			
		if (!_global.Fonts.TestLoadedFont(formatSpecchar)) {
			_global.Fonts.LoadFont(formatSpecchar, this);
		} else {
			
			
			//////////////////////
			var duplicate:MovieClip;
			var nmButtonCounter:Number = 0;
			this.SpeccharTool.scrpSpeccharTable.content.removeMovieClip();
			this.SpeccharTool.scrpSpeccharTable.contentPath = "mcSpeccharPanel";
			for(var i:Number = 0; i < Math.ceil(nmPanelLineButtonCounter/6); i++) {
			    var newY:Number = i * 31;
			    for (var j:Number = 0; j < 6; j++) {
			    	if ((j==0)||(j==1)) {
			    		var newX:Number = j * 33;
			    	} else {
			    		var newX:Number = (j * 33)+1;
			    	}
			    	if (nmButtonCounter < nmPanelLineButtonCounter) {
			    		duplicate = this.SpeccharTool.scrpSpeccharTable.content.mcSpeccharPanelSymbol.duplicateMovieClip("mcSpeccharPanelSymbol" + nmButtonCounter, nmButtonCounter, {_y:newY, _x:newX});
			    		duplicate.txtSpecchar.text = String.fromCharCode(arrayCurentRange[nmButtonCounter]);
			    		duplicate.txtSpecchar.embedFonts = true;
			    		duplicate.txtSpecchar.autoSize = false;		    		
			    		duplicate.txtSpecchar.setTextFormat(formatSpecchar);
			    		duplicate.btnSpecchar.onRelease = function () { 
	    					_global.UIController.ToolsController.PlaylistToolController.CallSpeccharPanelButton(String(this._parent._name.slice(21)));				
						};		    				
			    		nmButtonCounter++;
			    		
			    	}
			    }
			} 
		}
	}
	
	// Redraw Special char Tool (visible/not visible)
	function fnSpeccharToolRedraw() : Void {
		if (boolSpeccharToolMode) {
			btnSpecchar.label = "Hide Symbols";
			SpeccharTool.gotoAndStop(2);
			//txtText.RestoreSelection();
			intervalId = setInterval(this, "fnSpeccharToolContentInit", 100);
		} else {
			btnSpecchar.label = "Insert Symbols";
			SpeccharTool.gotoAndStop(1);
			txtText.setFocus();
			txtText.RestoreSelection();
		}	
		btnSpecchar.selected = boolSpeccharToolMode;
	}
	public function CloseCharacterToll() :Void {
		if (boolSpeccharToolMode) {
			boolSpeccharToolMode = false;
			fnSpeccharToolRedraw();
		}
	}
	// Method replace text in the TextArea
	// {oldBegin:0, oldEnd:0, insertedText:textTool.getCharSpeccharTool(strIndex), format:textTool.getFormatSpeccharTool()}
	public function ReplaceTextArea(objParam:Object) : Void {
		var oldTextStringBegin:String = txtText.text.slice(0,objParam.oldBegin);
		var oldTextStringEnd:String = txtText.text.slice(objParam.oldEnd);
		txtText.SetText(oldTextStringBegin + String(objParam.insertedText) + oldTextStringEnd);
		txtText.SetSelection(objParam.oldBegin+1, objParam.oldBegin+1);
	}
	// Method return character symbol
	public function getCharSpeccharTool(strIndex:String) : String {
	return 	String.fromCharCode(arrayCurentRange[parseInt(strIndex)]);
	}
	// Method return selection begin index
	public function getSelBeginIndex(): Number {
		return txtText.GetSelectionBeginIndex();
	}
	// Method return selection begin index
	public function getSelEndIndex(): Number {
		return txtText.GetSelectionEndIndex();
	}
	// Method return Text Format
	public function getFormatSpeccharTool() : TextFormat {
		var currentFont:String = _global.Project.CurrentUnit.font;
		var currentRange:String = this.SpeccharTool.cbxRangeSelect.selectedItem.label;
		
		var format:TextFormat = new TextFormat();
		format = _global.Project.CurrentUnit.FontFormat;
		
		format.font 	 = currentFont + _global.Fonts.RangeFromName(currentRange);
		//format.size      = oldFontSize;
		//format.bold      = btnBold.selected;
		//format.italic    = btnItalic.selected;
		//format.underline = btnUnderline.selected;
		//format.color     = colorTool.RGB;
		return format;
	}
	//////////////////////////////////////////////////////////////////////////////
}

