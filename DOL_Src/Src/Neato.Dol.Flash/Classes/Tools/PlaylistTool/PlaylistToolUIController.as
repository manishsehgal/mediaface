﻿class Tools.PlaylistTool.PlaylistToolUIController {
	//private var pTool:Tools.PlaylistTool.ShapeAddToolContainer;
	//private var playlistEditTool:Tools.PlaylistTool.PlaylistEditToolContainer;
	private var columnText:Tools.PlaylistTool.PlaylistColumnText;
	private var columnProps:Tools.PlaylistTool.PlaylistColumnProperties;
	private var columnOrder:Tools.PlaylistTool.PlaylistColumnOrder;
	private var columnGeneral:Tools.PlaylistTool.PlaylistGeneralProperties;
	private var playlistEffect:Tools.PlaylistTool.PlaylistEffect;
	private var playlistSubContainer:Tools.PlaylistTool.PlaylistToolSubContainer;
	
	private var playlistAddTool:Tools.PlaylistTool.AddPlaylistToolContainer;
	private var playlistContainer:Tools.PlaylistTool.PlaylistToolContainer;
	
	public function PlaylistToolUIController() {
		trace("PlaylistToolUIController ctr");
	}
	
	function OnComponentLoaded(compName:String, compObj:Object, param) :Void {
		trace("PlaylistTool OnComponentLoaded");
		var tool:String = compName.split(".")[1];
		if(tool == "AddPlaylistTool") {
			playlistAddTool = Tools.PlaylistTool.AddPlaylistToolContainer(compObj);
			playlistAddTool.RegisterOnAddHandler(this, OnPlaylistAddHandler);
		} else if(tool == "EditPlaylistTool") {
			var subtool:String = compName.split(".")[2];
			switch(subtool) {
				case 'PlaylistColumnText':
					columnText = Tools.PlaylistTool.PlaylistColumnText(compObj);
					columnText.RegisterChangeFontFormatHandler(this, OnChangeColumnText); 
				break;
				case 'PlaylistColumnProperties':
					columnProps = Tools.PlaylistTool.PlaylistColumnProperties(compObj);
					columnProps.RegisterOnColumnColorHandler(this, OnColumnColor);
					columnProps.RegisterOnColumnAlignHandler(this, OnColumnAlign);
				break;
				case 'PlaylistColumnOrder':
					columnOrder = Tools.PlaylistTool.PlaylistColumnOrder(compObj);
					columnOrder.RegisterOnMoveColumnHandler(this, OnMoveColumn);
					columnOrder.RegisterOnHideColumnHandler(this, OnColumnHide);
				break;
				case 'PlaylistGeneralProperties':
					columnGeneral = Tools.PlaylistTool.PlaylistGeneralProperties(compObj);
					columnGeneral.RegisterChangeFontFormatHandler(this, OnChangeFontFormat);
					columnGeneral.RegisterOnTextEffectHandler(this, OnSetPlaylistTextEffect);
				break;
				case 'PlaylistToolSubContainer':
					playlistSubContainer = Tools.PlaylistTool.PlaylistToolSubContainer(compObj);
					playlistSubContainer.RegisterOnToolClickHandler(this, OnSubControllerChangePanel);
				break;
				case 'PlaylistEffect':
					playlistEffect = Tools.PlaylistTool.PlaylistEffect(compObj);
					playlistEffect.RegisterOnApplyHandler(this, OnPlaylistEffect);
				break;
				case 'AddPlaylistToolShow':
					AddDataBind();
				break;
				
			}
			
		} else if(tool == "PlaylistToolContainer") {
			playlistContainer = Tools.PlaylistTool.PlaylistToolContainer(compObj);
			playlistContainer.RegisterOnToolClickHandler(this, OnChangePanel);
		}
	}

	public function RegisterOnDeleteUnitHandler(scopeObject:Object, callBackFunction:Function) : Void {
        columnGeneral.RegisterOnDeleteUnitHandler(scopeObject, callBackFunction);
    }
	
	function EditDataBind():Void {
		_global.Mode = _global.PlaylistEditMode;
		playlistContainer.SelectTool('EditPlaylist');
		
		//if(_global.Project.CurrentUnit.GetAllText().length < 1)
		//{
			playlistSubContainer.SelectTool('ColumnText');	
			columnText.DataBind(0);
		/*} else { //this block commented by CR158
			playlistSubContainer.SelectTool('General');
			columnGeneral.DataBind(0);
		}*/
		
		//Logic.DesignerLogic.UnselectCurrentUnit();
		//this._visible = true;
		
		//playlistEditTool.DataBind(data);
	}
	function AddDataBind():Void {
		playlistContainer.SelectTool('ImportPlaylist');
		playlistSubContainer.SelectTool('General');
		playlistAddTool.DataBind();
	}
	function OnPlaylistAddHandler(eventObj:Object) : Void {
			_global.Mode = _global.PlaylistEditMode;
		var playlist = eventObj.playlistNode;
		var bNew:Boolean = eventObj.bNew;
		if(_global.Project.CurrentUnit instanceof TableUnit)
			_global.Project.CurrentUnit.ReinitTable(playlist, false, false, false);
		else {
			var unit = _global.Project.CurrentPaper.CurrentFace.AddNewPlaylist(playlist,bNew,true);

			var xml:XMLNode = unit.GetXmlNode();
			var action:Actions.AddUnitAction = new Actions.AddUnitAction(unit, xml); 
			_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
		}
	}
	function OnSubControllerChangePanel(eventObj:Object) : Void {
		switch(eventObj.toolName) {
			case 'General':
				columnGeneral.DataBind(0);
			break;
			case 'ColumnText':
				columnText.DataBind(0);
			break;
			case 'ColumnOrder':
				columnOrder.DataBind(0);
			break;
			case 'ColumnProperties':
				columnProps.DataBind(0);
			break;
			case 'PlaylistEffect':
				playlistEffect.DataSource = _global.Effects.GetEffectsArray();
				playlistEffect.DataBind(true);
			break;
		}
		
	}
	function OnChangePanel(eventObj:Object) : Void {
		switch(eventObj.toolName) {
			case 'ImportPlaylist':
				AddDataBind();
			break;
			case 'EditPlaylist':
				if(_global.Project.CurrentUnit == null){
					_global.Project.CurrentPaper.CurrentFace.AddNewPlaylist(null, true, true);
				}
				playlistSubContainer.SelectTool('General');
				columnGeneral.DataBind(0);
			break;
		}
	}
	
	function OnChangeFontFormat(eventObject:Object) : Void {
		var unit = _global.Project.CurrentUnit; 
		var oldFormat:TextFormat = unit.FontFormat;
		unit.FontFormat = eventObject.format;
		var newFormat:TextFormat = unit.FontFormat;
		
		var action:Actions.TableFormatAction = new Actions.TableFormatAction(unit, oldFormat, newFormat);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	private function OnChangeColumnText(eventObject:Object) : Void {
		var unit = _global.Project.CurrentUnit;
		var index:Number = eventObject.index;
		
		var oldText:String = unit.GetColumnText(index);
		unit.SetColumnText(index, eventObject.text);
		var newText:String = unit.GetColumnText(index);
		
		var action:Actions.TableTextAction = new Actions.TableTextAction(unit, index, oldText, newText);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	//SB - Event from Specchar tool panel's button
	public function CallSpeccharPanelButton(strIndex:String) : Void {		
		var eventObject:Object = {oldBegin:columnText.getSelBeginIndex(), oldEnd:columnText.getSelEndIndex(), insertedText:columnText.getCharSpeccharTool(strIndex), format:columnText.getFormatSpeccharTool()};
		columnText.ReplaceTextArea(eventObject);
		OnChangeColumnText(columnText.GetEventObject());
		columnText.CloseCharacterToll();
	}
	
	private function OnMoveColumn(eventObject:Object) : Void {
		var unit = _global.Project.CurrentUnit;
		var oldIndex:Number = eventObject.oldIndex;
		var newIndex:Number = eventObject.newIndex;

		unit.SetColumnOrder(eventObject.item, newIndex);
		
		var action:Actions.TableColumnOrderAction = new Actions.TableColumnOrderAction(unit, eventObject.item, oldIndex, newIndex);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	private function OnColumnHide(eventObject:Object) : Void {
		var unit = _global.Project.CurrentUnit;

		var oldVisible:Boolean = unit.GetColumnVisible(eventObject.id);
		unit.SetColumnVisible(eventObject.id, !eventObject.currentVisibility);
		var newVisible:Boolean = unit.GetColumnVisible(eventObject.id);
		
		var action:Actions.TableColumnVisibleAction = new Actions.TableColumnVisibleAction(unit, eventObject.id, oldVisible, newVisible);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	private function OnColumnColor(eventObject:Object) : Void {
		var unit = _global.Project.CurrentUnit;

		var oldColor:Number = unit.GetColumnColor(eventObject.id);
		unit.SetColumnColor(eventObject.id, eventObject.color);
		var newColor:Number = unit.GetColumnColor(eventObject.id);
		
		var action:Actions.TableColumnColorAction = new Actions.TableColumnColorAction(unit, eventObject.id, oldColor, newColor);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	private function OnColumnAlign(eventObject:Object) : Void {
		var unit = _global.Project.CurrentUnit;

		var oldAlign:String = unit.GetColumnAlign(eventObject.id);
		unit.SetColumnAlign(eventObject.id, eventObject.align);
		var newAlign:String = unit.GetColumnAlign(eventObject.id);
		
		var action:Actions.TableColumnAlignAction = new Actions.TableColumnAlignAction(unit, eventObject.id, oldAlign, newAlign);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	function OnSetPlaylistTextEffect(eventObj:Object) : Void {
		_global.MessageBox.Confirm('Confirm applying text effect?', 'Warning', OnPlaylistEffect);
	}
	
	function OnPlaylistEffect(eventObj:Object) : Void {
		trace('Playlist will be converted to texteffect:NOT IMPLEMENTED');
		var unit = _global.Project.CurrentUnit;
		var oldEffect:String = "IsTableUnit";
		var newEffect:String = eventObj.effectName;
		var xml:XMLNode = unit.GetXmlNode();

		_global.Project.CurrentPaper.CurrentFace.ConvertPlaylistToEffect(eventObj.effectName);
		
		unit = _global.Project.CurrentUnit;
		
		var action:Actions.ConvertTableToTextEffectAction = new Actions.ConvertTableToTextEffectAction(unit, oldEffect, newEffect, xml);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
}