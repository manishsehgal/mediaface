import mx.core.UIObject;
import mx.utils.Delegate;

class Tools.PlaylistTool.FingerPrintClip extends UIObject {
	
	private var lstTrackList : CtrlLib.ListEx;
	private var btnFP        : CtrlLib.ButtonEx;
	private var btnCreate    : CtrlLib.ButtonEx;
	private var btnCancel    : CtrlLib.ButtonEx;
	private var lblStatus    : CtrlLib.LabelEx;
	
	private var xmlPL          : XMLNode;
	private var pluginsService : SAPlugins;
    private var inProgress     : Boolean;
    private var trackIds       : Array;
    private var curTrack       : Number;
	
	function FingerPrintClip() {
	}
	
	function onLoad() {
		this.btnFP.RegisterOnClickHandler(this, OnFP);
		this.btnCreate.RegisterOnClickHandler(this, OnCreate);
		this.btnCancel.RegisterOnClickHandler(this,	OnCancel);
		
		this.lblStatus.text = "";
		this.lstTrackList.hScrollPolicy = "on";
		this.lstTrackList.maxHPosition = 400;
		this.lstTrackList.cellRenderer = "CheckListCellRenderer";
		
		this.pluginsService = SAPlugins.GetInstance();
		this.pluginsService.RegisterOnInvokedHandler(this, onFpInvoked);
		this.inProgress = false;
	}
	
	public function RegisterOnFPCloceHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("fpclose", Delegate.create(scopeObject, callBackFunction));
    }
	public function RegisterOnAddHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("add", Delegate.create(scopeObject, callBackFunction));
    }
    
    public function SetPlayList(pl:XMLNode) {
    	this.xmlPL = pl.cloneNode(true);
    	SetPlayListText();
    }
    
    private function SetPlayListText() {
    	lstTrackList.removeAll();
		var nodeTracks:XMLNode = this.xmlPL.firstChild;
		for(var nodeTrack:XMLNode=nodeTracks.firstChild; nodeTrack!=null; nodeTrack=nodeTrack.nextSibling) {
			var item = new Object();
			item.label = nodeTrack.attributes.Artist + " - " + nodeTrack.attributes.Title;
			item.data = nodeTrack.attributes.Id;
			item.checked = false;
        	if (this.trackIds != undefined) {
        		for (var i=0; i<this.trackIds.length; i++) {
        			if (this.trackIds[i] == item.data) {
        				item.checked = true;
        				break;
        			}
        		}
        	}
        	lstTrackList.addItem(item);
		}
    }
    
	private function OnFP(eventObject) {
		if (SAPlugins.IsAvailable() == false) return;
		
		if (inProgress == false) {
			var nodeTracks:XMLNode = this.xmlPL.firstChild;
			if (nodeTracks == undefined) return;
			
			trackIds = new Array();
			for (var i=0; i<lstTrackList.length; i++) {
				var item = lstTrackList.getItemAt(i);
				if (item.checked == true) {
					trackIds.push(item.data);
				}
			}
			if (trackIds.length == 0) return;
			
			inProgress = true;
			curTrack = 0;
			btnFP.label = "Stop";
			Preloader.Show();
			FpNextTrack();
		} else {
			curTrack = trackIds.length;
		}
	}
	
    function FpNextTrack() {
		if (SAPlugins.IsAvailable() == false) return;
		
		if (curTrack >= trackIds.length) {
			FpFinish();
			return;
		}
		
		this.lblStatus.text = "Track " + trackIds[curTrack] + " in progress...";
		
		var cmd:XML = new XML();
		var CmdInfo:XMLNode = cmd.createElement("CmdInfo");
		CmdInfo.attributes.Type = "Request";
		cmd.appendChild(CmdInfo);
		
		var Request:XMLNode = cmd.createElement("Request");
		Request.attributes.Name = "FP_PROCESS_PLAYLIST";
		CmdInfo.appendChild(Request);
		
		var Params:XMLNode = cmd.createElement("Params");
		Request.appendChild(Params);
		
		var Sel:XMLNode = cmd.createElement("Selection");
		Sel.attributes.Ids = trackIds[curTrack];
		Params.appendChild(Sel);
		
		var playList:XMLNode = this.xmlPL.cloneNode(true);
		Params.appendChild(playList);
		
		curTrack++;
		pluginsService.InvokeCommand(cmd);
    }
    
    function FpFinish() {
    	this.inProgress = false;
    	this.trackIds = undefined;
    	this.curTrack = 0;
		this.btnFP.label = "FingerPrint";
		this.lblStatus.text = "Done";
		Preloader.Hide();
    }
    
	function onFpInvoked(eventObject) {
		try {
			var reply:XML = new XML();
			reply.parseXML(eventObject.result);
			var CmdInfo:XMLNode = reply.firstChild;
		
			var Response:XMLNode = GetChild(CmdInfo, "Response");
			if (Response == undefined) throw 0;
			if (Response.attributes.Error != "0") throw 0;
			var Params:XMLNode = GetChild(Response, "Params");
			if (Params == undefined) throw 0;
		
			if (CmdInfo.attributes.Name == "FP_PROCESS_PLAYLIST") {
				var pl:XMLNode = GetChild(Params, "PlayList");
				if (pl == undefined) throw 0;
				
				this.xmlPL = pl.cloneNode(true);
		    	SetPlayListText();
				FpNextTrack();
			}
		} catch(err:Number) {
			FpFinish();
		}
	}
	
	//Get first child node of 'parNode' with given name 'childName'
	function GetChild(parNode:XMLNode, childName:String) : XMLNode {
		for(var node:XMLNode=parNode.firstChild; node!=null; node=node.nextSibling){
			if (node.nodeName == childName) {
				return node;
			}
		}
		return undefined;
	}
	
	private function OnCreate(eventObject) {
		var eventObject = {type:"fpclose", target:this};
		this.dispatchEvent(eventObject);
		var eventObject = {type:"add", target:this, playlistNode:this.xmlPL};
		this.dispatchEvent(eventObject);
	}
	
	private function OnCancel(eventObject) {
		var eventObject = {type:"fpclose", target:this};
		this.dispatchEvent(eventObject);
	}
}