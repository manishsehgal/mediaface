﻿import mx.utils.Delegate;
import Logic.*;

[Event("add")]
class Tools.PlaylistTool.PlaylistEffect extends mx.core.UIComponent {
	
	static var symbolName:String = "Tools.PlaylistTool.PlaylistEffect";
	static var symbolOwner:Object = Tools.PlaylistTool.PlaylistEffect;
	var className:String = "PlaylistEffect";

	private var boundingBox_mc:MovieClip;
	private var ctlChooseEffect;
	//private var lblTextEffects;
	private var lblError;
	//private var btnEditText/*:MenuButton*/;
	//private var btnDelete:MenuButton;
	
	private var pluginsService : SAPlugins;

	private var xGap:Number = 4;
	private var yGap:Number = 4;
	private var bAvaible:Boolean = false;
	private var parent;
	function PlaylistEffect() {
		super();
		
	}

	function init():Void {
		super.init();

		if (boundingBox_mc != undefined) {
			boundingBox_mc._width = 0;
			boundingBox_mc._height = 0;
			boundingBox_mc._visible = false;
		}
	}

	function createChildren():Void {
		super.createChildren();
		if (ctlChooseEffect == undefined) {
			ctlChooseEffect = this.createObject("HListEx", "ctlChooseEffect", this.getNextHighestDepth());
		}
		/*if (btnCancel == undefined) {
			btnCancel = this.createObject("MenuButton", "btnCancel", this.getNextHighestDepth());
		}*/
	}

	private function onLoad() {
		//lblTextEffects.setStyle("styleName", "ToolPropertiesCaption");
		lblError.setStyle("styleName", "ToolPropertiesNote");
		pluginsService = SAPlugins.GetInstance();
		pluginsService.RegisterOnModuleAvaliableHandler(this, IsTextEffectsModuleAvaliable);
		
		ctlChooseEffect.RegisterOnChangeHandler(this, ctlChooseEffect_OnChange);

		//this.btnDelete.setStyle("styleName", "ToolPropertiesActiveButton");
		//btnDelete.RegisterOnClickHandler(this, _global.DeleteCurrentUnit);
		
		//this.btnEditText.setStyle("styleName", "ToolPropertiesActiveButton");
		//btnEditText.RegisterOnClickHandler(this,	btnEditText_OnClick);

		//DataBind();
		_global.GlobalNotificator.OnComponentLoaded("Tools.EditPlaylistTool.PlaylistEffect", this);
		currDataSource = new Array();
		FullDataSource = new Array();
		parent = this;
		//InitLocale();
	}
/*
    private function InitLocale() {
		_global.LocalHelper.LocalizeInstance(lblTextEffects, "ToolProperties", "IDS_LBLTEXTEFFECTS");
		_global.LocalHelper.LocalizeInstance(btnEditText, "ToolProperties", "IDS_BTNEDITTEXT");
		//_global.LocalHelper.LocalizeInstance(btnDelete, "ToolProperties", "IDS_BTNDELETE");
		
	//	TooltipHelper.SetTooltip(btnCancel, "ToolProperties", "IDS_TOOLTIP_CANCEL");
		TooltipHelper.SetTooltip(btnDelete, "ToolProperties", "IDS_TOOLTIP_DELETE");
    }
*/
	public var currDataSource:Array;
	public var FullDataSource:Array;
	public function set DataSource(value:Array) {
		if(bAvaible == false)
			pluginsService.IsModuleAvailable("TextEffectModule");
		
		FullDataSource = value;
		currDataSource =  value.slice(0,1);
		trace('Full data souce' + FullDataSource+" currDataSource "+currDataSource);
		
	}

	public function DataBind(bCallPlugin:Boolean) {
		trace("public function DataBind() " +bAvaible);

		ctlChooseEffect.DataSource = bAvaible == true ? FullDataSource : currDataSource;
		ctlChooseEffect.ItemLinkageName = "SymbolHolderEx";
		ctlChooseEffect.DataBind();
		trace('Full data souce' + FullDataSource+" currDataSource "+currDataSource);
		trace("ctlChooseEffect.DataSource  "+ ctlChooseEffect.DataSource);
		if(bAvaible == false && bCallPlugin != false)
			pluginsService.IsModuleAvailable("TextEffectModule");
	}
	
	private function IsTextEffectsModuleAvaliable(eventObject) {
		bAvaible = eventObject.isAvaliable.toString() == "true" ? true : false;
		trace("IsTextEffectsModuleAvaliable "+bAvaible);
		parent.DataBind(false);

	}
	//region Actions

	private function ctlChooseEffect_OnChange(eventObject) {
		if(eventObject.index == undefined)
			return;
		OnApply(eventObject.index);
		
		
		
	}
	//endregion

	//region add Shape event
	private function OnApply(index) {
		var effectName = FullDataSource[index].name;
		var eventObject = {type:"apply", target:this, effectName:effectName, index:index};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnApplyHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("apply", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
	
	private function btnEditText_OnClick(eventObject) {
		OnEditText();
	}
	
	
	//region Exit event
	private function OnEditText() {
		var eventObject = {type:"edittext", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnEditTextHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("edittext", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
}
