﻿import mx.utils.Delegate;
import mx.core.UIObject;
[Event("exit")]
[Event("reinit")]
class Tools.PlaylistTool.PlaylistEditToolContainer extends UIObject {
    /*private var lblPlaylistEdit:CtrlLib.LabelEx;
    private var btnDelete:CtrlLib.ButtonEx;
    private var lblFont:CtrlLib.LabelEx;
    private var lblSize:CtrlLib.LabelEx;
    private var cbFont:CtrlLib.ComboBoxEx;
    private var cbFontSize:CtrlLib.ComboBoxEx;
    private static var fontSizes:Array = [4,8,9,10,12,14,16,18,20,24,28,32,36,40,44,48,54,60,66,72,80,88,96,106,117,127];
    private var oldFontSize:Number;
    private var btnBold:CtrlLib.ButtonEx;
    private var btnItalic:CtrlLib.ButtonEx;
    private var lblColumns:CtrlLib.LabelEx;
    private var columnNames:Array = ["Number", "Artist", "Song", "Time"];
    private var lstColumns:CtrlLib.ListEx;
    private var btnUp:CtrlLib.ButtonEx;
    private var btnDown:CtrlLib.ButtonEx;
    private var btnHide:CtrlLib.ButtonEx;
	private var btnImport:CtrlLib.ButtonEx;
	private var btnFP:CtrlLib.ButtonEx;
    private var pnlColumnText:Tools.PlaylistTool.PlaylistColumnText;
    private var pnlColumnProps:Tools.PlaylistTool.PlaylistColumnProperties;
    private var columnEditMode:String;
    
	private var fp_pluginsService;
	private var fp_xmlPL:XML;
    private var fp_inProgress:Boolean;
    private var fp_trackIds:Array;
    private var fp_curTrack:Number;
    
    function PlaylistEditProperties() {
        this.lblPlaylistEdit.setStyle("styleName", "ToolPropertiesCaption");
        this.btnDelete.setStyle("styleName", "ToolPropertiesActiveButton");
        this.cbFont.setStyle("styleName", "ToolPropertiesCombo");
        this.cbFontSize.setStyle("styleName", "ToolPropertiesCombo");
       // this.chkBold.setStyle("styleName", "ToolPropertiesCheckBoxBold");
       // this.chkItalic.setStyle("styleName", "ToolPropertiesCheckBox");
        this.lstColumns.setStyle("styleName", "ToolPropertiesCombo");
        this.btnUp.setStyle("styleName", "ToolPropertiesActiveButton");
        this.btnDown.setStyle("styleName", "ToolPropertiesActiveButton");
        this.btnHide.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnImport.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnFP.setStyle("styleName", "ToolPropertiesActiveButton");
        
        //this.pnlColumnText.btnEditColProps.setStyle("styleName", "ToolPropertiesActiveButton");
        //this.pnlColumnText.txtText.setStyle("styleName", "ToolPropertiesInputText");
        
        this.pnlColumnProps.btnEditColText.setStyle("styleName", "ToolPropertiesActiveButton");
        this.pnlColumnProps.btnLeft.setStyle("styleName", "ToolPropertiesCheckBox");
        this.pnlColumnProps.btnCenter.setStyle("styleName", "ToolPropertiesCheckBox");
        this.pnlColumnProps.btnRight.setStyle("styleName", "ToolPropertiesCheckBox");
        
		fp_pluginsService = SAPlugins.GetInstance();
		fp_pluginsService.RegisterOnInvokedHandler(this, onFpInvoked);
		fp_inProgress = false;
    }
    
    function onLoad() {
        InitLocale();
        
        btnDelete.RegisterOnClickHandler(this, _global.DeleteCurrentUnit);
        btnImport.RegisterOnClickHandler(this, OnReinit);
        var FontsArray:Array = _global.Fonts.GetFontsArray();
		cbFont.dropdown.cellRenderer = "FontCellRenderer";
        for (var i in FontsArray) {
            this.cbFont.addItem(FontsArray[i]);
        }
        this.cbFont.sortItems(upperCaseFunc);
        this.cbFont.selectedIndex = 0;
        this.cbFont.addEventListener("change", Delegate.create(this, OnChangeFont));
        
        for (var i in fontSizes) {
            this.cbFontSize.addItem(fontSizes[i]);
        }
        this.cbFontSize.sortItems(numberFunc);
        this.cbFontSize.selectedIndex = 0;
        this.cbFontSize.textField.restrict="0-9";
        this.cbFontSize.textField.maxChars=3;
        this.oldFontSize = 4;
        this.cbFontSize.addEventListener("change", Delegate.create(this, OnChangeFontSize));
        this.cbFontSize.textField.addEventListener("enter", Delegate.create(this, ChangeFontSize));
        
        this.btnBold.addEventListener("click", Delegate.create(this, OnBold));
        this.btnItalic.addEventListener("click", Delegate.create(this, OnItalic));
        
        this.lstColumns.addItem("Number", 0);
        this.lstColumns.addItem("Artist", 1);
        this.lstColumns.addItem("Song",   2);
        this.lstColumns.addItem("Time",   3);
        this.lstColumns.selectedIndex = 0;
        this.lstColumns.vScrollPolicy = "auto";
        this.lstColumns.addEventListener("change", Delegate.create(this, OnChangeColumn));
        
        this.btnUp.RegisterOnClickHandler(this, OnColumnUp);
        this.btnDown.RegisterOnClickHandler(this, OnColumnDown);
        this.btnHide.RegisterOnClickHandler(this, OnColumnHide);
        
        this.btnFP.RegisterOnClickHandler(this, OnFP);
        
        this.pnlColumnText.btnEditColProps.RegisterOnClickHandler(this, OnEditColProps);
        this.pnlColumnText.txtText.addEventListener("change", Delegate.create(this, OnChangeText));

        
        this.pnlColumnProps.btnEditColText.RegisterOnClickHandler(this, OnEditColText);
        this.pnlColumnProps.colorTool.RegisterOnSelectHandler(this, OnColor);
        this.pnlColumnProps.btnLeft.addEventListener("click", Delegate.create(this, OnAlign));
        this.pnlColumnProps.btnCenter.addEventListener("click", Delegate.create(this, OnAlign));
        this.pnlColumnProps.btnRight.addEventListener("click", Delegate.create(this, OnAlign));
        
        pnlColumnText._visible = true;
        pnlColumnProps._visible = false;
        columnEditMode = "Text";
		_global.GlobalNotificator.OnComponentLoaded("Tools.EditPlaylistTool", this);
        
        //DataBind();
    }
    
    private function InitLocale() {
        _global.LocalHelper.LocalizeInstance(btnDelete, "ToolProperties", "IDS_BTNDELETE");
        _global.LocalHelper.LocalizeInstance(lblFont, "ToolProperties", "IDS_LBLFONT");
        _global.LocalHelper.LocalizeInstance(lblSize, "ToolProperties", "IDS_LBLSIZE");
        _global.LocalHelper.LocalizeInstance(lblColumns, "ToolProperties", "IDS_LBL_COLUMNS");
        _global.LocalHelper.LocalizeInstance(btnUp, "ToolProperties", "IDS_BTN_COLUMN_UP");
        _global.LocalHelper.LocalizeInstance(btnDown, "ToolProperties", "IDS_BTN_COLUMN_DOWN");
        _global.LocalHelper.LocalizeInstance(btnHide, "ToolProperties", "IDS_BTN_HIDE");
        
        _global.LocalHelper.LocalizeInstance(this.pnlColumnText.btnEditColProps, "ToolProperties", "IDS_BTN_EDIT_COLUMN_PROPS");
        _global.LocalHelper.LocalizeInstance(this.pnlColumnProps.btnEditColText, "ToolProperties", "IDS_BTN_EDIT_COLUMN_TEXT");
    }
    
    static function upperCaseFunc(a, b) {
        return a.label.toUpperCase() > b.label.toUpperCase();
    }
    static function numberFunc(a, b) {
        return parseInt(a.label) > parseInt(b.label);
    }
    
    function DataBind(data) {
        for (var i = 0; i < this.cbFont.length; ++i) {
            if (this.cbFont.getItemAt(i).label.toLowerCase() == data.Font) {
                this.cbFont.selectedIndex = i;
                break;
            }
        }
        
        RefreshFontSize();
        
        this.btnBold.selected   = data.Bold;
        this.btnItalic.selected = data.Italic;
		if (_global.Project.CurrentUnit.EmptyLayoutItem) {
			_global.Project.CurrentUnit.ReinitTable(null,false,true);
			_global.Project.CurrentUnit.EmptyLayoutItem = false;
		}
		
		for (var i=0; i<4; i++) {
			var order = _global.Project.CurrentUnit.GetColumnOrder(i);
			this.lstColumns.getItemAt(order).label = columnNames[i];
			this.lstColumns.getItemAt(order).data = i;
		}
		//this.lstColumns.redraw();
        
        var selInd = this.lstColumns.selectedIndex;
		this.lstColumns.selectedIndex = selInd;
        if (selInd == undefined) selInd = 0;
        var id = this.lstColumns.getItemAt(selInd).data;
        DataBindColumn(id);
        
        this.btnFP.enabled = (_global.Project.CurrentUnit.OrigPlayList.length > 0);
    }
    
    function DataBindColumn(id) {
    	var isVisible = _global.Project.CurrentUnit.GetColumnVisible(id);
    	this.btnHide.enabled = _global.Project.CurrentUnit.IsCanHideAnyOne() || !isVisible;
        _global.LocalHelper.LocalizeInstance(this.btnHide, "ToolProperties", isVisible ? "IDS_BTN_HIDE" : "IDS_BTN_UNHIDE");
        this.btnHide.setStyle("styleName", "ToolPropertiesActiveButton");

        this.pnlColumnText.txtText.text = _global.Project.CurrentUnit.GetColumnText(id);

        var selectedColor = _global.Project.CurrentUnit.GetColumnColor(id);
        if (selectedColor == undefined) selectedColor = 0;
        this.pnlColumnProps.colorTool.SetColorState(selectedColor);
        
        if (_global.Project.CurrentUnit.GetColumnAlign(id) == "right") {
            this.pnlColumnProps.btnRight.selected = true;
        } else if (_global.Project.CurrentUnit.Align == "center") {
            this.pnlColumnProps.btnCenter.selected = true;
        } else {
            this.pnlColumnProps.btnLeft.selected = true;
        }
    }
    
    function RefreshFontSize() {
        oldFontSize = _global.Project.CurrentUnit.Size;
        for (var i = 0; i < this.cbFontSize.length; ++i) {
            if (this.cbFontSize.getItemAt(i).label == oldFontSize) {
                this.cbFontSize.selectedIndex = i;
                break;
            }
        }
        this.cbFontSize.text = oldFontSize.toString();
    }
    
    function OnChangeFont(eventObject) {
        _global.Project.CurrentUnit.Font = eventObject.target.selectedItem.label;
    }
    
    function OnChangeFontSize(eventObject) {
        //if (cbFontSize.textField.getFocus() != null)
        //    return;
        ChangeFontSize(eventObject);
    }
    
    function ChangeFontSize(eventObject) {
        var newSize = parseInt(eventObject.target.text);
        if (newSize > 3 && newSize < 128) {
            oldFontSize = newSize;
            _global.Project.CurrentUnit.Size = newSize;
        } else {
            cbFontSize.text = oldFontSize.toString();
        }
    }
    
    function OnBold(eventObject) {
        _global.Project.CurrentUnit.Bold = eventObject.target.selected;
    }
    
    function OnItalic(eventObject) {
        _global.Project.CurrentUnit.Italic = eventObject.target.selected;
    }
    
    function OnChangeColumn(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined) return;
        var id = this.lstColumns.getItemAt(selInd).data;
        DataBindColumn(id);
    }
    
    function OnColumnUp(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined || selInd == 0) return;
        var tmpLabel = this.lstColumns.getItemAt(selInd).label;
        var tmpData = this.lstColumns.getItemAt(selInd).data;
        this.lstColumns.getItemAt(selInd).label = this.lstColumns.getItemAt(selInd-1).label;
        this.lstColumns.getItemAt(selInd).data = this.lstColumns.getItemAt(selInd-1).data;
        this.lstColumns.getItemAt(selInd-1).label = tmpLabel;
        this.lstColumns.getItemAt(selInd-1).data = tmpData;
        this.lstColumns.selectedIndex = selInd-1;
        _global.Project.CurrentUnit.SetColumnOrder(tmpData, selInd-1);
    }
    
    function OnColumnDown(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined || selInd == this.lstColumns.length-1) return;
        var tmpLabel = this.lstColumns.getItemAt(selInd).label;
        var tmpData = this.lstColumns.getItemAt(selInd).data;
        this.lstColumns.getItemAt(selInd).label = this.lstColumns.getItemAt(selInd+1).label;
        this.lstColumns.getItemAt(selInd).data = this.lstColumns.getItemAt(selInd+1).data;
        this.lstColumns.getItemAt(selInd+1).label = tmpLabel;
        this.lstColumns.getItemAt(selInd+1).data = tmpData;
        this.lstColumns.selectedIndex = selInd+1;
        _global.Project.CurrentUnit.SetColumnOrder(tmpData, selInd+1);
    }
    
    function OnColumnHide(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined) return;
        var id = this.lstColumns.getItemAt(selInd).data;
        var vis = _global.Project.CurrentUnit.GetColumnVisible(id);
        _global.Project.CurrentUnit.SetColumnVisible(id, !vis);
        _global.LocalHelper.LocalizeInstance(this.btnHide, "ToolProperties", !vis ? "IDS_BTN_HIDE" : "IDS_BTN_UNHIDE");
        this.btnHide.setStyle("styleName", "ToolPropertiesActiveButton");
    }
    
    function OnEditColProps(eventObject) {
        this.pnlColumnText._visible = false;
        this.pnlColumnProps._visible = true;
        this.columnEditMode = "Props";
    }
    
    function OnChangeText(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined) return;
        var id = this.lstColumns.getItemAt(selInd).data;
        _global.Project.CurrentUnit.SetColumnText(id, eventObject.target.text);
    }
    
    function OnEditColText(eventObject) {
        pnlColumnText._visible = true;
        pnlColumnProps._visible = false;
        columnEditMode = "Text";
    }
    
    function OnColor(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined) return;
        var id = this.lstColumns.getItemAt(selInd).data;
        _global.Project.CurrentUnit.SetColumnColor(id, eventObject.color);
    }
    
    function OnAlign(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined) return;
        var id = this.lstColumns.getItemAt(selInd).data;
        _global.Project.CurrentUnit.SetColumnAlign(id, eventObject.target.data);
    }
    
    private function OnReinit() {
        var eventObject = {type:"reinit", target:this};
        this.dispatchEvent(eventObject);
    };

    public function RegisterOnReinitHandler(scopeObject:Object, callBackFunction:Function) {
        this.addEventListener("reinit", Delegate.create(scopeObject, callBackFunction));
    }
    
    
    function OnFP(eventObject) {
		//if (SAPlugins.IsAvailable() == false) return;
		
		if (fp_inProgress == false) {
	    	var strPL = _global.Project.CurrentUnit.OrigPlayList;
    		if (strPL.length == 0) return;
    		
			fp_xmlPL = new XML();
			fp_xmlPL.parseXML(strPL);
			
			var nodeTracks:XMLNode = fp_xmlPL.firstChild.firstChild;
			if (nodeTracks == undefined) return;
			
			fp_trackIds = new Array();
			for(var nodeTrack:XMLNode=nodeTracks.firstChild; nodeTrack!=null; nodeTrack=nodeTrack.nextSibling) {
				fp_trackIds.push(nodeTrack.attributes.Id);
			}
			if (fp_trackIds.length == 0) return;
			
			fp_inProgress = true;
			fp_curTrack = 0;
			btnFP.label = "Stop";
		Preloader.Show();
			FpNextTrack();
		} else {
			fp_curTrack = fp_trackIds.length;
		}
    }
    
    function FpNextTrack() {
		//if (SAPlugins.IsAvailable() == false) return;
		
		if (fp_curTrack >= fp_trackIds.length) {
			FpFinish();
			return;
		}
		
		var Sel:XMLNode = new XMLNode(1,"Selection");
		Sel.attributes.Ids = fp_trackIds[fp_curTrack];
		
		var playList:XMLNode = fp_xmlPL.cloneNode(true);

		fp_curTrack++;
		fp_pluginsService.InvokeCommandExArr("FP_PROCESS_PLAYLIST",new Array(Sel,playList));
    }
    
    function FpFinish() {
    	fp_inProgress = false;
    	fp_xmlPL = undefined;
    	fp_trackIds = undefined;
    	fp_curTrack = 0;
		btnFP.label = "FP";
		Preloader.Hide();
    }
    
	function onFpInvoked(eventObject) {
		try {
			var reply:XML = new XML();
			reply.parseXML(eventObject.result);
			var CmdInfo:XMLNode = reply.firstChild;
		
			var Response:XMLNode = GetChild(CmdInfo, "Response");
			if (Response == undefined) throw 0;
			if (Response.attributes.Error != "0") throw 0;
			var Params:XMLNode = GetChild(Response, "Params");
			if (Params == undefined) throw 0;
		
			if (CmdInfo.attributes.Name == "FP_PROCESS_PLAYLIST") {
				var pl:XMLNode = GetChild(Params, "PlayList");
				if (pl == undefined) throw 0;
				
				fp_xmlPL.parseXML(pl.toString());
				_global.Project.CurrentUnit.ReinitTable(pl, false, false, false);
				FpNextTrack();
			}
		} catch(err:Number) {
			FpFinish();
		}
	}
	
	//Get first child node of 'parNode' with given name 'childName'
	function GetChild(parNode:XMLNode, childName:String) : XMLNode {
		for(var node:XMLNode=parNode.firstChild; node!=null; node=node.nextSibling){
			if (node.nodeName == childName) {
				return node;
			}
		}
		return undefined;
	}
	public function RegisterOnDeleteUnitHandler(scopeObject:Object, callBackFunction:Function) {
		btnDelete.RegisterOnClickHandler(scopeObject, callBackFunction);
	}*/
}
