﻿import mx.core.UIObject;
import mx.utils.Delegate;

class Tools.ToolContainer extends UIObject {
	private var accTools: CtrlLib.AccordionEx;
	private var mcProperties:MovieClip;	
	private var accToolsHeight:Number;	
	private static var offsetX:Number = 10;
	private static var offsetY:Number = 15;
	
	function ToolContainer() {
		instance = this;
	}
	
	static private var instance   : Tools.ToolContainer;
	static function getInstance() : Tools.ToolContainer {
		return instance;
	}
	
	public function get CurrentToolIndex():Number {
		return accTools.selectedIndex;
	}
	
	public function get CurrentToolName():String {
		return accTools.selectedChild._name;
	}
	
	function onLoad() : Void {
		accTools.setStyle("backgroundColor", "0xf7f7f7");
		
		accToolsHeight = accTools.height;
		accTools.RegisterOnTabClickHandler(this, onToolClick);
		accTools.RegisterOnItemDataBoundHandler(this, accTools_ItemDataBound);
		_global.GlobalNotificator.OnComponentLoaded("Tools", this);
		
		var listenerObject:Object = new Object();
		listenerObject.complete = mcProperties_LoadComplete;
		mcProperties.addEventListener("complete", listenerObject);

		DataBind();
		LoadProperties();
	}

	private function DataBind():Void {
		var names:Array = _global.Tools.Names;
		var labels:Array = _global.Tools.Labels;
		var length:Number = _global.Tools.Length;
		var visibility = _global.Tools.Visibility;
		var icons:Array = new Array(length);
		for(var i:Number = 0; i < length; ++i) {
			icons[i] = "IconPlace";
		}
		
		accTools.ChildNames = names;
		accTools.ChildLabels = labels;
		accTools.ChildIcons = icons;
		accTools.ChildVisibility = visibility;
		trace('Vis = '+visibility);
		accTools.DataBind();
		
		mcProperties._y = accTools.AllHeadersHeight;
	}
	
	private function onToolClick(eventObject:Object) : Void {
		var toolNamePrev:String = accTools.getChildAt(eventObject.prevValue)._name;
		var toolName:String = accTools.getChildAt(eventObject.newValue)._name;
		trace("toolName ="+toolName);
		var eventObject:Object = {type:"toolClick", target:this, toolName: toolName, toolNamePrev:toolNamePrev, index: eventObject.newValue};
		this.dispatchEvent(eventObject);
	}
	
	private function LoadProperties(index:Number) : Void {
		var length:Number = _global.Tools.Length;
		
		for(var i:Number = 0; i < length; ++i) {
			if (_global.Tools.HasCommandsAt(i)) {
				var url:String = _global.Tools.GetEditUrlAt(i);
				var name:String = _global.Tools.GetNameAt(i);
				var mc:MovieClip = mcProperties.createEmptyMovieClip(name, i);
				mc._visible = false;
				var listenerObject:Object = new Object();
				listenerObject.onLoadInit = mcProperties_LoadComplete;
		
				var mcl:MovieClipLoader = new MovieClipLoader();
				mcl.addListener(listenerObject);
		
				mcl.loadClip(url, mc);
			}
		}
	}
	
	public function ShowCommands():Void {
		var index:Number = CurrentToolIndex;
		accTools.setSize(accTools.width, accToolsHeight);
		accTools.getChildAt(index)["Content"]._visible = true;
		ShowProperty(-1);
	}
	
	public function ShowProperties():Void {
		var index:Number = CurrentToolIndex;
		accTools.setSize(accTools.width, accTools.AllHeadersHeight);
		accTools.getChildAt(index)["Content"]._visible = false;
		ShowProperty(index);
	}
	
	public function ShowPropertiesByName(toolNameToShow:String) : Void {
		accTools.setSize(accTools.width, accTools.AllHeadersHeight);
		accTools.getChildAt(CurrentToolIndex)["Content"]._visible = false;
		for (var i:Number = 0; i < _global.Tools.Length; ++i) {
			var name:String = _global.Tools.GetNameAt(i);
			if(toolNameToShow == name)	{ 
				ShowProperty(i);
				trace('ShowProperty(i);'+i);
				break;
			}
			
		}
	}
	private function ShowProperty(index:Number):Void {
		for (var i:Number = 0; i < _global.Tools.Length; ++i) {
			var name:String = _global.Tools.GetNameAt(i);
			var mc:MovieClip = mcProperties[name];
			mc._visible = (i == index);
		}
	}
	
	public function accTools_ItemDataBound(eventObject:Object) : Void {
		var view:mx.core.View = eventObject.target;
		var index:Number = eventObject.itemIndex;
		
		var mc:MovieClip = view.createEmptyMovieClip("Content", 0);
		mc._visible = false;

		var url:String = null;
		if (_global.Tools.HasCommandsAt(index))
			url = _global.Tools.GetAddUrlAt(index);
		else
			url = _global.Tools.GetEditUrlAt(index);
		var listenerObject:Object = new Object();
		listenerObject.onLoadInit = accTools_ItemLoadComplete;

		var mcl:MovieClipLoader = new MovieClipLoader();
		mcl.addListener(listenerObject);

		mcl.loadClip(url, mc);
	}

	private static function accTools_ItemLoadComplete(target) : Void {
		target._x = offsetX;
		target._y = offsetY;
	}

	private static function mcProperties_LoadComplete(target) : Void {
		target._x = offsetX;
		target._y = offsetY;
	}

	public function RegisterOnToolClickHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("toolClick", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function SelectProperties(toolName:String):Void {
		//_global.tr("SelectProperties = " + toolName);
		var length:Number = accTools.numChildren;
		for(var index:Number = 0; index < length; ++index) {
			if (toolName == accTools.getChildAt(index)._name) {
				if (accTools.selectedIndex != index)
					accTools.selectedIndex = index;
				Tools.ToolContainer.getInstance().ShowProperties();
			}
		}
	}
	
	public function SelectTool(toolName:String) : Void {
		//_global.tr("SelectTool = " + toolName);
		var length:Number = accTools.numChildren;
		for(var index:Number = 0; index < length; ++index) {
			if (toolName == accTools.getChildAt(index)._name) {
				if (accTools.selectedIndex != index)
					accTools.selectedIndex = index;

				Tools.ToolContainer.getInstance().ShowCommands();
				break;
			}
		}
	}
	
	public function get MaxToolHeight() : Number {
		return accToolsHeight - accTools.AllHeadersHeight;
	}
	
	public function ShowFillProperties() : Void {
		SelectTool("Background");
	}
}