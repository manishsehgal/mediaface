﻿import CtrlLib.LabelEx;
import CtrlLib.ButtonEx;
import CtrlLib.RadioButtonEx;
import CtrlLib.CheckBoxEx;
import mx.utils.Delegate;

[Event('onDelete')]
[Event('onKeepProportionChange')]
[Event('onRestoreProportions')]
[Event('onFitImage')]
class Tools.ImageTool.ImageProperties extends mx.core.UIObject {
	private var lblImageEdit:LabelEx;
	private var btnDelete:ButtonEx;

	private var chkKeepProportions:CheckBoxEx;
	private var btnRestoreProportions:ButtonEx;

	private var btnContinue:ButtonEx;
	
	private var optFitToFace:RadioButtonEx;
	private var optFillVertically:RadioButtonEx;
	private var optFillHorizontally:RadioButtonEx;
	private var optThumbnail:RadioButtonEx;
	
	
	
	function onLoad() {
		optFitToFace.RegisterOnClickHandler(this, optFitToFace_OnClick);
		optFillVertically.RegisterOnClickHandler(this, optFillVertically_OnClick);
		optFillHorizontally.RegisterOnClickHandler(this, optFillHorizontally_OnClick);
		optThumbnail.RegisterOnClickHandler(this, optThumbnail_OnClick);
		
		_global.GlobalNotificator.OnComponentLoaded("Tools.ImageTool", this);
		btnRestoreProportions._visible = false;
		
		TooltipHelper.SetTooltip2(btnDelete, "IDS_TOOLTIP_DELETE");
		TooltipHelper.SetTooltip2(chkKeepProportions, "IDS_TOOLTIP_KEEPPROPORTIONS");
		TooltipHelper.SetTooltip2(btnRestoreProportions, "IDS_TOOLTIP_RESTOREPROPORTIONS");
	}
	
	public function DataBind(data:Object) {
		chkKeepProportions.selected = data.keepProportions;
		SelectAutofitButton(data.autofitType);
	}

	private function SelectAutofitButton(autofitType:String) {
		optFitToFace.selected = autofitType == _global.FitToFaceAutofit;
		optFillVertically.selected = autofitType == _global.VerticalAutofit;
		optFillHorizontally.selected = autofitType == _global.HorizontalAutofit;
		optThumbnail.selected = autofitType == _global.ThumbailAutofit;
	}
	
	public function RegisterOnDeleteUnitHandler(scopeObject:Object, callBackFunction:Function) {
		btnDelete.RegisterOnClickHandler(scopeObject, callBackFunction);
	}
	
	public function RegisterOnKeepProportionsChangedHandler(scopeObject:Object, callBackFunction:Function) {
		chkKeepProportions.RegisterOnClickHandler(scopeObject, callBackFunction);
	}
	
	public function RegisterOnRestoreProportionsHandler(scopeObject:Object, callBackFunction:Function) {
		btnRestoreProportions.RegisterOnClickHandler(scopeObject, callBackFunction);
	}
	
	public function RegisterOnFitImageHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onFitImage", Delegate.create(scopeObject, callBackFunction));
	}
	
	public function OnFitImage(autofitType:String) {
		var eventObject = {type:"onFitImage", autofitType:autofitType};
		this.dispatchEvent(eventObject);
	}

	private function optFitToFace_OnClick(eventObject) {
		OnFitImage(_global.FitToFaceAutofit);
	}
	
	private function optFillVertically_OnClick(eventObject) {
		OnFitImage(_global.VerticalAutofit);
	}
	
	private function optFillHorizontally_OnClick(eventObject) {
		OnFitImage(_global.HorizontalAutofit);
	}
	
	private function optThumbnail_OnClick(eventObject) {
		OnFitImage(_global.ThumbailAutofit);
	}
}