﻿import CtrlLib.LabelEx;
import CtrlLib.TreeEx;
import CtrlLib.HListEx;
import CtrlLib.ButtonEx;
import mx.utils.Delegate;

[Event("add")]
[Event("change")]
class Tools.ImageTool.AddImage extends mx.core.UIObject {
	
	[Inspectable(defaultValue="AddImageTool")]
	var component:String = "AddImageTool";

	private var lblAddImage:LabelEx;
	private var ctlFolders:TreeEx;
	private var ctlImageList:HListEx;
	private var btnUpload:ButtonEx;
	private var boundingBox_mc:MovieClip;

	public var DataSource:XML;
    public var ImageIconUrlFormat:String;
		
	private var xGap:Number = 1;
	private var yGap:Number = 4;
	
	public function AddImage() {
		super();
	}
	
	function init():Void {
        super.init();

        if (boundingBox_mc != undefined) {
            boundingBox_mc._width = 0;
            boundingBox_mc._height = 0;
            boundingBox_mc._visible = false;
        }
    }

	function createChildren():Void {
		super.createChildren();
	}

    function size():Void {
        super.size();
        ctlFolders.invalidate();
        ctlImageList.invalidate();
    }
	
	public function onLoad() {
        ctlImageList.RegisterOnChangeHandler(this, ctlImageList_OnChange);
        ctlImageList.RegisterOnApplyHandler(this, ctlImageList_OnApply);
        ctlFolders.RegisterOnChangeHandler(this, ctlFolders_OnChange);
        ctlFolders.RegisterOnNodeCloseHandler(this, ctlFolders_OnNodeClose);
		
		_global.GlobalNotificator.OnComponentLoaded("Tools." + component, this);
		
		TooltipHelper.SetTooltip2(btnUpload, "IDS_TOOLTIP_UPLOADIMAGE");
	}
	
    public function RegisterOnAddHandler(scopeObject:Object, callBackFunction:Function) {
        this.addEventListener("add", Delegate.create(scopeObject, callBackFunction));
    }

    public function RegisterOnChangeHandler(scopeObject:Object, callBackFunction:Function) {
        this.addEventListener("change", Delegate.create(scopeObject, callBackFunction));
    }
	
    public function RegisterOnUploadHandler(scopeObject:Object, callBackFunction:Function) {
        btnUpload.RegisterOnClickHandler(scopeObject, callBackFunction);
    }
	
	public function DataBind() {
        var folderData:XML = new XML("<node></node>");
        folderData.ignoreWhite = true;
        Entity.ImageLibrary.CreateTreeData(folderData.firstChild, DataSource.firstChild, ctlFolders);

        ctlFolders.dataProvider = folderData.firstChild;
        ctlFolders.setIsOpen(ctlFolders.getTreeNodeAt(0), true);
        ctlFolders.selectedNode = ctlFolders.getTreeNodeAt(0);

        var node:XMLNode = Entity.ImageLibrary.FindFolder(DataSource.firstChild, ctlFolders.selectedNode.attributes.data);
        ctlImageList.DataSource = Entity.ImageLibrary.CreateImageListData(node, ImageIconUrlFormat);
		ctlImageList.ItemLinkageName = "ImageHolderEx";
		ctlImageList.XGap = xGap;
        ctlImageList.DataBind();
	}
	
    private function ctlImageList_OnChange(eventObject) {
		OnChange(eventObject.index);
    }

    private function ctlImageList_OnApply(eventObject) {
        OnChange(eventObject.index);
        OnAdd(eventObject.index);
    }

    private function ctlFolders_OnChange(eventObject) {
		for (var siblingNode:XMLNode = ctlFolders.selectedNode.parentNode.firstChild; siblingNode != null; siblingNode = siblingNode.nextSibling) {
			ctlFolders.setIsOpen(siblingNode, false);
		}
		ctlFolders.setIsOpen(ctlFolders.selectedNode, true);
		for (var child:XMLNode = ctlFolders.selectedNode.firstChild; child != null; child = child.nextSibling) {
			ctlFolders.setIsOpen(child, false);
		}
        var node:XMLNode = Entity.ImageLibrary.FindFolder(DataSource.firstChild, ctlFolders.selectedNode.attributes.data);
        ctlImageList.DataSource = Entity.ImageLibrary.CreateImageListData(node, ImageIconUrlFormat);
        ctlImageList.DataBind();
		ctlImageList.invalidate();
    }

	private function ctlFolders_OnNodeClose(eventObject) {
		if (ctlFolders.selectedNode == undefined) {
			if (eventObject.node.parentNode != ctlFolders.getTreeNodeAt(0).parentNode) {
				ctlFolders.selectedNode = eventObject.node.parentNode;
			} else {
				ctlFolders.selectedNode = eventObject.node;
			}
			ctlFolders_OnChange(eventObject);
		}
    }
	
    private function OnAdd(index) {
        var eventObject = {type:"add", target:this, id:ctlImageList.DataSource[index].id  };
        this.dispatchEvent(eventObject);
    }

    private function OnChange(index) {
        var eventObject = {type:"change", target:this, id:ctlImageList.DataSource[index].id };
        this.dispatchEvent(eventObject);
    }
	
	public function Clear() {
		ctlImageList.Clear();
	}
}