﻿import mx.utils.Delegate;
import mx.skins.RectBorder;
import mx.managers.DepthManager;
[Event("add")]
class Tools.ImageTool.UploadImageContent extends mx.core.View {
    static var symbolName:String = "UploadImageContent";
    static var symbolOwner:Object = Object(Tools.ImageTool.UploadImageContent);
    var className:String = "UploadImageContent";

    private var boundingBox_mc:MovieClip;
	private var border_mc:RectBorder;
	private var createClassChildAtDepth:Function;

    private var btnUploadImage;

    function LibraryContent() {
    }

    function init():Void {
        super.init();

        if (boundingBox_mc != undefined) {
            boundingBox_mc._width = 0;
            boundingBox_mc._height = 0;
            boundingBox_mc._visible = false;
        }
    }

    function createChildren():Void {
        super.createChildren();
		if (border_mc == undefined) {
			border_mc = createClassChildAtDepth(_global.styles.rectBorderClass, DepthManager.kBottom, {styleName : this});
		}
		if (btnUploadImage == undefined) {
			btnUploadImage = this.createObject("ButtonEx", "btnUploadImage", this.getNextHighestDepth());
		}
    }

    function size():Void {
        super.size();
		border_mc.setSize(width, height);
		border_mc.move(0, 0);
    }

    private function onLoad() {
        setStyle("styleName", "ToolPropertiesSubPanel");
		btnUploadImage.setStyle("styleName", "ToolPropertiesActiveButtonSmall");

		btnUploadImage.RegisterOnClickHandler(this, btnUploadImage_OnClick);

		//_global.LocalHelper.LocalizeInstance(btnUploadImage, "ToolProperties", "IDS_BTNUPLOADIMAGE");
		TooltipHelper.SetTooltip(btnUploadImage, "ToolProperties", "IDS_TOOLTIP_UPLOADIMAGE");

        DataBind();
    }

    //region Data Binding
    public function DataBind() {}
    //endregion Data Binding

    //region Upload Image event
	private function btnUploadImage_OnClick(eventObject) {
		OnUpload();
	}

	//region uploadImage event
	private function OnUpload() {
		var eventObject = {type:"upload", target:this};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnUploadHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("upload", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
}