﻿import flash.net.FileReference;
import Actions.ImageEffectAction;

class Tools.ImageTool.ImageToolUIController {
	private var imageProperties:Tools.ImageTool.ImageProperties;
	private var addImage : Tools.ImageTool.AddImage;
	private var imageEffects :Tools.ImageTool.ImageEffectsContainer;
	private var imagePropsContainer:Tools.ImageTool.ImageToolContainer;
	
	private var imageId;
	private var imageLibraryXml:XML;
	
	var fileRef:FileReference;
	private var allTypes:Array;
	private var mode:String = null;
	
	public function get CanAddImage():Boolean {
		return mode == null;
	}
	
	private function GetListener() {
		var listener:Object = new Object(); 
		
		var parent = this;
		listener.onSelect = function(file:FileReference):Void {
			_global.tr("onSelect");
			Preloader.Show();
			parent.mode = "upload";
			parent.CommandAddLibraryImage(null);
		};
		
		listener.onCancel = function(file:FileReference):Void {
			Preloader.Hide();
			_global.tr("onCancel");
		};
		
		listener.onOpen = function(file:FileReference):Void {
			_global.tr("onOpen: " + file.name);
		};
		
		listener.onProgress = function(file:FileReference, bytesLoaded:Number, bytesTotal:Number):Void {
			_global.tr("onProgress with bytesLoaded: " + bytesLoaded + " bytesTotal: " + bytesTotal);
			if (bytesLoaded == bytesTotal) {
			}
		};
		
		listener.onComplete = function(file:FileReference):Void {
			Preloader.Hide();
			_global.tr("onComplete: " + file.name);
			var imageLibariryService:SAImages = SAImages.GetInstance(new XML());
			imageLibariryService.RegisterOnAddedToProjectHandler(parent, parent.AddNewImageToProject);
			imageLibariryService.GetLastImageInfo();
		};
		
		listener.onHTTPError = function(file:FileReference, httpError:Number):Void {
			Preloader.Hide();
			_global.tr("onHTTPError: " + file.name + " http error = " + httpError);
		};
		
		listener.onIOError = function(file:FileReference):Void {
			Preloader.Hide();
			_global.tr("onIOError: " + file.name);
		};
		
		listener.onSecurityError = function(file:FileReference, errorString:String):Void {
			_global.tr("onSecurityError: " + file.name + " errorString: " + errorString);
		};
		
		return listener;
	}
		
	public function ImageToolUIController () {
		instance = this;

		allTypes = new Array();
		var imageTypes:Object = new Object();
		var extensions:Array = new Array("*.jpg", "*.jpeg", "*.gif", "*.bmp", "*.png", "*.tif", "*.tiff");
		imageTypes.description = "Images (" + extensions.join(", ") + ")";
		imageTypes.extension = extensions.join("; ");
		allTypes.push(imageTypes);

		fileRef = new FileReference();
		fileRef.addListener(GetListener());
	}
	
	private static var instance:Tools.ImageTool.ImageToolUIController;
	static function getInstance ():Tools.ImageTool.ImageToolUIController {
		return instance;
	}
	
	public function OnComponentLoaded (compName:String, compObj:Object, param) {
		//_global.tr("ImageUI ::: OnCompLoaded "+compName);
		switch (compName.split (".")[1]) {
		case "ImageTool" :
			imageProperties = Tools.ImageTool.ImageProperties (compObj);
			imageProperties.RegisterOnKeepProportionsChangedHandler (this, OnKeepProportionsCanged);
			imageProperties.RegisterOnRestoreProportionsHandler (this, OnRestoreProportions);
			imageProperties.RegisterOnFitImageHandler (this, OnFitImage);
			break;
		case "AddImageTool" :
			addImage = Tools.ImageTool.AddImage (compObj);
			addImage.RegisterOnChangeHandler (this, OnAddImage);
			//addImage.RegisterOnAddHandler (this, OnAddImage);
			addImage.RegisterOnUploadHandler (this, OnUploadImage);
			_global.Project.RegisterOnSavedHandler(this, onLoadSettings);
			break;
		case "ImageEffects":
			imageEffects = Tools.ImageTool.ImageEffectsContainer(compObj);
			imageEffects.RegisterOnApplyEffectHandler(this, OnImageEffect);
			break;
		case "ImageToolContainer":
			
			imagePropsContainer = Tools.ImageTool.ImageToolContainer(compObj);
			imagePropsContainer.RegisterOnToolClickHandler(this, OnToolChanged);
			break;
		}
	}
	
	public function RegisterOnDeleteUnitHandler(scopeObject:Object, callBackFunction:Function) {
		imageProperties.RegisterOnDeleteUnitHandler(scopeObject, callBackFunction);
	}
	
	public function OnKeepProportionsCanged (eventObject) {
		var oldKeepProportions:Boolean = _global.Project.CurrentUnit.KeepProportions;

		_global.Project.CurrentUnit.KeepProportions = eventObject.target.selected;

		var newKeepProportions:Boolean = _global.Project.CurrentUnit.KeepProportions;
		var action:Actions.KeepProportionsAction = new Actions.KeepProportionsAction(_global.Project.CurrentUnit, oldKeepProportions, newKeepProportions);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	public function OnFitImage (eventObject) {
		var unit = _global.Project.CurrentUnit;
		var scaleXString:String = Actions.ScaleAction.ScaleXProperty(unit);
		var scaleYString:String = Actions.ScaleAction.ScaleYProperty(unit);
		var x:Number = unit.X;
		var y:Number = unit.Y;
		var angle:Number = unit.Angle;
		var scaleX:Number = unit[scaleXString];
		var scaleY:Number = unit[scaleYString];
		var oldAutofitType:String = _global.Project.CurrentUnit.AutofitType;

		_global.Project.CurrentPaper.CurrentFace.AutofitCurrentUnit(eventObject.autofitType);

		var xNew:Number = unit.X;
		var yNew:Number = unit.Y;
		var angleNew:Number = unit.Angle;
		var scaleXNew:Number = unit[scaleXString];
		var scaleYNew:Number = unit[scaleYString];
		var newAutofitType:String = _global.Project.CurrentUnit.AutofitType;

		var action:Actions.AutofitAction = new Actions.AutofitAction(unit, x, y, angle, scaleX, scaleY, oldAutofitType, xNew, yNew, angleNew, scaleXNew, scaleYNew, newAutofitType);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	public function OnRestoreProportions (eventObject) {
		var unit = _global.Project.CurrentUnit;
		var scaleXString:String = Actions.ScaleAction.ScaleXProperty(unit);
		var scaleYString:String = Actions.ScaleAction.ScaleYProperty(unit);
		var scaleX:Number = unit[scaleXString];
		var scaleY:Number = unit[scaleYString];

		_global.Project.CurrentUnit.ScaleX = _global.Project.CurrentUnit.InitialScale;
		_global.Project.CurrentUnit.ScaleY = _global.Project.CurrentUnit.InitialScale;

		var scaleXNew:Number = unit[scaleXString];
		var scaleYNew:Number = unit[scaleYString];

		var action:Actions.ScaleAction = new Actions.ScaleAction(unit, scaleX, scaleY, scaleXNew, scaleYNew);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Focused = false;
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	public function OnChangeImage(eventObject) {
		imageId = eventObject.id;
	}
	
	public function OnAddImage(eventObject) {
		imageId = eventObject.id;
		if (imageId != null && imageId != undefined && CanAddImage == true) {
			mode = "add";
			CommandAddLibraryImage(null);
		}
	}
	
	private function onLoadSettings(eventObject) {
		var settingsService:SASettings = SASettings.GetInstance();
		settingsService.RegisterOnLoadedHandler(this, CommandAddLibraryImage);
		settingsService.BeginLoad();
	}
	
	public function CommandAddLibraryImage(eventObject) {
		_global.UIController.OnUnitChanged();
		var imageLibariryService:SAImages = SAImages.GetInstance(new XML());
		_global.tr("CommandAddLibraryImage() mode = " + mode);
		switch(mode) {
			case "add" :
				SATracking.AddImage();
				imageLibariryService.RegisterOnAddedToProjectHandler(this, AddNewImageToProject);
				imageLibariryService.BeginAddImageToProject(imageId);
				break;
			case "upload" :
				_global.tr("_global.freeSize = " + _global.freeSize);
				if (fileRef.size <= _global.freeSize) {
					SATracking.AddCustomImage();
					imageLibariryService.BeginUploadImage(fileRef);
				} else {
					Preloader.Hide();
					_global.MessageBox.Alert(
						" You image file must not exceed " + int(_global.freeSize / 1024)+"K",
	 					" Warning", null);
				}
				break;
		}
	}
	
	public function AddNewImageToProject(eventObject) {
		var imageDoc:XML = new XML(eventObject.LastInsertedImage);
		var imageUnit = _global.Project.CurrentPaper.CurrentFace.AddImageUnit(imageDoc.firstChild);
		_global.Project.CurrentUnit = imageUnit;
		imageUnit.Draw();
		mode = null;
	}

	public static function Contains(array:Array, value) {
		for (var i in array)
			if (value == array[i])
				return i;
		return -1;
	}
	
	public function OnUploadImage(eventObject:Object) {
		//BrowserHelper.InvokePageScript('log', 'uploadimage:nobug');
		fileRef.browse(allTypes);
	}
	
	public function DataBind () {
		_global.Mode = _global.ImageMode;
		var unit = _global.Project.CurrentUnit;
		var data:Object = new Object ();
		
		if (unit != null || unit != undefined) {
			data.keepProportions = unit.KeepProportions;
			data.autofitType = unit.AutofitType;
			data.blursharp = unit.effectBlurSharpen;
			data.contrast = unit.effectContrast;
			data.brightness = unit.effectBrightness;
			data.effect = unit.effectImage;
		}
		else {
			data.keepProportions = false;
			data.autofitType = _global.ThumbailAutofit;
			data.blursharp = 0;
			data.contrast = 0;
			data.brightness = 0;
			data.effect = "Normal";
		}
		
		imageProperties.DataBind (data);
		imageEffects.DataBind(data);
	}
	
	public function LoadImageLibrary() {
		this.imageLibraryXml = new XML();
		var imageLibariryService:SAImages = SAImages.GetInstance(this.imageLibraryXml);
		imageLibariryService.RegisterOnLoadedHandler(this, ImageLibraryDataBind);
		imageLibariryService.BeginLoad();
	}
	
	private function ImageLibraryDataBind(eventObject) {
		addImage.DataSource = imageLibraryXml;
		addImage.ImageIconUrlFormat = _global.LibraryImageIconUrlFormat;
		addImage.DataBind();
	}
	private function OnImageEffect(eventObj) {
		//_global.tr("ImageUICtrler::OnImageEffect");
		var unit = _global.Project.CurrentUnit;
		var oldContrast:Number = unit.effectContrast;
		var oldBrightness:Number = unit.effectBrightness;
		var oldBlurSharpen:Number = unit.effectBlurSharpen;
		var oldEffect:String = unit.effectImage;
		
		_global.Project.CurrentUnit.ApplyEffect(eventObj.contrast, eventObj.brightness, eventObj.blursharp, eventObj.effect);
		var newContrast:Number = unit.effectContrast;
		var newBrightness:Number = unit.effectBrightness;
		var newBlurSharpen:Number = unit.effectBlurSharpen;
		var newEffect:String = unit.effectImage;
		
		var action:Actions.ImageEffectAction = new Actions.ImageEffectAction(unit, oldBrightness, oldContrast, oldBlurSharpen, oldEffect, newBrightness, newContrast, newBlurSharpen, newEffect);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	private function OnToolChanged(eventObj) {
		//_global.tr("ImageUICtrler::OnToolChanged toolName = "+eventObj.toolName);
	    if(eventObj.toolName == "ImageEffects") {
			var data = _global.Project.CurrentUnit.GetEffect();
			imageEffects.DataBind(data);
		}
		
	}
	
	public function Clear() {
		addImage.Clear();
	}
}
