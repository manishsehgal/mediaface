﻿import mx.core.UIObject;
import mx.utils.Delegate;
[Event("exit")]
class Tools.ImageTool.ImageEffectsContainer extends UIObject {
	private var lblImageEffects:CtrlLib.LabelEx;
	private var btnDelete:CtrlLib.ButtonEx;
	private var btnApply:CtrlLib.ButtonEx;
	private var btnBack:CtrlLib.ButtonEx;
	private var sldBlurSharp:CtrlLib.SliderEx;
	private var sldContrast:CtrlLib.SliderEx;
	private var sldBrightness:CtrlLib.SliderEx;
	private var btnNormal,btnGrey,btnSepia:CtrlLib.RadioButtonEx;
	function ImageEffectProperties() {
		/*setStyle("styleName", "ToolPropertiesPanel");
		this.btnDelete.setStyle("styleName", "ToolPropertiesActiveButton");
		this.lblImageEffects.setStyle("styleName", "ToolPropertiesCaption");
		/*this.cbFont.setStyle("styleName", "ToolPropertiesCombo");
		this.cbFontSize.setStyle("styleName", "ToolPropertiesCombo");
		this.txtText.setStyle("styleName", "ToolPropertiesInputText");
		this.chkBold.setStyle("styleName", "ToolPropertiesCheckBoxBold");
		this.chkItalic.setStyle("styleName", "ToolPropertiesCheckBox");
		/*this.btnAlignLeft.setStyle("styleName", "ToolPropertiesCheckBox");
		this.btnAlignCenter.setStyle("styleName", "ToolPropertiesCheckBox");
		this.btnAlignRight.setStyle("styleName", "ToolPropertiesCheckBox");*/
/*		this.btnDelete.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnTextEffect.setStyle("styleName", "ToolPropertiesActiveButton");
		this.lblEditText.setStyle("styleName", "ToolPropertiesCaption");*/
	}
	
	function onLoad() {
		btnApply.RegisterOnClickHandler(this,OnApply);
		btnBack.RegisterOnClickHandler(this,OnBack);
		btnDelete.RegisterOnClickHandler(this, _global.DeleteCurrentUnit);
		_global.LocalHelper.LocalizeInstance(sldBlurSharp,"ToolProperties","IDS_LBLBLURSHARP");
		_global.LocalHelper.LocalizeInstance(sldContrast,"ToolProperties","IDS_LBLCONTRAST");
		_global.LocalHelper.LocalizeInstance(sldBrightness,"ToolProperties","IDS_LBLBRIGHTNESS");
		_global.LocalHelper.LocalizeInstance(lblImageEffects,"ToolProperties","IDS_LBLIMAGEEFFECTS");
		_global.LocalHelper.LocalizeInstance(btnNormal,"ToolProperties","IDS_LBLIMAGENORMAL");
		_global.LocalHelper.LocalizeInstance(btnGrey,"ToolProperties","IDS_LBLIMAGEGREY");
		_global.LocalHelper.LocalizeInstance(btnSepia,"ToolProperties","IDS_LBLIMAGESEPIA");
		_global.LocalHelper.LocalizeInstance(btnApply,"ToolProperties","IDS_BTNAPPLY");
		_global.LocalHelper.LocalizeInstance(btnBack,"ToolProperties","IDS_BTNBACK");
		TooltipHelper.SetTooltip2(btnApply, "IDS_TOOLTIP_APPLYIMAGEEFFECT");
		
		_global.GlobalNotificator.OnComponentLoaded("Tools.ImageEffects", this);
		trace("sldBlurSharp.text "+sldBlurSharp.text);
		
		sldBlurSharp.text = "Blur <-> Sharp";
		sldContrast.text = "Contrast";
		sldBrightness.text = "Brightness";
		sldBrightness.Max = 10;
		sldContrast.Max = 10;
		sldBlurSharp.Max = 10;
		sldBrightness.Min = -10;
		sldContrast.Min = -10;
		sldBlurSharp.Min = -10;
		sldBrightness.Step = 1;
		sldContrast.Step = 1;
		sldBlurSharp.Step = 1;
	}

	//region back event
	private function OnBack() {
		var eventObject = {type:"back", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnBackHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("back", Delegate.create(scopeObject, callBackFunction));
    }
	function OnApply(){
		_global.tr("ImageEffectctsContainer onApply");
		var brightness:Number = sldBrightness.Points; 
		var contrast:Number = sldContrast.Points;
		var blursharp:Number = sldBlurSharp.Points;
		var effect:String = "Normal";
		if(btnGrey.selected)
			effect = "Grey";
		else if(btnSepia.selected)
			effect = "Sepia";
		else
			effect = "Normal";
		ApplyEffect(contrast, brightness, blursharp, effect);
	}
	function DataBind(settings){
		//_global.tr("ImageEffectctsContainer DataBind")
		//for(var i in settings)
		//	_global.tr(" i = "+i+" ===> "+settings[i]);
		//var settings:Object = _global.CurrentUnit.GetEffect();
		sldBlurSharp.Points = parseInt(settings.blursharp);
		sldContrast.Points = parseInt(settings.contrast);
		sldBrightness.Points = parseInt(settings.brightness);
		if(settings.effect == "Normal")
			btnNormal.selected = true;
		else if(settings.effect == "Grey")
			btnGrey.selected = true;
		else if(settings.effect == "Sepia")
			btnSepia.selected = true;
	}
	//endregion
	function ApplyEffect(contrast:Number ,brightness:Number ,blursharp:Number ,effect:String) {
		_global.tr("ImageEffectctsContainer ApplyEffect");
		var eventObject = {type:"imageeffectapply", contrast:contrast, brightness:brightness,
							blursharp:blursharp, effect:effect};
		this.dispatchEvent(eventObject);
	}
	
	public function RegisterOnApplyEffectHandler(scopeObject:Object, callBackFunction:Function) {
		_global.tr("ImageEffectctsContainer RegisterOnApplyEffectHandler");
		this.addEventListener("imageeffectapply", Delegate.create(scopeObject, callBackFunction));
    }
}
