﻿import mx.core.UIObject;
import mx.utils.Delegate;

class Tools.ImageTool.ImageToolContainer extends UIObject {
	
	private var accImageTools: CtrlLib.AccordionEx;
	
	function ImageToolContainer() {
		instance = this;
	}
	
	static private var instance   : Tools.ImageTool.ImageToolContainer;
	static function getInstance() : Tools.ImageTool.ImageToolContainer {
		return instance;
	}
	
	function onLoad() {
		accImageTools.setStyle("backgroundColor", "0xf7f7f7");
		accImageTools.RegisterOnTabClickHandler(this, onToolClick);
		_global.tr("Tools.ImageTool.ImageToolContainer onLoad = "+this );
		_global.GlobalNotificator.OnComponentLoaded("Tools.ImageToolContainer", this);
	}
	
	private function onToolClick(eventObject) {
		var toolName = accImageTools.getChildAt(eventObject.newValue)._name;
		var eventObject = {type:"toolClick", target:this, toolName: toolName};
		this.dispatchEvent(eventObject);
		_global.tr("Tools.ImageTool.ImageToolContainer onToolClick "+toolName);
	}
	
	public function RegisterOnToolClickHandler(scopeObject:Object, callBackFunction:Function) {
		//accImageTools.RegisterOnTabClickHandler(scopeObject, callBackFunction);
		this.addEventListener("toolClick", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function SelectTool(toolName:String) {
		var length:Number = accImageTools.numChildren;
		for(var index:Number = 0; index < length; ++index) {
			if (toolName == accImageTools.getChildAt(index)._name) {
				if (accImageTools.selectedIndex != index)
					accImageTools.selectedIndex = index;
				break;
			}
		}
	}
	
	public function AdjustHeight(height:Number) {
		this.accImageTools.setSize(this.accImageTools.width, height);
	}

}