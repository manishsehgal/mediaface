﻿
class Tools.GradientTool.GradientToolUIController {
	
	private var gradientAddTool : Tools.GradientTool.GradientAddToolContainer;
	private var gradientEditTool : Tools.GradientTool.GradientEditPropertiesClip;
	
	public function GradientToolUIController() {
	}
	
	function OnComponentLoaded(compName:String, compObj:Object, param) {
		switch(compName.split(".")[1]) {
			case "GradientAddTool": {
				gradientAddTool = Tools.GradientTool.GradientAddToolContainer(compObj);
				gradientAddTool.RegisterOnAddHandler(this, gradientAddTool_OnAdd);
				break;
			}
			case "GradientEditTool": {
				gradientEditTool = Tools.GradientTool.GradientEditPropertiesClip(compObj);
				gradientEditTool.RegisterOnDataChangedHandler(this, gradientEditTool_OnDataChanged);
				break;
			}
		}
	}

	public function RegisterOnDeleteUnitHandler(scopeObject:Object, callBackFunction:Function) {
		gradientEditTool.RegisterOnDeleteUnitHandler(scopeObject, callBackFunction);
	}
	
	function AddDataBind():Void {
		gradientAddTool.DataSource = _global.Gradients.GetGradientsArray();
		gradientAddTool.DataBind();
		_global.Mode = _global.GradientMode;
	}
	
	function EditDataBind():Void {
		var unit = _global.Project.CurrentUnit;
		var data:Object = new Object();
		data.color1 = unit.Color1;
		data.color2 = unit.Color2;
		data.autofitType = unit.AutofitType;
		gradientEditTool.DataBind(data);
	}
	
	private function gradientAddTool_OnAdd(eventObject) {
		SATracking.AddGradient();
		_global.Mode = _global.GradientEditMode;
		_global.Project.CurrentPaper.CurrentFace.CreateNewGradientUnit(eventObject.gradientType);
	}
	
	private function gradientEditTool_OnDataChanged(eventObject) {
		var data:Object = eventObject.data;
		if (data.color1 != undefined) {
			_global.Project.CurrentUnit.Color1 = data.color1;
		}
		if (data.color2 != undefined) {
			_global.Project.CurrentUnit.Color2 = data.color2;
		}
		if (data.autofitType != undefined) {
			_global.Project.CurrentPaper.CurrentFace.AutofitCurrentUnit(data.autofitType);
		}
	}
	
}