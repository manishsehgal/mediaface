﻿import mx.utils.Delegate;
import mx.core.UIObject;


class Tools.GradientTool.GradientEditPropertiesClip extends UIObject {
	
	private var lblColor1:CtrlLib.LabelEx;
	private var lblColor2:CtrlLib.LabelEx;
	
	private var btnDelete:CtrlLib.ButtonEx;
	
	private var colorTool1:Tools.ColorSelectionTool.ColorTool;
	private var colorTool2:Tools.ColorSelectionTool.ColorTool;
	private var color1:Number;
	private var color2:Number;
	
	private var optFitToFace:CtrlLib.RadioButtonEx;
	private var optFillVertically:CtrlLib.RadioButtonEx;
	private var optFillHorizontally:CtrlLib.RadioButtonEx;
	private var optThumbnail:CtrlLib.RadioButtonEx;
	
	function GradientEditPropertiesClip() {
	}
	
	function onLoad() {
		colorTool1.RegisterOnSelectHandler(this, colorTool1_OnSelect);
		colorTool2.RegisterOnSelectHandler(this, colorTool2_OnSelect);
		optFitToFace.addEventListener("click", Delegate.create(this, optFitToFace_OnClick));
		optFillVertically.addEventListener("click", Delegate.create(this, optFillVertically_OnClick));
		optFillHorizontally.addEventListener("click", Delegate.create(this, optFillHorizontally_OnClick));
		optThumbnail.addEventListener("click", Delegate.create(this, optThumbnail_OnClick));
		
		InitLocale();
		_global.GlobalNotificator.OnComponentLoaded("Tools.GradientEditTool", this);
	}
	
	public function RegisterOnDeleteUnitHandler(scopeObject:Object, callBackFunction:Function) {
		btnDelete.RegisterOnClickHandler(scopeObject, callBackFunction);
	}

	public function DataBind(data:Object):Void {
		color1 = data.color1;
		colorTool1.SetColorState(color1);
		color2 = data.color2;
		colorTool2.SetColorState(color2);
		SelectAutofitButton(data.autofitType);
	}

	private function InitLocale() {
		TooltipHelper.SetTooltip2(btnDelete, "IDS_TOOLTIP_DELETE");
    }
	
	private function SelectAutofitButton(autofitType:String) {
		optFitToFace.selected        = (autofitType == _global.FitToFaceAutofit);
		optFillVertically.selected   = (autofitType == _global.VerticalAutofit);
		optFillHorizontally.selected = (autofitType == _global.HorizontalAutofit);
		optThumbnail.selected        = (autofitType == _global.ThumbailAutofit);
	}
	
	private function colorTool1_OnSelect(eventObject) {
		var data:Object = new Object();
		data.color1 = eventObject.color;
		OnDataChanged(data);
	}
	
	private function colorTool2_OnSelect(eventObject) {
		var data:Object = new Object();
		data.color2 = eventObject.color;
		OnDataChanged(data);
	}
	
	private function optFitToFace_OnClick(eventObject) {
		var data:Object = new Object();
		data.autofitType = _global.FitToFaceAutofit;
		OnDataChanged(data);
	}
	
	private function optFillVertically_OnClick(eventObject) {
		var data:Object = new Object();
		data.autofitType = _global.VerticalAutofit;
		OnDataChanged(data);
	}
	
	private function optFillHorizontally_OnClick(eventObject) {
		var data:Object = new Object();
		data.autofitType = _global.HorizontalAutofit;
		OnDataChanged(data);
	}
	
	private function optThumbnail_OnClick(eventObject) {
		var data:Object = new Object();
		data.autofitType = _global.ThumbailAutofit;
		OnDataChanged(data);
	}
	
	private function OnDataChanged(data:Object) {
		var eventObject = {type:"dataChanged", target:this, data:data};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnDataChangedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("dataChanged", Delegate.create(scopeObject, callBackFunction));
    }
}
