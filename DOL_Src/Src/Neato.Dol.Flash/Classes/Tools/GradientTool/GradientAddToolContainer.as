﻿import mx.utils.Delegate;
[Event("add")]

class Tools.GradientTool.GradientAddToolContainer extends mx.core.UIComponent {
	
	static var symbolName:String = "GradientAddToolContainer";
	static var symbolOwner:Object = Tools.GradientTool.GradientAddToolContainer;
	var className:String = "GradientAddToolContainer";

	private var boundingBox_mc  : MovieClip;
	private var ctlGradientType;

	private var xGap:Number = 4.5;
	private var yGap:Number = 4;

	function GradientAddToolContainer() {
		super();
	}

	function init():Void {
		super.init();

		if (boundingBox_mc != undefined) {
			boundingBox_mc._width = 0;
			boundingBox_mc._height = 0;
			boundingBox_mc._visible = false;
		}
	}
    
	function createChildren():Void {
		super.createChildren();
		if (ctlGradientType == undefined) {
			ctlGradientType = this.createObject("HListEx", "ctlGradientType", this.getNextHighestDepth());
		}
	}
    
	private function onLoad() {
		InitLocale();
		_global.GlobalNotificator.OnComponentLoaded("Tools.GradientAddTool", this);
		ctlGradientType.RegisterOnChangeHandler(this, ctlGradientType_OnChange);
	}

    private function InitLocale() {
		//_global.LocalHelper.LocalizeInstance(lblAddShapeText, "ToolProperties", "IDS_LBLADDSHAPETEXT");
		//TooltipHelper.SetTooltip(btnCancel, "ToolProperties", "IDS_TOOLTIP_CANCEL");
    }

	public var DataSource:Array;

	public function DataBind() {
		ctlGradientType.DataSource = DataSource;
		ctlGradientType.ItemLinkageName = "DrawingHolderEx";
		ctlGradientType.XGap = xGap;
		ctlGradientType.DataBind();
	}

	private function ctlGradientType_OnChange(eventObject) {
		if (!isNaN(eventObject.index)) {
			OnAdd(eventObject.index);
		}
	}

	private function OnAdd(index) {
		var gradientType = DataSource[index].name;
		var eventObject = {type:"add", target:this, gradientType:gradientType};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnAddHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("add", Delegate.create(scopeObject, callBackFunction));
    }
}
