﻿import CtrlLib.HListEx;
import CtrlLib.LabelEx;
import CtrlLib.ButtonEx;
import mx.utils.Delegate;
[Event("exit")]
class Tools.ChangePaperTool.ChangePaperTool extends mx.core.UIObject {	
	private var lblSelect:LabelEx;
    private var ctlPapers:HListEx;
	private var btnCurrentPaper:ButtonEx;
	
	private var boundingBox_mc:MovieClip;

    private var papersXml:XML;
	
	function init():Void {
        super.init();

        if (boundingBox_mc != undefined) {
            boundingBox_mc._width = 0;
            boundingBox_mc._height = 0;
            boundingBox_mc._visible = false;
        }
    }

	function createChildren():Void {
		super.createChildren();
	}
	
    function size():Void {
        super.size();
        ctlPapers.invalidate();
    }

	function onLoad() {
		this.setStyle("styleName", "ChangePaperTool");
		this.lblSelect.setStyle("styleName", "ChangePaperToolText");
		this.ctlPapers.setStyle("styleName", "ChangePaperTool");
		btnCurrentPaper.setStyle("styleName", "ChangePaperToolText");

		_global.GlobalNotificator.OnComponentLoaded("Tools.ChangePaperTool", this);		
		_global.LocalHelper.LocalizeInstance(lblSelect, "ToolProperties", "IDS_LBLSELECTPAPER");
		ctlPapers.RegisterOnChangeHandler(this, ctlPapers_OnChange);
		btnCurrentPaper.RegisterOnClickHandler(this, btnCurrentPaper_OnClick);
		
		TooltipHelper.SetTooltip2(btnCurrentPaper, "IDS_TOOLTIP_SELECTCURRENTPAPER");
	}
	
	public function DataBind() {
		papersXml = new XML();
		papersXml.ignoreWhite = true;
		var paperService:SAPapers = SAPapers.GetInstance(papersXml);
        paperService.RegisterOnLoadedHandler(this, OnXmlLoad);
		paperService.BeginLoad();
		_global.MainWindow.Mode = "changePaper";
	}
	
	private function OnXmlLoad(eventObject) {
		BindPapers();
	}
	
	private function BindPapers() {
		ctlPapers.DataSource = CreateListData(papersXml.firstChild);
		ctlPapers.ItemLinkageName = "SignImageHolderEx";
		ctlPapers.DataBind();
	}
    
    private function CreateListData(node:XMLNode):Array {
	    var papers:Array = new Array();

        for (var child:XMLNode = node.firstChild; child != null; child = child.nextSibling) {
            if (child.nodeName == "item") {
                var idAttribute:String = child.attributes["id"];
                var nameAttribute:String = child.attributes["name"];
                papers.push({
                    name:nameAttribute, id:idAttribute, url:_global.PaperIconUrlFormat + idAttribute});
            }
        }
		
		return papers;
    }
    
	private function ctlPapers_OnChange(eventObject) {
       if (!isNaN(eventObject.index)) {
            var paperId:Number = ctlPapers.DataSource[eventObject.index].id;
            var paperService:SAPapers = SAPapers.GetInstance();
            paperService.RegisterOnFacesLoadedHandler(this, DrawPaper);
            paperService.LoadPaperFaces(paperId);
       }
	}
    
	private function DrawPaper(eventObject) {
		_global.UIController.ToolsController.changePaperToolController.DrawPaper(eventObject.result);
    }
	
	private function btnCurrentPaper_OnClick() {
  		_global.UIController.ToolsController.changePaperToolController.SelectCurrentPaper();
	}
}