﻿import CtrlLib.LabelEx;
import CtrlLib.TextAreaEx;
import CtrlLib.ButtonEx;
import mx.utils.Delegate;

class Tools.ChangePaperTool.ChangePaperPanel extends MovieClip {
	private var lblSource:LabelEx;
	private var lblDesignFrom:TextAreaEx;
	private var lblTarget:LabelEx;
	private var lblDesignTo:TextAreaEx;
	public var btnOk:ButtonEx;
	private var btnCancel:ButtonEx;
	
	private var currentPaperClip:MovieClip;
	private var selectedPaperClip:MovieClip;
	private var selectedPaperClip2:MovieClip;
  
    private var maxWidth:Number = 220;
    private var maxHeight:Number = 300;
    private var hOffset:Number = 230;
	
	private var indexFaceCurrentPaper:Number;
	private var selectedPaper;
	private var facesXml:XML;

    private static var instance:Tools.ChangePaperTool.ChangePaperPanel = undefined;
	public static function GetInstance():Tools.ChangePaperTool.ChangePaperPanel {
		return instance;
	}
	
    function ChangePaperPanel() {
        instance = this;
		_parent._parent.setStyle("styleName", "ChangePaperPanel");
		lblSource.setStyle("styleName", "ChangePaperSourceTargetText");
		lblDesignFrom.setStyle("styleName", "ChangePaperDesignText");
		lblTarget.setStyle("styleName", "ChangePaperSourceTargetText");
		lblDesignTo.setStyle("styleName", "ChangePaperDesignText");
		btnOk.setStyle("styleName", "ToolPropertiesActiveButton");
		btnCancel.setStyle("styleName", "ToolPropertiesActiveButton");
		
		currentPaperClip = this.createEmptyMovieClip("currentPaperClip", this.getNextHighestDepth());
		selectedPaperClip = this.createEmptyMovieClip("selectedPaperClip", this.getNextHighestDepth());
		selectedPaperClip._visible = false;
		selectedPaperClip2 = this.createEmptyMovieClip("selectedPaperClip2", this.getNextHighestDepth());
		
		selectedPaper = _global.PaperHelper.CreatePaper();
		facesXml = new XML();
	}    
    	
	function onLoad() {
		_global.LocalHelper.LocalizeInstance(lblSource, "ToolProperties", "IDS_LBLSOURCE");
		_global.LocalHelper.LocalizeInstance(lblDesignFrom, "ToolProperties", "IDS_LBLDESIGNFROM");
		_global.LocalHelper.LocalizeInstance(lblTarget, "ToolProperties", "IDS_LBLTARGET");
		_global.LocalHelper.LocalizeInstance(lblDesignTo, "ToolProperties", "IDS_LBLDESIGNTO");
        
        btnOk.RegisterOnClickHandler(this, btnOk_OnClick);
        btnCancel.RegisterOnClickHandler(this, btnCancel_OnClick);

		TooltipHelper.SetTooltip2(btnOk, "IDS_TOOLTIP_YES");
		TooltipHelper.SetTooltip2(btnCancel, "IDS_TOOLTIP_NO");
        
		lblDesignTo.visible = false;
        lblTarget.visible = false;
    }
	
	function Hide() {
		facesXml = new XML();
		_global.PaperHelper.Clear(selectedPaperClip);
		_global.PaperHelper.Clear(selectedPaperClip2);
		_global.PaperHelper.Clear(currentPaperClip);
		
        _root.pnlMainLayer.content.pnlChangePaperLayer._visible = false;
		_global.MainWindow.Mode = "design";
		Tools.ToolContainer.getInstance().ShowFillProperties();
  	}
	
	function Show() {
        _root.pnlMainLayer.content.pnlChangePaperLayer._visible = true;
		
        lblDesignTo.visible = false;
        lblTarget.visible = false;

		var currentPaper:Entity.Paper = _global.Project.CurrentPaper;
		var w:Number = currentPaper.Width;
		var h:Number = currentPaper.Height;		
		var faces = currentPaper.Faces;
		indexFaceCurrentPaper = 0;		
		for (var i = 0; i < faces.length; ++i) {
			if(faces[i] == currentPaper.CurrentFace) {
				indexFaceCurrentPaper = i;
				break;
			}
		}
		_global.PaperHelper.Clear(currentPaperClip);
		_global.PaperHelper.Draw(currentPaperClip, this, ChangeFaceCurrentPaper, true, false, 
									faces, w, h, false, false, true, false);
		
		
		var kx:Number = maxWidth / w;
		var ky:Number = maxHeight / h;
		var k:Number = kx <= ky ? kx : ky;
		currentPaperClip._xscale = currentPaperClip._yscale = k * 100;
		currentPaperClip._x = (lblDesignFrom._x + lblDesignTo._width/2) - w*k/2;
		currentPaperClip._y = hOffset - h*k/2;
	}
    
	function DrawPaper(facesXmlBody:String){
        lblDesignTo.visible = true;
        lblTarget.visible = true;

        facesXml.parseXML(facesXmlBody);

        selectedPaper.ReadFaces(selectedPaperClip, facesXml);
		_global.PaperHelper.Draw(selectedPaperClip2, this, ChangeFaceSelectedPaper, true, false,
			selectedPaper.Faces, selectedPaper.Width, selectedPaper.Height, false, false, true, false);
		ScaleSelectedPaper();
	}
		
	private function ScaleSelectedPaper():Void {
		var w:Number = selectedPaper.Width;
		var h:Number = selectedPaper.Height;
		
		var kx:Number = maxWidth / w;
		var ky:Number = maxHeight / h;
		var k:Number = kx <= ky ? kx : ky;
		selectedPaperClip2._xscale = selectedPaperClip2._yscale = k * 100;
		selectedPaperClip2._x = (lblDesignTo._x + lblDesignTo._width/2) - w*k/2;
		selectedPaperClip2._y = hOffset - h*k/2;
	}
	
	function ChangeFaceCurrentPaper(index:Number){
		indexFaceCurrentPaper = index;
	}
    
	function ChangeFaceSelectedPaper(index:Number){
		var indexFaceSelectedPaper:Number = index;
		var sourceFaces:Array = _global.Project.CurrentPaper.Faces;
		_global.FaceHelper.CopyFace(sourceFaces, selectedPaper.Faces, indexFaceCurrentPaper, indexFaceSelectedPaper);		
		_global.PaperHelper.Draw(selectedPaperClip2, this, ChangeFaceSelectedPaper, true, false,
			selectedPaper.Faces, selectedPaper.Width, selectedPaper.Height, false, false, true, false);
		
		ScaleSelectedPaper();
	}
    
    private function btnOk_OnClick() {
		_global.MessageBox.Confirm(" Current paper will be replaced now. Are you sure?"," Warning", 
								   Delegate.create(this, SelectFaceOk), null);
	}

	public function SelectFaceOk() {
		SAPaperTracking.TrackPaperChange(selectedPaper.paperId);
		if (selectedPaper.paperId == _global.Project.CurrentPaper.PaperId)
			SATracking.CopyDesign();
		else
			SATracking.TransferDesign();
		var xml:XML = selectedPaper.GenerateProjectXml(facesXml);
		var projectService = SAProject.GetInstance(xml);
		projectService.RegisterOnSavedHandler(this, onSaved);
		projectService.BeginSave();
    }
	
	private function onSaved() {
		_global.Project.Load();
		_global.FaceHelper.ChangeFace(0);
		_global.tr("onSavedChangedPaper");
		Hide();
	}
    
    private function btnCancel_OnClick() {
  		Hide();
	}
	
	function SelectCurrentPaper() {
  		lblDesignTo.visible = true;
        lblTarget.visible = true;

        facesXml = _global.Project.GenerateProjectXml();

        selectedPaper.ReadFaces(selectedPaperClip, facesXml);
		_global.PaperHelper.Draw(selectedPaperClip2, this, ChangeFaceSelectedPaper, true, false,
			selectedPaper.Faces, selectedPaper.Width, selectedPaper.Height, false, false, true, false);
		ScaleSelectedPaper();
	}
}