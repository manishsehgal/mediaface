﻿class Tools.ChangePaperTool.ChangePaperToolUIController {
	private var changePaperTool:Tools.ChangePaperTool.ChangePaperTool;
	private var changePaperPanel:Tools.ChangePaperTool.ChangePaperPanel;
	
	public function ChangePaperToolUIController() {
	}
	
	function OnComponentLoaded(compName:String, compObj:Object, param) {
		changePaperTool = Tools.ChangePaperTool.ChangePaperTool(compObj);
		changePaperPanel = Tools.ChangePaperTool.ChangePaperPanel.GetInstance();
	}
	
	function DataBind():Void {
		changePaperPanel.btnOk.enabled = false;
		changePaperTool.DataBind();
		_global.Mode = _global.ChangePaperMode;
	}
	
	function DrawPaper(facesXmlBody:String) {
		changePaperPanel.DrawPaper(facesXmlBody);
		changePaperPanel.btnOk.enabled = true;
	}
	
	function Hide() {
		changePaperPanel.Hide();
	}
	
	function Show() {
		changePaperPanel.Show();
	}
	
	function SelectCurrentPaper() {
		changePaperPanel.SelectCurrentPaper();
		changePaperPanel.btnOk.enabled = true;
	}
}