﻿import mx.core.UIObject;
import mx.controls.*;
import mx.utils.Delegate;
[Event("exit")]
class Tools.TemplateTool.ChooseLayoutContainer extends UIObject {
	private var lblChooseLayout:CtrlLib.LabelEx;
	private var lblApply:CtrlLib.TextAreaEx;
	private var btnCancel:CtrlLib.ButtonEx;
	private var btnContinue:CtrlLib.ButtonEx;
	private var ctlLayoutList:CtrlLib.HListEx;
	private var selectedItemIndex:Number;

	private var dataSource:Array;
	private var layoutData:XML;
	private var faceLayouts:Array;
	private static var imageUrl:String = "DeviceIcon.aspx?";
	private static var FaceLayoutIdParamName:String = "FaceLayoutId";
	private static var LayoutIdParamName :String= "LayoutId";
	private var currentPaperId = 0;

	private var pluginsService : SAPlugins;
	
	function ChooseLayoutContainer() {
		layoutData = new XML();
		faceLayouts = new Array();
		dataSource =  new Array();
		this.lblChooseLayout.setStyle("styleName", "ToolPropertiesCaption");
		this.btnCancel.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnContinue.setStyle("styleName", "ToolPropertiesActiveButton");
		this.lblApply.setStyle("styleName", "TransferPropertiesText");
	}
	
	function onLoad() {
		
		ctlLayoutList.RegisterOnChangeHandler(this, ctlLayoutList_OnChange);
		ctlLayoutList.ItemWidth = ctlLayoutList.ItemHeight = 65;//set in design time
		btnCancel.RegisterOnClickHandler(this,	btnCancel_OnClick);
		btnContinue.RegisterOnClickHandler(this,	btnContinue_OnClick);
		//_global.FaceHelper.RegisterOnFaceChangedHandler(this, DataBind);
		_global.GlobalNotificator.OnComponentLoaded("Tools.ChooseLayoutContainer", this);
		currentPaperId = _global.Project.CurrentPaper.PaperId;
		InitLocale();
		trace('onload');
		pluginsService = SAPlugins.GetInstance();
		pluginsService.RegisterOnModuleAvaliableHandler(this, IsTextEffectsModuleAvaliable);
	}
	
	public function Clear() {
		ctlLayoutList.Clear();
	}
	
	function ContinueShow(b:Boolean)
	{
		btnContinue._visible = b;
		btnCancel._visible = !b;
		
	}
	private function InitLocale() {
		_global.LocalHelper.LocalizeInstance(lblChooseLayout, "ToolProperties", "IDS_LBLCHOOSELAYOUT");
		_global.LocalHelper.LocalizeInstance(lblApply, "ToolProperties", "IDS_LBLAPPLY");
		_global.LocalHelper.LocalizeInstance(btnCancel, "ToolProperties", "IDS_BTNCANCEL");
		_global.LocalHelper.LocalizeInstance(btnContinue, "ToolProperties", "IDS_BTNCONTINUE");
		
		TooltipHelper.SetTooltip(btnCancel, "ToolProperties", "IDS_TOOLTIP_CANCEL");
		TooltipHelper.SetTooltip(btnContinue, "ToolProperties", "IDS_TOOLTIP_CONTINUE");
	}

	private function ctlLayoutList_OnChange(eventObject) {
		_global.tr("LayoutList_OnChange() index="+eventObject.index);
		selectedItemIndex = eventObject.index;
		if (selectedItemIndex == undefined) {
			return;
		}
		var currentFaceLayout:FaceLayout = dataSource[selectedItemIndex];
		if (HasTextEffects(currentFaceLayout)) {
			pluginsService.IsModuleAvailable("TextEffectModule");
		} else {
			ApplyLayout();
		}
	}

	private function IsTextEffectsModuleAvaliable(eventObject) {
		//_global.tr("IsTextEffectsModuleAvaliable eventObject.isAvaliable = " + eventObject.isAvaliable);
		if(eventObject.isAvaliable) {
			ApplyLayout();
		}
	}


	private function ApplyLayout() {
		_global.tr("ApplyLayout() ind="+selectedItemIndex);
		var index:Number = selectedItemIndex;
		if (index == undefined) {
			return;
		}

		var face = _global.Project.CurrentPaper.CurrentFace;
		var oldNode:XML = new XML("<Face/>");
		face.AddUnitNodes(oldNode, true);

		SATracking.ApplyLayout();
		_global.UIController.FramesController.detach();
		var currentFaceLayout:FaceLayout = dataSource[index];
		ContinueShow(true);

		HideOddUnitsFromLastLayout(currentFaceLayout);
		ApplyFormattingToExistentLayoutUnits(currentFaceLayout);
		CreateNonExistentLayoutUnits(currentFaceLayout);
		var hasTextEffectUnits:Boolean = false;
		for (var j = 0; j < _global.Project.CurrentPaper.CurrentFace.units.length; j++) {
			var unit = _global.Project.CurrentPaper.CurrentFace.units[j];
			if(unit instanceof TextEffectUnit) {
				hasTextEffectUnits = true;
			}
		}
		_global.Project.CurrentPaper.CurrentFace.Draw();
		
		var newNode:XML = new XML("<Face/>");
		face.AddUnitNodes(newNode, true);
		var action:Actions.ApplayTemplateAction = new Actions.ApplayTemplateAction(face, oldNode, newNode); 
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	private function GetLayoutPrimitives(layout:FaceLayout):Array {
		var list:Array = new Array();
		
		for(var i = 0; i < layout.Items.length; i++) {

			var item:FaceLayoutItem  = layout.Items[i];
			var rectangle:RectanglePrimitive = new RectanglePrimitive();
			
			rectangle.X = parseFloat(item.X.toString());
			rectangle.Y = parseFloat(item.Y.toString());
			rectangle.W = parseInt(item.Width.toString());
			rectangle.H = parseInt(item.Height.toString());
			
			var angle = (item.Angle) * 2 * Math.PI / 360;
			rectangle.Angle = parseFloat(angle.toString());
			
			list.push(rectangle);
		}

		return list;
	}
	
	private function GetFacePrimitives(face):Array {
		var list:Array = new Array();
		
		for (var i = 0; i < face.contours.length; i++) {
			list = list.concat(face.contours[i].primitives);			
		}
		
		return list;
	}
	
	public function DataBind():Void {
		//_global.tr("ChooseLayoutTool:: DATABIND");
		var parent = this;
		layoutData = new XML();
		layoutData.onLoad = function(success) {
			parent.ParseLayoutData();
			parent.Bind();
			trace("!!!!!Layout data on load");
		};
		
		var currentDate:Date = new Date();
		var url:String = "FaceLayout.aspx?time=" + currentDate.getTime();
		if(SALocalWork.IsLocalWork)
			var url:String = "facelayout.xml";
		trace("Load layout yrl "+url);
		layoutData.load(url);
	}
	
	public function Bind():Void {
		ContinueShow(false);
		dataSource.splice(0);
		
		var defaultLayout:FaceLayout = new FaceLayout();
		
		//defaultLayout.drawSequence = GetFacePrimitives(_global.Project.CurrentPaper.CurrentFace);
		defaultLayout.url=imageUrl+FaceLayoutIdParamName+"="+_global.Project.CurrentPaper.CurrentFace.DbId;
		faceLayouts.push(defaultLayout);
		dataSource.push(defaultLayout);

		for (var i = 0; i < faceLayouts.length; i++) {
			var layout:FaceLayout = faceLayouts[i];
			if (layout.Guid == _global.Project.CurrentPaper.CurrentFace.PUID) {
				//layout.drawSequence = GetFacePrimitives(_global.Project.CurrentPaper.CurrentFace);
				//layout.textSequence = GetLayoutPrimitives(layout);
				layout.url=imageUrl+
							FaceLayoutIdParamName+"="+_global.Project.CurrentPaper.CurrentFace.DbId+"&"+
							LayoutIdParamName+"="+layout.Id;
				dataSource.push(layout);
			}
		}
		
		ctlLayoutList.DataSource = dataSource;
		
		/*if(dataSource.length < 2){
			var bLoaded:Boolean = false;
			var layout:FaceLayout = faceLayouts[i];
			for (var i = 0; i < faceLayouts.length; i++) {
				if(layout.FaceId == _global.Project.CurrentPaper.CurrentFace.DbId)
					bLoaded = true;
			}
			if(bLoaded == false)
					LoadLayout();
			
		}*/
			
		ctlLayoutList.ItemLinkageName = "ImageHolderEx";
		ctlLayoutList.DataBind();
	}
	
	private function ParseFaceLayouts():Void {
		faceLayouts.length = 0;
		for (var i = 0; i < layoutData.firstChild.childNodes.length; i++) {
			var node:XMLNode = layoutData.firstChild.childNodes[i];
			if (node.nodeName == "FaceLayout") {
				var layout:FaceLayout = new FaceLayout();
				layout.Id = parseInt(Utils.XMLGetChildValue(node, "Id"), 10);
				layout.FaceId = parseInt(Utils.XMLGetChildValue(node, "FaceId"), 10);
				layout.Guid = Utils.XMLGetChildValue(node, "Guid");
				faceLayouts.push(layout);
			}
		}
	}

	private function ParseFaceLayoutItems():Void {
		for (var k = 0; k < faceLayouts.length; k++) {
			var layout:FaceLayout = faceLayouts[k];
			layout.Items.length = 0;
			
			for (var i = 0; i < layoutData.firstChild.childNodes.length; i++) {
				var node:XMLNode = layoutData.firstChild.childNodes[i];
				var faceLayoutId = Utils.XMLGetChildValue(node, "FaceLayoutId");

				if (node.nodeName == "FaceLayoutItem" && layout.Id == faceLayoutId) {
					var item:FaceLayoutItem = new FaceLayoutItem();
					item.Id = parseInt(Utils.XMLGetChildValue(node, "Id"), 10);
					item.FaceLayoutId = faceLayoutId;
					item.X = parseFloat(Utils.XMLGetChildValue(node, "X"));
					item.Y = parseFloat(Utils.XMLGetChildValue(node, "Y"));
					item.ScaleX = parseFloat(Utils.XMLGetChildValue(node, "ScaleX"));
					item.ScaleY = parseFloat(Utils.XMLGetChildValue(node, "ScaleY"));
					item.Position = parseInt(Utils.XMLGetChildValue(node, "Position"), 10);
					item.Text = Utils.XMLGetChildValue(node, "Text");
					item.Font = Utils.XMLGetChildValue(node, "Font");
					item.Size = parseInt(Utils.XMLGetChildValue(node, "Size"), 10);
					item.Bold = Utils.XMLGetChildValue(node, "Bold") == "true" ? true : false;
					item.Italic = Utils.XMLGetChildValue(node, "Italic") == "true" ? true : false;
					item.Color = parseInt(Utils.XMLGetChildValue(node, "Color"), 10);
					item.Angle = parseFloat(Utils.XMLGetChildValue(node, "Angle"));
					item.Height = parseFloat(Utils.XMLGetChildValue(node, "Height"));
					item.Width = parseFloat(Utils.XMLGetChildValue(node, "Width"));
					item.Align = Utils.XMLGetChildValue(node, "Align");
					item.UnitType = Utils.XMLGetChildValue(node, "UnitType");
					item.Effect= Utils.XMLGetChildValue(node, "EffectNode");
					item.EmptyLayoutItem = true;
					if (item.Text == undefined) {
					
						item.Text = "Text " + item.Position;
					} 					

					layout.Items.push(item);
				}
			}
		}
	}

	private function ParseLayoutData():Void {
		ParseFaceLayouts();
		ParseFaceLayoutItems();
	}
	
	private function HasTextEffects(currentFaceLayout:FaceLayout):Boolean {
		var count:Number = 0;
		for (var i = 0; i < currentFaceLayout.Items.length; ++ i) {
			var layoutItem:FaceLayoutItem = currentFaceLayout.Items[i];
			if (layoutItem.UnitType == "MultilineTextUnit" && layoutItem.Effect != undefined && layoutItem.Effect != null)
				++ count;
		}
		return count != 0;
	}
		
	private function HideOddUnitsFromLastLayout(currentFaceLayout:FaceLayout):Void {
		/* ???
		var bNoPlaylist:Boolean=true;
		for (var j = 0; j < currentFaceLayout.Items.length; j++) {
			if(currentFaceLayout.Items[i].UnitType == "TableUnit")
			{
				bNoPlaylist = false;
			}
		}
		*/
		var unitsToDelete:Array = new Array();
		for (var i = 0; i < _global.Project.CurrentPaper.CurrentFace.units.length; i++) {
			var unit:Unit = _global.Project.CurrentPaper.CurrentFace.units[i];
			if ((unit instanceof TextUnit || unit instanceof TableUnit || unit instanceof TextEffectUnit)  && !isNaN(unit.LayoutPosition)) {
				var foundItem = false;
				for (var j = 0; j < currentFaceLayout.Items.length; j++) {
					var layoutItem:FaceLayoutItem = currentFaceLayout.Items[j];
					//_global.tr("unit.LayoutPosition = " + unit.LayoutPosition + " layoutItem.Position = " + layoutItem.Position);
					//_global.tr("unit.UnitClassName = " + unit.UnitClassName + " layoutItem.UnitType = " + layoutItem.UnitType);
					if (unit.LayoutPosition == layoutItem.Position && unit.UnitClassName == layoutItem.UnitType) {
						foundItem = true;
						break;
					}
				}

				if (!foundItem) {
					unitsToDelete.push(unit);
				}
			}
		}
		for (var i = 0; i < unitsToDelete.length; i++) {
			//unitsToDelete[i].Visible = false;
			_global.Project.CurrentPaper.CurrentFace.DeleteUnit(unitsToDelete[i]);
		}
	}		
	
	private function ApplyFormattingToExistentLayoutUnits(currentFaceLayout:FaceLayout):Void {
		var bWasPlaylist:Boolean = false;
		//_global.tr("ApplyFormattingToExistentLayoutUnits");
		//_global.tr("currentFaceLayout.Items.length = " + currentFaceLayout.Items.length);
		//_global.tr("_global.Project.CurrentPaper.CurrentFace.units.length = " + _global.Project.CurrentPaper.CurrentFace.units.length);
		for (var i = 0; i < currentFaceLayout.Items.length; i++) {
			var layoutItem:FaceLayoutItem = currentFaceLayout.Items[i];
			//_global.tr("layoutItem = " + layoutItem);
			
			for (var j = 0; j < _global.Project.CurrentPaper.CurrentFace.units.length; j++) {
				//_global.tr("i = " + i + " j = " + j);
				var unit = _global.Project.CurrentPaper.CurrentFace.units[j];
				
				//_global.tr((unit instanceof TextUnit || unit instanceof TextEffectUnit) + " " + (layoutItem.UnitType == "TextUnit" || layoutItem.UnitType == "MultilineTextUnit") + " " + unit.LayoutPosition == layoutItem.Position)
				
				if ((unit instanceof TextUnit || unit instanceof TextEffectUnit) && (layoutItem.UnitType == "TextUnit" || layoutItem.UnitType == "MultilineTextUnit") && unit.LayoutPosition == layoutItem.Position) {
					//_global.tr("ApplyFormattingToExistentLayoutUnits unit.EmptyLayoutItem = " + unit.EmptyLayoutItem);
					unit.Visible = true;
					layoutItem.EmptyLayoutItem = unit.EmptyLayoutItem;
					unit.ApplyLayoutFormat(layoutItem, false);
					if(layoutItem.Effect.length > 0) {
						unit = _global.Project.CurrentPaper.CurrentFace.ConvertTextUnitToEffect(unit,layoutItem.Effect, false, false);
					}
					else if(unit instanceof TextEffectUnit){
						unit = _global.Project.CurrentPaper.CurrentFace.ConvertEffectToTextUnit(unit);
					}
					layoutItem.EmptyLayoutItem = unit.EmptyLayoutItem;
					unit.ApplyLayoutFormat(layoutItem, false);
					break;
				}
				if (unit instanceof TableUnit && layoutItem.UnitType == "TableUnit" && !bWasPlaylist) {
					unit.Visible = true;
					//if(unit.EmptyLayoutItem == true)
						//layoutItem.EmptyLayoutItem = unit.EmptyLayoutItem;
					unit.ApplyLayoutFormat(layoutItem, false);
					unit.EmptyLayoutItem = false;
					bWasPlaylist = true;
					break;
				}
			}
		}
	}
	
	private function CreateNonExistentLayoutUnits(currentFaceLayout:FaceLayout):Void {
		for (var i = 0; i < currentFaceLayout.Items.length; i++) {
			var layoutItem:FaceLayoutItem = currentFaceLayout.Items[i];
			//_global.tr("CreateNonExistentLayoutUnits UT:"+layoutItem.UnitType + " i = " + i);
			if(layoutItem.UnitType == "TextUnit" || layoutItem.UnitType == "MultilineTextUnit"){
				//_global.tr("1");
				var foundItem = false;
				for (var j = 0; j < _global.Project.CurrentPaper.CurrentFace.units.length; j++) {
					var unit = _global.Project.CurrentPaper.CurrentFace.units[j];
				
					if ((unit instanceof TextUnit || unit instanceof TextEffectUnit) && unit.LayoutPosition == layoutItem.Position) {
						foundItem = true;
						break;
					}
				}
			
				if (!foundItem) {
					//_global.tr("no text");
					var textUnit:TextUnit = _global.Project.CurrentPaper.CurrentFace.AddText(false);
					textUnit.Multiline = true;
					textUnit.ApplyLayoutFormat(layoutItem, true);
					if(layoutItem.Effect.length > 0) {
						textUnit = _global.Project.CurrentPaper.CurrentFace.ConvertTextUnitToEffect(textUnit,layoutItem.Effect, false,false);
					}
					textUnit.ApplyLayoutFormat(layoutItem, true);
					textUnit.LayoutPosition = layoutItem.Position;
				}
			}
			else if(layoutItem.UnitType == "TableUnit") {
				//_global.tr("2");
				var foundItem = false;
				for (var j = 0; j < _global.Project.CurrentPaper.CurrentFace.units.length; j++) {
					var unit = _global.Project.CurrentPaper.CurrentFace.units[j];
				
					if (unit instanceof TableUnit ) {
						foundItem = true;
						trace("Table unit found +"+unit);
						break;
					}
				}
			
				if (!foundItem) {
					//_global.tr("No playlist");
					var tableUnit:TableUnit= _global.Project.CurrentPaper.CurrentFace.AddNewPlaylist(null,true,false);
					layoutItem.Text = "";
					tableUnit.ReinitTable(null,true,false);
					tableUnit.ApplyLayoutFormat(layoutItem, true);
					tableUnit.LayoutPosition = layoutItem.Position;
				}
			}
		}
	}
	private function btnCancel_OnClick(){
		OnExit();
	}
	private function btnContinue_OnClick(){
		OnExit();
	}
	//region Exit event
	private function OnExit() {
		var eventObject = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
}
