﻿import mx.utils.Delegate;
import mx.core.UIObject;

[Event("onapplytemplate")]
class Tools.TemplateTool.ChooseTemplateContainer extends mx.core.UIObject {	

	private var lblApply:CtrlLib.TextAreaEx;
	private var ctlTemplateList;

	private var dataSource:Array;
	private var templateData:XML;
	private var Templates:Array;
	private static var imageUrl:String = "DeviceIcon.aspx?";
	private static var templateUrl:String = "Template.aspx?";
    private static var TemplateIdParamName :String = "Template";
	private static var faceIdParamName:String = "FaceId";
	private static var facePUIDParamName:String = "PUID";
	private static var widthParamName:String = "Width";
	private static var heightParamName:String = "Height";
	
	private var lastTemplate:Object;
	
	private var bNoTemplates:Boolean = false;
	
	function ChooseTemplateContainer() {
		//Preloader.Show();
		templateData = new XML();
		lastTemplate = new Object;
		Templates = new Array();
		dataSource =  new Array();
		this.lblApply.setStyle("styleName", "TransferPropertiesText");
	}
	
	function onLoad() {
		
		ctlTemplateList.RegisterOnChangeHandler(this, ctlTemplateList_OnChange);
		ctlTemplateList.ItemWidth = ctlTemplateList.ItemHeight = 65;//set in design time
		_global.FaceHelper.RegisterOnFaceChangedHandler(this, FaceChanged);
		_global.GlobalNotificator.OnComponentLoaded("Tools.ChooseTemplateContainer", this);
		InitLocale();
		trace('onload');
	}
	
	public function Clear() {
		ctlTemplateList.Clear();
	}
	
	private function InitLocale() {
		_global.LocalHelper.LocalizeInstance(lblApply, "ToolProperties", "IDS_LBLAPPLY");
	}
	public function FaceChanged() {
		bNoTemplates = false;
		clearList();
		DataBind();
	}
	
	private function clearList() {
		Templates.length = 0;
		templateData = new XML();
		dataSource.length = 0;
		ctlTemplateList.DataSource = null;
		ctlTemplateList.ItemLinkageName = "ImageHolderEx";
		ctlTemplateList.DataBind();
	}

	private function ctlTemplateList_OnChange(eventObject) {
		if (eventObject.index == undefined) return;
		lastTemplate = dataSource[eventObject.index];
		trace('lasttemplate'+lastTemplate);
		var parent = this;
		var onconfirm:Function = function() {
			_global.UIController.FramesController.detach();
			trace('last template '+parent.lastTemplate+parent.lastTemplate.Id);
			parent.OnApplyTemplate(parent.lastTemplate.Xml,parent.lastTemplate.Id);
		};
		_global.MessageBox.Confirm(
				" Current design will be removed. Are you sure?",
				" Warning",
				onconfirm,
				null);
	}
	
	
	
	public function DataBind():Void {
		trace('_global.Project.CurrentPaper.CurrentFace.DbId'+_global.Project.CurrentPaper.CurrentFace.DbId );
		var parent = this;
		templateData.onLoad = function(success) {
			Templates = new Array();
			parent.ParseTemplateData();
			trace('parent.ParseTemplateData();');
			parent.Bind();			
		};
		
		var currentDate:Date = new Date();
		var url:String = templateUrl+facePUIDParamName+'='+_global.Project.CurrentPaper.CurrentFace.PUID;//+"&time=" + currentDate.getTime();
		if(SALocalWork.IsLocalWork)
			var url:String = "Template.xml";
		trace("Load template yrl "+url);
		templateData.load(url);
	}
	
	public function Bind():Void {
		if(bNoTemplates == true)
		{
			clearList();
			return;
		}
		dataSource.length = 0;
   	    for (var i = 0; i < Templates.length; i++) {
			var templ:Object = Templates[i];
				templ.url=imageUrl+	TemplateIdParamName+"="+templ.Id + "&" + widthParamName + "=" + ctlTemplateList.ItemWidth + "&" + heightParamName + "=" + ctlTemplateList.ItemHeight;
				trace(templ.url+'TemplateDatabind url');
				dataSource.push(templ);
			
		}
		ctlTemplateList.DataSource = dataSource;
		ctlTemplateList.ItemLinkageName = "ImageHolderEx";
		ctlTemplateList.DataBind();
		//Preloader.Hide();
	}
	
	private function ParseTemplates():Void {
		Templates.length = 0;
		if(templateData.firstChild.childNodes.length < 1) {
			bNoTemplates = true;
			return;
		}
		else 
		{
			bNoTemplates = false;
		}
		for (var i = 0; i < templateData.firstChild.childNodes.length; i++) {
			trace('i'+i);
			var node:XMLNode = templateData.firstChild.childNodes[i];
			trace(node.nodeName);
			if (node.nodeName == "Template") {
				var templ:Object = new Object();
				templ.Id = Number(node.attributes.id);//node.
				templ.FaceId = node.attributes.faceId;
				templ.Xml = node.firstChild;
				Templates.push(templ);
			}
		}
		
	}

	private function ParseTemplateData():Void {
		ParseTemplates();
	}
	
	//region ApplyTemplate event
	private function OnApplyTemplate(template,id) {
		var eventObject = {type:"onapplytemplate", target:this, template:template, templateId:id};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnApplyTemplateHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onapplytemplate", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion

	
}