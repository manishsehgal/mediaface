﻿
class Tools.TemplateTool.TemplateToolUIController {
	private var templateTool:Tools.TemplateTool.ChooseTemplateContainer;
	private var layoutTool:Tools.TemplateTool.ChooseLayoutContainer;
	private var templateContainer:Tools.TemplateTool.TemplateToolContainer;
	
	public function TemplateToolUIController() {
		trace("TemplateToolUIController ctr");
	}
	
	function OnComponentLoaded(compName:String, compObj:Object, param) {
		trace("TemplateTool OnComponentLoaded");
		var tool:String = compName.split(".")[1];
		switch(tool){
			case 'ChooseLayoutContainer':
				layoutTool = Tools.TemplateTool.ChooseLayoutContainer(compObj);
				break;
			case 'ChooseTemplateContainer':
				templateTool = Tools.TemplateTool.ChooseTemplateContainer(compObj);
				templateTool.RegisterOnApplyTemplateHandler(this, OnApplyTemplate);
				trace('RegisterOnApplyTemplateHandler'+templateTool.RegisterOnApplyTemplateHandler);
				break;
			case 'TemplateToolContainer':
				trace('TemplateToolContainer In UIController');
				templateContainer = Tools.TemplateTool.TemplateToolContainer(compObj);
				break;
				
		}
		/*fillTool = Tools.FillTool.FillTool(compObj);
		fillTool.RegisterOnClickHandler(this, colorTool_OnClick);
		fillTool.RegisterOnSelectHandler(this, colorTool_OnSelect);*/
	}
	
	public function DataBind():Void {
		trace('Templatetooluicontroller + databind'+templateContainer);
		templateContainer.SelectTool('Layout');
		templateTool.DataBind();
		layoutTool.DataBind();
	}
	
	public function OnApplyTemplate(eventObj){
		var face = _global.Project.CurrentPaper.CurrentFace;
		var oldNode:XML = new XML("<Face/>");
		face.AddUnitNodes(oldNode, true);

		SATracking.ApplyTemplate();
		var template:XMLNode = eventObj.template;
		var id:Number = eventObj.templateId;
		_global.UIController.UnselectCurrentUnit();
		_global.Project.CurrentPaper.CurrentFace.Clear();
		
		_global.Project.CurrentPaper.CurrentFace.ParseXMLUnits(template,id);
		_global.Project.CurrentPaper.CurrentFace.Draw();
		
		var newNode:XML = new XML("<Face/>");
		face.AddUnitNodes(newNode, true);
		var action:Actions.ApplayTemplateAction = new Actions.ApplayTemplateAction(face, oldNode, newNode); 
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	public function Clear() {
		layoutTool.Clear();
		templateTool.Clear();
	}
}