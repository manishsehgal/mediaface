﻿import mx.core.UIObject;
import mx.utils.Delegate;

class Tools.TemplateTool.TemplateToolContainer extends UIObject {
	
	private var accTemplateTools: CtrlLib.AccordionEx;
	
	function TemplateToolContainer() {
		instance = this;
	}
	
	static private var instance   : Tools.TemplateTool.TemplateToolContainer;
	static function getInstance() : Tools.TemplateTool.TemplateToolContainer {
		return instance;
	}
	
	function onLoad() {
		accTemplateTools.setStyle("backgroundColor", "0xf7f7f7");
		accTemplateTools.RegisterOnTabClickHandler(this, onToolClick);

		_global.GlobalNotificator.OnComponentLoaded("Tools.TemplateToolContainer", this);
	}
	
	private function onToolClick(eventObject) {
		var toolName = accTemplateTools.getChildAt(eventObject.newValue)._name;
		var eventObject = {type:"toolClick", target:this, toolName: toolName};
		this.dispatchEvent(eventObject);
	}
	
	public function RegisterOnToolClickHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("toolClick", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function SelectTool(toolName:String) {
		var length:Number = accTemplateTools.numChildren;
		for(var index:Number = 0; index < length; ++index) {
			if (toolName == accTemplateTools.getChildAt(index)._name) {
				if (accTemplateTools.selectedIndex != index)
					accTemplateTools.selectedIndex = index;
				break;
			}
		}
	}
	
	public function AdjustHeight(height:Number) {
		this.accTemplateTools.setSize(this.accTemplateTools.width, height);
	}

}