﻿import mx.utils.Delegate;
import mx.core.UIObject;
import mx.controls.UIScrollBar;

[Event("exit")]
[Event("EndPainting")]
class Tools.PaintTool.PaintAddToolContainer extends UIObject {
	private var chkKeepProportions:CtrlLib.CheckBoxEx;
	private var btnDelete:CtrlLib.ButtonEx;
	private var btnRestoreProportions:CtrlLib.ButtonEx;
	private var lblBrushColor:CtrlLib.LabelEx;
	private var btnContinueAndClose:CtrlLib.ButtonEx;
	private var btnUndo:CtrlLib.ButtonEx;
	private var ctlBrushType;
	private var scrollBrushWeight:UIScrollBar;
	private var defaultBrushes:Array;
	private var selectedBrushesGroup:Array;
	private var lblBrushWeight:CtrlLib.LabelEx;
		
	private var lineColorTool:Tools.ColorSelectionTool.ColorTool;
	//private var lineColor:Number;

	private var xGap:Number = 8;
	
	public function PaintAddToolContainer() {
		trace("PaintAddToolContainer cnstruct");
	}
	
	private function onLoad() {
		btnDelete.visible = false;
		chkKeepProportions.addEventListener("click", Delegate.create(this, chkKeepProportions_OnClick));
		btnRestoreProportions.addEventListener("click", Delegate.create(this, btnRestoreProportions_OnClick));
		btnContinueAndClose.addEventListener("click", Delegate.create(this, btnContinueAndClose_OnClick));
		btnUndo.addEventListener("click", Delegate.create(this, btnUndo_OnClick));
		lineColorTool.RegisterOnSelectHandler(this, lineColorTool_OnSelect);
		scrollBrushWeight.addEventListener("scroll", Delegate.create(this, scrollBrushWeight_OnScroll));
		
		if (ctlBrushType == undefined) {
			ctlBrushType = this.createObject("HList", "ctlBrushType", this.getNextHighestDepth());
		}
			
		defaultBrushes = _global.Brushes.GetBrushDefaultsArray();
		ctlBrushType.RegisterOnChangeHandler(this, ctlBrushType_OnChange);
		//ctlBrushType.RegisterOnApplyHandler(this, ctlBrushType_OnApply);
		_global.GlobalNotificator.OnComponentLoaded("Tools.PaintAddTool", this);
		DataBind();
		InitLocale();
	}

	public function RegisterOnDeleteUnitHandler(scopeObject:Object, callBackFunction:Function) {
		btnDelete.RegisterOnClickHandler(scopeObject, callBackFunction);
	}

	public function DataBind(data):Void {
		trace("PaintAddToolContainer.DataBind Mode = " + _global.Mode);
		chkKeepProportions.selected = data.bKeepProps;//_global.CurrentUnit.KeepProportions;
		chkKeepProportions.enabled = data.bEnabledKeepProps;//_global.CurrentUnit.IsNonScalable;
		
		var lastUsedBrushIndex = 0;
		if (ctlBrushType.SelectedItemIndex>0) lastUsedBrushIndex = ctlBrushType.SelectedItemIndex;
		
		ctlBrushType.DataSource = defaultBrushes;
		ctlBrushType.ItemLinkageName = "DrawingHolderEx";
		ctlBrushType.XGap = xGap;
		ctlBrushType.DataBind();
		ctlBrushType.SelectedItemIndex = lastUsedBrushIndex;
		
		if (lineColorTool.RGB == undefined) lineColorTool.SetColorState(0);
		
		//set brush and color for new paint unit
		//_global.CurrentUnit.PaintColor = 
		OnColor(lineColorTool.RGB);
		//_global.CurrentUnit.Brush = 
		OnBrush(defaultBrushes[ctlBrushType.SelectedItemIndex]);
	}

	private function InitLocale() {
		//_global.LocalHelper.LocalizeInstance(btnDelete, "ToolProperties", "IDS_BTNDELETE");
		_global.LocalHelper.LocalizeInstance(chkKeepProportions, "ToolProperties", "IDS_CHKKEEPPROPORTIONS");
		_global.LocalHelper.LocalizeInstance(btnRestoreProportions, "ToolProperties", "IDS_BTNRESTOREPROPORTIONS");
		_global.LocalHelper.LocalizeInstance(lblBrushColor, "ToolProperties", "IDS_LBLBRUSHCOLOR");
		//_global.LocalHelper.LocalizeInstance(btnContinueAndClose, "ToolProperties", "IDS_BTNCONTINUE");
		//_global.LocalHelper.LocalizeInstance(btnUndo, "ToolProperties", "IDS_BTNUNDO");
		_global.LocalHelper.LocalizeInstance(lblBrushWeight, "ToolProperties", "IDS_LBLBRUSHWEIGHT");
		
		TooltipHelper.SetTooltip2(btnDelete, "IDS_TOOLTIP_DELETE");
		TooltipHelper.SetTooltip2(btnContinueAndClose, "IDS_TOOLTIP_CONTINUE");
		TooltipHelper.SetTooltip2(btnUndo, "IDS_TOOLTIP_UNDO");
		TooltipHelper.SetTooltip2(chkKeepProportions, "IDS_TOOLTIP_KEEPPROPORTIONS");
		TooltipHelper.SetTooltip2(btnRestoreProportions, "IDS_TOOLTIP_RESTOREPROPORTIONS");
    }
	
	private function lineColorTool_OnSelect(eventObject) {
		OnColor(eventObject.color);
		//_global.CurrentUnit.PaintColor = eventObject.color;
	}
	
	//Actions
	private function ctlBrushType_OnChange(eventObject) {
		//find default brush for the group and set it
		var brush = defaultBrushes[ctlBrushType.SelectedItemIndex];
		//_global.CurrentUnit.Brush = 
		OnBrush(brush);
		
		//fill current group
		selectedBrushesGroup = _global.Brushes.GetBrushesArrayByGroup(brush.group);
		
		//set up size control
		scrollBrushWeight.setScrollProperties(1, 0, selectedBrushesGroup.length-1);
		var positionInGroup = brush.positionInGroup;
		if (positionInGroup == undefined) {
			positionInGroup = GetBrushIndexInArray(selectedBrushesGroup, brush);
		}
		scrollBrushWeight.scrollPosition = positionInGroup;
	}
	
	private function GetBrushIndexInArray(brushesArray:Array, brush:Object):Number {
		for (var i:Number=0; i<brushesArray.length; ++i) {
			if (brushesArray[i] == brush) return i;
		}
		return 0;
	}
	
	private function scrollBrushWeight_OnScroll(eventObject) {
		var selectedBrush = selectedBrushesGroup[scrollBrushWeight.scrollPosition];
		selectedBrush.positionInGroup = scrollBrushWeight.scrollPosition;
		//_global.CurrentUnit.Brush = 
		OnBrush(selectedBrush);
		//change default in HList control
		defaultBrushes[ctlBrushType.SelectedItemIndex] = selectedBrush;
		ctlBrushType.RebindItem(ctlBrushType.SelectedItemIndex);
	}
	
	private function btnContinueAndClose_OnClick(eventObject) {
		OnEndPainting();
	}
	
	private function btnUndo_OnClick(eventObject) {
		OnUndo();
		//_global.CurrentUnit.Undo();
	}
	
	private function chkKeepProportions_OnClick(eventObject)	{
		//_global.CurrentUnit.KeepProportions = 
		OnKeepProps(chkKeepProportions.selected);
	}
	
	private function btnRestoreProportions_OnClick(eventObject) {
		OnRestoreProps();
		/*_global.CurrentUnit.ScaleX = _global.CurrentUnit.InitialScale;
		_global.CurrentUnit.ScaleY = _global.CurrentUnit.InitialScale;
		_global.SelectionFrame.AttachUnit(_global.CurrentUnit);*/
	}
	
	//~Actions
	
	//region EndPainting event
	private function OnEndPainting() {
		var eventObject = {type:"EndPainting", target:this};
		this.dispatchEvent(eventObject);

	};

	public function RegisterOnEndPaintingHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("EndPainting", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion

	//region Exit event
	private function OnExit() {
		var eventObject = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
  
	private function OnRestoreProps() {
		var eventObject = {type:"restoreprops", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnRestorePropsHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("restoreprops", Delegate.create(scopeObject, callBackFunction));
    }
	
	private function OnKeepProps(bValue) {
		var eventObject = {type:"keepprops", target:this, newval:bValue};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnKeepPropsHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("keepprops", Delegate.create(scopeObject, callBackFunction));
    }
	
	private function OnColor(newColor:Number) {
		var eventObject = {type:"onpaintcolor", target:this, color:newColor};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnColorHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onpaintcolor", Delegate.create(scopeObject, callBackFunction));
	}
	private function OnBrush(newBrush) {
		var eventObject = {type:"onpaintbrush", target:this, brush:newBrush};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnBrushHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onpaintbrush", Delegate.create(scopeObject, callBackFunction));
	}
	private function OnUndo() {
		var eventObject = {type:"onpaintundo", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnUndoHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onpaintundo", Delegate.create(scopeObject, callBackFunction));
	}
	
	public function EnableButtons(empty:Boolean):Void {
		btnContinueAndClose.enabled = !empty;
		btnUndo.enabled = !empty;
		if (!btnUndo.enabled) {
			btnUndo.StopRepeat();
		}
	}
}
