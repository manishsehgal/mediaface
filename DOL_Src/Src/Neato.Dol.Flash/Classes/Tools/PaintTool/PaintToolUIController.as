﻿class Tools.PaintTool.PaintToolUIController {

	private var paintAddTool:Tools.PaintTool.PaintAddToolContainer;
	private var paintEditTool:Tools.PaintTool.PaintEditToolContainer;
	private var currentPaintUnit = null;
	
	public function PaintToolUIController() {
		trace("PaintToolUIController ctr");
	}
	
	function OnComponentLoaded(compName:String, compObj:Object, param) {
		trace("ShapeTool OnComponentLoaded");
		var tool:String = compName.split(".")[1];
		trace("On component loaded [paint]"+tool);
		if(tool == "PaintAddTool") {
			paintAddTool = Tools.PaintTool.PaintAddToolContainer(compObj);
			paintAddTool.RegisterOnRestorePropsHandler(this, paintAddTool_onRestoreProps);
   			paintAddTool.RegisterOnEndPaintingHandler(this, paintAddTool_onEnd);
			paintAddTool.RegisterOnColorHandler(this, paintAddTool_onColor);
			paintAddTool.RegisterOnBrushHandler(this, paintAddTool_onBrush);
			paintAddTool.RegisterOnUndoHandler(this, paintAddTool_onUndo);
			paintAddTool.RegisterOnKeepPropsHandler(this, paintAddTool_onKeepProps);
		} else if(tool == "PaintEditTool") {
			paintEditTool = Tools.PaintTool.PaintEditToolContainer(compObj);
			paintEditTool.RegisterOnKeepPropsHandler(this, paintEditTool_onKeepProps);
			paintEditTool.RegisterOnRestorePropsHandler(this, paintEditTool_onRestoreProps);
		}
	}
	
	public function RegisterOnDeleteUnitHandler(scopeObject:Object, callBackFunction:Function, tool:String) {
		switch (tool) {
			//case "PaintAddTool" :
			//	paintAddTool.RegisterOnDeleteUnitHandler(scopeObject, callBackFunction);
			//break;
			case "PaintEditTool" :
				paintEditTool.RegisterOnDeleteUnitHandler(scopeObject, callBackFunction);
			break;
		}
	}
	
	function AddDataBind():Void {
		_global.Mode = _global.PaintMode;
		_global.UIController.FramesController.detach();
		_global.Mode = _global.PaintMode;
		var paintUnit = _global.Project.CurrentPaper.CurrentFace.CreateNewPaintUnit();
		paintUnit.RegisterOnChangeHandler(this, paintUnit_Changed);
		_global.Project.CurrentPaper.CurrentFace.ChangeCurrentUnit(paintUnit);
		currentPaintUnit = paintUnit;
		trace("Add Paint data bind "+paintUnit+" "+_global.Project.CurrentUnit);
		var data = new Object();
		data.bKeepProps = currentPaintUnit.KeepProportions;
		data.bEnabledKeepProps = currentPaintUnit.IsNonScalable;
		paintAddTool.EnableButtons(paintUnit.Empty());
		paintAddTool.DataBind(data);
		
	}
	
	function EditDataBind():Void {
		currentPaintUnit = null;
		_global.Mode = _global.EditPaintMode;
		var data = new Object();
		data.bKeepProps = _global.Project.CurrentUnit.KeepProportions;
		data.bEnabledKeepProps = _global.Project.CurrentUnit.IsNonScalable;
		paintEditTool.DataBind(data);
	}
	
	function paintUnit_Changed(eventObject) {
		paintAddTool.EnableButtons(eventObject.target.Empty());
	}
	
	function paintAddTool_onRestoreProps(eventObj){
		trace("editTool_onRestoreProps");
		currentPaintUnit.RestoreProportions();
	}
	
	function paintEditTool_onRestoreProps() {
		var unit = _global.Project.CurrentUnit;
		var scaleXString:String = Actions.ScaleAction.ScaleXProperty(unit);
		var scaleYString:String = Actions.ScaleAction.ScaleYProperty(unit);
		var scaleX:Number = unit[scaleXString];
		var scaleY:Number = unit[scaleYString];

		_global.Project.CurrentUnit.Resize(_global.Project.CurrentUnit.InitialScale / _global.Project.CurrentUnit.ScaleX, _global.Project.CurrentUnit.InitialScale / _global.Project.CurrentUnit.ScaleY);
		_global.SelectionFrame.AttachUnit(_global.Project.CurrentUnit);

		var scaleXNew:Number = unit[scaleXString];
		var scaleYNew:Number = unit[scaleYString];

		var action:Actions.ScaleAction = new Actions.ScaleAction(unit, scaleX, scaleY, scaleXNew, scaleYNew);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Focused = false;
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	function paintAddTool_onEnd(){
		_global.Project.CurrentPaper.CurrentFace.ChangeCurrentUnit(currentPaintUnit);
		FinishPaint();
		if (_global.Project.CurrentUnit.IsEmpty) {
			_global.UIController.ToolsController.DeleteCurrentUnit();
		}
		else {
			SATracking.Paint();
			_global.Mode = _global.PaintEditMode;
			_global.UIController.OnUnitChanged();

			var unit = _global.Project.CurrentUnit; 
			var xml:XMLNode = unit.GetXmlNode();
			var action:Actions.AddUnitAction = new Actions.AddUnitAction(unit, xml); 
			_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
		}
	}

	function FinishPaint() {
		//_global.tr("Paint addTool on end = "+ currentPaintUnit);
		currentPaintUnit.ExitPaint();
		currentPaintUnit = null;
	}

	function paintAddTool_onColor(eventObj) {
		trace("paintAddTool_onColor + "+eventObj.color);
		currentPaintUnit.PaintColor = eventObj.color;
	}
	
	function paintAddTool_onBrush(eventObj) {
		currentPaintUnit.Brush = eventObj.brush;
	}
	
	function paintAddTool_onKeepProps(eventObj) {
		currentPaintUnit.KeepProportions = eventObj.newval;
	}
	
	function paintAddTool_onUndo() {
		currentPaintUnit.Undo();
	}
	
	function paintEditTool_onKeepProps(eventObj) {
		var oldKeepProportions:Boolean = _global.Project.CurrentUnit.KeepProportions;

		_global.Project.CurrentUnit.KeepProportions = eventObj.newval;

		var newKeepProportions:Boolean = _global.Project.CurrentUnit.KeepProportions;
		var action:Actions.KeepProportionsAction = new Actions.KeepProportionsAction(_global.Project.CurrentUnit, oldKeepProportions, newKeepProportions);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}

	
	
}