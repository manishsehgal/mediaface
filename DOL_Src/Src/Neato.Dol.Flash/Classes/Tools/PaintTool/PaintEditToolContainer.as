﻿import mx.utils.Delegate;
import mx.core.UIObject;
import mx.controls.*;
[Event("exit")]
class Tools.PaintTool.PaintEditToolContainer extends UIObject {
	private var chkKeepProportions:CtrlLib.CheckBoxEx;
	private var btnDelete:CtrlLib.ButtonEx;
	private var btnRestoreProportions:CtrlLib.ButtonEx;
	private var btnContinue:CtrlLib.ButtonEx;
	private var lblBrushWeight:CtrlLib.LabelEx;
	
	function PaintEditToolContainer() {
	}
	
	function onLoad() {
		chkKeepProportions.addEventListener("click", Delegate.create(this, chkKeepProportions_OnClick));
		btnRestoreProportions.addEventListener("click", Delegate.create(this, btnRestoreProportions_OnClick));
		btnContinue.addEventListener("click",	Delegate.create(this, btnContinue_OnClick));
		_global.GlobalNotificator.OnComponentLoaded("Tools.PaintEditTool", this);
		DataBind();
		InitLocale();
	}
	
	public function RegisterOnDeleteUnitHandler(scopeObject:Object, callBackFunction:Function) {
		btnDelete.RegisterOnClickHandler(scopeObject, callBackFunction);
	}

	private function chkKeepProportions_OnClick(eventObject)	{
		//_global.CurrentUnit.KeepProportions = chkKeepProportions.selected;
			OnKeepProps(chkKeepProportions.selected);
	}
	
	private function btnRestoreProportions_OnClick(eventObject) {
		OnRestoreProps();
	}
	
	public function DataBind(data):Void {
		trace("PaintEditProperties.DataBind Mode = " + _global.Mode);
		chkKeepProportions.selected = data.bKeepProps;//_global.CurrentUnit.KeepProportions;
		chkKeepProportions.enabled = data.bEnabledKeepProps;//_global.CurrentUnit.IsNonScalable;
		//chkKeepProportions.selected = _global.CurrentUnit.KeepProportions;
		//chkKeepProportions.enabled =_global.CurrentUnit.IsNonScalable;
	}

	private function InitLocale() {
		//_global.LocalHelper.LocalizeInstance(btnDelete, "ToolProperties", "IDS_BTNDELETE");
		_global.LocalHelper.LocalizeInstance(chkKeepProportions, "ToolProperties", "IDS_CHKKEEPPROPORTIONS");
		//_global.LocalHelper.LocalizeInstance(btnRestoreProportions, "ToolProperties", "IDS_BTNRESTOREPROPORTIONS");
		//_global.LocalHelper.LocalizeInstance(btnContinue, "ToolProperties", "IDS_BTNCONTINUE");
		_global.LocalHelper.LocalizeInstance(lblBrushWeight, "ToolProperties", "IDS_LBLBRUSHWEIGHT");
		
		TooltipHelper.SetTooltip2(btnDelete, "IDS_TOOLTIP_DELETE");
		TooltipHelper.SetTooltip2(btnContinue, "IDS_TOOLTIP_CONTINUE");
		TooltipHelper.SetTooltip2(chkKeepProportions, "IDS_TOOLTIP_KEEPPROPORTIONS");
		TooltipHelper.SetTooltip2(btnRestoreProportions, "IDS_TOOLTIP_RESTOREPROPORTIONS");
    }
	private function btnContinue_OnClick() {
		OnExit();
	}
	//region Exit event
	private function OnExit() {
		var eventObject = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
	private function OnKeepProps(bValue) {
		var eventObject = {type:"keepprops", target:this, newval:bValue};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnKeepPropsHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("keepprops", Delegate.create(scopeObject, callBackFunction));
    }
	
	private function OnRestoreProps() {
		var eventObject = {type:"restoreprops", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnRestorePropsHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("restoreprops", Delegate.create(scopeObject, callBackFunction));
    }
}