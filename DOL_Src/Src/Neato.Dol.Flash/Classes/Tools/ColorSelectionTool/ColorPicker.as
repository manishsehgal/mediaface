﻿import mx.utils.Delegate;
[Event("select")]
class Tools.ColorSelectionTool.ColorPicker extends mx.core.UIComponent {
    static var symbolName:String = "ColorPicker";
    static var symbolOwner:Object = Object(Tools.ColorSelectionTool.ColorPicker);
    var className:String = "ColorPicker";

	private var Palette:Array;
	private var rgb:Number;
	private var currentSelect:MovieClip;
	private var btnMoreColors:CtrlLib.ButtonEx;
	
	function ColorPicker() {
	}	
	
    private function OnSelect(color) {
		if (this.enabled == false) return;
		rgb = color;
        var eventObject = {type:"select", target:this, color:color };
        this.dispatchEvent(eventObject);
    };

    public function RegisterOnSelectHandler(scopeObject:Object, callBackFunction:Function) {
        this.addEventListener("select", Delegate.create(scopeObject, callBackFunction));
    }	

	private function onLoad() {
		if (Palette == null) {
			Palette = new Array(0x000000, 0x333333, 0x666666, 0xCCCCCC, 0xFFFFFF, 0x000099, 0x0033CC, 
								0x0066FF, 0x0099CC, 0x99CCFF, 0x336600, 0x339900, 0x33CC33, 0x33FF66, 
								0xCCFF99, 0x990066, 0x993399, 0x9966CC, 0x9966FF, 0xCC99FF, 0xCC0000, 
								0xFF0000, 0xFF6666, 0xFF9999, 0xFFCCCC, 0xFF6600, 0xFF9900, 0xFFCC33, 
								0xFFFF00, 0xFFFF99);
		}
		
		DrawPalette();
		
		//btnMoreColors.RegisterOnClickHandler(this,btnMoreColors_OnClick);
		var parent = this;
		btnMoreColors.onRelease = function() {
			if (parent.enabled == true)
				_parent._parent.colorPanels_OnChange();
		};
		TooltipHelper.SetTooltip2(btnMoreColors, "IDS_TOOLTIP_CHANGECOLORS");
	}
	
	private function btnMoreColors_OnClick() {
		_parent._parent.colorPanels_OnChange();
	}
	
	private function DrawPalette(enabled:Boolean):Void {
		var nRow:Number = 3;
		var nCol:Number = 10;
		var xSpace:Number = 0;
		var ySpace:Number = 0;
		var initX:Number = 0;
		var initY:Number = 0;
		var rgbArray:Array = new Array(Palette.length);
		for (var i=0; i<nRow; i++) {
			for (var j=0; j<nCol; j++) {
				var index = j+nCol*i;
				var mcDrop = this.attachMovie("mc", "mc"+index, index);
				mcDrop.useHandCursor = enabled;
				mcDrop._x = (j)*(mcDrop._width+xSpace-1)+initX;
				mcDrop._y = (i)*(mcDrop._height+ySpace-1)+initY;
				var bgColor:Color = new Color(mcDrop.mcColorBg);
				bgColor.setRGB(Palette[index]);
				rgbArray[index] = bgColor.getRGB();
				mcDrop.mcSelect._visible = false;
				mcDrop["selState"] = false;
				if (enabled == false) {
					mcDrop.onPress = null;
					mcDrop.onRollOver = null;
					mcDrop.onRollOut = null;
				}
				else {
					var parent = this;
					mcDrop.onPress = function() {
						parent.OnClick();
						parent.currentSelect._visible = false;
						parent.currentSelect._parent["selState"] = false;
						
						var rgb = rgbArray[this._name.slice(2)];
	
						this["mcSelect"]._visible = true;
						this["selState"] = true;
						parent.currentSelect = this["mcSelect"];
						
						parent.OnSelect(rgb);
					};
					mcDrop.onRollOver = function() {
						this["mcSelect"]._visible = true;
					};
					mcDrop.onRollOut = function() {
						if (this["selState"] == false) {
							this["mcSelect"]._visible = false;
						}
					};
				}
			}
		}
	}
	
	public function SetColorState(selectedColor:Number) {
		currentSelect._visible = false;
		currentSelect._parent["selState"] = false;
		rgb = selectedColor;
		
		for (var i:Number = 0; i < Palette.length; i++) {
			if(Palette[i] == selectedColor) {
				currentSelect = this["mc" + i].mcSelect;
				currentSelect._visible = true;
				currentSelect._parent["selState"] = true;
			}
		}
    }
	
	public function get RGB():Number {
		return rgb;
	}
	
	private function OnClick() {
		if (this.enabled == false) return;
		var eventObject = {type:"click", target:this};
        this.dispatchEvent(eventObject);
    };

    public function RegisterOnClickHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("click", Delegate.create(scopeObject, callBackFunction));
    }

	function setEnabled(enabled:Boolean):Void {
		super.setEnabled(enabled);
		DrawPalette(enabled);
		btnMoreColors.useHandCursor = enabled;
		SetColorState(rgb);
	}
}
