﻿import flash.geom.ColorTransform;
import flash.geom.Transform;
import mx.utils.Delegate;
[Event("select")]
class Tools.ColorSelectionTool.ColorTool extends mx.core.UIComponent {
    static var symbolName:String = "ColorTool";
    static var symbolOwner:Object = Object(Tools.ColorSelectionTool.ColorTool);
    var className:String = "ColorTool";

	//private var btnChangePanel;
	private var colorPicker:Tools.ColorSelectionTool.ColorPicker;
	private var colorSelector:Tools.ColorSelectionTool.ColorSelector;
	private var rgb:Number;
	private var captionSimpleColors:String;
	private var captionMoreColors:String;

	function ColorTool() {
	}	
	
	private function onLoad() {
		colorPicker._visible = true;
		colorSelector._visible = false;		
	}
	
		
	public function colorPanels_OnChange() {
		var flag = colorPicker._visible;
		
		colorPicker._visible = !flag;
		colorSelector._visible = flag;
				
		if (flag)
			colorSelector.SetColorState(rgb);
		else 
			colorPicker.SetColorState(rgb);
	}
	
    public function RegisterOnSelectHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("select", Delegate.create(scopeObject, callBackFunction));
		colorPicker.RegisterOnSelectHandler(this, colorTool_OnSelect);
		colorSelector.RegisterOnSelectHandler(this, colorTool_OnSelect);
    }
	
	function OnControlClick(){
		if (this.enabled == false) return;
		var eventObject = {type:"click", target:this};
		this.dispatchEvent(eventObject);
	}
	public function RegisterOnClickHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("click", Delegate.create(scopeObject, callBackFunction));
		colorSelector.RegisterOnClickHandler(this, OnControlClick);
		colorPicker.RegisterOnClickHandler(this, OnControlClick);
    }
	
	private function colorTool_OnSelect(eventObject) {
		if (this.enabled == false) return;
		rgb = eventObject.color;
		this.dispatchEvent(eventObject);
	}
	
	public function SetColorState(selectedColor:Number) {
		rgb = selectedColor;
		colorPicker.SetColorState(selectedColor);
		colorSelector.SetColorState(selectedColor);
	}
		
	public function get RGB():Number {
		return rgb;
	}

	function setEnabled(enabled:Boolean):Void {
		super.setEnabled(enabled);
		var colorTrans:ColorTransform = new ColorTransform();

		if (enabled == false) {
			colorTrans.redOffset   = -100;
			colorTrans.greenOffset = -100;
			colorTrans.blueOffset  = -100;
		}
		colorPicker.enabled = enabled;
		colorSelector.enabled = enabled;

		var trans:Transform = new Transform(this);
		trans.colorTransform = colorTrans;
	}
}