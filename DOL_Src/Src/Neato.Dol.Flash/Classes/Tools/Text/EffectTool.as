﻿import mx.utils.Delegate;
import Logic.*;

[Event("add")]
class Tools.Text.EffectTool extends mx.core.UIComponent {
	
	static var symbolName:String = "Tools.Text.EffectTool";
	static var symbolOwner:Object = Tools.Text.EffectTool;
	var className:String = "EffectTool";

	private var boundingBox_mc:MovieClip;
	private var ctlChooseEffect;
	private var lblError;
	private var pluginsService : SAPlugins;
	private var bAvaible:Boolean = false;
	private var parent;

	function init():Void {
		super.init();

		if (boundingBox_mc != undefined) {
			boundingBox_mc._width = 0;
			boundingBox_mc._height = 0;
			boundingBox_mc._visible = false;
		}
	}

	function createChildren():Void {
		super.createChildren();
		if (ctlChooseEffect == undefined) {
			ctlChooseEffect = this.createObject("HListEx", "ctlChooseEffect", this.getNextHighestDepth());
		}
	}

	private function onLoad() : Void {
		pluginsService = SAPlugins.GetInstance();
		pluginsService.RegisterOnModuleAvaliableHandler(this, IsTextEffectsModuleAvaliable);
		ctlChooseEffect.RegisterOnChangeHandler(this, ctlChooseEffect_OnChange);

		_global.GlobalNotificator.OnComponentLoaded("Tools.TextEffectTool", this);
		currDataSource = new Array();
		FullDataSource = new Array();
		parent = this;
	}

		public var currDataSource:Array;
		public var FullDataSource:Array;
		public function set DataSource(value:Array) {

		FullDataSource = value;
		currDataSource =  value.slice(0,1);
	}

	public function DataBind(bCallPlugin:Boolean) : Void {
		lblError.text = "";
		ctlChooseEffect.DataSource = bAvaible == true ? FullDataSource : currDataSource;
		ctlChooseEffect.ItemLinkageName = "SymbolHolderEx";
		ctlChooseEffect.DataBind();
		if(bAvaible == false && bCallPlugin != false) {
			pluginsService.IsModuleAvailable("TextEffectModule");
		}
	}
	
	private function IsTextEffectsModuleAvaliable(eventObject:Object) : Void {
		bAvaible = eventObject.isAvaliable.toString() == "true" ? true : false;
		parent.DataBind(false);
		lblError.text = "";
	}
	//region Actions

	private function ctlChooseEffect_OnChange(eventObject:Object) : Void {
		if(eventObject.index == undefined)
			return;
		OnApply(eventObject.index);
	}
	//endregion

	//region add Shape event
	private function OnApply(index:Number) :Void {
		var effectName:String = FullDataSource[index].name;
		var eventObject:Object = {type:"apply", target:this, effectName:effectName, index:index};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnApplyHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("apply", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
	
	private function btnEditText_OnClick(eventObject:Object) : Void {
		OnEditText();
	}
	
	//region Exit event
	private function OnEditText() : Void {
		var eventObject:Object = {type:"edittext", target:this};
		this.dispatchEvent(eventObject);
	};
	public function RegisterOnEditTextHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("edittext", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
}
