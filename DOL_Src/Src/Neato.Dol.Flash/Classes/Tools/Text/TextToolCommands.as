﻿import mx.utils.Delegate;
import Logic.*;
import CtrlLib.ButtonEx;

[Event("addSimpleText")]
[Event("addMultilineText")]
[Event("addArtisticText")]
[Event("addPlaylist")]
class Tools.Text.TextToolCommands extends mx.core.UIComponent {
	private var btnAddSimpleText:ButtonEx;
	private var btnAddMultilineText:ButtonEx;
	private var btnAddArtisticText:ButtonEx;
	private var btnAddPlaylist:ButtonEx;
	
	function TextToolCommands() {
		super();
	}

	function onLoad() {
		_global.GlobalNotificator.OnComponentLoaded("Tools.TextToolCommands", this);		
	}
	
	public function RegisterOnAddSimpleTextHandler(scopeObject:Object, callBackFunction:Function) {
		btnAddSimpleText.RegisterOnClickHandler(scopeObject, callBackFunction);
    }
	
	public function RegisterOnAddMultilineTextHandler(scopeObject:Object, callBackFunction:Function) {
		btnAddMultilineText.RegisterOnClickHandler(scopeObject, callBackFunction);
    }
	
	public function RegisterOnAddArtisticTextHandler(scopeObject:Object, callBackFunction:Function) {
		btnAddArtisticText.RegisterOnClickHandler(scopeObject, callBackFunction);
    }
	
	public function RegisterOnAddPlaylistHandler(scopeObject:Object, callBackFunction:Function) {
		btnAddPlaylist.RegisterOnClickHandler(scopeObject, callBackFunction);
    }
}