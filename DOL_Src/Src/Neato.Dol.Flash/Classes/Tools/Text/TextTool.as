﻿import CtrlLib.LabelEx;
import CtrlLib.ButtonEx;
import CtrlLib.ComboBoxEx;
import CtrlLib.RadioButtonEx;
import CtrlLib.CheckBoxEx;
import mx.utils.Delegate;

class Tools.Text.TextTool extends mx.core.UIObject {
	
	private var txtTextInput   : Tools.Text.SelectionTextInput;
	private var txtTextArea    : Tools.Text.SelectionTextArea;
	
	private var lblFont        : LabelEx;
	private var cbFont         : ComboBoxEx;
	private var lblSize        : LabelEx;
	private var cbFontSize     : ComboBoxEx;
	
	private var btnBold        : ButtonEx;
	private var btnItalic      : ButtonEx;
	private var btnUnderline   : ButtonEx;
	private var btnAlignLeft   : RadioButtonEx;
	private var btnAlignCenter : RadioButtonEx;
	private var btnAlignRight  : RadioButtonEx;
	
	private var lblColor       : LabelEx;
	private var colorTool      : Tools.ColorSelectionTool.ColorTool;
	
	private var btnDelete      : ButtonEx;
	private var btnUpdateEffect: ButtonEx;	
	
	private var txtText          : Tools.Text.ISelectionText;
	private static var fontSizes : Array = [4,5,6,7,8,9,10,12,14,16,18,20,24,28,32,36,40,44,48,54,60,66,72,80,88,96,106,117,127];
	private var oldFontSize      : Number;
	private var bTextChanged     : Boolean = false;
	private var unitType         : String;
	
	private var btnSpecchar          : ButtonEx;
	private var SpeccharTool         : MovieClip;
	private var boolSpeccharToolMode : Boolean;
	private var arrayCurentRange     : Array;
	
	private var intervalId        : Number;

	private var pluginsService;
	
	
	
	//private var unit;

	function TextTool(compObj:Object) {
		pluginsService = SAPlugins.GetInstance();
		//pluginsService.RegisterOnInvokedParsedHandler(this, onResponse);
		pluginsService.RegisterOnModuleAvaliableHandler(this, OnModuleAvailableChecked);
	}
	
	function onLoad() : Void {
		//------ styles
		this.setStyle("styleName", "ToolPropertiesPanel");
		this.txtTextInput.setStyle("styleName", "ToolPropertiesInputText");
		this.txtTextArea.setStyle("styleName", "ToolPropertiesInputText");
		this.lblFont.setStyle("styleName", "ToolPropertiesText");
		this.cbFont.setStyle("styleName", "ToolPropertiesCombo");
		this.lblSize.setStyle("styleName", "ToolPropertiesText");
		this.cbFontSize.setStyle("styleName", "ToolPropertiesCombo");
		this.lblColor.setStyle("styleName", "ToolPropertiesText");
		
		
		TooltipHelper.SetTooltip2(btnDelete, "IDS_TOOLTIP_DELETE");
		TooltipHelper.SetTooltip2(btnUpdateEffect, "IDS_TOOLTIP_UPDATEEFFECT");

		//------ text area
		this.txtTextInput.RegisterOnSelectionChangeHandler(this, onTextSelectionChanged);
		this.txtTextInput.RegisterOnTextChangedHandler(this, onTextChanged);
		this.txtTextInput.tabIndex = 1;
		this.txtTextArea.RegisterOnSelectionChangeHandler(this, onTextSelectionChanged);
		this.txtTextArea.RegisterOnTextChangedHandler(this, onTextChanged);
		this.txtTextArea.tabIndex = 1;
		this.txtText = txtTextInput;
		this.txtTextArea._visible = false;
		this.btnUpdateEffect._visible = false;
		
		//------ font combo
		this.cbFont.dropdown.cellRenderer = "FontCellRenderer";
		
		var FontsArray:Array = new Array();
		FontsArray = _global.Fonts.GetFontsArray();
		this.cbFont.dropdownWidth = 180;
		for (var i:Number = 0; i<FontsArray.length; i++) {
			if (FontsArray[i].label.toString() != "_") {
				this.cbFont.addItem({data:FontsArray[i].data, label:FontsArray[i].label});
			}
		}
		this.cbFont.sortItems(upperCaseFunc);
		this.cbFont.selectedIndex = 0;
		this.cbFont.addEventListener("change", Delegate.create(this, onFontChanged));
        
        //------ font size combo
        for (var i:Number=0; i<fontSizes.length; i++) {
            this.cbFontSize.addItem(fontSizes[i]);
        }
		this.cbFontSize.addEventListener("change", Delegate.create(this, onFontSizeChanged));
		this.cbFontSize.addEventListener("focusOut", Delegate.create(this, onFontSizeFocusOut));
		this.cbFontSize.textField.addEventListener("enter", Delegate.create(this, changeFontSize));
		this.cbFontSize.textField.restrict = "0-9";
		this.cbFontSize.textField.maxChars = 3;
		
		//------ bold button
		this.btnBold.addEventListener("click", Delegate.create(this, onBold));
		
		//------ italic button
		this.btnItalic.addEventListener("click", Delegate.create(this, onItalic));
		
		//------ underline button
		this.btnUnderline.addEventListener("click", Delegate.create(this, onUnderline));
		
		//------ special char button
		this.btnSpecchar.addEventListener("click", Delegate.create(this, onSpecchar));
		
		//------ align buttons
		this.btnAlignLeft.addEventListener("click",   Delegate.create(this, onAlign));
		this.btnAlignCenter.addEventListener("click", Delegate.create(this, onAlign));
		this.btnAlignRight.addEventListener("click",  Delegate.create(this, onAlign));
		this.btnUpdateEffect.RegisterOnClickHandler(this, UpdateEffect);//addEventListener("click",  Delegate.create(this, UpdateEffect));
		trace('this.btnUpdateEffect'+this.btnUpdateEffect);
		//------ color tool
		this.colorTool.RegisterOnSelectHandler(this, colorTool_OnSelect);
		
		//------ special char tool		
		fnSpeccharToolPanelInit();
		//------------------------
		_global.GlobalNotificator.OnComponentLoaded("Tools.TextTool", this);
	}
	
	static function upperCaseFunc(a, b) {
		return a.label.toUpperCase() > b.label.toUpperCase();
	}
	
	function onTextChanged(eventObject:Object) : Void {
		bTextChanged = true;
        
		var format:TextFormat = new TextFormat();
		format.font      = cbFont.selectedItem.data;
		format.size      = oldFontSize;
		format.bold      = btnBold.selected;
		format.italic    = btnItalic.selected;
		format.underline = btnUnderline.selected;
		format.color     = colorTool.RGB;
		
  		var eventObject:Object = {type:"changeText", target:this, oldBegin:eventObject.oldBegin, oldEnd:eventObject.oldEnd, insertedText:eventObject.insertedText, format:format};
  		this.dispatchEvent(eventObject);
	}
	
	public function RegisterOnDeleteUnitHandler(scopeObject:Object, callBackFunction:Function) : Void {
		btnDelete.RegisterOnClickHandler(scopeObject, callBackFunction);
	}

	function RegisterChangeTextHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("changeText", Delegate.create(scopeObject, callBackFunction));
	}
	
	function onMultiline(eventObject:Object) : Void {
  		var eventObject:Object = {type:"changeMultiline", target:this, multiline:eventObject.target.selected};
  		this.dispatchEvent(eventObject);
	}
	function RegisterChangeMultilineHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("changeMultiline", Delegate.create(scopeObject, callBackFunction));
	}
	
	function onFontChanged(eventObject:Object) : Void {
		var format:TextFormat = new TextFormat();
		format.font = eventObject.target.selectedItem.data;
		ChangeFontFormat(format);
		fnSpeccharToolRedraw();
	}

	function onFontSizeFocusOut(eventObject:Object) : Void {
		changeFontSize(eventObject);
	}
	
	function onFontSizeChanged(eventObject:Object) : Void {
		if (cbFontSize.textField.getFocus() != null)
			return;
		changeFontSize(eventObject);
	}
	function changeFontSize(eventObject:Object) : Void {
		var newSize:Number = parseInt(eventObject.target.text_mc.label.text);
		if (newSize > 3 && newSize < 128) {
			oldFontSize = newSize;
			var format:TextFormat = new TextFormat();
			format.size = newSize;
			var intervalObject:Object = new Object();
			var interval:Number = setInterval(Delegate.create(this, ChangeFontFormat), 100, format, intervalObject);
			intervalObject["interval"] = interval;
		} else {
			cbFontSize.text = oldFontSize.toString();
		}
	}
	
	function onBold(eventObject:Object) : Void {
		var format:TextFormat = new TextFormat();
		format.bold = eventObject.target.selected;
		ChangeFontFormat(format);
	}
		
	function onItalic(eventObject:Object) : Void {
		var format:TextFormat = new TextFormat();
		format.italic = eventObject.target.selected;
		ChangeFontFormat(format);
	}
	
	function onUnderline(eventObject:Object) : Void {
		var format:TextFormat = new TextFormat();
		format.underline = eventObject.target.selected;
		ChangeFontFormat(format);
	}
	
	
	function onAlign(eventObject:Object) : Void {
  		var eventObject:Object = {type:"changeAlign", target:this, align:eventObject.target.data};
  		this.dispatchEvent(eventObject);
		txtText.RestoreSelection();
	}
	function RegisterChangeAlignHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("changeAlign", Delegate.create(scopeObject, callBackFunction));
	}
	
	private function colorTool_OnSelect(eventObject:Object) : Void {
		var format:TextFormat = new TextFormat();
		format.color = colorTool.RGB;
		ChangeFontFormat(format);
	}
	
	function ChangeFontFormat(format:TextFormat, intervalObject:Object) : Void {
		if (intervalObject != undefined)
			clearInterval(intervalObject.interval);
			
		bTextChanged = true;
		if (format == undefined) return;
		
		/*_global.tr("ChangeFontFormat format = "+format);
 		for(var i:String in format)
			_global.tr(i+"->"+format[i]);
		_global.tr("-------------------");*/
		
  		var eventObject:Object = {type:"changeFontFormat", target:this, format:format, text:this.txtText};
  		this.dispatchEvent(eventObject);
		
		
		txtText.RestoreSelection();
	}
	public function RegisterChangeFontFormatHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("changeFontFormat", Delegate.create(scopeObject, callBackFunction));
    }
	
	function onTextSelectionChanged() : Void {
		BindTextFormat();
		
		bTextChanged = true;
		}
	
	public function DataBind(data:Object) : Void {
		
		// Prepare Special Char Tool (Default)
		boolSpeccharToolMode = false;
		
		fnSpeccharToolRedraw();
		
		bTextChanged = false;
			
		if (data.multiline) {
			txtTextInput._visible = false;
			txtTextArea._visible = true;
			txtText = txtTextArea;
		} else {
			txtTextInput._visible = true;
			txtTextArea._visible = false;
			txtText = txtTextInput;
		}
		
		unitType = data.unitType;
		if(unitType == 'Text') {
			btnUpdateEffect._visible = false;
			btnUnderline._visible = true;
		} else if(unitType == 'Effect') {
			btnUpdateEffect._visible = true;
			btnUnderline._visible = false;
		}
		
		this.txtText.SetText(data.text);
		this.txtText.SelectAllText();
		
		BindTextFormat();
		
		if (data.align == "right") {
			this.btnAlignRight.selected = true;
		} else if (data.align == "center") {
			this.btnAlignCenter.selected = true;
		} else {
			this.btnAlignLeft.selected = true;
		}
		this.cbFontSize.enabled = data.fontSizeActive;
		
		SetEnableEditing( CheckTextEffectAvailable() );
	}

	private function SetEnableEditing( enable:Boolean ) {
		this.txtTextInput.enabled = enable;
		this.txtTextArea.enabled = enable;
		this.cbFont.enabled = enable;
		this.cbFontSize.enabled = enable;
		this.btnBold.enabled = enable;
		this.btnItalic.enabled = enable;
		this.btnUnderline.enabled = enable;
		this.btnAlignRight.enabled = enable;
		this.btnAlignCenter.enabled = enable;
		this.btnAlignLeft.enabled = enable;
		this.btnSpecchar.enabled = enable;
		this.colorTool.enabled = enable;
	}
	
	private function BindTextFormat() : Void {
		var index:Number;
		
		if (txtText.GetSelectionBeginIndex() == txtText.GetSelectionEndIndex()) {
			index = (txtText.GetSelectionBeginIndex() <= 0) ? 0 : txtText.GetSelectionBeginIndex() - 1;
		} else {
			index = txtText.GetSelectionBeginIndex();
		}
		
		var format:TextFormat = _global.Project.CurrentUnit.GetFontFormat(index);
		var fontName:String = format.font;
		var fontRange:String = ""; 
		if (fontName.lastIndexOf("_") != -1) {
				fontRange = fontName.slice(fontName.lastIndexOf("_"));
				fontName = fontName.slice(0,fontName.lastIndexOf("_"));				
		} 
		if ((this.cbFont.value.toLowerCase() != fontName.toLowerCase())||(_global.Fonts.RangeFromName(this.SpeccharTool.cbxRangeSelect.value).toLowerCase() != fontRange.toLowerCase())) {
		
			for (var i:Number = 0; i < this.cbFont.length; ++i) {
				if (this.cbFont.getItemAt(i).data.toLowerCase() == fontName.toLowerCase()) {
					this.cbFont.selectedIndex = i;
					if (boolSpeccharToolMode) {
						fnSpeccharToolContentInit(fontRange);
					}
					break;
				}
			}
		}

		var fontSize:Number = format.size;
		if (fontSize != null && fontSize != undefined && fontSize != this.cbFontSize.value) {
			oldFontSize = fontSize;
			for (var i:Number = 0; i < this.cbFontSize.length; ++i) {
				if (this.cbFontSize.getItemAt(i).label == oldFontSize) {
					this.cbFontSize.selectedIndex = i;
					break;
				}
			}
			this.cbFontSize.text = oldFontSize.toString();
		}
		
		var selectedColor:Number = format.color;
		if (selectedColor == undefined) selectedColor = 0;
		colorTool.SetColorState(selectedColor);

		this.btnBold.selected = format.bold;
		this.btnItalic.selected = format.italic;
		this.btnUnderline.selected = format.underline;
	}
	
	function RefreshFontSize(size:Number) : Void {
		this.cbFontSize.text = size.toString();
	}
	
	function UpdateEffect(eventObj:Object) : Void {
		pluginsService.IsModuleAvailable("TextEffectModule");
	}

	private function OnModuleAvailableChecked(eventObject:Object) {
		var ok:Boolean = eventObject.isAvailabale || CheckTextEffectAvailable();
		if (ok) {
  			var eventObject:Object = {type:"updateTextEffect", target:this};
  			this.dispatchEvent(eventObject);
  		}
		SetEnableEditing( ok );
		txtText.RestoreSelection();
	}
	public function RegisterOnUpdateTextEffectHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("updateTextEffect", Delegate.create(scopeObject, callBackFunction));
	}		
	
	public function get Text():String {
		return txtText.GetText();
	}
	
	function CheckTextEffectAvailable():Boolean {
		return (unitType != "Effect" || SAPlugins.IsAvailable());
	}
	/////////////////////////////////////////////////////////////////////////////
	// Special Character Tool Events and Methods
	/////////////////////////////////////////////////////////////////////////////
	// Event from Range comboBox
	function onRangeChanged(eventObject:Object) : Void {
		fnSpeccharButtonPanelInit();
	}
	
	// Event from Special Char Button
	function onSpecchar(eventObject:Object) : Void {
		//_global.tr("Event ---- onSpecchar!");
		if (boolSpeccharToolMode) {
			boolSpeccharToolMode = false;			
		} else {
			boolSpeccharToolMode = true;
		}	
		fnSpeccharToolRedraw();	
	}
	
	// Initialisation of Special charactert Tool
	function fnSpeccharToolPanelInit() : Void {
		this.SpeccharTool = createObject("mcSpeccharTool", "mcSpeccharTool", this.getNextHighestDepth());		
		this.boolSpeccharToolMode = false;
		fnSpeccharToolRedraw();
	}
	// Initialisation of Special characters tool content
	function fnSpeccharToolContentInit(strRange:String) : Void {
		if (strRange == undefined) {
			strRange = "";
		}
		// Range ComboBox Init
		var RangeIndex:Number = 0;
		var RangeArray:Array = new Array();
		this.SpeccharTool.cbxRangeSelect.removeAll();
		RangeArray = _global.Fonts.GetRangesArray();
		var currentFont:String = this.cbFont.selectedItem.data;
		for (var i:Number = 0; i<RangeArray.length; i++ ) {
			if (RangeArray[i][0] == currentFont) {				
				this.SpeccharTool.cbxRangeSelect.addItem(RangeArray[i][1][0]);			
				if (_global.Fonts.NameFromRange(strRange) == RangeArray[i][1][0]) {
					RangeIndex = this.SpeccharTool.cbxRangeSelect.length-1;
				}
			}
		}
		
		//this.SpeccharTool.cbxRangeSelect.sortItems(upperCaseFunc);
		this.SpeccharTool.cbxRangeSelect.selectedIndex = RangeIndex;
		this.SpeccharTool.cbxRangeSelect.addEventListener("change", Delegate.create(this, onRangeChanged));
		// Special character buttons panel Init
		fnSpeccharButtonPanelInit();
		this.SpeccharTool.scrpSpeccharTable.invalidate();
		clearInterval(intervalId);
		/////////////////////////////////////////	
	}
	function Draw() : Void{
		fnSpeccharButtonPanelInit();
	}
	// Special character button panel Init
	function fnSpeccharButtonPanelInit() : Void {
		
		var nmPanelLineButtonCounter:Number = 0;
		var arrayCodeRanges:Array = new Array();
		arrayCurentRange = new Array();
		arrayCodeRanges = _global.Fonts.GetRangesArray();
		var currentFont:String = this.cbFont.selectedItem.data;
		for (var i:Number = 0; i<arrayCodeRanges.length; i++ ) {
			if ((arrayCodeRanges[i][0] == currentFont)&&(arrayCodeRanges[i][1][0] == this.SpeccharTool.cbxRangeSelect.selectedItem.label)) {	
							
				nmPanelLineButtonCounter = arrayCodeRanges[i][1][1].length;
				arrayCurentRange = arrayCodeRanges[i][1][1];
				
			}
		}
		//////////////////////
		var formatSpecchar:TextFormat = new TextFormat();
		formatSpecchar = getFormatSpeccharTool();
		
		formatSpecchar.size			= 16;
		formatSpecchar.align 		= "center";
		formatSpecchar.color		= 0x000000;
		formatSpecchar.bold 		= false;
		formatSpecchar.italic 		= false;
		formatSpecchar.underline 	= false;
			
		if (!_global.Fonts.TestLoadedFont(formatSpecchar)) {
			_global.Fonts.LoadFont(formatSpecchar, this);
		} else {
			
			
			//////////////////////
			var duplicate:MovieClip;
			var nmButtonCounter:Number = 0;
			this.SpeccharTool.scrpSpeccharTable.content.removeMovieClip();
			this.SpeccharTool.scrpSpeccharTable.contentPath = "mcSpeccharPanel";
			for(var i:Number = 0; i < Math.ceil(nmPanelLineButtonCounter/6); i++) {
			    var newY:Number = i * 31;
			    for (var j:Number = 0; j < 6; j++) {
			    	if ((j==0)||(j==1)) {
			    		var newX:Number = j * 33;
			    	} else {
			    		var newX:Number = (j * 33)+1;
			    	}
			    	if (nmButtonCounter < nmPanelLineButtonCounter) {
			    		duplicate = this.SpeccharTool.scrpSpeccharTable.content.mcSpeccharPanelSymbol.duplicateMovieClip("mcSpeccharPanelSymbol" + nmButtonCounter, nmButtonCounter, {_y:newY, _x:newX});
			    		duplicate.txtSpecchar.text = String.fromCharCode(arrayCurentRange[nmButtonCounter]);
			    		duplicate.txtSpecchar.embedFonts = true;
			    		duplicate.txtSpecchar.autoSize = false;		    		
			    		duplicate.txtSpecchar.setTextFormat(formatSpecchar);
			    		duplicate.btnSpecchar.onRelease = function () {    					
	    					_global.UIController.ToolsController.TextToolController.CallSpeccharPanelButton(String(this._parent._name.slice(21)));    					
					};		    				
			    		nmButtonCounter++;
			    		
			    	}
			    }
			} 
		}
	}
	
	// Redraw Special char Tool (visible/not visible)
	function fnSpeccharToolRedraw() : Void {
		if (boolSpeccharToolMode) {
			btnSpecchar.label = "Hide Symbols";
			SpeccharTool.gotoAndStop(2);
			txtText.RestoreSelection();
			intervalId = setInterval(this, "fnSpeccharToolContentInit", 100);
		} else {
			btnSpecchar.label = "Insert Symbols";
			SpeccharTool.gotoAndStop(1);
		}	
		btnSpecchar.selected = boolSpeccharToolMode;
	}
	// Method replace text in the TextArea
	// {oldBegin:0, oldEnd:0, insertedText:textTool.getCharSpeccharTool(strIndex), format:textTool.getFormatSpeccharTool()}
	public function ReplaceTextArea(objParam:Object) : Void {
		var oldTextStringBegin:String = txtTextArea.text.slice(0,objParam.oldBegin);
		var oldTextStringEnd:String = txtTextArea.text.slice(objParam.oldEnd);
		txtText.SetText(oldTextStringBegin + String(objParam.insertedText) + oldTextStringEnd);
		txtText.SetSelection(objParam.oldBegin+1, objParam.oldBegin+1);

	}
	// Method return character symbol
	public function getCharSpeccharTool(strIndex:String) : String {
	return 	String.fromCharCode(arrayCurentRange[parseInt(strIndex)]);
	}
	// Method return selection begin index
	public function getSelectionBeginIndex(): Number {
		return txtText.GetSelectionBeginIndex();
	}
	// Method return selection begin index
	public function getSelectionEndIndex(): Number {
		return txtText.GetSelectionEndIndex();
	}
	// Method return Text Format
	public function getFormatSpeccharTool() : TextFormat {
		var currentFont:String = this.cbFont.selectedItem.data;
		var currentRange:String = this.SpeccharTool.cbxRangeSelect.selectedItem.label;
		
		var format:TextFormat = new TextFormat();
		
		format.font 	 = currentFont + _global.Fonts.RangeFromName(currentRange);
		format.size      = oldFontSize;
		format.bold      = btnBold.selected;
		format.italic    = btnItalic.selected;
		format.underline = btnUnderline.selected;
		format.color     = colorTool.RGB;
		return format;
	}
	//////////////////////////////////////////////////////////////////////////////
}
