import Actions.TextFormatAction;
import Actions.AlignAction;
class Tools.Text.TextToolUIController {
	
	private var textTool : Tools.Text.TextTool;
	private var textCommands : Tools.Text.TextToolCommands;
	private var effectTool: Tools.Text.EffectTool;
	private var textToolContainer:Tools.Text.TextToolContainer;
	public function TextToolUIController() {
		instance = this;
	}
	
	static private var instance   : Tools.Text.TextToolUIController;
	static function getInstance() : Tools.Text.TextToolUIController {
		return instance;
	}
	
	private var pluginsService : SAPlugins;
	
	function OnComponentLoaded(compName:String, compObj:Object, param) : Void {
		switch(compName.split(".")[1]) {
			case "TextTool":
				textTool = Tools.Text.TextTool(compObj);
				textTool.RegisterChangeTextHandler(this, onChangeText);
				textTool.RegisterChangeFontFormatHandler(this, onChangeFontFormat);
				textTool.RegisterChangeAlignHandler(this, onChangeAlign);
				textTool.RegisterChangeMultilineHandler(this, onChangeMultiline);
				textTool.RegisterOnUpdateTextEffectHandler(this, OnUpdateTextEffect);
				break;
			case "TextToolCommands":
				textCommands = Tools.Text.TextToolCommands(compObj);
				//textCommands.RegisterOnAddSimpleTextHandler(this, onAddSimpleText);
				textCommands.RegisterOnAddMultilineTextHandler(this, onAddMultilineText);
				textCommands.RegisterOnAddArtisticTextHandler(this, onAddArtisticText);
				textCommands.RegisterOnAddPlaylistHandler(this, onAddPlaylist);
				pluginsService = SAPlugins.GetInstance();
				pluginsService.RegisterOnModuleAvaliableHandler(this, IsTextEffectsModuleAvaliable);
				break;
			case "TextEffectTool":
				effectTool = Tools.Text.EffectTool(compObj);
				effectTool.RegisterOnApplyHandler(this, OnEffect);
				break;
			case "TextToolsContainer":
				trace("TextToolUI::OnCompLoaded::TextToolsContainer");
				textToolContainer = Tools.Text.TextToolContainer(compObj);
				textToolContainer.RegisterOnToolClickHandler(this, OnChangePanel);
				
				break;
		}
	}
	
	public function RegisterOnDeleteUnitHandler(scopeObject:Object, callBackFunction:Function) : Void {
		textTool.RegisterOnDeleteUnitHandler(scopeObject, callBackFunction);
	}
	
	private function onChangeText(eventObject:Object) : Void {
		var unit = _global.Project.CurrentUnit; 

		var beginIndex:Number = 0;
		var endIndex:Number = oldText.length; 
		var oldText:String = unit.Text;
		var oldFormats:Array = unit.GetFontFormats(beginIndex, endIndex);
		
		_global.Project.CurrentUnit.ReplaceText(eventObject.oldBegin, eventObject.oldEnd, eventObject.insertedText, eventObject.format);
		if(_global.Project.CurrentUnit instanceof TextEffectUnit) {
			_global.Project.CurrentUnit.SetText(textTool.Text);
			textTool.ChangeFontFormat(eventObject.format);
		}
		
		beginIndex = 0;
		endIndex = newText.length; 
		var newText:String = unit.Text;
		var newFormats:Array = unit.GetFontFormats(beginIndex, endIndex);

		var action:Actions.TextAction = new Actions.TextAction(unit, oldFormats, oldText, newFormats, newText); 
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);

		//SB
	 	_global.SelectionFrame.RedrawUnit();
	}
	
	private function onChangeFontFormat(eventObject:Object) : Void {
		var unit = _global.Project.CurrentUnit; 
		var beginIndex:Number = 0;
		var endIndex:Number = eventObject.text.GetText().length; 
		
		if(unit instanceof TextUnit) {
			beginIndex = eventObject.text.GetSelectionBeginIndex();
			endIndex = eventObject.text.GetSelectionEndIndex();
		}

		var oldFormats:Array = unit.GetFontFormats(beginIndex, endIndex);
		unit.ChangeFontFormat(beginIndex, endIndex, eventObject.format);
		var newFormats:Array = unit.GetFontFormats(beginIndex, endIndex);
		
		_global.Project.CurrentPaper.CurrentFace.ActManager.Focused = false;
		if (beginIndex != endIndex) {
			var action:Actions.TextFormatAction = new Actions.TextFormatAction(unit, beginIndex, endIndex, oldFormats, newFormats); 
			_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
		}

		//SB
		_global.SelectionFrame.RedrawUnit();
	}
	
	private function onChangeAlign(eventObject:Object) : Void {
		var unit = _global.Project.CurrentUnit;
		var oldAlign:String = unit.Align;
		_global.Project.CurrentUnit.Align = eventObject.align;
		var newAlign:String = unit.Align;
		
		var action:Actions.AlignAction = new Actions.AlignAction(unit, oldAlign, newAlign); 
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	private function onChangeMultiline(eventObject:Object) : Void {
		_global.Project.CurrentUnit.Multiline = eventObject.multiline;
		DataBind();
		//SB
		_global.SelectionFrame.RedrawUnit();
	}
	
	private function onAddSimpleText(eventObject:Object) : Void {
		trace("AddTextUnit");
		var unit = _global.Project.CurrentPaper.CurrentFace.AddText();
		_global.Project.CurrentPaper.CurrentFace.ChangeCurrentUnit(unit);
		SATracking.AddText();

		var xml:XMLNode = unit.GetXmlNode();
		var action:Actions.AddUnitAction = new Actions.AddUnitAction(unit, xml); 
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	private function onAddMultilineText(eventObject:Object) : Void {
		var unit = _global.Project.CurrentPaper.CurrentFace.AddText();
		unit.Multiline = true;
		_global.Project.CurrentPaper.CurrentFace.ChangeCurrentUnit(unit);
		SATracking.AddText();

		var xml:XMLNode = unit.GetXmlNode(true);
		var action:Actions.AddUnitAction = new Actions.AddUnitAction(unit, xml); 
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	private function onAddArtisticText(eventObject:Object) : Void {
		pluginsService.IsModuleAvailable("TextEffectModule");
	}
	
	private function IsTextEffectsModuleAvaliable(eventObject:Object) : Void {
		if(eventObject.isAvaliable) {
			var unit = _global.Project.CurrentPaper.CurrentFace.AddText();
			unit.Multiline = true;
			_global.Project.CurrentPaper.CurrentFace.ChangeCurrentUnit(unit);
			_global.Project.CurrentUnit.ReplaceText(0, 0, 'Text', null);
			Logic.TextLogic.ConvertTextToEffect(_global.Project.CurrentUnit,'Balloon1');
			_global.Project.CurrentUnit.RegisterOnConvertHandler(this, NeedToConvertTextEffectUnit);
			SATracking.AddArtisticText();
			unit = _global.Project.CurrentUnit;

			var xml:XMLNode = unit.GetXmlNode();
			var action:Actions.AddUnitAction = new Actions.AddUnitAction(unit, xml); 
			_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
		}
	}

	private function NeedToConvertTextEffectUnit(eventObject:Object) : Void {
		_global.Project.CurrentPaper.CurrentFace.ConvertEffectToTextUnit(eventObject.target);
		eventObject.target.IntervalId = setInterval(this, "AttachAfterConvert", 100, eventObject.target);
	}
	
	private function AttachAfterConvert() : Void {
		_global.UIController.FramesController.attach("Frame", true);
	}
	
	private function onAddPlaylist(eventObject:Object) : Void {
		trace("AddPlaylistUnit");
		
		//Workaround: No Another fast way to do this:
		_global.GlobalNotificator.OnComponentLoaded("Tools.AddPlaylistToolShow", null);
	}
	
	function DataBind():Void {
		textToolContainer.SelectTool("TextProperties");
		_global.Mode = _global.TextMode;
		
		var unit = _global.Project.CurrentUnit;
		if (unit.EmptyLayoutItem) {
			var fmt:TextFormat = unit.GetFontFormat(0);
			var len:Number = unit.Text.length;
			unit.ReplaceText(0, len, "", fmt);
			if(unit instanceof TextEffectUnit){
				unit.Draw();
			}
			unit.EmptyLayoutItem = false;
		}
		
		var data:Object = new Object();
		data.multiline = unit.Multiline;
		data.text = unit.Text;
		data.align = unit.Align;
		data.fontSizeActive = true;
		if(unit instanceof TextEffectUnit) {
			data.unitType = 'Effect';
			if(unit.effect.type != 'Guided' && unit.effect.type != 'GuidedFit' && unit.effect.type != 'fit')
				data.fontSizeActive = true;
			
		} else {
			data.unitType = 'Text';
		}
		textTool.DataBind(data);
		
	}
	//SB - Event from Specchar tool panel's button
	public function CallSpeccharPanelButton(strIndex:String) : Void {		
		var eventObject:Object = {oldBegin:textTool.getSelectionBeginIndex(), oldEnd:textTool.getSelectionEndIndex(), insertedText:textTool.getCharSpeccharTool(strIndex), format:textTool.getFormatSpeccharTool()};
		textTool.ReplaceTextArea(eventObject);
		onChangeText(eventObject);
		
	}
	
	function RefreshFontSize() : Void {
		textTool.RefreshFontSize(_global.Project.CurrentUnit.Size);
		//SB
		_global.SelectionFrame.RedrawUnit();
	}
	
	function OnEffect(eventObject:Object) : Void {
		trace('OnEffect'+eventObject.index+eventObject.effectName);
		var unit = _global.Project.CurrentUnit;
		var xml:XMLNode = unit.GetXmlNode();
		var oldEffect:String = unit instanceof TextEffectUnit ? unit.getEffect().name : "IsTextUnit";
		/*if(eventObject.index == 0){
			_global.Project.CurrentPaper.CurrentFace.ConvertEffectToTextUnit(_global.Project.CurrentUnit);
		}
		else
			_global.Project.CurrentPaper.CurrentFace.ConvertTextUnitToEffect(_global.Project.CurrentUnit, _global.Effects.GetEffect(eventObject.effectName));*/
  		if(eventObject.index == 0){
  			Logic.TextLogic.ConvertEffectToText(unit);
  		}
  		else {
  			Logic.TextLogic.ConvertTextToEffect(unit, eventObject.effectName);
  		}
  	    //SB
		_global.SelectionFrame.RedrawUnit();

		unit = _global.Project.CurrentUnit;
		var newEffect:String = unit instanceof TextEffectUnit ? unit.getEffect().name : "IsTextUnit";
		
		var action:Actions.TextEffectChangeAction = new Actions.TextEffectChangeAction(unit, oldEffect, newEffect, xml); 
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
		
	public function OnChangePanel(eventObj:Object) : Void {
		trace("TextToolsContainer OnChangePanel + "+eventObj.toolName);
		if(eventObj.toolName == 'TextEffects'){
			_global.Mode = _global.TextEffectMode;
			Selection.setFocus(null);
			effectTool.DataSource = _global.Effects.GetEffectsArray();
			effectTool.DataBind(true);
		} else {
			Selection.setFocus(null);
			DataBind();
		}
	}
	
	public function OnUpdateTextEffect(eventObj:Object) : Void {
		_global.Project.CurrentUnit.UpdateTextEffect();
	}

}
