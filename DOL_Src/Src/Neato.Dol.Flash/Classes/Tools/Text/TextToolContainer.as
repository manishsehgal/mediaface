﻿import mx.core.UIObject;
import mx.utils.Delegate;

class Tools.Text.TextToolContainer extends UIObject {
	
	private var accTextTools: CtrlLib.AccordionEx;
	
	function TextToolContainer() {
		instance = this;
	}
	
	static private var instance   : Tools.Text.TextToolContainer;
	static function getInstance() : Tools.Text.TextToolContainer {
		return instance;
	}
	
	function onLoad() {
		accTextTools.setStyle("backgroundColor", "0xf7f7f7");

		accTextTools.RegisterOnTabClickHandler(this, onToolClick);
		_global.GlobalNotificator.OnComponentLoaded("Tools.TextToolsContainer", this);
		trace('_global.GlobalNotificator.OnComponentLoaded("Tools.TextToolsContainer", this);');
	}
	
	private function onToolClick(eventObject) {
		trace('OnToolClick!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1');
		var toolName = accTextTools.getChildAt(eventObject.newValue)._name;
		var eventObject = {type:"TexttoolClick", target:this, toolName: toolName};
		this.dispatchEvent(eventObject);
	}
	
	public function RegisterOnToolClickHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("TexttoolClick", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function SelectTool(toolName:String) {
		var length:Number = accTextTools.numChildren;
		for(var index:Number = 0; index < length; ++index) {
			if (toolName == accTextTools.getChildAt(index)._name) {
				if (accTextTools.selectedIndex != index)
					accTextTools.selectedIndex = index;
				break;
			}
		}
	}
	
	public function AdjustHeight(height:Number) {
		this.accTextTools.setSize(this.accTextTools.width, height);
	}

}