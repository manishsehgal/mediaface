interface Tools.Text.ISelectionText {
	function GetText():String;
	
	function SetText(value:String):Void;
	
	public function RegisterOnSelectionChangeHandler(scopeObject:Object, callBackFunction:Function) : Void;
	
	public function RegisterOnTextChangedHandler(scopeObject:Object, callBackFunction:Function) : Void;
	
	public function SelectAllText() : Void;
	
	public function RestoreSelection() : Void;
	
	public function GetSelectionBeginIndex():Number;
	
	public function GetSelectionEndIndex():Number;
	
	public function SetSelection(begin:Number, end:Number) : Void;
	
}