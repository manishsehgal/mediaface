import mx.utils.Delegate;

[Event("SelectionChange")]
[Event("TextChanged")]
class Tools.Text.SelectionTextArea  extends CtrlLib.TextAreaEx implements Tools.Text.ISelectionText {
	static var symbolName:String = "SelectionTextArea";
    static var symbolOwner:Object = Object(Tools.Text.SelectionTextArea);
	// it's a part of SelectionTextInput and SelectionTextArea classes. Do not check syntax
	private var keyListener:Object;
	private var focusListener:Object;
	private var mouseListener:Object;;
	
	function onLoad() : Void {
		keyListener = new Object();
		keyListener.parent = this;
		keyListener.keyUp = function(eventObject:Object) {
			if (_global.Mode == _global.TextMode) {
				switch(eventObject.code) {
					case Key.LEFT:
					case Key.RIGHT:
					case Key.HOME:
					case Key.END:
					case Key.PGDN:
					case Key.PGUP:
					case Key.UP:
					case Key.DOWN:
						this.parent.OnSelectionChange();
						break;
					case Key.ENTER:
						break;
				}
			}
		};
		addEventListener("keyUp", keyListener);
		
		focusListener = new Object();
		focusListener.parent = this;
		focusListener.focusIn = function(eventObject:Object) {};
		addEventListener("focusIn", focusListener);
		
		mouseListener = new Object();
		mouseListener.parent = this;
		mouseListener.onMouseDown = function() {
			if (this.parent.hitTest(_root._xmouse, _root._ymouse, false)
				&& this.parent.getFocus() == this.parent.label
				&& ((_global.Mode == _global.TextMode) || (_global.Mode == "PlaylistEdit" )))
			{
				this.flag = true;
				this.parent.OnSelectionChange();
			}
		};
		mouseListener.onMouseMove = function() {
			if (this.flag) {
				if (Selection.getBeginIndex() != this.parent.beginIndex || Selection.getEndIndex() != this.parent.endIndex) {
					this.parent.OnSelectionChange();
				}
			}
		};
		mouseListener.onMouseUp = function() {
			if (this.flag) {
				this.flag = false;
				this.parent.OnSelectionChange();
			}
		};
		Mouse.addListener(mouseListener);
		
		this.addEventListener("change", Delegate.create(this, SelectionTextInput_OnChange));
	}
	
	function GetText():String {
		var str:String = super.text;
		var str2:String = "";
		for (var i:Number=0; i < str.length; ++i) {
			str2 += str.charCodeAt(i).toString()+"|";
		}
		trace("GetText() = "+str2);
		return super.text;
	}

	function SetText(value:String):Void {
		oldLength = value.length;
		beginIndex = 0;
		endIndex = 0;
		caretIndex = 0;

		super.text = value;
	}

	private var beginIndex:Number = 0;
	private var endIndex:Number = 0;
	private var caretIndex:Number = 0;
	private var oldLength:Number = 0;
	
	private function SelectionTextInput_OnChange(eventObject:Object) : Void {
		OnTextChanged();
		OnSelectionChange();
	}
	
	public function RegisterOnSelectionChangeHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("SelectionChange", Delegate.create(scopeObject, callBackFunction));
    }
	
	private function OnSelectionChange():Void {
		caretIndex = Selection.getCaretIndex();
		beginIndex = Selection.getBeginIndex();
		endIndex = Selection.getEndIndex();
		SelectionTrick();

		var eventObject:Object = {type:"SelectionChange", target:this, beginIndex:beginIndex , endIndex:endIndex, caretIndex:caretIndex};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnTextChangedHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("TextChanged", Delegate.create(scopeObject, callBackFunction));
    }

	private function SelectionTrick() : Void {
		var currentSelection:Object = Selection;
		currentSelection.lastBeginIndex = beginIndex;
		currentSelection.lastEndIndex = endIndex;
	}
	
	private function OnTextChanged():Void {			
		caretIndex = Selection.getCaretIndex();
		var begin:Number = 0;
		var end:Number = 0;
		var insertedText:String;
		var labelTxt:String = this.label.text;
		trace("labelTxt = "+labelTxt);

		if(beginIndex==endIndex) {
			if (this.length < oldLength) {
				if (beginIndex > caretIndex) {
					insertedText = "";
					begin = beginIndex - (oldLength - this.length);
					end = beginIndex;
				} else {
					insertedText = "";
					begin = beginIndex;
					end = beginIndex + (oldLength - this.length);
				}
			} else {
				insertedText = labelTxt.substring(beginIndex, beginIndex + labelTxt.length - oldLength);
				begin = beginIndex;
				end = endIndex;
			}
		} else {
			if (beginIndex==caretIndex) {
				insertedText = "";
			} else {
				insertedText = labelTxt.substring(beginIndex, caretIndex);
			}
			begin = beginIndex;
			end = endIndex;
		}

		beginIndex = Selection.getBeginIndex();
		endIndex = Selection.getEndIndex();
		oldLength = this.length;
		
		SelectionTrick();

		var eventObject:Object = {type:"TextChanged", target:this, oldBegin:begin , oldEnd:end, insertedText:insertedText, caretIndex:caretIndex};
		this.dispatchEvent(eventObject);

	}

	public function SelectAllText() : Void {
		SetSelection(0, this.length);
		OnSelectionChange();
	}
	
	public function RestoreSelection() : Void {
		Selection.setFocus(null);
		SetSelection(beginIndex, endIndex);
	}
	
	public function SetSelection(begin:Number, end:Number) : Void {
		beginIndex = begin;
		endIndex = end;
		Selection.setFocus(this.label);
		SelectionTrick();
		Selection.setSelection(begin, end);
	}
	
	public function GetSelectionBeginIndex():Number {
		return this.beginIndex;
	}

	public function GetSelectionEndIndex():Number {
		return this.endIndex;
	}
}