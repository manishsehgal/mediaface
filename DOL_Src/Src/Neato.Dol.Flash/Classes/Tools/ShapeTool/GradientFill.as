﻿import mx.utils.Delegate;
import mx.core.UIObject;

class Tools.ShapeTool.GradientFill extends UIObject {
	private var fillColorTool:Tools.ColorSelectionTool.ColorTool;
	private var fillColorTool2:Tools.ColorSelectionTool.ColorTool;
	private var lblFillColor:CtrlLib.LabelEx;
	private var lblFillColor2:CtrlLib.LabelEx;

	private var boundingBox_mc  : MovieClip;
	private var ctlGradientType;
	private var xGap:Number = 4.5;
	private var yGap:Number = 4;
	
	private var lastSelectedGradientIndex:Number = -1;
	
	function onLoad() {
		_global.GlobalNotificator.OnComponentLoaded("Tools.ShapeGradientFill", this);
		InitLocale();
		
		fillColorTool.RegisterOnSelectHandler(this, OnFill);
		fillColorTool2.RegisterOnSelectHandler(this, OnFill);
		ctlGradientType.RegisterOnChangeHandler(this, ctlGradientType_OnChange);
	}
	
	private function InitLocale():Void {
		_global.LocalHelper.LocalizeInstance(lblFillColor, "ToolProperties", "IDS_LBLSHAPEFILLCOLOR");
		_global.LocalHelper.LocalizeInstance(lblFillColor2, "ToolProperties", "IDS_LBLSHAPEFILLCOLOR");
	}
	
	function init():Void {
		super.init();

		if (boundingBox_mc != undefined) {
			boundingBox_mc._width = 0;
			boundingBox_mc._height = 0;
			boundingBox_mc._visible = false;
		}
	}
    
	function createChildren():Void {
		super.createChildren();
		if (ctlGradientType == undefined) {
			ctlGradientType = this.createObject("HListEx", "ctlGradientType", this.getNextHighestDepth());
		}
	}
	
	public function RegisterOnFillHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("FillChange", Delegate.create(scopeObject, callBackFunction));
	}
    
	private function OnFill(index:Number):Void {
		if (!(index instanceof Number))
			index = lastSelectedGradientIndex;
		if (lastSelectedGradientIndex == -1)
			index = 0;

		var color:Number = fillColorTool.RGB;
		var color2:Number = fillColorTool2.RGB;
		var fillType:String = "gradient";
		var gradientType:String = DataSource[index].name;

		var eventObject = {type:"FillChange", target:this, color:color, color2:color2, gradientType:gradientType};
		this.dispatchEvent(eventObject);
	}

	private function ctlGradientType_OnChange(eventObject) {
		if (!isNaN(eventObject.index)) {
			lastSelectedGradientIndex = eventObject.index;
			OnFill(eventObject.index);
			fillColorTool2.enabled = eventObject.index != 0;
		}
	}
	
	public var DataSource:Array;	
	public function DataBind(data:Object):Void {
		fillColorTool.SetColorState(data.color);
		fillColorTool2.SetColorState(data.color2);

		ctlGradientType.DataSource = DataSource;
		ctlGradientType.ItemLinkageName = "DrawingHolderEx";
		ctlGradientType.XGap = xGap;
		ctlGradientType.DataBind();
		
		fillColorTool2.enabled = data.gradientType != "" && data.gradientType != "00" && data.gradientType != null && data.gradientType != undefined;
		
		if (data.gradientType == "" || data.gradientType == "00" || data.gradientType == null || data.gradientType == undefined)
			lastSelectedGradientIndex = -1;
		else {
			for (var i:Number = 0; i < DataSource.length; ++ i) {
				if (DataSource[i].name == data.gradientType) {
					lastSelectedGradientIndex = i;
					break;
				}
			}
		}
	}
}