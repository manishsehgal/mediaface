﻿import mx.utils.Delegate;
import mx.core.UIObject;
import mx.controls.NumericStepper;

class Tools.ShapeTool.GeneralProperties extends UIObject {
	private var lblLineColor:CtrlLib.LabelEx;
	private var lblLineWeight:CtrlLib.LabelEx;

	private var chkKeepProportions:CtrlLib.CheckBoxEx;
	private var nsLineWeight:NumericStepper;
	private var lineColorTool:Tools.ColorSelectionTool.ColorTool;
	private var chkNoFill:CtrlLib.CheckBoxEx;

	private var btnDelete:CtrlLib.ButtonEx;
	private var btnRestoreProportions:CtrlLib.ButtonEx;
	
	function GeneralProperties() {}
	
	function onLoad() {
		_global.GlobalNotificator.OnComponentLoaded("Tools.ShapeGeneralProperties", this);
		InitLocale();
		
		this.setStyle("backgroundColor", "0xf7f7f7");
		
		nsLineWeight.inputField.editable = false;
		nsLineWeight.setStyle("backgroundColor", "0xf7f7f7");
		nsLineWeight.setStyle("orderColor", "0xd5d5d5");
		
		lineColorTool.RegisterOnSelectHandler(this, lineColorTool_SelectColor);
		chkKeepProportions.RegisterOnClickHandler(this, chkKeepProportions_OnClick);
		chkNoFill.RegisterOnClickHandler(this, chkNoFill_OnClick);
		nsLineWeight.addEventListener("change", Delegate.create(this, nsLineWeight_OnChange));
	}

	private function InitLocale():Void {
		_global.LocalHelper.LocalizeInstance(chkKeepProportions, "ToolProperties", "IDS_CHKKEEPPROPORTIONS");
		_global.LocalHelper.LocalizeInstance(lblLineColor, "ToolProperties", "IDS_LBLLINECOLOR");
		_global.LocalHelper.LocalizeInstance(lblLineWeight, "ToolProperties", "IDS_LBLLINEWEIGHT");
		_global.LocalHelper.LocalizeInstance(chkNoFill, "ToolProperties", "IDS_CHKNOFILL");
		
		TooltipHelper.SetTooltip2(chkNoFill, "IDS_TOOLTIP_NOFILL");
		TooltipHelper.SetTooltip2(btnDelete, "IDS_TOOLTIP_DELETE");
		TooltipHelper.SetTooltip2(chkKeepProportions, "IDS_TOOLTIP_KEEPPROPORTIONS");
		TooltipHelper.SetTooltip2(btnRestoreProportions, "IDS_TOOLTIP_RESTOREPROPORTIONS");
    }
	
	private function OnLineColorChange(color:Number):Void {
		var eventObject = {type:"LineColorChange", target:this, color:color};
		this.dispatchEvent(eventObject);
	}
	
	private function OnLineWeightChange(size:Number):Void {
		var eventObject = {type:"LineWeightChange", target:this, size:size};
		this.dispatchEvent(eventObject);
	}
	
	private function OnKeepProportions(selected:Boolean):Void {
		var eventObject = {type:"KeepProportionsChange", target:this, selected:selected};
		this.dispatchEvent(eventObject);
	}
	
	private function OnTransparency(selected:Boolean):Void {
		var eventObject = {type:"TransparencyChange", target:this, selected:selected};
		this.dispatchEvent(eventObject);
	}
	
	public function RegisterOnDeleteUnitHandler(scopeObject:Object, callBackFunction:Function) {
		btnDelete.RegisterOnClickHandler(scopeObject, callBackFunction);
	}
	
	public function RegisterOnRestoreProportionsHandler(scopeObject:Object, callBackFunction:Function) {
		btnRestoreProportions.RegisterOnClickHandler(scopeObject, callBackFunction);
	}
	
	public function RegisterOnKeepProportionsHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("KeepProportionsChange", Delegate.create(scopeObject, callBackFunction));
	}
	
	public function RegisterOnTransparencyChangeHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("TransparencyChange", Delegate.create(scopeObject, callBackFunction));
	}
	
	public function RegisterOnLineColorChangeHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("LineColorChange", Delegate.create(scopeObject, callBackFunction));
	}
	
	public function RegisterOnLineWeightChangeHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("LineWeightChange", Delegate.create(scopeObject, callBackFunction));
	}
	
	public function DataBind(data):Void {
		chkKeepProportions.selected = data.keepprops;
		chkKeepProportions.enabled  = data.scalable ? true : false;
		chkNoFill.selected = data.transparency;
		lineColorTool.SetColorState(data.lineColor);
		nsLineWeight.inputField.maxChars = 5;
	}

	public function RefreshLineWeight(newmax ,newval) {
		nsLineWeight.maximum = newmax;
		nsLineWeight.value = newval;
		nsLineWeight.stepSize = 0.5;
	}
	
	private function lineColorTool_SelectColor(eventObject:Object):Void {
		OnLineColorChange(eventObject.color);
	}
	
	private function nsLineWeight_OnChange(eventObject:Object):Void {
		OnLineWeightChange(nsLineWeight.inputField.label.value);
	}
	
	private function chkKeepProportions_OnClick(eventObject:Object):Void {
		OnKeepProportions(eventObject.target.selected);
	}
	
	private function chkNoFill_OnClick(eventObject:Object):Void {
		OnTransparency(eventObject.target.selected);
	}
	
	public function set NoFill(value:Boolean):Void {
		chkNoFill.selected = value;
	}
}