﻿import mx.utils.Delegate;
import mx.core.UIObject;
import CtrlLib.NumericStepperEx;


[Event("exit")]
class Tools.ShapeTool.ShapeEditToolContainer extends UIObject {
	private var chkKeepProportions:CtrlLib.CheckBoxEx;
	private var chkNoFill:CtrlLib.CheckBoxEx;
	private var btnDelete:CtrlLib.ButtonEx;
	private var btnRestoreProportions:CtrlLib.ButtonEx;
	private var lblShapeEdit:CtrlLib.LabelEx;
	private var lblFillColor:CtrlLib.LabelEx;
	private var lblLineColor:CtrlLib.LabelEx;
	private var lblLineWeight:CtrlLib.LabelEx;
	private var nsLineWeight:NumericStepperEx;
	private var btnContinue:CtrlLib.ButtonEx;
	
	private var fillColorTool:Tools.ColorSelectionTool.ColorTool;
	private var lineColorTool:Tools.ColorSelectionTool.ColorTool;
	private var fillColor:Number;
	private var lineColor:Number;
	
	function ShapeEditToolContainer() {
	}
	
	function onLoad() : Void {
		chkKeepProportions.addEventListener("click", Delegate.create(this, chkKeepProportions_OnClick));
		chkNoFill.addEventListener("click", Delegate.create(this, chkNoFill_OnClick));
		btnRestoreProportions.addEventListener("click", Delegate.create(this, btnRestoreProportions_OnClick));
		nsLineWeight.addEventListener("change", Delegate.create(this, nsLineWeight_OnChange));
		//nsLineWeight.addEventListener("focusOut", Delegate.create(this, nsLineWeight_OnChange));
		fillColorTool.RegisterOnSelectHandler(this, fillColorTool_OnSelect);
		lineColorTool.RegisterOnSelectHandler(this, lineColorTool_OnSelect);
		btnContinue.addEventListener("click", Delegate.create(this, btnContinue_OnClick));
		
		nsLineWeight.inputField.editable = false;
		_global.GlobalNotificator.OnComponentLoaded("Tools.ShapeEditTool", this);
		DataBind();
		InitLocale();
		
		nsLineWeight.setStyle("backgroundColor", "0xf7f7f7");
		nsLineWeight.setStyle("orderColor", "0xd5d5d5");
	}
	
	
 	private function chkNoFill_OnClick(eventObject:Object) : Void {
		var bFill:Boolean = chkNoFill.selected;
		OnFill(fillColor, bFill);
 		//
   	}

	private function chkKeepProportions_OnClick(eventObject:Object) : Void {
		OnKeepProportions(chkKeepProportions.selected);
		//	RefreshLineWeight();
	}
	
	private function btnRestoreProportions_OnClick(eventObject:Object) : Void {
		//	_global.CurrentUnit.ScaleX = _global.CurrentUnit.InitialScale;
		//_global.CurrentUnit.ScaleY = _global.CurrentUnit.InitialScale;
		//OnRestoreProps
		OnRestoreProps();
		
		//RefreshLineWeight();
		//_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
	}
	
	private function nsLineWeight_OnChange(eventObject:Object) : Void {
		//_global.tr("OnLineWeight(${nsLineWeight.inputField.label.value} = " +nsLineWeight.inputField.label.value+ ")");
		OnLineWeight(nsLineWeight.inputField.label.value);//_global.CurrentUnit.SetBorderWidth(eventObject.target.value)
		//RefreshLineWeight();
		//_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
	}


	/*public function DataBind():Void {
		chkKeepProportions.selected = _global.CurrentUnit.KeepProportions;
		chkKeepProportions.enabled = _global.CurrentUnit.IsNonScalable ? false : true;
		chkNoFill.selected = _global.CurrentUnit.Transparency;
		fillColor=_global.CurrentUnit.GetFillColor();
		lineColor=_global.CurrentUnit.GetBorderColor();
		RefreshLineWeight();
		fillColorTool.SetColorState(fillColor);
		lineColorTool.SetColorState(lineColor);
		nsLineWeight.inputField.maxChars = 5;
	}*/
	public function DataBind(data:Object):Void {
		chkKeepProportions.selected = data.keepprops;//_global.CurrentUnit.KeepProportions;
		chkKeepProportions.enabled = data.scalable?true:false;//_global.CurrentUnit.IsNonScalable ? false : true;
		chkNoFill.selected = data.transparency;//_global.CurrentUnit.Transparency;
		fillColor=data.fillColor;//_global.CurrentUnit.GetFillColor();
		lineColor=data.lineColor;//_global.CurrentUnit.GetBorderColor();
		fillColorTool.SetColorState(fillColor);
		lineColorTool.SetColorState(lineColor);
		nsLineWeight.inputField.maxChars = 5;
	}

	private function InitLocale() : Void {
		//_global.LocalHelper.LocalizeInstance(btnDelete, "ToolProperties", "IDS_BTNDELETE");
		_global.LocalHelper.LocalizeInstance(chkKeepProportions, "ToolProperties", "IDS_CHKKEEPPROPORTIONS");
		//_global.LocalHelper.LocalizeInstance(btnRestoreProportions, "ToolProperties", "IDS_BTNRESTOREPROPORTIONS");
		_global.LocalHelper.LocalizeInstance(lblShapeEdit, "ToolProperties", "IDS_LBLSHAPEEDIT");
		_global.LocalHelper.LocalizeInstance(lblFillColor, "ToolProperties", "IDS_LBLSHAPEFILLCOLOR");
		_global.LocalHelper.LocalizeInstance(lblLineColor, "ToolProperties", "IDS_LBLLINECOLOR");
		_global.LocalHelper.LocalizeInstance(lblLineWeight, "ToolProperties", "IDS_LBLLINEWEIGHT");
		_global.LocalHelper.LocalizeInstance(chkNoFill, "ToolProperties", "IDS_CHKNOFILL");
		//_global.LocalHelper.LocalizeInstance(btnContinue, "ToolProperties", "IDS_BTNCONTINUE");

		TooltipHelper.SetTooltip2(btnDelete, "IDS_TOOLTIP_DELETE");
		TooltipHelper.SetTooltip2(btnContinue, "IDS_TOOLTIP_CONTINUE");
		TooltipHelper.SetTooltip2(chkKeepProportions, "IDS_TOOLTIP_KEEPPROPORTIONS");
		TooltipHelper.SetTooltip2(btnRestoreProportions, "IDS_TOOLTIP_RESTOREPROPORTIONS");
		TooltipHelper.SetTooltip2(chkNoFill, "IDS_TOOLTIP_NOFILL");
    }
	
	private function fillColorTool_OnSelect(eventObject:Object) : Void {
		chkNoFill.selected = false;
		fillColor = eventObject.color;
		//_global.CurrentUnit.SetFill(fillColor, chkNoFill.selected);
		OnFill(fillColor, chkNoFill.selected);

	}
	
	private function lineColorTool_OnSelect(eventObject:Object) : Void {
		lineColor = eventObject.color;
		//_global.CurrentUnit.SetBorderColor(lineColor);
		OnBorderColorChanged(lineColor);
	}
	function RefreshLineWeight(newmax:Number, newval:Number) : Void {
		//_global.tr("RefreshLineWeight ${newmax} = " + newmax + " ${newval} = "+ newval);
		nsLineWeight.maximum=newmax;
		nsLineWeight.value=newval;
		nsLineWeight.stepSize=0.5;
	}
	
	private function btnContinue_OnClick() : Void {
		OnExit();
	}
	
	
	//region Exit event
	private function OnExit() : Void {
		var eventObject:Object = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
	
	private function OnFill(fillColor:Number, chkNoFill:Boolean) : Void {
		var eventObject:Object = {type:"onfill", target:this, color:fillColor, fill:chkNoFill};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnFillClickHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("onfill", Delegate.create(scopeObject, callBackFunction));
    }
	private function OnKeepProportions(selected:Boolean) : Void {
		var eventObject:Object = {type:"keepprops", target:this, selected:selected};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnKeepPropsHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("keepprops", Delegate.create(scopeObject, callBackFunction));
    }
	private function OnBorderColorChanged(newcolor:Number) : Void {
		var eventObject:Object = {type:"bordcolor", target:this, color:newcolor};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnBorderColorHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("bordcolor", Delegate.create(scopeObject, callBackFunction));
    }
	private function OnLineWeight(newSize:Number) : Void {
		var eventObject:Object = {type:"lineweight", target:this, size:newSize};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnLineWeightHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("lineweight", Delegate.create(scopeObject, callBackFunction));
    }
	private function OnRestoreProps() : Void {
		var eventObject:Object = {type:"restoreprops", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnRestorePropsHandler(scopeObject:Object, callBackFunction:Function) : Void {
		this.addEventListener("restoreprops", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function RegisterOnDeleteUnitHandler(scopeObject:Object, callBackFunction:Function) : Void {
		btnDelete.RegisterOnClickHandler(scopeObject, callBackFunction);
	}
}
