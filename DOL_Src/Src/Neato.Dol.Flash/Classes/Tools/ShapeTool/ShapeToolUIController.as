import flash.net.FileReference;
import Actions.KeepProportionsAction;
import Actions.TransparentFillAction;

class Tools.ShapeTool.ShapeToolUIController {
	private var shapeAddTool:Tools.ShapeTool.ShapeAddToolContainer;
	private var shapeEditTool:Tools.ShapeTool.ShapeEditToolContainer;
	private var shapeGeneralProperties:Tools.ShapeTool.GeneralProperties;
	private var shapeGradientFill:Tools.ShapeTool.GradientFill;
	private var shapeImageFill:Tools.ImageTool.AddImage;
	
	private var imageId:String;
	private var imageLibraryXml:XML;

	private var mode:String = null;
	var fileRef:FileReference;
	private var allTypes:Array;
	
	public function ShapeToolUIController() {
		trace("ShapeToolUIController ctr");

		allTypes = new Array();
		var imageTypes:Object = new Object();
		var extensions:Array = new Array("*.jpg", "*.jpeg", "*.gif", "*.bmp", "*.png", "*.tif", "*.tiff");
		imageTypes.description = "Images (" + extensions.join(", ") + ")";
		imageTypes.extension = extensions.join("; ");
		allTypes.push(imageTypes);

		fileRef = new FileReference();
		fileRef.addListener(GetListener());
	}
	
	function OnComponentLoaded(compName:String, compObj:Object, param) : Void {
		trace("ShapeTool OnComponentLoaded");
		var tool:String = compName.split(".")[1];
		if(tool == "ShapeAddTool") {
			shapeAddTool = Tools.ShapeTool.ShapeAddToolContainer(compObj);
			shapeAddTool.RegisterOnAddHandler(this, addTool_OnClick);
			//shapeAddTool.RegisterOnSelectHandler(this, colorTool_OnSelect);
		}
		else if (tool == "ShapeGeneralProperties") {
			shapeGeneralProperties = Tools.ShapeTool.GeneralProperties(compObj);
			shapeGeneralProperties.RegisterOnKeepProportionsHandler(this, editTool_chkKeepPropsClick);
			shapeGeneralProperties.RegisterOnLineColorChangeHandler(this, editTool_chkBorderColorChange);
			shapeGeneralProperties.RegisterOnLineWeightChangeHandler(this, editTool_chkLineWeightChange);
			shapeGeneralProperties.RegisterOnRestoreProportionsHandler(this, editTool_onRestoreProps);
			shapeGeneralProperties.RegisterOnTransparencyChangeHandler(this, editTool_onTransparency);
		}
		else if (tool == "ShapeGradientFill") {
			shapeGradientFill = Tools.ShapeTool.GradientFill(compObj);
			shapeGradientFill.RegisterOnFillHandler(this, editTool_OnGradientFill);
		}
		else if (tool == "ShapeImageFill") {
			shapeImageFill = Tools.ImageTool.AddImage(compObj);
			shapeImageFill.RegisterOnChangeHandler (this, OnAddImage);
			shapeImageFill.RegisterOnUploadHandler (this, OnUploadImage);
			
			compObj.ctlImageList.setSize(compObj.ctlImageList.width, 110);
			compObj.btnUpload.move(compObj.btnUpload.x, compObj.ctlImageList.y + 125);
		}
	}
	
	public function get CanAddImage():Boolean {
		return mode == null;
	}
	
	private function GetListener():Object {
		var listener:Object = new Object(); 
		
		var parent = this;
		listener.onSelect = function(file:FileReference):Void {
			_global.tr("onSelect");
			Preloader.Show();
			parent.mode = "upload";
			parent.CommandAddLibraryImage(null);
		};
		
		listener.onCancel = function(file:FileReference):Void {
			Preloader.Hide();
			_global.tr("onCancel");
		};
		
		listener.onOpen = function(file:FileReference):Void {
			_global.tr("onOpen: " + file.name);
		};
		
		listener.onProgress = function(file:FileReference, bytesLoaded:Number, bytesTotal:Number):Void {
			_global.tr("onProgress with bytesLoaded: " + bytesLoaded + " bytesTotal: " + bytesTotal);
			if (bytesLoaded == bytesTotal) {
			}
		};
		
		listener.onComplete = function(file:FileReference):Void {
			Preloader.Hide();
			_global.tr("onComplete: " + file.name);
			var imageLibariryService:SAImages = SAImages.GetInstance(new XML());
			imageLibariryService.RegisterOnAddedToProjectHandler(parent, parent.editTool_OnImageFill);
			imageLibariryService.GetLastImageInfo();
		};
		
		listener.onHTTPError = function(file:FileReference, httpError:Number):Void {
			Preloader.Hide();
			_global.tr("onHTTPError: " + file.name + " http error = " + httpError);
		};
		
		listener.onIOError = function(file:FileReference):Void {
			Preloader.Hide();
			_global.tr("onIOError: " + file.name);
		};
		
		listener.onSecurityError = function(file:FileReference, errorString:String):Void {
			_global.tr("onSecurityError: " + file.name + " errorString: " + errorString);
		};
		
		return listener;
	}

	public static function Contains(array:Array, value):Number {
		for (var i in array)
			if (value == array[i])
				return i;
		return -1;
	}
	
	public function CommandAddLibraryImage(eventObject:Object) : Void {
		_global.UIController.OnUnitChanged();
		var imageLibariryService:SAImages = SAImages.GetInstance(new XML());
		_global.tr("### CommandAddLibraryImage() mode = " + mode);
		switch(mode) {
			case "add" :
				//SATracking.AddImage();
				imageLibariryService.RegisterOnAddedToProjectHandler(this, editTool_OnImageFill);
				imageLibariryService.BeginAddImageToProject(imageId);
				break;
			case "upload" :
				_global.tr("_global.freeSize = " + _global.freeSize);
				if (fileRef.size <= _global.freeSize) {
					//SATracking.AddCustomImage();
					imageLibariryService.BeginUploadImage(fileRef);
				} else {
					Preloader.Hide();
					_global.MessageBox.Alert(
						" You image file must not exceed " + int(_global.freeSize / 1024)+"K",
	 					" Warning", null);
				}
				break;
		}
	}
	
	private function OnAddImage(eventObject:Object):Void {
		//_global.tr("### OnAddImage");
		imageId = eventObject.id;
		if (imageId != null && imageId != undefined && CanAddImage == true) {
			mode = "add";
			CommandAddLibraryImage(null);
		}
	}
	
	private function OnUploadImage(eventObject:Object):Void {
		//_global.tr("### OnUploadImage");
		fileRef.browse(allTypes);
	}

	public function LoadImageLibrary():Void {
		this.imageLibraryXml = new XML();
		var imageLibariryService:SAImages = SAImages.GetInstance(this.imageLibraryXml);
		imageLibariryService.RegisterOnLoadedHandler(this, ImageLibraryDataBind);
		imageLibariryService.BeginLoad();
	}
	
	private function ImageLibraryDataBind(eventObject:Object) : Void {
		shapeImageFill.DataSource = imageLibraryXml;
		shapeImageFill.ImageIconUrlFormat = _global.LibraryImageIconUrlFormat;
		shapeImageFill.DataBind();
	}
	
	public function RegisterOnDeleteUnitHandler(scopeObject:Object, callBackFunction:Function) : Void {
		shapeGeneralProperties.RegisterOnDeleteUnitHandler(scopeObject, callBackFunction);
	}
	
	function AddDataBind():Void {
		_global.Mode = _global.AddShapeMode;
		shapeAddTool.DataSource = _global.Shapes.GetShapesArray();
		shapeAddTool.DataBind();
	}
	function EditDataBind():Void {
		//_global.tr("ShapeToolUICOntroller::EditDataBind");
		var data:Object = new Object();
		data.keepprops = _global.Project.CurrentUnit.KeepProportions;
		data.scalable = _global.Project.CurrentUnit.IsNonScalable ? false : true;
		data.transparency = _global.Project.CurrentUnit.Transparency;
		data.color = _global.Project.CurrentUnit.FillColor;
		data.color2 = _global.Project.CurrentUnit.FillColor2;
		data.lineColor = _global.Project.CurrentUnit.BorderColor;
		data.gradientType = _global.Project.CurrentUnit.GradientType;
		shapeGradientFill.DataSource = _global.Gradients.GetGradientsArray();
		shapeGeneralProperties.DataBind(data);
		shapeGradientFill.DataBind(data);
		editToolLineWeightRefresh();
		
	}
	function addTool_OnClick(eventObj:Object) : Void {
		trace("addTool_OnClick");
		var shapeType:String = eventObj.shapeType;
		SATracking.AddShape(shapeType);
		var unit = _global.Project.CurrentPaper.CurrentFace.CreateNewShapeUnit(shapeType);

		var xml:XMLNode = unit.GetXmlNode();
		var action:Actions.AddUnitAction = new Actions.AddUnitAction(unit, xml); 
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	function editTool_OnGradientFill(eventObj:Object) : Void {
		var unit = _global.Project.CurrentUnit;
		var oldColor:Number = unit.FillColor;
		var oldColor2:Number = unit.FillColor2;
		var oldGradientType:String = unit.GradientType;
		var oldIsFilled:Boolean = unit.Transparency;
		var oldImageId:String = unit.FillImageId;
		var oldFillType:String = unit.FillType;
		var oldImageUrl:String = unit.FillImageUrl;

		_global.Project.CurrentUnit.Fill(eventObj.color, eventObj.color2, eventObj.gradientType);
		shapeGeneralProperties.NoFill = false;

		var newColor:Number = unit.FillColor;
		var newColor2:Number = unit.FillColor2;
		var newGradientType:String = unit.GradientType;
		var newIsFilled:Boolean = unit.Transparency;
		var newImageId:String = null;
		var newFillType:String = unit.FillType;
		
		var action:Actions.ImageFillAction = new Actions.ImageFillAction(unit, oldColor, oldColor2, oldGradientType, oldIsFilled, oldImageId, oldFillType, newColor, newColor2, newGradientType, newIsFilled, newImageId, newFillType, oldImageUrl); 
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	function editTool_chkKeepPropsClick(eventObj:Object) : Void {
		var oldKeepProportions:Boolean = _global.Project.CurrentUnit.KeepProportions;

		_global.Project.CurrentUnit.KeepProportions = eventObj.selected;
		editToolLineWeightRefresh();

		var newKeepProportions:Boolean = _global.Project.CurrentUnit.KeepProportions;
		var action:Actions.KeepProportionsAction = new Actions.KeepProportionsAction(_global.Project.CurrentUnit, oldKeepProportions, newKeepProportions);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	function editTool_chkBorderColorChange(eventObj:Object) : Void {
		var oldColor:Number = _global.Project.CurrentUnit.BorderColor; 
		_global.Project.CurrentUnit.BorderColor = eventObj.color;
		var newColor:Number = _global.Project.CurrentUnit.BorderColor;
		
		var action:Actions.LineColorAction = new Actions.LineColorAction(_global.Project.CurrentUnit, oldColor, newColor);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	function editTool_chkLineWeightChange(eventObj:Object) : Void {
		//_global.tr("editTool_chkLineWeightChange " + eventObj.size);
		var oldLineWeight:Number = _global.Project.CurrentUnit.BorderWidth;

		var newSize:Number = eventObj.size;
		var maxBorderWidth:Number = _global.Project.CurrentUnit.MaxBorderWidth;
		if(maxBorderWidth < newSize)
			newSize = maxBorderWidth;
		_global.Project.CurrentUnit.BorderWidth = newSize;
		editToolLineWeightRefresh();
		_global.SelectionFrame.AttachUnit(_global.Project.CurrentUnit);
		
		var newLineWeight:Number = _global.Project.CurrentUnit.BorderWidth;
		var action:Actions.LineWeightAction = new Actions.LineWeightAction(_global.Project.CurrentUnit, oldLineWeight, newLineWeight);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	function editToolLineWeightRefresh() : Void {
		//_global.tr("editToolLineWeightRefresh");
		var newmaximum:Number = _global.Project.CurrentUnit.MaxBorderWidth;
		var newweight:Number = _global.Project.CurrentUnit.BorderWidth;
		shapeGeneralProperties.RefreshLineWeight(newmaximum, newweight);
	}
	
	function editTool_onRestoreProps(eventObj:Object) : Void {
		trace("editTool_onRestoreProps");
		var unit = _global.Project.CurrentUnit;
		var scaleXString:String = Actions.ScaleAction.ScaleXProperty(unit);
		var scaleYString:String = Actions.ScaleAction.ScaleYProperty(unit);
		var scaleX:Number = unit[scaleXString];
		var scaleY:Number = unit[scaleYString];

		_global.Project.CurrentUnit.RestoreProportions();
		_global.UIController.FramesController.attach(_global.Project.CurrentUnit.frameLinkageName,false);

		var scaleXNew:Number = unit[scaleXString];
		var scaleYNew:Number = unit[scaleYString];

		var action:Actions.ScaleAction = new Actions.ScaleAction(unit, scaleX, scaleY, scaleXNew, scaleYNew);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Focused = false;
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	function editTool_onTransparency(eventObj:Object) : Void {
		var oldTransparency:Boolean = _global.Project.CurrentUnit.Transparency;
		_global.Project.CurrentUnit.Transparency = eventObj.selected;
		var newTransparency:Boolean = _global.Project.CurrentUnit.Transparency;

		var action:Actions.TransparentFillAction = new Actions.TransparentFillAction(_global.Project.CurrentUnit, oldTransparency, newTransparency);
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	function editTool_onDelete() : Void {
		trace("editTool_onDelete");
		_global.DeleteCurrentUnit();
	}
	
	function editTool_OnImageFill(eventObject:Object) : Void {
		var imageDoc:XML = new XML(eventObject.LastInsertedImage);

		var unit = _global.Project.CurrentUnit;
		var oldColor:Number = unit.FillColor;
		var oldColor2:Number = unit.FillColor2;
		var oldGradientType:String = unit.GradientType;
		var oldIsFilled:Boolean = unit.Transparency;
		var oldImageId:String = unit.FillImageId;
		var oldFillType:String = unit.FillType;
		var oldImageUrl:String = unit.FillImageUrl;

		unit.FillImageId = imageDoc.firstChild.attributes.id;
		unit.FillType = "image";
		unit.Transparency = false;
		shapeGeneralProperties.NoFill = false;
		mode = null;

		var newColor:Number = unit.FillColor;
		var newColor2:Number = unit.FillColor2;
		var newGradientType:String = unit.GradientType;
		var newIsFilled:Boolean = unit.Transparency;
		var newImageId:String = unit.FillImageId;
		var newFillType:String = unit.FillType;
		
		var action:Actions.ImageFillAction = new Actions.ImageFillAction(unit, oldColor, oldColor2, oldGradientType, oldIsFilled, oldImageId, oldFillType, newColor, newColor2, newGradientType, newIsFilled, newImageId, newFillType, oldImageUrl); 
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
}