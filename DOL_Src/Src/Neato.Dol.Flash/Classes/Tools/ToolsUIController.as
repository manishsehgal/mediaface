﻿
class Tools.ToolsUIController {
	
	private var fillToolController	      : Tools.FillTool.FillToolUIController;
	private var textToolController 		  : Tools.Text.TextToolUIController;
	private var shapeToolController	      : Tools.ShapeTool.ShapeToolUIController;
	private var changePaperToolController : Tools.ChangePaperTool.ChangePaperToolUIController;
	private var gradientToolController    : Tools.GradientTool.GradientToolUIController;
	private var imageToolController       : Tools.ImageTool.ImageToolUIController;
	private var paintToolController       : Tools.PaintTool.PaintToolUIController;
	private var playlistToolController    : Tools.PlaylistTool.PlaylistToolUIController;
	private var templateToolController    : Tools.TemplateTool.TemplateToolUIController;
	
	
	function ToolsUIController() {
		fillToolController = new Tools.FillTool.FillToolUIController();
		textToolController = new Tools.Text.TextToolUIController();
		shapeToolController = new Tools.ShapeTool.ShapeToolUIController();
		changePaperToolController = new Tools.ChangePaperTool.ChangePaperToolUIController();
		gradientToolController = new Tools.GradientTool.GradientToolUIController();
		
		paintToolController = new Tools.PaintTool.PaintToolUIController();
		imageToolController = new Tools.ImageTool.ImageToolUIController();
		playlistToolController = new Tools.PlaylistTool.PlaylistToolUIController();
		templateToolController = new Tools.TemplateTool.TemplateToolUIController();
	}
	
	public function get TextToolController() {
		return textToolController;
	}
	public function get PlaylistToolController() {
		return playlistToolController;
	}
	
	function OnComponentLoaded(compName:String, compObj:Object, param) :Void {
		if (compName == "Tools") {
			compObj.RegisterOnToolClickHandler(this, onToolClick);
		}
		var tool:String = compName.split(".")[1];
		_global.tr("OnComponentLoaded toolName = " + compName.split(".")[1]);
		switch(tool) {
			case "FillTool":
				fillToolController.OnComponentLoaded(compName, compObj, param);
				Tools.ToolContainer.getInstance().SelectTool("Background");
				break;
			case "TextToolsContainer":
				compObj.AdjustHeight(Tools.ToolContainer.getInstance().MaxToolHeight);
				textToolController.OnComponentLoaded(compName, compObj, param);
//				compObj.RegisterOnToolClickHandler(this, onToolClick);
				break;
			case "TextTool":
				textToolController.OnComponentLoaded(compName, compObj, param);
				textToolController.RegisterOnDeleteUnitHandler(this, OnDeleteUnit);
				break;
			case "TextToolCommands":
			case "TextEffectTool":
			    textToolController.OnComponentLoaded(compName, compObj, param);
				break;
			case "ShapeAddTool":
			case "ShapeGradientFill":
			case "ShapeImageFill":
				shapeToolController.OnComponentLoaded(compName, compObj, param);
				break;
			case "ShapeGeneralProperties":
				shapeToolController.OnComponentLoaded(compName, compObj, param);
				shapeToolController.RegisterOnDeleteUnitHandler(this, OnDeleteUnit);
				break;
			case "ChangePaperTool":
				changePaperToolController.OnComponentLoaded(compName, compObj, param);
				break;
			case "GradientAddTool":
				gradientToolController.OnComponentLoaded(compName, compObj, param);
				break;
			case "GradientEditTool":
				gradientToolController.OnComponentLoaded(compName, compObj, param);
				gradientToolController.RegisterOnDeleteUnitHandler(this, OnDeleteUnit);
				break;
			case "PaintEditTool":
				paintToolController.OnComponentLoaded(compName, compObj, param);
				paintToolController.RegisterOnDeleteUnitHandler(this, OnDeleteUnit, tool);
				break;
			case "PaintAddTool":
				paintToolController.OnComponentLoaded(compName, compObj, param);
				break;
			case "ImageTool":
				imageToolController.OnComponentLoaded(compName, compObj, param);
				imageToolController.RegisterOnDeleteUnitHandler(this, OnDeleteUnit);
				break;
			case "AddImageTool":
			case "ImageEffects":
			case "ImageToolContainer":
				imageToolController.OnComponentLoaded(compName, compObj, param);
				break;
			case "AddPlaylistToolShow":
				Tools.ToolContainer.getInstance().ShowPropertiesByName("Playlist");
				playlistToolController.AddDataBind();
				break;
			case "EditPlaylistTool":
				playlistToolController.OnComponentLoaded(compName, compObj, param);
				if (compName.split(".")[2] == "PlaylistGeneralProperties")
					playlistToolController.RegisterOnDeleteUnitHandler(this, OnDeleteUnit);
				break;
			case "AddPlaylistTool":
			case "PlaylistToolContainer":
				playlistToolController.OnComponentLoaded(compName, compObj, param);
				break;
			case 'TemplateToolContainer':
			case 'ChooseLayoutContainer':
			case 'ChooseTemplateContainer':
				templateToolController.OnComponentLoaded(compName, compObj, param);
				break;
		}
	}
	
	function onToolClick(eventObject:Object) : Void {
		_global.tr("onToolClick toolName = " + eventObject.toolName);
		
		//_global.tr("onToolClick eventObject.toolNamePrev = " + eventObject.toolNamePrev);
		if (eventObject.toolNamePrev == "Templates") {
			templateToolController.Clear();
			imageToolController.Clear();
		}
		
		if (_global.Mode == _global.ChangePaperMode) {
			Tools.ChangePaperTool.ChangePaperPanel.GetInstance().Hide();
		} else if(_global.Mode == _global.PaintMode) {
			paintToolController.paintAddTool_onEnd();
		}
		
		_global.UIController.UnselectCurrentUnit();
		
		switch(eventObject.toolName) {
			case "Background":
				Tools.ToolContainer.getInstance().SelectTool("Background");
				fillToolController.DataBind();
				break;
			case "Text":
				Tools.ToolContainer.getInstance().SelectTool("Text");
				textToolController.DataBind();
				break;
			case "ChangePaper":
				Tools.ToolContainer.getInstance().SelectTool("ChangePaper");
				changePaperToolController.DataBind();
				break;
			case "Shapes":
				trace("Tooluicontroller::OnToolClick::Shapes case");
				Tools.ToolContainer.getInstance().SelectTool("Shapes");
				shapeToolController.AddDataBind();
				shapeToolController.LoadImageLibrary();
				break;
			case "Gradient":
				Tools.ToolContainer.getInstance().SelectTool("Gradient");
				gradientToolController.AddDataBind();
				break;
			case "Images":
				Tools.ToolContainer.getInstance().SelectTool("Images");
				imageToolController.LoadImageLibrary();
				imageToolController.DataBind();
				break;
			case "Drawing":
				trace("ToolsUIController::OnToolClick::Drawing case");
				Tools.ToolContainer.getInstance().SelectTool("Drawing");
				paintToolController.AddDataBind();
				break;
			case "Playlist":
				Tools.ToolContainer.getInstance().SelectTool("Playlist");
				//playlistAddToolController.DataBind();
				break;
			case 'Templates':
				Tools.ToolContainer.getInstance().SelectTool("Templates");
				//playlistAddToolController.DataBind();
				templateToolController.DataBind();
				break;
		}
	}
	
	function OnUnitChanged() : Void {
		_global.tr("OnUnitChanged : mode = " + _global.Mode);
		templateToolController.Clear();
		imageToolController.Clear();
		switch (_global.Mode) {
			case _global.TextMode:
				Tools.ToolContainer.getInstance().SelectProperties("Text");
				textToolController.DataBind();
				break;
			case _global.ShapeEditMode:
				trace("Tooluicontroller::OnUnitChanged::ShapeEditMode case");
				Tools.ToolContainer.getInstance().SelectProperties("Shapes");				
				//Tools.ToolContainer.getInstance().SelectedItemIndexChanged(null, null, true);
				shapeToolController.EditDataBind();
				break;
			case _global.GradientEditMode:
				Tools.ToolContainer.getInstance().SelectProperties("Gradient");				
				gradientToolController.EditDataBind();
				break;
			case _global.ChangePaperMode:
				Tools.ToolContainer.getInstance().SelectTool("ChangePaper");
				changePaperToolController.DataBind();
          		break; 
			case _global.FillMode:
			case _global.InactiveMode:
				Tools.ToolContainer.getInstance().SelectTool("Background");
				fillToolController.DataBind();
				break;
			case _global.ImageMode:
			case _global.ImageEditMode:
				Tools.ToolContainer.getInstance().SelectProperties("Images");
				imageToolController.DataBind();
				break;
			//case _global.PaintMode:
			case _global.PaintEditMode:
				trace("global paint edit mode");
				Tools.ToolContainer.getInstance().SelectProperties("Drawing");
				paintToolController.EditDataBind();
				break;
			case _global.PlaylistMode:
				Tools.ToolContainer.getInstance().ShowPropertiesByName("Playlist");
				playlistToolController.AddDataBind();
			break;
			case _global.PlaylistEditMode:
				Tools.ToolContainer.getInstance().ShowPropertiesByName("Playlist");
				playlistToolController.EditDataBind();
			break;
			
		}
	}
	
	private function DeleteCurrentUnit() : Void {
		_global.tr("### DeleteCurrentUnitUnit");
		_global.Project.CurrentPaper.CurrentFace.DeleteCurrentUnit();
		_global.Mode = _global.InactiveMode;
		OnUnitChanged();
	}
	
	private function OnDeleteUnit() : Void {
		_global.tr("### DeleteUnit");
		var unit = _global.Project.CurrentUnit;
		var xml:XMLNode = unit.GetXmlNode();
		var depth:Number = _global.Project.CurrentPaper.CurrentFace.GetUnitIndex(unit.id);
		var action:Actions.DeleteUnitAction = new Actions.DeleteUnitAction(unit, xml, depth); 
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
		
		DeleteCurrentUnit();
	}
}
