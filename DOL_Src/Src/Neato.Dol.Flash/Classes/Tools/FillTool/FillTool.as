﻿import mx.utils.Delegate;
[Event("exit")]
class Tools.FillTool.FillTool extends mx.core.UIObject {	
	private var colorTool:Tools.ColorSelectionTool.ColorTool;
	private var colorTool2:Tools.ColorSelectionTool.ColorTool;
	
	private var boundingBox_mc  : MovieClip;
	private var ctlGradientType;

	private var xGap:Number = 4.5;
	private var yGap:Number = 4;
	
	private var lastSelectedGradientIndex:Number = -1;

	public function FillTool() {
		Preloader.Show();
		trace("FillTool ctr");
	}
	
	function init():Void {
		super.init();

		if (boundingBox_mc != undefined) {
			boundingBox_mc._width = 0;
			boundingBox_mc._height = 0;
			boundingBox_mc._visible = false;
		}
	}
    
	function createChildren():Void {
		super.createChildren();
		if (ctlGradientType == undefined) {
			ctlGradientType = this.createObject("HListEx", "ctlGradientType", this.getNextHighestDepth());
		}
	}
    
	function onLoad() {
		setStyle("styleName", "ToolPropertiesPanel");
		_global.GlobalNotificator.OnComponentLoaded("Tools.FillTool", this);
		colorTool.RegisterOnSelectHandler(this, OnSelect);
		colorTool2.RegisterOnSelectHandler(this, OnSelect);
		ctlGradientType.RegisterOnChangeHandler(this, ctlGradientType_OnChange);
		Preloader.Hide();
	}
	
	function OnSelect(index:Number):Void {
		if (!(index instanceof Number))
			index = lastSelectedGradientIndex;
		if (lastSelectedGradientIndex == -1)
			index = 0;
		
		var color = colorTool.RGB;
		var color2 = colorTool2.RGB;
		var gradientType = DataSource[index].name;

		var eventObject = {type:"select", target:this, color:color, color2:color2, gradientType:gradientType};
		this.dispatchEvent(eventObject);
	}
	
	public function RegisterOnSelectHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("select", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function RegisterOnClickHandler(scopeObject:Object, callBackFunction:Function) {
		colorTool.RegisterOnClickHandler(scopeObject, callBackFunction);
    }
	
	public var DataSource:Array;	
	function DataBind(data:Object) {
		colorTool.SetColorState(data.color);
		colorTool2.SetColorState(data.color2);

		ctlGradientType.DataSource = DataSource;
		ctlGradientType.ItemLinkageName = "DrawingHolderEx";
		ctlGradientType.XGap = xGap;
		ctlGradientType.DataBind();
		
		colorTool2.enabled = data.gradientType != "" && data.gradientType != "00" && data.gradientType != null && data.gradientType != undefined;
		
		if (data.gradientType == "" || data.gradientType == "00" || data.gradientType == null || data.gradientType == undefined)
			lastSelectedGradientIndex = -1;
		else {
			for (var i:Number = 0; i < DataSource.length; ++ i) {
				if (DataSource[i].name == data.gradientType) {
					lastSelectedGradientIndex = i;
					break;
				}
			}
		}
	}

	private function ctlGradientType_OnChange(eventObject) {
		if (!isNaN(eventObject.index)) {
			lastSelectedGradientIndex = eventObject.index;
			OnSelect(eventObject.index);
			colorTool2.enabled = eventObject.index != 0;
		}
	}
}