import Actions.GradientFillAction;
class Tools.FillTool.FillToolUIController {
	private var fillTool:Tools.FillTool.FillTool;
	
	public function FillToolUIController() {
		trace("FillToolUIController ctr");
	}
	
	function OnComponentLoaded(compName:String, compObj:Object, param) {
		trace("FillTool OnComponentLoaded");
		fillTool = Tools.FillTool.FillTool(compObj);
		fillTool.RegisterOnClickHandler(this, colorTool_OnClick);
		fillTool.RegisterOnSelectHandler(this, colorTool_OnSelect);
		DataBind();
	}
	
	private function colorTool_OnClick(eventObject) {
		trace("colorTool_OnClick");
		SATracking.ColorFill();
	}
	private function colorTool_OnSelect(eventObject) {
		//_global.tr("colorTool_OnSelect color = "+eventObject.color + " color2 = " + eventObject.color2 + " gradientType = '" + eventObject.gradientType + "'");
		var unit = _global.Project.CurrentPaper.CurrentFace;
		var oldColor:Number = unit.bgColor;
		var oldColor2:Number = unit.bgColor2;
		var oldGradientType:String = unit.gradientType;
		var oldIsFilled:Boolean = unit.isFilled;
		
		_global.Project.CurrentPaper.CurrentFace.Fill(eventObject.color, eventObject.color2, eventObject.gradientType);

		var newColor:Number = unit.bgColor;
		var newColor2:Number = unit.bgColor2;
		var newGradientType:String = unit.gradientType;
		var newIsFilled:Boolean = unit.isFilled;
		
		var action:Actions.GradientFillAction = new Actions.GradientFillAction(unit, oldColor, oldColor2, oldGradientType, oldIsFilled, newColor, newColor2, newGradientType, newIsFilled); 
		_global.Project.CurrentPaper.CurrentFace.ActManager.Insert(action);
	}
	
	function DataBind():Void {
		_global.Mode = _global.FillMode;
		
		var selectedColor = _global.Project.CurrentPaper.CurrentFace.bgColor;
		var selectedColor2 = _global.Project.CurrentPaper.CurrentFace.bgColor2;
		var gradientType = _global.Project.CurrentPaper.CurrentFace.gradientType;
		if (selectedColor == undefined)
			selectedColor = 0xFFFFFF;
		if (selectedColor2 == undefined)
			selectedColor2 = 0xFFFFFF;
		var data:Object = new Object();
		data.color = selectedColor;
		data.color2 = selectedColor2;
		data.gradientType = gradientType;
		fillTool.DataSource = _global.Gradients.GetGradientsArray();
		fillTool.DataBind(data);
	}
}