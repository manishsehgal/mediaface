﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;
[Event("onLoaded")]
class SAPaint {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SAPaint.prototype);

	private var types:XML;
	
	public static function GetInstance(types:XML):SAPaint {
		return new SAPaint(types);
	}
	
	public function get IsLoaded():Boolean {
		return types.loaded;
	}
	
	private function SAPaint(types:XML) {
		var parent = this;
		
		this.types = types;
		this.types.onLoad = function(success) {
			var eventObject = {type:"onLoaded", target:parent, name:"SAPaint"};
			trace("SAPaint: dispatch types onLoaded");
			parent.dispatchEvent(eventObject);
		};
	}

	public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }

	public function BeginLoad() {
		var currentDate:Date = new Date();
		var url:String = "PaintTypes.xml";
		if (SALocalWork.IsLocalWork)
			url = "../Neato.Dol.WebDesigner/PaintTypes.xml";
		BrowserHelper.InvokePageScript("log", url);
		
		this.types.load(url);
		BrowserHelper.InvokePageScript("log", "SAPaint.as: types loaded");
    }
}