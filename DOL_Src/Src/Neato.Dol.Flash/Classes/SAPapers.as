﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;
[Event("loaded")]
[Event("facesloaded")]
class SAPapers {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SAPapers.prototype);

	private var papersXml:XML;

	public static function GetInstance(papers:XML):SAPapers {
		return new SAPapers(papers);
	}

	public function get IsLoaded():Boolean {
		return papersXml.loaded;
	}

	private function SAPapers(papersXml:XML) {
		var parent = this;
		
		this.papersXml = papersXml;
		this.papersXml.onLoad = function(success) {
			var eventObject = {type:"loaded", target:parent, result:this};
			trace("SAPapers: dispatch papers onLoaded");
			parent.dispatchEvent(eventObject);
		};
	}

	public function BeginLoad() {
        trace("SAPapers BeginLoad");
		var currentDate:Date = new Date();
		var url:String = "Paper.aspx?mode=xml&time=" + currentDate.getTime();
		if (SALocalWork.IsLocalWork)
			url = "papers.xml";
		BrowserHelper.InvokePageScript("log", url);
        this.papersXml.load(url);
    }

    public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("loaded", Delegate.create(scopeObject, callBackFunction));
    }
    
	public function LoadPaperFaces(paperId:Number) {
        var currentDate:Date = new Date();
		var url:String = "Paper.aspx?mode=paper&id="+paperId.toString()+"&time=" + currentDate.getTime();
		BrowserHelper.InvokePageScript("log", url);
        
        var facesXml:XML = new XML();
		var parent = this;
		facesXml.onLoad = function(success) {
			var eventObject = {type:"facesloaded", target:parent, result:this};
			trace("SAPapers: dispatch faces onLoaded");
			parent.dispatchEvent(eventObject);
		};
        facesXml.load(url);
    }

    public function RegisterOnFacesLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("facesloaded", Delegate.create(scopeObject, callBackFunction));
    }
}