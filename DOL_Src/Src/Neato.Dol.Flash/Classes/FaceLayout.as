﻿class FaceLayout {
	public var Id:Number;
	public var FaceId:Number;
	public var url:String;
	public var guid:String;
	
	public var Items:Array;
	//public var drawSequence:Array;
	//public var textSequence:Array;
	
	function FaceLayout() {
		Items = new Array();
		//drawSequence = new Array();
		//textSequence = new Array();
	}
	
	public function get Guid() : String {
		return guid;
	}
	
	public function set Guid(value:String) : Void {
		guid = value.split("-").join("");
	}
}