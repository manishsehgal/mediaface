﻿import mx.controls.Label;
import mx.controls.TextArea;

class PreviewDescription extends MovieClip {
	private var lblCaption:Label;
	private var lblDescription:TextArea;
	
	function PreviewDescription () {
		_parent._parent.setStyle("styleName", "PreviewPanel");
		lblCaption.setStyle("styleName", "PreviewCaptionText");
		lblDescription.setStyle("styleName", "PreviewDescriptionText");
	}
	
	function onLoad() {
		_global.LocalHelper.LocalizeInstance(lblCaption, "PreviewDescription", "IDS_LBLCAPTION");
		_global.LocalHelper.LocalizeInstance(lblDescription, "PreviewDescription", "IDS_LBLDESCRIPTION");
				
		var styleObject:Object = StylesHelper.GetStyle(".Preview.DescriptionText");
		var htmlStyle:Object = new Object();
		for(var i in styleObject)
			htmlStyle[i] = styleObject[i];
		/*
		Unfortunately, styles.setStyle("html", styleObject); - doesn't work.
		So there is qa workaround. It needs create new object htmlStyle with the same 
		properties as from styleObject.
		*/
		
		var styles = new TextField.StyleSheet();
		styles.setStyle("html", htmlStyle);
		styles.setStyle("smallbr",
			{fontSize: '4'}
		);
		
		lblDescription.styleSheet=styles;
	}
	
	public function Init() {
		_parent.ctlPreview._visible = false;
		_parent.ctlCalibration._visible = true;
	}
}