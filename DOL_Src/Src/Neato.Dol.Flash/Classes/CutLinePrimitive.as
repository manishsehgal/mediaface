﻿class CutLinePrimitive implements IDraw {
	private var x1:Number;
	private var y1:Number;
	private var x2:Number;
	private var y2:Number;
	private var scissors:String = "right";
	
	function CutLinePrimitive(node:XMLNode) {
		var attribs:Object = node.attributes; 
		x1 = Number(attribs.x1);
		y1 = Number(attribs.y1);
        x2 = Number(attribs.x2);
		y2 = Number(attribs.y2);
		var tmp:String = attribs.scissors.toLowerCase();
		switch(tmp) {
			case "left":
			case "top":
			case "bottom":
				scissors = tmp;
				break;
			default:
				scissors = "right";
		}
	}
	
	function Draw(mc:MovieClip, originalX:Number, originalY:Number,rotationAngle:Number,scaleX:Number,scaleY:Number):Void {
		if(scaleX==undefined)scaleX=1.0;
		if(scaleY==undefined)scaleY=1.0;
  		var begin:Object = new Object({x:originalX+x1, y:originalY+y1});
		var end:Object = new Object({x:originalX+x2, y:originalY+y2});
  		if (rotationAngle > 0 || rotationAngle < 0) {
  			begin = _global.RotateAboutCenter(begin.x, begin.y, originalX, originalY, rotationAngle);
			end = _global.RotateAboutCenter(end.x, end.y, originalX, originalY, rotationAngle);
  		}
		
		mc.moveTo(begin.x*scaleX, begin.y*scaleY);
		mc.lineTo(end.x*scaleX, end.y*scaleY);
		var mcScissors:MovieClip = mc.attachMovie("Scissors", "mcScissors" + mc.getNextHighestDepth(), mc.getNextHighestDepth());
		var scissorsWidth:Number = mcScissors._width / 2;
		mcScissors._xscale = scaleX*100;
		mcScissors._yscale = scaleY*100;
		switch(scissors) {
			case "left": 
			if (begin.x < end.x) {
				mcScissors._x = begin.x;
				mcScissors._y = begin.y;
			} else {
				mcScissors._x = end.x;
				mcScissors._y = end.y;
			}
			mcScissors._x *= scaleX;
			mcScissors._y *= scaleY;
			mcScissors._rotation = 180;
			mcScissors._x -= scissorsWidth;
			break;
			case "right":
			if (begin.x > end.x) {
				mcScissors._x = begin.x;
				mcScissors._y = begin.y;
			} else {
				mcScissors._x = end.x;
				mcScissors._y = end.y;
			}
			mcScissors._x *= scaleX;
			mcScissors._y *= scaleY;
			mcScissors._rotation = 0;
			mcScissors._x += scissorsWidth;
			break;
			case "top":
			if (begin.y < end.y) {
				mcScissors._x = begin.x;
				mcScissors._y = begin.y;
			} else {
				mcScissors._x = end.x;
				mcScissors._y = end.y;
			}
			mcScissors._x *= scaleX;
			mcScissors._y *= scaleY;
			mcScissors._rotation = 270;
			mcScissors._y -= scissorsWidth;
			break;
			case "bottom":
			if (begin.y > end.y) {
				mcScissors._x = begin.x;
				mcScissors._y = begin.y;
			} else {
				mcScissors._x = end.x;
				mcScissors._y = end.y;
			}
			mcScissors._x *= scaleX;
			mcScissors._y *= scaleY;
			mcScissors._rotation = 90;
			mcScissors._y += scissorsWidth;
			break;
		}
	}	
}