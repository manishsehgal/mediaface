﻿class SATracking {
	private static var url:String = "DesignerTracking.aspx?";
	private static var ColorFillText:String = "ColorFill";
	private static var AddImageText:String = "AddImage";
	private static var AddCustomImageText:String = "AddCustomImage";
	private static var AddTextText:String = "AddText";
	private static var AddArtisticTextText:String = "AddArtisticText";
	private static var PaintText:String = "Paint";
	private static var AddShapeText:String = "AddShape";
	private static var AddGradientText:String = "AddGradient";
	private static var ClearText:String = "Clear";
	private static var UsePreviousDesignText:String = "UsePreviousDesign";
	private static var SaveDesignText:String = "SaveDesign";
	private static var PluginsText:String = "Plugins";
	private static var QuickToolsText:String = "QuickTools";
	private static var PreviewText:String = "Preview";
	private static var PrintPDFText:String = "PrintPDF";
	private static var PrintLocalText:String = "PrintLocal";
	private static var PrintLightscribeText:String = "PrintLightscribe";
	private static var CopyDesignText:String = "CopyDesign";
	private static var TransferDesignText:String = "TransferDesign";
	private static var AddPlaylistText:String = "AddPlaylist";
	private static var AddPlaylistAudioCdTextText:String = "AddPlaylistAudioCdText";
	private static var AddPlaylistAudioScanTracksText:String = "AddPlaylistAudioScanTracks";
	private static var AddPlaylistScanFoldersText:String = "AddPlaylistScanFolders";
	private static var AddPlaylistFileText:String = "AddPlaylistFile";
	private static var AddPlaylistPlayerWMPText:String = "AddPlaylistPlayerWMP";
	private static var AddPlaylistPlayeriTunesText:String = "AddPlaylistPlayeriTunes";
	private static var ApplyTemplateText:String = "ApplyTemplate";
	private static var ApplyLayoutText:String = "ApplyLayout";
	private static var ZoomText:String = "Zoom";
	
	public static function TrackAction(action:String,Id:String) {
		//_global.tr("--- TrackAction = " + action);
		var xml:XML = new XML();
		var currentDate:Date = new Date();
		var lv:LoadVars = new LoadVars();
		lv.time = currentDate.getTime();
		if(Id!=undefined) {
			lv.Id = Id;
		}
		lv.type = action;
		xml.load(url+lv.toString());
		xml.onLoad = function(success) {
			trace("action "+action+" tracked ok");
		};
	}
	public static function ColorFill(){
		TrackAction(ColorFillText);
	}
	public static function AddImage(){
		TrackAction(AddImageText);
	}
	public static function AddCustomImage(){
		TrackAction(AddCustomImageText);
	}
	public static function AddText(){
		TrackAction(AddTextText);
	}
	public static function AddArtisticText(){
		TrackAction(AddArtisticTextText);
	}
	public static function Paint(){
		TrackAction(PaintText);
	}
	public static function AddShape(shapeType:String){
		TrackAction(AddShapeText,shapeType);
	}
	public static function AddGradient(){
		TrackAction(AddGradientText);
	}
	public static function Clear(){
		TrackAction(ClearText);
	}
	public static function UsePreviousDesign(){
		TrackAction(UsePreviousDesignText);
	}
	public static function SaveDesign(){
		TrackAction(SaveDesignText);
	}
	public static function Plugins(){
		TrackAction(PluginsText);
	}
	public static function QuickTools(toolId:String){
		TrackAction(QuickToolsText,toolId);
	}
	public static function Preview(){
		TrackAction(PreviewText);
	}
	public static function PrintPDF(){
		TrackAction(PrintPDFText);
	}
	public static function PrintLocal(){
		TrackAction(PrintLocalText);
	}
	public static function PrintLightscribe(){
		TrackAction(PrintLightscribeText);
	}
	public static function CopyDesign(){
		TrackAction(CopyDesignText);
	}
	public static function TransferDesign(){
		TrackAction(TransferDesignText);
	}
	public static function AddPlaylist(){
		TrackAction(AddPlaylistText);
	}
	public static function AddPlaylistAudioCd(){
		TrackAction(AddPlaylistAudioCdTextText);
	}
	public static function AddPlaylistAudioScanTracks(){
		TrackAction(AddPlaylistAudioScanTracksText);
	}
	public static function AddPlaylistScanFolders(){
		TrackAction(AddPlaylistScanFoldersText);
	}
	public static function AddPlaylistFromFile(){
		TrackAction(AddPlaylistFileText);
	}
	public static function AddPlaylistPlayerWMP(){
		TrackAction(AddPlaylistPlayerWMPText);
	}
	public static function AddPlaylistPlayeriTunes(){
		TrackAction(AddPlaylistPlayeriTunesText);
	}
	public static function ApplyTemplate(){
		TrackAction(ApplyTemplateText);
	}
	public static function ApplyLayout(){
		TrackAction(ApplyLayoutText);
	}
	public static function Zoom(){
		TrackAction(ZoomText);
	}
}