﻿import mx.controls.Label;
import mx.utils.Delegate;
import mx.core.UIObject;

[Event("press")]
[Event("ActivePress")]
class Step extends UIObject {
	private var mcActive:MovieClip;
	private var mcPassive:MovieClip;
	private var lblCaption:Label;
	
	function Step() {
		var parent = this;
		
		mcPassive.onPress = function() {
			var eventObject = {type:"press", target:parent};
			parent.dispatchEvent(eventObject);
		};
	}
	
	function SetText(txt:String):Void {
		lblCaption.text = txt;
	}
	
	function SetActive(active:Boolean):Void {
		mcActive._visible = active;
		mcPassive._visible = !active;
		lblCaption.setStyle("fontWeight", active ? "bold" : "none");
		lblCaption.setStyle("styleName", active ? "HeaderControlTextBold" : "HeaderControlTextNormal");
	}
	
	public function RegisterOnPressHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("press", Delegate.create(scopeObject, callBackFunction));
    }
	
	public function RegisterOnActivePressHandler(scopeObject:Object, callBackFunction:Function) {
		trace("RegisterOnActivePressHandler");
		var parent = this;
		mcActive.onPress = function() {
			var eventObject = {type:"ActivePress", target:parent};
			parent.dispatchEvent(eventObject);
		};
		this.addEventListener("ActivePress", Delegate.create(scopeObject, callBackFunction));
    }
	
	
}