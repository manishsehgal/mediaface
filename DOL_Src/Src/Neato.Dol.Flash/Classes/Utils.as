/**
 * @author Yuri Teterin
 * 
 * container for various statis utility functions
 */
class Utils {
	
	//Get first child node of 'parNode' with given name 'childName'
	public static function XMLGetChild(parNode:XMLNode, childName:String) : XMLNode {
		for(var node:XMLNode=parNode.firstChild; node!=null; node=node.nextSibling){
			if (node.nodeName == childName) {
				return node;
			}
		}
		return undefined;
	}

	public static function XMLGetChildValue(parNode:XMLNode, childName:String) : String {
		var node:XMLNode = XMLGetChild(parNode, childName);
		return node == undefined ? undefined : node.firstChild.nodeValue;
	}

}