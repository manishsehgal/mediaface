﻿class RectanglePrimitive implements IDraw {
	private var x:Number;
	private var y:Number;
	private var w:Number;
	private var h:Number;
	private var angle:Number;
	
	function RectanglePrimitive(node:XMLNode) {
		x = Number(node.attributes.x);
		y = Number(node.attributes.y);
		w = Number(node.attributes.w);
		h = Number(node.attributes.h);
	}
	
	function Draw(mc:MovieClip, originalX:Number, originalY:Number, rotationAngle:Number, scaleX:Number,scaleY:Number):Void {
		if(scaleX==undefined)scaleX=1.0;
		if(scaleY==undefined)scaleY=1.0;
		
		var centerX:Number = originalX;
		var centerY:Number = originalY;
		if (angle != undefined) {
			centerX = x;
			centerY = y;
		}		
		
		var a = new Object({x:originalX+x,   y:originalY+y});
  		var b = new Object({x:originalX+x,   y:originalY+y+h});
  		var c = new Object({x:originalX+x+w, y:originalY+y+h});
  		var d = new Object({x:originalX+x+w, y:originalY+y});
		
  		//rotate if needed
		var usedAngle:Number = (angle == undefined) ? rotationAngle : angle;
  		if (usedAngle != 0) {
  			a = _global.RotateAboutCenter(a.x, a.y, centerX, centerY, usedAngle);
  			b = _global.RotateAboutCenter(b.x, b.y, centerX, centerY, usedAngle);
  			c = _global.RotateAboutCenter(c.x, c.y, centerX, centerY, usedAngle);
  			d = _global.RotateAboutCenter(d.x, d.y, centerX, centerY, usedAngle);
  		}
		
		drawRectangle(mc, a, b, c, d, scaleX, scaleY);
	}
	
	static private function drawRectangle(mc:MovieClip, a, b, c, d, scaleX:Number, scaleY:Number):Void {
  		mc.moveTo(a.x*scaleX, a.y*scaleY);
  	    mc.lineTo(b.x*scaleX, b.y*scaleY);
      	mc.lineTo(c.x*scaleX, c.y*scaleY);
  	    mc.lineTo(d.x*scaleX, d.y*scaleY);
  	    mc.lineTo(a.x*scaleX, a.y*scaleY);
	}
	
	function set X(xValue:Number){x = xValue;}
	function set Y(yValue:Number){y = yValue;}
	function set W(wValue:Number){w = wValue;}
	function set H(hValue:Number){h = hValue;}
	function set Angle(angleValue:Number){angle = angleValue;}
	function get X():Number{return x;}
	function get Y():Number{return y;}
	function get W():Number{return w;}
	function get H():Number{return h;}
	function get Angle():Number{return angle;}
}
