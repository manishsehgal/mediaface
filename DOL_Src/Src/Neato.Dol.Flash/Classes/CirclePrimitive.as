﻿class CirclePrimitive implements IDraw {
	private var x:Number;
	private var y:Number;
	private var r:Number;
	
	function CirclePrimitive(node:XMLNode) {
		x = Number(node.attributes.x);
		y = Number(node.attributes.y);
		r = Number(node.attributes.r);
	}
	
	function Draw(mc:MovieClip, originalX:Number, originalY:Number, rotationAngle:Number, scaleX:Number, scaleY:Number):Void {
		if(scaleX==undefined)scaleX=1.0;
		if(scaleY==undefined)scaleY=1.0;
		var c:Object = new Object({x:originalX+x,  y:originalY+y});
		if (rotationAngle > 0 || rotationAngle < 0) {
			c = _global.RotateAboutCenter(c.x, c.y, originalX, originalY, rotationAngle);
		}
		drawCircle(mc, c.x*scaleX, c.y*scaleY, r*(scaleX<=scaleY?scaleX:scaleY));
	}
	
	static function drawCircle(mc:MovieClip, x:Number, y:Number, r:Number):Void {
		var exprTan:Number = Math.tan(Math.PI/8)*r;
		var exprSin:Number = Math.sin(Math.PI/4)*r;
		mc.moveTo(x+r, y);
		mc.curveTo(       r+x,  exprTan+y,  exprSin+x,  exprSin+y);
		mc.curveTo( exprTan+x,        r+y,          x,        r+y);
		mc.curveTo(-exprTan+x,        r+y, -exprSin+x,	exprSin+y);
		mc.curveTo(      -r+x,  exprTan+y,       -r+x,          y);
		mc.curveTo(      -r+x, -exprTan+y, -exprSin+x, -exprSin+y);
		mc.curveTo(-exprTan+x,       -r+y,          x,       -r+y);
		mc.curveTo( exprTan+x,       -r+y,  exprSin+x, -exprSin+y);
		mc.curveTo(       r+x, -exprTan+y,        r+x,          y);
	}
}
