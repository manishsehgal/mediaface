﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;

[Event("onInvoked")]
[Event("onInvokedParsed")]
[Event("onCheckService")]
class SAPlugins {
	public  var addEventListener:Function;
	public  var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SAPlugins.prototype);
	private static var chkStatusDispatcher:EventDispatcher = new EventDispatcher();

	private var pendingCommands: Array;
	
	public var socket : XMLSocket;

	private var onInvokedRegCount:Number;
	private var onInvokedParsedRegCount:Number;
	 
	private function SAPlugins() {
		chkStatusDispatcher.addEventListener("onCheckService", Delegate.create(this,OnServiceChecked));
//		SAPlugins.RegisterOnServiceCheckedHandler(this,OnServiceChecked);

		onInvokedRegCount = 0;
		onInvokedParsedRegCount = 0;

		pendingCommands = new Array();

		this.RegisterOnInvokedParsedHandler(this, onInvoke);
		socket = new XMLSocket();
	}

	public static function GetInstance():SAPlugins {
		return new SAPlugins();
	}
	
	//======= Discovering ===============================

	private static var isSecurityOK: Boolean = false;	
	private static var isAvailable  : Boolean = false;
	private static var isInstalled  : Boolean = false;
	private static var psPort         : Number  = 0;
	private static var isDataFollowSupported: Boolean = false;

	private static var psHost : String = "localhost";
	private static var base_port   : Number = 1212;
	private static var port_step   : Number = 1234;
	private static var max_used_port: Number = 0;
	private static var test_port   : Number;
	private static var test_count  : Number;
	private static var max_test_count: Number = 3;  // =10 in Service, but this is slightly annoying
	private static var timeoutCount:Number = 0;
	private static var max_timeoutCount:Number = 10;
	private static var isDiscoveryRunning:Boolean = false;
	private static var socketDisco : XMLSocket = null;
	private static var intervalId  : Number = null;
	private static var intervalIdChk : Number = null;
	private static var restartDisco: Boolean = false;

	private static var showIsNotAvailableError : Boolean = false;
	
	public static function IsAvailable():Boolean {
		return SAPlugins.isAvailable;
	}

	public static function IsInstalled():Boolean {
		return SAPlugins.isInstalled;
	}

	public static function IsDiscoveryRunning():Boolean {
		return SAPlugins.isDiscoveryRunning;
	}
	
//	public static function RegisterOnDiscoveredHandler(scopeObject:Object, callBackFunction:Function) {
//		this.addEventListener("onDiscovered", Delegate.create(scopeObject, callBackFunction));
//	}

//	public static function RegisterOnServiceCheckedHandler(scopeObject:Object, callBackFunction:Function) {
//		chkStatusDispatcher.addEventListener("onCheckService", Delegate.create(scopeObject, callBackFunction));
//	}
//	public static function UnRegisterOnServiceCheckedHandler(scopeObject:Object, callBackFunction:Function) {
//		chkStatusDispatcher.removeEventListener("onCheckService", Delegate.create(scopeObject, callBackFunction));
//	}

	//====== Plugin Service discovery =======================================

	private static function closeSocketDisco() {
		if(socketDisco != null) {
			socketDisco.close();
			socketDisco = null;
		}
	}

	public static function CheckService() {
		//_global.tr("CheckService: isAvailable="+isAvailable);
		if (intervalIdChk != null) {
			clearInterval(intervalIdChk);
			intervalIdChk = null;
		}
		// intervalIdChk = setInterval(SAPlugins.CheckService, 5000);

		if(isAvailable) {
			TryConnect();
		} else {
			StartDiscovery();
		}			
	}

	private static function TryPSPort() {
		if (intervalId != null) {
			clearInterval(intervalId);
			intervalId = null;
		}

		//_global.tr("TryPSPort: port="+test_port+" securityOK="+isSecurityOK);
		if(isSecurityOK) {
			TryConnect();
		} else {
			intervalId = setInterval(OnChkPSPortResultTimeout, 5000, test_port );
				// set timeout 5 sec for external port check
			BrowserHelper.InvokePageScript("ChkPSPort",test_port.toString());
		}
	}

	private static function OnChkPSPortResultTimeout(port:Number) {
		clearInterval(intervalId);
		intervalId = null;

		timeoutCount++;
		_global.tr("OnChkPSPortResultTimeout() port="+port+" timeoutCount="+timeoutCount);
		if(timeoutCount > max_timeoutCount) {
			// large timeoutCount means that external port check seems does not work -
			//   so try to connect directly to default port (but if this attempt fail
			//    then all other attempts will fail too)
			test_port = base_port;
			TryConnect();
		} else {
			OnChkPSPortResult(port,false);
		}
	}

	public static function OnChkPSPortResult(port:Number,result:Boolean) {
		if (intervalId != null) {	// clear timeout watchdog
			clearInterval(intervalId);
			intervalId = null;
		}

		if (result) {
			test_port = port;
			TryConnect();
		} else {
			NextPort();
		}
	}

	public static function StartDiscovery()
	{
	//_global.tr("PS: StartDiscovery");
		if(test_count > 0) {
			restartDisco = true;
			return;
		}
		restartDisco = false; 
		isDiscoveryRunning = true;
		closeSocketDisco();
		test_port  = base_port;
		test_count = 0;
		intervalId = setInterval(SAPlugins.TryPSPort, 5);
	}

	private static function NextPort() {
		closeSocketDisco();
		test_port += port_step;
		test_count++;
		if (test_count == max_test_count) {
			test_port = base_port;
			test_count = 0;
			max_test_count = 1;
			if(!restartDisco) {
				ServiceChecked();
				return;
			}
		} 
		//_global.tr("PS: NextPort: " + test_port);
		intervalId = setInterval(SAPlugins.TryPSPort, 5);
	}
	
	private static function TryConnect() {
		//_global.tr("PS: TryConnect port " + test_port.toString());

		if (intervalId != null) {
			clearInterval(intervalId);
			intervalId = null;
		}

		closeSocketDisco();		
		socketDisco = new XMLSocket();
		
		socketDisco.onConnect = Delegate.create(SAPlugins, OnConnectSocketDisco);
		if(!isSecurityOK) {
			System.security.loadPolicyFile("xmlsocket://"+psHost+":" + test_port.toString());
		}
		var res:Boolean = socketDisco.connect(psHost, test_port);
		if (!res) {
			OnConnectSocketDisco(false);
		}
	}
		
	private static function OnConnectSocketDisco(status) {
		//_global.tr("PS: OnConnectSocketDisco: " + status);
		if (status == true) {
			isSecurityOK = true;
			socketDisco.onXML = Delegate.create(SAPlugins, OnXmlSocketDisco);
			socketDisco.send("<Discovery />");
		}
		else {
			var wasAvailable:Boolean = SAPlugins.isAvailable;
			SAPlugins.isAvailable = false;
		if(wasAvailable) {
				StartDiscovery();
			} else {
				NextPort();
			}
		}
	}
	
	private static function OnXmlSocketDisco(recvXML:XML) {
		//_global.tr("PS: OnXmlSocketDisco: " + recvXML.toString());
		closeSocketDisco();

		try {		
			if(!recvXML.firstChild.nodeName == "Discovered") {
				throw new Error("not a Plugin Service");
			}
			SAPlugins.isAvailable = true;
			SAPlugins.isInstalled = true;
			SAPlugins.psPort = test_port;
			SAPlugins.isDataFollowSupported = (recvXML.firstChild.attributes.DataFollow != undefined);
		} catch ( err ) {}

		if(SAPlugins.isAvailable) {
			ServiceChecked();
		} else {
			NextPort();
		}
	}

	private static function ShowServiceNotAvailableMsg() {
		if(showIsNotAvailableError) {
			if(SAPlugins.isInstalled) {
				_global.MessageBox.Alert(" "+IsNotRespondingMsg(), " Warning", null);
			} else {
	 			_global.MessageBox.Alert(" You should install MediaFACE Online Plugins to use this feature.\n Please go to the \"Plugins\" menu to download and install required plugins.", " Warning                                                       ", null);
			}
			showIsNotAvailableError = false;
		}
	}

	private static function ServiceChecked() {
		//_global.tr("ServiceChecked() isAvailable="+isAvailable+" showErr="+showIsNotAvailableError);
		if(SAPlugins.isDiscoveryRunning) {
			isDiscoveryRunning = false;
		}

		max_test_count = 1;

		if(!SAPlugins.isAvailable) {
			ShowServiceNotAvailableMsg();
		}

		var eventObject = {type:"onCheckService", isAvaliable:SAPlugins.isAvailable};
		chkStatusDispatcher.dispatchEvent(eventObject);
	}

	//====== Plugin commands =======================================

	public static function Command(cmdName:String,cmdParams:XMLNode):XML {
		return CommandArr(cmdName,Array(cmdParams));
	}

	public static function CommandArr(cmdName:String,cmdParamsArr:Array):XML {
		var cmd:XML = new XML();
		var CmdInfo:XMLNode = cmd.createElement("CmdInfo");
		CmdInfo.attributes.Type = "Request";
		cmd.appendChild(CmdInfo);
		
		var Request:XMLNode = cmd.createElement("Request");
		Request.attributes.Name = cmdName;
		CmdInfo.appendChild(Request);
		
		var Params:XMLNode = cmd.createElement("Params");
		Request.appendChild(Params);
		
		for(var i:Number=0 ; i < cmdParamsArr.length ; i++) {
			Params.appendChild(cmdParamsArr[i]);
		}
		
		return cmd;
	}

	public static function ParseResponse(eventObject:Object, showErrors:Boolean):Object {
		// returns preparsed response of InvokeCommand in properties of returned object
		//  and optionally shows error message if found in response
		// Returned properties:
		//  command:String
		//  errorCode:Number
		//  errorDesc:String
		//  response:XMLNode
		//  params:XMLNode
		var reply:XML = new XML();
		reply.parseXML(eventObject.result);
		var CmdInfo:XMLNode  = Utils.XMLGetChild(reply,    "CmdInfo");
		var Response:XMLNode = Utils.XMLGetChild(CmdInfo,  "Response");
		var Params:XMLNode   = Utils.XMLGetChild(Response, "Params");

		var name:String = CmdInfo.attributes.Name;
		if(name == undefined) {
			name = Response.attributes.Name;
		}
		if(name == undefined) {
			var Request:XMLNode = Utils.XMLGetChild(CmdInfo,  "Request");
			name = Request.attributes.Name;
		}

		var errorCode:Number = parseInt(Response.attributes.Error);
		if(errorCode == NaN) {
			errorCode = 0;
		} 
		var errorDesc:String = Response.attributes.ErrorDesc; // general error

		if(Response == undefined || Params == undefined) {
			if(errorCode == 0) {
				errorCode = 1000;
			}
			if(errorDesc.length == 0) {
				errorDesc = "General error (invalid response from Plugin Service).";
			}
		}

		if(showErrors != false && errorCode != 0 && errorDesc.length > 0) {
 			_global.MessageBox.Alert(" "+errorDesc," Warning",null);
		}

		return {command:name, errorCode:errorCode, errorDesc:errorDesc, response:Response, params:Params};
	}

	private function SendResponse(respXML:XML) {
		//_global.tr("SendResponse() resp="+respXML);
		//_global.tr("SendResponse() dispatching: onInvokedRegCount="+onInvokedRegCount+" onInvokedParsedRegCount="+onInvokedParsedRegCount);
		if(onInvokedRegCount > 0) {
			var eventObject = {type:"onInvoked", target:this, result:respXML};
			this.dispatchEvent(eventObject);
		}
		if(onInvokedParsedRegCount > 0) {
			var eventObjectParsed = ParseResponse({result:respXML}, true);
			eventObjectParsed.type="onInvokedParsed";
			eventObjectParsed.target=this;
			this.dispatchEvent(eventObjectParsed);
		}
	}

	private function SendErrorResp(cmd:XML, errorDesc:String) {
		var xml:XML = new XML();

		var CmdInfo:XMLNode = Utils.XMLGetChild(cmd,     "CmdInfo");
		var Request:XMLNode = Utils.XMLGetChild(CmdInfo, "Request");
		var cmdName:String = CmdInfo.attributes.Name;

		if(cmdName == undefined) {
			cmdName = Request.attributes.Name;
		}

		var resp:XML = new XML();
//		resp.parseXML("<CmdInfo Type=\"Response\" Name=\""+cmdName+"\"><Response Error=\"1\"" + (errorDesc.length>0 ? " ErrorDesc=\""+errorDesc+"\"" : "") + "/></CmdInfo>");
		CmdInfo = resp.createElement("CmdInfo");
		CmdInfo.attributes.Type = "Response";
		CmdInfo.attributes.Name = cmdName;
		resp.appendChild(CmdInfo);
		
		var Response:XMLNode = resp.createElement("Response");
		Response.attributes.Name = cmdName;
		Response.attributes.Error = "1";
		if(errorDesc.length>0) {
			Response.attributes.ErrorDesc = errorDesc;
		}
		CmdInfo.appendChild(Response);

		if(Request.toString().length <50000) {
			CmdInfo.appendChild(Request);
		}
		SendResponse(resp);
	}

	private static function IsNotRespondingMsg():String {
		return "Plugin Service is not responding.\n Please start Plugin Service or reinstall it from the \"Plugins\" menu.";
	}

	private function OnServiceChecked() {
		//_global.tr("OnServiceChecked() isAvailable="+isAvailable+" pendCmdLeng="+pendingCommands.length);
		ProcessPendingCommands();
	}

	private function ProcessPendingCommands() {
		var command:Object = pendingCommands.pop();
		if(command != undefined) {
			//_global.tr("ProcessPendingCommands() isAvailable="+isAvailable+" cmd="+command.cmd);
			if(SAPlugins.isAvailable) {
				InvokeCommandOK(command.cmd,command.data);
			} else {
				SendErrorResp(command.cmd);
			}
		}
	}

	public function RegisterOnInvokedHandler(scopeObject:Object, callBackFunction:Function) {
		onInvokedRegCount++;
		this.addEventListener("onInvoked", Delegate.create(scopeObject, callBackFunction));
	}

	public function RegisterOnInvokedParsedHandler(scopeObject:Object, callBackFunction:Function) {
		onInvokedParsedRegCount++;
		this.addEventListener("onInvokedParsed", Delegate.create(scopeObject, callBackFunction));
	}

	public function InvokeCommandEx(cmdName:String, cmdParams:XMLNode, data:Object, showNotAvailableError:Boolean) {
		//_global.tr("InvokeCommandEx() showErr="+showIsNotAvailableError+"|"+showNotAvailableError);
		InvokeCommandExArr(cmdName, Array(cmdParams), data, showNotAvailableError);
	}

	public function InvokeCommandExArr(cmdName:String, cmdParamsArr:Array, data:Object, showNotAvailableError:Boolean) {
		//_global.tr("InvokeCommandEx() showErr="+showIsNotAvailableError+"|"+showNotAvailableError);
		var showError:Boolean = showNotAvailableError;
		if(showError == undefined) {
			showError = cmdName != "CORE_GET_UPDATE_INFO" && cmdName != "CORE_UPDATE_FILES";
		}
		InvokeCommand(CommandArr(cmdName,cmdParamsArr), data, showError);
	}

	public function InvokeCommand(cmd:XML, data:Object, showNotAvailableError:Boolean) {
	// use 'data' parameter to transfer huge unicode strings without parsing it in XML
		//_global.tr("InvokeCommand() showErr="+showIsNotAvailableError+"|"+showNotAvailableError);
		if(showNotAvailableError == undefined || showNotAvailableError) {
			showIsNotAvailableError	= true;
		}

		if (!SAPlugins.isAvailable) {
			this.pendingCommands.push({cmd:cmd, data:data});
			if(!SAPlugins.isDiscoveryRunning) {
				StartDiscovery();
			}
		} else {
			InvokeCommandOK(cmd, data);
		}
		return true;
	}

	private function InvokeCommandOK(cmd:XML, data:Object): Boolean {
		
		//_global.tr("InvokeCommandOK() cmd="+cmd);
		var parent = this;
		socket.onConnect = function(status) {
			if (status) {
				parent.OnSocketConnect(cmd,data);
			} else {
				parent.socket.close();
				SAPlugins.isAvailable = false;
				ShowServiceNotAvailableMsg();
				parent.SendErrorResp(cmd);
			}
		};
		
		if (!socket.connect(psHost, psPort)) {
			socket.onConnect(false);
			return false;
		}
		return true;
	}

	private function OnSocketConnect(cmd:XML, data:Object) {

		showIsNotAvailableError = false; 
		// slightly incorrect if there are several contemporary commands runing

		var parent = this;
		var isDataPresent:Boolean = (data != undefined && data != null);
		var isDataFollow:Boolean = isDataPresent && SAPlugins.isDataFollowSupported;
		//_global.tr("OnSocketConnect() dataL="+data.length+" follow="+SAPlugins.isDataFollowSupported);
		if(isDataPresent) {
			cmd.firstChild.attributes.DataFollow= (SAPlugins.isDataFollowSupported ? "1" : "0");
			if(!isDataFollow) {
				var dataNode:XMLNode = cmd.createElement("Data");
				var dataTextNode:XMLNode = cmd.createTextNode(data.toString());
				dataNode.appendChild(dataTextNode);
				cmd.firstChild.firstChild.appendChild(dataNode);
			}
		}

		socket.onXML = function(recvXML:XML) {
			//_global.tr("received Response: "+recvXML);
			parent.socket.onClose = function() {};
			parent.socket.close();
			parent.SendResponse(recvXML);
		};
		socket.onClose = function() {
			parent.SendErrorResp(cmd,"Connection to Plugins Service closed without response.");
		};

		socket.send(cmd);
		if(isDataFollow) {
			socket.send(data);
		}
	}
	
	//======== Update plugins =======================
	
	public function onInvoke(parsedResponse) {
		//_global.tr("onInvoke() command="+parsedResponse.command);
		switch (parsedResponse.command) {
			case "CORE_GET_UPDATE_INFO":
				OnGetUpdateInfo(parsedResponse);
				break;
			case "CORE_UPDATE_FILES": 
				OnUpdateFiles(parsedResponse);
				break;
			case "CORE_IS_MODULE_AVAILABLE":
				OnModuleAvailable(parsedResponse);
				break;
		}
		ProcessPendingCommands(); 
	}
		
	private static var serverPluginInfo : XML;
	private static var serverPluginInfoLoaded : Boolean = false;
	//private static var isServerPluginInfoLoading : Boolean = false;
	
	public function RegisterOnPluginUpdateInfoReady(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onPluginUpdateInfoReady", Delegate.create(scopeObject, callBackFunction));
	}
	
	public function LoadServerPluginInfo(showNotAvailableError:Boolean) {

		//if(isServerPluginInfoLoading) {
		//	return;
		//}

		//isServerPluginInfoLoading = true;

		if(showNotAvailableError == true) {
			showIsNotAvailableError = true;
		}

		if (serverPluginInfoLoaded == true) {
			onServerPluginInfoLoaded(true);
			return;
		}
		//make URL
		var currentDate:Date = new Date();
		var clientOS:String = System.capabilities.os;
		var url:String = "PluginFile.aspx?file=desc.xml&clientOS="+clientOS+"&time=" + currentDate.getTime();
		if (SALocalWork.IsLocalWork) url = "../Neato.Dol.WebDesigner/Plugins/Windows/desc.xml";
		//load xml with plugin info
		serverPluginInfo = new XML();
		serverPluginInfo.onLoad = Delegate.create(this, onServerPluginInfoLoaded);
		serverPluginInfo.load(url);
	}
	
	public function onServerPluginInfoLoaded(success) {
		if (success == false) {
			_global.MessageBox.Alert("Could not get plugins description from server", " Warning", null);
			return;
		}

		serverPluginInfoLoaded = true;

		var ModulesEl:XMLNode;
		for(var itemNode:XMLNode=serverPluginInfo.firstChild; itemNode!=null; itemNode=itemNode.nextSibling) {
			if (itemNode.nodeName == "Modules") {
				ModulesEl = itemNode.cloneNode(true);
				break;
			}
		}

		InvokeCommandEx("CORE_GET_UPDATE_INFO",ModulesEl);
	}
	
	public function OnGetUpdateInfo(parsedResponse) {   
/*		var reply:XML = new XML();
		reply.parseXML(eventObject.result);
		var CmdInfo:XMLNode  = Utils.XMLGetChild(reply,    "CmdInfo");
		var Response:XMLNode = Utils.XMLGetChild(CmdInfo,  "Response");
		var Params:XMLNode   = Utils.XMLGetChild(Response, "Params");
*/
		var Modules:XMLNode  = Utils.XMLGetChild(parsedResponse.params,   "Modules");
		if ( parsedResponse.errorCode > 0 || Modules == undefined ) {
			SendPluginUpdateInfoReady(false);
			return;
		}
		
		for(var itemNode:XMLNode=serverPluginInfo.firstChild; itemNode!=null; itemNode=itemNode.nextSibling) {
			if (itemNode.nodeName == "Modules") {
				serverPluginInfo.parseXML(Modules.toString());
				break;
			}
		}
		//isServerPluginInfoLoading = false;
		SendPluginUpdateInfoReady(true);
	}
	
	public function SendPluginUpdateInfoReady(ok:Boolean) {
		var eventObject = {type:"onPluginUpdateInfoReady", target:this, status:ok};
		this.dispatchEvent(eventObject);
	}
		
	public function GetPluginUpdateInfo():Array {
		var pluginInfo = new Array();
		for(var itemNode:XMLNode=serverPluginInfo.firstChild; itemNode!=null; itemNode=itemNode.nextSibling) {
			if (itemNode.nodeName == "Modules") {
				for (var node:XMLNode = itemNode.firstChild; node != null; node = node.nextSibling) {
					if (node.nodeName == "ModuleInfo") {
						var mod = new Object();
						mod.pluginName = node.attributes.Name;
						mod.name = node.attributes.Description;
						mod.state = node.attributes.UpdateState;
						mod.file = node.attributes.Filename;
						var depends:String = "";
						var dependsState:String = "";
						var nodeDepends:XMLNode = Utils.XMLGetChild(node, "Depends");
						if (nodeDepends != undefined) {
							for (var nodeFile:XMLNode = nodeDepends.firstChild; nodeFile != null; nodeFile = nodeFile.nextSibling) {
								if (nodeFile.nodeName == "File") {
									if (nodeFile.attributes.UpdateState != "NoChange") {
										if (depends.length > 0) {
											depends += ";";
											dependsState += ";";
										}
										depends += nodeFile.attributes.Filename;
										dependsState += nodeFile.attributes.UpdateState;
									}
								}
							}
						}
						mod.depends = depends;
						mod.dependsState = dependsState;
						pluginInfo.push(mod);
					}
				}
			}
		}
		return pluginInfo;
	}
	
	private var pluginFile : XML;
	
	public function DoUpdate(fileName:String, depends:String) {
		Preloader.Show();
		this.pluginFile = new XML();
		this.pluginFile.onLoad = Delegate.create(this, onPluginFileLoaded);
		var clientOS:String = System.capabilities.os;
		var currentDate:Date = new Date();
		var url:String = "PluginFile.aspx?name=" + fileName + "&depends=" + depends+"&clientOS="+clientOS+"&time="+currentDate.getTime();
		this.pluginFile.load(url);
	}
	
	public function onPluginFileLoaded(success) {
		Preloader.Hide();
		if (success == false) {
			SendFileUpdated(false);
			return;
		}

		InvokeCommandEx("CORE_UPDATE_FILES",this.pluginFile.firstChild.cloneNode(true));
	}
	
	public function OnUpdateFiles(parsedResponse) {
/*		var reply:XML = new XML();
		reply.parseXML(eventObject.result);
		var CmdInfo:XMLNode  = Utils.XMLGetChild(reply,    "CmdInfo");
		var Response:XMLNode = Utils.XMLGetChild(CmdInfo,  "Response");
		var Params:XMLNode   = Utils.XMLGetChild(Response, "Params");
*/
		SendFileUpdated(parsedResponse.errorCode == 0);
	}
	
	public function SendFileUpdated(ok:Boolean) {
		var eventObject = {type:"onFileUpdated", target:this, status:ok};
		this.dispatchEvent(eventObject);
	}
	
	public function RegisterOnFileUpdated(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onFileUpdated", Delegate.create(scopeObject, callBackFunction));
	}
	
	public function IsModuleAvailable(name:String):Void {
		//if (SAPlugins.isAvailable) {
		var Module:XMLNode = new XMLNode(1,"Module");
		Module.attributes.Name = name;
			
		InvokeCommandEx("CORE_IS_MODULE_AVAILABLE",Module);
		//}
	}
	
	private function OnModuleAvailable(parsedResponse) {
		//_global.tr("OnModuleAvailable()");
/*		var reply:XML = new XML();
		reply.parseXML(eventObject.result);
		var CmdInfo:XMLNode  = Utils.XMLGetChild(reply,    "CmdInfo");
		var Response:XMLNode = Utils.XMLGetChild(CmdInfo,  "Response");
		var Params:XMLNode   = Utils.XMLGetChild(Response, "Params");
*/
		var ok:Boolean = false;
		var name:String = "";
		var ver:String = "";

		if(parsedResponse.errorCode == 0) {
			var Status:XMLNode = parsedResponse.params.firstChild;
			ok   = Status.attributes.status == "Ok";
			name = Status.attributes.name;
			ver  = Status.attributes.version;
		}
		ModuleAvaliable(ok, name, ver);
	}
	
	public function ModuleAvaliable(ok:Boolean, name:String, ver:String) {
		//_global.tr("ModuleAvaliable() ok="+ok+" name="+name);

		var eventObject = {type:"onModuleAvaliable", target:this, isAvaliable:ok, moduleName:name, moduleVer:ver};
		this.dispatchEvent(eventObject);
		
		if(SAPlugins.isAvailable) {
			PluginNotAvaliable(ok, name);
		} // for !isAvailable we had shown warning earlier

	}
	
	public function RegisterOnModuleAvaliableHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onModuleAvaliable", Delegate.create(scopeObject, callBackFunction));
	}

	public function PluginNotAvaliable(ok:Boolean, name:String) {
 		if(!ok) {
			var pluginName:String = name;
			var pluginInfo:Array = GetPluginUpdateInfo();
			
			for (var i = 0; i < pluginInfo.length; ++i) {
				var item = pluginInfo[i];
				if (item.pluginName == pluginName)
					pluginName = item.name;
			}
 			_global.MessageBox.Alert(" You should install (reinstall) the \"" + pluginName + "\" to use this feature.\nPlease go to the \"Plugins\" menu to download and install the required plugin.",
	 								 " Warning                                                       ", null);
 		}
 	}

	private function SavePSInfo() {
		BrowserHelper.InvokePageScript("SetCookie","PSInfo","");
	}

}
