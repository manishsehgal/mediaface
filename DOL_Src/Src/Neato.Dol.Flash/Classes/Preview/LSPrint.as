import mx.core.UIObject;

class Preview.LSPrint extends UIObject {
	private var btnLightscribe:CtrlLib.ButtonEx;
	private var ctlLSPanel:Preview.LSPanel;
	
	function LSPrint() {
		super();
	}

	public function onLoad() {
		_global.GlobalNotificator.OnComponentLoaded("PreviewArea.LSPrint", this);
		
		TooltipHelper.SetTooltip2(btnLightscribe, "IDS_TOOLTIP_LIGHTSCRIBE");
	}

	public function RegisterLightscribeHandler(scopeObject:Object, callBackFunction:Function) {
		btnLightscribe.RegisterOnClickHandler(scopeObject, callBackFunction);
    }
	
	public function DataBind(data:Object):Void {
//		btnLightscribe._visible = data.isLightscribeDevice;
//		this.ctlLSPanel = _global.UIController.PreviewAreaController.ctlLSPanel;
//		ctlLSPanel._visible = data.isLightscribeDevice;
	}

	public function set LightscribeEnabled(value:Boolean):Void {
		btnLightscribe.enabled = value;
	}
}