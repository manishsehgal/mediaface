import mx.core.UIObject;

class Preview.PreviewTool extends UIObject {
	private var accPreview:CtrlLib.AccordionEx;
	private var accLSPreview:CtrlLib.AccordionEx;
	private var hideCalibration:Boolean = false;

	public function get Acc() : CtrlLib.AccordionEx {
		if (accPreview.visible)
			return accPreview;
		else
			return accLSPreview;
	}
	
	public function PreviewTool() {
		
	}
	
	public static var CalibrateTab:String = "Calibrate";
	public static var PrintTab:String = "Print";
	public static var HelpTab:String = "HelpText";
	
	private function GetTab(tab:String) : Number {
		var length:Number = Acc.numChildren;
		for (var i:Number = 0; i < length; ++ i) {
			if (Acc.getChildAt(i)._name == tab)
				return i; 
		}
		return -1;
	}
	
	public function onLoad() : Void {
		_global.GlobalNotificator.OnComponentLoaded("PreviewArea.PreviewTool", this);
		accPreview.setStyle("backgroundColor", "0xf7f7f7");
		accPreview.setStyle("fontFamily", "Arial");
		accPreview.setStyle("fontSize", 10);
		accLSPreview.setStyle("backgroundColor", "0xf7f7f7");
		accLSPreview.setStyle("fontFamily", "Arial");
		accLSPreview.setStyle("fontSize", 10);

		accLSPreview.visible = false;
		accLSPreview.selectedIndex = 1;
		
		if (_global.Project.CurrentPaper.Calibration.IsCalibrationSpecified)
			accPreview.selectedIndex = 2;
		else
			accPreview.selectedIndex = 1;
	}
	
	public function DataBind(data:Object) : Void {
		if (data.isLightScribeDevice) {
			accPreview.move(-10000, -10000);
			accLSPreview.move(0, 0);
		}
		else {
			accPreview.move(0, 0);
			accLSPreview.move(-10000, -10000);
		}
		accPreview.visible   = !data.isLightScribeDevice;
		accLSPreview.visible = data.isLightScribeDevice;
		if (data.tab != undefined && data.tab != null)
			Show(data.tab);
	}
	
	public function Show(tab:String):Void {
		var index:Number = GetTab(tab);
		if (index != -1)
			Acc.selectedIndex = index;
	}
}