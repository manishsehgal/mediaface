﻿class Preview.Print extends mx.core.UIObject {
	private var btnPrint:CtrlLib.ButtonEx;
	private var btnPrintToPdf:CtrlLib.ButtonEx;
	private var txtPopupBlockers:CtrlLib.TextAreaEx;
	
	public function Print() {
		
	}
	
	public function onLoad() {
		_global.GlobalNotificator.OnComponentLoaded("PreviewArea.Print", this);
		
		TooltipHelper.SetTooltip2(btnPrint, "IDS_TOOLTIP_PRINT");
		TooltipHelper.SetTooltip2(btnPrintToPdf, "IDS_TOOLTIP_PRINTTOPDF");

		txtPopupBlockers.setStyle("borderStyle", "none");
		txtPopupBlockers.setStyle("backgroundColor", "0xf7f7f7");
		txtPopupBlockers.setStyle("fontSize", 8);
		txtPopupBlockers.html = false;

		txtPopupBlockers.text = "<html><body><p><font color=\"#ff0000\">POPUP BLOCKERS must be turned off prior to printing.</font></p></body></html>";

		var styles = new TextField.StyleSheet();
		styles.setStyle("body",
			{fontSize: '11'}
		);
		txtPopupBlockers.styleSheet=styles;
		txtPopupBlockers.html = true;
	}
	
	public function RegisterPrintHandler(scopeObject:Object, callBackFunction:Function) {
		btnPrint.RegisterOnClickHandler(scopeObject, callBackFunction);
    }
	
	public function RegisterPrintToPdfHandler(scopeObject:Object, callBackFunction:Function) {
		btnPrintToPdf.RegisterOnClickHandler(scopeObject, callBackFunction);
    }
	
	public function DataBind(data:Object):Void {
//		btnPrint.enabled = !data.isLightscribeDevice;
//		btnPrintToPdf.enabled = !data.isLightscribeDevice;
	}
}