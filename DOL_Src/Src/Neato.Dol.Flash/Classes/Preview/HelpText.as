﻿class Preview.HelpText extends mx.core.UIObject {
	private var txtHelp:CtrlLib.TextAreaEx;
	
	function HelpText() {
	}

	public function onLoad() {
		txtHelp.setStyle("borderStyle", "none");
		txtHelp.setStyle("backgroundColor", "0xf7f7f7");
		txtHelp.setStyle("fontSize", 8);
		txtHelp.html = false;
		txtHelp.text = "<html><body>"
+"<b>Direct Printing</b> - When you are printing a layout that is in landscape mode (left to right) as opposed to portrait (up and down), you will need to set this in your printer properties. You can also change the printer output here; such as, paper type and print quality."
+"<br><b>NOTE:</b> If you are printing to our DVD insert or Slim DVD Insert, you will need to change the paper size to A4, instead of US Letter."
+"<br><br><b>Print to PDF</b> - Adobe&#174; Reader&#174; is required - we recommend using the newest version. To get the FREE download or upgrade to the latest version, click this link:"
+"<a href='http://www.adobe.com/products/acrobat/readstep2.html' target='_blank'><img src='images/AcrobatReader.png'></a>"
+"<br><br><br><br><br>Important: Learn how to get accurate printing results with <a href='PrintSetup.aspx?reader=w7' target='_blank'><u>Adobe Reader 7</u></a> or <a href='PrintSetup.aspx?reader=w6' target='_blank'><u>Adobe Reader 6</u></a>"
+"</body></html>";

		var styles = new TextField.StyleSheet();
		styles.setStyle("body",
			{fontSize: '11'}
		);
		txtHelp.styleSheet=styles;
		txtHelp.html = true;
	}
}