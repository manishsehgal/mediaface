﻿import mx.utils.Delegate;
import CtrlLib.ComboBoxEx;
import CtrlLib.ButtonEx;
import CtrlLib.TextInputEx;
import CtrlLib.LabelEx;
import CtrlLib.TextAreaEx;
[Event("onCalibrate")]
[Event("onCalibrateDTCD")]
[Event("onNeedRefresh")]
class Preview.Calibrate extends mx.core.UIObject {
	private var ctlCalibrateNonDTCD;
	private var ctlCalibrateDTCD;
	
	private var minX:Number;
	private var minY:Number;
	private var maxX:Number;
	private var maxY:Number;
	
	private var xPos:Number;
	private var yPos:Number;
	private var diamInner:Number;
	private var diamOuter:Number;
	

	private var step:Number = 5;
	private var pxToMmCoeff:Number = 0.35277777777777;
	private var shiftToUp:Number = 180;
	
	private var faceW:Number;
	private var faceH:Number;
	private var paperW:Number;
	private var paperH:Number;
	private var paperX:Number;
	private var paperY:Number;
	
	public function Calibrate() {}
	
	public function onLoad() {
		this.setStyle("styleName", "ToolPropertiesPanel");
		this.ctlCalibrateDTCD.cbPrinter.setStyle("styleName", "ToolPropertiesCombo");
		
		ctlCalibrateNonDTCD.sldCalibrationX.text = "Shift Horizontally";
		ctlCalibrateNonDTCD.sldCalibrationY.text = "Shift Vertically";
		ctlCalibrateDTCD.lblSelectPrinter.text = "Select printer:";
		ctlCalibrateDTCD.lblMoveCenterPoint.text = "Move center point";
		ctlCalibrateDTCD.lblX.text = "X";
		ctlCalibrateDTCD.lblY.text = "Y";
		ctlCalibrateDTCD.lblCalibrate.setStyle("borderStyle", "none");
		ctlCalibrateDTCD.lblCalibrate.setStyle("backgroundColor", "0xf7f7f7");
		
		ctlCalibrateDTCD.sldInnerDiam.text = "Inner Diameter:";
		//ctlCalibrateDTCD.sldInnerDiam.Min = 1;
		//ctlCalibrateDTCD.sldInnerDiam.Max = 27;//60;
		//ctlCalibrateDTCD.sldInnerDiam.Step = 1;
		//ctlCalibrateDTCD.sldInnerDiam.ScaleLength = 150;
		ctlCalibrateDTCD.sldInnerDiam.RegisterOnChangeHandler(this, OnDiamChanged);
		
		ctlCalibrateDTCD.sldOuterDiam.text = "Outer Diameter:";
		//ctlCalibrateDTCD.sldOuterDiam.Min = 28;//61;
		//ctlCalibrateDTCD.sldOuterDiam.Max = 50;//140;
		ctlCalibrateDTCD.sldOuterDiam.Step = 1;
		ctlCalibrateDTCD.sldOuterDiam.ScaleLength = 150;
		ctlCalibrateDTCD.sldOuterDiam.RegisterOnChangeHandler(this, OnDiamChanged);
		
		ctlCalibrateNonDTCD.sldCalibrationX.Min = -10;
		ctlCalibrateNonDTCD.sldCalibrationX.Max = +10;
		ctlCalibrateNonDTCD.sldCalibrationY.Min = -10;
		ctlCalibrateNonDTCD.sldCalibrationY.Max = +10;
		
		ctlCalibrateNonDTCD.sldCalibrationX.Step = 0.5;
		ctlCalibrateNonDTCD.sldCalibrationY.Step = 0.5;
		ctlCalibrateNonDTCD.sldCalibrationX.ScaleLength = 150;
		ctlCalibrateNonDTCD.sldCalibrationY.ScaleLength = 150;
		
		ctlCalibrateNonDTCD.sldCalibrationX.RegisterOnChangeHandler(this, OnCalibrate);
		ctlCalibrateNonDTCD.sldCalibrationY.RegisterOnChangeHandler(this, OnCalibrate);
		ctlCalibrateNonDTCD.btnReset.RegisterOnClickHandler(this, btnReset_Click);
		ctlCalibrateDTCD.btnMoveLeft.addEventListener("click", Delegate.create(this, onMoveLeft));
		ctlCalibrateDTCD.btnMoveDown.addEventListener("click", Delegate.create(this, onMoveDown));
		ctlCalibrateDTCD.btnMoveUp.addEventListener("click", Delegate.create(this, onMoveUp));
		ctlCalibrateDTCD.btnMoveRight.addEventListener("click", Delegate.create(this, onMoveRight));
		ctlCalibrateDTCD.txtX.RegisterOnEnterHandler(this, onTxtXEnter);
		ctlCalibrateDTCD.txtX.RegisterOnFocusOutHandler(this, onTxtXEnter);
		ctlCalibrateDTCD.txtY.RegisterOnEnterHandler(this, onTxtYEnter);
		ctlCalibrateDTCD.txtY.RegisterOnFocusOutHandler(this, onTxtYEnter);
		
		TooltipHelper.SetTooltip2(ctlCalibrateNonDTCD.btnReset, "IDS_TOOLTIP_RESET");
		
		_global.GlobalNotificator.OnComponentLoaded("PreviewArea.Calibrate", this);
		
		var printersArray:Array = new Array();
		printersArray = SAPrinter.Printers;
		ctlCalibrateDTCD.cbPrinter.addItem("Custom settings");
		for (var i in printersArray) {
			this.ctlCalibrateDTCD.cbPrinter.addItem(printersArray[i]);
		}
		ctlCalibrateDTCD.cbPrinter.addEventListener("change", Delegate.create(this, onPrinterChanged));		
		
		Init();
	}
	
	function onTxtXEnter(eventObject) {
		var number:Number = parseFloat(ctlCalibrateDTCD.txtX.label.text);
		number -= GetDefaultShiftX();
		
		if (!isNaN(number)) {
			if (number < minX) number = minX;
			if (number > maxX) number = maxX;
			xPos = number;
			OnCalibrateDTCD();
			ctlCalibrateDTCD.cbPrinter.selectedIndex = 0;
		} else {
			var xValue = Math.round((GetDefaultShiftX()+xPos) *10) / 10;
			ctlCalibrateDTCD.txtX.text = xValue.toString();
			}
	}
	
	function onTxtYEnter(eventObject) {
		var number:Number = parseFloat(ctlCalibrateDTCD.txtY.label.text);
		number -= GetDefaultShiftY();
		number *= -1;
		
		if (!isNaN(number)) {
			if (number > minY) number = minY;
			if (number < maxY) number = maxY;
			yPos = number;
			OnCalibrateDTCD();
			ctlCalibrateDTCD.cbPrinter.selectedIndex = 0;
		} else {
			var yValue = Math.round((GetDefaultShiftY()-yPos) *10) / 10;
			ctlCalibrateDTCD.txtY.text = yValue.toString();
			}
	}
	
	function onMoveLeft(eventObject) {
		if (xPos >= minX + step) {
			xPos -= step;
			if (Math.abs(xPos - minX) < step) xPos = minX;
			OnCalibrateDTCD();
			ctlCalibrateDTCD.cbPrinter.selectedIndex = 0;
		}
	}
	
	function onMoveDown(eventObject) {
		if (yPos >= maxY + step) {
			yPos -= step;
			if (Math.abs(yPos - maxY) < step) yPos = maxY;
			OnCalibrateDTCD();
			ctlCalibrateDTCD.cbPrinter.selectedIndex = 0;
		}
	}
	
	function onMoveUp(eventObject) {
		if (yPos <= minY - step) {
			yPos += step;
			if (Math.abs(yPos - minY) < step) yPos = minY;
			OnCalibrateDTCD();
			ctlCalibrateDTCD.cbPrinter.selectedIndex = 0;
		}
	}
	
	function onMoveRight(eventObject) {
		if (xPos <= maxX - step) {
			xPos += step;
			if (Math.abs(xPos - maxX) < step) xPos = maxX;
			OnCalibrateDTCD();
			ctlCalibrateDTCD.cbPrinter.selectedIndex = 0;
		}
	}
	
	private var intervalDiam : Number = null;
	
	private function OnDiamChanged(eventObject) {
		diamInner = ctlCalibrateDTCD.sldInnerDiam.Points;
		diamOuter = ctlCalibrateDTCD.sldOuterDiam.Points;
		if (intervalDiam != null) {
			clearInterval(intervalDiam);
		}
		intervalDiam = setInterval(this, "OnCalibrateDTCD", 300);
		//OnCalibrateDTCD();
	}
	
	function GetDefaultShiftX():Number {
		return (faceW/2 + paperX) * pxToMmCoeff;
	}
	
	function GetDefaultShiftY():Number {
		return (faceH/2 + paperY) * pxToMmCoeff;
	}
	
	function onPrinterChanged(eventObject) {
		var label:String = eventObject.target.selectedItem.label;
		var printerShifts:Object = SAPrinter.GetPrinterShifts(label);
		
		if (printerShifts == null) {Reset(); return;}
		
		var shiftX:Number = printerShifts.xshift - GetDefaultShiftX();
		var shiftY:Number = printerShifts.yshift - GetDefaultShiftY();

		xPos = +shiftX;
		yPos = -shiftY;
		OnCalibrateDTCD();
	}
	
	public function Init() {
		faceW = _global.Project.CurrentPaper.CurrentFace.Width;
		faceH = _global.Project.CurrentPaper.CurrentFace.Height;
		paperW = _global.Project.CurrentPaper.Width;
		paperH = _global.Project.CurrentPaper.Height;
		paperX = _global.Project.CurrentPaper.CurrentFace.PaperX;
		paperY = _global.Project.CurrentPaper.CurrentFace.PaperY;
		
		minX = -paperX * pxToMmCoeff;
		minY = paperY * pxToMmCoeff;
		maxX = (paperW - paperX - faceW) * pxToMmCoeff;
		maxY = -(paperH - paperY - faceH) * pxToMmCoeff;
		
		ctlCalibrateNonDTCD._visible = !_global.Project.CurrentPaper.IsDirectToCDDevice;
		ctlCalibrateDTCD._visible = _global.Project.CurrentPaper.IsDirectToCDDevice;
	}
	
	public function set Visibility(value:Boolean) : Void {
		ctlCalibrateNonDTCD._x = (ctlCalibrateNonDTCD._visible && value) ? 22 : -300;//bugfix (when control hidden it can't receave mouse events)
		ctlCalibrateDTCD._x = (ctlCalibrateDTCD._visible && value) ? 22 : -300;
	}
	
	public function DataBind(data:Object) {
		Init();
		
		if (_global.Project.CurrentPaper.IsDirectToCDDevice) {
			xPos = (!isNaN(data.x)) ? data.x - GetDefaultShiftX() : 0;
			yPos = (!isNaN(data.y)) ? - (data.y  - GetDefaultShiftY()) : 0;
			
			if (isNaN(data.x) || xPos + 0.1 < minX || xPos - 0.1 > maxX) {
				_global.Project.CurrentPaper.Calibration.XDirectToCD = GetDefaultShiftX();
				xPos = 0;
			}
			
			if (isNaN(data.y) || -yPos  + 0.1 < -minY || -yPos  - 0.1 > -maxY) {
				_global.Project.CurrentPaper.Calibration.YDirectToCD = GetDefaultShiftY();
				yPos = 0;
			}
			
			ctlCalibrateDTCD.sldInnerDiam.Min = data.diamInnerMin;
			ctlCalibrateDTCD.sldInnerDiam.Max = data.diamInnerMax;
			ctlCalibrateDTCD.sldInnerDiam.Step = 1;
			ctlCalibrateDTCD.sldInnerDiam.ScaleLength = 150;
			ctlCalibrateDTCD.sldInnerDiam.SetPoints(data.diamInner);
			ctlCalibrateDTCD.sldInnerDiam.IsZeroLabel = false;
			ctlCalibrateDTCD.sldInnerDiam.Refresh();
			
			ctlCalibrateDTCD.sldOuterDiam.Min = data.diamOuterMin;
			ctlCalibrateDTCD.sldOuterDiam.Max = data.diamOuterMax;
			ctlCalibrateDTCD.sldOuterDiam.Step = 1;
			ctlCalibrateDTCD.sldOuterDiam.ScaleLength = 150;
			ctlCalibrateDTCD.sldOuterDiam.SetPoints(data.diamOuter);
			ctlCalibrateDTCD.sldOuterDiam.IsZeroLabel = false;
			ctlCalibrateDTCD.sldOuterDiam.Refresh();
			
			UpdateTextFields();
			OnNeedRefresh(xPos / pxToMmCoeff, - yPos / pxToMmCoeff);
		} else {
			if (isNaN(data.x)) _global.Project.CurrentPaper.Calibration.X = 0;
			if (isNaN(data.y)) _global.Project.CurrentPaper.Calibration.Y = 0;
			
			ctlCalibrateNonDTCD.sldCalibrationX.SetPoints((!isNaN(data.x)) ? data.x * pxToMmCoeff : 0);
			ctlCalibrateNonDTCD.sldCalibrationY.SetPoints((!isNaN(data.y)) ? - data.y * pxToMmCoeff : 0);
			OnNeedRefresh(ctlCalibrateNonDTCD.sldCalibrationX.Points / pxToMmCoeff,  - ctlCalibrateNonDTCD.sldCalibrationY.Points / pxToMmCoeff);
			}
	}
	
	public function RegisterOnCalibrateHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onCalibrate", Delegate.create(scopeObject, callBackFunction));
	}
	
	public function RegisterOnCalibrateDTCDHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onCalibrateDTCD", Delegate.create(scopeObject, callBackFunction));
	}
	
	public function RegisterOnNeedRefreshHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onNeedRefresh", Delegate.create(scopeObject, callBackFunction));
	}
	
	private function OnCalibrate(eventObject) {
		if (_global.MainWindow.PreviewArea.visible == true) {
			var eventObject = {
				type:"onCalibrate", 
				x: ctlCalibrateNonDTCD.sldCalibrationX.Points / pxToMmCoeff,  
				y: - ctlCalibrateNonDTCD.sldCalibrationY.Points / pxToMmCoeff};
			this.dispatchEvent(eventObject);
		}
		else {
			eventObject.target.SetPoints(eventObject.oldValue);
		}
	}
	
	private function OnCalibrateDTCD() {
		if (intervalDiam != null) {
			clearInterval(intervalDiam);
			intervalDiam = null;
		}
		UpdateTextFields();
		var eventObject = {
			type:"onCalibrateDTCD", 
			x: xPos / pxToMmCoeff, 
			y: - yPos / pxToMmCoeff,
			xCenter: GetDefaultShiftX()+xPos, 
			yCenter: GetDefaultShiftY()-yPos,
			diamInner: this.diamInner,
			diamOuter: this.diamOuter};
		this.dispatchEvent(eventObject);
	}
	
	private function OnNeedRefresh(xValue:Number, yValue:Number) {
		var eventObject = {type:"onNeedRefresh", x: xValue, y: yValue};
		this.dispatchEvent(eventObject);
	}
	
	private function UpdateTextFields() {
		var xShift = GetDefaultShiftX()+xPos;
		var xFloorShift = Math.floor(xShift);
		var dx:Number = xShift - xFloorShift ;
		if (dx >= 0.25 && dx < 0.75) xFloorShift += 0.5;
		else if (dx >= 0.75) xFloorShift += 1;

		ctlCalibrateDTCD.txtX.text = xFloorShift.toString();
		
		var yShift = GetDefaultShiftY()-yPos;
		var yFloorShift = Math.floor(yShift);
		var dy:Number = yShift - yFloorShift ;
		if (dy >= 0.25 && dy < 0.75) yFloorShift += 0.5;
		else if (dy >= 0.75) yFloorShift += 1;
		
		ctlCalibrateDTCD.txtY.text = yFloorShift.toString();
	}
	
	private function Reset() {
		if (_global.Project.CurrentPaper.IsDirectToCDDevice) {
			ctlCalibrateDTCD.cbPrinter.selectedIndex = 0;
			xPos = yPos = 0;
			OnCalibrateDTCD();
		} else {
			ctlCalibrateNonDTCD.sldCalibrationX.Points = ctlCalibrateNonDTCD.sldCalibrationY.Points = 0;
			OnCalibrate();
			}
	}
	
	private function btnReset_Click(eventObject) {
		Reset();
	}
}