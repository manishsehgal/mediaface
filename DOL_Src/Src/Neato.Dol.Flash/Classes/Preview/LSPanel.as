﻿class Preview.LSPanel extends mx.core.UIObject {
	private var ctlLSProgressBar:mx.controls.ProgressBar;
	private var nLSQuality:Number;
	private var silentMode:Boolean;
	private var ctlSilentMode:mx.controls.CheckBox;
	private var txtWin:TextField;
	private var txtMac:TextField;

	public function LSPanel() {
	}

	public function SetProgress(scanDone:Number,scanTotal:Number) {
		ctlLSProgressBar.setProgress(scanDone,scanTotal);
	}
	
	public function onLoad() {
		_global.GlobalNotificator.OnComponentLoaded("PreviewArea.LSPanel", this);
		nLSQuality = 1; // Normal
		silentMode = false;
		if(_global.basePlatform != "Windows")
		{
			ctlSilentMode.visible = false;
			ctlSilentMode.enabled = false;
			txtWin._visible = false;
			txtMac._visible = true;
		} else {
			txtWin._visible = true;
			txtMac._visible = false;
		}
	}
	
	function get LSQuality():Number {
		return this.nLSQuality;
	}

	function set LSQuality(newLSQuality:Number) {
		this.nLSQuality = newLSQuality;
	}

	function get SilentMode():Boolean {
		return this.silentMode;
	}

	function set SilentMode(newMode:Boolean) {
		this.silentMode = newMode;
	}
	
}