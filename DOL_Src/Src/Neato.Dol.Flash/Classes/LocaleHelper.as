﻿class LocaleHelper {
	private var defaultLang:String;
	private var longLang:String;
	private var shortLang:String;
	private var stringMap:Object = new Object();
	
	private var parseLang:String;
	private var parseFla:String;
	
	private var strings:Object = new Object();
	
	function LocaleHelper(language:String, defaultLanguage:String) {
		longLang = language;
		shortLang = longLang.substr(0,2);
		defaultLang = defaultLanguage;
		
		strings["IDS_TOOLTIP_ALIGNHL"] = "Move to the nearest object at the left";
		strings["IDS_TOOLTIP_ALIGNHC"] = "Center horizontally";
		strings["IDS_TOOLTIP_ALIGNHR"] = "Move to the nearest object at the right";
		strings["IDS_TOOLTIP_ALIGNVT"] = "Move to the nearest object above";
		strings["IDS_TOOLTIP_ALIGNVC"] = "Center vertically";
		strings["IDS_TOOLTIP_ALIGNVB"] = "Move to the nearest object below";
		
		strings["IDS_TOOLTIP_LAYERUP"] = "Bring currently selected object one layer forward or backward";
		strings["IDS_TOOLTIP_LAYERDOWN"] = "Bring currently selected object one layer forward or backward";
		strings["IDS_TOOLTIP_LAYERTOFRONT"] = "Place currently selected object in front of all other objects";
		strings["IDS_TOOLTIP_LAYERTOBACK"] = "Place currently selected object behind all other objects";
		
		strings["IDS_TOOLTIP_MOVELEFT"] = "Shift currently selected object to the left";
		strings["IDS_TOOLTIP_MOVEDOWN"] = "Shift currently selected object down";
		strings["IDS_TOOLTIP_MOVEUP"] = "Shift currently selected object up";
		strings["IDS_TOOLTIP_MOVERIGHT"] = "Shift currently selected object to the right";
		
		strings["IDS_TOOLTIP_ROTATECCW"] = "Rotate currently selected object counter-clockwise";
		strings["IDS_TOOLTIP_ROTATE90CCW"] = "Rotate counter-clockwise the currently selected object on 90 degrees";
		strings["IDS_TOOLTIP_ROTATE90CW"] = "Rotate clockwise the currently selected object on 90 degrees";
		strings["IDS_TOOLTIP_ROTATECW"] = "Rotate currently selected object clockwise";
		
		strings["IDS_TOOLTIP_SCALEINC"] = "Make currently selected object larger";
		strings["IDS_TOOLTIP_SCALEDEC"] = "Make currently selected object smaller";
		
		strings["IDS_TOOLTIP_NEW"] = "Close the current design and return to the device selection step";
		strings["IDS_TOOLTIP_LOAD"] = "Uploads a previously completed MediaFACEonline design, saved on your computer";
		strings["IDS_TOOLTIP_SAVE"] = "Saves the design on your computer. The next time you go to www.mediaface.com, you can upload, print or edit your previous design";
		
		strings["IDS_TOOLTIP_CONFIG"] = "Install or update plug-ins";
		strings["IDS_TOOLTIP_HIDE"] = "Hide this menu";
		
		strings["IDS_TOOLTIP_ZOOMIN"] = "Zoom in the workarea";
		strings["IDS_TOOLTIP_ZOOMOUT"] = "Zoom out the workarea";
		strings["IDS_TOOLTIP_ZOOMRESTORE"] = "Restore the workarea";
		
		strings["IDS_TOOLTIP_CLEAR"] = "Completely clears the label displayed onscreen (Clears one label only)";
		
		strings["IDS_TOOLTIP_CHANGECOLORS"] = "Choose from standard colors or an advanced color palette";
		
		strings["IDS_TOOLTIP_UPDATEEFFECT"] = "Apply changes to the selected text on the workarea";
		
		strings["IDS_TOOLTIP_CANCEL"] = "Cancel this operation";
		strings["IDS_BTNAUDIO"] = " Import tracks from an AudioCD disk";
		strings["IDS_BTNCDTEXT"] = " Import playlist from a disk with CD-TEXT";
		strings["IDS_BTNFOLDER"] = " Build a playlist from a folder with music files";
		strings["IDS_BTNPLAYER"] = " Import playlist from a player";
		strings["IDS_BTNFILE"] = " Import a playlist file";
		strings["IDS_BTNMANUAL"] = " Create playlist manually";
		strings["IDS_BTNCREATENEW"] = "Create the playlist based of the selection above";
		strings["IDS_CHKFINGERPRINT"] = " Fingerprint the files in the playlist upon creation";
		
		strings["IDS_TOOLTIP_UPLOADIMAGE"] = "Upload your own image from your computer";
		
		strings["IDS_TOOLTIP_UNDO"] = "Undo the last paint command";
		
		strings["IDS_TOOLTIP_DELETE"] = "Deletes the currently selected design object";
		strings["IDS_TOOLTIP_CONTINUE"] = "Click here when you are done with the current operation";
		strings["IDS_TOOLTIP_KEEPPROPORTIONS"] = "While resizing keep the object's proportions (aspect ratio) as they currently are";
		strings["IDS_TOOLTIP_RESTOREPROPORTIONS"] = "Restore the initial object's proportions and size";
		
		strings["IDS_TOOLTIP_APPLYIMAGEEFFECT"] = "Apply the effects to the currently selected image";
		
		strings["IDS_TOOLTIP_NOFILL"] = "Check this box to make the internal area of the shape transparent.";
		
		strings["IDS_TOOLTIP_YES"] = "Click here if you really want to accomplish the current operation";
		strings["IDS_TOOLTIP_NO"] = "Click here if you have changed your mind and want to cancel the current operation";
		
		strings["IDS_TOOLTIP_SELECTCURRENTPAPER"] = "Copy design to another label on the same paper";
		
		strings["IDS_TOOLTIP_RESET"] = "Reset calibration to default values";
		
		strings["IDS_TOOLTIP_PRINT"] = "Print your design to your printer";
		strings["IDS_TOOLTIP_PRINTTOPDF"] = "Create a PDF file from your design";
		strings["IDS_TOOLTIP_LIGHTSCRIBE"] = "Print your design on a LightScribe CD disk";
		
		strings["IDS_BRUSH_Circle"] = "Pen";
		strings["IDS_BRUSH_GradientCircle"] = "Brush";
		strings["IDS_BRUSH_GradientLeftMarker"] = "Brush Tilt Left";
		strings["IDS_BRUSH_GradientRightMarker"] = "Brush Tilt Right";
		strings["IDS_BRUSH_LeftMarker"] = "Pen Tilt Left";
		strings["IDS_BRUSH_RightMarker"] = "Pen Tilt Right";

		strings["IDS_TOOLTIP_MENUUNDO"] = "Undo action";
		strings["IDS_TOOLTIP_MENUREDO"] = "Redo action";
	}
	
	public function AddStringsXML(language:String, flaName:String, doc:XML):Void {
		parseLang = language;
		parseFla = flaName;
		parseStringsXML(doc);
	}

	private function parseStringsXML(doc:XML):Void {
		if(doc.childNodes.length > 0 && doc.childNodes[0].nodeName == "xliff") {
			parseXLiff(doc.childNodes[0]);
		}
	}

	private function parseXLiff(node:XMLNode):Void {
		if(node.childNodes.length > 0 && node.childNodes[0].nodeName == "file") {
			parseFile(node.childNodes[0]);
		}
	}

	private function parseFile(node:XMLNode):Void {
		if(node.childNodes.length > 1 && node.childNodes[1].nodeName == "body") {
			parseBody(node.childNodes[1]);
		}
	}

	private function parseBody(node:XMLNode):Void {
		for(var i:Number = 0; i < node.childNodes.length; i++) {
			if(node.childNodes[i].nodeName == "trans-unit") {
				parseTransUnit(node.childNodes[i]);
			}
		}
	}

	private function parseTransUnit(node:XMLNode):Void {
		var id:String = node.attributes.resname;
		if(id.length > 0 && node.childNodes.length > 0 &&
				node.childNodes[0].nodeName == "source") {
			var value:String = parseSource(node.childNodes[0]);
			if(value.length > 0) {
				if (stringMap[parseLang] == undefined)
					stringMap[parseLang] = new Object();
				if (stringMap[parseLang][parseFla] == undefined)
					stringMap[parseLang][parseFla] = new Object();
				if (stringMap[parseLang][parseFla][id] == undefined)
					stringMap[parseLang][parseFla][id] = new Object();
					
				stringMap[parseLang][parseFla][id] = value;
			}
		}
	}
	
	private function parseSource(node:XMLNode):String {
		if(node.childNodes.length > 0) {
			return node.childNodes[0].nodeValue;
		}

		return "";
	}
	
	public function GetString(flaName:String, stringID:String):String {
		var localizedString:String;
		localizedString = stringMap[longLang][flaName][stringID];
		
		if (localizedString == undefined)
			localizedString = stringMap[shortLang][flaName][stringID];
		
		if (localizedString == undefined)
			localizedString = stringMap[defaultLang][flaName][stringID];
			
		return localizedString;		
	}

	public function GetString2(stringID:String):String {
		return strings[stringID];
	}
	
	public function LocalizeInstance(instance, flaName:String, stringID:String) {
		var localizedString:String = GetString(flaName, stringID);
		
		if (localizedString == undefined)
			return;
			
		if (instance.text != undefined) {
			instance.text = localizedString;
			return;
		}
			
		if (instance.label != undefined)
			instance.label = localizedString;
	}
}