﻿class SALocalWork {
	private static var localWork:Boolean = undefined;
	public static function get IsLocalWork():Boolean {
		if (localWork != undefined)
			return localWork;
	
		var tmp:String = _level0.localwork;
		localWork = tmp.toLowerCase() != "false";
		
		return localWork;
	}
}