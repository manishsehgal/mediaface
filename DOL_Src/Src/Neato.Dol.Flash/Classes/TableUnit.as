﻿class TableUnit extends MoveableUnit {
	var columns:Array;
	var columnsOrder:Array;
	static var columnsNum:Number = 4;
	//private var layoutPosition:Number; 
	private var emptyLayoutItem:Boolean = undefined;
	var italic:Boolean = false;
	var bold:Boolean = false;
	var underline:Boolean = false;
	var fontSize:Number = 12;
	var font:String = "Arial";
	var strokeInterval:Number = 0;
	public var frameLinkageName:String = "TableFrame";
	private var origPlayList:String = "";
	
	function TableUnit(mc:MovieClip, node:XML, bPlaylist:Boolean,bNew:Boolean) {
		super(mc, node);
		unitClassName = "TableUnit";
		keepProportions = false;
		columns=new Array(columnsNum);
		columnsOrder=new Array(columnsNum);
		var currWidth:Number = 0;
		trace("TableUnit constructor");
		this.Visible = true;
		if(bPlaylist == undefined)
			bPlaylist = false;
		//var bNew:Boolean = false;
		//trace(node);
		if(node == null)
				bNew = true;
		if (!bNew && !bPlaylist) {
			
			this.italic = node.attributes.italic.toString() == "true" ? true : false;
			this.bold = node.attributes.bold.toString() == "true" ? true : false;
			this.underline = node.attributes.underline.toString() == "true" ? true : false;
			this.fontSize = parseInt(node.attributes.fontSize);
			this.font = node.attributes.font;

			this.Visible = node.attributes.visible;
			if (node.attributes.layoutPosition != undefined || node.attributes.layoutPosition != "undefined")
				layoutPosition = node.attributes.layoutPosition;
			if (node.attributes.emptyLayoutItem != undefined && node.attributes.emptyLayoutItem != "undefined")
				this.EmptyLayoutItem = node.attributes.emptyLayoutItem;
			
			this.strokeInterval = parseInt(node.attributes.strokeInterval);
			for (var j:Number = 0; j < node.childNodes[j].childNodes.length;j++) {
				if(node.childNodes[j].nodeName == "Columns")
				for (var i:Number = 0; i < columnsNum/*node.childNodes.length*/; ++i) {
					columns[i]=new Object({
						text:String, 
						color:Number,
						columnOrder:Number,
						id:String,
						align:String,
						width:Number,
						height:Number,
						textField:TextField,
						visible:Boolean,
						fontFormat:TextFormat});
						//trace(childNode);
					var childNode:XML = node.childNodes[j].childNodes[i]; //Column
					var col = columns[i];
					col.text = childNode.attributes.text;
					col.color = parseInt(childNode.attributes.color.substr(1), 16);
					col.columnOrder = parseInt(childNode.attributes.columnOrder,10);
					if(col.visible ==  true)
						columnsOrder[col.columnOrder] = i;
				
					col.id = childNode.attributes.id;
					col.align = childNode.attributes.align;
					col.width = parseInt(childNode.attributes.width,10);
					col.visible =  childNode.attributes.visible.toString() == "true" ? true : false;
					
				}
			}
		} else if (bNew && !bPlaylist) {
			for (var i:Number = 0; i < columnsNum/*node.childNodes.length*/; ++i) {
					columns[i]=new Object({
						text:String, 
						color:Number,
						columnOrder:Number,
						id:String,
						align:String,
						width:Number,
						height:Number,
						textField:TextField,
						visible:Boolean,
						fontFormat:TextFormat});
					
					var col = columns[i];
					col.text ="" ;
					col.color = 0;
					col.columnOrder = i;
					col.id = "col"+i;
					col.align = "center";
					
					col.width = (i == 0 || i == 3)?40:80;
					col.visible = true;
					columns[i] = col;
			}
		} else if (bPlaylist) {
					//trace("Playlist received");
					var Tracks:XMLNode=node.firstChild;
					var dta:Array = new Array( //{
						"",//TrNumber:"",
						"",//TrArtist:"",
						"",//TrTitle:"",
						""//TrDuration:""
						);//})
					var j:Number = 1;
					var retline = String.fromCharCode(13);
					for(var TrckNode:XMLNode=Tracks.firstChild;TrckNode!=null;TrckNode=TrckNode.nextSibling)
					{
						
						dta[0] += j + retline;
						
						dta[1] += TrckNode.attributes.Artist + retline;
						
						dta[2] += TrckNode.attributes.Title + retline;
						
						dta[3] += TrckNode.attributes.Duration + retline;
						j++;
					}
					for (var i:Number = 0; i < columnsNum/*node.childNodes.length*/; ++i) {
					columns[i]=new Object({
						text:String, 
						color:Number,
						columnOrder:Number,
						id:String,
						align:String,
						width:Number,
						height:Number,
						textField:TextField,
						visible:Boolean,
						fontFormat:TextFormat});
					
					var col = columns[i];
					col.text = dta[i];
					col.color = 0;
					col.columnOrder = i;
					col.id = "col"+i;
					col.align = "center";
					col.width = (i == 0 || i == 3)?40:80;
					col.visible = true;
					
					origPlayList = node.toString();
				    }
		}
		if( node.attributes.visible == undefined || node.attributes.visible.toString() == "true"){
			this.Visible = true;
		}
		else 
			this.Visible = false;
		//trace("Table unit:this.visible = "+this.Visible);
		if(id == undefined)	
			id = this.mc._name;
		width = currWidth;
		/*for(var i in columns[0]){
						//trace("%%#%#%@#$%@#$%@#$%@#$%@#$5");
				trace("i="+i+" ==> "+columns[i][1]);
			trace("%%#%#%@#$%@#$%@#$%@#$%@#$5");
		}*/
		SortColumns();
	}
	function ReinitTable(node:XMLNode,bLayout:Boolean,bEmpty:Boolean,bRewriteFormatting:Boolean) : Void {
		var Tracks:XMLNode=node.firstChild;
		var oldcolumns:Array = columns;
		columns = new Array();
		var dta:Array = new Array( //{
			"",//TrNumber:"",
			"",//TrArtist:"",
			"",//TrTitle:"",
			""//TrDuration:""
				);//})
		if(bLayout == undefined){
			bLayout = false;
		}
		if(bEmpty == undefined){
			bEmpty = false;
		}
		if(bRewriteFormatting == undefined){
			bRewriteFormatting = false;
		}
		
		
		var retline = String.fromCharCode(13);
		//trace("Reinit table "+node+" blayout "+bLayout + "  bEmpty "+bEmpty);
		if(columns == undefined)
			columns = new Array();
		if(!bEmpty && node == null && bLayout){
			//trace("emptyLayoutItem = "+emptyLayoutItem);
			//trace("GetAllText().length <1  = "+GetAllText().length);
			if(GetAllText().length > 0 )
				return;
				
			for (var i:Number = 0; i < columnsNum; ++i) {
				columns[i]=new Object({
						text:String, 
						color:Number,
						columnOrder:Number,
						id:String,
						align:String,
						width:Number,
						height:Number,
						textField:TextField,
						visible:Boolean,
						fontFormat:TextFormat});
				
				var col = columns[i];
				col.color = bRewriteFormatting?0:oldcolumns[i].color;
				col.text = "";
				col.columnOrder = bRewriteFormatting?i:oldcolumns[i].columnOrder;;
				col.id = "col"+i;
				col.align = bRewriteFormatting?"center":oldcolumns[i].align;
				col.width = bRewriteFormatting?((i == 0 || i == 3)?40:80):oldcolumns[i].width;
				col.visible = bRewriteFormatting? true :oldcolumns[i].visible;
			}
			for(var i=0;i<4;i++){
				var no:Number = i+1;
				columns[0].text+=no+retline;
				columns[1].text+="Artist"+retline;
				columns[2].text+="Track "+no+retline;
				columns[3].text+="0:00"+retline;
			}
			
			
		}
		
		if(bEmpty && node == null && !bLayout) {
			if(GetAllText().length > 0 && LayoutPosition == undefined)
				return;
			for (var i:Number = 0; i < columnsNum/*node.childNodes.length*/; ++i) {
				columns[i]=new Object({
						text:String, 
						color:Number,
						columnOrder:Number,
						id:String,
						align:String,
						width:Number,
						height:Number,
						textField:TextField,
						visible:Boolean,
						fontFormat:TextFormat});					
				/*var col = columns[i];
				col.color = bRewriteFormatting?0:oldcolumns[i].color;
				col.text = "";
				col.columnOrder = i;
				col.id = "col"+i;
				col.align = bRewriteFormatting?"center":oldcolumns[i].align;
					
					col.width = (i == 0 || i == 3)?40:80;
					col.visible = true;*/
				var col = columns[i];
				col.color = bRewriteFormatting?0:oldcolumns[i].color;
				col.text = "";
				col.columnOrder = bRewriteFormatting?i:oldcolumns[i].columnOrder;;
				col.id = "col"+i;
				col.align = bRewriteFormatting?"center":oldcolumns[i].align;
				col.width = bRewriteFormatting?((i == 0 || i == 3)?40:80):oldcolumns[i].width;
				col.visible = bRewriteFormatting? true :oldcolumns[i].visible;
			}
		}
		if(!bEmpty && !bLayout){
			var j:Number = 1;
			EmptyLayoutItem = undefined;
			LayoutPosition = undefined;
			for(var TrckNode:XMLNode=Tracks.firstChild;TrckNode!=null;TrckNode=TrckNode.nextSibling)
			{
				
				dta[0] += j + retline;
				
				dta[1] += TrckNode.attributes.Artist + retline;
				
				dta[2] += TrckNode.attributes.Title + retline;
				
				dta[3] += TrckNode.attributes.Duration + retline;
				j++;
			}
			columns = new Array();
			for (var i:Number = 0; i < columnsNum/*node.childNodes.length*/; ++i) {
				columns[i]=new Object({
						text:String, 
						color:Number,
						columnOrder:Number,
						id:String,
						align:String,
						width:Number,
						height:Number,
						textField:TextField,
						visible:Boolean,
						fontFormat:TextFormat});
						
				var col = columns[i];
				col.color = bRewriteFormatting?0:oldcolumns[i].color;
				col.text = dta[i];
				col.columnOrder = bRewriteFormatting?i:oldcolumns[i].columnOrder;;
				col.id = "col"+i;
				col.align = bRewriteFormatting?"center":oldcolumns[i].align;
				col.width = bRewriteFormatting?((i == 0 || i == 3)?40:80):oldcolumns[i].width;
				col.visible = bRewriteFormatting? true :oldcolumns[i].visible;
			}
		}
		origPlayList = node.toString();
		Draw();
	}
	
	public function get OrigPlayList():String {
		return origPlayList;
	}
	
	function GetXmlNode():XMLNode {
		if(GetAllText().length < 1 &&  LayoutPosition == undefined)
			return null;
		//trace("GetXMLnode after layout verification "+GetAllText().length +" = GetAllText().length ");
		//trace("EmptyLayoutItem = " +EmptyLayoutItem);
		//trace("LayoutPosition = " +LayoutPosition);
		//trace ("TableUnit:GetXmlNode");
		var node:XMLNode = super.GetXmlNode();
		node.nodeName = "Playlist";
		node.attributes.italic = this.italic;
		node.attributes.bold = this.bold;
		node.attributes.underline = this.underline;
		node.attributes.fontSize = this.fontSize;
		node.attributes.font = this.font ;
		node.attributes.strokeInterval = this.strokeInterval;
		node.attributes.height = GetUnitHeight();
		node.attributes.width = UnitWidth;
		if (layoutPosition != undefined)
			node.attributes.layoutPosition = layoutPosition;
		node.attributes.visible = this.Visible;
		if (this.EmptyLayoutItem != undefined)
			node.attributes.emptyLayoutItem = this.EmptyLayoutItem;
		var columnsNode:XMLNode = new XMLNode(1,"Columns");
		for(var i:Number = 0; i < columnsNum; i++) {
			var columnNode:XMLNode = new XMLNode(1,"Column");
			columnNode.attributes.text = columns[i].text;
			columnNode.attributes.color = _global.GetColorForXML(columns[i].color);
			columnNode.attributes.columnOrder = columns[i].columnOrder;
			columnNode.attributes.id = columns[i].id;
			columnNode.attributes.visible = columns[i].visible;
			columnNode.attributes.align = columns[i].align;
			columnNode.attributes.width = columns[i].width;
			columnsNode.appendChild(columnNode);
		}
		node.appendChild(columnsNode);
		node.appendChild(super.GetXmlNodeRotate());
				

		return node;
	}
	
	public function set FontFormat(format:TextFormat) : Void {
		if (format.italic != null && format.italic != undefined)
			Italic = format.italic;
		if (format.bold != null && format.bold != undefined)
			Bold = format.bold;
		if (format.size != null && format.size != undefined)
			Size = format.size;
		if (format.font != null && format.font != undefined)
			Font = format.font;
	}
	
	public function get FontFormat() : TextFormat {
		var format:TextFormat = new TextFormat();
		format.italic = Italic;
		format.bold = Bold; 
		format.size = Size;
		format.font = Font;
		return format;
	}
	
	function Draw():Void {
		
		//trace ("TableUnit:Draw");
		DrawMC(mc,true);
	}
	function DrawMC(mc:MovieClip,bDrawOnWorkarea:Boolean):Void {
		SortColumns();
		mc.clear();
		
		var currWidth:Number = 0;
		var currHeight:Number = 0;
		if(bDrawOnWorkarea == undefined)
			bDrawOnWorkarea = false;
		if(bDrawOnWorkarea == false && !(EmptyLayoutItem == undefined || LayoutPosition ==undefined))
		{
			mc._visible = false;
		}
		else {
			mc._visible = true;
		}
	
		for(var i:Number = 0; i < columnsOrder.length; i++) {
				if(columnsOrder[i]== undefined || columnsOrder[i] == null )
					continue;
				var col = columns[columnsOrder[i]];
				mc[col.id].removeTextField();
				mc.createTextField(col.id , mc.getNextHighestDepth(), currWidth, 0, col.width, 0);
				
				col.textField = mc[col.id];
				col.textField.embedFonts = true;
				col.textField.autoSize = true;
				col.textField.wordWrap = false;
				col.textField.multiline = true;
				col.textField.html = false;
				col.textField.selectable = false;
				col.fontFormat = col.textField.getNewTextFormat();
				col.fontFormat.font = font;
				col.fontFormat.size = fontSize;
				col.fontFormat.bold = this.bold;
				col.fontFormat.italic = this.italic;
				col.fontFormat.underline = this.underline;
				col.fontFormat.color = col.color;
				col.fontFormat.align = col.align;
				col.fontFormat.leading = strokeInterval;
				
				if (!_global.Fonts.TestLoadedFont(col.fontFormat))
				{
					if(!_global.Fonts.TestFont(font, italic, bold)) {
						this.bold = false;
						this.italic = false;
						col.fontFormat.bold = this.bold;
						col.fontFormat.italic = this.italic;
					}
					else {
						_global.Fonts.LoadFont(col.fontFormat, this);
					}
				}
				
				col.textField._x= currWidth;
				col.textField.text = col.text;
				col.textField.setTextFormat(col.fontFormat);
				col.textField.setNewTextFormat(col.fontFormat);
				
				col.height = col.textField._height;
				currHeight = Math.max(col.height,currHeight);
				col.textField.autoSize = false;
				col.textField._height = col.height;
				col.textField._width = col.width;
				if(col.textField._width > col.width) {
					col.width = col.textField._width;
				}
				
				if(col.visible == false) {
					col.textField._width = 0;
				}
							
				col.textField._height += 2;
				col.textField._visible = col.visible;
				if(col.visible != false) {
					currWidth += col.width;
				}
				// Range Normalise ////////////////////////////////////
				for (var j:Number = 0; j < col.textField.length; j++) {
					var fontFormat:TextFormat = new TextFormat();
					fontFormat = col.fontFormat;
					fontFormat.font = _global.Fonts.FontRangeDetect(col.textField.text.charCodeAt(j), fontFormat.font);
					if (!_global.Fonts.TestLoadedFont(col.fontFormat))
					{
						_global.Fonts.LoadFont(col.fontFormat, this);
					}
					col.textField.setTextFormat(j, fontFormat);
				}
				///////////////////////////////////////////////////////
				
		}
		
		mc.beginFill(0, 0);
		mc.moveTo(0, 0);
		mc.lineTo(currWidth, 0);
		mc.lineTo(currWidth, GetUnitHeight());
		mc.lineTo(0, currHeight);
		mc.endFill();
		if(_global.Project.CurrentUnit == this)
			AttachInternal();
	}
	
	function getMode():String {
		return _global.PlaylistEditMode;
	}
	
	function GetColumnsWidth():Array {
		var result:Array = new Array();
		for(var i:Number = 0; i < columnsNum; i++) {
			if(columnsOrder[i] != undefined)
				if(columns[columnsOrder[i]].visible == true)
					result.push( columns[columnsOrder[i]].width) ;
		}
		return result;
	}
	
	
	function SortColumns() : Void {
		columnsOrder = new Array(columnsNum);
		for(var i:Number = 0; i < columnsNum ; i++) {
		//	if(columns[i].visible == true)
				columnsOrder[columns[i].columnOrder]=i;
		}	
		//trace("Col order"+columnsOrder);
	}
	
	public function ResizeByStep(isPositive:Boolean) : Void {
		var step:Number = isPositive ? 1 : -1;
		var k:Number = (this.Size + step) / this.Size;
		var size:Number = this.Size;
		Resize(1, k);
		if (size != this.Size) {
			Resize(k, 1);
		}
	}
	
	private function Resize(kx:Number, ky:Number) : Void {
 		var newSize:Number  = Math.round(this.Size * ky);

 		if (newSize >=4 && newSize <= 127 && newSize != this.fontSize) {
			if(GetAllText().length >0)
 			this.fontSize = newSize;
			_global.UIController.ToolsController.playlistToolController.columnGeneral.RefreshFontSize();
 		}
		
		var allWidth:Number = UnitWidth;
		if(allWidth < 40 && kx < 1) {
			kx = 1;
			allWidth = 40.1;
		}

		var currWidth:Number = 0;
		for(var i:Number = 0; i < columnsNum ; i++) {
			var colnum:Number = columnsOrder[i];
			if(colnum != undefined && columns[colnum].visible != false)
			{	
				columns[colnum].width = kx * columns[colnum].width ;
				if(columns[colnum].width <10){
					columns[colnum].width = 10; 
				}
				columns[colnum].textField._x = currWidth;
				currWidth += columns[colnum].width;
			}
		}
		Draw();
	}
	
	function ResizeColumnWidth(colNum:Number,deltaX:Number) : Void {
		var showed:Number = 0;
		//trace(colNum);
		for(var i:Number = 0; i < columnsNum; i++) {
			if(columns[columnsOrder[i]].visible == true){
				
				if(showed == colNum){
					if ((columns[columnsOrder[i]].width < 10 && deltaX < 0) || ((columns[columnsOrder[i]].width + deltaX )< 10))
							return;
					columns[columnsOrder[i]].width += deltaX;
					if(columns[columnsOrder[i]].width < 10)
						columns[columnsOrder[i]].width = 10;
					break;
				}
				showed++;
			}
		}
		
		Draw();
		AttachInternal();
		
	}
	function get UnitWidth():Number {
		var res:Number = 0;
		for(var i:Number = 0; i < columnsNum; i++) {
			if(columns[i].visible != false)
				res += columns[i].width;
		}
		return res;
	}
	function GetUnitHeight():Number {
		var res:Number = 0;
		for(var i:Number = 0; i < columnsNum; i++) {
		//	if(columns[i].visible != false)
				res = Math.max( columns[i].textField._height,res);
		}
		return res;
	}

	function SetLeading(pos:String, deltaY:Number) : Void {
		
		var delta:Number = Math.ceil(deltaY / 12);
		if(GetAllText().length <1)
			return;
		if(pos == "bottomleft") 
			strokeInterval += delta;
		else if(pos == "topright")
			strokeInterval -= delta;
		var oldHeight:Number = GetUnitHeight();
		var oldX:Number = this.mc._x;
		var oldY:Number = this.mc._y;
		if(strokeInterval < 0)
		strokeInterval = 0;
			
		
		Draw();
		var newHeight:Number = GetUnitHeight();
		//trace("Angle = "+Angle);
		var angle:Number = Angle*Math.PI/180;
		//trace("angle = "+angle);
		if(pos == "topright") 
		{
			_global.SelectionFrame.Y -= (newHeight-oldHeight) * Math.cos(angle);
			_global.SelectionFrame.X += (newHeight-oldHeight) * Math.sin(Math.PI-angle);
			
		}
		
	}
	
	public function get Size ():Number {
		return fontSize;
	}
	public function set Size (value:Number) {
		fontSize = value;
		Draw();
		AttachInternal();
	}
	public function get Bold ():Boolean {
		return bold;
	}
	public function set Bold (value:Boolean) {
		bold = value;
		Draw();
		AttachInternal();
	}
	
	public function get Italic ():Boolean {
		return italic;
	}
	public function set Italic (value:Boolean) {
		italic = value;
		Draw();
		AttachInternal();
	}
	public function get Underline ():Boolean {
		return underline;
	}
	public function set Underline (value:Boolean) {
		underline = value;
		Draw();
		AttachInternal();
	}
	public function get Font ():String {
		return font;
	}
	public function set Font (value:String) {
		font = value;
		Draw();
		AttachInternal();
	}
	
	function SetColumnOrder(colNum:Number, colNewOrder:Number) : Void {
		var oldPos:Number = columns[colNum].columnOrder ;
		
		for(var i:Number = 0; i < columnsNum; i++) {
			if(colNum == i)
				columns[i].columnOrder = colNewOrder;
			else {
				if(colNewOrder > oldPos) {
					if(columns[i].columnOrder <= colNewOrder && columns[i].columnOrder > oldPos)
						columns[i].columnOrder--;
				}
				else if (colNewOrder < oldPos) {
					if(columns[i].columnOrder >= colNewOrder && columns[i].columnOrder < oldPos)
						columns[i].columnOrder++;
				}
			}
		}
		
		SortColumns();
		Draw();
		AttachInternal();
	}
	
	public function ApplyLayoutFormat(layout:FaceLayoutItem, putNewText:Boolean):Void {		
		var format:TextFormat = new TextFormat();
		this.font = layout.Font;
		this.fontSize = layout.Size;
		this.bold = layout.Bold;
		this.italic = layout.Italic;
		for(var i:Number = 0; i < columnsNum; i++) {
			columns[i].color = layout.Color;
			columns[i].align = layout.Align;
		}
		
		this.X = layout.X;
		this.Y = layout.Y;
		this.Angle = layout.Angle;
		this.EmptyLayoutItem = layout.EmptyLayoutItem;
		var w:Number = layout.Width;
		columns[0].width = columns[3].width = w/8;
		columns[1].width = columns[2].width = 3*w/8;
		
		//trace(this.EmptyLayoutItem + " this.empty.layout.item");
		Draw();
	}
	
	
	function GetColumnOrder(colNum:Number):Number {
		return columns[colNum].columnOrder ;
	}
	
	function SetColumnColor(colNum:Number, colColor:Number) : Void {
		columns[colNum].color = colColor;
		Draw();
		AttachInternal();
	}
	
	function GetColumnColor(colNum:Number):Number {
		return columns[colNum].color ;
	}
	
	public function GetFirstColumn() {
		return GetColumn(0);
	}
	
	public function GetNextColumn(column) {
		return GetColumn(column.columnOrder + 1);
	}
	
	public function GetColumn(index:Number) {
		for (var i:Number = 0; i < columnsNum; ++i) {
			if (columns[i].columnOrder == index)
				return columns[i];
		}
	}
	
	public function GetFirstVisibleColumnColor():Number {
		var column = GetFirstColumn();
		while(column.visible == false) {
			column = GetNextColumn(column);
		}
		return column.color;
	}
	
	function SetColumnAlign(colNum:Number, colAlign:String) : Void {
		columns[colNum].align = colAlign;
		Draw();
		AttachInternal();
	}
	
	public function GetColumnVisible(colNum:Number):Boolean {
		return columns[colNum].visible;
	}
	
	public function SetColumnVisible(colNum:Number, bVisible:Boolean) : Void {
		bVisible = (bVisible == "true" || bVisible == true) ? true : false;
		var num:Number = 0;
		if(bVisible == false)
		{
			if( IsCanHideAnyOne() == false)
				return;
			
			mc[columns[colNum].id].removeTextField();
			
		}
		columns[colNum].visible = bVisible;
		Draw();
		AttachInternal();
	}
	function IsCanHideAnyOne():Boolean {
			
			var num:Number = 0;
			for(var i:Number = 0; i < columnsNum; i++) {
				if(columns[i].visible == true)
					num += 1;
				//trace(num);
			}
			if(num>1)
				return true;
			else 
				return false;
	}
	function GetColumnAlign(colNum:Number):String {
		return columns[colNum].align ;
	}
	
	function SetColumnText(colNum:Number, colText:String) : Void {
		EmptyLayoutItem = undefined;
		LayoutPosition = undefined;
		columns[colNum].text = colText;
		Draw();
		AttachInternal();
	}
	
	function GetColumnText(colNum:Number):String {
		return columns[colNum].text ;
	}
	function AttachInternal() : Void {
		_global.UIController.FramesController.attach(frameLinkageName, false);
	}
	function GetAllText():String {
		var res:String="";
			for(var i:Number = 0; i < columnsNum; i++) {
				if(columns[i].text!=undefined)
					res+=columns[i].text;
			}
		return res;
		
	}
		
	public function get EmptyLayoutItem():Boolean {
		return emptyLayoutItem;
	}
	public function set EmptyLayoutItem(value:Boolean):Void {
		//trace("Set emptylayoutitem:"+value);
		emptyLayoutItem = value;
	}
	
	public function get Visible():Boolean {
		return mc._visible;
	}
	public function set Visible(value:Boolean):Void {
		mc._visible = value;
	}
	
	public function Detach() : Void {
		
		if(GetAllText().length < 1 && EmptyLayoutItem == false) {
			trace("In resurect table Unit");
		EmptyLayoutItem = true;
		ReinitTable(null,true,false,false);
		}
	}
	public function GetFormattedText():String {
		trace('GetFormattedText');
		var result:String = "";
		var t1:String = columns[columnsOrder[0]].text.split(String.fromCharCode(13));
		var t2:String = columns[columnsOrder[1]].text.split(String.fromCharCode(13));
		var t3:String = columns[columnsOrder[2]].text.split(String.fromCharCode(13));
		var t4:String = columns[columnsOrder[3]].text.split(String.fromCharCode(13));

		         
		var rowsCnt:Number = Math.max(t1.length,t2.length);
		rowsCnt = Math.max(rowsCnt,t3.length);
		rowsCnt = Math.max(rowsCnt,t4.length);
		trace('RowsCnt '+rowsCnt);
		for(var i:Number = 0; i < rowsCnt; i++) {
			var hasPrevColumn:Boolean = false;
			if (columns[columnsOrder[0]].visible) {
				hasPrevColumn = true;
				if(t1[i] != undefined)
					result += t1[i];
				else
					result += " ";
			}
			if (columns[columnsOrder[1]].visible) {
				if (hasPrevColumn)
					result += " - ";
				hasPrevColumn = true;
				if(t2[i] != undefined)
					result += t2[i];
				else
					result += " ";
			}
			if (columns[columnsOrder[2]].visible) {
				if (hasPrevColumn)
					result += " - ";
				hasPrevColumn = true;
				if(t3[i]!= undefined)
					result += t3[i];
				else
					result += " ";
			}
			if (columns[columnsOrder[3]].visible) {
				if (hasPrevColumn)
					result += " - ";
				hasPrevColumn = true;
				if(t4[i] != undefined)
					result += t4[i];
			}
			if(i< (rowsCnt-1))
				result +=String.fromCharCode(13);
		}
		trace("result.lastIndexOf(\" -  -  - \") = "+result.lastIndexOf(" -  -  - ") );
		trace("result.length = "+result.length);
		if(result.lastIndexOf(" -  -  - ") == (result.length - 9)){
			result = result.substr(0, result.length-10);
		}

		trace('RESULT IS '+ result);
		return result;		
	}
	
}
