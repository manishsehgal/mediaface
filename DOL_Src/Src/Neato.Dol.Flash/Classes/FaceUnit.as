﻿import mx.behaviors.DepthControl;
import Actions.ActionManager;

class FaceUnit extends Unit {
	private var bgColor:Number;
	private var bgColor2:Number=0;
	private var gradientType:String = "00";
	private var isFilled:Boolean;
	private var contours:Array;
	private var units:Array; 
	private var caption:String = ""; 
	private var dbId:String;
	private var puid:String;

	private var mcContour:MovieClip;
	private var mcCanvas:MovieClip;
	private var mcContourTop:MovieClip;
	private var mcMask:MovieClip;
	
	private var scaleFactor:Number;	
	
	private var isUniversal:Boolean = false;
	
	public function get UseRealContour():Boolean {
		return isUniversal;
	}	
	
	public function get IsUniversal():Boolean {
		return isUniversal;
	}
	public function set IsUniversal(value:Boolean):Void {
		isUniversal = value;
	}
	public function get PUID() : String {
		return puid;
	}
	
	private var actionManager:ActionManager;
	public function get ActManager():ActionManager {
		return actionManager;
	}
	
	private var paperXValue:Number = 0;
	public function get PaperX():Number {
		return paperXValue;
	}

	private var paperYValue:Number = 0;
	public function get PaperY():Number {
		return paperYValue;
	}
	
	private var pluginsService : SAPlugins;

	function FaceUnit(mc:MovieClip, node:XML, parseOnly:Boolean) {
		super(mc, node);
		
		actionManager = new ActionManager(10);
		
		paperXValue = Number(node.attributes.x);
		paperYValue = Number(node.attributes.y);
		X = 0;
		Y = 0;
		if (node.attributes.zoomInd != undefined) {
			this.zoomInd = node.attributes.zoomInd;
		}
		if (node.attributes.scrollX != undefined) {
			_global.MainWindow.DesignArea.SetScrollPosition(Number(node.attributes.scrollX), Number(node.attributes.scrollY));
		}
		
		dbId = String(node.attributes.dbId);
		puid = String(node.attributes.PUID);

		isFilled = false;
		contours = new Array();
		this.mcContour = mc.createEmptyMovieClip("mcContour", mc.getNextHighestDepth());
		this.mcCanvas = mc.createEmptyMovieClip("mcCanvas", mc.getNextHighestDepth());
		for (var i:Number = 0; i<node.childNodes.length; ++i) {
			var childNode:XML = node.childNodes[i];
			var depth:Number = mcCanvas.getNextHighestDepth();
			switch (childNode.nodeName) {
			case "Contour" :
				var mcContourUnit:MovieClip = mcContour.createEmptyMovieClip("ContourUnit"+depth, depth);
				var contourUnit:ContourUnit = new ContourUnit(mcContourUnit, childNode, this);
				contours.push(contourUnit);
				break;
			case "Localization" :
				ParseFaceCaption(childNode);
				break;
			default :
				trace("Unknown unit name! "+childNode.nodeName);
			}
		}
        
        if (parseOnly) return;
		
		ParseXMLUnits(node);

		this.mcMask = mc.createEmptyMovieClip("mcMask", mc.getNextHighestDepth());
		this.mcContourTop = mc.createEmptyMovieClip("mcContourTop", mc.getNextHighestDepth());
		Draw();

		var xF:Number =_global.EditWidth / mc.mcContour._width;
		var yF:Number =_global.EditHeight / mc.mcContour._height;
		scaleFactor = (xF < yF) ? xF : yF;
		scaleFactor *= 0.9;
		
		AdjustPosition();
		
		mcContour.useHandCursor = false;
		_root.pnlMainLayer.content.WorkSpaceClickArea.useHandCursor = false;
		Hide();
		_global.GlobalNotificator.OnComponentLoaded("Frames.Face", this);

		this.pluginsService = SAPlugins.GetInstance();
		pluginsService.RegisterOnModuleAvaliableHandler(this, IsTextEffectModuleAvaliable);
	}
	
	public function ParseXMLUnits(node:XMLNode, templateId:Number) {
		units = new Array();
		var hasTextEffectUnits:Boolean;
		pluginsService.RegisterOnModuleAvaliableHandler(this, IsTextEffectModuleAvaliable);
		for (var i:Number = 0; i<node.childNodes.length; ++i) {
			var childNode:XML = node.childNodes[i];
			if(templateId != undefined) {
				childNode.attributes.isCurrentUnit = 'False';
			}
			ParseXMLUnit(childNode, templateId, hasTextEffectUnits);
		}		
		//if (hasTextEffectUnits) {
		//	pluginsService.IsModuleAvailable("TextEffectModule");
		//}
	}
	
	public function ParseXMLUnit(node:XML, templateId:Number, hasTextEffectUnits:Boolean) : Unit {
		var depth:Number = mcCanvas.getNextHighestDepth();
		var unit = null;
		switch (node.nodeName) {
		case "Text" :
			var mcText:MovieClip = mcCanvas.createEmptyMovieClip("TextUnit"+depth, depth);
			unit = new TextUnit(mcText, node);
			unit.RegisterOnClickHandler(this, onUnitClick);
			units.push(unit);
			break;
		case "TextEffect":
			var mcText:MovieClip = mcCanvas.createEmptyMovieClip("TextUnit"+depth, depth);
			unit = new TextEffectUnit(mcText, node);
			unit.RegisterOnClickHandler(this, onUnitClick);
			//textUnit.RegisterOnConvertHandler(this, NeedToConvertTextEffectUnit);
			units.push(unit);
			hasTextEffectUnits = true;
			break;
		case "Paint" :
			var mcPaint:MovieClip = mcCanvas.createEmptyMovieClip("PaintUnit"+depth, depth);
			unit = new PaintUnit(mcPaint, node);
			unit.RegisterOnClickHandler(this, onUnitClick);
			units.push(unit);
			break;
		case "Image" :
			unit = AddImageUnit(node, templateId);
			break;
		case "Shape" :
		    unit = AddShapeUnit(node, undefined);
			break;
		case "Gradient" :
		    unit = AddGradientUnit(node, undefined);
			break;
		case "Playlist":
			unit = AddPlaylistUnit(node);
			break;
		case "Fill" :
			bgColor = parseInt(node.attributes.color.substr(1), 16);
			if (node.attributes.color2 != undefined)
				bgColor2 = parseInt(node.attributes.color2.substr(1), 16);
			if (node.attributes.gradientType != undefined)
				gradientType = node.attributes.gradientType;
			//_global.tr("### bgColor2 = " + bgColor2 + " GT = " + gradientType);
			isFilled = true;
			break;
		}
		return unit;
	}
	
	public function FindUnitById(id:String) : Unit {
		var index:Number = GetUnitIndex(id);
		return index == -1 ? null : units[index];
	}
	
	public function GetUnitIndex(id:String) : Number {
		for (var index:Number = 0; index < units.length; ++ index)
			if (units[index].id == id)
				return index;
		return -1;
	}
	
	private function IsTextEffectModuleAvaliable(eventObject):Void {
		
			for(var i:Number = 0; i < units.length; ++i) {
				var unit = units[i];
				if(unit instanceof TextEffectUnit) {
					if (eventObject.isAvaliable.toString() == 'false') {
						ConvertEffectToTextUnit(unit);
					}
					else 
						unit.UpdateTextEffect(true);
				}
			}
		
	}
	
	private function NeedToConvertTextEffectUnit(eventObject) {
		ConvertEffectToTextUnit(eventObject.target);
	}
	
	public function GetCurrentUnit():MoveableUnit {
		for(var i:Number = 0; i < units.length; ++i) {
			if (units[i].IsCurrentUnit) {
				return units[i];
			}
		}
		return null;
	}
	
	public function RegisterOnUnitClickHandler(unit:MoveableUnit){
		unit.RegisterOnClickHandler(this, onUnitClick);
	}
	private function onUnitClick(eventObject) {
		var unit:MoveableUnit = eventObject.target;
		trace("unit.getMode()="+unit.getMode());
 		ChangeCurrentUnit(unit);
 	}
	function UpdateSelection(unit:MoveableUnit) {
		if (_global.Project.CurrentUnit != null) {
			_global.UIController.FramesController.attach(unit.frameLinkageName,true);
		}
	}
	function ChangeCurrentUnit(unit:MoveableUnit) {
		ActManager.Focused = false;
 		/*if (_global.Project.CurrentUnit != unit) {
 			if (_global.Project.CurrentUnit != null) {
				_global.Project.CurrentUnit.IsCurrentUnit = false;
				_global.UIController.FramesController.attach(unit.frameLinkageName,true);
 			}
				_global.Mode = unit.getMode();
				trace("onUnitClick: new mode " + _global.Mode);
				_global.Project.CurrentUnit = unit;
				_global.Project.CurrentUnit.IsCurrentUnit = true;
 		}
		else {
			//_global.SelectionFrame.AttachUnit(_global.Project.CurrentUnit);
			//_global.ChangeUnit();	
			_global.Project.CurrentUnit.IsCurrentUnit = true;
			_global.UIController.FramesController.attach(unit.frameLinkageName,false);
			_global.UIController.OnUnitChanged();
			return;
		}*/
		var bIsOtherUnit:Boolean = _global.Project.CurrentUnit != unit || _global.Project.CurrentUnit == null;
		if(bIsOtherUnit == true) {
			_global.Project.CurrentUnit.IsCurrentUnit = false;
			_global.Project.CurrentUnit.Detach();
			_global.UIController.FramesController.detach();
		}
		
		_global.Project.CurrentUnit = unit;
		_global.Project.CurrentUnit.IsCurrentUnit = true;
		_global.Mode = unit.getMode();
		_global.UIController.FramesController.attach(unit.frameLinkageName, false);
		trace("ChangeCurrentUnit::bIsOtherUnit "+bIsOtherUnit);
		if(bIsOtherUnit == true) {
			_global.UIController.OnUnitChanged();
		}
			
		/*var frameParent:MovieClip = _global.SelectionFrame._parent;
		_global.SelectionFrame.DetachUnit() ;
		_global.SelectionFrame = _global.Frames[_global.Project.CurrentUnit.frameLinkageName];
		_global.SelectionFrame.AttachUnit(_global.Project.CurrentUnit);*/
		//_global.ChangeUnit();	
	}
	
	
	private function AddImageUnit(unitNode, templateId:Number) {
		var depth:Number = mcCanvas.getNextHighestDepth();
		var mcImage:MovieClip = mcCanvas.createEmptyMovieClip("ImageUnit"+depth, depth);
		if(templateId != undefined)
			unitNode.attributes.templateId = templateId;
		var imageUnit:MoveableUnit = new ImageUnit(mcImage, unitNode, _global.ProjectImageUrlFormat);
		//_global.tr("unitNode.attributes.isNewImage.toLowerCase() == " + unitNode.attributes.isNewImage.toLowerCase());
		imageUnit.RegisterOnClickHandler(this, onUnitClick);
		imageUnit.RegisterOnAutofitCancelHandler(this, onAutofitCancel);
		units.push(imageUnit);
		return imageUnit;
	}
	
	private function AddPlaylistUnit(unitNode, playlist){
		trace("FaceUnit: AddPlaylistUnit");
		var depth:Number = mcCanvas.getNextHighestDepth();
		var mcPlaylist:MovieClip = mcCanvas.createEmptyMovieClip("PlaylistUnit"+depth, depth);
		var playlistUnit:TableUnit= new TableUnit(mcPlaylist, (unitNode != null?unitNode:playlist),(unitNode != null?false:true));
		//var size:Number = Math.min(mcContour._width, mcContour._height) / 3;
		playlistUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(playlistUnit);
		playlistUnit.Draw();
		//ChangeCurrentUnit(playlistUnit);
		return playlistUnit;
	}
	private function AddNewPlaylist(playlist,bNew:Boolean,bSelect:Boolean){
		trace("FaceUnit: AddNewPlaylistUnit");
		var depth:Number = mcCanvas.getNextHighestDepth();
		if(bSelect == undefined)
			bSelect = true;
		var mcPlaylist:MovieClip = mcCanvas.createEmptyMovieClip("PlaylistUnit"+depth, depth);
		var playlistUnit:TableUnit= new TableUnit(mcPlaylist, playlist,!bNew,bNew);
		if (playlist == null) {
			SetStartPosition(playlistUnit);
		}
		trace("Table unit is "+playlistUnit);
		playlistUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(playlistUnit);
		playlistUnit.Draw();
		if(bSelect){
			trace('AddNewPlaylistUnit in bSelect');
			//_global.Project.CurrentUnit = playlistUnit;
			ChangeCurrentUnit(playlistUnit);
			trace('Global MODE = '+_global.Mode);
		}
		return playlistUnit;
	}
	
	
	public function CreateNewShapeUnit(shapeType){
		trace("FaceUnit: CreateNewShapeUnit");
		var shapeUnit:ShapeUnit = AddShapeUnit(null, shapeType);
		ChangeCurrentUnit(shapeUnit);
		return shapeUnit;
	}
	private function AddShapeUnit(unitNode, shapeType){
		trace("FaceUnit: AddShapeUnit");
		var depth:Number = mcCanvas.getNextHighestDepth();
		var mcShape:MovieClip = mcCanvas.createEmptyMovieClip("ShapeUnit"+depth, depth);
		var size:Number = Math.min(mcContour._width, mcContour._height) / 3;
		var shapeUnit:ShapeUnit = new ShapeUnit(mcShape, unitNode, shapeType, size);
		if (unitNode == null) {
			SetStartPosition(shapeUnit); 
		}
		shapeUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(shapeUnit);
		shapeUnit.Draw();
		return shapeUnit;
	}
	
	public function CreateNewGradientUnit(gradientType){
		trace("FaceUnit: CreateNewGradientUnit");
		var gradientUnit:GradientUnit = AddGradientUnit(null, gradientType);
		ChangeCurrentUnit(gradientUnit);
		return gradientUnit;
	}
	private function AddGradientUnit(unitNode, gradientType){
		trace("FaceUnit: AddGradientUnit");
		var depth:Number = mcCanvas.getNextHighestDepth();
		var mcGradient:MovieClip = mcCanvas.createEmptyMovieClip("GradientUnit"+depth, depth);
		var size:Number = Math.min(mcContour._width, mcContour._height) / 3;
		var gradientUnit:GradientUnit = new GradientUnit(mcGradient, unitNode, gradientType, size);
		if (unitNode == null) {
			SetStartPosition(gradientUnit); 
		}
		gradientUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(gradientUnit);
		gradientUnit.Draw();
		return gradientUnit;
	}
	
	public function DuplicateImageUnit(unitNode) {
		var imageUnit = AddImageUnit(unitNode);
		imageUnit.Angle = 0;
		imageUnit.X = Math.random() * 10;
		imageUnit.Y = Math.random() * 70;
		imageUnit.ScaleX = imageUnit.InitialScale;
		imageUnit.ScaleY = imageUnit.InitialScale;
		imageUnit.Draw();
		// dispatch even to change current unit
		ChangeCurrentUnit(imageUnit);
	}
	public function CreateTextUnitFromEffect(unitNode){
		DeleteCurrentUnit();
		var depth:Number = mcCanvas.getNextHighestDepth();
		var mcText:MovieClip = mcCanvas.createEmptyMovieClip("TextUnit"+depth, depth);
		var textUnit:TextUnit = new TextUnit(mcText, unitNode);
		textUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(textUnit);
		textUnit.ScaleX = 100;
		textUnit.ScaleY = 100;
		ChangeCurrentUnit(textUnit);
		textUnit.Draw();
		break;
	}
	public function CreateEffectUnitFromText(unitNode,effect){
		trace("CreateEffectUnitFromText");
		DeleteCurrentUnit();
		var depth:Number = mcCanvas.getNextHighestDepth();
		var mcEffect:MovieClip = mcCanvas.createEmptyMovieClip("TextUnit"+depth, depth);
		var effectUnit = new TextEffectUnit(mcEffect,unitNode);
		ChangeCurrentUnit(effectUnit);
		effectUnit.SetTextEffect(effect);
		effectUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(effectUnit);
		ChangeCurrentUnit(effectUnit);
		effectUnit.Draw();
		break;
	}
	public function ConvertTextUnitToEffect(unit,effect:String, bDraw:Boolean, bSelect:Boolean):TextEffectUnit {
		
		trace("ConvertTextUnitToEffect");
		var effNode:XML = new XML(effect);
		if(bDraw == undefined)
			bDraw = true;
		if(bSelect == undefined)
			bSelect = true;
		var effect = _global.Effects.CreateTextEffectFromXml(effNode.firstChild);
		
		
		var unitNode:XML = unit.GetXmlNode();
		if(unit instanceof TextEffectUnit) {
			for(var i in unitNode.firstChild.childNodes){
				if(unitNode.firstChild.childNodes[i].nodeName == "TextEffect" || 
				unitNode.firstChild.childNodes[i].nodeName == "Contour")
				unitNode.firstChild.childNodes[i].removeNode();
			}
		}
		DeleteUnit(unit);
						
		var depth:Number = mcCanvas.getNextHighestDepth();
		var mcEffect:MovieClip = mcCanvas.createEmptyMovieClip("TextEffectUnit"+depth, depth);
		var effXmlNode:XMLNode = effNode.firstChild;
		unitNode.appendChild(effXmlNode);
		var effectUnit = new TextEffectUnit(mcEffect,unitNode);
		effectUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(effectUnit);
		if(bSelect)
			_global.Project.CurrentUnit = effectUnit;
		if(bDraw)
			effectUnit.UpdateTextEffect();
		
		return effectUnit;
		
		
	}
	public function ConvertEffectToTextUnit(unit):TextUnit {
		var unitNode:XML = unit.GetXmlNode();
		DeleteUnit(unit);
		var depth:Number = mcCanvas.getNextHighestDepth();
		var mcText:MovieClip = mcCanvas.createEmptyMovieClip("TextUnit"+depth, depth);
		var textUnit = new TextUnit(mcText, unitNode);
		textUnit.Multiline = true;
		textUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(textUnit);
		textUnit.Draw();
		return textUnit;
	}

	public function FindImageUnit(id) {
		for(var index in units) {
			var unit = units[index];
			if (unit.getMode() == _global.ImageEditMode && unit.id == id) {
				return unit;
			}
		}
		return undefined;
	}
	
	function Hide():Void {
		var scrollPos = _global.MainWindow.DesignArea.GetScrollPosition();
		this.hPos = scrollPos.x;
		this.vPos = scrollPos.y;
		mc._visible = false;
	}
	
	function Show():Void {
		mc._visible = true;
		_global.MainWindow.DesignArea.SetScrollPosition(this.hPos, this.vPos);
		AdjustPosition();
	}
	function AdjustPosition() {
		var zoom:Number = this.Zoom;
		var mcDA = _global.MainWindow.DesignArea;
		var scrollPos = mcDA.GetScrollPosition();
		this.hPos = scrollPos.x;
		this.vPos = scrollPos.y;
		this.hMin = scrollPos.minX;
		this.vMin = scrollPos.minY;
		this.hMax = scrollPos.maxX;
		this.vMax = scrollPos.maxY;
		
		mc._parent._xscale = mc._parent._yscale = scaleFactor*zoom*100;
		var rc:Object = new Object();
		if (this.Zoom > 1.0) {
			mc._parent._x = (_global.EditWidth  * zoom - scaleFactor * zoom * mc.mcContour._width ) / 2 - scrollPos.x;
			mc._parent._y = (_global.EditHeight * zoom - scaleFactor * zoom * mc.mcContour._height) / 2 - scrollPos.y;
			rc.w = ((_global.EditWidth + _global.ZoomPanelWidth) / scaleFactor);
			rc.h = (_global.EditHeight / scaleFactor);
		} else {
			mc._parent._x = (_global.EditWidth  - scaleFactor * zoom * mc.mcContour._width ) / 2;
			mc._parent._y = (_global.EditHeight - scaleFactor * zoom * mc.mcContour._height) / 2;
			rc.w = ((_global.EditWidth + _global.ZoomPanelWidth) / (scaleFactor*zoom));
			rc.h = (_global.EditHeight / (scaleFactor*zoom));
		}
		rc.x = (-mc._parent._x / (scaleFactor * zoom));
		rc.y = (-mc._parent._y / (scaleFactor * zoom));
		DrawMask(rc);
		
		var sd:Object = new Object();
		sd.hPageSize = _global.EditWidth;
		sd.hMaxSize  = _global.EditWidth  * this.Zoom;
		sd.vPageSize = _global.EditHeight;
		sd.vMaxSize  = _global.EditHeight * this.Zoom;
		mcDA.SetScrollProperties(sd);
		
		_global.Frames["Frame"].HandlersScale = _global.Frames["FrameEx"].HandlersScale =
		_global.Frames["TableFrame"].HandlersScale = 100 / (zoom*scaleFactor);
	}
	function DrawMask(rc:Object):Void {
		for (var i in contours) {
			var contour = contours[i];
			contour.DrawMask(this.mcMask, rc);
		}
	}
	
	//-------- Zoom ---------------------------
	private var zoomFactors:Array = [0.2, 0.5, 0.7, 1.0, 1.25, 1.5, 1.75, 2.0, 2.5, 3.0, 5.0];
	private var zoomInd:Number = 3;
	private var hPos:Number = 0;
	private var vPos:Number = 0;
	private var hMin:Number = 0;
	private var vMin:Number = 0;
	private var hMax:Number = 0;
	private var vMax:Number = 0;
	function get Zoom() : Number {
		return this.zoomFactors[this.zoomInd];
	}
	function zoomIn() {
		if (this.zoomInd == this.zoomFactors.length-1) return;
		var mcDA = _global.MainWindow.DesignArea;
		var scrollPos = mcDA.GetScrollPosition();
		var xCenterPos:Number = 0.5;
		var yCenterPos:Number = 0.5;
		if (this.Zoom > 1.0) {
			xCenterPos = (scrollPos.x + _global.EditWidth  / 2) / (this.Zoom * _global.EditWidth);
			yCenterPos = (scrollPos.y + _global.EditHeight / 2) / (this.Zoom * _global.EditHeight);
		}
		this.Hide();
		this.zoomInd++;
		if (this.Zoom > 1.0) {
			this.hPos = (xCenterPos * _global.EditWidth  * this.Zoom) - _global.EditWidth  / 2;
			this.vPos = (yCenterPos * _global.EditHeight * this.Zoom) - _global.EditHeight / 2;
		}
		this.Show();
		if(_global.Mode != _global.PaintMode)
			_global.UIController.FramesController.attach(undefined, false);
		AdjustPosition();
	}
	function zoomOut() {
		if (this.zoomInd == 0) return;
		var mcDA = _global.MainWindow.DesignArea;
		var scrollPos = mcDA.GetScrollPosition();
		var xCenterPos:Number = 0.5;
		var yCenterPos:Number = 0.5;
		if (this.Zoom > 1.0) {
			xCenterPos = (scrollPos.x + _global.EditWidth  / 2) / (this.Zoom * _global.EditWidth);
			yCenterPos = (scrollPos.y + _global.EditHeight / 2) / (this.Zoom * _global.EditHeight);
		}
		this.Hide();
		this.zoomInd--;
		if (this.Zoom > 1.0) {
			this.hPos = (xCenterPos * _global.EditWidth  * this.Zoom) - _global.EditWidth  / 2;
			this.vPos = (yCenterPos * _global.EditHeight * this.Zoom) - _global.EditHeight / 2;
		}
		this.Show();
		if(_global.Mode != _global.PaintMode)
			_global.UIController.FramesController.attach(undefined, false);
		AdjustPosition();
	}
	
	public function zoomRestore() {
		this.zoomInd = 3;
		AdjustPosition();
	}
	//--------------------------------------------
	
	public function get ScaleFactor():Number {
		return scaleFactor;
	}
	
	function ChangeUnitCoordinatesUnderTransfer(sourceWidth:Number, sourceHeight:Number):Void {
		var deltaX:Number = (this.Width - sourceWidth) / 2;
		var deltaY:Number = (this.Height - sourceHeight) / 2;
		for(var i:Number = 0; i < units.length; i++) {
			var unit:MoveableUnit = units[i];
			unit.X += deltaX;
			unit.Y += deltaY;
			if (unit.AutofitType != _global.NoneAutofit) {
				//AutofitUnit(unit, unit.AutofitType);//
				if (this.Width != sourceWidth || this.Height != sourceHeight) {
					unit.AutofitType = _global.ThumbailAutofit;
				}
			}
		}		
	}	

	function SmartXMLAppendChild(parentNode:XMLNode, child:XMLNode):Void {
		if (parentNode.childNodes.length<2) {
			parentNode.appendChild(child);
		} else {
			parentNode.insertBefore(child, parentNode.childNodes[1]);
		}
	}
	
	function GetFillXMLNode():XMLNode {
		var node:XMLNode = new XMLNode(1, "Fill");
		//_global.tr("### GetFillXMLNode() bgColor2 = " + bgColor2 + " GT = " + gradientType);
		node.attributes.color = _global.GetColorForXML(bgColor);
		if (gradientType != undefined && gradientType != null && gradientType != "00") {
			node.attributes.gradientType = gradientType;
			node.attributes.color2 = _global.GetColorForXML(bgColor2);
			node.attributes.width = contours[0].contourWidth;
			node.attributes.height= contours[0].contourHeight;
		}
		return node;
	}
	
	function AddUnitNodes(parentNode:XMLNode, smart:Boolean):Void {
		parentNode.attributes.zoomInd = this.zoomInd;
		var mcDA = _global.MainWindow.DesignArea;
		var scrollPos = mcDA.GetScrollPosition();
		parentNode.attributes.scrollX = scrollPos.x;
		parentNode.attributes.scrollY = scrollPos.y;
		
		if (smart == true) {
			for (var i = units.length-1; i>=0; i--) {
				SmartXMLAppendChild(parentNode, units[i].GetXmlNode());
			}
		} else {
			for (var i = 0; i<units.length; i++) {
				parentNode.appendChild(units[i].GetXmlNode());
			}
		}
		if (isFilled) {
			SmartXMLAppendChild(parentNode, GetFillXMLNode());
		}
	}
	
	function Fill(color:Number, color2:Number, GradientType:String, IsFilled:Boolean):Void {
		IsFilled = IsFilled == null || IsFilled == undefined ? true : IsFilled;
		//_global.tr("### FaceUnit.Fill color = "+color + " color2 = " + color2 + " gradientType = '" + GradientType + "'");
		bgColor = color;
		bgColor2 = color2;
		gradientType = GradientType;
		isFilled = true;
  		for (var i in contours) {
  			var contour = contours[i];
  			contour.SetBgAndDraw(isFilled, bgColor, bgColor2, gradientType);
  		}
	}
	
	function RedrawPaintUnits():Void {
		for (var i in units) {
			var unit = units[i];
			var isPaintMode = unit.getMode() == _global.PaintMode || unit.getMode() == _global.PaintEditMode;
			if(isPaintMode) {
				trace("isPaintMode == true");
				unit.Draw();
				unit.mc._xscale = unit.ScaleX;
				unit.mc._yscale = unit.ScaleY;
			}
		}
	}
	
	function Draw():Void {
		var mcContourTop:MovieClip = mc.mcContourTop; 
		for (var i in contours) {
			var contour:ContourUnit = contours[i];
			contour.SetBgAndDraw(isFilled, bgColor, bgColor2, gradientType);
			
			var hasOutline:Boolean = contour.HasOutline();

			if(hasOutline)
				//mcContourTop.lineStyle(6,0x0,20); // thick grey outline
				mcContourTop.lineStyle(0,0x0,100);
			else										
				mcContourTop.lineStyle(0,0,100);
				
			contour.DrawMC(mcContourTop, true);
			
			mcContourTop.lineStyle(0,0,100);
			if(hasOutline)						
				contour.DrawRealContour(mcContourTop);	
			contour.DrawMarkLines(mcContourTop);			
		}
		
		trace("FaceUnit.Draw: draw " + units.length + " units");
		for (var i in units) {
			var unit = units[i];
			unit.Draw();
			unit.mc._xscale = unit.ScaleX;
			unit.mc._yscale = unit.ScaleY;
		}
	}
	
	function DrawMC(movie:MovieClip):Void {
		for (var i in contours) {
			var contour = contours[i];
			contour.SetGradientColor(isFilled, bgColor, bgColor2, gradientType);
			contour.DrawMC(movie);
		}
		var count:Number = units.length;
		for (var j:Number = 0; j<count; ++j) {
			var unit = units[j];
			
			var mcUnit:MovieClip = movie.createEmptyMovieClip("Unit"+j, j);
			mcUnit.unitRef = unit;
			unit.DrawMC(mcUnit);
			mcUnit._x = unit.X;
			mcUnit._y = unit.Y;
			mcUnit._rotation = unit.Angle;
			mcUnit._xscale = unit.ScaleX;
			mcUnit._yscale = unit.ScaleY;
		}
		
		var ctr:MovieClip = movie.createEmptyMovieClip("Contour", movie.getNextHighestDepth());
		
		for (var i in contours) {
			var contour = contours[i];
			contour.DrawMC(ctr, true);
			contour.DrawHoles(movie);
		}
	}
	
	function DrawContourHoles(movie:MovieClip, transparent:Boolean):Void {
		for (var i in contours) {
			var contour = contours[i];
			contour.DrawMC(movie, transparent);
			contour.DrawHoles(movie);
		}
	}
	function DrawContour(movie:MovieClip, transparent:Boolean):Void {
		for (var i in contours) {
			var contour:ContourUnit = contours[i];
			contour.DrawMC(movie, transparent);
		}
	}
	function DrawRealContour(movie:MovieClip):Void {
		for (var i in contours) {
			var contour:ContourUnit = contours[i];
			contour.DrawRealContour(movie);
		}
	}
	function DrawMarkLines(movie:MovieClip):Void {
		for (var i in contours) {
			var contour:ContourUnit = contours[i];
			contour.DrawMarkLines(movie);
		}
	}
	function DrawCutLines(movie:MovieClip):Void {
		for (var i in contours) {
			var contour:ContourUnit = contours[i];
			contour.DrawCutLines(movie);
		}
	}

	public function SetStartPosition(unit):Void {
		var size:Number = Math.min(mcContour._width, mcContour._height) / 3;
		
		var v:Number = vMax == vMin ? 1 : vPos / (vMax - vMin);
		var h:Number = hMax == hMin ? 1 : hPos / (hMax - hMin);
		var cX:Number = 0;
		var cY:Number = 0;
		
		var z:Number = Zoom < 1 ? 1 : Zoom;
		var factor:Number    = 0.50 / z;
		var minFactor:Number = factor;
		var maxFactor:Number = 1 - factor;

		var max:Number = 0;
		var min:Number = 0;
		if (hMax == hMin) {
			cX = mcContour._width / 2;
			max = cX * (1 + factor);
			min = cX * (1 - factor);
		}
		else {
			if (hPos == hMin) {
				max = mcContour._width * minFactor;
				min = 0;
			}
			else if (hPos == hMax) {
				max = mcContour._width;
				var t:Number = mcContour._width * (1 - maxFactor);
				min = mcContour._width - t;
			}
			else {
				cX = mcContour._width * h;
				max = cX * (1 + factor);
				min = cX * (1 - factor);
				if (hPos > hMax / 2) {
					if (max > mcContour._width) {
						min -= max - mcContour._width;
						max = mcContour._width;
					}
				}
				else if (hPos > hMax / 2) {
					if (min < 0) {
						max += - min;
						min = 0;
					}
				}
			}
		}
		var oX:Number = Math.floor(Math.random() * (max - min + 1)) + min;
		
		max = 0;
		min = 0;
		cY = mcContour._height * v;
		if (vMax == vMin) {
			cY = mcContour._height / 2;
			max = cY * (1 + factor);
			min = cY * (1 - factor);
		}
		else {
			if (vPos == vMin) {
				max = mcContour._height * minFactor;
				min = 0;
			}
			else if (vPos == vMax) {
				max = mcContour._height;
				var t:Number = mcContour._height * (1 - maxFactor);
				min = mcContour._height - t;
			}
			else {
				cY = mcContour._height * v;
				max = cY * (1 + factor);
				min = cY * (1 - factor);
				if (vPos > vMax / 2) {
					if (max > mcContour._height) {
						min -= max - mcContour._height;
						max = mcContour._height;
					}
				}
				else if (vPos > vMax / 2) {
					if (min < 0) {
						max += - min;
						min = 0;
					}
				}
			}
		}
		var oY:Number = Math.floor(Math.random() * (max - min + 1)) + min;
		
		unit.X = oX; 
		unit.Y = oY; 
	}	
	function AddText():TextUnit {
		//_global.tr("FaceUnit.AddText");
		var depth:Number = mc.mcCanvas.getNextHighestDepth();
		var mcText:MovieClip = mc.mcCanvas.createEmptyMovieClip("TextUnit"+depth, depth);
		var textUnit:TextUnit = new TextUnit(mcText, null);
		SetStartPosition(textUnit);
		
		textUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(textUnit);
		
		textUnit.Draw();
		return textUnit;
	}
	function CreateNewPaintUnit():PaintUnit {
		trace("FaceUnit.CreateNewPaintUnit");
		var depth:Number = mc.mcCanvas.getNextHighestDepth();
		var mcPaint:MovieClip = mc.mcCanvas.createEmptyMovieClip("PaintUnit"+depth, depth);
		var paintUnit = new PaintUnit(mcPaint, null);
		paintUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(paintUnit);
		
		//ChangeCurrentUnit(paintUnit);
		paintUnit.mc.onMouseDown = paintUnit.PaintUnit_onMouseDown;
		return paintUnit;
	}	
	
	function get IsEmpty() {
		return units.length == 0 && isFilled == false;
	}
	
	function Clear():Void {
		trace("Clear");
		_global.SelectionFrame.DetachUnit();
		_global.Project.CurrentUnit = null;
		isFilled = false;
		gradientType = "00";
		for (var i in units) {
			var unit = units[i];
			unit.mc.removeMovieClip();
			delete unit; 
		}
		units = new Array();
		Draw();
	}

	function DeleteCurrentUnit() {
		trace("FaceUnit:DeleteCurrentUnit");
		if (_global.Project.CurrentUnit == null) return;
		DeleteUnit(_global.Project.CurrentUnit);
		_global.Project.CurrentUnit = null;
		_global.SelectionFrame.DetachUnit();
		
	}
	
	
	function DeleteUnit(deletedUnit) {
		trace("FaceUnit:DeleteUnit");
		for (var i = 0; i<units.length; ++i) {
			var unit = units[i];
			if (unit == deletedUnit) {
				unit.mc.removeMovieClip();
				units.splice(i, 1);
				break;
			}
		}
	}
	
	public function get Id():String {
		return id;
	}
	
	public function get DbId():String {
		return dbId;
	}
	
	function ParseFaceCaption(childNode:XMLNode) : Void {
		var captionSpecificCulture;
		var captionDefaultCulture;
		
		for(var i:Number = 0; i < childNode.firstChild.childNodes.length; i++) {
			var cultureNode = childNode.firstChild.childNodes[i];
			if (cultureNode.attributes.key == _global.culture) {
				captionSpecificCulture = cultureNode.attributes.value;
				}
			if (cultureNode.attributes.key == "") {
				captionDefaultCulture = cultureNode.attributes.value;
				}
		}
			
		if (captionSpecificCulture != undefined) {
			caption = captionSpecificCulture;
		} else if (captionDefaultCulture != undefined) {
			caption = captionDefaultCulture;
		}
	}
	
	public function get Caption():String {
		return caption;
	}
	
	public function ToBackCurrentUnit() : Void {
		if (_global.Project.CurrentUnit != null) {
			DepthControl.sendBackward(_global.Project.CurrentUnit.mc);
 			units.sort(MoveableUnit.order);
 		}
	}
	
	public function ToBackAllCurrentUnit() : Void {
		if (_global.Project.CurrentUnit != null) {
			DepthControl.sendToBack(_global.Project.CurrentUnit.mc);
 			units.sort(MoveableUnit.order);
 		}
	}
	
	public function ToFrontCurrentUnit() : Void {
		if (_global.Project.CurrentUnit != null) {
			DepthControl.bringForward(_global.Project.CurrentUnit.mc);
 			units.sort(MoveableUnit.order);
 		}
	}
	
	public function ToFrontAllCurrentUnit() : Void {
		if (_global.Project.CurrentUnit != null) {
			DepthControl.bringToFront(_global.Project.CurrentUnit.mc);
 			units.sort(MoveableUnit.order);
 		}
	}
	
	public function get Width():Number {
		return mcContour._width;
	}
	
	public function get Height():Number {
		return mcContour._height;
	}

	public function AutofitCurrentUnit(autofitType:String) : Void {
		AutofitUnit(_global.Project.CurrentUnit, autofitType);
	}
	
	public function AutofitUnit(unit:MoveableUnit, autofitType:String) : Void {
		//if(_global.Mode != _global.ImageEditMode && _global.Mode != _global.GradientEditMode)
		//	return;
		unit.AutofitType = _global.NoneAutofit;
			
		unit.ScaleX = 100;
		unit.ScaleY = 100;
		
		var angle = (_global.SelectionFrame.Angle >= 0 ? _global.SelectionFrame.Angle : _global.SelectionFrame.Angle + 360) % 90;
		if(angle != 0) {
			var antiClockwise = angle < 45;
			_global.RotateToRightAngle(antiClockwise);
		}

		var kx:Number = Width / unit.Width;
		var ky:Number = Height / unit.Height;
		
		switch(autofitType) {
			case _global.FitToFaceAutofit:
				if(_global.SelectionFrame.Angle == 90 || _global.SelectionFrame.Angle == -90) {
					var temp:Number = kx;
					kx = ky;
					ky = temp;
				}
			break;
			case _global.HorizontalAutofit:
				ky = kx;
			break;
			case _global.VerticalAutofit:
				kx = ky;
			break;
			case _global.ThumbailAutofit:
				_global.Project.CurrentUnit.ScaleX = _global.Project.CurrentUnit.InitialScale;
				_global.Project.CurrentUnit.ScaleY = _global.Project.CurrentUnit.InitialScale;
				kx = ky = 1;
			break;
		}
		unit.Resize(kx, ky);
		
		var dx:Number = (unit.Width - Width) / 2;
		var dy:Number = (unit.Height - Height) / 2;
		var offsetX:Number = - dx;
		var offsetY:Number = - dy;
		switch(_global.SelectionFrame.Angle) {
			case 90:
				offsetX += unit.Width;
			break;
			case 180:
				offsetX += unit.Width;
				offsetY += unit.Height;
			break;
			case -90:
				offsetY += unit.Height;
			break;
		}
		unit.X = this.mcContour._x + this.mcContour.getBounds().xMin + offsetX;
		unit.Y = this.mcContour._y + this.mcContour.getBounds().yMin + offsetY;
		
		//_global.SelectionFrame.AttachUnit(_global.Project.CurrentUnit);
		_global.UIController.FramesController.attach();
		unit.AutofitType = autofitType;
	}
	
	private function onAutofitCancel(eventObject) {
		var unit = eventObject.target;
		unit.AutofitType = _global.NoneAutofit;
	}
    
	public function AlignCurrentObjectHorizontally() : Void {
		var angleCopy:Number = _global.SelectionFrame.Angle;
		_global.SelectionFrame.Angle = 0;

		var contourBounds:Object = this.mcContour.getBounds(this.mcContour);
		var offset:Number = contourBounds.xMin;

		var contourWidth:Number = contourBounds.xMax - contourBounds.xMin;
		var unitWidth:Number   = _global.Project.CurrentUnit.Width;

		if(_global.Project.CurrentUnit instanceof TextEffectUnit) {
			var bounds:Object = _global.SelectionFrame.frameBorder.getBounds(_global.Project.CurrentUnit.mc);
			unitWidth = bounds.xMax - bounds.xMin;
			offset -= bounds.xMin;
		}

		_global.SelectionFrame.X = offset + (contourWidth - unitWidth) / 2.0;
		_global.SelectionFrame.Angle = angleCopy;
	}
    
	public function AlignCurrentObjectVertically() : Void {
		var angleCopy:Number = _global.SelectionFrame.Angle;
		_global.SelectionFrame.Angle = 0;

		var contourBounds:Object = this.mcContour.getBounds(this.mcContour);
		var offset:Number = contourBounds.yMin;

		var contourHeight:Number = contourBounds.yMax - contourBounds.yMin;
		var unitHeight:Number   = _global.Project.CurrentUnit.Height;

		if(_global.Project.CurrentUnit instanceof TextEffectUnit) {
			var bounds:Object = _global.SelectionFrame.frameBorder.getBounds(_global.Project.CurrentUnit.mc);
			unitHeight = bounds.yMax - bounds.yMin;
			offset -= bounds.yMin;
		}

		_global.SelectionFrame.Y = offset + (contourHeight - unitHeight) / 2.0;
		_global.SelectionFrame.Angle = angleCopy;
	}
    
	private function CalculateBounds
		(targetMovieClip:MovieClip, targetUnit) : Void {

		var bounds = targetMovieClip.getBounds(this.mcCanvas);
		if(_global.Project.CurrentUnit instanceof TextEffectUnit)
			bounds = _global.SelectionFrame.frameBorder.getBounds(this.mcCanvas);
		targetUnit.xMin = bounds.xMin;
		targetUnit.xMax = bounds.xMax;
		targetUnit.yMin = bounds.yMin;
		targetUnit.yMax = bounds.yMax;
	}

	var stickedUnit = null;
	var candidateForStickedUnit = null;
    
	private function StickInit() : Void {
		stickedUnit = null;
		candidateForStickedUnit = null;
		for(var i in units) CalculateBounds(units[i].mc, units[i]);
	}
    
    
	private function IsUnitSuitableForStick(unit):Boolean {
		return (
			((unit instanceof ShapeUnit ||
				unit instanceof ImageUnit ||
				unit instanceof GradientUnit) &&
			(unit.Angle % 90 == 0))
			&&
			((_global.Project.CurrentUnit instanceof ShapeUnit ||
			_global.Project.CurrentUnit instanceof ImageUnit ||
			_global.Project.CurrentUnit instanceof GradientUnit) &&
			(_global.Project.CurrentUnit.Angle % 90 == 0))
		);
	}
    
    public function StickUpCurrentObject() : Void {
        StickInit();

        for(var i in units) {
            var unit = units[i];
            
            if (unit.yMax < _global.Project.CurrentUnit.yMin &&
                _global.Project.CurrentUnit.xMin < unit.xMax &&
                _global.Project.CurrentUnit.xMax > unit.xMin) {
                    if (! IsUnitSuitableForStick(unit)) continue;
                    
                    candidateForStickedUnit = unit;
                    
                    if (stickedUnit == null ||
                    candidateForStickedUnit.yMax > stickedUnit.yMax) {
                        stickedUnit = candidateForStickedUnit;
                    }
            }
        }
        
		var stickObjectMoreDownThenBorder:Boolean = (stickedUnit.yMax > 0);
		var currentObjectMoreDownThenBorder:Boolean = (_global.Project.CurrentUnit.yMin > 0);
		var currentObjectStickToBorder:Boolean = (_global.Project.CurrentUnit.yMin == 0);

    	var contourBounds:Object = this.mcContour.getBounds(this.mcContour);
		var offset:Number = contourBounds.yMin;

		if (stickedUnit != null || currentObjectMoreDownThenBorder) {
		if (stickedUnit != null && (stickObjectMoreDownThenBorder || currentObjectStickToBorder)) {
            _global.SelectionFrame.Y -= (-offset + _global.Project.CurrentUnit.yMin - stickedUnit.yMax);
		} else {
			_global.SelectionFrame.Y -= (-offset + _global.Project.CurrentUnit.yMin);
			}
		}
    }
    
    public function StickLeftCurrentObject() : Void {
        StickInit();

        for(var i in units) {
            var unit = units[i];
            
            if (unit.xMax < _global.Project.CurrentUnit.xMin &&
                _global.Project.CurrentUnit.yMin < unit.yMax &&
                _global.Project.CurrentUnit.yMax > unit.yMin) {
                    if (! IsUnitSuitableForStick(unit)) continue;

                    candidateForStickedUnit = unit;
                    
                    if (stickedUnit == null ||
                    candidateForStickedUnit.xMax > stickedUnit.xMax) {
                        stickedUnit = candidateForStickedUnit;
                    }
            }
        }
        
		var stickObjectMoreRightThenBorder:Boolean = (stickedUnit.xMax > 0);
		var currentObjectMoreRightThenBorder:Boolean = (_global.Project.CurrentUnit.xMin > 0);
		var currentObjectStickToBorder:Boolean = (_global.Project.CurrentUnit.xMin == 0);

    	var contourBounds:Object = this.mcContour.getBounds(this.mcContour);
		var offset:Number = contourBounds.xMin;

		if (stickedUnit != null || currentObjectMoreRightThenBorder) {
		if (stickedUnit != null && (stickObjectMoreRightThenBorder || currentObjectStickToBorder)) {
            _global.SelectionFrame.X -= (-offset + _global.Project.CurrentUnit.xMin - stickedUnit.xMax);
		} else {
			_global.SelectionFrame.X -= (-offset + _global.Project.CurrentUnit.xMin);
			}
		}
    }
    
    public function StickDownCurrentObject() : Void {
        StickInit();

        for(var i in units) {
            var unit = units[i];
            
            if (unit.yMin > _global.Project.CurrentUnit.yMax &&
                _global.Project.CurrentUnit.xMin < unit.xMax &&
                _global.Project.CurrentUnit.xMax > unit.xMin) {
                    if (! IsUnitSuitableForStick(unit)) continue;

                    candidateForStickedUnit = unit;
                    
                    if (stickedUnit == null ||
                    candidateForStickedUnit.yMin < stickedUnit.yMin) {
                        stickedUnit = candidateForStickedUnit;
                    }
            }
        }
        
		var stickObjectMoreUpThenBorder:Boolean = (stickedUnit.yMin < this.Height);
		var currentObjectMoreUpThenBorder:Boolean = (_global.Project.CurrentUnit.yMax < this.Height);
		var currentObjectStickToBorder:Boolean = (_global.Project.CurrentUnit.yMax == this.Height);

    	var contourBounds:Object = this.mcContour.getBounds(this.mcContour);
		var offset:Number = contourBounds.yMin;

		if (stickedUnit != null || currentObjectMoreUpThenBorder) {
		if (stickedUnit != null && (stickObjectMoreUpThenBorder || currentObjectStickToBorder)) {
            _global.SelectionFrame.Y += (offset + stickedUnit.yMin - _global.Project.CurrentUnit.yMax);
		} else {
			_global.SelectionFrame.Y += (offset + this.Height - _global.Project.CurrentUnit.yMax);
			}
		}			
    }
	

    public function StickRightCurrentObject() : Void {
		StickInit();

        for(var i in units) {
            var unit = units[i];
            
            if (unit.xMin > _global.Project.CurrentUnit.xMax &&
                _global.Project.CurrentUnit.yMin < unit.yMax &&
                _global.Project.CurrentUnit.yMax > unit.yMin) {
                    if (! IsUnitSuitableForStick(unit)) continue;

                    candidateForStickedUnit = unit;
                    
                    if (stickedUnit == null ||
                    candidateForStickedUnit.xMin < stickedUnit.xMin) {
                        stickedUnit = candidateForStickedUnit;
                    }
            }
		}

		var stickObjectMoreLeftThenBorder:Boolean = (stickedUnit.xMin < this.Width);
		var currentObjectMoreLeftThenBorder:Boolean = (_global.Project.CurrentUnit.xMax < this.Width);
		var currentObjectStickToBorder:Boolean = (_global.Project.CurrentUnit.xMax == this.Width);

    	var contourBounds:Object = this.mcContour.getBounds(this.mcContour);
		var offset:Number = contourBounds.xMin;

		if (stickedUnit != null || currentObjectMoreLeftThenBorder) {
		if (stickedUnit != null && (stickObjectMoreLeftThenBorder || currentObjectStickToBorder)) {
            _global.SelectionFrame.X += (offset + stickedUnit.xMin - _global.Project.CurrentUnit.xMax);
		} else {
			_global.SelectionFrame.X += (offset + this.Width - _global.Project.CurrentUnit.xMax);
			}
		}
    }
	
	public function get CenterX():Number {
		return (mcContour.getBounds().xMax + mcContour.getBounds().xMin) / 2;	
	}
	
	public function get CenterY():Number {
		return (mcContour.getBounds().yMax + mcContour.getBounds().yMin) / 2;
	}
	
	public function GetBounds():Object {
		return this.mcContour.getBounds(this.mc);
	}
	public function ConvertPlaylistToEffect(effectName:String) : Void {
		if(!(_global.Project.CurrentUnit instanceof TableUnit))
			return;
		var fformat:TextFormat = new TextFormat();
		var playlistUnit = _global.Project.CurrentUnit;
		fformat.font = playlistUnit.Font;
		fformat.italic = playlistUnit.Italic;
		fformat.bold = playlistUnit.Bold;
		fformat.color = playlistUnit.GetFirstVisibleColumnColor();
		
		var size:Number = playlistUnit.Size;
		var width:Number = playlistUnit.UnitWidth;
		var height:Number = playlistUnit.GetUnitHeight();
		var playlistText:String = playlistUnit.GetFormattedText();
		playlistUnit['mc'].visible = false;

		var textUnit = AddText();
		textUnit.Size = size;
		textUnit.Multiline = true;
		textUnit.MultilineWidth = width * 4/3;
		textUnit.ReplaceText(0,0,playlistText,fformat);
		if((effectName == "Circular") || (effectName == "Circular2")){
			textUnit.X = CenterX;
			textUnit.Y = CenterY;
		} else {			
			textUnit.X = playlistUnit.X;
			textUnit.Y = playlistUnit.Y;
		}
		DeleteCurrentUnit();
		ChangeCurrentUnit(textUnit);
		if(effectName != 'Simple'){
			Logic.TextLogic.ConvertTextToEffect(_global.Project.CurrentUnit, effectName);
		}
		
	}
}
