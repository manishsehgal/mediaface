﻿import mx.skins.Border;
import mx.styles.CSSStyleDeclaration;
import mx.core.ext.UIObjectExtensions;
//The border class for drawing rectangular borders.  Does not use skins, draws programmatically.

class RectBorder extends mx.skins.halo.RectBorder {
	static var symbolName:String = "RectBorder";
	static var symbolOwner:Object = RectBorder;
	var className:String = "RectBorder";

	function RectBorder() {
	}
	
	function init(Void):Void {
		super.init();
	}

	function getBorderMetrics(Void):Object {
		return super.getBorderMetrics();
	}

	function drawTrapezium(color, x1, y1, x2, y2, x3, y3, x4, y4) {
		beginFill(color);
		lineStyle(0, color, 0);
		moveTo(x1, y1);
		lineTo(x2, y2);
		lineTo(x3, y3);
		lineTo(x4, y4);
		endFill();
	}
	
	function drawBorder(Void):Void {
		var b:String = getStyle(borderStyleName);
		var bw:Number = getStyle("borderWidth");
		super.drawBorder();
		if (b == "solid" && bw != undefined) {
			var blc:Number = getStyle("borderLeftColor");
			var btc:Number = getStyle("borderTopColor");
			var brc:Number = getStyle("borderRightColor");
			var bbc:Number = getStyle("borderBottomColor");

			var ww:Number = width;
			var hh:Number = height;
			
			drawTrapezium(brc, ww, 0, ww, hh, ww - bw, hh - bw, ww - bw, bw);
			drawTrapezium(bbc, 0, hh, ww, hh, ww - bw, hh - bw, bw, hh - bw);
			drawTrapezium(blc, 0, 0, 0, hh, bw, hh-bw, bw, bw);
			drawTrapezium(btc, 0, 0, ww, 0, ww - bw, bw, bw, bw);
		}
	}
	
	static function classConstruct():Boolean {
		UIObjectExtensions.Extensions();
		_global.styles.rectBorderClass = RectBorder;
		_global.skinRegistry["RectBorder"] = true;
		return true;
	}
	
	static var classConstructed:Boolean = classConstruct();
	static var UIObjectExtensionsDependency = UIObjectExtensions;
}
