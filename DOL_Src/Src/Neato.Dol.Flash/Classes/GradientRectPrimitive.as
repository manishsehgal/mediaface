﻿class GradientRectPrimitive implements IDraw {
	private var dir:String;
	private var points:Number;
	private var w:Number = 100;
	private var h:Number = 100;
	private var primitives:Array;
	private var sX:Number = 1;
	private var sY:Number = 1;
	
	function GradientRectPrimitive(object:Object) {
		if (object instanceof XMLNode) {
			this.dir = object.attributes.dir;
			this.points = Number(object.attributes.points);
		}
		else if (object instanceof GradientRectPrimitive) {
			this.dir = object.dir;
			this.points = object.points;
		}
		if (this.points < 2) this.points = 2;
		if (this.points > 5) this.points = 5;
	}

	public function get Primitives():Array {
		return primitives;
	}
	
	public function set Primitives(value:Array) {
		primitives = value;
	}
	
	public function set ScaleX(value:Number):Void {
		sX = value;
	}
	public function get ScaleX():Number {
		return sX;
	}
	
	public function set ScaleY(value:Number):Void {
		sY = value;
	}
	public function get Width():Number {
		return w;
	}
	public function get Height():Number {
		return h;
	}
	public function get ScaleY():Number {
		return sY;
	}
	
	function Draw(mc:MovieClip, originalX:Number, originalY:Number, rotationAngle:Number, scaleX:Number, scaleY:Number):Void {
		if(scaleX==undefined) scaleX = 1.0;
		if(scaleY==undefined) scaleY = 1.0;
		var absX:Number = originalX * scaleX;
		var absY:Number = originalY * scaleY;
		var absW:Number = this.w * ScaleX;
		var absH:Number = this.h * ScaleY;
		
		var colors:Array = new Array();
		var alphas:Array = new Array();
		var ratios:Array = new Array();
		for (var i:Number = 0; i < this.points; ++ i) {
			colors.push(i%2==0 ? mc.color : mc.color2);
			alphas.push(100);
			ratios.push(255*(i/(this.points-1)));
		}
		
		var angle:Number = 0;
		switch (this.dir) {
		case "L1":
			angle = 0;
			break;
		case "L2":
			angle = 45;
			break;
		case "L3":
			angle = 90;
			break;
		case "L4":
			angle = 135;
			break;
		}
		//var matrix:Object = {matrixType:"box", x:absX, y:absY, w:absW, h:absH, r:angle*Math.PI/180};
		var matrix:flash.geom.Matrix = new flash.geom.Matrix();
		matrix.createGradientBox(absW, absH, angle*Math.PI/180, absX, absY);
		
		mc.beginGradientFill(this.dir.charAt(0)=="R" ? "radial" : "linear", colors, alphas, ratios, matrix);
		for (var i:Number = 0; i < primitives.length; ++i) {
			var primitive:IDraw = primitives[i];
			primitive.Draw(mc, 0, 0, rotationAngle, scaleX, scaleY);
		}
		if (primitives.length == 0 || !(primitives instanceof Array)) {
			mc.moveTo(absX,      absY);
			mc.lineTo(absX+absW, absY);
			mc.lineTo(absX+absW, absY+absH);
			mc.lineTo(absX,      absY+absH);
			mc.lineTo(absX,      absY);
		}
		mc.endFill();
	}
}