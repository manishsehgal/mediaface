﻿_global.GetColorForXML = function (colorNumber:Number):String {
	if (colorNumber == null || colorNumber == undefined) {
		return "#000000";
	}
	
	var resultColor = "";
	var maxColorLength = 6;

	var colorString = colorNumber.toString(16);
	
	if (colorString.indexOf("0x") != -1)
	colorString = colorString.substr(2);
	
	for(var i=1; i<=(maxColorLength-colorString.length); i++) {
		resultColor += "0";
	}
	
	resultColor += colorString;
	
	return "#" + resultColor;
};

_global.ltrim = function(text:String) { 
	var i=-1; 
	while (text.charCodeAt(++i)<33); 
	return text.substring(i); 
}; 

_global.rtrim = function(text:String) { 
	var i = text.length; 
	while (text.charCodeAt(--i)<33); 
	return text.substring(0, ++i); 
}; 
 
_global.trim = function(text:String) { 
	return rtrim(ltrim(text)); 
};

_global.FilterText = function(text:String):String {
	text = text.split("\r").join("");
	text = text.split("\n").join("");
	text = text.split("\t").join(" ");
	text = trim(text);
	return text;
};

//todo: optimize for multiple calls with same angle
_global.RotateAbout0 = function(x:Number, y:Number, angle:Number):Object {
	if (angle == null || angle == 0) return new Object({x:x, y:y});
	 
	var sin = Math.sin(angle);
	var cos = Math.cos(angle); 
	//todo: test optimization for deriving cos from sin:
	//var cos = Math.sqrt(1 - sin*sin);
	//if (angle>Math.PI/2 || angle<-Math.PI/2) cos = -cos;
	var x1 = x * cos - y * sin;
	var y1 = x * sin + y * cos;
	return new Object({x:x1, y:y1});
};

_global.RotateAboutCenter = function(x:Number, y:Number, centerX:Number, centerY:Number, angle:Number):Object {
	if (angle == null || angle == 0) return new Object({x:x, y:y});
	var xRel = x - centerX;
	var yRel = y - centerY;
	var result = RotateAbout0(xRel, yRel, angle);
	result.x = result.x + centerX;
	result.y = result.y + centerY;
	//trace("Rotate: " + x + ", " + y + " (center " + centerX + ", " + centerY + ") -> " + result.x + ", " + result.y);
	return result;
};

_global.Quadrant = function (x:Number,y:Number):Number {
	trace('x = '+x+' ;;; y = '+y);
	if(x>=0 && y>=0)
		return 0;
	if(x<=0 && y>=0)
		return 1;
	if(x<=0 && y<=0)
		return 2;
	if(x>=0 && y<=0)
		return 3;
};

_global.AngleQuadrant= function (Angle:Number):Number {
	var newAngle = Angle % 360;
	if(newAngle >= 0 && newAngle <= 90)	
		return 0;
	if(newAngle >= 90 && newAngle <= 180)	
		return 1;
	if(newAngle >= 180 && newAngle <= 270)	
		return 2;
	if(newAngle >= 270 && newAngle <= 360)	
		return 3;
};
