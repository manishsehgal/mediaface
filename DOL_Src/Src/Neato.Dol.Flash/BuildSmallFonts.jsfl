﻿var fontInfoList = new Array (
//Times New Roman
	{
        	fontName:"Times New Roman",
	fontChars:"Times NwRoan",
	fontText:"Times New Roman",
	bold:false,
        	italic:false
    	},

//Courier New
	{
        	fontName:"Courier New",
	fontChars:"Courie Nw",
	fontText:"Courier New",
	bold:false,
        	italic:false
    	},
  
//Arial
    	{
        	fontName:"Arial",
	fontChars:"Arial",
	fontText:"Arial",
	embedRanges:"5",
	bold:false,
        	italic:false
    	},
//Arial
    	{
        	fontName:"Arial",
	fontChars:"Arial",
	fontText:"Arial",
	embedRanges:"5",
	bold:false,
        	italic:true
    	},

//Arial Narrow
	{
       	fontName:"Arial Narrow",
	fontChars:"Arial Now",
	fontText:"Arial Narrow",
	bold:false,
        	italic:false	
    	},
   
//Arial Black
	{
        	fontName:"Arial Black",
	fontChars:"Arial Bck",
	fontText:"Arial Black",
	bold:false,
        	italic:false		
    	},

//Comic Sans MS
	{
        	fontName:"Comic Sans MS",
	fontChars:"Comic SansM",
	fontText:"Comic Sans MS",
	bold:false,
        	italic:false		
    	},
   
//Impact
	{
        	fontName:"Impact",
	fontChars:"Impact",
	fontText:"Impact",
	bold:false,
        	italic:false		
    	},

//Monotype Corsiva
	{
        	fontName:"Monotype Corsiva",
	fontChars:"Montype Crsiva",
	fontText:"Monotype Corsiva",
	bold:false,
        	italic:false		
    	},
	
//Symbol
	{
        	fontName:"Symbol",
//	fontChars:"Symbol",
//	fontText:"Symbol",
	fontChars:"\"$'^åÕ",
	fontText:"\"$'^åÕ",
	bold:false,
        	italic:false
    	},
	
//Bookman Old Style
	{
        	fontName:"Bookman Old Style",
	fontChars:"Bokman OldStye",
	fontText:"Bookman Old Style",
	bold:false,
        	italic:false		
    	},
   
//Century Schoolbook
    	{
        	fontName:"Century Schoolbook",
	fontChars:"Century Scholbk",
	fontText:"Century Schoolbook",
	bold:false,
        	italic:false		
    	},
   
//Trebuchet MS
    	{
        	fontName:"Trebuchet MS",
	fontChars:"Trebucht MS",
	fontText:"Trebuchet MS",
	bold:false,
        	italic:false	
    	},
  
//Verdana
    	{
        	fontName:"Verdana",
	fontChars:"Verdan",
	fontText:"Verdana",
	bold:false,
        	italic:false
    	},
//Webdings
    	{
        	fontName:"Webdings",
//	fontChars:"Webdings",
//	fontText:"Webdings",
	fontChars:"!èx8m2",
	fontText:"!èx8m2",
	bold:false,
        	italic:false
	}
   
)

var designerDir = "../Neato.Dol.WebDesigner/Flash/Fonts/";
//var testDir = "Flash/Fonts/";
publishSmallFont(fontInfoList,designerDir);
//publishSmallFont(fontInfoList,testDir);

function publishSmallFont(info, dir) {
    var doc = CreateDoc(info);
			
	var symbolName = "smallFont";
	var flaName = symbolName + "_lib.swf";
	var linkageUrl = symbolName + "_lib.swf";

	var lib = doc.library;
	lib.items[0].linkageExportForAS = true;
	lib.items[0].linkageExportForRS = true;
	lib.items[0].linkageURL = linkageUrl;

    	doc.importPublishProfile("file:///FontPublishProfile.xml");
    	doc.currentPublishProfile = "FontPublishProfile";
    	doc.exportSWF("file:///" + dir + flaName, true);
    	doc.close(false);
	
	doc = CreateDoc(info);
	flaName = symbolName + ".swf";

	lib = doc.library;
	lib.items[0].linkageImportForRS = true;
	lib.items[0].linkageURL = "Flash/Fonts/" + linkageUrl;
	
	doc.importPublishProfile("file:///FontPublishProfile.xml");
    	doc.currentPublishProfile = "FontPublishProfile";
    	doc.exportSWF("file:///" + dir + flaName, true);
    	doc.close(false);
}

function CreateDoc(info) {
	var doc = flash.createDocument("timeline");
	doc.setElementProperty('fontRenderingMode', 'standard');
	for(var i in info){
		doc.addNewText({left:0, top:0, right:300, bottom:20});
  	    	doc.setTextString(info[i].fontText);
		var txt = doc.selection[0];
		doc.moveSelectionBy({x:0, y:20});
		txt.textType = "dynamic";
 		txt.setTextAttr("face", info[i].fontName);
		txt.setTextAttr("size", 12);
		txt.setTextAttr("fillColor", 0x0);
	    	txt.setTextAttr("bold", info[i].bold);
        		txt.setTextAttr("italic", info[i].italic);
		if( info[i].embedRanges != undefined)
			txt.embedRanges = info[i].embedRanges;
		else
	    	txt.embeddedCharacters  = info[i].fontChars;
		txt.autoExpand = true;
		txt.fontRenderingMode = "standard";
	}	
	
	doc.convertToSymbol("movie clip", "smallFont", "top left");
	
	return doc;
}

