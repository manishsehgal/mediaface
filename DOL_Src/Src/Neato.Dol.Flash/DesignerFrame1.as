﻿_root.preloader.removeMovieClip(); //remove starter preloader
this._lockroot = true;
stop();

_global._DEBUG = 1;
if (_global._DEBUG > 0) {
	_global.lc_debug = new LocalConnection();
}

_global.tr = function(msg:String):Void {
	if (_global._DEBUG > 0)
		_global.lc_debug.send("lc_debug", "log", msg);
}

tr("DesignerFrame1");

_global.GlobalNotificator = new GlobalNotificator();
_global.UIController = new UIController();

_global.pluginsService = SAPlugins.GetInstance();
SAPlugins.CheckService();

_global.Project = new Entity.Project();
_global.Project.RegisterOnChangedHandler(_global.UIController.MainMenuController, _global.UIController.MainMenuController.UndoRedoMenuDataBind);

_global.FaceUnit = FaceUnit;

//Load configuation Data
_global.fontsXml = new XML();
_global.fontsXml.ignoreWhite = true;

_global.brushesXml = new XML();
_global.brushesXml.ignoreWhite = true;

_global.shapesXml = new XML();
_global.shapesXml.ignoreWhite = true;

_global.gradientsXml = new XML();
_global.gradientsXml.ignoreWhite = true;

_global.effectsXml = new XML();
_global.effectsXml.ignoreWhite = true;

_global.toolsXml = new XML();
_global.toolsXml.ignoreWhite = true;

_global.printersXml = new XML();
_global.printersXml.ignoreWhite = true;

_global.watermarkXml = new XML();
_global.watermarkXml.ignoreWhite = true;

var fontsService:SAFont = SAFont.GetInstance(_global.fontsXml);
fontsService.RegisterOnLoadedHandler(this, onLoaded);

var paintService:SAPaint = SAPaint.GetInstance(_global.brushesXml);
paintService.RegisterOnLoadedHandler(this, onLoaded);

var stylesService:SAStyles = SAStyles.GetInstance();
stylesService.RegisterOnLoadedHandler(this, onLoaded);

var settingsService:SASettings = SASettings.GetInstance();
settingsService.RegisterOnLoadedHandler(this, onLoaded);

var localeService:SALocale = SALocale.GetInstance();
localeService.RegisterOnLoadedHandler(this, onLoaded);

var shapeService:SAShapes = SAShapes.GetInstance(_global.shapesXml);
shapeService.RegisterOnLoadedHandler(this, onLoaded);

var gradientService:SAGradients = SAGradients.GetInstance(_global.gradientsXml);
gradientService.RegisterOnLoadedHandler(this, onLoaded);

var effectService:SATextEffects = SATextEffects.GetInstance(_global.effectsXml);
effectService.RegisterOnLoadedHandler(this, onLoaded);

var toolService:SATools = SATools.GetInstance(_global.toolsXml);
toolService.RegisterOnLoadedHandler(this, onLoaded);

var printersService:SAPrinter = SAPrinter.GetInstance(_global.printersXml);
printersService.RegisterOnLoadedHandler(this, onLoaded);

var watermarkService:SAWatermark = SAWatermark.GetInstance(_global.watermarkXml);
watermarkService.RegisterOnLoadedHandler(this, onLoaded); 

function onLoaded(eventObject) {	
	if (eventObject.target == settingsService) {
		localeService.BeginLoad();
	}
	
	if (fontsService.IsLoaded &&
		stylesService.IsLoaded &&
		paintService.IsLoaded &&
		settingsService.IsLoaded &&
		localeService.IsLoaded &&
		shapeService.IsLoaded &&
		gradientService.IsLoaded &&
		effectService.IsLoaded &&
		toolService.IsLoaded &&
		printersService.IsLoaded &&
		watermarkService.IsLoaded) {
			nextFrame();
		}
}

fontsService.BeginLoad();
paintService.BeginLoad();
stylesService.BeginLoad();
settingsService.BeginLoad();
shapeService.BeginLoad();
gradientService.BeginLoad();
effectService.BeginLoad();
toolService.BeginLoad();
printersService.BeginLoad();
watermarkService.BeginLoad();