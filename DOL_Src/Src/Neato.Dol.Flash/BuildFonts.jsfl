﻿var fontInfoList = new Array (
	//Times New Roman/////////////////////////////////////////////////////////////////////////////////////////////////////
    {
        fontName:"Times New Roman",
        symbolName:"times_n",
        bold:false,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Times New Roman_Curr",
        symbolName:"times_curr_n",
        bold:false,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Times New Roman_Punct",
        symbolName:"times_punct_n",
        bold:false,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Times New Roman",
        symbolName:"times_i",
        bold:false,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Times New Roman_Curr",
        symbolName:"times_curr_i",
        bold:false,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Times New Roman_Punct",
        symbolName:"times_punct_i",
        bold:false,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Times New Roman",
        symbolName:"times_b",
        bold:true,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Times New Roman_Curr",
        symbolName:"times_curr_b",
        bold:true,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Times New Roman_Punct",
        symbolName:"times_punct_b",
        bold:true,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Times New Roman",
        symbolName:"times_bi",
        bold:true,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Times New Roman_Curr",
        symbolName:"times_curr_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Times New Roman_Punct",
        symbolName:"times_punct_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
	//Courier New/////////////////////////////////////////////////////////////////////////////////////////////////////////
    {
        fontName:"Courier New",
        symbolName:"cour_n",
        bold:false,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Courier New_Curr",
        symbolName:"cour_curr_n",
        bold:false,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Courier New_Punct",
        symbolName:"cour_punct_n",
        bold:false,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Courier New",
        symbolName:"cour_i",
        bold:false,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Courier New_Curr",
        symbolName:"cour_curr_i",
        bold:false,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Courier New_Punct",
        symbolName:"cour_punct_i",
        bold:false,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Courier New",
        symbolName:"cour_b",
        bold:true,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Courier New_Curr",
        symbolName:"cour_curr_b",
        bold:true,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Courier New_Punct",
        symbolName:"cour_punct_b",
        bold:true,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Courier New",
        symbolName:"cour_bi",
        bold:true,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Courier New_Curr",
        symbolName:"cour_curr_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Courier New_Punct",
        symbolName:"cour_punct_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
	//Arial///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	{
        fontName:"Arial",
        symbolName:"arial_n",
        bold:false,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Arial_Curr",
        symbolName:"arial_curr_n",
        bold:false,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Arial_Punct",
        symbolName:"arial_punct_n",
        bold:false,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Arial",
        symbolName:"arial_i",
        bold:false,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Arial_Curr",
        symbolName:"arial_curr_i",
        bold:false,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Arial_Punct",
        symbolName:"arial_punct_i",
        bold:false,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Arial",
        symbolName:"arial_b",
        bold:true,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Arial_Curr",
        symbolName:"arial_curr_b",
        bold:true,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Arial_Punct",
        symbolName:"arial_punct_b",
        bold:true,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Arial",
        symbolName:"arial_bi",
        bold:true,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Arial_Curr",
        symbolName:"arial_curr_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Arial_Punct",
        symbolName:"arial_punct_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
	//Arial Narrow////////////////////////////////////////////////////////////////////////////////////////////////////////
	{
        fontName:"Arial Narrow",
        symbolName:"arialn_n",
        bold:false,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Arial Narrow_Curr",
        symbolName:"arialn_curr_n",
        bold:false,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Arial Narrow_Punct",
        symbolName:"arialn_punct_n",
        bold:false,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Arial Narrow",
        symbolName:"arialn_i",
        bold:false,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Arial Narrow_Curr",
        symbolName:"arialn_curr_i",
        bold:false,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Arial Narrow_Punct",
        symbolName:"arialn_punct_i",
        bold:false,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Arial Narrow",
        symbolName:"arialn_b",
        bold:true,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Arial Narrow_Curr",
        symbolName:"arialn_curr_b",
        bold:true,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Arial Narrow_Punct",
        symbolName:"arialn_punct_b",
        bold:true,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Arial Narrow",
        symbolName:"arialn_bi",
        bold:true,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Arial Narrow_Curr",
        symbolName:"arialn_curr_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Arial Narrow_Punct",
        symbolName:"arialn_punct_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
	//Arial Black//////////////////////////////////////////////////////////////////////////////////////////////////////////
	{
        fontName:"Arial Black",
        symbolName:"ariblk_n",
        bold:false,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Arial Black_Curr",
        symbolName:"ariblk_curr_n",
        bold:false,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Arial Black_Punct",
        symbolName:"ariblk_punct_n",
        bold:false,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
	//Comic Sans MS////////////////////////////////////////////////////////////////////////////////////////////////////////
    {
        fontName:"Comic Sans MS",
        symbolName:"comic_n",
        bold:false,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Comic Sans MS_Curr",
        symbolName:"comic_curr_n",
        bold:false,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Comic Sans MS_Punct",
        symbolName:"comic_punct_n",
        bold:false,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Comic Sans MS",
        symbolName:"comic_b",
        bold:true,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Comic Sans MS_Curr",
        symbolName:"comic_curr_b",
        bold:true,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Comic Sans MS_Punct",
        symbolName:"comic_punct_b",
        bold:true,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
	//Impact//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	{
        fontName:"Impact",
        symbolName:"impact_n",
        bold:false,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Impact_Curr",
        symbolName:"impact_curr_n",
        bold:false,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Impact_Punct",
        symbolName:"impact_punct_n",
        bold:false,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
	//Monotype Corsiva////////////////////////////////////////////////////////////////////////////////////////////////////
	{
        fontName:"Monotype Corsiva",
        symbolName:"mtcorsva_n",
        bold:false,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Monotype Corsiva_Curr",
        symbolName:"mtcorsva_curr_n",
        bold:false,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Monotype Corsiva_Punct",
        symbolName:"mtcorsva_punct_n",
        bold:false,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
	//Symbol//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	{
        fontName:"Symbol",
        symbolName:"symbol_n",
        bold:false,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Symbol_Curr",
        symbolName:"symbol_curr_n",
        bold:false,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Symbol_Punct",
        symbolName:"symbol_punct_n",
        bold:false,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
	//Bookman Old Style///////////////////////////////////////////////////////////////////////////////////////////////////
	{
        fontName:"Bookman Old Style",
        symbolName:"bookos_n",
        bold:false,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Bookman Old Style_Curr",
        symbolName:"bookos_curr_n",
        bold:false,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Bookman Old Style_Punct",
        symbolName:"bookos_punct_n",
        bold:false,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Bookman Old Style",
        symbolName:"bookos_i",
        bold:false,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Bookman Old Style_Curr",
        symbolName:"bookos_curr_i",
        bold:false,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Bookman Old Style_Punct",
        symbolName:"bookos_punct_i",
        bold:false,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Bookman Old Style",
        symbolName:"bookos_b",
        bold:true,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Bookman Old Style_Curr",
        symbolName:"bookos_curr_b",
        bold:true,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Bookman Old Style_Punct",
        symbolName:"bookos_punct_b",
        bold:true,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Bookman Old Style",
        symbolName:"bookos_bi",
        bold:true,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Bookman Old Style_Curr",
        symbolName:"bookos_curr_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Bookman Old Style_Punct",
        symbolName:"bookos_punct_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
	//Century Schoolbook//////////////////////////////////////////////////////////////////////////////////////////////////
    {
        fontName:"Century Schoolbook",
        symbolName:"schlbk_n",
        bold:false,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Century Schoolbook_Curr",
        symbolName:"schlbk_curr_n",
        bold:false,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Century Schoolbook_Punct",
        symbolName:"schlbk_punct_n",
        bold:false,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Century Schoolbook",
        symbolName:"schlbk_i",
        bold:false,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Century Schoolbook_Curr",
        symbolName:"schlbk_curr_i",
        bold:false,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Century Schoolbook_Punct",
        symbolName:"schlbk_punct_i",
        bold:false,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Century Schoolbook",
        symbolName:"schlbk_b",
        bold:true,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Century Schoolbook_Curr",
        symbolName:"schlbk_curr_b",
        bold:true,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Century Schoolbook_Punct",
        symbolName:"schlbk_punct_b",
        bold:true,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Century Schoolbook",
        symbolName:"schlbk_bi",
        bold:true,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Century Schoolbook_Curr",
        symbolName:"schlbk_curr_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Century Schoolbook_Punct",
        symbolName:"schlbk_punct_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
	//Trebuchet MS////////////////////////////////////////////////////////////////////////////////////////////////////////
    {
        fontName:"Trebuchet MS",
        symbolName:"trebuc_n",
        bold:false,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Trebuchet MS_Curr",
        symbolName:"trebuc_curr_n",
        bold:false,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Trebuchet MS_Punct",
        symbolName:"trebuc_punct_n",
        bold:false,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Trebuchet MS",
        symbolName:"trebuc_i",
        bold:false,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Trebuchet MS_Curr",
        symbolName:"trebuc_curr_i",
        bold:false,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Trebuchet MS_Punct",
        symbolName:"trebuc_punct_i",
        bold:false,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Trebuchet MS",
        symbolName:"trebuc_b",
        bold:true,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Trebuchet MS_Curr",
        symbolName:"trebuc_curr_b",
        bold:true,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Trebuchet MS_Punct",
        symbolName:"trebuc_punct_b",
        bold:true,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Trebuchet MS",
        symbolName:"trebuc_bi",
        bold:true,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Trebuchet MS_Curr",
        symbolName:"trebuc_curr_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Trebuchet MS_Punct",
        symbolName:"trebuc_punct_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
	//Verdana/////////////////////////////////////////////////////////////////////////////////////////////////////////////
    {
        fontName:"Verdana",
        symbolName:"verdana_n",
        bold:false,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Verdana_Curr",
        symbolName:"verdana_curr_n",
        bold:false,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Verdana_Punct",
        symbolName:"verdana_punct_n",
        bold:false,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Verdana",
        symbolName:"verdana_i",
        bold:false,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Verdana_Curr",
        symbolName:"verdana_curr_i",
        bold:false,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Verdana_Punct",
        symbolName:"verdana_punct_i",
        bold:false,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Verdana",
        symbolName:"verdana_b",
        bold:true,
        italic:false,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Verdana_Curr",
        symbolName:"verdana_curr_b",
        bold:true,
        italic:false,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Verdana_Punct",
        symbolName:"verdana_punct_b",
        bold:true,
        italic:false,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
    {
        fontName:"Verdana",
        symbolName:"verdana_bi",
        bold:true,
        italic:true,
        embeddedCharacters:" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    },
    {
        fontName:"Verdana_Curr",
        symbolName:"verdana_curr_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"$¢£¤¥₣₤₧€"
    },
    {
        fontName:"Verdana_Punct",
        symbolName:"verdana_punct_bi",
        bold:true,
        italic:true,
        embeddedCharacters:"¡¦§©«¬­®°µ¶·»¿"
    },
//Webdings//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    {
        fontName:"Webdings",
        symbolName:"webdings_n",
        bold:false,
        italic:false,
        embedRanges:"5|17"
    }
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
);

var designerDir = "../Neato.Dol.WebDesigner/Flash/Fonts/";
//var testDir = "Flash/Fonts/";

for(var info in fontInfoList){
    publishFont(info, designerDir);
//    publishFont(info, testDir);
}

function publishFont(info, dir) {
    var doc = CreateDoc(info);
			
	var symbolName = fontInfoList[info].symbolName;
	var flaName = symbolName + "_lib.swf";
	var linkageUrl = symbolName + "_lib.swf";

	var lib = doc.library;
	lib.items[0].linkageExportForAS = true;
	lib.items[0].linkageExportForRS = true;
	lib.items[0].linkageURL = linkageUrl;

    	doc.importPublishProfile("file:///FontPublishProfile.xml");
    	doc.currentPublishProfile = "FontPublishProfile";
    	doc.exportSWF("file:///" + dir + flaName, true);
	doc.close(false);
	
	doc = CreateDoc(info);
	flaName = symbolName + ".swf";

	lib = doc.library;
	lib.items[0].linkageImportForRS = true;
	lib.items[0].linkageURL = "Flash/Fonts/" + linkageUrl;

	doc.importPublishProfile("file:///FontPublishProfile.xml");
    	doc.currentPublishProfile = "FontPublishProfile";
	doc.exportSWF("file:///" + dir + flaName, true);
	doc.close(false);
}

function CreateDoc(info) {
	var doc = flash.createDocument("timeline");
	doc.setElementProperty('fontRenderingMode', 'standard');
	
	doc.addNewText({left:0, top:0, right:300, bottom:50});
    	doc.setTextString(fontInfoList[info].fontName);
	var txt = doc.selection[0];
    	txt.textType = "dynamic";
    	txt.setTextAttr("face", fontInfoList[info].fontName);
    	txt.setTextAttr("size", 24);
    	txt.setTextAttr("bold", fontInfoList[info].bold);
    	txt.setTextAttr("italic", fontInfoList[info].italic);
	if(  fontInfoList[info].embedRanges != undefined)
		txt.embedRanges = fontInfoList[info].embedRanges;
	else
	    	txt.embeddedCharacters = fontInfoList[info].embeddedCharacters;
    	txt.autoExpand = true;
    	txt.setTextAttr("fillColor", 0x0);
	txt.fontRenderingMode = "standard";
	
	doc.convertToSymbol("movie clip", fontInfoList[info].symbolName, "top left");
	
	return doc;
}

