using System.Collections;
using System.Data;
using System.IO;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Business {
    public sealed class BCDeviceBrand {
        private BCDeviceBrand() {}

        private static DeviceBrand[] FilterBrands(DeviceBrand[] brands) {
            ArrayList brandList = new ArrayList();
            foreach(DeviceBrand brand in brands) {
                Device[] devices = BCDevice.GetDeviceList(brand);
                bool foundDevice = false;
                foreach (Device device in devices) {
                    PaperBase[] papers = BCPaper.GetPaperList(new DeviceBase(device.Id));
                    if (papers.Length == 0) //"Papers don`t exist.");
                        continue;

                    foundDevice = true;
                    break;
                }

                if (foundDevice)
                    brandList.Add(brand);
            }
            return (DeviceBrand[])brandList.ToArray(typeof(DeviceBrand));
        }

        public static DeviceBrand[] GetDeviceBrandList(DeviceType deviceType) {
            return DODeviceBrand.EnumerateDeviceBrandByParam(deviceType, null);
        }

        public static DeviceBrand[] GetDeviceWithPaperBrandList(DeviceType deviceType) {
            DeviceBrand[] brands = DODeviceBrand.EnumerateDeviceBrandByParam(deviceType, null);
            return FilterBrands(brands);
        }

        public static DeviceBrand[] GetDeviceBrandList(DeviceType deviceType, CarrierBase carrier) {
            return DODeviceBrand.EnumerateDeviceBrandByParam(deviceType, carrier);
        }

        public static DeviceBrand[] GetDeviceWithPaperBrandList(DeviceType deviceType, CarrierBase carrier) {
            DeviceBrand[] brands = DODeviceBrand.EnumerateDeviceBrandByParam(deviceType, carrier);
            return FilterBrands(brands);
        }

        public static DeviceBrand[] GetDeviceBrandList() {
            return DODeviceBrand.EnumerateDeviceBrandByParam(DeviceType.Undefined, null);
        }

        public static byte[] GetDeviceBrandIcon(DeviceBrandBase brand) {
            return DODeviceBrand.GetDeviceBrandIcon(brand);
        }

        public static DeviceBrand NewDeviceBrand() {
            DeviceBrand brand = new DeviceBrand();
            return brand;
        }

        public static void Save(DeviceBrand brand) {
            Save(brand, null);
        }

        public static void Save(DeviceBrand brand, Stream icon) {
            DOGlobal.BeginTransaction();
            try {
                if (brand.IsNew) {
                    DODeviceBrand.Add(brand);
                } else {
                    DODeviceBrand.Update(brand);
                }
                ChangeIcon(brand, icon);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void ChangeIcon(DeviceBrandBase brand, Stream icon) {
            if (icon != null && icon.Length > 0) {
                DOGlobal.BeginTransaction();
                try {
                    DODeviceBrand.UpdateIcon(brand, ConvertHelper.StreamToByteArray(icon));
                    DOGlobal.CommitTransaction();
                } catch (DBConcurrencyException ex) {
                    DOGlobal.RollbackTransaction();
                    throw new ConcurrencyException(ex.Message, ex);
                } catch {
                    DOGlobal.RollbackTransaction();
                    throw;
                }
            }
        }

        public static void Delete(DeviceBrandBase brand) {
            if (brand.IsNew) return;
            Device[] brandDevices = BCDevice.GetDeviceList(brand);
            DOGlobal.BeginTransaction();
            try {
                BCDevice.Delete(brandDevices);
                DODeviceBrand.Delete(brand);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
    }
}