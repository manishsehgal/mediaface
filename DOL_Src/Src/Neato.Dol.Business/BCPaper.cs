using System.Collections;
using System.Data;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Threading;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Business {
    public sealed class BCPaper {
        private BCPaper() {}

        public static PaperBase[] GetPaperList(DeviceBase device) {
            return EnumPaperInternal(null, null, null, device, null, null, null, null, null, -1, -1);
        }

        public static PaperBase[] GetPaperList(Category category) {
            return EnumPaperInternal(null, category, null, null, null, null, null, null, null, -1, -1);
        }
        public static PaperBase[] GetPaperList(PaperBrandBase paperBrandBase) {
            return EnumPaperInternal(paperBrandBase, null, null, null, null, null, null, null, null, -1, -1);
        }

        public static PaperBase[] GetPaperList(PaperBrandBase paperBrandBase, Category category) {
            return EnumPaperInternal(paperBrandBase, category, null, null, null, null, null, null, null, -1, -1);
        }
        public static PaperBase[] GetPaperList(PaperBrandBase paperBrandBase, Category category, DeviceBase device) {
            return EnumPaperInternal(paperBrandBase, category, null, device, null, null, null, null, null, -1, -1);
        }

        public static Paper[] GetPapers(PaperBrandBase paperBrandBase) {
            return GetPapers(paperBrandBase, null, null, null, null, null, null, null, null, -1, -1);
        }

        public static Paper[] GetPapers(PaperBrandBase paperBrandBase, Category category, FaceBase face, DeviceBase device, string paperName, PaperBase paperBase, string paperState, Category prohibitedCategory, PaperMetric paperMetric, int minDeviceTypeId, int maxDeviceTypeId) {
            PaperBase[] paperList = EnumPaperInternal(paperBrandBase, category, face, device, paperName, paperBase, paperState,prohibitedCategory, paperMetric, minDeviceTypeId, maxDeviceTypeId);

            ArrayList papers = new ArrayList(paperList.Length);
            foreach (PaperBase paper in paperList) {
                papers.Add(GetPaper(paper));
            }
            return (Paper[])papers.ToArray(typeof(Paper));
        }

        private static PaperBase[] EnumPaperInternal(PaperBrandBase paperBrand, Category category, FaceBase face, DeviceBase device, string paperName, PaperBase paperBase, string paperState, Category prohibitedCategory, PaperMetric paperMetric, int minDeviceTypeId, int maxDeviceTypeId) {
            string state = null;
            if (paperState != null) {
                state = paperState;
            } else {
                string login = Thread.CurrentPrincipal.Identity.Name;
                SpecialUser[] users = BCSpecialUser.GetSpecialUsers(login, 1);
                if (users.Length > 0) {
                    state = "ait";
                }
            }
            return DOPaper.EnumeratePaperByParam(paperBrand, category, face, device, paperName, paperBase, state, prohibitedCategory, paperMetric, minDeviceTypeId, maxDeviceTypeId);
        }

        public static Paper GetPaper(PaperBase paperBase) {
            return DOPaper.GetPaper(paperBase);
        }

        public static byte[] GetPaperIcon(PaperBase paper) {
            return DOPaper.GetPaperIcon(paper);
        }

        public static byte[] GetPaperIcon(PaperBase paper, int iconWidth, int iconHeight) {
            byte[] image = DOPaper.GetPaperIcon(paper);
            byte[] icon;
            if (image != null && image.Length > 0) {
                icon = ImageHelper.GetIcon(image, iconWidth, iconHeight);
            } else {
                icon = new byte[0];
            }
            return icon;
        }

        public static Paper NewPaper() {
            Paper paper = new Paper();
            return paper;
        }

        public static void Save(Paper paper) {
            Save(paper, null);
        }

        public static void Save(Paper paper, Stream icon) {
            Save(paper, icon, null);
        }

        public static void Save(Paper paper, Stream icon, DeviceBase[] devices) {
            DOGlobal.BeginTransaction();
            try {
                if (paper.IsNew) {
                    DOPaper.Add(paper);
                } else {
                    DOPaper.Update(paper);
                }
                ChangeIcon(paper, icon);
                SynchronizeDevices(paper, devices);
                SynchronizeFaces(paper);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        private static void SynchronizeFaces(Paper paper) {
            SynchronizeFaces(paper, paper.Faces);
        }

        private static void SynchronizeFaces(PaperBase paper, Face[] faces) {
            if (faces == null) return;
            Paper oldPaper = DOPaper.GetPaper(paper);

            UnbindFaces(oldPaper, oldPaper.Faces);
            BindFaces(paper, faces);
        }

        private static void SynchronizeDevices(Paper paper, DeviceBase[] newDevices) {
            if (newDevices == null) return;
            Paper oldPaper = DOPaper.GetPaper(paper);
            Device[] oldDevices = BCDevice.GetDeviceListByPaper(oldPaper);

            UnbindDevices(oldPaper, oldDevices);
            BCFace.Save(paper.Faces);
            BindDevices(paper, newDevices);
        }

        public static void ChangeIcon(PaperBase paper, Stream icon) {
            if (icon == null || icon.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                DOPaper.UpdateIcon(paper, ConvertHelper.StreamToByteArray(icon));
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Delete(PaperBrandBase paperBrand) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Paper paper in GetPapers(paperBrand)) {
                    DeleteWithFaces(paper);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void DeleteWithFaces(Paper paper) {
            DOGlobal.BeginTransaction();
            try {
                Delete(paper);
                BCFace.Delete(paper.Faces);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Delete(PaperBase paper) {
            DOGlobal.BeginTransaction();
            try {
                UnbindFaces(paper);
                DOPaper.Delete(paper);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UnbindFaces(PaperBase paper) {
            UnbindFaces(paper, null);
        }

        public static void UnbindFaces(PaperBase paper, FaceBase[] faces) {
            if (faces != null && faces.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                if (faces == null) {
                    DOFace.UnbindFromPaper(null, paper);
                } else {
                    foreach (FaceBase face in faces) {
                        DOFace.UnbindFromPaper(face, paper);
                    }
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void BindFaces(PaperBase paper, params Face[] faces) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Face face in faces) {
                    DOFace.BindToPaper(face, paper);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void BindDevices(Paper paper, params DeviceBase[] devices) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Face face in paper.Faces) {
                    BCFace.BindDevices(face, devices);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UnbindDevices(Paper paper, DeviceBase[] devices) {
            if (devices != null && devices.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                foreach (Face face in paper.Faces) {
                    if (devices == null) {
                        BCFace.UnbindDevices(face);
                    } else {
                        BCFace.UnbindDevices(face, devices);
                    }
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static XmlDocument EnumeratePaperXmlData() {
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("Library");

            foreach(Category category in BCCategory.GetVisibleCategoryList(false)) {
                foreach(Paper paper in DOPaper.EnumeratePaperByParam(null, category, null, null, string.Empty, null, null, null, null, -1, -1)) {
                    XmlNode node = doc.CreateElement("item");

                    XmlAttribute idAttr = doc.CreateAttribute("id");
                    idAttr.Value = paper.Id.ToString(CultureInfo.InvariantCulture);
                    node.Attributes.Append(idAttr);
                    XmlAttribute nameAttr = doc.CreateAttribute("name");
                    nameAttr.Value = paper.Name.ToString(CultureInfo.InvariantCulture);
                    node.Attributes.Append(nameAttr);

                    root.AppendChild(node);
                }    
            }
            doc.AppendChild(root);
            return doc;
        }

        public static XmlDocument GerPaperFacesXml(PaperBase paper) {
            DeviceType deviceType = DOPaper.GetDeviceType(paper);
            return DOPaper.GerPaperFacesXml(paper, deviceType);
        }

        public static Paper[] GetPaperSkuList(Category category ) {
            return DOPaper.GetPaperSkuList(category);
        }
    }
}
