using Neato.Dol.Data;
using Neato.Dol.Entity;

namespace Neato.Dol.Business {
    public sealed class BCSupportPage {
        private BCSupportPage() {}

        public static LocalizedText[] Enum(string culture) {
            return DOSupportPage.Enum(culture);
        }

        public static LocalizedText Get(string culture) {
            LocalizedText[] list = DOSupportPage.Enum(culture);
            return BCOverviewPage.GetTextFromList(list, culture);
        }

        public static void Update(LocalizedText localizedText) {
            LocalizedText text = Get(localizedText.Culture);
            if (text != null)
                DOSupportPage.Update(localizedText.Culture, localizedText.Text);
            else
                DOSupportPage.Insert(localizedText.Culture, localizedText.Text);
        }

        public static void Delete(string culture) {
            DOSupportPage.Delete(culture);
        }
    }
}