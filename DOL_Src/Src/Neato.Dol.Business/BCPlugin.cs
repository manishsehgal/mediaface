using System.Collections;
using System.IO;
using System.Xml;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Business {
    public sealed class BCPlugin {
        private BCPlugin() {}

        public const string PluginRootFolder = "Plugins";
        public const string PluginDescriptionFile = "desc.xml";
        public const string PluginFileExt = ".txt";
        
        public static string GetFileContent(string fileName, string clientOS) {
            try {
                string path = getFullFileName(fileName, clientOS);
                return DOFileSystem.ReadTextFile(path);;
            }
            catch {
                return "";
            }
        }

        private static string getFullFileName(string fileName, string clientOS) {
            string pluginRootPath = Path.Combine(Configuration.ApplicationRoot, PluginRootFolder);
            pluginRootPath = Path.Combine(pluginRootPath, GetOS(clientOS).ToString());
            return string.Format("{0}{1}", Path.Combine(pluginRootPath, fileName), PluginFileExt);
        }

        private static void setPluginPath(string filePath) {
            string path = Path.Combine(Configuration.ApplicationRoot, PluginRootFolder);
            //path = Path.Combine(path, );
            
            if(!DOFileSystem.DirectoryExist(path))
                DOFileSystem.CreateDirectory(path);

            char[] delimiter = {'\\', '/'};
            string[] tokens = filePath.Split(delimiter);
            int count = tokens.Length;
            for(int i = 0; i < count - 1; ++i) {
                path += string.Format("\\{0}", tokens[i]);
                if(!DOFileSystem.DirectoryExist(path))
                    DOFileSystem.CreateDirectory(path);
            }
        }

        public static void SaveFileContent(string fileName, byte[] data, string clientOS) {
            string cryptString = ConvertHelper.ZipAndBase64String(data);
            byte[] cryptData = ConvertHelper.StringToByteArray(cryptString);
            setPluginPath(fileName);
            string filePath = getFullFileName(fileName, clientOS);
            if(DOFileSystem.FileExist(filePath))
                DOFileSystem.UnsetReadOnlyAttribute(filePath);

            DOFileSystem.WriteFile(cryptData, filePath);
        }

        public static void DeleteFile(string fileName, string clientOS) {
            string filePath = getFullFileName(fileName, clientOS);

            if(DOFileSystem.FileExist(filePath)) {
                DOFileSystem.UnsetReadOnlyAttribute(filePath);
                DOFileSystem.DeleteFile(filePath);
            }
        }

        public static XmlDocument GetPluginDescription(string clientOS) {
            string pluginRootPath = Path.Combine(Configuration.ApplicationRoot, PluginRootFolder);
            pluginRootPath = Path.Combine(pluginRootPath, GetOS(clientOS).ToString());
            string xmlFile = Path.Combine(pluginRootPath, PluginDescriptionFile);
            XmlDocument xmlDoc = new XmlDocument();
            try {
                xmlDoc.Load(xmlFile);
            } catch {}
            
            return xmlDoc;
        }

        public static XmlDocument GetAddittionalFileNames(string clientOS) {
            //Get Modules filelist
            ArrayList moduleList = new ArrayList();
            XmlDocument xmlDesc = GetPluginDescription(clientOS);
            XmlNodeList moduleNodeList = xmlDesc.SelectNodes("//Modules/ModuleInfo");
            foreach (XmlNode node in moduleNodeList)
                moduleList.Add(node.SelectSingleNode("@Filename").Value);
            
            string path = Path.Combine(Configuration.ApplicationRoot, PluginRootFolder);
            path = Path.Combine(path, GetOS(clientOS).ToString());
            int rootPathLength = path.Length+1;
            
            string[] files = DOFileSystem.GetFiles(path, "*"+PluginFileExt, true);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml("<Files />");
            XmlElement root = xmlDoc.DocumentElement;
            foreach (string file in files) {
                string fileName = file.Substring(rootPathLength);
                fileName = fileName.Substring(0, fileName.Length - 4);
                if (moduleList.Contains(fileName))
                    continue;
                
                XmlNode newElem = xmlDoc.CreateNode(XmlNodeType.Element, "File", string.Empty);  
                newElem.InnerText = fileName;

                root.AppendChild(newElem);
            }

            return xmlDoc;
        }
        public static byte[] GetDescFile(string  clientOS) {
            string path = Path.Combine(Configuration.ApplicationRoot, PluginRootFolder);
            path = Path.Combine(path, GetOS(clientOS).ToString());
            path = Path.Combine(path, PluginDescriptionFile);
            if(DOFileSystem.FileExist(path))
                return DOFileSystem.ReadFile(path);
            return new byte[0];
            

        }

        public static ClientOS GetOS(string os) {
            if(os == null)
                return ClientOS.Windows;
            if(os.IndexOf("Windows") > -1)
                return ClientOS.Windows;
            if(os.IndexOf("PPC Mac ") > -1)
                return ClientOS.PowerMac;
            if(os.IndexOf("Intel Mac ") > -1)
                return ClientOS.IntelMac;
            else
                return ClientOS.Windows;
        }

        public static string GetPackageUrl(string  clientOS) {
            ClientOS os = GetOS(clientOS);
            if (os == ClientOS.PowerMac || os ==ClientOS.IntelMac)
                return "download/plugins/Mac/MediaFaceOnlinePluginsPack.pkg.zip";

            return "download/plugins/Win/MediaFaceOnlinePluginsPack.exe";
        }
    }
}