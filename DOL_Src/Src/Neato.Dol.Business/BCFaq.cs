using Neato.Dol.Data;
using Neato.Dol.Data.Scheme;

namespace Neato.Dol.Business {
	public class BCFaq {
		public BCFaq() {}

		public static FaqData EnumFaq() {
			return DOFaq.EnumFaq();
		}

        public static void UpdateFaq(FaqData data) {
            DOGlobal.BeginTransaction();
            try {
                DOFaq.UpdateFaq(data);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
	}
}