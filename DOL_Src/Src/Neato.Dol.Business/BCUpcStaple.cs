using System.Data;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

namespace Neato.Dol.Business {
    public sealed class BCUpcStaple {
        private BCUpcStaple() {}

        public static Upc[] Enum() {
            return DOUpcStaple.Enum();
        }

        public static Upc Get(string code) {
            return DOUpcStaple.Get(code);
        }

        public static void Insert(Upc upc) {
            DOGlobal.BeginTransaction();
            try {
                DOUpcStaple.Insert(upc);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Update(Upc upc) {
            DOGlobal.BeginTransaction();
            try {
                DOUpcStaple.Update(upc);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Delete(Upc upc) {
            DOGlobal.BeginTransaction();
            try {
                DOUpcStaple.Delete(upc);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static bool Validate(string code) {
            return DOUpcStaple.Get(code) != null;
        }
    }
}