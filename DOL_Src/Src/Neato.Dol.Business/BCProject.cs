using System;
using System.Threading;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Xml;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Business {
    public sealed class BCProject {
        private BCProject() {}

        private static string[] ImageFormats = {".jpg", ".jpeg", ".emf", ".exif", ".gif", ".ico", ".png", ".tiff", ".tif", ".wmf", ".bmp"};

        public static Stream Export(Project project) {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            formatter.Serialize(stream, project);
            stream = CryptoHelper.Encrypt(stream);
            return stream;
        }

        public static Project Import(Customer customer, Stream data) {
            IFormatter formatter = new BinaryFormatter();
            Project project = null;
            try {
                Object decryptedData = formatter.Deserialize(CryptoHelper.Decrypt(data));
                if (decryptedData.GetType() == typeof(Neato.Cpt.Entity.Project)) {
                    Neato.Cpt.Entity.Project cptProject = (Neato.Cpt.Entity.Project)decryptedData;
                    project = new Project();
                    project.ProjectXml = cptProject.ProjectXml;
                    project.ProjectPaper = new PaperBase(cptProject.ProjectPaper.Id);
                    FieldInfo fldCpt = cptProject.GetType().GetField("projectImagesValue", BindingFlags.NonPublic | BindingFlags.Instance);
                    FieldInfo fldDol = project.GetType().GetField("projectImagesValue", BindingFlags.NonPublic | BindingFlags.Instance);
                    fldDol.SetValue(project, fldCpt.GetValue(cptProject));
                } else {
                    project = (Project)decryptedData;
                }
                XmlElement doc = project.ProjectXml.DocumentElement;
                doc.SetAttribute("user", Thread.CurrentPrincipal.Identity.Name);

                foreach (XmlNode faceNode in project.ProjectXml.SelectNodes(@"/Project/Faces/Face")) {
                    if (faceNode.Attributes["PUID"] == null) {
                        XmlAttribute a = project.ProjectXml.CreateAttribute("PUID");
                        a.Value = Guid.Empty.ToString();
                        faceNode.Attributes.Append(a);
                    }
                    if (faceNode.Attributes["dbId"] == null) {
                        XmlAttribute a = project.ProjectXml.CreateAttribute("dbId");
                        a.Value = "0";
                        faceNode.Attributes.Append(a);
                    }
                    int faceId = 0;
                    Guid guid = new Guid(faceNode.Attributes["PUID"].Value);
                    try {
                        faceId = BCFace.GetFaceIdByGuid(guid);
                    }
                    catch(BusinessException ex) {
                    }
                    faceNode.Attributes["dbId"].Value = faceId.ToString();
                }

            } catch (CryptographicException ex) {
                throw new BusinessException(BCProjectStrings.FileStructureCorruptedErrorMessage(), ex);
            } catch (SerializationException ex) {
                throw new BusinessException(BCProjectStrings.FileStructureCorruptedErrorMessage(), ex);
            } catch (TargetInvocationException ex) {
                if (ex.InnerException is SerializationException)
                    throw new BusinessException(BCProjectStrings.FileStructureCorruptedErrorMessage(), ex);
                else
                    throw;
            }

            BCCustomer.ChangeLastEditedPaper(customer, project.ProjectPaper);
            return project;
        }

        public static Project GetProject(Customer customer, PaperBase paperBase) {
            BCCustomer.ChangeLastEditedPaper(customer, paperBase);
            Paper paper = DOPaper.GetPaper(paperBase);
            DeviceType deviceType = DOPaper.GetDeviceType(paper);
            return new Project(paper, deviceType);
        }

        public static void RemoveImage(Project project, string id) {
            Guid guid = new Guid(id);
            project.RemoveImage(guid);
        }

        public static string AddImage(Project project, Stream imageData) {
            Image image = Image.FromStream(imageData);
            imageData.Position = 0;
            int width = image.Width;
            int height = image.Height;
            
            byte[] data = ConvertHelper.StreamToByteArray(imageData);
            data = ImageHelper.TruncateImageTo2880Cond(data);
            data = ImageHelper.ConvertImageToJpegCond(data);
            if (IsImageAdditionAllowed(project, data.Length)) {
                XmlNode node = project.AddImage(data, width, height);
                return node == null ? null : node.OuterXml;
            } else {
                return null;
            }
        }

        public static bool AllowedImageExtention(string extention) {
            for (int i = 0; i < ImageFormats.Length; i++) {
                if (ImageFormats[i] == extention) {
                    return true;
                }
            }
            return false;
        }

        public static string AddImageFromLibrary(Project project, int id) {
            byte[] imageArray = BCImageLibrary.GetImage(id);
            using (MemoryStream stream = new MemoryStream(imageArray)) {
                return AddImage(project, stream);
            }
        }

        public static bool IsImageAdditionAllowed(Project project, int imageSize) {
            int maxImageSize = Configuration.MaxProjectSize * 1024 - project.Size;
            bool imageSizeValid = imageSize <= maxImageSize && imageSize > 0;
            return imageSizeValid;
        }
    }
}