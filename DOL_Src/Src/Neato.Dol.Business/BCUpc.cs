using System.Data;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

namespace Neato.Dol.Business {
    public sealed class BCUpc {
        private BCUpc() {}

        public static Upc[] Enum() {
            return DOUpc.Enum();
        }

        public static Upc Get(string code) {
            return DOUpc.Get(code);
        }

        public static void Insert(Upc upc) {
            DOGlobal.BeginTransaction();
            try {
                DOUpc.Insert(upc);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Update(Upc upc) {
            DOGlobal.BeginTransaction();
            try {
                DOUpc.Update(upc);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Delete(Upc upc) {
            DOGlobal.BeginTransaction();
            try {
                DOUpc.Delete(upc);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static bool Validate(string code) {
            return DOUpc.Get(code) != null;
        }
    }
}