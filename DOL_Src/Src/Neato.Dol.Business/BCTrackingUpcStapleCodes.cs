using System;
using System.Data;
using Neato.Dol.Data;

namespace Neato.Dol.Business {
    public sealed class BCTrackingUpcStapleCodes {
        private BCTrackingUpcStapleCodes() {
        }

        public static int Insert(string upc, string user, DateTime time) {
            return DOTrackingUpcStapleCodes.Insert(upc, user, time);
        }

        public static DataSet EnumByTime(DateTime startTime, DateTime endTime) {
            return DOTrackingUpcStapleCodes.EnumByTime(startTime, endTime);
        }
    }
}