using System.Data;

using Neato.Dol.Data;
using Neato.Dol.Entity.DataBase;
using Neato.Dol.Entity.Exceptions;

namespace Neato.Dol.Business {
	public class BCAnnouncement {
		public BCAnnouncement() {}

		public static AnnouncementData EnumAnnouncements() {
			return DOAnnouncement.EnumAnnouncements();
		}

        public static AnnouncementData GetLastAnnouncements() {
            return DOAnnouncement.GetLastAnnouncements();
        }

        public static void UpdateAnnouncement(AnnouncementData data) {
            DOGlobal.BeginTransaction();
            try {
                DOAnnouncement.UpdateAnnouncement(data);
                DOGlobal.CommitTransaction();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
	}
}
