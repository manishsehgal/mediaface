using Neato.Dol.Data;
using Neato.Dol.Data.Scheme;

namespace Neato.Dol.Business {
	public class BCParameters {
		public BCParameters() {}

		public static ParametersData EnumParameters() {
			return DOParameters.EnumParameters();
		}

        public static void UpdateParameters(ParametersData data) {
            DOGlobal.BeginTransaction();
            try {
                DOParameters.UpdateParameters(data);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static string GetParameterByKey(string key) {
            return DOParameters.GetParameterByKey(key);
        }
	}
}