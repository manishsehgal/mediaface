using System;
using System.Collections;
using System.Data;
using System.IO;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Business {
    public class BCFace {
        public BCFace() {}
        public static Face GetFace(FaceBase face) {
            return DOFace.GetFace(face);
        }

        public static Face NewFace() {
            Face face = new Face();
            return face;
        }

        public static void Save(Face face) {
            if (face == null) return;
            Save(new Face[] {face});
        }

        public static void Save(Face[] faces) {
            if (faces == null || faces.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                foreach (Face face in faces) {
                    if (face.IsNew) {
                        DOFace.Add(face);
                    } else {
                        DOFace.Update(face);
                    }
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Save(Face face, Stream icon, DeviceBase[] devices) {
            DOGlobal.BeginTransaction();
            try {
                if (face.IsNew) {
                    DOFace.Add(face);
                } else {
                    DOFace.Update(face);
                }
                ChangeIcon(face, icon);
                SynchronizeDevices(face, devices);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        private static void SynchronizeDevices(Face face, DeviceBase[] devices) {
            if (devices == null) return;
            ArrayList newDevices = new ArrayList(devices);
            ArrayList oldDevices = new ArrayList(BCDevice.GetDeviceList(face));

            ArrayList bindDevices = new ArrayList();
            ArrayList unbindDevices = new ArrayList();

            foreach (DeviceBase device in oldDevices) {
                if (newDevices.IndexOf(device) < 0) {
                    unbindDevices.Add(device);
                }
            }
            foreach (DeviceBase device in devices) {
                if (oldDevices.IndexOf(device) < 0) {
                    bindDevices.Add(device);
                }
            }
            BindDevices(face, (DeviceBase[])bindDevices.ToArray(typeof(DeviceBase)));
            UnbindDevices(face, (DeviceBase[])unbindDevices.ToArray(typeof(DeviceBase)));
        }

        public static void ChangeIcon(FaceBase face, Stream icon) {
            if (icon == null || icon.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                DOFace.UpdateIcon(face, ConvertHelper.StreamToByteArray(icon));
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Delete(FaceBase face) {
            Delete(new FaceBase[] {face});
        }
        public static void Delete(FaceBase[] faces) {
            if (faces == null || faces.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                foreach (FaceBase face in faces) {
                    UnbindPapers(face);
                    UnbindDevices(face);
                    DOFace.Delete(face);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UnbindPapers(FaceBase face) {
            UnbindPapers(face, null);
        }

        public static void UnbindPapers(FaceBase face, PaperBase[] papers) {
            if (papers != null && papers.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                if (papers == null) {
                    DOFace.UnbindFromPaper(face, null);
                } else {
                    foreach (PaperBase paper in papers) {
                        DOFace.UnbindFromPaper(face, paper);
                    }
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UnbindDevices(FaceBase face) {
            UnbindDevices(face, null);
        }

        public static void UnbindDevices(FaceBase face, DeviceBase[] devices) {
            if (devices != null && devices.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                if (devices == null) {
                    DOFace.UnbindFromDevice(face, null);
                } else {
                    foreach (DeviceBase device in devices) {
                        DOFace.UnbindFromDevice(face, device);
                    }
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void BindDevices(FaceBase face, DeviceBase[] devices) {
            DOGlobal.BeginTransaction();
            try {
                foreach (DeviceBase device in devices) {
                    DOFace.BindToDevice(face, device);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
        public static int GetFaceIdByGuid(Guid guid) {
            return DOFace.GetFaceIdByGuid(guid);
        }
        public static bool IsFaceGuidPresentInDB(Guid guid) {
            try {
                DOFace.GetFaceIdByGuid(guid);
                return true;
            }
            catch (BusinessException) {
                return false;
            }
        }

        public static Face[] FaceEnum() {
            return DOFace.FaceEnum();
        }
        public static Face[] EnumerateFaceByParam(DeviceBase device, DeviceType deviceType, string faceName) {
            return DOFace.EnumerateFaceByParam(device, deviceType, faceName);
        }
        public static byte[] GetIcon(FaceBase faceBase) {
            return DOFace.GetIcon(faceBase);
        }

    }
}