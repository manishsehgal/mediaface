using Neato.Dol.Data;
using Neato.Dol.Entity;

namespace Neato.Dol.Business {
    public sealed class BCPaperMetric {
        private BCPaperMetric() {
        }

        public static PaperMetric[] Enum() {
            return DOPaperMetric.Enum();
        }
    }
}