using System.IO;
using System.Xml;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Business {
    public sealed class BCExitPage {
        private BCExitPage() {}
        public const string ExitPageRootFolder = "ExitPage";
        private const string ExitPageFileName = "ExitPage.html";

        private static string getExitPagePath(int retailerId) {
            string exitPageRootPath = Path.Combine(Configuration.ApplicationRoot, ExitPageRootFolder);
            return Path.Combine(exitPageRootPath, retailerId.ToString());
        }

        private static void setExitPagePath(int retailerId) {
            string exitPageRootPath = Path.Combine(Configuration.ApplicationRoot, ExitPageRootFolder);
            string exitPagePath = Path.Combine(exitPageRootPath, retailerId.ToString());
            
            if(!DOFileSystem.DirectoryExist(exitPageRootPath))
                DOFileSystem.CreateDirectory(exitPageRootPath);
            
            if(!DOFileSystem.DirectoryExist(exitPagePath))
                DOFileSystem.CreateDirectory(exitPagePath);
        }

        public static Banner GetExitPage(int retailerId) {
            string filePath = ExitPageUrl(retailerId);
            string src = string.Format("{0}/{1}/{2}", ExitPageRootFolder, retailerId, ExitPageFileName);

            return new Banner(retailerId, DOFileSystem.FileExist(filePath), src, false);
        }

        public static void SaveHtml(int retailerId, string htmlSource) {
            byte[] data = ConvertHelper.StringToByteArray(htmlSource);

            setExitPagePath(retailerId);
            string filePath = ExitPageUrl(retailerId);
            
            if(DOFileSystem.FileExist(filePath))
                DOFileSystem.UnsetReadOnlyAttribute(filePath);

            DOFileSystem.WriteFile(data, filePath);
        }

        public static byte[] LoadHtml(int retailerId) {
            string filePath = ExitPageUrl(retailerId);
            if(DOFileSystem.FileExist(filePath))
                return DOFileSystem.ReadFile(filePath);
            else
                return new byte[0];
        }

        public static XmlDocument GetImageNames(int retailerId) {
            setExitPagePath(retailerId);
            
            string[] files = DOFileSystem.GetFiles(getExitPagePath(retailerId));
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml("<Files />");
            XmlElement root = xmlDoc.DocumentElement;
            foreach (string file in files) {
                string fileName = Path.GetFileName(file);
                if (fileName.ToUpper() == ExitPageFileName.ToUpper())
                    continue;

                XmlNode newElem = xmlDoc.CreateNode(XmlNodeType.Element, "File", string.Empty);  
                newElem.InnerText = fileName;

                root.AppendChild(newElem);
            }

            return xmlDoc;
        }

        public static void SaveImage(int retailerId, string fileName, byte[] data) {
            setExitPagePath(retailerId);

            string filePath = ImageUrl(retailerId, fileName);

            if(DOFileSystem.FileExist(filePath))
                DOFileSystem.UnsetReadOnlyAttribute(filePath);

            DOFileSystem.WriteFile(data, filePath);
        }

        public static void DeleteImage(int retailerId, string fileName) {
            string filePath = ImageUrl(retailerId, fileName);

            if(DOFileSystem.FileExist(filePath)) {
                DOFileSystem.UnsetReadOnlyAttribute(filePath);
                DOFileSystem.DeleteFile(filePath);
            }
        }

        public static string ExitPageUrl(int retailerId) {
            return Path.Combine(getExitPagePath(retailerId), ExitPageFileName);
        }

        public static string ImageUrl(int retailerId, string fileName) {
            return Path.Combine(getExitPagePath(retailerId), fileName);
        }
    }
}