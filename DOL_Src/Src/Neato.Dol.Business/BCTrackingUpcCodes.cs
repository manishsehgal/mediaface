using System;
using System.Data;
using Neato.Dol.Data;

namespace Neato.Dol.Business {
    public sealed class BCTrackingUpcCodes {
        private BCTrackingUpcCodes() {
        }

        public static int Insert(string upc, string user, DateTime time) {
            return DOTrackingUpcCodes.Insert(upc, user, time);
        }

        public static DataSet EnumByTime(DateTime startTime, DateTime endTime) {
            return DOTrackingUpcCodes.EnumByTime(startTime, endTime);
        }
    }
}