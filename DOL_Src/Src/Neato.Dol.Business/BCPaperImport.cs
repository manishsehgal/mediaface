using System;
using System.Collections;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;

using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

namespace Neato.Dol.Business {
    public enum ImportSchema {
        Paper,
        Face,
        MP3Player,
    }

    public class BCPaperImport {
        public BCPaperImport() {}

        private class XmlValidator {
            private XmlSchema schema;
            private XmlValidatingReader validatingReader;
            private ArrayList errorsValue = new ArrayList();

            public string[] Errors {
                get { return (string[])errorsValue.ToArray(typeof(string)); }
            }

            public bool IsDocumentValid {
                get { return errorsValue.Count == 0; }
            }

            public XmlValidator(Stream xsd) {
                this.schema = XmlSchema.Read(xsd, new ValidationEventHandler(ValidationHandler));
            }

            public void Validate(string xmlFragment) {
                XmlDocument doc = new XmlDocument();
                try {
                    doc.LoadXml(xmlFragment);
                    Validate(doc);
                } catch (XmlException ex) {
                    errorsValue.Add(ex.Message);
                }

            }

            public void Validate(XmlDocument document) {
                if (document.DocumentElement != null) {
                    document.DocumentElement.SetAttribute("xmlns", schema.TargetNamespace);
                }

                XmlTextReader docReader = new XmlTextReader(document.OuterXml, XmlNodeType.Document, null);
                validatingReader = new XmlValidatingReader(docReader);

                validatingReader.Schemas.Add(schema);
                validatingReader.ValidationType = ValidationType.Schema;
                validatingReader.ValidationEventHandler += new ValidationEventHandler(ValidationHandler);
                try {
                    while (validatingReader.Read()) {}
                } catch (XmlException ex) {
                    errorsValue.Add(ex.Message);
                }
            }

            private void ValidationHandler(object sender, ValidationEventArgs args) {
                string nodeName = validatingReader.Name;
                if (validatingReader.NodeType == XmlNodeType.Attribute) {
                    string attributeName = validatingReader.Name;
                    validatingReader.MoveToElement();
                    nodeName = validatingReader.Name;
                    validatingReader.MoveToAttribute(attributeName);
                }
                string error = string.Format("Node: {0} - {1}", nodeName, args.Message);
                errorsValue.Add(error);
            }
        }

        private static string GetSchemaFileNameByType(ImportSchema schema) {
            switch (schema) {
                case ImportSchema.Paper : return "Neato.Dol.Business.PaperImportSchema.xsd";
                case ImportSchema.Face : return "Neato.Dol.Business.FaceImportSchema.xsd";
                case ImportSchema.MP3Player : return "Neato.Dol.Business.MP3PlayerSchema.xsd";
            }
            return string.Empty;
        }

        public static string[] Validate(Stream stream, ImportSchema importSchema) {
            stream.Position = 0;
            string xml = string.Empty;
            if (stream != null) {
                StreamReader reader = new StreamReader(stream);
                xml = reader.ReadToEnd();
            }
            return Validate(xml, importSchema);
        }

        public static string[] Validate(string xml, ImportSchema importSchema) {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Stream xsd = assembly.GetManifestResourceStream(GetSchemaFileNameByType(importSchema));
            XmlValidator validator = new XmlValidator(xsd);
            validator.Validate(xml);
            return validator.Errors;
        }

        public static Paper ParsePaper(Stream stream, ImportSchema importSchema) {
            stream.Position = 0;
            string xml = string.Empty;
            if (stream != null) {
                StreamReader reader = new StreamReader(stream);
                xml = reader.ReadToEnd();
            }
            return ParsePaper(xml, importSchema);
        }
 
        public static Paper ParsePaper(string xml, ImportSchema importSchema) {
            string[] errors = BCPaperImport.Validate(xml, importSchema);
            if (errors.Length > 0) throw new BusinessException("Invalid xml document.");
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            Paper paper = BCPaper.NewPaper();
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("n", doc.DocumentElement.GetAttribute("xmlns"));

            XmlNode paperNode = doc.SelectSingleNode("//n:Paper", nsmgr);
            float paperWidth = float.Parse(paperNode.Attributes["w"].Value, CultureInfo.InvariantCulture);
            float paperHeight = float.Parse(paperNode.Attributes["h"].Value, CultureInfo.InvariantCulture);
            paper.Size = new SizeF(paperWidth, paperHeight);
            XmlAttribute paperType = paperNode.Attributes["type"];
            if (paperType != null) {
                try {
                    paper.PaperType = (PaperType)PaperType.Parse(typeof(PaperType),paperType.Value, true);
                } catch {}
            }
            XmlAttribute paperName = paperNode.Attributes["name"];
            if (paperName != null) {
                paper.Name = paperName.Value.ToString().Trim();
            }
            
            foreach (XmlNode faceNode in doc.SelectNodes("//n:Faces/Face", nsmgr)) {
                float faceX = float.Parse(faceNode.Attributes["x"].Value, CultureInfo.InvariantCulture);
                float faceY = float.Parse(faceNode.Attributes["y"].Value, CultureInfo.InvariantCulture);

                XmlAttribute guidAttr = faceNode.Attributes["guid"];
                Guid guid = (guidAttr != null)
                    ? new Guid(guidAttr.Value) : Guid.NewGuid();
                int faceId = (guidAttr != null)
                    ? BCFace.GetFaceIdByGuid(guid) : 0;
                string faceName = faceNode.Attributes["name"]!= null ? faceNode.Attributes["name"].Value : string.Empty;

                Face face = new Face();
                face.Id = faceId;
                face.Guid = guid;
                face.Contour = 
                    (faceNode.FirstChild != null)
                    ? faceNode.FirstChild.Clone()
                    : doc.CreateElement("Contour");
                face.AddName("", faceName);
                paper.AddFace(face, faceX, faceY);
            }
            return paper;
        }
        public static Face ParseFace(Stream stream) {
            stream.Position = 0;
            string xml = string.Empty;
            if (stream != null) {
                StreamReader reader = new StreamReader(stream);
                xml = reader.ReadToEnd();
            }
            return ParseFace(xml);
        }

        public static Face ParseFace(string xml) {
            string[] errors = BCPaperImport.Validate(xml, ImportSchema.Face);
            if (errors.Length > 0) throw new BusinessException("Invalid xml document.");
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("n", doc.DocumentElement.GetAttribute("xmlns"));

            Face face = BCFace.NewFace();

            XmlNode faceNode = doc.SelectSingleNode("//n:Face", nsmgr);

            Guid guid = Guid.NewGuid();
            try {
                if (faceNode.Attributes["guid"] != null)
                guid = new Guid(faceNode.Attributes["guid"].Value);
                face.IsGuidValid = true;
            } catch {
                face.IsGuidValid = false;
            }

            string faceName = faceNode.Attributes["name"]!= null ? faceNode.Attributes["name"].Value : string.Empty;
            face.Id = int.MinValue;
            face.Guid = guid;
            face.Contour = 
                (faceNode.FirstChild != null)
                ? faceNode.FirstChild.Clone()
                : doc.CreateElement("Contour");
            face.AddName("", faceName);
            
            return face;
        }
    }
}