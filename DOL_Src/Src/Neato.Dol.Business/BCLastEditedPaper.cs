using System.Collections;

using Neato.Dol.Data;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity;

namespace Neato.Dol.Business {
    public sealed class BCLastEditedPaper {
        private BCLastEditedPaper() {}

		public static void ChangeLastEditedPaper(Customer customer, PaperBase editedPaper) {
            LastEditedPaperData data = DOLastEditedPaper.
                EnumerateLastEditedPaperByCustomer(customer);

            int MaxLastEditedPaperRows = Configuration.MaxLastEditedPaperRows;

            //Save data to list
            bool addNewPaper = true;
		    ArrayList existPapers = new ArrayList();
		    foreach (LastEditedPaperData.LastEditedPaperRow paperRow in data.LastEditedPaper) {
		        existPapers.Add(paperRow.PaperId);
                if (paperRow.PaperId == editedPaper.Id) {
                    addNewPaper = false;
                }
		    }

            //Add row if need
            if (data.LastEditedPaper.Count < MaxLastEditedPaperRows && addNewPaper) {
                data.LastEditedPaper.AddLastEditedPaperRow(
                    customer.CustomerId, 0);
            }

            //Delete rows if need
            int count = data.LastEditedPaper.Count;
            for (int i = count - 1; i >= 0; i--) {
                if (i >= MaxLastEditedPaperRows) {
                    data.LastEditedPaper[i].Delete();
                }
            }

            //Fill rows
            if (MaxLastEditedPaperRows > 0) {
                data.LastEditedPaper[0].PaperId = editedPaper.Id;
            }
            for (int i = 1; i < MaxLastEditedPaperRows && i <= existPapers.Count; i++) {
                if ((int)existPapers[i-1] != editedPaper.Id) {
                    data.LastEditedPaper[i].PaperId = (int)existPapers[i-1];
                }
            }

            DOLastEditedPaper.UpdateLastEditedPaper(data);
        }

        public static LastEditedPaperData ShowPapersAsDataset(PaperBase[] papers) {
            LastEditedPaperData data = new LastEditedPaperData();
            foreach (PaperBase paper in papers) {
                data.LastEditedPaper.AddLastEditedPaperRow(0, paper.Id);
            }
            return data;
        }
    }
}