using System.Data;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

namespace Neato.Dol.Business {
    public sealed class BCGoogleTracking {
        public const int DefaultId = 1;

        private BCGoogleTracking() {}

        public static string GetCode() {
            string code = DOGoogleCode.GetCode(DefaultId);
            if (code == null)
                code = string.Empty;
            return code;
        }

        public static void SetCode(string code) {
            DOGlobal.BeginTransaction();
            try {
                DOGoogleCode.Update(DefaultId, code);
                DOGlobal.CommitTransaction();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static GoogleTrackPage GetGoogleTrackPage(string pageUrl) {
            GoogleTrackPage trackPage = DOGoogleTrackPage.GetGoogleTrackPage(pageUrl);
            if (trackPage == null)
                trackPage = new GoogleTrackPage(pageUrl, true);
            return trackPage;
        }

        public static void SetGoogleTrackPage(GoogleTrackPage trackPage) {
            DOGlobal.BeginTransaction();
            try {
                if (DOGoogleTrackPage.GetGoogleTrackPage(trackPage.PageUrl) == null)
                    DOGoogleTrackPage.Insert(trackPage);
                else
                    DOGoogleTrackPage.Update(trackPage);
                DOGlobal.CommitTransaction();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
    }
}