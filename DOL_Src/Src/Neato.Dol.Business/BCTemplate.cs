using System;
using System.Data;
using System.IO;
using System.Xml;
using System.Xml.XPath;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Business {
    public class BCTemplate {
        public BCTemplate() {}

        public static Template[] EnumTemplates(FaceBase face) {
            return DOTemplate.EnumTemplates(face);
        }

        public static byte[] GetTemplateIcon(Template template) {
            return DOTemplate.GetTemplateIcon(template);
        }

        public static Template[] EnumerateTemplates(DeviceType deviceType, Face face, DeviceBase device) {
            return DOTemplate.EnumerateTemplates(deviceType, face, device);
        }

        public static void Update(Template template) {
            DOTemplate.Update(template);
        }

        public static void Delete(Template template) {
            DOTemplate.TemplateIconDelete(template);
            DOTemplate.DeleteMediaForTemplate(template);
            DOTemplate.Delete(template);
        }

        public static void Insert(Template template, Stream projectFile, Stream iconFile) {
            
           // DOTemplate.Insert(template);
            DOGlobal.BeginTransaction();
            try 
            {
                Project project = BCProject.Import(null, projectFile);
                
                //XPathNavigator countNavigator = project.ProjectXml.CreateNavigator();
                
                //int faceCount = (int)(double)countNavigator.Evaluate("count(/Project/Faces[Face])");
                
                XmlNodeList nodeList =  project.ProjectXml.SelectNodes("/Project/Faces/Face");
                
                foreach(XmlNode node in nodeList) {
                    //countNavigator = node.CreateNavigator();
                    int cnt = node.ChildNodes.Count;//(int)(double)countNavigator.Evaluate("count([not(Localization|Contour))]");
                    if(cnt<3) // If there are only localization and contour return
                        continue;
                    else {
                        
                        XmlNode nodeId = node.Attributes.GetNamedItem("dbId");
                        Guid faceGuid= Guid.Empty;
                        try {
                            faceGuid = new Guid(node.Attributes.GetNamedItem("PUID").Value) ;
                        }
                        catch{}
                        template.FaceId = int.Parse(nodeId.Value);
                        if(faceGuid != Guid.Empty)
                            template.FaceId = BCFace.GetFaceIdByGuid(faceGuid);
                        template.XmlValue = node.OuterXml;
                        XmlNodeList imageNodes = node.SelectNodes("Image");
                        DOTemplate.Insert(template);
                        foreach(XmlNode imgNode in imageNodes) {
                            XmlNode nodeImgId  = imgNode.Attributes.GetNamedItem("id");
                            Guid guid = new Guid(nodeImgId.Value);
                            DOTemplate.InsertMediaForTemplate(template, guid, project.GetImage(guid));                            
                        }
                        TemplateIconInsert(template, ImageHelper.ConvertImageToJpeg(ConvertHelper.StreamToByteArray(iconFile)));
                        break;
                    }
                }
                
                
                /*
                if(faceCount > 1)
                    throw new BusinessException("The file of the project contains more than one label with  elements");
                else if(faceCount < 1)
                    throw new BusinessException("The file of the project does not contain labels with text elements");*/

                //XmlNode faceNode = project.ProjectXml.SelectSingleNode("/Project/Faces/Face");
              //  faceLayout.Parse(faceNode);

                

                

                
                DOGlobal.CommitTransaction();
            }
            catch (DBConcurrencyException ex) 
            {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch 
            {
                DOGlobal.RollbackTransaction();
                throw;
            }        
        }
        public static void Update(Template template, Stream projectFile, Stream iconFile) 
        {
            
            // DOTemplate.Insert(template);
            DOGlobal.BeginTransaction();
            try 
            {
                if(projectFile.Length < 1) {
                    TemplateIconUpdate(template, ImageHelper.ConvertImageToJpeg(ConvertHelper.StreamToByteArray(iconFile)));
                    DOGlobal.CommitTransaction();
                    return;
                }
                Project project = BCProject.Import(null, projectFile);
                
                //XPathNavigator countNavigator = project.ProjectXml.CreateNavigator();
                
                //int faceCount = (int)(double)countNavigator.Evaluate("count(/Project/Faces[Face])");
                
                XmlNodeList nodeList =  project.ProjectXml.SelectNodes("/Project/Faces/Face");
                
                foreach(XmlNode node in nodeList) 
                {
                    //countNavigator = node.CreateNavigator();
                    int cnt = node.ChildNodes.Count;//(int)(double)countNavigator.Evaluate("count([not(Localization|Contour))]");
                    if(cnt<3) // If there are only localization and contour return
                        continue;
                    else 
                    {
                        Guid id = new Guid(node.SelectSingleNode(@"@PUID").Value);
                        int faceId = BCFace.GetFaceIdByGuid(id);
                        if(faceId != template.FaceId)
                        template.XmlValue = node.OuterXml;
                        XmlNodeList imageNodes = node.SelectNodes("Image");
                        DOTemplate.DeleteMediaForTemplate(template);
                        foreach(XmlNode imgNode in imageNodes) {
                            XmlNode nodeImgId  = imgNode.Attributes.GetNamedItem("id");
                            Guid guid = new Guid(nodeImgId.Value);
                            DOTemplate.InsertMediaForTemplate(template, guid, project.GetImage(guid));                            
                        }
                        DOTemplate.Update(template);
                        if(iconFile.Length > 0)
                        TemplateIconUpdate(template, ImageHelper.ConvertImageToJpeg(ConvertHelper.StreamToByteArray(iconFile)));
                        break;
                    }
                }
                
                
                /*
                if(faceCount > 1)
                    throw new BusinessException("The file of the project contains more than one label with  elements");
                else if(faceCount < 1)
                    throw new BusinessException("The file of the project does not contain labels with text elements");*/

                //XmlNode faceNode = project.ProjectXml.SelectSingleNode("/Project/Faces/Face");
                //  faceLayout.Parse(faceNode);

                

                

                
                DOGlobal.CommitTransaction();
            }
            catch (DBConcurrencyException ex) 
            {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch 
            {
                DOGlobal.RollbackTransaction();
                throw;
            }        
        }

        public static void TemplateIconInsert(Template template, byte[] data) {
            DOTemplate.TemplateIconInsert(template, data);
        }

        public static void TemplateIconUpdate(Template template, byte[] data) {
            DOTemplate.TemplateIconUpdate(template, data);
        }

        public static void TemplateIconDelete(Template template) {
            DOTemplate.TemplateIconDelete(template);
        }

        public static void InsertMediaForTemplate(Template template, Guid guid, byte[] media) {
            DOTemplate.InsertMediaForTemplate(template, guid, media);
        }

        public static byte[] GetMediaByTemplateAndGuid(Template template, Guid guid) {
            return DOTemplate.GetMediaByTemplateAndGuid(template, guid);
        }

        public static void DeleteMediaForTemplate(Template template) {
            DOTemplate.DeleteMediaForTemplate(template);
        }
        public static void Save(Template template, Stream projectFile, Stream iconFile) {
            if(template.Id == 0) {
                Insert(template, projectFile, iconFile);
            } else {
                Update(template, projectFile, iconFile);
            }
       }
        public static XmlDocument GetTemplatesForFace(Face face) {
            Template[] templates = DOTemplate.EnumerateTemplates(DeviceType.Undefined, face, null);
            XmlDocument doc = new XmlDocument();
            XmlNode root = doc.CreateElement("Templates");
            foreach (Template t in templates){

                XmlNode el = doc.CreateElement("Template");//new XmlNode();
                XmlAttribute attrId = doc.CreateAttribute("id");
                XmlAttribute attrFaceId = doc.CreateAttribute("faceId");
                XmlDocumentFragment faceDoc = doc.CreateDocumentFragment();
                faceDoc.InnerXml = t.XmlValue;
                el.AppendChild(faceDoc);
                attrId.Value = t.Id.ToString();
                attrFaceId.Value = t.FaceId.ToString();
                el.Attributes.Append(attrId);
                el.Attributes.Append(attrFaceId);
                root.AppendChild(el);
            }
            doc.AppendChild(root);
            return doc;

        }

    }
}