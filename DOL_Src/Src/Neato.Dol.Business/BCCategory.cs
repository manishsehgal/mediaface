using System.Collections;
using System.Data;
using System.IO;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Business {
    public sealed class BCCategory {
        private BCCategory() {}

        /*public static Category[] GetCategoryListByDeviceType(DeviceType deviceType) {
            return DOCategory.EnumerateCategoryByParam(deviceType, null);
        }*/

        public static Category[] GetCategoryList() {
            return DOCategory.Enumerate();
        }

        public static Category[] GetDeviceCategories() {
            return DOCategory.GetDeviceCategories();
        }
        public static Category[] GetVisibleCategoryList(bool isSpecialUser) {
            Category[] cats = GetCategoryList();
            ArrayList result = new ArrayList();
            if (!isSpecialUser) {
                foreach (Category cat in cats)
                    if(cat.Visible) {
                        result.Add(cat);
                    }
            } else {
                foreach (Category cat in cats)
                    if(cat.Visible || cat.Name.StartsWith("Light")) {
                        result.Add(cat);
                    }
            }

            
            if(result.Count > 0 )
                 return (Category[])result.ToArray(typeof(Category));
            else 
                return new Category[0];
        }

        public static Category GetCategory(CategoryBase Category) {
            return DOCategory.GetCategory(Category);
        }
        
        /*public static Category[] GetCategoryList(DeviceBase device) {
            return DOCategory.EnumerateCategoryByParam(DeviceType.Undefined, device);
        }*/

        public static byte[] GetCategoryIcon(CategoryBase Category) {
            return DOCategory.GetCategoryIcon(Category);
        }

        public static Category NewCategory() {
            Category Category = new Category();
            return Category;
        }

        public static void Save(Category Category) {
            Save(Category, null);
        }

        public static void Save(Category Category, Stream icon) {
            DOGlobal.BeginTransaction();
            try {
                if (Category.IsNew) {
                    DOCategory.Add(Category);
                } else {
                    DOCategory.Update(Category);
                }
                ChangeIcon(Category, icon);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void ChangeIcon(CategoryBase Category, Stream icon) {
            if (icon != null && icon.Length > 0) {
                DOGlobal.BeginTransaction();
                try {
                    DOCategory.UpdateIcon(Category, ConvertHelper.StreamToByteArray(icon));
                    DOGlobal.CommitTransaction();
                } catch (DBConcurrencyException ex) {
                    DOGlobal.RollbackTransaction();
                    throw new ConcurrencyException(ex.Message, ex);
                } catch {
                    DOGlobal.RollbackTransaction();
                    throw;
                }
            }
        }

        public static void Delete(CategoryBase Category) {
            if (Category.IsNew) return;
            DOGlobal.BeginTransaction();
            try {
                DOCategory.Delete(Category);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static string GetName(CategoryBase category) {
            Category [] cats = GetCategoryList();
            foreach (Category cat in cats) {
                if(cat.Id == category.Id) {
                    return cat.Name;
                }
            }
            return "";
        }

        /*public static void RemoveDeviceFromCategory(CategoryBase Category, params Device[] devices) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Device device in devices) {
                    DODevice.RemoveFromCategory(device, Category);
                }
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void AddDeviceToCategory(CategoryBase Category, params Device[] devices) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Device device in devices) {
                    DODevice.AddToCategory(device, Category);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }*/

    }
}