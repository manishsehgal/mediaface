using System;
using System.IO;
using System.Xml;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Business {
    public sealed class BCBanner {
        private BCBanner() {}

        public const string BannerRootFolder = "Banner";
        private const string BannerFileName = "Banner.html";
        private const string BannerSettingsFileName = "Banner.xml";
        private const string SerialHowFind = "SerialHowFind.htm";
        private const string UpcHowFind = "UpcHowFind.htm";
        private const string OldPopUpText = "<div></div>";

        private static string getBannerPath(string page, int bannerId) {
            string bannerRootPath = Path.Combine(Configuration.ApplicationRoot, BannerRootFolder);
            string pagePath = Path.Combine(bannerRootPath, page);
            return Path.Combine(pagePath, bannerId.ToString());
        }

        private static void setBannerPath(string page, int bannerId) {
            string bannerRootPath = Path.Combine(Configuration.ApplicationRoot, BannerRootFolder);
            string pagePath = Path.Combine(bannerRootPath, page);
            string bannerPath = Path.Combine(pagePath, bannerId.ToString());
            
            if(!DOFileSystem.DirectoryExist(bannerRootPath))
                DOFileSystem.CreateDirectory(bannerRootPath);
            
            if(!DOFileSystem.DirectoryExist(pagePath))
                DOFileSystem.CreateDirectory(pagePath);

            if(!DOFileSystem.DirectoryExist(bannerPath))
                DOFileSystem.CreateDirectory(bannerPath);
        }


        public static Banner GetBanner(string page, int bannerId) {
            string filePath = BannerUrl(page, bannerId);
            string src = string.Format("{0}/{1}/{2}/{3}", BannerRootFolder, page, bannerId, BannerFileName);

            return new Banner(bannerId, DOFileSystem.FileExist(filePath), src, BCHiddenBanner.IsBannerHidden(new HiddenBanner(bannerId, page)));
        }

        public static string GetLabelText(string page, int bannerId) {
            return ConvertHelper.ByteArrayToString(LoadHtml(page, bannerId));
        }

        public static string GetPopUpSource(string page, int bannerId) {
            string filePath = BannerUrl(page, bannerId);
            string src = PopUpSrc(bannerId);
            
            if (DOFileSystem.FileExist(filePath)) {
                src = string.Format("{0}/{1}/{2}/{3}", BannerRootFolder, page, bannerId, BannerFileName); 
            }
            return src;                       
        }

        private static string PopUpSrc(int bannerId) {
            string src = string.Empty;

            if (bannerId == 6) {
                src = SerialHowFind;            
            }

            if (bannerId == 7) {
                src = UpcHowFind;
            }

            return src;        
        }

        public static void SaveHtml(string page, string htmlSource, int bannerId) {
            htmlSource = SetPopUpHtml(page, htmlSource, bannerId);
            byte[] data = ConvertHelper.StringToByteArray(htmlSource);

            setBannerPath(page, bannerId);
            string filePath = BannerUrl(page, bannerId);
            
            if(DOFileSystem.FileExist(filePath))
                DOFileSystem.UnsetReadOnlyAttribute(filePath);

            DOFileSystem.WriteFile(data, filePath);
        }

        public static string SetPopUpHtml(string page, string htmlSource, int bannerId) {
            if (page == "CreateAccount" && (bannerId == 6 || bannerId == 7)) {
                
                string fileContent = string.Empty;
            
                string filePath = Path.Combine(Configuration.ApplicationRoot, PopUpSrc(bannerId));
                if (DOFileSystem.FileExist(filePath)) {
                    StreamReader file = new StreamReader(filePath);
                    fileContent = file.ReadToEnd();
                    file.Close();
                    fileContent = fileContent.Replace(OldPopUpText, string.Format("<div>{0}</div>", htmlSource));
                }
                return fileContent;
            }
            return htmlSource;
        }

        public static byte[] LoadPopUpHtml(string filePath) {
            string fileContent = string.Empty;
            string cutFileContent = string.Empty;

            if (DOFileSystem.FileExist(filePath)) {
                StreamReader file = new StreamReader(filePath);
                fileContent = file.ReadToEnd();
                file.Close();
            
                string pTag = "<div>";
                int firstIndex = fileContent.IndexOf(pTag) + pTag.Length;
                int lastIndex = fileContent.LastIndexOf("</div>");

                if (firstIndex >= 0 && lastIndex > 0) {
                    cutFileContent = fileContent.Substring(firstIndex, lastIndex - firstIndex);
                }
            }
            return ConvertHelper.StringToByteArray(cutFileContent);
        }

        public static byte[] LoadHtml(string page, int bannerId) {
            string filePath = BannerUrl(page, bannerId);

            if (page == "CreateAccount" && (bannerId == 6 || bannerId == 7)) {
                return LoadPopUpHtml(filePath);
            }
            else {
                if(DOFileSystem.FileExist(filePath))
                    return DOFileSystem.ReadFile(filePath);
                else
                    return new byte[0];
            }
        }

        public static XmlDocument GetImageNames(string page, int bannerId) {
            setBannerPath(page, bannerId);
            
            string[] files = DOFileSystem.GetFiles(getBannerPath(page, bannerId));
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml("<Files />");
            XmlElement root = xmlDoc.DocumentElement;
            foreach (string file in files) {
                string fileName = Path.GetFileName(file);
                if (fileName.ToUpper() == BannerFileName.ToUpper() ||
                    fileName.ToUpper() == BannerSettingsFileName.ToUpper())
                    continue;

                XmlNode newElem = xmlDoc.CreateNode(XmlNodeType.Element, "File", string.Empty);  
                newElem.InnerText = fileName;

                root.AppendChild(newElem);
            }

            return xmlDoc;
        }

        public static void SaveImage(string page, string fileName, byte[] data, int bannerId) {
            setBannerPath(page, bannerId);

            string filePath = ImageUrl(page, bannerId, fileName);

            if(DOFileSystem.FileExist(filePath))
                DOFileSystem.UnsetReadOnlyAttribute(filePath);

            DOFileSystem.WriteFile(data, filePath);
        }

        public static void DeleteImage(string page, string fileName, int bannerId) {
            string filePath = ImageUrl(page, bannerId, fileName);

            if(DOFileSystem.FileExist(filePath)) {
                DOFileSystem.UnsetReadOnlyAttribute(filePath);
                DOFileSystem.DeleteFile(filePath);
            }
        }

        public static string BannerUrl(string page, int bannerId) {
            return Path.Combine(getBannerPath(page, bannerId), BannerFileName);
        }

        public static string BannerSettingsUrl(string page, int bannerId) {
            return Path.Combine(getBannerPath(page, bannerId), BannerSettingsFileName);
        }

        public static string ImageUrl(string page, int bannerId, string fileName) {
            return Path.Combine(getBannerPath(page, bannerId), fileName);
        }

        public static int GetBannerWidth(string page, int bannerId) {
            string filePath = BannerSettingsUrl(page, bannerId);
            try {
                XmlDocument xmlDoc = DOFileSystem.LoadXml(filePath);
                return int.Parse(xmlDoc.SelectSingleNode("/Banner/@Width").Value);
            }
            catch (Exception ex) {
                return -1;
            }
        }

        public static int GetBannerHeight(string page, int bannerId) {
            string filePath = BannerSettingsUrl(page, bannerId);
            try {
                XmlDocument xmlDoc = DOFileSystem.LoadXml(filePath);
                return int.Parse(xmlDoc.SelectSingleNode("/Banner/@Height").Value);
            }
            catch (Exception ex) {
                return -1;
            }
        }

        public static void SetBannerMetric(string page, int bannerId, string bannerWidth, string bannerHeight) {
            string filePath = BannerSettingsUrl(page, bannerId);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml("<Banner/>");
            xmlDoc.DocumentElement.SetAttribute("Width", bannerWidth);
            xmlDoc.DocumentElement.SetAttribute("Height", bannerHeight);

            DOFileSystem.WriteXml(xmlDoc, filePath);
        }
        
        public static void SaveBannerXml(string page, int bannerId, XmlNode bannerXml) {
            XmlNode textNode = bannerXml.SelectSingleNode(string.Format(@"/{0}/XmlText", page));
            string text = textNode.OuterXml;

            string bannerWidth = bannerXml.SelectSingleNode(@"/*/@Width").Value;
            string bannerHeight = bannerXml.SelectSingleNode(@"/*/@Height").Value;

            SaveHtml(page, text, bannerId);
            SetBannerMetric(page, bannerId, bannerWidth, bannerHeight);
        }

        public static XmlNode LoadBannerXml(string page, int bannerId) {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(string.Format("<{0}/>", page));

            string bannerWidth = GetBannerWidth(page, bannerId).ToString();
            string bannerHeight = GetBannerHeight(page, bannerId).ToString();

            xmlDoc.DocumentElement.SetAttribute("Width", bannerWidth);
            xmlDoc.DocumentElement.SetAttribute("Height", bannerHeight);

            string html = ConvertHelper.ByteArrayToString(LoadHtml(page, bannerId));
            xmlDoc.DocumentElement.InnerXml = html == null ? string.Empty : html;

            return xmlDoc;
        }
    }
}