using System;
using System.Data;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

namespace Neato.Dol.Business {
    public class BCDeviceRequest {
        public BCDeviceRequest() {}

        public static DeviceRequest[] GetPhoneRequests() {
            return DODeviceRequest.GetPhoneRequests();
        }

        public static void InsertPhoneRequest(DeviceRequest deviceRequest, string culture) {
            DOGlobal.BeginTransaction();
            try {
                deviceRequest.Created = BCTimeManager.GetCurrentTime();
                DODeviceRequest.InsertPhoneRequest(deviceRequest);
                BCMail.SendAddNewPhoneMail(deviceRequest, culture);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UpdatePhoneRequest(DeviceRequest deviceRequest) {
            DOGlobal.BeginTransaction();
            try {
                DODeviceRequest.UpdatePhoneRequest(deviceRequest);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void DeletePhoneRequest(int id) {
            DOGlobal.BeginTransaction();
            try {
                DODeviceRequest.DeletePhoneRequest(id);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static DeviceRequest[] GetPhoneRequestsByCarrierPhoneManufacturerPhoneModel(string carrier, bool otherCarrier, string deviceBrand, bool otherDeviceBrand, string device, bool otherDevice,DateTime timeStart, DateTime timeEnd) {
            return DODeviceRequest.GetPhoneRequestsByCarrierPhoneManufacturerPhoneModel(carrier, otherCarrier, deviceBrand, otherDeviceBrand, device, otherDevice,timeStart,timeEnd);
        }

        public static DataSet GetPhoneRequestReportByDatesAndBrand
            (string deviceBrand, bool otherDeviceBrand,
            DateTime startDate, DateTime endDate) {
            return DODeviceRequest.GetPhoneRequestReportByDatesAndBrand
                (deviceBrand, otherDeviceBrand,
                startDate, endDate);
        }

        public static DataSet GetPhoneRequestEmailsReportByDates
            (DateTime startDate, DateTime endDate) {
            return DODeviceRequest.GetPhoneRequestEmailsReportByDates
                (startDate, endDate);
        }
    }
}