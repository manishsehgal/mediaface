using System.Collections;

using Neato.Dol.Data;
using Neato.Dol.Entity;

namespace Neato.Dol.Business {
    public class BCPaperType {
        private static Hashtable table;

        static BCPaperType() {
            table = DOPaperType.Enum();
        }

        public static string GetName(PaperType paperType) {
            string name = table[paperType] as string;
            if (name == null)
                name = string.Empty;
            return name;
        }
    }
}