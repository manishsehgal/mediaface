using System;
using System.Data;
using System.Threading;

using Neato.Dol.Data;
using Neato.Dol.Entity;

namespace Neato.Dol.Business
{
	public class BCTrackingShapes
	{
		public BCTrackingShapes(){}
        public static void Add(string shape, string userHostAddress) 
        {
            string login = Thread.CurrentPrincipal.Identity.Name;
            Add(shape, login, userHostAddress);
        }

        public static void Add(string shape, string login, string userHostAddress) 
        {
            IPAddress userIP = IPAddress.ToIPAddress(userHostAddress);
            if(!BCIPFilter.IsUserContainsInIPFilters(userIP))
                DOTrackingShapes.Add(shape, login, BCTimeManager.GetCurrentTime());
        }

        public static DataSet GetShapesListByDate(DateTime startTime, DateTime endTime) 
        {
            return DOTrackingShapes.GetShapesListByDate( startTime, endTime);
        }

	}
}
