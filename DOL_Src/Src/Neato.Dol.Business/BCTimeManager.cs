using System;

namespace Neato.Dol.Business {
    public class BCTimeManager {
        public BCTimeManager() {}

        public static DateTime GetCurrentTime() {
            return DateTime.Now;
        }
    }
}