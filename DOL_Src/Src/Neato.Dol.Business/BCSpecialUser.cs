using System;
using Neato.Dol.Data;
using System.Collections;
using Neato.Dol.Entity;

namespace Neato.Dol.Business {

	public class BCSpecialUser {
		private BCSpecialUser() {}

        public static SpecialUser[] GetSpecialUsers() {
            return DOSpecialUser.EnumerateSpecialUserByParam(null, -1, -1, -1);
        }

        public static SpecialUser[] GetSpecialUsers(string email) {
            return DOSpecialUser.EnumerateSpecialUserByParam(email, -1, -1, -1);
        }

        public static SpecialUser[] GetSpecialUsers(string email, int showPaper) {
            return DOSpecialUser.EnumerateSpecialUserByParam(email, showPaper, -1, -1);
        }

        public static SpecialUser[] GetSpecialUsersToNotifyPaper() {
            return DOSpecialUser.EnumerateSpecialUserByParam(null, -1, 1, -1);
        }

        public static SpecialUser[] GetSpecialUsersToNotifyBanner() {
            return DOSpecialUser.EnumerateSpecialUserByParam(null, -1, -1, 1);
        }

        public static SpecialUser NewSpecialUser() {
            return new SpecialUser();
        }

        public static SpecialUser GetSpecialUserById(SpecialUserBase userBase) {
            return DOSpecialUser.GetSpecialUserById(userBase);
        }

        public static void Save(SpecialUser user) {
            if (user.IsNew) {
                DOSpecialUser.Add(user);
            } else {
                DOSpecialUser.Update(user);
            }
        }

        public static void Delete(SpecialUserBase userBase) {
            DOSpecialUser.Delete(userBase);
        }
	}
}
