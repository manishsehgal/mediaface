using System;
using System.IO;
using System.Text.RegularExpressions;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Business {
    public class BCMaintenanceClosing {
        public BCMaintenanceClosing() {}

        private const string RegexMathPattern = @"{0}\\?$";

        public static bool TestDirectoryPath(string serverName, string siteName, string virtualDirectoryName, string path) {
            string virtualPath = DOIis.GetVirtualDirectoryPath(serverName, siteName, virtualDirectoryName);
            Regex regex = new Regex(string.Format(RegexMathPattern, path));
            return regex.IsMatch(virtualPath);
        }

        public static bool SiteClosed {
            get { return BCMaintenanceClosing.TestDirectoryPath(Configuration.ServerName, Configuration.SiteName, Configuration.VirtualFolderName, Constants.MaintenanceClosingDirectory); } 
            set {
                string currentPath = DOIis.GetVirtualDirectoryPath(Configuration.ServerName, Configuration.SiteName, Configuration.VirtualFolderName);
                string newPath = string.Empty;

                string re = string.Format(RegexMathPattern, Constants.MaintenanceClosingDirectory); 
                Regex regex = new Regex(re);
                bool closed = regex.IsMatch(currentPath);

                //string aspxDll = DOIis.GetAspxDllParh(Configuration.ServerName, Configuration.VirtualFolderName);

                if(value) {
                    if(closed) {
                        newPath = currentPath;
                    }
                    else {
                        newPath = Path.Combine(currentPath, Constants.MaintenanceClosingDirectory); 
                    }
                    //DOIis.AddAnyMapping(Configuration.ServerName, Configuration.VirtualFolderName, aspxDll);
                }
                else {
                    if(closed) {
                        newPath = regex.Replace(currentPath, string.Empty);
                    }
                    else {
                        newPath = currentPath;
                    }
                    //DOIis.RemoveAnyMapping(Configuration.ServerName, Configuration.VirtualFolderName, aspxDll);
                }
                DOIis.ChangeVirtualDirectoryPath(Configuration.ServerName, Configuration.SiteName, Configuration.VirtualFolderName, newPath);
            }
        }

        public static void SaveClosePage(string htmlSource, string applicationRoot) {
            byte[] data = ConvertHelper.StringToByteArray(htmlSource);

            string filePath = Path.Combine(applicationRoot, Constants.MaintenanceClosingDirectory);
            filePath = Path.Combine(filePath, Constants.ClosePageFileName);

            if (DOFileSystem.FileExist(filePath)) {
                DOFileSystem.UnsetReadOnlyAttribute(filePath);
            }
            DOFileSystem.WriteFile(data, filePath);
        }

        public static byte[] LoadClosePage(string applicationRoot) {
            string filePath = Path.Combine(applicationRoot, Constants.MaintenanceClosingDirectory);
            filePath = Path.Combine(filePath, Constants.ClosePageFileName);

            if (DOFileSystem.FileExist(filePath))
                return DOFileSystem.ReadFile(filePath);
            else
                return new byte[0];
        }
    }
}
