using System.Data;
using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

namespace Neato.Dol.Business {
    public class BCIPFilter {
        private BCIPFilter() {}

        public static IPFilter[] EnumIPFilters() {
            return DOIPFilter.EnumIPFilters();
        }

        public static void InsertIPFilter(IPFilter ipFilter) {
            DOGlobal.BeginTransaction();
            try {
                DOIPFilter.InsertIPFilter(ipFilter);
                DOGlobal.CommitTransaction();
            } 
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UpdateIPFilter(IPFilter ipFilter) {
            DOGlobal.BeginTransaction();
            try {
                DOIPFilter.UpdateIPFilter(ipFilter);
                DOGlobal.CommitTransaction();
            } 
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void DeleteIPFilter(IPFilter ipFilter) {
            DOGlobal.BeginTransaction();
            try {
                DOIPFilter.DelIPFilter(ipFilter.Id);
                DOGlobal.CommitTransaction();
            } 
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static bool IsUserContainsInIPFilters(IPAddress userIP) {
            foreach (IPFilter ipFilter  in DOIPFilter.EnumIPFilters()) {
                if (IPAddress.ToUInt32(userIP) >=
                    IPAddress.ToUInt32(ipFilter.BeginIPAddress) &&
                    IPAddress.ToUInt32(userIP) <=
                    IPAddress.ToUInt32(ipFilter.EndIPAddress)
                    ) return true;
            }
            return false;
        }

        public static IPFilter NewIPFilter() {
            return new IPFilter();
        }
    }
}