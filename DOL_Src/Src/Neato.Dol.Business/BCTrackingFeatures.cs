using System;
using System.Data;
using System.Threading;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Tracking;

namespace Neato.Dol.Business {
    public class BCTrackingFeatures {
        public BCTrackingFeatures() {}

        public static void Add(Feature feature, string userHostAddress) {
            string login = Thread.CurrentPrincipal.Identity.Name;
            Add(feature, login, userHostAddress);
        }

        public static void Add(Feature feature, string login, string userHostAddress) {
            IPAddress userIP = IPAddress.ToIPAddress(userHostAddress);
            if(!BCIPFilter.IsUserContainsInIPFilters(userIP))
                DOTrackingFeatures.Add(feature, login, BCTimeManager.GetCurrentTime());
        }

        public static DataSet GetFeaturesListByDate(DateTime startTime, DateTime endTime) {
            return DOTrackingFeatures.GetFeaturesListByDate(startTime, endTime);
        }
    }
}