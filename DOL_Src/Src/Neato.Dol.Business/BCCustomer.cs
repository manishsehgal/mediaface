using System;
using System.Data;
using System.Text;

using Neato.Dol.Data;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

namespace Neato.Dol.Business {
    public class BCCustomer {
        public BCCustomer() {}

        public static Customer[] CustomerSearch(string firstName, string lastName, string email, DateTime startDate, DateTime endDate, bool showNotActiveUser) {
            return DOCustomer.CustomerSearch(firstName, lastName, email, startDate, endDate, showNotActiveUser);
        }

        public static Customer GetCustomer(string email, string password) {
            Customer customer = DOCustomer.GetCustomerByLogin(email, false);
            if (customer != null && customer.Password == password)
                return customer;
            return null;
        }

        public static Customer GetCustomerByLogin(string email, bool showNotActiveUser) {
            return DOCustomer.GetCustomerByLogin(email, showNotActiveUser);
        }

        public static Customer GetCustomerById(int customerId) {
            return DOCustomer.GetCustomerById(customerId);
        }

        public static void InsertCustomer(Customer customer, LastEditedPaperData data) {
            DOGlobal.BeginTransaction();
            try {
                Customer checkCustomer = DOCustomer.GetCustomerByLogin(customer.Email, false);
                if (checkCustomer != null)
                    throw new BusinessException(BCCustomerStrings.TextEmailIsNotUnique());

                checkCustomer = DOCustomer.GetCustomerByLogin(customer.Email, true);
                if (checkCustomer != null)
                    DOCustomer.DeleteCustomer(checkCustomer.CustomerId);

                customer.Created = BCTimeManager.GetCurrentTime();
                DOCustomer.InsertCustomer(customer, data);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UpdateCustomer(Customer customer) {
            DOGlobal.BeginTransaction();
            try {
                Customer checkCustomer = DOCustomer.GetCustomerByLogin(customer.Email, true);
                if (checkCustomer != null && checkCustomer.CustomerId != customer.CustomerId)
                    throw new BusinessException(BCCustomerStrings.TextEmailIsNotUnique());

                DOCustomer.UpdateCustomer(customer);
                DOGlobal.CommitTransaction();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void DeleteCustomer(int customerId) {
            DOGlobal.BeginTransaction();
            try {
                DOCustomer.DeleteCustomer(customerId);
                DOGlobal.CommitTransaction();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UpdatePasswordUid(string email, string pswuid) {
            Customer customer = DOCustomer.GetCustomerByLogin(email, true);
            if (customer == null)
                throw new BusinessException(BCCustomerStrings.TextWrongEmail());

            if (customer.PasswordUid != pswuid)
                throw new BusinessException(BCCustomerStrings.TextDoubleLink());

            customer.Password = GenerateNewPassword();
            customer.PasswordUid = string.Empty;

            UpdateCustomer(customer);
        }

        private static string GenerateNewPassword() {
            const int passwordLength = 6;
            const string Chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            int maxIndex = Chars.Length - 1;
            Random rnd = new Random(DateTime.Now.Millisecond);
            StringBuilder password = new StringBuilder(6);
            for (int i = 0; i < passwordLength; ++i) {
                password.Append(Chars[rnd.Next(0, maxIndex)]);
            }

            return password.ToString();
        }

        public static void ChangeLastEditedPaper(Customer customer, PaperBase paperBase) {
            if (customer != null) {
                Paper paper = DOPaper.GetPaper(paperBase);
                if (paper != null) {
                    BCLastEditedPaper.ChangeLastEditedPaper(customer, paperBase);
                    customer.LastEditedPapers =
                        DOLastEditedPaper.EnumerateLastEditedPaperByCustomerList(customer);
                }
            }
        }
        public static void ActivateCustomer(string email, string firstName, string lastName, string password, bool receiveInfo, string siteLink, Retailer retailer, CustomerGroup customerGroup) {
            //            DOGlobal.BeginTransaction();
            try {
                Customer cust = DOCustomer.GetCustomerByLogin(email, true);
                if (cust != null) {
                    cust.Active = true;
                    cust.Email = email;
                    if (firstName.Length > 0)
                        cust.FirstName = firstName;
                    if (lastName.Length > 0)
                        cust.LastName = lastName;
                    if (password.Length > 0)
                        cust.Password = password;
                    cust.RetailerId = 1; // neato
                    cust.Group = customerGroup;
                    cust.ReceiveInfo = receiveInfo;
                    UpdateCustomer(cust);
                }
                else {
                    cust = new Customer();
                    cust.Active = true;
                    cust.Email = email;
                    cust.FirstName = firstName;
                    cust.LastName = lastName;
                    cust.Password = password == string.Empty ? GenerateNewPassword() : password;
                    cust.RetailerId = 1; // neato
                    cust.Group = customerGroup;
                    cust.ReceiveInfo = receiveInfo;
                    InsertCustomer(cust, null);
                
                }
                BCMail.SendAccountCreatedMail(cust, siteLink, ""); 
                
//                DOGlobal.CommitTransaction();
            } catch {
//                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static PdfCalibration GetCalibration(Customer customer) {
            return DOCustomer.GetCalibration(customer);
        }

        public static void SetCalibration(Customer customer, PdfCalibration calibration) {
            DOCustomer.SetCalibration(customer, calibration);
        }

        public static PdfCalibration GetCalibrationDTCD(Customer customer) {
            return DOCustomer.GetCalibrationDTCD(customer);
        }

        public static void SetCalibrationDTCD(Customer customer, PdfCalibration calibration) {
            DOCustomer.SetCalibrationDTCD(customer, calibration);
        }
    }
}