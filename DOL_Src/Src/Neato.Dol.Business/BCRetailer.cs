using System.Data;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

namespace Neato.Dol.Business {
    public sealed class BCRetailer {
        public const int defaultId = 1;
        public const int RetailerNotFound = -1;
        private BCRetailer() {}

        public static Retailer[] Enum() {
            return DORetailer.Enum();
        }

        public static Retailer Get(int retailerId) {
            return DORetailer.Get(retailerId);
        }

        public static void Insert(Retailer retailer) {
            DOGlobal.BeginTransaction();
            try {
                DORetailer.Insert(retailer);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Update(Retailer retailer) {
            DOGlobal.BeginTransaction();
            try {
                DORetailer.Update(retailer);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Delete(Retailer retailer) {
            DOGlobal.BeginTransaction();
            try {
                DORetailer.Delete(retailer);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static bool IsUniqueRetailerName(string name) {
            Retailer[] retailerList = Enum();

            foreach (Retailer retailer in retailerList) {
                if (string.Compare(retailer.Name, name, true) == 0)
                    return false;
            }

            return true;
        }

        public static int GetId(string retailerName) {
            Retailer[] retailerList = Enum();

            foreach (Retailer retailer in retailerList) {
                if (string.Compare(retailer.Name, retailerName, true) == 0)
                    return retailer.Id;
            }

            return RetailerNotFound;
        }

        public static byte[] GetIcon(int retailerId) {
            Retailer retailer = DORetailer.Get(retailerId);
            if (retailer != null)
                return retailer.Icon;

            return new byte[0];
        }
    }
}