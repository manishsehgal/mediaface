using System.Data;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

namespace Neato.Dol.Business {
    public sealed class BCTermsOfUsePage {
        private BCTermsOfUsePage() {}

        public static LocalizedText Get(string culture) {
            LocalizedText[] list = DOTermsOfUsePage.Enum(culture);
            return BCLocalizedText.GetTextFromList(list, culture);
        }

        public static void Update(LocalizedText localizedText) {
            DOGlobal.BeginTransaction();
            try {
                LocalizedText text = Get(localizedText.Culture);
                if (text != null)
                    DOTermsOfUsePage.Update(localizedText.Culture, localizedText.Text);
                else
                    DOTermsOfUsePage.Insert(localizedText.Culture, localizedText.Text);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
    }
}