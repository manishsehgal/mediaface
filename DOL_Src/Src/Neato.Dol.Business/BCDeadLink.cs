using System;
using System.Data;
using System.Text.RegularExpressions;
using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

namespace Neato.Dol.Business {
    public class BCDeadLink {
        private const string Error404Template = @"(\?(?<Error>404);)(?<Url>.*)$";

        private BCDeadLink() {}

        public static bool CheckDeadLink(string url, string ip, string referrer) {
            System.Text.RegularExpressions.Regex regex = new Regex(Error404Template);
            System.Text.RegularExpressions.Match match = regex.Match(url);
            if (match.Success) {
                BCDeadLink.Insert(new DeadLink(match.Result("${Url}"), ip, referrer));
            }
            return match.Success;
        }

        public static void Insert(DeadLink deadlink) {
            DOGlobal.BeginTransaction();
            try {
                int index = deadlink.Url.IndexOf('?');
                if (index != -1)
                    deadlink.Url = deadlink.Url.Substring(0, index);
                DODeadLink.Insert(deadlink);
                DOGlobal.CommitTransaction();
            }
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        } 

        public static void Delete(DeadLink deadlink) {
            DOGlobal.BeginTransaction();
            try {
                DODeadLink.Delete(deadlink);
                DOGlobal.CommitTransaction();
            }
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void DeleteByUrl(DeadLink deadlink) {
            DOGlobal.BeginTransaction();
            try {
                DODeadLink.DeleteByUrl(deadlink);
                DOGlobal.CommitTransaction();
            }
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static DeadLink[] Enumerate(DateTime startTime, DateTime endTime) {
            return DODeadLink.Enumerate(startTime, endTime);
        }

        public static DataSet GetReport(DateTime startTime,DateTime endTime) {
            return DODeadLink.GetReport(startTime, endTime);
        }
    }
}