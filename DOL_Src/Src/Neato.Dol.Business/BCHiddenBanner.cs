using System.Data;

using Neato.Dol.Data;
using Neato.Dol.Entity;

namespace Neato.Dol.Business
{
	public class BCHiddenBanner	{
        public BCHiddenBanner()	{}


        public static void HideBanner(bool hide, HiddenBanner banner) {
            if(hide) {
                DOHiddenBanner.Insert(banner);                
            }
            else {
                DOHiddenBanner.Delete(banner);
            }
        }

        public static bool IsBannerHidden(HiddenBanner banner) {
            return DOHiddenBanner.GetHiddenBanner(banner) != null;
        }
	}
}
