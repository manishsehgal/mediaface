using System;
using System.Data;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using Neato.Dol.Data;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Business {
    public class BCFaceLayout {
        public BCFaceLayout() {}

        public static FaceLayoutData EnumerateFaceLayouts(PaperBase paper) {
            return DOFaceLayout.EnumerateFaceLayouts(paper);
        }
		 public static byte[] GetIconByFaceIdLayoutId(FaceBase face, int layout) {
            return DOFaceLayout.GetIconByFaceIdLayoutId(face,layout);
		}

        public static FaceLayout[] EnumerateFaceLayouts(DeviceType deviceType, Face face, DeviceBase device) {
            return DOFaceLayout.EnumerateFaceLayouts(deviceType, face, device);
        }

        public static FaceLayout NewLayout() {
            return new FaceLayout();
        }

        public static void Insert(FaceLayout faceLayout) {
            DOGlobal.BeginTransaction();
            try {
                DOFaceLayout.Insert(faceLayout);
                DOGlobal.CommitTransaction();
            }
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }        
        }

        public static void Delete(FaceLayout faceLayout) {
            DOGlobal.BeginTransaction();
            try {
                DOFaceLayout.Delete(faceLayout);
                DOGlobal.CommitTransaction();
            }
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }        
        }

        public static void Update(FaceLayout faceLayout) {
            DOGlobal.BeginTransaction();
            try {
                DOFaceLayout.Update(faceLayout);
                DOGlobal.CommitTransaction();
            }
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }        
        }

        public static void Insert(FaceLayout faceLayout, Stream projectFile, Stream iconFile) {
            DOGlobal.BeginTransaction();
            try {
                Project project = BCProject.Import(null, projectFile);

                XPathNavigator countNavigator = project.ProjectXml.CreateNavigator();
                int faceCount = (int)(double)countNavigator.Evaluate("count(/Project/Faces/Face[Text|TextEffect|Playlist])");

                if(faceCount > 1)
                    throw new BusinessException("The file of the project contains more than one label with text elements");
                else if(faceCount < 1)
                    throw new BusinessException("The file of the project does not contain labels with text elements");

                XmlNode faceNode = project.ProjectXml.SelectSingleNode("/Project/Faces/Face[Text|TextEffect|Playlist]");
                
                faceLayout.Parse(faceNode);
                if(faceLayout.Face.Guid != Guid.Empty)
                    faceLayout.Face.Id = BCFace.GetFaceIdByGuid(faceLayout.Face.Guid);
                DOFaceLayout.Insert(faceLayout);

                foreach(FaceLayoutItem faceLayoutItem in faceLayout.FaceLayoutItems)
                    DOFaceLayoutItem.Insert(faceLayout, faceLayoutItem);

                ChangeIcon(faceLayout, iconFile);
                DOGlobal.CommitTransaction();
            }
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }        
        }

        public static void Update(FaceLayout faceLayout, Stream projectFile, Stream iconFile) {
            DOGlobal.BeginTransaction();
            try {
                DOFaceLayout.FaceLayoutEnumById(faceLayout);
                if(projectFile != null && projectFile.Length != 0) {
                    Project project = BCProject.Import(null, projectFile);

                    XPathNavigator countNavigator = project.ProjectXml.CreateNavigator();
                    int faceCount = (int)(double)countNavigator.Evaluate("count(/Project/Faces/Face[Text|TextEffect|Playlist])");

                    if(faceCount != 1)
                        throw new BusinessException("Project contains more or less than one face with text elements");

                    XmlNode faceId = project.ProjectXml.SelectSingleNode("/Project/Faces/Face/@dbId");
                    if((int.Parse(faceId.Value)) != faceLayout.Face.Id)
                        throw new BusinessException("Project face not corresponding to layout face");

                    XmlNode faceNode = project.ProjectXml.SelectSingleNode("/Project/Faces/Face[Text|TextEffect|Playlist]");
                    faceLayout.Parse(faceNode);

                    DOFaceLayoutItem.Delete(faceLayout);
                    DOFaceLayout.Update(faceLayout);
                    DOFaceLayout.GetIconId(faceLayout);

                    foreach(FaceLayoutItem faceLayoutItem in faceLayout.FaceLayoutItems)
                        DOFaceLayoutItem.Insert(faceLayout, faceLayoutItem);
                }
                ChangeIcon(faceLayout, iconFile);
                DOGlobal.CommitTransaction();
            }
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }        
        }

        public static void Save(FaceLayout faceLayout, Stream projectFile, Stream iconFile) {
            if(faceLayout.IsNew)
                BCFaceLayout.Insert(faceLayout, projectFile, iconFile);
            else
                BCFaceLayout.Update(faceLayout, projectFile, iconFile);
        }

        public static void ChangeIcon(FaceLayout faceLayout, Stream icon) {
            if (icon == null || icon.Length == 0) return;
            try {
                if(faceLayout.ImageLibItem.IsNew) {
                    DOFaceLayout.LayoutIconInsert(faceLayout, ImageHelper.ConvertImageToJpeg(ConvertHelper.StreamToByteArray(icon)));
                    DOFaceLayout.LayoutFaceToIconInsert(faceLayout);
                }
                else {
                    DOFaceLayout.LayoutIconUpdate(faceLayout, ConvertHelper.StreamToByteArray(icon));
                }
            }
            catch(BusinessException) {
            }
        }

        public static FaceLayoutItem[] FaceLayoutItemsEnumByFaceLayoutId(FaceLayout faceLayout) {
            return DOFaceLayout.FaceLayoutItemsEnumByFaceLayoutId(faceLayout);
        }

        public static void Save(FaceLayoutItem faceLayoutItem) {
            DOGlobal.BeginTransaction();
            try {
                DOFaceLayout.Update(faceLayoutItem);
                DOGlobal.CommitTransaction();
            }
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }        
        }

        public static void UpdateBlankIcon(int faceId, Stream icon) {
            DOGlobal.BeginTransaction();
            try {
                FaceLayout faceLayout = new FaceLayout(0);
                faceLayout.Face.Id = faceId;
                int iconId = DOFaceLayout.GetLayoutIconId(faceId);
                if (iconId > -1) {
                    faceLayout.ImageLibItem.Id = iconId;
                    DOFaceLayout.LayoutIconUpdate(faceLayout, ConvertHelper.StreamToByteArray(icon));
                } else {
                    DOFaceLayout.LayoutIconInsert(faceLayout, ImageHelper.ConvertImageToJpeg(ConvertHelper.StreamToByteArray(icon)));
                    DOFaceLayout.LayoutFaceToIconInsert(faceLayout);
                }
                DOGlobal.CommitTransaction();
            }
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
    }
}
