using System.IO;

using Neato.Dol.Data;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Business {
    public class BCImageLibrary {
		public BCImageLibrary() {}

        public static ImageLibItem[] ImageLibItemEnumByFolderId(int folderId) {
            return DOImageLibrary.ImageLibItemEnumByFolderId(folderId);
        }

        public static byte[] GetImage(int id) {
            return DOImageLibrary.GetImage(id);
        }

        public static byte[] GetImageIcon(int id, int iconWidth, int iconHeight) {
            byte[] image = DOImageLibrary.GetImage(id);
            byte[] icon;
            if (image != null && image.Length > 0) {
                icon = ImageHelper.GetIcon(image, iconWidth, iconHeight);
            } else {
                icon = new byte[0];
            }
            return icon;
        }

        public static Library GetImageLibrary(string culture, CustomerGroup customerGroup) {
			ImageLibraries lib = ImageLibraries.Limited;
            if (customerGroup == CustomerGroup.MFOPE) {
				lib = ImageLibraries.Expanded;
			}

			return DOImageLibrary.GetImageLibrary(culture, lib);
        }

        public static int TestAddImage(Stream imageData) {
            byte[] buffer = new byte[imageData.Length];
            imageData.Position = 0;
            imageData.Read(buffer, 0, buffer.Length);
            byte[] arrayData = ImageHelper.ConvertImageToJpeg(buffer);
            return DOImageLibrary.TestAddImage(arrayData);
        }

        public static int ImageLibItemIns(int folderId, Stream imageData, bool convertToJpeg) {
            byte[] buffer = new byte[imageData.Length];
            imageData.Position = 0;
            imageData.Read(buffer, 0, buffer.Length);
            byte[] arrayData = null;
            if (convertToJpeg)
                arrayData = ImageHelper.ConvertImageToJpeg(buffer);
            else
                arrayData = buffer;
            return DOImageLibrary.ImageLibItemIns(folderId, arrayData);
        }

		public static bool ImageLibCheckState(string libId) {
			if (int.Parse(libId) == (int)ImageLibraries.Limited) {
				return true;
			} else {
				return false;
			}
		}

		public static int ImageLibCheckState(bool checkState) {
			if (checkState) {
				return (int)ImageLibraries.Limited;
			} else {
				return (int)ImageLibraries.Expanded;
			}
		}

        public static void ImageLibItemDel(int itemId) {
            DOImageLibrary.ImageLibItemDel(itemId);
        }

        public static void ImageLibItemsDel(ImageLibItem[] items) {
            DOGlobal.BeginTransaction();
            try {
                DOImageLibrary.ImageLibItemsDel(items);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }


        public static int InsertImageLibFolder(
            object parentFolderId, string caption, string culture, ImageLibraries lib) {
            return DOImageLibrary.InsertImageLibFolder(parentFolderId, caption, culture, lib);
        }

        public static void UpdateImageLibFolder(
            int folderId, string caption, string culture) {
            DOImageLibrary.UpdateImageLibFolder(folderId, caption, culture);
        }
		
		public static void UpdateImageLibId(
			int folderId, int libId) {
			DOImageLibrary.UpdateImageLibId(folderId, libId);
		}

        public static void DeleteImageLibFolder(int folderId) {
            DOImageLibrary.DeleteImageLibFolder(folderId);
        }
        public static void AddContentRecursively
            (string imagePath, string targetFolderName,
            bool insertIntoRoot, bool clearTargetFolder) {
            DOGlobal.BeginTransaction();
            try {
                DOImageLibrary.AddContentRecursively
                    (imagePath, targetFolderName,insertIntoRoot,
                    clearTargetFolder);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void ImageLibFolderSortOrderSwap
            (int folderId1, int folderId2) {
            DOImageLibrary.ImageLibFolderSortOrderSwap(folderId1, folderId2);
        }
    }
}