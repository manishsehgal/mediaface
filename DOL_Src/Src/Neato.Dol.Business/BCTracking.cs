using System.Data;
using System.Threading;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Tracking;

namespace Neato.Dol.Business {
    public class BCTracking {
        public BCTracking() {}

        public static void Add(UserAction userAction, string userHostAddress) {
            string login = Thread.CurrentPrincipal.Identity.Name;
            Add(userAction, login, userHostAddress);
        }

        public static void Add(UserAction userAction, string login, string userHostAddress) {
            IPAddress userIP = IPAddress.ToIPAddress(userHostAddress);
            if(!BCIPFilter.IsUserContainsInIPFilters(userIP))
                DOTracking.Add(userAction, login, BCTimeManager.GetCurrentTime());
        }

        public static DataSet GetList() {
            return DOTracking.GetList();
        }
    }
}