using System;
using System.Data;
using System.Threading;

using Neato.Dol.Data;
using Neato.Dol.Entity;

namespace Neato.Dol.Business
{
	public class BCTrackingPapers {
		public BCTrackingPapers() {}

        public static void Add(Paper paper, string userHostAddress) {
            string login = Thread.CurrentPrincipal.Identity.Name;
            Add(paper, login, userHostAddress);
        }

        public static void Add(Paper paper, string login, string userHostAddress) {
            IPAddress userIP = IPAddress.ToIPAddress(userHostAddress);
            if(!BCIPFilter.IsUserContainsInIPFilters(userIP))
                DOTrackingPapers.Add(paper, login, BCTimeManager.GetCurrentTime());
        }

        public static DataSet GetListByDate(DateTime startTime, DateTime endTime) {
            return DOTrackingPapers.GetListByDate(startTime, endTime);
        }
	}
}
