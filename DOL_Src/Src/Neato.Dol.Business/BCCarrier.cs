using System.Data;
using System.IO;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Business {
    public sealed class BCCarrier {
        private BCCarrier() {}

        public static Carrier[] GetCarrierListByDeviceType(DeviceType deviceType) {
            return DOCarrier.EnumerateCarrierByParam(deviceType, null);
        }

        public static Carrier[] GetCarrierList() {
            return DOCarrier.EnumerateCarrierByParam(DeviceType.Undefined, null);
        }

        public static Carrier[] GetCarrierList(DeviceBase device) {
            return DOCarrier.EnumerateCarrierByParam(DeviceType.Undefined, device);
        }

        public static byte[] GetCarrierIcon(CarrierBase carrier) {
            return DOCarrier.GetCarrierIcon(carrier);
        }

        public static Carrier NewCarrier() {
            Carrier carrier = new Carrier();
            return carrier;
        }

        public static void Save(Carrier carrier) {
            Save(carrier, null);
        }

        public static void Save(Carrier carrier, Stream icon) {
            DOGlobal.BeginTransaction();
            try {
                if (carrier.IsNew) {
                    DOCarrier.Add(carrier);
                } else {
                    DOCarrier.Update(carrier);
                }
                ChangeIcon(carrier, icon);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void ChangeIcon(CarrierBase carrier, Stream icon) {
            if (icon != null && icon.Length > 0) {
                DOGlobal.BeginTransaction();
                try {
                    DOCarrier.UpdateIcon(carrier, ConvertHelper.StreamToByteArray(icon));
                    DOGlobal.CommitTransaction();
                } catch (DBConcurrencyException ex) {
                    DOGlobal.RollbackTransaction();
                    throw new ConcurrencyException(ex.Message, ex);
                } catch {
                    DOGlobal.RollbackTransaction();
                    throw;
                }
            }
        }

        public static void Delete(CarrierBase carrier) {
            if (carrier.IsNew) return;
            Device[] devices = BCDevice.GetDeviceList(carrier);
            DOGlobal.BeginTransaction();
            try {
                RemoveDeviceFromCarrier(carrier, devices);
                DOCarrier.Delete(carrier);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void RemoveDeviceFromCarrier(CarrierBase carrier, params Device[] devices) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Device device in devices) {
                    DODevice.RemoveFromCarrier(device, carrier);
                }
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void AddDeviceToCarrier(CarrierBase carrier, params Device[] devices) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Device device in devices) {
                    DODevice.AddToCarrier(device, carrier);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

    }
}