using Neato.Dol.Entity;

namespace Neato.Dol.Business {
    public sealed class BCBannerPage {
        private static BannerPage[] bannerPages = {
            new BannerPage("Default", "Default", "Images/BannerPage_Default.jpg", new int[] {1, 2, 3, 4, 5, 6, 7}),
            new BannerPage("Login", "Login", "Images/BannerPage_Login.jpg", new int[] {1, 2}),
            new BannerPage("CreateAccount", "Create account", "Images/BannerPage_CreateAccount.jpg", new int[] {1, 2, 3, 4}),
            new BannerPage("Upc", "UPC", "Images/BannerPage_Upc.jpg", new int[] {1, 2}),
            //new BannerPage("SiteClosed", "Closed for Maintenance", "Images/BannerPage_SiteClosed.jpg", new int[] {1}),
        };

        private BCBannerPage() {}

        public static BannerPage[] Enum() {
            return bannerPages;
        }

        public static BannerPage Get(string bannerPageId) {
            foreach (BannerPage bannerPage in bannerPages) {
                if (bannerPage.Id == bannerPageId)
                    return bannerPage;
            }

            return null;
        }
    }
}