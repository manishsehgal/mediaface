using System.Web.UI.WebControls;
using Neato.Dol.Entity;

namespace Neato.Dol.Business {
    public sealed class BCPreviewPage {
        private BCPreviewPage() {}

        private static ListItem[] bannerPages = {
            new ListItem(CustomerGroup.Trial.ToString(), ((int)CustomerGroup.Trial).ToString()),
            new ListItem(CustomerGroup.MFO.ToString(), ((int)CustomerGroup.MFO).ToString()),
            new ListItem(CustomerGroup.MFOPE.ToString(), ((int)CustomerGroup.MFOPE).ToString())
        };

        public static ListItem[] Enum() {
            return bannerPages;
        }

        public const string Preview = "Preview";
    }
}