using System;
using System.Data;
using System.Threading;

using Neato.Dol.Data;
using Neato.Dol.Entity;

namespace Neato.Dol.Business
{
	public class BCTrackingImages
	{
		public BCTrackingImages(){}
        public static void Add(ImageLibItem image, string userHostAddress) 
        {
            string login = Thread.CurrentPrincipal.Identity.Name;
            Add(image, login, userHostAddress);
        }

        public static void Add(ImageLibItem image, string login, string userHostAddress) 
        {
            IPAddress userIP = IPAddress.ToIPAddress(userHostAddress);
            if(!BCIPFilter.IsUserContainsInIPFilters(userIP))
                DOTrackingImages.Add(image, login, BCTimeManager.GetCurrentTime());
        }

        public static DataSet GetImagesListByDate(DateTime startTime, DateTime endTime) 
        {
            return DOTrackingImages.GetImagesListByDate( startTime, endTime);
        }

	}
}
