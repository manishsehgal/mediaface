using System;
using System.Data;
using System.Threading;

using Neato.Dol.Data;
using Neato.Dol.Entity;

namespace Neato.Dol.Business
{
	/// <summary>
	/// Summary description for BCTrackingPhones.
	/// </summary>
	public class BCTrackingPhones
	{
		public BCTrackingPhones()
		{
		}
        public static void Add(Device device, string userHostAddress) 
        {
            string login = Thread.CurrentPrincipal.Identity.Name;
            Add(device, login, userHostAddress);
        }

        public static void Add(Device device, string login, string userHostAddress) 
        {
            IPAddress userIP = IPAddress.ToIPAddress(userHostAddress);
            if(!BCIPFilter.IsUserContainsInIPFilters(userIP))
                DOTrackingPhones.Add(device, login, BCTimeManager.GetCurrentTime());
        }

        public static DataSet GetListByBrandAndDate(DeviceBrand brand,DateTime startTime, DateTime endTime) 
        {
            return DOTrackingPhones.GetListByBrandAndDate(brand, startTime, endTime);
        }
        public static DataSet GetCarriersList() 
        {
            return DOTrackingPhones.GetCarriersList();
        }
	}
}
