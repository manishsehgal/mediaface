using System.Data;

using Neato.Dol.Data;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

namespace Neato.Dol.Business {
    public sealed class BCOverviewPage {
        private BCOverviewPage() {}

        public static LocalizedText[] Enum(string culture) {
            return DOOverviewPage.Enum(culture);
        }

        private static LocalizedText FindLocalizedText(LocalizedText[] list, string culture) {
            foreach(LocalizedText item in list) {
                if (item.Culture == culture)
                    return item;
            }
            return null;
        }

        public static LocalizedText GetTextFromList(LocalizedText[] list, string culture) {
            LocalizedText item = null;
            //long name
            item = FindLocalizedText(list, culture);
            if (item != null)
                return item;
            //short name
            if (culture.Length > 2) {
                culture = culture.Substring(0, 2);
                item = FindLocalizedText(list, culture);
                if (item != null)
                    return item;
            }
            //default name name
            item = FindLocalizedText(list, string.Empty);
            
            return item;
        }

        public static LocalizedText Get(string culture) {
            LocalizedText[] list = DOOverviewPage.Enum(culture);
            return GetTextFromList(list, culture);
        }

        public static void Update(LocalizedText localizedText) {
            DOGlobal.BeginTransaction();
            try {
                LocalizedText text = Get(localizedText.Culture);
                if (text != null)
                    DOOverviewPage.Update(localizedText.Culture, localizedText.Text);
                else
                    DOOverviewPage.Insert(localizedText.Culture, localizedText.Text);
                DOGlobal.CommitTransaction();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Delete(string culture) {
            DOGlobal.BeginTransaction();
            try {
                DOOverviewPage.Delete(culture);
                DOGlobal.CommitTransaction();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
    }
}