using System.Collections;
using System.Data;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data {
    public sealed class DOTermsOfUsePage {
        private DOTermsOfUsePage() {}

        public static LocalizedText[] Enum(string culture) {
            PrTermsOfUsePageEnumByCulture prc = new PrTermsOfUsePageEnumByCulture();
            prc.Culture = culture;

            ArrayList list = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int CultureIndex = reader.GetOrdinal("Culture");
                int HtmlTextIndex = reader.GetOrdinal("HtmlText");
                while (reader.Read()) {
                    LocalizedText localizedText = new LocalizedText();
                    localizedText.Culture = reader.GetString(CultureIndex);
                    localizedText.Text = reader.GetString(HtmlTextIndex);
                    list.Add(localizedText);
                }
            }
            return (LocalizedText[])list.ToArray(typeof(LocalizedText));
        }

        public static void Insert(string culture, string htmlText) {
            PrTermsOfUsePageIns prc = new PrTermsOfUsePageIns();
            prc.Culture = culture;
            prc.HtmlText = htmlText;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Terms Of Use does not exist.");
        }

        public static void Update(string culture, string htmlText) {
            PrTermsOfUsePageUpd prc = new PrTermsOfUsePageUpd();
            prc.Culture = culture;
            prc.HtmlText = htmlText;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Terms Of Use does not exist.");
        }
    }
}