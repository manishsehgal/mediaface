<?xml version="1.0" encoding="UTF-8" ?>
<?codegen ConnectionString="Provider=SQLOLEDB;UID=sa;PWD=sa;SERVER=MARS;DATABASE=db_dol" Procedure="Pr%" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxml="urn:schemas-microsoft-com:xslt">
<xsl:output method="text"/>
<xsl:preserve-space elements="*"/>
<xsl:variable name="name-space"/>

<xsl:template match="/*">using System;
using System.Data;
using System.Data.SqlTypes;
using Neato.Dol.Data.DBEngine;

namespace <xsl:value-of select="$name-space" /> {
<xsl:apply-templates select="procedure" />}</xsl:template>

<xsl:template match="procedure">
    <xsl:variable name="spname" select="sp_name" />
    /// &lt;exclude/&gt;
    internal sealed class <xsl:value-of select="sp_name" /> : ProcedureWrapper {
        internal <xsl:value-of select="sp_name" />() : this(string.Empty) {}
        internal <xsl:value-of select="sp_name" />(string databaseInstanceName) : base(databaseInstanceName, "<xsl:value-of select="sp_name" />") {}
         
        protected override void InitializeParameters() {<xsl:apply-templates select="/DSDatabase/parameter[sp_name = $spname]" mode="param-constructor"/>
        }<xsl:apply-templates select="/DSDatabase/parameter[sp_name = $spname]" mode="param-property"/>
    }
</xsl:template>


<xsl:template match="/DSDatabase/parameter" mode="param-constructor">
            RealWrapper.AddParameter("<xsl:value-of select="name" />", <xsl:value-of select="full_type" />, <xsl:value-of select="size" />, <xsl:value-of select="full_direction" />, true, <xsl:value-of select="precision" />, <xsl:value-of select="scale" />, "<xsl:value-of select="name" />", DataRowVersion.Default, DBNull.Value);</xsl:template>


<xsl:template match="/DSDatabase/parameter" mode="param-property">
    <xsl:variable name="c-sharp-type">
        <xsl:choose>
            <xsl:when test="starts-with(type, 'AnsiString')">string</xsl:when>
            <xsl:when test="starts-with(type, 'AnsiStringFixedLength')">string</xsl:when>
            <xsl:when test="starts-with(type, 'Binary')">byte[]</xsl:when>
            <xsl:when test="starts-with(type, 'Boolean')">SqlBoolean</xsl:when>
            <xsl:when test="starts-with(type, 'Byte')">SqlByte</xsl:when>
            <xsl:when test="starts-with(type, 'Currency')">SqlDecimal</xsl:when>
            <xsl:when test="starts-with(type, 'Date')">SqlDateTime</xsl:when>
            <xsl:when test="starts-with(type, 'DateTime')">SqlDateTime</xsl:when>
            <xsl:when test="starts-with(type, 'Decimal')">SqlDecimal</xsl:when>
            <xsl:when test="starts-with(type, 'Double')">SqlDouble</xsl:when>
            <xsl:when test="starts-with(type, 'Guid')">SqlGuid</xsl:when>
            <xsl:when test="starts-with(type, 'Int16')">SqlInt16</xsl:when>
            <xsl:when test="starts-with(type, 'Int32')">SqlInt32</xsl:when>
            <xsl:when test="starts-with(type, 'Int64')">SqlInt64</xsl:when>
            <xsl:when test="starts-with(type, 'Object')">Object</xsl:when>
            <xsl:when test="starts-with(type, 'SByte')">SqlInt16</xsl:when>
            <xsl:when test="starts-with(type, 'Single')">SqlSingle</xsl:when>
            <xsl:when test="starts-with(type, 'String')">string</xsl:when>
            <xsl:when test="starts-with(type, 'StringFixedLength')">string</xsl:when>
            <!--xsl:when test="starts-with(type, 'Time')"></xsl:when-->
            <xsl:when test="starts-with(type, 'UInt16')">SqlInt32</xsl:when>
            <xsl:when test="starts-with(type, 'UInt32')">SqlInt64</xsl:when>
            <xsl:when test="starts-with(type, 'UInt64')">SqlDecimal</xsl:when>
            <!--xsl:when test="starts-with(type, 'VarNumeric')"></xsl:when-->
            <xsl:otherwise><xsl:value-of select="full_type"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="c-sharp-subtype-cast">
        <xsl:choose>
            <xsl:when test="starts-with(type, 'AnsiString')"></xsl:when>
            <xsl:when test="starts-with(type, 'String')"></xsl:when>
            <xsl:when test="starts-with(type, 'Int32')">(Int32)</xsl:when>
            <xsl:when test="starts-with(type, 'Int64')"></xsl:when>
            <xsl:when test="starts-with(type, 'Date')">(DateTime)</xsl:when>
            <xsl:when test="starts-with(type, 'Currency')"></xsl:when>
            <xsl:when test="starts-with(type, 'Decimal')"></xsl:when>
            <xsl:when test="starts-with(type, 'Boolean')">(bool)</xsl:when>
            <xsl:when test="starts-with(type, 'Variant')">(object)</xsl:when>
            <xsl:when test="starts-with(type, 'Binary')"></xsl:when>
            <xsl:otherwise>(<xsl:value-of select="type"/>)</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:variable name="c-sharp-native-type">
        <xsl:choose>
            <xsl:when test="starts-with(type, 'String')">true</xsl:when>
            <xsl:when test="starts-with(type, 'StringFixedLength')">true</xsl:when>
            <xsl:when test="starts-with(type, 'AnsiString')">true</xsl:when>
            <xsl:when test="starts-with(type, 'AnsiStringFixedLength')">true</xsl:when>
            <xsl:when test="starts-with(type, 'Binary')">true</xsl:when>
            <xsl:otherwise>false</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="property-name">
        <xsl:choose>
            <xsl:when test="name = 'RETURN_VALUE'">ReturnValue</xsl:when>
            <xsl:otherwise><xsl:value-of select="name" /></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

        public <xsl:value-of select="$c-sharp-type"/>&#32;<xsl:value-of select="$property-name" /> {
            <xsl:if test="direction = 'Output' or direction = 'InputOutput' or direction = 'ReturnValue'">get {
                <xsl:choose>
                    <xsl:when test="$c-sharp-native-type = 'true'">return (<xsl:value-of select="$c-sharp-type" />)RealWrapper.GetParameterValue("<xsl:value-of select="name" />");</xsl:when>
                    <xsl:otherwise>object val = RealWrapper.GetParameterValue("<xsl:value-of select="name" />");
                return (val == null) ? <xsl:value-of select="$c-sharp-type" />.Null : (<xsl:value-of select="$c-sharp-type" />)<xsl:value-of select="$c-sharp-subtype-cast" />val;</xsl:otherwise>
                </xsl:choose>
            }
            </xsl:if>
            <xsl:if test="direction = 'Input' or direction = 'InputOutput'">set { RealWrapper.SetParameterValue("<xsl:value-of select="name" />", value); }</xsl:if>
        }</xsl:template>
        
</xsl:stylesheet>
