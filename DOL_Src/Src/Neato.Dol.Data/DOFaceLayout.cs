using System;
using System.Collections;
using System.Data;
using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

namespace Neato.Dol.Data {
    public class DOFaceLayout {
        public DOFaceLayout() {}

        public static FaceLayoutData EnumerateFaceLayouts(PaperBase paper) {
            PrFaceLayoutEnumByPaperId prc = new PrFaceLayoutEnumByPaperId();
            prc.PaperId = paper.Id;
            return (FaceLayoutData)prc.LoadDataSet(new FaceLayoutData());
        }
		 public static byte[] GetIconByFaceIdLayoutId(FaceBase face, int layout) {
            PrLayoutIconGetByFaceAndLayoutId  prc = new PrLayoutIconGetByFaceAndLayoutId();
            prc.FaceId = face.Id;
            prc.LayoutId = layout;
            object val;
            using (IDataReader reader = prc.ExecuteReader()) 
            {
                int StreamColumnIndex = reader.GetOrdinal("Icon");
                if (reader.Read()) 
                {
                    val = reader.GetValue(StreamColumnIndex);
                } 
                else 
                {
                    val = new byte[0];
                }
            }
            return (byte[])val;
		}

        public static FaceLayout[] EnumerateFaceLayouts(DeviceType deviceType, Face face, DeviceBase device) {
            PrFaceLayoutEnumByDeviceTypeIdDeviceIdFaceId prc = new PrFaceLayoutEnumByDeviceTypeIdDeviceIdFaceId();
            if (face != null) 
                prc.FaceId = face.Id;
            if (device != null) 
                prc.DeviceId = device.Id;
            if (deviceType != DeviceType.Undefined) 
                prc.DeviceTypeId = (int)deviceType;

            ArrayList list = new ArrayList();
            using (IDataReader reader = prc.ExecuteReader()) {
                int iconIdColumnIndex = reader.GetOrdinal("IconId");
                int nameColumnIndex = reader.GetOrdinal("Name");
                int posColumnIndex = reader.GetOrdinal("Position");
                int textColumnIndex = reader.GetOrdinal("Text");
                int faceIdColumnIndex = reader.GetOrdinal("FaceId");
                int faceNameColumnIndex = reader.GetOrdinal("FaceName");
                int faceLayoutIdColumnIndex = reader.GetOrdinal("FaceLayoutId");
                FaceLayout faceLayout = null;
                int faceLayoutId = -1;
                while (reader.Read()) {
                    bool isNewLayout = faceLayoutId != reader.GetInt32(faceLayoutIdColumnIndex);
                    if(isNewLayout) {
                        if(faceLayout != null)
                            list.Add(faceLayout);
                        faceLayoutId = reader.GetInt32(faceLayoutIdColumnIndex);
                        faceLayout = new FaceLayout(faceLayoutId);
                        int iconId = reader.IsDBNull(iconIdColumnIndex) ? 0 : reader.GetInt32(iconIdColumnIndex);
                        faceLayout.ImageLibItem = new ImageLibItem(iconId);
                        faceLayout.Name = reader.GetString(nameColumnIndex);
                        faceLayout.Face.Id = reader.GetInt32(faceIdColumnIndex);
                        faceLayout.Face.AddName(string.Empty, reader.GetString(faceNameColumnIndex));
                    }
                    FaceLayoutItem faceLayoutItem = new FaceLayoutItem();
                    faceLayoutItem.Position = reader.GetByte(posColumnIndex);
                    faceLayoutItem.Text = reader.GetString(textColumnIndex);
                    faceLayout.Add(faceLayoutItem);
                }
                if(faceLayout != null)
                    list.Add(faceLayout);
            }
            return (FaceLayout[])list.ToArray(typeof(FaceLayout));
        }


        public static void Insert(FaceLayout faceLayout) {
            if(!faceLayout.IsNew)
                 throw new InvalidOperationException("Adding label layout entity in 'Non-New' status");

            PrFaceLayoutIns prc = new PrFaceLayoutIns();
            
            prc.FaceId = faceLayout.Face.Id;
            prc.Name = faceLayout.Name;

            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Label layout does not exist.");
            faceLayout.Id = prc.FaceLayoutId.Value;
        }

        public static void Delete(FaceLayout faceLayout) {
            PrFaceLayoutDel prc = new PrFaceLayoutDel();
            prc.FaceLayoutId = faceLayout.Id;
            prc.ExecuteNonQuery();
        }

        public static void Update(FaceLayout faceLayout) {
            PrFaceLayoutUpd prc = new PrFaceLayoutUpd();
            prc.FaceLayoutId = faceLayout.Id;
            prc.Name = faceLayout.Name;
            prc.ExecuteNonQuery();
        }
        
        public static void LayoutIconInsert(FaceLayout faceLayout, byte[] data) {
            if(data.Length == 0) {
                throw new BusinessException("Wrong file format or corrupted file");
            }
            PrLayoutIconIns prc = new PrLayoutIconIns();
            prc.Stream = data;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Layout icon was not inserted.");
            faceLayout.ImageLibItem.Id = prc.IconId.Value;
        }

        public static void LayoutIconUpdate(FaceLayout faceLayout, byte[] data) {
            PrLayoutIconUpd prc = new PrLayoutIconUpd();
            prc.IconId = faceLayout.ImageLibItem.Id;
            prc.Stream = data;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Layout icon was not updated.");
        }

        public static void LayoutFaceToIconInsert(FaceLayout faceLayout) {
            PrLayoutFaceToIconIns prc = new PrLayoutFaceToIconIns();
            prc.FaceId = faceLayout.Face.Id;
            prc.ImageId = faceLayout.ImageLibItem.Id;
            if (faceLayout.Id > 0)
                prc.LayoutId = faceLayout.Id;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Layout LayoutToFace was not inserted.");
        }

        public static void GetIconId(FaceLayout faceLayout) {
            PrLayoutIconIdGetByFaceLayoutId prc = new PrLayoutIconIdGetByFaceLayoutId();
            prc.FaceLayoutId = faceLayout.Id;
            using(IDataReader reader = prc.ExecuteReader()) {
                int iconIdColumnIndex = reader.GetOrdinal("IconId");
                if (reader.Read()) {
                    object iconId = reader.GetValue(iconIdColumnIndex);
                    if(DBNull.Value != iconId)
                        faceLayout.ImageLibItem.Id = (int) iconId;
                }
            }
        }

        public static byte[] GetFaceLayoutIconById(ImageLibItem imageLibItem) {
            PrLayoutIconEnumById prc = new PrLayoutIconEnumById();

            prc.IconId = imageLibItem.Id;
            using (IDataReader reader = prc.ExecuteReader()) {
                int iconColumnIndex = reader.GetOrdinal("Icon");
                if (reader.Read()) {
                    object icon=reader.GetValue(iconColumnIndex);
                    if(DBNull.Value!=icon)
                        return (byte[]) icon;
                    else 
                        return (new byte[0]);
                }
                else {
                    return new byte[0];
                }
            }
        }

        public static FaceLayoutItem[] FaceLayoutItemsEnumByFaceLayoutId(FaceLayout faceLayout) {
            PrFaceLayoutItemEnumByFaceLayoutId prc = new PrFaceLayoutItemEnumByFaceLayoutId();
            prc.FaceLayoutId = faceLayout.Id;
            ArrayList list = new ArrayList();
            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int posColumnIndex = reader.GetOrdinal("Position");
                int textColumnIndex = reader.GetOrdinal("Text");
                while (reader.Read()) {
                    FaceLayoutItem faceLayoutItem = new FaceLayoutItem();
                    faceLayoutItem.Id = reader.GetInt32(idColumnIndex);
                    faceLayoutItem.Position = reader.GetByte(posColumnIndex);
                    faceLayoutItem.Text = reader.GetString(textColumnIndex);
                    list.Add(faceLayoutItem);
                }
            }
            return (FaceLayoutItem[])list.ToArray(typeof(FaceLayoutItem));
        }

        public static void Update(FaceLayoutItem faceLayoutItem) {
            PrFaceLayoutItemUpd prc = new PrFaceLayoutItemUpd();
            prc.FaceLayoutItemId = faceLayoutItem.Id;
            prc.Text = faceLayoutItem.Text;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Layout item was not updated.");
        }

        public static void FaceLayoutEnumById(FaceLayout faceLayout) {
            PrFaceLayoutEnumById prc = new PrFaceLayoutEnumById();
            prc.FaceLayoutId = faceLayout.Id;
            using (IDataReader reader = prc.ExecuteReader()) {
                int faceIdColumnIndex = reader.GetOrdinal("FaceId");
                int iconIdColumnIndex = reader.GetOrdinal("IconId");
                if (reader.Read()) {
                    int faceId = reader.GetInt32(faceIdColumnIndex);
                    faceLayout.Face.Id = faceId;
                    if(!reader.IsDBNull(iconIdColumnIndex)) {
                        int iconId = reader.GetInt32(iconIdColumnIndex);
                        faceLayout.ImageLibItem.Id = iconId;
                    }
                }
            }
        }

        public static int GetLayoutIconId(int faceId) {
            PrLayoutIconIdGetByFaceId prc = new PrLayoutIconIdGetByFaceId();
            prc.FaceId = faceId;
            using (IDataReader reader = prc.ExecuteReader()) {
                int iconIdColumnIndex = reader.GetOrdinal("IconId");
                if (reader.Read()) {
                    int iconId = reader.GetInt32(iconIdColumnIndex);
                    return iconId;
                }
            }

            return -1;
        }       
    }
}
