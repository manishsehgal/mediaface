using System;
using System.Data;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data
{

	public class DOTrackingImages
	{
		public DOTrackingImages(){}
        public static int Add(ImageLibItem image, string login, DateTime time) 
        {
            PrTrackingImagesIns prc = new PrTrackingImagesIns();
            prc.ImageId = image.Id;
            prc.Time = time;
            prc.Login = login;
            prc.ExecuteNonQuery();
            return prc.Id.Value;
        }

        public static DataSet GetImagesListByDate(DateTime startTime,DateTime endTime) 
        {
            PrTrackingImagesEnumByTime  prc = new PrTrackingImagesEnumByTime();
            prc.TimeEnd = endTime;
            prc.TimeStart = startTime;
            DataSet data = new DataSet();
            data.Tables.Add("Images");
            data.Tables.Add("Folders");
            return prc.LoadDataSet(data);
        }

     
	}
}
