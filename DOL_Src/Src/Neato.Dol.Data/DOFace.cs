using System;
using System.Collections;
using System.Data;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Data {
    public class DOFace {
        public DOFace() {}

        public static Face GetFace(FaceBase faceBase) {
            PrFaceGetById prc = new PrFaceGetById();
            prc.FaceId = faceBase.Id;
            Face face = null;
            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int contourColumnIndex = reader.GetOrdinal("Contour");
                int guidColumnIndex = reader.GetOrdinal("Guid");
                int nameColumnIndex = reader.GetOrdinal("FaceName");
                if (reader.Read()) {
                    face = new Face();
                    face.Id = reader.GetInt32(idColumnIndex);
                    face.AddName("",reader.GetString(nameColumnIndex));
                    object contourValue = reader.GetValue(contourColumnIndex);
                    if (DBNull.Value != contourValue) {
                        face.Contour = ConvertHelper.ByteArrayToXmlNode((byte[])contourValue);
                    }
                    face.Guid = reader.GetGuid(guidColumnIndex);
                }
            }
            return face;
        }

        public static void Add(Face face) {
            if (!face.IsNew) throw new InvalidOperationException("Adding Face entity in 'Non-New' status");

            PrFaceIns prc = new PrFaceIns();
            if (face.Contour != null) {
                prc.Contour = ConvertHelper.XmlNodeToByteArray(face.Contour);
            }

            prc.Guid = face.Guid;

            prc.ExecuteNonQuery();
            face.Id = prc.FaceId.Value;

            IDictionaryEnumerator names = face.GetNames();
            names.Reset();
            while (names.MoveNext()) {
                string culture = (string)names.Key;
                string faceName = (string)names.Value;
                AddName(face, culture, faceName);
            }
        }

        private static void AddName(FaceBase face, string culture, string faceName) {
            PrFaceLocalizationIns prc = new PrFaceLocalizationIns();
            prc.FaceId = face.Id;
            prc.Culture = culture;
            prc.FaceName = faceName;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Face name already exists.");
        }

        private static void DeleteName(FaceBase face, string culture) {
            PrFaceLocalizationDel prc = new PrFaceLocalizationDel();
            prc.FaceId = face.Id;
            if (culture != null) {
                prc.Culture = culture;
            }
            prc.ExecuteNonQuery();
        }

        public static void Update(Face face) {
            if (face.IsNew) throw new InvalidOperationException("Updating Face entity in 'New' status");
            PrFaceUpd prc = new PrFaceUpd();
            prc.FaceId = face.Id;

            if (face.Contour != null) {
                prc.Contour = ConvertHelper.XmlNodeToByteArray(face.Contour);
            }

            prc.Guid = face.Guid;

            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Face does not exist.");

            DeleteName(face, null);
            IDictionaryEnumerator names = face.GetNames();
            names.Reset();
            while (names.MoveNext()) {
                string culture = (string)names.Key;
                string faceName = (string)names.Value;
                AddName(face, culture, faceName);
            }
        }

        public static void Delete(FaceBase face) {
            DeleteName(face, null);
            PrFaceDel prc = new PrFaceDel();
            prc.FaceId = face.Id;
            prc.ExecuteNonQuery();
        }

        public static void BindToPaper(Face face, PaperBase paper) {
            PrFaceBindToPaper prc = new PrFaceBindToPaper();
            prc.FaceId = face.Id;
            prc.PaperId = paper.Id;
            prc.FaceX = face.Position.X;
            prc.FaceY = face.Position.Y;

            prc.ExecuteNonQuery();
        }

        public static void UnbindFromPaper(FaceBase face, PaperBase paper) {
            PrFaceUnbindFromPaper prc = new PrFaceUnbindFromPaper();
            if (face != null)
                prc.FaceId = face.Id;
            if (paper != null)
                prc.PaperId = paper.Id;
            prc.ExecuteNonQuery();
        }

        public static void BindToDevice(FaceBase face, DeviceBase device) {
            PrFaceBindToDevice prc = new PrFaceBindToDevice();
            prc.FaceId = face.Id;
            prc.DeviceId = device.Id;
            prc.ExecuteNonQuery();
        }

        public static void UnbindFromDevice(FaceBase face, DeviceBase device) {
            PrFaceUnbindFromDevice prc = new PrFaceUnbindFromDevice();
            if (face != null)
                prc.FaceId = face.Id;
            if (device != null)
                prc.DeviceId = device.Id;
            prc.ExecuteNonQuery();
        }

        public static int GetFaceIdByGuid(Guid guid) {
            PrFaceGetIdByGuid prc = new PrFaceGetIdByGuid();
            prc.Guid = guid;

            object id = prc.ExecuteScalar();
            if (id != null) {
                return (int)id;
            } else {
                throw new BusinessException(
                    string.Format("Label with GUID {0} is absent in DB.", guid));
            }
        }
        public static Face[] FaceEnum() {
            PrFaceEnum prc = new PrFaceEnum();

            ArrayList faces = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int faceNameColumnIndex = reader.GetOrdinal("FaceName");
                while (reader.Read()) {
                    Face face = new Face();
                    face.Id = reader.GetInt32(idColumnIndex);
                    face.AddName("", reader.GetString(faceNameColumnIndex));
                    faces.Add(face);
                }
            }
            return (Face[])faces.ToArray(typeof(Face));
        }
        public static Face[] EnumerateFaceByParam(DeviceBase device, DeviceType deviceType, string faceName) {
            PrFaceEnumByParam prc = new PrFaceEnumByParam();

            if (device != null) {
                prc.DeviceId = device.Id;
            }

            if (deviceType != DeviceType.Undefined) {
                prc.DeviceTypeId = (int)deviceType;
            }

            if (faceName != null) {
                prc.FaceName = faceName;
            }

            ArrayList faces = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int guidColumnIndex = reader.GetOrdinal("Guid");
                int faceNameColumnIndex = reader.GetOrdinal("FaceName");
                while (reader.Read()) {
                    Face face = new Face();
                    face.Id = reader.GetInt32(idColumnIndex);
                    face.Guid = reader.GetGuid(guidColumnIndex);
                    face.AddName("", reader.GetString(faceNameColumnIndex));
                    faces.Add(face);
                }
            }
            return (Face[])faces.ToArray(typeof(Face));
        }

        public static byte[] GetIcon(FaceBase faceBase) {
            PrFaceIconGetById prc = new PrFaceIconGetById();

            prc.FaceId = faceBase.Id;
            using (IDataReader reader = prc.ExecuteReader()) {
                int iconColumnIndex = reader.GetOrdinal("Icon");
                if (reader.Read()) {
                    object icon = reader.GetValue(iconColumnIndex);
                    if (DBNull.Value != icon)
                        return (byte[])icon;
                    else
                        return (new byte[0]);
                } else {
                    return new byte[0];
                }
            }
        }

        public static void UpdateIcon(FaceBase face, byte[] icon) {
            PrFaceIconUpd prc = new PrFaceIconUpd();
            prc.FaceId = face.Id;
            prc.Icon = icon;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Face does not exist.");
        }
    }
}