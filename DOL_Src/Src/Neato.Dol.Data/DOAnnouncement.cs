using Neato.Dol.Data.DBEngine;
using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity.DataBase;

namespace Neato.Dol.Data {
    public class DOAnnouncement {
        private DOAnnouncement() {}

        public static AnnouncementData EnumAnnouncements() {
            PrAnnouncementEnum prc = new PrAnnouncementEnum();
            return (AnnouncementData)prc.LoadDataSet(new AnnouncementData());
        }

        public static AnnouncementData GetLastAnnouncements() {
            PrAnnouncementEnumLast prc = new PrAnnouncementEnumLast();
            return (AnnouncementData)prc.LoadDataSet(new AnnouncementData());
        }

        public static void UpdateAnnouncement(AnnouncementData data) {
            PrAnnouncementIns prcIns = new PrAnnouncementIns();
            PrAnnouncementUpd prcUpd = new PrAnnouncementUpd();
            PrAnnouncementDel prcDel = new PrAnnouncementDel();
            ProcedureWrapper.UpdateDataTable(data.Announcement, prcIns, prcUpd, prcDel);
        }
    }
}