using Neato.Dol.Data.DBEngine;
using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Data.Scheme;

namespace Neato.Dol.Data {
    public class DOFaq {
        private DOFaq() {}

        public static FaqData EnumFaq() {
            PrFaqEnum prc = new PrFaqEnum();
            return (FaqData)prc.LoadDataSet(new FaqData());
        }

        public static void UpdateFaq(FaqData data) {
            PrFaqIns prcIns = new PrFaqIns();
            PrFaqUpd prcUpd = new PrFaqUpd();
            PrFaqDel prcDel = new PrFaqDel();
            ProcedureWrapper.UpdateDataTable(data.Faq, prcIns, prcUpd, prcDel);
        }
    }
}