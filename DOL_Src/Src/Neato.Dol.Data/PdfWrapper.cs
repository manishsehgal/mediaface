using System;
using System.Threading;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.util;
using System.Xml;

using iTextSharp.text;
using iTextSharp.text.pdf;

using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;
using Color = iTextSharp.text.Color;
using Font = iTextSharp.text.Font;
using Image = iTextSharp.text.Image;

namespace Neato.Dol.Data 
{
	public sealed class PdfWrapper 
	{

		private PdfWrapper() {}

		public static byte[] GeneratePdf(Project project, XmlNode paintTypesNode, XmlNode shapeTypesNode, XmlNode gradientTypesNode, XmlNode watermarkNode) 
		{
			XmlNode paperNode = project.ProjectXml.SelectSingleNode("//Paper");
			Paper paper = new Paper(paperNode);
			PdfCalibration calibration = new PdfCalibration();
			XmlNode calibrationNode = project.ProjectXml.SelectSingleNode("//Calibration");
			if (calibrationNode != null) 
			{
				float tmp = 0;
				XmlHelper.GetAttributeValue(calibrationNode, "x", ref tmp);
				if (tmp != 0)
					calibration.X = tmp;
				tmp = 0;
				XmlHelper.GetAttributeValue(calibrationNode, "y", ref tmp);
				if (tmp != 0)
					calibration.Y = tmp;
			}

			Face[] faces = GetFaces(project.ProjectXml, paper, calibration, shapeTypesNode, gradientTypesNode);

			iTextSharp.text.Rectangle pageSize = new iTextSharp.text.Rectangle(paper.PaperSize.Width, paper.PaperSize.Height);
			pageSize.BackgroundColor = new Color(0xFF, 0xFF, 0xFF);
			Document document = new Document(pageSize, 0f, 0f, 0f, 0f);

			MemoryStream pdfStream = new MemoryStream();

			try 
			{
				PdfWriter writer = PdfWriter.GetInstance(document, pdfStream);
				document.Open();

				PdfContentByte pdfContent = writer.DirectContent;

				foreach (Face face in faces) 
				{
					face.Draw(pdfContent, project);
				}
				DrawWaterMark(pdfContent, project, watermarkNode);

				document.Add(new Paragraph(" "));
                
			} 
			catch (DocumentException) 
			{
				throw;
			} 
			catch (IOException) 
			{
				throw;
			} 
			finally 
			{
            
				document.Close();
			}
			return pdfStream.ToArray();
		}

		public static void DrawWaterMark(PdfContentByte pdfContent, Project project, XmlNode watermarkNode) 
		{
			if (Thread.CurrentPrincipal.IsInRole(CustomerGroup.MFOPE.ToString()))
				return;
			Entity.Paper paper = DOPaper.GetPaper(project.ProjectPaper);
			if (Thread.CurrentPrincipal.IsInRole(CustomerGroup.MFO.ToString()) && (paper.Brand.Name.ToLower() == "neato" && project.DeviceType != DeviceType.DirectToCD && project.DeviceType != DeviceType.Lightscribe))
				return;

			WatermarkPrimitive p = new WatermarkPrimitive(watermarkNode);
			PointF point = new PointF();
			p.Draw(pdfContent, new PointF(), ref point, project);
		}

		#region Paper
		private class Paper 
		{
			private SizeF paperSizeValue = new SizeF();
			private PaperType paperTypeValue = PaperType.DieCut;

			public SizeF PaperSize 
			{
				get { return paperSizeValue; }
			}

			public PaperType PaperType 
			{
				get { return paperTypeValue; }
			}

			public Paper(XmlNode node) 
			{
				if (node != null) 
				{
					XmlHelper.GetAttributeValue(node, "w", "h", ref paperSizeValue);
					string paperType = string.Empty;
					XmlHelper.GetAttributeValue(node, "PaperType", ref paperType);
					if(paperType != null)
						paperTypeValue = (PaperType)PaperType.Parse(typeof(PaperType), paperType);
				}
			}
		}

		#endregion

		#region Fonts

		struct NodeStyle 
		{
			public Color textColor;
			public bool italic;
			public bool bold;
			public bool underline;
			public int fontSize;
			public string font;
		}

		private sealed class FontHelper 
		{
			private static Hashtable fontTable;
			private const string FontsXmlFile = "fonts.xml";
			private const string NameAttributeName = "name";
			private const string TtfAttributeName = "ttf";

			static FontHelper() 
			{
				fontTable = new Hashtable();
				XmlDocument fonts = new XmlDocument();

				string fontXMLPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FontsXmlFile);

				fonts.Load(fontXMLPath);

				foreach (XmlNode node in fonts.FirstChild.ChildNodes) 
				{
					string name = node.Attributes[NameAttributeName ].Value.ToLower();
					foreach (XmlNode styleNode in node.ChildNodes) 
					{
						bool italic = false;
						bool bold = false;
						switch (styleNode.Name) 
						{
							case "italic":
								italic = true;
								break;
							case "bold":
								bold = true;
								break;
							case "bold-italic":
								italic = true;
								bold = true;
								break;
							case "normal":
							default:
								italic = false;
								bold = false;
								break;
						}
						string ttf = styleNode.Attributes[TtfAttributeName].Value.ToLower();
						fontTable[CreateKey(name, italic, bold)] = ttf;
					}
				}
			}

			private static string CreateKey(string fontId, bool italic, bool bold) 
			{
				return string.Format(CultureInfo.InvariantCulture, "{0}.{1}.{2}", fontId, italic, bold);
			}

			public static string GetFontPath(string fontId, bool italic, bool bold) 
			{
				if (fontTable[CreateKey(fontId.ToLower(), true, false)] == null)
					italic = false;
				if (fontTable[CreateKey(fontId.ToLower(), false, true)] == null)
					bold = false;
				string fontRelativePath = (String)fontTable[CreateKey(fontId.ToLower(), italic, bold)];
				return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fontRelativePath);
			}
		}
		#endregion

		#region Primitives
		private interface IDraw 
		{
			void Draw(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project);
		}




		#region DrawingPrimitives
		private class FillCommand : IDraw 
		{
			private Color color = Color.BLACK;
			private Color color2 = Color.BLACK;
			private SizeF paperSize;
			private string gradientType;

			private int points;
			private string dir;
			private float scaleX;
			private float scaleY;
			private PointF deltaPoint = new PointF();

			public FillCommand(XmlNode node, SizeF paperSize, XmlNode gradientTypesNode) 
			{
				if (node != null) 
				{
					XmlHelper.GetAttributeValue(node, "color", ref color);
					XmlHelper.GetAttributeValue(node, "color2", ref color2);
					XmlHelper.GetAttributeValue(node, "gradientType", ref gradientType);
					this.paperSize = paperSize;

					if (gradientType == null)
						gradientType = "00";

					if (gradientType != "00") 
					{
						string gradientTypeXpath = string.Format(@"/GradientTypes/Type[@name='{0}']", gradientType);
						XmlNode gradientNode = gradientTypesNode.SelectSingleNode(gradientTypeXpath);
						XmlHelper.GetAttributeValue(gradientNode, "points", ref points);
						XmlHelper.GetAttributeValue(gradientNode, "dir", ref dir);

						float width = 0;
						float height = 0;
						XmlHelper.GetAttributeValue(node, "width", ref width);
						XmlHelper.GetAttributeValue(node, "height", ref height);
						XmlHelper.GetAttributeValue(node, "x", "y", ref deltaPoint);

						float gradientWidth = 100;
						float gradientHeight = 100;
						scaleX = (width / gradientWidth)   * 100f;
						scaleY = (height / gradientHeight) * 100f;
					}
				}
			}

			public void Draw(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project) 
			{
				PdfTemplate pdfTemplate = pdfContent.CreateTemplate(paperSize.Width, paperSize.Height);

				if (gradientType == "00") 
				{
					pdfTemplate.SetColorFill(color);
					pdfTemplate.Rectangle(0f, 0f, paperSize.Width, paperSize.Height);
					pdfTemplate.Fill();
					pdfTemplate.ResetRGBColorFill();
				}
				else 
				{
					System.Drawing.Color c1 = System.Drawing.Color.FromArgb(color.R, color.G, color.B);
					System.Drawing.Color c2 = System.Drawing.Color.FromArgb(color2.R, color2.G, color2.B);
					GradientPrimitive gp = new GradientPrimitive(deltaPoint, 100, scaleX, scaleY, 0, dir, points, c1, c2);
					gp.Draw(pdfTemplate, originPoint, ref currentPoint, project);
				}

				pdfContent.AddTemplate(pdfTemplate, 0.0f, 0.0f);
			}
		}

		private class CutLinePrimitive : IDraw 
		{
			private const string Left = "left";
			private const string Right = "right";
			private const string Top = "top";
			private const string Bottom = "bottom";
			private PointF startPoint = new PointF();
			private PointF endPoint = new PointF();
			private string scissors = Right;

			public CutLinePrimitive(XmlNode node) 
			{
				if (node != null) 
				{
					XmlHelper.GetAttributeValue(node, "x1", "y1", ref startPoint);
					XmlHelper.GetAttributeValue(node, "x2", "y2", ref endPoint);
					string tmp = string.Empty;
					XmlHelper.GetAttributeValue(node, "scissors", ref tmp);
					if (tmp.ToLower() == Left)
						scissors = Left;
					else if (tmp.ToLower() == Top)
						scissors = Top;
					else if (tmp.ToLower() == Bottom)
						scissors = Bottom;
				}
			}

			public void Draw(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project) 
			{
				float DocumentHeight = pdfContent.PdfDocument.Top;
				pdfContent.MoveTo(startPoint.X + originPoint.X, DocumentHeight - (startPoint.Y + originPoint.Y));
				pdfContent.LineTo(endPoint.X + originPoint.X, DocumentHeight - (endPoint.Y + originPoint.Y));
				pdfContent.Stroke();
                
				string imgPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images/Scissors.wmf");
				Image img = Image.GetInstance(imgPath);
				PointF imgPoint = new PointF();
				if (scissors == Left) 
				{
					if (startPoint.X < endPoint.X) 
					{
						imgPoint.X = startPoint.X;
						imgPoint.Y = startPoint.Y;
					} 
					else 
					{
						imgPoint.X = endPoint.X;
						imgPoint.Y = endPoint.Y;
					}
					imgPoint.X -= img.Width;
					imgPoint.Y += img.Height / 2;
					img.RotationDegrees = 180;
				} 
				else if (scissors == Right) 
				{
					if (startPoint.X > endPoint.X) 
					{
						imgPoint.X = startPoint.X;
						imgPoint.Y = startPoint.Y;
					} 
					else 
					{
						imgPoint.X = endPoint.X;
						imgPoint.Y = endPoint.Y;
					}
					imgPoint.X += 0;
					imgPoint.Y += img.Height / 2;
					img.RotationDegrees = 0;
				} 
				else if (scissors == Top) 
				{
					if (startPoint.Y < endPoint.Y) 
					{
						imgPoint.X = startPoint.X;
						imgPoint.Y = startPoint.Y;
					} 
					else 
					{
						imgPoint.X = endPoint.X;
						imgPoint.Y = endPoint.Y;
					}
					imgPoint.X -= img.Height / 2;
					imgPoint.Y += 0;
					img.RotationDegrees = 90;
				} 
				else if (scissors == Bottom) 
				{
					if (startPoint.Y > endPoint.Y) 
					{
						imgPoint.X = startPoint.X;
						imgPoint.Y = startPoint.Y;
					} 
					else 
					{
						imgPoint.X = endPoint.X;
						imgPoint.Y = endPoint.Y;
					}
					imgPoint.X -= img.Height / 2;
					imgPoint.Y += img.Width;
					img.RotationDegrees = 270;
				}
				img.SetAbsolutePosition(imgPoint.X + originPoint.X, DocumentHeight - (imgPoint.Y + originPoint.Y));
				pdfContent.AddImage(img, true);
			}
		}

		private class MoveToCommand : IDraw 
		{
			private PointF deltaPoint;

			public MoveToCommand(XmlNode node) 
			{
				if (node != null) 
				{
					XmlHelper.GetAttributeValue(node, "x", "y", ref deltaPoint);
				}
			}

			public void Draw(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project) 
			{
				PointF point = new PointF(originPoint.X + deltaPoint.X, originPoint.Y + deltaPoint.Y);
				float DocumentHeight = pdfContent.PdfDocument.Top;
				pdfContent.MoveTo(point.X, DocumentHeight - point.Y);
				currentPoint = point;
			}
		}

		private class LineToCommand : IDraw 
		{
			private PointF anchorPoint;

			public LineToCommand(XmlNode node) 
			{
				if (node != null) 
				{
					XmlHelper.GetAttributeValue(node, "x", "y", ref anchorPoint);
				}
			}

			public void Draw(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project) 
			{
				PointF endPoint = new PointF(originPoint.X + anchorPoint.X, originPoint.Y + anchorPoint.Y);
				float DocumentHeight = pdfContent.PdfDocument.Top;
				pdfContent.LineTo(endPoint.X, DocumentHeight - endPoint.Y);
				currentPoint = endPoint;
			}
		}

		private class CurveToCommand : IDraw 
		{
			private PointF anchorPoint;
			private PointF controlPoint;

			public CurveToCommand(XmlNode node) 
			{
				if (node != null) 
				{
					XmlHelper.GetAttributeValue(node, "x", "y", ref anchorPoint);
					XmlHelper.GetAttributeValue(node, "cx", "cy", ref controlPoint);
				}
			}

			public void Draw(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project) 
			{
				PointF endPoint = new PointF(originPoint.X + anchorPoint.X, originPoint.Y + anchorPoint.Y);

				PointF controlPoint1 = new PointF();
				controlPoint1.X = (currentPoint.X + 2 * (originPoint.X + controlPoint.X)) / 3;
				controlPoint1.Y = (currentPoint.Y + 2 * (originPoint.Y + controlPoint.Y)) / 3;

				PointF controlPoint2 = new PointF();
				controlPoint2.X = ((originPoint.X + anchorPoint.X) + 2 * (originPoint.X + controlPoint.X)) / 3;
				controlPoint2.Y = ((originPoint.Y + anchorPoint.Y) + 2 * (originPoint.Y + controlPoint.Y)) / 3;

				float DocumentHeight = pdfContent.PdfDocument.Top;
				pdfContent.CurveTo(controlPoint1.X, DocumentHeight - controlPoint1.Y, controlPoint2.X, DocumentHeight - controlPoint2.Y, endPoint.X, DocumentHeight - endPoint.Y);
				currentPoint = endPoint;
			}
		}

		private class RectanglePrimitive : IDraw 
		{
			private RectangleF rectangle = new RectangleF();

			public RectanglePrimitive(XmlNode node) 
			{
				if (node != null) 
				{
					XmlHelper.GetAttributeValue(node, "x", "y", "w", "h", ref rectangle);
				}
			}

			public void Draw(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project) 
			{
				float DocumentHeight = pdfContent.PdfDocument.Top;
				pdfContent.Rectangle(rectangle.X + originPoint.X, DocumentHeight - (rectangle.Y + originPoint.Y),
					rectangle.Width, -rectangle.Height);
			}
		}

		private class CirclePrimitive : IDraw 
		{
			private PointF center = new PointF();
			private float radius;
			private float borderWidth;
			private Color borderColor = Color.BLACK;
			private bool isFilled = false;
			private Color fillColor = Color.BLACK;

			public CirclePrimitive(XmlNode node) 
			{
				if (node != null) 
				{
					XmlHelper.GetAttributeValue(node, "x", "y", ref center);
					XmlHelper.GetAttributeValue(node, "r", ref radius);

					XmlNode style = node.SelectSingleNode("Style");
					if (style != null) 
					{
						XmlHelper.GetAttributeValue(style, "color", ref borderColor);
						XmlHelper.GetAttributeValue(style, "width", ref borderWidth);
					}

					XmlNode fill = node.SelectSingleNode("Fill");
					if (fill != null) 
					{
						isFilled = true;
						XmlHelper.GetAttributeValue(fill, "color", ref fillColor);
					}
				}
			}

			public void Draw(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project) 
			{
				pdfContent.SetLineWidth(borderWidth);
				pdfContent.SetColorStroke(borderColor);
				float DocumentHeight = pdfContent.PdfDocument.Top;
				pdfContent.Circle(center.X + originPoint.X, DocumentHeight - (center.Y + originPoint.Y), radius);
				if (isFilled) 
				{
					pdfContent.SetColorFill(fillColor);
					pdfContent.FillStroke();
					pdfContent.ResetRGBColorFill();
				}
				pdfContent.ResetRGBColorStroke();
			}

		}
		private static IDraw DrawingPrimitive(XmlNode nodePrimitive) 
		{
			switch (nodePrimitive.Name) 
			{
				case "MoveTo":
					return new MoveToCommand(nodePrimitive);
				case "LineTo":
					return new LineToCommand(nodePrimitive);
				case "CurveTo":
					return new CurveToCommand(nodePrimitive);
				case "Rectangle":
					return new RectanglePrimitive(nodePrimitive);
				case "Circle":
					return new CirclePrimitive(nodePrimitive);
				default:
					throw new NotImplementedException();
			}
		}

		#endregion DrawingPrimitives

		#region Contour
		public enum ContourPart { Contour, RealContour, MarkLines, CutLines };

		private interface IDrawContourPart 
		{
			void DrawContourPart(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project,
				ContourPart contourPart, PaperType paperType);
		}

		private class Contour : IDrawContourPart
		{
			private string id;
			private PointF deltaPoint = new PointF();
			private ArrayList outlineContour = new ArrayList();
			private ArrayList realContour = new ArrayList();
			private ArrayList markLines = new ArrayList();
			private ArrayList cutLines = new ArrayList();

			public Contour(XmlNode node) 
			{
				if (node == null)
					return;
				outlineContour = new ArrayList();

				XmlHelper.GetAttributeValue(node, "id", ref id);
				XmlHelper.GetAttributeValue(node, "x", "y", ref deltaPoint);    
				for (XmlNode nodePrimitive = node.FirstChild; 
					nodePrimitive != null; 
					nodePrimitive = nodePrimitive.NextSibling) 
				{
					switch (nodePrimitive.Name) 
					{
						case "RealContour":
							for (XmlNode nodePr = nodePrimitive.FirstChild; 
								nodePr != null; nodePr = nodePr.NextSibling) 
							{
								realContour.Add(DrawingPrimitive(nodePr));
							}
							break;
						case "MarkLines":
							// ignore mark lines during print. If neccessary, uncomment next codes:
							/*
							for (XmlNode nodePr = nodePrimitive.FirstChild; 
								nodePr != null; nodePr = nodePr.NextSibling) 
							{
								realContour.Add(DrawingPrimitive(nodePr));
							}
							*/
							break;
						case "CutLine":
							cutLines.Add(new CutLinePrimitive(nodePrimitive));
							break;
						default:	// this is "main" contour
							outlineContour.Add(DrawingPrimitive(nodePrimitive));
							break;
					}
				}
			}

			public void DrawContourPart(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project,
				ContourPart contourPart, PaperType paperType) 
			{
				ArrayList contour = null;
				switch(contourPart)
				{
					case ContourPart.Contour:
						contour = (paperType == PaperType.Universal) && (realContour.Count > 0) ?
						realContour : outlineContour ;
						break;
					case ContourPart.RealContour:
						contour = (paperType == PaperType.Universal) ? null :	realContour;
						break;
					case ContourPart.MarkLines: 
						contour = markLines;
						break;
					case ContourPart.CutLines:
						contour = cutLines;
						break;
				}
				if(contour != null) 
				{
					PointF startPoint = new PointF(originPoint.X + deltaPoint.X, originPoint.Y + deltaPoint.Y);
					foreach (IDraw draw in contour) 
					{
						if (draw != null) 
						{
							draw.Draw(pdfContent, startPoint, ref currentPoint, project);
						}
					}
				}
			}
		}

		#endregion Contour

		private class TextPrimitive : IDraw 
		{
			private PointF startPoint = new PointF();
			private string text;
			private bool isMultiline = false;
			private float multilineWidth = 100f;
			private int align = Element.ALIGN_LEFT;
			private float xscale = 1;
			private float yscale = 1;
			private float angle = 0f;
			private float innerX=0f;
			private float innerY=0f;
			private bool visible = true;
			private bool emptyLayoutItem = false;
			private bool isEffectMode = false;
			private string contour ="E";
			private Color color;
			private float width,height;
			private ArrayList styles = new ArrayList();

			public TextPrimitive(XmlNode node) 
			{
				if (node != null) 
				{
					XmlHelper.GetAttributeValue(node, "x", "y", ref startPoint);
					XmlHelper.GetAttributeValue(node, "text", ref text);
					XmlHelper.GetAttributeValue(node, "isMultiline", ref isMultiline);
					XmlHelper.GetAttributeValue(node, "multilineWidth", ref multilineWidth);
					string alignment = string.Empty;
					XmlHelper.GetAttributeValue(node, "align", ref alignment);
					if (Util.EqualsIgnoreCase(alignment, ElementTags.ALIGN_CENTER))
						align = Element.ALIGN_CENTER;
					else if (Util.EqualsIgnoreCase(alignment, ElementTags.ALIGN_RIGHT))
						align = Element.ALIGN_RIGHT;

					XmlHelper.GetAttributeValue(node, "scaleX", ref xscale);
					XmlHelper.GetAttributeValue(node, "scaleY", ref yscale);
					xscale /= 100;
					yscale /= 100;
					XmlHelper.GetAttributeValue(node, "visible", ref visible);
					XmlHelper.GetAttributeValue(node, "emptyLayoutItem", ref emptyLayoutItem);
					XmlNode contourNode = node.SelectSingleNode("Contour");
					if(contourNode == null) 
					{
						isEffectMode = false;
						XmlNodeList styleNodes = node.SelectNodes("Style");

						foreach(XmlNode style in styleNodes) 
						{
							NodeStyle letterStyle = new NodeStyle();

							XmlHelper.GetAttributeValue(style, "font", ref letterStyle.font);
							XmlHelper.GetAttributeValue(style, "size", ref letterStyle.fontSize);
							XmlHelper.GetAttributeValue(style, "bold", ref letterStyle.bold);
							XmlHelper.GetAttributeValue(style, "italic", ref letterStyle.italic);
							XmlHelper.GetAttributeValue(style, "underline", ref letterStyle.underline);
							XmlHelper.GetAttributeValue(style, "color", ref letterStyle.textColor);

							styles.Add(letterStyle);
						}
					}
					else 
					{
						isEffectMode = true;
						contour = contourNode.InnerText;
						XmlHelper.GetAttributeValue(contourNode, "color", ref color);
						XmlHelper.GetAttributeValue(contourNode, "width", ref width);
						XmlHelper.GetAttributeValue(contourNode, "height", ref height);
						//  height /= yscale;
						// width /= xscale;
						XmlHelper.GetAttributeValue(contourNode, "x", ref innerX);
						XmlHelper.GetAttributeValue(contourNode, "y", ref innerY);
                            
					}
					XmlNode rotate = node.SelectSingleNode("Rotate");
					if (rotate != null) 
					{
						XmlHelper.GetAttributeValue(rotate, "angle", ref angle);
						angle = - angle / 180 * (float)Math.PI;
					}
				}
			}


			private string GetTextSuitableFlash(int letterPos, char letter) 
			{
				string tmpText;
				if (letterPos == 0) 
					tmpText = string.Format(" {0}", letter);
				else if (letterPos == text.Length - 1)
					tmpText = string.Format("{0} ", letter);
				else tmpText = letter.ToString();
				return tmpText;
			}

			public static int atoi(string s, int radix)
				// assumed that s does not contain sign, i.e. number is non-negative
			{
				if((radix < 2) || (radix > 36))
					throw new System.ArgumentException("'range' should be between 2 and 36");

				int res = 0;
				int lastChSmall = 'a'-10+radix;
				int lastChLarge = 'A'-10+radix;

				for(int i=0;i<s.Length;i++) 
				{
					res *= radix;
					char c = s[i];
					if((c>='a') && (c<lastChSmall))
						res += c-('a'-10);
					else if((c>='A') && (c<lastChLarge))
						res += c-('A'-10);
					else if((c>='0') && (c<='9'))
						res += c-'0';
					else
						throw new System.FormatException("invalid int format for radix "+radix.ToString()+": "+s);
				}
				return res;
			}

			private static int atoi2(string s, int radix) 
				// sign is stored as parity in s representation, module is multiplied by 2
			{
				int res = atoi(s,radix);
				if((res & 0x1) != 0)
					res = 1-res;
				return res/2;
			}

			public void Draw(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project) 
			{
				if (!visible || emptyLayoutItem) return;
				float textHeight = 0.0f;
				float totalWidth = 0.0f;
				float fontSize = 0.0f;
				float maxDes = 0.0f;

				//Calculate baseLine ('des' of Max height letter) & textHeight (size=asc - des) of Max height letter
				PdfTemplate t;
				if(!isEffectMode) 
				{
					Phrase phrase = new Phrase();
					for (int i = 0; i < text.Length; i++) 
					{
						NodeStyle style = (NodeStyle)styles[i];

						fontSize = Math.Max(fontSize, style.fontSize);
						BaseFont bf = BaseFont.CreateFont(FontHelper.GetFontPath(style.font, style.italic, style.bold),
							BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

						maxDes = Math.Min(maxDes, bf.GetFontDescriptor(BaseFont.AWT_DESCENT, style.fontSize));
					}

					for (int i = 0; i < text.Length; i++) 
					{
						NodeStyle style = (NodeStyle)styles[i];

						BaseFont bf = BaseFont.CreateFont(FontHelper.GetFontPath(style.font, style.italic, style.bold),
							BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
						Font font = new Font(bf, style.fontSize);
                        
						font.Color = style.textColor;
                        
						Chunk ch = null;
						if (isMultiline)
							ch = new Chunk(text[i], font);
						else
							ch = new Chunk(GetTextSuitableFlash(i, text[i]), font);

						if (style.underline) 
						{
							ch.SetUnderline(1, maxDes / 2);
						}

						phrase.Add(ch);
						totalWidth += ch.GetWidthPoint();
					}

					PdfPTable table = new PdfPTable(1);
					PdfPCell cell = new PdfPCell(phrase);
					cell.Border = 0;
					cell.Padding = 0.0f;
					cell.HorizontalAlignment = align;
					table.AddCell(cell);
					if (isMultiline)
						totalWidth = multilineWidth - fontSize/4;
					table.TotalWidth = totalWidth;
					textHeight = table.TotalHeight;


					PdfTemplate template = pdfContent.CreateTemplate(totalWidth, textHeight);
					table.WriteSelectedRows(0, -1, isMultiline ? fontSize/8 : 0, textHeight, template);
                    
					t = template;
					float a = xscale * (float)Math.Cos(angle);
					float b = yscale * (float)Math.Sin(angle);
					float c = -xscale * (float)Math.Sin(angle);
					float d = yscale * (float)Math.Cos(angle);
					float e = startPoint.X + originPoint.X + (float)Math.Sin(angle) * textHeight;
					float DocumentHeight = pdfContent.PdfDocument.Top;
					float f = DocumentHeight - (startPoint.Y + originPoint.Y + textHeight - textHeight * (1 - (float)Math.Cos(angle)));

					pdfContent.AddTemplate(t, a, b, c, d, e, f);
				} 
				else 
				{
					float dx=innerX,dy=innerY;
					//     innerX = innerX < 0 ? innerX : 0;
					//     innerY = innerY < 0 ? innerY : 0;
					float tWidth = (width); //-innerX) ;//* xscale;
					float tHeight = (height);// - innerY);// * yscale;
                
					PdfTemplate template = pdfContent.CreateTemplate(tWidth, tHeight);
					string cont = contour;
					template.SetColorFill(color);
					template.SetColorStroke(color);
					template.SetLineWidth(0.0f);
					height+=innerY;
					if(cont.StartsWith("Ver:")) 
					{
						double ver = 1.00;
						bool errorInContour = false;

						int pos= 3;
						int p0 = 4;
						int p1 = cont.IndexOf(":", p0);

						if (p1 == -1) 
							errorInContour = true;
						else 
						{
							ver = double.Parse(cont.Substring(p0, p1-p0), CultureInfo.InvariantCulture);
							pos = p1;
						}

						// TBD: chk ver

						int radix = 10;
						int scaleX = 1;
						int scaleY = 1;

						int X = 0;
						int Y = 0;

						while (!errorInContour) 
						{
							char code = cont[++pos];
							pos++;
							if (code == 'E') break;
					
							switch(code) 
							{
								case 'B': 
								{
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									int delta = atoi2(cont.Substring(p0, p1-p0),radix);
									float x1 = (X+=delta)/(float)scaleX;
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									delta = atoi2(cont.Substring(p0, p1-p0),radix);
									float y1 = (Y+=delta)/(float)scaleY;
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									delta = atoi2(cont.Substring(p0, p1-p0),radix);
									float x2 = (X+=delta)/(float)scaleX;
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									delta = atoi2(cont.Substring(p0, p1-p0),radix);
									float y2 = (Y+=delta)/(float)scaleY;
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									delta = atoi2(cont.Substring(p0, p1-p0),radix);
									float x3 = (X+=delta)/(float)scaleX;
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									delta = atoi2(cont.Substring(p0, p1-p0),radix);
									float y3 = (Y+=delta)/(float)scaleY;
									pos = p1;
									//DrawCubicBezier2(template,new PointF(x0,originPoint.Y - y0), new PointF(x1,originPoint.Y - y1),new PointF(x2,originPoint.Y - y2),new PointF(x3,originPoint.Y - y3) );
									//template.MoveTo(x0, originPoint.Y -y0);
									template.CurveTo(x1-dx, height -y1,x2-dx,height -y2,x3-dx,height -y3);
									//GeometryHelper.DrawCubicBezier2(mc,{x:x0, y:y0}, {x:x1, y:y1}, {x:x2, y:y2}, {x:x3, y:y3});
									break;
								}
								case 'M': 
								{
									// template.Fill();
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									int delta = atoi2(cont.Substring(p0, p1-p0),radix);
									float x = (X+=delta)/(float)scaleX;
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									delta = atoi2(cont.Substring(p0, p1-p0),radix);
									float y = (Y+=delta)/(float)scaleY;
									pos = p1;
							
									template.MoveTo(x - dx,height - y);
									break;
								}
								case 'L': 
								{
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									int delta = atoi2(cont.Substring(p0, p1-p0),radix);
									float x = (X+=delta)/(float)scaleX;
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									delta = atoi2(cont.Substring(p0, p1-p0),radix);
									float y = (Y+=delta)/(float)scaleY;
									pos = p1;
							
									template.LineTo(x - dx, height -y);
									break;
								}
								case 'C': 
								{
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									int delta = atoi2(cont.Substring(p0, p1-p0),radix);
									float cx = (X+=delta)/(float)scaleX;
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									delta = atoi2(cont.Substring(p0, p1-p0),radix);
									float cy = (Y+=delta)/(float)scaleY;
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									delta = atoi2(cont.Substring(p0, p1-p0),radix);
									float x = (X+=delta)/(float)scaleX;
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									delta = atoi2(cont.Substring(p0, p1-p0),radix);
									float y = (Y+=delta)/(float)scaleY;
									pos = p1;
							
									template.CurveTo(cx-dx,height  - cy, x-dx, height - y);
									break;
								}
								case 'R': 
								{
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									radix = atoi(cont.Substring(p0, p1-p0),10);
									pos = p1;
									break;
								}
								case 'S': 
								{
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									scaleX = atoi(cont.Substring(p0, p1-p0),radix);
									pos = p1;

									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									scaleY = atoi(cont.Substring(p0, p1-p0),radix);
									pos = p1;

									break;
								}
								default:
									errorInContour = true;
									break;
							}
						}
					} 
					else 
					{ // old format, may be present in saved projects or projects creaded at MacOS
						int pos= 0;
						int p0 = 0;
						int p1= 0;
						while (true) 
						{
							char code = cont[pos];
							pos++;
							if (code == 'E') break;
					
							switch(code) 
							{
								case 'B': 
								{
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									float x1 = float.Parse(cont.Substring(p0, p1-p0), CultureInfo.InvariantCulture);
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									float y1 = float.Parse(cont.Substring(p0, p1-p0), CultureInfo.InvariantCulture);
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									float x2 = float.Parse(cont.Substring(p0, p1-p0), CultureInfo.InvariantCulture);
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									float y2 = float.Parse(cont.Substring(p0, p1-p0), CultureInfo.InvariantCulture);
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									float x3 = float.Parse(cont.Substring(p0, p1-p0), CultureInfo.InvariantCulture);
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									float y3 = float.Parse(cont.Substring(p0, p1-p0), CultureInfo.InvariantCulture);
									pos = p1;
									//DrawCubicBezier2(template,new PointF(x0,originPoint.Y - y0), new PointF(x1,originPoint.Y - y1),new PointF(x2,originPoint.Y - y2),new PointF(x3,originPoint.Y - y3) );
									//template.MoveTo(x0, originPoint.Y -y0);
									template.CurveTo(x1-dx, height -y1,x2-dx,height -y2,x3-dx,height -y3);
									//GeometryHelper.DrawCubicBezier2(mc,{x:x0, y:y0}, {x:x1, y:y1}, {x:x2, y:y2}, {x:x3, y:y3});
									break;
								}
								case 'M': 
								{
									// template.Fill();
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									float x = float.Parse(cont.Substring(p0, p1-p0), CultureInfo.InvariantCulture);
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									float y = float.Parse(cont.Substring(p0, p1-p0), CultureInfo.InvariantCulture);
									pos = p1;
							
									template.MoveTo(x - dx,height - y);
									break;
								}
								case 'L': 
								{
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									float x = float.Parse(cont.Substring(p0, p1-p0), CultureInfo.InvariantCulture);
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									float y = float.Parse(cont.Substring(p0, p1-p0), CultureInfo.InvariantCulture);
									pos = p1;
							
									template.LineTo(x - dx, height -y);
									break;
								}
								case 'C': 
								{
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									float cx = float.Parse(cont.Substring(p0, p1-p0), CultureInfo.InvariantCulture);
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									float cy = float.Parse(cont.Substring(p0, p1-p0), CultureInfo.InvariantCulture);
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									float x = float.Parse(cont.Substring(p0, p1-p0), CultureInfo.InvariantCulture);
									pos = p1;
							
									p0 = pos + 1;
									p1 = cont.IndexOf(":", p0);
									if (p1 == -1) break;
									float y = float.Parse(cont.Substring(p0, p1-p0), CultureInfo.InvariantCulture);
									pos = p1;
							
									template.CurveTo(cx-dx,height  - cy, x-dx, height - y);
									break;
								}
							}
						}
					}
					template.Fill();
					height-=innerY;
					/*   template.MoveTo(0,0);
					   template.LineTo(width,0);
					   template.LineTo(width,height);
					   template.LineTo(0,height);
					   template.LineTo(0,0);
					   */

                    
					/*template.MoveTo(innerX,innerY);
					template.LineTo(width+innerX,innerY);
					template.LineTo(width+innerX,height+innerY);
					template.LineTo(innerX,height+innerY);
					template.LineTo(innerX,innerY);*/
                    
					/* template.MoveTo(0,0);
					 template.LineTo((width-innerX),0);
					 template.LineTo((width-innerX),(height-innerY));
					 template.LineTo(0,(height-innerY));
					 template.LineTo(0,0);
					 template.Stroke();*/
					t = template;
					float a = xscale * (float)Math.Cos(angle);
					float b = xscale * (float)Math.Sin(angle);
					float c = -yscale * (float)Math.Sin(angle);
					float d = yscale * (float)Math.Cos(angle);
					float containerHeight = height;//tHeight*yscale;//(height-innerY);
					/*   float e = startPoint.X + originPoint.X + innerX*(1-(float)Math.Sin(angle)*xscale) + (float)Math.Sin(angle) * containerHeight; //+ (float)Math.Cos(angle)*innerX*xscale;
					   float DocumentHeight = pdfContent.PdfDocument.Top;
					   //float f = DocumentHeight - (startPoint.Y + originPoint.Y -innerY*(float)Math.Cos(angle)*yscale+containerHeight  *(float)Math.Cos(angle));
					   float f = DocumentHeight - (startPoint.Y + originPoint.Y+innerY*yscale*(1-(float)Math.Cos(angle)) + containerHeight*(float)Math.Cos(angle));
   */                  
					float ax=dx*(float)Math.Cos(angle)*xscale+dy*(float)Math.Sin(angle)*yscale+  (float)Math.Sin(angle) * containerHeight*yscale;
					float ay=(-dx)*(float)Math.Sin(angle)*xscale+(height+dy)*(float)Math.Cos(angle)*yscale;//+ containerHeight* (float)Math.Cos(angle)*yscale;
					float e = startPoint.X + (originPoint.X+ax);//dx*(float)Math.Cos(angle)*xscale-dy*(float)Math.Sin(angle)*yscale) + (float)Math.Sin(angle) * containerHeight;
					float DocumentHeight = pdfContent.PdfDocument.Top;
					float f = DocumentHeight - (startPoint.Y + originPoint.Y+ay);//dx*(float)Math.Sin(angle)*xscale+dy*(float)Math.Cos(angle)*yscale + containerHeight* (float)Math.Cos(angle)*yscale);
					pdfContent.AddTemplate(t, a, b, c, d, e, f);
					/*pdfContent.MoveTo(startPoint.X,DocumentHeight-startPoint.Y);
					pdfContent.LineTo(startPoint.X+100,DocumentHeight-startPoint.Y);
					pdfContent.MoveTo(startPoint.X,DocumentHeight-startPoint.Y);
					pdfContent.LineTo(startPoint.X,DocumentHeight-startPoint.Y+100);*/
					/*  pdfContent.SetColorStroke(new Color(0,127,255));
					  pdfContent.MoveTo(startPoint.X+ originPoint.X+ax,DocumentHeight-startPoint.Y- originPoint.Y-ay);
					  pdfContent.LineTo(startPoint.X+ originPoint.X+100+ax,DocumentHeight-startPoint.Y- originPoint.Y-ay);
					  pdfContent.MoveTo(startPoint.X+ originPoint.X+ax,DocumentHeight-startPoint.Y- originPoint.Y-ay);
					  pdfContent.LineTo(startPoint.X+ originPoint.X+ax,DocumentHeight-startPoint.Y+100- originPoint.Y-ay);
					  pdfContent.Stroke();
					  pdfContent.SetColorStroke(new Color(255,0,0));
					  pdfContent.MoveTo(startPoint.X+ originPoint.X,DocumentHeight-startPoint.Y- originPoint.Y);
					  pdfContent.LineTo(startPoint.X+ originPoint.X+100,DocumentHeight-startPoint.Y- originPoint.Y);
					  pdfContent.MoveTo(startPoint.X+ originPoint.X,DocumentHeight-startPoint.Y- originPoint.Y);
					  pdfContent.LineTo(startPoint.X+ originPoint.X,DocumentHeight-startPoint.Y+100- originPoint.Y);
					  pdfContent.Stroke();*/
					/*  float a = (float)Math.Cos(angle);
					 * 
					  float b = (float)Math.Sin(angle);
					  float c = -1 * (float)Math.Sin(angle);
					  float d = (float)Math.Cos(angle);
					  float DocumentHeight = pdfContent.PdfDocument.Top;
					  float e = startPoint.X + originPoint.X + (float)Math.Sin(angle) * height;
					  float f = DocumentHeight - (startPoint.Y + originPoint.Y + height - height * (1 - (float)Math.Cos(angle)));
					  pdfContent.AddTemplate(template, a, b, c, d, e, f);
					  t = template;*/
				}
              
			}
			/*   private void DrawCubicBezier2(PdfTemplate t, PointF P0, PointF P1, PointF P2, PointF P3) 
			   {

			   // calculates the useful base points
			   PointF PA = getPointOnSegment(P0, P1, 3/4);
			   PointF PB = getPointOnSegment(P3, P2, 3/4);
		
			   // get 1/16 of the [P3, P0] segment
			   float dx = (P3.X - P0.X)/16;
			   float dy = (P3.Y - P0.Y)/16;
	
			   // calculates control point 1
			   PointF Pc_1 = getPointOnSegment(P0, P1, 3/8);
	
			   // calculates control point 2
			   PointF Pc_2 = getPointOnSegment(PA, PB, 3/8);
			   Pc_2.X -= dx;
			   Pc_2.Y -= dy;
		
			   // calculates control point 3
			   PointF Pc_3 = getPointOnSegment(PB, PA, 3/8);
			   Pc_3.X += dx;
			   Pc_3.Y += dy;
	
			   // calculates control point 4
			   PointF Pc_4 = getPointOnSegment(P3, P2, 3/8);
	
			   // calculates the 3 anchor points
			   PointF Pa_1 = getMiddle(Pc_1, Pc_2);
			   PointF Pa_2 = getMiddle(PA, PB);
			   PointF Pa_3 = getMiddle(Pc_3, Pc_4);

			   // draw the four quadratic subsegments
			   t.CurveTo(Pc_1.X, Pc_1.Y, Pa_1.X, Pa_1.Y);
			   t.CurveTo(Pc_2.X, Pc_2.Y, Pa_2.X, Pa_2.Y);
			   t.CurveTo(Pc_3.X, Pc_3.Y, Pa_3.X, Pa_3.Y);
			   t.CurveTo(Pc_4.X, Pc_4.Y, P3.X, P3.Y);
		   }
	
		   // return the distance between two points
		   private float  distance(PointF P0, PointF P1) 
		   {
			   float dx= 0, dy = 0;
			   dx = P0.X - P1.X;
			   dy = P0.Y - P1.Y;
			   return (float)Math.Sqrt(dx*dx + dy*dy);
            
		   }

		   // return the middle of a segment define by two points
		   private PointF getMiddle(PointF P0,  PointF P1) 
		   {
			   return new PointF(((P0.X + P1.X) / 2),((P0.Y + P1.Y) / 2));
		   }


		   // return a point on a segment [P0, P1] which distance from P0
		   // is ratio of the length [P0, P1]
		   private PointF getPointOnSegment(PointF P0, PointF P1, float  ratio) 
		   {   
			   return new PointF( (P0.X + ((P1.X - P0.X) * ratio)), (P0.X + ((P1.X - P0.X) * ratio)));
		   }*/
	
		}

		private class TablePrimitive : IDraw 
		{
			private class columnSorter : IComparer 
			{
				int IComparer.Compare(Object x, Object y) 
				{
					NodeColumn a = (NodeColumn)x;
					NodeColumn b = (NodeColumn)y;
					return a.columnOrder - b.columnOrder;
				}
			}

			private struct NodeColumn 
			{
				public int columnOrder;
				public Color color;
				public string text;
				public int align;
				public float width;
				public bool visible;
			}

			private PointF startPoint = new PointF();
			private bool visible = true;
			private ArrayList columns = new ArrayList();
			private float fontSize = 12f;
			private float angle = 0f;
			private float rotation = 0f;
			private bool italic = false;
			private bool bold = false;
			private bool underline = false;
			private float strokeInterval;
			private string font = "arial";
			private float height;
			private float width;
			private bool emptyLayoutItem;

			public TablePrimitive(XmlNode node) 
			{
				if (node != null) 
				{
					XmlHelper.GetAttributeValue(node, "x", "y", ref startPoint);
					XmlHelper.GetAttributeValue(node, "rotation", ref rotation);
					XmlHelper.GetAttributeValue(node, "fontSize", ref fontSize);
					XmlHelper.GetAttributeValue(node, "italic", ref italic);
					XmlHelper.GetAttributeValue(node, "bold", ref bold);
					XmlHelper.GetAttributeValue(node, "underline", ref underline);
					XmlHelper.GetAttributeValue(node, "strokeInterval", ref strokeInterval);
					strokeInterval = strokeInterval < 2 ? fontSize/10 :strokeInterval+fontSize/10;
					XmlHelper.GetAttributeValue(node, "font", ref font);
					XmlHelper.GetAttributeValue(node, "width", ref width);
					XmlHelper.GetAttributeValue(node, "height", ref height);
					XmlHelper.GetAttributeValue(node, "visible", ref visible);
					XmlHelper.GetAttributeValue(node, "emptyLayoutItem", ref emptyLayoutItem);
					XmlNode columnsNode = node.SelectSingleNode("Columns");
					XmlNodeList columnNodeList = columnsNode.SelectNodes("Column");
                    
					foreach (XmlNode col in columnNodeList) 
					{
						NodeColumn c = new NodeColumn();
						XmlHelper.GetAttributeValue(col, "color", ref c.color);
						XmlHelper.GetAttributeValue(col, "columnOrder", ref c.columnOrder);
						string tempAlign = "";
						XmlHelper.GetAttributeValue(col, "align", ref tempAlign);
						switch (tempAlign) 
						{
							case "right":
								c.align = PdfContentByte.ALIGN_RIGHT;
								break;
							case "center":
								c.align = PdfContentByte.ALIGN_CENTER;
								break;
							case "left":
							default:
								c.align = PdfContentByte.ALIGN_LEFT;
								break;
						}
						XmlHelper.GetAttributeValue(col, "text", ref c.text);
						XmlHelper.GetAttributeValue(col, "width", ref c.width);
						XmlHelper.GetAttributeValue(col, "visible", ref c.visible);
						if (c.visible)
							columns.Add(c);
					}
					IComparer columnComparer = new columnSorter();
					columns.Sort(columnComparer);
					XmlNode rotate = node.SelectSingleNode("Rotate");
					if (rotate != null) 
					{
						XmlHelper.GetAttributeValue(rotate, "angle", ref angle);
						angle = - angle / 180 * (float)Math.PI;
					}
				}
			}

			public void Draw(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project) 
			{
				if (emptyLayoutItem)
					return;
				PdfTemplate template = pdfContent.CreateTemplate(width, height);
				//Render text
				float currWidth = 0;
				BaseFont bf = BaseFont.CreateFont(FontHelper.GetFontPath(font, italic, bold),
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				for (int i = 0; i < columns.Count; i++) 
				{
					NodeColumn col = (NodeColumn)columns[i];
					PdfTemplate columnTemplate = template.CreateTemplate(col.width-2, height);
					ArrayList arr = new ArrayList();
					int currIndex = 0, lastIndex = 0;
					while ((currIndex = col.text.IndexOf('\r', lastIndex)) != -1) 
					{
						arr.Add(col.text.Substring(lastIndex, currIndex - lastIndex));
						lastIndex = currIndex + 1;
					}
					arr.Add(col.text.Substring(lastIndex,col.text.Length-lastIndex));

					columnTemplate.BeginText();
					columnTemplate.SetFontAndSize(bf, fontSize);

					//template.SetLeading(strokeInterval);
					columnTemplate.SetColorFill(col.color);
					float tempX = 0;

					for (int j = 0; j < arr.Count; j++) 
					{
						string columnLineText = (string)arr[j];

						float textWidth = bf.GetWidthPoint(columnLineText, fontSize);

						switch (col.align) 
						{
							case PdfContentByte.ALIGN_RIGHT:
								tempX = (textWidth > col.width) ? 0 : (col.width - textWidth)-2;
								break;
							case PdfContentByte.ALIGN_CENTER:
								tempX = (textWidth > col.width) ? 0 : (col.width - textWidth) / 2-2;
								break;
							case PdfContentByte.ALIGN_LEFT:
							default:
								tempX = 0;
								break;
						}
						tempX = tempX < 0 ? 0 : tempX;
						columnTemplate.SetTextMatrix(tempX+2, height-(j+1) * fontSize - j * strokeInterval);
						columnTemplate.ShowText(columnLineText);
					}

					columnTemplate.EndText();
					columnTemplate.ResetRGBColorFill();

					template.AddTemplate(columnTemplate, currWidth, 0);
					currWidth += col.width;

				}

				float a = (float)Math.Cos(angle);
				float b = (float)Math.Sin(angle);
				float c = -1 * (float)Math.Sin(angle);
				float d = (float)Math.Cos(angle);
				float DocumentHeight = pdfContent.PdfDocument.Top;
				float e = startPoint.X + originPoint.X + (float)Math.Sin(angle) * height;
				float f = DocumentHeight - (startPoint.Y + originPoint.Y + height - height * (1 - (float)Math.Cos(angle)));
				pdfContent.AddTemplate(template, a, b, c, d, e, f);

			}
		} 
		private class ImagePrimitive : IDraw 
		{
			private PointF startPoint = new PointF();
			private float scaleX;
			private float scaleY;
			private float rotation;
			private ImageEffectProperties imgProps;
			private Guid id;
            
			public ImagePrimitive(Guid id, PointF startPoint, float scaleX, float scaleY, float rotation, ImageEffectProperties imgProps) 
			{
				this.id = id;
				this.startPoint = startPoint;
				this.scaleX = scaleX;
				this.scaleY = scaleY;
				this.rotation = rotation;
				this.imgProps = imgProps;
			}
            
			public ImagePrimitive(XmlNode node) 
			{
				if (node != null) 
				{
					imgProps = new ImageEffectProperties();
					XmlHelper.GetAttributeValue(node, "x", "y", ref startPoint);
					if(node.Attributes.GetNamedItem("scale") != null) 
					{
						XmlHelper.GetAttributeValue(node, "scale", ref scaleX);
						scaleY = scaleX;
					}
					if(node.Attributes.GetNamedItem("scaleX") != null)
						XmlHelper.GetAttributeValue(node, "scaleX", ref scaleX);
					if(node.Attributes.GetNamedItem("scaleY") != null)
						XmlHelper.GetAttributeValue(node, "scaleY", ref scaleY);
					//img effect node processing started
					try
					{
						if(node.Attributes.GetNamedItem("effectContrast") != null)
							XmlHelper.GetAttributeValue(node, "effectContrast", ref imgProps.Contrast);
                        
						if(node.Attributes.GetNamedItem("effectBrightness") != null)
							XmlHelper.GetAttributeValue(node, "effectBrightness", ref imgProps.Brightness);
                        
						if(node.Attributes.GetNamedItem("effectBlurSharpen") != null)
							XmlHelper.GetAttributeValue(node, "effectBlurSharpen", ref imgProps.BlurSharp);
						string imgEffect = null;
						if(node.Attributes.GetNamedItem("effectImage") != null)
							XmlHelper.GetAttributeValue(node, "effectImage", ref imgEffect);
						imgProps.Effect = (ImageEffect) ImageEffect.Parse(typeof(ImageEffect),imgEffect);
					}
					catch {imgProps=null;}
					//img effect node processing ended
					XmlHelper.GetAttributeValue(node, "rotation", ref rotation);
					XmlHelper.GetAttributeValue(node, "id", ref id);
				}
			}

			public void Draw(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project) 
			{
				byte[] imageContent = project.GetImage(id);
				imageContent = ImageEffectHelper.ProcessImage(imageContent, imgProps);
                
				Image image = Image.GetInstance(imageContent);
                
				rotation = - rotation * (float)Math.PI / 180;
				scaleX /= 100;
				scaleY /= 100;

				PdfTemplate template = pdfContent.CreateTemplate(image.Width, image.Height);

				image.SetAbsolutePosition(0, 0);
				template.AddImage(image);
				TransformationMatrix m = GetMatrix(
					pdfContent.PdfDocument.Top, image.Height, rotation,
					startPoint.X + originPoint.X, startPoint.Y + originPoint.Y, scaleX, scaleY );
				pdfContent.AddTemplate(template, m.a, m.b, m.c, m.d, m.e, m.f);
			}
		}

		struct TransformationMatrix { public float a, b, c, d, e, f; }
		private static TransformationMatrix GetMatrix
			(float top, float height, float angle, float x,  float y, float xScale, float yScale) 
		{
			TransformationMatrix matrix = new TransformationMatrix();
			float sin = (float)Math.Sin(angle);
			float cos = (float)Math.Cos(angle);
            
			matrix.a = xScale * cos;
			matrix.b = xScale * sin;
			matrix.c = -yScale * sin;
			matrix.d = yScale * cos;
			matrix.e = x + sin * height*yScale;
			matrix.f = top - (y + yScale * height * cos);
            
			return matrix;
		}

		private class GradientPrimitive : IDraw 
		{
			private PointF startPoint = new PointF();
			private float size;
			private float scaleX;
			private float scaleY;
			private float rotation;
			private string dir;
			private int points;
			private System.Drawing.Color color1;
			private System.Drawing.Color color2;

			public GradientPrimitive(PointF startPoint, float size, float scaleX, float scaleY, float rotation, string dir, int points, System.Drawing.Color color1, System.Drawing.Color color2) 
			{
				this.startPoint = startPoint;
				this.size = size;
				this.scaleX = scaleX;
				this.scaleY = scaleY;
				this.rotation = rotation;
				this.dir = dir;
				this.points = points;
				this.color1 = color1;
				this.color2 = color2;
			}

			public GradientPrimitive(XmlNode node) 
			{
				if (node != null) 
				{
					XmlHelper.GetAttributeValue(node, "x", "y", ref startPoint);
					XmlHelper.GetAttributeValue(node, "saveSize", ref size);
					if(node.Attributes.GetNamedItem("scaleX") != null)
						XmlHelper.GetAttributeValue(node, "scaleX", ref scaleX);
					if(node.Attributes.GetNamedItem("scaleY") != null)
						XmlHelper.GetAttributeValue(node, "scaleY", ref scaleY);
					XmlHelper.GetAttributeValue(node, "rotation", ref rotation);

					XmlHelper.GetAttributeValue(node, "dir", ref dir);
					XmlHelper.GetAttributeValue(node, "points", ref points);
					if (points < 2) points = 2;
					if (points > 5) points = 5;
					color1 = new System.Drawing.Color();
					XmlHelper.GetAttributeValue(node, "color1", ref color1);
					color2 = new System.Drawing.Color();
					XmlHelper.GetAttributeValue(node, "color2", ref color2);
				}
			}

			public void Draw(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project) 
			{
				int bmpW = (int)(size * scaleX / 100.0f + 0.5f);
				int bmpH = (int)(size * scaleY / 100.0f + 0.5f);
				Bitmap bmp = new Bitmap(bmpW, bmpH, PixelFormat.Format24bppRgb);
				Graphics gr = Graphics.FromImage(bmp);

				System.Drawing.Drawing2D.ColorBlend blend = new System.Drawing.Drawing2D.ColorBlend(points);
				blend.Colors    = new System.Drawing.Color[points];
				blend.Positions = new float[points];

				if (dir[0] == 'L') 
				{
					for (int i=0; i<points; i++) 
					{
						blend.Colors[i] = (i%2==0 ? color1 : color2);
						blend.Positions[i] = (float)i / (float)(points-1);
					}

					LinearGradientMode mode = LinearGradientMode.BackwardDiagonal;
					switch (this.dir) 
					{
						case "L1":
							mode = LinearGradientMode.Horizontal;
							break;
						case "L2":
							mode = LinearGradientMode.ForwardDiagonal;
							break;
						case "L3":
							mode = LinearGradientMode.Vertical;
							break;
						case "L4":
							mode = LinearGradientMode.BackwardDiagonal;
							break;
					}

					System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0 ,0, bmpW, bmpH);
					System.Drawing.Drawing2D.LinearGradientBrush br = new System.Drawing.Drawing2D.LinearGradientBrush(rect, color1, color2, mode);
					br.InterpolationColors = blend;
					gr.FillRectangle(br, 0, 0, bmpW, bmpH);
				}
				else if (dir[0] == 'R') 
				{
					for (int i=0; i<points; i++) 
					{
						blend.Colors[i] = (i%2==0 ? points%2==0?color2:color1 : points%2==0?color1:color2);
						blend.Positions[i] = (float)i / (float)(points-1);
					}

					System.Drawing.RectangleF rect = new System.Drawing.RectangleF(0, 0, bmpW, bmpH);
					System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
					path.AddEllipse(rect);
					System.Drawing.Drawing2D.PathGradientBrush br = new System.Drawing.Drawing2D.PathGradientBrush(path);

					br.CenterPoint = new PointF(bmpW/2 , bmpH/2);
					br.InterpolationColors = blend;
					gr.FillRectangle(new SolidBrush(points%2==0?color2:color1), 0, 0, bmpW, bmpH);
					gr.FillRectangle(br, 0, 0, bmpW, bmpH);
				}

				Image image = Image.GetInstance(bmp, ImageFormat.Png);
                
				rotation = - rotation * (float)Math.PI / 180;

				PdfTemplate template = pdfContent.CreateTemplate(image.Width, image.Height);

				image.SetAbsolutePosition(0, 0);
				template.AddImage(image);
				TransformationMatrix m = GetMatrix(
					pdfContent.PdfDocument.Top, image.Height, rotation,
					startPoint.X + originPoint.X, startPoint.Y + originPoint.Y, 1,1);
				pdfContent.AddTemplate(template, m.a, m.b, m.c, m.d, m.e, m.f);
			}
		}

		private class DrawSequence : IDraw 
		{
			private XmlNode paintSequence;
			private float width = 0;
			private float height = 0;
			private float scaleX = 1;
			private float scaleY = 1;

			public DrawSequence(XmlNode node) 
			{
				paintSequence = node;
			}

			public XmlNode PaintSequence 
			{
				get 
				{
					return paintSequence;
				}
			}

			public float Width 
			{
				get 
				{
					return width;
				}
				set 
				{
					width = value;
				}
			}

			public float Height 
			{
				get 
				{
					return height;
				}
				set 
				{
					height = value;
				}
			}

			public float ScaleX 
			{
				get 
				{
					return scaleX;
				}
				set 
				{
					scaleX = value;
				}
			}

			public float ScaleY 
			{
				get 
				{
					return scaleY;
				}
				set 
				{
					scaleY = value;
				}
			}

			public void Draw(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project) 
			{
				for(XmlNode actionNode = PaintSequence.FirstChild; actionNode != null; actionNode = actionNode.NextSibling) 
				{
					float x = float.Parse(actionNode.SelectSingleNode("@x").Value, CultureInfo.InvariantCulture) * ScaleX + originPoint.X;
					float y = float.Parse(actionNode.SelectSingleNode("@y").Value, CultureInfo.InvariantCulture) * ScaleY + originPoint.Y;
					y = Height - y;
					switch(actionNode.Name) 
					{
						case "Circle":
							float r = float.Parse(actionNode.SelectSingleNode("@r").Value, CultureInfo.InvariantCulture) * ScaleX;
							pdfContent.Circle(x, y, r);
							break;
						case "Ellipse":
							float rH = float.Parse(actionNode.SelectSingleNode("@rH").Value, CultureInfo.InvariantCulture) * ScaleX;
							float rV = float.Parse(actionNode.SelectSingleNode("@rV").Value, CultureInfo.InvariantCulture) * ScaleY;
							pdfContent.Ellipse(x - rH, y - rV, x + rH, y + rV);
							break;
						case "CurveTo":
							float cx = float.Parse(actionNode.SelectSingleNode("@cx").Value, CultureInfo.InvariantCulture) * ScaleX;
							float cy = float.Parse(actionNode.SelectSingleNode("@cy").Value, CultureInfo.InvariantCulture) * ScaleY;
							
							PointF controlPoint = new PointF(cx + originPoint.X, Height - (cy + originPoint.Y));
							PointF endPoint = new PointF(x, y);

							pdfContent.CurveTo(controlPoint.X, controlPoint.Y, endPoint.X, endPoint.Y);
							break;
						case "LineTo":
							pdfContent.LineTo(x, y);
							break;
						case "MoveTo":
							pdfContent.MoveTo(x, y);
							break;
						case "Rectangle":
							float w = float.Parse(actionNode.SelectSingleNode("@w").Value, CultureInfo.InvariantCulture) * ScaleX;
							float h = float.Parse(actionNode.SelectSingleNode("@h").Value, CultureInfo.InvariantCulture) * ScaleY;
							pdfContent.Rectangle(x, y, w, -h);
							break;
						default:
							throw new NotImplementedException();
					}
				}
			}
		}
		private class Paint : IDraw 
		{
			
			private const float increaseResolutionScale = 4;
			private struct TrackPoint 
			{
				public PointF position;
				public float rotation;

				public TrackPoint(XmlNode node, float scaleX, float scaleY) 
				{
					position = new PointF();
					rotation = 0;
					XmlHelper.GetAttributeValue(node, "x", "y", ref position);
					position.X *= scaleX;
					position.Y *= scaleY;
					if(node.Attributes.GetNamedItem("rotation") != null)
						XmlHelper.GetAttributeValue(node, "rotation", ref rotation);
				}
			}

			private class Brush : DrawSequence 
			{
				private bool filled = false;
				private float scaleX = 1;
				private float scaleY = 1;
				private const int alfaAtCenter = 128;
				private const int alfaAtBorder = 0;
				public PointF minBounds;
				public Brush(XmlNode node, float scalex, float scaley) : base(node) 
				{
					scaleX = scalex;
					scaleY = scaley;
					if (node.SelectSingleNode("@filled") != null)
						filled = true;
					else 
						filled = false;
				}

				private void Scale(ref float x, ref float y) 
				{
					x *= scaleX;
					y *= scaleY;
				}
				private void Scale(ref PointF point) 
				{
					point.X *= scaleX;
					point.Y *= scaleY;
				}
                    
				public void Draw(Graphics gr, PointF originPoint, float angle, System.Drawing.Color color) 
				{
					//todo: implement
					Pen pen = new Pen(color);
					SolidBrush solidBrush = new SolidBrush(color);
					GraphicsPath path = new GraphicsPath();
					//Scale(ref originPoint);
                    

					TrackPoint lastPoint = new TrackPoint();
					for(XmlNode actionNode = PaintSequence.FirstChild; actionNode != null; actionNode = actionNode.NextSibling) 
					{
						XmlNode xNode = actionNode.SelectSingleNode("@x");
						XmlNode yNode = actionNode.SelectSingleNode("@y");
						if (xNode == null) xNode = actionNode.SelectSingleNode("@x1");
						if (yNode == null) yNode = actionNode.SelectSingleNode("@y1");
						float x = float.Parse(xNode.Value, CultureInfo.InvariantCulture); //no brush rotation
						float y = float.Parse(yNode.Value, CultureInfo.InvariantCulture);
						Scale(ref x, ref y);
						TranslateCoords(ref x, ref y, originPoint);
						if (actionNode.SelectSingleNode("@filled") != null) filled = true;
						switch(actionNode.Name) 
						{
							case "Circle":
								float rH = float.Parse(actionNode.SelectSingleNode("@r").Value, CultureInfo.InvariantCulture);
								float rV = rH;
								Scale(ref rH, ref rV);
								if (filled) gr.FillEllipse(solidBrush, x-rH, y-rV, rH*2, rV*2);
								else gr.DrawEllipse(pen, x-rH, y-rV, rH*2, rV*2);
								break;
							case "Ellipse":
								rH = float.Parse(actionNode.SelectSingleNode("@rH").Value, CultureInfo.InvariantCulture);
								rV = float.Parse(actionNode.SelectSingleNode("@rV").Value, CultureInfo.InvariantCulture);
								Scale(ref rH, ref rV);
								if (filled) gr.FillEllipse(solidBrush, x-rH, y-rV, rH*2, rV*2);
								else gr.DrawEllipse(pen, x-rH, y-rV, rH*2, rV*2);
								break;
							case "LineTo":
								path.AddLine(lastPoint.position.X,  lastPoint.position.Y,  x, y);
								lastPoint.position.X = x;
								lastPoint.position.Y = y;
								break;
							case "MoveTo":
								path.StartFigure();
								lastPoint.position.X = x;
								lastPoint.position.Y = y;
								break;
							case "CurveTo":
								float cx = float.Parse(actionNode.SelectSingleNode("@cx").Value, CultureInfo.InvariantCulture);
								float cy = float.Parse(actionNode.SelectSingleNode("@cy").Value, CultureInfo.InvariantCulture);
								Scale(ref cx, ref cy);
								TranslateCoords(ref cx, ref cy, originPoint);
								//todo: implement with cubic curve
								//path.AddCurve(lastPoint.position.X,  lastPoint.position.Y,  cx, cy, x, y);
								lastPoint.position.X = x;
								lastPoint.position.Y = y;
								throw new NotImplementedException();
								//break;
							case "Rectangle":
								float w = float.Parse(actionNode.SelectSingleNode("@w").Value, CultureInfo.InvariantCulture);
								float h = float.Parse(actionNode.SelectSingleNode("@h").Value, CultureInfo.InvariantCulture);
								Scale(ref w, ref h);
								if (filled) gr.FillRectangle(solidBrush, x, y, w, h);
								else gr.DrawRectangle(pen, x, y, w, h);
								break;
							case "GradientCircle":
								DrawGradientCircle(gr, color, actionNode, x, y);
								break;
							case "GradientLine":
								DrawGradientLine(gr, color, actionNode, originPoint);
								break;
							default:
								throw new NotImplementedException();
						}
					}
					if (path.PointCount>0) 
					{
						if (filled)
							gr.FillPath(solidBrush, path);
						else
							gr.DrawPath(pen, path);
					}
				}

				private void TranslateCoords(ref float x, ref float y, PointF origin) 
				{
					x = origin.X + x - minBounds.X;
					y = origin.Y + y - minBounds.Y;
				}
				private void TranslateCoords(ref PointF point, PointF origin) 
				{
					point.X = origin.X + point.X - minBounds.X;
					point.Y = origin.Y + point.Y - minBounds.Y;
				}


				private void DrawGradientCircle(Graphics gr, System.Drawing.Color color, XmlNode actionNode, float x, float y) 
				{
					float r = float.Parse(actionNode.SelectSingleNode("@r").Value, CultureInfo.InvariantCulture);
					// Create a path that consists of a single circle
					GraphicsPath path = new GraphicsPath();
					path.AddEllipse(-r, -r, r*2, r*2);
					ScaleTranslatePath(ref path, new PointF(x,y));

					// Create brush
					PathGradientBrush pathGradientBrush = new PathGradientBrush(path);
					System.Drawing.Color boundaryColor = System.Drawing.Color.FromArgb(alfaAtBorder, color);
					pathGradientBrush.CenterColor = System.Drawing.Color.FromArgb(alfaAtCenter, color);
					pathGradientBrush.SurroundColors = new System.Drawing.Color[] {boundaryColor};
					pathGradientBrush.CenterPoint = new PointF(x,y);

					gr.FillPath(pathGradientBrush, path);
				}

				private void DrawGradientLine(Graphics gr, System.Drawing.Color color, XmlNode actionNode, PointF origin) 
				{
					float x1 = float.Parse(actionNode.SelectSingleNode("@x1").Value, CultureInfo.InvariantCulture);
					float y1 = float.Parse(actionNode.SelectSingleNode("@y1").Value, CultureInfo.InvariantCulture);
					float x2 = float.Parse(actionNode.SelectSingleNode("@x2").Value, CultureInfo.InvariantCulture);
					float y2 = float.Parse(actionNode.SelectSingleNode("@y2").Value, CultureInfo.InvariantCulture);
					float w = float.Parse(actionNode.SelectSingleNode("@w").Value, CultureInfo.InvariantCulture);
					//calculate anchor points
					double a = Math.Atan2(y2 - y1, x2 - x1);
					float sin = (float)Math.Sin(a);
					float cos = (float)Math.Cos(a);

					float dx = w/2*sin;
					float dy = w/2*cos;
					PointF C1 = new PointF(x1, y1);
					PointF D1 = new PointF(x1-dx, y1+dy);
					PointF E1 = new PointF(x1+dx, y1-dy);
					PointF F1 = new PointF(x1+dx-dy, y1-dy-dx);
					PointF G1 = new PointF(x1-dx-dy, y1+dy-dx);
					PointF A1 = new PointF(x1-dy, y1-dx);

					PointF C2 = new PointF(x2, y2);
					PointF D2 = new PointF(x2-dx, y2+dy);
					PointF E2 = new PointF(x2+dx, y2-dy);
					PointF F2 = new PointF(x2+dx+dy, y2-dy+dx);
					PointF G2 = new PointF(x2-dx+dy, y2+dy+dx);
					PointF A2 = new PointF(x2+dy, y2+dx);

					//center linear gradient (use non-translated D1 and E1) and scale/translate it separately
					System.Drawing.Color borderColor = System.Drawing.Color.FromArgb(alfaAtBorder, color);
					System.Drawing.Color centerColor = System.Drawing.Color.FromArgb(alfaAtCenter, color);
					LinearGradientBrush brushCenter1 = new LinearGradientBrush(D1, E1, borderColor, borderColor);
					Matrix matrix = brushCenter1.Transform;
					matrix.Scale(scaleX, scaleY, MatrixOrder.Append);
					matrix.Translate(origin.X,  origin.Y,  MatrixOrder.Append);
					matrix.Translate(-minBounds.X, -minBounds.Y,  MatrixOrder.Append);
					brushCenter1.Transform = matrix;
					ColorBlend cb = new ColorBlend(3);
					cb.Colors = new System.Drawing.Color[] {borderColor, centerColor, borderColor};
					cb.Positions = new float[] {0.0F, 0.5F, 1.0F};
					brushCenter1.InterpolationColors = cb;
                        
					//scale and translate anchor points
					Scale(ref C1); TranslateCoords(ref C1, origin);
					Scale(ref D1); TranslateCoords(ref D1, origin);
					Scale(ref E1); TranslateCoords(ref E1, origin);
					Scale(ref F1); TranslateCoords(ref F1, origin);
					Scale(ref G1); TranslateCoords(ref G1, origin);
					Scale(ref A1); TranslateCoords(ref A1, origin);
                    
					Scale(ref C2); TranslateCoords(ref C2, origin);
					Scale(ref D2); TranslateCoords(ref D2, origin);
					Scale(ref E2); TranslateCoords(ref E2, origin);
					Scale(ref F2); TranslateCoords(ref F2, origin);
					Scale(ref G2); TranslateCoords(ref G2, origin);
					Scale(ref A2); TranslateCoords(ref A2, origin);

					gr.FillPolygon(brushCenter1, new PointF[]{ E1, D1, D2, E2});
                    
					//radial gradient for line peaks
					GraphicsPath pathEdge1 = new GraphicsPath();
					GraphicsPath pathEdge2 = new GraphicsPath();
					pathEdge1.AddPolygon(new PointF[]{ D1, E1, F1, G1});
					pathEdge2.AddPolygon(new PointF[]{ D2, E2, F2, G2});
                    
					//brushes for line peaks
					GraphicsPath pathEdgeFill1 = new GraphicsPath();
					GraphicsPath pathEdgeFill2 = new GraphicsPath();
                    
					pathEdgeFill1.AddEllipse(-w/2, -w/2, w, w);
					pathEdgeFill2.AddEllipse(-w/2, -w/2, w, w);
					ScaleTranslatePath(ref pathEdgeFill1, C1);
					ScaleTranslatePath(ref pathEdgeFill2, C2);
                    
					PathGradientBrush brushEdge1 = new PathGradientBrush(pathEdgeFill1);
					PathGradientBrush brushEdge2 = new PathGradientBrush(pathEdgeFill2);
					brushEdge1.CenterColor = centerColor;
					brushEdge2.CenterColor = centerColor;
					brushEdge1.SurroundColors = new System.Drawing.Color[] {borderColor};
					brushEdge2.SurroundColors = new System.Drawing.Color[] {borderColor};
					brushEdge1.CenterPoint = C1;
					brushEdge2.CenterPoint = C2;

					//draw peaks
					gr.FillPath(brushEdge1, pathEdge1);
					gr.FillPath(brushEdge2, pathEdge2);
				}

				private void ScaleTranslatePath(ref GraphicsPath path, PointF center) 
				{
					Matrix matrix = new Matrix(1,0,0,1,0,0);
					matrix.Scale(scaleX, scaleY, MatrixOrder.Append);
					matrix.Translate(center.X,  center.Y,  MatrixOrder.Append);
					path.Transform(matrix);
				}
			}

			private string id;
			private PointF paintPosition = new PointF();

			private float scaleX;
			private float scaleY;
			private float rotation;
			
			//private Color color;
			private Brush currentBrush;
			private PointF minBounds = new PointF();
			private PointF maxBounds = new PointF();

			private ArrayList points;

			public Paint(XmlNode paintNode) 
			{
				if (paintNode == null)
					return;
				points = new ArrayList();

				//SaveStringToFile(paintNode.OuterXml);

				XmlHelper.GetAttributeValue(paintNode, "id", ref id);
				XmlHelper.GetAttributeValue(paintNode, "x", "y", ref paintPosition);
				
				if(paintNode.Attributes.GetNamedItem("scale") != null) 
				{
					XmlHelper.GetAttributeValue(paintNode, "scale", ref scaleX);
					XmlHelper.GetAttributeValue(paintNode, "scale", ref scaleY);
				}
				if(paintNode.Attributes.GetNamedItem("scaleX") != null)
					XmlHelper.GetAttributeValue(paintNode, "scaleX", ref scaleX);
				if(paintNode.Attributes.GetNamedItem("scaleY") != null)
					XmlHelper.GetAttributeValue(paintNode, "scaleY", ref scaleY);
				XmlHelper.GetAttributeValue(paintNode, "rotation", ref rotation);

				rotation = - rotation;// * (float)Math.PI / 180;
				scaleX /= 100;
				scaleY /= 100;
				//scale up to draw in higher resolution
				scaleX *= increaseResolutionScale;
				scaleY *= increaseResolutionScale;

				for(XmlNode node = paintNode.FirstChild; node != null; node = node.NextSibling) 
				{
					switch (node.Name) 
					{
						case "Brush" :
							currentBrush = new Brush(node, scaleX, scaleY);
							points.Add(currentBrush);
							break;
						case "Point" :
							TrackPoint point = new TrackPoint(node, scaleX, scaleY);
							points.Add(point);
							UpdateBounds(point);
							break;
						case "Color" :
							System.Drawing.Color color = new System.Drawing.Color();
							XmlHelper.GetAttributeValue(node, "color", ref color);
							points.Add(color);
							break;
					}
				}
				minBounds.X -= 40*scaleX;
				minBounds.Y -= 40*scaleY;
				maxBounds.X += 40*scaleX;
				maxBounds.Y += 40*scaleY;
			}

			private void UpdateBounds(TrackPoint point) 
			{
				if (minBounds.X > point.position.X) minBounds.X = point.position.X;
				if (minBounds.Y > point.position.Y) minBounds.Y = point.position.Y;
				if (maxBounds.X < point.position.X) maxBounds.X = point.position.X;
				if (maxBounds.Y < point.position.Y) maxBounds.Y = point.position.Y;
			}

			public void Draw(PdfContentByte pdfContent, PointF facePosition, ref PointF currentPoint, Project project) 
			{
                
				//create internal canvas
				float width = maxBounds.X - minBounds.X;
				float height = maxBounds.Y - minBounds.Y;
				int bmpWidth = (int) (width);
				int bmpHeight = (int) (height);

				Bitmap bitmap = new Bitmap(bmpWidth, bmpHeight, PixelFormat.Format32bppArgb);
				bitmap.SetResolution(150, 150); //remove?
				Graphics gr = Graphics.FromImage(bitmap);
                
				Brush currentBrush = null;
				System.Drawing.Color currentColor = new System.Drawing.Color();
				//stamp brush templates into the canvas
				foreach(object obj in points) 
				{
					Type type = obj.GetType();
					if (type == typeof(Brush)) 
					{
						currentBrush = (Brush)obj;
						currentBrush.minBounds = minBounds;
					} 
					else if (type == typeof(System.Drawing.Color)) 
					{
						currentColor = (System.Drawing.Color)obj;
					} 
					else if (type == typeof(TrackPoint)) 
					{
						//stamp brush at TrackPoint using Color
						TrackPoint point = (TrackPoint)obj;
						currentBrush.Draw(gr, point.position, point.rotation, currentColor);
					}
				}

				//draw canvas into the pdf as png image with the necessary rotation angle
				PdfTemplate template = pdfContent.CreateTemplate(bitmap.Width, bitmap.Height);
				Image image = Image.GetInstance(bitmap, ImageFormat.Png);
				image.SetAbsolutePosition(0, 0);
				template.AddImage(image);

				PointF globalPosition = new PointF(facePosition.X + paintPosition.X, pdfContent.PdfDocument.Top - facePosition.Y - paintPosition.Y);
                
				Matrix m = new Matrix(1, 0, 0, 1, 0, 0);
				//move to center point (painting starting point)
				m.Translate(minBounds.X,  -maxBounds.Y, MatrixOrder.Append);
				//scale down for higher resolution
				m.Scale(1/increaseResolutionScale, 1/increaseResolutionScale, MatrixOrder.Append);
				//rotate and move as initial paint object
				m.Rotate(rotation, MatrixOrder.Append);
				m.Translate(globalPosition.X,  globalPosition.Y, MatrixOrder.Append);

				pdfContent.AddTemplate(template, m.Elements[0], m.Elements[1], m.Elements[2], m.Elements[3], m.OffsetX, m.OffsetY);

                
			}
		}

		private class Shape : IDraw	
		{
			private string id;
			private PointF deltaPoint = new PointF();

			private float scaleX;
			private float scaleY;
			private float rotation;

			private float width;
			private float height;

			private float borderWidth;
			private Color borderColor;

			private string fillType;
			private const string GradientFill = "gradient";
			private const string ImageFill = "image";
            
			private Guid fillImageId;
			private float fillScaleX;
			private float fillScaleY;
            
			private Color fillColor;
			private Color fillColor2;
			private bool transparency;
			private string gradientType;
			private int points;
			private string dir;


			private class ShapeForm : DrawSequence 
			{
				public ShapeForm(XmlNode node) : base(node) 
				{
				}
			}

			ShapeForm shapeForm;

			public Shape(XmlNode shapeNode, XmlNode shapeTypesNode, XmlNode gradientTypesNode) 
			{
				if (shapeNode == null)
					return;

				XmlHelper.GetAttributeValue(shapeNode, "id", ref id);
				XmlHelper.GetAttributeValue(shapeNode, "x", "y", ref deltaPoint);

				if(shapeNode.Attributes.GetNamedItem("scale") != null) 
				{
					XmlHelper.GetAttributeValue(shapeNode, "scale", ref scaleX);
					XmlHelper.GetAttributeValue(shapeNode, "scale", ref scaleY);
				}
				if(shapeNode.Attributes.GetNamedItem("scaleX") != null)
					XmlHelper.GetAttributeValue(shapeNode, "scaleX", ref scaleX);
				if(shapeNode.Attributes.GetNamedItem("scaleY") != null)
					XmlHelper.GetAttributeValue(shapeNode, "scaleY", ref scaleY);
				XmlHelper.GetAttributeValue(shapeNode, "rotation", ref rotation);

				XmlHelper.GetAttributeValue(shapeNode, "currentWidth", ref width);
				XmlHelper.GetAttributeValue(shapeNode, "currentHeight", ref height);

				XmlNode typeNode = shapeNode.SelectSingleNode("Type");
				if(typeNode != null)
					shapeForm = new ShapeForm(typeNode);
				else 
				{
					string type = shapeNode.SelectSingleNode("@type").Value;
					shapeForm = new ShapeForm(shapeTypesNode.SelectSingleNode("Type[@name='" + type + "']"));
				}

				XmlNode border = shapeNode.SelectSingleNode("Border");
				if (border != null) 
				{
					XmlHelper.GetAttributeValue(border, "color", ref borderColor);
					XmlHelper.GetAttributeValue(border, "width", ref borderWidth);
				}

				XmlNode fill = shapeNode.SelectSingleNode("Fill");
				if (fill != null) 
				{
					XmlHelper.GetAttributeValue(fill, "color", ref fillColor);
					XmlHelper.GetAttributeValue(fill, "color2", ref fillColor2);
					XmlHelper.GetAttributeValue(fill, "transparency", ref transparency);
					XmlHelper.GetAttributeValue(fill, "gradientType", ref gradientType);
					XmlHelper.GetAttributeValue(fill, "type", ref fillType);
					XmlHelper.GetAttributeValue(fill, "fillScaleX", ref fillScaleX);
					XmlHelper.GetAttributeValue(fill, "fillScaleY", ref fillScaleY);
				    
					fillScaleX *= 100;
					fillScaleY *= 100;
				    
					if (fillType == ImageFill) 
					{
						string imageId = null;
						XmlHelper.GetAttributeValue(fill, "fillImageId", ref imageId);
						fillImageId = new Guid(imageId);
					}
				}

				if (gradientType == null)
					gradientType = "00";

				if (gradientType != "00") 
				{
					string gradientTypeXpath = string.Format(@"/GradientTypes/Type[@name='{0}']", gradientType);
					XmlNode gradientNode = gradientTypesNode.SelectSingleNode(gradientTypeXpath);
					XmlHelper.GetAttributeValue(gradientNode, "points", ref points);
					XmlHelper.GetAttributeValue(gradientNode, "dir", ref dir);
				}
			}

			public void Draw(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project) 
			{
				if (transparency && borderWidth == 0)
					return;

				shapeForm.Width = width + borderWidth;
				shapeForm.Height = height + borderWidth;
				shapeForm.ScaleX = width / float.Parse(shapeForm.PaintSequence.SelectSingleNode("@width").Value, CultureInfo.InvariantCulture);
				shapeForm.ScaleY = height / float.Parse(shapeForm.PaintSequence.SelectSingleNode("@height").Value, CultureInfo.InvariantCulture);

				PdfTemplate shapeTemplate = pdfContent.CreateTemplate(shapeForm.Width, shapeForm.Height);
				shapeTemplate.SetLineJoin(PdfContentByte.LINE_JOIN_ROUND);
				shapeTemplate.SetLineCap(PdfContentByte.LINE_CAP_ROUND);
				shapeTemplate.SetLineWidth(borderWidth);
				shapeTemplate.SetColorStroke(borderColor);

				PointF offset = new PointF(borderWidth / 2, borderWidth / 2);
				shapeForm.Draw(shapeTemplate, offset, ref currentPoint, project);

				shapeTemplate.ClosePath();
				if(transparency)
					shapeTemplate.Stroke();
				else 
				{
					PdfPatternPainter painter = null;
					switch (fillType) 
					{
						case GradientFill:
							if (gradientType != "00") 
							{
								painter = pdfContent.CreatePattern(width * scaleX, height * scaleY);

								System.Drawing.Color c1 = System.Drawing.Color.FromArgb(fillColor.R, fillColor.G, fillColor.B);
								System.Drawing.Color c2 = System.Drawing.Color.FromArgb(fillColor2.R, fillColor2.G, fillColor2.B);
								GradientPrimitive gp = new GradientPrimitive(deltaPoint, 100, fillScaleX, fillScaleY, rotation, dir, points, c1, c2);
								gp.Draw(painter, originPoint, ref currentPoint, project);
							}
							break;
						case ImageFill:
							painter = pdfContent.CreatePattern(width * scaleX, height * scaleY);

							ImagePrimitive ip = new ImagePrimitive(fillImageId, deltaPoint, fillScaleX, fillScaleY, rotation, null);
							ip.Draw(painter, originPoint, ref currentPoint, project);
							break;
					}

					if (painter != null)
						pdfContent.SetPatternFill(painter);
					else
						shapeTemplate.SetColorFill(fillColor);

					if (borderWidth == 0)
						shapeTemplate.EoFill();
					else
						shapeTemplate.EoFillStroke();
				}
				
				shapeTemplate.ResetRGBColorFill();
				shapeTemplate.ResetRGBColorStroke();

				scaleX = 1;
				scaleY = 1;

				float angle = - rotation * (float)Math.PI / 180;
				float x = deltaPoint.X + originPoint.X - offset.X * (float)Math.Cos(angle);
				float y = deltaPoint.Y + originPoint.Y + offset.Y * (float)Math.Sin(angle);
				TransformationMatrix m = GetMatrix(pdfContent.PdfDocument.Top, shapeForm.Height - borderWidth / 2, angle, x, y, scaleX, scaleY);
				pdfContent.AddTemplate(shapeTemplate, m.a, m.b, m.c, m.d, m.e, m.f);
			}
		}
		
		private class WatermarkPrimitive : IDraw 
		{
			private PointF startPoint = new PointF();
			private string text = string.Empty;
			private int width;
			private int height;
			private const float pxToMMCoeff = 0.35277777777777f;
			private const float Margin = 72;
			private int align = Element.ALIGN_CENTER;
			private float angle = - (float)(Math.PI / 4f);
			private ArrayList styles = new ArrayList();

			public WatermarkPrimitive(XmlNode node) 
			{
				if (node != null) 
				{
					XmlNode n = node.SelectSingleNode(@"/Watermark");
					XmlHelper.GetAttributeValue(n, "Width", ref width);
					XmlHelper.GetAttributeValue(n, "Height", ref height);
					n = node.SelectSingleNode(@"/Watermark/XmlText");
					XmlHelper.GetAttributeValue(n, "text", ref text);

					foreach(XmlNode style in node.SelectNodes("/Watermark/XmlText/Style")) 
					{
						NodeStyle letterStyle = new NodeStyle();

						XmlHelper.GetAttributeValue(style, "font", ref letterStyle.font);
						XmlHelper.GetAttributeValue(style, "size", ref letterStyle.fontSize);
						XmlHelper.GetAttributeValue(style, "bold", ref letterStyle.bold);
						XmlHelper.GetAttributeValue(style, "italic", ref letterStyle.italic);
						XmlHelper.GetAttributeValue(style, "underline", ref letterStyle.underline);
						XmlHelper.GetAttributeValue(style, "color", ref letterStyle.textColor);

						styles.Add(letterStyle);
					}
				}
			}

			public void Draw(PdfContentByte pdfContent, PointF originPoint, ref PointF currentPoint, Project project) 
			{
				if (text.Length == 0)
					return;
				float tfw = width / pxToMMCoeff;
				float tfh = height/ pxToMMCoeff;
				float w = (tfw + tfh) / (float)Math.Sqrt(2) + Margin;
				float h = w;
				float wl = pdfContent.PdfDocument.PageSize.Width  / w + 1;
				float hl = pdfContent.PdfDocument.PageSize.Height / h + 1;
				float tfx = Margin / 2;
				float tfy = tfh / (float)Math.Sqrt(2) + Margin / 2;

				PdfTemplate template = pdfContent.CreateTemplate(w, h);
				PdfTemplate t = template.CreateTemplate(tfw, tfh);

				/*template.SetRGBColorStroke(255, 0, 0);
				template.SetLineWidth(3);
				template.MoveTo(0,0);
				template.LineTo(0, w);
				template.LineTo(w, h);
				template.LineTo(0, h);
				template.LineTo(0, 0);
				template.Stroke();

				t.SetRGBColorStroke(0, 255, 0);
				t.SetLineWidth(3);
				t.MoveTo(0,0);
				t.LineTo(0, tfw);
				t.LineTo(tfw, tfh);
				t.LineTo(0, tfh);
				t.LineTo(0, 0);
				t.Stroke();*/

				float a =  (float)Math.Cos(angle);
				float b =  (float)Math.Sin(angle);
				float c = -(float)Math.Sin(angle);
				float d =  (float)Math.Cos(angle);
				float e = tfx;
				float f = h - tfy;

				float textHeight = 0.0f;
				float totalWidth = 0.0f;
				float fontSize = 0.0f;
				float maxDes = 0.0f;

				//Calculate baseLine ('des' of Max height letter) & textHeight (size=asc - des) of Max height letter
				Phrase phrase = new Phrase();
				for (int i = 0; i < text.Length; i++) 
				{
					NodeStyle style = (NodeStyle)styles[i];

					fontSize = Math.Max(fontSize, style.fontSize);
					BaseFont bf = BaseFont.CreateFont(FontHelper.GetFontPath(style.font, style.italic, style.bold),
						BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

					maxDes = Math.Min(maxDes, bf.GetFontDescriptor(BaseFont.AWT_DESCENT, style.fontSize));
				}

				for (int i = 0; i < text.Length; i++) 
				{
					NodeStyle style = (NodeStyle)styles[i];

					BaseFont bf = BaseFont.CreateFont(FontHelper.GetFontPath(style.font, style.italic, style.bold),
						BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
					Font font = new Font(bf, style.fontSize);
					font.Color = style.textColor;
					Chunk ch = new Chunk(text[i], font);
					if (style.underline) 
					{
						ch.SetUnderline(1, maxDes / 2);
					}

					phrase.Add(ch);
					totalWidth += ch.GetWidthPoint();
				}

				PdfPTable table = new PdfPTable(1);
				PdfPCell cell = new PdfPCell(phrase);
				cell.Border = 0;
				cell.Padding = 0.0f;
				cell.HorizontalAlignment = align;
				table.AddCell(cell);
				table.TotalWidth = tfw;
				table.WriteSelectedRows(0, -1, 0, tfh, t);
				template.AddTemplate(t, a, b, c, d, e, f);
                

				for (int i = 0; i < wl; ++ i) 
				{
					for (int j = 0; j < hl; ++ j) 
					{
						float x = w * i;
						float y = pdfContent.PdfDocument.PageSize.Height - h * (j + 1);
						pdfContent.AddTemplate(template, x, y);
					}
				}
			}
		}

		#endregion Primitives


		#region Face
		private class Face 
		{
			private string id;
			private PointF deltaPoint = new PointF();
			private ArrayList contours = new ArrayList();
			private ArrayList primitives = new ArrayList();
			private Paper paper;

			public Face(XmlNode node, Paper paper, PdfCalibration calibration, XmlNode shapeTypesNode, XmlNode gradientTypesNode) 
			{
				if (node == null)
					return;
				this.paper = paper;
				XmlHelper.GetAttributeValue(node, "id", ref id);
				XmlHelper.GetAttributeValue(node, "x", "y", ref deltaPoint);
				deltaPoint.X += calibration.X;
				deltaPoint.Y += calibration.Y;

				XmlNodeList childrenNodeList = node.ChildNodes;
				foreach (XmlNode childNode in childrenNodeList) 
				{
					switch (childNode.Name) 
					{
						case "Contour":
							contours.Add(new Contour(childNode));
							break;
						case "Circle":
							primitives.Add(new CirclePrimitive(childNode));
							break;
						case "Image":
							primitives.Add(new ImagePrimitive(childNode));
							break;
						case "Text":
						case "TextEffect":
							primitives.Add(new TextPrimitive(childNode));
							break;
						case "Rectangle":
							primitives.Add(new RectanglePrimitive(childNode));
							break;
						case "Playlist":
							primitives.Add(new TablePrimitive(childNode));
							break;
						case "Fill":
							primitives.Add(new FillCommand(childNode, paper.PaperSize, gradientTypesNode));
							break;
						case "Shape":
							primitives.Add(new Shape(childNode, shapeTypesNode, gradientTypesNode));
							break;
						case "Gradient":
							primitives.Add(new GradientPrimitive(childNode));
							break;
						case "Paint":
							primitives.Add(new Paint(childNode));
							break;
						case "Localization":
							//Info from this node not important for PDF
							break;
						default:
							throw new NotImplementedException();
					}
				}
			}

			private void DrawContourPart(ContourPart contourPart, PdfContentByte pdfContent, Project project) 
			{
				PointF currentGraficPoint = new PointF(0, 0);
				foreach (IDrawContourPart contour in contours) 
				{
					if (contour != null)
						contour.DrawContourPart(pdfContent, deltaPoint, ref currentGraficPoint, project, 
							contourPart, this.paper.PaperType);
				}
			}

			public void Draw(PdfContentByte pdfContent, Project project) 
			{
				PdfPatternPainter painter = pdfContent.CreatePattern(paper.PaperSize.Width, paper.PaperSize.Height);
                
				PointF currentGraficPoint = new PointF(0, 0);
				foreach (IDraw draw in primitives) 
				{
					if (draw != null)
						draw.Draw(painter, deltaPoint, ref currentGraficPoint, project);
				}
				
				DrawContourPart(ContourPart.Contour, pdfContent, project);
				pdfContent.SetPatternFill(painter);

				if(paper.PaperType == PaperType.Universal) 
				{
					pdfContent.SetColorStroke(Color.BLACK);
					pdfContent.SetLineWidth(0.2f);
					pdfContent.EoFillStroke();

					//DrawContourPart(RealContour);  - we do not draw real contour and mark lines in PDF 
					//DrawContourPart(MarkLines);

					DrawContourPart(ContourPart.CutLines, pdfContent, project);
				} 
				else
					pdfContent.EoFill(); 
			}
		}

		private static Face[] GetFaces(XmlDocument project, Paper paper, PdfCalibration calibration, XmlNode shapeTypesNode, XmlNode gradientTypesNode) 
		{
			XmlElement root = project.DocumentElement;

			XmlNodeList faceList = root.SelectNodes("//Faces/Face");
			Face[] faces = new Face[faceList.Count];
			for (int i = 0; i < faceList.Count; i++) 
			{
				faces[i] = new Face(faceList[i], paper, calibration, shapeTypesNode, gradientTypesNode);
			}
			return faces;
		}

		#endregion Face
        
		#region XmlHelper
		private sealed class XmlHelper 
		{
			private XmlHelper() {}

			private static string GetAttribute(XmlNode node, string attributeName) 
			{
				if (node == null) return null;

				XmlNode attributeNode = node.Attributes.GetNamedItem(attributeName);
				if (attributeNode == null) return null;

				return attributeNode.Value;
			}

			public static void GetAttributeValue(XmlNode node, string attributeName, ref int variable) 
			{
				try 
				{
					variable = int.Parse(GetAttribute(node, attributeName), CultureInfo.InvariantCulture);
				} 
				catch {}
			}

			public static void GetAttributeValue(XmlNode node, string xAttributeName, string yAttributeName, ref PointF variable) 
			{
				try 
				{
					variable.X = float.Parse(GetAttribute(node, xAttributeName), CultureInfo.InvariantCulture);
					variable.Y = float.Parse(GetAttribute(node, yAttributeName), CultureInfo.InvariantCulture);
				} 
				catch {}
			}

			public static void GetAttributeValue(XmlNode node, string wAttributeName, string hAttributeName, ref SizeF variable) 
			{
				try 
				{
					variable.Width = float.Parse(GetAttribute(node, wAttributeName), CultureInfo.InvariantCulture);
					variable.Height = float.Parse(GetAttribute(node, hAttributeName), CultureInfo.InvariantCulture);
				} 
				catch {}
			}

			public static void GetAttributeValue(XmlNode node, string xAttributeName, string yAttributeName, string wAttributeName, string hAttributeName, ref RectangleF variable) 
			{
				try 
				{
					variable.X = float.Parse(GetAttribute(node, xAttributeName), CultureInfo.InvariantCulture);
					variable.Y = float.Parse(GetAttribute(node, yAttributeName), CultureInfo.InvariantCulture);
					variable.Width = float.Parse(GetAttribute(node, wAttributeName), CultureInfo.InvariantCulture);
					variable.Height = float.Parse(GetAttribute(node, hAttributeName), CultureInfo.InvariantCulture);
				} 
				catch {}
			}

			public static void GetAttributeValue(XmlNode node, string attributeName, ref float variable) 
			{
				try 
				{
					variable = float.Parse(GetAttribute(node, attributeName), CultureInfo.InvariantCulture);
				} 
				catch {}
			}

			public static void GetAttributeValue(XmlNode node, string attributeName, ref double variable) 
			{
				try 
				{
					variable = double.Parse(GetAttribute(node, attributeName), CultureInfo.InvariantCulture);
				} 
				catch {}
			}

			public static void GetAttributeValue(XmlNode node, string attributeName, ref string variable) 
			{
				try 
				{
					string tmp = GetAttribute(node, attributeName);
					if (tmp != null)
						variable = tmp;
				} 
				catch {}
			}

			public static void GetAttributeValue(XmlNode node, string attributeName, ref bool variable) 
			{
				try 
				{
					variable = bool.Parse(GetAttribute(node, attributeName));
				} 
				catch {}
			}

			public static void GetAttributeValue(XmlNode node, string attributeName, ref Color variable) 
			{
				try 
				{
					variable = new Color(ColorTranslator.FromHtml(GetAttribute(node, attributeName)));
				} 
				catch {}
			}

			public static void GetAttributeValue(XmlNode node, string attributeName, ref System.Drawing.Color variable) 
			{
				try 
				{
					variable = ColorTranslator.FromHtml(GetAttribute(node, attributeName));
				} 
				catch {}
			}

			public static void GetAttributeValue(XmlNode node, string attributeName, ref Guid variable) 
			{
				try 
				{
					variable = new Guid(GetAttribute(node, attributeName));
				} 
				catch 
				{
					variable = Guid.Empty;
				}
			}
		}

		#endregion
	}
}
