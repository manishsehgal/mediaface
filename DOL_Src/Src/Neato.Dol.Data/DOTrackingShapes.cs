using System;
using System.Data;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data
{

	public class DOTrackingShapes
	{
		public DOTrackingShapes(){}
        public static int Add(string shape, string login, DateTime time) 
        {
            PrTrackingShapesIns prc = new PrTrackingShapesIns();
            prc.ShapeId = shape;
            prc.Time = time;
            prc.Login = login;
            prc.ExecuteNonQuery();
            return prc.Id.Value;
        }

        public static DataSet GetShapesListByDate(DateTime startTime,DateTime endTime) 
        {
            PrTrackingShapesEnumByTime  prc = new PrTrackingShapesEnumByTime();
            prc.TimeEnd = endTime;
            prc.TimeStart = startTime;
            DataSet data = new DataSet();
            data.Tables.Add("Shape");
            return prc.LoadDataSet(data);
        }

     
	}
}
