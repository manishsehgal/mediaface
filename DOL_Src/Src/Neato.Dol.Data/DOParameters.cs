using Neato.Dol.Data.DBEngine;
using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity.Exceptions;

namespace Neato.Dol.Data {
    public class DOParameters {
        private DOParameters() {}

        public static ParametersData EnumParameters() {
            PrParametersEnum prc = new PrParametersEnum();
            return (ParametersData) prc.LoadDataSet(new ParametersData());
        }

        public static void UpdateParameters(ParametersData data) {
            PrParametersUpd prcUpd = new PrParametersUpd();
            ProcedureWrapper.UpdateDataTable(data.Parameters, null, prcUpd, null);
        }

        public static string GetParameterByKey(string key) {
            PrParametersGetByKey prc = new PrParametersGetByKey();
            prc.Key = key;

            object value = prc.ExecuteScalar();

            if (value != null) {
                return value.ToString();
            } else {
                string error = string.Format("Parameter with key '{0}' absent in DB", key);
                throw new BusinessException(error);
            }
        }
    }
}