using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Xml;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Data {
    public class DOPaper {
        public DOPaper() {}

        public static Paper[] EnumeratePaperByParam(PaperBrandBase paperBrand, Category category, FaceBase face, DeviceBase device, string paperName, PaperBase paperBase, string paperState, Category prohibitedCategory, PaperMetric paperMetric, int minDeviceTypeId, int maxDeviceTypeId) {
            PrPaperEnumByParam prc = new PrPaperEnumByParam();

            if (face != null) {
                prc.FaceId = face.Id;
            }

            if (device != null) {
                prc.DeviceId = device.Id;
            }

            if (paperBrand != null) {
                prc.PaperBrandId = paperBrand.Id;
            }

            if (category != null && category.Id != 0) {
                prc.DeviceTypeId = category.Id;
            }
            
            if (minDeviceTypeId != -1) {
                prc.MinDeviceTypeId = minDeviceTypeId;
            }
            
            if (maxDeviceTypeId != -1) {
                prc.MaxDeviceTypeId = maxDeviceTypeId;
            }
			
			if(prohibitedCategory != null && prohibitedCategory.Id != 0){
			 prc.ProhibitedDeviceTypeId = prohibitedCategory.Id;
			}
/*            if (prohibitedCategoryeviceType != DeviceType.Undefined) {
                prc.ProhibitedDeviceTypeId = (int)prohibitedDeviceType;
            }*/

            if (paperName != null) {
                prc.PaperName = paperName;
            }

            if (paperBase != null) {
                prc.PaperId = paperBase.Id;
            }

            if (paperState != null) {
                prc.PaperState = paperState;
            }

            if (paperMetric != null) {
                prc.MetricId = paperMetric.Id;
            }

            ArrayList papers = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int nameColumnIndex = reader.GetOrdinal("Name");
                int scuColumnIndex = reader.GetOrdinal("Scu");
                int metricIdColumnIndex = reader.GetOrdinal("MetricId");
                int orderColumnIndex = reader.GetOrdinal("Order");
                int orientationColumnIndex = reader.GetOrdinal("Orientation");
                int minPaperAccessColumnIndex = reader.GetOrdinal("MinPaperAccessId");
                while (reader.Read()) {
                    Paper paper = new Paper();
                    paper.Id = reader.GetInt32(idColumnIndex);
                    paper.Name = reader.GetString(nameColumnIndex);
                    paper.Sku = reader.IsDBNull(scuColumnIndex) ? "" : reader.GetString(scuColumnIndex);
                    paper.MetricId = reader.IsDBNull(metricIdColumnIndex) ? 0 : reader.GetInt32(metricIdColumnIndex);
                    paper.Order = reader.GetInt32(orderColumnIndex);
                    paper.Orientation = reader.GetString(orientationColumnIndex);
                    paper.MinPaperAccess = (PaperAccess)reader.GetInt32(minPaperAccessColumnIndex);
                    papers.Add(paper);
                }
            }
            return (Paper[])papers.ToArray(typeof(Paper));
        }

        public static Paper GetPaper(PaperBase paperBase) {
            PrPaperGetById prc = new PrPaperGetById();

            if (paperBase != null) {
                prc.PaperId = paperBase.Id;
            }

            PaperData data = (PaperData)prc.LoadDataSet(new PaperData());
            Paper paper = null;
            if (data.Paper.Count == 1) {
                paper = new Paper();
                paper.Id = data.Paper[0].Id;
                paper.Name = data.Paper[0].Name;
                paper.Size = new SizeF(data.Paper[0].Width, data.Paper[0].Height);
                paper.Brand = DOPaperBrand.GetPaperBrand(new PaperBrandBase(data.Paper[0].BrandId));
                paper.PaperType = (PaperType)PaperType.Parse(typeof(PaperType),data.Paper[0].PaperType.ToString(), true);
                paper.Sku = data.Paper[0].IsScuNull() ? "" : data.Paper[0].Scu;
                paper.PaperState = data.Paper[0].State;
                paper.Orientation = data.Paper[0].Orientation;
                paper.Order = data.Paper[0].Order;
                paper.MetricId = data.Paper[0].IsMetricIdNull() ? 0 : data.Paper[0].MetricId;
                paper.MinPaperAccess = (PaperAccess)data.Paper[0].MinPaperAccessId;

                foreach (PaperData.FaceToPaperRow faceToPaper in data.FaceToPaper) {
                    XmlNode contour = null;
                    if (!faceToPaper.FaceRow.IsContourNull()) {
                        contour = ConvertHelper.ByteArrayToXmlNode(faceToPaper.FaceRow.Contour);
                    }
                    Face face = new Face(faceToPaper.FaceId, contour);
                    face.Guid = faceToPaper.FaceRow.Guid;
                    foreach (PaperData.FaceLocalizationRow localization in faceToPaper.FaceRow.GetFaceLocalizationRows()) {
                        face.AddName(localization.Culture, localization.FaceName);
                    }
                    paper.AddFace(face, faceToPaper.FaceX, faceToPaper.FaceY);
                }
            }
            return paper;
        }

        public static byte[] GetPaperIcon(PaperBase paperBase) {
            PrPaperIconGetById prc = new PrPaperIconGetById();

            prc.PaperId = paperBase.Id;
            using (IDataReader reader = prc.ExecuteReader()) {
                int iconColumnIndex = reader.GetOrdinal("Icon");
                if (reader.Read()) {
                    object icon = reader.GetValue(iconColumnIndex);
                    if (DBNull.Value != icon)
                        return (byte[])icon;
                    else
                        return (new byte[0]);
                } else {
                    return new byte[0];
                }
            }
        }

        public static void Add(Paper paper) {
            if (!paper.IsNew) throw new InvalidOperationException("Adding Paper entity in 'Non-New' status");

            PrPaperIns prc = new PrPaperIns();
            prc.Name = paper.Name;
            prc.PaperBrandId = paper.Brand.Id;
            prc.Height = paper.Size.Height;
            prc.Width = paper.Size.Width;
            prc.PaperType = (int)paper.PaperType;
            prc.Scu = paper.Sku;
            prc.PaperState = paper.PaperState;
            prc.Orientation = paper.Orientation;
            if (paper.MetricId != 0)
                prc.MetricId = paper.MetricId;
            prc.MinPaperAccessId = (int)paper.MinPaperAccess;

            prc.ExecuteNonQuery();
            paper.Id = prc.PaperId.Value;
        }

        public static void Update(Paper paper) {
            if (paper.IsNew) throw new InvalidOperationException("Updating Paper entity in 'New' status");
            PrPaperUpd prc = new PrPaperUpd();
            prc.PaperId = paper.Id;
            prc.Name = paper.Name;
            prc.PaperBrandId = paper.Brand.Id;
            prc.Height = paper.Size.Height;
            prc.Width = paper.Size.Width;
            prc.PaperType = (int)paper.PaperType;
            prc.Scu = paper.Sku;
            prc.PaperState = paper.PaperState;
            prc.Orientation = paper.Orientation;
            prc.Order = paper.Order;
            if (paper.MetricId != 0)
                prc.MetricId = paper.MetricId;
            prc.MinPaperAccessId = (int)paper.MinPaperAccess;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Paper does not exist.");
        }

        public static void UpdateIcon(PaperBase paper, byte[] icon) {
            PrPaperIconUpd prc = new PrPaperIconUpd();
            prc.PaperId = paper.Id;
            prc.Icon = icon;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Paper does not exist.");
        }

        public static void Delete(PaperBase paper) {
            PrPaperDel prc = new PrPaperDel();
            prc.PaperId = paper.Id;
            prc.ExecuteNonQuery();
        }
        public static XmlDocument EnumeratePaperXmlData() {
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("Library");

            foreach(Paper paper in EnumeratePaperByParam(null, null, null, null, string.Empty, null, null, null, null, -1, -1)) {
                XmlNode node = doc.CreateElement("item");

                XmlAttribute idAttr = doc.CreateAttribute("id");
                idAttr.Value = paper.Id.ToString(CultureInfo.InvariantCulture);
                node.Attributes.Append(idAttr);
                XmlAttribute nameAttr = doc.CreateAttribute("name");
                nameAttr.Value = paper.Name.ToString(CultureInfo.InvariantCulture);
                node.Attributes.Append(nameAttr);

                root.AppendChild(node);
            }

            doc.AppendChild(root);
            return doc;
        }

        public static XmlDocument GerPaperFacesXml(PaperBase paper, DeviceType deviceType) {
            return GetPaper(paper).GetXmlContent(deviceType);
        }

        public static Paper[] GetPaperSkuList(Category Category) {
            PrPaperScuGet prc = new PrPaperScuGet();
            if(Category != null && Category.Id != 0)
                prc.DeviceType = Category.Id;

            ArrayList papers = new ArrayList();
            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int scuColumnIndex = reader.GetOrdinal("Scu");
                int stateColumnIndex = reader.GetOrdinal("State");
                while (reader.Read()) {
                    Paper paper = new Paper();
                    paper.Id = reader.GetInt32(idColumnIndex);
                    paper.Sku = reader.GetString(scuColumnIndex);
                    paper.PaperState = reader.GetString(stateColumnIndex);
                    papers.Add(paper);
                }
            }
            return (Paper[])papers.ToArray(typeof(Paper));
        }

        public static DeviceType GetDeviceType(PaperBase paper) {
            PrDeviceTypeGetByPaperId prc = new PrDeviceTypeGetByPaperId();
            prc.PaperId = paper.Id;

            DeviceType deviceType = DeviceType.Undefined;
            
            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                if (reader.Read()) {
                    int deviceTypeId = reader.GetInt32(idColumnIndex);
                    deviceType = (DeviceType)deviceTypeId;
                }
            }
            return deviceType;
        }
    }
}
