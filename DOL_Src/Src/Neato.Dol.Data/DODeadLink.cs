using System;
using System.Collections;
using System.Data;
using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data {
    public class DODeadLink {
        private DODeadLink() {}

        public static void Insert(DeadLink deadlink) {
            PrDeadLinkIns prc = new PrDeadLinkIns();
            prc.Url = deadlink.Url;
            prc.IP = deadlink.IP;
            prc.Referrer = deadlink.Referrer;
            prc.Time = deadlink.Time;
            prc.ExecuteNonQuery();
            deadlink.Id = prc.DeadLinkId.Value;
        }

        public static void Delete(DeadLink deadlink) {
            PrDeadLinkDel prc = new PrDeadLinkDel();
            prc.DeadLinkId = deadlink.Id;
            prc.ExecuteNonQuery();
        }

        public static void DeleteByUrl(DeadLink deadlink) {
            PrDeadLinkDelByUrl prc = new PrDeadLinkDelByUrl();
            prc.Url = deadlink.Url;
            prc.ExecuteNonQuery();
        }

        public static DeadLink[] Enumerate(DateTime startTime, DateTime endTime) {
            return Enumerate(null, startTime, endTime);
        }

        public static DeadLink[] Enumerate(string Url, DateTime startTime, DateTime endTime) {
            PrDeadLinkEnumerate prc = new PrDeadLinkEnumerate();
            prc.StartTime = startTime;
            prc.EndTime = endTime;

            ArrayList list = new ArrayList();
            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int urlColumnIndex = reader.GetOrdinal("Url");
                int ipColumnIndex = reader.GetOrdinal("IP");
                int referrerColumnIndex = reader.GetOrdinal("Referrer");
                int timeColumnIndex = reader.GetOrdinal("Time");
                while (reader.Read()) {
                    list.Add(new DeadLink(reader.GetInt32(idColumnIndex),
                                          reader.GetString(urlColumnIndex),
                                          reader.GetString(ipColumnIndex),
                                          reader.GetString(referrerColumnIndex),
                                          reader.GetDateTime(timeColumnIndex)));
                }
            }
            return (DeadLink[]) list.ToArray(typeof (DeadLink));
        }

        public static DataSet GetReport(DateTime startTime,DateTime endTime) {
            PrDeadLinkGetReportByTime prc = new PrDeadLinkGetReportByTime();
            prc.StartTime = startTime;
            prc.EndTime = endTime;
            DataSet data = new DataSet();
            data.Tables.Add();
            return prc.LoadDataSet(data);
        }
    }
}