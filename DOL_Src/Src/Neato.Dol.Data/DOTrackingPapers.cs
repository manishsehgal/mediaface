using System;
using System.Data;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data
{
	public class DOTrackingPapers {
		public DOTrackingPapers() {}

        public static int Add(Paper paper, string login, DateTime time) {
            PrTrackingPapersIns prc = new PrTrackingPapersIns();
            prc.Paper = paper.Name;
            prc.Time = time;
            prc.Login = login;
            prc.Brand = paper.Brand.Name;
            prc.ExecuteNonQuery();
            return prc.Id.Value;
        }

        public static DataSet GetListByDate(DateTime startTime,DateTime endTime) {
            PrTrackingPapersEnumByTime prc = new PrTrackingPapersEnumByTime();
            prc.TimeEnd = endTime;
            prc.TimeStart = startTime;
            DataSet data = new DataSet();
            data.Tables.Add();
            return prc.LoadDataSet(data);
        }
	}
}
