using System.Data;

using Neato.Dol.Data.DBEngine;
using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity;

namespace Neato.Dol.Data {
    public sealed class DOMail {
        public static void GetMailFormat(MailType mailType, string culture, out string subject, out string body) {
            PrMailFormatGetByCulture prc = new PrMailFormatGetByCulture();
            prc.MailId = (int)mailType;
            prc.Culture = culture;
            prc.ExecuteNonQuery();
            subject = prc.Subject;
            body = prc.Body;
        }

        public static MailFormatData MailFormatEnum() {
            PrMailFormatEnum prc = new PrMailFormatEnum();
            MailFormatData data = new MailFormatData();
            prc.LoadDataSet(data);
            return data;
        }

        public static void MailFormatUpd(MailFormatData data) {
            PrMailFormatUpd prc = new PrMailFormatUpd();
            ProcedureWrapper.UpdateDataTable(data.MailFormat, null, prc, null);
        }

        public static string GetTellAFriendRefusedUserByEmail(string email) {
            PrTellAFriendRefusedUserGet prc = new PrTellAFriendRefusedUserGet();
            prc.Email = email;

            string refusedUserEmail = null;
            using (IDataReader reader = prc.ExecuteReader()) {
                int emailColumnIndex = reader.GetOrdinal("Email");
                if (reader.Read()) {
                    refusedUserEmail = reader.GetString(emailColumnIndex);                    
                }
            }

            return refusedUserEmail;
        }

        public static void InsertTellAFriendRefusedUser(string email) {
            PrTellAFriendRefusedUserIns prc = new PrTellAFriendRefusedUserIns();
            prc.Email = email;
            prc.ExecuteNonQuery();
        }
    }
}