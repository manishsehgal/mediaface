﻿//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a tool.
//     Runtime Version: 1.1.4322.2032
//
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------

namespace Neato.Dol.Data.Scheme {
    using System;
    using System.Data;
    using System.Xml;
    using System.Runtime.Serialization;
    
    
    [Serializable()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Diagnostics.DebuggerStepThrough()]
    [System.ComponentModel.ToolboxItem(true)]
    public class PageData : DataSet {
        
        private PageDataTable tablePage;
        
        public PageData() {
            this.InitClass();
            System.ComponentModel.CollectionChangeEventHandler schemaChangedHandler = new System.ComponentModel.CollectionChangeEventHandler(this.SchemaChanged);
            this.Tables.CollectionChanged += schemaChangedHandler;
            this.Relations.CollectionChanged += schemaChangedHandler;
        }
        
        protected PageData(SerializationInfo info, StreamingContext context) {
            string strSchema = ((string)(info.GetValue("XmlSchema", typeof(string))));
            if ((strSchema != null)) {
                DataSet ds = new DataSet();
                ds.ReadXmlSchema(new XmlTextReader(new System.IO.StringReader(strSchema)));
                if ((ds.Tables["Page"] != null)) {
                    this.Tables.Add(new PageDataTable(ds.Tables["Page"]));
                }
                this.DataSetName = ds.DataSetName;
                this.Prefix = ds.Prefix;
                this.Namespace = ds.Namespace;
                this.Locale = ds.Locale;
                this.CaseSensitive = ds.CaseSensitive;
                this.EnforceConstraints = ds.EnforceConstraints;
                this.Merge(ds, false, System.Data.MissingSchemaAction.Add);
                this.InitVars();
            }
            else {
                this.InitClass();
            }
            this.GetSerializationData(info, context);
            System.ComponentModel.CollectionChangeEventHandler schemaChangedHandler = new System.ComponentModel.CollectionChangeEventHandler(this.SchemaChanged);
            this.Tables.CollectionChanged += schemaChangedHandler;
            this.Relations.CollectionChanged += schemaChangedHandler;
        }
        
        [System.ComponentModel.Browsable(false)]
        [System.ComponentModel.DesignerSerializationVisibilityAttribute(System.ComponentModel.DesignerSerializationVisibility.Content)]
        public PageDataTable Page {
            get {
                return this.tablePage;
            }
        }
        
        public override DataSet Clone() {
            PageData cln = ((PageData)(base.Clone()));
            cln.InitVars();
            return cln;
        }
        
        protected override bool ShouldSerializeTables() {
            return false;
        }
        
        protected override bool ShouldSerializeRelations() {
            return false;
        }
        
        protected override void ReadXmlSerializable(XmlReader reader) {
            this.Reset();
            DataSet ds = new DataSet();
            ds.ReadXml(reader);
            if ((ds.Tables["Page"] != null)) {
                this.Tables.Add(new PageDataTable(ds.Tables["Page"]));
            }
            this.DataSetName = ds.DataSetName;
            this.Prefix = ds.Prefix;
            this.Namespace = ds.Namespace;
            this.Locale = ds.Locale;
            this.CaseSensitive = ds.CaseSensitive;
            this.EnforceConstraints = ds.EnforceConstraints;
            this.Merge(ds, false, System.Data.MissingSchemaAction.Add);
            this.InitVars();
        }
        
        protected override System.Xml.Schema.XmlSchema GetSchemaSerializable() {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            this.WriteXmlSchema(new XmlTextWriter(stream, null));
            stream.Position = 0;
            return System.Xml.Schema.XmlSchema.Read(new XmlTextReader(stream), null);
        }
        
        internal void InitVars() {
            this.tablePage = ((PageDataTable)(this.Tables["Page"]));
            if ((this.tablePage != null)) {
                this.tablePage.InitVars();
            }
        }
        
        private void InitClass() {
            this.DataSetName = "PageData";
            this.Prefix = "";
            this.Namespace = "http://tempuri.org/PageData.xsd";
            this.Locale = new System.Globalization.CultureInfo("en-US");
            this.CaseSensitive = false;
            this.EnforceConstraints = true;
            this.tablePage = new PageDataTable();
            this.Tables.Add(this.tablePage);
        }
        
        private bool ShouldSerializePage() {
            return false;
        }
        
        private void SchemaChanged(object sender, System.ComponentModel.CollectionChangeEventArgs e) {
            if ((e.Action == System.ComponentModel.CollectionChangeAction.Remove)) {
                this.InitVars();
            }
        }
        
        public delegate void PageRowChangeEventHandler(object sender, PageRowChangeEvent e);
        
        [System.Diagnostics.DebuggerStepThrough()]
        public class PageDataTable : DataTable, System.Collections.IEnumerable {
            
            private DataColumn columnPageId;
            
            private DataColumn columnUrl;
            
            private DataColumn columnDescription;
            
            internal PageDataTable() : 
                    base("Page") {
                this.InitClass();
            }
            
            internal PageDataTable(DataTable table) : 
                    base(table.TableName) {
                if ((table.CaseSensitive != table.DataSet.CaseSensitive)) {
                    this.CaseSensitive = table.CaseSensitive;
                }
                if ((table.Locale.ToString() != table.DataSet.Locale.ToString())) {
                    this.Locale = table.Locale;
                }
                if ((table.Namespace != table.DataSet.Namespace)) {
                    this.Namespace = table.Namespace;
                }
                this.Prefix = table.Prefix;
                this.MinimumCapacity = table.MinimumCapacity;
                this.DisplayExpression = table.DisplayExpression;
            }
            
            [System.ComponentModel.Browsable(false)]
            public int Count {
                get {
                    return this.Rows.Count;
                }
            }
            
            internal DataColumn PageIdColumn {
                get {
                    return this.columnPageId;
                }
            }
            
            internal DataColumn UrlColumn {
                get {
                    return this.columnUrl;
                }
            }
            
            internal DataColumn DescriptionColumn {
                get {
                    return this.columnDescription;
                }
            }
            
            public PageRow this[int index] {
                get {
                    return ((PageRow)(this.Rows[index]));
                }
            }
            
            public event PageRowChangeEventHandler PageRowChanged;
            
            public event PageRowChangeEventHandler PageRowChanging;
            
            public event PageRowChangeEventHandler PageRowDeleted;
            
            public event PageRowChangeEventHandler PageRowDeleting;
            
            public void AddPageRow(PageRow row) {
                this.Rows.Add(row);
            }
            
            public PageRow AddPageRow(string Url, string Description) {
                PageRow rowPageRow = ((PageRow)(this.NewRow()));
                rowPageRow.ItemArray = new object[] {
                        null,
                        Url,
                        Description};
                this.Rows.Add(rowPageRow);
                return rowPageRow;
            }
            
            public PageRow FindByPageId(int PageId) {
                return ((PageRow)(this.Rows.Find(new object[] {
                            PageId})));
            }
            
            public System.Collections.IEnumerator GetEnumerator() {
                return this.Rows.GetEnumerator();
            }
            
            public override DataTable Clone() {
                PageDataTable cln = ((PageDataTable)(base.Clone()));
                cln.InitVars();
                return cln;
            }
            
            protected override DataTable CreateInstance() {
                return new PageDataTable();
            }
            
            internal void InitVars() {
                this.columnPageId = this.Columns["PageId"];
                this.columnUrl = this.Columns["Url"];
                this.columnDescription = this.Columns["Description"];
            }
            
            private void InitClass() {
                this.columnPageId = new DataColumn("PageId", typeof(int), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnPageId);
                this.columnUrl = new DataColumn("Url", typeof(string), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnUrl);
                this.columnDescription = new DataColumn("Description", typeof(string), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnDescription);
                this.Constraints.Add(new UniqueConstraint("PageDataKey1", new DataColumn[] {
                                this.columnPageId}, true));
                this.columnPageId.AutoIncrement = true;
                this.columnPageId.AllowDBNull = false;
                this.columnPageId.ReadOnly = true;
                this.columnPageId.Unique = true;
                this.columnUrl.AllowDBNull = false;
                this.columnDescription.AllowDBNull = false;
            }
            
            public PageRow NewPageRow() {
                return ((PageRow)(this.NewRow()));
            }
            
            protected override DataRow NewRowFromBuilder(DataRowBuilder builder) {
                return new PageRow(builder);
            }
            
            protected override System.Type GetRowType() {
                return typeof(PageRow);
            }
            
            protected override void OnRowChanged(DataRowChangeEventArgs e) {
                base.OnRowChanged(e);
                if ((this.PageRowChanged != null)) {
                    this.PageRowChanged(this, new PageRowChangeEvent(((PageRow)(e.Row)), e.Action));
                }
            }
            
            protected override void OnRowChanging(DataRowChangeEventArgs e) {
                base.OnRowChanging(e);
                if ((this.PageRowChanging != null)) {
                    this.PageRowChanging(this, new PageRowChangeEvent(((PageRow)(e.Row)), e.Action));
                }
            }
            
            protected override void OnRowDeleted(DataRowChangeEventArgs e) {
                base.OnRowDeleted(e);
                if ((this.PageRowDeleted != null)) {
                    this.PageRowDeleted(this, new PageRowChangeEvent(((PageRow)(e.Row)), e.Action));
                }
            }
            
            protected override void OnRowDeleting(DataRowChangeEventArgs e) {
                base.OnRowDeleting(e);
                if ((this.PageRowDeleting != null)) {
                    this.PageRowDeleting(this, new PageRowChangeEvent(((PageRow)(e.Row)), e.Action));
                }
            }
            
            public void RemovePageRow(PageRow row) {
                this.Rows.Remove(row);
            }
        }
        
        [System.Diagnostics.DebuggerStepThrough()]
        public class PageRow : DataRow {
            
            private PageDataTable tablePage;
            
            internal PageRow(DataRowBuilder rb) : 
                    base(rb) {
                this.tablePage = ((PageDataTable)(this.Table));
            }
            
            public int PageId {
                get {
                    return ((int)(this[this.tablePage.PageIdColumn]));
                }
                set {
                    this[this.tablePage.PageIdColumn] = value;
                }
            }
            
            public string Url {
                get {
                    return ((string)(this[this.tablePage.UrlColumn]));
                }
                set {
                    this[this.tablePage.UrlColumn] = value;
                }
            }
            
            public string Description {
                get {
                    return ((string)(this[this.tablePage.DescriptionColumn]));
                }
                set {
                    this[this.tablePage.DescriptionColumn] = value;
                }
            }
        }
        
        [System.Diagnostics.DebuggerStepThrough()]
        public class PageRowChangeEvent : EventArgs {
            
            private PageRow eventRow;
            
            private DataRowAction eventAction;
            
            public PageRowChangeEvent(PageRow row, DataRowAction action) {
                this.eventRow = row;
                this.eventAction = action;
            }
            
            public PageRow Row {
                get {
                    return this.eventRow;
                }
            }
            
            public DataRowAction Action {
                get {
                    return this.eventAction;
                }
            }
        }
    }
}
