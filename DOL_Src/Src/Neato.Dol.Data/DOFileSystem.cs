using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Helpers;

namespace Neato.Dol.Data {
    public class DOFileSystem {
        public static ImageLibraryData GetImageLibraryData
            (string imagePath) {
            ImageLibraryData data = new ImageLibraryData();
            CreateFolderContent(imagePath, data, null);
            return data;
        }

        private static void CreateFolderContent(string folderPath, ImageLibraryData data, ImageLibraryData.FolderRow parentFolder) {
            string[] pathItems = folderPath.Split('\\');
            string folderCaption = pathItems[pathItems.Length - 1];

            ImageLibraryData.FolderRow folder = null;

            if (parentFolder == null) {
                folder = data.Folder.NewFolderRow();
                data.Folder.AddFolderRow(folder);
            } else {
                folder = data.Folder.AddFolderRow(parentFolder, "", 0, (int)ImageLibraries.Expanded);
            }

            data.FolderLocalization.AddFolderLocalizationRow(
                folder, "", folderCaption);

            foreach (string fileName in Directory.GetFiles(folderPath)) {
                FileStream fileStream =
                    new FileStream(fileName, FileMode.Open, FileAccess.Read);
                BinaryReader reader = new BinaryReader(fileStream);

                byte[] buffer = new byte[fileStream.Length];
                reader.Read(buffer, 0, (int)fileStream.Length);

                reader.Close();
                fileStream.Close();

                ImageLibraryData.LibraryItemRow item =
                    data.LibraryItem.AddLibraryItemRow(
                        ImageHelper.ConvertImageToJpeg(buffer));
                data.FolderItem.AddFolderItemRow(folder, item);
            }

            foreach (string dirName in Directory.GetDirectories(folderPath)) {
                CreateFolderContent(dirName, data, folder);
            }
        }

        public static byte[] ReadFile(string filePath) {
            byte[] data = null;
            try {
                using (FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.None)) {
                    int length = (int)stream.Length;
                    data = new byte[length];
                    stream.Read(data, 0, length);
                }
            }
            catch(Exception ex) {
                throw ex;
            }
            return data;
        }

        public static string ReadTextFile(string filePath) {
            string text = string.Empty;
            try {
                StreamReader sr = new StreamReader(filePath);
                text = sr.ReadLine();
                sr.Close();
            }
            catch(Exception ex) {
                throw ex;
            }
            return text;
        }

        public static void WriteFile(byte[] file, string filePath) {
            try {
                using (FileStream stream = File.Open(filePath, FileMode.Create, FileAccess.Write, FileShare.None)) {
                    stream.Write(file, 0, file.Length);
                }
            } catch (Exception ex) {
                throw ex;
            }
        }

        public static bool DirectoryExist(string path) {
            return Directory.Exists(path);
        }

        public static bool FileExist(string path) {
            return File.Exists(path);
        }

        public static string[] GetFiles(string path) {
            return Directory.GetFiles(path);
        }

        public static string[] GetFiles(string path, string pattern, bool recursive) {
            ArrayList list = new ArrayList();
            list.AddRange(Directory.GetFiles(path, pattern));
            if (recursive) {
                string[] directories = Directory.GetDirectories(path);
                foreach(string directory in directories)
                    list.AddRange(GetFiles(Path.Combine(path, directory), pattern, true));
            }
            return (string[])list.ToArray(typeof(string));
        }

        public static DirectoryInfo CreateDirectory(string path) {
            return Directory.CreateDirectory(path);
        }

        public static void DeleteFile(string path) {
            try {
                File.Delete(path);
            } 
            catch (Exception ex) {
                throw ex;
            }
        }

        public static void UnsetReadOnlyAttribute(string path) {
            FileAttributes fileAttributes = File.GetAttributes(path);
            if ((fileAttributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) {
                File.SetAttributes(path, fileAttributes &= ~FileAttributes.ReadOnly);
            }
        }

        public static void WriteXml(XmlDocument xmlDoc, string filePath) {
            try {
                using (FileStream stream = File.Open(filePath, FileMode.Create, FileAccess.Write, FileShare.None)) {
                    XmlTextWriter writer = new XmlTextWriter(stream, Encoding.UTF8);
                    xmlDoc.WriteTo(writer);
                    writer.Close();
                }
            } catch (Exception ex) {
                throw ex;
            }
        }

        public static XmlDocument LoadXml(string filePath) {
            XmlDocument xmlDoc = new XmlDocument();
            try {
                using (FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                    XmlTextReader reader = new XmlTextReader(stream);
                    xmlDoc.Load(reader);
                    reader.Close();
                    return xmlDoc;
                }
            } catch (Exception ex) {
                throw ex;
            }
        }
    }
}