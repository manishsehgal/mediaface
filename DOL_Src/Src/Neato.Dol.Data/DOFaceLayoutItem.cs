using System.Data;
using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data {
	public class DOFaceLayoutItem {
		public DOFaceLayoutItem() {
		}

        public static void Insert(FaceLayout faceLayout, FaceLayoutItem faceLayoutItem) {
            PrFaceLayoutItemIns prc = new PrFaceLayoutItemIns();
            prc.FaceLayoutId = faceLayout.Id;
            prc.Align = faceLayoutItem.Align;
            prc.Angle = faceLayoutItem.Angle;
            prc.Bold = faceLayoutItem.IsBold;
            prc.Color = faceLayoutItem.Color.ToArgb();
            if(faceLayoutItem.EffectNode != null)
                prc.EffectNode = faceLayoutItem.EffectNode.OuterXml;
            prc.Font = faceLayoutItem.Font;
            prc.Height = faceLayoutItem.Height;
            prc.Italic = faceLayoutItem.IsItalic;
            prc.Position = faceLayoutItem.Position;
            prc.Size = faceLayoutItem.Size;
            prc.Text = faceLayoutItem.Text;
            prc.UnitType = faceLayoutItem.UnitType;
            prc.Width = faceLayoutItem.Width;
            prc.X = faceLayoutItem.X;
            prc.Y = faceLayoutItem.Y;
            prc.ScaleX = faceLayoutItem.ScaleX;
            prc.ScaleY = faceLayoutItem.ScaleY;

            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Label layout does not exist.");
            faceLayoutItem.Id = prc.FaceLayoutItemId.Value;
        }

        public static void Update(FaceLayoutItem faceLayoutItem) {
            PrFaceLayoutItemUpd prc = new PrFaceLayoutItemUpd();
            prc.FaceLayoutItemId = faceLayoutItem.Id;
            prc.Text = faceLayoutItem.Text;
            prc.ExecuteNonQuery();
        }
        
        public static void Delete(FaceLayout faceLayout) {
            PrFaceLayoutItemDel prc = new PrFaceLayoutItemDel();
            prc.FaceLayoutId = faceLayout.Id;
            prc.ExecuteNonQuery();
        }
    }
}
