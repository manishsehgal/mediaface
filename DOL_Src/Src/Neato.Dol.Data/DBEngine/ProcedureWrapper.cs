using System;
using System.Data;

using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Neato.Dol.Data.DBEngine {
    /// <summary>
    /// Base class for all stored procedure wrappers
    /// </summary>
    internal abstract class ProcedureWrapper {
        protected readonly DBCommandWrapper RealWrapper;
        private readonly Database commandDatabase;
        private readonly string databaseInstanceName;

        protected ProcedureWrapper(string databaseInstanceName, string procedureName) {
            this.databaseInstanceName = databaseInstanceName;
            this.commandDatabase = DatabaseManager.GetDatabase(databaseInstanceName);
            RealWrapper = commandDatabase.GetStoredProcCommandWrapper(procedureName);
            InitializeParameters();
        }

        /// <summary>
        /// Add parameters to command.
        /// Should be overridden in the real wrapper.
        /// </summary>
        protected abstract void InitializeParameters();

        /// <summary>
        /// Executes the command and returns an IDataReader through which the result can be read. 
        /// It is the responsibility of the caller to close the connection and reader when finished.
        /// </summary>
        public IDataReader ExecuteReader() {
            IDbTransaction trans = TransactionalContext.GetTransaction(databaseInstanceName);
            if (null == trans) {
                // Connection opened by EntLib and will be closed after reader is closed
                return commandDatabase.ExecuteReader(RealWrapper);
            } else {
                // Connection already opened by TransactionalContext and will be closed after transaction is finished by TransactionalContext
                return commandDatabase.ExecuteReader(RealWrapper, trans);
            }
        }

        /// <summary>
        /// Executes the command and returns the first column of the first row in the resultset returned by the query. 
        /// Extra columns or rows are ignored.
        /// </summary>
        public object ExecuteScalar() {
            IDbTransaction trans = TransactionalContext.GetTransaction(databaseInstanceName);
            if (null == trans) {
                // Connection opened and closed by EntLib
                return commandDatabase.ExecuteScalar(RealWrapper);
            } else {
                // Connection already opened by TransactionalContext and will be closed after transaction is finished by TransactionalContext
                return commandDatabase.ExecuteScalar(RealWrapper, trans);
            }
        }

        /// <summary>
        /// Executes the command and returns the number of rows affected.
        /// </summary>
        public int ExecuteNonQuery() {
            IDbTransaction trans = TransactionalContext.GetTransaction(databaseInstanceName);
            if (null == trans) {
                // Connection opened and closed by EntLib
                commandDatabase.ExecuteNonQuery(RealWrapper);
            } else {
                // Connection already opened by TransactionalContext and will be closed after transaction is finished by TransactionalContext
                commandDatabase.ExecuteNonQuery(RealWrapper, trans);
            }
            return RealWrapper.RowsAffected;
        }

        /// <summary>
        /// Execute the command and return the results in a new DataSet
        /// </summary>
        public DataSet ExecuteDataSet() {
            IDbTransaction trans = TransactionalContext.GetTransaction(databaseInstanceName);
            if (null == trans) {
                // Connection opened by DataAdapter inside EntLib and closed by EntLib
                return commandDatabase.ExecuteDataSet(RealWrapper);
            } else {
                // Connection already opened by TransactionalContext and will be closed after transaction is finished by TransactionalContext
                return commandDatabase.ExecuteDataSet(RealWrapper, trans);
            }
        }

        /// <summary>
        /// Execute the command and load existing DataSet.
        /// </summary>
        /// <param name="ds">Dataset to fill</param>
        /// <returns>The same value as ds parameter</returns>
        public DataSet LoadDataSet(DataSet ds) {
            string[] tableNames = new string[ds.Tables.Count];
            for (int i = 0; i < ds.Tables.Count; i++) {
                tableNames[i] = ds.Tables[i].TableName;
            }
            IDbTransaction trans = TransactionalContext.GetTransaction(databaseInstanceName);
            ds.EnforceConstraints = false;
            if (null == trans) {
                // Connection opened by DataAdapter inside EntLib and closed by EntLib
                commandDatabase.LoadDataSet(RealWrapper, ds, tableNames);
            } else {
                // Connection already opened by TransactionalContext and will be closed after transaction is finished by TransactionalContext
                commandDatabase.LoadDataSet(RealWrapper, ds, tableNames, trans);
            }
            ds.EnforceConstraints = true;
            return ds;
        }

        /// <summary>
        /// Execute the command and load existing DataTable.
        /// </summary>
        /// <param name="dt">DataTable to fill</param>
        /// <returns>The same value as dt parameter</returns>
        public DataTable LoadDataTable(DataTable dt) {
            if (null == dt.DataSet) throw new ArgumentException("Table should be in dataset", "dt");
            IDbTransaction trans = TransactionalContext.GetTransaction(databaseInstanceName);
            if (null == trans) {
                // Connection opened by DataAdapter inside EntLib and closed by EntLib
                commandDatabase.LoadDataSet(RealWrapper, dt.DataSet, new string[] {dt.TableName});
            } else {
                // Connection already opened by TransactionalContext and will be closed after transaction is finished by TransactionalContext
                commandDatabase.LoadDataSet(RealWrapper, dt.DataSet, new string[] {dt.TableName}, trans);
            }
            return dt;
        }

        /// <summary>
        /// Calls the respective INSERT, UPDATE, or DELETE statements for each inserted, updated, or deleted row in the table
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="insertCommand"></param>
        /// <param name="updateCommand"></param>
        /// <param name="deleteCommand"></param>
        /// <returns>number of records affected</returns>
        public static int UpdateDataTable(DataTable dataTable,
                                          ProcedureWrapper insertCommand,
                                          ProcedureWrapper updateCommand,
                                          ProcedureWrapper deleteCommand) {
            // Determine database for update
            ProcedureWrapper firstNotNullWrapper = null;
            foreach (ProcedureWrapper wrapper in new ProcedureWrapper[] {insertCommand, updateCommand, deleteCommand}) {
                if (null == wrapper) continue;
                if (null == firstNotNullWrapper) firstNotNullWrapper = wrapper;
                else {
                    if (firstNotNullWrapper.commandDatabase.ConfigurationName != wrapper.commandDatabase.ConfigurationName) {
                        throw new DAException("All commands must be for the same database");
                    }
                }
            }
            if (null == firstNotNullWrapper) {
                throw new DAException("Cannot detect database, probably all commands are null");
            }
            Database db = firstNotNullWrapper.commandDatabase;
            string databaseInstanceName = firstNotNullWrapper.databaseInstanceName;
            if (null == dataTable.DataSet) throw new ArgumentException("Table should be in dataset", "dataTable");

            IDbTransaction trans = TransactionalContext.GetTransaction(databaseInstanceName);
            if (null == trans) {
                // Connection opened by DataAdapter inside EntLib and closed by EntLib
                return db.UpdateDataSet(dataTable.DataSet, dataTable.TableName,
                                        (null == insertCommand) ? null : insertCommand.RealWrapper,
                                        (null == updateCommand) ? null : updateCommand.RealWrapper,
                                        (null == deleteCommand) ? null : deleteCommand.RealWrapper,
                                        UpdateBehavior.Standard);
            } else {
                // Connection already opened by TransactionalContext and will be closed after transaction is finished by TransactionalContext
                return db.UpdateDataSet(dataTable.DataSet, dataTable.TableName,
                                        (null == insertCommand) ? null : insertCommand.RealWrapper,
                                        (null == updateCommand) ? null : updateCommand.RealWrapper,
                                        (null == deleteCommand) ? null : deleteCommand.RealWrapper,
                                        trans);
            }
        }

        /// <summary>
        /// Gets or sets the wait time before terminating the attempt to execute a command and generating an error.
        /// </summary>
        public int CommandTimeout {
            get { return RealWrapper.CommandTimeout; }
            set { RealWrapper.CommandTimeout = value; }
        }

        /// <summary>
        /// Gets the rows count affected by this command.
        /// </summary>
        public int RowsAffected {
            get { return RealWrapper.RowsAffected; }
        }
    }

}