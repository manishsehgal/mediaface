using System;
using System.Collections;
using System.Collections.Specialized;

using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Neato.Dol.Data.DBEngine {
    public sealed class DatabaseManager {
        private static IDictionary databases = new ListDictionary();

        private DatabaseManager() {}

        public static Database GetDatabase(string databaseInstanceName) {
            Database db = (Database)databases[databaseInstanceName];
            if (null != db) return db;

            // This check is not at the function start for performance reason
            if (null == databaseInstanceName) throw new ArgumentException();

            lock (databases.SyncRoot) {
                db = (Database)databases[databaseInstanceName];
                if (null != db) return db;
                db = (0 == databaseInstanceName.Length)
                    ? DatabaseFactory.CreateDatabase()
                    : DatabaseFactory.CreateDatabase(databaseInstanceName);
                databases[databaseInstanceName] = db;
            }
            return db;
        }
    }
}