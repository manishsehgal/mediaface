using System.Collections;
using System.Data;
using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data {
    public sealed class DOPaperMetric {
        private DOPaperMetric() {
        }

        public static PaperMetric[] Enum() {
            PrPaperMetricEnum prc = new PrPaperMetricEnum();

            ArrayList paperMetrics = new ArrayList();
            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int nameColumnIndex = reader.GetOrdinal("Name");
                while (reader.Read()) {
                    int id = reader.GetInt32(idColumnIndex);
                    string name = reader.GetString(nameColumnIndex);
                    paperMetrics.Add(new PaperMetric(id, name));
                }
            }
            return (PaperMetric[]) paperMetrics.ToArray(typeof (PaperMetric));
        }
    }
}