using System.Collections;
using System.Data;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data {
    public sealed class DOSupportPage {
        private DOSupportPage() {}

        public static LocalizedText[] Enum(string culture) {
            PrSupportPageEnumByCulture prc = new PrSupportPageEnumByCulture();
            prc.Culture = culture;

            ArrayList list = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int CultureIndex = reader.GetOrdinal("Culture");
                int HtmlTextIndex = reader.GetOrdinal("HtmlText");
                while (reader.Read()) {
                    LocalizedText localizedText = new LocalizedText();
                    localizedText.Culture = reader.GetString(CultureIndex);
                    localizedText.Text = reader.GetString(HtmlTextIndex);
                    list.Add(localizedText);
                }
            }
            return (LocalizedText[])list.ToArray(typeof(LocalizedText));
        }

        public static void Insert(string culture, string htmlText) {
            PrSupportPageIns prc = new PrSupportPageIns();
            prc.Culture = culture;
            prc.HtmlText = htmlText;
            prc.ExecuteNonQuery();
        }

        public static void Update(string culture, string htmlText) {
            PrSupportPageUpd prc = new PrSupportPageUpd();
            prc.Culture = culture;
            prc.HtmlText = htmlText;
            prc.ExecuteNonQuery();
        }

        public static void Delete(string culture) {
            PrSupportPageDel prc = new PrSupportPageDel();
            prc.Culture = culture;
            prc.ExecuteNonQuery();
        }
    }
}