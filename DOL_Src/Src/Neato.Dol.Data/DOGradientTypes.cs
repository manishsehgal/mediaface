using System.IO;
using System.Xml;

namespace Neato.Dol.Data {
    public class DOGradientTypes {
        private DOGradientTypes() {}

        public static XmlNode GetGradientTypes(string applicationRoot) {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(applicationRoot, "GradientTypes.xml"));
            return xmlDoc.DocumentElement;
        }
    }
}