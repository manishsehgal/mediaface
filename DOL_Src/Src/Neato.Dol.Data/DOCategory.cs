using System;
using System.Collections;
using System.Data;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data {
    public class DOCategory {
        public DOCategory() {}

        public static Category[] Enumerate() {
            PrCategoryEnum prc = new PrCategoryEnum();

            ArrayList Categorys = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int idColumnName = reader.GetOrdinal("Name");
                int idColumnVisible = reader.GetOrdinal("Visible");
                int idColumnTarget = reader.GetOrdinal("Target");
                int idColumnSortOrder = reader.GetOrdinal("SortOrder");
                while (reader.Read()) {
                    Category Category = new Category();
                    Category.Id = reader.GetInt32(idColumnIndex);
                    Category.Name = reader.GetString(idColumnName);
                    Category.Visible = reader.GetBoolean(idColumnVisible);
                    Category.TargetPage = reader.GetInt32(idColumnTarget);
                    Category.SortOrder = reader.GetInt32(idColumnSortOrder);
                    Categorys.Add(Category);
                }
            }
            return (Category[])Categorys.ToArray(typeof(Category));
        }

        public static Category[] GetDeviceCategories() {
            int mp3CategoryId = (int)DeviceType.MP3Player;
            int minCategoryId = (int)DeviceType.OtherLabels + 1;

            PrCategoryDeviceEnum prc = new PrCategoryDeviceEnum();
            prc.Mp3CategoryId = mp3CategoryId;
            prc.MinCategoryId = minCategoryId;

            ArrayList Categorys = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int idColumnName = reader.GetOrdinal("Name");
                int idColumnVisible = reader.GetOrdinal("Visible");
                int idColumnTarget = reader.GetOrdinal("Target");
                int idColumnSortOrder = reader.GetOrdinal("SortOrder");
                while (reader.Read()) {
                    Category Category = new Category();
                    Category.Id = reader.GetInt32(idColumnIndex);
                    Category.Name = reader.GetString(idColumnName);
                    Category.Visible = reader.GetBoolean(idColumnVisible);
                    Category.TargetPage = reader.GetInt32(idColumnTarget);
                    Category.SortOrder = reader.GetInt32(idColumnSortOrder);
                    Categorys.Add(Category);
                }
            }
            return (Category[])Categorys.ToArray(typeof(Category));
        }

        public static byte[] GetCategoryIcon(CategoryBase Category) {
            PrCategoryIconGetById prc = new PrCategoryIconGetById();

            prc.CategoryId = Category.Id;
            using (IDataReader reader = prc.ExecuteReader()) {
                int iconColumnIndex = reader.GetOrdinal("Icon");
                if (reader.Read()) {
                    object icon = reader.GetValue(iconColumnIndex);
                    if (DBNull.Value != icon)
                        return (byte[])icon;
                    else
                        return (new byte[0]);
                } else {
                    return new byte[0];
                }
            }
        }

        public static void Add(Category Category) {
            if (!Category.IsNew) throw new InvalidOperationException("Adding Category entity in 'Non-New' status");
            PrCategoryIns prc = new PrCategoryIns();
            prc.Name = Category.Name;
            prc.Visible = Category.Visible;
            prc.Target = Category.TargetPage;
            prc.SortOrder = Category.SortOrder;
            prc.ExecuteNonQuery();
            Category.Id = prc.CategoryId.Value;
        }

        public static void Update(Category Category) {
            if (Category.IsNew) throw new InvalidOperationException("Updating Category entity in 'New' status");
            PrCategoryUpd prc = new PrCategoryUpd();
            prc.CategoryId = Category.Id;
            prc.Name = Category.Name;
            prc.Visible = Category.Visible;
            prc.Target = Category.TargetPage;
            prc.SortOrder = Category.SortOrder;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Category does not exist.");
        }

        public static void UpdateIcon(CategoryBase Category, byte[] icon) {
            PrCategoryIconUpd prc = new PrCategoryIconUpd();
            prc.CategoryId = Category.Id;
            prc.Icon = icon;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Category does not exist.");
        }

        public static void Delete(CategoryBase Category) {
            PrCategoryDel prc = new PrCategoryDel();
            prc.CategoryId = Category.Id;
            prc.ExecuteNonQuery();
        }

        public static Category GetCategory(CategoryBase Category) {
            PrCategoryGetById prc = new PrCategoryGetById();
            prc.CategoryId = Category.Id;
            Category resultCategory = null;

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int nameColumnIndex = reader.GetOrdinal("Name");
                int idColumnVisible = reader.GetOrdinal("Visible");
                int idColumnTarget = reader.GetOrdinal("Target");
                int idColumnSortOrder = reader.GetOrdinal("SortOrder");
                if (reader.Read()) {
                    resultCategory = new Category();
                    resultCategory.Id = reader.GetInt32(idColumnIndex);
                    resultCategory.Name = reader.GetString(nameColumnIndex);
                    resultCategory.Visible = reader.GetBoolean(idColumnVisible);
                    resultCategory.TargetPage = reader.GetInt32(idColumnTarget);
                    resultCategory.SortOrder = reader.GetInt32(idColumnSortOrder);
                }
            }
            return resultCategory;
        }
    }
}