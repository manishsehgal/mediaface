using System;
using System.Collections;
using System.Data;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;
using Neato.Dol.Entity.Exceptions;

namespace Neato.Dol.Data
{
	public class DOTemplate{
		public DOTemplate(){}
        public static Template[] EnumTemplates(FaceBase face) {
            PrTemplateEnumByFaceId prc = new PrTemplateEnumByFaceId();
            if(face != null)
                prc.FaceId = face.Id;
            ArrayList templates = new ArrayList();
            using (IDataReader reader = prc.ExecuteReader()) 
            {
                int idColumnIndex = reader.GetOrdinal("Id");
                int idColumnName = reader.GetOrdinal("Name");
                int idColumnXml = reader.GetOrdinal("Xml");
                while (reader.Read()) 
                {
                    Template template = new Template();
                    template.Id = reader.GetInt32(idColumnIndex);
                    template.Name = reader.GetString(idColumnName);
                    template.XmlValue = reader.GetValue(idColumnXml) as string;
                    template.FaceId = face.Id;
                    templates.Add(template);
                }
            }
            return templates.ToArray(typeof(Template)) as Template[];
        }
        public static byte[] GetTemplateIcon(Template template) {
            PrTemplateIconEnumById prc = new PrTemplateIconEnumById();

            prc.TemplateId = template.Id;
            using (IDataReader reader = prc.ExecuteReader()) 
            {
                int streamColumnIndex = reader.GetOrdinal("Stream");
                if (reader.Read()) 
                {
                    object icon = reader.GetValue(streamColumnIndex);
                    if (DBNull.Value != icon)
                        return (byte[])icon;
                    else
                        return (new byte[0]);
                } 
                else 
                {
                    return new byte[0];
                }
            }
            
        }
        public static Template[] EnumerateTemplates(DeviceType deviceType, Face face, DeviceBase device) 
        {
            PrTemplateEnumByDeviceTypeIdDeviceIdFaceId prc = new PrTemplateEnumByDeviceTypeIdDeviceIdFaceId();
            if (face != null) 
                prc.FaceId = face.Id;
            if (device != null) 
                prc.DeviceId = device.Id;
            if (deviceType != DeviceType.Undefined) 
                prc.DeviceTypeId = (int)deviceType;

            ArrayList list = new ArrayList();
            using (IDataReader reader = prc.ExecuteReader()) 
            {
                int idColumnIndex = reader.GetOrdinal("TemplateId");
                int nameColumnIndex = reader.GetOrdinal("Name");
                int xmlColumnIndex = reader.GetOrdinal("Xml");
                int faceIdColumnIndex = reader.GetOrdinal("FaceId");
                
                
                while (reader.Read()) 
                {
                    Template template = new Template();
                    template.Id = reader.GetInt32(idColumnIndex);
                    template.Name = reader.GetString(nameColumnIndex);
                    template.XmlValue = reader.GetString(xmlColumnIndex);
                    template.FaceId = reader.GetInt32(faceIdColumnIndex);
                    list.Add(template);
                    
                }
                
            }
            return (Template[])list.ToArray(typeof(Template));
        }

        public static void Update(Template template) {
            PrTemplateUpd prc = new PrTemplateUpd();
            prc.Name = template.Name;
            prc.Xml = template.XmlValue;
            prc.TemplateId = template.Id;
            prc.ExecuteNonQuery();
        }

        public static void Delete(Template template) {
            PrTemplateDel prc = new PrTemplateDel();
            prc.TemplateId = template.Id;
            prc.ExecuteNonQuery();
        }
        
        public static void Insert(Template template) {
            PrTemplateIns prc = new PrTemplateIns();
            prc.Name = template.Name;
            prc.Xml = template.XmlValue;
            prc.FaceId = template.FaceId;
            prc.ExecuteNonQuery();
            template.Id = prc.TemplateId.Value;
        }
        
        public static void TemplateIconInsert(Template template, byte[] data) 
        {
            if(data.Length == 0) 
            {
                throw new BusinessException("Wrong file format or corrupted file");
            }
            PrTemplateIconIns prc = new PrTemplateIconIns();
            prc.Stream = data;
            prc.TemplateId = template.Id;
            prc.ExecuteNonQuery();
            
            
        }

        public static void TemplateIconUpdate(Template template, byte[] data) 
        {
            PrTemplateIconUpd prc = new PrTemplateIconUpd();
            prc.TemplateId = template.Id;
            prc.Stream = data;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Template icon was not updated.");
        }

        public static void TemplateIconDelete(Template template) 
        {
            PrTemplateIconDel prc = new PrTemplateIconDel();
            prc.TemplateId = template.Id;
            prc.ExecuteNonQuery();
            
        }
        
        public static void InsertMediaForTemplate(Template template, Guid guid, byte[] media) {
            PrTemplateMediaInsert prc = new PrTemplateMediaInsert();
            prc.TemplateId = template.Id;
            prc.Guid = guid;
            prc.Media = media;
            prc.ExecuteNonQuery();
            
        }

        public static byte[] GetMediaByTemplateAndGuid(Template template, Guid guid) {
            PrTemplateGetMediaByTemplateAndGuid prc = new PrTemplateGetMediaByTemplateAndGuid();
            prc.TemplateId = template.Id;
            prc.Guid = guid;
            using (IDataReader reader = prc.ExecuteReader()) 
            {
                int streamColumnIndex = reader.GetOrdinal("Stream");
                if (reader.Read()) 
                {
                    object icon = reader.GetValue(streamColumnIndex);
                    if (DBNull.Value != icon)
                        return (byte[])icon;
                    else
                        return (new byte[0]);
                } 
                else 
                {
                    return new byte[0];
                }
            }
        }
        public static void DeleteMediaForTemplate(Template template) {
            PrTemplateMediaDelete prc = new PrTemplateMediaDelete();
            prc.TemplateId = template.Id;
            prc.ExecuteNonQuery();
        }


	}
}
