using System.Data;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data {
    public sealed class DOBanner {
        private DOBanner() {}

        public static Banner Get(int bannerId) {
            PrBannerGet prc = new PrBannerGet();
            prc.Id = bannerId;
            
            Banner banner = null;
            using (IDataReader reader = prc.ExecuteReader()) {
                int IsVisibleIndex = reader.GetOrdinal("IsVisible");
                if (reader.Read()) {
                    banner = new Banner();
                    banner.IsVisible = reader.GetBoolean(IsVisibleIndex);
                    banner.Id = bannerId;
                }
            }

            return banner;
        }

        public static void Insert(Banner banner) {
            PrBannerIns prc = new PrBannerIns();
            prc.IsVisible = banner.IsVisible;
            prc.Id = banner.Id;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Banner exists already.");
        }

        public static void Update(Banner banner) {
            PrBannerUpd prc = new PrBannerUpd();
            prc.Id = banner.Id;
            prc.IsVisible = banner.IsVisible;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Banner does not exist.");
        }
    }
}