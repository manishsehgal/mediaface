using System;

using System.Data;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity.Tracking;

namespace Neato.Dol.Data
{

    public class DOTrackingFeatures
    {
        public DOTrackingFeatures(){}
        public static int Add(Feature feature, string login, DateTime time) 
        {
            PrTrackingFeaturesIns prc = new PrTrackingFeaturesIns();
            prc.Action = feature.ToString();
            prc.Time = time;
            prc.Login = login;
            prc.ExecuteNonQuery();
            return prc.Id.Value;
        }

        public static DataSet GetFeaturesListByDate(DateTime startTime,DateTime endTime) 
        {
            PrTrackingFeaturesEnumByTime  prc = new PrTrackingFeaturesEnumByTime();
            prc.TimeEnd = endTime;
            prc.TimeStart = startTime;
            DataSet data = new DataSet();
            data.Tables.Add("Features");
            return prc.LoadDataSet(data);
        }

     
    }
}
