using System;
using System.Data;
using Neato.Dol.Data.DbProcedures;

namespace Neato.Dol.Data {
    public sealed class DOTrackingUpcStapleCodes {
        private DOTrackingUpcStapleCodes() {
        }

        public static int Insert(string upc, string user, DateTime time) {
            PrTrackingUpcStapleCodeIns prc = new PrTrackingUpcStapleCodeIns();
            prc.Upc = upc;
            prc.User = user;
            prc.Time = time;

            prc.ExecuteNonQuery();
            return prc.Id.Value;
        }

        public static DataSet EnumByTime(DateTime startTime, DateTime endTime) {
            PrTrackingUpcStapleCodesEnumByTime prc = new PrTrackingUpcStapleCodesEnumByTime();
            prc.TimeEnd = endTime;
            prc.TimeStart = startTime;
            DataSet data = new DataSet();
            data.Tables.Add("Upc");
            return prc.LoadDataSet(data);
        }
    }
}