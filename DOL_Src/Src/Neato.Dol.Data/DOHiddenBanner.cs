using System.Data;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data {
	public sealed class DOHiddenBanner {
		public DOHiddenBanner()	{}

        public static void Insert(HiddenBanner banner) {
            PrHiddenBannerIns prc = new PrHiddenBannerIns();
            prc.BannerId = banner.BannerId;
            prc.PageName = banner.PageName;
            prc.ExecuteNonQuery();
        }

        public static void Delete(HiddenBanner banner) {
            PrHiddenBannerDel prc = new PrHiddenBannerDel();
            prc.BannerId = banner.BannerId;
            prc.PageName = banner.PageName;
            prc.ExecuteNonQuery();
        }

        public static HiddenBanner GetHiddenBanner(HiddenBanner banner) {
            PrHiddenBannerGet prc = new PrHiddenBannerGet();
            prc.BannerId = banner.BannerId;
            prc.PageName = banner.PageName;
            
            HiddenBanner bannerResult = null;

            using (IDataReader reader = prc.ExecuteReader()) {
                int pageNameColumnIndex = reader.GetOrdinal("PageName");
                int bannerIdColumnIndex = reader.GetOrdinal("BannerId");
                
                if (reader.Read()) {
                    bannerResult = new HiddenBanner();
                    bannerResult.PageName = reader.GetString(pageNameColumnIndex);
                    bannerResult.BannerId = reader.GetInt32(bannerIdColumnIndex);
                }
            }
            return bannerResult;
        
        }
	}
}
