using System.Collections;
using System.Data;

using Neato.Dol.Data.DBEngine;
using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data {
    public sealed class DORetailer {
        private DORetailer() {}

        private static Retailer[] ReadRetailerInfo(ProcedureWrapper prc) {
            ArrayList retailers = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int idColumnName = reader.GetOrdinal("Name");
                int idColumnDisplayName = reader.GetOrdinal("DisplayName");
                int idColumnUrl = reader.GetOrdinal("Url");
                int idColumnHasExitPage = reader.GetOrdinal("HasExitPage");
                int idColumnIcon = reader.GetOrdinal("Icon");
                string displayName;
                byte[] icon;
                while (reader.Read()) {
                    Retailer retailer = new Retailer();
                    retailer.Id = reader.GetInt32(idColumnIndex);
                    retailer.Name = reader.GetString(idColumnName);
                    displayName = reader.GetValue(idColumnDisplayName) as string;
                    retailer.DisplayName = displayName == null ? string.Empty : displayName;
                    icon = reader.GetValue(idColumnIcon) as byte[];
                    retailer.Icon = icon == null ? new byte[0] : icon;
                    retailer.Url = reader.GetString(idColumnUrl);
                    retailer.HasExitPage = reader.GetBoolean(idColumnHasExitPage);
                    retailers.Add(retailer);
                }
            }
            return (Retailer[])retailers.ToArray(typeof(Retailer));
        }

        public static Retailer[] Enum() {
            PrRetailerEnum prc = new PrRetailerEnum();
            return ReadRetailerInfo(prc);
        }

        public static Retailer Get(int retailerId) {
            PrRetailerGet prc = new PrRetailerGet();
            prc.Id = retailerId;
            Retailer[] retailers = ReadRetailerInfo(prc);
            if (retailers != null && retailers.Length > 0)
                return retailers[0];

            return null;
        }

        public static void Insert(Retailer retailer) {
            PrRetailerIns prc = new PrRetailerIns();
            prc.Name = retailer.Name;
            prc.DisplayName = retailer.DisplayName;
            prc.Icon = retailer.Icon;
            prc.Url = retailer.Url;
            prc.HasExitPage= retailer.HasExitPage;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Retailer does not exist.");
            retailer.Id = prc.Id.Value;
        }

        public static void Update(Retailer retailer) {
            PrRetailerUpd prc = new PrRetailerUpd();
            prc.Id = retailer.Id;
            prc.Name = retailer.Name;
            prc.DisplayName = retailer.DisplayName;
            prc.Icon = retailer.Icon;
            prc.Url = retailer.Url;
            prc.HasExitPage= retailer.HasExitPage;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Retailer does not exist.");
        }

        public static void Delete(Retailer retailer) {
            PrRetailerDel prc = new PrRetailerDel();
            prc.Id = retailer.Id;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Retailer does not exist.");
        }
    }
}