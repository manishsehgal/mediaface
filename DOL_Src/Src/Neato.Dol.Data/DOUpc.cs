using System.Collections;
using System.Data;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data {
    public sealed class DOUpc {
        private DOUpc() {}

        public static Upc[] Enum() {
            PrUpcEnum prc = new PrUpcEnum();

            ArrayList upcList = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int codeColumnIndex = reader.GetOrdinal("Code");
                while (reader.Read()) {
                    Upc upc = new Upc();
                    upc.Id = reader.GetInt32(idColumnIndex);
                    upc.Code = reader.GetString(codeColumnIndex);
                    upcList.Add(upc);
                }
            }
            return (Upc[])upcList.ToArray(typeof(Upc));
        }

        public static Upc Get(string code) {
            PrUpcGet prc = new PrUpcGet();
            prc.Code = code;

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int codeColumnIndex = reader.GetOrdinal("Code");
                if (reader.Read()) {
                    Upc upc = new Upc();
                    upc.Id = reader.GetInt32(idColumnIndex);
                    upc.Code = reader.GetString(codeColumnIndex);
                    return upc;
                }
            }
            return null;
        }

        public static void Insert(Upc upc) {
            PrUpcIns prc = new PrUpcIns();
            prc.Code = upc.Code;
            prc.ExecuteNonQuery();
            upc.Id = prc.Id.Value;
        }

        public static void Update(Upc upc) {
            PrUpcUpd prc = new PrUpcUpd();
            prc.Id = upc.Id;
            prc.Code = upc.Code;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Upc does not exist.");
        }

        public static void Delete(Upc upc) {
            PrUpcDel prc = new PrUpcDel();
            prc.Id = upc.Id;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Upc does not exist.");
        }
    }
}