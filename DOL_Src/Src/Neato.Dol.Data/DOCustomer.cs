using System;
using System.Collections;
using System.Data;

using Neato.Dol.Data.DBEngine;
using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity;

namespace Neato.Dol.Data {
    public class DOCustomer {
        public DOCustomer() {}

        private static Customer[] ReadCustomerInfo(ProcedureWrapper prc) {
            ArrayList list = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int customerIdColumnIndex = reader.GetOrdinal("CustomerId");
                int firstNameColumnIndex = reader.GetOrdinal("FirstName");
                int lastNameColumnIndex = reader.GetOrdinal("LastName");
                int emailColumnIndex = reader.GetOrdinal("Email");
                int passwordColumnIndex = reader.GetOrdinal("Password");
                int emailOptionsColumnIndex = reader.GetOrdinal("EmailOptions");
                int receiveInfoColumnIndex = reader.GetOrdinal("ReceiveInfo");
                int pswuidColumnIndex = reader.GetOrdinal("pswuid");
                int createdColumnIndex = reader.GetOrdinal("Created");
                int retailerColumnIndex = reader.GetOrdinal("RetailerId");
                int activeColumnIndex = reader.GetOrdinal("Active");
                int groupIdColumnIndex = reader.GetOrdinal("GroupId");

                while (reader.Read()) {
                    Customer customer = new Customer();
                    customer.CustomerId = reader.GetInt32(customerIdColumnIndex);
                    customer.FirstName = reader.GetString(firstNameColumnIndex);
                    customer.LastName = reader.GetString(lastNameColumnIndex);
                    customer.Email = reader.GetString(emailColumnIndex);
                    customer.Password = reader.GetString(passwordColumnIndex);
                    Object obj = reader.GetValue(pswuidColumnIndex);
                    if (obj != System.DBNull.Value)
                        customer.PasswordUid = Convert.ToString(obj);
                    customer.ReceiveInfo = reader.GetBoolean(receiveInfoColumnIndex);
                    string emailOptionsLetter = reader.GetString(emailOptionsColumnIndex);
                    switch (emailOptionsLetter) {
                        case "T":
                            customer.EmailOptions = EmailOptions.Text;
                            break;
                        case "H":
                            customer.EmailOptions = EmailOptions.Html;
                            break;
                    }
                    customer.Created = reader.GetDateTime(createdColumnIndex);
                    customer.RetailerId = reader.GetInt32(retailerColumnIndex);
                    customer.Active = reader.GetBoolean(activeColumnIndex);
                    customer.Group = (CustomerGroup)reader.GetInt32(groupIdColumnIndex);
                    list.Add(customer);
                }

                reader.Close();
            }

            return (Customer[])list.ToArray(typeof(Customer));
        }

        public static Customer[] CustomerSearch(string firstName, string lastName, string email, DateTime startDate, DateTime endDate, bool showNotActiveUser) {
            PrCustomerSearch prc = new PrCustomerSearch();
            if (firstName != string.Empty)
                prc.FirstName = firstName;
            if (lastName != string.Empty)
                prc.LastName = lastName;
            if (email != string.Empty)
                prc.Email = email;
            prc.StartDate = startDate; 
            prc.EndDate = endDate;
            prc.ShowNotActive = showNotActiveUser;
            Customer[] list = ReadCustomerInfo(prc);
            return list;
        }

        public static Customer GetCustomerByLogin(string email, bool showNotActiveUser) {
            PrCustomerGetByEmail prc = new PrCustomerGetByEmail();
            prc.Email = email;
            prc.ShowNotActive = showNotActiveUser;

            Customer customer = null;
            Customer[] list = ReadCustomerInfo(prc);
            if (list != null && list.Length > 0)
                customer = list[0];

            if (customer != null) {
                customer.LastEditedPapers =
                    DOLastEditedPaper.EnumerateLastEditedPaperByCustomerList(customer);
            }
            
            return customer;
        }

        public static Customer GetCustomerById(int customerId) {
            PrCustomerGetById prc = new PrCustomerGetById();
            prc.CustomerId = customerId;

            Customer customer = null;
            Customer[] list = ReadCustomerInfo(prc);
            if (list != null && list.Length > 0)
                customer = list[0];

            if (customer != null) {
                customer.LastEditedPapers =
                    DOLastEditedPaper.EnumerateLastEditedPaperByCustomerList(customer);
            }
            
            return customer;
        }

        public static void InsertCustomer(Customer customer, LastEditedPaperData data) {
            PrCustomerIns prc = new PrCustomerIns();
            prc.FirstName = customer.FirstName;
            prc.LastName = customer.LastName;
            prc.Email = customer.Email;
            prc.Password = customer.Password;
            prc.ReceiveInfo = customer.ReceiveInfo;
            prc.Created  = customer.Created;
            prc.RetailerId = customer.RetailerId;
            prc.GroupId = (int)customer.Group;
            switch (customer.EmailOptions) {
                case EmailOptions.Text:
                    prc.EmailOptions = "T";
                    break;
                case EmailOptions.Html:
                    prc.EmailOptions = "H";
                    break;
            }
            prc.Active = customer.Active;

            prc.ExecuteNonQuery();
            customer.CustomerId = prc.CustomerId.Value;

            if (data != null) {
                foreach (LastEditedPaperData.LastEditedPaperRow paper in data.LastEditedPaper) {
                    paper.CustomerId = customer.CustomerId;
                }
                try {
                    DOLastEditedPaper.UpdateLastEditedPaper(data);
                } catch {}
                customer.LastEditedPapers = DOLastEditedPaper.
                    EnumerateLastEditedPaperByCustomerList(customer);
            }
        }

        public static void UpdateCustomer(Customer customer) {
            PrCustomerUpd prc = new PrCustomerUpd();
            prc.CustomerId = customer.CustomerId;
            prc.FirstName = customer.FirstName;
            prc.LastName = customer.LastName;
            prc.Email = customer.Email;
            prc.Password = customer.Password;
            prc.PasswordUid = customer.PasswordUid;
            prc.ReceiveInfo = customer.ReceiveInfo;
            prc.RetailerId = customer.RetailerId;
            prc.Active  = customer.Active;
            prc.GroupId = (int)customer.Group;
            switch (customer.EmailOptions) {
                case EmailOptions.Text:
                    prc.EmailOptions = "T";
                    break;
                case EmailOptions.Html:
                    prc.EmailOptions = "H";
                    break;
            }

            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Customer does not exist.");
        }

        public static void DeleteCustomer(int customerId) {
            PrCustomerDel prc = new PrCustomerDel();
            prc.CustomerId = customerId;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Customer does not exist.");
        }

        public static void ActivateCustomer(string email, CustomerGroup customerGroup, Retailer retailer) {
            PrCustomerActivate prc = new PrCustomerActivate();
            prc.Email = email;
            prc.GroupId = (int)customerGroup;
            if (retailer != null) {
                prc.RetailerId = retailer.Id;
            }
            prc.ExecuteNonQuery();
        }

        public static PdfCalibration GetCalibration(Customer customer) {
            PrCustomerCalibrationGet prc = new PrCustomerCalibrationGet();
            prc.CustomerId = customer.CustomerId;
            return ReadCalibration(prc);
        }

        public static PdfCalibration GetCalibrationDTCD(Customer customer) {
            PrCustomerCalibrationDTCDGet prc = new PrCustomerCalibrationDTCDGet();
            prc.CustomerId = customer.CustomerId;
            return ReadCalibration(prc);
        }

        private static PdfCalibration ReadCalibration(ProcedureWrapper prc) {
            PdfCalibration calibration = new PdfCalibration();

            using(IDataReader reader = prc.ExecuteReader()) {
                int xColumnIndex = reader.GetOrdinal("X");
                int yColumnIndex = reader.GetOrdinal("Y");
                if (reader.Read()) {
                    if (!reader.IsDBNull(xColumnIndex))
                        calibration.X = (float)reader.GetDouble(xColumnIndex);
                    if (!reader.IsDBNull(yColumnIndex))
                        calibration.Y = (float)reader.GetDouble(yColumnIndex);
                }
            }
            return calibration;
        }

        public static void SetCalibration(Customer customer, PdfCalibration calibration) {
            PrCustomerCalibrationSet prc = new PrCustomerCalibrationSet();
            prc.CustomerId = customer.CustomerId;
            prc.X = calibration.X;
            prc.Y = calibration.Y;
            prc.ExecuteNonQuery();
        }

        public static void SetCalibrationDTCD(Customer customer, PdfCalibration calibration) {
            PrCustomerCalibrationDTCDSet prc = new PrCustomerCalibrationDTCDSet();
            prc.CustomerId = customer.CustomerId;
            prc.X = calibration.X;
            prc.Y = calibration.Y;
            prc.ExecuteNonQuery();
        }
    }
}