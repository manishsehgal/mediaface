using System;
using System.Data;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data 
{
    public class DOTrackingPhones 
    {
        public DOTrackingPhones() {}

        public static int Add(Device device, string login, DateTime time) {
            PrTrackingDevicesIns prc = new PrTrackingDevicesIns();
            prc.Device = device.Model;
            prc.DeviceBrand = device.Brand.Name;
            prc.Time = time;
            prc.Login = login;
            prc.ExecuteNonQuery();
            return prc.Id.Value;
        }

        public static DataSet GetListByBrandAndDate(DeviceBrand brand,DateTime startTime,DateTime endTime) {
            PrTrackingDevicesEnumByTimeAndBrand prc = new PrTrackingDevicesEnumByTimeAndBrand();
            prc.DeviceBrand = brand == null ? null : brand.Name;
            prc.TimeEnd = endTime;
            prc.TimeStart = startTime;
            DataSet data = new DataSet();
            data.Tables.Add();
            return prc.LoadDataSet(data);
        }

        public static DataSet GetCarriersList() {
            PrTrackingDevicesEnumBrands prc = new PrTrackingDevicesEnumBrands();
            DataSet data = new DataSet();
            data.Tables.Add();
            return prc.LoadDataSet(data);
        }
    }
}