using System.Collections;
using System.Data;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data {
    public class DOOverviewPage {
        private DOOverviewPage() {}

        public static LocalizedText[] Enum(string culture) {
            PrOverviewPageEnumByCulture prc = new PrOverviewPageEnumByCulture();
            prc.Culture = culture;

            ArrayList list = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int CultureIndex = reader.GetOrdinal("Culture");
                int HtmlTextIndex = reader.GetOrdinal("HtmlText");
                while (reader.Read()) {
                    LocalizedText localizedText = new LocalizedText();
                    localizedText.Culture = reader.GetString(CultureIndex);
                    localizedText.Text = reader.GetString(HtmlTextIndex);
                    list.Add(localizedText);
                }
            }
            return (LocalizedText[]) list.ToArray(typeof (LocalizedText));            
        }

        public static void Insert(string culture, string htmlText) {
            PrOverviewPageIns prc = new PrOverviewPageIns();
            prc.Culture = culture;
            prc.HtmlText = htmlText;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Overview does not exist.");
        }

        public static void Update(string culture, string htmlText) {
            PrOverviewPageUpd prc = new PrOverviewPageUpd();
            prc.Culture = culture;
            prc.HtmlText = htmlText;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Overview does not exist.");
        }

        public static void Delete(string culture) {
            PrOverviewPageDel prc = new PrOverviewPageDel();
            prc.Culture = culture;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Overview does not exist.");
        }
    }
}