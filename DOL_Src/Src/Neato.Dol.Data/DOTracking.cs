using System;
using System.Data;

using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity.Tracking;

namespace Neato.Dol.Data {
    public class DOTracking {
        public DOTracking() {}

        public static int Add(UserAction action, string login, DateTime time) {
            PrTrackingUserActionIns prc = new PrTrackingUserActionIns();
            prc.Login = login;
            prc.Time = time;
            prc.Action = action.ToString();
            prc.ExecuteNonQuery();
            return prc.Id.Value;
        }

        public static DataSet GetList() {
            PrTrackingUserActionEnum prc = new PrTrackingUserActionEnum();
            DataSet data = new DataSet();
            data.Tables.Add();
            return prc.LoadDataSet(data);
        }
    }
}