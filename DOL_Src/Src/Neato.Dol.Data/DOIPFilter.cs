using System;
using System.Collections;
using System.Data;
using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Entity;

namespace Neato.Dol.Data {
    public class DOIPFilter {
        private DOIPFilter() {
        }

        public static IPFilter[] EnumIPFilters() {
            PrIPFilterEnum prc = new PrIPFilterEnum();

            ArrayList list = new ArrayList();
            using(IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int beginField1ColumnIndex = reader.GetOrdinal("BeginField1");
                int beginField2ColumnIndex = reader.GetOrdinal("BeginField2");
                int beginField3ColumnIndex = reader.GetOrdinal("BeginField3");
                int beginField4ColumnIndex = reader.GetOrdinal("BeginField4");
                int endField1ColumnIndex = reader.GetOrdinal("EndField1");
                int endField2ColumnIndex = reader.GetOrdinal("EndField2");
                int endField3ColumnIndex = reader.GetOrdinal("EndField3");
                int endField4ColumnIndex = reader.GetOrdinal("EndField4");
                int descriptionColumnIndex = reader.GetOrdinal("Description");

                while(reader.Read()) {
                    IPAddress beginIPAddress = new IPAddress(
                        reader.GetByte(beginField1ColumnIndex),
                        reader.GetByte(beginField2ColumnIndex),
                        reader.GetByte(beginField3ColumnIndex),
                        reader.GetByte(beginField4ColumnIndex));
                    IPAddress endIPAddress = new IPAddress(
                        reader.GetByte(endField1ColumnIndex),
                        reader.GetByte(endField2ColumnIndex),
                        reader.GetByte(endField3ColumnIndex),
                        reader.GetByte(endField4ColumnIndex));
                    IPFilter ipFilter = new IPFilter(beginIPAddress, endIPAddress, reader.GetString(descriptionColumnIndex));
                    ipFilter.Id = reader.GetInt32(idColumnIndex);
                    list.Add(ipFilter);
                }
            }
            return (IPFilter[])list.ToArray(typeof(IPFilter));
        }
        
        public static void InsertIPFilter(IPFilter ipFilter) {
            PrIPFilterIns prc = new PrIPFilterIns();
            prc.BeginField1 = ipFilter.BeginIPAddress.Field1;
            prc.BeginField2 = ipFilter.BeginIPAddress.Field2;
            prc.BeginField3 = ipFilter.BeginIPAddress.Field3;
            prc.BeginField4 = ipFilter.BeginIPAddress.Field4;
            prc.EndField1 = ipFilter.EndIPAddress.Field1;
            prc.EndField2 = ipFilter.EndIPAddress.Field2;
            prc.EndField3 = ipFilter.EndIPAddress.Field3;
            prc.EndField4 = ipFilter.EndIPAddress.Field4;
            prc.Description = ipFilter.Description;

            prc.ExecuteNonQuery();
            ipFilter.Id = prc.Id.Value;
        }

        public static void UpdateIPFilter(IPFilter ipFilter) {
            PrIPFilterUpd prc = new PrIPFilterUpd();
            prc.BeginField1 = ipFilter.BeginIPAddress.Field1;
            prc.BeginField2 = ipFilter.BeginIPAddress.Field2;
            prc.BeginField3 = ipFilter.BeginIPAddress.Field3;
            prc.BeginField4 = ipFilter.BeginIPAddress.Field4;
            prc.EndField1 = ipFilter.EndIPAddress.Field1;
            prc.EndField2 = ipFilter.EndIPAddress.Field2;
            prc.EndField3 = ipFilter.EndIPAddress.Field3;
            prc.EndField4 = ipFilter.EndIPAddress.Field4;
            prc.Description = ipFilter.Description;
            prc.Id = ipFilter.Id;

            prc.ExecuteNonQuery();
        }

        public static void DelIPFilter(int id) {
            PrIPFilterDel prc = new PrIPFilterDel();
            prc.Id = id;
            prc.ExecuteNonQuery();
        }
    }
}
