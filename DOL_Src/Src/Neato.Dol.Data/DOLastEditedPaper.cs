using System.Collections;

using Neato.Dol.Data.DBEngine;
using Neato.Dol.Data.DbProcedures;
using Neato.Dol.Data.Scheme;
using Neato.Dol.Entity;

namespace Neato.Dol.Data {
    public class DOLastEditedPaper {
        public DOLastEditedPaper() {}

        public static LastEditedPaperData EnumerateLastEditedPaperByCustomer(Customer customer) {
            PrLastEditedPaperEnumByCustomer prc = new PrLastEditedPaperEnumByCustomer();
            prc.CustomerId = customer.CustomerId;

            LastEditedPaperData data = new LastEditedPaperData();
            prc.LoadDataSet(data);

            return data;
        }

        public static PaperBase[] EnumerateLastEditedPaperByCustomerList(Customer customer) {
            ArrayList list = new ArrayList();
            LastEditedPaperData data =
                EnumerateLastEditedPaperByCustomer(customer);

            foreach (LastEditedPaperData.LastEditedPaperRow paperRow in
                data.LastEditedPaper) {
                list.Add(new PaperBase(paperRow.PaperId));
            }

            return (PaperBase[])list.ToArray(typeof(PaperBase));
        }

        
        public static void UpdateLastEditedPaper(LastEditedPaperData data)
        {
            PrLastEditedPaperIns prcIns = new PrLastEditedPaperIns();
            PrLastEditedPaperUpd prcUpd = new PrLastEditedPaperUpd();
            PrLastEditedPaperDel prcDel = new PrLastEditedPaperDel();
            ProcedureWrapper.UpdateDataTable(data.LastEditedPaper, prcIns, prcUpd, prcDel);
        }
    }
}