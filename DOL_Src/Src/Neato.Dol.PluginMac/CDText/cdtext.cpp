#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <paths.h>
#include <sys/param.h>

#include <string>
#include <vector>

#include <IOKit/IOKitLib.h>
#include <IOKit/IOBSD.h>
#include <IOKit/storage/IOCDMedia.h>
#include <IOKit/storage/IOMedia.h>
#include <IOKit/storage/IOCDTypes.h>
#include <IOKit/storage/IOMediaBSDClient.h>
#include <IOKit/storage/IOCDMediaBSDClient.h>
#include </System/Library/Frameworks/DiscRecording.framework/Frameworks/DiscRecordingEngine.framework/Headers/DRCoreCDText.h>

#include <CoreFoundation/CoreFoundation.h>

#include "../Common/xmlhelper.h"
#include "cdtext.h"

#define CD_MIN_TRACK_NO 1
#define CD_MAX_TRACK_NO 99


xmlDoc *  GetDrives(xmlNode * reqElem);
xmlDoc *  GetPlayList(xmlNode * reqElem);
void      getDriveNames(std::vector<std::string> & vNames);
CFDataRef getCdTextData(const char * name);
CFDataRef getTocData(const char * name);
void      getTrackDuration(CFDataRef data, int nTrack, std::string & strDur);


extern "C"
bool mod_init(bool bInit)
{
	return true;
}

extern "C"
const char * mod_getinfo() 
{
	return 
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
		"<ModuleInfo Version=\"1\" Name=\"CDTextModule\" Description=\"CDText Plugin\" Filename=\"modcdtext.dylib\">"
			"<Commands>"
				"<Command Name=\"GET_CD_TEXT_DRIVES\" />"
				"<Command Name=\"GET_CD_TEXT_PLAYLIST\" />"
			"</Commands>"
		"</ModuleInfo>";
}

extern "C"
xmlDoc * mod_invoke(xmlNode * reqElem)
{
	try
	{
		char * cmdName = (char*)xmlGetProp(reqElem, BAD_CAST "Name");
		if (cmdName == NULL) throw 1;
		std::string strCmdName = cmdName;
		xmlFree(cmdName);
		
		if (strcmp(strCmdName.c_str(), "GET_CD_TEXT_DRIVES") == 0)
		{
			return GetDrives(reqElem);
		}
		else if (strcmp(strCmdName.c_str(), "GET_CD_TEXT_PLAYLIST") == 0)
		{
			return GetPlayList(reqElem);
		}
		else
		{
			return NULL;
		}
	}
	catch(...) {return NULL;}
	return NULL;
}

xmlDoc * GetDrives(xmlNode * reqElem)
{
	try
	{
		std::vector<std::string> vNames;
		
		getDriveNames(vNames);
		for (int i=0; i<vNames.size(); i++)
		{
			CFDataRef data = getCdTextData(vNames[i].c_str());
			if (data == NULL)
			{
				vNames.erase(vNames.begin() + i);
				i--;
			}
			else
			{
				CFRelease(data);
			}
		}
		if (vNames.size() == 0) throw 1;
		
		xmlDoc * respDoc = xmlNewDoc(BAD_CAST "1.0");
		if (respDoc == NULL) throw 2;
		xmlNode * respElem = xmlNewNode(NULL, BAD_CAST "Response");
		if (respElem == NULL) throw 3;
		xmlDocSetRootElement(respDoc, respElem);
		xmlSetProp(respElem, BAD_CAST "Error", BAD_CAST "0");
		xmlNode * paramsElem = xmlNewChild(respElem, NULL, BAD_CAST "Params", NULL);
		if (paramsElem == NULL) throw 4;
		xmlNode * drivesElem = xmlNewChild(paramsElem, NULL, BAD_CAST "Drives", NULL);
		if (drivesElem == NULL) throw 5;
		for (int i=0; i<vNames.size(); i++)
		{
			xmlNode * driveElem = xmlNewChild(drivesElem, NULL, BAD_CAST "Drive", NULL);
			if (drivesElem == NULL) continue;
			xmlSetProp(driveElem, BAD_CAST "Name", BAD_CAST vNames[i].c_str());
		}
		char buf[128];
		sprintf(buf, "%d", vNames.size());
		xmlSetProp(drivesElem, BAD_CAST "Count", BAD_CAST buf);
		
		return respDoc;
	}
	catch (int nErr)
	{
		std::string strErrDesc = "Can't get CD-TEXT drives.";
		if (nErr == 1) strErrDesc = "No CD drives with CD-TEXT support found.";
		std::string strResp = "<Response Error=\"1\" ErrorDesc=\"";
		strResp += strErrDesc;
		strResp +="\"></Response>";
		xmlDoc * respDoc = xmlReadMemory(strResp.c_str(), strResp.size(), NULL, NULL, 0);
		return respDoc;
	}
	
	return NULL;
}

xmlDoc * GetPlayList(xmlNode * reqElem)
{
	try
	{
		xmlNode * paramsElem = CXmlHelper::GetChildElement(reqElem, "Params");
		if (paramsElem == NULL) throw 1;
		xmlNode * driveElem = CXmlHelper::GetChildElement(paramsElem, "Drive");
		if (driveElem == NULL) throw 2;
		char * driveName = (char*)xmlGetProp(driveElem, BAD_CAST "Name");
		if (driveName == NULL) throw 3;
		
		CFDataRef data = getCdTextData(driveName);
		if (data == NULL) throw 4;
		CFDataRef tocData = getTocData(driveName);
		if (tocData == NULL) throw 5;
		
		//--------------------------------------

		xmlDoc * respDoc = xmlNewDoc(BAD_CAST "1.0");
		if (respDoc == NULL) throw 6;
		xmlNode * respElem = xmlNewNode(NULL, BAD_CAST "Response");
		if (respElem == NULL) throw 7;
		xmlDocSetRootElement(respDoc, respElem);
		xmlSetProp(respElem, BAD_CAST "Error", BAD_CAST "0");
		xmlNode * paramsRElem = xmlNewChild(respElem, NULL, BAD_CAST "Params", NULL);
		if (paramsRElem == NULL) throw 8;

		CFArrayRef blocks = DRCDTextBlockCreateArrayFromPackList(data);
		if (blocks == NULL) throw 9;
		DRCDTextBlockRef block = (DRCDTextBlockRef)CFArrayGetValueAtIndex(blocks, 0);
		if (block == NULL) throw 10;
		CFArrayRef tracks = DRCDTextBlockGetTrackDictionaries(block);
		if (tracks == NULL) throw 11;
		if (CFArrayGetCount(tracks) == 0) throw 12;
						 
		xmlNode * plElem = xmlNewChild(paramsRElem, NULL, BAD_CAST "PlayList", NULL);
		if (plElem == NULL) throw 13;
		
		if (CFArrayGetCount(tracks) > 1)
		{
			CFDictionaryRef dict = (CFDictionaryRef)CFArrayGetValueAtIndex(tracks, 0);
			
			char buf[128];
			sprintf(buf, "%d", CFArrayGetCount(tracks)-1);
			xmlSetProp(plElem, BAD_CAST "TrackCount", BAD_CAST buf);
			
			CFStringRef strArtist = (CFStringRef)CFDictionaryGetValue(dict, kDRCDTextPerformerKey);
			CFStringGetCString(strArtist, buf, 128, kCFStringEncodingASCII);
			xmlSetProp(plElem, BAD_CAST "Artist", BAD_CAST buf);
			
			CFStringRef strAlbum = (CFStringRef)CFDictionaryGetValue(dict, kDRCDTextTitleKey);
			CFStringGetCString(strAlbum, buf, 128, kCFStringEncodingASCII);
			xmlSetProp(plElem, BAD_CAST "Album", BAD_CAST buf);
			
			xmlSetProp(plElem, BAD_CAST "SrcType", BAD_CAST "CDText");
			xmlSetProp(plElem, BAD_CAST "SrcName", BAD_CAST driveName);
		}
		
		xmlNode * tracksElem = xmlNewChild(plElem, NULL, BAD_CAST "Tracks", NULL);
		if (tracksElem == NULL) throw 14;
		
		for (int i=1; i<CFArrayGetCount(tracks)-1; i++)
		{
			xmlNode * trackElem = xmlNewChild(tracksElem, NULL, BAD_CAST "Track", NULL);
			if (trackElem == NULL) continue;
			
			CFDictionaryRef dict = (CFDictionaryRef)CFArrayGetValueAtIndex(tracks, i);
			if (dict == NULL) continue;
			
			char buf[128];
			sprintf(buf, "%d", i);
			xmlSetProp(trackElem, BAD_CAST "Id", BAD_CAST buf);
			
			CFStringRef strArtist = (CFStringRef)CFDictionaryGetValue(dict, kDRCDTextPerformerKey);
			CFStringGetCString(strArtist, buf, 128, kCFStringEncodingASCII);
			xmlSetProp(trackElem, BAD_CAST "Artist", BAD_CAST buf);
			
			CFStringRef strTitle = (CFStringRef)CFDictionaryGetValue(dict, kDRCDTextTitleKey);
			CFStringGetCString(strTitle, buf, 128, kCFStringEncodingASCII);
			xmlSetProp(trackElem, BAD_CAST "Title", BAD_CAST buf);
			
			std::string strDur = "00:00";
			getTrackDuration(tocData, i, strDur);
			xmlSetProp(trackElem, BAD_CAST "Duration", BAD_CAST strDur.c_str());
		}
		
		CFRelease(blocks);
		CFRelease(tocData);
		CFRelease(data);
		xmlFree(driveName);
		
		return respDoc;
	}
	catch (int nErr)
	{
		std::string strErrDesc = "Can't read CD-TEXT.";
		std::string strResp = "<Response Error=\"1\" ErrorDesc=\"";
		strResp += strErrDesc;
		strResp +="\"></Response>";
		xmlDoc * respDoc = xmlReadMemory(strResp.c_str(), strResp.size(), NULL, NULL, 0);
		return respDoc;
	}
	
	return NULL;
}

void getDriveNames(std::vector<std::string> & vNames)
{
	mach_port_t masterPort;
	kern_return_t kernResult = IOMasterPort(MACH_PORT_NULL, &masterPort);
	if (kernResult != KERN_SUCCESS ) return;
	
	CFMutableDictionaryRef classesToMatch = IOServiceMatching(kIOCDMediaClass);
	if (classesToMatch != NULL)
	{
		CFDictionarySetValue(classesToMatch, CFSTR(kIOMediaEjectableKey), kCFBooleanTrue);
	}
	
	io_iterator_t mediaIterator;
	kernResult = IOServiceGetMatchingServices(masterPort, classesToMatch, &mediaIterator);
	if (kernResult != KERN_SUCCESS || mediaIterator == 0) return;
	
	char deviceFilePath[ MAXPATHLEN ];
	
	io_object_t nextMedia = 0;
	while (nextMedia = IOIteratorNext(mediaIterator))
	{
		CFStringRef deviceFilePathAsCFString = (CFStringRef)IORegistryEntryCreateCFProperty(nextMedia, CFSTR(kIOBSDNameKey), kCFAllocatorDefault, 0);
		deviceFilePath[0] = '\0';
		if (deviceFilePathAsCFString != NULL)
		{
			strcpy(deviceFilePath, _PATH_DEV);
			strcat(deviceFilePath, "r");
			size_t devPathLength = strlen(deviceFilePath);
			if (CFStringGetCString(deviceFilePathAsCFString, deviceFilePath + devPathLength, sizeof(deviceFilePath) - devPathLength, kCFStringEncodingASCII ) )
			{
				vNames.push_back(std::string(deviceFilePath));
			}
			CFRelease( deviceFilePathAsCFString );
		}
		IOObjectRelease(nextMedia);
	}
}

CFDataRef getCdTextData(const char * name)
{
	int fd = open(name, O_RDONLY);
	if (fd == -1) return NULL;

	u_int8_t * toc = new u_int8_t[sizeof(CDTEXT)];
	memset(toc, 0, sizeof(CDTEXT));
					
	dk_cd_read_toc_t tocRequest = {0};
	tocRequest.buffer = toc;
	tocRequest.bufferLength = sizeof(CDTEXT);
	tocRequest.format = kCDTOCFormatTEXT;

	int res = ioctl(fd, DKIOCCDREADTOC, &tocRequest);
	if (res	< 0 ) {delete[] toc; close(fd); return NULL;}
	
	CDTEXT * pCdText = (CDTEXT*)toc;
	UInt16 len = pCdText->dataLength + sizeof(CDTEXT);
	delete[] toc;
	toc = new u_int8_t[len];
	memset(toc, 0, len);
	tocRequest.buffer = toc;
	tocRequest.bufferLength = len;
	
	res = ioctl(fd, DKIOCCDREADTOC, &tocRequest);
	if (res	< 0 ) {delete[] toc; close(fd); return NULL;}
	if (tocRequest.bufferLength < sizeof(CDTEXT)) {delete[] toc; return NULL;}
	
	pCdText = (CDTEXT*)toc;
	CFDataRef data = CFDataCreate(NULL, (UInt8*)pCdText->descriptors, pCdText->dataLength);
	delete[] toc;
	
	close(fd);
	return data;
}

CFDataRef getTocData(const char * name)
{
	int fd = open(name, O_RDONLY);
	if (fd == -1) return NULL;

	u_int8_t * toc = new u_int8_t[sizeof(CDTOC)];
	memset(toc, 0, sizeof(CDTOC));

	dk_cd_read_toc_t tocRequest = {0};
	tocRequest.buffer = toc;
	tocRequest.bufferLength = sizeof(CDTOC);
	tocRequest.format = kCDTOCFormatTOC;

	int res = ioctl(fd, DKIOCCDREADTOC, &tocRequest);
	if (res	< 0 ) {delete[] toc; close(fd); return NULL;}
	
	CDTOC * pToc = (CDTOC*)toc;
	UInt16 len = pToc->length + sizeof(CDTOC);
	delete[] toc;
	toc = new u_int8_t[len];
	memset(toc, 0, len);
	tocRequest.buffer = toc;
	tocRequest.bufferLength = len;
	
	res = ioctl(fd, DKIOCCDREADTOC, &tocRequest);
	if (res	< 0 ) {delete[] toc; close(fd); return NULL;}
	if (tocRequest.bufferLength < sizeof(CDTOC)) {delete[] toc; return NULL;}
	
	pToc = (CDTOC*)toc;
	CFDataRef data = CFDataCreate(NULL, (UInt8*)pToc, len);
	delete[] toc;
	
	close(fd);
	return data;
}

void getTrackDuration(CFDataRef data, int nTrack, std::string & strDur)
{
	CDTOC * pToc = (CDTOC*)CFDataGetBytePtr(data);
	int nDescriptors = pToc->length;
	nDescriptors -= ( sizeof(pToc->sessionFirst) + sizeof(pToc->sessionLast) );
	nDescriptors /= sizeof(CDTOCDescriptor);
	
	CDTOCDescriptor * pTrackDescriptors = pToc->descriptors;
	for(int i = nDescriptors; i >= 0; i-- )
	{
		u_char track = pTrackDescriptors[i].point;
		if (track > CD_MAX_TRACK_NO || track < CD_MIN_TRACK_NO) continue;
		if (track == nTrack)
		{
			CDMSF & msf0 = pTrackDescriptors[i].p;
			CDMSF & msf1 = pTrackDescriptors[i+1].p;
			int iSeconds = (msf1.minute - msf0.minute) * 60 + (msf1.second - msf0.second) - ((msf1.frame - msf0.frame) < 0 ? 1 : 0);
			char buf[128];
			sprintf(buf, "%02d:%02d", iSeconds/60, iSeconds%60);
			strDur = std::string(buf);
		}
	}
}





