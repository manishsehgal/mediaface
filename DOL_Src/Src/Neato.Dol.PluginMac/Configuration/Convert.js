var fso     = new ActiveXObject("Scripting.FileSystemObject");
var b2t     = new ActiveXObject("DataConversion.Bin2Txt");
var xml     = new ActiveXObject("Msxml2.DOMDocument");
var dirSrc  = "..\\bin\\";
var dirDest1 = "..\\..\\Neato.Dol.WebDesigner\\Plugins\\PowerMac\\";
var dirDest2 = "..\\..\\Neato.Dol.WebDesigner\\Plugins\\IntelMac\\";
var xmlFile = "desc.xml";

//WScript.echo(txt7);

xml.async = false;
xml.load(xmlFile);

var ModulesEl = xml.documentElement;
for (ModuleInfoEl=ModulesEl.firstChild; ModuleInfoEl!=null; ModuleInfoEl=ModuleInfoEl.nextSibling)
{
	ProcessNode(ModuleInfoEl);
	var DependsEl = ModuleInfoEl.getElementsByTagName("Depends").item(0);
	if (DependsEl != null)
	{
		for (FileEl=DependsEl.firstChild; FileEl!=null; FileEl=FileEl.nextSibling)
		{
			ProcessNode(FileEl);
		}
	}
}
xml.save(dirDest1 + xmlFile);
xml.save(dirDest2 + xmlFile);

function ProcessNode(node)
{
	var fileName = node.getAttribute("Filename");
	var file = fso.GetFile(dirSrc + fileName);
	
	b2t.File2ZipAndBase64F(file.Path, dirDest1 + fileName + ".txt", false);
	b2t.File2ZipAndBase64F(file.Path, dirDest2 + fileName + ".txt", false);
	
	var sum = b2t.GetFileSum(file.Path);
	node.setAttribute("Sum", sum);
}





//	var name = dir + fileList[i];
//	var txt = b2t.File2ZipAndBase64F(name, name+".txt", true);



