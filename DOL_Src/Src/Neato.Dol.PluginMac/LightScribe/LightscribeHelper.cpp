#include <string> 
#include <math.h>
#include <string.h>
#include <dlfcn.h>
#include "LightscribeHelper.h"
#include </usr/include/lightscribe_cxx.h>
#include <Carbon/Carbon.h>
using namespace std;
using namespace LightScribe;

typedef unsigned char	BYTE;
typedef unsigned short  WORD;
typedef unsigned long   DWORD;
typedef long            LONG;

const WORD BITMAP_HEADER = 0x4d42;

/* Minimum and maximum macros */
#define __max(a,b)  (((a) > (b)) ? (a) : (b))
#define __min(a,b)  (((a) < (b)) ? (a) : (b))

void StringToByteArrayR2C2PR_256Colors(int width, int height, double largeRadius, double smallRadius, 
										  char* szBmpData, BYTE** bmpData, int* pStrLen);
										  
void SaveBitmap256Colors(int width, int height, int stride, BYTE* bmpData, const char* filename);
void GetBurnerPath(string &path);
int RunBurner(const char* tempFile, const char* lsMode, const char* lsQuality, const char* lsSilent);
										  
CLightscribeHelper::CLightscribeHelper(void)
{
}

CLightscribeHelper::~CLightscribeHelper(void)
{
}

xmlDoc* CLightscribeHelper::Print(xmlNode* reqElem)
{
	xmlNode * paramsElem = CXmlHelper::GetChildElement(reqElem, "Params");
	if (paramsElem == NULL) throw 1;
	
	char *tmp = NULL;
	string LSquality="1";
	string LSsilentMode="0";
	xmlNode * guiDataElem = CXmlHelper::GetChildElement(paramsElem, "GuiData");
	if (guiDataElem != NULL) {		
		tmp = (char*)xmlGetProp(guiDataElem, BAD_CAST "LSquality");
		if (tmp != NULL)
			LSquality = tmp;
		xmlFree(tmp);
		tmp = (char*)xmlGetProp(guiDataElem, BAD_CAST "silent");
		if (tmp != NULL)
			LSsilentMode = tmp;
		xmlFree(tmp);
	}
	
	xmlNode * bmpDataElem = CXmlHelper::GetChildElement(paramsElem, "BmpData");
	if (bmpDataElem == NULL) throw 2;
	
	tmp = (char*)xmlGetProp(bmpDataElem, BAD_CAST "Encoding");
	if (tmp == NULL) throw 3;
	string encoding = tmp;
	xmlFree(tmp); 
	if (encoding != "SQRT_R2C2_PROGR")
		throw 4;

	tmp = (char*)xmlGetProp(bmpDataElem, BAD_CAST "LabelMode");
	if (tmp == NULL) throw 5;
	string LSMode = tmp;
	xmlFree(tmp);
	
	tmp = (char*)xmlGetProp(bmpDataElem, BAD_CAST "Width");
	if (tmp == NULL) throw 6;
	int width = atoi(tmp);
	xmlFree(tmp);
	
	tmp = (char*)xmlGetProp(bmpDataElem, BAD_CAST "Height");
	if (tmp == NULL) throw 6;
	int height = atoi(tmp);
	xmlFree(tmp);
	
	tmp = (char*)xmlGetProp(bmpDataElem, BAD_CAST "LargeRadius");
	if (tmp == NULL) throw 6;
	double largeRadius = atof(tmp);
	xmlFree(tmp);
	
	tmp = (char*)xmlGetProp(bmpDataElem, BAD_CAST "SmallRadius");
	if (tmp == NULL) throw 6;
	double smallRadius = atof(tmp);
	xmlFree(tmp);
	
	tmp = (char*)xmlNodeGetContent(bmpDataElem);
	if ((tmp == NULL) || (*tmp == 0)) {
		xmlFree(tmp);
		xmlNode * dataElem = CXmlHelper::GetChildElement(reqElem, "Data");
		if (dataElem == NULL) 
			throw 7;
		tmp = (char*)xmlNodeGetContent(dataElem);
		if ((tmp == NULL) || (*tmp == 0))
			throw 7;
	}
	
	BYTE* bmpData;
	int strLen;

	StringToByteArrayR2C2PR_256Colors(width, height, largeRadius, smallRadius, 
										tmp, &bmpData, &strLen);
	if(bmpData==NULL) throw 8;
	
	char tmpFile[256] = "/tmp/ls.bmp";
	SaveBitmap256Colors(width, height, strLen, bmpData, tmpFile);

	free(bmpData);
	xmlFree(tmp);
	
	RunBurner(tmpFile, LSMode.c_str(), LSquality.c_str(), LSsilentMode.c_str());
		
	// Prepare response xml
	xmlDoc * respDoc = xmlNewDoc(BAD_CAST "1.0");
	if (respDoc == NULL) throw 7;
	xmlNode * respElem = xmlNewNode(NULL, BAD_CAST "Response");
	if (respElem == NULL) throw 8;
	xmlDocSetRootElement(respDoc, respElem);
	xmlSetProp(respElem, BAD_CAST "Error", BAD_CAST "0");
	xmlNode * paramsRElem = xmlNewChild(respElem, NULL, BAD_CAST "Params", NULL);
	if (paramsRElem == NULL) throw 9;
	xmlNode * statusElem = xmlNewChild(paramsRElem, NULL, BAD_CAST "Status", NULL);
	if (statusElem == NULL) throw 10;
		
	return respDoc;
}

xmlDoc* CLightscribeHelper::DetectDevice(xmlNode* reqElem)
{
	try {
		xmlDoc * respDoc = xmlNewDoc(BAD_CAST "1.0");
        if (respDoc == NULL) throw 1;
        xmlNode * respElem = xmlNewNode(NULL, BAD_CAST "Response");
        if (respElem == NULL) throw 2;
        xmlDocSetRootElement(respDoc, respElem);
        xmlSetProp(respElem, BAD_CAST "Error", BAD_CAST "0");
        xmlNode * paramsElem = xmlNewChild(respElem, NULL, BAD_CAST "Params", NULL);
        if (paramsElem == NULL) throw 3;
		
		void* handle = dlopen("liblightscribe.dylib", RTLD_GLOBAL);
		int err = 0;
		if (handle == NULL)
			err = 1;
		else {
			if (err == 0) {
				try {
					typedef LSError (*LP_LS_DiscPrintMgr_Create)(LS_DiscPrintMgrHandle*);
					typedef LSError (*LP_LS_DiscPrintMgr_EnumDiscPrinters)(LS_DiscPrintMgrHandle, LS_EnumDiscPrintersHandle*);
					typedef LSError (*LP_LS_EnumDiscPrinters_Count)(LS_EnumDiscPrintersHandle, unsigned long*);
					typedef LSError (*LP_LS_DiscPrintMgr_Destroy)(LS_DiscPrintMgrHandle*);
					typedef LSError (*LP_LS_EnumDiscPrinters_Destroy)(LS_EnumDiscPrintersHandle*);
					
					LP_LS_DiscPrintMgr_Create			LS_DiscPrintMgr_Create				= (LP_LS_DiscPrintMgr_Create)dlsym(handle, "LS_DiscPrintMgr_Create");
					LP_LS_DiscPrintMgr_EnumDiscPrinters	LS_DiscPrintMgr_EnumDiscPrinters	= (LP_LS_DiscPrintMgr_EnumDiscPrinters)dlsym(handle, "LS_DiscPrintMgr_EnumDiscPrinters");
					LP_LS_EnumDiscPrinters_Count		LS_EnumDiscPrinters_Count			= (LP_LS_EnumDiscPrinters_Count)dlsym(handle, "LS_EnumDiscPrinters_Count");
					LP_LS_DiscPrintMgr_Destroy			LS_DiscPrintMgr_Destroy				= (LP_LS_DiscPrintMgr_Destroy)dlsym(handle, "LS_DiscPrintMgr_Destroy");
					LP_LS_EnumDiscPrinters_Destroy		LS_EnumDiscPrinters_Destroy			= (LP_LS_EnumDiscPrinters_Destroy)dlsym(handle, "LS_EnumDiscPrinters_Destroy");

					LS_DiscPrintMgrHandle manager;
					LS_EnumDiscPrintersHandle enumPrinters;
					unsigned long count = 0;
					LSError lsError;
					
					lsError = LS_DiscPrintMgr_Create(&manager);
					lsError = LS_DiscPrintMgr_EnumDiscPrinters(manager, &enumPrinters);
					lsError = LS_EnumDiscPrinters_Count(enumPrinters, &count);

					lsError = LS_EnumDiscPrinters_Destroy(&enumPrinters);
					lsError = LS_DiscPrintMgr_Destroy(&manager);
					if (count == 0) {
						err = 2;
					}
				}
				catch (...) {
					err = 1;
				}
			}
			if (err == 0) {
				string burnerPath;
				GetBurnerPath(burnerPath);
				if (access(burnerPath.c_str(), 0) == -1)
					err = 3;
			}
			dlclose(handle);
		}

		switch(err)
		{
			case 1:
				xmlSetProp(respElem, BAD_CAST "Error", BAD_CAST "NoDll");
				break;
			case 2:
				xmlSetProp(respElem, BAD_CAST "Error", BAD_CAST "NoDrive");
				break;
			case 3:
				xmlSetProp(respElem, BAD_CAST "Error", BAD_CAST "NoLSBurner");
				break;
		}
		
		return respDoc;
	}
	catch(int nError)
	{
		return NULL;
	}

	return NULL;
}

void SetColor(char * &pCurrChar, char * pDataEnd, 
							BYTE* lineStart, int strLen, 
							int i, int j, int width, int height, int passN) {

	int color;
	BYTE* pByte = lineStart+j;

	if(pCurrChar < pDataEnd) {
		char C1 = *pCurrChar++;
		char C2 = *pCurrChar++;
		color = C1*0x10 + C2 - 65*17;	// = ( C1-65 )*0x10 + C2 - 65;
	} else {
		if((i<=0) || (j<=0) || (i>=height) || (j>=width))
			return;
		if(passN == 1) 
			color =(*(pByte-strLen-1)+
				    *(pByte-strLen+1)+
					*(pByte+strLen-1)+
					*(pByte+strLen+1)+2)/4;
		else
			color =(*(pByte-strLen)+
					*(pByte-1)+
					*(pByte+1)+
					*(pByte+strLen)+2)/4;
	}
	
	*pByte=(BYTE)color;
}

void StringToByteArrayR2C2PR_256Colors(int width, int height, double largeRadius, double smallRadius, 
										  char* szBmpData, BYTE** bmpData, int* pStrLen)
{
	int strLen = width;
	if(strLen % 4 != 0)
		strLen += (4 - strLen%4); // length of each scan line in the buffer; must be a multiple of 4
	int buffLen = strLen*height;

	BYTE* data = (BYTE*)malloc(buffLen);
	if(data == NULL)
		return;

	*bmpData = data;
	*pStrLen = strLen;

	memset(data,0xFF,buffLen);	// fill buffer by White color

	double centerX   = width / 2.0f;
	double centerY   = height / 2.0f;

	double largeR2 = largeRadius*largeRadius;
	double smallR2 = smallRadius*smallRadius;

	int delta[4][2] =  {{0,0},{1,1},{1,0},{0,1}};

	int	startI = (int)floor(centerY - largeRadius);
	int	endY = __min((int)ceil(centerY + largeRadius),height);

	char* pCurrChar = szBmpData;
	char* pDataEnd = pCurrChar + strlen(szBmpData);

	for(int passN=0;passN<4;passN++) {
		int deltaX = delta[passN][0];
		for (int i = startI + delta[passN][1]; i < endY; i+=2 ) {

			BYTE* lineStart = data + strLen*i;

			double dy = centerY - i;
			double dy2 = dy*dy;
			
			if (dy2 > largeR2) 
				continue;

			double dx = sqrt(largeR2 - dy2);
			int largeB = (int)floor((centerX - dx - deltaX)/2)*2 + deltaX;
			int largeE = (int)floor(centerX + dx);

			if (dy2 > smallR2) {
				for (int j=largeB; j<=largeE; j+=2)
					SetColor(pCurrChar, pDataEnd, lineStart, strLen, i, j, width, height, passN);
			} else {
				dx = sqrt(smallR2 - dy2);
				int smallB = (int)floor(centerX - dx);
				int smallE = (int)floor((centerX + dx - deltaX)/2)*2 + deltaX;

				for (int j=largeB; j<=smallB; j+=2)
					SetColor(pCurrChar, pDataEnd, lineStart, strLen, i, j, width, height, passN);
				for (int j=smallE; j<=largeE; j+=2)
					SetColor(pCurrChar, pDataEnd, lineStart, strLen, i, j, width, height, passN);
			}
		}
	}
}

#pragma pack(2)
/**
 * Win32 BITMAPFILEHEADER strucutre. 
 */
struct BITMAPFILEHEADER
{
  WORD    bfType;
  DWORD   bfSize;
  WORD    bfReserved1;
  WORD    bfReserved2;
  DWORD   bfOffBits;
};

/**
 * Win32 BITMAPINFOHEADER strucutre. 
 */
struct BITMAPINFOHEADER
{
  DWORD      biSize;
  LONG       biWidth;
  LONG       biHeight;
  WORD       biPlanes;
  WORD       biBitCount;
  DWORD      biCompression;
  DWORD      biSizeImage;
  LONG       biXPelsPerMeter;
  LONG       biYPelsPerMeter;
  DWORD      biClrUsed;
  DWORD      biClrImportant;
};

/**
 * Win32 RGBQUAD strucutre. 
 */
struct RGBQUAD
{
  BYTE    rgbBlue;
  BYTE    rgbGreen;
  BYTE    rgbRed;
  BYTE    rgbReserved;
};

/**
 * Win32 BITMAPINFO strucutre. 
 */

struct BITMAPINFO
{
  BITMAPINFOHEADER bmiHeader;
  RGBQUAD bmiColors[1];
};
#pragma pop

void SaveBitmap256Colors(int width, int height, int stride, BYTE* bmpData, const char* filename)
{
	BITMAPFILEHEADER bmfh;
	BITMAPINFOHEADER bmih;

	const int N_COLORS = 256;
	RGBQUAD bmPalette[N_COLORS];

	bmih.biSize = sizeof(bmih); // = 40;
	bmih.biWidth = width;
	bmih.biHeight = -height;	// scan from top to bottom
	bmih.biPlanes = 1;
	bmih.biBitCount = 8;
	bmih.biCompression = 0;
	bmih.biSizeImage = stride*height;
	const double InchesPerMeter = 1000/25.4;
	bmih.biXPelsPerMeter = (LONG)(600*InchesPerMeter); // = 23622; resolution=600 bpi
	bmih.biYPelsPerMeter = (LONG)(600*InchesPerMeter); // = 23622;
	bmih.biClrUsed = N_COLORS;
	bmih.biClrImportant = N_COLORS;

	memset(&bmfh, 0, sizeof(bmfh));
	bmfh.bfType = BITMAP_HEADER;
	bmfh.bfOffBits = sizeof(bmfh) +sizeof(bmih)+sizeof(bmPalette); // = 1078;
	bmfh.bfSize = bmfh.bfOffBits+bmih.biSizeImage; // = 8295478;
	
	bmih.biSize = EndianU32_NtoL(bmih.biSize);
	bmih.biWidth = EndianS32_NtoL(bmih.biWidth);
	bmih.biHeight = EndianS32_NtoL(bmih.biHeight);
	bmih.biPlanes = EndianU16_NtoL(bmih.biPlanes);
	bmih.biBitCount = EndianU16_NtoL(bmih.biBitCount);
	bmih.biCompression = EndianU32_NtoL(bmih.biCompression);
	bmih.biSizeImage = EndianU32_NtoL(bmih.biSizeImage);
	bmih.biXPelsPerMeter = EndianS32_NtoL(bmih.biXPelsPerMeter);
	bmih.biYPelsPerMeter = EndianS32_NtoL(bmih.biYPelsPerMeter);
	bmih.biClrUsed = EndianU32_NtoL(bmih.biClrUsed);
	bmih.biClrImportant = EndianU32_NtoL(bmih.biClrImportant);

	bmfh.bfType = EndianU16_NtoL(bmfh.bfType);
	bmfh.bfOffBits = EndianU32_NtoL(bmfh.bfOffBits);
	bmfh.bfSize = EndianU32_NtoL(bmfh.bfSize);

	// bmPalette.bmih = bmih;
	for(int i=0;i<N_COLORS;i++) {
		RGBQUAD* p = bmPalette+i;
		p->rgbBlue = (BYTE)(i);
		p->rgbRed = p->rgbGreen = p->rgbBlue;
		p->rgbReserved = 0;
	}

	FILE * hFile = fopen(filename, "w+");
	if (hFile == NULL) throw 1;

	DWORD bytesWritten;
	DWORD totalWritten = 0;

	bytesWritten = fwrite(&bmfh, 1, sizeof(bmfh), hFile);
	totalWritten += bytesWritten;
	bytesWritten = fwrite(&bmih, 1, sizeof(bmih), hFile);
	totalWritten += bytesWritten;
	bytesWritten = fwrite(&bmPalette, 1, sizeof(bmPalette), hFile);
	totalWritten += bytesWritten;
	bytesWritten = fwrite(bmpData, 1, bmih.biSizeImage, hFile);
	totalWritten += bytesWritten;

	fclose(hFile);
}

void GetBurnerPath(string &path)
{
	CFURLRef fURL =  CFBundleCopyBundleURL(CFBundleGetMainBundle());
	path = CFStringGetCStringPtr(CFURLCopyPath(fURL), kCFStringEncodingMacRoman);
	path += "../LsBurner.app/Contents/MacOS/LsBurner";
}

int RunBurner(const char* tempFile, const char* lsMode, const char* lsQuality, const char* lsSilent)
{
	string burnerPath;
	GetBurnerPath(burnerPath);
		
	int res = fork(); 
	if (res < 0) return 0;//error
	if (res == 0)//child
	{
		execlp(burnerPath.c_str(), burnerPath.c_str(), tempFile, lsMode, lsQuality, lsSilent, PLUGIN_VERSION, NULL);
	}
	return 0;
}