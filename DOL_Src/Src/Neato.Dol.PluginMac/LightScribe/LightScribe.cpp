#include <string.h>

#include <string>

#include "LightScribe.h"
#include "LightscribeHelper.h"

CLightscribeHelper * g_pHelper = 0;

extern "C"
bool mod_init(bool bInit)
{
	if(bInit)
	{
		g_pHelper = new CLightscribeHelper();
	}
	else
	{
		if (g_pHelper) delete g_pHelper;
	}
	return true;
}

extern "C"
const char * mod_getinfo() 
{
	return 
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
		"<ModuleInfo Version=\""PLUGIN_VERSION"\" Name=\"LightscribeModule\" Description=\"Lightscribe printing Module\">"
			"<Commands>"
				"<Command Name=\"LS_PRINT\" CopyRequest=\"0\" />"
				"<Command Name=\"LS_DETECT_DEVICE\" CopyRequest=\"0\" />"
			"</Commands>"
		"</ModuleInfo>";
}

extern "C"
xmlDoc * mod_invoke(xmlNode * reqElem)
{
	try
	{
		char * cmdName = (char*)xmlGetProp(reqElem, BAD_CAST "Name");
		if (cmdName == NULL) throw 1;
		std::string strCmdName = cmdName;
		xmlFree(cmdName);
		
		if (strcmp(strCmdName.c_str(), "LS_PRINT") == 0)
		{
			return g_pHelper->Print(reqElem);
		}
		else if (strcmp(strCmdName.c_str(), "LS_DETECT_DEVICE") == 0)
		{
			return g_pHelper->DetectDevice(reqElem);
		}
		else
		{
			return NULL;
		}
	}
	catch(...) {return NULL;}
	return NULL;
}