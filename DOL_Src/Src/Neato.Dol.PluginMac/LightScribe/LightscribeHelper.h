#include "../Common/xmlhelper.h"

#define PLUGIN_VERSION   "3"

class CLightscribeHelper
{
public:
	CLightscribeHelper();
	~CLightscribeHelper();

	xmlDoc* Print(xmlNode* reqElem);
	xmlDoc* DetectDevice(xmlNode* reqElem);
};