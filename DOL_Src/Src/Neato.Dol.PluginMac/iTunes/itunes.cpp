#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <paths.h>
#include <sys/param.h>

#include <string>
#include <vector>


#include <CoreFoundation/CoreFoundation.h>

#include "../Common/xmlhelper.h"
#include "itunes.h"

// TestConsole.cpp : Defines the entry point for the console application.
//


using namespace std;
typedef struct _tagTRACK{
    std::string Name;
    std::string Artist;
    std::string Album;
    std::string Duration;
}TRACK, *LPTRACK;


void parsePlaylists(char *str,     std::vector <std::string> &m_vString);
void parsePlaylistData(char *str,  std::vector<TRACK>  & vTracks) ;
xmlDoc * GetPlayList(xmlNode * reqElem);
xmlDoc * GetCategories(xmlNode * reqElem);
void getCategories(std::vector<std::string> &vString);
void getPlaylist(char * playlistName, std::vector<TRACK> &vTrack);
bool isiTunesRunning();
char GET_PLAYLISTS_REQUEST[] = "osascript -e\
                               'tell application \"iTunes\"' -e\
                               'set res to {}' -e\
                               'repeat with i from 1 to (the count of the playlists)' -e\
                               'set currentPlaylist to playlist i' -e\
                               'set the playlistName to the name of currentPlaylist' -e\
                               'copy the res & i & playlistName to res' -e\
                               'end repeat' -e\
                               'get res' -e\
                               'end tell'";

char * GET_PLAYLIST_REQUEST_TEMPLATE = "osascript -e\
'tell application \"iTunes\"' -e\
'set res to {} ' -e\
'set currentPlaylist to playlist %d ' -e\
'repeat with i from 1 to the count of the tracks of currentPlaylist ' -e\
'set currentTrack to track i of currentPlaylist ' -e\
'set trk to name of currentTrack & \"	^	\" & artist of currentTrack & \"	^	\" & album of currentTrack & \"	^	\" & duration of currentTrack as list' -e\
'set res to res & trk as list' -e\
'end repeat' -e\
'end tell'";
               

extern "C"
bool mod_init(bool bInit)
{
    return true;
}

extern "C"
const char * mod_getinfo() 
{
    return 
        "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
        "<ModuleInfo Version=\"1\" Name=\"iTunesPlayListModule\" Description=\"iTunes Playlist Extraction Module\" Filename=\"modules\\itunes.dylib\">"
        "  <Commands>"
        "    <Command Name=\"ITUNES_GET_CATEGORIES\" />"
        "    <Command Name=\"ITUNES_GET_PLAYLIST\" />"
        "  </Commands>"
        "</ModuleInfo>";
}
extern "C"
xmlDoc * mod_invoke(xmlNode * reqElem)
{
    try
    {
        char * cmdName = (char*)xmlGetProp(reqElem, BAD_CAST "Name");
        if (cmdName == NULL) throw 1;
        std::string strCmdName = cmdName;
        xmlFree(cmdName);
        if(!isiTunesRunning()){
			std::string strResp = "<Response Error=\"1\" ErrorDesc=\"";
	        strResp += "To import an iTunes playlist, the iTunes application has to be running first.";
        	strResp +="\"></Response>";
			xmlDoc * respDoc = xmlReadMemory(strResp.c_str(), strResp.size(), NULL, NULL, 0);
			return respDoc;
		}
    
        if (strcmp(strCmdName.c_str(), "ITUNES_GET_CATEGORIES") == 0)
        {
            return GetCategories(reqElem);
        }
        else if (strcmp(strCmdName.c_str(), "ITUNES_GET_PLAYLIST") == 0)
        {
            return GetPlayList(reqElem);
        }
        else
        {
            return NULL;
        }
    }
    catch(...) {return NULL;}
    return NULL;
}

xmlDoc * GetCategories(xmlNode * reqElem)
{
    try
    {
        std::vector<std::string> vNames;
        getCategories(vNames);
        
        if (vNames.size() == 0) throw 1;

        xmlDoc * respDoc = xmlNewDoc(BAD_CAST "1.0");
        if (respDoc == NULL) throw 2;
        xmlNode * respElem = xmlNewNode(NULL, BAD_CAST "Response");
        if (respElem == NULL) throw 3;
        xmlDocSetRootElement(respDoc, respElem);
        xmlSetProp(respElem, BAD_CAST "Error", BAD_CAST "0");
        xmlNode * paramsElem = xmlNewChild(respElem, NULL, BAD_CAST "Params", NULL);
        if (paramsElem == NULL) throw 4;
        xmlNode * categorysElem = xmlNewChild(paramsElem, NULL, BAD_CAST "Categories", NULL);
        if (categorysElem == NULL) throw 5;
        for (int i=1; i<vNames.size(); i+=2)
        {
            xmlNode * categoryElem = xmlNewChild(categorysElem, NULL, BAD_CAST "Category", NULL);
            if (categorysElem == NULL) continue;
            xmlSetProp(categoryElem, BAD_CAST "Name", BAD_CAST vNames[i].c_str());
        }
        char buf[128];
        sprintf(buf, "%d", vNames.size());
        xmlSetProp(categorysElem, BAD_CAST "Count", BAD_CAST buf);

        return respDoc;
    }
    catch (int nErr)
    {
        std::string strErrDesc = "Can't get categories.";
        if (nErr == 1) strErrDesc = "No categories found.";
        std::string strResp = "<Response Error=\"1\" ErrorDesc=\"";
        strResp += strErrDesc;
        strResp +="\"></Response>";
        xmlDoc * respDoc = xmlReadMemory(strResp.c_str(), strResp.size(), NULL, NULL, 0);
        return respDoc;
    }

    return NULL;

}

xmlDoc * GetPlayList(xmlNode * reqElem)
{
    try
    {
        xmlNode * paramsElem = CXmlHelper::GetChildElement(reqElem, "Params");
        if (paramsElem == NULL) throw 1;
        xmlNode * categoryElem = CXmlHelper::GetChildElement(paramsElem, "Category");
        if (categoryElem == NULL) throw 2;
        char * categoryName = (char*)xmlGetProp(categoryElem, BAD_CAST "CatId");
        if (categoryName == NULL) throw 3;

        std::vector<TRACK> vTrack;
        getPlaylist(categoryName, vTrack);

        //--------------------------------------

        xmlDoc * respDoc = xmlNewDoc(BAD_CAST "1.0");
        if (respDoc == NULL) throw 6;
        xmlNode * respElem = xmlNewNode(NULL, BAD_CAST "Response");
        if (respElem == NULL) throw 7;
        xmlDocSetRootElement(respDoc, respElem);
        xmlSetProp(respElem, BAD_CAST "Error", BAD_CAST "0");
        xmlNode * paramsRElem = xmlNewChild(respElem, NULL, BAD_CAST "Params", NULL);
        if (paramsRElem == NULL) throw 8;

        xmlNode * plElem = xmlNewChild(paramsRElem, NULL, BAD_CAST "PlayList", NULL);
        if (plElem == NULL) throw 13;

       

        xmlNode * tracksElem = xmlNewChild(plElem, NULL, BAD_CAST "Tracks", NULL);
        if (tracksElem == NULL) throw 14;

        for (int i=0; i<vTrack.size(); i++)
        {
            xmlNode * trackElem = xmlNewChild(tracksElem, NULL, BAD_CAST "Track", NULL);
            if (trackElem == NULL) continue;

            //CFDictionaryRef dict = (CFDictionaryRef)CFArrayGetValueAtIndex(tracks, i);
            TRACK track = (TRACK)vTrack[i];
            //if (track == NULL) continue;

            char buf[128];
            sprintf(buf, "%d", i+1);
            xmlSetProp(trackElem, BAD_CAST "Id", BAD_CAST buf);

            //CFStringRef strArtist = track.Artist.c_str();//(CFStringRef)CFDictionaryGetValue(dict, kDRCDTextPerformerKey);
            //CFStringGetCString(strArtist, buf, 128, kCFStringEncodingASCII);
            //xmlSetProp(trackElem, BAD_CAST "Artist", BAD_CAST buf);
			xmlSetProp(trackElem, BAD_CAST "Artist", BAD_CAST track.Artist.c_str());

            /*CFStringRef strTitle = track.Name.c_str();
            CFStringGetCString(strTitle, buf, 128, kCFStringEncodingASCII);
            xmlSetProp(trackElem, BAD_CAST "Title", BAD_CAST buf);*/
			xmlSetProp(trackElem, BAD_CAST "Title", BAD_CAST track.Name.c_str());
			
           /* CFStringRef strAlbum = track.Album.c_str();
            CFStringGetCString(strAlbum, buf, 128, kCFStringEncodingASCII);
            xmlSetProp(trackElem, BAD_CAST "Album", BAD_CAST buf);*/
			xmlSetProp(trackElem, BAD_CAST "Album", BAD_CAST track.Album.c_str());

            
			int t = atoi( track.Duration.c_str());
			sprintf(buf,"%02d:%02d", t/60,t%60);
			std::string strDur = buf;//"00:00";
           // getTrackDuration(atoi(track.Duration.c_str(), i, strDur);
            xmlSetProp(trackElem, BAD_CAST "Duration", BAD_CAST strDur.c_str());
        }

        
        xmlFree(categoryName);

        return respDoc;
    }
    catch (int nErr)
    {
        std::string strErrDesc = "Can't read playlist.";
        std::string strResp = "<Response Error=\"1\" ErrorDesc=\"";
        strResp += strErrDesc;
        strResp +="\"></Response>";
        xmlDoc * respDoc = xmlReadMemory(strResp.c_str(), strResp.size(), NULL, NULL, 0);
        return respDoc;
    }

    return NULL;
}

/*int _tmain(int argc, _TCHAR* argv[])
{   
   // std::cout << GET_PLAYLISTS_REQUEST;
    std::vector <std::string> m_vString;
    parsePlaylists(strPlaylists, m_vString);
    std::vector<TRACK> m_vTracks;
    parsePlaylistData(strPlaylistData,  m_vTracks);
    for(int i=0;i<m_vTracks.size();i++) {
        cout<<"***BEGIN ***"<<i<<endl;
        cout<<"Name = "<<m_vTracks[i].Name.c_str()<<endl;
        cout<<"Artist = "<<m_vTracks[i].Artist.c_str()<<endl;
        cout<<"Album = "<<m_vTracks[i].Album.c_str()<<endl;
        cout<<"Duration = "<<m_vTracks[i].Duration.c_str()<<endl;
        cout<<"***end ***"<<endl;

    }
	return 0;
}
*/
void getCategories(std::vector<std::string> &vString) {
    FILE * osascript_pipe = NULL;
    char fileBuffer[131072];// = {0};
    memset(fileBuffer, 0, sizeof(fileBuffer));
    //std::cout << GET_PLAYLISTS_REQUEST;
    osascript_pipe = popen(GET_PLAYLISTS_REQUEST, "r");
    if(!osascript_pipe)
        return;
    fread(fileBuffer, sizeof(char), sizeof(fileBuffer), osascript_pipe);
  
    parsePlaylists(fileBuffer, vString);
    
    pclose(osascript_pipe);

}
void getPlaylist(char * playlistName, std::vector<TRACK> &vTrack) {
    FILE * osascript_pipe = NULL;
    std::vector<std::string> vString;
    char fileBuffer[131072];// = {0};
    memset(fileBuffer, 0, sizeof(fileBuffer));
    //std::cout << GET_PLAYLISTS_REQUEST;
    osascript_pipe = popen(GET_PLAYLISTS_REQUEST, "r");
    if(!osascript_pipe)
        return;
    fread(fileBuffer, sizeof(char), sizeof(fileBuffer), osascript_pipe);

    parsePlaylists(fileBuffer, vString);

    pclose(osascript_pipe);
    int playlistNo = 0;
    for(int i = 1; i < vString.size(); i+=2) {
        if(!strcmp(vString[i].c_str(), playlistName)){
            playlistNo = (int)(i+1) / 2;
			break;
        }
    }

   char playlistRequest[65536];// = {0};
   memset(playlistRequest, 0, sizeof(playlistRequest));
   sprintf(playlistRequest, GET_PLAYLIST_REQUEST_TEMPLATE, playlistNo);

   osascript_pipe = popen(playlistRequest, "r");
   if(!osascript_pipe)
       return;
   fread(fileBuffer, sizeof(char), sizeof(fileBuffer), osascript_pipe);
   parsePlaylistData(fileBuffer, vTrack);

   pclose(osascript_pipe);


}
void parsePlaylists(char *str,     std::vector <std::string> &vString) {

        vString.clear();
        if(strlen(str)<1)
            return;
        char * t = str;

        char * pos = NULL;
        int offset = 0;
        while((pos = strchr(t+offset,',')) != NULL) {

            char buff[65535];// = {0};
			memset(buff, 0, sizeof(buff));
            strncpy(buff,t+offset,pos-(t+offset));
            std::string line(buff);
            vString.push_back(line);
            offset = pos- t+2;
        }
        if(strlen(t)>offset){
            char buff[65535];// = {0};
			memset(buff, 0, sizeof(buff));
            strncpy(buff,t+offset,strlen(t)-offset);
            std::string line(buff);
            vString.push_back(line);
        }
  
}
void parsePlaylistData(char *str,  std::vector<TRACK>  & vTracks) {
    std::vector<std::string> vString;
    vString.clear();
    if(strlen(str)<1)
        return;
    char *  t = str;

    char * pos = NULL;
    int offset = 0;
    while((pos = strchr(t+offset,',')) != NULL) {

        char buff[65535];// = {0};
        memset(buff, 0, sizeof(buff));
        strncpy(buff,t+offset,pos-(t+offset));
        std::string line(buff);
        vString.push_back(line);
        offset = pos- t+2;
    }
    if(strlen(t)>offset){
        char buff[65535];// = {0};
		memset(buff, 0, sizeof(buff));
        strncpy(buff,t+offset,strlen(t)-offset);
        std::string line(buff);
        vString.push_back(line);
    }
    for(int i=0; i < vString.size(); i++)
    {
        const char *cstr = vString[i].c_str();
        std::vector<std::string> vTrack;
        vTrack.clear();
        if(strlen(cstr)<1)
            return;
        const char * t = cstr;

        char * pos = NULL;
        int offset = 0;
        while((pos = strstr(	t+offset,"\t^\t")) != NULL) {

            char buff[65535];// = {0};
			memset(buff, 0, sizeof(buff));
            if(*(t+offset) == '^')
                offset+=2;
            strncpy(buff,t+offset,pos-(t+offset));
            std::string line(buff);
            vTrack.push_back(line);
            offset = pos- t+1;
        }
        if(strlen(t)>offset){
            char buff[65535];// = {0};
			memset(buff, 0, sizeof(buff));
            if(*(t+offset) == '^')
                offset+=2;
            strncpy(buff,t+offset,strlen(t)-offset);
            std::string line(buff);
            vTrack.push_back(line);
        }
        if(vTrack.size() == 4){
            TRACK tr;
            tr.Name = vTrack[0];
            tr.Artist = vTrack[1];
            tr.Album = vTrack[2];
            tr.Duration = vTrack[3];
            vTracks.push_back(tr);
        }
        
     /*   if(!(i%2)) cout<<endl;
        cout<<vString[i].c_str();
        cout<<"\t";*/

    }


    return;
}

bool isiTunesRunning(){ 
   FILE * itunes_pipe = NULL;
    char fileBuffer[262144];// = {0};
	memset(fileBuffer, 0, sizeof(fileBuffer));
    itunes_pipe = popen("ps -x", "r");
    bool result = false;
    if(!itunes_pipe)
        return result;
    fread(fileBuffer, sizeof(char), sizeof(fileBuffer), itunes_pipe);
    if(strstr(fileBuffer, "iTunes.app/Contents/MacOS/iTunes") != NULL)
	result = true;
    pclose(itunes_pipe);
    return result;
}