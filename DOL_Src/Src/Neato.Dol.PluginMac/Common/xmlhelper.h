#ifndef _XML_HELPER_H_
#define _XML_HELPER_H_

#include <libxml/parser.h>
#include <libxml/tree.h>


class CXmlHelper
{
public:
	static xmlNode * GetChildElement(xmlNode * pNode, const char * pName);
};


#endif _XML_HELPER_H_
