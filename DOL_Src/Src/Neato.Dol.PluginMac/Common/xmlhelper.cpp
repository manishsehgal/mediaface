#include <string.h>
#include "xmlhelper.h"


xmlNode * CXmlHelper::GetChildElement(xmlNode * pNode, const char * pName)
{
	for (xmlNode * node = pNode->children; node != NULL; node = node->next)
	{
		if (node->type == XML_ELEMENT_NODE && strcmp((const char*)node->name, pName) == 0)
		{
            return node;
        }
    }
	return NULL;
}



