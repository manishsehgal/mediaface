#include "Preview.h"
#include "PrintProgress.h"
#include "bmpHelper.h"
#include "util/GuiUtils.h"

#define PREVIEW_BMP_FILE_NAME "/tmp/qtBurnerPreview.bmp"

const ControlID kImageID = { 'Imag', 1 };

//--------------------------------------------------------------------------------------------
void
CPreview::Create(LSUtil::PrintOptions printOptions, bool isMediaInserted)
{
	if (!isMediaInserted) {
		DiscPrinter printer = LSUtil::CreatePrinter(printOptions.driveIndex);
		printer.OpenDriveTray();
		MessageBox("Please insert a LightScribe disc, label side down, into the drive.");	
		printer.CloseDriveTray();
	}

    CPreview* wind = new CPreview(printOptions, isMediaInserted);

    // Position new windows in a staggered arrangement on the main screen
    RepositionWindow( *wind, NULL, kWindowCascadeOnMainScreen );
    
    // The window was created hidden, so show it
    wind->Show();
}

//--------------------------------------------------------------------------------------------
CPreview::CPreview(LSUtil::PrintOptions printOptions, bool isMediaInserted)
	: TWindow( CFSTR("Preview") )
	, m_printOptions(printOptions)
{
	HIViewFindByID( HIViewGetRoot( GetWindowRef() ), kImageID, &fImageView );
	
	if (updatePreview(isMediaInserted)) {
		CBmpHelper bmpPreview;
		if (bmpPreview.Load(PREVIEW_BMP_FILE_NAME) != noErr) {
			ErrorBox("Can not load image");
		} else {
			CGImageRef image = bmpPreview.GetImage();
			HIImageViewSetImage(fImageView, image);
			CGImageRelease (image);
		}
		//if (access(PREVIEW_BMP_FILE_NAME, 0) != -1)
		remove(PREVIEW_BMP_FILE_NAME);
	}
}

//--------------------------------------------------------------------------------------------
bool
CPreview::updatePreview(bool isMediaInserted) {
	LS_Size previewSize;
	previewSize.x = 400;
	previewSize.y = 400;
	
	BYTE *header=m_printOptions.pBmpHelper->GetImageHeader();
	int headerLength=m_printOptions.pBmpHelper->GetImageHeaderSize();
	BYTE *data=m_printOptions.pBmpHelper->GetImageData();
	int dataLength=m_printOptions.pBmpHelper->GetImageDataSize();


	DiscPrinter discPrinter = LSUtil::CreatePrinter(m_printOptions.driveIndex);
	discPrinter.CloseDriveTray();
	try {
		if (isMediaInserted) {
			DiscPrintSession s = LSUtil::CreateSession(m_printOptions.driveIndex);
			s.PrintPreview(LS_windows_bitmap, m_printOptions.labelMode, LS_draw_fit_smallest_to_label,
					m_printOptions.quality, m_printOptions.optLevel, 
					header, headerLength, 
					data, dataLength, 
					PREVIEW_BMP_FILE_NAME, LS_windows_bitmap, previewSize, false);
			return true;
		}
	}
	catch (LSException& ex) {
	}
	m_printOptions.optLevel = LS_media_generic;
	try {
		if (isMediaInserted) {
			DiscPrintSession s = LSUtil::CreateSession(m_printOptions.driveIndex);
			s.PrintPreview(LS_windows_bitmap, m_printOptions.labelMode, LS_draw_fit_smallest_to_label,
					m_printOptions.quality, m_printOptions.optLevel, 
					header, headerLength, 
					data, dataLength, 
					PREVIEW_BMP_FILE_NAME, LS_windows_bitmap, previewSize, true);
			return true;
		}
	}
	catch (LSException& ex) {
		return false;
	}
}

//--------------------------------------------------------------------------------------------
Boolean
CPreview::HandleCommand( const HICommandExtended& inCommand )
{
    switch ( inCommand.commandID )
    {
        // Add your own command-handling cases here
		case kHICommandPrint:
			Close();
			CPrintProgress::Create(m_printOptions);
			return true;
        
        default:
            return false;
    }
}


