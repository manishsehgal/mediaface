#include <Carbon/Carbon.h>
#include "TApplication.h"
#include "MainWindow.h"


// Our custom application class
class CarbonApp : public TApplication
{
    public:
							
                            CarbonApp() {}
        virtual             ~CarbonApp() {}
        
    protected:
        virtual Boolean     HandleCommand( const HICommandExtended& inCommand );
};

//--------------------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	CarbonApp app;
    
	//char path[] = "/Users/azh/ls.bmp";
	char *path = argv[1];
    CMainWindow::Create(path, argv[2], argv[3], argv[4], argv[5]);
	remove(path);
    
    app.Run();
    return 0;
}

//--------------------------------------------------------------------------------------------
Boolean
CarbonApp::HandleCommand( const HICommandExtended& inCommand )
{
    switch ( inCommand.commandID )
    {
        // Add your own command-handling cases here
        
        default:
            return false;
    }
}
