#include <pthread.h>
#include "DetectMediaDialog.h"
#include </usr/include/lightscribe_cxx.h>

using namespace LightScribe;

struct DetectParam { 
	LS_MediaOptimizationLevel optLevel;
	LS_MediaInformation info;
	int driveIndex;
};
	
pthread_mutex_t CDetectMediaDialog::dialog_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t detect_media_mutex = PTHREAD_MUTEX_INITIALIZER;

void *DetectMediaThread(void * arg) {
	DetectParam *param = (DetectParam*)arg;
	int localIndex;
	LS_MediaOptimizationLevel localOptLevel;
	LS_MediaInformation localInfo;
	LSError localError = LS_OK;

	{
		pthread_mutex_lock(&detect_media_mutex);
		localIndex = param->driveIndex;
		localOptLevel = param->optLevel;
		pthread_mutex_unlock(&detect_media_mutex);
	}

	try {
		DiscPrintMgr mgr;
		EnumDiscPrinters e = mgr.EnumDiscPrinters();
		DiscPrinter p = e.Item(localIndex);

		p.CloseDriveTray();
		localInfo = p.GetCurrentMedia(LS_media_recognized);
  
		// just for testing...
		//    sleep(2);
	}
	catch (LightScribe::LSException& e) {
		localError = e.GetCode();
	}
    
	// Indicate that we've finished, setting the error code as appropriate
	{
		pthread_mutex_lock(&detect_media_mutex);
		param->info = localInfo;
		pthread_mutex_unlock(&detect_media_mutex);
	}
	
	return (void*)localError;
}
 
//--------------------------------------------------------------------------------------------
CDetectMediaDialog::CDetectMediaDialog() : TWindow( CFSTR("DetectMedia") )
{
}

//--------------------------------------------------------------------------------------------
LSError
CDetectMediaDialog::getMediaInformation( 
	int driveIndex, 
	LS_MediaOptimizationLevel optLevel,
	LS_MediaInformation* info)
{
	DetectParam param;
	param.optLevel = optLevel;
	param.driveIndex = driveIndex;
	LSError localError = LS_OK;
	
	pthread_mutex_lock(&dialog_mutex);
	CDetectMediaDialog* w = new CDetectMediaDialog();
	RepositionWindow( *w, NULL, kWindowCascadeOnMainScreen );
	w->Show();
	
	pthread_t thread = 0;
	pthread_create(&thread, NULL, DetectMediaThread, &param);
	pthread_join(thread, (void**)&localError);
	
	*info = param.info;
	
	w->Hide();	
	delete w;
	pthread_mutex_unlock(&dialog_mutex);
	
	return localError;
}


//--------------------------------------------------------------------------------------------
Boolean
CDetectMediaDialog::HandleCommand( const HICommandExtended& inCommand )
{
    switch ( inCommand.commandID )
    {
        // Add your own command-handling cases here        
        default:
            return false;
    }
}


