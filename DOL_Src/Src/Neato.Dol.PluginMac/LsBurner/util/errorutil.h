// LightScribe Sample Application
// Hewlett-Packard Confidential
// Copyright 2004 Hewlett-Packard Development Company, LP

// $Id$

#ifndef _ERRORUTIL_H
#define _ERRORUTIL_H

#include "/usr/include/lightscribe.h"

/**
 * A class which holds utility functions for dealing with 
 * LightScribe error codes. 
 */
class ErrorUtil {
public:
  //! Possible actions to take as the result of an error
  enum ErrorAction {
    cancelPrintAction, 
    genericPrintAction, 
    updateAction, 
    tryAgainAction
  };

  //! Show a critical error dialog - good for things like init failures. 
  static void criticalError(/*QWidget* parent*/);

  //! Is this error a member of the media error class?
  static bool isMediaError(LSError e);

  //! Is this error a member of the drive error class?
  static bool isDriveError(LSError e);

  //! Will an update possibly resolve this error?
  static bool updateWillFix(LSError e);

  //! Can this error be bypassed by specifying generic printing?
  static bool genericOK(LSError e);

  //! Given this error, does it make sense to prompt the user to retry?
  static bool retryIsSensible(LSError e);

  //! Return an error message suitable for display.  
  static const char * getMessage(LSError e);
};

#endif
