#include <Carbon/Carbon.h>

void MessageBox(const char* text);
void ErrorBox(const char* text);
bool Confirm(const char* text);

OSStatus SetEditTextText( ControlHandle control, const char *text );
OSStatus GetPopupButtonMenuRef( ControlRef popupButton, MenuRef* outMenu );

OSStatus SetProgressIndicatorState( ControlHandle control, Boolean  isDeterminate );
OSStatus GetProgressIndicatorState( ControlHandle control, Boolean* isDeterminate );
