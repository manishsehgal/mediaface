#include "GuiUtils.h"

SInt16 ShowAlert(AlertType inAlertType, const char* text, bool confirm) {
	AlertStdAlertParamRec alertParameters;
	alertParameters.movable			= true;
	alertParameters.helpButton		= false;
	alertParameters.filterProc		= NULL;
	alertParameters.defaultText		= (StringPtr)kAlertDefaultOKText;
	alertParameters.cancelText		= confirm ? (StringPtr)kAlertDefaultCancelText : 0;
	alertParameters.otherText		= NULL;
	alertParameters.defaultButton	= kAlertStdAlertOKButton;
	alertParameters.cancelButton	= confirm ? kAlertStdAlertCancelButton : 0;
	alertParameters.position		= kWindowDefaultPosition;
	SInt16					outItemHit;

	CFStringRef str = CFStringCreateWithCString(NULL, text,  GetApplicationTextEncoding());
	Str255      stringBuf;
	CFStringGetPascalString(str, stringBuf, sizeof (stringBuf), GetApplicationTextEncoding());
	
	StandardAlert(inAlertType,
					stringBuf,
					NULL, 
					&alertParameters, &outItemHit);
					
	CFRelease (str);
	
	return outItemHit;
}

void MessageBox(const char* text) {
	ShowAlert(kAlertNoteAlert, text, false);
}

void ErrorBox(const char* text) {
	ShowAlert(kAlertStopAlert, text, false);
}

bool Confirm (const char* text) {
	SInt16 choice = ShowAlert(kAlertCautionAlert, text, true);
	return choice == kAlertStdAlertOKButton;
}

OSStatus
SetEditTextText( ControlHandle control, const char *text )
{
	OSStatus	err;
	
	if ( control == nil || text == nil)
		return paramErr;

	err = SetControlData( control, 0, kControlEditTextTextTag, strlen(text), text );
	//if ( (err == noErr) && draw )
	//	DrawOneControl( control );
	
	return err;
}


OSStatus
GetPopupButtonMenuRef( ControlRef popupButton, MenuRef* outMenu )
{
	OSStatus	err;
	
	err = GetControlData( popupButton, 0, kControlPopupButtonMenuRefTag,
			sizeof( MenuRef ), outMenu, NULL );
	
	return err;
}

//	Sets a progress bar to the determinate or indeterminate state.
//
OSStatus
SetProgressIndicatorState( ControlHandle control, Boolean isDeterminate )
{
	OSStatus	err;
	Boolean		state;
	
	if ( control == nil )
		return paramErr;

	state = !isDeterminate;	
	err = SetControlData( control, 0, kControlProgressBarIndeterminateTag, sizeof( state ),
			(Ptr)&state );
	
	return err;
}

//	Gets progress bar determinate or indeterminate state.
//
OSStatus
GetProgressIndicatorState( ControlHandle control, Boolean* isDeterminate )
{
	Size		actualSize;
	OSStatus	err;
	Boolean		temp;
	
	if ( control == nil )
		return paramErr;
		
	if ( isDeterminate == nil )
		return paramErr;
		
	err = GetControlData( control, 0, kControlProgressBarIndeterminateTag, sizeof( temp ),
			 (Ptr)&temp, &actualSize );
	
	if ( err == noErr )
		*isDeterminate = !temp;
		
	return err;
}
