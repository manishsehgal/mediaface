#include "errorutil.h"

void ErrorUtil::criticalError(/*QWidget* parent*/){
	printf("In ErrorUtil::criticalError()\n");
	/*QMessageBox::critical(parent, tr("Critical LightScribe error"), 
						  tr("There was a critical error accessing the LightScribe subsystem.  Please check your installation. "));
	*/
	printf("showed error message\n");
}


bool ErrorUtil::isMediaError(LSError e){
	switch(e) {
	case LS_E_NoMedia:
	case LS_E_NonLightScribeMedia:
	case LS_E_UnableToReadMediaParams:
	case LS_E_UnsupportedMediaId:
	case LS_E_InvalidMediaParams:
	case LS_E_MediaNotSupportedByDrive:
	case LS_E_InvalidMediaOrientation:
	case LS_E_UnsupportedMedia:
	case LS_E_UnsupportedMediaNoUpdate:
	case LS_E_IncompatibleMedia:
		return true;

	default:
		return false;
	}
}

bool ErrorUtil::isDriveError(LSError e){
	switch(e) {
	case LS_E_UnsupportedDrive:
	case LS_E_UnsupportedDriveCommunications:
	case LS_E_CorruptResourceFile:
	case LS_E_UpdateRequired:
		return true;
	default:
		return false;
	}
}

bool ErrorUtil::updateWillFix(LSError e){
	switch(e) {
	case LS_E_UnsupportedDrive:
	case LS_E_UnsupportedDriveCommunications:
	case LS_E_CorruptResourceFile:
	case LS_E_UpdateRequired:
	case LS_E_UnsupportedMedia:  
	case LS_E_UnsupportedMediaId:
		return true;
	default:
		return false;
	}
}

bool ErrorUtil::genericOK(LSError e){
	switch(e) {
	case LS_E_UnsupportedMedia:  
	case LS_E_UnsupportedMediaNoUpdate:
		return true;
	default:
		return false;
	}
}

bool ErrorUtil::retryIsSensible(LSError e){
	switch(e) {
	case LS_E_UnsupportedMediaId:  
	case LS_E_InvalidMediaOrientation:
	case LS_E_NoMedia:    
	case LS_E_DriveInUse:
	case LS_E_UnsupportedMedia:
	case LS_E_UnsupportedMediaNoUpdate:
	case LS_E_InvalidMediaParams:
	case LS_E_MediaNotSupportedByDrive:
	case LS_E_IncompatibleMedia:
	case LS_E_NonLightScribeMedia:
	case LS_E_UnableToReadMediaParams:
		return true;
	default:
		return false;
	}
}

const char * ErrorUtil::getMessage(LSError e){
	switch(e) {
	case LS_E_MemoryError:
	case LS_E_BadArgument:
	case LS_E_InternalError:
	case LS_E_PrintPreviewFileError:
	case LS_E_CallbackException:
		return "An internal error has occurred.  Please restart the software application.";

	case LS_E_InstallationError:
		return "An error was detected in the LightScribe software installation - please reinstall and try again.";

	case LS_E_HardwareError:
	case LS_E_CommunicationsError:
		return "A communication error has occurred with the LightScribe drive.  Please restart your PC and try the label again.";

	case LS_E_UnsupportedSource:
	case LS_E_InvalidSource:
	case LS_E_UnsupportedPreview:
	case LS_E_ReentrantCall:
		return "An internal error has occured.  Please contact your labeling software support.";

	case LS_E_UnsupportedDrive:
	case LS_E_UnsupportedDriveCommunications:
	case LS_E_CorruptResourceFile:
	case LS_E_UpdateRequired:
		return "An update is required to label discs with this drive.";

	case LS_E_UnsupportedMediaId:
		return "An update is required to label this kind of media.";

	case LS_E_InvalidMediaOrientation:
		return "In order to label your disc, please turn the LightScribe disc over.";

	case LS_E_NoMedia:
		return "A LightScribe disc was not detected in the drive.  Please insert a LightScribe disc, label-side down.";

	case LS_E_Aborted:
		return "LightScribe labelling has been cancelled.";
    
	case LS_E_DriveTrayLocked:
	case LS_E_DriveInUse:
		return "The LightScribe drive is currently in use.";

	case LS_E_UnsupportedMedia:
		return "The LightScribe drive cannot print to the current media at full quality.  An update may fix this.  Alternatively, generic labeling parameters may be used at reduced quality.";
	
	case LS_E_UnsupportedMediaNoUpdate:
		return "The LightScribe drive cannot print to the current media at full quality.  Alternitavely, generic labeling parameters may be used at reduced quality.";    

	case LS_E_InvalidMediaParams:
	case LS_E_MediaNotSupportedByDrive:
	case LS_E_IncompatibleMedia:
		return "This disc is incompatible with the selected LightScribe drive.  Please refer to your LightScribe documentation";

	case LS_E_NonLightScribeMedia:
	case LS_E_UnableToReadMediaParams:
		return "The disc in the LightScribe drive is not recognized. Please ensure that the disc is a LightScribe disc and properly oriented.  Clean the disc per the LightScribe documentation.";

	case LS_E_DriveChanged:
		return "The LightScribe drive has changed.  Please re-select the LightScribe drive you wish to use.";
	default:
		return "An unknown error occurred.";
	}
}
