// LightScribe Sample Application
// Hewlett-Packard Confidential
// Copyright 2004 Hewlett-Packard Development Company, LP

// $Id$

#include "infobox.h"

/*#include <QDialog>
#include <QApplication>
#include <QLabel>
#include <QCursor>
#include <QTimer>
#include <QMouseEvent>
#include <QCloseEvent>

#define MINIMUM_HEIGHT 80

InfoBox::InfoBox(QWidget* parent) :	QTextBrowser(parent),
									_moves(0), 
									_popupParent(NULL){
	this->setCurrentFont(font());
	this->setMinimumHeight(MINIMUM_HEIGHT);
	
	this->setFrameStyle(QFrame::NoFrame);

}

void InfoBox::setSource (const QUrl &name){
	emit linkClicked(name.toString());
}

void InfoBox::popup(QWidget* popupParent, const QString& text, bool animate) {
	_popupParent = popupParent;

	this->setHtml(text);
	this->setCurrentFont(font());

	if(_popupParent){
		this->resize(_popupParent->width() - 4, this->height());
	}
	else{
		this->resize(150, this->height());
	}

	if(animate) {
		_timer = new QTimer(this); // automatically freed when 'this' is destroyed. 
		connect( _timer, SIGNAL(timeout()), this, SLOT(updateAnimation()) );
		_currentFrame=0;
		_timer->start(1);
		resize(QSize(this->width(), 0) );
	} else {
		resize(QSize(this->width(), this->height()) );
	}

	if (_popupParent)
		move(_popupParent->mapToParent(_popupParent->rect().bottomLeft() + QPoint(2, 2)));

	show();
}

void InfoBox::updateAnimation(){
	double sizeRatio = (double)_currentFrame / 10.0f;
	int height = (int) ((double)(this->height()) * sizeRatio);
	resize( QSize(this->width(), height) );

	_currentFrame++;
	if(_currentFrame >= 10) {
		_timer->stop();
		resize( QSize(this->width(), this->height()) );
	}
}
*/
