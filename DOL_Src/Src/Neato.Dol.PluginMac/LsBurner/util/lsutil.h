#ifndef _LSUTIL_H
#define _LSUTIL_H

#include "errorutil.h"
#include </usr/include/lightscribe_cxx.h>
#include "bmphelper.h"
#include "GuiUtils.h"
#include <string>
using namespace LightScribe;

/**
 * Miscellaneous stuff to make it easier to deal with the LightScribe
 * API. 
 */
class LSUtil {
public:
  //! A useful structure used to pass around everything you might want in a print. 
  struct PrintOptions
  {
    int driveIndex;
    LS_LabelMode labelMode;
    LS_PrintQuality quality;
    LS_MediaOptimizationLevel optLevel;
    CBmpHelper* pBmpHelper;
  };

  static inline DiscPrinter CreatePrinter(int index) 
  {
    DiscPrintMgr mgr;
    EnumDiscPrinters e = mgr.EnumDiscPrinters();
    return e.Item(index);
  };

  static inline DiscPrintSession CreateSession(int index)
  {
    DiscPrinter p = CreatePrinter(index);
    return p.OpenPrintSession();
  }

  //! Get the update command and run it.  
  static void GetUpdate()
  {
    try {
      system(LightScribe::GetUpdateShellCommand().c_str());
    } catch (LSException& e) {
      ErrorBox( ErrorUtil::getMessage(e.GetCode()) );
    }
  }

  static void OpenTray(int index)
  {
    DiscPrinter p = CreatePrinter(index);
    p.OpenDriveTray();
  }

  static void CloseTray(int index)
  {
    DiscPrinter p = CreatePrinter(index);
    p.CloseDriveTray();
  }
  
};

#endif
