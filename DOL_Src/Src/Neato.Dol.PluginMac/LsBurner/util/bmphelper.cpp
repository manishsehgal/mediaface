#include "bmphelper.h"

const long PIXELS_PER_INCH=72;
const long PIXELS_PER_METRE=(long)(PIXELS_PER_INCH*100/2.54 + 0.5);

CBmpHelper::CBmpHelper()
	: _fileData(NULL)
	, _bitmapInfo(NULL)
	, _rawBits(NULL)
	, _fileSize(0)
	, _headerSize(0)
	, _imageSize(0)
{
}

CBmpHelper::~CBmpHelper() {
	reset();
}

OSStatus CBmpHelper::Load(const char *path) {
	reset();
	
	OSStatus err = noErr;
	
	FILE* f = fopen(path, "r");
	if (f != NULL) {
		err = loadFile(f);
		fclose(f);
	} else {
		err = -1;
		reset();
	}
	
	return err;
}

OSStatus CBmpHelper::loadFile(FILE *f) {
	fseek(f, 0, SEEK_END);
	_fileSize = ftell(f);
	fseek(f, 0, SEEK_SET);

	_fileData = new BYTE[_fileSize];
	if (!_fileData)
		return 1;

	if (fread(_fileData, _fileSize, 1, f) != 1)
		return 2;

	BITMAPFILEHEADER* pFileHeader = (BITMAPFILEHEADER*)_fileData;
	
	if(EndianU16_LtoN(pFileHeader->bfType) != BITMAP_HEADER)
		return 3;
	
	//header
	_headerSize = EndianU32_LtoN(pFileHeader->bfOffBits) - sizeof(BITMAPFILEHEADER);
	_bitmapInfo = new BYTE[_headerSize];
	memcpy(_bitmapInfo, _fileData + sizeof(BITMAPFILEHEADER), _headerSize);
	BITMAPINFOHEADER &bmiHeader = *(BITMAPINFOHEADER *)_bitmapInfo;
	
	bmiHeader.biSize = EndianU32_LtoN(bmiHeader.biSize);
	bmiHeader.biWidth = EndianS32_LtoN(bmiHeader.biWidth);
	bmiHeader.biHeight = EndianS32_LtoN(bmiHeader.biHeight);
	bmiHeader.biPlanes = EndianU16_LtoN(bmiHeader.biPlanes);
	bmiHeader.biBitCount = EndianU16_LtoN(bmiHeader.biBitCount);
	bmiHeader.biCompression = EndianU32_LtoN(bmiHeader.biCompression);
	bmiHeader.biSizeImage = EndianU32_LtoN(bmiHeader.biSizeImage);
	bmiHeader.biXPelsPerMeter = EndianS32_LtoN(bmiHeader.biXPelsPerMeter);
	bmiHeader.biYPelsPerMeter = EndianS32_LtoN(bmiHeader.biYPelsPerMeter);
	bmiHeader.biClrUsed = EndianU32_LtoN(bmiHeader.biClrUsed);
	bmiHeader.biClrImportant = EndianU32_LtoN(bmiHeader.biClrImportant);
		
	//validate the header size
	//NB 0x28h =v3, 0x6C=V4 and 0x7c=v5 
	DWORD infoHeaderSize=bmiHeader.biSize;
	if(infoHeaderSize != sizeof(BITMAPINFOHEADER)
		//&& infoHeaderSize != sizeof(BITMAPV4HEADER)
		//&& infoHeaderSize != sizeof(BITMAPV5HEADER)
	)
	{
		return 4;
	}
	
	//now parse the header to determine bitmap size
	// check number of planes. Windows 3.x supports only 1 plane DIBs
	if (bmiHeader.biPlanes != 1) {
		return 5;
	}
	
	if (bmiHeader.biCompression!= BI_RGB) {
		return 6;
	}
	
	//patch resolution
	if(bmiHeader.biXPelsPerMeter==0)
		bmiHeader.biXPelsPerMeter=PIXELS_PER_METRE;
	
	if(bmiHeader.biYPelsPerMeter==0)
		bmiHeader.biYPelsPerMeter=PIXELS_PER_METRE;
	
	int bitsPerPixel = bmiHeader.biBitCount;
	int width=bmiHeader.biWidth;
	int height=bmiHeader.biHeight;
	int trueWidth= (int)((width*bitsPerPixel+31L) / 32) * 4;
	int trueHeight=height>=0?height:(-height);
	_imageSize=trueWidth*trueHeight;
	
	_rawBits = _fileData + sizeof(BITMAPFILEHEADER) + _headerSize;	

	return noErr;
}

CGImageRef CBmpHelper::GetImage() {
	if (_fileData == NULL)
		return NULL;
	
	CFDataRef data = CFDataCreate(NULL, _fileData, _fileSize);	
	CGImageSourceRef imageSource = CGImageSourceCreateWithData(data, NULL);  
    CGImageRef image = CGImageSourceCreateImageAtIndex(imageSource, 0, NULL);
	CFRelease(data);
	CFRelease(imageSource);
	
	return image;
}

void CBmpHelper::reset() {
	if (_fileData != NULL)
		delete [] _fileData;

	if (_bitmapInfo != NULL)
		delete [] _bitmapInfo;

	_fileData = NULL;
	_rawBits = NULL;
	
	_fileSize = 0;	
	_headerSize = 0;
	_imageSize = 0;		
}

BYTE* CBmpHelper::GetImageHeader() {
	if (_fileData == NULL)
		return NULL;
		
	return _bitmapInfo;
}

int CBmpHelper::GetImageHeaderSize() {
	if (_fileData == NULL)
		return 0;
		
	return _headerSize;
}
	
BYTE* CBmpHelper::GetImageData() {
	if (_fileData == NULL)
		return NULL;
		
	return _rawBits;
}

int CBmpHelper::GetImageDataSize() {
	if (_fileData == NULL)
		return 0;
		
	return _imageSize;
}