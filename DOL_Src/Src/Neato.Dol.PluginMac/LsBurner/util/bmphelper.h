#pragma once

#include <Carbon/Carbon.h>

typedef unsigned char	BYTE;
typedef unsigned short	WORD;
typedef unsigned long	DWORD;
typedef long			LONG;

/* constants for the biCompression field */
#define BI_RGB        0L
const WORD BITMAP_HEADER = 0x4d42;

#pragma pack(2)
/**
 * Win32 BITMAPFILEHEADER strucutre. 
 */
struct BITMAPFILEHEADER
{
  WORD  bfType;
  DWORD bfSize;
  WORD  bfReserved1;
  WORD  bfReserved2;
  DWORD bfOffBits;
};

/**
 * Win32 BITMAPINFOHEADER strucutre. 
 */
struct BITMAPINFOHEADER
{
  DWORD	biSize;
  LONG  biWidth;
  LONG  biHeight;
  WORD  biPlanes;
  WORD  biBitCount;
  DWORD biCompression;
  DWORD biSizeImage;
  LONG  biXPelsPerMeter;
  LONG  biYPelsPerMeter;
  DWORD biClrUsed;
  DWORD biClrImportant;
};

/**
 * Win32 RGBQUAD strucutre. 
 */
struct RGBQUAD
{
  BYTE rgbBlue;
  BYTE rgbGreen;
  BYTE rgbRed;
  BYTE rgbReserved;
};

/**
 * Win32 BITMAPINFO strucutre. 
 */

struct BITMAPINFO
{
  BITMAPINFOHEADER bmiHeader;
  RGBQUAD bmiColors[1];
};
#pragma pop

class CBmpHelper {
private:
	BYTE* _bitmapInfo;
	int _headerSize;
	BYTE* _rawBits;
	int _imageSize;
	BYTE* _fileData;
	int _fileSize;

public:
	CBmpHelper();
	virtual ~CBmpHelper();

	OSStatus Load(const char *path);
	CGImageRef GetImage();
	
	BYTE* GetImageHeader();
	int GetImageHeaderSize();
	
	BYTE* GetImageData();
	int GetImageDataSize();
	
private:
	void reset();
	OSStatus loadFile(FILE *f);
};

