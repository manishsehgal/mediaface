// LightScribe Sample Application
// Hewlett-Packard Confidential
// Copyright 2004 Hewlett-Packard Development Company, LP

// $Id$

#ifndef _PROGRESSCALCULATOR_H
#define _PROGRESSCALCULATOR_H

#include <sstream>
#include <math.h>

/**
 * A helper class to make progress estimation easy. 
 */
class ProgressCalculator {
  long _currentValue;
  long _finalValue;
  time_t _durationHint;
  time_t _startTime;

public:
  /**
   * @param finalValue The progress value that is considered "complete". 
   * @param durationhint A hint of how long the process is expected to take.  A value of 0 is ignored. 
   */
  ProgressCalculator(long finalValue, time_t durationHint) :
    _finalValue(finalValue), _durationHint(durationHint)
  {
    time(&_startTime);
  }
  
  void setCurrentValue(long currentValue)
  {
    _currentValue = currentValue;
  }
  
  float getPercentDone()
  {
    return ((float)_currentValue / (float)_finalValue) * 100.0f;
  }

  time_t getTimeRemaining()
  {
    time_t now;
    time(&now);

    time_t elapsed = now - _startTime;

    float fractionDone = getPercentDone() / 100.0f;
    float extrapolatedTotalTime = elapsed / fractionDone;

    float totalTime;

    // If we have a hint as to how long it'll take
    if(_durationHint != 0 && (_durationHint-elapsed) > 0) {
      if(fractionDone <= 0)
	totalTime = _durationHint;
      else
	// The duration hint is weighted more strongly at the beginning aof the print, and the
	// extrapolated time is weighted more strongly towards the end. 
	totalTime = static_cast<float>(_durationHint) * (1.0f - fractionDone) + extrapolatedTotalTime * fractionDone;

    }
    else {
      // as a fallback, just use the extrapolation
      totalTime = extrapolatedTotalTime;
    }
    
	time_t remaining = static_cast<time_t>(totalTime) - elapsed;
    return remaining;
  }


  /**
   * Get a mac-style "about somuch time" string. 
   */
  std::string getFuzzyEstimate()
  {
	std::ostringstream fuzzyEstimate;
    long remaining = getTimeRemaining();   

    // Above 1 minute, round to the nearest minute. 
    if(remaining > 60) {
      long minutes = (long) floor((remaining/60.0f) + 0.5);
      fuzzyEstimate << minutes << " minute";
      if(minutes > 1)
	fuzzyEstimate << "s";
    }
    else {
      fuzzyEstimate << "Less than 1 minute";
    }

    fuzzyEstimate << " remaining";
    return fuzzyEstimate.str();
  }
  
};

#endif
