#include "PrintProgress.h"
#include <String.h>
#include "ErrorDialog.h"
#include "util/GuiUtils.h"

enum
{
	kCancelCmd = 'CANC'
};

const ControlID kTextStatus  = { 'TEXT', 1 };
const ControlID kProgressBar = { 'PRGB', 2 };
const ControlID kCancelBtn   = { 'BTN ', 3 };


//--------------------------------------------------------------------------------------------
CPrintProgress::CPrintProgress() 
	: TWindow( CFSTR("PrintProgress") )
	,_busyUpdate(false)
	,_progressCalc(0,0)
	,_progressInitialized(false)
	,_lastProgress(-1)
{
	m_flags.set_abort(false);
	m_flags.set_prepare_progress(0, 0);
	m_flags.set_print_progress(0, 0);
	m_flags.set_estimate_time(0);
	const EventTypeSpec	kEvents[] = {
		{ kEventClassCust, kEventDetectingMedia },
		{ kEventClassCust, kEventMediaDetectionFinished },	
		{ kEventClassCust, kEventQueryCancel },	
		{ kEventClassCust, kEventPrepareProgress },
		{ kEventClassCust, kEventPrintProgress },
		{ kEventClassCust, kEventTimeEstimate },
		{ kEventClassCust, kEventError },
		{ kEventClassCust, kEventPrintFinished }		
	};
	
	RegisterForEvents( GetEventTypeCount( kEvents ), kEvents );
	
	initControls();
	
	InstallEventLoopTimer( GetCurrentEventLoop(), 0, 1, GetTimerUPP(), this, &m_busyTimer );
}

CPrintProgress::~CPrintProgress()
{
	if ( m_busyTimer )
		RemoveEventLoopTimer( m_busyTimer );
}

//--------------------------------------------------------------------------------------------
void
CPrintProgress::initControls() {
	GetControlByID( GetWindowRef(), &kProgressBar, &m_progressBar);	
	GetControlByID( GetWindowRef(), &kTextStatus , &m_textStatus);
	GetControlByID( GetWindowRef(), &kCancelBtn  , &m_btnCancel);
	
	SetEditTextText(m_textStatus, "");
	SetControlValue(m_progressBar, 0);
}

//--------------------------------------------------------------------------------------------
void
CPrintProgress::Create(LSUtil::PrintOptions printOptions)
{
	CPrintProgress* wind = new CPrintProgress();
	// Position new windows in a staggered arrangement on the main screen
    RepositionWindow( *wind, NULL, kWindowCascadeOnMainScreen );
    
    // The window was created hidden, so show it
    wind->Show();
	
	DiscPrinter printer = LSUtil::CreatePrinter(printOptions.driveIndex);
	printer.OpenDriveTray();
	MessageBox("Please insert a LightScribe disc, label side down, into the drive.");	
	printer.CloseDriveTray();
	wind->m_printOptions = printOptions;
	
	wind->m_printThread.start(&(wind->m_printOptions), &wind->m_flags, wind->GetEventTarget());
}

//--------------------------------------------------------------------------------------------
Boolean
CPrintProgress::HandleCommand( const HICommandExtended& inCommand )
{
    switch ( inCommand.commandID )
    {
        // Add your own command-handling cases here
		case kCancelCmd:
			//Close();
			closeWhilePrinting();
			return true;
        
        default:
            return false;
    }
}

//--------------------------------------------------------------------------------------------
OSStatus
CPrintProgress::HandleEvent(
	EventHandlerCallRef		inRef,
	TCarbonEvent&			inEvent )
{
	OSStatus result = TWindow::HandleEvent(inRef, inEvent);
	
	switch ( inEvent.GetClass() )
	{
		case kEventClassCust:
			result = customEvent( inEvent );
			break;
	};

	return result;
}

//--------------------------------------------------------------------------------------------
OSStatus
CPrintProgress::customEvent(TCarbonEvent& inEvent)
{
	OSStatus result = noErr;
	switch ( inEvent.GetKind() )
	{
		case kEventDetectingMedia:
			SetEditTextText(m_textStatus, "Detecting Media...");
			SetControlValue(m_progressBar, 0);
			SetProgressIndicatorState(m_progressBar, false);
			break;
		case kEventMediaDetectionFinished:
			SetEditTextText(m_textStatus, "Detecting Media Finished");
			SetControlValue(m_progressBar, 100);
			break;
		case kEventQueryCancel:
			printf("kEventQueryCancel\n");
			break;
		case kEventPrepareProgress:
			prepareProgress();
			break;
		case kEventPrintProgress:
			printProgress();
			break;
		case kEventTimeEstimate:
			break;
		case kEventError:
			onError();
			break;
		case kEventPrintFinished:
			Close();
			break;
	}
	return result;
}

void CPrintProgress::onError() {
	LSError errorCode = m_flags.get_error();
	
	// Abort events are special - confirm the cancel and eject the tray. 
	if(errorCode == LS_E_Aborted) {
		MessageBox("Canceled\n\nPrint canceled");
		m_flags.set_should_try_print(false);	      
		try {
				LightScribe::DiscPrinter p = LSUtil::CreatePrinter(m_printOptions.driveIndex);
				p.OpenDriveTray();
		} catch(...) { }  // TODO: what does an error mean at this point?
	} //if(errorCode == LS_E_Aborted)
	else {
		// All other error events can use the dynamically generated error dialog
	
		// Generally, if a media error occurs the user will want to swap out or flip the disc
		if(ErrorUtil::isMediaError(errorCode))
			LSUtil::OpenTray(m_printOptions.driveIndex);
	
		ErrorUtil::ErrorAction exit_code = CErrorDialog::ShowError(errorCode);
	
		// Sepending on the user action, we may not want to close the tray. 
		bool shouldCloseTray = false;
	
		// Show the error dialog, and depending on what the user selected...
		switch(exit_code) {
			case ErrorUtil::tryAgainAction:
				m_flags.set_should_try_print(true);
				shouldCloseTray = true;
				break;
			
			case ErrorUtil::genericPrintAction:	
				m_printOptions.optLevel = LS_media_generic;
				m_flags.set_should_try_print(true);
				shouldCloseTray = true;
				break;
				
			case ErrorUtil::updateAction:
				LSUtil::GetUpdate();
				// pass through to cancel
			case ErrorUtil::cancelPrintAction:
				m_flags.set_should_try_print(false);	
				shouldCloseTray = false;
				break;
		}

		if(ErrorUtil::isMediaError(errorCode) && shouldCloseTray)
			LSUtil::CloseTray(m_printOptions.driveIndex);
	}
	m_flags.post();
}

void CPrintProgress::prepareProgress() {
	long current;
	long final;
	m_flags.get_prepare_progress(current, final);
	// There tends to be a delay between preparation and actual printing when the drive
	// has accepted some amount of data but hasn't started printing it yet.  We'll have
	// the UI show a busy status until we start getting real progress. 
	if(current == final) {
		SetEditTextText(m_textStatus, "Starting up drive...");
		SetControlValue(m_progressBar, 0);
		SetProgressIndicatorState(m_progressBar, false);
		
		_busyUpdate = true;
	}
	else {
		// normal preparation progress
		SetEditTextText(m_textStatus, "Preparing to print...");
		double done = (double)current / final;
		SetControlValue(m_progressBar, (int)(done * 100.0f));
		SetProgressIndicatorState(m_progressBar, true);
		_busyUpdate = false;
	}			
}

void CPrintProgress::printProgress() {
	long current;
	long final;
	m_flags.get_print_progress(current, final);
	printf("printProgress current = %d, final = %d\n", current, final);	
	
	// Don't consider the print actually started until we get two different
	// callbacks - many of one value won't cut it. 
	if(_lastProgress == -1 || current == _lastProgress) {
		_lastProgress = current;
		return;
	}

	double done = (double)current / final;
	SetControlValue(m_progressBar, (int)(done * 100.0f));
	SetProgressIndicatorState(m_progressBar, true);
	_busyUpdate = false;      

	// Make sure the progress is reset the first time we actually hit this callback
	if(! _progressInitialized) {
		_progressCalc = ProgressCalculator(final, m_flags.get_estimate_time());
		_progressInitialized = true;
	}
	_progressCalc.setCurrentValue(current);

	char buff[256];
	sprintf(buff, "Printing - %s", _progressCalc.getFuzzyEstimate().c_str());
	SetEditTextText(m_textStatus, buff);	
}

void CPrintProgress::closeWhilePrinting(){
	bool choice = Confirm("Really cancel?\n\nThis will cause the printed disc to be incomplete.  Are you sure you want to cancel the print?");
	if(choice) {
		m_flags.set_abort(true);
		SetEditTextText(m_textStatus, "Cancelling...");
		DisableControl(m_btnCancel);
	}

	// The actual closing will happen after the CheckAbort callback (seen here as the 
	// QueryCancel event) happens. 
}

EventLoopTimerUPP
CPrintProgress::GetTimerUPP()
{
	static EventLoopTimerUPP sUPP = NULL;
	
	if ( sUPP == NULL )
		sUPP = NewEventLoopTimerUPP( TimerHook );
	
	return sUPP;
}

pascal void
CPrintProgress::TimerHook( EventLoopTimerRef inTimerRef, void* userData )
{
	((CPrintProgress*)userData)->updateWhileBusy();
}

void CPrintProgress::updateWhileBusy()
{
	if(_busyUpdate) {
		SetControlValue(m_progressBar, 0);
		SetProgressIndicatorState(m_progressBar, false);
	}
}
