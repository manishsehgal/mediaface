#include <Carbon/Carbon.h>
#include "util/errorutil.h"

class CErrorDialog : public TWindow
{
	private:
		ErrorUtil::ErrorAction m_exit_code;
		ControlRef			m_textError;
		ControlRef			m_btnUpdateNow;
		ControlRef			m_btnUseGeneric;
		ControlRef			m_btnTryAgain;
		ControlRef			m_btnCancelPrint;
		
    private:
                            CErrorDialog(LSError errorCode);
        virtual             ~CErrorDialog() {}

	public:
		static ErrorUtil::ErrorAction
							ShowError(LSError errorCode);
        
    protected:
        virtual Boolean     HandleCommand( const HICommandExtended& inCommand );
		void				initControls();
};
