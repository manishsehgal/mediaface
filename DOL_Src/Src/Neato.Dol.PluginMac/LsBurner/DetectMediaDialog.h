#include <Carbon/Carbon.h>
#include "/usr/include/lightscribe.h"

class CDetectMediaDialog : public TWindow
{
    private:
                            CDetectMediaDialog();
        virtual             ~CDetectMediaDialog() {}

	public:
		static LSError 		getMediaInformation( 
										int driveIndex, 
										LS_MediaOptimizationLevel optLevel,
										LS_MediaInformation* info);
        
    protected:
        virtual Boolean     HandleCommand( const HICommandExtended& inCommand );
		
	private:
		static pthread_mutex_t		dialog_mutex;
};


