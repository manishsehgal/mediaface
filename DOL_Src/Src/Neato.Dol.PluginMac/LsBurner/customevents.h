#ifndef _CUSTOMEVENTS_H
#define _CUSTOMEVENTS_H
#include <semaphore.h>

//Custom event class
enum {
	kEventClassCust = 'cust'
};

enum {
	kEventParamAbort = 'abor'
};

enum {
	kEventCustomValue = 20000,
	kEventMediaDetectionFinished,
	kEventError,
	kEventPrepareProgress,
	kEventPrintProgress,
	kEventPrintFinished,
	kEventTimeEstimate,
	kEventQueryCancel,
	kEventDetectingMedia
};

class CSyncroFlags {
private:
	pthread_mutex_t mutex;
	bool _abort;
	long _current_prepare;
	long _final_prepare;
	long _current_print;
	long _final_print;
	time_t _estimate_time;
	bool _should_try_print;
	LSError _ls_error;
	MPSemaphoreID _locker;

public:
	CSyncroFlags()
	: _abort(false)	
	, _current_prepare(0)
	, _final_prepare(0)
	, _current_print(0)
	, _final_print(0)
	, _estimate_time(0)
	, _should_try_print(false)
	, _ls_error(LS_OK)
	{
		pthread_mutex_init(&mutex, NULL);
		MPCreateSemaphore(1, 0, &_locker);
	}
	
	bool get_abort() {
		bool abort;
		pthread_mutex_lock(&mutex);
		abort = _abort;
		pthread_mutex_unlock(&mutex);
		return abort;
	}

	void set_abort(bool abort) {
		pthread_mutex_lock(&mutex);
		_abort = abort;
		pthread_mutex_unlock(&mutex);
	}
	
	void get_prepare_progress(long& current, long& final) {
		pthread_mutex_lock(&mutex);
		current = _current_prepare;
		final = _final_prepare;
		pthread_mutex_unlock(&mutex);
	}
	
	void set_prepare_progress(long current, long final) {
		pthread_mutex_lock(&mutex);
		_current_prepare = current;
		_final_prepare = final;
		pthread_mutex_unlock(&mutex);
	}
	
	void get_print_progress(long& current, long& final) {
		pthread_mutex_lock(&mutex);
		current = _current_print;
		final = _final_print;
		pthread_mutex_unlock(&mutex);
	}
	
	void set_print_progress(long current, long final) {
		pthread_mutex_lock(&mutex);
		_current_print = current;
		_final_print = final;
		pthread_mutex_unlock(&mutex);
	}
	
	time_t get_estimate_time() {
		time_t estimate_time;
		pthread_mutex_lock(&mutex);
		estimate_time = _estimate_time;
		pthread_mutex_unlock(&mutex);
		return estimate_time;
	}

	void set_estimate_time(time_t estimate_time) {
		pthread_mutex_lock(&mutex);
		_estimate_time = estimate_time;
		pthread_mutex_unlock(&mutex);
	}
	
	bool get_should_try_print() {
		bool should_try_print;
		pthread_mutex_lock(&mutex);
		should_try_print = _should_try_print;
		pthread_mutex_unlock(&mutex);
		return should_try_print;
	}

	void set_should_try_print(bool should_try_print) {
		pthread_mutex_lock(&mutex);
		_should_try_print = should_try_print;
		pthread_mutex_unlock(&mutex);
	}
	
	LSError get_error() {
		LSError error;
		pthread_mutex_lock(&mutex);
		error = _ls_error;
		pthread_mutex_unlock(&mutex);
		return error;
	}

	void set_error(LSError error) {
		pthread_mutex_lock(&mutex);
		_ls_error = error;
		pthread_mutex_unlock(&mutex);
	}
	
	void wait() {
		MPWaitOnSemaphore(_locker, kDurationForever);
	}
	
	void post() {
		MPSignalSemaphore(_locker);
	}
};

#endif
