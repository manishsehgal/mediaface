#include "ErrorDialog.h"
#include "util/GuiUtils.h"

enum
{
	kUpdateNowCmd	= 'UPDA',
	kUseGenereicCmd	= 'GENE',
	kTryAgainCmd	= 'TRYA',
    kCancelPrintCmd = 'NPRN'
};

const ControlID kErrorText		= { 'TEXT', 1 };
const ControlID kUpdateNowBtn   = { 'BTN ', 2 };
const ControlID kUseGenereicBtn = { 'BTN ', 3 };
const ControlID kTryAgainBtn	= { 'BTN ', 4 };
const ControlID kCancelPrintBtn = { 'BTN ', 5 };

//--------------------------------------------------------------------------------------------
CErrorDialog::CErrorDialog(LSError errorCode) : TWindow( CFSTR("ErrorDialog") )
{
	initControls();
	
	// Make only the right buttons visible
	if(ErrorUtil::updateWillFix(errorCode))
		ShowControl(m_btnUpdateNow);
	else
		HideControl(m_btnUpdateNow);
	
	if(ErrorUtil::genericOK(errorCode))
		ShowControl(m_btnUseGeneric);
	else
		HideControl(m_btnUseGeneric);

	if(ErrorUtil::retryIsSensible(errorCode))
		ShowControl(m_btnTryAgain);
	else
		HideControl(m_btnTryAgain);
			
	// and set the error message
	SetEditTextText(m_textError, ErrorUtil::getMessage(errorCode));
}

//--------------------------------------------------------------------------------------------
void
CErrorDialog::initControls()
{
	GetControlByID( GetWindowRef(), &kErrorText,		&m_textError);	
	GetControlByID( GetWindowRef(), &kUpdateNowBtn,		&m_btnUpdateNow);
	GetControlByID( GetWindowRef(), &kUseGenereicBtn,	&m_btnUseGeneric);
	GetControlByID( GetWindowRef(), &kTryAgainBtn,		&m_btnTryAgain);
	GetControlByID( GetWindowRef(), &kCancelPrintBtn,	&m_btnCancelPrint);
}

//--------------------------------------------------------------------------------------------
ErrorUtil::ErrorAction
CErrorDialog::ShowError(LSError errorCode)
{
	CErrorDialog*	w = new CErrorDialog(errorCode);
	w->Show();
	RunAppModalLoopForWindow(w->GetWindowRef());
	w->Hide();
	ErrorUtil::ErrorAction exit_code = w->m_exit_code;
	delete w;
	
	return exit_code;
}


//--------------------------------------------------------------------------------------------
Boolean
CErrorDialog::HandleCommand( const HICommandExtended& inCommand )
{
    switch ( inCommand.commandID )
    {
        // Add your own command-handling cases here
		case kUpdateNowCmd:
			m_exit_code = ErrorUtil::updateAction;
			QuitAppModalLoopForWindow( GetWindowRef() );
			return true;
			
		case kUseGenereicCmd:
			m_exit_code = ErrorUtil::genericPrintAction;
			QuitAppModalLoopForWindow( GetWindowRef() );
			return true;
			
		case kTryAgainCmd:
			m_exit_code = ErrorUtil::tryAgainAction;
			QuitAppModalLoopForWindow( GetWindowRef() );
			return true;
			
		case kCancelPrintCmd:
			m_exit_code = ErrorUtil::cancelPrintAction;
			QuitAppModalLoopForWindow( GetWindowRef() );
			return true;
    
        default:
            return false;
    }
}

