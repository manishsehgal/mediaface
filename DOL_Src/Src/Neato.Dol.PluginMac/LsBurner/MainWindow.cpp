#include "MainWindow.h"
#include "Preview.h"
#include "PrintProgress.h"
#include "DetectMediaDialog.h"

enum
{
	kPreviewCmd		 = 'PREV',
    kDeviceSelectCmd = 'DEVS',
	kHelpCmd		 = 'HELP',
};

const ControlID 	kComboDrive            = { 'COMB', 10 };
const ControlID 	kTextStatus            = { 'TEXT', 11 };
const ControlID 	kTextDriveManufacture  = { 'TEXT', 12 };
const ControlID 	kTextDriveModel        = { 'TEXT', 13 };
const ControlID 	kTextDriveCapabilities = { 'TEXT', 14 };
const ControlID 	kTextDevicePath        = { 'TEXT', 15 };
const ControlID 	kTextPluginVersion     = { 'TEXT', 17 };
const ControlID 	kTextMediaDetails      = { 'TEXT', 16 };
const ControlID 	kRadioQuality          = { 'RADI', 19 };

//--------------------------------------------------------------------------------------------
CMainWindow::CMainWindow(const char* lsMode, const char* lsQuality, const char* lsSilent, const char* lsVer) 
: TWindow( CFSTR("MainWindow") )
, m_lsMode(lsMode)
{
	m_isMediaPresent = false;
	m_lsQuality = lsQuality[0] - '0' + 1;
	m_lsSilent = lsSilent[0] != '0';
	
	m_lsPluginVersion = lsVer;
	
	initControls();
	
	enumDrivers();
	
	if (m_driveArray.size() > 0) {
		bindComboControl();
		showDeviceInfo();
	}
}

//--------------------------------------------------------------------------------------------
void
CMainWindow::Create(const char * imagePath, const char* lsMode, const char* lsQuality, const char* lsSilent, const char* lsVer)
{
    CMainWindow* wind = new CMainWindow(lsMode, lsQuality, lsSilent, lsVer);

    // Position new windows in a staggered arrangement on the main screen
    RepositionWindow( *wind, NULL, kWindowCascadeOnMainScreen );
	
	ProcessSerialNumber psn;
	OSErr err = GetCurrentProcess(&psn);
	SetFrontProcess(&psn);
    
    // The window was created hidden, so show it
    wind->Show();	
	if (wind->m_bmpHelper.Load(imagePath) != noErr)
		ErrorBox("Can not load image");
		
	wind->showMediaInfo();
}

//--------------------------------------------------------------------------------------------
Boolean
CMainWindow::HandleCommand( const HICommandExtended& inCommand )
{
    switch ( inCommand.commandID )
    {
        // Add your own command-handling cases here
		case kPreviewCmd:
			CPreview::Create(printOptions(), m_isMediaPresent);
			return true;
			
		case kHICommandPrint:
			CPrintProgress::Create(printOptions());		
			return true;

		case kDeviceSelectCmd:
			showDeviceInfo();
			showMediaInfo();
			return true;
			
		case kHelpCmd:
			showHelp();
			return true;
        
        default:
            return false;
    }
}

//--------------------------------------------------------------------------------------------
OSStatus
CMainWindow::HandleEvent(
	EventHandlerCallRef		inRef,
	TCarbonEvent&			inEvent )
{
	OSStatus result = TWindow::HandleEvent(inRef, inEvent);
	
	switch ( inEvent.GetClass() )
	{
		case kEventClassWindow:
			switch ( inEvent.GetKind() )
			{
				case kEventWindowClose:
					QuitApplicationEventLoop();
					result = noErr;
					break;
			}
	};

	return result;
}

void CMainWindow::initControls() {
	GetControlByID( GetWindowRef(), &kComboDrive,            &m_comboDrive);	
	GetControlByID( GetWindowRef(), &kTextStatus, 		     &m_textStatus);
	GetControlByID( GetWindowRef(), &kTextDriveManufacture,  &m_textDriveManufacture);
	GetControlByID( GetWindowRef(), &kTextDriveModel,        &m_textDriveModel);
	GetControlByID( GetWindowRef(), &kTextDriveCapabilities, &m_textDriveCapabilities);
	GetControlByID( GetWindowRef(), &kTextDevicePath,        &m_textDevicePath);
	GetControlByID( GetWindowRef(), &kTextPluginVersion,     &m_textPluginVersion);
	GetControlByID( GetWindowRef(), &kTextMediaDetails,      &m_textMediaDetails);
	GetControlByID( GetWindowRef(), &kRadioQuality,          &m_radioQuality);

	resetTextFields();
	SetControlValue(m_radioQuality, m_lsQuality);
	
	MenuRef	menu;
	GetPopupButtonMenuRef( m_comboDrive, &menu );
	DeleteMenuItems(menu, 1, CountMenuItems(menu));
}

void CMainWindow::resetTextFields() {
	SetEditTextText(m_textStatus,            "");
    SetEditTextText(m_textDriveManufacture,  "");
    SetEditTextText(m_textDriveModel,        "");
    SetEditTextText(m_textDriveCapabilities, "");
    SetEditTextText(m_textDevicePath,        "");
    SetEditTextText(m_textPluginVersion,     "");
    SetEditTextText(m_textMediaDetails,      "");
}

void CMainWindow::enumDrivers() {
	LightScribe::DiscPrintMgr mgr;
	if(!mgr.isValid())
		return;
	
	LightScribe::EnumDiscPrinters e = mgr.EnumDiscPrinters();
	
	int count = e.Count();
	
	for(int i=0; i<count; ++i) {
		DriveInfo info;
		try {
			LightScribe::DiscPrinter p = e.Item(i);
			p.Validate();
			info.displayName = p.GetPrinterDisplayName();
			info.vendorName  = p.GetPrinterVendorName();
			info.path        = p.GetPrinterPath();
			info.productName = p.GetPrinterProductName();
			info.innerRadius = p.GetDriveInnerRadius();
			info.outerRadius = p.GetDriveOuterRadius();
			info.capabilities = p.GetPrinterCapabilities();
			m_driveArray.push_back(info);
		} catch (LightScribe::LSException& e) {
			SetEditTextText(m_textStatus, ErrorUtil::getMessage(e.GetCode()));
		}
	}
}

void CMainWindow::bindComboControl() {
	SetEditTextText( m_textStatus, "OK" );

    MenuRef	menu;
	GetPopupButtonMenuRef( m_comboDrive, &menu );
	int count = m_driveArray.size();

	for(int i=0; i<count; ++i) {
		try {
			CFStringRef	displayNameCF = CFStringCreateWithCString(NULL,
                                            m_driveArray[i].displayName.c_str(),
                                            GetApplicationTextEncoding());
			Str255      stringBuf;
			CFStringGetPascalString(displayNameCF, stringBuf, sizeof (stringBuf), GetApplicationTextEncoding());
			AppendMenuItemText(menu, stringBuf);
			CFRelease( displayNameCF ); 
		} catch (LightScribe::LSException& e) {
			SetEditTextText(m_textStatus, ErrorUtil::getMessage(e.GetCode()));
		}
	}
}

void CMainWindow::showDeviceInfo() {
	resetTextFields();
	
	int i = GetControlValue(m_comboDrive) - 1;
	const DriveInfo &info = m_driveArray[i];
	
	SetEditTextText(m_textDriveManufacture, info.vendorName.c_str());
	
	SetEditTextText(m_textDriveModel, info.productName.c_str());
	
	SetEditTextText(m_textDevicePath, info.path.c_str());
	
	SetEditTextText(m_textPluginVersion, m_lsPluginVersion.c_str());
	
	switch (info.capabilities) {
		case LS_label_monochrome:
			SetEditTextText(m_textDriveCapabilities, "Monochrome");
			break;
		case LS_label_color:
			SetEditTextText(m_textDriveCapabilities, "Color");
			break;
		default:
			SetEditTextText(m_textDriveCapabilities, "Color");
	}
}

void CMainWindow::showMediaInfo() {
	LS_MediaInformation mediaInfo;
	m_isMediaPresent = false;
	LSError localError = LS_OK;
	
	localError = CDetectMediaDialog::getMediaInformation(
			GetControlValue(m_comboDrive) - 1,
			LS_media_recognized,
			&mediaInfo);
			
	if (localError != LS_OK) {
		SetEditTextText(m_textMediaDetails, "Unable to read media");
	}
	
	m_isMediaPresent = mediaInfo.mediaPresent;
	if (!mediaInfo.mediaPresent) {
		SetEditTextText(m_textMediaDetails, "no disc");
		return; 
	}

	std::string strSize;
	switch(mediaInfo.shapeAndSize) 
	{
		case LS_label_12cm_circle:
			strSize = "Full size CD/DVD";
			break;
		case LS_label_8cm_circle:
			strSize = "Mini CD/DVD";
			break;
		case LS_label_unknown:
		default:
			strSize = "Unknown disc size";
			break;
	}


	char buff[256];
	sprintf(buff,
			"Disc Type: %s\n"
			"Manufacturer: #%d\n"
			"Label dimensions: %3.3fmm - %3.3fmm",
			strSize.c_str(),
			mediaInfo.manufacturer,
			mediaInfo.innerRadius/1000.0, mediaInfo.outerRadius/1000.0);
			
	SetEditTextText(m_textMediaDetails, buff);	
}

//! Create the print options structure that matches the current ui state
LSUtil::PrintOptions CMainWindow::printOptions() {
	LSUtil::PrintOptions opt;
	opt.driveIndex = GetControlValue(m_comboDrive) - 1;
  
	switch (GetControlValue(m_radioQuality)) {
	case 3:
		opt.quality = LS_quality_draft;
		break;
	case 2:
		opt.quality = LS_quality_normal;
		break;
	case 1:
		opt.quality = LS_quality_best;
		break;
	}
  
	if (m_lsMode == "full")
		opt.labelMode = LS_label_mode_full;
	else if (m_lsMode == "title")
		opt.labelMode = LS_label_mode_content;
	else if (m_lsMode == "title")
		opt.labelMode = LS_label_mode_title;
	else
		opt.labelMode = LS_label_mode_full;
      
	opt.optLevel = LS_media_recognized;

	opt.pBmpHelper = &m_bmpHelper;
	
	return opt;
}

void CMainWindow::showHelp() {
	CFURLRef fURL = CFBundleCopyResourceURL( CFBundleGetMainBundle(), CFSTR("LightscribeHelp.htm"), NULL, NULL );
	/*CFURLRef fBundle =  CFBundleCopyBundleURL(CFBundleGetMainBundle());
	CFURLRef fURL = CFURLCreateWithFileSystemPathRelativeToBase(NULL, CFSTR("../LightscribeHelp.htm"), 
		kCFURLPOSIXPathStyle, false, fBundle);*/
	LSOpenCFURLRef( fURL, NULL );
}