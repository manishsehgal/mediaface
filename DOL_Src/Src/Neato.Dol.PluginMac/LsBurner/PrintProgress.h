#include <Carbon/Carbon.h>
#include "TWindow.h"
#include "util/lsutil.h"
#include "PrintThread.h"
#include "customevents.h"
#include "util/progresscalculator.h"

class CPrintProgress : public TWindow
{
    public:
                            CPrintProgress();
        virtual             ~CPrintProgress();
        
        static void         Create(LSUtil::PrintOptions printOptions);
        
    protected:
        virtual Boolean		HandleCommand( const HICommandExtended& inCommand );
		virtual OSStatus	HandleEvent(
							EventHandlerCallRef		inCallRef,
							TCarbonEvent&			inEvent );
		
	private:
		PrintThread			m_printThread;
		EventLoopTimerRef	m_busyTimer;
		LSUtil::PrintOptions m_printOptions;
		
		ControlRef			m_textStatus;
		ControlRef			m_progressBar;
		ControlRef			m_btnCancel;
		CSyncroFlags		m_flags;
		//! Should the progress bar be periodically updated with the "busy" indicator?
		bool _busyUpdate;
		//! Store the last progress update we recieved. 
		/** 
		* -1 means we haven't gottent any.  This allows us to filter out redundant 
		* progress messages and determine when the print has actually started. 
		*/
		long _lastProgress;
		ProgressCalculator _progressCalc;
		//! Has the print progress calculator been initialized yet?
		bool _progressInitialized;
		
	private:
		void				initControls();
		void				closeWhilePrinting();
		OSStatus			customEvent(TCarbonEvent& inEvent);
		EventLoopTimerUPP	GetTimerUPP();
		static pascal void	TimerHook( EventLoopTimerRef inTimerRef, void* userData );
		void				updateWhileBusy();
		void				prepareProgress();
		void				printProgress();
		void				onError();		
};
