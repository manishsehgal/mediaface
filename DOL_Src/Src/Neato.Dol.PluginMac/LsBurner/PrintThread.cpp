#include <pthread.h>
#include <semaphore.h>
#include "PrintThread.h"

using namespace LightScribe;

class Callbacks{
public:
	static PrintThread* thread;
  
	/**
	 * @brief AbortLabel callback - return true if the print should be aborted. 
	 * The pattern used here is common to many of the callbacks, and to some 
	 * of the other event handling seen in the printing thread.  See the method body
	 * for furthur documentation
	 */
	static bool AbortLabel(void){
		bool shouldAbort = thread->GetFlags()->get_abort();
		return shouldAbort;
	}

	/**
	 * Report label preaparation progress - this posts an asynchronous event to the UI thread. 
	 */
	static void ReportPrepareProgress(long current, long final) {
		//printf("ReportPrepareProgress current = %d, final = %d\n", current, final);
		thread->GetFlags()->set_prepare_progress(current, final);
		TCarbonEvent event( kEventClassCust, kEventPrepareProgress );
		SendEventToEventTarget( event, thread->GetNotificationTarget() );
	}

	/**
	 * Report label progress - this posts an asynchronous event to the UI thread. 
	 */	
	static void ReportLabelProgress(long current, long final){
		//printf("ReportLabelProgress current = %d, final = %d\n", current, final);
		thread->GetFlags()->set_print_progress(current, final);
		TCarbonEvent event( kEventClassCust, kEventPrintProgress );
		SendEventToEventTarget( event, thread->GetNotificationTarget() );
	}

	/**
	 * We don't particularly care if the print is finished, since we use the return status 
	 * of PrintDisc() instead. 
	 */
	static void ReportFinished(LSError err) { }

	/**
	 * Report the estimated labeling time and see if the user wants to continue.  
	 * As it requires data back from the UI thread, this posts a synchronous event. 
	 */
	static bool ReportLabelTimeEstimate(long time) {
		printf("ReportLabelTimeEstimate time = %d\n", time);		
		thread->GetFlags()->set_estimate_time(time);
		TCarbonEvent event( kEventClassCust, kEventTimeEstimate );
		SendEventToEventTarget( event, thread->GetNotificationTarget() );
		
		bool shouldAbort = thread->GetFlags()->get_abort();
		return shouldAbort;
	}

	//! Create a callback structure approriate for passing to the api. 
	static LS_PrintCallbacks getStruct(){
		LS_PrintCallbacks cb;

		cb.AbortLabel = AbortLabel;
		cb.ReportPrepareProgress = ReportPrepareProgress;
		cb.ReportLabelProgress = ReportLabelProgress;
		cb.ReportFinished = ReportFinished;
		cb.ReportLabelTimeEstimate = ReportLabelTimeEstimate;

		return cb;
	}
};

PrintThread* Callbacks::thread;


PrintThread::PrintThread()
: _flags(NULL)
, _options(NULL)
{
}

void PrintThread::start(LSUtil::PrintOptions* options,  CSyncroFlags *flags, EventTargetRef notificationTarget) {
	_notificationTarget = notificationTarget;
	_options = options;
	_flags = flags;

	
	pthread_t thread = 0;
	pthread_create(&thread, NULL, PrintThread::PrintProc, this);
}

void * PrintThread::PrintProc(void * pArg)
{
	PrintThread * pThread = (PrintThread*)pArg;
    pThread->run();	
	pthread_exit(NULL);
	return NULL;
}

void PrintThread::run()
{
	// Printing happens in a retry loop.  This variable is passed to some synchronous event
	// handlers to allow the UI to trigger retries under certain error conditions.  A successful
	// print causes the loop to complete.
	GetFlags()->set_should_try_print(true); 
	while(GetFlags()->get_should_try_print()) {
		try {
			DiscPrinter printer = LSUtil::CreatePrinter(_options->driveIndex);
      
			// ensure the tray is closed
			printer.CloseDriveTray();
			      
			// probe for media - doing this before printing allows us 
			// to give more informative user feedback.
			SendEventToEventTarget( TCarbonEvent( kEventClassCust, kEventDetectingMedia ), _notificationTarget );
			LS_MediaInformation mediaInfo = printer.GetCurrentMedia(_options->optLevel);			
			SendEventToEventTarget( TCarbonEvent( kEventClassCust, kEventMediaDetectionFinished ), _notificationTarget );
									
			// Scope the session
			{
				DiscPrintSession session = printer.OpenPrintSession();
	
				// Set up the callbacks
				Callbacks::thread = this;
				LS_PrintCallbacks cb = Callbacks::getStruct();
				session.SetProgressCallback(&cb);
				
				// print!
				session.PrintDisc(LS_windows_bitmap, _options->labelMode, LS_draw_fit_smallest_to_label, 
								  _options->quality, _options->optLevel, 
								  _options->pBmpHelper->GetImageHeader(), _options->pBmpHelper->GetImageHeaderSize(), 
								  _options->pBmpHelper->GetImageData(), _options->pBmpHelper->GetImageDataSize());
			}
			

			// since we succeeded (having gotten this far) we don't need to retry
			GetFlags()->set_should_try_print(false);
			printer.OpenDriveTray();
			printf("printer.OpenDriveTray();\n");
		}
		catch (LSException& ex) {
			GetFlags()->set_error( ex.GetCode());
			printf("qq1\n");
				
			TCarbonEvent event( kEventClassCust, kEventError );
			event.SetParameter(kEventParamPostTarget, typeEventTargetRef,
								sizeof(_notificationTarget), &(_notificationTarget));
			event.PostToQueue(GetMainEventQueue());
			GetFlags()->wait();
		}

	} //while

	printf("Finish;\n");
	TCarbonEvent event( kEventClassCust, kEventPrintFinished );
	event.SetParameter(kEventParamPostTarget, typeEventTargetRef,
						sizeof(_notificationTarget), &(_notificationTarget));
	event.PostToQueue(GetMainEventQueue());
}

