#pragma once
#include <Carbon/Carbon.h>
#include "util/lsutil.h"
#include "customevents.h"

class PrintThread {
private:
	LSUtil::PrintOptions *_options;
	CSyncroFlags *_flags;
	EventTargetRef _notificationTarget;
	WindowRef _win;

	static void* PrintProc(void* pArg);
	void run();
	
public:
	PrintThread();
	void start(LSUtil::PrintOptions* options, CSyncroFlags *flags, EventTargetRef notificationTarget);
	inline EventTargetRef GetNotificationTarget() {return _notificationTarget;};
	inline CSyncroFlags*  GetFlags() {return _flags;};
};
