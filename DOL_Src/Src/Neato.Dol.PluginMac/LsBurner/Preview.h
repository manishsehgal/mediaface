#include <Carbon/Carbon.h>
#include "TWindow.h"
#include "util/lsutil.h"

class CPreview : public TWindow
{
    public:
                            CPreview(LSUtil::PrintOptions printOptions, bool isMediaInserted);
        virtual             ~CPreview() {}
        
        static void			Create(LSUtil::PrintOptions printOptions, bool isMediaInserted);

    protected:
        virtual Boolean     HandleCommand( const HICommandExtended& inCommand );
	
	private:
		HIViewRef			 fImageView;
		LSUtil::PrintOptions m_printOptions;
		
		bool updatePreview(bool isMediaInserted);
};
