#include <Carbon/Carbon.h>
#include "TWindow.h"

#include </usr/include/lightscribe_cxx.h>
#include </usr/include/lightscribe_errors.h>
#include "util/lsutil.h"

#include <vector>
#include <string>

class DriveInfo
{
public:
	std::string path;
	std::string vendorName;
	std::string displayName;
	std::string productName;
	long innerRadius;
	long outerRadius;
	LS_DeviceCapabilitiesEnum capabilities;
};

typedef std::vector<DriveInfo> DriveArray;

class CMainWindow : public TWindow
{
    public:
                            CMainWindow(const char* lsMode, const char* lsQuality, const char* lsSilent, const char* lsVer);
        virtual             ~CMainWindow() {}
        
        static void         Create(const char* imagePath, const char* lsMode, const char* lsQuality, const char* lsSilent, const char* lsVer);
        
    protected:
        virtual Boolean     HandleCommand( const HICommandExtended& inCommand );
		
		virtual OSStatus	HandleEvent(
							EventHandlerCallRef		inCallRef,
							TCarbonEvent&			inEvent );
	
	private:
		void initControls();
		void enumDrivers();
		void bindComboControl();
		void showDeviceInfo();
		void showMediaInfo();
		void showHelp();
		void resetTextFields();
		LSUtil::PrintOptions printOptions();
		
	private:
		ControlRef m_comboDrive;
		ControlRef m_textStatus;
		ControlRef m_textDriveManufacture;
		ControlRef m_textDriveModel;
		ControlRef m_textDriveCapabilities;
		ControlRef m_textDevicePath;
		ControlRef m_textPluginVersion;
		ControlRef m_textMediaDetails;
		ControlRef m_radioQuality;

		DriveArray m_driveArray;
		CBmpHelper m_bmpHelper;
		std::string m_lsMode;
		int m_lsQuality;
		bool m_lsSilent;
		std::string m_lsPluginVersion;
		bool m_isMediaPresent;
};