#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <map>
#include <zlib.h>

#include "curves.h"
#include "effects/base.h"

CCurves::CCurves()
{
}

CCurves::~CCurves()
{
}
 
xmlDoc * CCurves::GetContour(xmlNode * reqElem)
{
	try
	{
		xmlNode * paramsElem = CXmlHelper::GetChildElement(reqElem, "Params");
		if (paramsElem == NULL) throw 1;
		xmlNode * textElem = CXmlHelper::GetChildElement(paramsElem, "Text");
		if (textElem == NULL) throw 2;
		xmlNode * effDescElem = CXmlHelper::GetChildElement(paramsElem, "EffectDesc");
		if (effDescElem == NULL) throw 3;
		
		char * effType = (char*)xmlGetProp(effDescElem, BAD_CAST "Type");
		if (effType == NULL) throw 4;
		m_strEffType = effType;
		xmlFree(effType);
		
		char * effData = (char*)xmlNodeGetContent(effDescElem);
		if (effData == NULL) throw 5;
		m_strEffData = effData;
		xmlFree(effData);
		
		char * text = (char*)xmlNodeGetContent(textElem);
		if (text == NULL) throw 6;
		m_strText = text;
		xmlFree(text);
		
		m_strFont = "arial.ttf";
        m_nFontSize = 10;
		m_nAlign = 0;
		m_bUnderline = false;
        m_bUseLocalFont = true;
        
		xmlNode * styleElem = CXmlHelper::GetChildElement(textElem, "Style");
		if (styleElem != NULL)
		{
			char * font = (char*)xmlGetProp(styleElem, BAD_CAST "font");
			if (font != NULL)
			{
				m_strFont = font;
				xmlFree(font);
			}
			
			char * size = (char*)xmlGetProp(styleElem, BAD_CAST "size");
			if (size != NULL)
			{
				m_nFontSize = atoi(size);
				xmlFree(size);
			}
			
			char * align = (char*)xmlGetProp(styleElem, BAD_CAST "align");
			if (align != NULL)
			{
                if      (strcmp(align, "left")   == 0) m_nAlign = 0;
                else if (strcmp(align, "center") == 0) m_nAlign = 1;
                else if (strcmp(align, "right")  == 0) m_nAlign = 2;
				xmlFree(align);
			}
			
			char * underline = (char*)xmlGetProp(styleElem, BAD_CAST "underline");
			if (underline != NULL)
			{
                m_bUnderline = (strcmp(underline, "1") == 0);
				xmlFree(underline);
			}
			
			char * useLocal = (char*)xmlGetProp(styleElem, BAD_CAST "useLocal");
			if (useLocal != NULL)
			{
                m_bUseLocalFont = (strcmp(useLocal, "1") == 0);
				xmlFree(useLocal);
			}
		}
		
		//------------------------------------------------
		
		xmlDoc * respDoc = xmlNewDoc(BAD_CAST "1.0");
		if (respDoc == NULL) throw 7;
		xmlNode * respElem = xmlNewNode(NULL, BAD_CAST "Response");
		if (respElem == NULL) throw 8;
		xmlDocSetRootElement(respDoc, respElem);
		xmlSetProp(respElem, BAD_CAST "Error", BAD_CAST "0");
		xmlNode * paramsRElem = xmlNewChild(respElem, NULL, BAD_CAST "Params", NULL);
		if (paramsRElem == NULL) throw 9;
		xmlNode * statusElem = xmlNewChild(paramsRElem, NULL, BAD_CAST "Status", NULL);
		if (statusElem == NULL) throw 10;
		
		//------------------------------------------------
		
		std::string strContourXml;
		std::string strGeometryXml;
		int nRes = makeContour(strContourXml, strGeometryXml);
		
		if (nRes == 0)
		{
			xmlSetProp(statusElem, BAD_CAST "status", BAD_CAST "Ok");
			
			xmlDoc * contDoc = xmlReadMemory(strContourXml.c_str(), strContourXml.size(), NULL, NULL, 0);
			if (contDoc == NULL) throw 11;
			xmlNode * contElem = xmlDocGetRootElement(contDoc);
        	if (contElem == NULL) throw 12;
			xmlNode * contCopyElem = xmlCopyNode(contElem, 1);
			if (contCopyElem == NULL) throw 13;
			xmlAddChild(paramsRElem, contCopyElem);
			xmlFreeDoc(contDoc);
			
            if (strGeometryXml.size() > 0)
            {
				xmlDoc * geomDoc = xmlReadMemory(strGeometryXml.c_str(), strGeometryXml.size(), NULL, NULL, 0);
				if (geomDoc == NULL) throw 14;
				xmlNode * geomElem = xmlDocGetRootElement(geomDoc);
        		if (geomElem == NULL) throw 15;
				xmlNode * geomCopyElem = xmlCopyNode(geomElem, 1);
				if (geomCopyElem == NULL) throw 16;
				xmlAddChild(paramsRElem, geomCopyElem);
				xmlFreeDoc(geomDoc);
            }
			
		}
		else if (nRes == 1)
		{
			xmlSetProp(statusElem, BAD_CAST "status", BAD_CAST "NeedFont");
			xmlSetProp(statusElem, BAD_CAST "font", BAD_CAST m_strFont.c_str());
		}
		
		return respDoc;
	}
	catch (int nErr)
	{
		std::string strErrDesc = "Can't get contour.";
		std::string strResp = "<Response Error=\"1\" ErrorDesc=\"";
		strResp += strErrDesc;
		strResp +="\"></Response>";
		xmlDoc * respDoc = xmlReadMemory(strResp.c_str(), strResp.size(), NULL, NULL, 0);
		return respDoc;
	}
	return NULL;
}

int CCurves::makeContour(std::string & strContourXml, std::string & strGeometryXml)
{
    //check if ttf is exist
    std::string strFontFileName;
	if (getFullFontFileName(m_strFont, strFontFileName) == false) return 1;
	
    CTextRenderer tr;
    if (tr.IsFontAvailable(strFontFileName) == false) return 1;
    
    //=====================================
	//init variables describing contour limits
	CSegment::InitGlobalLimits();
	
	//create and init appropr. effect
    CEffectBase * pEff = CEffectBase::CreateEffect(m_strEffType);
    if (pEff == NULL || !pEff->Init(m_strEffData))
	{
        strContourXml = "<Contour/>"; 
        return 0;
    }
    
    tr.Init(m_strText, strFontFileName, m_nFontSize, m_bUnderline, m_nAlign);
    
    PathData data;
	pEff->ApplyEffect(tr, data, m_nAlign);
    std::string strGeometry;
    pEff->GetGeometry(strGeometry);
	delete pEff;
    
	//make contour xml
	std::string strXml;
	strXml += "<Contour>";
    
    /*
	char buf[256];
	int  p = 0;
	while (true)
	{
		if (p >= data.Types.size()) break;

		UInt8 type = data.Types[p];

		if (type == PointTypeStart)
		{
			sprintf(buf, "M:%.1f:%.1f:", data.Points[p].X, data.Points[p].Y);
			strXml += buf;
			p++;
		}
		else if (type == PointTypeLine)
		{
			sprintf(buf, "L:%.1f:%.1f:", data.Points[p].X, data.Points[p].Y);
			strXml += buf;
			p++;
		}
		else if (type == PointTypeCurve)
		{
			if (p+2 > data.Types.size()) break;
			sprintf(buf, "C:%.1f:%.1f:%.1f:%.1f:", data.Points[p].X, data.Points[p].Y, data.Points[p+1].X, data.Points[p+1].Y);
			strXml += buf;
			p += 2;
		}
		else
		{
			break;
		}
	}
	strXml += "E";
	*/
	
    std::string strContour;
    pathData2String(data, strContour);
    strXml += strContour;
    
	strXml += "</Contour>";
	strContourXml = strXml;
	
    //=====================================
	//make geometry xml
   	strGeometryXml.clear();
    if (strGeometry.size() > 0) {
        strGeometryXml += "<Geometry ";
        strGeometryXml += strGeometry;
        strGeometryXml += " />";
    }
    
	return 0;
}

#define FormatVERSION "1.00"
#define __max(a,b) (((a)>(b))?(a):(b))
#define __min(a,b) (((a)<(b))?(a):(b))

bool CCurves::pathData2String(PathData & data, std::string & strData)
{
	char buf[256];
	int p = 0;


	int radix = 36;

	// determine text parameters: number of lines nad maximum line length
	int txtLeng = m_strText.length();
	int lineCount = 0;
	int maxStrLength = 0;
	int pos = 0;
	int nextPos;
	do {
		nextPos = m_strText.find('\n',pos);
		if(nextPos == -1) 
			nextPos = txtLeng;
		lineCount++;
		maxStrLength = __max(maxStrLength,nextPos-pos);
		pos = nextPos+1;
	} while(pos < txtLeng);

	// get width and height of effect
	double effWidth  = CSegment::XMaxGlobal()-CSegment::XMinGlobal();
	double effHeight = CSegment::YMaxGlobal()-CSegment::YMinGlobal();
	if(effWidth <= 0)
		effWidth = 60;
	if(effHeight <= 0)
		effHeight = 40;

	// !!! IMPORTANT: scales should be set to powers of 2 to avoid inaccurities in flash,
	//     because flash silently rounds scales if movieclips to a multiple of 1/2^16
	// So we round values maxStrLength/effWidth*200 and maxStrLength/effWidth*200 to
	// a power of 2 (between 16 and 2^15):
	int scaleX = (int)pow(2,__max(4,__min(15,(int)ceil((double)log((double)maxStrLength/effWidth*200)/log(2.0f))))); 
	int scaleY = (int)pow(2,__max(4,__min(15,(int)ceil((double)log((double)maxStrLength/effWidth*200)/log(2.0f))))); 

	int x=0;
	int y=0;

#define XMLAppendWStr( str )  { strData += (str); strData += ":";}
#define XMLAppendInt( n )   _itoa(n,buf,radix);XMLAppendWStr(buf);
#define XMLAppendInt2( n ) {_itoa((n)<0?((-n)<<1)+1:(n)<<1,buf,radix);XMLAppendWStr(buf);} 
#define XMLAppendDX(new_x) {int newX=(int)(new_x+0.5); int dx=(newX)-x; XMLAppendInt2(dx); x=(newX);}
#define XMLAppendDY(new_y) {int newY=(int)(new_y+0.5); int dy=(newY)-y; XMLAppendInt2(dy); y=(newY);}
#define XMLAppendPoint( Point ) { PointF* pPoint = &(Point); XMLAppendDX(pPoint->X*scaleX); XMLAppendDY(pPoint->Y*scaleY); } 

	XMLAppendWStr("Ver");
	XMLAppendWStr(FormatVERSION);

	XMLAppendWStr("R");
	_itoa((int)(radix),buf,10); 
	XMLAppendWStr(buf);

	XMLAppendWStr("S");
	XMLAppendInt(scaleX);
	XMLAppendInt(scaleY);


	while (true)
	{
		if (p >= data.Types.size()) break;

		UInt8 type = data.Types[p];

		if (type == PointTypeStart)
		{
		    XMLAppendWStr("M");
		    XMLAppendPoint(data.Points[p++]);
			//sprintf(buf, "M:%.1f:%.1f:", data.Points[p].X, data.Points[p].Y);
			//strXml += buf;
			//p++;
		}
		else if (type == PointTypeLine)
		{
		    XMLAppendWStr("L");
		    XMLAppendPoint(data.Points[p++]);
			//sprintf(buf, "L:%.1f:%.1f:", data.Points[p].X, data.Points[p].Y);
			//strXml += buf;
			//p++;
		}
		else if (type == PointTypeCurve)
		{
			if (p+2 > data.Types.size()) break;
		    XMLAppendWStr("C");
		    XMLAppendPoint(data.Points[p++]);
		    XMLAppendPoint(data.Points[p++]);
			//if (p+2 > data.Types.size()) break;
			//sprintf(buf, "C:%.1f:%.1f:%.1f:%.1f:", data.Points[p].X, data.Points[p].Y, data.Points[p+1].X, data.Points[p+1].Y);
			//strXml += buf;
			//p += 2;
		}
		else
		{
			break;
		}
		
		/*
		if (p >= data.Count) break;

		BYTE type = data.Types[p] & (BYTE)PathPointTypePathTypeMask;

		switch (type)
        {
        case 0:
		    {
			    XMLAppendWStr(L"M");
			    XMLAppendPoint(data.Points[p++]);
                break;
		    }
        case 1:
		    {
			    XMLAppendWStr(L"L");
			    XMLAppendPoint(data.Points[p++]);
                break;
		    }
        case 2:
		    {
			    if (p+2 > data.Count) break;
			    XMLAppendWStr(L"A");
			    XMLAppendPoint(data.Points[p++]);
			    XMLAppendPoint(data.Points[p++]);
                break;
		    }
        case 3:
		    {
			    if (p+3 > data.Count) break;
			    XMLAppendWStr(L"B");
			    XMLAppendPoint(data.Points[p++]);
			    XMLAppendPoint(data.Points[p++]);
			    XMLAppendPoint(data.Points[p++]);
                break;
		    }
        default:
		    {
			    break;
		    }
        }
        */
	}

	strData += "E";
    return true;
}

char * CCurves::_itoa(int value, char * string, int radix)
{
	string[0] = 0;
	if (radix < 2 || radix > 36) return string;
	int p=0;
	unsigned int val = (unsigned int)value;
	if (radix == 10 && value < 0) {
		string[p++] = '-';
		val = (unsigned int)(-value);
	}
	if (val == 0) {
		string[p++] = '0';
	}
	static char sym[] = "0123456789abcdefghijklmnopqrstuvwxyz";
	while (val > 0) {
		string[p++] = sym[val % radix];
		val /= radix;
	}
	string[p] = 0;
	char * p0 = string + ((string[0] == '-') ? 1 : 0);
	char * p1 = string + p - 1;
	while (p0 < p1) {
		char t = *p0; *p0 = *p1; *p1 = t;
		p0++; p1--;
	}
	return string;
}

xmlDoc * CCurves::SetFont(xmlNode * reqElem)
{
	try
	{
		xmlNode * paramsElem = CXmlHelper::GetChildElement(reqElem, "Params");
		if (paramsElem == NULL) throw 1;
		
		xmlNode * fontElem = CXmlHelper::GetChildElement(paramsElem, "Font");
		if (fontElem == NULL) throw 2;
		
		char * name = (char*)xmlGetProp(fontElem, BAD_CAST "Name");
		if (name == NULL) throw 3;
		
		char * fontBin = (char*)xmlNodeGetContent(fontElem);
		if (fontBin == NULL) throw 4;
		
		if (!saveFont(name, fontBin)) throw 5;

		xmlFree(fontBin);
		xmlFree(name);
		
		xmlDoc * respDoc = xmlNewDoc(BAD_CAST "1.0");
		if (respDoc == NULL) throw 6;
		xmlNode * respElem = xmlNewNode(NULL, BAD_CAST "Response");
		if (respElem == NULL) throw 7;
		xmlDocSetRootElement(respDoc, respElem);
		xmlSetProp(respElem, BAD_CAST "Error", BAD_CAST "0");
		xmlNode * paramsRElem = xmlNewChild(respElem, NULL, BAD_CAST "Params", NULL);
		if (paramsRElem == NULL) throw 8;

		return respDoc;
	}
	catch (int nErr)
	{
		std::string strErrDesc = "Can't save font.";
		std::string strResp = "<Response Error=\"1\" ErrorDesc=\"";
		strResp += strErrDesc;
		strResp +="\"></Response>";
		xmlDoc * respDoc = xmlReadMemory(strResp.c_str(), strResp.size(), NULL, NULL, 0);
		return respDoc;
	}
	return NULL;
}

bool CCurves::saveFont(const char * name, const char * data)
{
	//-------------------------------------------
	//Make full font file name
	
	std::string strFontName(name);
	std::string strFileName;
	if (getFullFontFileName(strFontName, strFileName) == false) return false;
	
	//-------------------------------------------
	//Un-base64
	
    static char TableBase64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	unsigned long dwSizeB = strlen(data);
	if (dwSizeB == 0) return false;
	if (dwSizeB % 4 != 0) return false;

	unsigned long dwSizeC = dwSizeB / 4 * 3;
	unsigned char * pBufC = new unsigned char[dwSizeC];
	std::map<char, int> chars;
	for (int j=0; j<64; j++)
	{
		chars[TableBase64[j]] = j;
	}
    
    unsigned long i=0;
    unsigned long c=0;
	for (; i<dwSizeB; i+=4)
	{
		int  nInSym  = 4;
		int  nOutSym = 3;
		if (data[i+2] == '=' && data[i+3] == '=')
		{
			nInSym    = 2;
			nOutSym   = 1;
		}
		else if (data[i+3] == '=')
		{
			nInSym    = 3;
			nOutSym   = 2;
		}

		int dwQ = (chars[data[i]]<<18) | (chars[data[i+1]]<<12);
		if (nInSym > 2)
		{
			dwQ |= chars[data[i+2]]<<6;
		}
		if (nInSym > 3)
		{
			dwQ |= chars[data[i+3]];
		}

		pBufC[c++] = (unsigned char)(dwQ >> 16);
		if (nOutSym > 1)
		{
			pBufC[c++] = (unsigned char)(dwQ >> 8);
		}
		if (nOutSym > 2)
		{
			pBufC[c++] = (unsigned char)dwQ;
		}
	}
	dwSizeC = c;

	//-------------------------------------------
	//Un-zip data
	
	unsigned long dwSize = dwSizeC * 20;
	unsigned char * pBuf = new unsigned char[dwSize];
	int dwRes = uncompress(pBuf, &dwSize, pBufC, dwSizeC);
	delete[] pBufC;
	if (dwRes != 0) return false;

	//Save file
	//-------------------------------------------
	
	int fd = open(strFileName.c_str(), O_RDWR|O_CREAT, 0644);
	if (fd < 0) return false;
	unsigned long wr = write(fd, pBuf, dwSize);
	close(fd);
	if (wr != dwSize) return false;
	
	delete[] pBuf;
	
	return true;
}

bool CCurves::getFullFontFileName(std::string & strFontName, std::string & strFileName)
{
	std::string strFontsDir;
	if (checkFontsDir(strFontsDir) == false) return false;
	strFileName = strFontsDir;
	strFileName += "/";
	strFileName += strFontName;
	return true;
}

bool CCurves::checkFontsDir(std::string & strDir)
{
	std::string dir = getenv("HOME");
	if (dir.size() == 0) return false;
	if (dir[dir.size()-1] == '/') dir.erase(dir.size()-1);
	if (checkOneDir(dir, false) == false) return false;
	
	dir += "/Library";
	if (checkOneDir(dir) == false) return false;
	
	dir += "/MfoPlugins";
	if (checkOneDir(dir) == false) return false;
	
	dir += "/Fonts";
	if (checkOneDir(dir) == false) return false;
	
	strDir = dir;
	return true;
}

bool CCurves::checkOneDir(std::string & strDir, bool tryCreate)
{
	DIR * dir = opendir(strDir.c_str());
	if (dir == NULL && tryCreate == true)
	{
		int fd = mkdir(strDir.c_str(), 0755);
		if (fd != 0) return false;
		dir = opendir(strDir.c_str());
	}
	//printf("check %s: %s\n", strDir.c_str(), dir == NULL ? "Failed" : "Ok");
	if (dir != NULL) closedir(dir);
	return (dir != NULL);
}
 