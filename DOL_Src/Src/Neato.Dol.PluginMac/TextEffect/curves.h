#ifndef _CURVES_H_
#define _CURVES_H_

#include <string>
#include "../Common/xmlhelper.h"
#include "textrenderer.h"


class CCurves
{
public:
	CCurves();
	~CCurves();
	
private:
	std::string m_strText;

	std::string m_strFont;
    int         m_nFontSize;
    bool        m_bUseLocalFont;
    int         m_nAlign;
    bool        m_bUnderline;

	std::string m_strEffType;
	std::string m_strEffData;
	
public:
	xmlDoc * GetContour(xmlNode * reqElem);
	xmlDoc * SetFont(xmlNode * reqElem);
	
private:
	int  makeContour(std::string & strContourXml, std::string & strGeometryXml);
    bool pathData2String(PathData & data, std::string & strData);
    char *_itoa( int value, char *string, int radix );
	bool saveFont(const char * name, const char * data);
	bool getFullFontFileName(std::string & strFontName, std::string & strFileName);
	bool checkFontsDir(std::string & strDir);
	bool checkOneDir(std::string & strDir, bool tryCreate = true);
};


#endif //_CURVES_H_
