#include "circle.h"

bool CEffectCircle::Init(std::string strData)
{
    const char * pE = strData.c_str() + strData.size();
	const char * p0 = strData.c_str();
	char * wcStop = 0;
	float pCoords[4];

	for (int i=0; i<4; i++)
	{
		pCoords[i] = (Float32)strtod(p0, &wcStop);
		p0 = std::find(p0, pE, ':');
		if (p0 == pE) return 0;
		p0++;
	}

    Float32 fX = pCoords[0];
    Float32 fY = pCoords[1];
    Float32 fW = pCoords[2];
    Float32 fH = pCoords[3];
    if(fH < 0)
        fY-=fH;
    m_fWidth = fW;
    CSegment * pSeg = 0;

	pSeg = m_ContourGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX, fY + 0.5f * fH));
	pSeg->setRefPoint(1, PointF(fX, fY + 0.225f * fH));
	pSeg->setRefPoint(2, PointF(fX + 0.225f * fW, fY));
	pSeg->setRefPoint(3, PointF(fX + 0.5f * fW, fY));

	pSeg = m_ContourGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX + 0.5f * fW, fY));
	pSeg->setRefPoint(1, PointF(fX + 0.775f * fW, fY));
	pSeg->setRefPoint(2, PointF(fX + fW, fY + 0.225f * fH));
	pSeg->setRefPoint(3, PointF(fX + fW, fY + 0.5f * fH));

	pSeg = m_ContourGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX + fW, fY + 0.5f * fH));
	pSeg->setRefPoint(1, PointF(fX + fW, fY + 0.775f * fH));
	pSeg->setRefPoint(2, PointF(fX + 0.775f * fW, fY + fH));
	pSeg->setRefPoint(3, PointF(fX + 0.5f * fW, fY + fH));

	pSeg = m_ContourGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX + 0.5f * fW, fY + fH));
	pSeg->setRefPoint(1, PointF(fX + 0.225f * fW, fY + fH));
	pSeg->setRefPoint(2, PointF(fX, fY + 0.775f * fH));
	pSeg->setRefPoint(3, PointF(fX, fY + 0.5f * fH));

	return true;
}

void CEffectCircle::ApplyEffect(CTextRenderer & tr, PathData & dataOut, int nAlign)
{
    float fWidth = m_ContourGuide.length();
	if (fWidth == 0.0f) return;
	
    tr.AddTextToPath(dataOut, fWidth);
    
	PointF minPt, maxPt;
	dataOut.bounds(minPt, maxPt);
	
	for (int i=0; i<dataOut.Points.size(); i++)
	{
		PointF & pt = dataOut.Points[i];
		pt.X += nAlign * fWidth / 4.0f;
		pt.Y += -minPt.Y - 0.5f * (maxPt.Y-minPt.Y);
		pt = m_ContourGuide.norm(pt.X) * pt.Y + m_ContourGuide.point(pt.X);
	}
}