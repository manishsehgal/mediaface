#include "guided.h"

bool CEffectGuided::Init(std::string strData)
{
	int nPos = 0;

	CSegment * pSeg = m_Guide.AddSegment(strData[nPos]);
	if (!pSeg) return false;
	nPos += 2;
	int nCnt = pSeg->init(strData.c_str() + nPos);
	if (!nCnt) return false;
	nPos += nCnt;

	return true;
}

void CEffectGuided::ApplyEffect(CTextRenderer & tr, PathData & dataOut, int nAlign)
{
	float fWidth = m_Guide.length();
	if (fWidth <= 0.0) return;
	
    tr.AddTextToPath(dataOut, fWidth);
	
	PointF minPt, maxPt;
	dataOut.bounds(minPt, maxPt);
    
    Float32 fX0 = 0.0f;
    switch (nAlign)
    {
    case 0: fX0 = 0.0f; break;
    case 1: fX0 = 0.5f * fWidth - (minPt.X + 0.5f * (maxPt.X-minPt.X)); break;
    case 2: fX0 = fWidth - (minPt.X + (maxPt.X-minPt.X)); break;
    default: break;
    }
    
	for (int i=0; i<dataOut.Points.size(); i++)
	{
		PointF & pt = dataOut.Points[i];
		pt.X += fX0;
		pt.Y += -minPt.Y - 0.5f * (maxPt.Y-minPt.Y);
		pt = m_Guide.norm(pt.X) * pt.Y + m_Guide.point(pt.X);
	}
}
