#ifndef _EFFECT_CIRCLE_H_
#define _EFFECT_CIRCLE_H_

#include "base.h"


class CEffectCircle : public CEffectBase
{
protected:
	CCurveDescriptor m_ContourGuide;
    Float32 m_fWidth;
public:
    CEffectCircle() {m_fWidth = 0.0f;}
	virtual bool Init(std::string strData);
	virtual void ApplyEffect(CTextRenderer & tr, PathData & dataOut, int nAlign);
};



#endif //_EFFECT_CIRCLE_H_

