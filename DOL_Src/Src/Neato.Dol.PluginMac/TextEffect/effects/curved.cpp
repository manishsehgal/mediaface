#include "curved.h"


bool CEffectCurved::Init(std::string strData)
{
	int nPos = 0;

	CSegment * pSeg1 = m_TopGuide.AddSegment(strData[nPos]);
	if (!pSeg1) return false;
	nPos += 2;
	int nCnt = pSeg1->init(strData.c_str() + nPos);
	if (!nCnt) return false;
	nPos += nCnt;

	CSegment * pSeg2 = m_BottomGuide.AddSegment(strData[nPos]);
	if (!pSeg2) return false;
	nPos += 2;
	nCnt = pSeg2->init(strData.c_str() + nPos);
	if (!nCnt) return false;
	nPos += nCnt;

	return true;
}
 
void CEffectCurved::ApplyEffect(CTextRenderer & tr, PathData & dataOut, int nAlign)
{
    tr.AddTextToPath(dataOut, -1.0f);
    
    dataOut.normalize();
    dataOut.scale();
    
	Float32 fTopL    = m_TopGuide.length();
	Float32 fBottomL = m_BottomGuide.length();
	for (int i=0; i<dataOut.Points.size(); i++)
	{
		PointF & pt = dataOut.Points[i];

		PointF ptTop(m_TopGuide.point(pt.X * fTopL));
		PointF ptBottom(m_BottomGuide.point(pt.X * fBottomL));

		PointF vecN(ptBottom - ptTop);
		pt = vecN * pt.Y + ptTop;
	}
} 