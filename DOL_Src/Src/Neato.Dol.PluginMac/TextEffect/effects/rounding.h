#ifndef _EFFECT_ROUNDING_H_
#define _EFFECT_ROUNDING_H_

#include "base.h"

class CEffectRounding : public CEffectBase
{
protected:
    PointF      m_pt0;
    PointF      m_pt1;
    std::string m_strGeometry;
    bool m_bInverse;
	
public:
	CEffectRounding(bool bInverse);
	
public:
	virtual bool Init(std::string strData);
	virtual void ApplyEffect(CTextRenderer & tr, PathData & dataOut, int nAlign);
    virtual void GetGeometry(std::string & strGeometry);

protected:
    static PointF Polar2Cart(Float32 ro, Float32 fi);
};

#endif //_EFFECT_ROUNDING_H_
