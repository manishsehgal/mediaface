#include "button.h"

CEffectButton::CEffectButton()
{
	m_fWidth = 0.0f;
}

bool CEffectButton::Init(std::string strData)
{
    const char * pE = strData.c_str() + strData.size();
	const char * p0 = strData.c_str();
	char * wcStop = 0;
	Float32 pCoords[4];
    
    int i;
	for (i=0; i<4; i++)
	{
		pCoords[i] = (Float32)strtod(p0, &wcStop);
		p0 = std::find(p0, pE, ':');
		if (p0 == pE) return 0;
		p0++;
	}

    Float32 fX = pCoords[0];
    Float32 fY = pCoords[1];
    Float32 fW = pCoords[2];
    Float32 fH = pCoords[3];
    m_fWidth = fW;
    CSegment * pSeg = 0;

	pSeg = m_TopGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX, fY + 0.5f * fH));
	pSeg->setRefPoint(1, PointF(fX, fY + 0.225f * fH));
	pSeg->setRefPoint(2, PointF(fX + 0.225f * fW, fY));
	pSeg->setRefPoint(3, PointF(fX + 0.5f * fW, fY));
	pSeg = m_TopGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX + 0.5f * fW, fY));
	pSeg->setRefPoint(1, PointF(fX + 0.775f * fW, fY));
	pSeg->setRefPoint(2, PointF(fX + fW, fY + 0.225f * fH));
	pSeg->setRefPoint(3, PointF(fX + fW, fY + 0.5f * fH));

	pSeg = m_CenterGuide.AddSegment(cstLine);
	pSeg->setRefPoint(0, PointF(fX, fY + 0.5f * fH));
	pSeg->setRefPoint(1, PointF(fX + fW, fY + 0.5f * fH));

	pSeg = m_BottomGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX, fY + 0.5f * fH));
	pSeg->setRefPoint(1, PointF(fX, fY + 0.775f * fH));
	pSeg->setRefPoint(2, PointF(fX + 0.225f * fW, fY + fH));
	pSeg->setRefPoint(3, PointF(fX + 0.5f * fW, fY + fH));
	pSeg = m_BottomGuide.AddSegment(cstBezier);
	pSeg->setRefPoint(0, PointF(fX + 0.5f * fW, fY + fH));
	pSeg->setRefPoint(1, PointF(fX + 0.775f * fW, fY + fH));
	pSeg->setRefPoint(2, PointF(fX + fW, fY + 0.775f * fH));
	pSeg->setRefPoint(3, PointF(fX + fW, fY + 0.5f * fH));

	return true;
}

void CEffectButton::ApplyEffect(CTextRenderer & tr, PathData & dataOut, int nAlign)
{
	int nParaCount = tr.GetParaCount();
	if (nParaCount > 0)
	{
		PathData path;
		tr.AddParaToPath(0, path, m_TopGuide.length(), false, NULL);
	    GuidePath(path, m_TopGuide, nAlign);
		dataOut.addpath(path);
	}
	if (nParaCount > 1)
	{
		PathData path;
		tr.AddParaToPath(1, path, m_CenterGuide.length(), false, NULL);
	    GuidePath(path, m_CenterGuide, nAlign);
		dataOut.addpath(path);
	}
	if (nParaCount > 2)
	{
		PathData path;
		tr.AddParaToPath(2, path, m_BottomGuide.length(), false, NULL);
	    GuidePath(path, m_BottomGuide, nAlign);
		dataOut.addpath(path);
	}
}

void CEffectButton::GuidePath(PathData & path, CCurveDescriptor & guide, int nAlign)
{
    float fWidth = guide.length();
	if (fWidth == 0.0f) return;
	
	PointF minPt, maxPt;
	path.bounds(minPt, maxPt);
	
	for (int i=0; i<path.Points.size(); i++)
	{
		PointF & pt = path.Points[i];
		pt.X += nAlign * fWidth / 2.0f;
		pt.Y += -minPt.Y - 0.5f * (maxPt.Y-minPt.Y);
		pt = guide.norm(pt.X) * pt.Y + guide.point(pt.X);
	}
}
