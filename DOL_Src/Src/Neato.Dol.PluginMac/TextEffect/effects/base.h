#ifndef _EFFECT_BASE_H_
#define _EFFECT_BASE_H_

#include <string>
#include "../curvedescriptor.h"
#include "../textrenderer.h"


class CEffectBase
{
public:
    static CEffectBase * CreateEffect(std::string & strType);

public:
	virtual bool Init(std::string strData)=0;
	virtual void ApplyEffect(CTextRenderer & tr, PathData & dataOut, int nAlign)=0;
    virtual void GetGeometry(std::string & strGeometry)
    {
        strGeometry.clear();
    }
};


#endif //_EFFECT_BASE_H_
