#include "guidedfit.h"

bool CEffectGuidedFit::Init(std::string strData)
{
	int nPos = 0;

	CSegment * pSeg = m_Guide.AddSegment(strData[nPos]);
	if (!pSeg) return false;
	nPos += 2;
	int nCnt = pSeg->init(strData.c_str() + nPos);
	if (!nCnt) return false;
	nPos += nCnt;

	return true;
}

void CEffectGuidedFit::ApplyEffect(CTextRenderer & tr, PathData & dataOut, int nAlign)
{
    float fWidth = m_Guide.length();
	if (fWidth == 0.0f) return;
	
    tr.AddTextToPath(dataOut, fWidth);
	
	PointF minPt, maxPt;
	dataOut.bounds(minPt, maxPt);
	
	for (int i=0; i<dataOut.Points.size(); i++)
	{
		PointF & pt = dataOut.Points[i];
		pt.X *= fWidth / (maxPt.X-minPt.X);
		pt.X += (Float32)nAlign * fWidth / 2.0f;
		pt.Y += -minPt.Y - (maxPt.Y-minPt.Y) / 2.0f;
		pt = m_Guide.norm(pt.X) * pt.Y + m_Guide.point(pt.X);
	}
}
