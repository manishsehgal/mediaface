#ifndef _EFFECT_GUIDED_H_
#define _EFFECT_GUIDED_H_

#include "base.h"

class CEffectGuided : public CEffectBase
{
protected:
	CCurveDescriptor m_Guide;
public:
	virtual bool Init(std::string strData);
	virtual void ApplyEffect(CTextRenderer & tr, PathData & dataOut, int nAlign);
};

#endif //_EFFECT_GUIDED_H_
