#ifndef _EFFECT_CURVED_H_
#define _EFFECT_CURVED_H_

#include "base.h"


class CEffectCurved : public CEffectBase
{
protected:
	CCurveDescriptor m_TopGuide, m_BottomGuide;
public:
	virtual bool Init(std::string strData);
	virtual void ApplyEffect(CTextRenderer & tr, PathData & dataOut, int nAlign);
};


#endif //_EFFECT_CURVED_H_

