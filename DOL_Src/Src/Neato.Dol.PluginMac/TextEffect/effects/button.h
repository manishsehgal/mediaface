#ifndef _EFFECT_BUTTON_H_
#define _EFFECT_BUTTON_H_

#include "base.h"


class CEffectButton : public CEffectBase
{
protected:
	CCurveDescriptor m_TopGuide, m_CenterGuide, m_BottomGuide;
    Float32 m_fWidth;
public:
	CEffectButton();
	virtual bool Init(std::string strData);
	virtual void ApplyEffect(CTextRenderer & tr, PathData & dataOut, int nAlign);
protected:
    void GuidePath(PathData & path, CCurveDescriptor & guide, int nAlign);
};


#endif //_EFFECT_BUTTON_H_

