#ifndef _EFFECT_GUIDEDFIT_H_
#define _EFFECT_GUIDEDFIT_H_

#include "base.h"

class CEffectGuidedFit : public CEffectBase
{
protected:
	CCurveDescriptor m_Guide;
public:
	virtual bool Init(std::string strData);
	virtual void ApplyEffect(CTextRenderer & tr, PathData & dataOut, int nAlign);
};


#endif //_EFFECT_GUIDEDFIT_H_
