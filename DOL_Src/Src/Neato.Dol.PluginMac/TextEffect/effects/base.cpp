#include "base.h"
#include "curved.h"
#include "button.h"
#include "circle.h"
#include "guided.h"
#include "guidedfit.h"
#include "rounding.h"

CEffectBase * CEffectBase::CreateEffect(std::string & strType)
{
	printf("CreateEffect: %s\n", strType.c_str());
	if (strcmp(strType.c_str(), "Curved") == 0) {
		return new CEffectCurved();
	}
	else if (strcmp(strType.c_str(), "Button") == 0) {
		return new CEffectButton();
	}
	else if (strcmp(strType.c_str(), "Circle") == 0) {
		return new CEffectCircle();
	}
	else if (strcmp(strType.c_str(), "Guided") == 0) {
		return new CEffectGuided();
	}
	else if (strcmp(strType.c_str(), "GuidedFit") == 0) {
		return new CEffectGuidedFit();
	}
	else if (strcmp(strType.c_str(), "Rounding") == 0) {
		return new CEffectRounding(false);
	}
	else if (strcmp(strType.c_str(), "Rounding2") == 0) {
		return new CEffectRounding(true);
	}
	return NULL;
}


