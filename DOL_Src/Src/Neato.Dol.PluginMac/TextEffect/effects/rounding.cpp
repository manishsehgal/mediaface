#include "rounding.h"

CEffectRounding::CEffectRounding(bool bInverse)
{
   m_bInverse = bInverse;
}

bool CEffectRounding::Init(std::string strData)
{
    int nPos = 0;
    CCurveDescriptor cd;
    CSegment * pLine = cd.AddSegment(strData[nPos]);
    if (!pLine) return false;
    nPos += 2;
    int nCnt = pLine->init(strData.c_str() + nPos);
    if (!nCnt) return false;
    nPos += nCnt;
    if (pLine->refCount() != 2) return false;
    m_pt0 = (*pLine)[0];
    m_pt1 = (*pLine)[1];

    return true;
}

PointF CEffectRounding::Polar2Cart(Float32 ro, Float32 fi)
{
    return PointF(ro * cos(fi), -ro * sin(fi));
}

void CEffectRounding::ApplyEffect(CTextRenderer & tr, PathData & dataOut, int nAlign)
{
    //draw text in rectangle
    tr.AddTextToPath(dataOut, -1.0f);
    
    // rotate path on 180 deg for inversed effect type
    if (m_bInverse == true) {
		for (int i=0; i<dataOut.Points.size(); i++) {
			PointF & pt = dataOut.Points[i];
			pt.X = -pt.X;
			pt.Y = -pt.Y;
		}
    }
    
    // get the rectangle width and height
	PointF minPt, maxPt;
	dataOut.bounds(minPt, maxPt);
    float fW = maxPt.X - minPt.X;
    if (fabs(fW) < 1e-6f) return;
    float fH = maxPt.Y - minPt.Y;
    if (fabs(fH) < 1e-6f) return;

	//normalize path and scale path
    dataOut.normalize();
    dataOut.scale();

    //translate center point of the rectangle to polar coords (ro, fi)
    PointF pt = m_pt1 - m_pt0; // m_pt0 is in (0,0)
    pt.Y = -pt.Y;
    Float32 pi = 3.1415926f;
    Float32 ro = sqrt(pt.X * pt.X + pt.Y * pt.Y); // radius
    Float32 minro = fH / 2.0f + fH * 0.01f;
    if (ro < minro) {
        ro = minro;
    }
    Float32 fi = 0.0f; //angle
    if (fabs(pt.X) > 1e-6f) {
        fi = atan2f(pt.Y, pt.X);
    } else {
        if (pt.Y > 0.0f) {
            fi = pi / 2.0f;
        } else {
            fi = -pi / 2.0f;
        }
    }

    // inner and outer radiuses of text shape
    Float32 ro0 = ro - fH / 2.0f;
    Float32 ro1 = ro0 + fH;
    //full angle of text shape
    Float32 angle = fW / ro;
    if (angle > 1.99f*pi) angle = 1.99f*pi;
    // start and end angles of text shape
    Float32 fi0 = fi - angle / 2.0f;
    Float32 fi1 = fi0 + angle;

	PointF m_Frame[6];
    // points of inner arc in cartesian
    m_Frame[0] = Polar2Cart(ro0, fi0) + m_pt0;
    m_Frame[1] = Polar2Cart(ro0, fi)  + m_pt0;
    m_Frame[2] = Polar2Cart(ro0, fi1) + m_pt0;

    // points of outer arc in cartesian
    m_Frame[3] = Polar2Cart(ro1, fi0) + m_pt0;
    m_Frame[4] = Polar2Cart(ro1, fi)  + m_pt0;
    m_Frame[5] = Polar2Cart(ro1, fi1) + m_pt0;

    // inner guide
	CCurveDescriptor innerGuide;
	CSegment * pArc0 = innerGuide.AddSegment(cstArc);
    pArc0->setRefPoint(0, m_Frame[0]);
    pArc0->setRefPoint(1, m_Frame[1]);
    pArc0->setRefPoint(2, m_Frame[2]);

    // outer guige
	CCurveDescriptor outerGuide;
	CSegment * pArc1 = outerGuide.AddSegment(cstArc);
    pArc1->setRefPoint(0, m_Frame[3]);
    pArc1->setRefPoint(1, m_Frame[4]);
    pArc1->setRefPoint(2, m_Frame[5]);

    // transform path points
	Float32 fTopL    = innerGuide.length();
	Float32 fBottomL = outerGuide.length();
	for (int i=0; i<dataOut.Points.size(); i++)
	{
		PointF & pt = dataOut.Points[i];

		PointF ptTop(innerGuide.point(pt.X * fTopL));
		PointF ptBottom(outerGuide.point(pt.X * fBottomL));

		PointF vecN(ptBottom - ptTop);
		pt = vecN * pt.Y + ptTop;
	}

    char buf[256];
    sprintf(buf, "Width=\"%.2f\" Height=\"%.2f\" Radius=\"%.2f\" Alpha=\"%.2f\" Beta=\"%.2f\"", fW, fH, ro, fi0, angle);
    m_strGeometry = buf;
}

void CEffectRounding::GetGeometry(std::string & strGeometry)
{
    strGeometry = m_strGeometry;
}
