#ifndef _CURVE_DESCRIPTOR_H_
#define _CURVE_DESCRIPTOR_H_

#include <Carbon/Carbon.h>
#include <math.h>
#include <vector>

struct PointF
{
	PointF() {X=0.0f; Y=0.0f;}
	PointF(const PointF & pt) {X=pt.X; Y=pt.Y;}
	//PointF(const Float32Point & pt) {X=pt.x; Y=pt.y;}
	PointF(Float32 x, Float32 y) {X=x; Y=y;}
	PointF operator+(const PointF & pt) const {return PointF(X+pt.X, Y+pt.Y);}
	PointF operator-(const PointF & pt) const {return PointF(X-pt.X, Y-pt.Y);}
	PointF const & operator-=(const PointF & pt) {X-=pt.X; Y-=pt.Y; return *this;}
	Float32 X,Y;
};
inline const PointF & operator*=(PointF & arg1, Float32 arg2) {arg1.X *= arg2, arg1.Y *= arg2; return arg1;}
inline PointF operator*(PointF arg1, Float32 arg2) {return arg1 *= arg2;}
inline PointF operator*(Float32 arg1, PointF arg2) {return arg2 *= arg1;}

typedef PointF VectorF;

const float M_RAD_2_DEG = 57.295779f;
const float M_DEG_2_RAD = 1.0f / M_RAD_2_DEG;

//===========================================================

enum SegmentType {cstNone, cstLine, cstBezier, cstArc};

class CSegment
{
public:
	virtual int     init(const char * pwcData) = 0;
	virtual long    refCount() const = 0;
	virtual PointF  operator[](long lIndex) const = 0;
	virtual bool    setRefPoint(long lIndex, const PointF & rptRef) = 0;
	virtual Float32 length() const = 0;
	virtual PointF  point(Float32 fT) const = 0;
	virtual VectorF dir(Float32 fT) const = 0;
	virtual VectorF norm(Float32 fT) const = 0;
	
protected:
	int parseRefPoints(const char * pwcData, int nCount);
	
	void updateLimits(PointF point);

	Float32 xMin;
	Float32 xMax;
	Float32 yMin;
	Float32 yMax;

	static Float32 xMinGlobal;
	static Float32 xMaxGlobal;
	static Float32 yMinGlobal;
	static Float32 yMaxGlobal;

public:
	static void InitGlobalLimits();
	static Float32 XMinGlobal() { return xMinGlobal; }
	static Float32 XMaxGlobal() { return xMaxGlobal; }
	static Float32 YMinGlobal() { return yMinGlobal; }
	static Float32 YMaxGlobal() { return yMaxGlobal; }
	
protected:
	static Float32 dist(const PointF & arg1, const PointF & arg2);
	static void normalize(VectorF & vecN);
};


//===========================================================
class CBezierSegment : public CSegment
{
public:
	CBezierSegment();
public:
	virtual int     init(const char * pwcData);
	virtual long    refCount() const;
	virtual PointF  operator[](long lIndex) const;
	virtual bool    setRefPoint(long lIndex, const PointF & rptRef);
	virtual Float32 length () const;
	virtual PointF  point(Float32 fT) const;
	virtual VectorF dir(Float32 fT) const;
	virtual VectorF norm(Float32 fT) const;
private:
	Float32 m_fLength;
	PointF m_pptBezier[4];
private:
	void calc_length();
};


//===========================================================
class CLineSegment : public CSegment
{
public:
	CLineSegment();
public:
	virtual int     init(const char * pwcData);
	virtual long    refCount() const;
	virtual PointF  operator[](long lIndex) const;
	virtual bool    setRefPoint(long lIndex, const PointF & rptRef);
	virtual Float32 length() const;
	virtual PointF  point(Float32 fT) const;
	virtual VectorF dir(Float32 fT) const;
	virtual VectorF norm(Float32 fT) const;
private:
	PointF m_pptLine[2];
	Float32 m_fLength;
private:
	void calc_length();
};


//===========================================================
class CArcSegment : public CSegment
{
public:
	CArcSegment();
public:
	virtual int     init(const char * pwcData);
	virtual long    refCount() const;
	virtual PointF  operator[](long lIndex) const;
	virtual bool    setRefPoint(long lIndex, const PointF & rptRef);
	virtual Float32 length ()         const;
	virtual PointF  point(Float32 fT) const;
	virtual VectorF dir(Float32 fT) const;
	virtual VectorF norm(Float32 fT) const;
private:
	Float32 m_fLength;
	PointF m_pptArc[3];
	PointF m_ptCenter;
	Float32 m_fRadius;
	Float32 m_fStartAngle, m_fSweepAngle;
private:
	void calc_arc_params();
};


//===========================================================
class CCurveDescriptor : protected std::vector<CSegment*>
{
public:
	CCurveDescriptor();
	~CCurveDescriptor();
public:
	CSegment * AddSegment(SegmentType type);
	CSegment * AddSegment(char wcCode);
	void    add(CSegment * pSegment);
	void    RemoveAllSegments();
	long    refCount() const;
	PointF  getRefPoint(long lIndex) const;
	bool    setRefPoint(long lIndex, const PointF& rptRef);
	Float32 length() const;
	PointF  point(Float32 fT) const;
	PointF  norm(Float32 fT) const;
protected:
	bool m_bClosed;
};


#endif //_CURVE_DESCRIPTOR_H_
                            