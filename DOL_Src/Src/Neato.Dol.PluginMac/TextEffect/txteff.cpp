#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <paths.h>
#include <sys/param.h>

#include <string>
#include <vector>

#include <CoreFoundation/CoreFoundation.h>

#include "../Common/xmlhelper.h"
#include "txteff.h"
#include "curves.h"


CCurves * pCurves = NULL;


extern "C"
bool mod_init(bool bInit)
{
	if (bInit == true)
	{
		pCurves = new CCurves();
	}
	else
	{
		if (pCurves) delete pCurves;
		pCurves = NULL;
	}
	return true;
}

extern "C"
const char * mod_getinfo() 
{
		pCurves = new CCurves();
		if (pCurves) delete pCurves;
		pCurves = NULL;
	return 
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
		"<ModuleInfo Version=\"1\" Name=\"TextEffectModule\" Description=\"Text Effects Plugin\" Filename=\"mod_txteff.dylib\">"
			"<Commands>"
				"<Command Name=\"TEXTEFFECT_GET_CONTOUR\" />"
				"<Command Name=\"TEXTEFFECT_SET_FONT\" CopyRequest=\"0\" />"
			"</Commands>"
		"</ModuleInfo>";
}

extern "C"
xmlDoc * mod_invoke(xmlNode * reqElem)
{
	try
	{
		char * cmdName = (char*)xmlGetProp(reqElem, BAD_CAST "Name");
		if (cmdName == NULL) throw 1;
		std::string strCmdName = cmdName;
		xmlFree(cmdName);
		
		if (strcmp(strCmdName.c_str(), "TEXTEFFECT_GET_CONTOUR") == 0)
		{
			return pCurves->GetContour(reqElem);
		}
		else if (strcmp(strCmdName.c_str(), "TEXTEFFECT_SET_FONT") == 0)
		{
			return pCurves->SetFont(reqElem);
		}
		else
		{
			return NULL;
		}
	}
	catch(...) {return NULL;}
	return NULL;
}



