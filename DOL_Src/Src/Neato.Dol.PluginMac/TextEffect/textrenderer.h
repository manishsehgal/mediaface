#ifndef _TEXT_RENDERER_H_
#define _TEXT_RENDERER_H_

#include <Carbon/Carbon.h>
#include <vector>
#include <string>

#include "curvedescriptor.h"

//=================================================================

enum PointType {PointTypeStart, PointTypeLine, PointTypeCurve};

struct PathData
{
	bool                bFirst;
	Float32             Origin;
	Float32             Y;
	std::vector<PointF> Points;
	std::vector<UInt8>  Types;
	PathData();
	void clear();
	void addpath(PathData & path);
	void normalize();
	void scale();
	void bounds(PointF & minPt, PointF & maxPt);
	void align(int align);
};


//=================================================================

class CTextLayout
{
public:
	CTextLayout(CFStringRef text, ATSUStyle style);
	~CTextLayout();
	void MeasureTextImage(Rect & rc);
	void GetQuadraticPaths(PathData & path, Float32 fY);
protected:
   	ATSUStyle      m_Style;
	UniChar *      m_pText;
	ATSUTextLayout m_Layout;
	ATSQuadraticNewPathUPP   m_newPathProc;
	ATSQuadraticLineUPP      m_lineProc;
	ATSQuadraticCurveUPP     m_curveProc;
	ATSQuadraticClosePathUPP m_closePathProc;
protected:
	static OSStatus LineProc(const Float32Point *pt1, const Float32Point *pt2, void *callBackDataPtr);
	static OSStatus CurveProc (const Float32Point * pt1, const Float32Point * ptC, const Float32Point * pt2, void * callBackDataPtr);
	static OSStatus NewPathProc (void * callBackDataPtr);
	static OSStatus ClosePathProc (void * callBackDataPtr);
};


//=================================================================


class CTextRenderer
{
public:
    CTextRenderer();
    ~CTextRenderer();
    
public:
	bool Init(std::string & strText, std::string & strFontFileName, int nFontHeight, bool bUnderline, int nAlign);
	int  GetParaCount();
    bool AddTextToPath(PathData & pathDest, Float32 fWidth);
    bool AddParaToPath(int nPara, PathData & pathDest, Float32 fWidth, bool bWrap, Float32 * pfY);
	int  AddLineToPath(PathData & pathDest, Float32 fWidth, Float32 * pfY);
	bool IsFontAvailable(std::string & strFullFontName);
	
protected:
	void splitText();
	
protected:
    CFStringRef m_strText;
    std::vector<CFStringRef> m_vParas;
    CFStringRef m_strPara;
    
    int m_nFontHeight;
    int m_nFontHeightCoeff;
    int m_nAlign;
    
    ATSFontContainerRef m_fontCont;
    ATSUFontID m_fontId;
   	ATSUStyle m_Style;

    UniCharCount m_nNextChar;
	Float32 m_fYPos;
};

#endif //_TEXT_RENDERER_H_
