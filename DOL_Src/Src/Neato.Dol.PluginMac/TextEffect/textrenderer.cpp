#include "textrenderer.h"

//===============================================

PathData::PathData()
{
	clear();
}

void PathData::clear()
{
	bFirst = true;
	Origin = 0.0f;
	Points.clear();
	Types.clear();
}

void PathData::bounds(PointF & minPt, PointF & maxPt)
{
	if (Points.size() == 0) return;
	minPt = Points[0];
	maxPt = Points[0];
	int i;
	for (i=1; i<Points.size(); i++)
	{
		if (Points[i].X < minPt.X) minPt.X = Points[i].X;
		if (Points[i].Y < minPt.Y) minPt.Y = Points[i].Y;
		if (Points[i].X > maxPt.X) maxPt.X = Points[i].X;
		if (Points[i].Y > maxPt.Y) maxPt.Y = Points[i].Y;
	}
}

void PathData::normalize()
{
	if (Points.size() == 0) return;
	PointF minPt, maxPt;
	bounds(minPt, maxPt);
	int i;
	for (i=0; i<Points.size(); i++)
	{
		Points[i] -= minPt;
	}
}

void PathData::scale()
{
	if (Points.size() == 0) return;
	PointF minPt, maxPt;
	bounds(minPt, maxPt);
	int i;
	PointF dPt = maxPt - minPt;
	if (dPt.X > 0.0f)
	{
		for (i=0; i<Points.size(); i++)
		{
			Points[i].X /= dPt.X;
		}
	}
	if (dPt.Y > 0.0f)
	{
		for (i=0; i<Points.size(); i++)
		{
			Points[i].Y /= dPt.Y;
		}
	}
}

void PathData::addpath(PathData & path)
{
	Points.insert(Points.end(), path.Points.begin(), path.Points.end());
	Types.insert(Types.end(), path.Types.begin(), path.Types.end());
}

void PathData::align(int align)
{
	PointF minPt, maxPt;
	bounds(minPt, maxPt);
	for (int i=0; i<Points.size(); i++)
	{
		Points[i].X -= minPt.X;
		Points[i].X -= (maxPt.X - minPt.X) * (Float32)align / 2.0f;
	}
}

 
//===============================================


CTextLayout::CTextLayout(CFStringRef text, ATSUStyle style)
{
	ATSUCreateAndCopyStyle(style, &m_Style);
	
	UniCharCount nLen = CFStringGetLength(text);
	m_pText = new UniChar[nLen];
	CFRange range = CFRangeMake(0, nLen);
	CFStringGetCharacters(text, range, m_pText);
	ATSUCreateTextLayoutWithTextPtr(m_pText, kATSUFromTextBeginning, kATSUToTextEnd, nLen, 1, &nLen, &m_Style, &m_Layout);
	
	m_newPathProc   = NewATSQuadraticNewPathUPP(NewPathProc);
	m_lineProc      = NewATSQuadraticLineUPP(LineProc);
	m_curveProc     = NewATSQuadraticCurveUPP(CurveProc);
	m_closePathProc = NewATSQuadraticClosePathUPP(ClosePathProc);
}

CTextLayout::~CTextLayout()
{
	ATSUDisposeTextLayout(m_Layout);
	delete[] m_pText;
	ATSUDisposeStyle(m_Style);
	DisposeATSQuadraticNewPathUPP(m_newPathProc);
	DisposeATSQuadraticLineUPP(m_lineProc);
	DisposeATSQuadraticCurveUPP(m_curveProc);
	DisposeATSQuadraticClosePathUPP(m_closePathProc);	
}

void CTextLayout::MeasureTextImage(Rect & rc)
{
	memset(&rc, 0, sizeof(Rect));
	ATSUMeasureTextImage(m_Layout, kATSUFromTextBeginning, kATSUToTextEnd, 0, 0, &rc);
}

void CTextLayout::GetQuadraticPaths(PathData & path, Float32 fY)
{
	ATSLayoutRecord * layoutRecords;
	ItemCount numRecords;	
	ATSUDirectGetLayoutDataArrayPtrFromTextLayout(m_Layout, 0, kATSUDirectDataLayoutRecordATSLayoutRecordCurrent, (void**)&layoutRecords, &numRecords);
		
	ItemCount i;
	for (i=0; i<numRecords; i++)
	{
		if (layoutRecords[i].glyphID != kATSDeletedGlyphcode)
		{
				path.bFirst = true;
				path.Origin = layoutRecords[i].realPos / 65536;
				path.Y = fY;
				OSStatus status;
				ATSUGlyphGetQuadraticPaths(m_Style, layoutRecords[i].glyphID, m_newPathProc, m_lineProc, m_curveProc, m_closePathProc, &path, &status);
		}
	}
	ATSUDirectReleaseLayoutDataArrayPtr(NULL, kATSUDirectDataLayoutRecordATSLayoutRecordCurrent, (void**)&layoutRecords);
}

OSStatus CTextLayout::NewPathProc (void * callBackDataPtr)
{
	//printf("New Path\n");
	PathData * path = (PathData*)callBackDataPtr;
	path->bFirst = true;
	return noErr;
}
OSStatus CTextLayout::LineProc(const Float32Point *pt1, const Float32Point *pt2, void *callBackDataPtr)
{
	//printf("Line: %f,%f -> %f,%f\n", pt1->x, pt1->y, pt2->x, pt2->y);
	PathData * path = (PathData*)callBackDataPtr;
	if (path->bFirst == true)
	{
		path->Types.push_back(PointTypeStart);
		path->Points.push_back(PointF(pt1->x+path->Origin, pt1->y+path->Y));
		path->bFirst = false;
	}
	path->Types.push_back(PointTypeLine);
	path->Points.push_back(PointF(pt2->x+path->Origin, pt2->y+path->Y));
	return noErr;
}
OSStatus CTextLayout::CurveProc(const Float32Point * pt1, const Float32Point * ptC, const Float32Point * pt2, void * callBackDataPtr)
{
	//printf("Curve: %f,%f -> %f,%f\n", pt1->x, pt1->y, pt2->x, pt2->y);
	PathData * path = (PathData*)callBackDataPtr;
	if (path->bFirst == true)
	{
		path->Types.push_back(PointTypeStart);
		path->Points.push_back(PointF(pt1->x+path->Origin, pt1->y+path->Y));
		path->bFirst = false;
	}
	path->Types.push_back(PointTypeCurve);
	path->Points.push_back(PointF(ptC->x+path->Origin, ptC->y+path->Y));
	path->Types.push_back(PointTypeCurve);
	path->Points.push_back(PointF(pt2->x+path->Origin, pt2->y+path->Y));
	return noErr;
}
OSStatus CTextLayout::ClosePathProc (void * callBackDataPtr)
{
	//printf("Close Path\n");
	PathData * path = (PathData*)callBackDataPtr;
	path->bFirst = true;
	return noErr;
}
	
//===============================================


CTextRenderer::CTextRenderer()
{
	m_strText = NULL;
	m_nAlign = 0;
	m_fontCont = 0;
	m_fontId = 0;
	m_Style = NULL;
    m_nNextChar = 0;
    m_fYPos = 0.0f;
    m_nFontHeightCoeff = 1;
}

CTextRenderer::~CTextRenderer()
{
	OSStatus status;
	if (m_fontCont != 0)
	{
		status = ATSFontDeactivate(m_fontCont, NULL, kATSOptionFlagsDefault);
	}
	if (m_Style != NULL)
	{
		ATSUDisposeStyle(m_Style);
	}
	for (int i=0; i<m_vParas.size(); i++)
	{
		if (m_vParas[i] != NULL) CFRelease(m_vParas[i]);
	}
	m_vParas.clear();
	if (m_strText != NULL)
	{
		CFRelease(m_strText);
	}
}
    
bool CTextRenderer::Init(std::string & strText, std::string & strFontFileName, int nFontHeight, bool bUnderline, int nAlign)
{
	if (m_strText != NULL) CFRelease(m_strText);
	m_strText = CFStringCreateWithCString(NULL, strText.c_str(), kCFStringEncodingUTF8);
	splitText();
	
    m_nFontHeight = nFontHeight;
    m_nFontHeightCoeff = 1000 / m_nFontHeight;
    m_nAlign = nAlign;
    m_nNextChar = 0;
    m_fYPos = 0.0f;
	
	FSRef fontFSRef;
    OSStatus status = FSPathMakeRef((const UInt8 *)strFontFileName.c_str(), &fontFSRef, NULL);
    if (status != 0) return false;
    
	FSSpec fontFSSpec;
    status = FSGetCatalogInfo(&fontFSRef, kFSCatInfoNone, NULL, NULL, &fontFSSpec, NULL);    
    if (status != 0) return false;
    
	status = ATSFontActivateFromFileSpecification(&fontFSSpec, kATSFontContextLocal, kATSFontFormatUnspecified, NULL, kATSOptionFlagsDefault, &m_fontCont);
    if (status != 0) return false;
    
    ATSFontRef fontRef;
    ItemCount count;
    status = ATSFontFindFromContainer(m_fontCont, kATSOptionFlagsDefault, 1, &fontRef, &count);
    if (status != 0) return false;
    
    m_fontId = FMGetFontFromATSFontRef(fontRef);
    
	//---------------------------------------------------
	// Create style with font and size attributes
	
	ATSUCreateStyle(&m_Style);
	Fixed atsuSize = Long2Fix(m_nFontHeight * m_nFontHeightCoeff);
	ATSStyleRenderingOptions opt = kATSStyleNoHinting;
	ATSUAttributeTag      theTags[]   = {kATSUSizeTag,  kATSUFontTag,       kATSUStyleRenderingOptionsTag};
	ByteCount             theSizes[]  = {sizeof(Fixed), sizeof(ATSUFontID), sizeof(ATSStyleRenderingOptions)};
	ATSUAttributeValuePtr theValues[] = {&atsuSize,     &m_fontId,          &opt};	
	status = ATSUSetAttributes(m_Style, 2, theTags, theSizes, theValues);	
	
	return true;
}

int CTextRenderer::GetParaCount()
{
	return m_vParas.size();
}

bool CTextRenderer::AddTextToPath(PathData & pathDest, Float32 fWidth)
{
	m_fYPos = 0.0f;
	int i;
	for (i=0; i<m_vParas.size(); i++)
	{
		AddParaToPath(i, pathDest, fWidth, true, &m_fYPos);
	}
	return true;
}

bool CTextRenderer::AddParaToPath(int nPara, PathData & pathDest, Float32 fWidth, bool bWrap, Float32 * pfY)
{
	m_strPara = m_vParas[nPara];
    m_nNextChar = 0;
    while (AddLineToPath(pathDest, fWidth, pfY) != 0 && bWrap == true) {}
    return true;
}

int CTextRenderer::AddLineToPath(PathData & pathDest, Float32 fWidth, Float32 * pfY)
{
	fWidth*=(Float32)m_nFontHeightCoeff;
	
	UniCharCount nLength = CFStringGetLength(m_strPara);
	if (nLength == 0) return 0;
	if (m_nNextChar >= nLength) return 0;
	
	//---------------------------------------------------
	
	UniCharCount nFit = 0;
    if (fWidth > 0.0f)
    {
    	UniCharCount i;
        for (i=m_nNextChar; i<nLength; i++)
        {
        	CTextLayout layout(m_strPara, m_Style);
        	
			Rect rc;
			layout.MeasureTextImage(rc);
			
            if (rc.right-rc.left > fWidth)
            {
            	break;
            }
        }
        nFit = i - m_nNextChar;
    }
    else
    {
        nFit = nLength;
    }
    if (nFit == 0) return 0;
	
	//---------------------------------------------------
	
	{
        CTextLayout textLayout(m_strPara, m_Style);
		PathData path;
        textLayout.GetQuadraticPaths(path, pfY==NULL ? 0.0f : *pfY);
		path.align(m_nAlign);
		
		for (int i=0; i<path.Points.size(); i++) {
			path.Points[i].X /= (Float32)m_nFontHeightCoeff;
			path.Points[i].Y /= (Float32)m_nFontHeightCoeff;
		}
		
		pathDest.addpath(path);
		
    	m_nNextChar += nFit;
    	if (pfY != NULL)
    	{
    		*pfY += (Float32)(m_nFontHeight*m_nFontHeightCoeff);
    	}
	}
	
	return (m_nNextChar >= nLength ? 0 : 1);
}

bool CTextRenderer::IsFontAvailable(std::string & strFullFontName)
{
	FILE * fd = fopen(strFullFontName.c_str(), "r");
	if (fd) fclose(fd);
	return (fd != NULL);
}

void CTextRenderer::splitText()
{
	m_vParas.clear();
	UniCharCount len = CFStringGetLength(m_strText);
	if (len == 0) return;
	CFRange rng0 = {0,len};
	CFRange rng1 = {0,0};
	while (true)
	{
		Boolean bRes = CFStringFindWithOptions(m_strText, CFSTR("\n"), rng0, 0, &rng1);
		if (bRes == true)
		{
			rng0.length = rng1.location - rng0.location;
			CFStringRef str = CFStringCreateWithSubstring(NULL, m_strText, rng0);
			m_vParas.push_back(str);
			rng0.location = rng1.location + 1;
			rng0.length   = len - rng0.location;
		}
		else
		{
			rng0.length = len - rng0.location;
			CFStringRef str = CFStringCreateWithSubstring(NULL, m_strText, rng0);
			m_vParas.push_back(str);
			break;
		}
	}
}