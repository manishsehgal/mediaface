#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <netinet/in.h>
#include <pthread.h>
#include <string>

#include "interfaces.h"
#include "listener.h"


CListener::CListener(ICommandManager * pCmdMgr)
{
	m_pCmdMgr = pCmdMgr;
	m_nPort = 0;
	m_bBusy = false;
	m_listener = -1;
	
	pthread_t thread = 0;
	pthread_create(&thread, NULL, CListener::ListenerProc, this);
}

CListener::~CListener()
{
	if (m_listener != -1) {
		shutdown(m_listener, 2);
		close(m_listener);
		m_listener = -1;
	}
}

bool CListener::IsBusy()
{
	return m_bBusy;
}

void * CListener::ListenerProc(void * pArg)
{
    CListener * pListener = (CListener*)pArg;
    pListener->Listen();
	pthread_exit(NULL);
	return NULL;
}

void CListener::Listen()
{
    m_listener = socket(AF_INET, SOCK_STREAM, 0);
	if (m_listener == -1)
	{
		m_pCmdMgr->Trace("Error 1\n");
		return;
	}
	
	{
		int j = 1;
		setsockopt(m_listener, SOL_SOCKET, SO_REUSEADDR, (void *)&j, sizeof j);
	}
	
	sockaddr_in addr = {0};
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	
	unsigned short nPort = 1212;
    for (int i=0; i<10; i++)
    {
		addr.sin_port = htons(nPort);
		int nRet = bind(m_listener, (struct sockaddr *)&addr, sizeof(struct sockaddr_in));
		if (nRet == 0)
		{
			m_nPort = nPort;
            break;
		}
		else if (errno == EADDRINUSE)
		{
			nPort += 1234;
            continue;	
		}
		else
		{
			m_pCmdMgr->Trace("Error 2\n");
			return;
		}
	}
	if (m_nPort == 0) return;
	m_pCmdMgr->Trace("Port bound: %d\n", nPort);
	
	if (listen(m_listener, SOMAXCONN)) 
	{
		m_pCmdMgr->Trace("Error 3\n");
		return;
	}
	
	while (true)
	{
		int s = accept(m_listener, 0, 0);
		if (s < 0)
		{
			m_pCmdMgr->Trace("Error 4\n");
			usleep(500);
			continue;
		}
		m_bBusy = true;
		Respond(s);
		m_bBusy = false;
		
		shutdown(s, 2);
		close(s);
	}
}

#define MAX_STR 1024

void CListener::Respond(int s)
{
	m_pCmdMgr->Trace("Start respond.\n");
	
	std::string strIn = ReadFromSocket(s);
	
	m_pCmdMgr->Trace("In: %s\n", strIn.c_str());
	
	std::string strOut = "";
	if (strcmp(strIn.c_str(), "<policy-file-request/>") == 0)
	{
		char buf[128] = {0};
		sprintf(buf, "<cross-domain-policy><allow-access-from domain=\"*\" to-ports=\"%d\" /></cross-domain-policy>", m_nPort);
		strOut = buf;
	}
	else if (strcmp(strIn.c_str(), "<Discovery />") == 0)
	{
		strOut = "<Discovered />";
	}
	else
	{
		if (m_pCmdMgr != NULL)
        {
			strOut = m_pCmdMgr->ProcessRequest(strIn);
        }
		else
		{
			strOut = "<CmdInfo Type=\"Response\"><Response Error=\"1\" ErrorDesc=\"No Command Manager.\" /></CmdInfo>";
		}
	}

	//remove xml header because flash-code doesn't consider it
	int nPos = strOut.find("<?");
	if (nPos != std::string::npos)
	{
		nPos = strOut.find("?>", nPos);
		if (nPos != std::string::npos)
		{
			nPos = strOut.find("<", nPos);
			if (nPos != std::string::npos)
			{
				strOut.erase(0, nPos);
			}
		}
	}

	m_pCmdMgr->Trace("Out: %s\n", strOut.c_str());
	
	send(s, strOut.c_str(), strOut.size()+1, 0);
	
	m_pCmdMgr->Trace("Finish respond.\n\n");
}

std::string CListener::ReadFromSocket(int s)
{
	std::string str;
	while (true)
	{
		char buf[BUF_SIZE] = {0};
		int nRet = recv(s, buf, BUF_SIZE-1, 0);
		if (nRet <= 0)
		{
			m_pCmdMgr->Trace("Receiving Error.\n");
			break;
		}
		str += buf;
		if (buf[nRet-1] == 0) break;
	}
	return str;
}

