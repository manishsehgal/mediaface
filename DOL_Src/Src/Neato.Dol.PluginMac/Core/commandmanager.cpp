#include <stdio.h>
#include <unistd.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <dlfcn.h>
#include <map>
#include <zlib.h>

#include "commandmanager.h"


CCommandManager::CCommandManager(const char * path, WindowRef mainWindow)
{
	m_mainWindow = mainWindow;
	m_nTermAction = 0;
	
	int i, nPos;
	m_strPath = path;
	for (i=0; i<1; i++)
	{
		if ((nPos = m_strPath.rfind('/')) != std::string::npos)
		{
			m_strExeName = m_strPath.substr(nPos+1);
			m_strPath.erase(nPos);
		}
	}
	m_strAppPath = m_strPath;
	for (i=0; i<2; i++)
	{
		if ((nPos = m_strAppPath.rfind('/')) != std::string::npos)
		{
			m_strAppPath.erase(nPos);
		}
	}
	
	std::string logFN = m_strPath;
	logFN += "/log.log";
	int fd = open(logFN.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0644);
	if (fd >= 0)
	{
		char buf[] = "Logging started...\n";
		write(fd, buf, strlen(buf));
		close(fd);
	}
}

CCommandManager::~CCommandManager()
{
}

bool CCommandManager::InitModules()
{
    m_vModules.clear();
	
    AddSelfModule();
	
	DIR * pDir = opendir(m_strPath.c_str());
	if (pDir == NULL) return false;
	struct dirent * pDirEnt = 0;
	while (true)
	{
		pDirEnt = readdir(pDir);
		if (pDirEnt != NULL)
		{
			if (strlen(pDirEnt->d_name) > 3 && strncmp(pDirEnt->d_name, "mod_", 3) == 0)
			{
				std::string fn = m_strPath;
				fn += "/";
				fn += pDirEnt->d_name;
				AddModule(fn.c_str());
			}
		}
		else
		{
			break;
		}
	}
	closedir(pDir);
	
	return true;
}

void CCommandManager::AddModule(const char * fn)
{
	void * pLib = dlopen(fn, RTLD_LOCAL);
	if (pLib != NULL)
	{
		typedef const char * (*LP_GETINFO)();
		LP_GETINFO lpGetInfo = (LP_GETINFO)dlsym(pLib, "mod_getinfo");
		if (lpGetInfo != NULL)
		{
			const char * strInfo = lpGetInfo();
			if (strInfo != NULL)
			{
				CModule module;
                module.m_strFileName = fn;

                if (ParseModuleInfo(strInfo, module) == true && module.m_vCommands.size() > 0)
				{
					m_vModules.push_back(module);
				}
			}
		}
	}
	
	dlclose(pLib);
}

bool CCommandManager::ParseModuleInfo(const char * strInfo, CModule & module)
{
	xmlDocPtr doc = xmlReadMemory(strInfo, strlen(strInfo), NULL, NULL, 0);
	if (doc == NULL) return false;
	xmlNode * modInfoElem = xmlDocGetRootElement(doc);
	if (modInfoElem == NULL) return false;
	
	char * name = (char*)xmlGetProp(modInfoElem, BAD_CAST "Name");
	if (name == NULL) return false;
	module.m_strName = name;
	xmlFree(name);
	
	module.m_nVersion = 0;
	char * ver = (char*)xmlGetProp(modInfoElem, BAD_CAST "Version");
	if (ver != NULL)
	{
		module.m_nVersion = atoi(ver);
		xmlFree(ver);
	}
	
	module.m_bNeedReinstall = false;
	
	xmlNode * commandsElem = CXmlHelper::GetChildElement(modInfoElem, "Commands");
	if (commandsElem == NULL) return false;
	
	Trace("Module found: %s Ver=%d\n", module.m_strName.c_str(), module.m_nVersion);
	
	for (xmlNode * commandElem = commandsElem->children; commandElem != NULL; commandElem = commandElem->next)
	{
		if (commandElem->type == XML_ELEMENT_NODE && strcmp((const char*)commandElem->name, "Command") == 0)
		{
			CCommand cmd;
			
			char * cmdName = (char*)xmlGetProp(commandElem, BAD_CAST "Name");
			if (cmdName == NULL) continue;
			cmd.m_strName = cmdName;
			xmlFree(cmdName);
			
			Trace("  Command found: %s\n", cmd.m_strName.c_str());
			
			cmd.m_bCopyRequest = true;
			char * cr = (char*)xmlGetProp(commandElem, BAD_CAST "CopyRequest");
			if (cr != NULL && strcmp(cr, "0") == 0)
			{
				cmd.m_bCopyRequest = false;
			}
			xmlFree(cr);
			
			cmd.m_bSelf = false;
			
			module.m_vCommands.push_back(cmd);
		}
	}
	
	return true;
}

void CCommandManager::AddSelfModule()
{
    CModule module;
    module.m_strName        = ("CORE");
    module.m_strFileName    = ("dolcore");
	module.m_nVersion       = 1;
	module.m_bNeedReinstall = false;
    {
        CCommand cmd;
        cmd.m_strName = ("CORE_IS_COMMAND_AVAILABLE");
        cmd.m_bCopyRequest = true;
        cmd.m_bSelf = true;
        module.m_vCommands.push_back(cmd);
    }
    {
        CCommand cmd;
        cmd.m_strName = ("CORE_GET_UPDATE_INFO");
        cmd.m_bCopyRequest = false;
        cmd.m_bSelf = true;
        module.m_vCommands.push_back(cmd);
    }
    {
        CCommand cmd;
        cmd.m_strName = ("CORE_UPDATE_FILES");
        cmd.m_bCopyRequest = false;
        cmd.m_bSelf = true;
        module.m_vCommands.push_back(cmd);
    }
    {
        CCommand cmd;
		cmd.m_strName = ("CORE_IS_MODULE_AVAILABLE");
        cmd.m_bCopyRequest = false;
        cmd.m_bSelf = true;
        module.m_vCommands.push_back(cmd);
    }
    m_vModules.push_back(module);
}

CModule * CCommandManager::FindModule(const char * strName)
{
    for (int m=0; m<m_vModules.size(); m++)
    {
        CModule & mod = m_vModules[m];
        if (strcmp(mod.m_strName.c_str(), strName) == 0) return &mod;
    }
    return NULL;
}

bool CCommandManager::FindCommand(const char * strCmdName, int & nModInd, int & nCmdInd)
{
    for (int m=0; m<m_vModules.size(); m++)
    {
        CModule & mod = m_vModules[m];
        for (int c=0; c<mod.m_vCommands.size(); c++)
        {
            CCommand & cmd = mod.m_vCommands[c];
            if (strcmp(strCmdName, cmd.m_strName.c_str()) == 0)
            {
                nModInd = m;
                nCmdInd = c;
                return true;
            }
        }
    }
    return false;
}

void CCommandManager::Terminate(int nAction)
{
	m_nTermAction = nAction;
	//exit(0);
	EventTargetRef target = GetWindowEventTarget(m_mainWindow);
	if (target != NULL)
	{
		EventRef event;
		CreateEvent(NULL, kEventClassWindow, kEventWindowClose, 0, kEventAttributeNone, &event);
		if (event != NULL)
		{
			SetEventParameter(event, kEventParamPostTarget, typeEventTargetRef, sizeof(EventTargetRef), &target);
			PostEventToQueue(GetMainEventQueue(), event, kEventPriorityStandard);
		}
	}
}

void CCommandManager::Trace(const char * format, ...)
{
	va_list args;
	va_start(args, format);
	char buf[1024];
	int nBuf = vsnprintf(buf, sizeof(buf), format, args);
	if(nBuf < 0 || nBuf >= sizeof(buf))
	{
		char cont[] = "...\n";
		strcpy(buf + sizeof(buf) - sizeof(cont), cont);
	}
	
	printf(buf);
	
	std::string logFN = m_strPath;
	logFN += "/log.log";
	int fd = open(logFN.c_str(), O_WRONLY|O_CREAT|O_APPEND, 0644);
	if (fd >= 0)
	{
		write(fd, buf, strlen(buf));
		close(fd);
	}
	
	/*
	EventTargetRef target = GetWindowEventTarget(m_mainWindow);
	if (target != NULL)
	{
		EventRef event;
		CreateEvent(NULL, kEventClassUser, kEventUserTrace, 0, kEventAttributeNone, &event);
		if (event != NULL)
		{
			CFStringRef msg = CFStringCreateWithCString(NULL, buf, kCFStringEncodingUTF8);
			SetEventParameter(event, kEventParamTraceMsg, typeCFStringRef, sizeof(CFStringRef), &msg);
			SendEventToEventTarget(event, target);
			CFRelease(msg);
		}
	}
	*/	
	va_end(args);
}

std::string CCommandManager::ProcessRequest(std::string strCmd)
{
	std::string strCmdName("");
	try
	{
		xmlDocPtr doc = xmlReadMemory(strCmd.c_str(), strCmd.size(), NULL, NULL, 0);
		if (doc == NULL) throw 1;
		xmlNode * cmdInfoElem = xmlDocGetRootElement(doc);
		if (cmdInfoElem == NULL) throw 2;

		char * type = (char*)xmlGetProp(cmdInfoElem, BAD_CAST "Type");
		if (type == NULL || strcmp(type, "Request") != 0) throw 3;
		xmlFree(type);

		xmlNode * reqElem = CXmlHelper::GetChildElement(cmdInfoElem, "Request");
		if (reqElem == NULL) throw 4;

		char * cmdName = (char*)xmlGetProp(reqElem, BAD_CAST "Name");
		if (cmdName == NULL) throw 5;
		strCmdName = cmdName;

        bool bCopyRequest = true;
        xmlDoc * respDoc = InvokeCommand(cmdName, reqElem, bCopyRequest);
        if (respDoc == NULL) throw 6;
		xmlNode * respElem = xmlDocGetRootElement(respDoc);
        if (respElem == NULL) throw 7;
		xmlNode * respCopyElem = xmlCopyNode(respElem, 1);
		if (respCopyElem == NULL) throw 3;
	
        if (bCopyRequest == false)
        {
			xmlUnlinkNode(reqElem);
			xmlFreeNode(reqElem);
        }
		
		//xmlUnlinkNode(respElem);
		xmlAddChild(cmdInfoElem, respCopyElem);
		
		xmlSetProp(cmdInfoElem, BAD_CAST "Type", BAD_CAST "Response");
		xmlSetProp(cmdInfoElem, BAD_CAST "Name", BAD_CAST cmdName);
		xmlFree(cmdName);
		
		
		xmlChar * pMem = NULL;
		int nSize = 0;
		xmlDocDumpMemory(doc, &pMem, &nSize);
		std::string strResult((char*)pMem);
		xmlFree(pMem);
		
		xmlFreeDoc(doc);
		
        return strResult;
	}
	
	catch (int nErr)
	{
		Trace("ProcessRequest() error: %d\n", nErr); 
	}
	catch (...) {}
	
	std::string strRet;
	strRet += "<CmdInfo Type=\"Response\" Name=\"";
	strRet += strCmdName;
	strRet += "\"><Response Error=\"1\" ErrorDesc=\"General Command Error.\" /></CmdInfo>";
	return strRet;
}

xmlDoc * CCommandManager::InvokeCommand(const char * cmdName, xmlNode * reqElem, bool & bCopyRequest)
{
    try
    {
        int nModInd = -1;
        int nCmdInd = -1;
        if (!FindCommand(cmdName, nModInd, nCmdInd)) throw 1;

        CModule  & mod = m_vModules[nModInd];
        CCommand & cmd = mod.m_vCommands[nCmdInd];

        bCopyRequest = cmd.m_bCopyRequest;

        if (cmd.m_bSelf)
        {
            return InvokeSelfCommand(cmdName, reqElem);
        }

		void * pLib = dlopen(mod.m_strFileName.c_str(), RTLD_LOCAL);
		if (pLib == NULL) throw 2;

        typedef bool (*LP_INIT)(bool);
        LP_INIT lpInitFunc = (LP_INIT)dlsym(pLib, "mod_init");

		typedef xmlDoc* (*LP_INVOKE)(xmlNode*);
        LP_INVOKE lpInvokeFunc = (LP_INVOKE)dlsym(pLib, "mod_invoke");
        if (lpInvokeFunc == NULL) {dlclose(pLib); throw 3;}

        if (lpInitFunc != NULL) lpInitFunc(true);
        xmlDoc * respDoc = lpInvokeFunc(reqElem);
        if (lpInitFunc != NULL) lpInitFunc(false);

        dlclose(pLib);

        if (respDoc == NULL) throw 4;

        return respDoc;
    }
    catch (int nErr)
    {
		Trace("InvokeCommand() error: %d\n", nErr); 
    }
    catch(...){}
	
    return NULL;
}

xmlDoc * CCommandManager::InvokeSelfCommand(const char * cmdName, xmlNode * reqElem)
{
    if (strcmp(cmdName, "CORE_IS_COMMAND_AVAILABLE") == 0)
    {
        //return Command_IsCommandAvailable(reqElem);
    }
	else if (strcmp(cmdName, "CORE_IS_MODULE_AVAILABLE") == 0)
	{
		return Command_IsModuleAvailable(reqElem);
	}
    else if (strcmp(cmdName, "CORE_GET_UPDATE_INFO") == 0)
    {
        return Command_GetUpdateInfo(reqElem);
    }
    else if (strcmp(cmdName, "CORE_UPDATE_FILES") == 0)
    {
        return Command_UpdateFiles(reqElem);
    }
	return NULL;
}

xmlDoc * CCommandManager::Command_IsModuleAvailable(xmlNode * reqElem)
{
	try
	{
		xmlNode * paramsElem = CXmlHelper::GetChildElement(reqElem, "Params");
		if (paramsElem == NULL) throw 1;
	
		xmlNode * moduleElem = CXmlHelper::GetChildElement(paramsElem, "Module");
		if (moduleElem == NULL) throw 2;
	
		char * name = (char*)xmlGetProp(moduleElem, BAD_CAST "Name");
		if (name == NULL) throw 3;
		
        CModule * module = FindModule(name);
        bool bIsMod = (module != NULL);
        if (module != NULL)
		{
            bIsMod = !module->m_bNeedReinstall;
		}

		//-----------------------
		
		xmlDoc * respDoc = xmlNewDoc(BAD_CAST "1.0");
		if (respDoc == NULL) throw 4;
		
		xmlNode * respElem = xmlNewNode(NULL, BAD_CAST "Response");
		if (respElem == NULL) throw 5;
		xmlDocSetRootElement(respDoc, respElem);
		
		xmlSetProp(respElem, BAD_CAST "Error", BAD_CAST "0");

		xmlNode * params2Elem = xmlNewChild(respElem, NULL, BAD_CAST "Params", NULL);
		if (params2Elem == NULL) throw 6;
		
		xmlNode * statusElem = xmlNewChild(params2Elem, NULL, BAD_CAST "Status", NULL);
		if (statusElem == NULL) throw 7;
		
		xmlSetProp(statusElem, BAD_CAST "status", bIsMod ? BAD_CAST "Ok" : BAD_CAST "NoSuchModule");
		xmlSetProp(statusElem, BAD_CAST "name", BAD_CAST name);
		
		xmlFree(name);
		return respDoc;
	}
    catch (int nErr)
    {
		Trace("Command_IsModuleAvailable() error: %d\n", nErr); 
    }
    catch(...){}
	
	return NULL;
}

xmlDoc * CCommandManager::Command_GetUpdateInfo(xmlNode * reqElem)
{
	try
	{
		m_mapFileNames.clear();
		
		xmlNode * paramsElem = CXmlHelper::GetChildElement(reqElem, "Params");
		if (paramsElem == NULL) throw 1;
		
		xmlNode * modulesElem = CXmlHelper::GetChildElement(paramsElem, "Modules");
		if (modulesElem == NULL) throw 2;
		
		xmlNode * modulesRElem = xmlCopyNode(modulesElem, 1);
		if (modulesRElem == NULL) throw 3;
		
		for (xmlNode * modInfoElem = modulesRElem->children; modInfoElem != NULL; modInfoElem = modInfoElem->next)
		{
			if (modInfoElem->type == XML_ELEMENT_NODE && strcmp((const char*)modInfoElem->name, "ModuleInfo") == 0)
			{
				CheckFileInfo(modInfoElem);
				
				xmlNode * dependsElem = CXmlHelper::GetChildElement(modInfoElem, "Depends");
				if (dependsElem != NULL)
				{
					for (xmlNode * fileElem = dependsElem->children; fileElem != NULL; fileElem = fileElem->next)
					{
						if (fileElem->type == XML_ELEMENT_NODE && strcmp((const char*)fileElem->name, "File") == 0)
						{
							CheckFileInfo(fileElem);
						}
					}
				}
			}
		}
		
		//-----------------------
		
		xmlDoc * respDoc = xmlNewDoc(BAD_CAST "1.0");
		if (respDoc == NULL) throw 4;
		
		xmlNode * respElem = xmlNewNode(NULL, BAD_CAST "Response");
		if (respElem == NULL) throw 5;
		xmlDocSetRootElement(respDoc, respElem);
		
		xmlSetProp(respElem, BAD_CAST "Error", BAD_CAST "0");

		xmlNode * paramsRElem = xmlNewChild(respElem, NULL, BAD_CAST "Params", NULL);
		if (paramsRElem == NULL) throw 6;
		
		xmlAddChild(paramsRElem, modulesRElem);
		
		return respDoc;
	}
    catch (int nErr)
    {
		Trace("Command_GetUpdateInfo() error: %d\n", nErr); 
    }
    catch(...){}
	
	return NULL;
}

void CCommandManager::CheckFileInfo(xmlNode * fileElem)
{
	xmlSetProp(fileElem, BAD_CAST "UpdateState", BAD_CAST "Updated");
	
	char * fileName = (char*)xmlGetProp(fileElem, BAD_CAST "Filename");
	char * sum      = (char*)xmlGetProp(fileElem, BAD_CAST "Sum");
	if (fileName == NULL || sum == NULL) return;
	std::string strFilename(fileName);
	
	char * localPath = (char*)xmlGetProp(fileElem, BAD_CAST "LocalPath");
	if (localPath != NULL)
	{
		std::string strLocalPath(localPath);
		m_mapFileNames[strFilename] = strLocalPath;
		xmlFree(localPath);
		strFilename = strLocalPath;
	}
	
    std::string strState = "NoChange";
    std::string strSum = "";
    int nRes = CalcSum(strFilename.c_str(), strSum);
	//printf("Sum of %s = %s\n", fileName, strSum.c_str());
    if (nRes == 0 && strcmp(strSum.c_str(), sum) != 0)
    {
        strState = "Updated";
    }
    else if (nRes == 1)
    {
        strState = "New";
    }
	
	char * name = (char*)xmlGetProp(fileElem, BAD_CAST "Name");
	char * ver  = (char*)xmlGetProp(fileElem, BAD_CAST "Version");
	
	int nVer = 0;
	if (ver != NULL) nVer = atoi(ver);
	
	if (name != NULL && ver != NULL && nVer > 0)
	{
		CModule * module = FindModule(name);
		if (module != NULL && nVer > module->m_nVersion)
		{
            module->m_bNeedReinstall = true;
            strState = "Reinstall";
		}
	}
	
	if (name != NULL) xmlFree(name);
	if (ver  != NULL) xmlFree(ver);
	
	xmlSetProp(fileElem, BAD_CAST "UpdateState", BAD_CAST strState.c_str());
	
	if (fileName != NULL) xmlFree(fileName);
	if (sum      != NULL) xmlFree(sum);
}
	
int CCommandManager::CalcSum(const char * fileName, std::string & strSum)
{
	std::string fn = m_strPath;
	fn += "/";
	fn += fileName;
	FILE * fd = fopen(fn.c_str(), "r");
	if (fd == NULL) return 1;
	
	unsigned int sum = 0;
	unsigned char ch = 0;
	while (fread(&ch, 1, 1, fd) == 1)
	{
		sum += ch;
	}
	
	fclose(fd);
	
	char buf[64] = {0};
	sprintf(buf, "%u", sum);
    strSum = buf;

    return 0;
}

xmlDoc * CCommandManager::Command_UpdateFiles(xmlNode * reqElem)
{
    try
    {

		xmlNode * paramsElem = CXmlHelper::GetChildElement(reqElem, "Params");
		if (paramsElem == NULL) throw 1;
		
		xmlNode * filesElem = CXmlHelper::GetChildElement(paramsElem, "Files");
		if (filesElem == NULL) throw 2;
		
		for (xmlNode * fileElem = filesElem->children; fileElem != NULL; fileElem = fileElem->next)
		{
			if (fileElem->type == XML_ELEMENT_NODE && strcmp((const char*)fileElem->name, "File") == 0)
			{
				char * filename = (char*)xmlGetProp(fileElem, BAD_CAST "Filename");
				char * filebin  = (char*)xmlNodeGetContent(fileElem);
				if (filename != NULL && filebin != NULL)
				{
					if (saveFile(filename, filebin) == false) throw 12;
					xmlFree(filename);
					xmlFree(filebin);
				}
			}
		}
		
        InitModules();
		
		xmlDoc * respDoc = xmlNewDoc(BAD_CAST "1.0");
		if (respDoc == NULL) throw 4;
		
		xmlNode * respElem = xmlNewNode(NULL, BAD_CAST "Response");
		if (respElem == NULL) throw 5;
		xmlDocSetRootElement(respDoc, respElem);
		
		xmlSetProp(respElem, BAD_CAST "Error", BAD_CAST "0");

		xmlNode * paramsRElem = xmlNewChild(respElem, NULL, BAD_CAST "Params", NULL);
		if (paramsRElem == NULL) throw 6;
		
		return respDoc;
	}
    catch (int nErr)
    {
		Trace("Command_UpdateFiles() error: %d\n", nErr); 
    }
    catch(...){}
	
	return NULL;
}

bool CCommandManager::saveFile(const char * fileName, const char * data)
{
	//-------------------------------------------
	//Un-base64
	
    static char TableBase64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	unsigned long dwSizeB = strlen(data);
	if (dwSizeB == 0) return false;
	if (dwSizeB % 4 != 0) return false;

	unsigned long dwSizeC = dwSizeB / 4 * 3;
	unsigned char * pBufC = new unsigned char[dwSizeC];
	std::map<char, int> chars;
	for (int j=0; j<64; j++)
	{
		chars[TableBase64[j]] = j;
	}
    
    unsigned long i=0;
    unsigned long c=0;
	for (; i<dwSizeB; i+=4)
	{
		int  nInSym  = 4;
		int  nOutSym = 3;
		if (data[i+2] == '=' && data[i+3] == '=')
		{
			nInSym    = 2;
			nOutSym   = 1;
		}
		else if (data[i+3] == '=')
		{
			nInSym    = 3;
			nOutSym   = 2;
		}

		int dwQ = (chars[data[i]]<<18) | (chars[data[i+1]]<<12);
		if (nInSym > 2)
		{
			dwQ |= chars[data[i+2]]<<6;
		}
		if (nInSym > 3)
		{
			dwQ |= chars[data[i+3]];
		}

		pBufC[c++] = (unsigned char)(dwQ >> 16);
		if (nOutSym > 1)
		{
			pBufC[c++] = (unsigned char)(dwQ >> 8);
		}
		if (nOutSym > 2)
		{
			pBufC[c++] = (unsigned char)dwQ;
		}
	}
	dwSizeC = c;

	//-------------------------------------------
	// get full file name
	
	std::string strFilename(fileName);
	std::string fullFileName = m_strPath;
	fullFileName += "/";
	
	if (m_mapFileNames.find(strFilename) != m_mapFileNames.end())
	{
		strFilename = m_mapFileNames[strFilename];
	}
	
	fullFileName += strFilename;
	
	//-------------------------------------------
	//Un-compress data
	
	unsigned long   dwSize = 0;
	unsigned char * pBuf = NULL;
	
	if (dwSizeC > 2 && strncmp((char*)pBufC, "BZ", 2)==0) // new format is ordinary bzip2 archive
	{
		// save archive file
		std::string bzFileName = fullFileName;
		int fbz = open(bzFileName.c_str(), O_RDWR|O_CREAT|O_TRUNC, 0666);
		if (fbz < 0) {delete[] pBufC; return false;}
		unsigned long wr = write(fbz, pBufC+5, dwSizeC-5);
		close(fbz);
		delete[] pBufC;
		chmod(bzFileName.c_str(), 0666);
		if (wr != dwSizeC) {return false;}
	}
	else {
		// un-zip data
		dwSize = dwSizeC * 5;
		pBuf = NULL;
		int dwRes = Z_OK;
		do {
			delete[] pBuf;
			dwSize *= 2;
			pBuf = new unsigned char[dwSize];
			if (pBuf == NULL) {delete[] pBufC; return false;}
			dwRes = uncompress(pBuf, &dwSize, pBufC, dwSizeC);
		} while (dwRes == Z_BUF_ERROR && dwSize < dwSizeC*500);
		delete[] pBufC;
		if (dwRes != 0) return false;
		
		// save file
		int fd = open(fullFileName.c_str(), O_RDWR|O_CREAT|O_TRUNC, 0777);
		if (fd < 0) return false;
		unsigned long wr = write(fd, pBuf, dwSize);
		close(fd);
		delete[] pBuf;
		if (wr != dwSize) return false;
	}
	
	//-------------------------------------------
	
	//if (strcmp(fileName, m_strExeName.c_str()) == 0)
	{
		Terminate(1);
	}
		
	return true;
}

void CCommandManager::DoTermAction()
{
	if (m_nTermAction == 1)
	{
		reload();
	}
}

void CCommandManager::reload()
{
	//save reloader script
	std::string reloaderFN = m_strPath;
	reloaderFN += "/reloader";
	int fd = open(reloaderFN.c_str(), O_RDWR|O_CREAT|O_TRUNC, 0755);
	if (fd < 0) return;
	char buf[] = "#! /bin/bash\n"
	             "sleep 3\n"
	             "open $1\n";
	write(fd, buf, strlen(buf));
	close(fd);
	
	int res = fork(); 
	//printf("fork = %d\n", res);
	if (res < 0) return;//error
	//if (res > 0) {Terminate(); return true;}//parent
	if (res == 0)//child
	{
		execlp(reloaderFN.c_str(), reloaderFN.c_str(), m_strAppPath.c_str(), (char*)0);
		//printf("execlp = %d\n", errno);
	}
	
}



 
 