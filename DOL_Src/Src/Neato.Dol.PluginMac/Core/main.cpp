#include <sys/param.h>
#include <sys/stat.h>
#include <string>
#include <Carbon/Carbon.h>
#include "commandmanager.h"
#include "listener.h"

static OSStatus   AppEventHandler( EventHandlerCallRef inCaller, EventRef inEvent, void* inRefcon );
static OSStatus   HandleNew();
static OSStatus   WindowEventHandler( EventHandlerCallRef inCaller, EventRef inEvent, void* inRefcon );
static void       OnStartOnLoginPressed();
static void       Trace(CFStringRef msg);
static void       Trace(const char * msg);
static bool       SetAutoLaunch(bool bSet, bool bCheckOnly);
static void       CreateDesktopAlias();
static void       ShowApp(bool bShow);

static IBNibRef   sNibRef;
static WindowRef  mainWindow;
static char       g_Path[MAXPATHLEN];

static const ControlID kCheck = { 'mfop', 2 };

//--------------------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  	printf("Start server.\n");
  	printf("Path: %s\n", argv[0]);
  	
  	strcpy(g_Path, argv[0]);
  	
  	bool bExit = false;
  	for (int a=1; a<argc; a++)
  	{
  		if (strcmp(argv[a], "-autostart") == 0)
  		{
	    	SetAutoLaunch(true, false);
	    	bExit = true;
  		}
  		else if (strcmp(argv[a], "-makealias") == 0)
  		{
			CreateDesktopAlias();
	    	bExit = true;
	    }
	}
	if (bExit == true) return 0;
  		
    // Create a Nib reference, passing the name of the nib file (without the .nib extension).
    // CreateNibReference only searches into the application bundle.
    OSStatus err = CreateNibReference( CFSTR("main"), &sNibRef );
    require_noerr( err, CantGetNibRef );
    
    // Once the nib reference is created, set the menu bar. "MainMenu" is the name of the menu bar
    // object. This name is set in InterfaceBuilder when the nib is created.
    err = SetMenuBarFromNib( sNibRef, CFSTR("MenuBar") );
    require_noerr( err, CantSetMenuBar );
    
    // Install our handler for common commands on the application target
    static const EventTypeSpec kAppEvents[] =
    {
        { kEventClassCommand, kEventCommandProcess }
    };
    InstallApplicationEventHandler( NewEventHandlerUPP( AppEventHandler ), GetEventTypeCount( kAppEvents ), kAppEvents, 0, NULL );
    
    // Create a new window. A full-fledged application would do this from an AppleEvent handler for kAEOpenApplication.
    HandleNew();
	
	{
		CCommandManager cmdMgr(argv[0], mainWindow);
		cmdMgr.InitModules();
		CListener listener(&cmdMgr);
    
		// Run the event loop
		RunApplicationEventLoop();
		
		for (int i=0; i<10; i++)
		{
			if (listener.IsBusy() == false) break;
			sleep(1);  
		}
		cmdMgr.DoTermAction();
	}

CantSetMenuBar:
CantGetNibRef:

  	printf("Exit server.\n");
    return err;
}

//--------------------------------------------------------------------------------------------
static OSStatus AppEventHandler( EventHandlerCallRef inCaller, EventRef inEvent, void* inRefcon )
{
    OSStatus    result = eventNotHandledErr;
    
    switch ( GetEventClass( inEvent ) )
    {
        case kEventClassCommand:
        {
            HICommandExtended cmd;
            verify_noerr( GetEventParameter( inEvent, kEventParamDirectObject, typeHICommand, NULL, sizeof( cmd ), NULL, &cmd ) );
            
            switch ( GetEventKind( inEvent ) )
            {
                case kEventCommandProcess:
                    switch ( cmd.commandID )
                    {
                        case kHICommandNew:
                            result = HandleNew();
                            break;
                       default:
                            break;
                    }
                    break;
            }
            break;
        }
		case kEventClassWindow:
		{
			break;
		}
            
        default:
            break;
    }
    
    return result;
}

//--------------------------------------------------------------------------------------------
DEFINE_ONE_SHOT_HANDLER_GETTER( WindowEventHandler )

//--------------------------------------------------------------------------------------------
static OSStatus HandleNew()
{
    // Create a window. "MainWindow" is the name of the window object. This name is set in 
    // InterfaceBuilder when the nib is created.
    OSStatus err = CreateWindowFromNib( sNibRef, CFSTR("MainWindow"), &mainWindow );
    require_noerr( err, CantCreateWindow );

    // Install a command handler on the window. We don't use this handler yet, but nearly all
    // Carbon apps will need to handle commands, so this saves everyone a little typing.
    static const EventTypeSpec  kWindowEvents[] =
    {
        { kEventClassCommand, kEventCommandProcess },
		{ kEventClassWindow,  kEventWindowClose    },
		{ kEventClassUser,    kEventUserTrace      }
    };
    InstallWindowEventHandler( mainWindow, GetWindowEventHandlerUPP(), GetEventTypeCount(kWindowEvents), kWindowEvents, mainWindow, NULL );
    
	ControlRef ctlCheck;
	GetControlByID(mainWindow, &kCheck, &ctlCheck);
	if (ctlCheck != NULL)
	{
		SetControlValue(ctlCheck, SetAutoLaunch(false, true)==true ? kControlCheckBoxCheckedValue : kControlCheckBoxUncheckedValue);
	}
	
	// Hide the application
	ShowApp(false);
	
    // Position new windows in a staggered arrangement on the main screen
    RepositionWindow( mainWindow, NULL, kWindowCascadeOnMainScreen );
    
    // The window was created hidden, so show it
    ShowWindow(mainWindow);
	
CantCreateWindow:
    return err;
}

//--------------------------------------------------------------------------------------------
static OSStatus WindowEventHandler( EventHandlerCallRef inCaller, EventRef inEvent, void* inRefcon )
{
    OSStatus err    = eventNotHandledErr;
    UInt32 evtClass = GetEventClass(inEvent);
    UInt32 evtKind  = GetEventKind(inEvent);
    printf("WindowEventHandler: evtClass=%d  evtKind=%d\n", evtClass, evtKind);
    
    switch (evtClass)
    {
        case kEventClassCommand:
        {
            HICommandExtended cmd;
            verify_noerr( GetEventParameter( inEvent, kEventParamDirectObject, typeHICommand, NULL, sizeof( cmd ), NULL, &cmd ) );
            
            switch (evtKind)
            {
                case kEventCommandProcess:
				    //printf("commandID=%.4s\n", &cmd.commandID);
                    switch ( cmd.commandID )
                    {
						case 'stop':
							QuitApplicationEventLoop();
							break;
						case 'satl':
							OnStartOnLoginPressed();
							break;
                        default:
                            break;
                    }
                    break;
            }
            break;
        }
        case kEventClassWindow:
		{
            switch (evtKind)
            {
                case kEventWindowClose:
					QuitApplicationEventLoop();
					break;
                default:
                    break;
			}
			break;
		}
        case kEventClassUser:
		{
            switch (evtKind)
            {
                case kEventUserTrace:
                {
            		CFStringRef msg;
            		verify_noerr(GetEventParameter(inEvent, kEventParamTraceMsg, typeCFStringRef, NULL, sizeof(msg), NULL, &msg));
            		Trace(msg);
					break;
                }
                default:
                    break;
			}
			break;
		}
        default:
            break;
    }
    
    return err;
}

static void OnStartOnLoginPressed()
{
	//ControlID kCheck = { 'mfop', 2 };
	ControlRef ctlCheck;
	GetControlByID(mainWindow, &kCheck, &ctlCheck);
	if (ctlCheck == NULL) return;
	SInt16 val = GetControlValue(ctlCheck);
	SetAutoLaunch((val == kControlCheckBoxCheckedValue), false);
}

static void Trace(CFStringRef msg)
{
	/*
	const ControlID kText = { 'mfop', 0 };
	ControlRef ctlText;
	GetControlByID(mainWindow, &kText, &ctlText);
	
	CFStringRef oldText;
	GetControlData(ctlText, 0, kControlEditTextCFStringTag, sizeof(CFStringRef), &oldText, NULL);
	CFMutableStringRef newText = CFStringCreateMutableCopy(NULL, 0, oldText);
	CFStringAppend(newText, msg);
	
	SetControlData(ctlText, 0, kControlEditTextCFStringTag, sizeof(CFStringRef), &newText);
	CFRelease(oldText);
	CFRelease(newText);
	*/
}

static void Trace(const char * msg)
{
	/*
	CFStringRef text = CFStringCreateWithCString(NULL, msg, kCFStringEncodingUTF8);
	Trace(text);
	CFRelease(text);
	*/
}

static bool SetAutoLaunch(bool bSet, bool bCheckOnly)
{
	std::string strPath(g_Path);
	for (int i=0; i<3; i++)
	{
		int nPos;
		if ((nPos = strPath.rfind('/')) != std::string::npos)
		{
			strPath.erase(nPos);
		}
	}
	
	//-----------------------------------------
	
	CFStringRef app = CFStringCreateWithCString(NULL, strPath.c_str(), kCFStringEncodingUTF8);
	
	//-----------------------------------------
	
	CFArrayRef arr  = (CFArrayRef)CFPreferencesCopyValue(CFSTR("AutoLaunchedApplicationDictionary"), CFSTR("loginwindow"), kCFPreferencesCurrentUser, kCFPreferencesAnyHost);
	if (arr == NULL)
	{
		 if (bSet == false) return false;
		 arr = CFArrayCreate(NULL, NULL, 0, NULL);
	}
	CFMutableArrayRef arrM = CFArrayCreateMutableCopy(NULL, 0, arr);
	if (arrM == NULL) return false;
	int arrCount = CFArrayGetCount(arrM);
	
	//-----------------------------------------
	
	int nInd = -1;
	CFMutableDictionaryRef dictM = NULL;
	for (int i=0; i<arrCount; i++)
	{
		CFDictionaryRef dict = (CFDictionaryRef)CFArrayGetValueAtIndex(arrM, i);
		if (dict == NULL) continue;
		CFMutableDictionaryRef dictMM = CFDictionaryCreateMutableCopy(NULL, 0, dict);
		if (dictMM == NULL) continue;
		CFStringRef path = (CFStringRef)CFDictionaryGetValue(dictMM, CFSTR("Path"));
		if (path != NULL && CFStringFind(path, CFSTR("MFOPlugins.app"), 0).location >= 0)
		{
			dictM = dictMM;
			nInd = i;
			break;
		}
	}
	
	//-----------------------------------------
	
	if (bCheckOnly == true)
	{
		return (nInd >= 0);
	}
	
	//-----------------------------------------
	
	if (nInd >= 0)
	{
		if (bSet == true)
		{
			CFDictionarySetValue(dictM, CFSTR("Hide"), kCFBooleanTrue);
			CFDictionarySetValue(dictM, CFSTR("Path"), app);
			CFArraySetValueAtIndex(arrM, nInd, dictM);
		}
		else
		{
			CFArrayRemoveValueAtIndex(arrM, nInd);
		}
	}
	
	if (nInd == -1 && bSet == true)
	{
		CFMutableDictionaryRef dictM = CFDictionaryCreateMutable(NULL, 0, NULL, NULL);
		CFDictionaryAddValue(dictM, CFSTR("Hide"), kCFBooleanTrue);
		CFDictionaryAddValue(dictM, CFSTR("Path"), app);
		CFArrayAppendValue(arrM, dictM);
	}
	
	//-----------------------------------------
	
	CFPreferencesSetValue(CFSTR("AutoLaunchedApplicationDictionary"), arrM, CFSTR("loginwindow"), kCFPreferencesCurrentUser, kCFPreferencesAnyHost);
	CFPreferencesSynchronize(CFSTR("loginwindow"), kCFPreferencesCurrentUser, kCFPreferencesAnyHost);
	CFRelease(app);
	
	//-----------------------------------------
	return true;
}

static void CreateDesktopAlias()
{
	//-------------------------------------------
	// Make paths
	
	std::string strPath(g_Path);
	for (int i=0; i<3; i++)
	{
		int nPos;
		if ((nPos = strPath.rfind('/')) != std::string::npos)
		{
			strPath.erase(nPos);
		}
	}
	std::string strAppPath(strPath);
	std::string strIconsFullName = strPath + std::string("/Contents/Resources/mfo.icns");
	std::string strAliasPath = std::string(getenv("HOME")) + std::string("/Desktop");
	std::string strAliasName = "MFOPlugins";
	std::string strAliasFullName = strAliasPath + std::string("/") + strAliasName;
	
	OSStatus err;
	
	//-------------------------------------------
	// Try to delete old alias
	
	FSRef aliasItemRef;
    err = FSPathMakeRef((const UInt8 *)strAliasFullName.c_str(), &aliasItemRef, NULL);
    if (err == noErr) {
    	err = FSDeleteObject(&aliasItemRef);
    }
	
	//-------------------------------------------
	// Create resource file
	
    FSRef destinationFolderRef;
   	err = FSPathMakeRef((const UInt8 *)strAliasPath.c_str(), &destinationFolderRef, NULL);
   	if (err != noErr) return;
    	
    CFStringRef destFileName = CFStringCreateWithCString(NULL, strAliasName.c_str(), kCFStringEncodingUTF8);
    HFSUniStr255 destinationName;
	destinationName.length = CFStringGetLength(destFileName);
	CFRange range = CFRangeMake(0, destinationName.length);
    CFStringGetCharacters(destFileName, range, destinationName.unicode);
    
	FSCatalogInfo catalogInfo;
	((FileInfo*)&catalogInfo.finderInfo)->fileType = 0;
	((FileInfo*)&catalogInfo.finderInfo)->fileCreator = 0;
	((FileInfo*)&catalogInfo.finderInfo)->finderFlags = kIsAlias|kHasBundle|kHasCustomIcon;
	((FileInfo*)&catalogInfo.finderInfo)->location.h = 0;
	((FileInfo*)&catalogInfo.finderInfo)->location.v = 0;
	((FileInfo*)&catalogInfo.finderInfo)->reservedField = 0;
	
    HFSUniStr255 resourceForkName;
	err = FSGetResourceForkName(&resourceForkName);
    if (err != noErr) return;
    
	FSRef destinationItemRef;
	err = FSCreateResourceFile(
			&destinationFolderRef,
			destinationName.length,
			destinationName.unicode,
			kFSCatInfoFinderInfo,
			&catalogInfo,
			resourceForkName.length,
			resourceForkName.unicode,
			&destinationItemRef,
			NULL);
    if (err != noErr) return;
    
	SInt16 aliasFileRefNum;
	err = FSOpenResourceFile(
			&destinationItemRef,
			resourceForkName.length,
			resourceForkName.unicode,
			fsRdWrPerm,
			&aliasFileRefNum);
    if (err != noErr) return;
			
	UseResFile(aliasFileRefNum);
	
	//-------------------------------------------
	// Add alias resource
	
	FSRef originalItemRef;
    err = FSPathMakeRef((const UInt8 *)strAppPath.c_str(), &originalItemRef, NULL);
    if (err == noErr)
    {
	   	AliasHandle aliasHandle;
   		err  = FSNewAlias(&destinationFolderRef, &originalItemRef, &aliasHandle);
   		if (err == noErr)
   		{
			AddResource((Handle)aliasHandle, 'alis', 0, "\p");
		}
	}
    
	//-------------------------------------------
	// Add icons resource
	
    struct stat fst;
    err = stat(strIconsFullName.c_str(), &fst);
    if (err == noErr)
    {
    	int fsize = (int)fst.st_size;
	    if (fsize > 0)
	    {
		    unsigned char * buf = new unsigned char[fsize];
		    if (buf != NULL)
		    {
   				FILE * fd = fopen(strIconsFullName.c_str(), "r");
   				if (fd != NULL)
   				{
					fread(buf, fsize, 1, fd);
					fclose(fd);
					
					Handle icnsResource = NewHandle(fsize);
					HLock(icnsResource);
					memcpy(*icnsResource, buf, fsize);
					HUnlock(icnsResource);
					AddResource(icnsResource,'icns',-16455,"\p");	
				}
   				delete[] buf;
   			}
   		}
   	}
				
	//-------------------------------------------
	
	CloseResFile(aliasFileRefNum);
}

static void ShowApp(bool bShow)
{
	EventTargetRef target = GetApplicationEventTarget();
	if (target != NULL)
	{
		EventRef event;
		CreateEvent(NULL, kEventClassCommand, kEventCommandProcess, 0, kEventAttributeNone, &event);
		if (event != NULL)
		{
			SetEventParameter(event, kEventParamPostTarget, typeEventTargetRef, sizeof(EventTargetRef), &target);
			
			HICommandExtended cmd;
			memset(&cmd, 0, sizeof(cmd));
			cmd.commandID = (bShow == true ? kHICommandShowAll : kHICommandHide);
			SetEventParameter(event, kEventParamDirectObject, typeHICommand, sizeof(cmd), &cmd);
			
			SendEventToEventTarget(event, target);
		}
	}
}




