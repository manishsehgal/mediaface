#ifndef _INTERFACES_H_
#define _INTERFACES_H_

struct ICommandManager
{
    virtual void Trace(const char * format, ...) = 0;
    virtual void Terminate(int nAction) = 0;
    virtual std::string ProcessRequest(std::string strCmd) = 0;
};

#endif _INTERFACES_H_
