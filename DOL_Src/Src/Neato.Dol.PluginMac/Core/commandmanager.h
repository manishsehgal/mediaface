#ifndef _COMMAND_MANAGER_H_
#define _COMMAND_MANAGER_H_

#include <vector>
#include <map>
#include <string>

#include "xmlhelper.h"
#include "Interfaces.h"


const UInt32 kEventClassUser = 'user';
const UInt32 kEventUserTrace = 1001;
const UInt32 kEventParamTraceMsg = 'tmsg';


struct CCommand
{
public:
	std::string m_strName;
    bool m_bCopyRequest;
    bool m_bSelf;
};


struct CModule
{
    std::string m_strName;
    std::string m_strFileName;
	int m_nVersion; // 1,2,3...
	bool m_bNeedReinstall;
    std::vector<CCommand> m_vCommands;
};


class CCommandManager : public ICommandManager
{
public:
    CCommandManager(const char * path, WindowRef mainWindow);
    ~CCommandManager();

public:
    // ICommandManager
    void Trace(const char * format, ...);
    void Terminate(int nAction);
    std::string ProcessRequest(std::string strCmd);

public:
    bool InitModules();
	void DoTermAction();

private:
	bool      ParseModuleInfo(const char * strInfo, CModule & module);
    void      AddModule(const char * strFileName);
    void      AddSelfModule();
	CModule * FindModule(const char * strName);
	bool      FindCommand(const char * strCmdName, int & nModInd, int & nCmdInd);
	xmlDoc *  InvokeCommand(const char * cmdName, xmlNode * reqElem, bool & bCopyRequest);
	xmlDoc *  InvokeSelfCommand(const char * cmdName, xmlNode * reqElem);
	xmlDoc *  Command_IsModuleAvailable(xmlNode * reqElem);
	xmlDoc *  Command_GetUpdateInfo(xmlNode * reqElem);
	xmlDoc *  Command_UpdateFiles(xmlNode * reqElem);
	void      CheckFileInfo(xmlNode * fileElem);
	int       CalcSum(const char * fileName, std::string & strSum);
    bool      saveFile(const char * fileName, const char * data);
	void      reload();

private:
	WindowRef m_mainWindow;
	std::string m_strExeName;
	std::string m_strPath;
	std::string m_strAppPath;
    std::vector<CModule> m_vModules;
    std::map<std::string, std::string> m_mapFileNames;
	int m_nTermAction;
};


#endif _COMMAND_MANAGER_H_
