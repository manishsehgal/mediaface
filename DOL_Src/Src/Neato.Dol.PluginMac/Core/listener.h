#ifndef _LISTENER_H_
#define _LISTENER_H_

#define BUF_SIZE 512

class CListener
{
public:
    CListener(ICommandManager * pCmdMgr);
    ~CListener();

    static void * ListenerProc(void * pArg);
    void Listen();
    void Respond(int s);
	bool IsBusy();

private:
	std::string ReadFromSocket(int s);

private:
	int m_listener;
    unsigned short m_nPort;
    ICommandManager * m_pCmdMgr;
	bool m_bBusy;
    //HANDLE m_hShutdownEvent;
    //HANDLE m_hListenerThread;
};

#endif //_LISTENER_H_