#include "PlayListTrackEx.h"
#include "PlayListEx.h"

using namespace std;

CPlayListEx::CPlayListEx()
{
    m_lDuration = 0;
    m_bLongFormat = false;
}

int CPlayListEx::GetTrackCount()
{
    return (int)m_vTracks.size();
}

CPlayListTrackEx & CPlayListEx::GetTrack(int nPos)
{
    if (nPos < 0 || nPos >= (int)m_vTracks.size()) throw 0;
    return m_vTracks[nPos];
}

int CPlayListEx::AddEmptyTrack(int nPos)
{
    if (nPos < 0 || nPos >= (int)m_vTracks.size()) 
    {
        nPos = (int)m_vTracks.size();
    }

    CPlayListTrackEx emptyTrack;
    m_vTracks.insert(m_vTracks.begin() + nPos, emptyTrack);
    return nPos;
}

xmlNode* CPlayListEx::GetXml()
{
    xmlNode* pPlayListEl = xmlNewNode(NULL, BAD_CAST "PlayList");
	
    char buf[128];
    sprintf(buf, "%d", m_vTracks.size());
    xmlSetProp(pPlayListEl, BAD_CAST "TrackCount", BAD_CAST buf);
    xmlSetProp(pPlayListEl, BAD_CAST "Artist", BAD_CAST "");
    xmlSetProp(pPlayListEl, BAD_CAST "Title", BAD_CAST "");
    xmlSetProp(pPlayListEl, BAD_CAST "Album", BAD_CAST "");
    xmlSetProp(pPlayListEl, BAD_CAST "Duration", m_bLongFormat ? BAD_CAST m_strLDuration.c_str() : BAD_CAST m_strSDuration.c_str());
    xmlSetProp(pPlayListEl, BAD_CAST "SrcType", BAD_CAST m_strSrcType.c_str());
    xmlSetProp(pPlayListEl, BAD_CAST "SrcName", BAD_CAST m_strSrcName.c_str());

    xmlNode* pTracksEl = xmlNewChild(pPlayListEl, NULL, BAD_CAST "Tracks", NULL);
    for (int i=0; i<(int)m_vTracks.size(); i++)
    {
        CPlayListTrackEx & track = m_vTracks[i];
        xmlNode* pTrackEl = track.GetXml();
        if (pTrackEl == NULL)
            continue;

        sprintf(buf, "%d", i+1);
        xmlSetProp(pTrackEl, BAD_CAST "Id", BAD_CAST buf);
        xmlAddChild(pTracksEl, pTrackEl);
    }

    return pPlayListEl;
}