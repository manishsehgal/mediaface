#pragma once

#include <string>
#include <vector>

class CFolderContent
{
private:
    std::string m_strPath;
    std::vector<std::string> m_vFolders;
    std::vector<std::string> m_vFiles;

public:
    bool Init(const char* cPath, int nFlags);
    std::string GetPath();
    std::vector<std::string> GetFolders();
    std::vector<std::string> GetFiles();
    xmlNode* GetXml();

private:
    bool checkExt(const char* wcfn, int nFlags);
};
