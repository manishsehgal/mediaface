#include <string>

#include "../Common/xmlhelper.h"
#include "FileList.h"
#include "FileHandler.h"
//#include "QuickTimeHelper.h"

CFileHandler * g_pFileHandler = 0;

extern "C"
bool mod_init(bool bInit)
{
	if (g_pFileHandler) {
        delete g_pFileHandler; 
        g_pFileHandler=0;
    }

	if(bInit) {
		g_pFileHandler = new CFileHandler();
	}
	
	return true;
}

extern "C"
const char * mod_getinfo() 
{
	return 
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
		"<ModuleInfo Version=\"1\" Name=\"FileListModule\" Description=\"Module for import playlists from files\" Filename=\"modcdtext.dylib\">"
			"<Commands>"
				"<Command Name=\"FILELIST_GET_PLAYLIST\" />"
				"<Command Name=\"FILELIST_GET_FOLDER_CONTENT\" />"
			"</Commands>"
		"</ModuleInfo>";
}

extern "C"
xmlDoc * mod_invoke(xmlNode * reqElem)
{
	try
	{
		char * cmdName = (char*)xmlGetProp(reqElem, BAD_CAST "Name");
		if (cmdName == NULL) throw 1;
		std::string strCmdName = cmdName;
		xmlFree(cmdName);
		/*
		if(!isQuickTimeRunning()){
			std::string strResp = "<Response Error=\"1\" ErrorDesc=\"";
	        strResp += "To import an playlist, the Quick Time application has to be running first.";
        	strResp +="\"></Response>";
			xmlDoc * respDoc = xmlReadMemory(strResp.c_str(), strResp.size(), NULL, NULL, 0);
			return respDoc;
		}
		*/
		if (strcmp(strCmdName.c_str(), "FILELIST_GET_PLAYLIST") == 0)
		{
			return g_pFileHandler->CreatePlayList(reqElem);
		}
		else if (strcmp(strCmdName.c_str(), "FILELIST_GET_FOLDER_CONTENT") == 0)
		{
			return g_pFileHandler->GetFolderContent(reqElem);
		}
		else
		{
			return NULL;
		}
	}
	catch(...)
	{
		return NULL;
	}
	
	return NULL;
}