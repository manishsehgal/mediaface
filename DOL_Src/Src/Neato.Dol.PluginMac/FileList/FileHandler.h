#pragma once

class CFileHandler
{
public:
    xmlDoc* CreatePlayList(xmlNode *xmlRequest);
    xmlDoc* GetFolderContent(xmlNode *xmlRequest);

private:
};