#pragma once

#include <string>
#include <vector>
#include "PlayListTrackEx.h"

class CPlayListEx
{
public:
    CPlayListEx();

protected:
    std::vector<CPlayListTrackEx> m_vTracks;
    long m_lDuration;
    std::string m_strSDuration;
    std::string m_strLDuration;
    bool m_bLongFormat;
    std::string m_strSrcType;
    std::string m_strSrcName;

public:
    int GetTrackCount();
    CPlayListTrackEx& GetTrack(int nPos);
    int AddEmptyTrack(int nPos = -1);
    xmlNode* GetXml();
};
