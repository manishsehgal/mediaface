#pragma once

#include "PlayListEx.h"

class CPlayListFL: public CPlayListEx
{
public:
    CPlayListFL();

public:
    bool AddFile(const char* cFile, bool bAddSubFiles, int nPos = -1, int * pnPos = 0, int * pnCount = 0);
    bool AddFolder(const char* cFolder, int nFlags, int nPos = -1, int * pnPos = 0, int * pnCount = 0);
    bool AddPlayList(CPlayListFL & pl, int nPos = -1);

private:
    bool parseQTMovie(const char* cFile, CPlayListTrackEx & track);
};