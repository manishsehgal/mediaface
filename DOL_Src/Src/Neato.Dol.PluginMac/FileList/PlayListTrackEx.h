#pragma once

#include <string>
#include "../Common/xmlhelper.h"

class CPlayListTrackEx
{
public:
    public:
    CPlayListTrackEx();

    void SetTitle(const char* cVal);
	const char* GetTitle();

    void SetArtist(const char* cVal);
	const char* GetArtist();

    void SetAlbum(const char* cVal);
	const char* GetAlbum();

    void SetUrl(const char* cVal);
	const char* GetUrl();

    void SetDuration(long lVal);
	long GetDuration();
	
	void SetLongFormat(bool bVal);
    xmlNode* GetXml();

private:
    std::string m_strTitle;
    std::string m_strArtist;
    std::string m_strAlbum;
    std::string m_strUrl;
    long m_lDuration;
    std::string m_strSDuration;
    std::string m_strLDuration;
    bool m_bLongFormat;
};