#include "../Common/xmlhelper.h"
#include "PlayListTrackEx.h"

using namespace std;

CPlayListTrackEx::CPlayListTrackEx()
{
    m_lDuration = 0;
    m_bLongFormat = false;
}

void CPlayListTrackEx::SetTitle(const char* cVal)
{
    if (!cVal) return;
    m_strTitle = cVal;
}

const char* CPlayListTrackEx::GetTitle()
{
    return m_strTitle.c_str();
}

void CPlayListTrackEx::SetArtist(const char* cVal)
{
    if (!cVal) return;
    m_strArtist = cVal;
}

const char* CPlayListTrackEx::GetArtist()
{
    return m_strArtist.c_str();
}

void CPlayListTrackEx::SetAlbum(const char* cVal)
{
    if (!cVal) return;
    m_strAlbum = cVal;
}

const char* CPlayListTrackEx::GetAlbum()
{
    return m_strAlbum.c_str();
}

void CPlayListTrackEx::SetUrl(const char* cVal)
{
    if (!cVal) return;
    m_strUrl = cVal;
}

const char* CPlayListTrackEx::GetUrl()
{
    return m_strUrl.c_str();
}

void CPlayListTrackEx::SetDuration(long lVal)
{
    m_lDuration = lVal;
	if (m_lDuration > 0) {
		char buf[128];
		sprintf(buf, "%02d:%02d", m_lDuration/60, m_lDuration%60);
		m_strSDuration = buf;
		sprintf(buf, "%02d:%02d:%02d", m_lDuration/3600, (m_lDuration%3600)/60, m_lDuration%60);
		m_strLDuration = buf;
	} else {
		m_strSDuration = "";
		m_strLDuration = "";		
	}	
}

long CPlayListTrackEx::GetDuration()
{
    return m_lDuration;
}

xmlNode* CPlayListTrackEx::GetXml()
{
    xmlNode* pTrackEl = xmlNewNode(NULL, BAD_CAST "Track");
   	xmlSetProp(pTrackEl, BAD_CAST "Artist", BAD_CAST m_strArtist.c_str());
    xmlSetProp(pTrackEl, BAD_CAST "Title", BAD_CAST m_strTitle.c_str());
    xmlSetProp(pTrackEl, BAD_CAST "Album", BAD_CAST m_strAlbum.c_str());
    xmlSetProp(pTrackEl, BAD_CAST "Url", BAD_CAST m_strUrl.c_str());
    xmlSetProp(pTrackEl, BAD_CAST "Duration", m_bLongFormat ? BAD_CAST m_strLDuration.c_str() : BAD_CAST m_strSDuration.c_str());

    return pTrackEl;
}

