#include <dirent.h>
#include <string.h>

#include <algorithm>
#include <Carbon/Carbon.h>
using namespace std;

#include "../Common/xmlhelper.h"
#include "FolderContent.h"
#include "Constants.h"

bool StrLess(const string & str1, const string & str2)
{
	int value = strcasecmp(str1.c_str(), str2.c_str());
	return (value < 0);
};

bool CFolderContent::Init(const char* cPath, int nFlags)
{
    string strPath(cPath);
    m_vFolders.clear();
    m_vFiles.clear();

	//replace left slashes
    replace(strPath.begin(), strPath.end(), '\\', '/');
	
	//if folder up needed
    string::size_type pos = strPath.rfind("/..");
    if (pos == strPath.size()-3) {
        strPath.erase(pos);
		pos = strPath.rfind('/');
        if (pos != wstring::npos)
			strPath.erase(pos+1);
    }
	
	//take default start folder
    if (strPath.size() == 0)
		strPath = getenv("HOME");    
	
    //append ending slash if needed
    if (strPath[strPath.size()-1] != L'/') 
        strPath += "/";
    
    //check if this is correct folder path
	DIR * pDir = opendir(strPath.c_str());
	if (pDir == NULL) return false;

    m_strPath = strPath;
	
	struct dirent * pDirEnt = 0;
	while (true)
	{
		pDirEnt = readdir(pDir);
		if (pDirEnt != NULL)
		{
			if (strncmp(pDirEnt->d_name, ".", 1) == 0)
				continue;
				
			if (pDirEnt->d_type == DT_DIR)
			{
				if (nFlags & FFFD)
				{
					m_vFolders.push_back(pDirEnt->d_name);
				}
			}
			else
			{
                if (nFlags & (FFPL|FFSF))
                {
                    if (checkExt(pDirEnt->d_name, nFlags))
                    {
                        m_vFiles.push_back(pDirEnt->d_name);
                    }
                }
            }
		}
		else
		{
			break;
		}
	}
	closedir(pDir);
	
    std::sort(m_vFolders.begin(), m_vFolders.end(), StrLess);
    
    /*
    {
    	FSRef dir;
	    OSStatus err = FSPathMakeRef((const UInt8 *)strPath.c_str(), &dir, NULL);
	    if (err == noErr) {
	    	FSIterator it;
			err = FSOpenIterator(&dir, kFSIterateFlat, &it);
		    if (err == noErr) {
		    	while (true) {
		    		ItemCount cnt;
					FSCatalogInfo catInfo;
					FSRef ref;
    				HFSUniStr255 nameStr;
    				err = FSGetCatalogInfoBulk (it, 1, &cnt, NULL, kFSCatInfoNodeFlags, &catInfo, &ref, NULL, &nameStr);
				    if (err != noErr) break;
				    
				    CFStringRef name;
                	name = CFStringCreateWithCharacters(NULL, nameStr.unicode, nameStr.length);
                	
                	char * buf = (char*)alloca(nameStr.length * 2);
                	CFStringGetCString(name, buf, nameStr.length * 2, kCFStringEncodingUTF8);
                	CFRelease(name);
                	
                	if ((catInfo.nodeFlags & kFSNodeIsDirectoryMask) != 0) {
           	printf("Dir:  %s\n", buf);
                		if ((nFlags & FFFD) && (buf[0] != '.')) {
							m_vFolders.push_back(std::string(buf));
						}
                	} else {
		                if ((nFlags & (FFPL|FFSF)) && (buf[0] != '.')) {
		                    if (checkExt(buf, nFlags)) {
								m_vFiles.push_back(std::string(buf));
           	printf("File: %s - %02x %02x %02x %02x %02x %02x\n", buf, (unsigned char)buf[0], (unsigned char)buf[1], (unsigned char)buf[2], (unsigned char)buf[3], (unsigned char)buf[4], (unsigned char)buf[5]);
           	printf("File: %s - %04x %04x %04x %04x\n", buf, nameStr.unicode[0], nameStr.unicode[1], nameStr.unicode[2], nameStr.unicode[3]);
		                    }
		                }
                	}
                	
    			}
		    	FSCloseIterator(it);
		    }
		}
    	std::sort(m_vFolders.begin(), m_vFolders.end(), StrLess);
    	std::sort(m_vFiles.begin(),   m_vFiles.end(),   StrLess);
	}
	*/
	
    return true;
}

string CFolderContent::GetPath()
{
    return m_strPath;
}

vector<string> CFolderContent::GetFolders()
{
    return m_vFolders;
}

vector<string> CFolderContent::GetFiles()
{
    return m_vFiles;
}

xmlNode* CFolderContent::GetXml()
{
	xmlNode* pFCEl = xmlNewNode(NULL, BAD_CAST "FolderContent");
    xmlSetProp(pFCEl, BAD_CAST "Path", BAD_CAST m_strPath.c_str());

    xmlNode* pFoldersEl = xmlNewChild(pFCEl, NULL, BAD_CAST "Folders", NULL);
    for (int i=0; i<(int)m_vFolders.size(); i++)
    {
        xmlNode* pFolderEl = xmlNewChild(pFoldersEl, NULL, BAD_CAST "Folder", NULL);
        xmlSetProp(pFolderEl, BAD_CAST "Name", BAD_CAST m_vFolders[i].c_str());
    }

    xmlNode* pFilesEl = xmlNewChild(pFCEl, NULL, BAD_CAST "Files", NULL);
	for (int i=0; i<(int)m_vFiles.size(); i++)
    {
        xmlNode* pFileEl = xmlNewChild(pFilesEl, NULL, BAD_CAST "File", NULL);
        xmlSetProp(pFileEl, BAD_CAST "Name", BAD_CAST m_vFiles[i].c_str());
    }

    return pFCEl;
}


//file extensions:
const char * plExt[] = {".m3u",  ".pls",  ".asx",  ".wpl"};
const char * sfExt[] = {".aac",  ".adts", ".ac3",  ".aif",
                        ".aiff", ".aifc", ".aiff", ".caf",
                        ".mp3",  ".mp4",  ".m4a",  ".snd",
                        ".au",   ".sd2",  ".wav"};

bool CFolderContent::checkExt(const char* cfn, int nFlags)
{
    string fn = cfn;
    string::size_type pos = fn.rfind('.');
    if (pos != string::npos)
    {
        string ext = fn.substr(pos);
        if (nFlags & FFPL)
        {
        	for (int i=0; i<sizeof(plExt)/sizeof(plExt[0]); i++)
        	{
        		if (strcasecmp(ext.c_str(), plExt[i]) == 0) return true;
        	}
        }
        if (nFlags & FFSF)
        {
        	for (int i=0; i<sizeof(sfExt)/sizeof(sfExt[0]); i++)
        	{
        		if (strcasecmp(ext.c_str(), sfExt[i]) == 0) return true;
        	}
        }
    }
	
    return false;
}

