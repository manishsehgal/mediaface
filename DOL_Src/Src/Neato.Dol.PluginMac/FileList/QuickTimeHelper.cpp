#include <AudioToolbox/AudioFile.h>
#include "QuickTimeHelper.h"
#include <QuickTime/QuickTime.h>
#include <string>
using namespace std;

bool checkExt(const char* cfn)
{
    string fn = cfn;
    string::size_type pos = fn.rfind('.');
    if (pos != string::npos)
    {
        string ext = fn.substr(pos);
        if (strcasecmp(ext.c_str(), ".aac") == 0)
			return true;
		if (strcasecmp(ext.c_str(), ".m4a") == 0)
			return true;
    }
	
    return false;
}

bool GetTrackDataFromQuickTime(const char *path, CPlayListTrackEx & track);

bool GetTrackDataFromAudioFile(const char *path, CPlayListTrackEx & track)
{
	if (checkExt(path))
		return GetTrackDataFromQuickTime(path, track);

	OSStatus err = noErr;
	
	FSRef fileRef;
	err = FSPathMakeRef((const UInt8 *)path, &fileRef, 0);
	if (err == noErr)
	{
		AudioFileID fileId;
		err = AudioFileOpen(&fileRef, fsRdPerm, 0, &fileId);
		if (err == noErr)
		{
			CFDictionaryRef dic = NULL;
			UInt32 size = sizeof(dic);
			err = AudioFileGetProperty(fileId, kAudioFilePropertyInfoDictionary, &size, &dic);
			if (err == noErr && dic != NULL)
			{
				char buf[256];
				CFStringRef strTitle = (CFStringRef)CFDictionaryGetValue(dic, CFSTR(kAFInfoDictionary_Title));
				if (strTitle != NULL)
				{
					CFStringGetCString(strTitle, buf, 256, kCFStringEncodingUTF8);
					track.SetTitle(buf);
				}
				CFStringRef strArtist = (CFStringRef)CFDictionaryGetValue(dic, CFSTR(kAFInfoDictionary_Artist));
				if (strArtist != NULL)
				{
					CFStringGetCString(strArtist, buf, 256, kCFStringEncodingUTF8);
					track.SetArtist(buf);
				}
				CFStringRef strAlbum = (CFStringRef)CFDictionaryGetValue(dic, CFSTR(kAFInfoDictionary_Album));
				if (strAlbum != NULL)
				{
					CFStringGetCString(strAlbum, buf, 256, kCFStringEncodingUTF8);
					track.SetAlbum(buf);
				}
				CFStringRef strDur = (CFStringRef)CFDictionaryGetValue(dic, CFSTR(kAFInfoDictionary_ApproximateDurationInSeconds));
				if (strDur != NULL)
				{
					track.SetDuration(CFStringGetIntValue(strDur));
				}
				CFRelease(dic);
			}
			AudioFileClose(fileId);
		}
	}
	
	if (strlen(track.GetTitle()) == 0)
	{
		char * fn = (char*)alloca(strlen(path)+1);
		if (fn != NULL)
		{
			strcpy(fn, path);
			char * pos = strrchr(fn, '/');
			pos = (pos != NULL) ? pos+1 : fn;
			char * ext = strrchr(pos, '.');
			if (ext != NULL) *ext = 0;
			track.SetTitle(pos);
		}
	}
	
	if (track.GetDuration() == 0)
	{
		track.SetDuration(0);
	}
	
	return true;
}

OSStatus CreateMovieFromAudioFile(char *path, Movie *outMovie, short *outFileRef);
void DumpMovieMetaData(Movie theMovie, CPlayListTrackEx & track);

bool GetTrackDataFromQuickTime(const char *path, CPlayListTrackEx & track)
{
	Movie		movie = NULL;
	short		movieFile = -1;
	OSStatus	err = noErr;
	
	// Get a Movie object.
	err = CreateMovieFromAudioFile((char*)path, &movie, &movieFile);
	if (err != noErr)
		return false;

	DumpMovieMetaData(movie, track);
			
	// Global Time Information
	long ts = GetMovieTimeScale(movie);
	long tv = GetMovieDuration(movie);
	long seconds = tv / ts;
	track.SetDuration(seconds);
 	
	return true;
}

// --------------------------------------------------------------
//	CreateMovieFromAudioFile
// --------------------------------------------------------------
//
OSStatus CreateMovieFromAudioFile(char *path, Movie *outMovie, short *outFileRef)
{
	OSStatus				err = noErr;
	FSRef					ref = {};
	FSSpec					spec = {};
	short					movieFile = -1;
	Movie					movie = NULL;
	
	// It's important to call EnterMovies before doing anything.
	err = EnterMovies();
	if (err) goto done;
	
	// Convert the path into an FSRef.
	const UInt8* p = (const UInt8*)path;
	err = FSPathMakeRef(p, &ref,NULL);
	if (err) goto done;
	
	// Convert the FSRef into an FSSpec.
	err = FSGetCatalogInfo(&ref,kFSCatInfoNone,NULL,NULL,&spec,NULL);
	if (err) goto done;
	
	// Open the FSSpec.
	err = OpenMovieFile(&spec,&movieFile,fsRdPerm);
	if (err) goto done;
	
	// Attempt to instantiate a Movie from the file.
	err = NewMovieFromFile(&movie,movieFile,NULL,NULL,0,NULL);
	if (err) goto done;
	if (movie == NULL) goto done;
	
	// We've successfully created the Movie - copy our local variables back
	//	to the caller.
	*outMovie = movie;
	*outFileRef = movieFile;
	movieFile = -1;
	movie = NULL;
	
done:
	if (movie != NULL)
		DisposeMovie(movie);
	if (movieFile != -1)
		CloseMovieFile(movieFile);
	return err;
}

// an array of all the metadata item properties we want to display
OSType gPropTypesArray[] = {
	//kQTMetaDataItemPropertyID_StorageFormat,
	kQTMetaDataItemPropertyID_Key,
	//kQTMetaDataItemPropertyID_KeyFormat,
	//kQTMetaDataItemPropertyID_Locale,
	//kQTMetaDataItemPropertyID_DataType,
	kQTMetaDataItemPropertyID_Value
};

// calculate size of our metadata item property array
short gPropTypesArraySize = sizeof (gPropTypesArray)/sizeof (OSType);

OSStatus MDUtils_GetItemPropertyValue(	QTMetaDataRef		metaDataRef, 
										QTMetaDataItem		item,
										QTPropertyClass     inPropClass,
										QTPropertyID        inPropID,
										QTPropertyValuePtr	*outValPtr,
										ByteCount			*outPropValueSizeUsed)
{
	QTPropertyValueType outPropType;
	ByteCount			outPropValueSize;
	UInt32				outPropFlags;

	// first get the size of the property
	OSStatus status = QTMetaDataGetItemPropertyInfo (metaDataRef,
													item,
													inPropClass,
													inPropID,
													&outPropType,
													&outPropValueSize,
													&outPropFlags );
 
	// allocate memory to hold the property value
    *outValPtr = malloc(outPropValueSize);
	
	// Return the property of the metadata item.
	status =  QTMetaDataGetItemProperty(metaDataRef,
										item,
										inPropClass,
										inPropID,
										outPropValueSize,
										*outValPtr,
										outPropValueSizeUsed);
	
    // QTMetaDataKeyFormat types will be native endian in our byte buffer, we need
    // big endian so they look correct when we create a string. 
    if (outPropType == 'code' || outPropType == 'itsk' || outPropType == 'itlk') {
    	OSTypePtr pType = (OSTypePtr)*outValPtr;
    	*pType = EndianU32_NtoB(*pType);
    }
    
	return status;
}

void DumpAllPropertiesForMetaDataItem(	QTMetaDataRef metaDataRef,
										QTMetaDataItem item, CPlayListTrackEx & track)
{
	short i;
	
	string strKey;
	string strData;
	for (i = 0 ; i < gPropTypesArraySize; ++i)
	{
		QTPropertyValuePtr	propValuePtr			= nil;
		ByteCount			outPropValueSizeUsed	= 0;

		// for each metadata item we'll iterate over every
		// metadata property and retrieve the property value
		OSStatus status = MDUtils_GetItemPropertyValue( metaDataRef, 
														item,
														kPropertyClass_MetaDataItem, // Metadata Item Property Class ID
														gPropTypesArray[i],	// Metadata Item Property ID
														&propValuePtr,
														&outPropValueSizeUsed);

		// display each item property 
		switch(gPropTypesArray[i])
		{
			case kQTMetaDataItemPropertyID_Key:
                strKey = (char*)propValuePtr;
            break;

			case kQTMetaDataItemPropertyID_Value:
			{
				QTPropertyValuePtr	dataTypeValuePtr = nil;
				ByteCount			ignore = 0;
				
				// get the type of the data
				// we'll need this to know how to display the data properly
				status = MDUtils_GetItemPropertyValue(metaDataRef, 
													item,
													kPropertyClass_MetaDataItem,
													kQTMetaDataItemPropertyID_DataType,
													&dataTypeValuePtr,
													&ignore);
				char buff[256];
				if (outPropValueSizeUsed > 256)
					outPropValueSizeUsed = 256;
				strncpy(buff, (char*)propValuePtr, outPropValueSizeUsed);
				buff[outPropValueSizeUsed] = 0;
				strData = buff;
			}
			break;
		}

		// free memory allocated for property value
		free(propValuePtr);
	}
	if (strKey == "\xa9""nam")
		track.SetTitle(strData.c_str());
	if (strKey == "\xa9""ART")
		track.SetArtist(strData.c_str());
	if (strKey == "\xa9""alb")
		track.SetAlbum(strData.c_str());
}


OSStatus MDUtils_GetNextMetaDataItem(	QTMetaDataRef				metaDataRef, 
										QTMetaDataItem				currentItem,
										QTMetaDataStorageFormat		inMetaDataFormat,
										QTMetaDataKeyFormat			inKeyFormat,
										QTMetaDataItem				*nextItem)
{
	return (QTMetaDataGetNextItem(	metaDataRef,
									inMetaDataFormat,
									currentItem,
									inKeyFormat,
									nil,
									0,
									nextItem));
}

OSStatus MDUtils_GetNextItemForAnyStorageFormat(QTMetaDataRef metaDataRef, QTMetaDataItem currentItem, QTMetaDataItem *nextItem)
{
	return( MDUtils_GetNextMetaDataItem(metaDataRef, 
										currentItem,
										kQTMetaDataStorageFormatWildcard,
										kQTMetaDataKeyFormatWildcard,
										nextItem));
}


void DumpMovieMetaData(Movie theMovie, CPlayListTrackEx & track)
{
	// get movie metadata reference which is necessary to iterate
	// all the metadata items
	QTMetaDataRef metaDataRef;
	OSStatus status = QTCopyMovieMetaData (theMovie, &metaDataRef );
	if (status != noErr)
		return;
		
	// iterate and display all metadata items

	QTMetaDataItem item = kQTMetaDataItemUninitialized;
	// Get the next metadata item, regardless of the type of storage format
	while (noErr == (MDUtils_GetNextItemForAnyStorageFormat(metaDataRef,
						item, &item))) //MDUtils_GetNextItemForAnyStorageFormat
	{
		DumpAllPropertiesForMetaDataItem(metaDataRef, item, track);			
	}

	// we are done so release our metadata object
	QTMetaDataRelease(metaDataRef);	

}