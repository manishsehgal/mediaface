#include <string>
#include <vector>
using namespace std;

#include "../Common/xmlhelper.h"
#include "FolderContent.h"
#include "PlayListFL.h"
#include "Constants.h"
#include "QuickTimeHelper.h"

CPlayListFL::CPlayListFL()
{
    m_strSrcType = "FileList";
}

bool CPlayListFL::AddFile(const char* cFile, bool bAddSubFiles, int nPos, int * pnPos, int * pnCount)
{
    FILE *stream = fopen(cFile, "r");
    if(stream == NULL)
        return false;
    fclose(stream);

    CPlayListFL pl;
    pl.AddEmptyTrack();
    if (!parseQTMovie(cFile, pl.GetTrack(0)))
        return false;
        
    AddPlayList(pl, nPos);    
    return true;
}

bool CPlayListFL::AddFolder(const char* cFolder, int nFlags, int nPos, int * pnPos, int * pnCount)
{
    CFolderContent fc;
    if (!fc.Init(cFolder, nFlags)) return false;

    vector<string> vFiles = fc.GetFiles();
    for (unsigned int i=0; i<vFiles.size(); i++)
    {
        string fn = fc.GetPath() + vFiles[i];
        AddFile(fn.c_str(), true);
    }

    if (nFlags & FFFD)
    {
        vector<string> vFolders = fc.GetFolders();
        for (unsigned int i=0; i<vFolders.size(); i++)
        {
            string fn = fc.GetPath() + vFolders[i];
            AddFolder(fn.c_str(), nFlags);
        }
    }

    return true;
}

bool CPlayListFL::AddPlayList(CPlayListFL & pl, int nPos)
{
    if (nPos < 0 || nPos >= (int)m_vTracks.size()) 
    {
        nPos = (int)m_vTracks.size();
    }
    m_vTracks.insert(m_vTracks.begin()+nPos, pl.m_vTracks.begin(), pl.m_vTracks.end());
    return true;
}

bool GetFileName(const char* cPath, string & strResult)
{
	string strPath(cPath);
	unsigned int index = strPath.find_last_of('/');
	if (index != -1) {
		strResult = strPath.substr(index + 1);
		return true;
	}
	return false;
}


bool CPlayListFL::parseQTMovie(const char* cFile, CPlayListTrackEx & track)
{
	string strUrl;
	if (!GetFileName(cFile, strUrl))
		return false;
		
	track.SetUrl(cFile);
    return GetTrackDataFromAudioFile(cFile, track);
}