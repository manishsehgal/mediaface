#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "../Common/xmlhelper.h"
#include "PlayListFL.h"
#include "FolderContent.h"
#include "FileHandler.h"
#include "Constants.h"

xmlDoc* CFileHandler::CreatePlayList(xmlNode *xmlRequest)
{
	try
	{
		xmlNode * paramsElem = CXmlHelper::GetChildElement(xmlRequest, "Params");
		if (paramsElem == NULL) throw 1;
				
        CPlayListFL playList;
        //parse files
        {
			xmlNode * pFileElem = CXmlHelper::GetChildElement(paramsElem, "File");
			while (pFileElem)
            {
				char * fileName = (char*)xmlGetProp(pFileElem, BAD_CAST "Name");
                if (fileName != NULL) {
					playList.AddFile(fileName, true);
                }

                pFileElem = pFileElem->next;
            }
        }
        //parse folders
        {
			xmlNode * pFolderElem = CXmlHelper::GetChildElement(paramsElem, "Folder");
			while (pFolderElem)
            {
				char * folderName = (char*)xmlGetProp(pFolderElem, BAD_CAST "Name");
				char * subFolders = (char*)xmlGetProp(pFolderElem, BAD_CAST "GetSubFolders");
                bool bGetSubFolders = false;
                if (subFolders != NULL) {
                    bGetSubFolders = (strcmp(subFolders, "1") == 0 || strcasecmp(subFolders, "true") == 0);
                }

                if (folderName != NULL) {
                    playList.AddFolder(folderName, FFSF|(bGetSubFolders?FFFD:0));
                }

                pFolderElem = pFolderElem->next;
            }
        }

		//=============================
		// Prepare response xml
		xmlDoc * respDoc = xmlNewDoc(BAD_CAST "1.0");
		if (respDoc == NULL) throw 2;
		
		xmlNode * pRespElem = xmlNewNode(NULL, BAD_CAST "Response");
		if (pRespElem == NULL) throw 3;
		
		xmlDocSetRootElement(respDoc, pRespElem);
		xmlSetProp(pRespElem, BAD_CAST "Error", BAD_CAST "0");
		
		xmlNode * pParamsElem = xmlNewChild(pRespElem, NULL, BAD_CAST "Params", NULL);
		if (pParamsElem == NULL) throw 4;
		
		//=============================
		xmlNode * pPLElem = playList.GetXml();
		if (pPLElem == NULL) throw 5;

		xmlAddChild(pParamsElem, pPLElem);

		return respDoc;
    }
	catch (...) {
		return NULL;
	}

	return NULL;
}

xmlDoc* CFileHandler::GetFolderContent(xmlNode *xmlRequest)
{
	try
	{
		//=============================
		//Parse request xml
		xmlNode * paramsElem = CXmlHelper::GetChildElement(xmlRequest, "Params");
		if (paramsElem == NULL) throw 1;
		
		xmlNode * pFolderElem = CXmlHelper::GetChildElement(paramsElem, "Folder");
		if (pFolderElem == NULL) throw 2;
		
		char * path = (char*)xmlGetProp(pFolderElem, BAD_CAST "Path");
        if (path == NULL) throw 3;

        bool bGetFolders = true;
		char * getFolders = (char*)xmlGetProp(pFolderElem, BAD_CAST "GetFolders");
        if (getFolders != NULL) {
            bGetFolders = (strcmp(getFolders, "1") == 0 || strcasecmp(getFolders, "true") == 0);
        }

		bool bGetFiles = true;
		char * getFiles = (char*)xmlGetProp(pFolderElem, BAD_CAST "GetFiles");
        if (getFiles != NULL) {
            bGetFiles = (strcmp(getFiles, "1") == 0 || strcasecmp(getFiles, "true") == 0);
        }
		
        //=============================
		// Prepare response xml
		xmlDoc * respDoc = xmlNewDoc(BAD_CAST "1.0");
		if (respDoc == NULL) throw 8;
		
		xmlNode * pRespElem = xmlNewNode(NULL, BAD_CAST "Response");
		if (pRespElem == NULL) throw 9;
		
		xmlDocSetRootElement(respDoc, pRespElem);
		xmlSetProp(pRespElem, BAD_CAST "Error", BAD_CAST "0");
		
		xmlNode * pParamsElem = xmlNewChild(pRespElem, NULL, BAD_CAST "Params", NULL);
		if (pParamsElem == NULL) throw 10;
		
		//=============================

        CFolderContent fc;
        if (fc.Init(path, FFFD|FFPL|FFSF))
        {
			xmlNode * pFCElem = fc.GetXml();
			if (pRespElem != NULL)
            {
        		xmlAddChild(pParamsElem, pFCElem);
            }
        }

		return respDoc;
    }
    catch (...) {
		return NULL;
	}

	return NULL;
}

