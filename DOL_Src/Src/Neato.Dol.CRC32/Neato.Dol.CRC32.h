// NeatoDolCRC32.h

#pragma once

using namespace System;

namespace NeatoDolCRC32
{
    public __gc class ValidateUtil
    {
    public:
        static bool CheckSerialNumber
            (String __gc* pszSerNum1,
            String __gc* pszSerNum2,
            String __gc* pszSerNum3,
            String __gc* pszSerNum4);
    };
}
