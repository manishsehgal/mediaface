BOOL GenerateKey(TCHAR cAlgorithm, LPCTSTR pszString, LPTSTR pszKey);
BOOL VerifyKey(LPCTSTR pszString, LPCTSTR pszKey, BOOL& bIsEqul);
BOOL CheckSerialNumberValid (LPCTSTR pszSerNum1, LPCTSTR pszSerNum2, LPCTSTR pszSerNum3, LPCTSTR pszSerNum4);
BOOL PackNumber(unsigned long Number, LPTSTR strString, int MaxAllowedLength, int MinAllowedLength);

///////////////////////////////////////////////////////////////

BOOL VerifyKeyOldFormat(LPCTSTR pszString, LPCTSTR pszKey, BOOL& bIsEqul);

