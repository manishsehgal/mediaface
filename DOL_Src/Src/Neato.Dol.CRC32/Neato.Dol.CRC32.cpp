// This is the main DLL file.

#include "stdafx.h"

#include "Neato.Dol.CRC32.h"
#include <string>
#include <atlconv.h>

#include "CRC32.h"

void MarshalString ( System::String* s, std::string& os )
{
   using namespace System::Runtime::InteropServices;
   const char* chars = 
      (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
   os = chars;
   Marshal::FreeHGlobal(System::IntPtr((void*)chars));
}


bool NeatoDolCRC32::ValidateUtil::CheckSerialNumber
(String __gc* pszSerNum1,
 String __gc* pszSerNum2,
 String __gc* pszSerNum3,
 String __gc* pszSerNum4) {
     USES_CONVERSION;
    
     std::string s1, s2, s3, s4;
     MarshalString(pszSerNum1, s1);
     MarshalString(pszSerNum2, s2);
     MarshalString(pszSerNum3, s3);
     MarshalString(pszSerNum4, s4);

     int i = CheckSerialNumberValid(
         A2CT(s1.c_str()),
         A2CT(s2.c_str()),
         A2CT(s3.c_str()),
         A2CT(s4.c_str()));
     bool b = i > 0;
     return b;
 }
