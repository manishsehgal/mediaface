<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:template match="/">
<html>
  <head>
    <title>Unit testing results</title>
  </head>
  <body>
    <xsl:apply-templates select="test-results"/>
  </body>
  </html>
</xsl:template>

<xsl:template match="test-results">
  <xsl:if test="not (@failures = 0)">
    <font size="5" color="Red">
        Test failed!
    </font>
  </xsl:if>
  <xsl:if test="@failures = 0">
    <font size="5" color="Green">
        Test Succesful!
    </font>
  </xsl:if>
  <br/>
  <font size="4" color="Black">
    <xsl:value-of select="@name"/>
  </font>
  <br/>
  <font size="3" color="Black">
    <br/>
    <b>Total Tests: </b><xsl:value-of select="@total"/>
    <br/>
    <b>Failed: </b><xsl:value-of select="@failures"/>
    <br/>
    <b>Skipped:  </b><xsl:value-of select="@not-run"/>
    <br/>
  </font>
  <xsl:apply-templates select="test-suite"/>
    <br/>
  <font size="2" color="Black">
    <b>Test started:  </b><xsl:value-of select="@date"/> at <xsl:value-of select="@time"/>
  </font>
</xsl:template>

<xsl:template match="test-suite">
  <p/>
  <table border="1" cellspacing="0" bordercolor="silver" cellpadding="3">
   <tr>
    <td align="center"><b>Test Case</b></td>
    <td align="center"><b>Execute results</b></td>
    <td align="center"><b>Comments</b></td>
   </tr>
  <xsl:apply-templates select="//test-case"/>
  </table>
    <br/>
  <font size="2" color="Black">
    <b>Total time:  </b><xsl:value-of select="@time"/>
  </font>
</xsl:template>

<xsl:template match="test-case">
   <tr>
    <td align="left">
    <font size="2" color="Black" face="Arial">
      <xsl:value-of select="substring-after(substring-after(substring-after(@name,'.'),'.'),'.')"/>
    </font>
    </td>
    <xsl:choose>
        <xsl:when test = "@executed='True' and @success='True'">
          <td align="left"><font size="2" color="Green" face="Arial"><b>
            Succesful
          </b></font></td>
          <td align="left"></td>
        </xsl:when>  
        <xsl:when test = "@executed='True' and @success='False'">
          <td align="left"><font size="2" color="Red" face="Arial"><b>
            Failed
          </b></font></td>
          <td align="left">
            <pre><xsl:value-of select="failure/message"/></pre>
            <pre><xsl:value-of select="failure/stack-trace"/></pre>
          </td>
        </xsl:when>  
        <xsl:when test = "@executed='False'">
          <td align="left"><font size="2" color="#CCCC00" face="Arial"><b>
            Skipped
          </b></font></td>
          <td align="left"><xsl:value-of select="reason/message"/></td>
        </xsl:when>  
        <xsl:otherwise>XSLT Script Error</xsl:otherwise>
    </xsl:choose>
   </tr>
</xsl:template>

</xsl:stylesheet> 