cd ../Neato.Dol.PluginService/Configuration
call Build.bat
cd ../../Configuration

cd ../Neato.Dol.PluginMac/Configuration
call Build.bat
cd ../../Configuration

NAnt -t:net-1.1 -f:Build.nant -D:ini.file=Build.ini

@echo off
if NOT ERRORLEVEL 0 exit
if exist Neato.Dol.Test.dll-results.html (
    start Neato.Dol.Test.dll-results.html
) else (
    start logs\Compiler.log
)