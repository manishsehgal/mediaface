<?xml version="1.0" ?>
<project name="Build" default="Finish">
    <!-- ini properties -->
    <fail unless="${property::exists('ini.file')}" message="ini.file property must be set" />
    <foreach item="Line" in="${ini.file}" delim="=" property="key,value" trim="Both">
        <ifnot test="${key == '' or string::starts-with(key, '[') or string::starts-with(key, '//')}">
            <property name="${key}" value="${value}" />
            <echo message="${key} = ${value}" if="false" />
        </ifnot>
    </foreach>
    <property name="app.root.path" value="${path::get-full-path(app.root.path)}" />
    <!-- ini properties -->

    <!-- SCC usage -->
    <property name="scc.used.flag" value="false" unless="${property::exists('scc.used.flag')}" />
    <if test="${scc.used.flag}">
        <p4info user="scc.user" client="scc.client" host="scc.host" root="scc.root" failonerror="false" />
        <echo message="SCC root not found. Probably, no perforce client installed" unless="${property::exists('scc.root')}" />
        <if test="${property::exists('scc.root')}">
            <echo message="SCC root = '${scc.root}'" />
            <ifnot test="${string::starts-with(string::to-lower(app.root.path), string::to-lower(scc.root))}" >
                <fail>Application path is '${app.root.path}', but SCC path is '${scc.root}'</fail>
            </ifnot>
        </if>
    </if>
    <property name="use.scc" value="${property::exists('scc.root') and scc.used.flag}" />
    <!-- SCC usage -->

    <!-- Visual studio location -->
    <property name="devenv.comntools" value="${environment::get-variable('VS71COMNTOOLS')}" if="${environment::variable-exists('VS71COMNTOOLS')}" />
    <property name="devenv.common7"   value="${path::get-directory-name(path::combine(devenv.comntools, 'dummy'))}" if="${property::exists('devenv.comntools')}" />
    <property name="devenv.exe"       value="${path::combine(path::get-directory-name(devenv.common7), 'IDE\devenv.exe')}" if="${property::exists('devenv.common7')}" />
    <echo message="devenv.exe not found" unless="${property::exists('devenv.exe')}" />
    <if test="${property::exists('devenv.exe')}">
        <echo message="devenv.exe = '${devenv.exe}'" if="${property::exists('devenv.exe')}" />
    </if>
    <!-- Visual studio location -->
    
    <!-- Build properties -->
    <property readonly="true" name="solution.file"      value="${path::combine(app.root.path, app.solution.file)}" />
    <property readonly="true" name="flash.build.file"   value="${path::combine(app.root.path, app.flash.build.file)}" />
    <property readonly="true" name="app.src.path"       value="${path::combine(app.root.path, 'Src')}" />
    <property readonly="true" name="app.bin.path"       value="${path::combine(app.root.path, 'Bin')}" />
    <!-- Build properties -->

    <!-- Logging properties -->
    <property readonly="true" name="common.log.file"        value="${path::combine(path::combine(app.root.path, build.logs.path), build.common.log.file)}" />
    <property readonly="true" name="compile.log.file"       value="${path::combine(path::combine(app.root.path, build.logs.path), build.compile.log.file)}" />
    <property readonly="true" name="flash.compile.log.file" value="${path::combine(path::combine(app.root.path, build.logs.path), build.flash.compile.log.file)}" />
    <property readonly="true" name="test.xsl.file"          value="${path::combine(app.root.path, build.test.xsl.file)}" />
    <!-- Logging properties -->

    <!-- NUnit test properties -->
    <property name="test.assembly.file"  value="${path::combine(app.root.path, app.test.path)}" />
    <property name="test.assembly.file"  value="${path::combine(test.assembly.file, 'bin')}" />
    <property name="test.assembly.file"  value="${path::combine(test.assembly.file, build.config)}" />
    <property name="test.assembly.file"  value="${path::combine(test.assembly.file, app.test.assembly.file)}" />
    <property readonly="true" name="test.config.file"   value="${path::combine(app.root.path, app.test.config.file)}" />
    <!-- NUnit test properties -->

    <target name="Logging">
        <mkdir dir="${path::get-directory-name(common.log.file)}" unless="${directory::exists(path::get-directory-name(common.log.file))}" />
        <mkdir dir="${path::get-directory-name(compile.log.file)}" unless="${directory::exists(path::get-directory-name(compile.log.file))}" />
        <delete file="${common.log.file}" if="${file::exists(common.log.file)}" />
        <delete file="${compile.log.file}" if="${file::exists(compile.log.file)}" />
        <delete verbose="true">
            <fileset>
                <include name="${path::get-directory-name(test.xsl.file)}/*-results.html" />
            </fileset>
        </delete>
        
        <record action="Start" autoflush="true" name="${common.log.file}" level="Verbose" />

        <echo message="Logging started." />
    </target>
    
    <target name="Prepare" depends="Logging">
        <echo message="Build log path:   ${common.log.file}" />
        <echo message="Compile log path: ${compile.log.file}" />
        <echo message="App location:     ${app.root.path}" />
        <echo message="Build started." />
        
        <echo message="Project sources:  ${app.src.path}" />

        <echo message="Delete old compiled files..." />
        <delete>
            <fileset>
                <include name="${app.src.path}/*/bin/${build.config}/**" />
                <include name="${app.src.path}/*/obj/${build.config}/**" />
                <include name="${app.src.path}/*/bin/*.*" />
                <exclude name="${app.src.path}/Neato.Dol.PluginMac/bin/*.*" />
                <include name="${path::combine(path::combine(app.root.path, app.ttf.path),'*.ttf')}" />
            </fileset>
        </delete>

        <echo message="Taking files from SCC for ${app.src.path}" if="${use.scc}" />
        <p4sync view="${app.src.path}/..." verbose="true" if="${use.scc}"/>
        <echo message="Taking files from SCC for ${app.bin.path}" if="${use.scc}" />
        <p4sync view="${app.bin.path}/..." verbose="true" if="${use.scc}"/>
    </target>

    <target name="CopyFonts" depends="Prepare" >
        <copy todir="${path::combine(app.root.path, app.ttf.path)}">
            <fileset basedir="${res.ttf.path}">
                <include name="*.ttf" />
                <include name="*.TTF" />
                <include name="*.txt" />
            </fileset>
        </copy>
    </target>

    <target name="ChangeVersion" depends="Prepare" if="${build.incrementversion.flag}">
        <property name="version.number.file" value="${path::combine(app.root.path, app.version.number.file)}"/>
        <echo message="Generate new version" />
        <p4edit view="${version.number.file}" changelist="${scc.version.changedescription}" if="${use.scc}" />
        <attrib file="${version.number.file}" readonly="false" />
        <echo message="Version number file opened for edit" />

        <version path="${version.number.file}"
            buildtype="NoIncrement" 
            revisiontype="Increment"
        />
        <echo message="New version generated." />

        <property name="version.info.file"   value="${path::combine(app.root.path, app.version.info1.file)}" />
        <call target="SetVersion" />

        <property name="version.info.file"   value="${path::combine(app.root.path, app.version.info2.file)}" />
        <call target="SetVersion" />

        <echo message="VersionInfo files prepared, submitting" if="${use.scc}" />
        <echo message="${scc.version.changedescription}" if="${use.scc}" />
        <p4submit changelist="${scc.version.changedescription}" if="${use.scc}" />
        <echo message="Submitted" if="${use.scc}" />
    </target>

    <target name="SetVersion" if="${build.incrementversion.flag}">
        <echo message="Generate AssemblyInfo file ${version.info.file}" />
        <p4edit view="${version.info.file}"   changelist="${scc.version.changedescription}" if="${use.scc}" />
        <attrib file="${version.info.file}" readonly="false" />
        <echo message="Version info file opened for edit" />

        <echo message="Edit VersionInfo file" />
        <loadfile file="${version.number.file}" property="version.number"/>
        <property name="version.number" value="${string::trim(version.number)}" />

        <asminfo language="CSharp" output="${version.info.file}">
            <imports>
                <import namespace="System" />
                <import namespace="System.Reflection" />
                <import namespace="System.Runtime.CompilerServices" />
                <import namespace="System.Runtime.InteropServices" />
                <import namespace="System.Security.Permissions" />
            </imports>
            <attributes>
                <attribute type="AssemblyConfigurationAttribute" value="${build.config}" />
                <attribute type="AssemblyVersionAttribute"       value="${version.number}" />

                <attribute type="AssemblyTitleAttribute"         value="${app.name}" />
                <attribute type="AssemblyDescriptionAttribute"   value="" />

                <attribute type="AssemblyCompanyAttribute"       value="" />
                <attribute type="AssemblyProductAttribute"       value="${app.name}" />
                <attribute type="AssemblyCopyrightAttribute"     value="" />
                <attribute type="AssemblyTrademarkAttribute"     value="" />
                <attribute type="AssemblyCultureAttribute"       value="" />

                <attribute type="AssemblyDelaySignAttribute"     value="false" />
                <attribute type="AssemblyKeyFileAttribute"       value="" />
                <attribute type="AssemblyKeyNameAttribute"       value="" />
                <attribute type="CLSCompliantAttribute"          value="false" />
                <attribute type="ComVisibleAttribute"            value="false" />
                <attribute type="SecurityPermissionAttribute"    value="SecurityAction.RequestMinimum, Execution = true" asis="true"/>
            </attributes>
        </asminfo>
    </target>


    <target name="Build" depends="Prepare,ChangeVersion,CopyFonts" >
        <echo message="Building solution: ${solution.file}" />
        <echo message="Configuration: ${build.config}" />
        <!-- Exec used instead of "solution" to get good log -->
        <exec program="${devenv.exe}" >
            <arg line="${solution.file}" />
            <arg line="/build ${build.config}" />
            <arg line="/out ${compile.log.file}" />
        </exec>
        
        <echo message="flash build file: ${flash.build.file}"/>
        <echo message="flash log: ${flash.compile.log.file}" if="false"/>
        <exec program="${tool.flash.exe.file}" workingdir="${path::get-directory-name(flash.build.file)} " verbose="true">
            <arg value="${flash.build.file}" />
        </exec>
    </target>

    <target name="SyncDB" depends="Prepare">
        <exec program="Update.cmd" verbose="true" workingdir="${path::combine(app.root.path, app.dbproject.path)}" basedir="${path::combine(app.root.path, app.dbproject.path)}">
            <arg value="${app.db.server}" />
            <arg value="${app.db.name}" />
            <arg value="${app.db.adminlogin}" />
            <arg value="${app.db.adminpassword}" />
        </exec>
    </target>

    <target name="RunTests" depends="Build, SyncDB" >
        <echo message="Running NUnit" />
       
        <nunit2 failonerror="false" verbose="true">  <!-- Test with NUnit 2.2.x -->
            <formatter type="Plain" extension=".html" outputdir="${path::get-directory-name(test.xsl.file)}" usefile="true" />
            <test assemblyname="${test.assembly.file}"
                  appconfig="${test.config.file}" 
                  transformfile="${test.xsl.file}"
            />
        </nunit2>
    </target>

    <target name="Finish" depends="Build,RunTests">
        <echo message="Build succesful" />
    </target>

</project>
