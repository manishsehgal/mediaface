#!perl -w

###############################################################################
#
# Lines Of Code, Counting
# File: count_lines.pl
#
# @author Andrey Vlasov
#
# $DateTime: 2006/04/14 14:26:22 $
# $Revision: #1 $
################################################################################
my ($f_datetime) = q$DateTime: 2006/04/14 14:26:22 $ =~ /DateTime:\s+([^\s]+).*/;
my ($f_version) = q$Revision: #1 $ =~ /Revision:\s+([^\s]+)/;

use Getopt::Long;
use Config;
use strict 'vars';

################################################################################
# Constants
################################################################################

# Common definitions
my $f_EmptyLines = '^\s*$';
# CPP definitions
my $f_ScopeLinesCPP = '^[\s;\{\}]*$';
my $f_CommentC = '\/\*.*?\*\/';
my $f_CommentCPP = '\/\/.*$';
my $f_CommentC_Begin = '\/\*';
my $f_CommentC_End = '\*\/';
my $f_StringCPP = '""|".*?[^\\\\]"';                   #
my $f_CharCPP = '\'.*?[^\\\\]\'';
my $f_CodeCPP = '[^\s;\{\}]+';
my $f_EmptyLinesCPP = "(?:($f_EmptyLines)|($f_ScopeLinesCPP))";
my $f_TextLinesCPP = "(?:($f_CommentCPP)|($f_CommentC)|($f_StringCPP)|($f_CharCPP)|($f_CommentC_Begin))";
my $f_DirectivesCPP = '^\s*#(?:include|import|if|endif|else|elif|error|line).*$';
# Java definitions
my $f_CodeJava = '[^\s;\{\}]+';
my $f_EmptyLinesJava = "(?:($f_EmptyLines)|($f_ScopeLinesCPP))";
my $f_TextLinesJava = "(?:($f_CommentCPP)|($f_CommentC)|($f_StringCPP)|($f_CharCPP)|($f_CommentC_Begin))";
my $f_DirectivesJava = '^\s*(?:package|import).*$';

# JavaScript definitions
my $f_CodeJavaScript = '[^\s;\{\}]+';
my $f_EmptyLinesJavaScript = "(?:($f_EmptyLines)|($f_ScopeLinesCPP))";
my $f_TextLinesJavaScript = "(?:($f_CommentCPP)|($f_CommentC)|($f_StringCPP)|($f_CharCPP)|($f_CommentC_Begin))";

# ActionScript(Flash) definitions
my $f_CodeActionScript = '[^\s;\{\}]+';
my $f_EmptyLinesActionScript = "(?:($f_EmptyLines)|($f_ScopeLinesCPP))";
my $f_TextLinesActionScript = "(?:($f_CommentCPP)|($f_CommentC)|($f_StringCPP)|($f_CharCPP)|($f_CommentC_Begin))";

# C# definitions
my $f_CodeCS = '[^\s;\{\}]+';
my $f_EmptyLinesCS = "(?:($f_EmptyLines)|($f_ScopeLinesCPP))";
my $f_TextLinesCS = "(?:($f_CommentCPP)|($f_CommentC)|($f_StringCPP)|($f_CharCPP)|($f_CommentC_Begin))";
my $f_DirectivesCS = '^\s*(?:using).*$';
# VB definitions
# TODO: fix Scope+noncode lines
my $f_ScopeLinesVB = '^\s*end\s.*$';
my $f_CommentVB = '\'.*$';
my $f_StringVB = '".*?"';                   # it works even for '""' character
my $f_CodeVB = '[^\s]+';
my $f_EmptyLinesVB = "(?:($f_EmptyLines)|($f_ScopeLinesVB))";
my $f_DirectivesVB = '^\s*(?:option).*$';
my $f_TextLinesVB = "(?:($f_CommentVB)|($f_StringVB))";
my $f_EndOfHeaderFRM = "^(Attribute VB_Exposed =)|(Private Sub)";
# VB.NET definitions
# TODO: fix Scope+noncode lines
my $f_CodeVBnet = '[^\s]+';
my $f_EmptyLinesVBnet = "(?:($f_EmptyLines)|($f_ScopeLinesVB))";
my $f_DirectivesVBnet = '^\s*(?:imports).*$';
my $f_TextLinesVBnet = "(?:($f_CommentVB)|($f_StringVB))";
# ASP definitions
# TODO: fix Scope+noncode lines
my $f_ScriptASP_Begin = '<%';
my $f_ScriptASP_End = '%>';
my $f_CodeASP = '[^\s]+';
my $f_TextLinesASP = "(?:($f_CommentVB)|($f_StringVB))";
my $f_DirectivesASP = '^\s*(?:option).*$';
# ASP.NET definitions
# TODO: implement multilines comments
# JSP definitions
# TODO: implement multilines comments
# TODO: check for <%@ page language="java" import="java.sql.*" %>
# TODO: check for <%@ include file="Connections/Webuser.jsp" %>
# SQL definitions
# TODO: check for "%*" comments, strings, escape chars, fix Scope+noncode lines
my $f_ScopeLinesSQL = '^(?:begin|end|;|\(|\)|\s)*$';
my $f_CommentSQL = '--.*$';
my $f_StringSQL = '\'.*?\'';                   # it works even for '' character
my $f_CodeSQL = '[^\s;]+';
my $f_EmptyLinesSQL = "(?:($f_EmptyLines)|($f_ScopeLinesSQL))";
my $f_TextLinesSQL = "(?:($f_CommentSQL)|($f_CommentC)|($f_StringSQL)|($f_CommentC_Begin))";
my $f_DirectivesSQL = '^\s*(?:go).*$';
# Pascal definitions
# TODO: multiline 'uses', check for multiline string?
# TODO: fix Scope+noncode lines
my $f_ScopeLinesPas = '^(?:begin|end|;|\s|\.)*$';
my $f_CommentPasOld = '\{.*?\}';
my $f_CommentPasOld_Begin = '\{';
my $f_CommentPasOld_End = '\}';
my $f_CommentPasNew = '\(\*.*?\*\)';
my $f_CommentPasNew_Begin = '\(\*';
my $f_CommentPasNew_End = '\*\)';
my $f_StringPas = '\'.*?\'';                   # it works even for '' character
my $f_CodePas = '[^\s;]+';
my $f_EmptyLinesPas = "(?:($f_EmptyLines)|($f_ScopeLinesPas))";
my $f_TextLinesPas = "(?:($f_CommentPasOld)|($f_CommentPasNew)|($f_CommentCPP)|($f_StringPas)|($f_CommentPasOld_Begin)|($f_CommentPasNew_Begin))";
my $f_DirectivesPas = '^\s*(?:unit|uses|interface|implementation).*$';
# XML definitions
my $f_CommentXML_Begin = '<!--';
my $f_CommentXML_End = '-->';
# Scripts definitions
my $f_CommentLineScripts = "^#.*?";

# States
my $f_state = 0;
my $f_state_Code = 0;
my $f_state_MLComments = 1;
my $f_state_MLCommentsPasOld = 1;
my $f_state_MLCommentsPasNew = 2;
my $f_state_asp = 5;

# Lines
my $f_cpp_lines = 0;
my $f_java_lines = 0;
my $f_js_lines = 0;
my $f_as_lines = 0;
my $f_cs_lines = 0;
my $f_vb_lines = 0;
my $f_vbnet_lines = 0;
my $f_asp_lines = 0;
my $f_aspnet_lines = 0;
my $f_jsp_lines = 0;
my $f_sql_lines = 0;
my $f_pas_lines = 0;
my $f_codegen_lines = 0;
my $f_scripts_lines = 0;
# Files
my $f_cpp_files = 0;
my $f_java_files = 0;
my $f_js_files = 0;
my $f_as_files = 0;
my $f_cs_files = 0;
my $f_vb_files = 0;
my $f_vbnet_files = 0;
my $f_asp_files = 0;
my $f_aspnet_files = 0;
my $f_jsp_files = 0;
my $f_sql_files = 0;
my $f_pas_files = 0;
my $f_codegen_files = 0;
my $f_scripts_files = 0;


###############################################################################

print "  Lines Of Code, Counting, Revision $f_version, $f_datetime\n\n";

my $slash = "/";
GetOptions("overrideslash=s" => \$slash);

if (scalar(@ARGV) == 0)
{
    print "Usage: perl $0 [--overrideslash=SLASHCHAR] <DIR> [<DIR> ...]\n";
    print "Set optional parameter '--overrideslash' to directory separator on you system if it does not work otherwise.\n\n"
}
else
{
    my @directories = @ARGV;
    for my $dir (@directories) {
        print "Processing directories starting from '$dir'...\n";
        process_dir($dir);
    }

    print "Processing complete.\n\n";
    print "C\\C++, total executable lines:\t\t$f_cpp_lines\tfiles: $f_cpp_files\n";
    print "Java, total executable lines:\t\t$f_java_lines\tfiles: $f_java_files\n";
    print "JavaScript, total executable lines:\t$f_js_lines\tfiles: $f_js_files\n";
    print "ActionScript, total executable lines:\t$f_as_lines\tfiles: $f_as_files\n";
    print "C#, total executable lines:\t\t$f_cs_lines\tfiles: $f_cs_files\n";
    print "VB, total executable lines:\t\t$f_vb_lines\tfiles: $f_vb_files\n";
    print "VB.NET, total executable lines:\t\t$f_vbnet_lines\tfiles: $f_vbnet_files\n";
    print "ASP, total executable lines:\t\t$f_asp_lines\tfiles: $f_asp_files\n";
    print "ASPNET, total executable lines:\t\t$f_aspnet_lines\tfiles: $f_aspnet_files\n";
    print "JSP, total executable lines:\t\t$f_jsp_lines\tfiles: $f_jsp_files\n";
    print "SQL, total executable lines:\t\t$f_sql_lines\tfiles: $f_sql_files\n";
    print "Pas, total executable lines:\t\t$f_pas_lines\tfiles: $f_pas_files\n";
    print "CodeGen, total 'executable' lines:\t$f_codegen_lines\tfiles: $f_codegen_files\n";
    print "Scripts, total executable lines:\t$f_scripts_lines\t files: $f_scripts_files\n";
    print "------------------------------------\n";
    print "LOC, total Lines Of Code:\t\t", $f_cpp_lines+$f_java_lines+$f_js_lines+$f_as_lines+$f_cs_lines+$f_vb_lines+$f_vbnet_lines+
        +$f_asp_lines+$f_aspnet_lines+$f_jsp_lines+$f_sql_lines+$f_pas_lines+$f_codegen_lines+$f_scripts_lines;
    print "\tfiles: ", $f_cpp_files+$f_java_files+$f_js_files+$f_as_files+$f_cs_files+$f_vb_files+$f_vbnet_files+
        +$f_asp_files+$f_aspnet_files+$f_jsp_files+$f_sql_files+$f_pas_files+$f_codegen_files+$f_scripts_files, " \n";
}

#----------------------------------------------------------------------------

sub process_dir
{
    local (*DIRHANDLE);
    my ($p_dir_name) = @_;

#   print "Enter opendir ($p_dir_name)\n";

    opendir (*DIRHANDLE, $p_dir_name) || die
        "cannot open directory '$p_dir_name': $!";

    my @a_dir_items = readdir (*DIRHANDLE);

    foreach my $a_name (@a_dir_items)
    {
        # if the item is a directory and is not a link to
        # directories then process it recursively

        if ($a_name !~ /^\.+$/ and -d "$p_dir_name$slash$a_name")
        {
#            print "processing directory '$a_name'\n";
            process_dir ("$p_dir_name$slash$a_name");
        }
        else
        {
            if ($a_name =~ /\.(?:cpp|c|hpp|h)$/i)
            {
#                print ("Found file $p_dir_name\\$a_name\n");
                count_lines_cpp ("$p_dir_name$slash$a_name", \$f_cpp_lines);
                $f_cpp_files ++;
            }
            elsif ($a_name =~ /\.(?:js|sj)$/i)
            {
                count_lines_js("$p_dir_name$slash$a_name", \$f_js_lines);
                $f_js_files ++;
            }
            elsif ($a_name =~ /\.(?:jsfl|as)$/i)
            {
                count_lines_js("$p_dir_name$slash$a_name", \$f_as_lines);
                $f_as_files ++;
            }
            elsif ($a_name =~ /\.(?:java)$/i)
            {
                count_lines_java("$p_dir_name$slash$a_name", \$f_java_lines);
                $f_java_files ++;
            }
            elsif ($a_name =~ /\.(?:cs)$/i)
            {
                count_lines_cs  ("$p_dir_name$slash$a_name", \$f_cs_lines);
                $f_cs_files ++;
            }
            elsif ($a_name =~ /\.(?:frm)$/i)
            {
                count_lines_frm  ("$p_dir_name$slash$a_name", \$f_vb_lines);
                $f_vb_files ++;
            }
            elsif ($a_name =~ /\.(?:mst|inc|bas|cls)$/i)
            {
                count_lines_vb  ("$p_dir_name$slash$a_name", \$f_vb_lines);
                $f_vb_files ++;
            }
            elsif ($a_name =~ /\.(?:vb)$/i)
            {
                count_lines_vbnet  ("$p_dir_name$slash$a_name", \$f_vbnet_lines);
                $f_vbnet_files ++;
            }
            elsif ($a_name =~ /\.(?:asp|asa)$/i)
            {
                count_lines_asp ("$p_dir_name$slash$a_name", \$f_asp_lines);
                $f_asp_files ++;
            }
            elsif ($a_name =~ /\.(?:aspx|ascx|asmx)$/i)
            {
                count_lines_aspnet ("$p_dir_name$slash$a_name", \$f_aspnet_lines);
                $f_aspnet_files ++;
            }
            elsif ($a_name =~ /\.(?:jsp)$/i)
            {
                count_lines_jsp ("$p_dir_name$slash$a_name", \$f_jsp_lines);
                $f_jsp_files ++;
            }
            elsif ($a_name =~ /\.(?:sql)$/i)
            {
                count_lines_sql ("$p_dir_name$slash$a_name", \$f_sql_lines);
                $f_sql_files ++;
            }
            elsif ($a_name =~ /\.(?:pas)$/i)
            {
                count_lines_pas ("$p_dir_name$slash$a_name", \$f_pas_lines);
                $f_pas_files ++;
            }
            elsif ($a_name =~ /\.(?:vm|xsl|xslt)$/i)
            {
                count_lines_xslt ("$p_dir_name$slash$a_name", \$f_codegen_lines);
                $f_codegen_files ++;
            }
            elsif ($a_name =~ /\.(?:sh|pl|tcl|nant)$/i)
            {
                count_lines_scripts ("$p_dir_name$slash$a_name", \$f_scripts_lines);
                $f_scripts_files ++;
            }
        }
    }

    closedir (*DIRHANDLE);
}

# ---------------------------------------------------------------------------------

sub count_lines_cpp
{   # void count_lines (const string, &unsigned);
    my ($p_filename, $op_codelines) = @_;

    open (INPUTFILE, $p_filename) || die "Cannot open file $p_filename: $!\n";

    $f_state = $f_state_Code;

    while (<INPUTFILE>)
    {
         my $op_codeline=0;

         chomp;

         next if (/$f_EmptyLinesCPP/);
         next if (/$f_DirectivesCPP/);

#print "'------------: $_'\n";
         {s/\\\\/SS/g}                                      # keep $_ to be local

         do{
             if ($f_state == $f_state_Code)
             {# (?:($f_CommentCPP)|($f_CommentC)|($f_StringCPP)|($f_CharCPP)|($f_CommentC_Begin))
                 if (/$f_TextLinesCPP/)
                 {
                     {$op_codeline=1 if ($` && $` =~ /$f_CodeCPP/)} # $` is local
                     $op_codeline=1 if ($3||$4);
                     $f_state = $f_state_MLComments if ($5);
                 }else{
                     $op_codeline=1 if (/$f_CodeCPP/);
                 }
                 $_= $';
             }
             elsif ($f_state == $f_state_MLComments)
             {
                 $f_state = $f_state_Code if (/$f_CommentC_End/);
                 $_= $';
             }
             else
             {die "Error state!\n"}
         }while($_);
#print "Code\n" if ($op_codeline ==1);

         $$op_codelines += $op_codeline;
    }

    close (INPUTFILE);
}

# ---------------------------------------------------------------------------------

sub count_lines_java
{   # void count_lines (const string, &unsigned);
    my ($p_filename, $op_codelines) = @_;

    open (INPUTFILE, $p_filename) || die "Cannot open file $p_filename: $!\n";

    $f_state = $f_state_Code;

    while (<INPUTFILE>)
    {
         my $op_codeline=0;

         chomp;

         next if (/$f_EmptyLinesJava/);
         next if (/$f_DirectivesJava/);

#print "'------------: $_'\n";
         {s/\\\\/SS/g}                                      # keep $_ to be local

         do{
             if ($f_state == $f_state_Code)
             {# (?:($f_CommentCPP)|($f_CommentC)|($f_StringCPP)|($f_CharCPP)|($f_CommentC_Begin))
                 if (/$f_TextLinesJava/)
                 {
                     {$op_codeline=1 if ($` && $` =~ /$f_CodeJava/)} # $` is local
                     $op_codeline=1 if ($3||$4);
                     $f_state = $f_state_MLComments if ($5);
                 }else{
                     $op_codeline=1 if (/$f_CodeJava/);
                 }
                 $_= $';
             }
             elsif ($f_state == $f_state_MLComments)
             {
                 $f_state = $f_state_Code if (/$f_CommentC_End/);
                 $_= $';
             }
             else
             {die "Error state!\n"}
         }while($_);
#print "Code\n" if ($op_codeline ==1);

         $$op_codelines += $op_codeline;
    }

    close (INPUTFILE);
}

# ---------------------------------------------------------------------------------
sub count_lines_js
{   # void count_lines (const string, &unsigned);
    my ($p_filename, $op_codelines) = @_;
    open (INPUTFILE, $p_filename) || die "Cannot open file $p_filename: $!\n";

    $f_state = $f_state_Code;
    while (<INPUTFILE>) {
         my $op_codeline=0;
         chomp;
         next if (/$f_EmptyLinesJavaScript/);

#print "'------------: $_'\n";
         {s/\\\\/SS/g}                                      # keep $_ to be local

         do {
             if ($f_state == $f_state_Code) {
                 if (/$f_TextLinesJavaScript/) {
                     {$op_codeline=1 if ($` && $` =~ /$f_CodeJavaScript/)} # $` is local
                     $op_codeline=1 if ($3||$4);
                     $f_state = $f_state_MLComments if ($5);
                 }else{
                     $op_codeline=1 if (/$f_CodeJavaScript/);
                 }
                 $_= $';
             }
             elsif ($f_state == $f_state_MLComments)
             {
                 $f_state = $f_state_Code if (/$f_CommentC_End/);
                 $_= $';
             }
             else {
                 die "Error state!\n";
             }
         } while($_);

         $$op_codelines += $op_codeline;
    }

    close (INPUTFILE);
}

# ---------------------------------------------------------------------------------
sub count_lines_as # as, jsfl
{   # void count_lines (const string, &unsigned);
    my ($p_filename, $op_codelines) = @_;
    open (INPUTFILE, $p_filename) || die "Cannot open file $p_filename: $!\n";

    $f_state = $f_state_Code;
    while (<INPUTFILE>) {
         my $op_codeline=0;
         chomp;
         next if (/$f_EmptyLinesActionScript/);

#print "'------------: $_'\n";
         {s/\\\\/SS/g}                                      # keep $_ to be local

         do {
             if ($f_state == $f_state_Code) {
                 if (/$f_TextLinesActionScript/) {
                     {$op_codeline=1 if ($` && $` =~ /$f_CodeActionScript/)} # $` is local
                     $op_codeline=1 if ($3||$4);
                     $f_state = $f_state_MLComments if ($5);
                 }else{
                     $op_codeline=1 if (/$f_CodeActionScript/);
                 }
                 $_= $';
             }
             elsif ($f_state == $f_state_MLComments)
             {
                 $f_state = $f_state_Code if (/$f_CommentC_End/);
                 $_= $';
             }
             else {
                 die "Error state!\n";
             }
         } while($_);

         $$op_codelines += $op_codeline;
    }

    close (INPUTFILE);
}

# ---------------------------------------------------------------------------------

sub count_lines_cs
{   # void count_lines (const string, &unsigned);
    my ($p_filename, $op_codelines) = @_;

    open (INPUTFILE, $p_filename) || die "Cannot open file $p_filename: $!\n";

    $f_state = $f_state_Code;

    while (<INPUTFILE>)
    {
         my $op_codeline=0;

         chomp;

         next if (/$f_EmptyLinesCS/);
         next if (/$f_DirectivesCS/);

#print "'------------: $_'\n";
         {s/\\\\/SS/g}                                      # keep $_ to be local

         do{
             if ($f_state == $f_state_Code)
             {# (?:($f_CommentCPP)|($f_CommentC)|($f_StringCPP)|($f_CharCPP)|($f_CommentC_Begin))
                 if (/$f_TextLinesCS/)
                 {
                     {$op_codeline=1 if ($` && $` =~ /$f_CodeCS/)} # $` is local
                     $op_codeline=1 if ($3||$4);
                     $f_state = $f_state_MLComments if ($5);
                 }else{
                     $op_codeline=1 if (/$f_CodeCS/);
                 }
                 $_= $';
             }
             elsif ($f_state == $f_state_MLComments)
             {
                 $f_state = $f_state_Code if (/$f_CommentC_End/);
                 $_= $';
             }
             else
             {die "Error state!\n"}
         }while($_);
#print "Code\n" if ($op_codeline ==1);

         $$op_codelines += $op_codeline;
    }

    close (INPUTFILE);
}

# ---------------------------------------------------------------------------------

sub count_lines_vb
{   # void count_lines (const string, &unsigned);
    my ($p_filename, $op_codelines) = @_;

    open (INPUTFILE, $p_filename) || die "Cannot open file $p_filename: $!\n";

    while (<INPUTFILE>)
    {
         my $op_codeline=0;

         chomp;

         next if (/$f_EmptyLinesVB/i);
         next if (/$f_DirectivesVB/i);

#print "'------------: $_'\n";
         do{# (?:($f_CommentVB)|($f_StringVB))
             if (/$f_TextLinesVB/i)
             {
                 {$op_codeline=1 if ($` && $` =~ /$f_CodeVB/i)} # $` is local
                 $op_codeline=1 if ($2);
             }else{
                 $op_codeline=1 if (/$f_CodeVB/i);
             }
             $_= $';
         }while($_);

#print "Code\n" if ($op_codeline ==1);
         $$op_codelines += $op_codeline;
    }

    close (INPUTFILE);
}

# ---------------------------------------------------------------------------------

# almost count_lines_vb, just skip window description header
sub count_lines_frm
{   # void count_lines (const string, &unsigned);
    my ($p_filename, $op_codelines) = @_;

    open (INPUTFILE, $p_filename) || die "Cannot open file $p_filename: $!\n";

    my $skipping_header = 1;
    while (<INPUTFILE>)
    {
         my $op_codeline=0;

         chomp;

         if ($skipping_header == 1) 
         {
             if (/$f_EndOfHeaderFRM/i)
             {
                 $skipping_header = 0;
             }
             next;
         }

         next if (/$f_EmptyLinesVB/i);
         next if (/$f_DirectivesVB/i);

         do{# (?:($f_CommentVB)|($f_StringVB))
             if (/$f_TextLinesVB/i)
             {
                 {$op_codeline=1 if ($` && $` =~ /$f_CodeVB/i)} # $` is local
                 $op_codeline=1 if ($2);
             }else{
                 $op_codeline=1 if (/$f_CodeVB/i);
             }
             $_= $';
         }while($_);

         $$op_codelines += $op_codeline;
    }

    close (INPUTFILE);
}

# ---------------------------------------------------------------------------------

sub count_lines_vbnet
{   # void count_lines (const string, &unsigned);
    my ($p_filename, $op_codelines) = @_;

    open (INPUTFILE, $p_filename) || die "Cannot open file $p_filename: $!\n";

    while (<INPUTFILE>)
    {
         my $op_codeline=0;

         chomp;

         next if (/$f_EmptyLinesVBnet/i);
         next if (/$f_DirectivesVBnet/i);

#print "'------------: $_'\n";
         do{# (?:($f_CommentVB)|($f_StringVB))
             if (/$f_TextLinesVBnet/i)
             {
                 {$op_codeline=1 if ($` && $` =~ /$f_CodeVBnet/i)} # $` is local
                 $op_codeline=1 if ($2);
             }else{
                 $op_codeline=1 if (/$f_CodeVBnet/i);
             }
             $_= $';
         }while($_);

#print "Code\n" if ($op_codeline ==1);
         $$op_codelines += $op_codeline;
    }

    close (INPUTFILE);
}

# ---------------------------------------------------------------------------------

sub count_lines_asp # ASP, VBScript
{   # void count_lines (const string, &unsigned);
    my ($p_filename, $op_codelines) = @_;

    open (INPUTFILE, $p_filename) || die "Cannot open file $p_filename: $!\n";

    $f_state = $f_state_asp;

    while (<INPUTFILE>)
    {
         my $op_codeline=0;

         chomp;

         next if (/$f_EmptyLines/i);

#print "'------------: $_'\n";
         do{ # there are 2 cycles: html-code detection; code parsing
             if ($f_state == $f_state_Code)
             {
                 my $rest;

                 {if (/$f_ScriptASP_End/i) # '%>' has the highest priority
                 {
                     $rest = $'; # after
                     $_ = $`; # before

                     $f_state = $f_state_asp;
                 }}

                 do{ # code parsing
                     next if (/$f_DirectivesASP/i);
                     next if (/$f_ScopeLinesVB/i);

                     # (?:($f_CommentVB)|($f_StringVB))
                     if (/$f_TextLinesASP/i)
                     {
                         {$op_codeline=1 if ($` && $` =~ /$f_CodeASP/i)} # $` is local
                         $op_codeline=1 if ($2);
                     }else{
                         $op_codeline=1 if (/$f_CodeASP/i);
                     }
                     $_= $';
                 }while($_);

                 $_= $rest;
             }
             elsif ($f_state == $f_state_asp)
             {
                     if (/$f_ScriptASP_Begin/)
                         {$f_state = $f_state_Code}
                     $_= $';
             }
             else
                 {die "Error state!\n"}
         }while($_);

#print "Code\n" if ($op_codeline ==1);
         $$op_codelines += $op_codeline;
    }

    close (INPUTFILE);
}

# ---------------------------------------------------------------------------------

sub count_lines_aspnet # ASP.NET, C#
{   # void count_lines (const string, &unsigned);
    my ($p_filename, $op_codelines) = @_;

    open (INPUTFILE, $p_filename) || die "Cannot open file $p_filename: $!\n";

    $f_state = $f_state_asp;

    while (<INPUTFILE>)
    {
         my $op_codeline=0;

         chomp;

         next if (/$f_EmptyLines/);

#print "'------------: $_'\n";
         do{ # there are 2 cycles: html-code detection; code parsing
             if ($f_state == $f_state_Code)
             {
                 my $rest;

                 {if (/$f_ScriptASP_End/) # '%>' has the highest priority
                 {
                     $rest = $'; # after
                     $_ = $`; # before

                     $f_state = $f_state_asp;
                 }}

                 {s/\\\\/SS/g}                                      # keep $_ to be local

                 do{ # code parsing
                     next if (/$f_DirectivesASP/i);
                     next if (/$f_EmptyLinesCS/i);

                     # (?:($f_CommentCPP)|($f_CommentC)|($f_StringCPP)|($f_CharCPP)|($f_CommentC_Begin))
                     if (/$f_TextLinesCS/)
                     {
                         {$op_codeline=1 if ($` && $` =~ /$f_CodeCS/)} # $` is local
                         $op_codeline=1 if ($3||$4);
                         if ($5) {die "!!! Error !!! ASP.NET/C# multiline comments are not implemented yet !\n";}
                     }else{
                         $op_codeline=1 if (/$f_CodeCS/);
                     }
                     $_= $';
                 }while($_);

                 $_= $rest;
             }
             elsif ($f_state == $f_state_asp)
             {
                     if (/$f_ScriptASP_Begin/)
                         {$f_state = $f_state_Code}
                     $_= $';
             }
             else
                 {die "Error state!\n"}
         }while($_);

#print "Code\n" if ($op_codeline ==1);
         $$op_codelines += $op_codeline;
    }

    close (INPUTFILE);
}

# ---------------------------------------------------------------------------------

sub count_lines_jsp # JSP, Java
{   # void count_lines (const string, &unsigned);
    my ($p_filename, $op_codelines) = @_;

    open (INPUTFILE, $p_filename) || die "Cannot open file $p_filename: $!\n";

    $f_state = $f_state_asp;

    while (<INPUTFILE>)
    {
         my $op_codeline=0;

         chomp;

         next if (/$f_EmptyLines/);

#print "'------------: $_'\n";
         do{ # there are 2 cycles: html-code detection; code parsing
             if ($f_state == $f_state_Code)
             {
                 my $rest;

                 {if (/$f_ScriptASP_End/) # '%>' has the highest priority
                 {
                     $rest = $'; # after
                     $_ = $`; # before

                     $f_state = $f_state_asp;
                 }}

                 {s/\\\\/SS/g}                                      # keep $_ to be local

                 do{ # code parsing
                     next if (/$f_DirectivesASP/i);
                     next if (/$f_EmptyLinesJava/);

                     # (?:($f_CommentCPP)|($f_CommentC)|($f_StringCPP)|($f_CharCPP)|($f_CommentC_Begin))
                     if (/$f_TextLinesJava/)
                     {
                         {$op_codeline=1 if ($` && $` =~ /$f_CodeJava/)} # $` is local
                         $op_codeline=1 if ($3||$4);
                         if ($5) {die "!!! Error !!! JSP/Java multiline comments are not implemented yet !\n";}
                     }else{
                         $op_codeline=1 if (/$f_CodeJava/);
                     }
                     $_= $';
                 }while($_);

                 $_= $rest;
             }
             elsif ($f_state == $f_state_asp)
             {
                     if (/$f_ScriptASP_Begin/)
                         {$f_state = $f_state_Code}
                     $_= $';
             }
             else
                 {die "Error state!\n"}
         }while($_);

#print "Code\n" if ($op_codeline ==1);
         $$op_codelines += $op_codeline;
    }

    close (INPUTFILE);
}

# ---------------------------------------------------------------------------------

sub count_lines_sql
{   # void count_lines (const string, &unsigned);
    my ($p_filename, $op_codelines) = @_;

    open (INPUTFILE, $p_filename) || die "Cannot open file $p_filename: $!\n";

    $f_state = $f_state_Code;

    while (<INPUTFILE>)
    {
         my $op_codeline=0;

         chomp;

         next if (/$f_EmptyLinesSQL/i);
         next if (/$f_DirectivesSQL/i);

#print "'------------: $_'\n";
         do{
             if ($f_state == $f_state_Code)
             {# (?:($f_CommentSQL)|($f_CommentC)|($f_StringSQL)|($f_CommentC_Begin))
                 if (/$f_TextLinesSQL/i)
                 {
                     {$op_codeline=1 if ($` && $` =~ /$f_CodeSQL/i)} # $` is local
                     $op_codeline=1 if ($3);
                     $f_state = $f_state_MLComments if ($4);
                 }else{
                     $op_codeline=1 if (/$f_CodeSQL/i);
                 }
                 $_= $';
             }
             elsif ($f_state == $f_state_MLComments)
             {
                 $f_state = $f_state_Code if (/$f_CommentC_End/i);
                 $_= $';
             }
             else
             {die "Error state!\n"}
         }while($_);
#print "Code\n" if ($op_codeline ==1);

         $$op_codelines += $op_codeline;
    }

    close (INPUTFILE);
}

# ---------------------------------------------------------------------------------

sub count_lines_pas
{   # void count_lines (const string, &unsigned);
    my ($p_filename, $op_codelines) = @_;

    open (INPUTFILE, $p_filename) || die "Cannot open file $p_filename: $!\n";

    $f_state = $f_state_Code;

    while (<INPUTFILE>)
    {
         my $op_codeline=0;

         chomp;

         next if (/$f_EmptyLinesPas/i);
         next if (/$f_DirectivesPas/i);

#print "'------------: $_'\n";
         do{
             if ($f_state == $f_state_Code)
             {# (?:($f_CommentPasOld)|($f_CommentPasNew)|($f_CommentCPP)|($f_StringPas)|($f_CommentPasOld_Begin)|($f_CommentPasNew_Begin))
                 if (/$f_TextLinesPas/i)
                 {
                     {$op_codeline=1 if ($` && $` =~ /$f_CodePas/i)} # $` is local
                     $op_codeline=1 if ($4);
                     $f_state = $f_state_MLCommentsPasOld if ($5);
                     $f_state = $f_state_MLCommentsPasNew if ($6);
                 }else{
                     $op_codeline=1 if (/$f_CodePas/i);
                 }
                 $_= $';
             }
             elsif ($f_state == $f_state_MLCommentsPasOld)
             {
                 $f_state = $f_state_Code if (/$f_CommentPasOld_End/i);
                 $_= $';
             }
             elsif ($f_state == $f_state_MLCommentsPasNew)
             {
                 $f_state = $f_state_Code if (/$f_CommentPasNew_End/i);
                 $_= $';
             }
             else
             {die "Error state!\n"}
         }while($_);
#print "Code\n" if ($op_codeline ==1);

         $$op_codelines += $op_codeline;
    }

    close (INPUTFILE);
}

# ---------------------------------------------------------------------------------

sub count_lines_xslt # xslt
{   # void count_lines (const string, &unsigned);
    my ($p_filename, $op_codelines) = @_;

    open (INPUTFILE, $p_filename) || die "Cannot open file $p_filename: $!\n";

    $f_state = $f_state_Code;
    while (<INPUTFILE>) {
         chomp;

         my $nonCommented = '';
         while ($_) {
             if ($f_state == $f_state_Code) {
                 if (/$f_CommentXML_Begin/i) {
                     $_ = $'; # after
                     $nonCommented .= $`; # before
                     $f_state = $f_state_MLComments;
                 } else {
                     $nonCommented .= $_;
                     $_ = '';
                 }
             } elsif ($f_state == $f_state_MLComments) {
                 if (/$f_CommentXML_End/i) {
                     $_ = $'; # after
                     $f_state = $f_state_Code;
                 } else {
                     $_ = '';
                 }
             }
         }

         next if ($nonCommented =~ /$f_EmptyLines/i);
         $$op_codelines++;
    }

    close (INPUTFILE);
}

# ---------------------------------------------------------------------------------

sub count_lines_scripts # sh, pl, tcl
{   # void count_lines (const string, &unsigned);
    my ($p_filename, $op_codelines) = @_;

    open (INPUTFILE, $p_filename) || die "Cannot open file $p_filename: $!\n";

    $f_state = $f_state_Code;
    while (<INPUTFILE>) {
         chomp;

         next if (/$f_EmptyLines/i);
         next if (/$f_CommentLineScripts/i);
         $$op_codelines++;
    }

    close (INPUTFILE);
}

# ---------------------------------------------------------------------------------

