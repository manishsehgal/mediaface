<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

<xsl:import href="format.loc.xslt" />
<xsl:import href="format.unittest.xslt" />
<xsl:import href="format.coverage.xslt" />
<xsl:import href="format.complexity.xslt" />
<xsl:import href="format.fxcop.xslt" />

<xsl:param name="project" />
<xsl:param name="appversion" />
<xsl:param name="metricsstarted" />
<xsl:param name="metricsfinished" />

<xsl:output method="xml" />

<xsl:template match="/">
    <xsl:element name="metricdata">
        <xsl:attribute name="project"><xsl:value-of select="$project" /></xsl:attribute>
        <xsl:attribute name="appversion"><xsl:value-of select="$appversion" /></xsl:attribute>
        <xsl:attribute name="metricsstarted"><xsl:value-of select="$metricsstarted" /></xsl:attribute>
        <xsl:attribute name="metricsfinished"><xsl:value-of select="$metricsfinished" /></xsl:attribute>

        <xsl:copy-of select="/metricdata/metric" />

        <xsl:apply-templates select="." mode="addtotals.loc" />
        <xsl:apply-templates select="." mode="addtotals.unittest" />
        <xsl:apply-templates select="." mode="addtotals.coverage" />
        <xsl:apply-templates select="." mode="addtotals.complexity" />
        <xsl:apply-templates select="." mode="addtotals.fxcop" />
    </xsl:element>
</xsl:template>

</xsl:stylesheet>
