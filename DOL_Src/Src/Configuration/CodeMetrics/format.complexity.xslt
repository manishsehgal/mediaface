<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:devmetrics="http://www.tempuri.org/MeasurementDataSet.xsd"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://star-sw.com/metrics/functions"
    >

<xsl:output method="xml" />

<xsl:param name="tag" />

<msxsl:script language="JScript" implements-prefix="user">
    function maxValue(iterator) {
        var maxVal = null;
        while(iterator.MoveNext()) {
            var val = Number(iterator.Current);
            if (maxVal == null || maxVal &lt; val) maxVal = val;
        }
        if (maxVal == null) maxVal = 0;
        return maxVal;
    }
</msxsl:script>

<!-- Prepare data in unified format -->
    
<xsl:template match="/">
    <xsl:element name="metricdata">
        <xsl:element name="metric">
            <xsl:attribute name="id">complexity</xsl:attribute>
            <xsl:element name="tag">
                <xsl:attribute name="name"><xsl:value-of select="$tag"/></xsl:attribute>
                <xsl:apply-templates select="/devmetrics:MeasurementDataSet/devmetrics:CodeSolution/devmetrics:Assembly"/>
            </xsl:element>
        </xsl:element>
    </xsl:element>
</xsl:template>

<xsl:template match="/devmetrics:MeasurementDataSet/devmetrics:CodeSolution/devmetrics:Assembly">
    <xsl:element name="module">
        <xsl:attribute name="name"><xsl:value-of select="devmetrics:name"/></xsl:attribute>
        <xsl:element name="counter">
            <xsl:attribute name="name">mcc</xsl:attribute>
            <xsl:value-of select="devmetrics:MaximumCyclomaticComplexity"/>
        </xsl:element>
        <xsl:element name="counter">
            <xsl:attribute name="name">files</xsl:attribute>
            <xsl:value-of select="devmetrics:AssemblyFilesCount"/>
        </xsl:element>
        <xsl:element name="counter">
            <xsl:attribute name="name">types</xsl:attribute>
            <xsl:value-of select="devmetrics:AssemblyTypesCount"/>
        </xsl:element>
        <xsl:element name="counter">
            <xsl:attribute name="name">lines</xsl:attribute>
            <xsl:value-of select="devmetrics:AccumulatedLinesCount"/>
        </xsl:element>
    </xsl:element>
</xsl:template>
    
<!-- Adding totals (called from summary.totals.xslt) -->
    
<xsl:template match="/metricdata" mode="addtotals.complexity">
    <xsl:element name="metric">
        <xsl:attribute name="id">complexity</xsl:attribute>
        <xsl:element name="tag">
            <xsl:attribute name="name"></xsl:attribute>
            <xsl:element name="module">
                <xsl:attribute name="name">Total</xsl:attribute>
                <xsl:element name="counter">
                    <xsl:attribute name="name">mcc</xsl:attribute>
                    <xsl:value-of select="user:maxValue(/metricdata/metric[@id='complexity']/tag/module/counter[@name='mcc'])"/>
                </xsl:element>
                <xsl:element name="counter">
                    <xsl:attribute name="name">files</xsl:attribute>
                    <xsl:value-of select="sum(/metricdata/metric[@id='complexity']/tag/module/counter[@name='files'])"/>
                </xsl:element>
                <xsl:element name="counter">
                    <xsl:attribute name="name">types</xsl:attribute>
                    <xsl:value-of select="sum(/metricdata/metric[@id='complexity']/tag/module/counter[@name='types'])"/>
                </xsl:element>
                <xsl:element name="counter">
                    <xsl:attribute name="name">lines</xsl:attribute>
                    <xsl:value-of select="sum(/metricdata/metric[@id='complexity']/tag/module/counter[@name='lines'])"/>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:element>
</xsl:template>

<!-- Format HTML with results (called from summary.html.xslt) -->

<xsl:template match="/metricdata" mode="formathtml.complexity">
    <xsl:apply-templates select="." mode="writeheader" >
        <xsl:with-param name="metric">complexity</xsl:with-param>
        <xsl:with-param name="metricName">Code complexity</xsl:with-param>
    </xsl:apply-templates>

    <xsl:element name="table">
        <xsl:attribute name="class">rptdata</xsl:attribute>
        <xsl:element name="tr">
            <xsl:element name="th">Project</xsl:element>
            <xsl:element name="th">Maximum complexity</xsl:element>
            <xsl:element name="th">Files</xsl:element>
            <xsl:element name="th">Types</xsl:element>
            <xsl:element name="th">Lines of code</xsl:element>        
        </xsl:element>
        <xsl:apply-templates select="metric[@id='complexity']/tag/module[@name != '']" mode="formathtml.complexity" />
        <xsl:apply-templates select="metric[@id='complexity']/tag/module[@name = '']" mode="formathtml.complexity" />
    </xsl:element>
</xsl:template>

<xsl:template match="/metricdata/metric/tag/module" mode="formathtml.complexity">
    <xsl:element name="tr">
        <xsl:if test="@name = 'Total'">
            <xsl:attribute name="class">totalrow</xsl:attribute>
        </xsl:if>

        <xsl:element name="td">
            <xsl:value-of select="@name" />
        </xsl:element>
        <xsl:element name="td">
            <xsl:attribute name="class">
            <xsl:choose>
                <xsl:when test="number(counter[@name='mcc']) &lt;= 20">good</xsl:when>
                <xsl:otherwise>bad</xsl:otherwise>
            </xsl:choose>
            </xsl:attribute>
            <xsl:value-of select="counter[@name='mcc']" />
        </xsl:element>
        <xsl:element name="td">
            <xsl:value-of select="counter[@name='files']" />
        </xsl:element>
        <xsl:element name="td">
            <xsl:value-of select="counter[@name='types']" />
        </xsl:element>
        <xsl:element name="td">
            <xsl:value-of select="counter[@name='lines']" />
        </xsl:element>

    </xsl:element>
</xsl:template>

</xsl:stylesheet>

  