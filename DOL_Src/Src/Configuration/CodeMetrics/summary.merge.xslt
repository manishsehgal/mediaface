<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" />

<xsl:template match="/">
    <xsl:element name="metricdata">
        <xsl:apply-templates select="/files/file" />
    </xsl:element>
</xsl:template>

<xsl:template match="/files/file">
    <xsl:copy-of select="document(current())/metricdata/metric" />
</xsl:template>

</xsl:stylesheet>

<!-- 
    __________Unified metrcs format:_________

<metricdata>
    <metric id="{MetricCodeName}">
        <tag name="{Tag name, can be empty string if data is not related to any tag}">
            <module name="{Module name, can be empty string if data is not related to any module}">
                <counter name="{Counter1 name}">{Counter1 value}</counter>
                <counter name="{Counter2 name}">{Counter2 value}</counter>
                ...
                <counter name="{CounterN name}">{CounterN value}</counter>
            </module>
        </tag>
    </metric>
</metricdata>    

    ___________Terms explanation: ___________
    
    Metric - class of values that can be gathered by corresponding tool.
    Metric examples: "Lines of code", "Unit test coverage", "Code complexity"...
    
    Tag - part of solution that should be measured separately. Usually this is some business part of solution
    Tag examples: "Application code", "Automated testing code" ...
    
    Module - compiled unit of the solution, Usually, language project
    Module examples: "MyApplication.Web", "MyApplication.DataAccess" ...
    
    Counter - value of some characteristic of module, tag or entire solution
    Counter examples: "Files count", "Failed test count" ...
    
    ________________Example: ________________
    
<metricdata>
    <metric id="loc">
        <tag name="Automation">
            <module name="">
                <counter name="lines">33</counter>
                <counter name="files">10</counter>
            </module>
        </tag>
    </metric>
    <metric id="unittest">
        <tag name="">
            <module name="Intel.Mpes.Test.csproj">
                <counter name="totaltesta">120</counter>
                <counter name="failures">10</counter>
            </module>
        </tag>
    </metric>
    <metric id="coverage">
        <tag name="">
            <module name="Intel.Mpes.BusinessComponents">
                <counter name="seq">120</counter>
                <counter name="covered">10</counter>
            </module>
        </tag>
    </metric>
</metricdata>    

-->
