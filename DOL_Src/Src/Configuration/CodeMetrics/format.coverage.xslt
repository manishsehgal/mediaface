<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
<xsl:output method="xml" />

<xsl:param name="tag" />

<!-- Prepare data in unified format -->
    
<xsl:key name="seqpnt-total" match="seqpnt" use="../../@assembly" />
<xsl:key name="seqpnt-visited" match="seqpnt[number(@visitcount) &gt; 0]" use="../../@assembly" />
    
<xsl:template match="/">
    <xsl:element name="metricdata">
        <xsl:element name="metric">
            <xsl:attribute name="id">coverage</xsl:attribute>
            <xsl:element name="tag">
                <xsl:attribute name="name"><xsl:value-of select="$tag"/></xsl:attribute>
                <xsl:apply-templates select="/coverage/module"/>
            </xsl:element>
        </xsl:element>
    </xsl:element>
</xsl:template>

<xsl:template match="/coverage/module">
    <xsl:element name="module">
        <xsl:attribute name="name"><xsl:value-of select="@assembly"/></xsl:attribute>
        <xsl:element name="counter">
            <xsl:attribute name="name">total</xsl:attribute>
            <xsl:value-of select="count(key('seqpnt-total', @assembly))"/>
        </xsl:element>
        <xsl:element name="counter">
            <xsl:attribute name="name">visited</xsl:attribute>
            <xsl:value-of select="count(key('seqpnt-visited', @assembly))"/>
        </xsl:element>
        <xsl:element name="counter">
            <xsl:attribute name="name">percent</xsl:attribute>
            <xsl:value-of select="format-number(count(key('seqpnt-visited', @assembly)) * 100 div count(key('seqpnt-total', @assembly)), '#.00')"/>
        </xsl:element>
    </xsl:element>
</xsl:template>

<!-- Adding totals (called from summary.totals.xslt) -->

<xsl:template match="/metricdata" mode="addtotals.coverage">
    <xsl:element name="metric">
        <xsl:attribute name="id">coverage</xsl:attribute>
        <xsl:element name="tag">
            <xsl:attribute name="name"></xsl:attribute>
            <xsl:element name="module">
                <xsl:attribute name="name">Total</xsl:attribute>
                <xsl:element name="counter">
                    <xsl:attribute name="name">total</xsl:attribute>
                    <xsl:value-of select="sum(/metricdata/metric[@id='coverage']/tag/module/counter[@name='total'])"/>
                </xsl:element>
                <xsl:element name="counter">
                    <xsl:attribute name="name">visited</xsl:attribute>
                    <xsl:value-of select="sum(/metricdata/metric[@id='coverage']/tag/module/counter[@name='visited'])"/>
                </xsl:element>
                <xsl:element name="counter">
                    <xsl:attribute name="name">percent</xsl:attribute>
                    <xsl:value-of select="format-number(sum(/metricdata/metric[@id='coverage']/tag/module/counter[@name='visited']) * 100 div sum(/metricdata/metric[@id='coverage']/tag/module/counter[@name='total']), '#.00')"/>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:element>
</xsl:template>

<!-- Format HTML with results (called from summary.html.xslt) -->


<xsl:template match="/metricdata" mode="formathtml.coverage">
    <xsl:apply-templates select="." mode="writeheader" >
        <xsl:with-param name="metric">coverage</xsl:with-param>
        <xsl:with-param name="metricName">Unit test coverage</xsl:with-param>
    </xsl:apply-templates>
    
    <xsl:element name="table">
        <xsl:attribute name="class">rptdata</xsl:attribute>
        <xsl:element name="tr">
            <xsl:element name="th">Project</xsl:element>
            <xsl:element name="th">Coverage, %</xsl:element>
        </xsl:element>
        <xsl:apply-templates select="metric[@id='coverage']/tag/module[@name != '']" mode="formathtml.coverage" />
        <xsl:apply-templates select="metric[@id='coverage']/tag/module[@name = '']" mode="formathtml.coverage" />
    </xsl:element>
</xsl:template>

<xsl:template match="/metricdata/metric/tag/module" mode="formathtml.coverage">
    <xsl:element name="tr">
        <xsl:if test="@name = 'Total'">
            <xsl:attribute name="class">totalrow</xsl:attribute>
        </xsl:if>

        <xsl:element name="td">
            <xsl:value-of select="@name" />
        </xsl:element>
        <xsl:element name="td">
            <xsl:attribute name="class">
            <xsl:choose>
                <xsl:when test="number(counter[@name='percent']) &gt;= 85">good</xsl:when>
                <xsl:otherwise>bad</xsl:otherwise>
            </xsl:choose>
            </xsl:attribute>
            <xsl:value-of select="counter[@name='percent']" />
        </xsl:element>

    </xsl:element>
</xsl:template>

</xsl:stylesheet>
