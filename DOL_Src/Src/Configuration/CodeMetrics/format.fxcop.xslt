<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://star-sw.com/metrics/functions"
    >
    
<xsl:output method="xml" />

<xsl:param name="tag" />

<!-- Prepare data in unified format -->

<msxsl:script language="JScript" implements-prefix="user">
    function extractFxCopModuleName(iterator) {
        iterator.MoveNext();
        var parts = String(iterator.Current).split('/');
        var projectFile = parts[parts.length - 1];
        var lastDotPosition = projectFile.lastIndexOf('.');
        return (lastDotPosition == -1) ? projectFile : projectFile.substr(0, lastDotPosition);
    }
</msxsl:script>
    
<xsl:template match="/">
    <xsl:element name="metricdata">
        <xsl:element name="metric">
            <xsl:attribute name="id">fxcop</xsl:attribute>
            <xsl:element name="tag">
                <xsl:attribute name="name"><xsl:value-of select="$tag"/></xsl:attribute>
                <xsl:apply-templates select="/FxCopReport/Targets/Target"/>
            </xsl:element>
        </xsl:element>
    </xsl:element>
</xsl:template>

<xsl:template match="/FxCopReport/Targets/Target">
    <xsl:element name="module">
        <xsl:attribute name="name"><xsl:value-of select="user:extractFxCopModuleName(@Name)"/></xsl:attribute>
        <xsl:element name="counter">
            <xsl:attribute name="name">breaking95</xsl:attribute>
            <xsl:value-of select="count(Modules//Message[@FixCategory='Breaking' and @Status='Active' and Issue/@Certainty &gt;= 95])"/>
        </xsl:element>
        <xsl:element name="counter">
            <xsl:attribute name="name">criticalerror</xsl:attribute>
            <xsl:value-of select="count(Modules//Message[@Status='Active' and Issue/@Level = 'Critical Error'])"/>
        </xsl:element>
    </xsl:element>
</xsl:template>

<!-- Adding totals (called from summary.totals.xslt) -->

<xsl:template match="/metricdata" mode="addtotals.fxcop">
    <xsl:element name="metric">
        <xsl:attribute name="id">fxcop</xsl:attribute>
        <xsl:element name="tag">
            <xsl:attribute name="name"></xsl:attribute>
            <xsl:element name="module">
                <xsl:attribute name="name">Total</xsl:attribute>
                <xsl:element name="counter">
                    <xsl:attribute name="name">breaking95</xsl:attribute>
                    <xsl:value-of select="sum(/metricdata/metric[@id='fxcop']/tag/module/counter[@name='breaking95'])"/>
                </xsl:element>
                <xsl:element name="counter">
                    <xsl:attribute name="name">criticalerror</xsl:attribute>
                    <xsl:value-of select="sum(/metricdata/metric[@id='fxcop']/tag/module/counter[@name='criticalerror'])"/>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:element>
</xsl:template>

<!-- Format HTML with results (called from summary.html.xslt) -->

<xsl:template match="/metricdata" mode="formathtml.fxcop">
    <xsl:apply-templates select="." mode="writeheader" >
        <xsl:with-param name="metric">fxcop</xsl:with-param>
        <xsl:with-param name="metricName">FxCop issues</xsl:with-param>
    </xsl:apply-templates>

    <xsl:element name="table">
        <xsl:attribute name="class">rptdata</xsl:attribute>
        <xsl:element name="tr">
            <xsl:element name="th">Project</xsl:element>
            <xsl:element name="th">Breaking, &gt;= 95%</xsl:element>
            <xsl:element name="th">Critical Errors</xsl:element>
        </xsl:element>
        <xsl:apply-templates select="metric[@id='fxcop']/tag/module[@name != '']" mode="formathtml.fxcop" />
        <xsl:apply-templates select="metric[@id='fxcop']/tag/module[@name = '']" mode="formathtml.fxcop" />
    </xsl:element>
</xsl:template>

<xsl:template match="/metricdata/metric/tag/module" mode="formathtml.fxcop">
    <xsl:element name="tr">
        <xsl:if test="@name = 'Total'">
            <xsl:attribute name="class">totalrow</xsl:attribute>
        </xsl:if>

        <xsl:element name="td">
            <xsl:value-of select="@name" />
        </xsl:element>
        <xsl:element name="td">
            <xsl:attribute name="class">
            <xsl:choose>
                <xsl:when test="number(counter[@name='breaking95']) &gt; 0">warning</xsl:when>
                <xsl:otherwise>good</xsl:otherwise>
            </xsl:choose>
            </xsl:attribute>
            <xsl:value-of select="counter[@name='breaking95']" />
        </xsl:element>
        <xsl:element name="td">
            <xsl:attribute name="class">
            <xsl:choose>
                <xsl:when test="number(counter[@name='criticalerror']) &gt; 0">bad</xsl:when>
                <xsl:otherwise>good</xsl:otherwise>
            </xsl:choose>
            </xsl:attribute>
            <xsl:value-of select="counter[@name='criticalerror']" />
        </xsl:element>

    </xsl:element>
</xsl:template>

</xsl:stylesheet>
