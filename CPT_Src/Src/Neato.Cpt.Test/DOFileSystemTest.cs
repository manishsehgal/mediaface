using System;
using System.Data;
using System.IO;

using Neato.Cpt.Data;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class DOFileSystemTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void CreateDirectoryTest() {
            string dirName = Guid.NewGuid().ToString("D");
            string dirPath = Path.Combine(@"..\..\", dirName);
            DOFileSystem.CreateDirectory(dirPath);

            Assert.IsTrue(DOFileSystem.DirectoryExist(dirPath));
            Directory.Delete(dirPath, true);
        }

        [Test]
        public void WriteFileTest() {
            string dirName = Guid.NewGuid().ToString("D");
            string dirPath = Path.Combine(@"..\..\", dirName);
            DOFileSystem.CreateDirectory(dirPath);
            string filePath = Path.Combine(dirPath, "test.tmp");
            byte[] file = new byte[] {1, 2, 3};
            DOFileSystem.WriteFile(file, filePath);
            Assert.IsTrue(DOFileSystem.FileExist(filePath));
            Directory.Delete(dirPath, true);
        }
    }
}