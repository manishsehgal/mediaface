using System.Data;

using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Entity;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class DOCarrierTest {
        public DOCarrierTest() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void CarrierInsertTest() {
            string carrierName = string.Empty.PadRight(1000, 't');
            Carrier carrier = BCCarrier.NewCarrier();
            carrier.Name = carrierName;

            DOCarrier.Add(carrier);

            Assert.IsNotNull(carrier);
            Assert.AreEqual(false, carrier.IsNew);
            Assert.IsTrue(carrier.Id > 0);

            carrier = DOCarrier.GetCarrier(carrier);

            Assert.IsNotNull(carrier);
            Assert.AreEqual(false, carrier.IsNew);
            Assert.IsTrue(carrier.Id > 0);
            Assert.AreEqual(carrierName.Substring(0, 40), carrier.Name);

            byte[] dbIcon = DOCarrier.GetCarrierIcon(carrier);
            Assert.AreEqual(0, dbIcon.Length);

        }

        [Test]
        public void CarrierGetByIdTest() {
            string carrierName = string.Empty.PadRight(1000, 't');
            Carrier carrier = BCCarrier.NewCarrier();
            carrier.Name = carrierName;

            DOCarrier.Add(carrier);

            Assert.IsNotNull(carrier);
            Assert.AreEqual(false, carrier.IsNew);
            Assert.IsTrue(carrier.Id > 0);

            Carrier dbCarrier = DOCarrier.GetCarrier(carrier);

            Assert.IsNotNull(dbCarrier);
            Assert.AreEqual(false, dbCarrier.IsNew);
            Assert.AreEqual(carrier.Id, dbCarrier.Id);
            Assert.AreEqual(carrierName.Substring(0, 40), dbCarrier.Name);
        }

        [Test]
        public void EnumerateAllCarrierTest() {
            string carrierName1 = string.Empty.PadRight(1000, '1');
            string carrierName2 = string.Empty.PadRight(1000, '2');
            Carrier carrier1 = BCCarrier.NewCarrier();
            carrier1.Name = carrierName1;
            Carrier carrier2 = BCCarrier.NewCarrier();
            carrier2.Name = carrierName2;

            Carrier[] carriersBefore = DOCarrier.EnumerateCarrierByParam(DeviceType.Undefined, null, null, null);
            Assert.IsNotNull(carriersBefore);

            DOCarrier.Add(carrier1);
            Assert.IsNotNull(carrier1);

            DOCarrier.Add(carrier2);
            Assert.IsNotNull(carrier2);

            Carrier[] carriersAfter = DOCarrier.EnumerateCarrierByParam(DeviceType.Undefined, null, null, null);
            Assert.IsNotNull(carriersAfter);
            Assert.AreEqual(carriersBefore.Length + 2, carriersAfter.Length);
        }

        [Test]
        public void EnumerateCarrierByDeviceTypeTest() {
            string carrierName1 = string.Empty.PadRight(1000, '1');
            string carrierName2 = string.Empty.PadRight(1000, '2');

            Carrier[] carriersBefore = DOCarrier.EnumerateCarrierByParam(DeviceType.Phone, null, null, null);
            Assert.IsNotNull(carriersBefore);

            Carrier carrier1 = BCCarrier.NewCarrier();
            carrier1.Name = carrierName1;

            Carrier carrier2 = BCCarrier.NewCarrier();
            carrier2.Name = carrierName2;

            DOCarrier.Add(carrier1);
            Assert.IsNotNull(carrier1);

            DOCarrier.Add(carrier2);
            Assert.IsNotNull(carrier2);

            // Devices are not assigned
            Carrier[] carriersAfter = DOCarrier.EnumerateCarrierByParam(DeviceType.Phone, null, null, null);
            Assert.IsNotNull(carriersAfter);
            Assert.AreEqual(carriersBefore.Length, carriersAfter.Length);
        }

        [Test]
        public void CarrierDeleteTest() {
            string CarrierName = string.Empty.PadRight(1000, 't');
            Carrier carrier = BCCarrier.NewCarrier();
            carrier.Name = CarrierName;

            DOCarrier.Add(carrier);

            Assert.IsNotNull(carrier);
            Assert.AreEqual(false, carrier.IsNew);
            Assert.IsTrue(carrier.Id > 0);

            DOCarrier.Delete(carrier);

            Carrier dbCarrier = DOCarrier.GetCarrier(carrier);
            Assert.IsNull(dbCarrier);
        }

        [Test]
        public void CarrierUpdateTest() {
            string carrierName = string.Empty.PadRight(1000, 't');
            Carrier carrier = BCCarrier.NewCarrier();
            carrier.Name = carrierName;

            DOCarrier.Add(carrier);
            Assert.IsNotNull(carrier);

            string carrierName2 = string.Empty.PadRight(1000, 'q');
            carrier.Name = carrierName2;

            DOCarrier.Update(carrier);

            Assert.IsNotNull(carrier);
            Assert.AreEqual(false, carrier.IsNew);
            Assert.IsTrue(carrier.Id > 0);

            Carrier dbCarrier = DOCarrier.GetCarrier(carrier);

            Assert.IsNotNull(dbCarrier);
            Assert.AreEqual(false, dbCarrier.IsNew);
            Assert.AreEqual(carrier.Id, dbCarrier.Id);
            Assert.AreEqual(carrierName2.Substring(0, 40), dbCarrier.Name);

            byte[] dbIcon = DOCarrier.GetCarrierIcon(carrier);
            Assert.AreEqual(0, dbIcon.Length);
        }

        [Test]
        public void CarrierIconUpdateTest() {
            string carrierName = string.Empty.PadRight(1000, 't');
            Carrier carrier = BCCarrier.NewCarrier();
            carrier.Name = carrierName;

            DOCarrier.Add(carrier);
            Assert.IsNotNull(carrier);

            byte[] dbIcon = DOCarrier.GetCarrierIcon(carrier);
            Assert.AreEqual(0, dbIcon.Length);

            byte[] icon = {1, 2, 3};
            DOCarrier.UpdateIcon(carrier, icon);

            dbIcon = DOCarrier.GetCarrierIcon(carrier);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }
    }
}