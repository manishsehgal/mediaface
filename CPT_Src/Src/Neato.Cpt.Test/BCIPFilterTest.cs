using System.Data;

using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Entity;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class BCIPFilterTest {
        public BCIPFilterTest() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void IsUserContainsInIPFiltersTest() {
            foreach (IPFilter ipFilter in BCIPFilter.EnumIPFilters()) {
                BCIPFilter.DeleteIPFilter(ipFilter);
            }

            Assert.AreEqual(0, BCIPFilter.EnumIPFilters().Length);

            BCIPFilter.InsertIPFilter(
                new IPFilter(
                    new IPAddress(100, 0, 0, 50),
                    new IPAddress(100, 0, 20, 30),
                    "Desc"));

            BCIPFilter.InsertIPFilter(
                new IPFilter(
                    new IPAddress(200, 0, 0, 10),
                    new IPAddress(210, 0, 0, 0),
                    "Desc"));

            Assert.AreEqual(2, BCIPFilter.EnumIPFilters().Length);

            Assert.IsFalse(BCIPFilter.IsUserContainsInIPFilters(
                new IPAddress(100, 0, 0, 49)));
            Assert.IsFalse(BCIPFilter.IsUserContainsInIPFilters(
                new IPAddress(50, 0, 0, 70)));
            Assert.IsFalse(BCIPFilter.IsUserContainsInIPFilters(
                new IPAddress(100, 0, 20, 31)));
            Assert.IsFalse(BCIPFilter.IsUserContainsInIPFilters(
                new IPAddress(100, 0, 21, 29)));
            Assert.IsFalse(BCIPFilter.IsUserContainsInIPFilters(
                new IPAddress(100, 1, 19, 29)));
            Assert.IsFalse(BCIPFilter.IsUserContainsInIPFilters(
                new IPAddress(101, 0, 19, 29)));

            Assert.IsTrue(BCIPFilter.IsUserContainsInIPFilters(
                new IPAddress(100, 0, 0, 50)));
            Assert.IsTrue(BCIPFilter.IsUserContainsInIPFilters(
                new IPAddress(100, 0, 0, 51)));
            Assert.IsTrue(BCIPFilter.IsUserContainsInIPFilters(
                new IPAddress(100, 0, 19, 29)));
            Assert.IsTrue(BCIPFilter.IsUserContainsInIPFilters(
                new IPAddress(210, 0, 0, 0)));
            Assert.IsTrue(BCIPFilter.IsUserContainsInIPFilters(
                new IPAddress(200, 0, 0, 15)));
        }
    }
}