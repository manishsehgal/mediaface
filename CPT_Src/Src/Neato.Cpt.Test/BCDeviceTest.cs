using System.Data;
using System.IO;

using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Entity;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class BCDeviceTest {
        public BCDeviceTest() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void NewDeviceTest() {
            Device device = BCDevice.NewDevice();
            Assert.IsNotNull(device);
            Assert.IsNull(device.Brand);
            Assert.AreEqual(0, device.Id);
            Assert.AreEqual(string.Empty, device.Model);
            Assert.AreEqual(0, device.Rating);
        }

        [Test]
        public void SaveNewDeviceWithoutIconTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);

            Device device = BCDevice.NewDevice();
            device.Model = string.Empty.PadRight(10, '1');
            device.Brand = brand;
            device.DeviceType = DeviceType.Phone;
            device.Rating = 9;

            BCDevice.Save(device);

            Assert.IsTrue(device.Id > 0);

            Device dbDevice = DODevice.GetDevice(device);

            Assert.IsNotNull(dbDevice);
            Assert.AreEqual(device.Id, dbDevice.Id);
            Assert.AreEqual(device.Model, dbDevice.Model);
            Assert.AreEqual(device.DeviceType, dbDevice.DeviceType);
            Assert.AreEqual(device.Rating, dbDevice.Rating);
            Assert.AreEqual(device.Brand, dbDevice.Brand);

            byte[] icon = BCDevice.GetDeviceIcon(device);
            Assert.IsNotNull(icon);
            Assert.AreEqual(0, icon.Length);
        }

        [Test]
        public void SaveNewDeviceWithIconTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);

            Device device = BCDevice.NewDevice();
            device.Model = string.Empty.PadRight(10, '1');
            device.Brand = brand;
            device.DeviceType = DeviceType.Phone;
            device.Rating = 9;

            byte[] icon = {1, 2, 3};
            byte[] bigIcon = {4, 5, 6};

            BCDevice.Save(device, new MemoryStream(icon), new MemoryStream(bigIcon));

            Assert.IsTrue(device.Id > 0);

            Device dbDevice = DODevice.GetDevice(device);

            Assert.IsNotNull(dbDevice);
            Assert.AreEqual(device.Id, dbDevice.Id);
            Assert.AreEqual(device.Model, dbDevice.Model);
            Assert.AreEqual(device.DeviceType, dbDevice.DeviceType);
            Assert.AreEqual(device.Rating, dbDevice.Rating);
            Assert.AreEqual(device.Brand, dbDevice.Brand);

            byte[] dbIcon = DODevice.GetDeviceIcon(device);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }

            byte[] dbBigIcon = DODevice.GetDeviceBigIcon(device);
            Assert.IsNotNull(dbBigIcon);
            Assert.AreEqual(bigIcon.Length, dbBigIcon.Length);
            for (int index = 0; index < bigIcon.Length; index++) {
                Assert.AreEqual(bigIcon[index], dbBigIcon[index]);
            }
        }

        [Test]
        public void SaveExistingDeviceWithoutIconTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);

            string deviceModel1 = string.Empty.PadRight(10, '1');
            string deviceModel2 = string.Empty.PadRight(10, '2');
            byte[] icon = {1, 2, 3};

            Device device = BCDevice.NewDevice();
            device.Model = deviceModel1;
            device.Brand = brand;
            device.DeviceType = DeviceType.Phone;
            device.Rating = 9;
            BCDevice.Save(device, new MemoryStream(icon), new MemoryStream(icon));

            device.Model = deviceModel2;
            BCDevice.Save(device);
            
            Device dbDevice = DODevice.GetDevice(device);

            Assert.IsNotNull(dbDevice);
            Assert.AreEqual(device.Id, dbDevice.Id);
            Assert.AreEqual(device.Model, dbDevice.Model);
            Assert.AreEqual(device.DeviceType, dbDevice.DeviceType);
            Assert.AreEqual(device.Rating, dbDevice.Rating);
            Assert.AreEqual(device.Brand, dbDevice.Brand);

            byte[] dbIcon = DODevice.GetDeviceIcon(device);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }

            dbIcon = DODevice.GetDeviceBigIcon(device);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        public void SaveExistingDeviceWithIconTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);

            string deviceModel1 = string.Empty.PadRight(10, '1');
            string deviceModel2 = string.Empty.PadRight(10, '2');

            byte[] icon1 = {1, 2, 3};
            byte[] icon2 = {4, 5, 6, 7};

            Device device = BCDevice.NewDevice();
            device.Model = deviceModel1;
            device.Brand = brand;
            device.DeviceType = DeviceType.Phone;
            device.Rating = 9;

            BCDevice.Save(device, new MemoryStream(icon1), new MemoryStream(icon1));

            device.Model = deviceModel2;
            BCDevice.Save(device, new MemoryStream(icon2), new MemoryStream(icon2));
            
            Device dbDevice = DODevice.GetDevice(device);

            Assert.IsNotNull(dbDevice);
            Assert.AreEqual(device.Id, dbDevice.Id);
            Assert.AreEqual(deviceModel2, dbDevice.Model);
            Assert.AreEqual(device.DeviceType, dbDevice.DeviceType);
            Assert.AreEqual(device.Rating, dbDevice.Rating);
            Assert.AreEqual(device.Brand, dbDevice.Brand);

            byte[] dbIcon = DODevice.GetDeviceIcon(device);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon2.Length, dbIcon.Length);
            for (int index = 0; index < icon2.Length; index++) {
                Assert.AreEqual(icon2[index], dbIcon[index]);
            }

            dbIcon = DODevice.GetDeviceBigIcon(device);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon2.Length, dbIcon.Length);
            for (int index = 0; index < icon2.Length; index++) {
                Assert.AreEqual(icon2[index], dbIcon[index]);
            }
        }

        [Test]
        public void ChangeIconTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);

            string deviceModel1 = string.Empty.PadRight(10, '1');
            string deviceModel2 = string.Empty.PadRight(10, '2');

            byte[] icon1 = {1, 2, 3};
            byte[] icon2 = {4, 5, 6, 7};

            Device device = BCDevice.NewDevice();
            device.Model = deviceModel1;
            device.Brand = brand;
            device.DeviceType = DeviceType.Phone;
            device.Rating = 9;

            BCDevice.Save(device, new MemoryStream(icon1), new MemoryStream(icon1));

            BCDevice.ChangeIcon(device, null, null);

            byte[] dbIcon = DODevice.GetDeviceIcon(device);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon1.Length, dbIcon.Length);
            for (int index = 0; index < icon1.Length; index++) {
                Assert.AreEqual(icon1[index], dbIcon[index]);
            }

            dbIcon = DODevice.GetDeviceBigIcon(device);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon1.Length, dbIcon.Length);
            for (int index = 0; index < icon1.Length; index++) {
                Assert.AreEqual(icon1[index], dbIcon[index]);
            }
            
            BCDevice.ChangeIcon(device, new MemoryStream(0), new MemoryStream(0));

            dbIcon = BCDevice.GetDeviceIcon(device);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon1.Length, dbIcon.Length);
            for (int index = 0; index < icon1.Length; index++) {
                Assert.AreEqual(icon1[index], dbIcon[index]);
            }

            dbIcon = DODevice.GetDeviceBigIcon(device);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon1.Length, dbIcon.Length);
            for (int index = 0; index < icon1.Length; index++) {
                Assert.AreEqual(icon1[index], dbIcon[index]);
            }

            BCDevice.ChangeIcon(device, new MemoryStream(icon2), new MemoryStream(icon2));

            dbIcon = BCDevice.GetDeviceIcon(device);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon2.Length, dbIcon.Length);
            for (int index = 0; index < icon2.Length; index++) {
                Assert.AreEqual(icon2[index], dbIcon[index]);
            }

            dbIcon = DODevice.GetDeviceBigIcon(device);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon2.Length, dbIcon.Length);
            for (int index = 0; index < icon2.Length; index++) {
                Assert.AreEqual(icon2[index], dbIcon[index]);
            }
        }

        [Test]
        public void GetDeviceIconTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);

            Device device = BCDevice.NewDevice();
            device.Model = string.Empty.PadRight(10, '1');
            device.Brand = brand;
            device.DeviceType = DeviceType.Phone;
            device.Rating = 9;
            byte[] icon = {1, 2, 3};

            BCDevice.Save(device, new MemoryStream(icon), new MemoryStream(icon));

            byte[] dbIcon = DODevice.GetDeviceIcon(device);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }

            dbIcon = DODevice.GetDeviceBigIcon(device);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }


        [Test]
        [Ignore("Not implemented")]
        public void GetDeviceListTest() {}

        [Test]
        public void GetRatingListTest() {
            int[] ratings = BCDevice.GetRatingList();
            Assert.IsNotNull(ratings);
            Assert.AreEqual(10, ratings.Length);

            for (int index = 0; index < ratings.Length; index++) {
                Assert.AreEqual(index, ratings[index]);
            }
        }

        [Test]
        public void DeleteTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);

            Carrier carrier = BCCarrier.NewCarrier();
            carrier.Name = "test cacrier";
            DOCarrier.Add(carrier);

            Device device = BCDevice.NewDevice();
            device.Model = string.Empty.PadRight(10, '1');
            device.Brand = brand;
            device.DeviceType = DeviceType.Phone;
            device.Rating = 9;
            byte[] icon = {1, 2, 3};

            BCDevice.Save(device, new MemoryStream(icon), new MemoryStream(icon));
            BCDevice.AddToCarrier(device, carrier);

            BCDevice.Delete(device);

            device = BCDevice.GetDevice(device);
            Assert.IsNull(device);

        }

    }
}