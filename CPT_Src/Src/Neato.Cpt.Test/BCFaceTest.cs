using System.Data;
using System.Drawing;
using System.IO;
using System.Xml;

using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Entity;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class BCFaceTest {
        public BCFaceTest() {}
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void NewFaceTest() {
            Face face = BCFace.NewFace();
            Assert.IsNotNull(face);
            Assert.IsNull(face.Contour);
            Assert.AreEqual(0, face.Id);
            Assert.AreEqual(0, face.Position.X);
            Assert.AreEqual(0, face.Position.Y);
        }

        [Test]
        public void SaveOneFaceTest() {
            XmlDocument doc = new XmlDocument();
            doc.InnerXml = "<Test>Doc</Test>";

            Face face = BCFace.NewFace();
            face.Contour = doc.FirstChild;
            face.Position = new PointF(100, 100);

            BCFace.Save(face);

            Face dbFace = DOFace.GetFace(face);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.IsTrue(dbFace.Id > 0);
            Assert.AreEqual(doc.FirstChild.OuterXml, dbFace.Contour.OuterXml);
            Assert.AreEqual(0, dbFace.Position.X);
            Assert.AreEqual(0, dbFace.Position.Y);

            doc.InnerXml = "<Test>Doc2</Test>";
            face.Contour = doc.FirstChild;

            BCFace.Save(face);

            dbFace = DOFace.GetFace(face);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.IsTrue(dbFace.Id > 0);
            Assert.AreEqual(doc.FirstChild.OuterXml, dbFace.Contour.OuterXml);
            Assert.AreEqual(0, dbFace.Position.X);
            Assert.AreEqual(0, dbFace.Position.Y);
        }

        [Test]
        public void SaveFacesTest() {
            XmlDocument doc = new XmlDocument();
            doc.InnerXml = "<Test>Doc</Test>";

            Face face1 = BCFace.NewFace();
            face1.Contour = doc.FirstChild;
            face1.Position = new PointF(100, 100);

            Face face2 = BCFace.NewFace();
            face2.Contour = doc.FirstChild;
            face2.Position = new PointF(100, 100);

            BCFace.Save(new Face[] {face1, face2});

            Face dbFace = DOFace.GetFace(face1);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.IsTrue(dbFace.Id > 0);
            Assert.AreEqual(doc.FirstChild.OuterXml, dbFace.Contour.OuterXml);
            Assert.AreEqual(0, dbFace.Position.X);
            Assert.AreEqual(0, dbFace.Position.Y);

            dbFace = DOFace.GetFace(face2);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.IsTrue(dbFace.Id > 0);
            Assert.AreEqual(doc.FirstChild.OuterXml, dbFace.Contour.OuterXml);
            Assert.AreEqual(0, dbFace.Position.X);
            Assert.AreEqual(0, dbFace.Position.Y);

            doc.InnerXml = "<Test>Doc2</Test>";
            face1.Contour = doc.FirstChild;
            face2.Contour = doc.FirstChild;

            BCFace.Save(new Face[] {face1, face2});

            dbFace = DOFace.GetFace(face1);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.IsTrue(dbFace.Id > 0);
            Assert.AreEqual(doc.FirstChild.OuterXml, dbFace.Contour.OuterXml);
            Assert.AreEqual(0, dbFace.Position.X);
            Assert.AreEqual(0, dbFace.Position.Y);

            dbFace = DOFace.GetFace(face2);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.IsTrue(dbFace.Id > 0);
            Assert.AreEqual(doc.FirstChild.OuterXml, dbFace.Contour.OuterXml);
            Assert.AreEqual(0, dbFace.Position.X);
            Assert.AreEqual(0, dbFace.Position.Y);
        }


        [Test]
        public void DeleteFaceTest() {
            PaperBrand paperBrand = BCPaperBrand.NewPaperBrand();
            paperBrand.Name = "test";
            DOPaperBrand.Add(paperBrand);
            paperBrand = DOPaperBrand.GetPaperBrand(paperBrand);
            Assert.IsNotNull(paperBrand);

            Paper paper = BCPaper.NewPaper();
            paper.Brand = paperBrand;
            paper.Name = "test";
            paper.Size = new SizeF(100, 100);
            DOPaper.Add(paper);
            
            Face face = BCFace.NewFace();
            face.Contour = null;
            face.Position = new PointF(90, 90);
            DOFace.Add(face);

            DOFace.BindToPaper(face, paper);

            DeviceBrand deviceBrand = BCDeviceBrand.NewDeviceBrand();
            deviceBrand.Name = "test";
            DODeviceBrand.Add(deviceBrand);
            deviceBrand = DODeviceBrand.GetDeviceBrand(deviceBrand);
            Assert.IsNotNull(deviceBrand);

            Device device = BCDevice.NewDevice();
            device.Brand = deviceBrand;
            device.Model = "test";
            device.Rating = 8;
            device.DeviceType = DeviceType.Phone;
            DODevice.Add(device);

            DOFace.BindToDevice(face, device);

            Face dbFace = DOFace.GetFace(face);

            Assert.IsNotNull(dbFace);
            Assert.AreEqual(false, dbFace.IsNew);
            Assert.IsTrue(dbFace.Id > 0);

            BCFace.Delete(new FaceBase[] {face});

            dbFace = DOFace.GetFace(face);

            Assert.IsNull(dbFace);
        }
    }
}