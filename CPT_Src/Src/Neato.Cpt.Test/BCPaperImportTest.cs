using System;
using System.Data;
using System.IO;
using System.Xml;

using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Entity.Exceptions;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class BCPaperImportTest {
        public BCPaperImportTest() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void ValidateInvalidXmlTest() {
            Console.WriteLine("ValidateInvalidXmlTest");
            string paperXml = "";
            string[] errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            foreach (string error in errors) {
                Console.WriteLine("Message: {0}", error);
            }
            Assert.AreEqual(true, errors.Length > 0);
            Assert.AreEqual(1, errors.Length);
            Assert.AreEqual("The root element is missing.", errors[0]);

            paperXml ="<?xml version=\"1.0\" encoding=\"utf-8\"?>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            foreach (string error in errors) {
                Console.WriteLine("Message: {0}", error);
            }
            Assert.AreEqual(true, errors.Length > 0);
            Assert.AreEqual(1, errors.Length);
            Assert.AreEqual("The root element is missing.", errors[0]);

            paperXml ="<Project>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            foreach (string error in errors) {
                Console.WriteLine("Message: {0}", error);
            }
            Assert.AreEqual(true, errors.Length > 0);
            Assert.AreEqual(1, errors.Length);

            paperXml ="<Project/>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            foreach (string error in errors) {
                Console.WriteLine("Message: {0}", error);
            }
            Assert.AreEqual(true, errors.Length > 0);

            paperXml ="<?xml version=\"1.0\" encoding=\"utf-8\"?><Project/>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            foreach (string error in errors) {
                Console.WriteLine("Message: {0}", error);
            }
            Assert.AreEqual(true, errors.Length > 0);

            paperXml ="<?xml version=\"1.0\" encoding=\"utf-8\"?><Project><Faces><F></F><Face x=\"20\" y=\"20\"><Contour x=\"0\" y=\"0\"></Contour></Face></Faces><Paper w=\"612\" h=\"435.6\" /></Project>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            foreach (string error in errors) {
                Console.WriteLine("Message: {0}", error);
            }
            Assert.AreEqual(true, errors.Length > 0);
            Assert.AreEqual(2, errors.Length);

            paperXml ="<?xml version=\"1.0\" encoding=\"utf-8\"?><Project><Faces><Face y=\"20\"><Contour x=\"0\" y=\"0\"></Contour></Face></Faces><Paper w=\"612\" h=\"435.6\" /></Project>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            foreach (string error in errors) {
                Console.WriteLine("Message: {0}", error);
            }
            Assert.AreEqual(true, errors.Length > 0);
            Assert.AreEqual(1, errors.Length);

            paperXml ="<?xml version=\"1.0\" encoding=\"utf-8\"?><Project><Faces><Face x=\"a\" y=\"20\"><Contour x=\"0\" y=\"0\"></Contour></Face></Faces><Paper w=\"612\" h=\"435.6\" /></Project>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            foreach (string error in errors) {
                Console.WriteLine("Message: {0}", error);
            }
            Assert.AreEqual(true, errors.Length > 0);
            Assert.AreEqual(1, errors.Length);

            paperXml ="<?xml version=\"1.0\" encoding=\"utf-8\"?><Project><Faces><Face x=\"20\" y=\"20\"><Contour x=\"0\" y=\"0\"><Elem x=\"0\" y=\"0\"></Elem></Contour></Face></Faces><Paper w=\"612\" h=\"435.6\" /></Project>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            foreach (string error in errors) {
                Console.WriteLine("Message: {0}", error);
            }
            Assert.AreEqual(true, errors.Length > 0);
            Assert.AreEqual(4, errors.Length);

            paperXml ="<?xml version=\"1.0\" encoding=\"utf-8\"?><Project><Faces><Face x=\"20\" y=\"20\"><Contour x=\"0\" y=\"0\"></Contour></Face></Faces><Paper w=\"-612\" h=\"-435.6\" /></Project>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            foreach (string error in errors) {
                Console.WriteLine("Message: {0}", error);
            }
            Assert.AreEqual(true, errors.Length > 0);
            Assert.AreEqual(2, errors.Length);
        }
        
        [Test]
        public void ValidateValidXmlTest() {
            Console.WriteLine();
            Console.WriteLine("ValidateValidXmlTest");
            string paperXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><Project><Faces><Face x=\"20.3\" y=\"20.3\"><Contour x=\"0\" y=\"0\"></Contour></Face></Faces><Paper w=\"612\" h=\"435.6\" /></Project>";
            string[] errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            Assert.AreEqual(true, errors.Length == 0);

            paperXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><Project><Faces><Face x=\"20.3\" y=\"20.3\"><Contour x=\"0.78\" y=\"-0.98\"><MoveTo x=\"10.9\" y=\"-10.5555334\" /><MoveTo x=\"12.9\" y=\"13.5555334\" /><LineTo x=\"120.9\" y=\"130.5555334\" /><CurveTo x=\"120.9\" y=\"130.5555334\" cx=\"140.9\" cy=\"140.5555334\" /><CurveTo x=\"10.349\" y=\"0.5555334\" cx=\"0.9\" cy=\"40.5555334\" /></Contour></Face></Faces><Paper w=\"612.9999\" h=\"435.6\" /></Project>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            Assert.AreEqual(true, errors.Length == 0);

            paperXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><Project><Faces><Face y=\"20.3\" x=\"20.3\"><Contour y=\"0.98\" x=\"0.78\"><MoveTo x=\"10.9\" y=\"10.5555334\" /><MoveTo y=\"13.5555334\" x=\"12.9\"/><LineTo y=\"130.5555334\" x=\"120.9\"/><CurveTo cy=\"140.5555334\" y=\"130.5555334\" cx=\"140.9\" x=\"120.9\" /><CurveTo x=\"10.349\" y=\"0.5555334\" cx=\"0.9\" cy=\"40.5555334\" /></Contour></Face></Faces><Paper h=\"435.6\" w=\"612.9999\" /></Project>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            Assert.AreEqual(true, errors.Length == 0);

            paperXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><Project><Faces><Face y=\"20.3\" x=\"20.3\"><Contour y=\"0.98\" x=\"0.78\"><MoveTo y=\"13.5555334\" x=\"12.9\"/></Contour></Face></Faces><Paper h=\"435.6\" w=\"612.9999\" /></Project>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            Assert.AreEqual(true, errors.Length == 0);

            paperXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><Project><Faces><Face y=\"20.3\" x=\"20.3\"><Contour y=\"0.98\" x=\"0.78\"><LineTo y=\"13.5555334\" x=\"12.9\"/></Contour></Face></Faces><Paper h=\"435.6\" w=\"612.9999\" /></Project>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            Assert.AreEqual(true, errors.Length == 0);

            paperXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><Project><Faces><Face y=\"20.3\" x=\"20.3\"><Contour y=\"0.98\" x=\"0.78\"><CurveTo y=\"-13.5555334\" x=\"12.9\" cy=\"13.5555334\" cx=\"-12.9\"/></Contour></Face></Faces><Paper h=\"435.6\" w=\"612.9999\" /></Project>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            Assert.AreEqual(true, errors.Length == 0);

            paperXml = "<Project><Faces><Face y=\"20.3\" x=\"20.3\"><Contour y=\"0.98\" x=\"0.78\"><CurveTo y=\"-13.5555334\" x=\"12.9\" cy=\"13.5555334\" cx=\"-12.9\"/></Contour></Face></Faces><Paper h=\"435.6\" w=\"612.9999\" /></Project>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            Assert.AreEqual(true, errors.Length == 0);

            paperXml = "<Project><Faces><Face id=\"Face1\" x=\"97.2\" y=\"61.28\"><Contour x=\"0\" y=\"0\"><MoveTo x=\"142.75\" y=\"218.45\" /><LineTo x=\"142.9\" y=\"10\" /><LineTo x=\"138.8\" y=\"8.35\" /><CurveTo cx=\"133.75\" cy=\"6.35\" x=\"117.3\" y=\"3.5\" /><CurveTo cx=\"102.05\" cy=\"0.9\" x=\"95.1\" y=\"0.45\" /><LineTo x=\"87\" y=\"0\" /><LineTo x=\"88.3\" y=\"8.05\" /><CurveTo cx=\"91.2\" cy=\"24.75\" x=\"91.45\" y=\"25.85\" /><LineTo x=\"91.7\" y=\"27.15\" /><LineTo x=\"53.6\" y=\"27.15\" /><LineTo x=\"53.9\" y=\"25.85\" /><CurveTo cx=\"55.15\" cy=\"20.4\" x=\"57\" y=\"8.05\" /><LineTo x=\"58.3\" y=\"0\" /><LineTo x=\"50.2\" y=\"0.45\" /><CurveTo cx=\"43.05\" cy=\"0.9\" x=\"27.25\" y=\"3.45\" /><CurveTo cx=\"9.6\" cy=\"6.3\" x=\"4.5\" y=\"8.35\" /><LineTo x=\"0.4\" y=\"10\" /><LineTo x=\"0.2\" y=\"218.45\" /><MoveTo x=\"0.2\" y=\"218.45\" /><LineTo x=\"0\" y=\"270.3\" /><LineTo x=\"4.2\" y=\"271.85\" /><CurveTo cx=\"15\" cy=\"275.9\" x=\"36.35\" y=\"278.95\" /><CurveTo cx=\"56.5\" cy=\"281.85\" x=\"71.3\" y=\"281.85\" /><CurveTo cx=\"86.05\" cy=\"281.85\" x=\"106.25\" y=\"278.95\" /><CurveTo cx=\"127.6\" cy=\"275.9\" x=\"138.35\" y=\"271.85\" /><LineTo x=\"142.6\" y=\"270.3\" /><LineTo x=\"142.75\" y=\"218.45\" /><MoveTo x=\"123.55\" y=\"64.25\" /><LineTo x=\"123.55\" y=\"130.2\" /><CurveTo cx=\"123.6\" cy=\"133.1\" x=\"123.15\" y=\"133.9\" /><CurveTo cx=\"122.5\" cy=\"134.9\" x=\"119.9\" y=\"135.45\" /><CurveTo cx=\"113.2\" cy=\"136.75\" x=\"99.2\" y=\"138.1\" /><CurveTo cx=\"83.05\" cy=\"139.7\" x=\"72.35\" y=\"139.7\" /><CurveTo cx=\"61.45\" cy=\"139.7\" x=\"45.5\" y=\"138.15\" />"+
                "<CurveTo cx=\"31.65\" cy=\"136.8\" x=\"24.8\" y=\"135.45\" /><CurveTo cx=\"22.4\" cy=\"135\" x=\"21.75\" y=\"134.1\" /><CurveTo cx=\"21.15\" cy=\"133.4\" x=\"21.15\" y=\"131.25\" /><LineTo x=\"21.15\" y=\"64.25\" /><CurveTo cx=\"21.15\" cy=\"60.25\" x=\"25.3\" y=\"60.25\" /><LineTo x=\"119.4\" y=\"60.25\" /><CurveTo cx=\"123.55\" cy=\"60.25\" x=\"123.55\" y=\"64.25\" /></Contour></Face><Face id=\"Face2\" x=\"371.9\" y=\"59.93\"><Contour x=\"0\" y=\"0\"><MoveTo x=\"95.4\" y=\"250.6\" /><LineTo x=\"95.4\" y=\"250.6\" /><MoveTo x=\"144.95\" y=\"147.3\" /><LineTo x=\"144.35\" y=\"142.9\" /><CurveTo cx=\"144.15\" cy=\"141.45\" x=\"144.4\" y=\"139.4\" /><CurveTo cx=\"144.85\" cy=\"135.3\" x=\"146.9\" y=\"132.15\" /><CurveTo cx=\"153.3\" cy=\"122.25\" x=\"153.3\" y=\"108.05\" /><LineTo x=\"153.3\" y=\"0\" /><LineTo x=\"145.25\" y=\"2\" /><CurveTo cx=\"137.7\" cy=\"3.85\" x=\"125.45\" y=\"5.65\" /><CurveTo cx=\"100.95\" cy=\"9.3\" x=\"76.65\" y=\"9.3\" /><CurveTo cx=\"52.35\" cy=\"9.3\" x=\"27.9\" y=\"5.65\" /><LineTo x=\"8.1\" y=\"2\" /><LineTo x=\"0\" y=\"0\" /><LineTo x=\"0\" y=\"108.05\" /><CurveTo cx=\"0\" cy=\"122.25\" x=\"6.4\" y=\"132.15\" /><CurveTo cx=\"9.05\" cy=\"136.2\" x=\"9.05\" y=\"141.4\" /><LineTo x=\"8.95\" y=\"142.95\" /><LineTo x=\"8.6\" y=\"147.25\" /><MoveTo x=\"8.6\" y=\"147.25\" /><LineTo x=\"9.05\" y=\"240.1\" /><LineTo x=\"8.8\" y=\"262.3\" /><LineTo x=\"13.35\" y=\"263.8\" /><CurveTo cx=\"38.15\" cy=\"271.95\" x=\"76.9\" y=\"271.95\" /><CurveTo cx=\"115.3\" cy=\"271.95\" x=\"140.05\" y=\"263.9\" /><LineTo x=\"144.5\" y=\"262.5\" /><LineTo x=\"144.95\" y=\"147.3\" /><MoveTo x=\"95.4\" y=\"250.6\" /><LineTo x=\"58.4\" y=\"250.6\" /><LineTo x=\"58.4\" y=\"241.95\" /><LineTo x=\"95.4\" y=\"241.95\" /><LineTo x=\"95.4\" y=\"250.6\" /></Contour></Face></Faces><Paper w=\"612\" h=\"435.6\" /></Project>";
            errors = BCPaperImport.Validate(paperXml);
            Console.WriteLine();
            Console.WriteLine("XML: {0}", paperXml);
            Assert.AreEqual(true, errors.Length == 0);

        }
    }
}