using System.Data;
using System.IO;

using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Entity;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class BCCarrierTest {
        public BCCarrierTest() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void NewCarrierTest() {
            Carrier carrier = BCCarrier.NewCarrier();
            Assert.IsNotNull(carrier);
            Assert.AreEqual(0, carrier.Id);
            Assert.AreEqual(string.Empty, carrier.Name);
        }

        [Test]
        public void SaveNewCarrierWithoutIconTest() {
            Carrier carrier = BCCarrier.NewCarrier();
            carrier.Name = string.Empty.PadRight(10, '1');
            BCCarrier.Save(carrier);

            Assert.IsTrue(carrier.Id > 0);

            Carrier dbCarrier = DOCarrier.GetCarrier(carrier);

            Assert.IsNotNull(dbCarrier);
            Assert.AreEqual(carrier.Id, dbCarrier.Id);
            Assert.AreEqual(carrier.Name, dbCarrier.Name);

            byte[] icon = BCCarrier.GetCarrierIcon(carrier);
            Assert.IsNotNull(icon);
            Assert.AreEqual(0, icon.Length);
        }

        [Test]
        public void SaveNewCarrierWithIconTest() {
            Carrier carrier = BCCarrier.NewCarrier();
            carrier.Name = string.Empty.PadRight(10, '1');

            byte[] icon = {1, 2, 3};

            BCCarrier.Save(carrier, new MemoryStream(icon), null);

            Assert.IsTrue(carrier.Id > 0);

            Carrier dbCarrier = DOCarrier.GetCarrier(carrier);

            Assert.IsNotNull(dbCarrier);
            Assert.AreEqual(carrier.Id, dbCarrier.Id);
            Assert.AreEqual(carrier.Name, dbCarrier.Name);

            byte[] dbIcon = BCCarrier.GetCarrierIcon(carrier);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        public void SaveExistingCarrierWithoutIconTest() {
            string carrierName1 = string.Empty.PadRight(10, '1');
            string carrierName2 = string.Empty.PadRight(10, '2');
            byte[] icon = {1, 2, 3};

            Carrier carrier = BCCarrier.NewCarrier();
            carrier.Name = carrierName1;
            BCCarrier.Save(carrier, new MemoryStream(icon), null);

            carrier.Name = carrierName2;
            BCCarrier.Save(carrier);
            
            Carrier dbCarrier = DOCarrier.GetCarrier(carrier);

            Assert.IsNotNull(dbCarrier);
            Assert.AreEqual(carrier.Id, dbCarrier.Id);
            Assert.AreEqual(carrierName2, dbCarrier.Name);

            byte[] dbIcon = BCCarrier.GetCarrierIcon(carrier);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        public void SaveExistingCarrierWithIconTest() {
            string carrierName1 = string.Empty.PadRight(10, '1');
            string carrierName2 = string.Empty.PadRight(10, '2');

            byte[] icon1 = {1, 2, 3};
            byte[] icon2 = {4, 5, 6, 7};

            Carrier carrier = BCCarrier.NewCarrier();
            carrier.Name = carrierName1;

            BCCarrier.Save(carrier, new MemoryStream(icon1), null);

            carrier.Name = carrierName2;
            BCCarrier.Save(carrier, new MemoryStream(icon2), null);
            
            Carrier dbCarrier = DOCarrier.GetCarrier(carrier);

            Assert.IsNotNull(dbCarrier);
            Assert.AreEqual(carrier.Id, dbCarrier.Id);
            Assert.AreEqual(carrierName2, dbCarrier.Name);

            byte[] dbIcon = BCCarrier.GetCarrierIcon(carrier);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon2.Length, dbIcon.Length);
            for (int index = 0; index < icon2.Length; index++) {
                Assert.AreEqual(icon2[index], dbIcon[index]);
            }
        }

        [Test]
        public void ChangeIconTest() {
            Carrier carrier = BCCarrier.NewCarrier();
            carrier.Name = string.Empty.PadRight(10, '1');
            byte[] icon1 = {1, 2, 3};
            byte[] icon2 = {4, 5, 6, 7};

            BCCarrier.Save(carrier, new MemoryStream(icon1), null);

            BCCarrier.ChangeIcon(carrier, null);

            byte[] dbIcon = BCCarrier.GetCarrierIcon(carrier);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon1.Length, dbIcon.Length);
            for (int index = 0; index < icon1.Length; index++) {
                Assert.AreEqual(icon1[index], dbIcon[index]);
            }
            
            BCCarrier.ChangeIcon(carrier, new MemoryStream(0));

            dbIcon = BCCarrier.GetCarrierIcon(carrier);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon1.Length, dbIcon.Length);
            for (int index = 0; index < icon1.Length; index++) {
                Assert.AreEqual(icon1[index], dbIcon[index]);
            }

            BCCarrier.ChangeIcon(carrier, new MemoryStream(icon2));

            dbIcon = BCCarrier.GetCarrierIcon(carrier);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon2.Length, dbIcon.Length);
            for (int index = 0; index < icon2.Length; index++) {
                Assert.AreEqual(icon2[index], dbIcon[index]);
            }
        }

        [Test]
        public void GetCarrierIconTest() {
            Carrier carrier = BCCarrier.NewCarrier();
            carrier.Name = string.Empty.PadRight(10, '1');
            byte[] icon = {1, 2, 3};

            BCCarrier.Save(carrier, new MemoryStream(icon), null);

            byte[] dbIcon = BCCarrier.GetCarrierIcon(carrier);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        [Ignore("Not implemented")]
        public void AddRemoveDeviceFromCarrierTest() {}

        [Ignore("Not implemented")]
        public void GetCarrierListTest() {}

        [Test]
        public void DeleteTest() {
            Carrier carrier = BCCarrier.NewCarrier();
            carrier.Name = string.Empty.PadRight(10, '1');
            
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);

            Device device = BCDevice.NewDevice();
            device.Model = string.Empty.PadRight(10, '2');
            device.Brand = brand;
            device.DeviceType = DeviceType.Phone;
            device.Rating = 9;

            BCCarrier.Save(carrier);
            BCDevice.Save(device);
            BCDevice.AddToCarrier(device, carrier);

            BCCarrier.Delete(carrier);

            carrier = DOCarrier.GetCarrier(carrier);

            Assert.IsNull(carrier);
        }

    }
}