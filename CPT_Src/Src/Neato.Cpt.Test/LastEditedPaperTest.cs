using System.Data;

using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Data.Scheme;
using Neato.Cpt.Entity;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class LastEditedPaperTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void GetLastEditedPaperTest() {
            Customer customer = new Customer();
            customer.CustomerId = int.MaxValue/2;
            customer.Email = "UnitTestOnly@UnitTestOnly.com";
            Retailer retailer = new Retailer(1);
            LastEditedPaperData customerData = new LastEditedPaperData();
            customerData.LastEditedPaper.AddLastEditedPaperRow(customer.CustomerId, 1);
            BCCustomer.InsertCustomer(customer, customerData, retailer);
            LastEditedPaperData data =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            Assert.IsTrue(data.LastEditedPaper.Count >= 0);
        }

        [Test]
        public void UpdateLastEditedPaper() {
            Customer customer = new Customer();
            customer.CustomerId = int.MaxValue/2;
            customer.Email = "UnitTestOnly@UnitTestOnly.com";
            Retailer retailer = new Retailer(1);
            LastEditedPaperData customerData = new LastEditedPaperData();
            customerData.LastEditedPaper.AddLastEditedPaperRow(customer.CustomerId, 1);
            BCCustomer.InsertCustomer(customer, customerData, retailer);

            LastEditedPaperData data =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers = data.LastEditedPaper.Count;

            //Test add 1 row for this customer
            data.LastEditedPaper.AddLastEditedPaperRow(customer.CustomerId, 2);

            DOLastEditedPaper.UpdateLastEditedPaper(data);
            
            LastEditedPaperData data2 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers2 = data2.LastEditedPaper.Count;
            Assert.AreEqual(countPapers + 1, countPapers2);

            //Test modify 1 row for this customer
            data2.LastEditedPaper[0].PaperId = 3;

            DOLastEditedPaper.UpdateLastEditedPaper(data2);
            
            LastEditedPaperData data3 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers3 = data3.LastEditedPaper.Count;
            Assert.AreEqual(countPapers2, countPapers3);
            Assert.AreEqual(3, data3.LastEditedPaper[0].PaperId);
            Assert.AreEqual(2, data3.LastEditedPaper[1].PaperId);

            //Test clear 1 row for this customer
            data3.LastEditedPaper[0].Delete();
            DOLastEditedPaper.UpdateLastEditedPaper(data3);

            LastEditedPaperData data4 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers4 = data4.LastEditedPaper.Count;
            Assert.AreEqual(countPapers3 - 1, countPapers4);

            //Test clear all rows for this customer
            int count = data4.LastEditedPaper.Count;
            for(int i = count - 1; i >= 0; i--) {
                data4.LastEditedPaper[i].Delete();
            }
            DOLastEditedPaper.UpdateLastEditedPaper(data4);
            LastEditedPaperData data5 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers5 = data5.LastEditedPaper.Count;
            Assert.AreEqual(0, countPapers5);
        }
        [Test]
        public void ChangeLastEditedPaper() {
            Customer customer = new Customer();
            customer.CustomerId = int.MaxValue/2;
            customer.Email = "UnitTestOnly@UnitTestOnly.com";
            Retailer retailer = new Retailer(1);
            LastEditedPaperData customerData = new LastEditedPaperData();
            customerData.LastEditedPaper.AddLastEditedPaperRow(customer.CustomerId, 1);
            BCCustomer.InsertCustomer(customer, customerData, retailer);
            Customer customer2 = new Customer();
            customer2.CustomerId = int.MaxValue/2-1;
            customer2.Email = "UnitTestOnly2@UnitTestOnly.com";
            Retailer retailer2 = new Retailer(1);
            LastEditedPaperData customerData2 = new LastEditedPaperData();
            customerData2.LastEditedPaper.AddLastEditedPaperRow(customer2.CustomerId, 1);
            BCCustomer.InsertCustomer(customer2, customerData2, retailer2);

            //Delete all rows
            LastEditedPaperData data =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);

            int countPapers = data.LastEditedPaper.Count;
            for(int i = countPapers - 1; i >= 0; i--) {
                data.LastEditedPaper[i].Delete();
            }
            DOLastEditedPaper.UpdateLastEditedPaper(data);

            //Test add 1 row for this customer
            BCLastEditedPaper.ChangeLastEditedPaper(customer, new PaperBase(1));
            
            LastEditedPaperData data2 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers2 = data2.LastEditedPaper.Count;
            Assert.AreEqual(1, countPapers2);
            Assert.AreEqual(1, data2.LastEditedPaper[0].PaperId);
            Assert.AreEqual(customer.CustomerId, data2.LastEditedPaper[0].CustomerId);

            //Test add +1 row for this customer
            BCLastEditedPaper.ChangeLastEditedPaper(customer, new PaperBase(2));
            
            LastEditedPaperData data3 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers3 = data3.LastEditedPaper.Count;
            Assert.AreEqual(2, countPapers3);
            Assert.AreEqual(2, data3.LastEditedPaper[0].PaperId);
            Assert.AreEqual(1, data3.LastEditedPaper[1].PaperId);

            //Test add +1 row for this customer (new paper==last edit paper)
            BCLastEditedPaper.ChangeLastEditedPaper(customer, new PaperBase(2));
            
            LastEditedPaperData data4 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers4 = data4.LastEditedPaper.Count;
            Assert.AreEqual(2, countPapers4);
            Assert.AreEqual(2, data4.LastEditedPaper[0].PaperId);
            Assert.AreEqual(1, data4.LastEditedPaper[1].PaperId);

            //Test add +1 row for this customer (new paper!=last edit paper)
            BCLastEditedPaper.ChangeLastEditedPaper(customer, new PaperBase(3));
            
            LastEditedPaperData data5 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer);
            int countPapers5 = data5.LastEditedPaper.Count;
            Assert.AreEqual(2, countPapers5);
            Assert.AreEqual(3, data5.LastEditedPaper[0].PaperId);
            Assert.AreEqual(2, data5.LastEditedPaper[1].PaperId);

            //Test add 1 row for other customer
            BCLastEditedPaper.ChangeLastEditedPaper(customer2, new PaperBase(1));
            
            LastEditedPaperData data6 =
                DOLastEditedPaper.EnumerateLastEditedPaperByCustomer(customer2);
            int countPapers6 = data6.LastEditedPaper.Count;
            Assert.AreEqual(1, countPapers6);
        }
    }
}