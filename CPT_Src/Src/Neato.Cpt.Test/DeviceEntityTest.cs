using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class DeviceEntityTest {
        public DeviceEntityTest() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {}

        [TearDown]
        public void TearDown() {}
        #endregion

        [Test]
        [Ignore("Doesn`t implemented")]
        public void DeviceConstructorTest() {}

        [Test]
        [Ignore("Doesn`t implemented")]
        public void DeviceBaseConstructorTest() {}

        [Test]
        [Ignore("Doesn`t implemented")]
        public void DeviceBaseEqualTest() {}

        [Test]
        [Ignore("Doesn`t implemented")]
        public void DeviceEqualTest() {}
    }
}