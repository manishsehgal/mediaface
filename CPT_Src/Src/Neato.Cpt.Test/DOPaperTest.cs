using System.Data;
using System.Drawing;

using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Entity;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class DOPaperTest {
        public DOPaperTest() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void PaperInsertTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);
            brand = DOPaperBrand.GetPaperBrand(brand);
            Assert.IsNotNull(brand);

            string paperName = string.Empty.PadRight(1000, 't');
            Paper paper = BCPaper.NewPaper();
            paper.Name = paperName;
            paper.Size = new SizeF(100, 100);
            paper.Brand = brand;

            DOPaper.Add(paper);

            Assert.IsNotNull(paper);
            Assert.AreEqual(false, paper.IsNew);
            Assert.IsTrue(paper.Id > 0);

            paper = DOPaper.GetPaper(paper);

            Assert.IsNotNull(paper);
            Assert.AreEqual(false, paper.IsNew);
            Assert.IsTrue(paper.Id > 0);
            Assert.AreEqual(paperName.Substring(0, 64), paper.Name);
            Assert.AreEqual(100, paper.Size.Width);
            Assert.AreEqual(100, paper.Size.Height);
            Assert.AreEqual(brand, paper.Brand);

            byte[] dbIcon = DOPaper.GetPaperIcon(paper);
            Assert.AreEqual(0, dbIcon.Length);

        }

        [Test]
        public void PaperGetByIdTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);
            brand = DOPaperBrand.GetPaperBrand(brand);
            Assert.IsNotNull(brand);

            string paperName = string.Empty.PadRight(1000, 't');
            Paper paper = BCPaper.NewPaper();
            paper.Name = paperName;
            paper.Size = new SizeF(100, 100);
            paper.Brand = brand;

            DOPaper.Add(paper);

            Assert.IsNotNull(paper);
            Assert.AreEqual(false, paper.IsNew);
            Assert.IsTrue(paper.Id > 0);

            Paper dbPaper = DOPaper.GetPaper(paper);

            Assert.IsNotNull(dbPaper);
            Assert.AreEqual(false, dbPaper.IsNew);
            Assert.AreEqual(paper.Id, dbPaper.Id);
            Assert.AreEqual(paperName.Substring(0, 64), dbPaper.Name);
            Assert.AreEqual(100, dbPaper.Size.Width);
            Assert.AreEqual(100, dbPaper.Size.Height);
            Assert.AreEqual(brand, dbPaper.Brand);
        }

        [Test]
        public void EnumerateAllPaperTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);
            brand = DOPaperBrand.GetPaperBrand(brand);
            Assert.IsNotNull(brand);

            string paperName1 = string.Empty.PadRight(1000, '1');
            string paperName2 = string.Empty.PadRight(1000, '2');
            Paper paper1 = BCPaper.NewPaper();
            paper1.Name = paperName1;
            paper1.Size = new SizeF(100, 100);
            paper1.Brand = brand;

            Paper paper2 = BCPaper.NewPaper();
            paper2.Name = paperName2;
            paper2.Size = new SizeF(100, 100);
            paper2.Brand = brand;

            PaperBase[] papersBefore = DOPaper.EnumeratePaperByParam(null, DeviceType.Undefined, null, null, "ait", null);
            Assert.IsNotNull(papersBefore);

            DOPaper.Add(paper1);
            Assert.IsNotNull(paper1);

            DOPaper.Add(paper2);
            Assert.IsNotNull(paper2);

            PaperBase[] papersAfter = DOPaper.EnumeratePaperByParam(null, DeviceType.Undefined, null, null, "ait",null);
            Assert.IsNotNull(papersAfter);
            Assert.AreEqual(papersBefore.Length + 2, papersAfter.Length);
        }

        [Test]
        public void EnumeratePaperByPaperBrandTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);
            brand = DOPaperBrand.GetPaperBrand(brand);
            Assert.IsNotNull(brand);

            string paperName1 = string.Empty.PadRight(1000, '1');
            string paperName2 = string.Empty.PadRight(1000, '2');

            PaperBase[] papersBefore = DOPaper.EnumeratePaperByParam(brand, DeviceType.Undefined, null, null, "ait",null);
            Assert.IsNotNull(papersBefore);

            Paper paper1 = BCPaper.NewPaper();
            paper1.Name = paperName1;
            paper1.Size = new SizeF(100, 100);
            paper1.Brand = brand;

            Paper paper2 = BCPaper.NewPaper();
            paper2.Name = paperName2;
            paper2.Size = new SizeF(100, 100);
            paper2.Brand = brand;

            DOPaper.Add(paper1);
            Assert.IsNotNull(paper1);

            DOPaper.Add(paper2);
            Assert.IsNotNull(paper2);

            PaperBase[] papersAfter = DOPaper.EnumeratePaperByParam(brand, DeviceType.Undefined, null, null, "ait", null);
            Assert.IsNotNull(papersAfter);
            Assert.AreEqual(papersBefore.Length + 2, papersAfter.Length);
        }

        [Test]
        public void PaperDeleteTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);
            brand = DOPaperBrand.GetPaperBrand(brand);
            Assert.IsNotNull(brand);

            string paperName = string.Empty.PadRight(1000, 't');
            Paper paper = BCPaper.NewPaper();
            paper.Name = paperName;
            paper.Size = new SizeF(100, 100);
            paper.Brand = brand;

            DOPaper.Add(paper);

            Assert.IsNotNull(paper);
            Assert.AreEqual(false, paper.IsNew);
            Assert.IsTrue(paper.Id > 0);

            DOPaper.Delete(paper);

            Paper dbPaper = DOPaper.GetPaper(paper);
            Assert.IsNull(dbPaper);
        }

        [Test]
        public void PaperUpdateTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);
            brand = DOPaperBrand.GetPaperBrand(brand);
            Assert.IsNotNull(brand);

            string paperName = string.Empty.PadRight(1000, 't');
            Paper paper = BCPaper.NewPaper();
            paper.Name = paperName;
            paper.Size = new SizeF(100, 100);
            paper.Brand = brand;

            DOPaper.Add(paper);
            Assert.IsNotNull(paper);

            string paperName2 = string.Empty.PadRight(1000, 'q');
            paper.Name = paperName2;

            DOPaper.Update(paper);

            Assert.IsNotNull(paper);
            Assert.AreEqual(false, paper.IsNew);
            Assert.IsTrue(paper.Id > 0);

            Paper dbPaper = DOPaper.GetPaper(paper);

            Assert.IsNotNull(dbPaper);
            Assert.AreEqual(false, dbPaper.IsNew);
            Assert.AreEqual(paper.Id, dbPaper.Id);
            Assert.AreEqual(paperName2.Substring(0, 64), dbPaper.Name);

            byte[] dbIcon = DOPaper.GetPaperIcon(paper);
            Assert.AreEqual(0, dbIcon.Length);
        }

        [Test]
        public void PaperIconFullTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);
            brand = DOPaperBrand.GetPaperBrand(brand);
            Assert.IsNotNull(brand);

            string paperName = string.Empty.PadRight(1000, 't');
            Paper paper = BCPaper.NewPaper();
            paper.Name = paperName;
            paper.Size = new SizeF(100, 100);
            paper.Brand = brand;

            DOPaper.Add(paper);
            Assert.IsNotNull(paper);

            byte[] dbIcon = DOPaper.GetPaperIcon(paper);
            Assert.AreEqual(0, dbIcon.Length);

            byte[] icon = {1, 2, 3};
            DOPaper.UpdateIcon(paper, icon);

            dbIcon = DOPaper.GetPaperIcon(paper);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        public void EnumeratePaperByParamPaperNameTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = "test";
            DOPaperBrand.Add(brand);

            Paper paper = BCPaper.NewPaper();
            paper.Name = "TestPaperNameForUnitTests_1";
            paper.Size = new SizeF(100, 100);
            paper.Brand = brand;
            DOPaper.Add(paper);

            Paper paper2 = BCPaper.NewPaper();
            paper2.Name = "TestPaperNameForUnitTests_2";
            paper2.Size = new SizeF(100, 100);
            paper2.Brand = brand;
            DOPaper.Add(paper2);

            PaperBase[] papers = DOPaper.EnumeratePaperByParam
                (null, DeviceType.Undefined, null, "TestPaperNameForUnitTests_1", "ait", null);
            Assert.AreEqual(1, papers.Length);

            PaperBase[] papers2 = DOPaper.EnumeratePaperByParam
                (null, DeviceType.Undefined, null, "TestPaperNameForUnitTests", "ait", null);
            Assert.AreEqual(2, papers2.Length);
        }
    }
}