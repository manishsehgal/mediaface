using System.Data;
using System.IO;

using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Entity;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class BCPaperBrandTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void NewPaperBrandTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            Assert.IsNotNull(brand);
            Assert.AreEqual(0, brand.Id);
            Assert.AreEqual(string.Empty, brand.Name);
        }

        [Test]
        public void SaveNewPaperBrandWithoutIconTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = string.Empty.PadRight(10, '1');
            BCPaperBrand.Save(brand);

            Assert.IsTrue(brand.Id > 0);

            PaperBrand dbBrand = DOPaperBrand.GetPaperBrand(brand);

            Assert.IsNotNull(dbBrand);
            Assert.AreEqual(brand.Id, dbBrand.Id);
            Assert.AreEqual(brand.Name, dbBrand.Name);

            byte[] icon = BCPaperBrand.GetPaperBrandIcon(brand);
            Assert.IsNotNull(icon);
            Assert.AreEqual(0, icon.Length);
        }

        [Test]
        public void SaveNewPaperBrandWithIconTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = string.Empty.PadRight(10, '1');

            byte[] icon = {1, 2, 3};

            BCPaperBrand.Save(brand, new MemoryStream(icon));

            Assert.IsTrue(brand.Id > 0);

            PaperBrand dbBrand = DOPaperBrand.GetPaperBrand(brand);

            Assert.IsNotNull(dbBrand);
            Assert.AreEqual(brand.Id, dbBrand.Id);
            Assert.AreEqual(brand.Name, dbBrand.Name);

            byte[] dbIcon = BCPaperBrand.GetPaperBrandIcon(brand);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        public void SaveExistingPaperBrandWithoutIconTest() {
            string brandName1 = string.Empty.PadRight(10, '1');
            string brandName2 = string.Empty.PadRight(10, '2');
            byte[] icon = {1, 2, 3};

            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = brandName1;
            BCPaperBrand.Save(brand, new MemoryStream(icon));

            brand.Name = brandName2;
            BCPaperBrand.Save(brand);

            PaperBrand dbBrand = DOPaperBrand.GetPaperBrand(brand);

            Assert.IsNotNull(dbBrand);
            Assert.AreEqual(brand.Id, dbBrand.Id);
            Assert.AreEqual(brandName2, dbBrand.Name);

            byte[] dbIcon = BCPaperBrand.GetPaperBrandIcon(brand);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        public void SaveExistingPaperBrandWithIconTest() {
            string brandName1 = string.Empty.PadRight(10, '1');
            string brandName2 = string.Empty.PadRight(10, '2');

            byte[] icon1 = {1, 2, 3};
            byte[] icon2 = {4, 5, 6, 7};

            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = brandName1;

            BCPaperBrand.Save(brand, new MemoryStream(icon1));

            brand.Name = brandName2;
            BCPaperBrand.Save(brand, new MemoryStream(icon2));

            PaperBrand dbBrand = DOPaperBrand.GetPaperBrand(brand);

            Assert.IsNotNull(dbBrand);
            Assert.AreEqual(brand.Id, dbBrand.Id);
            Assert.AreEqual(brandName2, dbBrand.Name);

            byte[] dbIcon = BCPaperBrand.GetPaperBrandIcon(brand);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon2.Length, dbIcon.Length);
            for (int index = 0; index < icon2.Length; index++) {
                Assert.AreEqual(icon2[index], dbIcon[index]);
            }
        }

        [Test]
        public void DeleteTest() {
            PaperBrand paperBrand = BCPaperBrand.NewPaperBrand();
            paperBrand.Name = string.Empty.PadRight(10, '1');
            byte[] brandIcon = {1, 2, 3};

            BCPaperBrand.Save(paperBrand, new MemoryStream(brandIcon));

            Paper paper = BCPaper.NewPaper();
            paper.Name = "test";
            paper.Brand = paperBrand;
            paper.PaperState = "a";
            Face face = BCFace.NewFace();
            face.AddName("", "face1");
            paper.AddFace(face, 0, 0, 0);
            BCPaper.Save(paper);

            Assert.IsNotNull(BCPaper.GetPaper(paper));
            Assert.IsNotNull(DOPaperBrand.GetPaperBrand(paperBrand));

            BCPaper.Delete(paper);
            BCPaperBrand.Delete(paperBrand);

            Assert.IsNull(BCPaper.GetPaper(paper));
            Assert.IsNull(DOPaperBrand.GetPaperBrand(paperBrand));
        }

        [Test]
        public void ChangeIconTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = string.Empty.PadRight(10, '1');
            byte[] icon1 = {1, 2, 3};
            byte[] icon2 = {4, 5, 6, 7};

            BCPaperBrand.Save(brand, new MemoryStream(icon1));

            BCPaperBrand.ChangeIcon(brand, null);

            byte[] dbIcon = BCPaperBrand.GetPaperBrandIcon(brand);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon1.Length, dbIcon.Length);
            for (int index = 0; index < icon1.Length; index++) {
                Assert.AreEqual(icon1[index], dbIcon[index]);
            }

            BCPaperBrand.ChangeIcon(brand, new MemoryStream(0));

            dbIcon = BCPaperBrand.GetPaperBrandIcon(brand);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon1.Length, dbIcon.Length);
            for (int index = 0; index < icon1.Length; index++) {
                Assert.AreEqual(icon1[index], dbIcon[index]);
            }

            BCPaperBrand.ChangeIcon(brand, new MemoryStream(icon2));

            dbIcon = BCPaperBrand.GetPaperBrandIcon(brand);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon2.Length, dbIcon.Length);
            for (int index = 0; index < icon2.Length; index++) {
                Assert.AreEqual(icon2[index], dbIcon[index]);
            }
        }

        [Test]
        public void GetPaperBrandIconTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = string.Empty.PadRight(10, '1');
            byte[] icon = {1, 2, 3};

            BCPaperBrand.Save(brand, new MemoryStream(icon));

            byte[] dbIcon = BCPaperBrand.GetPaperBrandIcon(brand);

            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }

        [Test]
        public void GetPaperBrandListTest() {
            PaperBrand brand = BCPaperBrand.NewPaperBrand();
            brand.Name = string.Empty.PadRight(10, '1');
            BCPaperBrand.Save(brand);

            int count = BCPaperBrand.GetPaperBrandList().Length;
            Assert.IsTrue(count > 0);
        }

    }
}