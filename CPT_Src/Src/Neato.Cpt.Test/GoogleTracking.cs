using System.Data;

using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Entity;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class GoogleTracking {
        public GoogleTracking() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void GetScriptCodeTest() {
            string code = BCGoogleTracking.GetCode();
            Assert.IsNotNull(code);
        }

        [Test]
        public void UpdareScriptCodeTest() {
            const string updatedScript = "Updated - (Test)";
            BCGoogleTracking.SetCode(updatedScript);
            string code = BCGoogleTracking.GetCode();
            Assert.IsNotNull(code);
            Assert.AreEqual(updatedScript, code);
        }

        [Test]
        public void GetGoogleTrackPageTest() {
            const string wrongUrl = "Updated - (Test)";
            GoogleTrackPage trackPage = BCGoogleTracking.GetGoogleTrackPage(wrongUrl);
            Assert.IsNotNull(trackPage);
            Assert.AreEqual(trackPage.EnableScript, true);
            Assert.AreEqual(trackPage.PageUrl, wrongUrl);
        }

        [Test]
        public void UpdateGoogleTrackPageTest() {
            const string wrongUrl = "Updated - (Test)";
            GoogleTrackPage trackPage = new GoogleTrackPage(wrongUrl, false);
            BCGoogleTracking.SetGoogleTrackPage(trackPage);
            trackPage = BCGoogleTracking.GetGoogleTrackPage(wrongUrl);
            Assert.IsNotNull(trackPage);
            Assert.AreEqual(trackPage.EnableScript, false);
            Assert.AreEqual(trackPage.PageUrl, wrongUrl);
        }
    }
}