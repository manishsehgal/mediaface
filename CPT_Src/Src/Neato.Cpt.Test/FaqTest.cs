using System.Data;
using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.DataBase;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class FaqTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void EnumTest() {
            FaqData data = BCFaq.Enum(new Retailer(1));
            Assert.IsNotNull(data);
        }

        [Test]
        public void UpdateInsertTest() {
            FaqData data = BCFaq.Enum(new Retailer(1));
            int count = data.Faq.Count;

            FaqData.FaqRow row = data.Faq.NewFaqRow();
            row.Question = "Q";
            row.Answer = "A";
            row.SortOrder = 1000;
            row.RetailerId = 1;
            data.Faq.AddFaqRow(row);

            DOFaq.Update(data);
            FaqData newData = BCFaq.Enum(new Retailer(1));

            Assert.AreEqual(count+1, newData.Faq.Count);
        }

        [Test]
        public void UpdateUpdateTest() {
            FaqData data = BCFaq.Enum(new Retailer(1));
            int index = data.Faq.Count;

            FaqData.FaqRow row = data.Faq.NewFaqRow();
            row.Question = "Q";
            row.Answer = "A";
            row.SortOrder = 1000;
            row.RetailerId = 1;
            data.Faq.AddFaqRow(row);

            DOFaq.Update(data);
            FaqData newData = BCFaq.Enum(new Retailer(1));

            newData.Faq[index].Question = "Q1";
            newData.Faq[index].Answer = "A1";
            newData.Faq[index].SortOrder = 2000;
            newData.Faq[index].RetailerId = 1;


            DOFaq.Update(newData);
            FaqData newData2 = BCFaq.Enum(new Retailer(1));

            Assert.AreEqual("Q1", newData2.Faq[index].Question);
            Assert.AreEqual("A1", newData2.Faq[index].Answer);
            Assert.AreEqual(2000, newData2.Faq[index].SortOrder);
            Assert.AreEqual(1, newData2.Faq[index].RetailerId);
        }

        [Test]
        public void UpdateDeleteTest() {
            FaqData data = BCFaq.Enum(new Retailer(1));

            FaqData.FaqRow row = data.Faq.NewFaqRow();
            row.Question = "Q";
            row.Answer = "A";
            row.SortOrder = 1000;
            row.RetailerId = 1;
            data.Faq.AddFaqRow(row);

            DOFaq.Update(data);
            FaqData newData = BCFaq.Enum(new Retailer(1));

            int count = newData.Faq.Count;

            newData.Faq[count-1].Delete();

            DOFaq.Update(newData);
            FaqData newData2 = BCFaq.Enum(new Retailer(1));
            int count2 = newData2.Faq.Count;

            Assert.AreEqual(count-1, count2);
        }
    }
}