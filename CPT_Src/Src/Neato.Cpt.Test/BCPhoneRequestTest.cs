using System;
using System.Data;

using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Entity;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class BCPhoneRequestTest {
        public BCPhoneRequestTest() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        private void CreatePhoneRequest
            (string phoneManufacturer, string phoneModel, DateTime created) {
            DeviceRequest request = new DeviceRequest(phoneManufacturer, phoneModel, created);
            DODeviceRequest.InsertPhoneRequest(request, new Retailer(1));
        }

        private void CreatePhoneRequest
            (string phoneManufacturer, string phoneModel,
            DateTime created, string emailAddress) {
            DeviceRequest request = new DeviceRequest(phoneManufacturer, phoneModel, created, emailAddress);
            DODeviceRequest.InsertPhoneRequest(request,new Retailer(1));
        }

        private void CreatePhoneRequest
            (string phoneManufacturer, string phoneModel,
            DateTime created, Retailer retailer) {
            DeviceRequest request = new DeviceRequest(phoneManufacturer, phoneModel, created);
            DODeviceRequest.InsertPhoneRequest(request, retailer);
        }

        private void DeleteExistingRequests() {
            foreach (DeviceRequest request in BCDeviceRequest.GetPhoneRequests(new Retailer(1))) {
                DODeviceRequest.DeletePhoneRequest(request.RequestDeviceId);               
            }
        }


        [Test]
        public void GetPhoneRequestReportByDatesAndBrandTest() {
            DeleteExistingRequests();

            //Insert new requests
            CreatePhoneRequest("Nokia", "3310", new DateTime(2001, 1, 1));
            CreatePhoneRequest("Nokia", "3310", new DateTime(2003, 1, 1));
            CreatePhoneRequest("Motorola", "v180", new DateTime(2001, 1, 1));
            CreatePhoneRequest("Nakia", "3310", new DateTime(2001, 1, 1));
            CreatePhoneRequest("Nukia", "3310", new DateTime(2001, 1, 1));

            //test all
            DataSet data = BCDeviceRequest.
                GetPhoneRequestReportByDatesAndBrand(null, false,
                new DateTime(2000, 1, 1), new DateTime(2005, 1, 1),new Retailer(1));
            Assert.AreEqual(4, data.Tables[0].Rows.Count);
            Assert.AreEqual("Nokia 3310", data.Tables[0].Rows[0]["Model name"]);
            Assert.AreEqual(2, data.Tables[0].Rows[0]["Requests"]);
            Assert.AreEqual("Motorola v180", data.Tables[0].Rows[1]["Model name"]);
            Assert.AreEqual(1, data.Tables[0].Rows[1]["Requests"]);
            Assert.AreEqual("Nakia 3310", data.Tables[0].Rows[2]["Model name"]);
            Assert.AreEqual(1, data.Tables[0].Rows[2]["Requests"]);
            Assert.AreEqual("Nukia 3310", data.Tables[0].Rows[3]["Model name"]);
            Assert.AreEqual(1, data.Tables[0].Rows[3]["Requests"]);

            //test Nokia
            DataSet data2 = BCDeviceRequest.
                GetPhoneRequestReportByDatesAndBrand("Nokia", false,
                new DateTime(2000, 1, 1), new DateTime(2005, 1, 1),new Retailer(1));
            Assert.AreEqual(1, data2.Tables[0].Rows.Count);
            Assert.AreEqual("Nokia 3310", data2.Tables[0].Rows[0]["Model name"]);
            Assert.AreEqual(2, data2.Tables[0].Rows[0]["Requests"]);

            //test Others Brands
            DataSet data3 = BCDeviceRequest.
                GetPhoneRequestReportByDatesAndBrand("", true,
                new DateTime(2000, 1, 1), new DateTime(2005, 1, 1),new Retailer(1));
            Assert.AreEqual(2, data3.Tables[0].Rows.Count);
            Assert.AreEqual("Nakia 3310", data3.Tables[0].Rows[0]["Model name"]);
            Assert.AreEqual(1, data3.Tables[0].Rows[0]["Requests"]);
            Assert.AreEqual("Nukia 3310", data3.Tables[0].Rows[1]["Model name"]);
            Assert.AreEqual(1, data3.Tables[0].Rows[1]["Requests"]);

            //test date range + Nokia
            DataSet data4 = BCDeviceRequest.
                GetPhoneRequestReportByDatesAndBrand("Nokia", false,
                new DateTime(2001, 1, 1), new DateTime(2002, 1, 1),new Retailer(1));
            Assert.AreEqual(1, data4.Tables[0].Rows.Count);
            Assert.AreEqual("Nokia 3310", data4.Tables[0].Rows[0]["Model name"]);
            Assert.AreEqual(1, data4.Tables[0].Rows[0]["Requests"]);
        }


        [Test]
        public void GetPhoneRequestEmailsReportByDatesTest() {
            DeleteExistingRequests();

            //Insert new requests
            CreatePhoneRequest("Nokia", "3310", new DateTime(2001, 1, 1), "user1@yahoo.com");
            CreatePhoneRequest("Nokia", "3310", new DateTime(2003, 1, 1), "user1@yahoo.com");
            CreatePhoneRequest("Motorola", "v180", new DateTime(2001, 1, 1), "user1@yahoo.com");
            CreatePhoneRequest("Nakia", "3310", new DateTime(2001, 1, 1), "user2@yahoo.com");
            CreatePhoneRequest("Nukia", "3310", new DateTime(2001, 1, 1), "user3@yahoo.com");

            DataSet data = BCDeviceRequest.
                GetPhoneRequestEmailsReportByDates
                (new DateTime(2000, 1, 1), new DateTime(2002, 1, 1),new Retailer(1));
            Assert.AreEqual(4, data.Tables[0].Rows.Count);
            Assert.AreEqual("Motorola v180", data.Tables[0].Rows[0]["Model name"]);
            Assert.AreEqual("user1@yahoo.com", data.Tables[0].Rows[0]["Email"]);
            Assert.AreEqual("Nakia 3310", data.Tables[0].Rows[1]["Model name"]);
            Assert.AreEqual("user2@yahoo.com", data.Tables[0].Rows[1]["Email"]);
            Assert.AreEqual("Nokia 3310", data.Tables[0].Rows[2]["Model name"]);
            Assert.AreEqual("user1@yahoo.com", data.Tables[0].Rows[2]["Email"]);
            Assert.AreEqual("Nukia 3310", data.Tables[0].Rows[3]["Model name"]);
            Assert.AreEqual("user3@yahoo.com", data.Tables[0].Rows[3]["Email"]);
        }

        
        [Test]
        public void GetPhoneRequestEmailsReportByDatesTest2() {
            string retailerName1 = "UTestRetailer1";
            string retailerDisplayName1 = "UTestRetailerDisplayName1";
            Retailer retailer1 =
                new Retailer(0, retailerName1, retailerDisplayName1, "");
            BCRetailers.InsertRetailer( retailer1, new byte[0], new Retailer[0]);
            BCRetailers.Update(retailer1, new Retailer[0]);

            string retailerName2 = "UTestRetailer2";
            string retailerDisplayName2 = string.Empty;
            Retailer retailer2 =
                new Retailer(0, retailerName2, retailerDisplayName2, "");
            BCRetailers.InsertRetailer(retailer2, new byte[0], new Retailer[0]);
            BCRetailers.Update(retailer2, new Retailer[0]);

            string retailerName3 = "UTestRetailer3";
            string retailerDisplayName3 = null;
            Retailer retailer3 =
                new Retailer(0, retailerName3, retailerDisplayName3, "");
            BCRetailers.InsertRetailer(retailer3, new byte[0], new Retailer[0]);
            BCRetailers.Update(retailer3, new Retailer[0]);

            CreatePhoneRequest("Nokia", "1111", new DateTime(2001, 1, 1), retailer1);
            CreatePhoneRequest("Nokia", "2222", new DateTime(2001, 1, 1), retailer2);
            CreatePhoneRequest("Nokia", "3333", new DateTime(2001, 1, 1), retailer3);

            DataSet ds = BCDeviceRequest.GetPhoneRequestEmailsReportByDates(
                DateTime.Now.AddYears(-100), DateTime.Now.AddYears(+100), null);

            DataRow request1Row = ds.Tables[0].Select("[Model name] = '" + "Nokia 1111" + "'")[0];
            DataRow request2Row = ds.Tables[0].Select("[Model name] = '" + "Nokia 2222" + "'")[0];
            DataRow request3Row = ds.Tables[0].Select("[Model name] = '" + "Nokia 3333" + "'")[0];

            Assert.AreEqual(retailerDisplayName1, request1Row["Retailer"]);
            Assert.AreEqual(retailerName2, request2Row["Retailer"]);
            Assert.AreEqual(retailerName3, request3Row["Retailer"]);
        }
    }
}