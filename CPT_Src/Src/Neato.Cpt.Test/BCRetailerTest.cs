using System.Data;

using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Entity;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class BCRetailerTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void GetRetailerByNameTest() {
            string retailerName = "UTestRetailer";
            Retailer retailer = new Retailer(retailerName);
            BCRetailers.InsertRetailer(retailer, new byte[0], new Retailer[0]);

            Retailer retailerFromDB = BCRetailers.GetRetailerByName(retailerName);
            Assert.IsTrue(retailerFromDB.Id > 1);
        }
    }
}