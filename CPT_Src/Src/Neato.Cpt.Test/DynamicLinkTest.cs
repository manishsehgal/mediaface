using System.Data;

using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Entity;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class DynamicLinkTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void GetDefaultPageLinksTest() {
            LinkItem[] links = DODynamicLink.GetLinks(1, BCDynamicLink.DefaultMenuPage, Place.Header, Align.None, "");
            Assert.IsTrue(links.Length > 0);
            
            links = DODynamicLink.GetLinks(1, BCDynamicLink.DefaultMenuPage, Place.Header, Align.None, "US");
            Assert.IsTrue(links.Length > 0);

            //links = DODynamicLink.GetLinks(BCDynamicLink.DefaultMenuPage, Place.Footer, Align.Left, "");
            //Assert.AreEqual(1, links.Length);

            //links = DODynamicLink.GetLinks(BCDynamicLink.DefaultUrl, Place.Footer, Align.Right, "");
            //Assert.AreEqual(3, links.Length);
        }
    }
}