using System.Data;
using System.Xml;

using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Entity;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class DeviceTest {
        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        public void GetDevicesByPaper() {
            Device[] devices = BCDevice.GetDeviceListByPaper(new PaperBase(-1));
            Assert.AreEqual(0, devices.Length);

            Device[] devices2 = BCDevice.GetDeviceListByPaper(new PaperBase(1));
            Assert.AreEqual(1, devices2.Length);
        }

        // TODO: move this test to PaperTest class
        [Test]
        public void PrintDevicesToPdf() {
            PaperBase paperBase = DOPaper.EnumeratePaperByParam(null, DeviceType.Undefined, null, null, null,null)[0];
            Customer customer = null;

            Project project = BCProject.GetProject(customer, paperBase, DeviceType.Undefined);
            PdfWrapper.GeneratePdf(project, new XmlDocument(), new XmlDocument());
        }

        [Test]
        public void DeviceInsertTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNotNull(brand);

            string deviceModel = string.Empty.PadRight(1000, 't');
            Device device = BCDevice.NewDevice();
            device.Model = deviceModel;
            device.DeviceType = DeviceType.Phone;
            device.Brand = brand;

            DODevice.Add(device);

            Assert.IsNotNull(device);
            Assert.AreEqual(false, device.IsNew);
            Assert.IsTrue(device.Id > 0);

            device = DODevice.GetDevice(device);

            Assert.IsNotNull(device);
            Assert.AreEqual(false, device.IsNew);
            Assert.IsTrue(device.Id > 0);
            Assert.AreEqual(deviceModel.Substring(0, 50), device.Model);
            Assert.AreEqual(DeviceType.Phone, device.DeviceType);
            Assert.AreEqual(brand, device.Brand);

            byte[] dbIcon = DODevice.GetDeviceIcon(device);
            Assert.AreEqual(0, dbIcon.Length);

        }

        [Test]
        public void DeviceGetByIdTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNotNull(brand);

            string deviceModel = string.Empty.PadRight(1000, 't');
            Device device = BCDevice.NewDevice();
            device.Model = deviceModel;
            device.DeviceType = DeviceType.Phone;
            device.Brand = brand;

            DODevice.Add(device);

            Assert.IsNotNull(device);
            Assert.AreEqual(false, device.IsNew);
            Assert.IsTrue(device.Id > 0);

            Device dbDevice = DODevice.GetDevice(device);

            Assert.IsNotNull(dbDevice);
            Assert.AreEqual(false, dbDevice.IsNew);
            Assert.AreEqual(device.Id, dbDevice.Id);
            Assert.AreEqual(deviceModel.Substring(0, 50), dbDevice.Model);
            Assert.AreEqual(DeviceType.Phone, dbDevice.DeviceType);
            Assert.AreEqual(brand, dbDevice.Brand);
        }

        [Test]
        public void EnumerateAllDeviceTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNotNull(brand);

            string deviceModel1 = string.Empty.PadRight(1000, '1');
            string deviceModel2 = string.Empty.PadRight(1000, '2');
            Device device1 = BCDevice.NewDevice();
            device1.Model = deviceModel1;
            device1.DeviceType = DeviceType.Phone;
            device1.Brand = brand;

            Device device2 = BCDevice.NewDevice();
            device2.Model = deviceModel2;
            device2.DeviceType = DeviceType.Phone;
            device2.Brand = brand;

            Device[] devicesBefore = DODevice.EnumerateDeviceByParam(null, DeviceType.Undefined, null, null);
            Assert.IsNotNull(devicesBefore);

            DODevice.Add(device1);
            Assert.IsNotNull(device1);

            DODevice.Add(device2);
            Assert.IsNotNull(device2);

            Device[] devicesAfter = DODevice.EnumerateDeviceByParam(null, DeviceType.Undefined, null, null);
            Assert.IsNotNull(devicesAfter);
            Assert.AreEqual(devicesBefore.Length + 2, devicesAfter.Length);
        }

        [Test]
        public void EnumerateDeviceByDeviceTypeTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNotNull(brand);

            string deviceModel1 = string.Empty.PadRight(1000, '1');
            string deviceModel2 = string.Empty.PadRight(1000, '2');

            Device[] devicesBefore = DODevice.EnumerateDeviceByParam(null, DeviceType.Phone, null, null);
            Assert.IsNotNull(devicesBefore);

            Device device1 = BCDevice.NewDevice();
            device1.Model = deviceModel1;
            device1.DeviceType = DeviceType.Phone;
            device1.Brand = brand;

            Device device2 = BCDevice.NewDevice();
            device2.Model = deviceModel2;
            device2.DeviceType = DeviceType.Phone;
            device2.Brand = brand;

            DODevice.Add(device1);
            Assert.IsNotNull(device1);

            DODevice.Add(device2);
            Assert.IsNotNull(device2);

            Device[] devicesAfter = DODevice.EnumerateDeviceByParam(null, DeviceType.Phone, null, null);
            Assert.IsNotNull(devicesAfter);
            Assert.AreEqual(devicesBefore.Length + 2, devicesAfter.Length);
        }

        [Test]
        public void DeviceDeleteTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNotNull(brand);

            string deviceModel = string.Empty.PadRight(1000, 't');
            Device device = BCDevice.NewDevice();
            device.Model = deviceModel;
            device.DeviceType = DeviceType.Phone;
            device.Brand = brand;

            DODevice.Add(device);

            Assert.IsNotNull(device);
            Assert.AreEqual(false, device.IsNew);
            Assert.IsTrue(device.Id > 0);

            DODevice.Delete(device);

            Device dbDevice = DODevice.GetDevice(device);
            Assert.IsNull(dbDevice);
        }

        [Test]
        public void DeviceUpdateTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNotNull(brand);

            string deviceModel = string.Empty.PadRight(1000, 't');
            Device device = BCDevice.NewDevice();
            device.Model = deviceModel;
            device.DeviceType = DeviceType.Phone;
            device.Brand = brand;

            DODevice.Add(device);
            Assert.IsNotNull(device);

            string deviceModel2 = string.Empty.PadRight(1000, 'q');
            device.Model = deviceModel2;

            DODevice.Update(device);

            Assert.IsNotNull(device);
            Assert.AreEqual(false, device.IsNew);
            Assert.IsTrue(device.Id > 0);

            Device dbDevice = DODevice.GetDevice(device);

            Assert.IsNotNull(dbDevice);
            Assert.AreEqual(false, dbDevice.IsNew);
            Assert.AreEqual(device.Id, dbDevice.Id);
            Assert.AreEqual(deviceModel2.Substring(0, 50), dbDevice.Model);

            byte[] dbIcon = DODevice.GetDeviceIcon(device);
            Assert.AreEqual(0, dbIcon.Length);
        }

        [Test]
        public void DeviceIconFullTest() {
            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();
            brand.Name = "test";
            DODeviceBrand.Add(brand);
            brand = DODeviceBrand.GetDeviceBrand(brand);
            Assert.IsNotNull(brand);

            string deviceModel = string.Empty.PadRight(1000, 't');
            Device device = BCDevice.NewDevice();
            device.Model = deviceModel;
            device.DeviceType = DeviceType.Phone;
            device.Brand = brand;

            DODevice.Add(device);
            Assert.IsNotNull(device);

            byte[] dbIcon = DODevice.GetDeviceIcon(device);
            Assert.AreEqual(0, dbIcon.Length);

            byte[] icon = {1, 2, 3};
            DODevice.UpdateIcon(device, icon);

            dbIcon = DODevice.GetDeviceIcon(device);
            Assert.IsNotNull(dbIcon);
            Assert.AreEqual(icon.Length, dbIcon.Length);
            for (int index = 0; index < icon.Length; index++) {
                Assert.AreEqual(icon[index], dbIcon[index]);
            }
        }
    }
}