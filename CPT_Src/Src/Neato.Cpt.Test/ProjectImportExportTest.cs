using System.Data;
using System.IO;

using Neato.Cpt.Business;
using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;

using NUnit.Framework;

namespace Neato.Cpt.Test {
    [TestFixture]
    public class ProjectImportExportTest {
        public ProjectImportExportTest() {}

        #region SetUp/TearDown
        [SetUp]
        public void SetUp() {
            DOGlobal.BeginTransaction(IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDown() {
            DOGlobal.RollbackAllActiveTransactions();
        }
        #endregion

        [Test]
        [ExpectedException(typeof(BusinessException), "File structure is corrupted")]
        public void ImportNullStreamTest() {
            Customer customer = null;
            BCProject.Import(customer, MemoryStream.Null);
        }

        [Test]
        [ExpectedException(typeof(BusinessException), "File structure is corrupted")]
        public void ImportEmptyStreamTest() {
            Customer customer = null;
            BCProject.Import(customer, new MemoryStream(0));
        }

        [Test]
        [ExpectedException(typeof(BusinessException), "File structure is corrupted")]
        public void ImportCorruptedTest() {
            Customer customer = null;
            MemoryStream data = new MemoryStream();
            byte[] buffer = new byte[] {0, 1, 2, 3, 4, 5, 6, 7};
            data.Write(buffer, 0, buffer.Length);
            BCProject.Import(customer, data);
        }

        [Test]
        public void CorrectExportImportWithoutCustomerUpdateTest() {
            Customer customer = null;
            PaperBase paper = DOPaper.EnumeratePaperByParam(null, DeviceType.Undefined, null, null, null,null)[0];
            Project project = BCProject.GetProject(customer, paper, DeviceType.Undefined);
            Stream data = BCProject.Export(project);
            Project importProject = BCProject.Import(customer, data);
            Assert.AreEqual(project.ProjectXml.OuterXml, importProject.ProjectXml.OuterXml);
            Assert.AreEqual(project.Size, importProject.Size);
            Assert.AreEqual(project.ProjectPaper, importProject.ProjectPaper);
        }

    }
}