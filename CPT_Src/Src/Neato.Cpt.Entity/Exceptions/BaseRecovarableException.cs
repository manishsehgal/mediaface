using System;
using System.Runtime.Serialization;

namespace Neato.Cpt.Entity.Exceptions {
    [Serializable]
    public abstract class BaseRecoverableException : ApplicationException {
        public BaseRecoverableException() {}

        public BaseRecoverableException(string message) : base(message) {}

        public BaseRecoverableException(string message, Exception innerException) : base(message, innerException) {}

        protected BaseRecoverableException(SerializationInfo info, StreamingContext context) : base(info, context) {}
    }
}