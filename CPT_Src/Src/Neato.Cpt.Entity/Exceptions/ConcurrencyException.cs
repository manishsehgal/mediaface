using System;
using System.Runtime.Serialization;

namespace Neato.Cpt.Entity.Exceptions {
    public class ConcurrencyException : BaseBusinessException {
        public ConcurrencyException() : base() {}

        public ConcurrencyException(string message) : base(message) {}

        public ConcurrencyException(string message, Exception innerException) : base(message, innerException) {}

        protected ConcurrencyException(SerializationInfo info, StreamingContext context) : base(info, context) {}
    }
}