namespace Neato.Cpt.Entity {
    public enum DeviceType {
        Undefined = 0,
        Phone     = 1,
        TabletPC  = 2,
        PDA       = 3,
        MP3Player = 4,
        Media     = 5,
        Sticker   = 6
    }
}