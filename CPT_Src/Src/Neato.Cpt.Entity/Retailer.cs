using System;
using System.Collections;

namespace Neato.Cpt.Entity {

	public enum DeviceAvailability {
		ec_NotAvailable = 0,
		ec_Available    = 1,
		ec_ComingSoon   = 2
	}

    public class Retailer {
        public class RetailerNameCompare : IComparer  {
            int IComparer.Compare( Object x, Object y )  {
                Retailer retX = x as Retailer;
                Retailer retY = y as Retailer;
                return string.Compare(retX.Name, retY.Name, true);
            }
        }

        private int retailerIdValue = 0;
        private string nameValue = string.Empty;
        private string displayNameValue = string.Empty;
        private string retailerUrlValue = string.Empty;
        public static string IdField = "ID";
        public const string NameField = "Name";
        private DeviceAvailability phoneAvailabilityValue = DeviceAvailability.ec_Available;
		private DeviceAvailability mp3AvailabilityValue = DeviceAvailability.ec_ComingSoon;
		private DeviceAvailability stickerAvailabilityValue = DeviceAvailability.ec_ComingSoon;
        private bool hasBrandedSiteValue = false;

        public Retailer() {}

        public Retailer(int id, string name, string displayName, string url) {
            retailerIdValue = id;
            nameValue = name;
            displayNameValue = displayName;
            retailerUrlValue = url;
        }

        public Retailer(int id) {
            this.retailerIdValue = id;
        }

        public Retailer(string name) {
            this.nameValue = name;
        }

        public int Id {
            set { retailerIdValue = value; }
            get { return retailerIdValue; }
        }

        public string Name {
            set { nameValue = value; }
            get { return nameValue; }
        }

        public string DisplayName {
            set { displayNameValue = value != null ? value : string.Empty; }
            get { return displayNameValue; }
        }

        public string Url {
            set { retailerUrlValue = value; }
            get { return retailerUrlValue; }
        }

        public DeviceAvailability PhoneAvailability {
            get { return phoneAvailabilityValue; }
            set { phoneAvailabilityValue = value; }
        }

		public DeviceAvailability Mp3Availability 
		{
			get { return mp3AvailabilityValue; }
			set { mp3AvailabilityValue = value; }
		}

		public DeviceAvailability StickerAvailability 
		{
			get { return stickerAvailabilityValue; }
			set { stickerAvailabilityValue = value; }
		}
        public bool HasBrandedSite 
		{
            get { return hasBrandedSiteValue; }
            set { hasBrandedSiteValue = value; }
        }

        #region Equals, GetHashCode, ==, !=
        public override bool Equals(object obj) {
            Retailer other = obj as Retailer;
            if (other == null) {
                return false;
            } else {
                return this.retailerIdValue == other.retailerIdValue;
            }
        }

        public override int GetHashCode() {
            return retailerIdValue.GetHashCode();
        }

        public static bool operator ==(Retailer first, Retailer second) {
            if (object.Equals(first, null)) {
                return object.Equals(second, null);
            }
            return first.Equals(second);
        }

        public static bool operator !=(Retailer first, Retailer second) {
            if (object.Equals(first, null)) {
                return !object.Equals(second, null);
            }
            return !first.Equals(second);
        }
        #endregion
    }
}