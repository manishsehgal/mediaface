namespace Neato.Cpt.Entity {
    public enum PaperType {
        DieCut = 1,
        Universal = 2
    }
}