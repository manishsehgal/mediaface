namespace Neato.Cpt.Entity {
    public class PlaceProperty {
        private int retailerIdValue = 0;
        private string placeValue = string.Empty;
        private string backgroundColorValue = "#000000";

        public int RetailerId {
            set { retailerIdValue = value; }
            get { return retailerIdValue; }
        }

        public string Place {
            set { placeValue = value; }
            get { return placeValue; }
        }

        public string BackgroundColor {
            set { backgroundColorValue = value; }
            get { return backgroundColorValue; }
        }

        public PlaceProperty() {}

        public PlaceProperty(int retailerId, string place, string backgroundColor) {
            retailerIdValue = retailerId;
            placeValue = place;
            backgroundColorValue = backgroundColor;
        }
    }
}