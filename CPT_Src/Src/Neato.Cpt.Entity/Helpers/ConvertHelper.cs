using System.IO;
using System.Text;
using System.Xml;

namespace Neato.Cpt.Entity.Helpers {
    public class ConvertHelper {
        private ConvertHelper() {}

        public static byte[] StreamToByteArray(Stream stream) {
            byte[] resultArray = new byte[stream.Length];
            stream.Position = 0;
            stream.Read(resultArray, 0, resultArray.Length);
            return resultArray;
        }

        public static byte[] XmlNodeToByteArray(XmlNode node) {
            MemoryStream stream = new MemoryStream();
            BinaryWriter wr = new BinaryWriter(stream);
            wr.Write(node.OuterXml.ToCharArray());
            return stream.ToArray();
        }

        public static XmlNode ByteArrayToXmlNode(byte[] array) {
            if (array.Length == 0) return null;
            MemoryStream stream = new MemoryStream(array);
            BinaryReader reader = new BinaryReader(stream);
            
            string nodeString = new string(reader.ReadChars((int)stream.Length));
            XmlDocument doc = new XmlDocument();
            doc.InnerXml = nodeString;
            return doc.FirstChild;
        }

        public static byte[] StringToByteArray(string str) {
            char[] chars = str.ToCharArray();
            Encoder uniEncoder = Encoding.UTF8.GetEncoder();
            int byteCount = uniEncoder.GetByteCount(chars, 0, chars.Length, true);
            byte[] bytes = new byte[byteCount];
            uniEncoder.GetBytes(chars, 0, chars.Length, bytes, 0, true);
            return bytes;
        }
    }
}