using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace Neato.Cpt.Entity.Helpers {
    public class ImageHelper {
        private ImageHelper() {}

        private static ImageCodecInfo jpegCodecInfo;

        static ImageHelper() {
            ImageCodecInfo[] encoders = ImageCodecInfo.GetImageEncoders();
            for(int j = 0; j < encoders.Length; ++j) {
                if(encoders[j].MimeType == "image/jpeg") {
                    jpegCodecInfo = encoders[j];
                    break;
                }
            }
        }

        public static byte[] ConvertImageToJpeg(byte[] imageArray) {
            Image image = null;
            Graphics gr = null;
            try {
                image = GetImage(imageArray);
                Bitmap bitmap = new Bitmap(image.Width, image.Height);
                bitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);
                gr = Graphics.FromImage(bitmap);
                gr.SmoothingMode = SmoothingMode.AntiAlias;
                //gr.Clear(Color.FromArgb(255, 255, 255));
                gr.DrawImage(image, 0, 0);
                return SaveToJpeg(bitmap);
            } catch {
                return new byte[0];
            } finally {
                if (gr != null) {
                    gr.Dispose();
                }
                if (image != null) {
                    image.Dispose();
                }
            }
        }

        private static byte[] SaveToJpeg(Image image) {
            using (MemoryStream chartStream = new MemoryStream()) {
                Encoder qualityEncoder = Encoder.Quality;
                EncoderParameter ratio = new EncoderParameter(qualityEncoder, 90L);
                EncoderParameters codecParams = new EncoderParameters(1);
                codecParams.Param[0] = ratio;
                image.Save(chartStream, jpegCodecInfo, codecParams);
                //image.Save(chartStream, ImageFormat.Jpeg);
                return ConvertHelper.StreamToByteArray(chartStream);
            }
        }

        public static byte[] SaveToPng(Image image) {
            using (MemoryStream chartStream = new MemoryStream()) {
                image.Save(chartStream, ImageFormat.Png);
                return ConvertHelper.StreamToByteArray(chartStream);
            }
        }

        /*
        public static void SaveToPngFile(Image image) {
            FileStream fileStream = new FileStream("E:\\tmp\\CPT\\asp\\test2.png", FileMode.Create);
            image.Save(fileStream, ImageFormat.Png);
            fileStream.Close();
        }
        */

        public static Image GetImage(byte[] imageArray) {
            using (MemoryStream chartStream = new MemoryStream(imageArray)) {
                Image image = Image.FromStream(chartStream);
                return image;
            }
        }

        private static bool ThumbnailCallback() {
            return false;
        }

        public static byte[] GetIcon(byte[] imageArray, int iconWidth, int iconHeight) {
            Image.GetThumbnailImageAbort myCallback = new Image.GetThumbnailImageAbort(ThumbnailCallback);

            Image image = GetImage(imageArray);
            if (image.Height > image.Width) {
                iconWidth = (int)Math.Round(iconWidth * (double)image.Width / image.Height);
            } else {
                iconHeight = (int)Math.Round(iconHeight * (double)image.Height / image.Width);
            }
            Image icon = new Bitmap(image).GetThumbnailImage(iconWidth, iconHeight, myCallback, IntPtr.Zero);
            return SaveToJpeg(icon);
        }

    }
}