namespace  Neato.Cpt.Entity.Helpers {
    public class CommandLineParser {
        private CommandLineParser() {}

        public static string GetValueByKey(string[] args, string key) {
            int keyPosition = GetKeyPosition(args, key);
            return
                (IsKeyPresent(args, key) &&
                keyPosition < args.Length + 1)
                ? args[keyPosition + 1]
                : string.Empty;
        }

        public static int GetKeyPosition(string[] args, string key) {
            for (int i = 0; i < args.Length; i++) {
                if (args[i] == key) {
                    return i;
                }
            }

            return int.MinValue;
        }

        public static bool IsKeyPresent(string[] args, string key) {
            return (GetKeyPosition(args, key) != int.MinValue);
        }

    }
}