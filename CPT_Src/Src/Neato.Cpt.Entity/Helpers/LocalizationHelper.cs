using System.Collections;

namespace Neato.Cpt.Entity.Helpers {
    public sealed class LocalizationHelper {
        private LocalizationHelper() {}

        public static string GetString(string culture, ICollection strings, string cultureField, string dataField) {
            string fullCulture = culture;
            string smallCulture = culture.Split('-')[0];
            string defaultCulture = string.Empty;
            
            string result = null;
            foreach (object data in strings) {
                string dataCulture = (string)ReflectionHelper.GetFieldOrPropertyValue(data, cultureField);
                string dataValue = (string)ReflectionHelper.GetFieldOrPropertyValue(data, dataField);
                
                if (result == null && dataCulture == defaultCulture) {
                    result = dataValue;
                } 

                if (dataCulture == smallCulture) {
                    result = dataValue;
                } 
                
                if (dataCulture == fullCulture) {
                    result = dataValue;
                    break;
                } 
            }
            if (result == null) {
                result = string.Empty;
            }

            return result;
        }
    }
}