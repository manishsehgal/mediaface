using System;

namespace Neato.Cpt.Entity {
    [Serializable]
    public class Logo {
        private byte[] logoImgValue;
        private byte[] flashLogoImgValue;

        public byte[] LogoImg {
            get { return logoImgValue; }
            set { logoImgValue = value; }
        }

        public byte[] FlashLogoImg {
            get { return flashLogoImgValue; }
            set { flashLogoImgValue = value; }
        }

        public Logo() {}

        public Logo(byte[] logoImg, byte[] flashLogoImg) {
            this.logoImgValue = logoImg;
            this.flashLogoImgValue = flashLogoImg;
        }
    }
}