using System;
using System.Collections;
using System.Drawing;
using System.Xml;

using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.Entity {
    [Serializable]
    public class Face: FaceBase {
        public Face(): base()  {}

        public Face(int id, XmlNode contour) : base(id) {
            this.contourValue = contour;
        }

        private Hashtable namesValue = new Hashtable();

        public void AddName(string culture, string name) {
            this.namesValue[culture] = name;
        }

        public string GetName(string culture) {
            return LocalizationHelper.GetString(culture, namesValue, "Key", "Value");
        }

        public IDictionaryEnumerator GetNames() {
            return this.namesValue.GetEnumerator();
        }

        private XmlNode contourValue = null;

        public XmlNode Contour {
            get { return contourValue; }
            set { contourValue = value; }
        }

        private PointF positionValue;

        public PointF Position {
            get { return positionValue; }
            set { positionValue = value; }
        }

        private float rotationValue = 0;

        public float Rotation {
            get { return rotationValue; }
            set { rotationValue = value; }
        }
    }
}