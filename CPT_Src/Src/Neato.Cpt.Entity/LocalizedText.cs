using System;

namespace Neato.Cpt.Entity {
    [Serializable]
    public class LocalizedText {
        private string textValue = string.Empty;
        private string cultureValue = string.Empty;

        public string Text {
            get { return textValue; }
            set { textValue = value; }
        }

        public string Culture {
            get { return cultureValue; }
            set { cultureValue = value; }
        }

        public LocalizedText() {}

        public LocalizedText(string culture, string text) {
            this.textValue = text;
            this.cultureValue = culture;
        }
    }
}