using System;

namespace Neato.Cpt.Entity {
    [Serializable]
	public class SpecialUser : SpecialUserBase {

        public SpecialUser() : base() {}

        public SpecialUser(int id, string email, bool showPaper, bool notifyPaperState) : base(id) {
            this.emailValue = email;
            this.showPaperValue = showPaper;
            this.notifyPaperStateValue = notifyPaperState;
        }

        private string emailValue = string.Empty;
        public  string Email {
            get { return emailValue; }
            set { emailValue = value; }
        }

        private bool showPaperValue = false;
        public  bool ShowPaper {
            get { return showPaperValue; }
            set { showPaperValue = value; }
        }

        private bool notifyPaperStateValue = false;
        public  bool NotifyPaperState {
            get { return notifyPaperStateValue; }
            set { notifyPaperStateValue = value; }
        }

	}
}
