using System;
using System.Configuration;
using System.Globalization;

namespace Neato.Cpt.Entity {
    public class Configuration {
        private Configuration() {}

        private const int DefaultCommandTimeout = 30;

        #region Command Timeouts
        public static int CommandTimeout {
            get {
                string commandTimeoutStr = ConfigurationSettings.AppSettings["CommandTimeoutSec"];
                return CommandTimeoutParse(commandTimeoutStr);
            }
        }

        private static int CommandTimeoutParse(string commandTimeoutStr) {
            if (commandTimeoutStr == null) {
                return DefaultCommandTimeout;
            }
            int timeoutValue;
            try {
                timeoutValue = int.Parse(commandTimeoutStr, NumberFormatInfo.InvariantInfo);
            } catch (ArgumentNullException) {
                return DefaultCommandTimeout;
            } catch (FormatException) {
                return DefaultCommandTimeout;
            } catch (OverflowException) {
                return DefaultCommandTimeout;
            }
            if (timeoutValue <= 0) {
                return DefaultCommandTimeout;
            } else {
                return timeoutValue;
            }
        }
        #endregion

        #region Mail
        private const string SmtpServerKey = "SmtpServer";
        private const string SmtpLoginKey = "SmtpLogin";
        private const string SmtpPasswordKey = "SmtpPassword";
        private const string MailFromKey = "MailFrom";

        public static string SmtpServer {
            get { return ConfigurationSettings.AppSettings[SmtpServerKey]; }
        }

        public static string SmtpLogin {
            get { return ConfigurationSettings.AppSettings[SmtpLoginKey]; }
        }

        public static string SmtpPassword {
            get { return ConfigurationSettings.AppSettings[SmtpPasswordKey]; }
        }

        public static string MailFrom {
            get { return ConfigurationSettings.AppSettings[MailFromKey]; }
        }
        
        #endregion

        #region PublicSite
        private const string PublicDesignerUrlKey = "PublicDesignerUrl";

        public static string PublicDesignerSiteUrl {
            get { return ConfigurationSettings.AppSettings[PublicDesignerUrlKey]; }
        }

        private const string IntranetDesignerUrlKey = "IntranetDesignerUrl";

        public static string IntranetDesignerSiteUrl {
            get { return ConfigurationSettings.AppSettings[IntranetDesignerUrlKey]; }
        }
        #endregion

        private const string AppNameKey = "AppName";

        public static string AppName {
            get { return ConfigurationSettings.AppSettings[AppNameKey]; }
        }

        private const string DesignProjectContentTypeKey = "DesignProjectContentType";

        public static string DesignProjectContentType {
            get { return ConfigurationSettings.AppSettings[DesignProjectContentTypeKey]; }
        }

        private const string DesignProjectFileTypeKey = "DesignProjectFileType";

        public static string DesignProjectFileType {
            get { return ConfigurationSettings.AppSettings[DesignProjectFileTypeKey]; }
        }

        private const string DesignProjectDefaultFileNameKey = "DesignProjectDefaultFileName";

        public static string DesignProjectDefaultFileName {
            get { return ConfigurationSettings.AppSettings[DesignProjectDefaultFileNameKey]; }
        }

        private const string MaxProjectSizeKey = "MaxProjectSize";

        // TODO: create parser
        public static int MaxProjectSize {
            get { return Convert.ToInt32(ConfigurationSettings.AppSettings[MaxProjectSizeKey]); }
        }


        private const string MaxLastEditedPaperRowsKey = "MaxLastEditedPaperRows";
        public static int MaxLastEditedPaperRows {
            get { return Convert.ToInt32(ConfigurationSettings.AppSettings[MaxLastEditedPaperRowsKey]); }
        }


        private const string MaxFaqSectionLengthKey = "MaxFaqSectionLength";
        public static int MaxFaqSectionLength {
            get { return Convert.ToInt32(ConfigurationSettings.AppSettings[MaxFaqSectionLengthKey]); }
        }


        private const string MaxFeedbackEmailLengthKey = "MaxFeedbackEmailLength";
        public static int MaxFeedbackEmailLength {
            get { return Convert.ToInt32(ConfigurationSettings.AppSettings[MaxFeedbackEmailLengthKey]); }
        }



        #region Image Library
        private const string LibraryImageCacheSecKey = "LibraryImageCacheSec";

        public static int LibraryImageCacheSec {
            get { return LibraryImageCacheSecParse(ConfigurationSettings.AppSettings[LibraryImageCacheSecKey]); }
        }

        private const int DefaultLibraryImageCache = 30;

        private static int LibraryImageCacheSecParse(string expiresStr) {
            if (expiresStr == null) {
                return DefaultLibraryImageCache;
            }
            int expiresValue;
            try {
                expiresValue = int.Parse(expiresStr, NumberFormatInfo.InvariantInfo);
            } catch (ArgumentNullException) {
                return DefaultLibraryImageCache;
            } catch (FormatException) {
                return DefaultLibraryImageCache;
            } catch (OverflowException) {
                return DefaultLibraryImageCache;
            }
            if (expiresValue <= 0) {
                return 0;
            } else {
                return expiresValue;
            }
        }

        private const string PdfHeapPathKey = "PdfHeapPath";
        public static string PdfHeapPath {
            get { return ConfigurationSettings.AppSettings[PdfHeapPathKey]; }
        }

        private const string SavePdfInHeapKey = "SavePdfInHeap";
        public static bool SavePdfInHeap {
            get {
                string val = ConfigurationSettings.AppSettings[SavePdfInHeapKey];
                bool result = false;
                try {
                    result = bool.Parse(val);
                } catch (ArgumentNullException) {
                    result = false;
                } catch (FormatException) {
                    result = false;
                }
                return result;
            }
        }
        #endregion

        #region Maintenance Closing
        private const string ServerNameKey = "ServerName";
        public static string ServerName {
            get { return ConfigurationSettings.AppSettings[ServerNameKey]; }
        }

        private const string VirtualFolderNameKey = "VirtualFolderName";
        public static string VirtualFolderName {
            get { return ConfigurationSettings.AppSettings[VirtualFolderNameKey]; }
        }

		private const string SiteNameKey = "SiteName";
		public static string SiteName {
			get { return ConfigurationSettings.AppSettings[SiteNameKey]; }
		}
		#endregion
    }
}