namespace Neato.Cpt.Entity {
    public sealed class RetailerConstants {
        private RetailerConstants() {
        }

        public static string RetailersFolder = "Retailers";
        public static string ClosePageFileName = "Close.html";
    }
}