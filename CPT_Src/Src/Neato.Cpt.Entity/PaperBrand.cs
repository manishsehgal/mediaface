using System;

namespace Neato.Cpt.Entity {
    [Serializable]
    public class PaperBrand : PaperBrandBase {
        private string nameValue = string.Empty;
        public const string NameField = "Name";

        public string Name {
            get { return nameValue; }
            set { nameValue = value; }
        }

        public PaperBrand() : base() {}

        public PaperBrand(int id, string name) : base(id) {
            this.nameValue = name;
        }
    }
}