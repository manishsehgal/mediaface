using System;

namespace Neato.Cpt.Entity.Tracking {
    public enum Feature {
        None = 0,
        ColorFill,
        AddImage,
        AddCustomImage,
        AddText,
        Paint,
        AddShape,
        Clear,
        UsePreviousDesign,
        SaveDesign,
        QuickTools,
        Preview,
        PrintPDF,
        CopyDesign
    }
}
