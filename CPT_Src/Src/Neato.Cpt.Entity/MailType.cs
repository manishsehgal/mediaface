namespace Neato.Cpt.Entity {
    public enum MailType {
        None = 0,
        ForgotPassword,
        TellAFriend,
        AddNewPhone,
        UserFeedback,
        PhoneRequestNotification
    }
}