using System;

namespace Neato.Cpt.Entity {
    [Serializable]
    public class LinkItem {
        private string textValue = string.Empty;
        private string urlValue = string.Empty;
        private bool isNewWindowValue = false;
        private bool isVisibleValue = false;
        private Align alignValue = Align.None;
        private int sortOrderValue = 0;
        private int linkIdValue = 0;
        private byte[] iconValue = new byte[0];

        public string Text {
            get { return textValue; }
            set { textValue = value; }
        }

        public string Url {
            get { return urlValue; }
            set { urlValue = value; }
        }

        public bool IsNewWindow {
            get { return isNewWindowValue; }
            set { isNewWindowValue = value; }
        }

        public bool IsVisible {
            get { return isVisibleValue; }
            set { isVisibleValue = value; }
        }

        public Align Align {
            get { return alignValue; }
            set { alignValue = value; }
        }

        public int SortOrder {
            get { return sortOrderValue; }
            set { sortOrderValue = value; }
        }

        public int LinkId {
            get { return linkIdValue; }
            set { linkIdValue = value; }
        }

        public byte[] Icon {
            get { return iconValue; }
            set { iconValue = value; }
        }

        public LinkItem() {}

        public LinkItem(string text, string url, bool isNewWindow, bool isVisible, Align align, int sortOrder, int linkId, byte[] icon) {
            this.textValue = text;
            this.urlValue = url;
            this.isNewWindowValue = isNewWindow;
            this.isVisibleValue = isVisible;
            this.alignValue = align;
            this.sortOrderValue = sortOrder;
            this.linkIdValue = linkId;
            this.iconValue = icon;
        }
    }
}