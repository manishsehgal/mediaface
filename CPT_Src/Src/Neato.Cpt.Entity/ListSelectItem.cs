using System;
namespace Neato.Cpt.Entity
{
    //====================================
    // used to fill ImageLibrary control
    [Serializable]
	public class  ListSelectItem {
		public ListSelectItem() {
			Text="1";
			ImageUrl=null;
			NavigateUrl=null;
			ExtInfoUrl=null;
		}
		public string Text;
		public string ImageUrl;
		public string NavigateUrl;
		public string ExtInfoUrl;
	}

    //====================================
    // used to fill ChoiceArea control
    public class ChoiceAreaItem {
        public class DropDownItem {
            public DropDownItem() {
                Text = "";
                Url = "";
            }
            public DropDownItem(string text, string url) {
                Text = text;
                Url = url;
            }
            public string Text;
            public string Url;
        }
        public ChoiceAreaItem() {
        }
        public DropDownItem[] DropDownItems; 
    }
}