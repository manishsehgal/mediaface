using System;
using System.Collections;
using System.Drawing;

namespace Neato.Cpt.Entity {
    [Serializable]
    public class Paper : PaperBase {
        private string nameValue = string.Empty;

        public string Name {
            get { return nameValue; }
            set { nameValue = value; }
        }

        private SizeF sizeValue = new SizeF(0, 0);

        public SizeF Size {
            get { return sizeValue; }
            set { sizeValue = value; }
        }

        private PaperBrand brandValue = null;

        public PaperBrand Brand {
            get { return brandValue; }
            set { brandValue = value; }
        }

        private PaperType paperTypeValue = PaperType.DieCut;

        public PaperType PaperType {
            get { return paperTypeValue; }
            set { paperTypeValue = value; }
        }

        private string paperStateValue = "t";

        public string PaperState {
            get { return paperStateValue; }
            set { paperStateValue = value; }
        }


        public Paper() : base() {}

        public Paper(int id, string name, PaperBrand brand, float width, float height, PaperType paperType, string state) : base(id) {
            this.nameValue = name;
            this.sizeValue.Width = width;
            this.sizeValue.Height = height;
            this.brandValue = brand;
            this.paperTypeValue = paperType;
            this.paperStateValue = state;
        }

        #region Faces
        private ArrayList facesValue = new ArrayList();

        public virtual void AddFace(Face face, float x, float y, float rotation) {
            face.Position = new PointF(x, y);
            face.Rotation = rotation;
            facesValue.Add(face);
        }

        public virtual Face[] Faces {
            get { return (Face[])this.facesValue.ToArray(typeof(Face)); }
        }

        public virtual string[] GetFaceNames(string culture) {
            ArrayList names = new ArrayList(this.facesValue.Count);
            foreach (Face face in this.facesValue) {
                names.Add(face.GetName(culture));
            }
            return (string[])names.ToArray(typeof(string));
        }
        #endregion
    }
}