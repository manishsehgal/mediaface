using System;
using System.Collections;
using System.Globalization;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace Neato.Cpt.Entity {
    [Serializable]
    public class Project : ISerializable {
        private const int LastVersion = 1;
        private int version = 1;
        
        public Project() {}

        public Project(Paper paper, DeviceType deviceType) : this() {
            this.projectXmlValue = new XmlDocument();
            XmlElement projectNode = this.projectXmlValue.CreateElement("Project");
            projectNode.SetAttribute("appName", Configuration.AppName);
            projectNode.SetAttribute("deviceType", deviceType.ToString());
            this.projectXmlValue.AppendChild(projectNode);

            XmlElement facesNode = this.projectXmlValue.CreateElement("Faces");
            projectNode.AppendChild(facesNode);

            foreach (Face face in paper.Faces) {
                XmlElement faceNode = this.projectXmlValue.CreateElement("Face");
                faceNode.SetAttribute("id", Guid.NewGuid().ToString("N"));
                faceNode.SetAttribute("dbId", face.Id.ToString(CultureInfo.InvariantCulture));
                faceNode.SetAttribute("x", face.Position.X.ToString(CultureInfo.InvariantCulture));
                faceNode.SetAttribute("y", face.Position.Y.ToString(CultureInfo.InvariantCulture));
                faceNode.SetAttribute("rotation", face.Rotation.ToString(CultureInfo.InvariantCulture));
                facesNode.AppendChild(faceNode);

                XmlElement contourNode = this.projectXmlValue.CreateElement("Contour");
                contourNode.InnerXml = face.Contour.InnerXml;
                contourNode.SetAttribute("x", 0.ToString(CultureInfo.InvariantCulture));
                contourNode.SetAttribute("y", 0.ToString(CultureInfo.InvariantCulture));
                faceNode.AppendChild(contourNode);

                XmlElement localizationNode = this.projectXmlValue.CreateElement("Localization");
                faceNode.AppendChild(localizationNode);

                XmlElement captionNode = this.projectXmlValue.CreateElement("Caption");
                localizationNode.AppendChild(captionNode);

                IEnumerator captions = face.GetNames();
                captions.Reset();
                while (captions.MoveNext()) {
                    DictionaryEntry caption = (DictionaryEntry)captions.Current;
                    XmlElement cultureNode = this.projectXmlValue.CreateElement("Culture");
                    cultureNode.SetAttribute("key", caption.Key.ToString());
                    cultureNode.SetAttribute("value", caption.Value.ToString());
                    captionNode.AppendChild(cultureNode);
                }
            }

            XmlElement paperNode = this.projectXmlValue.CreateElement("Paper");
            paperNode.SetAttribute("w", paper.Size.Width.ToString(CultureInfo.InvariantCulture));
            paperNode.SetAttribute("h", paper.Size.Height.ToString(CultureInfo.InvariantCulture));
            projectNode.AppendChild(paperNode);

            XmlElement propertiesNode = this.projectXmlValue.CreateElement("Properties");
            propertiesNode.SetAttribute("name", paper.Name);
            projectNode.AppendChild(propertiesNode);

            this.projectPaperValue = new PaperBase(paper.Id);
            paperTypeValue = paper.PaperType;
        }

        #region ISerializable
        protected Project(SerializationInfo info, StreamingContext context) {
            // do not remove try - catch
            try {
                this.version = (int)info.GetValue("version", typeof(int));
            } catch (SerializationException) {
                this.version = 0;
            }

            this.projectImagesValue = (Hashtable)info.GetValue("projectImagesValue", typeof(Hashtable));

            this.projectXmlValue = new XmlDocument();
            this.projectXmlValue.InnerXml = info.GetString("projectXmlValueInnerXml");

            // do not remove try - catch
            try {
                this.projectPaperValue = (PaperBase)info.GetValue("projectPaperValue", typeof(PaperBase));
            } catch (SerializationException) {
                this.projectPaperValue = null;
            }

            try {
                this.paperTypeValue = (PaperType)info.GetValue("paperTypeValue", typeof(PaperType));
            } catch (SerializationException) {
                this.paperTypeValue = PaperType.DieCut;
            }

            this.version = LastVersion;
        }

        void ISerializable.GetObjectData(
            SerializationInfo info, StreamingContext context) {
            info.AddValue("version", this.version);
            info.AddValue("projectXmlValueInnerXml", this.projectXmlValue.InnerXml);
            info.AddValue("projectImagesValue", this.projectImagesValue);
            info.AddValue("projectPaperValue", this.projectPaperValue);
            info.AddValue("paperTypeValue", this.paperTypeValue);
        }
        #endregion

        private Hashtable projectImagesValue = new Hashtable();

        private XmlDocument projectXmlValue = null;

        public XmlDocument ProjectXml {
            get { return projectXmlValue; }
            set { PutProjectXml(value); }
        }

        private void PutProjectXml(XmlDocument project) {
            if (project.FirstChild.NodeType != XmlNodeType.XmlDeclaration) {
                XmlDeclaration declaration = project.CreateXmlDeclaration("1.0", Encoding.UTF8.WebName, null);
                XmlElement root = project.DocumentElement;
                project.InsertBefore(declaration, root);
            }
            projectXmlValue = project;
            SynchronizeImageContent();
        }

        private void SynchronizeImageContent() {
            ArrayList deleteImageIds = new ArrayList();
            foreach (Guid imageId in projectImagesValue.Keys) {
                string id = imageId.ToString("N");
                if (projectXmlValue.InnerXml.IndexOf(id) == -1) {
                    deleteImageIds.Add(imageId);
                }
            }

            foreach (Guid imageId in deleteImageIds) {
                projectImagesValue.Remove(imageId);
            }
        }

        private PaperBase projectPaperValue = null;

        public PaperBase ProjectPaper {
            get { return projectPaperValue; }
            set { projectPaperValue = value; }
        }

        private PaperType paperTypeValue = PaperType.DieCut;

        public PaperType PaperType {
            get { return paperTypeValue; }
            set { paperTypeValue = value; }
        }

        public int Size {
            get {
                int projectXmlSize = ProjectXml.OuterXml.Length;
                int projectImagesSize = 0;
                foreach (byte[] projectImage in projectImagesValue.Values) {
                    projectImagesSize += projectImage.Length;
                }

                return projectXmlSize + projectImagesSize;
            }
        }

        public void AddImage(byte[] fileData, int width, int height) {
            Guid id = Guid.NewGuid();
            projectImagesValue.Add(id, fileData);

            XmlNodeList faceList = projectXmlValue.DocumentElement.SelectNodes("//Faces/Face");
            XmlElement imageNode = projectXmlValue.CreateElement("Image");

            Random random = new Random();

            const float WorkspaceWidth = 410f;
            const float WorkspaceHeight = 340f;

            float scale = CalculateImageScale(WorkspaceWidth, width, WorkspaceHeight, height);
            float rotation = 0f;
            bool isCurrentUnit = true;
            float x = random.Next(10);
            float y = random.Next(10);

            imageNode.SetAttribute("x", x.ToString(CultureInfo.InvariantCulture));
            imageNode.SetAttribute("y", y.ToString(CultureInfo.InvariantCulture));
            imageNode.SetAttribute("scaleX", scale.ToString(CultureInfo.InvariantCulture));
            imageNode.SetAttribute("scaleY", scale.ToString(CultureInfo.InvariantCulture));
            imageNode.SetAttribute("initScale", scale.ToString(CultureInfo.InvariantCulture));
            imageNode.SetAttribute("id", id.ToString("N"));
            imageNode.SetAttribute("isCurrentUnit", isCurrentUnit.ToString(CultureInfo.InvariantCulture));
            imageNode.SetAttribute("rotation", rotation.ToString(CultureInfo.InvariantCulture));
            imageNode.SetAttribute("isNewImage", bool.TrueString);

            XmlNode propertiesNode = projectXmlValue.DocumentElement.SelectSingleNode("Properties");

            string currentFaceId = propertiesNode.Attributes["currentFaceId"].Value;
            XmlNode currentFace = faceList[0];

            for (int i = 0; i < faceList.Count; i++) {
                if (faceList[i].Attributes["id"].Value == currentFaceId) {
                    currentFace = faceList[i];
                    break;
                }
            }

            currentFace.AppendChild(imageNode);
        }

        public static float CalculateImageScale(float workspaceWidth, int width, float workspaceHeight, int height) {
            float scaleWidth = workspaceWidth / width;
            float scaleHeight = workspaceHeight / height;
            int scaleFactor = 3;
            float scale = ((scaleWidth < scaleHeight) ? scaleWidth : scaleHeight) * 100 / scaleFactor;
            return (scale > 100) ? 100 : scale;
        }

        public byte[] GetImage(Guid id) {
            byte[] imageArray = (byte[])projectImagesValue[id];
            if (imageArray == null) {
                imageArray = new byte[0];
            }
            return imageArray;
        }

    }
}