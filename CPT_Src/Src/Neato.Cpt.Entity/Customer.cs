using System;

namespace Neato.Cpt.Entity {
    [Serializable]
    public class Customer {
        public const string FieldId = "CustomerId";

        private int customerIdValue = 0;
        private string firstNameValue = string.Empty;
        private string lastNameValue = string.Empty;
        private string emailValue = string.Empty;
        private string passwordValue = string.Empty;
        private string passwordUidValue = string.Empty;
        private EmailOptions emailOptionsValue = EmailOptions.Text;
        private bool receiveInfoValue = false;
        private PaperBase[] lastEditedPapersValue;
        private DateTime createdValue;
        private int retailerIdValue;

        public int CustomerId {
            get { return customerIdValue; }
            set { customerIdValue = value; }
        }

        public string FirstName {
            get { return firstNameValue; }
            set { firstNameValue = value; }
        }

        public string LastName {
            get { return lastNameValue; }
            set { lastNameValue = value; }
        }

        public string FullName {
            get { return string.Format("{0} {1}", firstNameValue, lastNameValue); }
        }

        public string Email {
            get { return emailValue; }
            set { emailValue = value; }
        }

        public string Password {
            get { return passwordValue; }
            set { passwordValue = value; }
        }

        public string PasswordUid {
            get { return passwordUidValue; }
            set { passwordUidValue = Convert.ToString(value); }
        }

        public EmailOptions EmailOptions {
            get { return emailOptionsValue; }
            set { emailOptionsValue = value; }
        }

        public bool ReceiveInfo {
            get { return receiveInfoValue; }
            set { receiveInfoValue = value; }
        }

        public PaperBase LastEditedPaper {
            get {
                return (LastEditedPapers != null && LastEditedPapers.Length > 0)
                    ? LastEditedPapers[0] : null;
            }
        }

        public PaperBase[] LastEditedPapers {
            get { return lastEditedPapersValue; }
            set { lastEditedPapersValue = value; }
        }

        public DateTime Created {
            get { return createdValue; }
            set { createdValue = value; }
        }

        public int RetailerId {
            get { return retailerIdValue; }
            set { retailerIdValue = value; }
        }

        public Customer(){}

        public Customer(int customerId, string firstName, string lastName, string email, string password,
                        EmailOptions emailOptions, bool receiveInfo) {
            this.customerIdValue = customerId;
            this.firstNameValue = firstName;
            this.lastNameValue = lastName;
            this.emailValue = email;
            this.passwordValue = password;
            this.emailOptionsValue = emailOptions;
            this.receiveInfoValue = receiveInfo;
        }

        public bool IsNew {
            get { return customerIdValue == 0; }
        }

        #region Equals, GetHashCode, ==, !=
        public override bool Equals(object obj) {
            Customer other = obj as Customer;
            if (other == null) {
                return false;
            } else {
                return this.customerIdValue == other.customerIdValue;
            }
        }

        public override int GetHashCode() {
            return customerIdValue.GetHashCode();
        }

        public static bool operator ==(Customer first, Customer second) {
            if (object.Equals(first, null)) {
                return object.Equals(second, null);
            }
            return first.Equals(second);
        }

        public static bool operator !=(Customer first, Customer second) {
            if (object.Equals(first, null)) {
                return !object.Equals(second, null);
            }
            return !first.Equals(second);
        }
        #endregion

    }
}