namespace Neato.Cpt.Entity {
    public enum Align {
        None = 0,
        Left,
        Right
    }
}