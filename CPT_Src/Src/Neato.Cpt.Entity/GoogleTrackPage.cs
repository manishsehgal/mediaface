namespace Neato.Cpt.Entity {
    public class GoogleTrackPage {
        private string pageUrlValue = string.Empty;
        private bool enableScriptValue = false;

        public string PageUrl {
            get { return pageUrlValue; }
            set { pageUrlValue = value; }
        }

        public bool EnableScript {
            get { return enableScriptValue; }
            set { enableScriptValue = value; }
        }

        public GoogleTrackPage() {}

        public GoogleTrackPage(string pageUrl, bool enableScript) {
            this.pageUrlValue = pageUrl;
            this.enableScriptValue = enableScript;
        }
    }
}