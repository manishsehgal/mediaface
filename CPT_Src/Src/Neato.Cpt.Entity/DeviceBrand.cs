using System;
using System.Collections;

namespace Neato.Cpt.Entity {
    [Serializable]
    public class DeviceBrand : DeviceBrandBase {
        public class DeviceBrandNameCompare : IComparer  {
            int IComparer.Compare( Object x, Object y )  {
                DeviceBrand devX = x as DeviceBrand;
                DeviceBrand devY = y as DeviceBrand;
                return string.Compare(devX.Name, devY.Name, true);
            }
        }

        private string nameValue = string.Empty;

        public const string NameField = "Name";
        public string Name {
            get { return nameValue; }
            set { nameValue = value; }
        }

		public DeviceBrand() : base() {}

        public DeviceBrand(int id, string name) : base(id) {
            this.nameValue = name;
        }
    }
}