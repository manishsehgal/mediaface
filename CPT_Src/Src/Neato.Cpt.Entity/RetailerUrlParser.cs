using System.Text.RegularExpressions;

namespace Neato.Cpt.Entity.Helpers {
    public class RetailerUrlParser {
        private const string RetailerUrlTemplate = @"^((?<Protocol>\w+)://(((?<Server>[^/:]+))/?)[:]?((?<Port>\d*)/?)((?<VirtualDirectory>[^/\.]+)/?)?)(((?<Retailer>[^/\.]+))(?<Path>(/[^/\.]+)*))?(/(?<Page>[\w-]+\.\w+)?)?(\?(?<Query>.*))?$";

        private const string Error404Template = @"^((?<Error>404);)(?<ErrorUrl>.*)$";

        private string urlValue;
        private Match matchValue;

        public string Url {
            get { return urlValue; }
            set { urlValue = value; }
        }

        public RetailerUrlParser(string url) {
            this.urlValue = url;
            Regex regex = new Regex(RetailerUrlTemplate, RegexOptions.IgnoreCase);
            matchValue = regex.Match(Url);
			if (IsMatch == false) {
				System.Exception ex = new System.Exception("Url Matching Error. Url=" + this.urlValue);
				throw ex;
			}

        }

        public bool IsMatch {
            get { return matchValue.Success; }
        }

        public bool IsError404 {
            get {
                Regex regex = new Regex(Error404Template, RegexOptions.IgnoreCase);
                return regex.IsMatch(Query);
            }
        }

        public string ErrorRedirectUrl {
            get {
                Regex regex = new Regex(Error404Template, RegexOptions.IgnoreCase);
                return regex.Match(Query).Result("${ErrorUrl}");
            }
        }

        public bool NoRetailer {
            get { return matchValue.Result("${Retailer}") == string.Empty; }
        }

        public bool HasPath {
            get { return matchValue.Result("${Path}") != string.Empty; }
        }

        public string Protocol {
            get { return matchValue.Result("${Protocol}"); }
        }

        public string Server {
            get { return matchValue.Result("${Server}"); }
        }

        public string Port {
            get { return matchValue.Result("${Port}"); }
        }

        public string VirtualDirectory {
            get { return matchValue.Result("${VirtualDirectory}"); }
        }

        public string Retailer {
            get { return matchValue.Result("${Retailer}"); }
        }

        public string Page {
            get { return matchValue.Result("${Page}"); }
        }

        public string Query {
            get { return matchValue.Result("${Query}"); }
        }

    }
}