using System;

namespace Neato.Cpt.Entity {
    [Serializable]
    public class ExitPage {
        private int retailerIdValue = 0;
        private string htmlTextValue = string.Empty;
        private bool enabledValue = false;

        public int RetailerId {
            set { retailerIdValue = value; }
            get { return retailerIdValue; }
        }

        public string HtmlText {
            get { return htmlTextValue; }
            set { htmlTextValue = value; }
        }

        public bool Enabled {
            set { enabledValue = value; }
            get { return enabledValue; }
        }

        public ExitPage() {}

        public ExitPage(int retailerId, string htmlText, bool enabled) {
            retailerIdValue = retailerId;
            htmlTextValue = htmlText;
            enabledValue = enabled;
        }
    }
}