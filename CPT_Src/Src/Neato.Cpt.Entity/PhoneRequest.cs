using System;

namespace Neato.Cpt.Entity {
    [Serializable]
    public class DeviceRequest {
        private int requestDeviceIdValue;
        private string firstNameValue;
        private string lastNameValue;
        private string emailAddressValue;
        private string carrierValue;
        private string deviceBrandValue;
        private string deviceValue;
        private string commentValue;
        private DateTime createdValue;
        private int retailerIdValue;

        public DeviceRequest() {}
        public DeviceRequest(string phoneManufacturer, string phoneModel, DateTime created) {
            FirstName = LastName = 
                EmailAddress = Carrier = 
                Comment = "";
            
            DeviceBrand = phoneManufacturer;
            Device = phoneModel;
            Created = created;
        }

        public DeviceRequest(string phoneManufacturer, string phoneModel, DateTime created, string emailAddress) {
            FirstName = LastName = 
                Carrier = Comment = "";
            
            DeviceBrand = phoneManufacturer;
            Device = phoneModel;
            Created = created;
            EmailAddress = emailAddress;
        }


        public int RequestDeviceId {
            get { return requestDeviceIdValue;}
            set { requestDeviceIdValue = value;}
        }

        public int RetailerId {
            get { return retailerIdValue;}
            set { retailerIdValue = value;}
        }

        public string FirstName {
            get { return firstNameValue;}
            set { firstNameValue = value;}
        }

        public string LastName {
            get { return lastNameValue;}
            set { lastNameValue = value;}
        }

        public string EmailAddress {
            get { return emailAddressValue;}
            set { emailAddressValue = value;}
        }

        public string Carrier {
            get { return carrierValue; }
            set { carrierValue = value; }
        }

        public string DeviceBrand {
            get { return deviceBrandValue;}
            set { deviceBrandValue = value;}
        }

        public string Device {
            get { return deviceValue;}
            set { deviceValue = value;}
        }

        public string Comment {
            get { return commentValue;}
            set { commentValue = value;}
        }

        public DateTime Created {
            get { return createdValue; }
            set { createdValue = value; }
        }
    }
}