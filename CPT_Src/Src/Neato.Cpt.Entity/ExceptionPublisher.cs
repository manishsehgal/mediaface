using System;
using System.Diagnostics;

namespace Neato.Cpt.Entity {
    public class ExceptionPublisher {
        private ExceptionPublisher() {}

        public static void Publish(Exception ex) {
            string message = string.Format("Message: {0} \nStackTrace: {1}", ex.Message, ex.StackTrace);
            EventLog.WriteEntry("CPT", message, EventLogEntryType.Error);
        }
    }
}