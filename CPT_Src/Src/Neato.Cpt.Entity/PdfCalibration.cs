using System;

namespace Neato.Cpt.Entity {
    [Serializable]
    public class PdfCalibration {
        private Single x;
        private Single y;
        private bool isSpecified;

        public Single X {
            get { return x; }
            set { x = value; isSpecified = true; }
        }

        public Single Y {
            get { return y; }
            set { y = value; isSpecified = true; }
        }

        public bool IsSpecified {
            get { return isSpecified; }
            set { isSpecified = value; }
        }

        public PdfCalibration() {
            this.x = 0;
            this.y = 0;
            this.isSpecified = false;
        }
    }
}