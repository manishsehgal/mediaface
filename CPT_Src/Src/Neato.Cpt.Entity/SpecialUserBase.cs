using System;

namespace Neato.Cpt.Entity {
    [Serializable]
	public class SpecialUserBase {

        public SpecialUserBase() {}

        public SpecialUserBase(int id) {
            this.idValue = id;
        }

        private int idValue = 0;
        public const string IdField = "Id";
        public  int Id {
            get { return idValue; }
            set { idValue = value; }
        }

        public bool IsNew {
            get { return idValue <= 0; }
        }

        #region Equals, GetHashCode, ==, !=
        public override bool Equals(object obj) {
            SpecialUserBase other = obj as SpecialUserBase;
            if (other == null) {
                return false;
            } else {
                return this.Id == other.Id;
            }
        }

        public override int GetHashCode() {
            return idValue.GetHashCode();
        }

        public static bool operator ==(SpecialUserBase first, SpecialUserBase second) {
            if (object.Equals(first, null)) {
                return object.Equals(second, null);
            }
            return first.Equals(second);
        }

        public static bool operator !=(SpecialUserBase first, SpecialUserBase second) {
            if (object.Equals(first, null)) {
                return !object.Equals(second, null);
            }
            return !first.Equals(second);
        }
        #endregion
    }
}
