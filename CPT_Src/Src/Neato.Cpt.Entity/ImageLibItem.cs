using System;

namespace Neato.Cpt.Entity {
    [Serializable]
    public class ImageLibItem {
        private int idValue;

        public int Id {
            get { return idValue; }
            set { idValue = value; }
        }

        public ImageLibItem(int id) {
            this.idValue = id;
        }
    }
}