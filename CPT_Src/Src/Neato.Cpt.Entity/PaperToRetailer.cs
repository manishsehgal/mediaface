using System;

namespace Neato.Cpt.Entity
{
	public class PaperToRetailer
	{
        private int paperIdValue;
        private int retailerIdValue;
        public int PaperId {
            get { return paperIdValue; }
            set { paperIdValue = value; }
        }
        public int RetailerId {
            get { return retailerIdValue; }
            set { retailerIdValue = value; }
        }
        public PaperToRetailer(){}
        public PaperToRetailer(PaperBase paper, Retailer retailer) {
            PaperId =  paper.Id;
            RetailerId = retailer.Id;
        }
        public PaperToRetailer(int paperId, int retailerId) {
            PaperId =  paperId;
            RetailerId = retailerId;
        }
        
	}
}
