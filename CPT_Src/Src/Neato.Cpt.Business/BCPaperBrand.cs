using System.Data;
using System.IO;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.Business {
    public sealed class BCPaperBrand {
        private BCPaperBrand() {}

        public static PaperBrand[] GetPaperBrandList() {
            return DOPaperBrand.EnumeratePaperBrands();
        }

        public static PaperBrand[] GetPaperBrandList(int idDeviceType) {
            return DOPaperBrand.EnumeratePaperBrandsByDeviceType(idDeviceType);
        }

        public static byte[] GetPaperBrandIcon(PaperBrandBase paperBrandBase) {
            return DOPaperBrand.GetPaperBrandIconByID(paperBrandBase);
        }

        public static PaperBrand NewPaperBrand() {
            PaperBrand brand = new PaperBrand();
            return brand;
        }

        public static void Save(PaperBrand paperBrand) {
            Save(paperBrand, null);
        }

        public static void Save(PaperBrand paperBrand, Stream icon) {
            DOGlobal.BeginTransaction();
            try {
                if (paperBrand.IsNew) {
                    DOPaperBrand.Add(paperBrand);
                } else {
                    DOPaperBrand.Update(paperBrand);
                }
                ChangeIcon(paperBrand, icon);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void ChangeIcon(PaperBrandBase paperBrand, Stream icon) {
            if (icon != null && icon.Length > 0) {
                DOGlobal.BeginTransaction();
                try {
                    DOPaperBrand.UpdateIcon(paperBrand, ConvertHelper.StreamToByteArray(icon));
                    DOGlobal.CommitTransaction();
                } catch (DBConcurrencyException ex) {
                    DOGlobal.RollbackTransaction();
                    throw new ConcurrencyException(ex.Message, ex);
                } catch {
                    DOGlobal.RollbackTransaction();
                    throw;
                }
            }
        }

        public static void Delete(PaperBrandBase paperBrand) {
            if (paperBrand.IsNew) return;
            DOGlobal.BeginTransaction();
            try {
                BCPaper.Delete(paperBrand);
                DOPaperBrand.Delete(paperBrand);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
    }
}