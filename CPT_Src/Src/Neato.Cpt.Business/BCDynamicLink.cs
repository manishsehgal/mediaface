using System;
using System.Collections;
using System.Data;

using Neato.Cpt.Data;
using Neato.Cpt.Data.Scheme;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;

namespace Neato.Cpt.Business {
    public class BCDynamicLink {
        public const string DefaultMenuPage = "Default Menu Page";
        public const string DefaultCulture = "";
        public const int DefaultPageId = 1;
        public const int NotFoundPageId = -1;
        private BCDynamicLink() {}

        public static LinkItem[] GetLinks(int retailerId, string pageUrl, Place place, Align align, string Culture) {

            LinkItem[] links = DODynamicLink.GetLinks(retailerId, pageUrl, place, align, Culture);
            if (links.Length == 0) {
                int count = 0;
                count += DODynamicLink.GetLinks(retailerId, pageUrl, place, Align.Left, Culture).Length;
                count += DODynamicLink.GetLinks(retailerId, pageUrl, place, Align.Right, Culture).Length;
                if (count == 0)
                    links = DODynamicLink.GetLinks(retailerId, DefaultMenuPage, place, align, Culture);
            }
            
            ArrayList linkList = new ArrayList();
            foreach(LinkItem item in links)
                if (item.IsVisible)
                    linkList.Add(item);

            return (LinkItem[])linkList.ToArray(typeof(LinkItem));;
        }

        public static LinkItem[] GetLinks(int pageId, Place place, string Culture) {
            return DODynamicLink.GetLinks(pageId, place, Culture);
        }

        public static byte[] GetIcon(int linkId) {
            return DODynamicLink.GetIcon(linkId);
        }

        public static int GetPageIdFromPageUrl(int retailerId, string pageUrl) {
            PageData page = DODynamicLink.PageEnumByUrl(retailerId, pageUrl);
            foreach(PageData.PageRow row in page.Page.Rows) {
                return row.PageId;
            }

            return NotFoundPageId;
        }

        public static int AddPage(int retailerId, string pageUrl) {
            int pageId = GetPageIdFromPageUrl(retailerId, pageUrl);
            if (pageId != NotFoundPageId)
                return pageId;

            DOGlobal.BeginTransaction();
            try {
                pageId = DODynamicLink.AddPage(retailerId, pageUrl, pageUrl);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                return NotFoundPageId;
            }

            return pageId;
        }

        public static void DynamicLinkDelByPageId(int pageId) {
            DOGlobal.BeginTransaction();
            try {
                DODynamicLink.DynamicLinkDelByPageId(pageId);
                DOGlobal.CommitTransaction();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void AddDynamicLink(int retailerId, int pageId, Place place, string culture, LinkItem item) {
            DOGlobal.BeginTransaction();
            try {
                DODynamicLink.AddDynamicLink(retailerId, pageId, place, culture, item);
                DOGlobal.CommitTransaction();
            } catch(Exception ex) {
                DOGlobal.RollbackTransaction();
                throw ex;
            }
        }

        public static void UpdateDynamicLink(int retailerId, int pageId, Place place, string culture, LinkItem item){
            DOGlobal.BeginTransaction();
            try {
                DODynamicLink.UpdateDynamicLink(retailerId, pageId, place, culture, item);
                DOGlobal.CommitTransaction();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void DeleteDynamicLink(int pageId, int linkId, Place place) {
            DOGlobal.BeginTransaction();
            try {
                DODynamicLink.DeleteDynamicLink(pageId, linkId, place);
                DOGlobal.CommitTransaction();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void ResetDynamicLinks() {
            DODynamicLink.ResetDynamicLinks();
        }
    }
}