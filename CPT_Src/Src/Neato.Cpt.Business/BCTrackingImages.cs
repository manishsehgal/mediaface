using System;
using System.Data;
using System.Threading;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Business {
    public class BCTrackingImages {
        public BCTrackingImages() {}

        public static void Add(ImageLibItem image, string userHostAddress, Retailer retailer) {
            string login = Thread.CurrentPrincipal.Identity.Name;
            Add(image, login, userHostAddress, retailer);
        }

        public static void Add(ImageLibItem image, string login, string userHostAddress, Retailer retailer) {
            IPAddress userIP = IPAddress.ToIPAddress(userHostAddress);
            if (!BCIPFilter.IsUserContainsInIPFilters(userIP))
                DOTrackingImages.Add(image, login, BCTimeManager.GetCurrentTime(), retailer);
        }

        public static DataSet GetImagesListByDate(DateTime startTime, DateTime endTime, Retailer retailer) {
            return DOTrackingImages.GetImagesListByDate(startTime, endTime, retailer);
        }

    }
}