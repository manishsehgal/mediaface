using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.DataBase;

namespace Neato.Cpt.Business {
	public class BCFaq {
		public BCFaq() {}

		public static FaqData Enum(Retailer retailer) {
			return DOFaq.Enum(retailer);
		}

        public static void Update(FaqData data) {
            DOGlobal.BeginTransaction();
            try {
                DOFaq.Update(data);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
	}
}