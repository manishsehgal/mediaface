using System.Data;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;

namespace Neato.Cpt.Business
{
	public sealed class BCExitPage
	{
		private BCExitPage()
		{
		}

        public static ExitPage Get(int retailerId) {
            return DOExitPage.Get(retailerId);
        }

        public static void Update(ExitPage exitPage) {
            DOGlobal.BeginTransaction();
            try {
                ExitPage page = Get(exitPage.RetailerId);
                if (page != null)
                    DOExitPage.Update(exitPage);
                else
                    DOExitPage.Insert(exitPage);
                DOGlobal.CommitTransaction();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
	}
}
