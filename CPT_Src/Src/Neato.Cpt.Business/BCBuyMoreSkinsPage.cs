using System.Data;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;

namespace Neato.Cpt.Business {
    public sealed class BCBuyMoreSkinsPage {
        private BCBuyMoreSkinsPage() {}

        public static LocalizedText Get(string culture, Retailer retailer) {
            LocalizedText[] list = DOBuyMoreSkinsPage.Enum(culture, retailer);
            return BCLocalizedText.GetTextFromList(list, culture);
        }

        public static void Update(LocalizedText localizedText, Retailer retailer) {
            DOGlobal.BeginTransaction();
            try {
                LocalizedText text = Get(localizedText.Culture, retailer);
                if (text != null)
                    DOBuyMoreSkinsPage.Update(localizedText.Culture, localizedText.Text, retailer);
                else
                    DOBuyMoreSkinsPage.Insert(localizedText.Culture, localizedText.Text, retailer);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
    }
}