using Neato.Cpt.Entity;

namespace Neato.Cpt.Business {
    public sealed class BCLocalizedText {
        private BCLocalizedText() {}

        private static LocalizedText FindLocalizedText(LocalizedText[] list, string culture) {
            foreach (LocalizedText item in list) {
                if (item.Culture == culture)
                    return item;
            }
            return null;
        }

        public static LocalizedText GetTextFromList(LocalizedText[] list, string culture) {
            LocalizedText item = null;
            //long name
            item = FindLocalizedText(list, culture);
            if (item != null)
                return item;
            //short name
            if (culture.Length > 2) {
                culture = culture.Substring(0, 2);
                item = FindLocalizedText(list, culture);
                if (item != null)
                    return item;
            }
            //default name name
            item = FindLocalizedText(list, string.Empty);

            return item;
        }
    }
}