using System.Data;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;

namespace Neato.Cpt.Business {
    public sealed class BCPlaceProperty {
        private static PlaceProperty[] props = null;
        private BCPlaceProperty() {}

        public static PlaceProperty Get(int retailerId, Place place) {
            if (props == null)
                props = DOPlaceProperty.Enum();

            foreach(PlaceProperty prop in props) {
                if (prop.RetailerId == retailerId && prop.Place == place.ToString())
                    return prop;
            }

            return null;
        }

        public static void Delete(int retailerId, Place place) {
            DOGlobal.BeginTransaction();
            try {
                DOPlaceProperty.Delete(retailerId, place);
                DOGlobal.CommitTransaction();
                ResetPlaceProperty();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Update(int retailerId, Place place, PlaceProperty placeProperty) {
            bool hasProp = Get(retailerId, place) != null;
            DOGlobal.BeginTransaction();
            try {
                if (hasProp)
                    DOPlaceProperty.Update(retailerId, place, placeProperty);
                else
                    DOPlaceProperty.Insert(retailerId, place, placeProperty);
                DOGlobal.CommitTransaction();
                ResetPlaceProperty();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void ResetPlaceProperty() {
            props = null;
        }
    }
}