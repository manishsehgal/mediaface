using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.Business {
    public sealed class BCProject {
        private BCProject() {}

        private static string[] ImageFormats = {".jpg", ".jpeg", ".emf", ".exif", ".gif", ".ico", ".png", ".tiff", ".tif", ".wmf", ".bmp"};

        public static Stream Export(Project project) {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            formatter.Serialize(stream, project);
            stream = CryptoHelper.Encrypt(stream);
            return stream;
        }

        public static Project Import(Customer customer, Stream data) {
            IFormatter formatter = new BinaryFormatter();
            Project project = null;
            try {
                project = (Project)formatter.Deserialize(CryptoHelper.Decrypt(data));
            } catch (CryptographicException ex) {
                throw new BusinessException(BCProjectStrings.FileStructureCorruptedErrorMessage(), ex);
            } catch (SerializationException ex) {
                throw new BusinessException(BCProjectStrings.FileStructureCorruptedErrorMessage(), ex);
            } catch (TargetInvocationException ex) {
                if (ex.InnerException is SerializationException)
                    throw new BusinessException(BCProjectStrings.FileStructureCorruptedErrorMessage(), ex);
                else
                    throw;
            }

            BCCustomer.ChangeLastEditedPaper(customer, project.ProjectPaper);
            return project;
        }

        public static Project GetProject(Customer customer, PaperBase paperBase, DeviceType deviceType) {
            BCCustomer.ChangeLastEditedPaper(customer, paperBase);
            Paper paper = DOPaper.GetPaper(paperBase);
            return new Project(paper, deviceType);
        }

        public static bool AddImage(Project project, Stream imageData) {
            Image image = Image.FromStream(imageData);
            imageData.Position = 0;
            int width = image.Width;
            int height = image.Height;
            image.Dispose();

            byte[] data = ConvertHelper.StreamToByteArray(imageData);
            byte[] jpegData = ImageHelper.ConvertImageToJpeg(data);
            if (IsImageAdditionAllowed(project, jpegData.Length)) {
                project.AddImage(jpegData, width, height);
                return true;
            } else {
                return false;
            }
        }

        public static bool AllowedImageExtention(string extention) {
            for (int i = 0; i < ImageFormats.Length; i++) {
                if (ImageFormats[i] == extention) {
                    return true;
                }
            }
            return false;
        }

        public static bool AddImageFromLibrary(Project project, int id) {
            byte[] imageArray = BCImageLibrary.GetImage(id);
            using (MemoryStream stream = new MemoryStream(imageArray)) {
                return AddImage(project, stream);
            }
        }

        public static bool IsImageAdditionAllowed(Project project, int imageSize) {
            int maxImageSize = Configuration.MaxProjectSize * 1024 - project.Size;
            bool imageSizeValid = imageSize <= maxImageSize && imageSize > 0;
            return imageSizeValid;
        }
    }
}