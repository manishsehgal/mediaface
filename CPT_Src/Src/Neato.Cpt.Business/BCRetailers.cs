using System.Collections;
using System.Data;
using System.IO;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.Business {
    public class BCRetailers {
        private static Retailer[] retailers = null;
        public const int defaultId = 1;

        private BCRetailers() {}

        public static Retailer[] GetRetailers() {
            if (retailers == null) {
                retailers = DORetailer.GetRetailers();
                foreach (Retailer retailer in retailers) {
                    if (retailer.Id == defaultId) {
                        retailer.HasBrandedSite = true;
                        break;
                    }
                }
            }

            return retailers;
        }

        public static int GetId(string retailerName) {
            Retailer[] retailerList = GetRetailers();

            foreach (Retailer retailer in retailerList) {
                if (string.Compare(retailer.Name, retailerName, true) == 0)
                    return retailer.Id;
            }

            return defaultId;
        }

        public static string GetRetailerNameById(int id) {
            Retailer[] retailerList = GetRetailers();
            foreach (Retailer retailer in retailerList) {
                if (retailer.Id == id)
                    return retailer.Name;
            }

            return GetDefaultRetailerName();

        }

        public static byte[] GetRetailerIcon(int id) {
            return DORetailer.GetRetailerIcon(id);
        }

        public static void DeleteRetailer(Retailer retailer) {
            DOGlobal.BeginTransaction();
            try {
                SynchronizeBuyNowRetailers(retailer, new Retailer[0]);
                DORetailer.DeleteRetailer(retailer.Id);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
            ResetRetailers();
        }

        public static void InsertRetailer(Retailer retailer, byte[] icon, Retailer[] buyNowRetailers) {
            DOGlobal.BeginTransaction();
            try {
                DORetailer.InsertRetailer(retailer, icon);
                SynchronizeBuyNowRetailers(retailer, buyNowRetailers);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
            ResetRetailers();
        }

        public static void Update(Retailer retailer, Retailer[] buyNowRetailers) {
            DOGlobal.BeginTransaction();
            try {
                DORetailer.Update(retailer);
                SynchronizeBuyNowRetailers(retailer, buyNowRetailers);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
            ResetRetailers();
        }

        public static int UpdateIcon(Retailer retailer, byte[] image) {
            return DORetailer.UpdateIcon(retailer, image);
        }

        public static void ResetRetailers() {
            retailers = null;
        }

        public static Retailer GetRetailer(int id) {
            Retailer[] retailerList = GetRetailers();
            Retailer defaultRetailer = null;
            foreach (Retailer retailer in retailerList) {
                if (retailer.Id == id)
                    return retailer;
                if (retailer.Id == defaultId)
                    defaultRetailer = retailer;
            }
            return defaultRetailer;
        }

        public static Retailer FindRetailer(int id) {
            Retailer[] retailerList = GetRetailers();
            foreach (Retailer retailer in retailerList) {
                if (retailer.Id == id)
                    return retailer;
            }
            return null;
        }

        public static string GetDefaultRetailerName() {
            Retailer retailer = GetRetailer(defaultId);
            return retailer.Name;
        }

        public static void SaveClosePage(string htmlSource, string applicationRoot, string retailerName) {
            byte[] data = ConvertHelper.StringToByteArray(htmlSource);

            string retailersPath = Path.Combine(applicationRoot, RetailerConstants.RetailersFolder);
            string retailerPath = Path.Combine(retailersPath, retailerName);
            string filePath = Path.Combine(retailerPath, RetailerConstants.ClosePageFileName);

            if (DOFileSystem.FileExist(filePath))
                DOFileSystem.UnsetReadOnlyAttribute(filePath);
            else
                DOFileSystem.CreateDirectory(retailerPath);
            DOFileSystem.WriteFile(data, filePath);
        }

        public static byte[] LoadClosePage(string applicationRoot, string retailerName) {
            string retailersPath = Path.Combine(applicationRoot, RetailerConstants.RetailersFolder);
            string filePath = Path.Combine(Path.Combine(retailersPath, retailerName), RetailerConstants.ClosePageFileName);
            if (DOFileSystem.FileExist(filePath))
                return DOFileSystem.ReadFile(filePath);
            else
                return new byte[0];
        }

        public static bool IsUniqueRetailerName(string name) {
            Retailer[] retailerList = GetRetailers();

            foreach (Retailer retailer in retailerList) {
                if (string.Compare(retailer.Name, name, true) == 0)
                    return false;
            }

            return true;
        }

        public static Retailer GetRetailerByName(string name) {
            Retailer[] retailers = GetRetailers();
            foreach (Retailer retailer in retailers) {
                if (string.Compare(retailer.Name, name, true) == 0)
                    return retailer;
            }
            return null;
        }

        public static Retailer[] GetRetailers(Carrier carrier) {
            return DORetailer.GetRetailers(carrier);
        }

        public static bool CheckCarrierForAnyDevices(Carrier carrier, Retailer retailer, string paperState) {
            return DORetailer.CheckCarrierForAnyDevices(carrier, retailer, paperState);
        }

        public static bool CheckBrandForAnyDevices(DeviceBrand brand, Retailer retailer, string paperState) {
            return DORetailer.CheckBrandForAnyDevices(brand, retailer, paperState);
        }

        public static Retailer[] GetRetailers(Paper paper) {
            return DORetailer.GetRetailers(paper);
        }

        public static Retailer[] BuyNowRetailerEnum(Retailer retailer) {
            ArrayList retailerList = new ArrayList();
            int[] currentRetailers = DORetailerBuyNow.Enum(retailer.Id);
            for(int i = 0; i < currentRetailers.Length; ++i)
                retailerList.Add(GetRetailer(currentRetailers[i]));
            
            retailerList.Sort(new Retailer.RetailerNameCompare());
            return (Retailer[])retailerList.ToArray(typeof(Retailer));
        }

        private static void SynchronizeBuyNowRetailers(Retailer retailer, Retailer[] buyNowRetailers) {
            Retailer[] currentRetailers = BuyNowRetailerEnum(retailer);
            ArrayList retailersToAdd = new ArrayList();
            ArrayList retailersToRemove = new ArrayList();

            foreach (Retailer ret in currentRetailers) {
                if (!FindValue(buyNowRetailers, ret)) {
                    retailersToRemove.Add(ret);
                }
            }
            foreach (Retailer ret in buyNowRetailers) {
                if (!FindValue(currentRetailers, ret)) {
                    retailersToAdd.Add(ret);
                }
            }
            AddBuyNowRetailer(retailer, (Retailer[])retailersToAdd.ToArray(typeof(Retailer)));
            DelBuyNowRetailer(retailer, (Retailer[])retailersToRemove.ToArray(typeof(Retailer)));
        }

        private static bool FindValue(Retailer[] list, Retailer value) {
            foreach (Retailer i in list) {
                if (i == value)
                    return true;
            }
            return false;
        }

        private static void AddBuyNowRetailer(Retailer retailer, Retailer[] retailers) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Retailer ret in retailers) {
                    DORetailerBuyNow.Insert(retailer.Id, ret.Id);
                }
                DOGlobal.CommitTransaction();
            } 
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        private static void DelBuyNowRetailer(Retailer retailer, Retailer[] retailers) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Retailer ret in retailers) {
                    DORetailerBuyNow.Delete(retailer.Id, ret.Id);
                }
                DOGlobal.CommitTransaction();
            } 
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
    }
}