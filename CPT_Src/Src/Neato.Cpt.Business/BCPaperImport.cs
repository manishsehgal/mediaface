using System.Collections;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;

using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;

namespace Neato.Cpt.Business {
    public class BCPaperImport {
        public BCPaperImport() {}

        private class XmlValidator {
            private XmlSchema schema;
            private XmlValidatingReader validatingReader;
            private ArrayList errorsValue = new ArrayList();

            public string[] Errors {
                get { return (string[])errorsValue.ToArray(typeof(string)); }
            }

            public bool IsDocumentValid {
                get { return errorsValue.Count == 0; }
            }

            public XmlValidator(Stream xsd) {
                this.schema = XmlSchema.Read(xsd, new ValidationEventHandler(ValidationHandler));
            }

            public void Validate(string xmlFragment) {
                XmlDocument doc = new XmlDocument();
                try {
                    doc.LoadXml(xmlFragment);
                    Validate(doc);
                } catch (XmlException ex) {
                    errorsValue.Add(ex.Message);
                }

            }

            public void Validate(XmlDocument document) {
                if (document.DocumentElement != null) {
                    document.DocumentElement.SetAttribute("xmlns", schema.TargetNamespace);
                }

                XmlTextReader docReader = new XmlTextReader(document.OuterXml, XmlNodeType.Document, null);
                validatingReader = new XmlValidatingReader(docReader);

                validatingReader.Schemas.Add(schema);
                validatingReader.ValidationType = ValidationType.Schema;
                validatingReader.ValidationEventHandler += new ValidationEventHandler(ValidationHandler);
                try {
                    while (validatingReader.Read()) {}
                } catch (XmlException ex) {
                    errorsValue.Add(ex.Message);
                }
            }

            private void ValidationHandler(object sender, ValidationEventArgs args) {
                string nodeName = validatingReader.Name;
                if (validatingReader.NodeType == XmlNodeType.Attribute) {
                    string attributeName = validatingReader.Name;
                    validatingReader.MoveToElement();
                    nodeName = validatingReader.Name;
                    validatingReader.MoveToAttribute(attributeName);
                }
                string error = string.Format("Node: {0} - {1}", nodeName, args.Message);
                errorsValue.Add(error);
            }
        }

        public static string[] Validate(Stream paperXmlStream) {
            paperXmlStream.Position = 0;
            string paperXml = string.Empty;
            if (paperXmlStream != null) {
                StreamReader reader = new StreamReader(paperXmlStream);
                paperXml = reader.ReadToEnd();
            }
            return Validate(paperXml);
        }

        public static string[] Validate(string paperXml) {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Stream xsd = assembly.GetManifestResourceStream("Neato.Cpt.Business.PaperImportSchema.xsd");
            XmlValidator validator = new XmlValidator(xsd);
            validator.Validate(paperXml);
            return validator.Errors;
        }

        public static Paper ParsePaper(Stream paperXmlStream) {
            paperXmlStream.Position = 0;
            string paperXml = string.Empty;
            if (paperXmlStream != null) {
                StreamReader reader = new StreamReader(paperXmlStream);
                paperXml = reader.ReadToEnd();
            }
            return ParsePaper(paperXml);
        }

        public static Paper ParsePaper(string paperXml) {
            string[] errors = BCPaperImport.Validate(paperXml);
            if (errors.Length > 0) throw new BusinessException("Invalid xml document.");
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(paperXml);

            Paper paper = BCPaper.NewPaper();
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("n", doc.DocumentElement.GetAttribute("xmlns"));

            XmlNode paperNode = doc.SelectSingleNode("//n:Paper", nsmgr);
            float paperWidth = float.Parse(paperNode.Attributes["w"].Value, CultureInfo.InvariantCulture);
            float paperHeight = float.Parse(paperNode.Attributes["h"].Value, CultureInfo.InvariantCulture);
            paper.Size = new SizeF(paperWidth, paperHeight);
            XmlAttribute paperType = paperNode.Attributes["type"];
            if (paperType != null) {
                try {
                    paper.PaperType = (PaperType)PaperType.Parse(typeof(PaperType),paperType.Value, true);
                } catch {}
            }
            
            foreach (XmlNode faceNode in doc.SelectNodes("//n:Faces/Face", nsmgr)) {
                float faceX = float.Parse(faceNode.Attributes["x"].Value, CultureInfo.InvariantCulture);
                float faceY = float.Parse(faceNode.Attributes["y"].Value, CultureInfo.InvariantCulture);
                XmlAttribute rotationNode = faceNode.Attributes["rotation"];
                float faceRotation = rotationNode == null ? 0 : float.Parse(rotationNode.Value, CultureInfo.InvariantCulture);
                 
                string faceName = faceNode.Attributes["name"]!= null ? faceNode.Attributes["name"].Value : string.Empty;
                Face face = new Face();
                face.Contour = faceNode.FirstChild.Clone();
                face.AddName("", faceName);
                paper.AddFace(face, faceX, faceY, faceRotation);
            }
            return paper;
        }
    }
}