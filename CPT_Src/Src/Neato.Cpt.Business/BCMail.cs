using System;
using System.Web.Mail;

using Neato.Cpt.Data;
using Neato.Cpt.Data.Scheme;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;

namespace Neato.Cpt.Business {
    public sealed class BCMail {
        private const string emailConst = "email";
        private const string pswuidConst = "pswuid";
        public static string Email {
            get { return emailConst; }
        }

        public static string Pswuid {
            get { return pswuidConst; }
        }


        public static void SendForgotPasswordMail(string serverName, string pageName, string email, string culture, Retailer retailer) {
            DOGlobal.BeginTransaction();
            try {
                string link = string.Format("{0}{1}?{2}={3}&{4}=",
                    serverName, pageName, emailConst, email, pswuidConst);
                string subject = string.Empty;
                string bodyFormat = string.Empty;
                DOMail.GetMailFormat(MailType.ForgotPassword, culture, out subject, out bodyFormat);
            
                Customer customer = DOCustomer.GetCustomerByLogin(email, retailer.Id);
                string userName = (customer != null) ? customer.FullName : string.Empty;//"%UserName";
                string pswuid = Guid.NewGuid().ToString().Replace("-","");
                if (customer != null) {
                    customer.PasswordUid = pswuid;
                    BCCustomer.UpdateCustomer(customer, retailer);
                }
                string body = bodyFormat.Replace("%UserName", userName).Replace("%NewPasswordLink", link + pswuid);
                Send(email, string.Empty, Configuration.MailFrom, subject, body);
                DOGlobal.CommitTransaction();
            } catch(Exception exception) {
                DOGlobal.RollbackTransaction();
                throw new BusinessException(exception.Message, exception);
            }
        }

        public static void InsertTellAFriendRefusedUser(string email) {
            DOGlobal.BeginTransaction();
            try {
                DOMail.InsertTellAFriendRefusedUser(email);
                DOGlobal.CommitTransaction();
            } catch(Exception exception) {
                DOGlobal.RollbackTransaction();
                throw new BusinessException(exception.Message, exception);
            }
        }


        public static void SendTellAFriendMail(string mailToName, string mailTo,
            string mailFromName, string mailFrom,
            string mailText, string siteLink, string refuseLink,
            bool sendCopy, string culture)
        {
            try {
                if (DOMail.GetTellAFriendRefusedUserByEmail(mailTo) != null)
                    return;

                string mailToFullName = string.Format("{0} <{1}>", mailToName, mailTo);
                string mailFromFullName = string.Format("{0} <{1}>", mailFromName, mailFrom);

                string subject = string.Empty;
                string footerFormat = string.Empty;
                DOMail.GetMailFormat(MailType.TellAFriend, culture, out subject, out footerFormat);
            
                string footer = footerFormat.Replace("%PrintzHomeLink", siteLink).Replace("%UnsubscribeLink", refuseLink);
                string body = footer.Replace("%MailText", mailText);
                
                string cc = sendCopy ? mailFromFullName : string.Empty;

                Send(mailToFullName, cc, mailFromFullName, subject, body);
            } catch(Exception exception) {
                throw new BusinessException(exception.Message, exception);
            }
        }

        public static void SendAddNewPhoneMail(DeviceRequest deviceRequest, string culture) {
            try  {
                string to = deviceRequest.EmailAddress.Trim();
                string cc = string.Empty;
                string from = Configuration.MailFrom;
                string subject = string.Empty;
                string bodyFormat = string.Empty;
                DOMail.GetMailFormat(MailType.AddNewPhone, culture, out subject, out bodyFormat);

                string body = string.Format(bodyFormat);

                Send(to, cc, from, subject, body);
            }
            catch(Exception exception) {
                throw new BusinessException(exception.Message, exception);
            }
        }
        
        public static void SendPaperStateChangeNotification(string paperName, string oldState, string newState, string culture) {
            try  {
                SpecialUser[] users = BCSpecialUser.GetSpecialUsersToNotify();
                for (int i=0; i<users.Length; i++) {
                    SpecialUser user = users[i];
                    string to = user.Email.Trim();
                    string cc = string.Empty;
                    string from = Configuration.MailFrom;
                    string subject = "Paper state changed.";
                    string body = "Paper state for " + paperName + " changed from " + ConvertPaperStateToString(oldState) + " to " + ConvertPaperStateToString(newState) + ".";

                    Send(to, cc, from, subject, body);
                }
            }
            catch(Exception exception) {
                throw new BusinessException(exception.Message, exception);
            }
        }
        private static string ConvertPaperStateToString(string state) {
            switch (state) {
                case "a": return "Active";
                case "i": return "Inactive";
                case "t": return "TestMode";
                default:  return "Unknown";
            }
        }

        public static void SendPhoneRequestNotificationMail(DeviceRequest deviceRequest, string siteLink, string culture) {
            try {
                string to = deviceRequest.EmailAddress.Trim();
                string cc = string.Empty;
                string from = Configuration.MailFrom;
                string subject = string.Empty;
                string bodyFormat = string.Empty;
                DOMail.GetMailFormat(MailType.PhoneRequestNotification, culture, out subject, out bodyFormat);

                string body = bodyFormat.Replace
                    ("%PrintzHomeLink", siteLink).Replace
                    ("%DeviceName", deviceRequest.DeviceBrand + " "
                    + deviceRequest.Device);

                Send(to, cc, from, subject, body);
            }
            catch(Exception exception) 
            {
                throw new BusinessException(exception.Message, exception);
            }
        }

        public static void SendUserFeedbackMail(string userEmail, string userMessage, string culture, bool sendUserMessage) {
            try  {
                string to = userEmail.Trim();
                string cc = string.Empty;
                string from = Configuration.MailFrom;
                string subject = string.Empty;
                string bodyFormat = string.Empty;
                DOMail.GetMailFormat(MailType.UserFeedback, culture, out subject, out bodyFormat);

                string body = string.Format(bodyFormat);

                string userMessageSubject = "Tell Us What You Think";
                
                //send user message to support service
                if (sendUserMessage) {
                    Send(from, cc, userEmail, userMessageSubject, userMessage);
                }

                //send thank you message from support service to user
                Send(to, cc, from, subject, body);

            }
            catch(Exception exception) {
                throw new BusinessException(exception.Message, exception);
            }
        }

        private static void Send(string to, string cc, string from, string subject, string body) {
            const string ShemaPrefix = "http://schemas.microsoft.com/cdo/configuration/";

            MailMessage mailMessage = new MailMessage();
            mailMessage.To = to;
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.From = from;
            mailMessage.Cc = cc;
            mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
            lock (typeof(SmtpMail)) {
                SmtpMail.SmtpServer = Configuration.SmtpServer;
                if (Configuration.SmtpLogin.Length > 0) {
                    mailMessage.Fields[ShemaPrefix + "smtpauthenticate"] = 1;
                    mailMessage.Fields[ShemaPrefix + "sendusername"] = Configuration.SmtpLogin;
                    mailMessage.Fields[ShemaPrefix + "sendpassword"] = Configuration.SmtpPassword;
                }

                SmtpMail.Send(mailMessage);
            }
        }

        
        public static MailFormatData MailFormatEnum() {
            return DOMail.MailFormatEnum();
        }

        public static void MailFormatUpd(MailFormatData data) {
            DOGlobal.BeginTransaction();
            try {
                DOMail.MailFormatUpd(data);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
    }
}