using System;
using System.Collections;
using System.Data;
using System.IO;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.Business {
    public class BCDevice {
        public BCDevice() {}

        public static Device GetDevice(DeviceBase device) {
            return DODevice.GetDevice(device);
        }

        public static Device[] GetDeviceList() {
            return DODevice.EnumerateDeviceByParam(null, DeviceType.Undefined, null, null);
        }

        public static Device[] GetDeviceList(DeviceType deviceType) {
            return DODevice.EnumerateDeviceByParam(null, deviceType, null, null);
        }
        public static Device[] GetDeviceList(DeviceType deviceType, Retailer retailer) {
            Device [] result = DODevice.EnumerateDeviceByParam(null, deviceType, null, null);
            return GetDevicesForRetailer(result, retailer);
       }

        public static Device[] GetDeviceList(DeviceBrandBase brandBase, DeviceType deviceType) {
            return DODevice.EnumerateDeviceByParam(brandBase, deviceType, null, null);
        }

        public static Device[] GetDeviceList(DeviceBrandBase brandBase, DeviceType deviceType, CarrierBase carrier) {
            return DODevice.EnumerateDeviceByParam(brandBase, deviceType, carrier, null);
        }
        public static Device[] GetDeviceList(DeviceBrandBase brandBase, DeviceType deviceType, CarrierBase carrier, Retailer retailer) 
        {
            Device [] result = DODevice.EnumerateDeviceByParam(brandBase, deviceType, carrier, null);
            return GetDevicesForRetailer(result, retailer);
        }


        public static Device[] GetDeviceList(CarrierBase carrier) 
        {
            return DODevice.EnumerateDeviceByParam(null, DeviceType.Undefined, carrier, null);
        }

        public static Device[] GetDeviceList(DeviceBrandBase brand) {
            return DODevice.EnumerateDeviceByParam(brand, DeviceType.Undefined, null, null);
        }

        public static Device[] GetDeviceList(FaceBase face) {
            return DODevice.EnumerateDeviceByParam(null, DeviceType.Undefined, null, face);
        }

        public static Device[] GetDeviceListByPaper(PaperBase paper) {
            return DODevice.GetDeviceListByPaper(paper);
        }

        public static byte[] GetDeviceIcon(DeviceBase device) {
            return DODevice.GetDeviceIcon(device);
        }

        public static Device NewDevice() {
            Device device = new Device();
            return device;
        }

        public static void Save(Device device) {
            Save(device, null, null);
        }
        
        public static void Save(Device device, Stream icon, Stream bigIcon) {
            Save(device, icon, bigIcon, null);
        }
        
        public static void Save(Device device, Stream icon, Stream bigIcon, CarrierBase[] newCarriers) {
            DOGlobal.BeginTransaction();
            try {
                if (device.IsNew) {
                    DODevice.Add(device);
                } else {
                    DODevice.Update(device);
                }
                ChangeIcon(device, icon, bigIcon);
                SynchronizeCarriers(device, newCarriers);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        private static void SynchronizeCarriers(DeviceBase device, CarrierBase[] newCarriers) {
            if (newCarriers == null) return;
            ArrayList currentCarrierList = new ArrayList(BCCarrier.GetCarrierList(device, null));
            ArrayList newCarrierList = new ArrayList(newCarriers);
            ArrayList carriersToAdd = new ArrayList();
            ArrayList carriersToRemove = new ArrayList();

            foreach (Carrier carrier in currentCarrierList) {
                if (newCarrierList.IndexOf(carrier) < 0) {
                    carriersToRemove.Add(carrier);
                }
            }
            foreach (CarrierBase carrier in newCarrierList) {
                if (currentCarrierList.IndexOf(carrier) < 0) {
                    carriersToAdd.Add(carrier);
                }
            }
            AddToCarrier(device, (CarrierBase[])carriersToAdd.ToArray(typeof(CarrierBase)));
            RemoveFromCarrier(device, (CarrierBase[])carriersToRemove.ToArray(typeof(CarrierBase)));
        }

        public static void ChangeIcon(DeviceBase device, Stream icon, Stream bigIcon) {
            DOGlobal.BeginTransaction();
            try {
                if (bigIcon != null && bigIcon.Length > 0) {
                    DODevice.UpdateBigIcon(device, ConvertHelper.StreamToByteArray(bigIcon));
                }
                if (icon != null && icon.Length > 0) {
                    DODevice.UpdateIcon(device, ConvertHelper.StreamToByteArray(icon));
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Delete(params DeviceBase[] devices) {
            DOGlobal.BeginTransaction();
            try {
                foreach (DeviceBase device in devices) {
                    RemoveFromCarrier(device);
                    UnbindFaces(device);
                    DODevice.Delete(device);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void RemoveFromCarrier(DeviceBase device) {
            RemoveFromCarrier(device, null);
        }

        public static void RemoveFromCarrier(DeviceBase device, CarrierBase[] carriers) {
            DOGlobal.BeginTransaction();
            try {
                if (carriers == null) {
                    DODevice.RemoveFromCarrier(device, null);
                } else {
                    foreach (CarrierBase carrier in carriers) {
                        DODevice.RemoveFromCarrier(device, carrier);
                    }
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void AddToCarrier(DeviceBase device, params CarrierBase[] carriers) {
            DOGlobal.BeginTransaction();
            try {
                foreach (CarrierBase carrier in carriers) {
                    DODevice.AddToCarrier(device, carrier);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UnbindFaces(DeviceBase device) {
            UnbindFaces(device, null);
        }

        public static void UnbindFaces(DeviceBase device, Face[] faces) {
            if (faces != null && faces.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                if (faces == null) {
                    DOFace.UnbindFromDevice(null, device);
                } else {
                    foreach (FaceBase face in faces) {
                        DOFace.UnbindFromDevice(face, device);
                    }
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void BindFaces(DeviceBase device, Face[] faces) {
            DOGlobal.BeginTransaction();
            try {
                foreach (FaceBase face in faces) {
                    DOFace.BindToDevice(face, device);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static int[] GetRatingList() {
            int[] ratings = new int[10];
            for (int index = 0; index < ratings.Length; index++) {
                ratings[index] = index;
            }
            return ratings;
        }

        public static byte[] GetDeviceBigIcon(DeviceBase device) {
            return DODevice.GetDeviceBigIcon(device);
        }
        public static Device [] GetDevicesForRetailer(Device [] devices, Retailer retailer) {
            ArrayList result = new ArrayList();
            foreach (Device device in devices) {
                PaperBase[] papers = BCPaper.GetPaperList(device, retailer);
                if (papers.Length > 0)
                    result.Add(device);
            }

            return (Device[])result.ToArray(typeof(Device));
        }
    }
}