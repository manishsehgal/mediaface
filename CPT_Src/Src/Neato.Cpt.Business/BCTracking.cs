using System.Data;
using System.Threading;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Tracking;

namespace Neato.Cpt.Business {
    public class BCTracking {
        public BCTracking() {}

        public static void Add(UserAction userAction, string userHostAddress, Retailer retailer) {
            string login = Thread.CurrentPrincipal.Identity.Name;
            Add(userAction, login, userHostAddress, retailer);
        }

        public static void Add(UserAction userAction, string login, string userHostAddress, Retailer retailer) {
            IPAddress userIP = IPAddress.ToIPAddress(userHostAddress);
            if (!BCIPFilter.IsUserContainsInIPFilters(userIP))
                DOTracking.Add(userAction, login, BCTimeManager.GetCurrentTime(), retailer);
        }

        public static DataSet GetList(Retailer retailer) {
            return DOTracking.GetList(retailer);
        }
    }
}