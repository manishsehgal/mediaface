using Neato.Cpt.Data;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Business {
    public sealed class BCSupportPage {
        private BCSupportPage() {}

        public static LocalizedText Get(string culture, Retailer retailer) {
            LocalizedText[] list = DOSupportPage.Enum(culture, retailer);
            return BCLocalizedText.GetTextFromList(list, culture);
        }

        public static void Update(LocalizedText localizedText,Retailer retailer) {
            LocalizedText text = Get(localizedText.Culture, retailer);
            if (text != null)
                DOSupportPage.Update(localizedText.Culture, localizedText.Text, retailer);
            else
                DOSupportPage.Insert(localizedText.Culture, localizedText.Text, retailer);
        }

    }
}