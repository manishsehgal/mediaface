using System;
using System.IO;
using System.Xml;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Business {
    public sealed class BCPrinting {
        private BCPrinting() {}

        public static byte[] GetPdfOnly(Project project, string applicationRoot) {
            XmlNode paintTypes = DOPaintTypes.GetPaintTypes(applicationRoot);
            XmlNode shapeTypes = DOShapeTypes.GetShapeTypes(applicationRoot);

            return PdfWrapper.GeneratePdf(project, paintTypes, shapeTypes);
        }

        public static byte[] GetPdf(Project project, string applicationRoot) {

            byte[] pdf = GetPdfOnly(project, applicationRoot);
            
            bool saveToHeap = Configuration.SavePdfInHeap && Configuration.PdfHeapPath.Length > 0;
            if (saveToHeap) {
                string heapPath = Path.Combine(applicationRoot, Configuration.PdfHeapPath);
                try {
                    SavePdfInHeap(pdf, heapPath);
                } catch (Exception ex) {
                    ExceptionPublisher.Publish(ex);
                }
            }
            
            return pdf;
        }

        public static string SavePdfInHeap(byte[] pdf, string heapPath) {
            if (!DOFileSystem.DirectoryExist(heapPath)) {
                DOFileSystem.CreateDirectory(heapPath);
            }

            string fileName = Guid.NewGuid().ToString("D");
            fileName = Path.ChangeExtension(fileName, ".pdf");
            string path = Path.Combine(heapPath, fileName);

            DOFileSystem.WriteFile(pdf, path);
            return path;
        }
    }
}