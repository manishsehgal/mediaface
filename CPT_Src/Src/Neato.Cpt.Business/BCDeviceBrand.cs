using System.Collections;
using System.Data;
using System.IO;
using System.Threading;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.Business {
    public sealed class BCDeviceBrand {
        private BCDeviceBrand() {}

        private static DeviceBrand[] FilterBrands(DeviceBrand[] brands) {
            ArrayList brandList = new ArrayList();
            foreach(DeviceBrand brand in brands) {
                Device[] devices = BCDevice.GetDeviceList(brand);
                bool foundDevice = false;
                foreach (Device device in devices) {
                    PaperBase[] papers = BCPaper.GetPaperList(new DeviceBase(device.Id));
                    if (papers.Length == 0) //"Papers don`t exist.");
                        continue;

                    foundDevice = true;
                    break;
                }

                if (foundDevice)
                    brandList.Add(brand);
            }
            return (DeviceBrand[])brandList.ToArray(typeof(DeviceBrand));
        }

        public static DeviceBrand[] GetDeviceBrandList() {
            return EnumBrandInternal(DeviceType.Undefined, null, null);
        }
        public static DeviceBrand[] GetDeviceBrandList(DeviceType deviceType) {
            return EnumBrandInternal(deviceType, null, null);
        }
        public static DeviceBrand[] GetDeviceBrandList(DeviceType deviceType, string paperState) {
            return EnumBrandInternal(deviceType, null, paperState);
        }
        /*
        public static DeviceBrand[] GetDeviceBrandList(DeviceType deviceType, CarrierBase carrier) {
            return EnumBrandInternal(deviceType, carrier, null);
        }
        */

        /*
        public static DeviceBrand[] GetDeviceWithPaperBrandList(DeviceType deviceType) 
        {
            DeviceBrand[] brands = EnumBrandInternal(deviceType, null, null);
            return FilterBrands(brands);
        }
        public static DeviceBrand[] GetDeviceWithPaperBrandList(DeviceType deviceType, CarrierBase carrier) {
            DeviceBrand[] brands = EnumBrandInternal(deviceType, carrier, null);
            return FilterBrands(brands);
        }
        */

        public static DeviceBrand[] GetDeviceWithPaperBrandListWithRetailerCheck(DeviceType deviceType,Retailer retailer) 
        {
            DeviceBrand[] brands = EnumBrandInternal(deviceType, null, "");
            brands = FilterBrands(brands);
            return CheckBrandForRetailer(brands, retailer,"");

        }
        public static DeviceBrand[] GetDeviceWithPaperBrandListWithRetailerCheck(DeviceType deviceType, CarrierBase carrier,Retailer retailer) 
        {
            DeviceBrand[] brands = EnumBrandInternal(deviceType, carrier, "");
            brands = FilterBrands(brands);
            return  CheckBrandForRetailer(brands, retailer,"");
        }

        private static DeviceBrand[] EnumBrandInternal(DeviceType deviceType, CarrierBase carrier, string paperState) {
            string state = null;
            if (paperState != null) {
                state = "a";
                string login = Thread.CurrentPrincipal.Identity.Name;
                SpecialUser[] users = BCSpecialUser.GetSpecialUsers(login, 1);
                if (users.Length > 0) {
                    state = "ait";
                }
            }
            return DODeviceBrand.EnumerateDeviceBrandByParam(deviceType, carrier, state);
        }



        public static byte[] GetDeviceBrandIcon(DeviceBrandBase brand) {
            return DODeviceBrand.GetDeviceBrandIcon(brand);
        }

        public static DeviceBrand NewDeviceBrand() {
            DeviceBrand brand = new DeviceBrand();
            return brand;
        }

        public static void Save(DeviceBrand brand) {
            Save(brand, null);
        }

        public static void Save(DeviceBrand brand, Stream icon) {
            DOGlobal.BeginTransaction();
            try {
                if (brand.IsNew) {
                    DODeviceBrand.Add(brand);
                } else {
                    DODeviceBrand.Update(brand);
                }
                ChangeIcon(brand, icon);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void ChangeIcon(DeviceBrandBase brand, Stream icon) {
            if (icon != null && icon.Length > 0) {
                DOGlobal.BeginTransaction();
                try {
                    DODeviceBrand.UpdateIcon(brand, ConvertHelper.StreamToByteArray(icon));
                    DOGlobal.CommitTransaction();
                } catch (DBConcurrencyException ex) {
                    DOGlobal.RollbackTransaction();
                    throw new ConcurrencyException(ex.Message, ex);
                } catch {
                    DOGlobal.RollbackTransaction();
                    throw;
                }
            }
        }

        public static void Delete(DeviceBrandBase brand) {
            if (brand.IsNew) return;
            Device[] brandDevices = BCDevice.GetDeviceList(brand);
            DOGlobal.BeginTransaction();
            try {
                BCDevice.Delete(brandDevices);
                DODeviceBrand.Delete(brand);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
        public static DeviceBrand[] CheckBrandForRetailer(DeviceBrand[] brands, Retailer retailer, string paperState) {
            string state = null;
            if (paperState != null) 
            {
                state = "a";
                string login = Thread.CurrentPrincipal.Identity.Name;
                SpecialUser[] users = BCSpecialUser.GetSpecialUsers(login, 1);
                if (users.Length > 0) 
                {
                    state = "ait";
                }
            }
            ArrayList result = new ArrayList();
            foreach (DeviceBrand brand in brands)
            {
                if(true == BCRetailers.CheckBrandForAnyDevices(brand, retailer,state))
                    result.Add(brand);
            }
            return (DeviceBrand[]) result.ToArray(typeof(DeviceBrand));
        }
        
    }
}