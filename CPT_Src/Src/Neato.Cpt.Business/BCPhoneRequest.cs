using System;
using System.Data;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;

namespace Neato.Cpt.Business {
    public class BCDeviceRequest {
        public BCDeviceRequest() {}

        public static DeviceRequest[] GetPhoneRequests( Retailer retailer) {
            return DODeviceRequest.GetPhoneRequests(retailer);
        }

        public static void InsertPhoneRequest(DeviceRequest deviceRequest, string culture, Retailer retailer) {
            DOGlobal.BeginTransaction();
            try {
                deviceRequest.Created = BCTimeManager.GetCurrentTime();
                DODeviceRequest.InsertPhoneRequest(deviceRequest, retailer);
                BCMail.SendAddNewPhoneMail(deviceRequest, culture);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UpdatePhoneRequest(DeviceRequest deviceRequest, Retailer retailer) {
            DOGlobal.BeginTransaction();
            try {
                DODeviceRequest.UpdatePhoneRequest(deviceRequest, retailer);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void DeletePhoneRequest(int id) {
            DOGlobal.BeginTransaction();
            try {
                DODeviceRequest.DeletePhoneRequest(id);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static DeviceRequest[] GetPhoneRequestsByCarrierPhoneManufacturerPhoneModel(string carrier, bool otherCarrier, string deviceBrand, bool otherDeviceBrand, string device, bool otherDevice,DateTime timeStart, DateTime timeEnd, Retailer retailer) {
            return DODeviceRequest.GetPhoneRequestsByCarrierPhoneManufacturerPhoneModel(carrier, otherCarrier, deviceBrand, otherDeviceBrand, device, otherDevice,timeStart,timeEnd, retailer);
        }

        public static DataSet GetPhoneRequestReportByDatesAndBrand
            (string deviceBrand, bool otherDeviceBrand,
            DateTime startDate, DateTime endDate, Retailer retailer) {
            return DODeviceRequest.GetPhoneRequestReportByDatesAndBrand
                (deviceBrand, otherDeviceBrand,
                startDate, endDate,retailer);
        }

        public static DataSet GetPhoneRequestEmailsReportByDates
            (DateTime startDate, DateTime endDate, Retailer retailer) {
            return DODeviceRequest.GetPhoneRequestEmailsReportByDates
                (startDate, endDate,retailer);
        }
    }
}