using System;
using System.IO;
using System.Text.RegularExpressions;
using Neato.Cpt.Data;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Business {
    public class BCMaintenanceClosing {
        public BCMaintenanceClosing() {}

        private const string RegexMathPattern = @"{0}\\?$";

        public static bool TestDirectoryPath(string serverName, string siteName, string virtualDirectoryName, string path) {
            string virtualPath = DOIis.GetVirtualDirectoryPath(serverName, siteName, virtualDirectoryName);
            Regex regex = new Regex(string.Format(RegexMathPattern, path));
            return regex.IsMatch(virtualPath);
        }

        public static bool SiteClosed {
            get { return BCMaintenanceClosing.TestDirectoryPath(Configuration.ServerName, Configuration.SiteName, Configuration.VirtualFolderName, RetailerConstants.RetailersFolder); }
            set {
                string currentPath = DOIis.GetVirtualDirectoryPath(Configuration.ServerName, Configuration.SiteName, Configuration.VirtualFolderName);
                string newPath = string.Empty;

                string re = string.Format(RegexMathPattern, RetailerConstants.RetailersFolder);
                Regex regex = new Regex(re);
                bool closed = regex.IsMatch(currentPath);

                //string aspxDll = DOIis.GetAspxDllParh(Configuration.ServerName, Configuration.VirtualFolderName);

                if(value) {
                    if(closed) {
                        newPath = currentPath;
                    }
                    else {
                        newPath = Path.Combine(currentPath, RetailerConstants.RetailersFolder);
                    }
                    //DOIis.AddAnyMapping(Configuration.ServerName, Configuration.VirtualFolderName, aspxDll);
                }
                else {
                    if(closed) {
                        newPath = regex.Replace(currentPath, string.Empty);
                    }
                    else {
                        newPath = currentPath;
                    }
                    //DOIis.RemoveAnyMapping(Configuration.ServerName, Configuration.VirtualFolderName, aspxDll);
                }
                DOIis.ChangeVirtualDirectoryPath(Configuration.ServerName, Configuration.SiteName, Configuration.VirtualFolderName, newPath);
            }
        }
    }
}
