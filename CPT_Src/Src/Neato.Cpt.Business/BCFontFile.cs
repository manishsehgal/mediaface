using System;
using System.IO;
using System.Web;

namespace Neato.Cpt.Business
{
	public class BCFontFile
	{
		public BCFontFile() {}

		public static string GetFile(string fileName) {
            try {
                fileName.Replace("\\", "");
                fileName.Replace("/", "");
                string path = HttpContext.Current.Server.MapPath(null);
                path += "\\Fonts\\" + fileName + ".txt";
                StreamReader sr = new StreamReader(path);
                string text = sr.ReadLine();
                sr.Close();
			    return text;
            }
            catch(Exception) {
                return "";
            }
		}
	}
}
