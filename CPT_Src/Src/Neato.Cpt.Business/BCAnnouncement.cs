using System.Data;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.DataBase;
using Neato.Cpt.Entity.Exceptions;

namespace Neato.Cpt.Business {
	public class BCAnnouncement {
		public BCAnnouncement() {}

		public static AnnouncementData EnumAnnouncements(Retailer retailer) {
			return DOAnnouncement.EnumAnnouncements(retailer);
		}

        public static AnnouncementData GetLastAnnouncements(Retailer retailer) {
            return DOAnnouncement.GetLastAnnouncements(retailer);
        }

        public static void UpdateAnnouncement(AnnouncementData data) {
            DOGlobal.BeginTransaction();
            try {
                DOAnnouncement.UpdateAnnouncement(data);
                DOGlobal.CommitTransaction();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
	}
}
