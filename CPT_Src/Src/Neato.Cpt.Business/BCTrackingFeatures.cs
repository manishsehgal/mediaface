using System;
using System.Data;
using System.Threading;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Tracking;

namespace Neato.Cpt.Business {
    public class BCTrackingFeatures {
        public BCTrackingFeatures() {}

        public static void Add(Feature feature, string userHostAddress, Retailer retailer) {
            string login = Thread.CurrentPrincipal.Identity.Name;
            Add(feature, login, userHostAddress, retailer);
        }

        public static void Add(Feature feature, string login, string userHostAddress, Retailer retailer) {
            IPAddress userIP = IPAddress.ToIPAddress(userHostAddress);
            if (!BCIPFilter.IsUserContainsInIPFilters(userIP))
                DOTrackingFeatures.Add(feature, login, BCTimeManager.GetCurrentTime(), retailer);
        }

        public static DataSet GetFeaturesListByDate(DateTime startTime, DateTime endTime, Retailer retailer) {
            return DOTrackingFeatures.GetFeaturesListByDate(startTime, endTime, retailer);
        }
    }
}