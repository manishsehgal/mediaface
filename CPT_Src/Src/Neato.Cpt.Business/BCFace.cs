using System.Data;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;

namespace Neato.Cpt.Business {
    public class BCFace {
        public BCFace() {}
        public static Face GetFace(FaceBase face) {
            return DOFace.GetFace(face);
        }

        public static Face NewFace() {
            Face face = new Face();
            return face;
        }

        public static void Save(Face face) {
            if (face == null) return;
            Save(new Face[] {face});
        }

        public static void Save(Face[] faces) {
            if (faces == null || faces.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                foreach (Face face in faces) {
                    if (face.IsNew) {
                        DOFace.Add(face);
                    } else {
                        DOFace.Update(face);
                    }
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Delete(FaceBase[] faces) {
            if (faces == null || faces.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                foreach (FaceBase face in faces) {
                    UnbindPapers(face);
                    UnbindDevices(face);
                    DOFace.Delete(face);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UnbindPapers(FaceBase face) {
            UnbindPapers(face, null);
        }

        public static void UnbindPapers(FaceBase face, PaperBase[] papers) {
            if (papers != null && papers.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                if (papers == null) {
                    DOFace.UnbindFromPaper(face, null);
                } else {
                    foreach (PaperBase paper in papers) {
                        DOFace.UnbindFromPaper(face, paper);
                    }
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UnbindDevices(FaceBase face) {
            UnbindDevices(face, null);
        }

        public static void UnbindDevices(FaceBase face, DeviceBase[] devices) {
            if (devices != null && devices.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                if (devices == null) {
                    DOFace.UnbindFromDevice(face, null);
                } else {
                    foreach (DeviceBase device in devices) {
                        DOFace.UnbindFromDevice(face, device);
                    }
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void BindDevices(FaceBase face, DeviceBase[] devices) {
            DOGlobal.BeginTransaction();
            try {
                foreach (DeviceBase device in devices) {
                    DOFace.BindToDevice(face, device);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
    }
}