using System.Collections;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Business {
    public class BCPaperType {
        private static Hashtable table;

        static BCPaperType() {
            table = DOPaperType.Enum();
        }

        public static string GetName(PaperType paperType) {
            string name = table[paperType] as string;
            if (name == null)
                name = string.Empty;
            return name;
        }
    }
}