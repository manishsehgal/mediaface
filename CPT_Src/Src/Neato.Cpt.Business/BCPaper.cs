using System.Collections;
using System.Data;
using System.IO;
using System.Threading;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.Business {
    public sealed class BCPaper {
        private BCPaper() {}

        public static PaperBase[] GetPaperList(DeviceBase device) {
            return EnumPaperInternal(null, DeviceType.Undefined, device, null, null, null);
        }
        
        public static PaperBase[] GetPaperList(DeviceBase device, Retailer retailer) 
        {
            PaperBase[] papers = EnumPaperInternal(null, DeviceType.Undefined, device, null, null, null);
            return CheckPapersForRetailer(papers, retailer);
        }

        public static PaperBase[] GetPaperList(PaperBrandBase paperBrandBase) {
            return EnumPaperInternal(paperBrandBase, DeviceType.Undefined, null, null, null, null);
        }

        public static PaperBase[] GetPaperList(PaperBrandBase paperBrandBase, DeviceType deviceType) {
            return EnumPaperInternal(paperBrandBase, deviceType, null, null, null, null);
        }

        public static PaperBase[] GetPaperList(PaperBrandBase paperBrandBase, DeviceType deviceType, DeviceBase device) {
            return EnumPaperInternal(paperBrandBase, deviceType, device, null, null, null);
        }

        public static Paper[] GetPapers(PaperBrandBase paperBrandBase, string paperState) {
            return GetPapers(paperBrandBase, DeviceType.Undefined, null, null, paperState, null);
        }

        public static Paper[] GetPapers(PaperBrandBase paperBrandBase, DeviceType deviceType, DeviceBase device, string paperName, string paperState, Retailer retailer) {
            PaperBase[] paperList = EnumPaperInternal(paperBrandBase, deviceType, device, paperName, paperState, retailer);

            ArrayList papers = new ArrayList(paperList.Length);
            foreach (PaperBase paper in paperList) {
                papers.Add(GetPaper(paper));
            }
            return (Paper[])papers.ToArray(typeof(Paper));
        }

        private static PaperBase[] EnumPaperInternal(PaperBrandBase paperBrand, DeviceType deviceType, DeviceBase device, string paperName, string paperState, Retailer retailer) {
            string state = null;
            if (paperState != null) {
                state = paperState;
            } else {
                string login = Thread.CurrentPrincipal.Identity.Name;
                SpecialUser[] users = BCSpecialUser.GetSpecialUsers(login, 1);
                if (users.Length > 0) {
                    state = "ait";
                }
            }
            return DOPaper.EnumeratePaperByParam(paperBrand, deviceType, device, paperName, state, retailer);
        }

        public static Paper GetPaper(PaperBase paperBase) {
            return DOPaper.GetPaper(paperBase);
        }



        public static byte[] GetPaperIcon(PaperBase paper) {
            return DOPaper.GetPaperIcon(paper);
        }

        public static Paper NewPaper() {
            Paper paper = new Paper();
            return paper;
        }

        public static void Save(Paper paper) {
            Save(paper, null);
        }

        public static void Save(Paper paper, Stream icon) {
            Save(paper, icon, null, null);
        }

        public static void Save(Paper paper, Stream icon, DeviceBase[] devices, Retailer[] newRetailers) {
            DOGlobal.BeginTransaction();
            try {
                if (paper.IsNew) {
                    DOPaper.Add(paper);
                } else {
                    DOPaper.Update(paper);
                }
                ChangeIcon(paper, icon);
                BCFace.Save(paper.Faces);
                SynchronizeFaces(paper);
                SynchronizeDevices(paper, devices);
                SynchronizeRetailers(paper, newRetailers);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        private static void SynchronizeFaces(Paper paper) {
            SynchronizeFaces(paper, paper.Faces);
        }

        private static void SynchronizeFaces(PaperBase paper, Face[] faces) {
            if (faces == null) return;
            ArrayList newFaces = new ArrayList(faces);

            Paper oldPaper = DOPaper.GetPaper(paper);
            ArrayList oldFaces = new ArrayList(oldPaper.Faces);

            ArrayList bindFaces = new ArrayList();
            ArrayList unbindFaces = new ArrayList();

            foreach (Face face in oldPaper.Faces) {
                if (newFaces.IndexOf(face) < 0) {
                    unbindFaces.Add(face);
                }
            }
            foreach (Face face in faces) {
                if (oldFaces.IndexOf(face) < 0) {
                    bindFaces.Add(face);
                }
            }
            BindFaces(paper, (Face[])bindFaces.ToArray(typeof(Face)));
            UnbindFaces(paper, (Face[])unbindFaces.ToArray(typeof(Face)));
        }

        private static void SynchronizeDevices(Paper paper, DeviceBase[] devices) {
            if (devices == null) return;
            ArrayList newDevices = new ArrayList(devices);
            ArrayList oldDevices = new ArrayList(BCDevice.GetDeviceListByPaper(paper));

            ArrayList bindDevices = new ArrayList();
            ArrayList unbindDevices = new ArrayList();

            foreach (DeviceBase device in oldDevices) {
                if (newDevices.IndexOf(device) < 0) {
                    unbindDevices.Add(device);
                }
            }
            foreach (DeviceBase device in devices) {
                if (oldDevices.IndexOf(device) < 0) {
                    bindDevices.Add(device);
                }
            }
            BindDevices(paper, (DeviceBase[])bindDevices.ToArray(typeof(DeviceBase)));
            UnbindDevices(paper, (DeviceBase[])unbindDevices.ToArray(typeof(DeviceBase)));
        }

        public static void ChangeIcon(PaperBase paper, Stream icon) {
            if (icon == null || icon.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                DOPaper.UpdateIcon(paper, ConvertHelper.StreamToByteArray(icon));
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Delete(PaperBrandBase paperBrand) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Paper paper in GetPapers(paperBrand, "ait")) {
                    DeleteWithFaces(paper);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void DeleteWithFaces(Paper paper) {
            DOGlobal.BeginTransaction();
            try {
                Delete(paper);
                BCFace.Delete(paper.Faces);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Delete(PaperBase paper) {
            DOGlobal.BeginTransaction();
            try {
                UnbindFaces(paper);
                DOPaper.Delete(paper);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UnbindFaces(PaperBase paper) {
            UnbindFaces(paper, null);
        }

        public static void UnbindFaces(PaperBase paper, FaceBase[] faces) {
            if (faces != null && faces.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                if (faces == null) {
                    DOFace.UnbindFromPaper(null, paper);
                } else {
                    foreach (FaceBase face in faces) {
                        DOFace.UnbindFromPaper(face, paper);
                    }
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void BindFaces(PaperBase paper, params Face[] faces) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Face face in faces) {
                    DOFace.BindToPaper(face, paper);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void BindDevices(Paper paper, params DeviceBase[] devices) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Face face in paper.Faces) {
                    BCFace.BindDevices(face, devices);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UnbindDevices(Paper paper, DeviceBase[] devices) {
            if (devices != null && devices.Length == 0) return;
            DOGlobal.BeginTransaction();
            try {
                foreach (Face face in paper.Faces) {
                    if (devices == null) {
                        BCFace.UnbindDevices(face);
                    } else {
                        BCFace.UnbindDevices(face, devices);
                    }
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        private static void SynchronizeRetailers(Paper paper, Retailer[] newRetailers) {
            if (newRetailers == null) return;
            ArrayList currentRetailerList = new ArrayList(BCRetailers.GetRetailers(paper));
            ArrayList newRetailerList = new ArrayList(newRetailers);
            ArrayList retailersToAdd = new ArrayList();
            ArrayList retailersToRemove = new ArrayList();

            foreach (Retailer retailer in currentRetailerList) {
                if (newRetailerList.IndexOf(retailer) < 0) {
                    retailersToRemove.Add(retailer);
                }
            }
            foreach (Retailer retailer in newRetailerList) {
                if (currentRetailerList.IndexOf(retailer) < 0) {
                    retailersToAdd.Add(retailer);
                }
            }
            AddToRetailer(paper, (Retailer[])retailersToAdd.ToArray(typeof(Retailer)));
            RemoveFromRetailer(paper, (Retailer[])retailersToRemove.ToArray(typeof(Retailer)));
        }

        public static void AddToRetailer(Paper paper, params Retailer[] retailers) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Retailer retailer in retailers) {
                    DOPaper.AddToRetailer(paper, retailer);
                }
                DOGlobal.CommitTransaction();
            } 
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void RemoveFromRetailer(Paper paper, Retailer[] retailers) {
            DOGlobal.BeginTransaction();
            try {
                if (retailers == null) {
                    DOPaper.RemoveFromRetailer(paper, null);
                } 
                else {
                    foreach (Retailer retailer in retailers) {
                        DOPaper.RemoveFromRetailer(paper, retailer);
                    }
                }
                DOGlobal.CommitTransaction();
            } 
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
        public static PaperBase[] CheckPapersForRetailer(PaperBase[] papers,Retailer retailer) {
            PaperToRetailer [] ptrs =  BCPaperToRetailer.GetLinks(retailer);
            ArrayList result = new ArrayList();
            foreach ( PaperBase p in papers) {
                foreach (PaperToRetailer r in ptrs) {
                    if(p.Id == r.PaperId ) {
                        result.Add(p);
                        break;
                    }
                }
                
            }

            return (PaperBase[])result.ToArray(typeof(PaperBase));
        }
    }
}