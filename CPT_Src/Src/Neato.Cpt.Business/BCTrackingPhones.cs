using System;
using System.Data;
using System.Threading;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Business {
    /// <summary>
    /// Summary description for BCTrackingPhones.
    /// </summary>
    public class BCTrackingPhones {
        public BCTrackingPhones() {}

        public static void Add(Device device, string userHostAddress, Retailer retailer) {
            string login = Thread.CurrentPrincipal.Identity.Name;
            Add(device, login, userHostAddress, retailer);
        }

        public static void Add(Device device, string login, string userHostAddress, Retailer retailer) {
            IPAddress userIP = IPAddress.ToIPAddress(userHostAddress);
            if (!BCIPFilter.IsUserContainsInIPFilters(userIP))
                DOTrackingPhones.Add(device, login, BCTimeManager.GetCurrentTime(), retailer);
        }

        public static DataSet GetListByBrandAndDate(DeviceBrand brand, DateTime startTime, DateTime endTime, Retailer retailer) {
            return DOTrackingPhones.GetListByBrandAndDate(brand, startTime, endTime, retailer);
        }

        public static DataSet GetCarriersList(Retailer retailer) {
            return DOTrackingPhones.GetCarriersList(retailer);
        }
    }
}
