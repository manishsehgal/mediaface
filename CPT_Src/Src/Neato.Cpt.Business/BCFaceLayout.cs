using Neato.Cpt.Data;
using Neato.Cpt.Data.Scheme;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Business {
    public class BCFaceLayout {
        public BCFaceLayout() {}

        public static FaceLayoutData EnumerateFaceLayouts(PaperBase paper) {
            return DOFaceLayout.EnumerateFaceLayouts(paper);
        }
    }
}