using System.IO;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.Business {
    public class BCImageLibrary {
        public BCImageLibrary() {}

        public static ImageLibItem[] ImageLibItemEnumByFolderId(int folderId) {
            return DOImageLibrary.ImageLibItemEnumByFolderId(folderId);
        }

        public static byte[] GetImage(int id) {
            return DOImageLibrary.GetImage(id);
        }

        public static byte[] GetImageIcon(int id, int iconWidth, int iconHeight) {
            byte[] image = DOImageLibrary.GetImage(id);
            byte[] icon;
            if (image != null && image.Length > 0) {
                icon = ImageHelper.GetIcon(image, iconWidth, iconHeight);
            } else {
                icon = new byte[0];
            }
            return icon;
        }

        public static Library GetImageLibrary(string culture, Retailer retailer) {
            return DOImageLibrary.GetImageLibrary(culture, retailer);
        }

        public static int TestAddImage(Stream imageData) {
            byte[] buffer = new byte[imageData.Length];
            imageData.Position = 0;
            imageData.Read(buffer, 0, buffer.Length);
            byte[] arrayData = ImageHelper.ConvertImageToJpeg(buffer);
            return DOImageLibrary.TestAddImage(arrayData);
        }

        public static int ImageLibItemIns(int folderId, Stream imageData) {
            byte[] buffer = new byte[imageData.Length];
            imageData.Position = 0;
            imageData.Read(buffer, 0, buffer.Length);
            byte[] arrayData = ImageHelper.ConvertImageToJpeg(buffer);
            return DOImageLibrary.ImageLibItemIns(folderId, arrayData);
        }

        public static void ImageLibItemDel(int itemId) {
            DOImageLibrary.ImageLibItemDel(itemId);
        }

        public static void ImageLibItemsDel(ImageLibItem[] items) {
            DOGlobal.BeginTransaction();
            try {
                DOImageLibrary.ImageLibItemsDel(items);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static int InsertImageLibFolder(
            object parentFolderId, string caption, string culture, Retailer retailer) {
            return DOImageLibrary.InsertImageLibFolder(parentFolderId, caption, culture, retailer);
        }

        public static void UpdateImageLibFolder(
            int folderId, string caption, string culture) {
            DOImageLibrary.UpdateImageLibFolder(folderId, caption, culture);
        }

        public static void DeleteImageLibFolder(int folderId) {
            DOImageLibrary.DeleteImageLibFolder(folderId);
        }

        public static void AddContentRecursively
            (string imagePath, string targetFolderName,
             bool insertIntoRoot, bool clearTargetFolder, Retailer retailer) {
            DOGlobal.BeginTransaction();
            try {
                DOImageLibrary.AddContentRecursively
                    (imagePath, targetFolderName, insertIntoRoot,
                     clearTargetFolder, retailer);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void ImageLibFolderSortOrderSwap
            (int folderId1, int folderId2) {
            DOImageLibrary.ImageLibFolderSortOrderSwap(folderId1, folderId2);
        }

    }
}