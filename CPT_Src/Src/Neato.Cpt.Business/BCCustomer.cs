using System;
using System.Data;
using System.Text;

using Neato.Cpt.Data;
using Neato.Cpt.Data.Scheme;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;

namespace Neato.Cpt.Business {
    public class BCCustomer {
        public BCCustomer() {}

        public static Customer[] CustomerSearch(string firstName, string lastName, string email, DateTime startDate, DateTime endDate, Retailer retailer) {
            return DOCustomer.CustomerSearch(firstName, lastName, email, startDate, endDate, retailer);
        }

        public static Customer GetCustomer(string email, string password, Retailer retailer) {
            Customer customer = DOCustomer.GetCustomerByLogin(email, retailer.Id);
            if (customer != null && customer.Password == password)
                return customer;
            return null;
        }

        public static Customer GetCustomerByLogin(string email, Retailer retailer) {
            return DOCustomer.GetCustomerByLogin(email, retailer.Id);
        }

        public static Customer GetCustomerById(int customerId) {
            return DOCustomer.GetCustomerById(customerId);
        }

        public static void InsertCustomer(Customer customer, LastEditedPaperData data, Retailer retailer) {
            DOGlobal.BeginTransaction();
            try {
                Customer checkCustomer = DOCustomer.GetCustomerByLogin(customer.Email, retailer.Id);
                if (checkCustomer != null)
                    throw new BusinessException(BCCustomerStrings.TextEmailIsNotUnique());

                customer.Created = BCTimeManager.GetCurrentTime();
                customer.RetailerId = retailer.Id;
                DOCustomer.InsertCustomer(customer, data);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UpdateCustomer(Customer customer, Retailer retailer) {
            DOGlobal.BeginTransaction();
            try {
                Customer checkCustomer = DOCustomer.GetCustomerByLogin(customer.Email, retailer.Id);
                if (checkCustomer != null && checkCustomer.CustomerId != customer.CustomerId)
                    throw new BusinessException(BCCustomerStrings.TextEmailIsNotUnique());

                customer.RetailerId = retailer.Id;
                DOCustomer.UpdateCustomer(customer);
                DOGlobal.CommitTransaction();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void DeleteCustomer(int customerId) {
            DOGlobal.BeginTransaction();
            try {
                DOCustomer.DeleteCustomer(customerId);
                DOGlobal.CommitTransaction();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void UpdatePasswordUid(string email, string pswuid, Retailer retailer) {
            Customer customer = DOCustomer.GetCustomerByLogin(email, retailer.Id);
            if (customer == null)
                throw new BusinessException(BCCustomerStrings.TextWrongEmail());

            if (customer.PasswordUid != pswuid)
                throw new BusinessException(BCCustomerStrings.TextDoubleLink());

            customer.Password = GenerateNewPassword();
            customer.PasswordUid = string.Empty;

            UpdateCustomer(customer, retailer);
        }

        private static string GenerateNewPassword() {
            const int passwordLength = 6;
            const string Chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            int maxIndex = Chars.Length - 1;
            Random rnd = new Random(DateTime.Now.Millisecond);
            StringBuilder password = new StringBuilder(6);
            for (int i = 0; i < passwordLength; ++i) {
                password.Append(Chars[rnd.Next(0, maxIndex)]);
            }

            return password.ToString();
        }

        public static void ChangeLastEditedPaper(Customer customer, PaperBase paperBase) {
            if (customer != null) {
                Paper paper = DOPaper.GetPaper(paperBase);
                if (paper != null) {
                    BCLastEditedPaper.ChangeLastEditedPaper(customer, paperBase);
                    customer.LastEditedPapers =
                        DOLastEditedPaper.EnumerateLastEditedPaperByCustomerList(customer);
                }
            }
        }
    }
}