using System.Collections;
using System.Data;
using System.IO;
using System.Threading;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.Business {
    public sealed class BCCarrier {
        private BCCarrier() {}

        public static Carrier[] GetCarrierListByDeviceType(DeviceType deviceType, Retailer retailer) {
            Carrier [] result = EnumCarrierInternal(deviceType, null, "", retailer);
            return retailer == null ? result : SelectCarriersByRetailer(result,retailer,"");
        }

        public static Carrier[] GetCarrierList() {
            return EnumCarrierInternal(DeviceType.Undefined, null, null, null);
        }

        public static Carrier[] GetCarrierList(DeviceBase device, Retailer retailer) {
            Carrier [] result = EnumCarrierInternal(DeviceType.Undefined, device, null, retailer);
            return retailer == null ? result : SelectCarriersByRetailer(result,retailer,null);
        }

        private static Carrier[] EnumCarrierInternal(DeviceType deviceType, DeviceBase device, string paperState, Retailer retailer) {
            string state = null;
            if (paperState != null) {
                state = "a";
                string login = Thread.CurrentPrincipal.Identity.Name;
                SpecialUser[] users = BCSpecialUser.GetSpecialUsers(login, 1);
                if (users.Length > 0) {
                    state = "ait";
                }
            }
            return DOCarrier.EnumerateCarrierByParam(deviceType, device, state, retailer);
        }

        public static byte[] GetCarrierIcon(CarrierBase carrier) {
            return DOCarrier.GetCarrierIcon(carrier);
        }

        public static Carrier NewCarrier() {
            Carrier carrier = new Carrier();
            return carrier;
        }

        public static void Save(Carrier carrier) {
            Save(carrier, null, null);
        }

        public static void Save(Carrier carrier, Stream icon, Retailer[] newRetailers) {
            DOGlobal.BeginTransaction();
            try {
                if (carrier.IsNew) {
                    DOCarrier.Add(carrier);
                } else {
                    DOCarrier.Update(carrier);
                }
                ChangeIcon(carrier, icon);
                SynchronizeRetailers(carrier, newRetailers);
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void ChangeIcon(CarrierBase carrier, Stream icon) {
            if (icon != null && icon.Length > 0) {
                DOGlobal.BeginTransaction();
                try {
                    DOCarrier.UpdateIcon(carrier, ConvertHelper.StreamToByteArray(icon));
                    DOGlobal.CommitTransaction();
                } catch (DBConcurrencyException ex) {
                    DOGlobal.RollbackTransaction();
                    throw new ConcurrencyException(ex.Message, ex);
                } catch {
                    DOGlobal.RollbackTransaction();
                    throw;
                }
            }
        }

        public static void Delete(CarrierBase carrier) {
            if (carrier.IsNew) return;
            Device[] devices = BCDevice.GetDeviceList(carrier);
            DOGlobal.BeginTransaction();
            try {
                RemoveDeviceFromCarrier(carrier, devices);
                DOCarrier.Delete(carrier);
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void RemoveDeviceFromCarrier(CarrierBase carrier, params Device[] devices) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Device device in devices) {
                    DODevice.RemoveFromCarrier(device, carrier);
                }
                DOGlobal.CommitTransaction();
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void AddDeviceToCarrier(CarrierBase carrier, params Device[] devices) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Device device in devices) {
                    DODevice.AddToCarrier(device, carrier);
                }
                DOGlobal.CommitTransaction();
            } catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        private static void SynchronizeRetailers(Carrier carrier, Retailer[] newRetailers) {
            if (newRetailers == null) return;
            ArrayList currentRetailerList = new ArrayList(BCRetailers.GetRetailers(carrier));
            ArrayList newRetailerList = new ArrayList(newRetailers);
            ArrayList retailersToAdd = new ArrayList();
            ArrayList retailersToRemove = new ArrayList();

            foreach (Retailer retailer in currentRetailerList) {
                if (newRetailerList.IndexOf(retailer) < 0) {
                    retailersToRemove.Add(retailer);
                }
            }
            foreach (Retailer retailer in newRetailerList) {
                if (currentRetailerList.IndexOf(retailer) < 0) {
                    retailersToAdd.Add(retailer);
                }
            }
            AddToRetailer(carrier, (Retailer[])retailersToAdd.ToArray(typeof(Retailer)));
            RemoveFromRetailer(carrier, (Retailer[])retailersToRemove.ToArray(typeof(Retailer)));
        }

        public static void AddToRetailer(Carrier carrier, params Retailer[] retailers) {
            DOGlobal.BeginTransaction();
            try {
                foreach (Retailer retailer in retailers) {
                    DOCarrier.AddToRetailer(carrier, retailer);
                }
                DOGlobal.CommitTransaction();
            } 
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void RemoveFromRetailer(Carrier carrier, Retailer[] retailers) {
            DOGlobal.BeginTransaction();
            try {
                if (retailers == null) {
                    DOCarrier.RemoveFromRetailer(carrier, null);
                } 
                else {
                    foreach (Retailer retailer in retailers) {
                        DOCarrier.RemoveFromRetailer(carrier, retailer);
                    }
                }
                DOGlobal.CommitTransaction();
            } 
            catch (DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } 
            catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }
        public static Carrier[] SelectCarriersByRetailer(Carrier [] carriers, Retailer retailer, string paperState) {
            ArrayList carriersList = new ArrayList(carriers);
            ArrayList result = new ArrayList();
            string state = null;
            if (paperState != null) 
            {
                state = "a";
                string login = Thread.CurrentPrincipal.Identity.Name;
                SpecialUser[] users = BCSpecialUser.GetSpecialUsers(login, 1);
                if (users.Length > 0) 
                {
                    state = "ait";
                }
            }
            foreach (Carrier carrier in carriersList){
                if(true == BCRetailers.CheckCarrierForAnyDevices(carrier, retailer,state)) {
                    result.Add(carrier);
                }

            }
            return (Carrier[])result.ToArray(typeof(Carrier));
        }
    }
}
