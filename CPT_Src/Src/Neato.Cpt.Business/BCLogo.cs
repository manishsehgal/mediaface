using System;
using System.Collections;
using System.Data;

using Neato.Cpt.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.Business {
    public sealed class BCLogo {
        private static Hashtable logos;

        private BCLogo() {}

        public static byte[] GetLogo(int retailerId, bool hasMenu) {
            if (retailerId == BCRetailers.defaultId)
                return GetLogo(retailerId);

            if (hasMenu)
                return GetLogo(retailerId);

            return null;
        }

        private static byte[] GetLogo(int retailerId) {
            if (logos == null)
                logos = DOLogo.Enum();

            if (logos.ContainsKey(retailerId)) {
                Logo logo = logos[retailerId] as Logo;
                if (logo != null)
                    return logo.LogoImg;
            }

            return null;
        }

        public static byte[] GetFlashLogo(int retailerId) {
            if (retailerId != BCRetailers.defaultId)
                return new byte[0];

            if (logos == null)
                logos = DOLogo.Enum();

            if (logos.ContainsKey(retailerId)) {
                Logo logo = logos[retailerId] as Logo;
                if (logo != null)
                    return logo.FlashLogoImg;
            }

            return null;
        }

        public static void Update(int retailerId, byte[] logo) {
            DOGlobal.BeginTransaction();
            try {
                byte[] flashLogo = ImageHelper.ConvertImageToJpeg(logo);
                Object obj = GetLogo(retailerId);
                if (obj != null)
                    DOLogo.Update(retailerId, logo, flashLogo);
                else
                    DOLogo.Insert(retailerId, logo, flashLogo);
                DOGlobal.CommitTransaction();
                Reset();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Delete(int retailerId) {
            Object obj = GetLogo(retailerId);
            if (obj == null)
                return;

            DOGlobal.BeginTransaction();
            try {
                DOLogo.Delete(retailerId);
                DOGlobal.CommitTransaction();
                Reset();
            } catch(DBConcurrencyException ex) {
                DOGlobal.RollbackTransaction();
                throw new ConcurrencyException(ex.Message, ex);
            } catch {
                DOGlobal.RollbackTransaction();
                throw;
            }
        }

        public static void Reset() {
            logos = null;
        }
    }
}