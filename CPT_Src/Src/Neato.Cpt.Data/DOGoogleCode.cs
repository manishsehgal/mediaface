using System.Data;

using Neato.Cpt.Data.DbProcedures;

namespace Neato.Cpt.Data {
    public sealed class DOGoogleCode {
        private DOGoogleCode() {}

        public static string GetCode(int id) {
            PrGoogleCodeGetById prc = new PrGoogleCodeGetById();
            prc.Id = id;
            
            string code = null;
            using (IDataReader reader = prc.ExecuteReader()) {
                int CodeIndex = reader.GetOrdinal("Code");
                if (reader.Read())
                    code = reader.GetString(CodeIndex);
            }

            return code;
        }

        public static void Update(int id, string code) {
            PrGoogleCodeUpd prc = new PrGoogleCodeUpd();
            prc.Id = id;
            prc.Code = code;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Google code does not exist.");
        }
    }
}