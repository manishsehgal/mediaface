using System;
using System.Collections;
using System.Data;
using Neato.Cpt.Entity;
using Neato.Cpt.Data.DbProcedures;

namespace Neato.Cpt.Data {
	public class DOSpecialUser {

		public DOSpecialUser() {}

        public static SpecialUser[] EnumerateSpecialUserByParam(string email, int showPaper, int notifyPaperState) {
            PrSpecialUserEnumByParam prc = new PrSpecialUserEnumByParam();

            if (email != null) {
                prc.Email = email.ToLower();
            }

            if (showPaper >= 0) {
                prc.ShowPaper = showPaper > 0 ? true : false;
            }

            if (notifyPaperState > 0) {
                prc.NotifyPaperState = notifyPaperState > 0 ? true : false;
            }

            ArrayList users = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int emailColumnIndex = reader.GetOrdinal("Email");
                int showPaperColumnIndex = reader.GetOrdinal("ShowPaper");
                int notifyPaperStateColumnIndex = reader.GetOrdinal("NotifyPaperState");

                while (reader.Read()) {
                    SpecialUser user = new SpecialUser();
                    user.Id = reader.GetInt32(idColumnIndex);
                    user.Email = reader.GetString(emailColumnIndex);
                    user.ShowPaper = reader.GetBoolean(showPaperColumnIndex);
                    user.NotifyPaperState = reader.GetBoolean(notifyPaperStateColumnIndex);
                    users.Add(user);
                }
            }
            return (SpecialUser[])users.ToArray(typeof(SpecialUser));
        }

        public static SpecialUser GetSpecialUserById(SpecialUserBase userBase) {
            PrSpecialUserGetById prc = new PrSpecialUserGetById();

            if (userBase != null) {
                prc.SpecialUserId = userBase.Id;
            }

            SpecialUser user = null;
            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int emailColumnIndex = reader.GetOrdinal("Email");
                int showPaperColumnIndex = reader.GetOrdinal("ShowPaper");
                int notifyPaperStateColumnIndex = reader.GetOrdinal("NotifyPaperState");
                if (reader.Read()) {
                    user = new SpecialUser();
                    user.Id = reader.GetInt32(idColumnIndex);
                    user.Email = reader.GetString(emailColumnIndex);
                    user.ShowPaper = reader.GetBoolean(showPaperColumnIndex);
                    user.NotifyPaperState = reader.GetBoolean(notifyPaperStateColumnIndex);
                }
            }
            return user;
        }

        public static void Add(SpecialUser user) {
            if (!user.IsNew) throw new InvalidOperationException("Adding SpecialUser entity in 'Non-New' status");

            PrSpecialUserIns prc = new PrSpecialUserIns();
            prc.Email = user.Email.ToLower();
            prc.ShowPaper = user.ShowPaper;
            prc.NotifyPaperState = user.NotifyPaperState;

            prc.ExecuteNonQuery();
            user.Id = prc.SpecialUserId.Value;
        }

        public static void Update(SpecialUser user) {
            if (user.IsNew) throw new InvalidOperationException("Updating SpecialUser entity in 'New' status");

            PrSpecialUserUpd prc = new PrSpecialUserUpd();
            prc.SpecialUserId = user.Id;
            prc.Email = user.Email.ToLower();
            prc.ShowPaper = user.ShowPaper;
            prc.NotifyPaperState = user.NotifyPaperState;

            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("SpecialUser does not exist.");
        }

        public static void Delete(SpecialUserBase user) {
            PrSpecialUserDel prc = new PrSpecialUserDel();
            prc.SpecialUserId = user.Id;
            prc.ExecuteNonQuery();
        }
	}
}
