using System.Collections;
using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data {
    public sealed class DOTermsOfUsePage {
        private DOTermsOfUsePage() {}

        public static LocalizedText[] Enum(string culture, Retailer retailer) {
            PrTermsOfUsePageEnumByCulture prc = new PrTermsOfUsePageEnumByCulture();
            prc.Culture = culture;
            prc.RetailerId = retailer.Id;

            ArrayList list = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int CultureIndex = reader.GetOrdinal("Culture");
                int HtmlTextIndex = reader.GetOrdinal("HtmlText");
                while (reader.Read()) {
                    LocalizedText localizedText = new LocalizedText();
                    localizedText.Culture = reader.GetString(CultureIndex);
                    localizedText.Text = reader.GetString(HtmlTextIndex);
                    list.Add(localizedText);
                }
            }
            return (LocalizedText[])list.ToArray(typeof(LocalizedText));
        }

        public static void Insert(string culture, string htmlText, Retailer retailer) {
            PrTermsOfUsePageIns prc = new PrTermsOfUsePageIns();
            prc.Culture = culture;
            prc.HtmlText = htmlText;
            prc.RetailerId = retailer.Id;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Terms Of Use does not exist.");
        }

        public static void Update(string culture, string htmlText, Retailer retailer) {
            PrTermsOfUsePageUpd prc = new PrTermsOfUsePageUpd();
            prc.Culture = culture;
            prc.HtmlText = htmlText;
            prc.RetailerId = retailer.Id;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Terms Of Use does not exist.");
        }
    }
}