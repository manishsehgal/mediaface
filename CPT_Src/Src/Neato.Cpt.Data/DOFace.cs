using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.Data {
    public class DOFace {
        public DOFace() {}

        public static Face GetFace(FaceBase faceBase) {
            PrFaceGetById prc = new PrFaceGetById();
            prc.FaceId = faceBase.Id;
            Face face = null;
            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int contourColumnIndex = reader.GetOrdinal("Contour");
                if (reader.Read()) {
                    face = new Face();
                    face.Id = reader.GetInt32(idColumnIndex);
                    object contourValue = reader.GetValue(contourColumnIndex);
                    if (DBNull.Value != contourValue) {
                        face.Contour = ConvertHelper.ByteArrayToXmlNode((byte[])contourValue);
                    }
                }
            }
            return face;
        }

        public static void Add(Face face) {
            if (!face.IsNew) throw new InvalidOperationException("Adding Face entity in 'Non-New' status");

            PrFaceIns prc = new PrFaceIns();
            if (face.Contour != null) {
                prc.Contour = ConvertHelper.XmlNodeToByteArray(face.Contour);
            }

            prc.ExecuteNonQuery();
            face.Id = prc.FaceId.Value;

            IDictionaryEnumerator names = face.GetNames();
            names.Reset();
            while (names.MoveNext()) {
                string culture = (string)names.Key;
                string faceName = (string)names.Value;
                AddName(face, culture, faceName);
            }
        }

        private static void AddName(FaceBase face, string culture, string faceName) {
            PrFaceLocalizationIns prc = new PrFaceLocalizationIns();
            prc.FaceId = face.Id;
            prc.Culture = culture;
            prc.FaceName = faceName;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Face name already exists.");
        }

        private static void DeleteName(FaceBase face, string culture) {
            PrFaceLocalizationDel prc = new PrFaceLocalizationDel();
            prc.FaceId = face.Id;
            if (culture != null) {
                prc.Culture = culture;
            }
            prc.ExecuteNonQuery();
        }

        public static void Update(Face face) {
            if (face.IsNew) throw new InvalidOperationException("Updating Face entity in 'New' status");
            PrFaceUpd prc = new PrFaceUpd();
            prc.FaceId = face.Id;

            if (face.Contour != null) {
                prc.Contour = ConvertHelper.XmlNodeToByteArray(face.Contour);
            }

            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Face does not exist.");

            DeleteName(face, null);
            IDictionaryEnumerator names = face.GetNames();
            names.Reset();
            while (names.MoveNext()) {
                string culture = (string)names.Key;
                string faceName = (string)names.Value;
                AddName(face, culture, faceName);
            }
        }

        public static void Delete(FaceBase face) {
            DeleteName(face, null);
            PrFaceDel prc = new PrFaceDel();
            prc.FaceId = face.Id;
            prc.ExecuteNonQuery();
        }

        public static void BindToPaper(Face face, PaperBase paper) {
            PrFaceBindToPaper prc = new PrFaceBindToPaper();
            prc.FaceId = face.Id;
            prc.PaperId = paper.Id;
            prc.FaceX = face.Position.X;
            prc.FaceY = face.Position.Y;
            prc.Rotation = face.Rotation;

            prc.ExecuteNonQuery();
        }

        public static void UnbindFromPaper(FaceBase face, PaperBase paper) {
            PrFaceUnbindFromPaper prc = new PrFaceUnbindFromPaper();
            if (face != null)
                prc.FaceId = face.Id;
            if (paper != null)
                prc.PaperId = paper.Id;
            prc.ExecuteNonQuery();
        }

        public static void BindToDevice(FaceBase face, DeviceBase device) {
            PrFaceBindToDevice prc = new PrFaceBindToDevice();
            prc.FaceId = face.Id;
            prc.DeviceId = device.Id;
            prc.ExecuteNonQuery();
        }

        public static void UnbindFromDevice(FaceBase face, DeviceBase device) {
            PrFaceUnbindFromDevice prc = new PrFaceUnbindFromDevice();
            if (face != null)
                prc.FaceId = face.Id;
            if (device != null)
                prc.DeviceId = device.Id;
            prc.ExecuteNonQuery();
        }
    }
}