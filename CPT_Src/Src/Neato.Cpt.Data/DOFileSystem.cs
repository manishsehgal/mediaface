using System;
using System.IO;

namespace Neato.Cpt.Data {
    public class DOFileSystem {
        public static string[] GetFiles(string dirPath) {
            return Directory.GetFiles(dirPath);
        }

        public static string[] GetDirectories(string path) {
            return Directory.GetDirectories(path);
        }

        public static string GetFolderNameFromPath(string path) {
            string[] pathItems = path.Split('\\');
            return pathItems[pathItems.Length - 1];
        }

        public static byte[] ReadFile(string filePath) {
            byte[] data = null;
            try {
                using (FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.None)) {
                    int length = (int)stream.Length;
                    data = new byte[length];
                    stream.Read(data, 0, length);
                }
            }
            catch(Exception ex) {
                throw ex;
            }
            return data;
        }

        public static void WriteFile(byte[] file, string filePath) {
            try {
                using (FileStream stream = File.Open(filePath, FileMode.Create, FileAccess.Write, FileShare.None)) {
                    stream.Write(file, 0, file.Length);
                }
            } catch (Exception ex) {
                throw ex;
            }
        }

        public static bool DirectoryExist(string path) {
            return Directory.Exists(path);
        }

        public static bool FileExist(string path) {
            return File.Exists(path);
        }

        public static DirectoryInfo CreateDirectory(string path) {
            return Directory.CreateDirectory(path);
        }

        public static void DeleteFile(string path) {
            try {
                File.Delete(path);
            } 
            catch (Exception ex) {
                throw ex;
            }
        }

        public static void UnsetReadOnlyAttribute(string path) {
            FileAttributes fileAttributes = File.GetAttributes(path);
            if ((fileAttributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) {
                File.SetAttributes(path, fileAttributes &= ~FileAttributes.ReadOnly);
            }
        }

    }
}