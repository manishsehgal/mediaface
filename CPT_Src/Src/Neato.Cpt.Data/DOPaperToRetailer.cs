using System;
using System.Collections;
using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data
{
    public class DOPaperToRetailer
    {
        public DOPaperToRetailer(){}
        public static PaperToRetailer[] GetLinks(Retailer retailer) 
        {
            PrPaperToRetailerEnum prc = new PrPaperToRetailerEnum();
            if(retailer != null)
                prc.RetailerId = retailer.Id;
            ArrayList result = new ArrayList();
            
            using (IDataReader reader = prc.ExecuteReader()) 
            {
                int paperIdColumnIndex = reader.GetOrdinal("PaperId");
                int retailerIdColumnIndex = reader.GetOrdinal("RetailerId");
                while (reader.Read()) 
                {
                    PaperToRetailer ptr = new PaperToRetailer();
                    ptr.PaperId = reader.GetInt32(paperIdColumnIndex);
                    ptr.RetailerId = reader.GetInt32(retailerIdColumnIndex);
                    result.Add(ptr);
                }
            }
            
            return (PaperToRetailer[])result.ToArray(typeof(PaperToRetailer));
        }
    }
}
