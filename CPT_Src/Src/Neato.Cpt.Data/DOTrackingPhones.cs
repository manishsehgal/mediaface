using System;
using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data {
    public class DOTrackingPhones {
        public DOTrackingPhones() {}

        public static int Add(Device device, string login, DateTime time, Retailer retailer) {
            PrTrackingDevicesIns prc = new PrTrackingDevicesIns();
            prc.Device = device.Model;
            prc.DeviceBrand = device.Brand.Name;
            prc.Time = time;
            prc.Login = login;
            if(retailer != null)prc.RetailerId = retailer.Id;
            prc.ExecuteNonQuery();
            return prc.Id.Value;
        }

        public static DataSet GetListByBrandAndDate(DeviceBrand brand, DateTime startTime, DateTime endTime, Retailer retailer) {
            PrTrackingDevicesEnumByTimeAndBrand prc = new PrTrackingDevicesEnumByTimeAndBrand();
            prc.DeviceBrand = brand == null ? null : brand.Name;
            prc.TimeEnd = endTime;
            prc.TimeStart = startTime;
            if(retailer != null)prc.RetailerId = retailer.Id;
            DataSet data = new DataSet();
            data.Tables.Add();
            return prc.LoadDataSet(data);
        }

        public static DataSet GetCarriersList(Retailer retailer) {
            PrTrackingDevicesEnumBrands prc = new PrTrackingDevicesEnumBrands();
            if(retailer != null)prc.RetailerId = retailer.Id;
            DataSet data = new DataSet();
            data.Tables.Add();
            return prc.LoadDataSet(data);
        }
    }
}