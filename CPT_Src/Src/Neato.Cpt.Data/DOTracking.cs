using System;
using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Tracking;

namespace Neato.Cpt.Data {
    public class DOTracking {
        public DOTracking() {}

        public static int Add(UserAction action, string login, DateTime time, Retailer retailer) {
            PrTrackingUserActionIns prc = new PrTrackingUserActionIns();
            prc.Login = login;
            prc.Time = time;
            prc.Action = action.ToString();
            if(retailer != null)prc.RetailerId = retailer.Id;
            prc.ExecuteNonQuery();
            return prc.Id.Value;
        }

        public static DataSet GetList(Retailer retailer) {
            PrTrackingUserActionEnum prc = new PrTrackingUserActionEnum();
            if(retailer != null)prc.RetailerId = retailer.Id;
            DataSet data = new DataSet();
            data.Tables.Add();
            return prc.LoadDataSet(data);
        }
    }
}