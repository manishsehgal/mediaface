using System;
using System.Xml;
using System.IO;

namespace Neato.Cpt.Data {
	public class DOPaintTypes {
		public static XmlNode GetPaintTypes(string applicationRoot) {
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load(Path.Combine(applicationRoot, "PaintTypes.xml"));
			return xmlDoc.DocumentElement;
		}
	}
}
