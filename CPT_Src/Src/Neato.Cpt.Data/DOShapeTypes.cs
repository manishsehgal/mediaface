using System;
using System.Xml;
using System.IO;

namespace Neato.Cpt.Data {
	public class DOShapeTypes {
		public static XmlNode GetShapeTypes(string applicationRoot) {
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load(Path.Combine(applicationRoot, "ShapeTypes.xml"));
			return xmlDoc.DocumentElement;
		}
	}
}
