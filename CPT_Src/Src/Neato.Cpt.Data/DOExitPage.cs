using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data {
    public sealed class DOExitPage {
        private DOExitPage() {}

        public static ExitPage Get(int retailerId) {
            PrExitPageGet prc = new PrExitPageGet();
            prc.RetailerId = retailerId;

            ExitPage exitPage = null;

            using (IDataReader reader = prc.ExecuteReader()) {
                int HtmlTextIndex = reader.GetOrdinal("HtmlText");
                int EnabledIndex = reader.GetOrdinal("Enabled");
                if (reader.Read()) {
                    exitPage = new ExitPage();
                    exitPage.RetailerId = retailerId;
                    exitPage.HtmlText = reader.GetString(HtmlTextIndex);
                    exitPage.Enabled = reader.GetBoolean(EnabledIndex);
                }
            }

            return exitPage;
        }

        public static void Insert(ExitPage exitPage) {
            PrExitPageIns prc = new PrExitPageIns();
            prc.RetailerId = exitPage.RetailerId;
            prc.HtmlText = exitPage.HtmlText;
            prc.Enabled = exitPage.Enabled;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Exit page does not exist.");

        }

        public static void Update(ExitPage exitPage) {
            PrExitPageUpd prc = new PrExitPageUpd();
            prc.RetailerId = exitPage.RetailerId;
            prc.HtmlText = exitPage.HtmlText;
            prc.Enabled = exitPage.Enabled;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Exit page does not exist.");

        }
    }
}