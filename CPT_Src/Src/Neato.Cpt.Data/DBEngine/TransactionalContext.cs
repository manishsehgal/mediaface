using System;
using System.Collections;
using System.Data;
using System.Diagnostics;

using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Neato.Cpt.Data.DBEngine {
    internal class TransactionalContext {
        [ThreadStatic]
        private static Hashtable activeTransactionsValue;

        private static Hashtable activeTransactions {
            get {
                if (null == activeTransactionsValue) {
                    activeTransactionsValue = new Hashtable();
                }
                return activeTransactionsValue;
            }
        }

        private class ThreadTransaction {
            public readonly IDbTransaction Transaction;
            public int TransactionNestingLevel;

            public ThreadTransaction(IDbTransaction transaction) {
                this.Transaction = transaction;
                TransactionNestingLevel = 1;
            }
        }

        internal static void RollbackAllActiveTransactions() {
            foreach (ThreadTransaction transaction in activeTransactions.Values) {
                Debug.Assert(transaction.TransactionNestingLevel > 0);
                IDbConnection connection = transaction.Transaction.Connection;
                transaction.Transaction.Rollback();
                transaction.Transaction.Dispose();
                connection.Dispose();
            }
            activeTransactions.Clear();
        }

        internal static IDbTransaction GetTransaction(string databaseInstanceName) {
            Database db = DatabaseManager.GetDatabase(databaseInstanceName);
            ThreadTransaction currentTransaction = (ThreadTransaction)activeTransactions[db.ConfigurationName];
            Debug.Assert(null == currentTransaction || null != currentTransaction.Transaction);
            return (null == currentTransaction) ? null : currentTransaction.Transaction;
        }

        internal static void BeginTransaction(string databaseInstanceName, IsolationLevel level) {
            Database db = DatabaseManager.GetDatabase(databaseInstanceName);
            ThreadTransaction currentTransaction = (ThreadTransaction)activeTransactions[db.ConfigurationName];
            if (null == currentTransaction) {
                IDbConnection connection = db.GetConnection();
                connection.Open();
                currentTransaction = new ThreadTransaction(connection.BeginTransaction(level));
                activeTransactions[db.ConfigurationName] = currentTransaction;
            } else {
                currentTransaction.TransactionNestingLevel++;
            }
        }

        internal static void CommitTransaction(string databaseInstanceName) {
            Database db = DatabaseManager.GetDatabase(databaseInstanceName);
            ThreadTransaction currentTransaction = (ThreadTransaction)activeTransactions[db.ConfigurationName];
            if (null == currentTransaction) {
                throw new DAException("No active transaction");
            }
            Debug.Assert(currentTransaction.TransactionNestingLevel > 0);
            currentTransaction.TransactionNestingLevel--;
            if (0 == currentTransaction.TransactionNestingLevel) {
                IDbConnection connection = currentTransaction.Transaction.Connection;
                currentTransaction.Transaction.Commit();
                currentTransaction.Transaction.Dispose();
                connection.Dispose();
                activeTransactions.Remove(db.ConfigurationName);
            }
        }

        internal static void RollbackTransaction(string databaseInstanceName) {
            Database db = DatabaseManager.GetDatabase(databaseInstanceName);
            ThreadTransaction currentTransaction = (ThreadTransaction)activeTransactions[db.ConfigurationName];
            if (null == currentTransaction) {
                throw new DAException("No active transaction");
            }
            Debug.Assert(currentTransaction.TransactionNestingLevel > 0);
            IDbConnection connection = currentTransaction.Transaction.Connection;
            currentTransaction.Transaction.Rollback();
            currentTransaction.Transaction.Dispose();
            connection.Dispose();
            activeTransactions.Remove(db.ConfigurationName);
        }
    }
}