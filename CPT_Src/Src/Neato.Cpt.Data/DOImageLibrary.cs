using System;
using System.Collections;
using System.Data;
using System.Globalization;
using System.Xml;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Data.Scheme;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.Data {
    public class DOImageLibrary {
        public DOImageLibrary() {}

        public static ImageLibItem[] ImageLibItemEnumByFolderId(int folderId) {
            PrImageLibItemEnumByFolderId prc = new PrImageLibItemEnumByFolderId();
            prc.FolderId = folderId;

            DataSet ds = prc.ExecuteDataSet();
            ArrayList list = new ArrayList();

            foreach (DataRow row in ds.Tables[0].Rows) {
                list.Add(new ImageLibItem((int)row.ItemArray[0]));
            }

            return (ImageLibItem[])list.ToArray(typeof(ImageLibItem));
        }

        public static byte[] GetImage(int id) {
            PrImageLibItemGetById prc = new PrImageLibItemGetById();
            prc.ItemId = id;
            object val;
            using (IDataReader reader = prc.ExecuteReader()) {
                int StreamColumnIndex = reader.GetOrdinal("Stream");
                if (reader.Read()) {
                    val = reader.GetValue(StreamColumnIndex);
                } else {
                    val = new byte[0];
                }
            }
            return (byte[])val;
        }

        public static ImageLibraryData EnumerateData(Retailer retailer) {
            PrImageLibEnum prc = new PrImageLibEnum();
            prc.RetailerId = retailer.Id;
            return (ImageLibraryData)prc.LoadDataSet(new ImageLibraryData());
        }

        public static Library GetImageLibrary(string culture, Retailer retailer) {
            ImageLibraryData dbData = EnumerateData(retailer);
            XmlDocument doc = new XmlDocument();
            XmlNode documentNode = doc.CreateElement("Library");
            foreach (ImageLibraryData.FolderRow folder in dbData.Folder) {
                if (folder.IsParentFolderIdNull()) {
                    CreateFolder(doc, documentNode, folder, dbData, culture);
                }
            }
            doc.AppendChild(documentNode);

            Library library = new Library();
            library.XmlData = doc;
            return library;
        }

        private static void CreateFolder(XmlDocument doc, XmlNode parentNode, ImageLibraryData.FolderRow folder, ImageLibraryData dbData, string culture) {
            XmlNode folderNode = doc.CreateElement("folder");

            XmlAttribute folderIdAttr = doc.CreateAttribute("id");
            folderIdAttr.Value = folder.FolderId.ToString(CultureInfo.InvariantCulture);
            folderNode.Attributes.Append(folderIdAttr);

            XmlAttribute folderNameAttr = doc.CreateAttribute("name");
            DataRow[] rows = dbData.FolderLocalization.Select("FolderId = " + folder.FolderId);
            folderNameAttr.Value = LocalizationHelper.GetString(culture, rows, "Culture", "Caption");
            folderNode.Attributes.Append(folderNameAttr);

            foreach (ImageLibraryData.FolderRow child in folder.GetFolderRows()) {
                CreateFolder(doc, folderNode, child, dbData, culture);
            }

            foreach (ImageLibraryData.FolderItemRow folderItem in folder.GetFolderItemRows()) {
                XmlNode itemNode = doc.CreateElement("item");

                XmlAttribute itemId = doc.CreateAttribute("id");
                itemId.Value = folderItem.ItemId.ToString(CultureInfo.InvariantCulture);
                itemNode.Attributes.Append(itemId);

                folderNode.AppendChild(itemNode);
            }

            parentNode.AppendChild(folderNode);
        }

        public static int TestAddImage(byte[] imageData) {
            PrTestImageLibItemIns prc = new PrTestImageLibItemIns();
            prc.Stream = imageData;
            prc.ExecuteNonQuery();
            return prc.Id.Value;
        }

        public static int ImageLibItemIns(int folderId, byte[] imageData) {
            if (imageData.Length == 0) {
                throw new BusinessException("Wrong file format or corrupted file");
            }
            PrImageLibItemIns prc = new PrImageLibItemIns();
            prc.FolderId = folderId;
            prc.Stream = imageData;
            prc.ExecuteNonQuery();
            return prc.ItemId.Value;
        }

        public static void ImageLibItemDel(int itemId) {
            PrImageLibItemDel prc = new PrImageLibItemDel();
            prc.ItemId = itemId;
            prc.ExecuteNonQuery();
        }

        public static void ImageLibItemsDel(ImageLibItem[] items) {
            foreach (ImageLibItem item in items) {
                PrImageLibItemDel prc = new PrImageLibItemDel();
                prc.ItemId = item.Id;
                prc.ExecuteNonQuery();
            }
        }

        public static int InsertImageLibFolder(
            object parentFolderId, string caption, string culture, Retailer retailer) {
            PrImageLibFolderIns prc = new PrImageLibFolderIns();
            if (parentFolderId != null) {
                prc.ParentFolderId = (int)parentFolderId;
            }
            prc.Caption = caption;
            prc.Culture = culture;
            prc.RetailerId = retailer.Id;
            prc.ExecuteNonQuery();

            return (int)prc.FolderId;
        }

        public static void UpdateImageLibFolder(
            int folderId, string caption, string culture) {
            PrImageLibFolderLocalizationUpd prc = new PrImageLibFolderLocalizationUpd();
            prc.FolderId = folderId;
            prc.Caption = caption;
            prc.Culture = culture;
            prc.ExecuteNonQuery();
        }

        public static void DeleteImageLibFolder(int folderId) {
            PrImageLibFolderDel prc = new PrImageLibFolderDel();
            prc.FolderId = folderId;
            prc.ExecuteNonQuery();
        }

        public static void UpdateImageLibrary(ImageLibraryData data, bool insertParentFolder) {
            PrImageLibFolderDel prcFolderDel = new PrImageLibFolderDel();
            PrImageLibFolderIns prcFolderIns = new PrImageLibFolderIns();
            PrImageLibItemDel prcItemDel = new PrImageLibItemDel();
            PrImageLibItemIns prcItemIns = new PrImageLibItemIns();

            //del
            //...del child
            foreach (ImageLibraryData.FolderItemRow item in
                data.FolderItem.Select
                    (null, null, DataViewRowState.Deleted)) {
                prcItemDel.ItemId = (int)item["ItemId", DataRowVersion.Original];
                prcItemDel.ExecuteNonQuery();
            }
            //...del parent
            foreach (ImageLibraryData.FolderRow folder in
                data.Folder.Select
                    (null, null, DataViewRowState.Deleted)) {
                prcFolderDel.FolderId = (int)folder["FolderId", DataRowVersion.Original];
                prcFolderDel.ExecuteNonQuery();
            }

            //ins
            //...ins parent
            foreach (ImageLibraryData.FolderRow folder in
                data.Folder.Select
                    (null, null, DataViewRowState.Added)) {
                if (!insertParentFolder &&
                    folder.IsParentFolderIdNull()) continue;

                if (!folder.IsParentFolderIdNull()) {
                    prcFolderIns.ParentFolderId =
                        folder.ParentFolderId;
                }

                ImageLibraryData.FolderLocalizationRow
                    folderLocalization =
                        folder.GetFolderLocalizationRows()[0];

                prcFolderIns.Caption = folderLocalization.Caption;
                prcFolderIns.Culture = folderLocalization.Culture;
                prcFolderIns.RetailerId = folder.RetailerId;

                prcFolderIns.ExecuteNonQuery();

                folder.FolderId = (int)prcFolderIns.FolderId;
            }
            //...ins child
            foreach (ImageLibraryData.LibraryItemRow item in
                data.LibraryItem.Select
                    (null, null, DataViewRowState.Added)) {
                ImageLibraryData.FolderItemRow
                    folderItem = item.GetFolderItemRows()[0];

                prcItemIns.FolderId = folderItem.FolderId;
                prcItemIns.Stream = item.Stream;

                prcItemIns.ExecuteNonQuery();

                item.ItemId = (int)prcItemIns.ItemId;
            }
        }

        public static int GetImageLibFolderIdByCaption(string folderCaption, Retailer retailer) {
            PrImageLibFolderGetIdByCaption prc = new PrImageLibFolderGetIdByCaption();
            prc.Caption = folderCaption;
            prc.Culture = string.Empty;
            prc.RetailerId = retailer.Id;

            object result = prc.ExecuteScalar();

            return (result == null)
                ? int.MinValue : Convert.ToInt32(result);
        }

        public static void DeleteImageLibFolderRecursively(int folderId, bool deleteParentFolder, Retailer retailer) {
            ImageLibraryData data = GetFolderContent(folderId, retailer);

            ImageLibraryData.FolderRow parentFolder =
                data.Folder.FindByFolderId(folderId);

            if (deleteParentFolder) {
                parentFolder.Delete();
            } else {
                ImageLibraryData.FolderRow[] childFolders =
                    parentFolder.GetFolderRows();
                for (int i = childFolders.Length - 1; i >= 0; i--) {
                    childFolders[i].Delete();
                }

                ImageLibraryData.FolderItemRow[] childItems =
                    parentFolder.GetFolderItemRows();
                for (int i = childItems.Length - 1; i >= 0; i--) {
                    childItems[i].Delete();
                }
            }

            UpdateImageLibrary(data, true);
        }

        public static bool IsFolderRecursiveParent
            (int parentFolderId, int childFolderId, ImageLibraryData data) {
            ImageLibraryData.FolderRow child =
                data.Folder.FindByFolderId(childFolderId);

            if (child.IsParentFolderIdNull()) return false;
            if (!child.IsParentFolderIdNull() &&
                child.ParentFolderId == parentFolderId) return true;
            if (!child.IsParentFolderIdNull() &&
                child.ParentFolderId != parentFolderId)
                return IsFolderRecursiveParent
                    (parentFolderId, child.ParentFolderId, data);

            return false;
        }

        public static ImageLibraryData GetFolderContent(int folderId, Retailer retailer) {
            //Get all ImageLibrary Data and copy her
            ImageLibraryData allData = EnumerateData(retailer);
            ImageLibraryData allDataCopy =
                (ImageLibraryData)allData.Copy();

            //Get not Related Folders using:
            // - a) deleting parent folder...
            // - b) ... and detect other (not deleted) folders
            ArrayList notRelatedFolderIDs = new ArrayList();

            ImageLibraryData.FolderRow parentFolder =
                allData.Folder.FindByFolderId(folderId);

            ImageLibraryData.FolderRow[] childFolders =
                parentFolder.GetFolderRows();
            for (int i = childFolders.Length - 1; i >= 0; i--) {
                childFolders[i].Delete();
            }

            foreach (ImageLibraryData.FolderRow folder in
                allData.Folder.Rows) {
                if (folder.RowState != DataRowState.Deleted &&
                    folder.FolderId != folderId &&
                    !IsFolderRecursiveParent
                        (folder.FolderId, folderId, allData)) {
                    notRelatedFolderIDs.Add(folder.FolderId);
                }
            }

            parentFolder.Delete();

            //Delete not Related Folders
            foreach (int notRelatedFolderId in notRelatedFolderIDs) {
                ImageLibraryData.FolderRow notRelatedFolder =
                    allDataCopy.Folder.FindByFolderId
                        (notRelatedFolderId);

                if (notRelatedFolder != null) {
                    allDataCopy.Folder.RemoveFolderRow
                        (notRelatedFolder);
                }
            }

            //Delete content for not Related Folders
            for (int i = allDataCopy.LibraryItem.Count - 1; i >= 0; i--) {
                if (allDataCopy.LibraryItem[i].GetFolderItemRows().Length == 0) {
                    allDataCopy.LibraryItem.Rows.RemoveAt(i);
                }
            }

            return allDataCopy;
        }

        public static void AddContentRecursively
            (string imagePath, string targetFolderCaption,
             bool insertIntoRoot, bool clearTargetFolder, Retailer retailer) {
            ImageLibraryData newData = GetImageLibraryData(imagePath, retailer);

            int targetFolderId =
                GetImageLibFolderIdByCaption(targetFolderCaption, retailer);

            if (clearTargetFolder && targetFolderId != int.MinValue) {
                DeleteImageLibFolderRecursively
                    (targetFolderId, insertIntoRoot, retailer);
            }

            if (!insertIntoRoot) {
                newData.Folder[0].FolderId = targetFolderId;
            }

            UpdateImageLibrary(newData, insertIntoRoot);
        }

        public static ImageLibraryData GetImageLibraryData
            (string imagePath, Retailer retailer) {
            ImageLibraryData data = new ImageLibraryData();
            CreateFolderContent(imagePath, data, null, retailer);
            return data;
        }

        private static void CreateFolderContent(string folderPath, ImageLibraryData data, ImageLibraryData.FolderRow parentFolder, Retailer retailer) {
            ImageLibraryData.FolderRow folder = null;

            if (parentFolder == null) {
                folder = data.Folder.NewFolderRow();
                folder.RetailerId = retailer.Id;
                data.Folder.AddFolderRow(folder);
            } else {
                folder = data.Folder.AddFolderRow(parentFolder, "", retailer.Id, 0);
            }

            data.FolderLocalization.AddFolderLocalizationRow(
                folder, "", DOFileSystem.GetFolderNameFromPath(folderPath));

            foreach (string fileName in DOFileSystem.GetFiles(folderPath)) {
                byte[] buffer = DOFileSystem.ReadFile(fileName);
                ImageLibraryData.LibraryItemRow item =
                    data.LibraryItem.AddLibraryItemRow(
                        ImageHelper.ConvertImageToJpeg(buffer));
                data.FolderItem.AddFolderItemRow(folder, item);
            }

            foreach (string dirName in DOFileSystem.GetDirectories(folderPath)) {
                CreateFolderContent(dirName, data, folder, retailer);
            }
        }
        public static void ImageLibFolderSortOrderSwap
            (int folderId1, int folderId2) {
            PrImageLibFolderSortOrderSwap prc = new PrImageLibFolderSortOrderSwap();
            prc.FolderId1 = folderId1;
            prc.FolderId2 = folderId2;
            prc.ExecuteNonQuery();
        }
    }
}