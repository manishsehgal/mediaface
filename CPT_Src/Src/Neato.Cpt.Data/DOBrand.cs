using System;
using System.Collections;
using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data {
    public class DODeviceBrand {
        private DODeviceBrand() {}

        public static DeviceBrand GetDeviceBrand(DeviceBrandBase brand) {
            // TODO: rename Procedure to PrDeviceBrandGetById
            PrBrandGetById prc = new PrBrandGetById();
            prc.BrandId = brand.Id;
            DeviceBrand resultBrand = null;

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int nameColumnIndex = reader.GetOrdinal("Name");
                if (reader.Read()) {
                    resultBrand = new DeviceBrand();
                    resultBrand.Id = reader.GetInt32(idColumnIndex);
                    resultBrand.Name = reader.GetString(nameColumnIndex);
                }
            }
            return resultBrand;
        }

        public static DeviceBrand[] EnumerateDeviceBrandByParam(DeviceType deviceType, CarrierBase carrier, string paperState) {
            PrDeviceBrandEnumerate prc = new PrDeviceBrandEnumerate();

            if (deviceType != DeviceType.Undefined) {
                prc.DeviceTypeId = (int)deviceType;
            }
            if (carrier != null) {
                prc.CarrierId = carrier.Id;
            }
            if (paperState != null) {
                prc.PaperState = paperState;
            }

            ArrayList carriers = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int idColumnName = reader.GetOrdinal("Name");
                while (reader.Read()) {
                    DeviceBrand brand = new DeviceBrand();
                    brand.Id = reader.GetInt32(idColumnIndex);
                    brand.Name = reader.GetString(idColumnName);
                    carriers.Add(brand);
                }
            }
            return (DeviceBrand[])carriers.ToArray(typeof(DeviceBrand));
        }

        public static byte[] GetDeviceBrandIcon(DeviceBrandBase brand) {
            PrDeviceBrandIconGetById prc = new PrDeviceBrandIconGetById();

            prc.DeviceBrandId = brand.Id;
            using (IDataReader reader = prc.ExecuteReader()) {
                int iconColumnIndex = reader.GetOrdinal("Icon");
                if (reader.Read()) {
                    object icon = reader.GetValue(iconColumnIndex);
                    if (DBNull.Value != icon)
                        return (byte[])icon;
                    else
                        return (new byte[0]);
                } else {
                    return new byte[0];
                }
            }
        }

        public static void Add(DeviceBrand brand) {
            if (!brand.IsNew) throw new InvalidOperationException("Adding Device Brand entity in 'Non-New' status");
            PrDeviceBrandIns prc = new PrDeviceBrandIns();
            prc.Name = brand.Name;
            prc.ExecuteNonQuery();
            brand.Id = prc.DeviceBrandId.Value;
        }

        public static void Update(DeviceBrand brand) {
            if (brand.IsNew) throw new InvalidOperationException("Updating Device Brand entity in 'New' status");
            PrDeviceBrandUpd prc = new PrDeviceBrandUpd();
            prc.DeviceBrandId = brand.Id;
            prc.Name = brand.Name;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Device Brand does not exist.");
        }

        public static void UpdateIcon(DeviceBrandBase brand, byte[] icon) {
            PrDeviceBrandIconUpd prc = new PrDeviceBrandIconUpd();
            prc.DeviceBrandId = brand.Id;
            prc.Icon = icon;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Device Brand does not exist.");
        }

        public static void Delete(DeviceBrandBase brand) {
            PrDeviceBrandDel prc = new PrDeviceBrandDel();
            prc.DeviceBrandId = brand.Id;
            prc.ExecuteNonQuery();
        }

    }
}