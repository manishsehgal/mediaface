using System;
using System.Collections;
using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Data.Scheme;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.Data {
    public class DODynamicLink {
        private const string YesConst = "Y";
        private static DynamicLink dynamicLink;

        private DODynamicLink() {}

        private static DynamicLink EnumDynamicLink() {
            PrDynamicLinkEnum prc = new PrDynamicLinkEnum();
            return (DynamicLink)prc.LoadDataSet(new DynamicLink());
        }

        private static String GetText(int linkId, string culture) {
            Hashtable namesValue = new Hashtable();
            foreach(DynamicLink.LinkLocalizationRow row in dynamicLink.LinkLocalization.Rows) {
                if (row.LinkId != linkId)
                    continue;
                namesValue.Add(row.Culture, row.Text);
            }
            string defaultText = LocalizationHelper.GetString(culture, namesValue, "Key", "Value");
            if (defaultText != null)
                return defaultText;

            return string.Empty;
        }

        public static void ResetDynamicLinks() {
            dynamicLink = null;
        }

        public static LinkItem[] GetLinks(int retailerId, string pageUrl, Place place, Align align, string Culture) {
            if (dynamicLink == null)
                dynamicLink = EnumDynamicLink();

            string placeValue = place.ToString();
            
            ArrayList links = new ArrayList();
            foreach(DynamicLink.LinkPlaceRow linkPlace in dynamicLink.LinkPlace.Rows) {
                if (linkPlace.PageURL.ToUpper() != pageUrl.ToUpper() ||
                    linkPlace.Place.ToUpper() != placeValue.ToUpper()||
                    linkPlace.RetailerId != retailerId)
                    continue;

                string alignValue = convertAlignToString(align);
                if (alignValue.Length > 0 && linkPlace.Align != alignValue)
                    continue;

                Align newAlign = Align.None;
                if (linkPlace.Align == "L")
                    newAlign = Align.Left;
                else if (linkPlace.Align == "R")
                    newAlign = Align.Right;

                links.Add(new LinkItem(GetText(linkPlace.LinkId, Culture),
                    linkPlace.LinkURL, linkPlace.IsNewWindow == YesConst, linkPlace.IsVisible,
                    newAlign, linkPlace.SortOrder, linkPlace.LinkId, linkPlace.Icon));
            }

            LinkItem[] item = (LinkItem[])links.ToArray(typeof(LinkItem));

            return item;
        }

        public static byte[] GetIcon(int linkId) {
            if (dynamicLink == null)
                dynamicLink = EnumDynamicLink();

            foreach(DynamicLink.LinkPlaceRow linkPlace in dynamicLink.LinkPlace.Rows) {
                if (linkPlace.LinkId == linkId)
                    return linkPlace.Icon;
            }
            
            return new byte[0];
        }

        public static PageData PageEnumByUrl(int retailerId, string pageUrl) {
            PrPageEnumByUrl prc = new PrPageEnumByUrl();
            prc.RetailerId = retailerId;
            prc.Url = pageUrl;
            
            return (PageData)prc.LoadDataSet(new PageData());
        }

        public static int AddPage(int retailerId, string pageUrl, string description) {
            PrPageIns prc = new PrPageIns();
            prc.RetailerId = retailerId;
            prc.PageUrl = pageUrl;
            prc.Description = description;
            prc.ExecuteNonQuery();

            return prc.PageId.Value;
        }

        public static LinkItem[] GetLinks(int pageId, Place place, string Culture) {
            PrDynamicLinkEnumByParam prc = new PrDynamicLinkEnumByParam();
            prc.PageId = pageId;
            prc.Place = place.ToString();
            prc.Culture = Culture;

            ArrayList list = new ArrayList();
            string tmp;
            byte[] icon;
            using (IDataReader reader = prc.ExecuteReader()) {
                int linkIdColumnIndex = reader.GetOrdinal("LinkId");
                int iconColumnIndex = reader.GetOrdinal("Icon");
                int alignColumnIndex = reader.GetOrdinal("Align");
                int isNewWindowColumnIndex = reader.GetOrdinal("IsNewWindow");
                int isVisibleColumnIndex = reader.GetOrdinal("IsVisible");
                int urlColumnIndex = reader.GetOrdinal("Url");
                int sortOrderColumnIndex = reader.GetOrdinal("SortOrder");
                int textColumnIndex = reader.GetOrdinal("Text");
                while (reader.Read()) {
                    LinkItem item = new LinkItem();
                    item.LinkId = reader.GetInt32(linkIdColumnIndex);
                    tmp = reader.GetString(alignColumnIndex);
                    if (tmp == "L")
                        item.Align = Align.Left;
                    else if (tmp == "R")
                        item.Align = Align.Right;
                    icon = reader.GetValue(iconColumnIndex) as byte[];
                    if (icon == null)
                        icon = new byte[0];
                    item.IsNewWindow = reader.GetString(isNewWindowColumnIndex) == YesConst;
                    item.IsVisible = reader.GetBoolean(isVisibleColumnIndex);
                    item.Url = reader.GetString(urlColumnIndex);
                    item.Icon = icon;
                    item.SortOrder = reader.GetInt32(sortOrderColumnIndex);
                    item.Text = reader.GetString(textColumnIndex);
                    list.Add(item);
                }
            }

            return (LinkItem[])list.ToArray(typeof(LinkItem));
        }

        public static void DynamicLinkDelByPageId(int pageId) {
            PrDynamicLinkDelByPageId prc = new PrDynamicLinkDelByPageId();
            prc.ParentPageId = pageId;
            prc.ExecuteNonQuery();
        }

        public static void AddDynamicLink(int retailerId, int pageId, Place place, string culture, LinkItem item) {
            PrDynamicLinkIns prc = new PrDynamicLinkIns();
            prc.RetailerId = retailerId;
            prc.ParentPageId = pageId;
            prc.LinkUrl = item.Url;
            prc.Icon = item.Icon;
            prc.SortOrder = item.SortOrder;
            prc.Place = place.ToString();
            prc.Align = convertAlignToString(item.Align);
            prc.IsNewWindow = item.IsNewWindow ? "Y" : "N";
            prc.IsVisible = item.IsVisible;
            prc.Culture = culture;
            prc.Text = item.Text;
            prc.ExecuteNonQuery();
            item.LinkId = prc.LinkId.Value;
        }

        public static void UpdateDynamicLink(int retailerId, int pageId, Place place, string culture, LinkItem item) {
            PrDynamicLinkUpd prc = new PrDynamicLinkUpd();
            prc.RetailerId = retailerId;
            prc.ParentPageId = pageId;
            prc.LinkId = item.LinkId;
            prc.LinkUrl = item.Url;
            prc.Icon = item.Icon;
            prc.SortOrder = item.SortOrder;
            prc.Place = place.ToString();
            prc.Align = convertAlignToString(item.Align);
            prc.IsNewWindow = item.IsNewWindow ? "Y" : "N";
            prc.IsVisible = item.IsVisible;
            prc.Culture = culture;
            prc.Text = item.Text;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("DynamicLink does not exist.");
        }

        public static void DeleteDynamicLink(int pageId, int linkId, Place place) {
            PrDynamicLinkDelByParam prc = new PrDynamicLinkDelByParam();
            prc.ParentPageId = pageId;
            prc.LinkId = linkId;
            prc.Place = place.ToString();
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("DynamicLink does not exist.");
        }

        private static string convertAlignToString(Align align) {
            string alignValue;
            if (align == Align.Left)
                alignValue = "L";
            else if (align == Align.Right)
                alignValue = "R";
            else
                alignValue = string.Empty;

            return alignValue;
        }
    }
}