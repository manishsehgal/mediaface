using System;
using System.Collections;
using System.Data;

using Neato.Cpt.Data.DBEngine;
using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Data.Scheme;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data {
    public class DOCustomer {
        public DOCustomer() {}

        private static Customer[] ReadCustomerInfo(ProcedureWrapper prc) {
            ArrayList list = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int customerIdColumnIndex = reader.GetOrdinal("CustomerId");
                int firstNameColumnIndex = reader.GetOrdinal("FirstName");
                int lastNameColumnIndex = reader.GetOrdinal("LastName");
                int emailColumnIndex = reader.GetOrdinal("Email");
                int passwordColumnIndex = reader.GetOrdinal("Password");
                int emailOptionsColumnIndex = reader.GetOrdinal("EmailOptions");
                int receiveInfoColumnIndex = reader.GetOrdinal("ReceiveInfo");
                int pswuidColumnIndex = reader.GetOrdinal("pswuid");
                int createdColumnIndex = reader.GetOrdinal("Created");
                int retailerIdColumnIndex = reader.GetOrdinal("RetailerId");

                while (reader.Read()) {
                    Customer customer = new Customer();
                    customer.CustomerId = reader.GetInt32(customerIdColumnIndex);
                    customer.FirstName = reader.GetString(firstNameColumnIndex);
                    customer.LastName = reader.GetString(lastNameColumnIndex);
                    customer.Email = reader.GetString(emailColumnIndex);
                    customer.Password = reader.GetString(passwordColumnIndex);
                    Object obj = reader.GetValue(pswuidColumnIndex);
                    if (obj != System.DBNull.Value)
                        customer.PasswordUid = Convert.ToString(obj);
                    customer.ReceiveInfo = reader.GetBoolean(receiveInfoColumnIndex);
                    string emailOptionsLetter = reader.GetString(emailOptionsColumnIndex);
                    switch (emailOptionsLetter) {
                        case "T":
                            customer.EmailOptions = EmailOptions.Text;
                            break;
                        case "H":
                            customer.EmailOptions = EmailOptions.Html;
                            break;
                    }
                    customer.Created = reader.GetDateTime(createdColumnIndex);
                    customer.RetailerId = reader.GetInt32(retailerIdColumnIndex);
                    list.Add(customer);
                }

                reader.Close();
            }

            return (Customer[])list.ToArray(typeof(Customer));
        }

        public static Customer[] CustomerSearch(string firstName, string lastName, string email, DateTime startDate, DateTime endDate, Retailer retailer) {
            PrCustomerSearch prc = new PrCustomerSearch();
            if (firstName != string.Empty)
                prc.FirstName = firstName;
            if (lastName != string.Empty)
                prc.LastName = lastName;
            if (email != string.Empty)
                prc.Email = email;
            prc.StartDate = startDate; 
            prc.EndDate = endDate;
            if(retailer != null)
                prc.RetailerId = retailer.Id;
            Customer[] list = ReadCustomerInfo(prc);
            return list;
        }

        public static Customer GetCustomerByLogin(string email, int retailerId) {
            PrCustomerGetByEmail prc = new PrCustomerGetByEmail();
            prc.Email = email;
            prc.RetailerId = retailerId;

            Customer customer = null;
            Customer[] list = ReadCustomerInfo(prc);
            if (list != null && list.Length > 0)
                customer = list[0];

            if (customer != null) {
                customer.LastEditedPapers =
                    DOLastEditedPaper.EnumerateLastEditedPaperByCustomerList(customer);
            }
            
            return customer;
        }

        public static Customer GetCustomerById(int customerId) {
            PrCustomerGetById prc = new PrCustomerGetById();
            prc.CustomerId = customerId;

            Customer customer = null;
            Customer[] list = ReadCustomerInfo(prc);
            if (list != null && list.Length > 0)
                customer = list[0];

            if (customer != null) {
                customer.LastEditedPapers =
                    DOLastEditedPaper.EnumerateLastEditedPaperByCustomerList(customer);
            }
            
            return customer;
        }

        public static void InsertCustomer(Customer customer, LastEditedPaperData data) {
            PrCustomerIns prc = new PrCustomerIns();
            prc.FirstName = customer.FirstName;
            prc.LastName = customer.LastName;
            prc.Email = customer.Email;
            prc.Password = customer.Password;
            prc.ReceiveInfo = customer.ReceiveInfo;
            prc.Created  = customer.Created;
            prc.RetailerId = customer.RetailerId;
            switch (customer.EmailOptions) {
                case EmailOptions.Text:
                    prc.EmailOptions = "T";
                    break;
                case EmailOptions.Html:
                    prc.EmailOptions = "H";
                    break;
            }

            prc.ExecuteNonQuery();
            customer.CustomerId = prc.CustomerId.Value;

            if (data != null) {
                foreach (LastEditedPaperData.LastEditedPaperRow paper in data.LastEditedPaper) {
                    paper.CustomerId = customer.CustomerId;
                }
                DOLastEditedPaper.UpdateLastEditedPaper(data);
                customer.LastEditedPapers = DOLastEditedPaper.
                    EnumerateLastEditedPaperByCustomerList(customer);
            }
        }

        public static void UpdateCustomer(Customer customer) {
            PrCustomerUpd prc = new PrCustomerUpd();
            prc.CustomerId = customer.CustomerId;
            prc.FirstName = customer.FirstName;
            prc.LastName = customer.LastName;
            prc.Email = customer.Email;
            prc.Password = customer.Password;
            prc.PasswordUid = customer.PasswordUid;
            prc.ReceiveInfo = customer.ReceiveInfo;
            prc.RetailerId = customer.RetailerId;
            switch (customer.EmailOptions) {
                case EmailOptions.Text:
                    prc.EmailOptions = "T";
                    break;
                case EmailOptions.Html:
                    prc.EmailOptions = "H";
                    break;
            }

            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Customer does not exist.");
        }

        public static void DeleteCustomer(int customerId) {
            PrCustomerDel prc = new PrCustomerDel();
            prc.CustomerId = customerId;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Customer does not exist.");
        }
    }
}