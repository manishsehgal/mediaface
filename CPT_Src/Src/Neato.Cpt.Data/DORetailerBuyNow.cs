using System.Collections;
using System.Data;

using Neato.Cpt.Data.DbProcedures;

namespace Neato.Cpt.Data {
    public sealed class DORetailerBuyNow {
        private DORetailerBuyNow() {}

        public static int[] Enum(int retailerId) {
            PrRetailerBuyNowEnum prc = new PrRetailerBuyNowEnum();
            prc.RetailerId = retailerId;
            ArrayList retailers = new ArrayList();
            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnVisibleRetailerIdIndex = reader.GetOrdinal("VisibleRetailerId");
                while (reader.Read()) {
                    retailers.Add(reader.GetInt32(idColumnVisibleRetailerIdIndex));
                }
            }

            return (int[])retailers.ToArray(typeof(int));
        }

        public static void Insert(int retailerId, int visibleRetailerId) {
            PrRetailerBuyNowIns prc = new PrRetailerBuyNowIns();
            prc.RetailerId = retailerId;
            prc.VisibleRetailerId = visibleRetailerId;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected == 0)
                throw new DBConcurrencyException("Cant add record to RetailerBuyNow");
        }

        public static void Delete(int retailerId, int visibleRetailerId) {
            PrRetailerBuyNowDel prc = new PrRetailerBuyNowDel();
            prc.RetailerId = retailerId;
            prc.VisibleRetailerId = visibleRetailerId;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected == 0)
                throw new DBConcurrencyException("Cant delete record from RetailerBuyNow");
        }
    }
}