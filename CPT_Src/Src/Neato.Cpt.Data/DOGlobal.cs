using System.Data;

using Neato.Cpt.Data.DBEngine;

namespace Neato.Cpt.Data {
    public sealed class DOGlobal {
        private DOGlobal() {}

        private const string DatabaseInstanceName = ""; // Default instance

        public static void BeginTransaction() {
            BeginTransaction(IsolationLevel.ReadCommitted);
        }

        public static void BeginTransaction(IsolationLevel level) {
            TransactionalContext.BeginTransaction(DatabaseInstanceName, level);
        }

        public static void CommitTransaction() {
            TransactionalContext.CommitTransaction(DatabaseInstanceName);
        }

        public static void RollbackTransaction() {
            TransactionalContext.RollbackTransaction(DatabaseInstanceName);
        }

        public static void RollbackAllActiveTransactions() {
            TransactionalContext.RollbackAllActiveTransactions();
        }
    }
}