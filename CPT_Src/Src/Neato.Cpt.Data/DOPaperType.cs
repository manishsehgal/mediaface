using System.Collections;
using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data {
    public sealed class DOPaperType {
        private DOPaperType() {}

        public static Hashtable Enum() {
            PrPaperTypeEnum prc = new PrPaperTypeEnum();

            Hashtable table = new Hashtable();
            PaperType paperType;
            
            using (IDataReader reader = prc.ExecuteReader()) {
                int IdIndex = reader.GetOrdinal("Id");
                int NameIndex = reader.GetOrdinal("Name");
                while (reader.Read()) {
                    paperType = (PaperType)PaperType.Parse(typeof(PaperType),reader.GetInt32(IdIndex).ToString(), true);
                    table.Add(paperType, reader.GetString(NameIndex));
                }
            }

            return table;
        }
    }
}