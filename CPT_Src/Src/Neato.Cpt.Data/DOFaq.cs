using Neato.Cpt.Data.DBEngine;
using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.DataBase;

namespace Neato.Cpt.Data {
    public class DOFaq {
        private DOFaq() {}

        public static FaqData Enum(Retailer retailer) {
            PrFaqEnum prc = new PrFaqEnum();
            prc.RetailerId = retailer.Id;
            return (FaqData)prc.LoadDataSet(new FaqData());
        }

        public static void Update(FaqData data) {
            PrFaqIns prcIns = new PrFaqIns();
            PrFaqUpd prcUpd = new PrFaqUpd();
            PrFaqDel prcDel = new PrFaqDel();
            ProcedureWrapper.UpdateDataTable(data.Faq, prcIns, prcUpd, prcDel);
        }
    }
}