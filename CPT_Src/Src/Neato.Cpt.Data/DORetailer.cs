using System;
using System.Collections;
using System.Data;

using Neato.Cpt.Data.DBEngine;
using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data {
    public class DORetailer {
        public DORetailer() {}

        private static Retailer[] ReadRetailersInfo(ProcedureWrapper prc) {
            ArrayList retailers = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int idColumnName = reader.GetOrdinal("Name");
                int idColumnDisplayName = reader.GetOrdinal("DisplayName");
                int idColumnUrl = reader.GetOrdinal("Url");
                int idColumnPhoneAvailable = reader.GetOrdinal("PhoneAvailable");
                int idColumnMp3Available = reader.GetOrdinal("Mp3Available");
                int idColumnStickerAvailable = reader.GetOrdinal("StickerAvailable");
                int idColumnHasBrandedSite = reader.GetOrdinal("HasBrandedSite");
                object displayName;
                while (reader.Read()) {
                    Retailer retailer = new Retailer();
                    retailer.Id = reader.GetInt32(idColumnIndex);
                    retailer.Name = reader.GetString(idColumnName);
                    displayName = reader.GetValue(idColumnDisplayName);
                    retailer.DisplayName = displayName as string;
                    retailer.Url = reader.GetString(idColumnUrl);
                    retailer.PhoneAvailability = (DeviceAvailability)reader.GetInt32(idColumnPhoneAvailable);
                    retailer.Mp3Availability = (DeviceAvailability)reader.GetInt32(idColumnMp3Available);
                    retailer.StickerAvailability = (DeviceAvailability)reader.GetInt32(idColumnStickerAvailable);
                    retailer.HasBrandedSite = reader.GetBoolean(idColumnHasBrandedSite);
                    retailers.Add(retailer);
                }
            }
            return (Retailer[])retailers.ToArray(typeof(Retailer));
        }

        public static Retailer[] GetRetailers() {
            PrRetailersEnum prc = new PrRetailersEnum();
            return ReadRetailersInfo(prc);
        }

        public static byte[] GetRetailerIcon(int id) {
            PrRetailersGetIconById prc = new PrRetailersGetIconById();
            prc.Id = id;
            using (IDataReader reader = prc.ExecuteReader()) {
                int iconColumnIndex = reader.GetOrdinal("Icon");
                if (reader.Read()) {
                    object icon = reader.GetValue(iconColumnIndex);
                    if (DBNull.Value != icon)
                        return (byte[])icon;
                    else
                        return (new byte[0]);
                } else {
                    return new byte[0];
                }
            }

        }

        public static void DeleteRetailer(int id) {
            PrRetailersDel prc = new PrRetailersDel();
            prc.Id = id;
            prc.ExecuteNonQuery();
            return;
        }

        public static int InsertRetailer(Retailer retailer, byte[] icon) {
            PrRetailersIns prc = new PrRetailersIns();
            prc.Icon = icon;
            prc.Name = retailer.Name;
            prc.DisplayName = retailer.Name;
            prc.Url = retailer.Url;
            prc.PhoneAvailable = (Int32)retailer.PhoneAvailability;
            prc.Mp3Available = (Int32)retailer.Mp3Availability;
            prc.StickerAvailable = (Int32)retailer.StickerAvailability;
            prc.HasBrandedSite = retailer.HasBrandedSite;
            prc.ExecuteNonQuery();
            retailer.Id = prc.Id.Value;
            return 0;
        }

        public static int Update(Retailer retailer) {
            PrRetailersUpd prc = new PrRetailersUpd();
            prc.Id = retailer.Id;
            prc.Name = retailer.Name;
            prc.DisplayName = retailer.DisplayName;
            prc.Url = retailer.Url;
            prc.PhoneAvailable = (Int32)retailer.PhoneAvailability;
            prc.Mp3Available = (Int32)retailer.Mp3Availability;
            prc.StickerAvailable = (Int32)retailer.StickerAvailability;
            prc.HasBrandedSite = retailer.HasBrandedSite;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Retailer does not exist.");
            return prc.RowsAffected;
        }

        public static int UpdateIcon(Retailer retailer, byte[] image) {
            PrRetailersIconUpd prc = new PrRetailersIconUpd();
            prc.Id = retailer.Id;
            prc.Icon = image;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Retailer does not exist.");
            return prc.RowsAffected;
        }

        public static Retailer[] GetRetailers(Carrier carrier) {
            PrRetailersGetByCarrierId prc = new PrRetailersGetByCarrierId();
            prc.CarrierId = carrier.Id;
            return ReadRetailersInfo(prc);
        }

        public static Retailer[] GetRetailers(Paper paper) {
            PrRetailersGetByPaperId prc = new PrRetailersGetByPaperId();
            prc.PaperId = paper.Id;
            return ReadRetailersInfo(prc);
        }

        public static bool CheckCarrierForAnyDevices(Carrier carrier, Retailer retailer, string paperState) {
            PrIsAnyDevicesForCarrierAndRetailer prc = new PrIsAnyDevicesForCarrierAndRetailer();
            prc.RetailerId = retailer.Id;
            prc.CarrierId = carrier.Id;
            if (paperState != string.Empty)
                prc.PaperState = paperState;
            prc.ExecuteNonQuery();
            return (prc.Result > 0).Value;
        }

        public static bool CheckBrandForAnyDevices(DeviceBrand brand, Retailer retailer, string paperState) {
            PrIsAnyDeviceForBrandAndRetailer prc = new PrIsAnyDeviceForBrandAndRetailer();
            prc.RetailerId = retailer.Id;
            prc.BrandId = brand.Id;
            if (paperState != string.Empty)
                prc.PaperState = paperState;
            prc.ExecuteNonQuery();
            return (prc.Result > 0).Value;
        }
    }
}