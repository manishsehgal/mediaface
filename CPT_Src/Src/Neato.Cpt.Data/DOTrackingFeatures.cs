using System;
using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Tracking;

namespace Neato.Cpt.Data {
    public class DOTrackingFeatures {
        public DOTrackingFeatures() {}

        public static int Add(Feature feature, string login, DateTime time, Retailer retailer) {
            PrTrackingFeaturesIns prc = new PrTrackingFeaturesIns();
            prc.Action = feature.ToString();
            prc.Time = time;
            prc.Login = login;
            if(retailer != null)prc.RetailerId = retailer.Id;
            prc.ExecuteNonQuery();
            return prc.Id.Value;
        }

        public static DataSet GetFeaturesListByDate(DateTime startTime, DateTime endTime, Retailer retailer) {
            PrTrackingFeaturesEnumByTime prc = new PrTrackingFeaturesEnumByTime();
            prc.TimeEnd = endTime;
            prc.TimeStart = startTime;
            if(retailer != null)prc.RetailerId = retailer.Id;
            DataSet data = new DataSet();
            data.Tables.Add("Features");
            return prc.LoadDataSet(data);
        }

    }
}