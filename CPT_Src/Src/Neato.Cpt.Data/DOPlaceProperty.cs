using System.Collections;
using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data {
    public sealed class DOPlaceProperty {
        private DOPlaceProperty() {}

        public static PlaceProperty[] Enum() {
            PrPlacePropertyEnum prc = new PrPlacePropertyEnum();
            ArrayList props = new ArrayList();
            
            using (IDataReader reader = prc.ExecuteReader()) {
                int idRetailerIdIndex = reader.GetOrdinal("RetailerId");
                int idPlaceIndex = reader.GetOrdinal("Place");
                int idBackgroundColorIndex = reader.GetOrdinal("BackgroundColor");
                while (reader.Read()) {
                    PlaceProperty property = new PlaceProperty();
                    property.RetailerId = reader.GetInt32(idRetailerIdIndex);
                    property.Place = reader.GetString(idPlaceIndex);
                    property.BackgroundColor = reader.GetString(idBackgroundColorIndex);
                    props.Add(property);
                }
            }
            return (PlaceProperty[])props.ToArray(typeof(PlaceProperty));
        }

        public static void Delete(int retailerId, Place place) {
            PrPlacePropertyDel prc = new PrPlacePropertyDel();
            prc.RetailerId = retailerId;
            prc.Place = place.ToString();
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("PlaceProperty does not exist.");
            
        }

        public static void Update(int retailerId, Place place, PlaceProperty placeProperty) {
            PrPlacePropertyUpd prc = new PrPlacePropertyUpd();
            prc.RetailerId = retailerId;
            prc.Place = place.ToString();
            prc.BackgroundColor = placeProperty.BackgroundColor;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("PlaceProperty does not exist.");            
        }

        public static void Insert(int retailerId, Place place, PlaceProperty placeProperty) {
            PrPlacePropertyIns prc = new PrPlacePropertyIns();
            prc.RetailerId = retailerId;
            prc.Place = place.ToString();
            prc.BackgroundColor = placeProperty.BackgroundColor;
            prc.ExecuteNonQuery();
        }
    }
}