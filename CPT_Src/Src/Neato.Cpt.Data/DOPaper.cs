using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Xml;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Data.Scheme;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.Data {
    public class DOPaper {
        public DOPaper() {}

        public static PaperBase[] EnumeratePaperByParam(PaperBrandBase paperBrand, DeviceType deviceType, DeviceBase device, string paperName,  string paperState, Retailer retailer) {
            PrPaperEnumByParam prc = new PrPaperEnumByParam();

            if (device != null) {
                prc.DeviceId = device.Id;
            }

            if (paperBrand != null) {
                prc.PaperBrandId = paperBrand.Id;
            }

            if (deviceType != DeviceType.Undefined) {
                prc.DeviceTypeId = (int)deviceType;
            }

            if (paperName != null) {
                prc.PaperName = paperName;
            }

            if (paperState != null) {
                prc.PaperState = paperState;
            }
            if(retailer != null)
                prc.RetailerId = retailer.Id;

            ArrayList papers = new ArrayList();
            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                while (reader.Read()) {
                    PaperBase paper = new PaperBase();
                    paper.Id = reader.GetInt32(idColumnIndex);
                    papers.Add(paper);
                }
            }
            return (PaperBase[])papers.ToArray(typeof(PaperBase));
        }

        public static Paper GetPaper(PaperBase paperBase) {
            PrPaperGetById prc = new PrPaperGetById();

            if (paperBase != null) {
                prc.PaperId = paperBase.Id;
            }

            PaperData data = (PaperData)prc.LoadDataSet(new PaperData());
            Paper paper = null;
            if (data.Paper.Count == 1) {
                paper = new Paper();
                paper.Id = data.Paper[0].Id;
                paper.Name = data.Paper[0].Name;
                paper.Size = new SizeF(data.Paper[0].Width, data.Paper[0].Height);
                paper.Brand = DOPaperBrand.GetPaperBrand(new PaperBrandBase(data.Paper[0].BrandId));
                paper.PaperType = (PaperType)PaperType.Parse(typeof(PaperType),data.Paper[0].PaperType.ToString(), true);
                paper.PaperState = data.Paper[0].State;

                foreach (PaperData.FaceToPaperRow faceToPaper in data.FaceToPaper) {
                    XmlNode contour = null;
                    if (!faceToPaper.FaceRow.IsContourNull()) {
                        contour = ConvertHelper.ByteArrayToXmlNode(faceToPaper.FaceRow.Contour);
                    }
                    Face face = new Face(faceToPaper.FaceId, contour);
                    foreach (PaperData.FaceLocalizationRow localization in faceToPaper.FaceRow.GetFaceLocalizationRows()) {
                        face.AddName(localization.Culture, localization.FaceName);
                    }
                    paper.AddFace(face, faceToPaper.FaceX, faceToPaper.FaceY, faceToPaper.Rotation);
                }
            }
            return paper;
        }

        public static byte[] GetPaperIcon(PaperBase paperBase) {
            PrPaperIconGetById prc = new PrPaperIconGetById();

            prc.PaperId = paperBase.Id;
            using (IDataReader reader = prc.ExecuteReader()) {
                int iconColumnIndex = reader.GetOrdinal("Icon");
                if (reader.Read()) {
                    object icon = reader.GetValue(iconColumnIndex);
                    if (DBNull.Value != icon)
                        return (byte[])icon;
                    else
                        return (new byte[0]);
                } else {
                    return new byte[0];
                }
            }
        }

        public static void Add(Paper paper) {
            if (!paper.IsNew) throw new InvalidOperationException("Adding Paper entity in 'Non-New' status");

            PrPaperIns prc = new PrPaperIns();
            prc.Name = paper.Name;
            prc.PaperBrandId = paper.Brand.Id;
            prc.Height = paper.Size.Height;
            prc.Width = paper.Size.Width;
            prc.PaperType = (int)paper.PaperType;
            prc.PaperState = paper.PaperState;

            prc.ExecuteNonQuery();
            paper.Id = prc.PaperId.Value;
        }

        public static void Update(Paper paper) {
            if (paper.IsNew) throw new InvalidOperationException("Updating Paper entity in 'New' status");
            PrPaperUpd prc = new PrPaperUpd();
            prc.PaperId = paper.Id;
            prc.Name = paper.Name;
            prc.PaperBrandId = paper.Brand.Id;
            prc.Height = paper.Size.Height;
            prc.Width = paper.Size.Width;
            prc.PaperType = (int)paper.PaperType;
            prc.PaperState = paper.PaperState;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Paper does not exist.");
        }

        public static void UpdateIcon(PaperBase paper, byte[] icon) {
            PrPaperIconUpd prc = new PrPaperIconUpd();
            prc.PaperId = paper.Id;
            prc.Icon = icon;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Paper does not exist.");
        }

        public static void Delete(PaperBase paper) {
            PrPaperDel prc = new PrPaperDel();
            prc.PaperId = paper.Id;
            prc.ExecuteNonQuery();
        }

        public static void AddToRetailer(Paper paper, Retailer retailer) {
            PrPaperAddToRetailer prc = new PrPaperAddToRetailer();
            prc.RetailerId = retailer.Id;
            prc.PaperId = paper.Id;
            prc.ExecuteNonQuery();
            if(prc.RowsAffected == 0)
                throw new BusinessException("Cant add PaperToRetailer");
        }

        public static void RemoveFromRetailer(Paper paper, Retailer retailer) {
            PrPaperRemoveFromRetailer prc = new PrPaperRemoveFromRetailer();
            if(retailer != null)
                prc.RetailerId = retailer.Id;
            if(paper != null)
                prc.PaperId = paper.Id;
            prc.ExecuteNonQuery();
            if(prc.RowsAffected == 0)
                throw new BusinessException("Cant delete PaperToRetailer");
        }
        
    }
}
