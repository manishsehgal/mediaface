using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.Data {
    public sealed class DOIis {
        private DOIis() {
        }

        public static void ChangeVirtualDirectoryPath(string serverName, string siteName, string virtualDirectoryName, string physicalPath) {
            VirtualDirectoryInfo vd;
            WebSiteInfoCollection webSites = IISHelper.GetWebSiteList(serverName);
            foreach (WebSiteInfo webSite in webSites.Values) {
                if (webSite.Name != siteName)
                    continue;
                if (virtualDirectoryName == string.Empty) {
                    vd = IISHelper.GetRootVDir(webSite);
                    vd.Path = physicalPath;
                    break;
                }
                vd = IISHelper.GetVDir(webSite, virtualDirectoryName);
                if (vd != null) {
                    vd.Path = physicalPath;
                    break;
                }
            }
        }

        public static string GetVirtualDirectoryPath(string serverName, string siteName, string virtualDirectoryName) {
            VirtualDirectoryInfo vd;
            WebSiteInfoCollection webSites = IISHelper.GetWebSiteList(serverName);
            foreach (WebSiteInfo webSite in webSites.Values) {
                if (webSite.Name != siteName)
                    continue;
                if (virtualDirectoryName == string.Empty) {
                    vd = IISHelper.GetRootVDir(webSite);
                    return vd.Path;
                }
                vd = IISHelper.GetVDir(webSite, virtualDirectoryName);
                if (vd != null)
                    return vd.Path;
            }
            return null;
        }
    }
}