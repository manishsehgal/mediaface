using System;
using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data {
    public class DOTrackingImages {
        public DOTrackingImages() {}

        public static int Add(ImageLibItem image, string login, DateTime time, Retailer retailer) {
            PrTrackingImagesIns prc = new PrTrackingImagesIns();
            prc.ImageId = image.Id;
            prc.Time = time;
            prc.Login = login;
            if(retailer != null)prc.RetailerId = retailer.Id;
            prc.ExecuteNonQuery();
            return prc.Id.Value;
        }

        public static DataSet GetImagesListByDate(DateTime startTime, DateTime endTime, Retailer retailer) {
            PrTrackingImagesEnumByTime prc = new PrTrackingImagesEnumByTime();
            prc.TimeEnd = endTime;
            prc.TimeStart = startTime;
            if(retailer != null)prc.RetailerId = retailer.Id;
            DataSet data = new DataSet();
            data.Tables.Add("Images");
            data.Tables.Add("Folders");
            return prc.LoadDataSet(data);
        }

    }
}