using System;
using System.Runtime.Serialization;

using Neato.Cpt.Entity.Exceptions;

namespace Neato.Cpt.Data {
    [Serializable]
    public class DAException : BaseNonRecoverableException {
        public DAException() : base() {}

        public DAException(string message) : base(message) {}

        public DAException(string message, Exception inner) : base(message, inner) {}

        protected DAException(SerializationInfo info, StreamingContext context) : base(info, context) {}

        public override void GetObjectData(SerializationInfo info, StreamingContext context) {
            base.GetObjectData(info, context);
        }
    }
}