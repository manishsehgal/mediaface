using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data {
    public sealed class DOGoogleTrackPage {
        private DOGoogleTrackPage() {}

        public static GoogleTrackPage GetGoogleTrackPage(string pageUrl) {
            PrGoogleTrackPageGetByPageUrl prc = new PrGoogleTrackPageGetByPageUrl();
            prc.PageUrl = pageUrl;
            
            GoogleTrackPage trackPage = null;
            using (IDataReader reader = prc.ExecuteReader()) {
                int PageUrlIndex = reader.GetOrdinal("PageUrl");
                int EnableScriptIndex = reader.GetOrdinal("EnableScript");
                
                if (reader.Read()) {
                    trackPage =  new GoogleTrackPage();
                    trackPage.PageUrl = reader.GetString(PageUrlIndex);
                    trackPage.EnableScript = reader.GetBoolean(EnableScriptIndex);
                }
            }

            return trackPage;
        }

        public static void Insert(GoogleTrackPage trackPage) {
            PrGoogleTrackPageIns prc = new PrGoogleTrackPageIns();
            prc.PageUrl = trackPage.PageUrl;
            prc.EnableScript = trackPage.EnableScript;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Record for the Google tracking page exists already.");
        }

        public static void Update(GoogleTrackPage trackPage) {
            PrGoogleTrackPageUpd prc = new PrGoogleTrackPageUpd();
            prc.PageUrl = trackPage.PageUrl;
            prc.EnableScript = trackPage.EnableScript;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Record for the Google tracking page does not exist.");
        }
    }
}