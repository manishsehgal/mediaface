using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Data.Scheme;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data {
    public class DOFaceLayout {
        public DOFaceLayout() {}

        public static FaceLayoutData EnumerateFaceLayouts(PaperBase paper) {
            PrFaceLayoutEnumByPaperId prc = new PrFaceLayoutEnumByPaperId();
            prc.PaperId = paper.Id;
            return (FaceLayoutData)prc.LoadDataSet(new FaceLayoutData());
        }
    }
}