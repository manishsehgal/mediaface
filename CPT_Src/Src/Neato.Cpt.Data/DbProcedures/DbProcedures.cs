using System;
using System.Data;
using System.Data.SqlTypes;
using Neato.Cpt.Data.DBEngine;

namespace Neato.Cpt.Data.DbProcedures {

    /// <exclude/>
    internal sealed class PrAnnouncementDel : ProcedureWrapper {
        internal PrAnnouncementDel() : this(string.Empty) {}
        internal PrAnnouncementDel(string databaseInstanceName) : base(databaseInstanceName, "PrAnnouncementDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrAnnouncementEnum : ProcedureWrapper {
        internal PrAnnouncementEnum() : this(string.Empty) {}
        internal PrAnnouncementEnum(string databaseInstanceName) : base(databaseInstanceName, "PrAnnouncementEnum") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrAnnouncementEnumLast : ProcedureWrapper {
        internal PrAnnouncementEnumLast() : this(string.Empty) {}
        internal PrAnnouncementEnumLast(string databaseInstanceName) : base(databaseInstanceName, "PrAnnouncementEnumLast") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrAnnouncementIns : ProcedureWrapper {
        internal PrAnnouncementIns() : this(string.Empty) {}
        internal PrAnnouncementIns(string databaseInstanceName) : base(databaseInstanceName, "PrAnnouncementIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Text", DbType.String, 1073741823, ParameterDirection.Input, true, 0, 0, "Text", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Time", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "Time", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("IsActive", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "IsActive", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Text {
            set { RealWrapper.SetParameterValue("Text", value); }
        }

        public SqlDateTime Time {
            set { RealWrapper.SetParameterValue("Time", value); }
        }

        public SqlBoolean IsActive {
            set { RealWrapper.SetParameterValue("IsActive", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public SqlInt32 Id {
            get {
                object val = RealWrapper.GetParameterValue("Id");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("Id", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrAnnouncementUpd : ProcedureWrapper {
        internal PrAnnouncementUpd() : this(string.Empty) {}
        internal PrAnnouncementUpd(string databaseInstanceName) : base(databaseInstanceName, "PrAnnouncementUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Text", DbType.String, 1073741823, ParameterDirection.Input, true, 0, 0, "Text", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Time", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "Time", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("IsActive", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "IsActive", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }

        public string Text {
            set { RealWrapper.SetParameterValue("Text", value); }
        }

        public SqlDateTime Time {
            set { RealWrapper.SetParameterValue("Time", value); }
        }

        public SqlBoolean IsActive {
            set { RealWrapper.SetParameterValue("IsActive", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrBrandGetById : ProcedureWrapper {
        internal PrBrandGetById() : this(string.Empty) {}
        internal PrBrandGetById(string databaseInstanceName) : base(databaseInstanceName, "PrBrandGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("BrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "BrandId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 BrandId {
            set { RealWrapper.SetParameterValue("BrandId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrBuyMoreSkinsPageEnumByCulture : ProcedureWrapper {
        internal PrBuyMoreSkinsPageEnumByCulture() : this(string.Empty) {}
        internal PrBuyMoreSkinsPageEnumByCulture(string databaseInstanceName) : base(databaseInstanceName, "PrBuyMoreSkinsPageEnumByCulture") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrBuyMoreSkinsPageIns : ProcedureWrapper {
        internal PrBuyMoreSkinsPageIns() : this(string.Empty) {}
        internal PrBuyMoreSkinsPageIns(string databaseInstanceName) : base(databaseInstanceName, "PrBuyMoreSkinsPageIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("HtmlText", DbType.AnsiString, 2147483647, ParameterDirection.Input, true, 0, 0, "HtmlText", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public string HtmlText {
            set { RealWrapper.SetParameterValue("HtmlText", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrBuyMoreSkinsPageUpd : ProcedureWrapper {
        internal PrBuyMoreSkinsPageUpd() : this(string.Empty) {}
        internal PrBuyMoreSkinsPageUpd(string databaseInstanceName) : base(databaseInstanceName, "PrBuyMoreSkinsPageUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("HtmlText", DbType.AnsiString, 2147483647, ParameterDirection.Input, true, 0, 0, "HtmlText", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public string HtmlText {
            set { RealWrapper.SetParameterValue("HtmlText", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrCarrierAddToRetailer : ProcedureWrapper {
        internal PrCarrierAddToRetailer() : this(string.Empty) {}
        internal PrCarrierAddToRetailer(string databaseInstanceName) : base(databaseInstanceName, "PrCarrierAddToRetailer") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CarrierId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CarrierId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 CarrierId {
            set { RealWrapper.SetParameterValue("CarrierId", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrCarrierDel : ProcedureWrapper {
        internal PrCarrierDel() : this(string.Empty) {}
        internal PrCarrierDel(string databaseInstanceName) : base(databaseInstanceName, "PrCarrierDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CarrierId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CarrierId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 CarrierId {
            set { RealWrapper.SetParameterValue("CarrierId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrCarrierGetById : ProcedureWrapper {
        internal PrCarrierGetById() : this(string.Empty) {}
        internal PrCarrierGetById(string databaseInstanceName) : base(databaseInstanceName, "PrCarrierGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CarrierId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CarrierId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 CarrierId {
            set { RealWrapper.SetParameterValue("CarrierId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrCarrierIconGetById : ProcedureWrapper {
        internal PrCarrierIconGetById() : this(string.Empty) {}
        internal PrCarrierIconGetById(string databaseInstanceName) : base(databaseInstanceName, "PrCarrierIconGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CarrierId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CarrierId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 CarrierId {
            set { RealWrapper.SetParameterValue("CarrierId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrCarrierIconUpd : ProcedureWrapper {
        internal PrCarrierIconUpd() : this(string.Empty) {}
        internal PrCarrierIconUpd(string databaseInstanceName) : base(databaseInstanceName, "PrCarrierIconUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CarrierId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CarrierId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Icon", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Icon", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 CarrierId {
            set { RealWrapper.SetParameterValue("CarrierId", value); }
        }

        public byte[] Icon {
            set { RealWrapper.SetParameterValue("Icon", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrCarrierIns : ProcedureWrapper {
        internal PrCarrierIns() : this(string.Empty) {}
        internal PrCarrierIns(string databaseInstanceName) : base(databaseInstanceName, "PrCarrierIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Name", DbType.String, 40, ParameterDirection.Input, true, 0, 0, "Name", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CarrierId", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "CarrierId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Name {
            set { RealWrapper.SetParameterValue("Name", value); }
        }

        public SqlInt32 CarrierId {
            get {
                object val = RealWrapper.GetParameterValue("CarrierId");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("CarrierId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrCarrierRemoveFromRetailer : ProcedureWrapper {
        internal PrCarrierRemoveFromRetailer() : this(string.Empty) {}
        internal PrCarrierRemoveFromRetailer(string databaseInstanceName) : base(databaseInstanceName, "PrCarrierRemoveFromRetailer") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CarrierId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CarrierId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public SqlInt32 CarrierId {
            set { RealWrapper.SetParameterValue("CarrierId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrCarriersEnumerate : ProcedureWrapper {
        internal PrCarriersEnumerate() : this(string.Empty) {}
        internal PrCarriersEnumerate(string databaseInstanceName) : base(databaseInstanceName, "PrCarriersEnumerate") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceTypeId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceTypeId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperState", DbType.StringFixedLength, 16, ParameterDirection.Input, true, 0, 0, "PaperState", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceTypeId {
            set { RealWrapper.SetParameterValue("DeviceTypeId", value); }
        }

        public SqlInt32 DeviceId {
            set { RealWrapper.SetParameterValue("DeviceId", value); }
        }

        public string PaperState {
            set { RealWrapper.SetParameterValue("PaperState", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrCarrierUpd : ProcedureWrapper {
        internal PrCarrierUpd() : this(string.Empty) {}
        internal PrCarrierUpd(string databaseInstanceName) : base(databaseInstanceName, "PrCarrierUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CarrierId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CarrierId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Name", DbType.String, 40, ParameterDirection.Input, true, 0, 0, "Name", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 CarrierId {
            set { RealWrapper.SetParameterValue("CarrierId", value); }
        }

        public string Name {
            set { RealWrapper.SetParameterValue("Name", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrCustomerDel : ProcedureWrapper {
        internal PrCustomerDel() : this(string.Empty) {}
        internal PrCustomerDel(string databaseInstanceName) : base(databaseInstanceName, "PrCustomerDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CustomerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CustomerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 CustomerId {
            set { RealWrapper.SetParameterValue("CustomerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrCustomerGetByEmail : ProcedureWrapper {
        internal PrCustomerGetByEmail() : this(string.Empty) {}
        internal PrCustomerGetByEmail(string databaseInstanceName) : base(databaseInstanceName, "PrCustomerGetByEmail") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Email", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "Email", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Email {
            set { RealWrapper.SetParameterValue("Email", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrCustomerGetById : ProcedureWrapper {
        internal PrCustomerGetById() : this(string.Empty) {}
        internal PrCustomerGetById(string databaseInstanceName) : base(databaseInstanceName, "PrCustomerGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CustomerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CustomerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 CustomerId {
            set { RealWrapper.SetParameterValue("CustomerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrCustomerIns : ProcedureWrapper {
        internal PrCustomerIns() : this(string.Empty) {}
        internal PrCustomerIns(string databaseInstanceName) : base(databaseInstanceName, "PrCustomerIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FirstName", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "FirstName", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("LastName", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "LastName", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Email", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "Email", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Password", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "Password", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PasswordUid", DbType.AnsiString, 50, ParameterDirection.Input, true, 0, 0, "PasswordUid", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EmailOptions", DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, true, 0, 0, "EmailOptions", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("ReceiveInfo", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "ReceiveInfo", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Created", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "Created", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CustomerId", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "CustomerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string FirstName {
            set { RealWrapper.SetParameterValue("FirstName", value); }
        }

        public string LastName {
            set { RealWrapper.SetParameterValue("LastName", value); }
        }

        public string Email {
            set { RealWrapper.SetParameterValue("Email", value); }
        }

        public string Password {
            set { RealWrapper.SetParameterValue("Password", value); }
        }

        public string PasswordUid {
            set { RealWrapper.SetParameterValue("PasswordUid", value); }
        }

        public string EmailOptions {
            set { RealWrapper.SetParameterValue("EmailOptions", value); }
        }

        public SqlBoolean ReceiveInfo {
            set { RealWrapper.SetParameterValue("ReceiveInfo", value); }
        }

        public SqlDateTime Created {
            set { RealWrapper.SetParameterValue("Created", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public SqlInt32 CustomerId {
            get {
                object val = RealWrapper.GetParameterValue("CustomerId");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("CustomerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrCustomerSearch : ProcedureWrapper {
        internal PrCustomerSearch() : this(string.Empty) {}
        internal PrCustomerSearch(string databaseInstanceName) : base(databaseInstanceName, "PrCustomerSearch") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Email", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "Email", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FirstName", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "FirstName", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("LastName", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "LastName", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("StartDate", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "StartDate", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EndDate", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "EndDate", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Email {
            set { RealWrapper.SetParameterValue("Email", value); }
        }

        public string FirstName {
            set { RealWrapper.SetParameterValue("FirstName", value); }
        }

        public string LastName {
            set { RealWrapper.SetParameterValue("LastName", value); }
        }

        public SqlDateTime StartDate {
            set { RealWrapper.SetParameterValue("StartDate", value); }
        }

        public SqlDateTime EndDate {
            set { RealWrapper.SetParameterValue("EndDate", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrCustomerUpd : ProcedureWrapper {
        internal PrCustomerUpd() : this(string.Empty) {}
        internal PrCustomerUpd(string databaseInstanceName) : base(databaseInstanceName, "PrCustomerUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CustomerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CustomerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FirstName", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "FirstName", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("LastName", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "LastName", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Email", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "Email", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Password", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "Password", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PasswordUid", DbType.AnsiString, 50, ParameterDirection.Input, true, 0, 0, "PasswordUid", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EmailOptions", DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, true, 0, 0, "EmailOptions", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("ReceiveInfo", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "ReceiveInfo", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 CustomerId {
            set { RealWrapper.SetParameterValue("CustomerId", value); }
        }

        public string FirstName {
            set { RealWrapper.SetParameterValue("FirstName", value); }
        }

        public string LastName {
            set { RealWrapper.SetParameterValue("LastName", value); }
        }

        public string Email {
            set { RealWrapper.SetParameterValue("Email", value); }
        }

        public string Password {
            set { RealWrapper.SetParameterValue("Password", value); }
        }

        public string PasswordUid {
            set { RealWrapper.SetParameterValue("PasswordUid", value); }
        }

        public string EmailOptions {
            set { RealWrapper.SetParameterValue("EmailOptions", value); }
        }

        public SqlBoolean ReceiveInfo {
            set { RealWrapper.SetParameterValue("ReceiveInfo", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceAddToCarrier : ProcedureWrapper {
        internal PrDeviceAddToCarrier() : this(string.Empty) {}
        internal PrDeviceAddToCarrier(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceAddToCarrier") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CarrierId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CarrierId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceId {
            set { RealWrapper.SetParameterValue("DeviceId", value); }
        }

        public SqlInt32 CarrierId {
            set { RealWrapper.SetParameterValue("CarrierId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceBrandDel : ProcedureWrapper {
        internal PrDeviceBrandDel() : this(string.Empty) {}
        internal PrDeviceBrandDel(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceBrandDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceBrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceBrandId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceBrandId {
            set { RealWrapper.SetParameterValue("DeviceBrandId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceBrandEnumerate : ProcedureWrapper {
        internal PrDeviceBrandEnumerate() : this(string.Empty) {}
        internal PrDeviceBrandEnumerate(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceBrandEnumerate") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CarrierId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CarrierId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceTypeId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceTypeId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperState", DbType.StringFixedLength, 16, ParameterDirection.Input, true, 0, 0, "PaperState", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 CarrierId {
            set { RealWrapper.SetParameterValue("CarrierId", value); }
        }

        public SqlInt32 DeviceTypeId {
            set { RealWrapper.SetParameterValue("DeviceTypeId", value); }
        }

        public string PaperState {
            set { RealWrapper.SetParameterValue("PaperState", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceBrandIconGetById : ProcedureWrapper {
        internal PrDeviceBrandIconGetById() : this(string.Empty) {}
        internal PrDeviceBrandIconGetById(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceBrandIconGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceBrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceBrandId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceBrandId {
            set { RealWrapper.SetParameterValue("DeviceBrandId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceBrandIconUpd : ProcedureWrapper {
        internal PrDeviceBrandIconUpd() : this(string.Empty) {}
        internal PrDeviceBrandIconUpd(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceBrandIconUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceBrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceBrandId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Icon", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Icon", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceBrandId {
            set { RealWrapper.SetParameterValue("DeviceBrandId", value); }
        }

        public byte[] Icon {
            set { RealWrapper.SetParameterValue("Icon", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceBrandIns : ProcedureWrapper {
        internal PrDeviceBrandIns() : this(string.Empty) {}
        internal PrDeviceBrandIns(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceBrandIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Name", DbType.String, 30, ParameterDirection.Input, true, 0, 0, "Name", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceBrandId", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "DeviceBrandId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Name {
            set { RealWrapper.SetParameterValue("Name", value); }
        }

        public SqlInt32 DeviceBrandId {
            get {
                object val = RealWrapper.GetParameterValue("DeviceBrandId");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("DeviceBrandId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceBrandUpd : ProcedureWrapper {
        internal PrDeviceBrandUpd() : this(string.Empty) {}
        internal PrDeviceBrandUpd(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceBrandUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceBrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceBrandId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Name", DbType.String, 30, ParameterDirection.Input, true, 0, 0, "Name", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceBrandId {
            set { RealWrapper.SetParameterValue("DeviceBrandId", value); }
        }

        public string Name {
            set { RealWrapper.SetParameterValue("Name", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceDel : ProcedureWrapper {
        internal PrDeviceDel() : this(string.Empty) {}
        internal PrDeviceDel(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceId {
            set { RealWrapper.SetParameterValue("DeviceId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceEnumByPaper : ProcedureWrapper {
        internal PrDeviceEnumByPaper() : this(string.Empty) {}
        internal PrDeviceEnumByPaper(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceEnumByPaper") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 PaperId {
            set { RealWrapper.SetParameterValue("PaperId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceEnumerate : ProcedureWrapper {
        internal PrDeviceEnumerate() : this(string.Empty) {}
        internal PrDeviceEnumerate(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceEnumerate") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceTypeId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceTypeId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("BrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "BrandId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CarrierId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CarrierId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FaceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FaceId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceTypeId {
            set { RealWrapper.SetParameterValue("DeviceTypeId", value); }
        }

        public SqlInt32 BrandId {
            set { RealWrapper.SetParameterValue("BrandId", value); }
        }

        public SqlInt32 CarrierId {
            set { RealWrapper.SetParameterValue("CarrierId", value); }
        }

        public SqlInt32 FaceId {
            set { RealWrapper.SetParameterValue("FaceId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceGetById : ProcedureWrapper {
        internal PrDeviceGetById() : this(string.Empty) {}
        internal PrDeviceGetById(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceId {
            set { RealWrapper.SetParameterValue("DeviceId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceIconBigGetById : ProcedureWrapper {
        internal PrDeviceIconBigGetById() : this(string.Empty) {}
        internal PrDeviceIconBigGetById(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceIconBigGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceId {
            set { RealWrapper.SetParameterValue("DeviceId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceIconBigUpd : ProcedureWrapper {
        internal PrDeviceIconBigUpd() : this(string.Empty) {}
        internal PrDeviceIconBigUpd(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceIconBigUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Icon", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Icon", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceId {
            set { RealWrapper.SetParameterValue("DeviceId", value); }
        }

        public byte[] Icon {
            set { RealWrapper.SetParameterValue("Icon", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceIconGetById : ProcedureWrapper {
        internal PrDeviceIconGetById() : this(string.Empty) {}
        internal PrDeviceIconGetById(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceIconGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceId {
            set { RealWrapper.SetParameterValue("DeviceId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceIconUpd : ProcedureWrapper {
        internal PrDeviceIconUpd() : this(string.Empty) {}
        internal PrDeviceIconUpd(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceIconUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Icon", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Icon", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceId {
            set { RealWrapper.SetParameterValue("DeviceId", value); }
        }

        public byte[] Icon {
            set { RealWrapper.SetParameterValue("Icon", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceIns : ProcedureWrapper {
        internal PrDeviceIns() : this(string.Empty) {}
        internal PrDeviceIns(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Model", DbType.String, 50, ParameterDirection.Input, true, 0, 0, "Model", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("BrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "BrandId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceTypeId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceTypeId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("SortOrder", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "SortOrder", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceId", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "DeviceId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Model {
            set { RealWrapper.SetParameterValue("Model", value); }
        }

        public SqlInt32 BrandId {
            set { RealWrapper.SetParameterValue("BrandId", value); }
        }

        public SqlInt32 DeviceTypeId {
            set { RealWrapper.SetParameterValue("DeviceTypeId", value); }
        }

        public SqlInt32 SortOrder {
            set { RealWrapper.SetParameterValue("SortOrder", value); }
        }

        public SqlInt32 DeviceId {
            get {
                object val = RealWrapper.GetParameterValue("DeviceId");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("DeviceId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceRemoveFromCarrier : ProcedureWrapper {
        internal PrDeviceRemoveFromCarrier() : this(string.Empty) {}
        internal PrDeviceRemoveFromCarrier(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceRemoveFromCarrier") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CarrierId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CarrierId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceId {
            set { RealWrapper.SetParameterValue("DeviceId", value); }
        }

        public SqlInt32 CarrierId {
            set { RealWrapper.SetParameterValue("CarrierId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceRequestDel : ProcedureWrapper {
        internal PrDeviceRequestDel() : this(string.Empty) {}
        internal PrDeviceRequestDel(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceRequestDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceRequestEmailsGetByDates : ProcedureWrapper {
        internal PrDeviceRequestEmailsGetByDates() : this(string.Empty) {}
        internal PrDeviceRequestEmailsGetByDates(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceRequestEmailsGetByDates") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("StartDate", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "StartDate", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EndDate", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "EndDate", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlDateTime StartDate {
            set { RealWrapper.SetParameterValue("StartDate", value); }
        }

        public SqlDateTime EndDate {
            set { RealWrapper.SetParameterValue("EndDate", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceRequestEnum : ProcedureWrapper {
        internal PrDeviceRequestEnum() : this(string.Empty) {}
        internal PrDeviceRequestEnum(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceRequestEnum") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceRequestEnumByCarrierPhoneManufacturerPhoneModel : ProcedureWrapper {
        internal PrDeviceRequestEnumByCarrierPhoneManufacturerPhoneModel() : this(string.Empty) {}
        internal PrDeviceRequestEnumByCarrierPhoneManufacturerPhoneModel(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceRequestEnumByCarrierPhoneManufacturerPhoneModel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Carrier", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "Carrier", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("OtherCarrier", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "OtherCarrier", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceBrand", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "DeviceBrand", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("OtherDeviceBrand", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "OtherDeviceBrand", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Device", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "Device", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("OtherDevice", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "OtherDevice", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("TimeStart", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "TimeStart", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("TimeEnd", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "TimeEnd", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Carrier {
            set { RealWrapper.SetParameterValue("Carrier", value); }
        }

        public SqlBoolean OtherCarrier {
            set { RealWrapper.SetParameterValue("OtherCarrier", value); }
        }

        public string DeviceBrand {
            set { RealWrapper.SetParameterValue("DeviceBrand", value); }
        }

        public SqlBoolean OtherDeviceBrand {
            set { RealWrapper.SetParameterValue("OtherDeviceBrand", value); }
        }

        public string Device {
            set { RealWrapper.SetParameterValue("Device", value); }
        }

        public SqlBoolean OtherDevice {
            set { RealWrapper.SetParameterValue("OtherDevice", value); }
        }

        public SqlDateTime TimeStart {
            set { RealWrapper.SetParameterValue("TimeStart", value); }
        }

        public SqlDateTime TimeEnd {
            set { RealWrapper.SetParameterValue("TimeEnd", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceRequestGetByDatesAndBrand : ProcedureWrapper {
        internal PrDeviceRequestGetByDatesAndBrand() : this(string.Empty) {}
        internal PrDeviceRequestGetByDatesAndBrand(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceRequestGetByDatesAndBrand") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceBrand", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "DeviceBrand", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("OtherDeviceBrand", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "OtherDeviceBrand", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("StartDate", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "StartDate", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EndDate", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "EndDate", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string DeviceBrand {
            set { RealWrapper.SetParameterValue("DeviceBrand", value); }
        }

        public SqlBoolean OtherDeviceBrand {
            set { RealWrapper.SetParameterValue("OtherDeviceBrand", value); }
        }

        public SqlDateTime StartDate {
            set { RealWrapper.SetParameterValue("StartDate", value); }
        }

        public SqlDateTime EndDate {
            set { RealWrapper.SetParameterValue("EndDate", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceRequestIns : ProcedureWrapper {
        internal PrDeviceRequestIns() : this(string.Empty) {}
        internal PrDeviceRequestIns(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceRequestIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FirstName", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "FirstName", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("LastName", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "LastName", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EmailAddress", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "EmailAddress", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Carrier", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "Carrier", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceBrand", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "DeviceBrand", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Device", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "Device", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Comment", DbType.String, 2000, ParameterDirection.Input, true, 0, 0, "Comment", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Created", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "Created", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string FirstName {
            set { RealWrapper.SetParameterValue("FirstName", value); }
        }

        public string LastName {
            set { RealWrapper.SetParameterValue("LastName", value); }
        }

        public string EmailAddress {
            set { RealWrapper.SetParameterValue("EmailAddress", value); }
        }

        public string Carrier {
            set { RealWrapper.SetParameterValue("Carrier", value); }
        }

        public string DeviceBrand {
            set { RealWrapper.SetParameterValue("DeviceBrand", value); }
        }

        public string Device {
            set { RealWrapper.SetParameterValue("Device", value); }
        }

        public string Comment {
            set { RealWrapper.SetParameterValue("Comment", value); }
        }

        public SqlDateTime Created {
            set { RealWrapper.SetParameterValue("Created", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public SqlInt32 Id {
            get {
                object val = RealWrapper.GetParameterValue("Id");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("Id", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceRequestUpd : ProcedureWrapper {
        internal PrDeviceRequestUpd() : this(string.Empty) {}
        internal PrDeviceRequestUpd(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceRequestUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FirstName", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "FirstName", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("LastName", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "LastName", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EmailAddress", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "EmailAddress", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Carrier", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "Carrier", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceBrand", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "DeviceBrand", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Device", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "Device", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Comment", DbType.String, 2000, ParameterDirection.Input, true, 0, 0, "Comment", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }

        public string FirstName {
            set { RealWrapper.SetParameterValue("FirstName", value); }
        }

        public string LastName {
            set { RealWrapper.SetParameterValue("LastName", value); }
        }

        public string EmailAddress {
            set { RealWrapper.SetParameterValue("EmailAddress", value); }
        }

        public string Carrier {
            set { RealWrapper.SetParameterValue("Carrier", value); }
        }

        public string DeviceBrand {
            set { RealWrapper.SetParameterValue("DeviceBrand", value); }
        }

        public string Device {
            set { RealWrapper.SetParameterValue("Device", value); }
        }

        public string Comment {
            set { RealWrapper.SetParameterValue("Comment", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceTypeIconGetById : ProcedureWrapper {
        internal PrDeviceTypeIconGetById() : this(string.Empty) {}
        internal PrDeviceTypeIconGetById(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceTypeIconGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceTypeId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceTypeId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceTypeId {
            set { RealWrapper.SetParameterValue("DeviceTypeId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceTypesEnumerate : ProcedureWrapper {
        internal PrDeviceTypesEnumerate() : this(string.Empty) {}
        internal PrDeviceTypesEnumerate(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceTypesEnumerate") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }
    }

    /// <exclude/>
    internal sealed class PrDeviceUpd : ProcedureWrapper {
        internal PrDeviceUpd() : this(string.Empty) {}
        internal PrDeviceUpd(string databaseInstanceName) : base(databaseInstanceName, "PrDeviceUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Model", DbType.String, 50, ParameterDirection.Input, true, 0, 0, "Model", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("BrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "BrandId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceTypeId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceTypeId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("SortOrder", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "SortOrder", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceId {
            set { RealWrapper.SetParameterValue("DeviceId", value); }
        }

        public string Model {
            set { RealWrapper.SetParameterValue("Model", value); }
        }

        public SqlInt32 BrandId {
            set { RealWrapper.SetParameterValue("BrandId", value); }
        }

        public SqlInt32 DeviceTypeId {
            set { RealWrapper.SetParameterValue("DeviceTypeId", value); }
        }

        public SqlInt32 SortOrder {
            set { RealWrapper.SetParameterValue("SortOrder", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDynamicLinkDelByPageId : ProcedureWrapper {
        internal PrDynamicLinkDelByPageId() : this(string.Empty) {}
        internal PrDynamicLinkDelByPageId(string databaseInstanceName) : base(databaseInstanceName, "PrDynamicLinkDelByPageId") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("ParentPageId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "ParentPageId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 ParentPageId {
            set { RealWrapper.SetParameterValue("ParentPageId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDynamicLinkDelByParam : ProcedureWrapper {
        internal PrDynamicLinkDelByParam() : this(string.Empty) {}
        internal PrDynamicLinkDelByParam(string databaseInstanceName) : base(databaseInstanceName, "PrDynamicLinkDelByParam") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("ParentPageId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "ParentPageId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Place", DbType.AnsiString, 50, ParameterDirection.Input, true, 0, 0, "Place", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("LinkId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "LinkId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 ParentPageId {
            set { RealWrapper.SetParameterValue("ParentPageId", value); }
        }

        public string Place {
            set { RealWrapper.SetParameterValue("Place", value); }
        }

        public SqlInt32 LinkId {
            set { RealWrapper.SetParameterValue("LinkId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDynamicLinkEnum : ProcedureWrapper {
        internal PrDynamicLinkEnum() : this(string.Empty) {}
        internal PrDynamicLinkEnum(string databaseInstanceName) : base(databaseInstanceName, "PrDynamicLinkEnum") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }
    }

    /// <exclude/>
    internal sealed class PrDynamicLinkEnumByParam : ProcedureWrapper {
        internal PrDynamicLinkEnumByParam() : this(string.Empty) {}
        internal PrDynamicLinkEnumByParam(string databaseInstanceName) : base(databaseInstanceName, "PrDynamicLinkEnumByParam") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PageId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PageId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Place", DbType.AnsiString, 50, ParameterDirection.Input, true, 0, 0, "Place", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Icon", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Icon", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 PageId {
            set { RealWrapper.SetParameterValue("PageId", value); }
        }

        public string Place {
            set { RealWrapper.SetParameterValue("Place", value); }
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public byte[] Icon {
            set { RealWrapper.SetParameterValue("Icon", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDynamicLinkIns : ProcedureWrapper {
        internal PrDynamicLinkIns() : this(string.Empty) {}
        internal PrDynamicLinkIns(string databaseInstanceName) : base(databaseInstanceName, "PrDynamicLinkIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("ParentPageId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "ParentPageId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("LinkUrl", DbType.String, 200, ParameterDirection.Input, true, 0, 0, "LinkUrl", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Icon", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Icon", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("SortOrder", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "SortOrder", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Align", DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, true, 0, 0, "Align", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Place", DbType.AnsiString, 50, ParameterDirection.Input, true, 0, 0, "Place", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("IsNewWindow", DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, true, 0, 0, "IsNewWindow", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("IsVisible", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "IsVisible", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Text", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "Text", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("LinkId", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "LinkId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public SqlInt32 ParentPageId {
            set { RealWrapper.SetParameterValue("ParentPageId", value); }
        }

        public string LinkUrl {
            set { RealWrapper.SetParameterValue("LinkUrl", value); }
        }

        public byte[] Icon {
            set { RealWrapper.SetParameterValue("Icon", value); }
        }

        public SqlInt32 SortOrder {
            set { RealWrapper.SetParameterValue("SortOrder", value); }
        }

        public string Align {
            set { RealWrapper.SetParameterValue("Align", value); }
        }

        public string Place {
            set { RealWrapper.SetParameterValue("Place", value); }
        }

        public string IsNewWindow {
            set { RealWrapper.SetParameterValue("IsNewWindow", value); }
        }

        public SqlBoolean IsVisible {
            set { RealWrapper.SetParameterValue("IsVisible", value); }
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public string Text {
            set { RealWrapper.SetParameterValue("Text", value); }
        }

        public SqlInt32 LinkId {
            get {
                object val = RealWrapper.GetParameterValue("LinkId");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("LinkId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrDynamicLinkUpd : ProcedureWrapper {
        internal PrDynamicLinkUpd() : this(string.Empty) {}
        internal PrDynamicLinkUpd(string databaseInstanceName) : base(databaseInstanceName, "PrDynamicLinkUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("ParentPageId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "ParentPageId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("LinkUrl", DbType.String, 200, ParameterDirection.Input, true, 0, 0, "LinkUrl", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Icon", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Icon", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("SortOrder", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "SortOrder", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Align", DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, true, 0, 0, "Align", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Place", DbType.AnsiString, 50, ParameterDirection.Input, true, 0, 0, "Place", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("IsNewWindow", DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, true, 0, 0, "IsNewWindow", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("IsVisible", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "IsVisible", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Text", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "Text", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("LinkId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "LinkId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public SqlInt32 ParentPageId {
            set { RealWrapper.SetParameterValue("ParentPageId", value); }
        }

        public string LinkUrl {
            set { RealWrapper.SetParameterValue("LinkUrl", value); }
        }

        public byte[] Icon {
            set { RealWrapper.SetParameterValue("Icon", value); }
        }

        public SqlInt32 SortOrder {
            set { RealWrapper.SetParameterValue("SortOrder", value); }
        }

        public string Align {
            set { RealWrapper.SetParameterValue("Align", value); }
        }

        public string Place {
            set { RealWrapper.SetParameterValue("Place", value); }
        }

        public string IsNewWindow {
            set { RealWrapper.SetParameterValue("IsNewWindow", value); }
        }

        public SqlBoolean IsVisible {
            set { RealWrapper.SetParameterValue("IsVisible", value); }
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public string Text {
            set { RealWrapper.SetParameterValue("Text", value); }
        }

        public SqlInt32 LinkId {
            set { RealWrapper.SetParameterValue("LinkId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrExitPageGet : ProcedureWrapper {
        internal PrExitPageGet() : this(string.Empty) {}
        internal PrExitPageGet(string databaseInstanceName) : base(databaseInstanceName, "PrExitPageGet") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrExitPageIns : ProcedureWrapper {
        internal PrExitPageIns() : this(string.Empty) {}
        internal PrExitPageIns(string databaseInstanceName) : base(databaseInstanceName, "PrExitPageIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("HtmlText", DbType.String, 1073741823, ParameterDirection.Input, true, 0, 0, "HtmlText", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Enabled", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "Enabled", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public string HtmlText {
            set { RealWrapper.SetParameterValue("HtmlText", value); }
        }

        public SqlBoolean Enabled {
            set { RealWrapper.SetParameterValue("Enabled", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrExitPageUpd : ProcedureWrapper {
        internal PrExitPageUpd() : this(string.Empty) {}
        internal PrExitPageUpd(string databaseInstanceName) : base(databaseInstanceName, "PrExitPageUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("HtmlText", DbType.String, 1073741823, ParameterDirection.Input, true, 0, 0, "HtmlText", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Enabled", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "Enabled", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public string HtmlText {
            set { RealWrapper.SetParameterValue("HtmlText", value); }
        }

        public SqlBoolean Enabled {
            set { RealWrapper.SetParameterValue("Enabled", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrFaceBindToDevice : ProcedureWrapper {
        internal PrFaceBindToDevice() : this(string.Empty) {}
        internal PrFaceBindToDevice(string databaseInstanceName) : base(databaseInstanceName, "PrFaceBindToDevice") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FaceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FaceId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 FaceId {
            set { RealWrapper.SetParameterValue("FaceId", value); }
        }

        public SqlInt32 DeviceId {
            set { RealWrapper.SetParameterValue("DeviceId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrFaceBindToPaper : ProcedureWrapper {
        internal PrFaceBindToPaper() : this(string.Empty) {}
        internal PrFaceBindToPaper(string databaseInstanceName) : base(databaseInstanceName, "PrFaceBindToPaper") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FaceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FaceId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FaceX", DbType.Single, 0, ParameterDirection.Input, true, 0, 0, "FaceX", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FaceY", DbType.Single, 0, ParameterDirection.Input, true, 0, 0, "FaceY", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Rotation", DbType.Single, 0, ParameterDirection.Input, true, 0, 0, "Rotation", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 FaceId {
            set { RealWrapper.SetParameterValue("FaceId", value); }
        }

        public SqlInt32 PaperId {
            set { RealWrapper.SetParameterValue("PaperId", value); }
        }

        public SqlSingle FaceX {
            set { RealWrapper.SetParameterValue("FaceX", value); }
        }

        public SqlSingle FaceY {
            set { RealWrapper.SetParameterValue("FaceY", value); }
        }

        public SqlSingle Rotation {
            set { RealWrapper.SetParameterValue("Rotation", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrFaceDel : ProcedureWrapper {
        internal PrFaceDel() : this(string.Empty) {}
        internal PrFaceDel(string databaseInstanceName) : base(databaseInstanceName, "PrFaceDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FaceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FaceId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 FaceId {
            set { RealWrapper.SetParameterValue("FaceId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrFaceGetById : ProcedureWrapper {
        internal PrFaceGetById() : this(string.Empty) {}
        internal PrFaceGetById(string databaseInstanceName) : base(databaseInstanceName, "PrFaceGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FaceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FaceId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 FaceId {
            set { RealWrapper.SetParameterValue("FaceId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrFaceIns : ProcedureWrapper {
        internal PrFaceIns() : this(string.Empty) {}
        internal PrFaceIns(string databaseInstanceName) : base(databaseInstanceName, "PrFaceIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Contour", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Contour", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FaceId", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "FaceId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public byte[] Contour {
            set { RealWrapper.SetParameterValue("Contour", value); }
        }

        public SqlInt32 FaceId {
            get {
                object val = RealWrapper.GetParameterValue("FaceId");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("FaceId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrFaceLayoutEnumByPaperId : ProcedureWrapper {
        internal PrFaceLayoutEnumByPaperId() : this(string.Empty) {}
        internal PrFaceLayoutEnumByPaperId(string databaseInstanceName) : base(databaseInstanceName, "PrFaceLayoutEnumByPaperId") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 PaperId {
            set { RealWrapper.SetParameterValue("PaperId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrFaceLocalizationDel : ProcedureWrapper {
        internal PrFaceLocalizationDel() : this(string.Empty) {}
        internal PrFaceLocalizationDel(string databaseInstanceName) : base(databaseInstanceName, "PrFaceLocalizationDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FaceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FaceId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 FaceId {
            set { RealWrapper.SetParameterValue("FaceId", value); }
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrFaceLocalizationIns : ProcedureWrapper {
        internal PrFaceLocalizationIns() : this(string.Empty) {}
        internal PrFaceLocalizationIns(string databaseInstanceName) : base(databaseInstanceName, "PrFaceLocalizationIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FaceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FaceId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FaceName", DbType.String, 255, ParameterDirection.Input, true, 0, 0, "FaceName", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 FaceId {
            set { RealWrapper.SetParameterValue("FaceId", value); }
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public string FaceName {
            set { RealWrapper.SetParameterValue("FaceName", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrFaceUnbindFromDevice : ProcedureWrapper {
        internal PrFaceUnbindFromDevice() : this(string.Empty) {}
        internal PrFaceUnbindFromDevice(string databaseInstanceName) : base(databaseInstanceName, "PrFaceUnbindFromDevice") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FaceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FaceId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 FaceId {
            set { RealWrapper.SetParameterValue("FaceId", value); }
        }

        public SqlInt32 DeviceId {
            set { RealWrapper.SetParameterValue("DeviceId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrFaceUnbindFromPaper : ProcedureWrapper {
        internal PrFaceUnbindFromPaper() : this(string.Empty) {}
        internal PrFaceUnbindFromPaper(string databaseInstanceName) : base(databaseInstanceName, "PrFaceUnbindFromPaper") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FaceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FaceId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 FaceId {
            set { RealWrapper.SetParameterValue("FaceId", value); }
        }

        public SqlInt32 PaperId {
            set { RealWrapper.SetParameterValue("PaperId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrFaceUpd : ProcedureWrapper {
        internal PrFaceUpd() : this(string.Empty) {}
        internal PrFaceUpd(string databaseInstanceName) : base(databaseInstanceName, "PrFaceUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FaceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FaceId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Contour", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Contour", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 FaceId {
            set { RealWrapper.SetParameterValue("FaceId", value); }
        }

        public byte[] Contour {
            set { RealWrapper.SetParameterValue("Contour", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrFaqDel : ProcedureWrapper {
        internal PrFaqDel() : this(string.Empty) {}
        internal PrFaqDel(string databaseInstanceName) : base(databaseInstanceName, "PrFaqDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrFaqEnum : ProcedureWrapper {
        internal PrFaqEnum() : this(string.Empty) {}
        internal PrFaqEnum(string databaseInstanceName) : base(databaseInstanceName, "PrFaqEnum") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrFaqIns : ProcedureWrapper {
        internal PrFaqIns() : this(string.Empty) {}
        internal PrFaqIns(string databaseInstanceName) : base(databaseInstanceName, "PrFaqIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Question", DbType.String, 1073741823, ParameterDirection.Input, true, 0, 0, "Question", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Answer", DbType.String, 1073741823, ParameterDirection.Input, true, 0, 0, "Answer", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("SortOrder", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "SortOrder", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Question {
            set { RealWrapper.SetParameterValue("Question", value); }
        }

        public string Answer {
            set { RealWrapper.SetParameterValue("Answer", value); }
        }

        public SqlInt32 SortOrder {
            set { RealWrapper.SetParameterValue("SortOrder", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public SqlInt32 Id {
            get {
                object val = RealWrapper.GetParameterValue("Id");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("Id", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrFaqUpd : ProcedureWrapper {
        internal PrFaqUpd() : this(string.Empty) {}
        internal PrFaqUpd(string databaseInstanceName) : base(databaseInstanceName, "PrFaqUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Question", DbType.String, 1073741823, ParameterDirection.Input, true, 0, 0, "Question", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Answer", DbType.String, 1073741823, ParameterDirection.Input, true, 0, 0, "Answer", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("SortOrder", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "SortOrder", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }

        public string Question {
            set { RealWrapper.SetParameterValue("Question", value); }
        }

        public string Answer {
            set { RealWrapper.SetParameterValue("Answer", value); }
        }

        public SqlInt32 SortOrder {
            set { RealWrapper.SetParameterValue("SortOrder", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrGoogleCodeGetById : ProcedureWrapper {
        internal PrGoogleCodeGetById() : this(string.Empty) {}
        internal PrGoogleCodeGetById(string databaseInstanceName) : base(databaseInstanceName, "PrGoogleCodeGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrGoogleCodeUpd : ProcedureWrapper {
        internal PrGoogleCodeUpd() : this(string.Empty) {}
        internal PrGoogleCodeUpd(string databaseInstanceName) : base(databaseInstanceName, "PrGoogleCodeUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Code", DbType.String, 1073741823, ParameterDirection.Input, true, 0, 0, "Code", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }

        public string Code {
            set { RealWrapper.SetParameterValue("Code", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrGoogleTrackPageGetByPageUrl : ProcedureWrapper {
        internal PrGoogleTrackPageGetByPageUrl() : this(string.Empty) {}
        internal PrGoogleTrackPageGetByPageUrl(string databaseInstanceName) : base(databaseInstanceName, "PrGoogleTrackPageGetByPageUrl") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PageUrl", DbType.String, 200, ParameterDirection.Input, true, 0, 0, "PageUrl", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string PageUrl {
            set { RealWrapper.SetParameterValue("PageUrl", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrGoogleTrackPageIns : ProcedureWrapper {
        internal PrGoogleTrackPageIns() : this(string.Empty) {}
        internal PrGoogleTrackPageIns(string databaseInstanceName) : base(databaseInstanceName, "PrGoogleTrackPageIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PageUrl", DbType.String, 200, ParameterDirection.Input, true, 0, 0, "PageUrl", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EnableScript", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "EnableScript", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string PageUrl {
            set { RealWrapper.SetParameterValue("PageUrl", value); }
        }

        public SqlBoolean EnableScript {
            set { RealWrapper.SetParameterValue("EnableScript", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrGoogleTrackPageUpd : ProcedureWrapper {
        internal PrGoogleTrackPageUpd() : this(string.Empty) {}
        internal PrGoogleTrackPageUpd(string databaseInstanceName) : base(databaseInstanceName, "PrGoogleTrackPageUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PageUrl", DbType.String, 200, ParameterDirection.Input, true, 0, 0, "PageUrl", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EnableScript", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "EnableScript", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string PageUrl {
            set { RealWrapper.SetParameterValue("PageUrl", value); }
        }

        public SqlBoolean EnableScript {
            set { RealWrapper.SetParameterValue("EnableScript", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrHelpPageDel : ProcedureWrapper {
        internal PrHelpPageDel() : this(string.Empty) {}
        internal PrHelpPageDel(string databaseInstanceName) : base(databaseInstanceName, "PrHelpPageDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrHelpPageEnumByCulture : ProcedureWrapper {
        internal PrHelpPageEnumByCulture() : this(string.Empty) {}
        internal PrHelpPageEnumByCulture(string databaseInstanceName) : base(databaseInstanceName, "PrHelpPageEnumByCulture") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrHelpPageIns : ProcedureWrapper {
        internal PrHelpPageIns() : this(string.Empty) {}
        internal PrHelpPageIns(string databaseInstanceName) : base(databaseInstanceName, "PrHelpPageIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("HtmlText", DbType.AnsiString, 2147483647, ParameterDirection.Input, true, 0, 0, "HtmlText", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public string HtmlText {
            set { RealWrapper.SetParameterValue("HtmlText", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrHelpPageUpd : ProcedureWrapper {
        internal PrHelpPageUpd() : this(string.Empty) {}
        internal PrHelpPageUpd(string databaseInstanceName) : base(databaseInstanceName, "PrHelpPageUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("HtmlText", DbType.AnsiString, 2147483647, ParameterDirection.Input, true, 0, 0, "HtmlText", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public string HtmlText {
            set { RealWrapper.SetParameterValue("HtmlText", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrImageLibEnum : ProcedureWrapper {
        internal PrImageLibEnum() : this(string.Empty) {}
        internal PrImageLibEnum(string databaseInstanceName) : base(databaseInstanceName, "PrImageLibEnum") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrImageLibFolderDel : ProcedureWrapper {
        internal PrImageLibFolderDel() : this(string.Empty) {}
        internal PrImageLibFolderDel(string databaseInstanceName) : base(databaseInstanceName, "PrImageLibFolderDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FolderId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FolderId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 FolderId {
            set { RealWrapper.SetParameterValue("FolderId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrImageLibFolderGetIdByCaption : ProcedureWrapper {
        internal PrImageLibFolderGetIdByCaption() : this(string.Empty) {}
        internal PrImageLibFolderGetIdByCaption(string databaseInstanceName) : base(databaseInstanceName, "PrImageLibFolderGetIdByCaption") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Caption", DbType.String, 255, ParameterDirection.Input, true, 0, 0, "Caption", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public string Caption {
            set { RealWrapper.SetParameterValue("Caption", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrImageLibFolderIns : ProcedureWrapper {
        internal PrImageLibFolderIns() : this(string.Empty) {}
        internal PrImageLibFolderIns(string databaseInstanceName) : base(databaseInstanceName, "PrImageLibFolderIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("ParentFolderId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "ParentFolderId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Caption", DbType.String, 255, ParameterDirection.Input, true, 0, 0, "Caption", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FolderId", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "FolderId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 ParentFolderId {
            set { RealWrapper.SetParameterValue("ParentFolderId", value); }
        }

        public string Caption {
            set { RealWrapper.SetParameterValue("Caption", value); }
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public SqlInt32 FolderId {
            get {
                object val = RealWrapper.GetParameterValue("FolderId");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("FolderId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrImageLibFolderLocalizationUpd : ProcedureWrapper {
        internal PrImageLibFolderLocalizationUpd() : this(string.Empty) {}
        internal PrImageLibFolderLocalizationUpd(string databaseInstanceName) : base(databaseInstanceName, "PrImageLibFolderLocalizationUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FolderId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FolderId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Caption", DbType.String, 255, ParameterDirection.Input, true, 0, 0, "Caption", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 FolderId {
            set { RealWrapper.SetParameterValue("FolderId", value); }
        }

        public string Caption {
            set { RealWrapper.SetParameterValue("Caption", value); }
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrImageLibFolderSortOrderSwap : ProcedureWrapper {
        internal PrImageLibFolderSortOrderSwap() : this(string.Empty) {}
        internal PrImageLibFolderSortOrderSwap(string databaseInstanceName) : base(databaseInstanceName, "PrImageLibFolderSortOrderSwap") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FolderId1", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FolderId1", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FolderId2", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FolderId2", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 FolderId1 {
            set { RealWrapper.SetParameterValue("FolderId1", value); }
        }

        public SqlInt32 FolderId2 {
            set { RealWrapper.SetParameterValue("FolderId2", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrImageLibItemDel : ProcedureWrapper {
        internal PrImageLibItemDel() : this(string.Empty) {}
        internal PrImageLibItemDel(string databaseInstanceName) : base(databaseInstanceName, "PrImageLibItemDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("ItemId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "ItemId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 ItemId {
            set { RealWrapper.SetParameterValue("ItemId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrImageLibItemEnumByFolderId : ProcedureWrapper {
        internal PrImageLibItemEnumByFolderId() : this(string.Empty) {}
        internal PrImageLibItemEnumByFolderId(string databaseInstanceName) : base(databaseInstanceName, "PrImageLibItemEnumByFolderId") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FolderId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FolderId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 FolderId {
            set { RealWrapper.SetParameterValue("FolderId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrImageLibItemGetById : ProcedureWrapper {
        internal PrImageLibItemGetById() : this(string.Empty) {}
        internal PrImageLibItemGetById(string databaseInstanceName) : base(databaseInstanceName, "PrImageLibItemGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("ItemId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "ItemId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 ItemId {
            set { RealWrapper.SetParameterValue("ItemId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrImageLibItemIns : ProcedureWrapper {
        internal PrImageLibItemIns() : this(string.Empty) {}
        internal PrImageLibItemIns(string databaseInstanceName) : base(databaseInstanceName, "PrImageLibItemIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FolderId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "FolderId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("ItemId", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "ItemId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Stream", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Stream", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 FolderId {
            set { RealWrapper.SetParameterValue("FolderId", value); }
        }

        public SqlInt32 ItemId {
            get {
                object val = RealWrapper.GetParameterValue("ItemId");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("ItemId", value); }
        }

        public byte[] Stream {
            set { RealWrapper.SetParameterValue("Stream", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrIPFilterDel : ProcedureWrapper {
        internal PrIPFilterDel() : this(string.Empty) {}
        internal PrIPFilterDel(string databaseInstanceName) : base(databaseInstanceName, "PrIPFilterDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrIPFilterEnum : ProcedureWrapper {
        internal PrIPFilterEnum() : this(string.Empty) {}
        internal PrIPFilterEnum(string databaseInstanceName) : base(databaseInstanceName, "PrIPFilterEnum") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }
    }

    /// <exclude/>
    internal sealed class PrIPFilterIns : ProcedureWrapper {
        internal PrIPFilterIns() : this(string.Empty) {}
        internal PrIPFilterIns(string databaseInstanceName) : base(databaseInstanceName, "PrIPFilterIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("BeginField1", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "BeginField1", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("BeginField2", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "BeginField2", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("BeginField3", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "BeginField3", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("BeginField4", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "BeginField4", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EndField1", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "EndField1", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EndField2", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "EndField2", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EndField3", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "EndField3", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EndField4", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "EndField4", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Description", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "Description", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlByte BeginField1 {
            set { RealWrapper.SetParameterValue("BeginField1", value); }
        }

        public SqlByte BeginField2 {
            set { RealWrapper.SetParameterValue("BeginField2", value); }
        }

        public SqlByte BeginField3 {
            set { RealWrapper.SetParameterValue("BeginField3", value); }
        }

        public SqlByte BeginField4 {
            set { RealWrapper.SetParameterValue("BeginField4", value); }
        }

        public SqlByte EndField1 {
            set { RealWrapper.SetParameterValue("EndField1", value); }
        }

        public SqlByte EndField2 {
            set { RealWrapper.SetParameterValue("EndField2", value); }
        }

        public SqlByte EndField3 {
            set { RealWrapper.SetParameterValue("EndField3", value); }
        }

        public SqlByte EndField4 {
            set { RealWrapper.SetParameterValue("EndField4", value); }
        }

        public string Description {
            set { RealWrapper.SetParameterValue("Description", value); }
        }

        public SqlInt32 Id {
            get {
                object val = RealWrapper.GetParameterValue("Id");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("Id", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrIPFilterUpd : ProcedureWrapper {
        internal PrIPFilterUpd() : this(string.Empty) {}
        internal PrIPFilterUpd(string databaseInstanceName) : base(databaseInstanceName, "PrIPFilterUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("BeginField1", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "BeginField1", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("BeginField2", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "BeginField2", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("BeginField3", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "BeginField3", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("BeginField4", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "BeginField4", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EndField1", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "EndField1", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EndField2", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "EndField2", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EndField3", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "EndField3", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("EndField4", DbType.Byte, 0, ParameterDirection.Input, true, 0, 0, "EndField4", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Description", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "Description", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }

        public SqlByte BeginField1 {
            set { RealWrapper.SetParameterValue("BeginField1", value); }
        }

        public SqlByte BeginField2 {
            set { RealWrapper.SetParameterValue("BeginField2", value); }
        }

        public SqlByte BeginField3 {
            set { RealWrapper.SetParameterValue("BeginField3", value); }
        }

        public SqlByte BeginField4 {
            set { RealWrapper.SetParameterValue("BeginField4", value); }
        }

        public SqlByte EndField1 {
            set { RealWrapper.SetParameterValue("EndField1", value); }
        }

        public SqlByte EndField2 {
            set { RealWrapper.SetParameterValue("EndField2", value); }
        }

        public SqlByte EndField3 {
            set { RealWrapper.SetParameterValue("EndField3", value); }
        }

        public SqlByte EndField4 {
            set { RealWrapper.SetParameterValue("EndField4", value); }
        }

        public string Description {
            set { RealWrapper.SetParameterValue("Description", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrIsAnyDeviceForBrandAndRetailer : ProcedureWrapper {
        internal PrIsAnyDeviceForBrandAndRetailer() : this(string.Empty) {}
        internal PrIsAnyDeviceForBrandAndRetailer(string databaseInstanceName) : base(databaseInstanceName, "PrIsAnyDeviceForBrandAndRetailer") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("BrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "BrandId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperState", DbType.StringFixedLength, 16, ParameterDirection.Input, true, 0, 0, "PaperState", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Result", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "Result", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public SqlInt32 BrandId {
            set { RealWrapper.SetParameterValue("BrandId", value); }
        }

        public string PaperState {
            set { RealWrapper.SetParameterValue("PaperState", value); }
        }

        public SqlInt32 Result {
            get {
                object val = RealWrapper.GetParameterValue("Result");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("Result", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrIsAnyDevicesForCarrierAndRetailer : ProcedureWrapper {
        internal PrIsAnyDevicesForCarrierAndRetailer() : this(string.Empty) {}
        internal PrIsAnyDevicesForCarrierAndRetailer(string databaseInstanceName) : base(databaseInstanceName, "PrIsAnyDevicesForCarrierAndRetailer") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CarrierId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CarrierId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperState", DbType.StringFixedLength, 16, ParameterDirection.Input, true, 0, 0, "PaperState", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Result", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "Result", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public SqlInt32 CarrierId {
            set { RealWrapper.SetParameterValue("CarrierId", value); }
        }

        public string PaperState {
            set { RealWrapper.SetParameterValue("PaperState", value); }
        }

        public SqlInt32 Result {
            get {
                object val = RealWrapper.GetParameterValue("Result");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("Result", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrLastEditedPaperDel : ProcedureWrapper {
        internal PrLastEditedPaperDel() : this(string.Empty) {}
        internal PrLastEditedPaperDel(string databaseInstanceName) : base(databaseInstanceName, "PrLastEditedPaperDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrLastEditedPaperEnumByCustomer : ProcedureWrapper {
        internal PrLastEditedPaperEnumByCustomer() : this(string.Empty) {}
        internal PrLastEditedPaperEnumByCustomer(string databaseInstanceName) : base(databaseInstanceName, "PrLastEditedPaperEnumByCustomer") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CustomerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CustomerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 CustomerId {
            set { RealWrapper.SetParameterValue("CustomerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrLastEditedPaperIns : ProcedureWrapper {
        internal PrLastEditedPaperIns() : this(string.Empty) {}
        internal PrLastEditedPaperIns(string databaseInstanceName) : base(databaseInstanceName, "PrLastEditedPaperIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CustomerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CustomerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 CustomerId {
            set { RealWrapper.SetParameterValue("CustomerId", value); }
        }

        public SqlInt32 PaperId {
            set { RealWrapper.SetParameterValue("PaperId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrLastEditedPaperUpd : ProcedureWrapper {
        internal PrLastEditedPaperUpd() : this(string.Empty) {}
        internal PrLastEditedPaperUpd(string databaseInstanceName) : base(databaseInstanceName, "PrLastEditedPaperUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }

        public SqlInt32 PaperId {
            set { RealWrapper.SetParameterValue("PaperId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrLogoDel : ProcedureWrapper {
        internal PrLogoDel() : this(string.Empty) {}
        internal PrLogoDel(string databaseInstanceName) : base(databaseInstanceName, "PrLogoDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrLogoEnum : ProcedureWrapper {
        internal PrLogoEnum() : this(string.Empty) {}
        internal PrLogoEnum(string databaseInstanceName) : base(databaseInstanceName, "PrLogoEnum") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }
    }

    /// <exclude/>
    internal sealed class PrLogoIns : ProcedureWrapper {
        internal PrLogoIns() : this(string.Empty) {}
        internal PrLogoIns(string databaseInstanceName) : base(databaseInstanceName, "PrLogoIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Logo", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Logo", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FlashLogo", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "FlashLogo", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public byte[] Logo {
            set { RealWrapper.SetParameterValue("Logo", value); }
        }

        public byte[] FlashLogo {
            set { RealWrapper.SetParameterValue("FlashLogo", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrLogoUpd : ProcedureWrapper {
        internal PrLogoUpd() : this(string.Empty) {}
        internal PrLogoUpd(string databaseInstanceName) : base(databaseInstanceName, "PrLogoUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Logo", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Logo", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("FlashLogo", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "FlashLogo", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public byte[] Logo {
            set { RealWrapper.SetParameterValue("Logo", value); }
        }

        public byte[] FlashLogo {
            set { RealWrapper.SetParameterValue("FlashLogo", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrMailFormatEnum : ProcedureWrapper {
        internal PrMailFormatEnum() : this(string.Empty) {}
        internal PrMailFormatEnum(string databaseInstanceName) : base(databaseInstanceName, "PrMailFormatEnum") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }
    }

    /// <exclude/>
    internal sealed class PrMailFormatGetByCulture : ProcedureWrapper {
        internal PrMailFormatGetByCulture() : this(string.Empty) {}
        internal PrMailFormatGetByCulture(string databaseInstanceName) : base(databaseInstanceName, "PrMailFormatGetByCulture") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("MailId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "MailId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.AnsiString, 50, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Subject", DbType.String, 100, ParameterDirection.InputOutput, true, 0, 0, "Subject", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Body", DbType.String, 1000, ParameterDirection.InputOutput, true, 0, 0, "Body", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 MailId {
            set { RealWrapper.SetParameterValue("MailId", value); }
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public string Subject {
            get {
                return (string)RealWrapper.GetParameterValue("Subject");
            }
            set { RealWrapper.SetParameterValue("Subject", value); }
        }

        public string Body {
            get {
                return (string)RealWrapper.GetParameterValue("Body");
            }
            set { RealWrapper.SetParameterValue("Body", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrMailFormatUpd : ProcedureWrapper {
        internal PrMailFormatUpd() : this(string.Empty) {}
        internal PrMailFormatUpd(string databaseInstanceName) : base(databaseInstanceName, "PrMailFormatUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("MailId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "MailId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Subject", DbType.String, 100, ParameterDirection.Input, true, 0, 0, "Subject", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Body", DbType.String, 1000, ParameterDirection.Input, true, 0, 0, "Body", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 MailId {
            set { RealWrapper.SetParameterValue("MailId", value); }
        }

        public string Subject {
            set { RealWrapper.SetParameterValue("Subject", value); }
        }

        public string Body {
            set { RealWrapper.SetParameterValue("Body", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrOverviewPageDel : ProcedureWrapper {
        internal PrOverviewPageDel() : this(string.Empty) {}
        internal PrOverviewPageDel(string databaseInstanceName) : base(databaseInstanceName, "PrOverviewPageDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrOverviewPageEnumByCulture : ProcedureWrapper {
        internal PrOverviewPageEnumByCulture() : this(string.Empty) {}
        internal PrOverviewPageEnumByCulture(string databaseInstanceName) : base(databaseInstanceName, "PrOverviewPageEnumByCulture") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrOverviewPageIns : ProcedureWrapper {
        internal PrOverviewPageIns() : this(string.Empty) {}
        internal PrOverviewPageIns(string databaseInstanceName) : base(databaseInstanceName, "PrOverviewPageIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("HtmlText", DbType.AnsiString, 2147483647, ParameterDirection.Input, true, 0, 0, "HtmlText", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public string HtmlText {
            set { RealWrapper.SetParameterValue("HtmlText", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrOverviewPageUpd : ProcedureWrapper {
        internal PrOverviewPageUpd() : this(string.Empty) {}
        internal PrOverviewPageUpd(string databaseInstanceName) : base(databaseInstanceName, "PrOverviewPageUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("HtmlText", DbType.AnsiString, 2147483647, ParameterDirection.Input, true, 0, 0, "HtmlText", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public string HtmlText {
            set { RealWrapper.SetParameterValue("HtmlText", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPageEnumByUrl : ProcedureWrapper {
        internal PrPageEnumByUrl() : this(string.Empty) {}
        internal PrPageEnumByUrl(string databaseInstanceName) : base(databaseInstanceName, "PrPageEnumByUrl") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Url", DbType.String, 200, ParameterDirection.Input, true, 0, 0, "Url", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public string Url {
            set { RealWrapper.SetParameterValue("Url", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPageIns : ProcedureWrapper {
        internal PrPageIns() : this(string.Empty) {}
        internal PrPageIns(string databaseInstanceName) : base(databaseInstanceName, "PrPageIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PageUrl", DbType.String, 200, ParameterDirection.Input, true, 0, 0, "PageUrl", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Description", DbType.String, 50, ParameterDirection.Input, true, 0, 0, "Description", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PageId", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "PageId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public string PageUrl {
            set { RealWrapper.SetParameterValue("PageUrl", value); }
        }

        public string Description {
            set { RealWrapper.SetParameterValue("Description", value); }
        }

        public SqlInt32 PageId {
            get {
                object val = RealWrapper.GetParameterValue("PageId");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("PageId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperAddToRetailer : ProcedureWrapper {
        internal PrPaperAddToRetailer() : this(string.Empty) {}
        internal PrPaperAddToRetailer(string databaseInstanceName) : base(databaseInstanceName, "PrPaperAddToRetailer") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 PaperId {
            set { RealWrapper.SetParameterValue("PaperId", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperBrandDel : ProcedureWrapper {
        internal PrPaperBrandDel() : this(string.Empty) {}
        internal PrPaperBrandDel(string databaseInstanceName) : base(databaseInstanceName, "PrPaperBrandDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperBrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperBrandId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 PaperBrandId {
            set { RealWrapper.SetParameterValue("PaperBrandId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperBrandGetById : ProcedureWrapper {
        internal PrPaperBrandGetById() : this(string.Empty) {}
        internal PrPaperBrandGetById(string databaseInstanceName) : base(databaseInstanceName, "PrPaperBrandGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperBrandIconGetById : ProcedureWrapper {
        internal PrPaperBrandIconGetById() : this(string.Empty) {}
        internal PrPaperBrandIconGetById(string databaseInstanceName) : base(databaseInstanceName, "PrPaperBrandIconGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperBrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperBrandId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 PaperBrandId {
            set { RealWrapper.SetParameterValue("PaperBrandId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperBrandIconUpd : ProcedureWrapper {
        internal PrPaperBrandIconUpd() : this(string.Empty) {}
        internal PrPaperBrandIconUpd(string databaseInstanceName) : base(databaseInstanceName, "PrPaperBrandIconUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperBrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperBrandId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Icon", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Icon", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 PaperBrandId {
            set { RealWrapper.SetParameterValue("PaperBrandId", value); }
        }

        public byte[] Icon {
            set { RealWrapper.SetParameterValue("Icon", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperBrandIns : ProcedureWrapper {
        internal PrPaperBrandIns() : this(string.Empty) {}
        internal PrPaperBrandIns(string databaseInstanceName) : base(databaseInstanceName, "PrPaperBrandIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Name", DbType.String, 30, ParameterDirection.Input, true, 0, 0, "Name", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperBrandId", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "PaperBrandId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Name {
            set { RealWrapper.SetParameterValue("Name", value); }
        }

        public SqlInt32 PaperBrandId {
            get {
                object val = RealWrapper.GetParameterValue("PaperBrandId");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("PaperBrandId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperBrandsEnumByDeviceTypeId : ProcedureWrapper {
        internal PrPaperBrandsEnumByDeviceTypeId() : this(string.Empty) {}
        internal PrPaperBrandsEnumByDeviceTypeId(string databaseInstanceName) : base(databaseInstanceName, "PrPaperBrandsEnumByDeviceTypeId") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceTypeId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceTypeId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 DeviceTypeId {
            set { RealWrapper.SetParameterValue("DeviceTypeId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperBrandsEnumerate : ProcedureWrapper {
        internal PrPaperBrandsEnumerate() : this(string.Empty) {}
        internal PrPaperBrandsEnumerate(string databaseInstanceName) : base(databaseInstanceName, "PrPaperBrandsEnumerate") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }
    }

    /// <exclude/>
    internal sealed class PrPaperBrandUpd : ProcedureWrapper {
        internal PrPaperBrandUpd() : this(string.Empty) {}
        internal PrPaperBrandUpd(string databaseInstanceName) : base(databaseInstanceName, "PrPaperBrandUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperBrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperBrandId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Name", DbType.String, 30, ParameterDirection.Input, true, 0, 0, "Name", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 PaperBrandId {
            set { RealWrapper.SetParameterValue("PaperBrandId", value); }
        }

        public string Name {
            set { RealWrapper.SetParameterValue("Name", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperDel : ProcedureWrapper {
        internal PrPaperDel() : this(string.Empty) {}
        internal PrPaperDel(string databaseInstanceName) : base(databaseInstanceName, "PrPaperDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 PaperId {
            set { RealWrapper.SetParameterValue("PaperId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperEnumByParam : ProcedureWrapper {
        internal PrPaperEnumByParam() : this(string.Empty) {}
        internal PrPaperEnumByParam(string databaseInstanceName) : base(databaseInstanceName, "PrPaperEnumByParam") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperBrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperBrandId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceTypeId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceTypeId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "DeviceId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperName", DbType.String, 64, ParameterDirection.Input, true, 0, 0, "PaperName", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperState", DbType.StringFixedLength, 16, ParameterDirection.Input, true, 0, 0, "PaperState", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 PaperBrandId {
            set { RealWrapper.SetParameterValue("PaperBrandId", value); }
        }

        public SqlInt32 DeviceTypeId {
            set { RealWrapper.SetParameterValue("DeviceTypeId", value); }
        }

        public SqlInt32 DeviceId {
            set { RealWrapper.SetParameterValue("DeviceId", value); }
        }

        public string PaperName {
            set { RealWrapper.SetParameterValue("PaperName", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public string PaperState {
            set { RealWrapper.SetParameterValue("PaperState", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperGetById : ProcedureWrapper {
        internal PrPaperGetById() : this(string.Empty) {}
        internal PrPaperGetById(string databaseInstanceName) : base(databaseInstanceName, "PrPaperGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 PaperId {
            set { RealWrapper.SetParameterValue("PaperId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperIconGetById : ProcedureWrapper {
        internal PrPaperIconGetById() : this(string.Empty) {}
        internal PrPaperIconGetById(string databaseInstanceName) : base(databaseInstanceName, "PrPaperIconGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 PaperId {
            set { RealWrapper.SetParameterValue("PaperId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperIconUpd : ProcedureWrapper {
        internal PrPaperIconUpd() : this(string.Empty) {}
        internal PrPaperIconUpd(string databaseInstanceName) : base(databaseInstanceName, "PrPaperIconUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Icon", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Icon", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 PaperId {
            set { RealWrapper.SetParameterValue("PaperId", value); }
        }

        public byte[] Icon {
            set { RealWrapper.SetParameterValue("Icon", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperIns : ProcedureWrapper {
        internal PrPaperIns() : this(string.Empty) {}
        internal PrPaperIns(string databaseInstanceName) : base(databaseInstanceName, "PrPaperIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Name", DbType.String, 64, ParameterDirection.Input, true, 0, 0, "Name", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperBrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperBrandId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Width", DbType.Single, 0, ParameterDirection.Input, true, 0, 0, "Width", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Height", DbType.Single, 0, ParameterDirection.Input, true, 0, 0, "Height", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperType", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperType", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperState", DbType.StringFixedLength, 1, ParameterDirection.Input, true, 0, 0, "PaperState", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperId", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "PaperId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Name {
            set { RealWrapper.SetParameterValue("Name", value); }
        }

        public SqlInt32 PaperBrandId {
            set { RealWrapper.SetParameterValue("PaperBrandId", value); }
        }

        public SqlSingle Width {
            set { RealWrapper.SetParameterValue("Width", value); }
        }

        public SqlSingle Height {
            set { RealWrapper.SetParameterValue("Height", value); }
        }

        public SqlInt32 PaperType {
            set { RealWrapper.SetParameterValue("PaperType", value); }
        }

        public string PaperState {
            set { RealWrapper.SetParameterValue("PaperState", value); }
        }

        public SqlInt32 PaperId {
            get {
                object val = RealWrapper.GetParameterValue("PaperId");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("PaperId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperRemoveFromRetailer : ProcedureWrapper {
        internal PrPaperRemoveFromRetailer() : this(string.Empty) {}
        internal PrPaperRemoveFromRetailer(string databaseInstanceName) : base(databaseInstanceName, "PrPaperRemoveFromRetailer") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public SqlInt32 PaperId {
            set { RealWrapper.SetParameterValue("PaperId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperToRetailerEnum : ProcedureWrapper {
        internal PrPaperToRetailerEnum() : this(string.Empty) {}
        internal PrPaperToRetailerEnum(string databaseInstanceName) : base(databaseInstanceName, "PrPaperToRetailerEnum") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPaperTypeEnum : ProcedureWrapper {
        internal PrPaperTypeEnum() : this(string.Empty) {}
        internal PrPaperTypeEnum(string databaseInstanceName) : base(databaseInstanceName, "PrPaperTypeEnum") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }
    }

    /// <exclude/>
    internal sealed class PrPaperUpd : ProcedureWrapper {
        internal PrPaperUpd() : this(string.Empty) {}
        internal PrPaperUpd(string databaseInstanceName) : base(databaseInstanceName, "PrPaperUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Name", DbType.String, 64, ParameterDirection.Input, true, 0, 0, "Name", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperBrandId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperBrandId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Width", DbType.Single, 0, ParameterDirection.Input, true, 0, 0, "Width", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Height", DbType.Single, 0, ParameterDirection.Input, true, 0, 0, "Height", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperType", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperType", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperState", DbType.StringFixedLength, 1, ParameterDirection.Input, true, 0, 0, "PaperState", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 PaperId {
            set { RealWrapper.SetParameterValue("PaperId", value); }
        }

        public string Name {
            set { RealWrapper.SetParameterValue("Name", value); }
        }

        public SqlInt32 PaperBrandId {
            set { RealWrapper.SetParameterValue("PaperBrandId", value); }
        }

        public SqlSingle Width {
            set { RealWrapper.SetParameterValue("Width", value); }
        }

        public SqlSingle Height {
            set { RealWrapper.SetParameterValue("Height", value); }
        }

        public SqlInt32 PaperType {
            set { RealWrapper.SetParameterValue("PaperType", value); }
        }

        public string PaperState {
            set { RealWrapper.SetParameterValue("PaperState", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPlacePropertyDel : ProcedureWrapper {
        internal PrPlacePropertyDel() : this(string.Empty) {}
        internal PrPlacePropertyDel(string databaseInstanceName) : base(databaseInstanceName, "PrPlacePropertyDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Place", DbType.AnsiString, 50, ParameterDirection.Input, true, 0, 0, "Place", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public string Place {
            set { RealWrapper.SetParameterValue("Place", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPlacePropertyEnum : ProcedureWrapper {
        internal PrPlacePropertyEnum() : this(string.Empty) {}
        internal PrPlacePropertyEnum(string databaseInstanceName) : base(databaseInstanceName, "PrPlacePropertyEnum") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }
    }

    /// <exclude/>
    internal sealed class PrPlacePropertyIns : ProcedureWrapper {
        internal PrPlacePropertyIns() : this(string.Empty) {}
        internal PrPlacePropertyIns(string databaseInstanceName) : base(databaseInstanceName, "PrPlacePropertyIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Place", DbType.AnsiString, 50, ParameterDirection.Input, true, 0, 0, "Place", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("BackgroundColor", DbType.AnsiString, 50, ParameterDirection.Input, true, 0, 0, "BackgroundColor", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public string Place {
            set { RealWrapper.SetParameterValue("Place", value); }
        }

        public string BackgroundColor {
            set { RealWrapper.SetParameterValue("BackgroundColor", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrPlacePropertyUpd : ProcedureWrapper {
        internal PrPlacePropertyUpd() : this(string.Empty) {}
        internal PrPlacePropertyUpd(string databaseInstanceName) : base(databaseInstanceName, "PrPlacePropertyUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Place", DbType.AnsiString, 50, ParameterDirection.Input, true, 0, 0, "Place", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("BackgroundColor", DbType.AnsiString, 50, ParameterDirection.Input, true, 0, 0, "BackgroundColor", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public string Place {
            set { RealWrapper.SetParameterValue("Place", value); }
        }

        public string BackgroundColor {
            set { RealWrapper.SetParameterValue("BackgroundColor", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrRetailerBuyNowDel : ProcedureWrapper {
        internal PrRetailerBuyNowDel() : this(string.Empty) {}
        internal PrRetailerBuyNowDel(string databaseInstanceName) : base(databaseInstanceName, "PrRetailerBuyNowDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("VisibleRetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "VisibleRetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public SqlInt32 VisibleRetailerId {
            set { RealWrapper.SetParameterValue("VisibleRetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrRetailerBuyNowEnum : ProcedureWrapper {
        internal PrRetailerBuyNowEnum() : this(string.Empty) {}
        internal PrRetailerBuyNowEnum(string databaseInstanceName) : base(databaseInstanceName, "PrRetailerBuyNowEnum") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrRetailerBuyNowIns : ProcedureWrapper {
        internal PrRetailerBuyNowIns() : this(string.Empty) {}
        internal PrRetailerBuyNowIns(string databaseInstanceName) : base(databaseInstanceName, "PrRetailerBuyNowIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("VisibleRetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "VisibleRetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }

        public SqlInt32 VisibleRetailerId {
            set { RealWrapper.SetParameterValue("VisibleRetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrRetailersDel : ProcedureWrapper {
        internal PrRetailersDel() : this(string.Empty) {}
        internal PrRetailersDel(string databaseInstanceName) : base(databaseInstanceName, "PrRetailersDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrRetailersEnum : ProcedureWrapper {
        internal PrRetailersEnum() : this(string.Empty) {}
        internal PrRetailersEnum(string databaseInstanceName) : base(databaseInstanceName, "PrRetailersEnum") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }
    }

    /// <exclude/>
    internal sealed class PrRetailersGetByCarrierId : ProcedureWrapper {
        internal PrRetailersGetByCarrierId() : this(string.Empty) {}
        internal PrRetailersGetByCarrierId(string databaseInstanceName) : base(databaseInstanceName, "PrRetailersGetByCarrierId") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("CarrierId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "CarrierId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 CarrierId {
            set { RealWrapper.SetParameterValue("CarrierId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrRetailersGetByPaperId : ProcedureWrapper {
        internal PrRetailersGetByPaperId() : this(string.Empty) {}
        internal PrRetailersGetByPaperId(string databaseInstanceName) : base(databaseInstanceName, "PrRetailersGetByPaperId") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PaperId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PaperId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 PaperId {
            set { RealWrapper.SetParameterValue("PaperId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrRetailersGetIconById : ProcedureWrapper {
        internal PrRetailersGetIconById() : this(string.Empty) {}
        internal PrRetailersGetIconById(string databaseInstanceName) : base(databaseInstanceName, "PrRetailersGetIconById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrRetailersIconUpd : ProcedureWrapper {
        internal PrRetailersIconUpd() : this(string.Empty) {}
        internal PrRetailersIconUpd(string databaseInstanceName) : base(databaseInstanceName, "PrRetailersIconUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Icon", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Icon", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }

        public byte[] Icon {
            set { RealWrapper.SetParameterValue("Icon", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrRetailersIns : ProcedureWrapper {
        internal PrRetailersIns() : this(string.Empty) {}
        internal PrRetailersIns(string databaseInstanceName) : base(databaseInstanceName, "PrRetailersIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Name", DbType.AnsiString, 50, ParameterDirection.Input, true, 0, 0, "Name", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DisplayName", DbType.String, 50, ParameterDirection.Input, true, 0, 0, "DisplayName", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Icon", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Icon", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Url", DbType.AnsiString, 200, ParameterDirection.Input, true, 0, 0, "Url", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PhoneAvailable", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PhoneAvailable", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Mp3Available", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Mp3Available", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("StickerAvailable", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "StickerAvailable", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("HasBrandedSite", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "HasBrandedSite", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Name {
            set { RealWrapper.SetParameterValue("Name", value); }
        }

        public string DisplayName {
            set { RealWrapper.SetParameterValue("DisplayName", value); }
        }

        public byte[] Icon {
            set { RealWrapper.SetParameterValue("Icon", value); }
        }

        public string Url {
            set { RealWrapper.SetParameterValue("Url", value); }
        }

        public SqlInt32 PhoneAvailable {
            set { RealWrapper.SetParameterValue("PhoneAvailable", value); }
        }

        public SqlInt32 Mp3Available {
            set { RealWrapper.SetParameterValue("Mp3Available", value); }
        }

        public SqlInt32 StickerAvailable {
            set { RealWrapper.SetParameterValue("StickerAvailable", value); }
        }

        public SqlBoolean HasBrandedSite {
            set { RealWrapper.SetParameterValue("HasBrandedSite", value); }
        }

        public SqlInt32 Id {
            get {
                object val = RealWrapper.GetParameterValue("Id");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("Id", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrRetailersUpd : ProcedureWrapper {
        internal PrRetailersUpd() : this(string.Empty) {}
        internal PrRetailersUpd(string databaseInstanceName) : base(databaseInstanceName, "PrRetailersUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Name", DbType.AnsiString, 50, ParameterDirection.Input, true, 0, 0, "Name", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DisplayName", DbType.String, 50, ParameterDirection.Input, true, 0, 0, "DisplayName", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Url", DbType.AnsiString, 200, ParameterDirection.Input, true, 0, 0, "Url", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("PhoneAvailable", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "PhoneAvailable", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Mp3Available", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "Mp3Available", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("StickerAvailable", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "StickerAvailable", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("HasBrandedSite", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "HasBrandedSite", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            set { RealWrapper.SetParameterValue("Id", value); }
        }

        public string Name {
            set { RealWrapper.SetParameterValue("Name", value); }
        }

        public string DisplayName {
            set { RealWrapper.SetParameterValue("DisplayName", value); }
        }

        public string Url {
            set { RealWrapper.SetParameterValue("Url", value); }
        }

        public SqlInt32 PhoneAvailable {
            set { RealWrapper.SetParameterValue("PhoneAvailable", value); }
        }

        public SqlInt32 Mp3Available {
            set { RealWrapper.SetParameterValue("Mp3Available", value); }
        }

        public SqlInt32 StickerAvailable {
            set { RealWrapper.SetParameterValue("StickerAvailable", value); }
        }

        public SqlBoolean HasBrandedSite {
            set { RealWrapper.SetParameterValue("HasBrandedSite", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrSpecialUserDel : ProcedureWrapper {
        internal PrSpecialUserDel() : this(string.Empty) {}
        internal PrSpecialUserDel(string databaseInstanceName) : base(databaseInstanceName, "PrSpecialUserDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("SpecialUserId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "SpecialUserId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 SpecialUserId {
            set { RealWrapper.SetParameterValue("SpecialUserId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrSpecialUserEnumByParam : ProcedureWrapper {
        internal PrSpecialUserEnumByParam() : this(string.Empty) {}
        internal PrSpecialUserEnumByParam(string databaseInstanceName) : base(databaseInstanceName, "PrSpecialUserEnumByParam") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Email", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "Email", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("ShowPaper", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "ShowPaper", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("NotifyPaperState", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "NotifyPaperState", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Email {
            set { RealWrapper.SetParameterValue("Email", value); }
        }

        public SqlBoolean ShowPaper {
            set { RealWrapper.SetParameterValue("ShowPaper", value); }
        }

        public SqlBoolean NotifyPaperState {
            set { RealWrapper.SetParameterValue("NotifyPaperState", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrSpecialUserGetById : ProcedureWrapper {
        internal PrSpecialUserGetById() : this(string.Empty) {}
        internal PrSpecialUserGetById(string databaseInstanceName) : base(databaseInstanceName, "PrSpecialUserGetById") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("SpecialUserId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "SpecialUserId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 SpecialUserId {
            set { RealWrapper.SetParameterValue("SpecialUserId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrSpecialUserIns : ProcedureWrapper {
        internal PrSpecialUserIns() : this(string.Empty) {}
        internal PrSpecialUserIns(string databaseInstanceName) : base(databaseInstanceName, "PrSpecialUserIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Email", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "Email", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("ShowPaper", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "ShowPaper", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("NotifyPaperState", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "NotifyPaperState", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("SpecialUserId", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "SpecialUserId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Email {
            set { RealWrapper.SetParameterValue("Email", value); }
        }

        public SqlBoolean ShowPaper {
            set { RealWrapper.SetParameterValue("ShowPaper", value); }
        }

        public SqlBoolean NotifyPaperState {
            set { RealWrapper.SetParameterValue("NotifyPaperState", value); }
        }

        public SqlInt32 SpecialUserId {
            get {
                object val = RealWrapper.GetParameterValue("SpecialUserId");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("SpecialUserId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrSpecialUserUpd : ProcedureWrapper {
        internal PrSpecialUserUpd() : this(string.Empty) {}
        internal PrSpecialUserUpd(string databaseInstanceName) : base(databaseInstanceName, "PrSpecialUserUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("SpecialUserId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "SpecialUserId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Email", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "Email", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("ShowPaper", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "ShowPaper", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("NotifyPaperState", DbType.Boolean, 0, ParameterDirection.Input, true, 0, 0, "NotifyPaperState", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 SpecialUserId {
            set { RealWrapper.SetParameterValue("SpecialUserId", value); }
        }

        public string Email {
            set { RealWrapper.SetParameterValue("Email", value); }
        }

        public SqlBoolean ShowPaper {
            set { RealWrapper.SetParameterValue("ShowPaper", value); }
        }

        public SqlBoolean NotifyPaperState {
            set { RealWrapper.SetParameterValue("NotifyPaperState", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrSupportPageDel : ProcedureWrapper {
        internal PrSupportPageDel() : this(string.Empty) {}
        internal PrSupportPageDel(string databaseInstanceName) : base(databaseInstanceName, "PrSupportPageDel") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrSupportPageEnumByCulture : ProcedureWrapper {
        internal PrSupportPageEnumByCulture() : this(string.Empty) {}
        internal PrSupportPageEnumByCulture(string databaseInstanceName) : base(databaseInstanceName, "PrSupportPageEnumByCulture") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrSupportPageIns : ProcedureWrapper {
        internal PrSupportPageIns() : this(string.Empty) {}
        internal PrSupportPageIns(string databaseInstanceName) : base(databaseInstanceName, "PrSupportPageIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("HtmlText", DbType.AnsiString, 2147483647, ParameterDirection.Input, true, 0, 0, "HtmlText", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public string HtmlText {
            set { RealWrapper.SetParameterValue("HtmlText", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrSupportPageUpd : ProcedureWrapper {
        internal PrSupportPageUpd() : this(string.Empty) {}
        internal PrSupportPageUpd(string databaseInstanceName) : base(databaseInstanceName, "PrSupportPageUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("HtmlText", DbType.AnsiString, 2147483647, ParameterDirection.Input, true, 0, 0, "HtmlText", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public string HtmlText {
            set { RealWrapper.SetParameterValue("HtmlText", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrTellAFriendRefusedUserGet : ProcedureWrapper {
        internal PrTellAFriendRefusedUserGet() : this(string.Empty) {}
        internal PrTellAFriendRefusedUserGet(string databaseInstanceName) : base(databaseInstanceName, "PrTellAFriendRefusedUserGet") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Email", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "Email", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Email {
            set { RealWrapper.SetParameterValue("Email", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrTellAFriendRefusedUserIns : ProcedureWrapper {
        internal PrTellAFriendRefusedUserIns() : this(string.Empty) {}
        internal PrTellAFriendRefusedUserIns(string databaseInstanceName) : base(databaseInstanceName, "PrTellAFriendRefusedUserIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Email", DbType.AnsiString, 100, ParameterDirection.Input, true, 0, 0, "Email", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Email {
            set { RealWrapper.SetParameterValue("Email", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrTermsOfUsePageEnumByCulture : ProcedureWrapper {
        internal PrTermsOfUsePageEnumByCulture() : this(string.Empty) {}
        internal PrTermsOfUsePageEnumByCulture(string databaseInstanceName) : base(databaseInstanceName, "PrTermsOfUsePageEnumByCulture") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrTermsOfUsePageIns : ProcedureWrapper {
        internal PrTermsOfUsePageIns() : this(string.Empty) {}
        internal PrTermsOfUsePageIns(string databaseInstanceName) : base(databaseInstanceName, "PrTermsOfUsePageIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("HtmlText", DbType.AnsiString, 2147483647, ParameterDirection.Input, true, 0, 0, "HtmlText", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public string HtmlText {
            set { RealWrapper.SetParameterValue("HtmlText", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrTermsOfUsePageUpd : ProcedureWrapper {
        internal PrTermsOfUsePageUpd() : this(string.Empty) {}
        internal PrTermsOfUsePageUpd(string databaseInstanceName) : base(databaseInstanceName, "PrTermsOfUsePageUpd") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Culture", DbType.String, 10, ParameterDirection.Input, true, 0, 0, "Culture", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("HtmlText", DbType.AnsiString, 2147483647, ParameterDirection.Input, true, 0, 0, "HtmlText", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string Culture {
            set { RealWrapper.SetParameterValue("Culture", value); }
        }

        public string HtmlText {
            set { RealWrapper.SetParameterValue("HtmlText", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrTestImageLibItemIns : ProcedureWrapper {
        internal PrTestImageLibItemIns() : this(string.Empty) {}
        internal PrTestImageLibItemIns(string databaseInstanceName) : base(databaseInstanceName, "PrTestImageLibItemIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Stream", DbType.Binary, 2147483647, ParameterDirection.Input, true, 0, 0, "Stream", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            get {
                object val = RealWrapper.GetParameterValue("Id");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("Id", value); }
        }

        public byte[] Stream {
            set { RealWrapper.SetParameterValue("Stream", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrTrackingDevicesEnumBrands : ProcedureWrapper {
        internal PrTrackingDevicesEnumBrands() : this(string.Empty) {}
        internal PrTrackingDevicesEnumBrands(string databaseInstanceName) : base(databaseInstanceName, "PrTrackingDevicesEnumBrands") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrTrackingDevicesEnumByTimeAndBrand : ProcedureWrapper {
        internal PrTrackingDevicesEnumByTimeAndBrand() : this(string.Empty) {}
        internal PrTrackingDevicesEnumByTimeAndBrand(string databaseInstanceName) : base(databaseInstanceName, "PrTrackingDevicesEnumByTimeAndBrand") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceBrand", DbType.AnsiString, 200, ParameterDirection.Input, true, 0, 0, "DeviceBrand", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("TimeStart", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "TimeStart", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("TimeEnd", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "TimeEnd", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public string DeviceBrand {
            set { RealWrapper.SetParameterValue("DeviceBrand", value); }
        }

        public SqlDateTime TimeStart {
            set { RealWrapper.SetParameterValue("TimeStart", value); }
        }

        public SqlDateTime TimeEnd {
            set { RealWrapper.SetParameterValue("TimeEnd", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrTrackingDevicesIns : ProcedureWrapper {
        internal PrTrackingDevicesIns() : this(string.Empty) {}
        internal PrTrackingDevicesIns(string databaseInstanceName) : base(databaseInstanceName, "PrTrackingDevicesIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("DeviceBrand", DbType.AnsiString, 200, ParameterDirection.Input, true, 0, 0, "DeviceBrand", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Device", DbType.AnsiString, 200, ParameterDirection.Input, true, 0, 0, "Device", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Login", DbType.AnsiString, 200, ParameterDirection.Input, true, 0, 0, "Login", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Time", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "Time", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            get {
                object val = RealWrapper.GetParameterValue("Id");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("Id", value); }
        }

        public string DeviceBrand {
            set { RealWrapper.SetParameterValue("DeviceBrand", value); }
        }

        public string Device {
            set { RealWrapper.SetParameterValue("Device", value); }
        }

        public string Login {
            set { RealWrapper.SetParameterValue("Login", value); }
        }

        public SqlDateTime Time {
            set { RealWrapper.SetParameterValue("Time", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrTrackingFeaturesEnumByTime : ProcedureWrapper {
        internal PrTrackingFeaturesEnumByTime() : this(string.Empty) {}
        internal PrTrackingFeaturesEnumByTime(string databaseInstanceName) : base(databaseInstanceName, "PrTrackingFeaturesEnumByTime") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("TimeStart", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "TimeStart", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("TimeEnd", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "TimeEnd", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlDateTime TimeStart {
            set { RealWrapper.SetParameterValue("TimeStart", value); }
        }

        public SqlDateTime TimeEnd {
            set { RealWrapper.SetParameterValue("TimeEnd", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrTrackingFeaturesIns : ProcedureWrapper {
        internal PrTrackingFeaturesIns() : this(string.Empty) {}
        internal PrTrackingFeaturesIns(string databaseInstanceName) : base(databaseInstanceName, "PrTrackingFeaturesIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Action", DbType.AnsiString, 200, ParameterDirection.Input, true, 0, 0, "Action", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Login", DbType.AnsiString, 200, ParameterDirection.Input, true, 0, 0, "Login", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Time", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "Time", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            get {
                object val = RealWrapper.GetParameterValue("Id");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("Id", value); }
        }

        public string Action {
            set { RealWrapper.SetParameterValue("Action", value); }
        }

        public string Login {
            set { RealWrapper.SetParameterValue("Login", value); }
        }

        public SqlDateTime Time {
            set { RealWrapper.SetParameterValue("Time", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrTrackingImagesEnumByTime : ProcedureWrapper {
        internal PrTrackingImagesEnumByTime() : this(string.Empty) {}
        internal PrTrackingImagesEnumByTime(string databaseInstanceName) : base(databaseInstanceName, "PrTrackingImagesEnumByTime") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("TimeStart", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "TimeStart", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("TimeEnd", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "TimeEnd", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlDateTime TimeStart {
            set { RealWrapper.SetParameterValue("TimeStart", value); }
        }

        public SqlDateTime TimeEnd {
            set { RealWrapper.SetParameterValue("TimeEnd", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrTrackingImagesIns : ProcedureWrapper {
        internal PrTrackingImagesIns() : this(string.Empty) {}
        internal PrTrackingImagesIns(string databaseInstanceName) : base(databaseInstanceName, "PrTrackingImagesIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("ImageId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "ImageId", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Login", DbType.AnsiString, 200, ParameterDirection.Input, true, 0, 0, "Login", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Time", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "Time", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            get {
                object val = RealWrapper.GetParameterValue("Id");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("Id", value); }
        }

        public SqlInt32 ImageId {
            set { RealWrapper.SetParameterValue("ImageId", value); }
        }

        public string Login {
            set { RealWrapper.SetParameterValue("Login", value); }
        }

        public SqlDateTime Time {
            set { RealWrapper.SetParameterValue("Time", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrTrackingUserActionEnum : ProcedureWrapper {
        internal PrTrackingUserActionEnum() : this(string.Empty) {}
        internal PrTrackingUserActionEnum(string databaseInstanceName) : base(databaseInstanceName, "PrTrackingUserActionEnum") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }

    /// <exclude/>
    internal sealed class PrTrackingUserActionIns : ProcedureWrapper {
        internal PrTrackingUserActionIns() : this(string.Empty) {}
        internal PrTrackingUserActionIns(string databaseInstanceName) : base(databaseInstanceName, "PrTrackingUserActionIns") {}
         
        protected override void InitializeParameters() {
            RealWrapper.AddParameter("RETURN_VALUE", DbType.Int32, 0, ParameterDirection.ReturnValue, true, 0, 0, "RETURN_VALUE", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Id", DbType.Int32, 0, ParameterDirection.InputOutput, true, 0, 0, "Id", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Action", DbType.AnsiString, 200, ParameterDirection.Input, true, 0, 0, "Action", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Login", DbType.AnsiString, 200, ParameterDirection.Input, true, 0, 0, "Login", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("Time", DbType.DateTime, 0, ParameterDirection.Input, true, 0, 0, "Time", DataRowVersion.Default, DBNull.Value);
            RealWrapper.AddParameter("RetailerId", DbType.Int32, 0, ParameterDirection.Input, true, 0, 0, "RetailerId", DataRowVersion.Default, DBNull.Value);
        }

        public SqlInt32 ReturnValue {
            get {
                object val = RealWrapper.GetParameterValue("RETURN_VALUE");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            
        }

        public SqlInt32 Id {
            get {
                object val = RealWrapper.GetParameterValue("Id");
                return (val == null) ? SqlInt32.Null : (SqlInt32)(Int32)val;
            }
            set { RealWrapper.SetParameterValue("Id", value); }
        }

        public string Action {
            set { RealWrapper.SetParameterValue("Action", value); }
        }

        public string Login {
            set { RealWrapper.SetParameterValue("Login", value); }
        }

        public SqlDateTime Time {
            set { RealWrapper.SetParameterValue("Time", value); }
        }

        public SqlInt32 RetailerId {
            set { RealWrapper.SetParameterValue("RetailerId", value); }
        }
    }
}