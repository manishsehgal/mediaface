using System;
using System.Collections;
using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data {
	public class DOPaperBrand {
		public DOPaperBrand() {}

        public static PaperBrand GetPaperBrand(PaperBrandBase paperBrand) {
            PrPaperBrandGetById prc = new PrPaperBrandGetById();
            prc.Id = paperBrand.Id;
            PaperBrand resultBrand = null;

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int nameColumnIndex = reader.GetOrdinal("Name");
                if (reader.Read()) {
                    resultBrand = new PaperBrand();
                    resultBrand.Id = reader.GetInt32(idColumnIndex);
                    resultBrand.Name = reader.GetString(nameColumnIndex);
                }
            }
            return resultBrand;
        }

        public static PaperBrand[] EnumeratePaperBrands() {
            PrPaperBrandsEnumerate prc = new PrPaperBrandsEnumerate();

            ArrayList paperBrands = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int idColumnName  = reader.GetOrdinal("Name");
                while (reader.Read()) {
                    PaperBrand paperBrand = new PaperBrand();
                    paperBrand.Id   = reader.GetInt32(idColumnIndex);
                    paperBrand.Name = reader.GetString(idColumnName);
                    paperBrands.Add(paperBrand);
                }
            }
            return (PaperBrand[])paperBrands.ToArray(typeof(PaperBrand));
        }

        public static PaperBrand[] EnumeratePaperBrandsByDeviceType(int idDeviceType) {
            PrPaperBrandsEnumByDeviceTypeId prc = new PrPaperBrandsEnumByDeviceTypeId();

            prc.DeviceTypeId = idDeviceType;

            ArrayList paperBrands = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int idColumnName  = reader.GetOrdinal("Name");
                while (reader.Read()) {
                    PaperBrand paperBrand = new PaperBrand();
                    paperBrand.Id   = reader.GetInt32(idColumnIndex);
                    paperBrand.Name = reader.GetString(idColumnName);
                    paperBrands.Add(paperBrand);
                }
            }
            return (PaperBrand[])paperBrands.ToArray(typeof(PaperBrand));
        }

        public static byte[] GetPaperBrandIconByID(PaperBrandBase paperBrandBase) {
            PrPaperBrandIconGetById prc = new PrPaperBrandIconGetById();

            prc.PaperBrandId= paperBrandBase.Id;
            using (IDataReader reader = prc.ExecuteReader()) {
                int iconColumnIndex = reader.GetOrdinal("Icon");
                if (reader.Read()) {
                    object icon=reader.GetValue(iconColumnIndex);
                    if(DBNull.Value!=icon)
                        return (byte[]) icon;
                    else 
                        return (new byte[0]);
                }
                else {
                    return new byte[0];
                }
            }
        }

        public static void Add(PaperBrand paperBrand) {
            if (!paperBrand.IsNew) throw new InvalidOperationException("Adding Paper Brand entity in 'Non-New' status");
            PrPaperBrandIns prc = new PrPaperBrandIns();
            prc.Name = paperBrand.Name;
            prc.ExecuteNonQuery();
            paperBrand.Id = prc.PaperBrandId.Value;
        }

        public static void Update(PaperBrand paperBrand) {
            if (paperBrand.IsNew) throw new InvalidOperationException("Updating Paper Brand entity in 'New' status");
            PrPaperBrandUpd prc = new PrPaperBrandUpd();
            prc.PaperBrandId = paperBrand.Id;
            prc.Name = paperBrand.Name;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Paper Brand does not exist.");
        }

        public static void UpdateIcon(PaperBrandBase paperBrand, byte[] icon) {
            PrPaperBrandIconUpd prc = new PrPaperBrandIconUpd();
            prc.PaperBrandId = paperBrand.Id;
            prc.Icon = icon;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Paper Brand does not exist.");
        }

        public static void Delete(PaperBrandBase paperBrand) {
            PrPaperBrandDel prc = new PrPaperBrandDel();
            prc.PaperBrandId = paperBrand.Id;
            prc.ExecuteNonQuery();
        }
	}
}
