using System.Collections;
using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data {
    public sealed class DOLogo {
        private DOLogo() {}

        public static Hashtable Enum() {
            PrLogoEnum prc = new PrLogoEnum();
            Hashtable logos = new Hashtable();
            byte[] logo;
            byte[] flashLogo;
            int retailerId;
            using (IDataReader reader = prc.ExecuteReader()) {
                int RetailerIdIndex = reader.GetOrdinal("RetailerId");
                int LogoIndex = reader.GetOrdinal("Logo");
                int FlashLogoIndex = reader.GetOrdinal("FlashLogo");
                while (reader.Read()) {
                    retailerId = reader.GetInt32(RetailerIdIndex);
                    logo = reader.GetValue(LogoIndex) as byte[];
                    if (logo == null)
                        logo = new byte[0];
                    flashLogo = reader.GetValue(FlashLogoIndex) as byte[];
                    if (flashLogo == null)
                        flashLogo = new byte[0];

                    logos.Add(retailerId, new Logo(logo, flashLogo));
                }
            }
            return logos;
        }

        public static void Insert(int retailerId, byte[] logo, byte[] flashLogo) {
            PrLogoIns prc = new PrLogoIns();
            prc.RetailerId = retailerId;
            prc.Logo = logo;
            prc.FlashLogo = flashLogo;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Logo does not exist.");
        }

        public static void Update(int retailerId, byte[] logo, byte[] flashLogo) {
            PrLogoUpd prc = new PrLogoUpd();
            prc.RetailerId = retailerId;
            prc.Logo = logo;
            prc.FlashLogo = flashLogo;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Logo does not exist.");
        }

        public static void Delete(int retailerId) {
            PrLogoDel prc = new PrLogoDel();
            prc.RetailerId = retailerId;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0)
                throw new DBConcurrencyException("Logo does not exist.");
        }
    }
}