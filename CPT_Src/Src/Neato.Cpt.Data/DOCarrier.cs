using System;
using System.Collections;
using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;

namespace Neato.Cpt.Data {
    public class DOCarrier {
        public DOCarrier() {}

        public static Carrier[] EnumerateCarrierByParam(DeviceType deviceType, DeviceBase device, string paperState, Retailer retailer) {
            PrCarriersEnumerate prc = new PrCarriersEnumerate();

            if (deviceType != DeviceType.Undefined) {
                prc.DeviceTypeId = (int)deviceType;
            }
            if (device != null) {
                prc.DeviceId = device.Id;
            }
            if (paperState != null) {
                prc.PaperState = paperState;
            }
            if (retailer != null) {
                prc.RetailerId = retailer.Id;
            }

            ArrayList carriers = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int idColumnName = reader.GetOrdinal("Name");
                while (reader.Read()) {
                    Carrier carrier = new Carrier();
                    carrier.Id = reader.GetInt32(idColumnIndex);
                    carrier.Name = reader.GetString(idColumnName);
                    carriers.Add(carrier);
                }
            }
            return (Carrier[])carriers.ToArray(typeof(Carrier));
        }

        public static byte[] GetCarrierIcon(CarrierBase carrier) {
            PrCarrierIconGetById prc = new PrCarrierIconGetById();

            prc.CarrierId = carrier.Id;
            using (IDataReader reader = prc.ExecuteReader()) {
                int iconColumnIndex = reader.GetOrdinal("Icon");
                if (reader.Read()) {
                    object icon = reader.GetValue(iconColumnIndex);
                    if (DBNull.Value != icon)
                        return (byte[])icon;
                    else
                        return (new byte[0]);
                } else {
                    return new byte[0];
                }
            }
        }

        public static void Add(Carrier carrier) {
            if (!carrier.IsNew) throw new InvalidOperationException("Adding Carrier entity in 'Non-New' status");
            PrCarrierIns prc = new PrCarrierIns();
            prc.Name = carrier.Name;
            prc.ExecuteNonQuery();
            carrier.Id = prc.CarrierId.Value;
        }

        public static void Update(Carrier carrier) {
            if (carrier.IsNew) throw new InvalidOperationException("Updating Carrier entity in 'New' status");
            PrCarrierUpd prc = new PrCarrierUpd();
            prc.CarrierId = carrier.Id;
            prc.Name = carrier.Name;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Carrier does not exist.");
        }

        public static void UpdateIcon(CarrierBase carrier, byte[] icon) {
            PrCarrierIconUpd prc = new PrCarrierIconUpd();
            prc.CarrierId = carrier.Id;
            prc.Icon = icon;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Carrier does not exist.");
        }

        public static void Delete(CarrierBase carrier) {
            PrCarrierDel prc = new PrCarrierDel();
            prc.CarrierId = carrier.Id;
            prc.ExecuteNonQuery();
        }

        public static Carrier GetCarrier(CarrierBase carrier) {
            PrCarrierGetById prc = new PrCarrierGetById();
            prc.CarrierId = carrier.Id;
            Carrier resultCarrier = null;

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int nameColumnIndex = reader.GetOrdinal("Name");
                if (reader.Read()) {
                    resultCarrier = new Carrier();
                    resultCarrier.Id = reader.GetInt32(idColumnIndex);
                    resultCarrier.Name = reader.GetString(nameColumnIndex);
                }
            }
            return resultCarrier;
        }

        public static void AddToRetailer(Carrier carrier, Retailer retailer) {
            PrCarrierAddToRetailer prc = new PrCarrierAddToRetailer();
            prc.RetailerId = retailer.Id;
            prc.CarrierId = carrier.Id;
            prc.ExecuteNonQuery();
            if(prc.RowsAffected == 0)
                throw new BusinessException("Cant add CarrierToRetailer");
        }

        public static void RemoveFromRetailer(Carrier carrier, Retailer retailer) {
            PrCarrierRemoveFromRetailer prc = new PrCarrierRemoveFromRetailer();
            if(retailer != null)
                prc.RetailerId = retailer.Id;
            if(carrier != null)
                prc.CarrierId = carrier.Id;
            prc.ExecuteNonQuery();
            if(prc.RowsAffected == 0)
                throw new BusinessException("Cant delete CarrierToRetailer");
        }
    }
}