using System;
using System.Data;
using System.Collections;
using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data {
	public class DODeviceRequest {
		public DODeviceRequest() {}

        public static DeviceRequest[] GetPhoneRequests(Retailer retailer) {
            PrDeviceRequestEnum prc = new PrDeviceRequestEnum();
            if(retailer != null)prc.RetailerId = retailer.Id;
            ArrayList phoneRequests = new ArrayList();

            using(IDataReader reader = prc.ExecuteReader()) {
                int phoneRequestIdColumnIndex = reader.GetOrdinal("Id");
                int firstNameColumnIndex = reader.GetOrdinal("FirstName");
                int lastNameColumnIndex = reader.GetOrdinal("LastName");
                int emailColumnIndex = reader.GetOrdinal("EmailAddress");
                int carrierColumnIndex = reader.GetOrdinal("Carrier");
                int deviceBrandColumnIndex = reader.GetOrdinal("DeviceBrand");
                int deviceColumnIndex = reader.GetOrdinal("Device");
                int commentColumnIndex = reader.GetOrdinal("Comment");
                int createdColumnIndex = reader.GetOrdinal("Created");
                int retailerColumnIndex = reader.GetOrdinal("RetailerId");

                while(reader.Read()) {
                    DeviceRequest deviceRequest = new DeviceRequest();
                    deviceRequest.RequestDeviceId = reader.GetInt32(phoneRequestIdColumnIndex);
                    deviceRequest.FirstName = reader.GetString(firstNameColumnIndex);
                    deviceRequest.LastName = reader.GetString(lastNameColumnIndex);
                    deviceRequest.EmailAddress = reader.GetString(emailColumnIndex);
                    deviceRequest.Carrier = reader.GetString(carrierColumnIndex);
                    deviceRequest.DeviceBrand = reader.GetString(deviceBrandColumnIndex);
                    deviceRequest.Device = reader.GetString(deviceColumnIndex);
                    deviceRequest.Comment = reader.GetString(commentColumnIndex);
                    deviceRequest.Created = reader.GetDateTime(createdColumnIndex);
                    deviceRequest.RetailerId = reader.GetInt32(retailerColumnIndex);

                    phoneRequests.Add(deviceRequest);
                }
            }
            return (DeviceRequest[])phoneRequests.ToArray(typeof(DeviceRequest));
        }

        public static void InsertPhoneRequest(DeviceRequest deviceRequest, Retailer retailer) {
            PrDeviceRequestIns prc = new PrDeviceRequestIns();
            prc.FirstName = deviceRequest.FirstName;
            prc.LastName = deviceRequest.LastName;
            prc.EmailAddress = deviceRequest.EmailAddress;
            prc.Carrier = deviceRequest.Carrier;
            prc.DeviceBrand = deviceRequest.DeviceBrand;
            prc.Device = deviceRequest.Device;
            prc.Comment = deviceRequest.Comment;
            prc.Created  = deviceRequest.Created;
            if(retailer != null)prc.RetailerId = retailer.Id;

            prc.ExecuteNonQuery();
            deviceRequest.RequestDeviceId = prc.Id.Value;
        }

        public static void UpdatePhoneRequest(DeviceRequest deviceRequest, Retailer retailer) {
            PrDeviceRequestUpd prc = new PrDeviceRequestUpd();
            prc.FirstName = deviceRequest.FirstName;
            prc.LastName = deviceRequest.LastName;
            prc.EmailAddress = deviceRequest.EmailAddress;
            prc.Carrier = deviceRequest.Carrier;
            prc.DeviceBrand = deviceRequest.DeviceBrand;
            prc.Device = deviceRequest.Device;
            prc.Comment = deviceRequest.Comment;
            prc.Id = deviceRequest.RequestDeviceId;

            prc.ExecuteNonQuery();
        }

        public static void DeletePhoneRequest(int id) {
            PrDeviceRequestDel prc = new PrDeviceRequestDel();
            prc.Id = id;
            prc.ExecuteNonQuery();
        }

        public static DeviceRequest[] GetPhoneRequestsByCarrierPhoneManufacturerPhoneModel(string carrier, bool otherCarrier,
                                                        string deviceBrand, bool otherDeviceBrand, string device, 
                                                        bool otherDevice,DateTime timeStart, DateTime timeEnd, Retailer retailer) {
            PrDeviceRequestEnumByCarrierPhoneManufacturerPhoneModel prc = new PrDeviceRequestEnumByCarrierPhoneManufacturerPhoneModel();
            ArrayList phoneRequests = new ArrayList();

            if(carrier != null)
                prc.Carrier = carrier;
            if(deviceBrand != null)
                prc.DeviceBrand = deviceBrand;
            if(device != null)
                prc.Device = device;

            prc.OtherCarrier = otherCarrier;
            prc.OtherDeviceBrand = otherDeviceBrand;
            prc.OtherDevice = otherDevice;
            prc.TimeStart = timeStart; 
            prc.TimeEnd = timeEnd;
            if(retailer != null)prc.RetailerId = retailer.Id;

            using(IDataReader reader = prc.ExecuteReader()) {
                int phoneRequestIdColumnIndex = reader.GetOrdinal("Id");
                int firstNameColumnIndex = reader.GetOrdinal("FirstName");
                int lastNameColumnIndex = reader.GetOrdinal("LastName");
                int emailColumnIndex = reader.GetOrdinal("EmailAddress");
                int carrierColumnIndex = reader.GetOrdinal("Carrier");
                int deviceBrandColumnIndex = reader.GetOrdinal("DeviceBrand");
                int deviceColumnIndex = reader.GetOrdinal("Device");
                int commentColumnIndex = reader.GetOrdinal("Comment");
                int retailerColumnIndex = reader.GetOrdinal("RetailerId");

                while(reader.Read()) {
                    DeviceRequest deviceRequest = new DeviceRequest();
                    deviceRequest.RequestDeviceId = reader.GetInt32(phoneRequestIdColumnIndex);
                    deviceRequest.FirstName = reader.GetString(firstNameColumnIndex);
                    deviceRequest.LastName = reader.GetString(lastNameColumnIndex);
                    deviceRequest.EmailAddress = reader.GetString(emailColumnIndex);
                    deviceRequest.Carrier = reader.GetString(carrierColumnIndex);
                    deviceRequest.DeviceBrand = reader.GetString(deviceBrandColumnIndex);
                    deviceRequest.Device = reader.GetString(deviceColumnIndex);
                    deviceRequest.Comment = reader.GetString(commentColumnIndex);
                    deviceRequest.RetailerId = reader.GetInt32(retailerColumnIndex);

                    phoneRequests.Add(deviceRequest);
                }
            }
            return (DeviceRequest[])phoneRequests.ToArray(typeof(DeviceRequest));
        }

        public static DataSet GetPhoneRequestReportByDatesAndBrand
            (string deviceBrand, bool otherDeviceBrand,
            DateTime startDate, DateTime endDate, Retailer retailer) {
            PrDeviceRequestGetByDatesAndBrand prc =
                new PrDeviceRequestGetByDatesAndBrand();

            if(deviceBrand != null) {
                prc.DeviceBrand = deviceBrand;
            }
            prc.OtherDeviceBrand = otherDeviceBrand;
            prc.StartDate = startDate;
            prc.EndDate = endDate;
            if(retailer != null)prc.RetailerId = retailer.Id;

            return prc.ExecuteDataSet();
        }

        public static DataSet GetPhoneRequestEmailsReportByDates
            (DateTime startDate, DateTime endDate, Retailer retailer) {
            PrDeviceRequestEmailsGetByDates prc =
                new PrDeviceRequestEmailsGetByDates();

            prc.StartDate = startDate;
            prc.EndDate = endDate;
            if(retailer != null)prc.RetailerId = retailer.Id;

            return prc.ExecuteDataSet();
        }    
    }
}