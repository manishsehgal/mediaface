using System;
using System.Collections;
using System.Data;

using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;

namespace Neato.Cpt.Data {
    public class DODevice {
        public DODevice() {}

        public static Device[] EnumerateDeviceByParam(DeviceBrandBase brandBase, DeviceType deviceType, CarrierBase carrierBase, FaceBase face) {
            PrDeviceEnumerate prc = new PrDeviceEnumerate();

            if (brandBase != null) {
                prc.BrandId = brandBase.Id;
            }
            if (deviceType != DeviceType.Undefined) {
                prc.DeviceTypeId = (int)deviceType;
            }
            if (carrierBase != null) {
                prc.CarrierId = carrierBase.Id;
            }
            if (face != null) {
                prc.FaceId = face.Id;
            }

            ArrayList list = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int modelColumnIndex = reader.GetOrdinal("Model");
                int deviceTypeIdColumnIndex = reader.GetOrdinal("DeviceTypeId");
                int sortOrderColumnIndex = reader.GetOrdinal("SortOrder");
                while (reader.Read()) {
                    Device device = new Device();
                    device.Id = reader.GetInt32(idColumnIndex);
                    device.Model = reader.GetString(modelColumnIndex);
                    device.DeviceType = (DeviceType)reader.GetInt32(deviceTypeIdColumnIndex);
                    device.Rating = reader.GetInt32(sortOrderColumnIndex);
                    list.Add(device);
                }
            }
            for (int i = 0; i < list.Count; ++i) {
                list[i] = GetDevice((DeviceBase)list[i]);
            }
            return (Device[])list.ToArray(typeof(Device));
        }

        public static Device[] GetDeviceListByPaper(PaperBase paper) {
            PrDeviceEnumByPaper prc = new PrDeviceEnumByPaper();
            prc.PaperId = paper.Id;

            ArrayList list = new ArrayList();

            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int modelColumnIndex = reader.GetOrdinal("Model");
                int sortOrderColumnIndex = reader.GetOrdinal("SortOrder");
                while (reader.Read()) {
                    Device device = new Device();
                    device.Id = reader.GetInt32(idColumnIndex);
                    device.Model = reader.GetString(modelColumnIndex);
                    device.Rating = reader.GetInt32(sortOrderColumnIndex);
                    list.Add(device);
                }
            }
            for (int i = 0; i < list.Count; ++i) {
                list[i] = GetDevice((DeviceBase)list[i]);
            }
            return (Device[])list.ToArray(typeof(Device));
        }

        public static Device GetDevice(DeviceBase deviceBase) {
            PrDeviceGetById prc = new PrDeviceGetById();
            prc.DeviceId = deviceBase.Id;
            Device device = null;
            int brandId = 0;
            using (IDataReader reader = prc.ExecuteReader()) {
                int idColumnIndex = reader.GetOrdinal("Id");
                int modelColumnIndex = reader.GetOrdinal("Model");
                int deviceTypeIdColumnIndex = reader.GetOrdinal("DeviceTypeId");
                int brandIdColumnIndex = reader.GetOrdinal("BrandId");
                int sortOrderColumnIndex = reader.GetOrdinal("SortOrder");
                if (reader.Read()) {
                    device = new Device();
                    device.Id = reader.GetInt32(idColumnIndex);
                    device.Model = reader.GetString(modelColumnIndex);
                    device.DeviceType = (DeviceType)reader.GetInt32(deviceTypeIdColumnIndex);
                    brandId = reader.GetInt32(brandIdColumnIndex);
                    device.Rating = reader.GetInt32(sortOrderColumnIndex);
                }
            }
            if (device != null) {
                device.Brand = DODeviceBrand.GetDeviceBrand(new DeviceBrandBase(brandId));
            }
            return device;
        }

        public static byte[] GetDeviceIcon(DeviceBase device) {
            PrDeviceIconGetById prc = new PrDeviceIconGetById();

            prc.DeviceId = device.Id;

            using (IDataReader reader = prc.ExecuteReader()) {
                int iconColumnIndex = reader.GetOrdinal("Icon");
                if (reader.Read()) {
                    object icon = reader.GetValue(iconColumnIndex);
                    if (DBNull.Value != icon)
                        return (byte[])icon;
                    else
                        return (new byte[0]);
                } else {
                    return new byte[0];
                }
            }
        }

        public static byte[] GetDeviceBigIcon(DeviceBase device) {
            PrDeviceIconBigGetById prc = new PrDeviceIconBigGetById();

            prc.DeviceId = device.Id;

            using (IDataReader reader = prc.ExecuteReader()) {
                int iconColumnIndex = reader.GetOrdinal("IconBig");
                if (reader.Read()) {
                    object icon = reader.GetValue(iconColumnIndex);
                    if (DBNull.Value != icon)
                        return (byte[])icon;
                    else
                        return (new byte[0]);
                } else {
                    return new byte[0];
                }
            }
        }

        public static void Add(Device device) {
            if (!device.IsNew) throw new InvalidOperationException("Adding Device entity in 'Non-New' status.");
            if (device.DeviceType == DeviceType.Undefined) throw new InvalidOperationException("Adding Device entity with Undefined device type.");
            PrDeviceIns prc = new PrDeviceIns();
            prc.BrandId = device.Brand.Id;
            prc.DeviceTypeId = (int)device.DeviceType;
            prc.Model = device.Model;
            prc.SortOrder = device.Rating;
            prc.ExecuteNonQuery();
            device.Id = prc.DeviceId.Value;
        }

        public static void Update(Device device) {
            if (device.IsNew) throw new InvalidOperationException("Updating Device entity in 'New' status.");
            if (device.DeviceType == DeviceType.Undefined) throw new InvalidOperationException("Updating Device entity with Undefined device type.");
            PrDeviceUpd prc = new PrDeviceUpd();
            prc.DeviceId = device.Id;
            prc.BrandId = device.Brand.Id;
            prc.DeviceTypeId = (int)device.DeviceType;
            prc.Model = device.Model;
            prc.SortOrder = device.Rating;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Device does not exist.");
        }

        public static void Delete(DeviceBase device) {
            PrDeviceDel prc = new PrDeviceDel();
            prc.DeviceId = device.Id;
            prc.ExecuteNonQuery();
        }

        public static void UpdateIcon(DeviceBase device, byte[] icon) {
            PrDeviceIconUpd prc = new PrDeviceIconUpd();
            prc.DeviceId = device.Id;
            prc.Icon = icon;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Device does not exist.");
        }

        public static void UpdateBigIcon(DeviceBase device, byte[] icon) {
            PrDeviceIconBigUpd prc = new PrDeviceIconBigUpd();
            prc.DeviceId = device.Id;
            prc.Icon = icon;
            prc.ExecuteNonQuery();
            if (prc.RowsAffected <= 0) throw new DBConcurrencyException("Device does not exist.");
        }

        /// <summary>
        /// Remove Device from carrier
        /// </summary>
        /// <param name="device">If null - remove all device from the specified carrier</param>
        /// <param name="carrier">If null - remove the device from all carriers</param>
        /// <remarks>If all parameters are null removed all devices from all carriers</remarks>
        public static void RemoveFromCarrier(DeviceBase device, CarrierBase carrier) {
            PrDeviceRemoveFromCarrier prc = new PrDeviceRemoveFromCarrier();
            if (device != null)
                prc.DeviceId = device.Id;
            if (carrier != null)
                prc.CarrierId = carrier.Id;
            prc.ExecuteNonQuery();
        }

        public static void AddToCarrier(DeviceBase device, CarrierBase carrier) {
            PrDeviceAddToCarrier prc = new PrDeviceAddToCarrier();
            prc.DeviceId = device.Id;
            prc.CarrierId = carrier.Id;
            prc.ExecuteNonQuery();
        }
    }
}