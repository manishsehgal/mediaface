using Neato.Cpt.Data.DBEngine;
using Neato.Cpt.Data.DbProcedures;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.DataBase;

namespace Neato.Cpt.Data {
    public class DOAnnouncement {
        private DOAnnouncement() {}

        public static AnnouncementData EnumAnnouncements(Retailer retailer) {
            PrAnnouncementEnum prc = new PrAnnouncementEnum();
            prc.RetailerId = retailer.Id;
            return (AnnouncementData)prc.LoadDataSet(new AnnouncementData());
        }

        public static AnnouncementData GetLastAnnouncements(Retailer retailer) {
            PrAnnouncementEnumLast prc = new PrAnnouncementEnumLast();
            prc.RetailerId = retailer.Id;
            return (AnnouncementData)prc.LoadDataSet(new AnnouncementData());
        }

        public static void UpdateAnnouncement(AnnouncementData data) {
            PrAnnouncementIns prcIns = new PrAnnouncementIns();
            PrAnnouncementUpd prcUpd = new PrAnnouncementUpd();
            PrAnnouncementDel prcDel = new PrAnnouncementDel();
            ProcedureWrapper.UpdateDataTable(data.Announcement, prcIns, prcUpd, prcDel);
        }
    }
}