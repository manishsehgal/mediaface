/******************************************************************************* 
  Module: asserts
  Short Description: part of TCUnit
  
  Detailed Description: 

  Author: zak & phil

  Change list:
    +26.08.2004 zak & phil, created
    +05.10.2004 zak, refactored
    +07.10.2004 zak, doc++ comments
  
*******************************************************************************/ 

//USEUNIT globvars
//USEUNIT trace

var mediator = new Mediator( );

/** TSAssertObj
       @param suiteRunner pointer to SuiteRunner object
       @return Assert object
       @author zak
       @version 1.0
    */    
function TSAssertObj(suiteRunner) {
   return new AssertObj(suiteRunner, "ts_assert");         
}

/** TSFAssertObj
       @param suiteRunner pointer to SuiteRunner object
       @return AssertFail object
       @author zak
       @version 1.0
    */    
function TSFAssertObj(suiteRunner) {
   return new AssertObj(suiteRunner, "tsf_assert");
}

/** getAssertObjEx
       @param type type assertion type
       @return AssertFail object
       @author zak
       @version 1.0
    */    
function getAssertObjEx(type) {
   if (type == "tsf_assert") return AssertF;
   else if (type == "ts_assert") return Assert;
}

function AssertObj(suiteRunner, type) {
   this.IsFalse = assertObj_IsFalse;
   this.IsTrue = assertObj_IsTrue;
   this.AreEqual = assertObj_AreEqual;
   this.AreNotEqual = assertObj_AreNotEqual;
   this.IsNull = assertObj_IsNull;
   this.IsNotNull = assertObj_IsNotNull;
   this.IsPass = assertObj_IsPass;   
   this.IsFail = assertObj_IsFail;
   this.IsNotFail = assertObj_IsNotFail;
   this.DoFail = assertObj_DoFail;
   
   this._assert = assertObj_assert;
   
   this.makeAssertion = assertObj_makeAssertion;
   
   this.suiteRunner = suiteRunner;
   this.type = type;
         
   function assertObj_assert(assert_expr, std_message, /*optional*/ caseMsg) {
      if ( !caseMsg ) {
         if ( mediator.assertionType ) caseMsg = mediator.assertionType;
         else caseMsg = "unspecified test case";
      }
         
      mediator.clearBuf( );
     
      if (!assert_expr) {  
         suiteRunner.makeFail(std_message + " " + caseMsg + " - FAILED", this.type);
      } else {
         tracePass(caseMsg + " - PASSED");
      }
      
   }  

   function assertObj_IsFalse(expr, /*optional*/ caseMsg) {
      this._assert((expr == false), "expected false", caseMsg);
   }
        
   function assertObj_IsTrue(expr, /*optional*/ caseMsg) {
      this._assert((expr == true), "expected true", caseMsg);
   }
   
   function assertObj_AreEqual(expr1, expr2, /*optional*/ caseMsg) {
      this._assert((expr1 == expr2), "expected equality", caseMsg);
   }

   function assertObj_AreNotEqual(expr1, expr2, /*optional*/ caseMsg) {
      this._assert((expr1 != expr2), "expected inequality", caseMsg);
   }
      
   function assertObj_IsNull(expr, /*optional*/ caseMsg) {
      this._assert((expr == null), "expected null", caseMsg);
   }
   
   function assertObj_IsNotNull(expr, /*optional*/ caseMsg) {
      this._assert((expr != null), "expected not null", caseMsg);
   }
   
   function assertObj_IsPass(expr, /*optional*/ caseMsg) {
      this._assert((expr == PASS), "expected PASS", caseMsg);
   }
            
   function assertObj_IsFail(expr, /*optional*/ caseMsg) {
      this._assert((expr == FAIL), "expected FAIL", caseMsg);
   }
   
   function assertObj_IsNotFail(expr, /*optional*/ caseMsg) {
      this._assert((expr != FAIL), "expected not FAIL", caseMsg);
   }
   
   function assertObj_DoFail( /*optional*/ caseMsg ) {
      caseMsg = caseMsg ? ": " + caseMsg : "";
      suiteRunner.makeFail("make FAIL" + caseMsg, this.type);
   }
   
   function assertObj_makeAssertion(assertionType, res, failMsg) {
      switch (assertionType) {
         case "ISPASS"    : this.IsPass(res, failMsg); break;
         case "ISFAIL"    : this.IsFail(res, failMsg); break;
         case "ISNOTFAIL" : this.IsNotFail(res, failMsg); break;
         case "ISTRUE"    : this.IsTrue(res, failMsg); break;
         case "ISFALSE"   : this.IsFalse(res, failMsg); break; 
	 /* add more features here */
      }   
   }   
}




function TS_ASSERT_ISPASS() {
   mediator.setAssertionType("ISPASS");
   mediator.setFailureType("ts_assert");      
}

function TSF_ASSERT_ISPASS() {
   mediator.setAssertionType("ISPASS");
   mediator.setFailureType("tsf_assert");   
}

function TS_ASSERT_ISTRUE() {
   mediator.setAssertionType("ISTRUE");
   mediator.setFailureType("ts_assert");      
}

function TSF_ASSERT_ISTRUE() {
   mediator.setAssertionType("ISTRUE");
   mediator.setFailureType("tsf_assert");   
}

function END() {
   mediator.tearDown();
}



function Mediator() {
   this.setResult = mediator_setResult; 
   this.setAssertionType = mediator_setAssertionType;
   this.setFailureType = mediator_setFailureType;
   
   this.releaseBuf = mediator_releaseBuf;
   this.tearDown = mediator_tearDown;
   this.clearBuf = mediator_clearBuf;
   
   this.assertionType;
   this.failureType;
   this.buf;
   
   function mediator_releaseBuf() {
      if (this.buf) {
         if (!(obj = getAssertObjEx(this.buf.failureType))) return;
         obj.makeAssertion(this.buf.assertionType, this.buf.res, this.buf.failMsg);
      }
   }
   
   function mediator_clearBuf() {
      if ( this.buf ) delete this.buf;   
   }
   
   function mediator_setResult(res, /* optional */ failMsg) {
      this.buf = new Object();      
      this.buf.res = res;
      this.buf.failMsg = failMsg;
      this.buf.failureType = this.failureType;
      this.buf.assertionType = this.assertionType;   
   }
   
   function mediator_setAssertionType(type) {
      this.assertionType = type;
   }
   
   function mediator_setFailureType(type) {
      this.failureType = type;
   }
   
   function mediator_tearDown() {
      this.releaseBuf();               
      this.setAssertionType(null);
      this.setFailureType(null); 
   }
}
