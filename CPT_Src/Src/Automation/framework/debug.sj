//USEUNIT suite_runner
//USEUNIT test_suite

var testedFunction;

function BEGIN_DEBUG(func) {   
   if (func == testedFunction) return;
   
   testedFunction = func;     
   var suiteRunner = new SuiteRunner();
   
   suiteRunner.addSuite(new dummy(), "debug");   
   suiteRunner.runWorld();     
   
   suiteRunner.dispose();
   delete   suiteRunner;      
   
   Runner.Stop();   
}

function dummy()  {
   TestSuite.call(this);
   
   this.testedFunction = testedFunction;
}

function traceAlert(message) {
   Win32API.MessageBox(
      0, 
      message, 
      "information", 
      MB_OK | MB_ICONINFORMATION
   );
}     

