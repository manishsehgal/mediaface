//USEUNIT asserts
//USEUNIT commonlib
//USEUNIT globvars
       
suiteRunner = new SuiteRunner( );

function SuiteRunner() {
   this.addSuite = suiteRunner_addSuite;
   this.runWorld = suiteRunner_runWorld;
   this.dispose = suiteRunner_dispose;
   this.makeFail = suiteRunner_makeFail;
   
   this.names  = new Array();
   this.suites = new Array();   
   this.iterator;
      
   function suiteRunner_addSuite(testSuite, suiteName) {   
      if (suiteName && testSuite) {
         this.names[this.names.length] = suiteName;
         this.suites[suiteName] = testSuite;
      }
   }
   
   function suiteRunner_runWorld() {
      if (!Assert) Assert = new TSAssertObj(this);
      if (!AssertF) AssertF = new TSFAssertObj(this);
      
      for (this.iterator = 0; this.iterator < this.names.length; this.iterator++) {
         var suiteName = this.names[this.iterator];
         Log.CreateNode(suiteName);                     
         this.suites[suiteName].runAllTests();                  
         Log.CloseNode();
      }                 
   }
   
   function suiteRunner_makeFail(stdMsg, type) {
      var suiteName = this.names[this.iterator];
      this.suites[suiteName].throwException(stdMsg, type);
   }
   
   function suiteRunner_dispose() {
      delete this.suites;
      delete this.names;
   }   
}

function cleanUpMemory( ) {
   suiteRunner.dispose( );
   delete suiteRunner;
   
   delete Assert;
   delete AssertF;
   delete mediator;   
   delete PASS;
   delete FAIL;
      
   delete _OBJECT;
   delete _CTLNAME;  
}
