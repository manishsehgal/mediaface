//USEUNIT commonlib
//USEUNIT constants
//USEUNIT globvars

function goHome() {
    _toURL(START_PAGE);
    var Res = objExists(LBL_SELECDEV) && objExists(LBL_FAVOR);
    Assert.IsNotFail(Res);
    return Res;
}


function presslink_verifypage(lnkname,lblpage){

    pressLink(lnkname);
    Assert.IsNotFail(objFind(lblpage));

}


//**************** Functions for Site Map links***********************************************


 function pressLink_SM( linkName, wait, tag ) {
 // a work-around: TestComplete can not determine properly coordinates of links 
 // at our Site Map page, so we just get link URL and re-direct browser to this URL

/*   var _linkName = linkName, _wait, _tag;
   
   switch( typeof( wait ) ) {
   case "boolean" : break;
   case "string"  : tag = wait; wait = null;
   }   
   _wait = ( wait == null ) ? true : wait;
   _tag  = ( !tag ) ? "DD" : tag;

    var linkURL = objFindByProperty( "innerText", _linkName, "tagName", _tag ).get("href");
*/    
    var linkURL = objFindByProperty( "innerText", linkName, "tagName", "A" ).get("href");    
    _toURL(linkURL);
    
/*   return _process( function _pressLink( ) { 
   
   objFindByProperty( "innerText", _linkName, "tagName", _tag ).make( "click", 0, 0 ); 
   traceDebug( MSG_LINK_PRESSED( _linkName ) );
      
   }, MAKE_REFRESH=_wait );                                             
*/
   
}


function presslink_verifypage_SM(lnkname,lblpage){

    pressLink_SM(lnkname);
    Assert.IsNotFail(objFind(lblpage)); 
}
//****************************************************************************************************


function cmpEditText( edtName, text, property ) {
   var _edtName = edtName, _text = text, _property = property;

   return _process( function _cmpText( ) {
   
   if ( objFind( _edtName ).get( _property ) != _text ) {
      traceDebug( MSG_TEXT_NOT_EQUALS( _edtName, _text ) );      
      return FAIL;
   } else {
      traceDebug( MSG_TEXT_EQUALS( _edtName, _text ) );      
      return PASS;
   }      
   
   } );
}

//**************** Functions for Model Selection page *****************************

function verifyBtnExists(txtButton) {
    Assert.IsNotFail(objExists("type","submit","value",txtButton));
}
    
function verifyMS_Step(txtStep){
    var Res =  objExists("Name",LBL_CURRSTEP,"innerText",txtStep);
    Assert.IsNotFail(Res);
    return Res;
}

function pressSelect_verifyMS_Step(txtSelect,txtStep){
    var obj = objFindByProperty("type","submit","value",txtSelect);
    pressButton(obj.obj["Name"]);
    return verifyMS_Step(txtStep);
}  


