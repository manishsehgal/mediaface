function _CONTROL( controlname ) {
   return " control: " + controlname + " ";
}

function _METHOD( methodname ) {
   return " method: " + methodname + " ";
}

function _PROPERTY( propname ) {
   return " property: " + propname + " ";
}

function _TEXT( text ) {
   return " text: " + text + " ";
}

function _ITEM( itemname ) {
   return " item: " + itemname + " ";
}

function _LINK( linkname ) {
   return " link: " + linkname + " ";
}

function _BUTTON( buttonname ) {
   return " button: " + buttonname + " ";
}

function _FUNCTION( funcname ) {
   return " function: " + funcname + " ";
}

function _DIALOG( dlgname ) {
   return " dialog: " + dlgname;
}

function MSG_INVALID_METHOD( controlname, methodname ) { 
   return _CONTROL( controlname ) + "doesn't support" + _METHOD( methodname ) +
      "or method is called with invalid number of arguments";       
}      

function MSG_NO_SUCH_PROPERTY( controlname, propname ) { 
   return _CONTROL( controlname ) + " doesn't suppport" + _PROPERTY( propname );
}

function MSG_OBJECT_NOT_FOUND( controlname ) { 
   return _CONTROL( controlname ) + "not exist";
}

function MSG_OBJECT_FOUND( controlname ) {
   return _CONTROL( controlname ) + "exist";
}

function MSG_SELECTED_ITEM_ARE_EQUAL( controlname, text ) {
   return "selected item of" + _CONTROL( controlname ) + "equals to" + _TEXT( text );
}

function MSG_SELECTED_ITEM_ARE_NOT_EQUAL( controlname, text ) {
   return "selected item of" + _CONTROL( controlname ) + "not equals to" + _TEXT( text );
}

function MSG_TEXT_EQUALS( controlname, text ) {
   return "value of" + _CONTROL( controlname ) + "equals to" + _TEXT( text );
}

function MSG_TEXT_NOT_EQUALS( controlname, text ) { 
   return "value of" + _CONTROL( controlname ) + "not equals to" + _TEXT( text );
}

function MSG_LINK_PRESSED( controlname ) {
   return _LINK( controlname ) + "pressed";
}

function MSG_BUTTON_PRESSED( controlname ) {
   return _BUTTON( controlname ) + "pressed";
}

function MSG_ITEM_AT_INDEX_SELECTED( controlname, index ) {
   return _CONTROL( controlname ) + _ITEM( index ) + "selected";
}

function MSG_ITEM_SELECTED( controlname, text ) {
   return _CONTROL( controlname ) + _ITEM( text ) + "selected"; 
}

function MSG_ITEM_NOT_SELECTED( controlname, text ) {
   return _CONTROL( controlname ) + _ITEM( text ) + "no such item in dropdown"; 
}

function MSG_TEXT_SET( controlname, text ) {
   return _CONTROL( controlname ) + _TEXT( text ) + "set on the assumption of that control is editable";
}

function MSG_CTRL_DISABLED( controlname ) {
   return _CONTROL( controlname ) + "is disabled"; 
}

function MSG_PAGEHEADER_CORRECT( controlname ) {
   return " page header is " + controlname;
}

function MSG_PAGEHEADER_INVALID( controlname ) {
   return " page header is not " + controlname;
}

function MSG_DIALOG_CATCHED( name ) {
   return _DIALOG( name ) + " catched ";
}

function MSG_DIALOG_NOT_CATCHED( name ) {
   return _DIALOG( name ) + " not catched ";
}

function MSG_SYSTEM_CRASHED( msg ) {
   return " System crashed! " + msg + ", skiping test... ";
}

function MSG_ITEM_NOT_EXISTS( controlname, text ) {
   return _CONTROL( controlname ) + _ITEM( text ) + "no such item"; 
}

function FORMAT_MSG( ex, func ) {
   if ( func ) {
      var funcname = /\b_(\w+)\s*/;
      return ex + _FUNCTION( funcname.exec( func )[1] ) + "throwed exception";
   } else return ex;      
}

