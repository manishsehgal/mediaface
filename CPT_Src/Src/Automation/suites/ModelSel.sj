/******************************************************************************* 
  Short description:  
   Model Selection
   
   Script click on all possible links and verify pages displaying.

  Author:  Yuri Teterin
  
  Change list:
    + 11.01.2006 - created
  
*******************************************************************************/ 
//USEUNIT std_include 



function ModelSel() {

   TestSuite.call( this );
   
   this.suiteIni = goHome; 

   this.testSelectModelPage = goToSelectModelPage;
   this.testSelectModel = TestSelectModel;   
   
}

   
function goToSelectModelPage() {

   TS_ASSERT_ISPASS(); 
   
    presslink_verifypage(SELECT_PHONE,LBL_SELECTMODEL);  
    verifyMS_Step(LBL_INSTRUCTIONS);
    
    verifyBtnExists(BTN_SEL_BY_CARRIER);
    verifyBtnExists(BTN_SEL_BY_BRAND);
    verifyBtnExists(BTN_SEL_DEVICE);        
   
}

function TestSelectModel() {

    pressSelect_verifyMS_Step( BTN_SEL_BY_CARRIER, LBL_SEL_BY_CARRIER);
    pressSelect_verifyMS_Step( BTN_SEL_BY_BRAND, LBL_SEL_BY_BRAND);
    pressSelect_verifyMS_Step( BTN_SEL_DEVICE, LBL_SEL_DEVICE);
    
    presslink_verifypage(SELECT_PHONE,LBL_SELECTMODEL);  
    verifyMS_Step(LBL_INSTRUCTIONS);
             
    END();

}



 






