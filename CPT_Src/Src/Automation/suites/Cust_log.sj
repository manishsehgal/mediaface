/******************************************************************************* 
  Short description:
    
   Customers accounts
   
   There are 200 accounts were created in the database, 
   script verify succesful logging into the system by all of them.

  Author:  Marina Ryabova
  
  Change list:
    + 08.12.2005 - created
  
*******************************************************************************/  

//USEUNIT std_include

function Cust_log() {

   TestSuite.call( this );
 
// this.suiteFini = test_clearTestCustomer(); 
   
  this.suiteIni  = _clearTestCustomer;
  this.suiteIni2 = goHome; 
  //this.suiteFini = _clearTestCustomer;
  this.testTest_CreateCustomer  = Test_CreateCustomer;   
  //this.testLog  = Log;
  this.testCheck_vld = Check_vld; 
      
  function _clearTestCustomer()   
{ 
   var res=_execQuery("DELETE FROM dbo.Customer WHERE Email='" + TEST_MAIL + "'");
}
    
   
 }
 
function Test_CreateCustomer()
{
TS_ASSERT_ISPASS();

    pressLink(LNK_NEWUSER);
    
    FillForm(TEST_FNAME, TEST_LNAME, TEST_MAIL, TEST_PASSWORD, TEST_PASSWORD,TEST_CHECKED);
    pressButton(BTN_SUBMIT); 
    pressLink(HOME);
    pressLink(LNK_LOGOUT);
    
    var rs = getTestCustomer();

    if (rs != null)
    
    {
        Assert.AreEqual(getField(rs, "FirstName"), TEST_FNAME, "Test user was succesfully added!");
        
    }   
    
END();

} 
 
 

 function Log ()  {
 
     var CUST_MAIL   = new Array();
     
        for (i=0; i<200; i++){                          
        CUST_MAIL[i] = "CptTestUser" + i +"@yahoo.com" ;                                   
       } 
    
     var CUST_PSW   = new Array(); 
        for (i=0; i<200; i++){           
        CUST_PSW[i] = "psw" + i ;                                   
        } 
        
        
    var NAMES = new Array (
                                    "Marie Pontes",
                                    "Jean McKenna",
                                    "Jean Ottlieb",
                                    "Eduardo Fonseca",
                                    "Mary Simpson",
                                    "Eduardo Saveley",
                                    "Karl Josephs",
                                    "Giovanni Larsson",
                                    "Karin Parente",
                                    "Palle Citeaux",
                                    "Mario Chang",
                                    "Lino Steel",
                                    "Janine Fresniere",
                                    "Jaime Henriot",
                                    "Yvonne Henriot",
                                    "Howard Fernandez",
                                    "Dominique Messner",
                                    "Catherine Pereira",
                                    "Fran Wang",
                                    "Karl Parente",
                                    "Ana Wilson",
                                    "Guillermo Holz",
                                    "Horst Simpson",
                                    "Paul Muller",
                                    "Maurizio Carvalho",
                                    "Liu Yorres",
                                    "Alexander Petersen",
                                    "Felipe Ashworth",
                                    "Guillermo Pipps",
                                    "Rita Tannamuri",
                                    "Jytte Gutierrez",
                                    "Frederique Trujillo",
                                    "Mario Wilson",
                                    "Paul Yorres",
                                    "Karin Angel",
                                    "Henriette Pereira",
                                    "Patricia Kloss",
                                    "Carine Chang",
                                    "Ana Parente",
                                    "Pascale Cramer",
                                    "Anabela Crowther",
                                    "Lucia Bennett",
                                    "Maria Wang",
                                    "Hanna Cartrain",
                                    "Peter Lebihan",
                                    "Isabel Phillips",
                                    "Palle Devon",
                                    "Jaime Tannamuri",
                                    "Victoria Angel",
                                    "Patricia Fernandez",
                                    "Maria Rance",
                                    "Mario Wilson",
                                    "Felipe Moncada",
                                    "Daniel Snyder",
                                    "Lino Limeira",
                                    "Patricio Moroni",
                                    "John Fonseca",
                                    "Helvetius Tonini",
                                    "Ann Anders",
                                    "Eduardo Afonso",
                                    "Thomas Lincoln",
                                    "Simon Koskitalo",
                                    "Matti Batista",
                                    "Jonas Tonini",
                                    "Marie Bergulfsen",
                                    "Michael Karttunen",
                                    "Alexander Accorti",
                                    "Pirkko Steel",
                                    "Liu Batista",
                                    "Diego Camino",
                                    "Howard Devon",
                                    "Maurizio Pereira",
                                    "Francisco Lincoln",
                                    "Fran Wong",
                                    "Art Bennett",
                                    "Helen Fresniere",
                                    "Horst Phillips",
                                    "Thomas Nixon",
                                    "Hari Wang",
                                    "Jose Berglund",
                                    "Daniel Kumar",
                                    "Sergio Bennett",
                                    "Janine Snyder",
                                    "Patricia Wilson",
                                    "Patricia Wilson",
                                    "Alexander Camino",
                                    "Christina Henriot",
                                    "Sven Muller",
                                    "Frederique Afonso",
                                    "Carine Muller",
                                    "Karl Perrier",
                                    "Georg Roel",
                                    "Christina Domingues",
                                    "Jaime Cruz",
                                    "Hanna Jablonski",
                                    "Lucia Franken",
                                    "Frederique Cramer",
                                    "Miguel Tannamuri",
                                    "Anabela Accorti",
                                    "Liz Hardy",
                                    "Renate Carvalho",
                                    "Dominique Devon",
                                    "Catherine Chang",
                                    "Victoria Moos",
                                    "Karl Carvalho",
                                    "Fran Mendel",
                                    "Mario Batista",
                                    "Martin Domingues",
                                    "Paolo Anders",
                                    "Karl Rovelli",
                                    "Francisco Domingues",
                                    "Anabela Mendel",
                                    "Paula Tonini",
                                    "Philip Messner",
                                    "John Moroni",
                                    "Antonio Lebihan",
                                    "Lino Rovelli",
                                    "Jose Phillips",
                                    "Aria Camino",
                                    "Fran Steel",
                                    "Eduardo Pereira",
                                    "Laurence Kloss",
                                    "Sergio Hardy",
                                    "Henriette Schmitt",
                                    "Victoria Citeaux",
                                    "Roland Cruz",
                                    "Maurizio Fonseca",
                                    "Annette Gutierrez",
                                    "Ana Perrier",
                                    "Guillermo Lincoln",
                                    "Mary Devon",
                                    "Frederiquei Tannamuri",
                                    "Pallecz Piestrzeniewicz",
                                    "Antonio Pedro",
                                    "Eduardo Pontes",
                                    "Janete Muller",
                                    "Paolo Dewey",
                                    "Annette Brown",
                                    "Catherine Cartrain",
                                    "Jytte Rodriguez",
                                    "Fran Trujillo",
                                    "Ann Saavedra",
                                    "Miguel Pavarotti",
                                    "Sven Petersen",
                                    "Liu Cramer",
                                    "Roland Hardy",
                                    "Simon Tannamuri",
                                    "Anabela Ibsen",
                                    "Manuel Kloss",
                                    "Pedro Ibsen",
                                    "Rene Fresniere",
                                    "Dominique Ottlieb",
                                    "Paul Roulet",
                                    "Liu Messner",
                                    "Catherine Trujillo",
                                    "Helen Camino",
                                    "Felipe Pereira",
                                    "Catherine Ottlieb",
                                    "Isabel Messner",
                                    "Victoria Perrier",
                                    "Guillermo Brown",
                                    "Alejandra Saveley",
                                    "Isabel Petersen",
                                    "Ana Berglund",
                                    "Jose Koskitalo",
                                    "Lino Fonseca",
                                    "Georg Devon",
                                    "Jean Pontes",
                                    "Martin Yorres",
                                    "Renate Snyder",
                                    "Aria Moreno",
                                    "Maria Carvalho",
                                    "Paul Lincoln",
                                    "Guillermo Messner",
                                    "Karl Izquierdo",
                                    "Ann Bergulfsen",
                                    "Annette Roulet",
                                    "Rita Saavedra",
                                    "Maurizio Pfalzheim",
                                    "Felipe Roulet",
                                    "Carine Ashworth",
                                    "Jaime Afonso",
                                    "Patricia Cartrain",
                                    "Jean Gutierrez",
                                    "Paula Moncada",
                                    "Rene McKenna",
                                    "Janete Steel",
                                    "Carine Cartrain",
                                    "Hari Nixon",
                                    "Jose Nagy",
                                    "Howard Steel",
                                    "Hanna Cramer",
                                    "Martine Camino",
                                    "Pedro Ottlieb",
                                    "Renate Cartrain",
                                    "Paula Lincoln",
                                    "Thomas Camino",
                                    "Philip Franken",
                                    "Dominique Anders",
                                    "Paolo Rovelli"
     ); 
         
 
    TS_ASSERT_ISPASS();
    
    for (i=0;i<200;i++){   
      setText( EDT_EMAIL,CUST_MAIL[i] );
      setText( EDT_PASSWORD,CUST_PSW[i]);
      pressLink (LNK_ENTER);
      objFindByProperty( "innerText", NAMES[i]);
      pressLink (LNK_LOGOUT);
   }   
    END();
 }
 
 function FillForm (fn, ln, email, pw, con_pw, chk_st)
{
    setText(EDT_FNAME, fn);
    setText(EDT_LNAME, ln);
    setText(EDT_EMAIL, email);
    setText(EDT_PASSWORD, pw);
    setText(EDT_CONFIRM, con_pw);
    setCheckBoxState( CHK_RECEIVE, chk_st )
}

function FillFormModify (fn, ln, email, pw, oldpw, con_pw, chk_st)
{
    setText(EDT_FNAME, fn);
    setText(EDT_LNAME, ln);
    setText(EDT_EMAIL, email);
    setText(EDT_OLD_PASS, oldpw);  
    setText(EDT_PASSWORD, pw);
    setText(EDT_CONFIRM, con_pw);
    setCheckBoxState( CHK_RECEIVE, chk_st )
}

function login (login, password) 

{
   setText(LOG_EMAIL, login); 
   setText(LOG_PASSWORD, password);
   pressLink (LNK_ENTER); 
}

function Check_vld ()

{
 TS_ASSERT_ISPASS();

    login (TEST_MAIL, TEST_PASSWORD);
    pressLink (LNK_MODIFY);
    Assert.IsNotFail(objFind(LBL_CUST_ACCOUNT));
    cmpEditText( EDT_FNAME, TEST_FNAME, "value" );
    cmpEditText( EDT_LNAME, TEST_LNAME, "value" );
    cmpEditText( EDT_EMAIL, TEST_MAIL, "value" );  
    
    pressButton( BTN_SUBMIT);
    Assert.IsNotFail(objFindByProperty("innerText",VLD_OLD_PASSWORD));
    Assert.IsNotFail(objFindByProperty("innerText",VLD_NEW_PASSWORD)); 
    Assert.IsNotFail(objFindByProperty("innerText",VLD_CONF_PASSWORD)); 
    
   FillFormModify (TEST_FNAME, TEST_LNAME, TEST_MAIL, TEST_PASSWORD, TEST_PASSWORD, TEST_PASSWORD,TEST_CHECKED);
   pressButton( BTN_SUBMIT);
   Assert.IsNotFail(objFindByProperty("innerText","Your account was successfully updated!"));   
   pressLink(HOME);
   Assert.IsNotFail(objFind(LBL_FAVOR)); 
   pressLink(LNK_LOGOUT);   
       
 END();    
    
}
