/******************************************************************************* 
  Short description:
    
   Site_map
   
   Script click on all possible links on the Site Map page and it verifies correct pages displaying .

  Author:  Marina Ryabova
  
  Change list:
    + 28.10.2005 - created
  
*******************************************************************************/  

//USEUNIT std_include


function Site_map() {
   
   TestSuite.call( this );
   
   this.suiteIni = goHome; 

   this.testsm_links  = sm_links;
   
 }

 function sm_links (){
 
 TS_ASSERT_ISPASS(); 

     presslink_verifypage(SITE_MAP,LBL_SITEMAP);     
     presslink_verifypage_SM(HOME,LBL_FAVOR ); 
         
     presslink_verifypage(SITE_MAP,LBL_SITEMAP);   
     presslink_verifypage_SM(SM_OVERVIEW,LBL_OVERVIEW);     
     presslink_verifypage(HOME,LBL_FAVOR ); 
     
     presslink_verifypage(SITE_MAP,LBL_SITEMAP); 
     presslink_verifypage_SM(SM_FRIEND,LBL_FRIEND);      
     presslink_verifypage(HOME,LBL_FAVOR ); 

/* For "Our Brands" - script does not work    
     presslink_verifypage(SITE_MAP,LBL_SITEMAP);  
    
     pressLink_SM(SM_BODYGLOVE);
     var bg_wnd1;
     bg_wnd1 = Sys.Process(IE).Window( IE_WNDCLASS, BG_PAGE);
     bg_wnd1.Activate();
     WND = bg_wnd1; 
     Assert.IsNotFail(objFind(BG_SITE_HOME)); 
     bg_wnd1.Close();    
     WND = Sys.Process( IE ).Window( IE_WNDCLASS, COMMON_CAPTION ); 
     WND.Activate();        
     PAGE = WND.Page( COMMON_CAPTION ); 
*/     
   
  END();         
  
 }
 
 
 

