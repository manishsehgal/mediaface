using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

using Neato.Cpt.Business;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner {
    public class UploadCheck : Page {

        private const string CheckScript = @"<script>Check('{0}', '{1}');</script>";

        protected HtmlInputHidden hdnState;

        #region Url
        private const string RawUrl = "UploadCheck.aspx";
        private const string HrefUrlKey = "HrefUrl";
        private const string RequestSizeUrlKey = "check_key";
        private const string UploadSizeOK = "size_ok";
        private const string UploadOversize = "oversize";

        public static Uri Url(Uri oversizeUrl, string requestSizeKey) {
            return UrlHelper.BuildUrl(RawUrl, HrefUrlKey, HttpUtility.UrlEncode(oversizeUrl.PathAndQuery), RequestSizeUrlKey, requestSizeKey);
        }

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        private string HrefUrl {
            get {
                string oversizeUrl = Request.QueryString[HrefUrlKey];
                if (oversizeUrl == null) {
                    throw new Exception();
                }
                return HttpUtility.UrlDecode(oversizeUrl);
            }
        }

        public static string GetRequestSizeKey(Uri url) {
            string query = url.Query;
            string[] split = query.Split('&', '?');
            foreach (string s in split) {
                if (s.StartsWith(RequestSizeUrlKey)) {
                    return s.Split('=')[1];
                }
            }
            return null;
        }
        #endregion

        private void Page_Load(object sender, System.EventArgs e) {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetValidUntilExpires(false);

            if (!IsPostBack) {
                string requestSizeKey = Request.QueryString[RequestSizeUrlKey];
                if (Context.Cache[requestSizeKey] != null) {
                    int size = Convert.ToInt32(Context.Cache[requestSizeKey]);
                    if (BCProject.IsImageAdditionAllowed(StorageManager.CurrentProject, size)) {
                        hdnState.Value = UploadSizeOK;
                    } else {
                        hdnState.Value = UploadOversize;
                    }
                    Context.Cache.Remove(requestSizeKey);
                } else {
                    hdnState.Value = "undefined";
                }
            }
            RegisterStartupScript("script", String.Format(CheckScript, hdnState.ClientID, HrefUrl));
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e) {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {    
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
