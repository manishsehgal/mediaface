<%@ Page language="c#" Codebehind="PreviewDescription.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebDesigner.PreviewDescription" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
        <title>Preview Description</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <link href="../css/style.css" type="text/css" rel="stylesheet"
  </HEAD>
    <body MS_POSITIONING="GridLayout">
        <form id="ctlPreviewDescriptionForm" method="post" runat="server">
        <div id="ContentLevel2Block">
            <p><asp:Label ID="lblHeader" Runat= "server" /></p>
            <p><asp:Label ID="lblInstructionHeader" Runat= "server"/></p>
            <ul class="printInstructions">
                <li><asp:Label ID="lblPrintDesignInstruction" Runat="server" /></li>
                <li><asp:Label ID="lblSaveDesignInstruction" Runat="server" /></li>
            </ul>
            <p><asp:Label ID="lblChangeDesignInstruction" Runat="server" /></p>
        </div>
        </form>
    </body>
</HTML>
