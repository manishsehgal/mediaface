<%@ Page language="c#" Codebehind="ForgotPassword.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebDesigner.ForgotPassword" %>
<%@ Register TagPrefix="cpt" TagName="MainMenu" Src="Controls/MainMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="FooterMenu" Src="Controls/FooterMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="Header" Src="Controls/Header.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
  <head>
    <title>Fellowes - Forgot Password</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../css/style.css" type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
		<div>
			<table ID="tblForgotPassword" Runat="server" cellpadding="0" cellspacing="0" class="FormTable">
				<tr valign="middle">
					<td colspan="3">
						<div style="margin-top:20px; margin-bottom:20px;">
							<asp:Label ID="lblDescription" Runat="server" CssClass="Text"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<asp:Label ID="lblEmailAddress" Runat="server" CssClass="Text Header"/>
					</td>
					<td>
						<asp:TextBox ID="txtEmailAddress" Runat="server" CssClass="Field" MaxLength="100"/>
					</td>
					<td>
						<asp:ImageButton Runat="server" ID="btnSubmit"
							CausesValidation="False"
							ImageAlign="AbsMiddle"
							ImageUrl="../Images/buttons/submit.gif"
                            onmouseup="src='../images/buttons/submit.gif';"
                            onmousedown="src='../images/buttons/submit_pressed.gif';"
                            onmouseout="src='../images/buttons/submit.gif';"/>
                        <img src="../images/buttons/submit_pressed.gif" style="display: none;">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div>
							<asp:Label ID="lblSendMail" Runat="server" CssClass="Text Alert" Visible="False"/>
						</div>
						<div>
							<asp:Label ID="lblError" Runat="server" CssClass="Text Alert" Visible="False"/>
						</div>
						<div>
							<asp:RequiredFieldValidator ControlToValidate = "txtEmailAddress" Display="Dynamic" CssClass="Text Alert" 
								Runat="server" ID="vldEmailAddress"/>
						</div>
						<div>
						    <asp:CustomValidator ControlToValidate="txtEmailAddress" Display="Dynamic" CssClass="Text Alert"
						        Runat="server" ID="vldValidEmail" EnableClientScript="False"/>
						</div>
						<div>
							<asp:CustomValidator ControlToValidate = "txtEmailAddress" Display="Dynamic" CssClass="Text Alert" Runat="server" ID="vldEmailNotRegistered"/> 
						</div>
					</td>
				</tr>
			</table>
			<table ID="tblResetPassword" Runat="server" cellpadding="0" cellspacing="0" class="FormTable">
				<tr>
					<td>
						<asp:Label ID="lblEmailAddressText" Runat="server" CssClass="Text Header"/>
					</td>
					<td>
						<asp:Label ID="lblEmailAddressValue" Runat="server" CssClass="Text"/>
					</td>
				</tr>
				<tr>
					<td>
						<asp:Label ID="lblPasswordText" Runat="server" CssClass="Text Header" />
					</td>
					<td>
						<asp:Label ID="lblPasswordValue" Runat="server" CssClass="Text" />
					</td>
				</tr>
			</table>
			<table ID="tblWrongPasswordUID" Runat="server" Visible="False" cellpadding="0" cellspacing="0" class="FormTable">
				<tr>
					<td>
						<asp:Label ID="lblDescription2" Runat="server" CssClass="Text"/>
					</td>
				</tr>
				<tr>
					<td align="center" >
						<asp:ImageButton Runat="server" ID="btnContinue"
							CausesValidation="False"
							ImageUrl="../Images/buttons/continue.gif"
                            onmouseup="src='../images/buttons/continue.gif';"
                            onmousedown="src='../images/buttons/continue_pressed.gif';"
                            onmouseout="src='../images/buttons/continue.gif';"/>
                        <img src="../images/buttons/continue_pressed.gif" style="display: none;">
					</td>
				</tr>
			</table>
        </div>
    </form>
  </body>
</html>

