<%@ Page language="c#" Codebehind="default.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebDesigner.DefaultPage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
    <HEAD>
        <title>Cell phone skins � design your own skins from Printz</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <meta name="keywords" content="cell phone skins, cell phone accessories, cell phone skin, ipod skins, mp3 player skins ">
        <meta name="description" content="Printz offers cell phone skins, you can design your own skins and place them on your cell phone which is a great if you are looking for cell phone accessories to make your cell phone stand out.">
        <link href="../css/style.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" language="javascript" src="../js/Cookies.js"></script>
    </HEAD>
    <body MS_POSITIONING="GridLayout">
        <form id="HomePageForm" method="post" runat="server">
            <div class="alert" align="center">
                <script language="javascript">
				<!--
				CheckCookies("<asp:Literal id="litCookieDisabled" runat="Server"/>");
				-->
                </script>
            </div>
            <div style="PADDING-TOP:10px;position:absolute">
                <div>
                    <img src="../images/starthere3.gif">
                </div>
                <asp:Panel ID="divDevices" runat="server">
                    <table>
                        <tr>
                            <asp:Repeater ID="rptDevices" Runat="server">
                                <ItemTemplate>
                                    <td>
                                        <asp:HyperLink ID="lnkDevice" Runat="server" />
                                        <a><img ID="imgDevice" runat="server" border="0" /> </a>
                                    </td>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </form>
    </body>
</HTML>
