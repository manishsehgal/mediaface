using System;
namespace Neato.Cpt.WebDesigner.Controls {
    public sealed class RedirectEventArgs : EventArgs {
        private string navigateUrlValue;

        public string NavigateUrl {
            get { return navigateUrlValue; }
        }

        public RedirectEventArgs(string navigateUrl) : base() {
            navigateUrlValue = navigateUrl;
        }
    }
}