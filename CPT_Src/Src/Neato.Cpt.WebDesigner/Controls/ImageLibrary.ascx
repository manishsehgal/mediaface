<%@ Control Language="c#" AutoEventWireup="false" EnableViewState="true" Codebehind="ImageLibrary.ascx.cs" Inherits="Neato.Cpt.WebDesigner.Controls.ImageLibrary" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<div class="ScrollPanel">
    <div class="MarginText">
        <div class="PaperSelectPrompt">
           <asp:Label ID="lblPrompt" runat="server" Visible="False"/>
        </div>
        <asp:DataList ID="lstItems"
            Runat="server" 
            BorderColor="black"
            CellPadding="5"
            CellSpacing="5"
            RepeatDirection="Horizontal"
            RepeatLayout="Table"
            RepeatColumns="4"
            EnableViewState="true">
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
            <ItemTemplate>
              <div Id="divStandard" Runat="server">
				<div class="ImageLibraryItem">
					<div>
						<asp:ImageButton Runat="server" ID="hypImage" CommandName="RedirectToUrl" CssClass="ImageLibrary" CausesValidation="False"/>
					</div>
					<div>
						<asp:LinkButton ID="hypText" Runat="server" CommandName="RedirectToUrl" CssClass="LinkText Active" CausesValidation="False"/>
					</div>
					<asp:HyperLink ID="hypExtInfo" Runat="server" EnableViewState="true"/>
				</div>
			  </div>
			  <div Id="divPaperSelect" Runat="server" Visible="False">
			    <table border="0" cellpadding="0" cellspacing="0">
			      <tr>
			        <td>
			          <asp:LinkButton ID="hypTextPaper" Runat="server" CommandName="RedirectToUrl" CssClass="LinkText Small" />
			        </td>
			        <td align="right">&nbsp;<img src="../images/arrow.jpg"></td>
			        <td>
			          <asp:ImageButton ID="hypImagePaper" Runat="server" CommandName="RedirectToUrl"/>
			        </td>
			      </tr>
			    </table>
			  </div>
            </ItemTemplate>
        </asp:DataList>
    </div>
</div>
