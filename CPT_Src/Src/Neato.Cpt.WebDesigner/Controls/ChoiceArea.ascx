<%@ Control Language="c#" AutoEventWireup="false" EnableViewState="false" Codebehind="ChoiceArea.ascx.cs" Inherits="Neato.Cpt.WebDesigner.Controls.ChoiceArea" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
        <asp:DataList ID="lstItems"
            Runat="server" 
            BorderColor="black"
            CellPadding="0"
            CellSpacing="0"
            RepeatDirection="Horizontal"
            RepeatLayout="Table"
            RepeatColumns="1"
            EnableViewState="False">
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
            <ItemTemplate>
                <asp:DropDownList ID="ctlDropDown" CssClass="comboChoiceArea" Runat="server" EnableViewState="False"/><br>
            </ItemTemplate>
        </asp:DataList>
    
