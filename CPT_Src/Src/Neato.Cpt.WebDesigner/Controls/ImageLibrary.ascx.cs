using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.WebDesigner.Controls {
    public class ImageLibrary : UserControl {
        protected DataList lstItems;
        protected Label lblPrompt;
        
        private string navigateUrlFieldValue;
        private string imageUrlFieldValue;
        private string textFieldValue;
        private string extinfoFieldValue;
        private string scale = string.Empty;
        private string imageCssStyle = string.Empty;
        private string hyperLinkCssStyle = string.Empty;
        private bool isPaperSelect = false;
        
        public object DataSource {
            get { return ViewState["ImageLibraryDataSource"]; }
            set { ViewState["ImageLibraryDataSource"] = value; }
        }

		public int ColumsNum {
			get{return lstItems.RepeatColumns;}
			set{lstItems.RepeatColumns= value;}
		}
		
        public string TextField {
            get { return textFieldValue; }
            set { textFieldValue = value; }
        }

        public string ImageUrlField {
            get { return imageUrlFieldValue; }
            set { imageUrlFieldValue = value; }
        }
		public string ExtInfoUrlField {
			get{return extinfoFieldValue;}
			set {extinfoFieldValue =value;}

		}
        public string NavigateUrlField {
            get { return navigateUrlFieldValue; }
            set { navigateUrlFieldValue = value; }
        }

        public string Scale {
            get { return scale; }
            set { scale = value; }
        }

        public string ImageCssStyle {
            get { return imageCssStyle; }
            set { imageCssStyle = value; }
        }

        public string HyperLinkCssStyle {
            get { return hyperLinkCssStyle; }
            set { hyperLinkCssStyle = value; }
        }

        public string Prompt {
            get { return lblPrompt.Text; }
            set {
                lblPrompt.Text = value;
                lblPrompt.Visible = value != string.Empty;
            }
        }

        public bool IsPaperSelect {
            get { return isPaperSelect; }
            set {isPaperSelect = value; }
        }

        public delegate void SelectItemEventHandler(Object sender, ImageLibrarySelectItemEventArgs e);

        public event SelectItemEventHandler RedirectToUrl;

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.lstItems.ItemDataBound += new DataListItemEventHandler(lstItems_ItemDataBound);
            this.DataBinding += new EventHandler(ImageLibrary_DataBinding);
            this.lstItems.ItemCommand += new DataListCommandEventHandler(lstItems_ItemCommand);
        }
        #endregion

        #region Data binding
        private void ImageLibrary_DataBinding(object sender, EventArgs e) {
            lstItems.DataSource = DataSource;
            if (isPaperSelect) {
                lstItems.ItemStyle.VerticalAlign = VerticalAlign.Middle;
                lstItems.CellPadding = 0;
                lstItems.CellSpacing = 0;
            }
        }

        private void lstItems_ItemDataBound(object sender, DataListItemEventArgs e) {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem) {
                ItemDataBound(e.Item);
            }
        }

        private void ItemDataBound(DataListItem listItem) {
            Object item = listItem.DataItem;

            string text = ReflectionHelper.GetFieldOrPropertyValue(item, TextField).ToString();

            string imageUrl = null;
            if (ReflectionHelper.GetFieldOrPropertyValue(item, ImageUrlField) != null) {
                imageUrl = ReflectionHelper.GetFieldOrPropertyValue(item, ImageUrlField).ToString();
            }

			string extinfoUrl=null;
			if (ReflectionHelper.GetFieldOrPropertyValue(item,ExtInfoUrlField) != null) {
                extinfoUrl =  ReflectionHelper.GetFieldOrPropertyValue(item,ExtInfoUrlField).ToString();
            }
			

            Control div = null;
            div = listItem.FindControl("divStandard");
            div.Visible = !isPaperSelect;
            div = listItem.FindControl("divPaperSelect");
            div.Visible = isPaperSelect;
            
            ImageButton hypImage = null;
            if (isPaperSelect)
                hypImage = (ImageButton)listItem.FindControl("hypImagePaper");
            else
                hypImage = (ImageButton)listItem.FindControl("hypImage");

            if (imageUrl != null) {
				hypImage.Visible = true;
                hypImage.Attributes["src"] = imageUrl;

                if (scale != string.Empty) {
                    hypImage.Style["width"] = scale;
                }
                if(ImageCssStyle != string.Empty)
                    hypImage.CssClass = ImageCssStyle;
            }
            else {
				hypImage.Visible = false;
            }

            LinkButton hypText = null;
            if (isPaperSelect)
                hypText = (LinkButton)listItem.FindControl("hypTextPaper");
            else
                hypText = (LinkButton)listItem.FindControl("hypText");

            hypText.Text = text;
            
			HyperLink hypExtInfo=(HyperLink)listItem.FindControl("hypExtInfo");
			if (extinfoUrl != null) {
				hypExtInfo.Visible=true;
				hypExtInfo.Text=ImageLibraryStrings.HypEnlarge();
				hypExtInfo.NavigateUrl="#";
                hypExtInfo.Attributes["onclick"] = "return OpenInfoWnd('"+extinfoUrl+"','"+text+"');";
			}
		else {
				hypExtInfo.Visible=false;
            }
            if(HyperLinkCssStyle != string.Empty)
                hypExtInfo.CssClass = HyperLinkCssStyle;
            else
                hypExtInfo.CssClass = "LinkEnlarge";
        }
        #endregion

        protected virtual void OnRedirectToUrl(DataListItem dataListItem) {
            ListSelectItem selectedItem = ((ListSelectItem[])DataSource)[dataListItem.ItemIndex];
            string url = selectedItem.NavigateUrl;
            if (RedirectToUrl != null) {
                ImageLibrarySelectItemEventArgs e = new ImageLibrarySelectItemEventArgs(url);
                RedirectToUrl(this, e);
            }
        }

        private void lstItems_ItemCommand(object source, DataListCommandEventArgs e) {
            switch(e.CommandName) {
                case "RedirectToUrl":
                    OnRedirectToUrl(e.Item);
                    break;
            }
        }
    }
}