using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace Neato.Cpt.WebDesigner.Controls {
    public class FooterMenu : UserControl {
        protected LinkMenu ctlLinkMenuLeft;
        protected LinkMenu ctlLinkMenuRight;
        protected HtmlControl divFooter;
        private object dataSourceLeftValue;
        private object dataSourceRightValue;
        private string textFieldValue = "Text";
        private string urlFieldValue = "Url";
        private string isNewWindowFieldValue = "IsNewWindow";
        private string backgroundColorValue = string.Empty;
        

        #region Properties
        public object DataSourceLeft {
            get { return dataSourceLeftValue; }
            set { dataSourceLeftValue = value; }
        }

        public object DataSourceRight {
            get { return dataSourceRightValue; }
            set { dataSourceRightValue = value; }
        }

        public string TextField {
            get { return textFieldValue; }
            set { textFieldValue = value; }
        }

        public string UrlField {
            get { return urlFieldValue; }
            set { urlFieldValue = value; }
        }

        public string IsNewWindowField {
            get { return isNewWindowFieldValue; }
            set { isNewWindowFieldValue = value; }
        }

        public string BackgroundColor {
            get { return backgroundColorValue; }
            set { backgroundColorValue = value; }
        }
        #endregion Properties

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.DataBinding += new EventHandler(FooterMenu_DataBinding);
        }
        #endregion

        #region Data binding
        private void FooterMenu_DataBinding(object sender, EventArgs e) {
            ctlLinkMenuLeft.DataSource = dataSourceLeftValue;
            ctlLinkMenuLeft.TextField = textFieldValue;
            ctlLinkMenuLeft.UrlField = urlFieldValue;
            ctlLinkMenuLeft.IsNewWindowField = isNewWindowFieldValue;

            ctlLinkMenuRight.DataSource = dataSourceRightValue;
            ctlLinkMenuRight.TextField = textFieldValue;
            ctlLinkMenuRight.UrlField = urlFieldValue;
            ctlLinkMenuRight.IsNewWindowField = isNewWindowFieldValue;

            divFooter.Style.Add("background-color", backgroundColorValue);
        }
        #endregion //Data binding
    }
}