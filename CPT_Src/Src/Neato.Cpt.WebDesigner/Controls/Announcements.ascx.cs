using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Cpt.Entity.DataBase;

namespace Neato.Cpt.WebDesigner.Controls {
    public class Announcements : UserControl {
        protected DataList lstAnnouncement;
        protected Label lblHeadLine;

        private AnnouncementData dataSourceValue;

        public AnnouncementData DataSource {
            get { return dataSourceValue; }
            set { dataSourceValue = value; }
        }

        private void Page_Load(object sender, EventArgs e) {
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Announcements_DataBinding);
            this.lstAnnouncement.ItemDataBound += new DataListItemEventHandler(lstAnnouncement_ItemDataBound);
        }

        private void Announcements_DataBinding(object sender, EventArgs e) {
            lstAnnouncement.DataSource = dataSourceValue.Announcement.DefaultView;
            lblHeadLine.Text = AnnouncementsStrings.TextHeadline();
        }

        private void lstAnnouncement_ItemDataBound(object sender, DataListItemEventArgs e) {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem) {
                ItemDataBound(e.Item);
            }
        }

        private void ItemDataBound(DataListItem listItem) {
            AnnouncementData.AnnouncementRow row = (AnnouncementData.AnnouncementRow)((DataRowView)listItem.DataItem).Row;
            Label lbl = (Label)listItem.FindControl("lblAnnouncement");
            lbl.Text = row.Text;
        }
        #endregion
    }
}