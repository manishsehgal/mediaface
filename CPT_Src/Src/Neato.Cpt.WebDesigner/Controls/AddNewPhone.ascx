<%@ Control Language="c#" AutoEventWireup="false" Codebehind="AddNewPhone.ascx.cs" Inherits="Neato.Cpt.WebDesigner.Controls.AddNewPhone" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<div>
	<table class="FormTable" width="400px">
		<tr>
			<td>
				<asp:Label ID="lblRequestPhone" Runat="server" CssClass="Text YellowHeader" />
			</td>
		</tr>
		<tr>
			<td>
				<div>
					<asp:Label ID="lblHint" Runat="server" CssClass="Text" />
				</div>
			</td>
		</tr>
	</table>
	<table class="FormTable" width="400px">
		<tr>
			<td>
			    <span class="requiredText">&nbsp;*&nbsp;</span>
			</td>
			<td>
				<asp:Label Runat="server" ID="lblFirstName" CssClass="Text Header" />
			</td>
			<td width="60%">
				<asp:TextBox Runat="server" ID="txtFirstName" MaxLength="100" CssClass="Field" />
			</td>
		</tr>
		<tr>
		    <td/>
		    <td colspan="2">
		        <asp:RequiredFieldValidator ControlToValidate="txtFirstName" Display="Dynamic" CssClass="Text Alert" Runat="server"
					ID="vldFirstName" EnableClientScript="False" />
		    </td>
		</tr>
		<tr>
			<td>
			    <span class="requiredText">&nbsp;*&nbsp;</span>
			</td>
			<td >
				<asp:Label Runat="server" ID="lblLastName" CssClass="Text Header"/>
			</td>
			<td >
				<asp:TextBox Runat="server" ID="txtLastName" MaxLength="100" CssClass="Field" />
			</td>
		</tr>
		<tr>
		    <td/>
		    <td colspan="2">
		        <asp:RequiredFieldValidator ControlToValidate="txtLastName" Display="Dynamic" CssClass="Text Alert" Runat="server"
					ID="vldLastName" EnableClientScript="False" />
		    </td>
		</tr>
		<tr>
			<td>
			    <span class="Text Required">&nbsp;*&nbsp;</span>
			</td>
			<td >
				<asp:Label Runat="server" ID="lblEmailAddress" CssClass="Text Header"/>
			</td>
			<td >
				<asp:TextBox Runat="server" ID="txtEmailAddress" MaxLength="100" CssClass="Field" />
			</td>
		</tr>
		<tr>
		    <td/>
		    <td colspan="2">
				<div>
					<asp:RequiredFieldValidator ControlToValidate="txtEmailAddress" Display="Dynamic" CssClass="Text Alert" Runat="server"
						ID="vldEmailAddress" EnableClientScript="False" />
				</div>
				<div>
					<asp:CustomValidator ControlToValidate="txtEmailAddress" Display="Dynamic" CssClass="Text Alert" Runat="server"
						ID="vldValidEmail" EnableClientScript="False" />
				</div>
		    </td>
		    <td/>
		</tr>
		<tr>
			<td/>
			<td >
				<asp:Label Runat="server" ID="lblCarrier" CssClass="Text Header" />
			</td>
			<td >
				<asp:TextBox Runat="server" ID="txtCarrier" MaxLength="100" CssClass="Field" />
			</td>
		</tr>
		<tr>
			<td>
			    <span class="Text Required">&nbsp;*&nbsp;</span>
			</td>
			<td >
				<asp:Label Runat="server" ID="lblPhoneManufacturer" CssClass="Text Header"/>
			</td>
			<td >
				<asp:DropDownList Runat="server" ID="cboPhoneManufacturer" AutoPostBack="True" CssClass="Field" />
			</td>
		</tr>
		<tr>
			<td/>
			<td/>
			<td>
				<asp:TextBox Runat="server" ID="txtPhoneManufacturer" MaxLength="100" Visible="False" CssClass="Field" />
			</td>
		</tr>
		<tr>
		    <td/>
		    <td colspan="2">
		        <asp:CustomValidator ControlToValidate="cboPhoneManufacturer" Display="Dynamic" CssClass="Text Alert" Runat="server"
					ID="vldPhoneManufacturer" EnableClientScript="False" />
		    </td>
		</tr>
		<tr>
			<td>
			    <span class="Text Required">&nbsp;*&nbsp;</span>
			</td>
			<td >
				<asp:Label Runat="server" ID="lblPhoneModel" CssClass="Text Header" />
			</td>
			<td >
				<asp:TextBox Runat="server" ID="txtPhoneModel" MaxLength="100" CssClass="Field" />
			</td>
		</tr>
		<tr>
		    <td/>
		    <td colspan="2">
		        <asp:RequiredFieldValidator ControlToValidate="txtPhoneModel" Display="Dynamic" CssClass="Text Alert" Runat="server"
					ID="vldPhoneModel" EnableClientScript="False" />
		    </td>
		</tr>
		<tr>
			<td/>
			<td >
				<asp:Label Runat="server" ID="lblComment" CssClass="Text Header" />
			</td>
		</tr>
		<tr>
			<td/>
			<td colspan="2">
				<asp:TextBox Runat="server" ID="txtComment" MaxLength="2000" CssClass="Multiline" TextMode="MultiLine" Rows="5" />
			</td>
		</tr>
		<tr>
			<td/>
			<td  align="right" colspan="2">
				<asp:ImageButton Runat="server" ID="btnSend"
					src="../images/buttons/send.gif"
				    onmouseup="src='../images/buttons/send.gif';"
                    onmousedown="src='../images/buttons/send_pressed.gif';"
                    onmouseout="src='../images/buttons/send.gif';"
                />
                <img src="../images/buttons/send_pressed.gif" style="display: none;">
				<asp:ImageButton Runat="server" ID="btnCancel"
					src="../Images/buttons/cancel.gif"
				    onmouseup="src='../images/buttons/cancel.gif';"
                    onmousedown="src='../images/buttons/cancel_pressed.gif';"
                    onmouseout="src='../images/buttons/cancel.gif';"
                    style="margin-left:20px;"
                />
                <img src="../images/buttons/cancel_pressed.gif" style="display: none;">
			</td>
		</tr>
	</table>
</div>
