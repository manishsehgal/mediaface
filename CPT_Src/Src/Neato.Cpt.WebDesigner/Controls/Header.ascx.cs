using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;

namespace Neato.Cpt.WebDesigner.Controls {
    public class Header : UserControl {
        protected MainMenu ctlMainMenu;
        private string parentUrl;
        public string ParentUrl {
            set { parentUrl = value; }
        }
        private string culture;
        public string Culture 
        {
            set { culture = value; }
        }
        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.DataBinding += new EventHandler(Header_DataBinding);
        }
        #endregion

        private void Header_DataBinding(object sender, EventArgs e) {
            ctlMainMenu.DataSource = BCDynamicLink.GetLinks(1+0, parentUrl, Place.Header, Align.None, culture);

        }
    }
}