<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FooterMenu.ascx.cs" Inherits="Neato.Cpt.WebDesigner.Controls.FooterMenu" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="cpt" TagName="LinkMenu" Src="LinkMenu.ascx" %>
<div class="FooterMenu" id="divFooter" runat="server">
  <table style="width:100%;height:100%;">
    <tr>
        <td align="left">
            <cpt:LinkMenu id="ctlLinkMenuLeft" runat="server" LinkCssClass="footerLink" LabelCssClass="copyRightText"/>
        </td>
        <td align="right">
            <cpt:LinkMenu id="ctlLinkMenuRight" runat="server" LinkCssClass="footerLink"/>
        </td>
    </tr>
  </table>
</div>
