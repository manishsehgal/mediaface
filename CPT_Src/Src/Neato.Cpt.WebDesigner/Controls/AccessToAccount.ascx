<%@ Control Language="c#" AutoEventWireup="false" Codebehind="AccessToAccount.ascx.cs" Inherits="Neato.Cpt.WebDesigner.Controls.AccessToAccount" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<div class="AccessToAccount">
	<div class="LeftPanelCaption">
		<asp:Label Runat="server" ID="lblAccessToAccount" />
	</div>
	<asp:Panel Runat="server" ID="panLoginForm">
		<div>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<asp:Label Runat="server" ID="lblEmail" CssClass="LeftPanelText" />
					</td>
					<td>
						<asp:TextBox Runat="server" ID="txtEmail" CssClass="LoginInput" />
					</td>
				</tr>
				<tr>
					<td>
						<asp:Label Runat="server" ID="lblPassword" CssClass="LeftPanelText" />
					</td>
					<td>
						<asp:TextBox Runat="server" ID="txtPassword" TextMode="Password" CssClass="LoginInput" />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<asp:CustomValidator Runat="server" ID="vldLogin" Display="Static" CssClass="LeftPanelText Validation" ForeColor="#000000" />
					</td>
				</tr>
				<tr>
					<td>
						<asp:ImageButton Runat="server" ID="btnLogin" CausesValidation="False"
						    src="../images/buttons/login.gif"
						    onmouseup="src='../Images/buttons/login.gif'"
                            onmousedown="src='../Images/buttons/login_pressed.gif'"
                            onmouseout="src='../Images/buttons/login.gif'"/>
                        <img src="../Images/buttons/login_pressed.gif" style="DISPLAY: none;">
					</td>
					<td>
						<asp:LinkButton Runat="server" ID="btnForgotPassword" CausesValidation="False" />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<asp:ImageButton Runat="server" ID="btnNewUser" CausesValidation="False"
						    src="../images/buttons/NewUser.gif"
						    onmouseup="src='../Images/buttons/NewUser.gif'"
                            onmousedown="src='../Images/buttons/newuser_pressed.gif'"
                            onmouseout="src='../Images/buttons/NewUser.gif'"/>
                        <img src="../Images/buttons/newuser_pressed.gif" style="DISPLAY: none;">
					</td>
				</tr>
			</table>
		</div>
		<div>
			<div>
				<asp:Label Runat="server" ID="lblUserBenefits" CssClass="LeftPanelText Bold" />
			</div>
			<div>
				<img src="../images/buttons/bullet.gif" />
				<asp:Label Runat="server" ID="lblPersonalSettings" CssClass="LeftPanelText" />
			</div>
			<div>
				<img src="../images/buttons/bullet.gif" />
				<asp:Label Runat="server" ID="lblInformtion" CssClass="LeftPanelText" />
			</div>
		</div>
		<a href="#" onclick="javascript:window.open('../LearnMore.htm','_blank','height=200,width=400,resizable=yes,menubar=no,location=no,left=200,top=200,');">
			<asp:Label Runat="server" ID="lblLearnMore" />
		</a>
	</asp:Panel>
	<asp:Panel Runat="server" ID="panManageAccount" Visible="False">
		<div>
			<asp:Label Runat="server" ID="lblLogged" CssClass="LeftPanelText" />
		</div>
		<div>
			<asp:Label Runat="server" ID="lblUserName" CssClass="LeftPanelText" />
		</div>
		<div>
			<asp:LinkButton Runat="server" ID="btnModifyProfile" CausesValidation="False" />
		</div>
		<div>
			<asp:LinkButton Runat="server" ID="btnLogout" CausesValidation="False" />
		</div>
	</asp:Panel>
</div>