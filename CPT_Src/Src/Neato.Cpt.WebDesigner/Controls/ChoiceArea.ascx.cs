using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.WebDesigner.Controls {

	public class ChoiceArea : UserControl {
        protected DataList lstItems;
        private object dataSourceValue;

        public object DataSource {
            get { return dataSourceValue; }
            set { dataSourceValue = value; }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent()
		{
            this.DataBinding += new EventHandler(ChoiceArea_DataBinding);
            this.lstItems.ItemDataBound += new DataListItemEventHandler(lstItems_ItemDataBound);
		}
		#endregion

        #region Data binding
        private void ChoiceArea_DataBinding(object sender, EventArgs e) {
            lstItems.DataSource = dataSourceValue;
        }

        private void lstItems_ItemDataBound(object sender, DataListItemEventArgs e) {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem) {
                ItemDataBound(e.Item);
            }
        }

        private void ItemDataBound(DataListItem listItem) {
            DropDownList ctlDropDown = (DropDownList)listItem.FindControl("ctlDropDown");
            ctlDropDown.Attributes["onchange"] = "window.location.href=this.options[this.selectedIndex].value";

            Object item = listItem.DataItem;
            object[] ddItems = (object[])ReflectionHelper.GetFieldOrPropertyValue(item, "DropDownItems");
            foreach (object ddItem in ddItems) {
                string text = ReflectionHelper.GetFieldOrPropertyValue(ddItem, "Text").ToString();
                string url  = ReflectionHelper.GetFieldOrPropertyValue(ddItem, "Url").ToString();
                ctlDropDown.Items.Add(new ListItem(text, url));
            }
        }
        #endregion
	}
}
