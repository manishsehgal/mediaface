using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Neato.Cpt.WebDesigner.Controls {
    [DefaultProperty("Title")]
    [ToolboxData("<{0}:AppVersion runat=server></{0}:AppVersion>")]
    public class AppVersion : WebControl {
        private string versionValue;
        private string titleValue;

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        public string Version {
            get { return versionValue; }
            set { versionValue = value; }
        }

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("Version")]
        public string Title {
            get { return titleValue; }
            set { titleValue = value; }
        }

        protected override void Render(HtmlTextWriter output) {
            Label lblVersion = new Label();
            lblVersion.Text = Title + " " + Version;
            lblVersion.CssClass = CssClass;
            lblVersion.RenderControl(output);
        }
    }
}