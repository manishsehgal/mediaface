<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Announcements.ascx.cs" Inherits="Neato.Cpt.WebDesigner.Controls.Announcements" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<div class="LeftPanelCaption" id="AnnounceCaption"><asp:Label ID="lblHeadLine" Runat="server"/></div>
<asp:DataList ID="lstAnnouncement" Runat="server">
    <ItemTemplate>
        <asp:Label ID="lblAnnouncement" Runat="server" CssClass="LeftPanelText"/>
	</ItemTemplate>
</asp:DataList>
