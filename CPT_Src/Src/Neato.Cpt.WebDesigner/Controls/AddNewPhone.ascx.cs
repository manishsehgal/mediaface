using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner.Controls {
    public class AddNewPhone : UserControl {
        protected Label lblFirstName;
        protected TextBox txtFirstName;
        protected RequiredFieldValidator vldLastName;
        protected Label lblLastName;
        protected Label lblPhoneManufacturer;
        protected Label lblPhoneModel;
        protected RequiredFieldValidator vldFirstName;
        protected TextBox txtLastName;
        protected TextBox txtPhoneManufacturer;
        protected CustomValidator vldPhoneManufacturer;
        protected TextBox txtPhoneModel;
        protected RequiredFieldValidator vldPhoneModel;
        protected TextBox txtComment;
        protected Label lblEmailAddress;
        protected TextBox txtEmailAddress;
        protected RequiredFieldValidator vldEmailAddress;
        protected CustomValidator vldValidEmail;
        protected Label lblRequestPhone;
        protected Label lblHint;
        protected Label lblComment;
        protected DropDownList cboPhoneManufacturer;
        protected Label lblCarrier;
        protected TextBox txtCarrier;
        protected ImageButton btnSend;
        protected ImageButton btnCancel;

        private object phoneManufacturersDataSource;

        public object PhoneManufacturersDataSource {
            set { phoneManufacturersDataSource = value; }
            get { return phoneManufacturersDataSource; }
        }

        public delegate void AddNewPhoneEventHandler(Object sender, AddNewPhoneEventArgs e);

        public event AddNewPhoneEventHandler Save;
        public event AddNewPhoneEventHandler Cancel;
        public event AddNewPhoneEventHandler Reload;

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                //    DataBind();
            }

        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(this.RequestPhone_DataBinding);
            this.vldValidEmail.ServerValidate += new ServerValidateEventHandler(this.vldValidEmail_ServerValidate);
            this.vldPhoneManufacturer.ServerValidate += new ServerValidateEventHandler(vldPhoneManufacturer_ServerValidate);
            this.btnSend.Click += new ImageClickEventHandler(btnSend_Click);
            this.btnCancel.Click += new ImageClickEventHandler(btnCancel_Click);
            this.cboPhoneManufacturer.SelectedIndexChanged += new EventHandler(cboPhoneManufacturer_SelectedIndexChanged);
            this.cboPhoneManufacturer.DataBinding += new EventHandler(cboPhoneManufacturer_DataBinding);
        }
        #endregion

        private void RequestPhone_DataBinding(object sender, EventArgs e) {
            this.lblRequestPhone.Text = AddNewPhoneStrings.TextRequestYourPhone();
            this.lblHint.Text = AddNewPhoneStrings.TextHint();
            this.lblFirstName.Text = AddNewPhoneStrings.TextFirstName();
            this.lblLastName.Text = AddNewPhoneStrings.TextLastName();
            this.lblCarrier.Text = AddNewPhoneStrings.TextCarrier();
            this.lblPhoneManufacturer.Text = AddNewPhoneStrings.TextPhoneManufacturer();
            this.lblPhoneModel.Text = AddNewPhoneStrings.TextPhoneModel();
            this.lblEmailAddress.Text = AddNewPhoneStrings.TextEmailAddress();
            this.lblComment.Text = AddNewPhoneStrings.TextComment();

            this.vldFirstName.Text = AddNewPhoneStrings.ValidatorFirstName();
            this.vldLastName.Text = AddNewPhoneStrings.ValidatorLastName();
            this.vldPhoneManufacturer.Text = AddNewPhoneStrings.ValidatorPhoneManufacturer();
            this.vldPhoneModel.Text = AddNewPhoneStrings.ValidatorPhoneModel();
            this.vldEmailAddress.Text = AddNewPhoneStrings.ValidatorEmailAddress();
            this.vldValidEmail.Text = AddNewPhoneStrings.ValidatorInvalidEmailAddress();
        }

        private void vldValidEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = FormatValidator.EnsureValidEmail(txtEmailAddress.Text.Trim());
        }

        private void vldPhoneManufacturer_ServerValidate(object source, ServerValidateEventArgs args) {
            if (cboPhoneManufacturer.SelectedIndex == 0)
                args.IsValid = false;
            else if (cboPhoneManufacturer.SelectedIndex == (cboPhoneManufacturer.Items.Count - 1))
                args.IsValid = txtPhoneManufacturer.Text.Length != 0;
            else
                args.IsValid = true;
        }

        private void cboPhoneManufacturer_SelectedIndexChanged(object sender, EventArgs e) {
            if (cboPhoneManufacturer.SelectedIndex == (cboPhoneManufacturer.Items.Count - 1)) {
                txtPhoneManufacturer.Visible = true;
            } else {
                txtPhoneManufacturer.Visible = false;
            }
            OnReload();
        }

        private void btnSend_Click(object sender, ImageClickEventArgs e) {
            DeviceRequest deviceRequest = new DeviceRequest();
            deviceRequest.FirstName = txtFirstName.Text;
            deviceRequest.LastName = txtLastName.Text;
            deviceRequest.EmailAddress = txtEmailAddress.Text;
            deviceRequest.Device = txtPhoneModel.Text;
            deviceRequest.Comment = txtComment.Text;
            deviceRequest.DeviceBrand = cboPhoneManufacturer.SelectedIndex == (cboPhoneManufacturer.Items.Count - 1) ? txtPhoneManufacturer.Text : cboPhoneManufacturer.SelectedValue;
            deviceRequest.Carrier = txtCarrier.Text;
            OnSave(deviceRequest);
        }

        public bool IsValid {
            get {
                if (vldFirstName.IsValid
                    && vldLastName.IsValid
                    && vldEmailAddress.IsValid
                    && vldValidEmail.IsValid
                    && vldPhoneManufacturer.IsValid
                    && vldPhoneModel.IsValid)
                    return true;
                return false;
            }
        }

        private void btnCancel_Click(object sender, ImageClickEventArgs e) {
            Clear();
            OnCancel();
        }

        public void Clear() {
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtEmailAddress.Text = string.Empty;
            txtCarrier.Text = string.Empty;
            txtPhoneModel.Text = string.Empty;
            txtComment.Text = string.Empty;
            txtPhoneManufacturer.Text = string.Empty;
            cboPhoneManufacturer.SelectedIndex = 0;
            txtPhoneManufacturer.Visible = false;
        }
    
        protected virtual void OnSave(DeviceRequest deviceRequest) {
            if (Save != null) {
                AddNewPhoneEventArgs e = new AddNewPhoneEventArgs(deviceRequest);
                Save(this, e);
            }
        }

        protected virtual void OnCancel() {
            if (Cancel != null) {
                AddNewPhoneEventArgs e = new AddNewPhoneEventArgs();
                Cancel(this, e);
            }
        }

        protected virtual void OnReload() {
            if (Reload != null) {
                AddNewPhoneEventArgs e = new AddNewPhoneEventArgs();
                Reload(this, e);
            }
        }

        private void cboPhoneManufacturer_DataBinding(object sender, EventArgs e) {
            cboPhoneManufacturer.DataSource = phoneManufacturersDataSource;
        }
    }
}