using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Entity.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;

namespace Neato.Cpt.WebDesigner.Controls {
    public class LinkMenu : UserControl {
        protected Repeater repItems;
        protected HtmlTable menuTable;

        private object dataSourceValue;
        private string textFieldValue = string.Empty;
        private string urlFieldValue = string.Empty;
        private string isNewWindowFieldValue = string.Empty;
        private string cellCssClassValue = string.Empty;
        private string linkCssClassValue = string.Empty;
        private string currentLinkCssClassValue = string.Empty;
        private string labelCssClassValue = string.Empty;
        private string delimeterValue = string.Empty;
        private string linkIdValue = "LinkId";
        private string iconValue = "Icon";
        
        #region Properties
        public object DataSource {
            get { return dataSourceValue; }
            set { dataSourceValue = value; }
        }

        public string TextField {
            get { return textFieldValue; }
            set { textFieldValue = value; }
        }

        public string UrlField {
            get { return urlFieldValue; }
            set { urlFieldValue = value; }
        }

        public string IsNewWindowField {
            get { return isNewWindowFieldValue; }
            set { isNewWindowFieldValue = value; }
        }

        public string CellCssClass {
            get { return cellCssClassValue; }
            set { cellCssClassValue = value; }
        }
        
        public string LinkCssClass {
            get { return linkCssClassValue; }
            set { linkCssClassValue = value; }
        }
        public string CurrentLinkCssClass {
            get { return currentLinkCssClassValue; }
            set { currentLinkCssClassValue = value; }
        }
        public string LabelCssClass {
            get { return labelCssClassValue; }
            set { labelCssClassValue = value; }
        }
        public string Delimeter {
            get { return delimeterValue; }
            set { delimeterValue = value; }
        }
        public string LinkId {
            get { return linkIdValue; }
            set { linkIdValue = value; }
        }
        #endregion Properties

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.DataBinding += new EventHandler(LinkMenu_DataBinding);
            this.repItems.ItemDataBound += new RepeaterItemEventHandler(lstItems_ItemDataBound);
        }
        #endregion

        #region Data binding
        private void LinkMenu_DataBinding(object sender, EventArgs e) {
            repItems.DataSource = dataSourceValue;
        }

        private void lstItems_ItemDataBound(object sender, RepeaterItemEventArgs e) {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem) {
                ItemDataBound(e.Item);
            }
        }

        private void ItemDataBound(RepeaterItem repeaterItem) {
            Object item = repeaterItem.DataItem;
            
            byte[] icon = ReflectionHelper.GetFieldOrPropertyValue(item, iconValue) as byte[];
            string linkId = ReflectionHelper.GetFieldOrPropertyValue(item, LinkId).ToString();
            string text = ReflectionHelper.GetFieldOrPropertyValue(item, TextField).ToString();
            string url = ReflectionHelper.GetFieldOrPropertyValue(item, UrlField).ToString();
            string target = (bool)ReflectionHelper.GetFieldOrPropertyValue(item, IsNewWindowField) ? "_blank" : "";
            
            Literal lit = (Literal)repeaterItem.FindControl("ctlItemTemplate");
            bool isCurrentLink = Request.Url.AbsoluteUri.IndexOf(url) > 0;
            string linkStyle =  (isCurrentLink && currentLinkCssClassValue.Length > 0) ? currentLinkCssClassValue : linkCssClassValue;
            lit.Text = delimeterValue == string.Empty ? string.Empty : string.Format("<img src='{0}'>", delimeterValue);
            
            string img = icon.Length > 1 ? string.Format("<img src='{0}' border=0 align='AbsMiddle'>", IconHandler.ImageUrlForDynamicLink(linkId)) : string.Empty;
            
            if (cellCssClassValue.Length > 0){
                if (url.Length > 0)
                    lit.Text += string.Format("<span class='{0}'><a class='{1}' href='{2}' target='{3}'>{4}{5}</a></span>",
                        cellCssClassValue, linkStyle, url, target, img, text);
                else
                    lit.Text += string.Format("<span class='{0}'><span class='{1}'>{2}{3}</span></span>",
                        cellCssClassValue, linkStyle, img, text);
            } else {
                if (url.Length > 0)
                    lit.Text += string.Format("<a class='{0}' href='{1}' target='{2}'>&nbsp;&nbsp;{3}{4}&nbsp;</a>",
                        linkStyle, url, target, img, text);
                else
                    lit.Text += string.Format("<span class='{0}'>&nbsp;&nbsp;{1}{2}&nbsp;</span>",
                        labelCssClassValue.Length > 0 ? labelCssClassValue : linkStyle, img, text);
            }
        }
        #endregion //Data binding
    }
}