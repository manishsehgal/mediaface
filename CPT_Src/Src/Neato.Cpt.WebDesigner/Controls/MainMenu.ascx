<%@ Control Language="c#" AutoEventWireup="false" Codebehind="MainMenu.ascx.cs" Inherits="Neato.Cpt.WebDesigner.Controls.MainMenu" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="cpt" TagName="LinkMenu" Src="LinkMenu.ascx" %>
<div class="HeaderMenu" id="divHeader" runat="server">
  <div class="MainMenu" style="overflow: hidden;" align="right">
    <cpt:LinkMenu id="ctlLinkMenu" runat="server" CellCssClass="MainMenuCell"
      CurrentLinkCssClass="MainMenuLinkInactive" LinkCssClass="MainMenuLinkActive"
      Delimeter="../images/Spacer.gif" />
  </div>
</div>
