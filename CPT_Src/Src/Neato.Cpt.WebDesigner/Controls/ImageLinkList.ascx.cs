using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.WebDesigner.Controls {
    public class ImageLinkList : UserControl {
        protected DataList lstItems;

        private string navigateUrlFieldValue;
        private string imageUrlFieldValue;
        private string textFieldValue;

        private string scale = string.Empty;
        private string imageCssStyle = string.Empty;
        private string hyperLinkCssStyle = string.Empty;
        private string target = "_self";
        private int width = 135;
        private int height = 135;

        public object DataSource {
            get { return ViewState["ImageLibraryDataSource"]; }
            set { ViewState["ImageLibraryDataSource"] = value; }
        }

        public int ColumsNum {
            get { return lstItems.RepeatColumns; }
            set { lstItems.RepeatColumns = value; }
        }

        public int Width {
            get { return width; }
            set { width = value; }
        }

        public int Height {
            get { return height; }
            set { height = value; }
        }

        public string TextField {
            get { return textFieldValue; }
            set { textFieldValue = value; }
        }

        public string Target {
            get { return target; }
            set { target = value; }
        }

        public string ImageUrlField {
            get { return imageUrlFieldValue; }
            set { imageUrlFieldValue = value; }
        }

        public string NavigateUrlField {
            get { return navigateUrlFieldValue; }
            set { navigateUrlFieldValue = value; }
        }

        public string Scale {
            get { return scale; }
            set { scale = value; }
        }

        public string ImageCssStyle {
            get { return imageCssStyle; }
            set { imageCssStyle = value; }
        }

        public string HyperLinkCssStyle {
            get { return hyperLinkCssStyle; }
            set { hyperLinkCssStyle = value; }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.lstItems.ItemDataBound += new DataListItemEventHandler(lstItems_ItemDataBound);
            this.DataBinding += new EventHandler(ImageLibrary_DataBinding);

        }
        #endregion

        #region Data binding
        private void ImageLibrary_DataBinding(object sender, EventArgs e) {
            lstItems.DataSource = DataSource;
        }

        private void lstItems_ItemDataBound(object sender, DataListItemEventArgs e) {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem) {
                ItemDataBound(e.Item);
            }
        }

        private void ItemDataBound(DataListItem listItem) {
            Object item = listItem.DataItem;

            string text = ReflectionHelper.GetFieldOrPropertyValue(item, TextField).ToString();

            string imageUrl = null;
            if (ReflectionHelper.GetFieldOrPropertyValue(item, ImageUrlField) != null) {
                imageUrl = ReflectionHelper.GetFieldOrPropertyValue(item, ImageUrlField).ToString();
            }

            string navigateUrl = null;
            if (ReflectionHelper.GetFieldOrPropertyValue(item, NavigateUrlField) != null) {
                navigateUrl = ReflectionHelper.GetFieldOrPropertyValue(item, NavigateUrlField).ToString();
            }
            if (!navigateUrl.StartsWith("http://"))
                navigateUrl = "http://" + navigateUrl;

            HyperLink hypImage = null;
            hypImage = (HyperLink)listItem.FindControl("hypImage");

            if (imageUrl != null) {
                hypImage.Visible = true;
                Image img = new Image();
                img.ImageUrl = imageUrl;
                img.Height = Height;
                img.Width = Width;
                hypImage.Controls.Add(img); //    hypImage.ImageUrl = imageUrl;
                hypImage.NavigateUrl = navigateUrl;

                /*if (scale != string.Empty) 
                {
                    hypImage.Style["width"] = scale;
                }*/

                /* if(ImageCssStyle != string.Empty)
                    hypImage.CssClass = ImageCssStyle;*/
                hypImage.Target = Target;
            } else {
                hypImage.Visible = false;
            }

            HyperLink hypText = null;
            hypText = (HyperLink)listItem.FindControl("hypText");

            hypText.Text = HttpUtility.HtmlEncode(text);
            hypText.NavigateUrl = navigateUrl;
            hypText.Target = Target;

        }
        #endregion
    }
}