using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace Neato.Cpt.WebDesigner.Controls
{
	public class FaqSection : UserControl {
        protected HtmlAnchor hypQuestion;
        protected HtmlGenericControl lblAnswer;

        private const string scriptFormat =
            @"  var lblAnswer = document.getElementById('{0}');
                var state = lblAnswer.style.display;
                lblAnswer.style.display = (state == '') ? 'none': '';
                return false;";
        
        private void Page_Load(object sender, System.EventArgs e) {
            if (!IsPostBack) DataBind();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent() {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        public void SetData(string question, string answer) {
            hypQuestion.InnerHtml = "Q: " + question;
            lblAnswer.InnerHtml = "<br>A: " + answer;

            hypQuestion.Attributes["OnClick"] =
                string.Format(scriptFormat, lblAnswer.ClientID);
        }
    }
}