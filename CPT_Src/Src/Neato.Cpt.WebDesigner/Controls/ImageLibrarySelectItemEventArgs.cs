using System;

namespace Neato.Cpt.WebDesigner.Controls {
	public sealed class ImageLibrarySelectItemEventArgs : EventArgs {
        private string navigateUrlValue;

        public string NavigateUrl {
            get { return navigateUrlValue; }
        }

		public ImageLibrarySelectItemEventArgs(string navigateUrl) : base() {
            navigateUrlValue = navigateUrl;
		}
	}
}