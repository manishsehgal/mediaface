<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Header.ascx.cs" Inherits="Neato.Cpt.WebDesigner.Controls.Header" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="cpt" NameSpace="Neato.Cpt.WebDesigner.Controls" Assembly="Neato.Cpt.WebDesigner" %>
<%@ Register TagPrefix="cpt" TagName="MainMenu" Src="MainMenu.ascx" %>
<table class="logoTable" cellpadding="0" cellspacing="0">
    <tr>
        <td class="printzImg" rowspan="2">
            <div><img src="../images/printz.jpg" border="0"></div>
            
        </td>
        
        <td class="stepHeader"><div><img src="../images/header.JPG"></div>
        </td>
    <tr>
        <td class="stepMenu">
            <cpt:MainMenu id="ctlMainMenu" runat="server"/>
        </td>
    </tr>
    <!--<asp:Image ID="imgStep" Runat="server" ImageAlign=AbsMiddle/>
            <asp:Label ID="lblStep" Runat="server" CssClass="numOfStepText"/>--> 
    </TD> </tr>
    <tr>
        <td colspan="2" ><div class="headerRedLine">&nbsp;</div>
        </td>
    </tr>
</table>
<!--<div id="yellLine"></div>-->
