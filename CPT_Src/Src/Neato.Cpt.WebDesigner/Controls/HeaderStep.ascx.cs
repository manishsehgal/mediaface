using System;
using System.Web.UI;

namespace Neato.Cpt.WebDesigner.Controls 
{
    public class HeaderStep : UserControl 
    {
        protected StepSelector ctlStepSelector;
        private int currentStep;
        public int Step 
        {
            set 
            {
                if (value > 0 && value < 4)
                    currentStep = value;
                else
                    currentStep = 1;
            }
        }
        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) 
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() 
        {
            this.DataBinding += new EventHandler(HeaderStep_DataBinding);
        }
        #endregion

        private void HeaderStep_DataBinding(object sender, EventArgs e) 
        {
            ctlStepSelector.Step = currentStep;

        }
    }
}