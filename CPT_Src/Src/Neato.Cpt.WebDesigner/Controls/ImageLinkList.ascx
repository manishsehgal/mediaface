<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ImageLinkList.ascx.cs" Inherits="Neato.Cpt.WebDesigner.Controls.ImageLinkList" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<div class="ScrollPanel2">
    <div class="MarginText">
        <asp:DataList ID="lstItems"
            Runat="server" 
            BorderColor="black"
            CellPadding="5"
            CellSpacing="5"
            RepeatDirection="Horizontal"
            RepeatLayout="Table"
            RepeatColumns="4"
            EnableViewState="true">
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
            <ItemTemplate>
              <div Id="divStandard" Runat="server">
					<div class="ImageLibrary">
					    <asp:HyperLink Runat="server" ID="hypImage"  CssClass="ImageLibrary"  />
					</div>
					<div>
    			        <asp:HyperLink  ID="hypText" Runat="server" CssClass="LinkText Active" />
					</div>
				
			  </div>
		</ItemTemplate>
        </asp:DataList>
    </div>
</div>
