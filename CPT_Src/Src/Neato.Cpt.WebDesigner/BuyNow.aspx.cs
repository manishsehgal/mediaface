using System;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.Controls;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;

namespace Neato.Cpt.WebDesigner {
    public class BuyNow : StandardPage {
        protected Label lblCaption;
        protected ImageLinkList ctlRetailers;

        #region Url
        private const string RawUrl = "BuyNow.aspx";

        public static string PageName() {
            return RawUrl;
        }

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(BuyNow_DataBinding);
            //ctlRetailers.RedirectToUrl += new ImageLibrary.SelectItemEventHandler(RedirectToUrl);
            rawUrl = RawUrl;
        }
        #endregion

        #region DataBinding
        private void BuyNow_DataBinding(object sender, EventArgs e) {
            headerText = BuyNowStrings.TextBuyNow();
            ArrayList list = new ArrayList();
            Retailer[] retailers = BCRetailers.BuyNowRetailerEnum(StorageManager.CurrentRetailer);
            foreach (Retailer ret in retailers) {
                ListSelectItem itm = new ListSelectItem();
                itm.Text = ret.DisplayName;
                itm.ExtInfoUrl = null;
                itm.ImageUrl = IconHandler.ImageUrl(ret).PathAndQuery;
                itm.NavigateUrl = ret.Url;
                list.Add(itm);
            }
            if (list.Count > 0) {
                ctlRetailers.DataSource = (ListSelectItem[])list.ToArray(typeof(ListSelectItem));
                ctlRetailers.TextField = "Text";
                ctlRetailers.ImageUrlField = "ImageUrl";
                ctlRetailers.NavigateUrlField = "NavigateUrl";
                ctlRetailers.Target = "_blank";
                ctlRetailers.Width = ctlRetailers.Height = 135;
            }
            lblCaption.Text = BuyNowStrings.TextBuyNowCaption();
        }
        #endregion
    }
}