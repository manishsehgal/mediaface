using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;

using Neato.Cpt.Business;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner {
    public class Print : Page {
        private const string PdfContentType = "application/pdf";
        private const string scriptKey = "ExitPage";
        private const string scriptData = "<script>window.open('ExitPage.aspx','_blank','height=400,width=600,resizable=1,menubar=1,location=1,scrollbars=1,titlebar=1,toolbar=1,status=1,left=200,top=200,');location.href='Print.aspx'</script>";

        #region Url
        private const string RawUrl = "Print.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl, "time", DateTime.Now.Ticks);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            string time = Request.QueryString["time"];
            Entity.ExitPage exitPage = BCExitPage.Get(StorageManager.CurrentRetailer.Id);
            if (time != null && exitPage != null && exitPage.Enabled) {
                if (!IsClientScriptBlockRegistered(scriptKey))
                    RegisterClientScriptBlock(scriptKey, scriptData);
                return;
            }
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            byte[] content = BCPrinting.GetPdf(StorageManager.CurrentProject, Request.PhysicalApplicationPath);
            Response.Clear();
            Response.ClearHeaders();

            Response.ContentType = PdfContentType;
            Response.AddHeader("content-length", content.Length.ToString(CultureInfo.InvariantCulture));
            Response.BinaryWrite(content);
            Response.End();
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
        }
        #endregion
    }
}