using System;
using System.Collections;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;

namespace Neato.Cpt.WebDesigner.Logic {
    public class SelectFaceLogic {

        public  const string RawUrlParameter     = "URL";
        public  const string ExtInfoUrlParameter = "ExtURL";

        // URL parameter names:
        public const string NodeParamName         = "Node";
        public const string CarrierIdParamName    = "CarrierId";
        public const string BrandIdParamName      = "BrandId";
        public const string DeviceIdParamName     = "DeviceId";
        public const string PaperBrandIdParamName = "PaperBrandId";

        public SelectFaceLogic() {
            ;
        }
        static SelectFaceLogic() {
            CreateSelectHandlers();
        }

        #region Node Handlers Setup
        public const string NodeSelectPhoneByBrandName    = "NodeSelectPhoneByBrand";
        public const string NodeSelectPhoneByDeviceName   = "NodeSelectPhoneByDevice";
        public const string NodeSelectPhoneByCarrierName  = "NodeSelectPhoneByCarrier";
        
        public const string NodeSelectDeviceTypeName      = "NodeSelectDeviceType";
        public const string NodeSelectPhoneName           = "NodeSelectPhone";
        public const string NodeSelectPhonePaperName      = "NodeSelectPhonePaper";
        public const string NodeSelectMediaName           = "NodeSelectMedia";
        public const string NodeSelectMediaByDeviceName   = "NodeSelectMediaByDevice";
        public const string NodeSelectMediaByPaperName    = "NodeSelectMediaByPaper";
        public const string NodeSelectMediaByPaperBrandName = "NodeSelectMediaByPaperBrand";

        public const string NodeSelectPhoneByLastEditedPapersName = "NodeSelectPhoneByLastEditedPapers";

		public const string NodeSelectStickerName      = "NodeSelectSticker";
        public const string NodeSelectStickerPaperName = "NodeSelectStickerPaper";
        
        private static Hashtable selectHandlers;

        public delegate void SelectHandler(SelectHandlerParams selectParams); 

        private static void CreateSelectHandlers() {
            selectHandlers = new Hashtable();
            selectHandlers[NodeSelectPhoneByBrandName] = new SelectHandler(NodeSelectPhoneByBrand);
            selectHandlers[NodeSelectPhoneByCarrierName] = new SelectHandler(NodeSelectPhoneByCarrier);
            selectHandlers[NodeSelectPhoneByDeviceName] = new SelectHandler(NodeSelectPhoneByDevice);
            selectHandlers[NodeSelectPhonePaperName] = new SelectHandler(NodeSelectPhonePaper);
            
            //selectHandlers[NodeSelectDeviceTypeName] = new SelectHandler(NodeSelectDeviceType);
            //selectHandlers[NodeSelectPhoneName] = new SelectHandler(NodeSelectPhone);
            selectHandlers[NodeSelectMediaName] = new SelectHandler(NodeSelectMedia);
            //selectHandlers[NodeSelectMediaByDeviceName] = new SelectHandler(NodeSelectMediaByDevice);
            //selectHandlers[NodeSelectMediaByPaperName] = new SelectHandler(NodeSelectMediaByPaper);
            //selectHandlers[NodeSelectMediaByPaperBrandName] = new SelectHandler(NodeSelectMediaByPaperBrand);

			selectHandlers[NodeSelectStickerName] = new SelectHandler(NodeSelectSticker);
			selectHandlers[NodeSelectStickerPaperName] = new SelectHandler(NodeSelectStickerPaper);
        }

        public static SelectHandler GetSelectHandler(string node) {
            if (selectHandlers.Contains(node)) {
                return (SelectHandler)selectHandlers[node];
            }
            return null;
        }
        #endregion	

        #region SelectHandlerParams Class
        public class SelectHandlerParams {
            public SelectHandlerParams() {
                RequestParams = null;
                ChoiceAreaList = null;
                ItemsList = null;
                StepText = "";
                InfoText = "";
            }
            public Hashtable RequestParams;
            public ChoiceAreaItem[] ChoiceAreaList;
            public ListSelectItem[] ItemsList;
            public string StepText;
            public string InfoText;
        }
        #endregion

        #region Node Handler Methods
        public static void NodeSelectPhoneByBrand(SelectHandlerParams selectParams) {
            selectParams.StepText = DeviceSelectStrings.TextSelectBrand() + ":";

            CarrierBase carrier = null;
            try {
                if (selectParams.RequestParams[CarrierIdParamName] != null) {
                    carrier = new CarrierBase(Int32.Parse((string)selectParams.RequestParams[CarrierIdParamName]));
                }
            }catch(Exception){carrier = null;}

            DeviceBrand[] brands = null;
            if (carrier == null) {
                brands = BCDeviceBrand.GetDeviceWithPaperBrandListWithRetailerCheck(DeviceType.Phone, StorageManager.CurrentRetailer);
            }
            else {
                brands = BCDeviceBrand.GetDeviceWithPaperBrandListWithRetailerCheck(DeviceType.Phone, carrier, StorageManager.CurrentRetailer);
            }

            ListSelectItem[] items = new ListSelectItem[brands.Length];
            int i = 0;
            foreach (DeviceBrand brand in brands) {
                items[i] = new ListSelectItem();
                items[i].Text = brand.Name + DeviceSelectStrings.TextPhones();
                items[i].ImageUrl = IconHandler.ImageUrl(IconHandler.BrandIdParamName, brand.Id).PathAndQuery;
                if (carrier != null) {
                    items[i].NavigateUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhoneByDeviceName, CarrierIdParamName, carrier.Id, BrandIdParamName, brand.Id).PathAndQuery;
                }
                else {
                    items[i].NavigateUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhoneByDeviceName, BrandIdParamName, brand.Id).PathAndQuery;
                }
                i++;
            }
            selectParams.ItemsList = items;

            #region Choice List
            selectParams.ChoiceAreaList = CreatePhoneChoiceAreaList(selectParams);
            #endregion	
        }

        
//        public static void NodeSelectDeviceType(SelectHandlerParams selectParams) 
//        {
//            selectParams.StepText = DeviceSelectStrings.TextSelectDevice();
//
//            #region Choice List
//            ListSelectItem[] choices = new ListSelectItem[2];
//            choices[0] = new ListSelectItem();
//            choices[0].Text = DefaultPageStrings.TextCellPhone();
//            choices[0].ImageUrl = "../images/phone.gif";
//            choices[0].NavigateUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhoneName).PathAndQuery;
//            
//            choices[1] = new ListSelectItem();
//            choices[1].Text = DefaultPageStrings.TextMedia();
//            choices[1].ImageUrl = "../images/media.gif";
//            choices[1].NavigateUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectMediaName).PathAndQuery;
//            
//            choices[1] = new ListSelectItem();
//            choices[1].Text = DefaultPageStrings.TextMp3Player();
//            choices[1].ImageUrl = "../images/mp3.gif";
//            choices[1].NavigateUrl = "";
//            selectParams.ItemsList = choices;
//            #endregion	
//        }
//
//        public static void NodeSelectPhone(SelectHandlerParams selectParams) {
//            selectParams.StepText = DeviceSelectStrings.TextInstructions();
//            selectParams.InfoText = DeviceSelectStrings.TextInfo();
//
//            #region Choice List
//            ListSelectItem[] choices = new ListSelectItem[3];
//            choices[0] = new ListSelectItem();
//            choices[0].Text = DeviceSelectStrings.TextSelectByCarrier();
//            choices[0].NavigateUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhoneByCarrierName).PathAndQuery;
//            choices[1] = new ListSelectItem();
//            choices[1].Text = DeviceSelectStrings.TextSelectByBrand();
//            choices[1].NavigateUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhoneByBrandName).PathAndQuery;
//            choices[2] = new ListSelectItem();
//            choices[2].Text = DeviceSelectStrings.TextSelectAnotherDevice();
//            choices[2].NavigateUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectDeviceTypeName).PathAndQuery;
//            selectParams.ChoiceList = choices;
//            #endregion	
//        }

        public static void NodeSelectPhoneByCarrier(SelectHandlerParams selectParams) {
            selectParams.StepText = DeviceSelectStrings.TextSelectCarrier() + ":";

            Carrier[] carriers = BCCarrier.GetCarrierListByDeviceType(DeviceType.Phone, StorageManager.CurrentRetailer);
            ListSelectItem[] items = new ListSelectItem[carriers.Length];
            int i = 0;
            foreach (Carrier carrier in carriers) 
            {
                items[i] = new ListSelectItem();
                items[i].Text = carrier.Name + DeviceSelectStrings.TextPhones();
                items[i].ImageUrl = IconHandler.ImageUrl(IconHandler.CarrierIdParamName, carrier.Id).PathAndQuery;
                items[i].NavigateUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhoneByDeviceName, CarrierIdParamName, carrier.Id).PathAndQuery;
                i++;
            }
            selectParams.ItemsList = items;

            #region Choice List
            selectParams.ChoiceAreaList = CreatePhoneChoiceAreaList(selectParams);
            #endregion	
        }

        public static void NodeSelectPhoneByDevice(SelectHandlerParams selectParams) {
            selectParams.StepText = DeviceSelectStrings.TextSelectModelShort() + ":";

            CarrierBase carrier = null;
            try {
                if (selectParams.RequestParams[CarrierIdParamName] != null) {
                    carrier = new CarrierBase(Int32.Parse((string)selectParams.RequestParams[CarrierIdParamName]));
                }
            }catch(Exception){carrier = null;}

            DeviceBrandBase brand = null;
            try {
                if (selectParams.RequestParams[BrandIdParamName] != null) {
                    brand = new DeviceBrandBase(Int32.Parse((string)selectParams.RequestParams[BrandIdParamName]));
                }
            }catch(Exception){brand = null;}

            Device[] devices = BCDevice.GetDeviceList(brand, DeviceType.Phone, carrier, StorageManager.CurrentRetailer);

            ArrayList items = new ArrayList();
            foreach (Device device in devices) 
            {
                ListSelectItem item = new ListSelectItem();
                item.Text = device.FullModel;
                item.ImageUrl = IconHandler.ImageUrl(IconHandler.DeviceIdParamName, device.Id).PathAndQuery;

                PaperBase[] papers = BCPaper.GetPaperList(new DeviceBase(device.Id), StorageManager.CurrentRetailer);
                if (papers.Length == 0) //"Papers don`t exist.");
                    continue;

                if (papers.Length == 1) {
                   //  StorageManager.CurrentDevice = device;
                     item.NavigateUrl = Designer.Url(papers[0], device).PathAndQuery;
                }
                else {
                    item.NavigateUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhonePaperName, DeviceIdParamName, device.Id).PathAndQuery;
                }
                item.ExtInfoUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[ExtInfoUrlParameter], IconHandler.DeviceIdParamName, device.Id).AbsoluteUri;
                items.Add(item);
            }
            selectParams.ItemsList = (ListSelectItem[])items.ToArray(typeof(ListSelectItem));

            #region Choice List
            selectParams.ChoiceAreaList = CreatePhoneChoiceAreaList(selectParams);
            #endregion	
        }

        public static void NodeSelectPhonePaper(SelectHandlerParams selectParams) {
            selectParams.StepText = DeviceSelectStrings.TextSelectByPaper() + ":";

            int deviceId = 0;
            try {
                if (selectParams.RequestParams[DeviceIdParamName] != null) {
                    deviceId = Int32.Parse((string)selectParams.RequestParams[DeviceIdParamName]);
                }
            }catch(Exception){deviceId = 0;}
            StorageManager.CurrentDevice = BCDevice.GetDevice(new DeviceBase(deviceId));
            PaperBase[] papers = BCPaper.GetPaperList(new DeviceBase(deviceId), StorageManager.CurrentRetailer);
            ListSelectItem[] items = new ListSelectItem[papers.Length];
            int i = 0;
            foreach (PaperBase paper in papers) {
                Paper p = BCPaper.GetPaper(paper);
                items[i] = new ListSelectItem();
                
                if (p.PaperType == PaperType.DieCut) {
                    items[i].ImageUrl = "../Images/DieCut.gif";
                    items[i].Text = DeviceSelectStrings.TextDieCut();
                } else if (p.PaperType == PaperType.Universal) {
                    items[i].ImageUrl = "../Images/Universal.gif";
                    items[i].Text = DeviceSelectStrings.TextUniversall();
                }
                items[i].NavigateUrl = Designer.Url(paper, new DeviceBase(deviceId)).PathAndQuery;
                i++;
            }
            selectParams.ItemsList = items;

            #region Choice List
            selectParams.ChoiceAreaList = CreatePhoneChoiceAreaList(selectParams);
            #endregion	
        }

        public static void NodeSelectMedia(SelectHandlerParams selectParams) {
            selectParams.StepText = DeviceSelectStrings.TextSelectMP3();
            //selectParams.InfoText = DeviceSelectStrings.TextInfoMedia();

            Device[] devices = BCDevice.GetDeviceList(DeviceType.MP3Player, StorageManager.CurrentRetailer);
            
            ArrayList items = new ArrayList();
            foreach (Device device in devices) {
                ListSelectItem item = new ListSelectItem();
                item.Text = device.FullModel;
                item.ImageUrl = IconHandler.ImageUrl(IconHandler.DeviceIdParamName, device.Id).PathAndQuery;

                
                PaperBase[] papers = BCPaper.GetPaperList(new DeviceBase(device.Id), StorageManager.CurrentRetailer);
                if (papers.Length == 0) //"Papers don`t exist.");
                    continue;

                if (papers.Length == 1) {
                    //  StorageManager.CurrentDevice = device;
                    item.NavigateUrl = Designer.Url(papers[0], device).PathAndQuery;
                }
                else {
                    item.NavigateUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhonePaperName, DeviceIdParamName, device.Id).PathAndQuery;
                }
                item.ExtInfoUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[ExtInfoUrlParameter], IconHandler.DeviceIdParamName, device.Id).AbsoluteUri;
                items.Add(item);
            }
            selectParams.ItemsList = (ListSelectItem[])items.ToArray(typeof(ListSelectItem));
        }

		public static void NodeSelectSticker(SelectHandlerParams selectParams) 
		{
            selectParams.StepText = DeviceSelectStrings.TextSelectSticker();

            Device[] devices = BCDevice.GetDeviceList(DeviceType.Sticker, StorageManager.CurrentRetailer);
            
            ArrayList items = new ArrayList();
            foreach (Device device in devices) {
                ListSelectItem item = new ListSelectItem();
                item.Text = device.Model;
                item.ImageUrl = IconHandler.ImageUrl(IconHandler.DeviceIdParamName, device.Id).PathAndQuery;
                
                PaperBase[] papers = BCPaper.GetPaperList(new DeviceBase(device.Id), StorageManager.CurrentRetailer);
                if (papers.Length == 0) //"Papers don`t exist.");
                    continue;

                if (papers.Length == 1) {
                    //  StorageManager.CurrentDevice = device;
                    item.NavigateUrl = Designer.Url(papers[0], device).PathAndQuery;
                }
                else {
                    item.NavigateUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectStickerPaperName, DeviceIdParamName, device.Id).PathAndQuery;
                }
                item.ExtInfoUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[ExtInfoUrlParameter], IconHandler.DeviceIdParamName, device.Id).AbsoluteUri;
                items.Add(item);
            }
            selectParams.ItemsList = (ListSelectItem[])items.ToArray(typeof(ListSelectItem));
		}
        public static void NodeSelectStickerPaper(SelectHandlerParams selectParams) {
            selectParams.StepText = DeviceSelectStrings.TextSelectByPaper() + ":";

            int deviceId = 0;
            try {
                if (selectParams.RequestParams[DeviceIdParamName] != null) {
                    deviceId = Int32.Parse((string)selectParams.RequestParams[DeviceIdParamName]);
                }
            }catch(Exception){deviceId = 0;}
            StorageManager.CurrentDevice = BCDevice.GetDevice(new DeviceBase(deviceId));
            PaperBase[] papers = BCPaper.GetPaperList(new DeviceBase(deviceId), StorageManager.CurrentRetailer);
            ListSelectItem[] items = new ListSelectItem[papers.Length];
            int i = 0;
            foreach (PaperBase paper in papers) {
                Paper p = BCPaper.GetPaper(paper);
                items[i] = new ListSelectItem();
                
                if (p.PaperType == PaperType.DieCut) {
                    items[i].ImageUrl = "../Images/DieCut.gif";
                    items[i].Text = DeviceSelectStrings.TextDieCut();
                } else if (p.PaperType == PaperType.Universal) {
                    items[i].ImageUrl = "../Images/Universal.gif";
                    items[i].Text = DeviceSelectStrings.TextUniversall();
                }
                items[i].NavigateUrl = Designer.Url(paper, new DeviceBase(deviceId)).PathAndQuery;
                i++;
            }
            selectParams.ItemsList = items;
        }

                
        public static void NodeSelectMediaByDevice(SelectHandlerParams selectParams) {
            selectParams.StepText = DeviceSelectStrings.TextSelectMediaByDevice() + ":";

            Device[] itemsDB = BCDevice.GetDeviceList(null, DeviceType.Media, null, StorageManager.CurrentRetailer);
            ListSelectItem[] items = new ListSelectItem[itemsDB.Length];
            int i = 0;
            foreach (Device itemDB in itemsDB) 
            {
                items[i] = new ListSelectItem();
                items[i].Text = itemDB.FullModel;
                items[i].ImageUrl = IconHandler.ImageUrl(IconHandler.DeviceIdParamName, itemDB.Id).PathAndQuery;

                PaperBase[] papers = BCPaper.GetPaperList(new DeviceBase(itemDB.Id), StorageManager.CurrentRetailer);
                if (papers.Length == 0) throw new NotImplementedException("Papers don`t exist.");
                if (papers.Length == 1) {
                    StorageManager.CurrentDevice = itemDB;
                    items[i].NavigateUrl = Designer.Url(papers[0], itemDB).PathAndQuery;
                }
                else {
                    items[i].NavigateUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectMediaByPaperName, DeviceIdParamName, itemDB.Id).PathAndQuery;
                }
                //items[i].ExtInfoUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[ExtInfoUrlParameter], IconHandler.DeviceIdParamName, itemDB.Id).AbsoluteUri;
                i++;
            }
            selectParams.ItemsList = items;
        }

        public static void NodeSelectMediaByPaperBrand(SelectHandlerParams selectParams) {
            selectParams.StepText = DeviceSelectStrings.TextSelectMediaByPaperBrand() + ":";

            PaperBrand[] itemsDB = BCPaperBrand.GetPaperBrandList((int)DeviceType.Media);
            ListSelectItem[] items = new ListSelectItem[itemsDB.Length];
            int i = 0;
            foreach (PaperBrand itemDB in itemsDB) 
            {
                items[i] = new ListSelectItem();
                items[i].Text = itemDB.Name;
                items[i].ImageUrl = IconHandler.ImageUrl(IconHandler.PaperBrandIdParamName, itemDB.Id).PathAndQuery;
                items[i].NavigateUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectMediaByPaperName, PaperBrandIdParamName, itemDB.Id).PathAndQuery;
                i++;
            }
            selectParams.ItemsList = items;
        }

        public static void NodeSelectMediaByPaper(SelectHandlerParams selectParams) {
            selectParams.StepText = DeviceSelectStrings.TextSelectMediaByPaper() + ":";

            int deviceId = 0;
            try {
                if (selectParams.RequestParams[DeviceIdParamName] != null) {
                    deviceId = Int32.Parse((string)selectParams.RequestParams[DeviceIdParamName]);
                }
            }catch(Exception){deviceId = 0;}

            int paperBrandId = 0;
            try {
                if (selectParams.RequestParams[PaperBrandIdParamName] != null) {
                    paperBrandId = Int32.Parse((string)selectParams.RequestParams[PaperBrandIdParamName]);
                }
            }catch(Exception){paperBrandId = 0;}

            PaperBase[] papers;
            if (deviceId != 0) {
                StorageManager.CurrentDevice = BCDevice.GetDevice(new DeviceBase(deviceId));
                papers = BCPaper.GetPaperList(new DeviceBase(deviceId), StorageManager.CurrentRetailer);
            }
            else if (paperBrandId != 0) {
                papers = BCPaper.GetPaperList(new PaperBrandBase(paperBrandId), DeviceType.Media);
            }
            else {
                papers = BCPaper.GetPaperList(null, DeviceType.Media);
            }

            ListSelectItem[] items = new ListSelectItem[papers.Length];

            int i = 0;
            foreach (PaperBase paper in papers) 
            {
                Paper p = BCPaper.GetPaper(paper);
                items[i] = new ListSelectItem();
                items[i].Text = p.Name;
                items[i].ImageUrl = IconHandler.ImageUrl(IconHandler.PaperIdParamName, paper.Id).PathAndQuery;
                items[i].NavigateUrl = Designer.Url(paper, new DeviceBase(deviceId)).PathAndQuery;
                i++;
            }
            selectParams.ItemsList = items;
        }

        public static ListSelectItem[] NodeSelectPhoneByLastEditedPapers(SelectHandlerParams selectParams) {
            PaperBase[] papers = (PaperBase[]) selectParams.RequestParams[NodeSelectPhoneByLastEditedPapersName];

            ArrayList items = new ArrayList();
            ArrayList uniqueDeviceArray = new ArrayList();

            foreach (PaperBase paper in papers) {
                Device[] devices = BCDevice.GetDeviceListByPaper(paper);

                if (devices.Length == 0) continue;

                Device device = devices[0];
                if (uniqueDeviceArray.Contains(device))
                    continue;
                uniqueDeviceArray.Add(device);

                ListSelectItem item = new ListSelectItem();

                item.Text = device.FullModel;
                item.ImageUrl = IconHandler.ImageUrl(IconHandler.DeviceIdParamName, device.Id).PathAndQuery;
                
                if (devices.Length == 1) {
                    if (BCPaper.GetPaperList(device, StorageManager.CurrentRetailer).Length > 1) {
                        item.NavigateUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhonePaperName, DeviceIdParamName, device.Id).PathAndQuery;
                    } else {
                        StorageManager.CurrentDevice = device;
                        item.NavigateUrl = Designer.Url(paper, device).PathAndQuery;
                    }
                } else {
                    item.NavigateUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhonePaperName, DeviceIdParamName, device.Id).PathAndQuery;
                }
                
                item.ExtInfoUrl = UrlHelper.BuildUrl((string)selectParams.RequestParams[ExtInfoUrlParameter],
                    IconHandler.DeviceIdParamName, device.Id).AbsoluteUri;
                items.Add(item);
            }

            return (ListSelectItem[])items.ToArray(typeof(ListSelectItem));
        }

        #endregion

        private static ChoiceAreaItem[] CreatePhoneChoiceAreaList(SelectHandlerParams selectParams) {
            ChoiceAreaItem[] chItems = new ChoiceAreaItem[2]; 
            {
                DeviceBrand[] brands = BCDeviceBrand.GetDeviceWithPaperBrandListWithRetailerCheck(DeviceType.Phone, StorageManager.CurrentRetailer);
                ChoiceAreaItem chItem = new ChoiceAreaItem();
                chItem.DropDownItems = new ChoiceAreaItem.DropDownItem[brands.Length+2];
                chItem.DropDownItems[0] = new ChoiceAreaItem.DropDownItem(DeviceSelectStrings.TextSelectByBrand(), UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhoneByBrandName).PathAndQuery);
                chItem.DropDownItems[1] = new ChoiceAreaItem.DropDownItem(DeviceSelectStrings.TextSelectAllBrands(), UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhoneByBrandName).PathAndQuery);
                int i = 2;
                foreach (DeviceBrand brand in brands) {
                    chItem.DropDownItems[i] = new ChoiceAreaItem.DropDownItem(brand.Name + DeviceSelectStrings.TextPhones(), UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhoneByDeviceName, BrandIdParamName, brand.Id).PathAndQuery);
                    i++;
                }
                chItems[0] = chItem;
            }
            {
                Carrier[] carriers = BCCarrier.GetCarrierListByDeviceType(DeviceType.Phone, StorageManager.CurrentRetailer);
                ChoiceAreaItem chItem = new ChoiceAreaItem();
                chItem.DropDownItems = new ChoiceAreaItem.DropDownItem[carriers.Length+2];
                chItem.DropDownItems[0] = new ChoiceAreaItem.DropDownItem(DeviceSelectStrings.TextSelectByCarrier(), UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhoneByCarrierName).PathAndQuery);
                chItem.DropDownItems[1] = new ChoiceAreaItem.DropDownItem(DeviceSelectStrings.TextSelectAllCarriers(), UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhoneByCarrierName).PathAndQuery);
                int i = 2;
                foreach (Carrier carrier in carriers) {
                    chItem.DropDownItems[i] = new ChoiceAreaItem.DropDownItem(carrier.Name + DeviceSelectStrings.TextPhones(), UrlHelper.BuildUrl((string)selectParams.RequestParams[RawUrlParameter], NodeParamName, NodeSelectPhoneByDeviceName, CarrierIdParamName, carrier.Id).PathAndQuery);
                    i++;
                }
                chItems[1] = chItem;
            }

            return chItems;
        }
    }
}