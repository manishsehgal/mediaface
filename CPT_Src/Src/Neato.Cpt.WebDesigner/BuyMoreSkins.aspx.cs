using System;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner {
    public class BuyMoreSkins : StandardPage {
        protected Literal ctlPageContent;

        #region Url
        private const string RawUrl = "BuyMoreSkins.aspx";

        public static string PageName() {
            return RawUrl;
        }

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(BuyMoreSkins_DataBinding);
            rawUrl = RawUrl;
        }
        #endregion

        #region DataBinding
        private void BuyMoreSkins_DataBinding(object sender, EventArgs e) {
            string culture = StorageManager.CurrentLanguage;
            LocalizedText pageContent = BCBuyMoreSkinsPage.Get(culture, StorageManager.CurrentRetailer);
            if (pageContent == null)
                pageContent = new LocalizedText(culture, string.Empty);
            ctlPageContent.Text = pageContent.Text;
            headerText = BuyMoreSkinsStrings.TextBuyMoreSkins();
        }
        #endregion
    }
}