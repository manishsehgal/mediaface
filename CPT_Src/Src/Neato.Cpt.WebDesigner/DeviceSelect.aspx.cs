using System;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.Controls;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;
using Neato.Cpt.WebDesigner.Logic;

namespace Neato.Cpt.WebDesigner {
    public class DeviceSelect : StepPage 
    {
        protected ChoiceArea ctlChoiceArea;
        protected ImageLibrary ctlChoiceList;
        protected ImageLibrary ctlDeviceList;
        protected ImageLibrary ctlPrevModelList;
        protected Label lblInfo;
        protected Label lblPreviousModelSelection;
        protected Label lblCurrentStep;
        protected Label lblSelectDevice;
        protected Label lblCantFindYourDevice;
        protected Label lblPhoneRequestInfo;
        protected Label lblRequestPhoneHint1;
        protected Label lblRequestPhoneHint2;

        protected LinkButton btnRequestPhone;
        protected ImageButton btnRequestPhoneArrow;
        protected AddNewPhone ctlRequestPhone;
        protected ImageButton btnClose;
        protected HyperLink hypHome0;
        protected HyperLink hypHome;
        
        protected Label lblStep1;
        protected Label lblStep2;
        protected Label lblStep3;

        #region Url
        private const string RawUrl     = "DeviceSelect.aspx";
        private const string ExtInfoUrl = "DeviceInfo.aspx";

        public static Uri Url() 
        {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri Url(string nodeName) 
        {
            return UrlHelper.BuildUrl(RawUrl, SelectFaceLogic.NodeParamName, nodeName);
        }

        public static Uri Url(string nodeName, int deviceId) 
        {
            return UrlHelper.BuildUrl(RawUrl, SelectFaceLogic.NodeParamName, nodeName, SelectFaceLogic.DeviceIdParamName, deviceId.ToString());
        }

        private Hashtable Params 
        {
            get 
            {
                int count = Request.QueryString.Count;
                Hashtable parameters = new Hashtable(count);
                for (int i = 0; i < count; i++) 
                {
                    parameters[Request.QueryString.Keys[i]] = Request.QueryString[i];
                }
                parameters[SelectFaceLogic.RawUrlParameter] = RawUrl;
                parameters[SelectFaceLogic.ExtInfoUrlParameter] = ExtInfoUrl;
                return parameters;
            }
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);

            Session["FaceSelectionParams"] = Params;

            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) 
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            rawUrl = RawUrl;
            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new System.EventHandler(this.DeviceSelect_DataBinding);
            this.btnRequestPhone.Click += new EventHandler(btnRequestPhone_Click);
            this.btnRequestPhoneArrow.Click += new ImageClickEventHandler(btnRequestPhoneArrow_Click);
            this.btnClose.Click += new ImageClickEventHandler(btnClose_Click);
            this.ctlRequestPhone.Save += new Neato.Cpt.WebDesigner.Controls.AddNewPhone.AddNewPhoneEventHandler(ctlRequestPhone_Save);
            this.ctlRequestPhone.Cancel += new Neato.Cpt.WebDesigner.Controls.AddNewPhone.AddNewPhoneEventHandler(ctlRequestPhone_Cancel);
            this.ctlRequestPhone.Reload += new Neato.Cpt.WebDesigner.Controls.AddNewPhone.AddNewPhoneEventHandler(ctlRequestPhone_Reload);

            this.ctlDeviceList.RedirectToUrl += new ImageLibrary.SelectItemEventHandler(RedirectToUrl);
            this.ctlPrevModelList.RedirectToUrl += new ImageLibrary.SelectItemEventHandler(RedirectToUrl);
        }
        #endregion

        #region Data binding
        private void DeviceSelect_DataBinding(object sender, EventArgs e) {
            Hashtable parameters = (Hashtable)Session["FaceSelectionParams"];

            lblSelectDevice.Text = DeviceSelectStrings.TextSelectDevice();
            SelectFaceLogic.SelectHandler handler =
                SelectFaceLogic.GetSelectHandler
                (parameters[SelectFaceLogic.NodeParamName].ToString());
            if (handler != null) {
                SelectFaceLogic.SelectHandlerParams selectParams = new SelectFaceLogic.SelectHandlerParams();
                selectParams.RequestParams = parameters;
                handler(selectParams);

                lblCurrentStep.Text = selectParams.StepText;
                //lblCurrentStep.Text = DeviceSelectStrings.TextSelectPhone();

                if (selectParams.ItemsList == null) {
                    lblInfo.Visible = true;
                    lblInfo.Text = selectParams.InfoText;
                    ctlDeviceList.Visible = false;
                } 
                else {
                    lblInfo.Visible = false;
                    ctlDeviceList.Visible = true;
                    ctlDeviceList.DataSource = selectParams.ItemsList;
                    if (parameters[SelectFaceLogic.NodeParamName].ToString() == SelectFaceLogic.NodeSelectPhonePaperName ||
                        parameters[SelectFaceLogic.NodeParamName].ToString() == SelectFaceLogic.NodeSelectStickerPaperName) {
                        ctlDeviceList.ColumsNum = 2;
                        ctlDeviceList.Prompt = DeviceSelectStrings.TextPaperSelectPrompt();
                        ctlDeviceList.IsPaperSelect = true;
                    }
                    ctlDeviceList.TextField = "Text";
                    ctlDeviceList.ImageUrlField = "ImageUrl";
                    ctlDeviceList.NavigateUrlField = "NavigateUrl";
                    ctlDeviceList.ExtInfoUrlField = "ExtInfoUrl";
                }

                /*
                if (selectParams.ChoiceList == null) {
                    ctlChoiceList.Visible = false;
                }
                else {
                    ctlChoiceList.Visible = true;
                    ctlChoiceList.DataSource = selectParams.ChoiceList;
                    ctlChoiceList.TextField = "Text";
                    ctlChoiceList.ImageUrlField = "ImageUrl";
                    ctlChoiceList.NavigateUrlField = "NavigateUrl";
                    ctlChoiceList.ExtInfoUrlField = "ExtInfoUrl";
                }
                */
                if (selectParams.ChoiceAreaList == null) {
                    lblSelectDevice.Visible = false;
                    ctlChoiceArea.Visible = false;
                }
                else {
                    lblSelectDevice.Visible = true;
                    ctlChoiceArea.Visible = true;
                    ctlChoiceArea.DataSource = selectParams.ChoiceAreaList;
                }
            } 
            
            //----------- Load lastEditPaper list -------------
            if (StorageManager.CurrentCustomer == null || 
                StorageManager.CurrentCustomer.LastEditedPapers.Length == 0) {
                lblPreviousModelSelection.Visible = false;
                ctlPrevModelList.Visible = false;
            } 
            else {
                Hashtable parameters2 = (Hashtable)Session["FaceSelectionParams"];
                if (!parameters2.ContainsKey(SelectFaceLogic.NodeSelectPhoneByLastEditedPapersName)) {
                    parameters2.Add(
                        SelectFaceLogic.NodeSelectPhoneByLastEditedPapersName,
                        StorageManager.CurrentCustomer.LastEditedPapers);
                }
            
                SelectFaceLogic.SelectHandlerParams selectParams2 = new SelectFaceLogic.SelectHandlerParams();
                selectParams2.RequestParams = parameters2;

                ListSelectItem[] items = SelectFaceLogic.
                    NodeSelectPhoneByLastEditedPapers(selectParams2);

                lblPreviousModelSelection.Visible = true;
                ctlPrevModelList.Visible = true;
                ctlPrevModelList.DataSource = items;
                ctlPrevModelList.TextField = "Text";
                ctlPrevModelList.ImageUrlField = "ImageUrl";
                ctlPrevModelList.NavigateUrlField = "NavigateUrl";
                ctlPrevModelList.ExtInfoUrlField = "ExtInfoUrl";
            }

            ctlRequestPhone.Visible = false;
            btnRequestPhone.Text = DeviceSelectStrings.TextRequestPhone();
            lblPhoneRequestInfo.Text = DeviceSelectStrings.TextCompleteRequestPhone();
            lblPreviousModelSelection.Text = DeviceSelectStrings.TextPreviousModelSelection();
            lblCantFindYourDevice.Text = DeviceSelectStrings.TextCantFindYourDevice();
            lblRequestPhoneHint1.Text = DeviceSelectStrings.TextRequestPhoneHint1();
            lblRequestPhoneHint2.Text = DeviceSelectStrings.TextRequestPhoneHint2();
            lblStep1.Text = DeviceSelectStrings.TextStep1();
            lblStep2.Text = DeviceSelectStrings.TextStep2();
            lblStep3.Text = DeviceSelectStrings.TextStep3();
            if (!IsPostBack) {
                DeviceType[] deviceTypes = new DeviceType[] {DeviceType.Phone, DeviceType.MP3Player};
                ctlRequestPhone.PhoneManufacturersDataSource = GetManufacturerData(deviceTypes);
            }
            hypHome.NavigateUrl = DefaultPage.Url().PathAndQuery;
            int retailerId = StorageManager.CurrentRetailer.Id;
            hypHome.ImageUrl = IconHandler.ImageUrlForLogo(retailerId, false).PathAndQuery;
            hypHome.Visible = BCLogo.GetLogo(retailerId, false) != null;
            hypHome0.ImageUrl = hypHome.ImageUrl;
            hypHome0.Visible = hypHome.Visible;
        }

        private static object GetManufacturerData(DeviceType[] deviceTypes) {
            ListItemCollection items = new ListItemCollection();
            items.Add(new ListItem(AddNewPhoneStrings.TextSelectPhoneManufacturer()));

            ArrayList manufactureList = new ArrayList();
            foreach(DeviceType deviceType in deviceTypes)
                manufactureList.AddRange(BCDeviceBrand.GetDeviceBrandList(deviceType));

            manufactureList.Sort(new DeviceBrand.DeviceBrandNameCompare());
            DeviceBrand[] brands = (DeviceBrand[])manufactureList.ToArray(typeof(DeviceBrand));
            foreach (DeviceBrand brand in brands)
                items.Add(new ListItem(brand.Name));

            items.Add(new ListItem(AddNewPhoneStrings.TextOtherPhoneManufacturer()));
            return items;
        }
        #endregion

        public void ShowRequestPhone() {
            DataBind();
            lblCurrentStep.Visible = false;
            ctlDeviceList.Visible = false;
            lblPhoneRequestInfo.Visible = false;
            btnClose.Visible = false;
            ctlRequestPhone.Visible = true;
        }

        public void ShowCompleteRequestPhone() {
            DataBind();
            if(ctlRequestPhone.IsValid) {
                lblCurrentStep.Visible = false;
                ctlDeviceList.Visible = false;
                lblPhoneRequestInfo.Visible = true;
                btnClose.Visible = true;
                ctlRequestPhone.Visible = false;
            } else {
                lblCurrentStep.Visible = false;
                ctlDeviceList.Visible = false;
                lblPhoneRequestInfo.Visible = false;
                btnClose.Visible = false;
                ctlRequestPhone.Visible = true;
            }
        }

        public void ShowDefault() {
            DataBind();
            this.btnClose.Visible = false;
            this.lblPhoneRequestInfo.Visible = false;
            this.lblCurrentStep.Visible = true;
        }

        private void RedirectToUrl(Object sender, ImageLibrarySelectItemEventArgs e) {
            StorageManager.GenerateNewProject = true;
            Response.Redirect(e.NavigateUrl);
        }

        private void btnClose_Click(object sender, ImageClickEventArgs e) {
            ShowDefault();
        }

        private void btnRequestPhone_Click(object sender, EventArgs e) {
            ShowRequestPhone();
        }

        private void btnRequestPhoneArrow_Click(object sender, ImageClickEventArgs e) {
            ShowRequestPhone();
        }

        private void ctlRequestPhone_Save(Object sender, AddNewPhoneEventArgs e) {
            if (ctlRequestPhone.IsValid) {
                BCDeviceRequest.InsertPhoneRequest(e.DeviceRequest, StorageManager.CurrentLanguage, StorageManager.CurrentRetailer);
                ctlRequestPhone.Clear();
            }
            ShowCompleteRequestPhone();
        }

        private void ctlRequestPhone_Cancel(Object sender, AddNewPhoneEventArgs e) {
            ShowDefault();
        }

        private void ctlRequestPhone_Reload(Object sender, AddNewPhoneEventArgs e) {
            ShowRequestPhone();
        }
    }
}
