using System;
using System.Web.UI;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;

namespace Neato.Cpt.WebDesigner {
    abstract public class BasePage : Page {
        private const string scriptKey="GoogleTracking";
        
        protected string rawUrl;

        private void BasePage_Load(object sender, EventArgs e) {
            GoogleTrackPage trackPage = BCGoogleTracking.GetGoogleTrackPage(rawUrl);
            if (!trackPage.EnableScript)
                return;

            if (!IsClientScriptBlockRegistered(scriptKey))
                RegisterClientScriptBlock(scriptKey, BCGoogleTracking.GetCode());
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(BasePage_Load);
        }
        #endregion
    }
}