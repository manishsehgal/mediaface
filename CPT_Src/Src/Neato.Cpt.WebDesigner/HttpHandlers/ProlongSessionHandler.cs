using System;
using System.Globalization;
using System.Web;
using System.Web.SessionState;

using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner.HttpHandlers {
    public class ProlongSessionHandler : IHttpHandler, IRequiresSessionState {
        #region Url
        private const string RawUrl = "ProlongSession.aspx";
        #endregion

        public ProlongSessionHandler() {}

        public void ProcessRequest(HttpContext context) {
            bool sessionExpired = (StorageManager.CurrentProject == null);
            HttpResponse Response = context.Response;
            String vars = UrlHelper.BuildParameters(
                "sessionExpired", sessionExpired.ToString(CultureInfo.InvariantCulture));
            Response.Write(vars);
            Response.End();
        }
        public bool IsReusable {get { return true; }}
    }
}