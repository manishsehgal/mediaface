using System;
using System.Web;
using System.Web.SessionState;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner.HttpHandlers {
    public class IconHandler : IHttpHandler, IRequiresSessionState {
        #region Url
        private const string RawUrl = "DeviceIcon.aspx";

        public const string DeviceIdParamName = "DeviceId";
        public const string DeviceIdParamNameBigPic = "DeviceIdBig";
        public const string DeviceTypeIdParamName = "DeviceTypeId";
        public const string CarrierIdParamName = "CarrierId";
        public const string BrandIdParamName = "BrandId";
        public const string PaperIdParamName = "PaperId";
        public const string PaperBrandIdParamName = "PaperBrandId";
        public const string RetailerIdParamName = "Retailer";
        public const string LinkIdParamName = "LinkId";
        public const string LogoIdParamName = "LogoId";
        public const string HasMenuParamName = "HasMenuId";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri ImageUrl(string paramName, int Id) {
            return UrlHelper.BuildUrl(RawUrl, paramName, Id);
        }

        public static Uri ImageUrl(CarrierBase carrier) {
            return ImageUrl(CarrierIdParamName, carrier.Id);
        }

        public static Uri ImageUrl(DeviceBase device, bool isBigIcon) {
            return ImageUrl(isBigIcon ? DeviceIdParamNameBigPic : DeviceIdParamName, device.Id);
        }

        public static Uri ImageUrl(DeviceBrandBase brand) {
            return ImageUrl(BrandIdParamName, brand.Id);
        }

        public static Uri ImageUrl(PaperBrandBase paperBrand) {
            return ImageUrl(PaperBrandIdParamName, paperBrand.Id);
        }

        public static Uri ImageUrl(PaperBase paper) {
            return ImageUrl(PaperIdParamName, paper.Id);
        }
        
        public static Uri ImageUrl(Retailer retailer){
            return ImageUrl(RetailerIdParamName, retailer.Id);
        }

        public static Uri ImageUrl(LinkItem item){
            return ImageUrl(LinkIdParamName, item.LinkId);
        }

        public static Uri ImageUrlForDynamicLink(string linkId){
            return ImageUrl(LinkIdParamName, Convert.ToInt32(linkId));
        }

        public static Uri ImageUrlForLogo(int retailerId, bool hasMenu){
            return UrlHelper.BuildUrl(RawUrl, LogoIdParamName, retailerId, HasMenuParamName, hasMenu);
        }
        #endregion

        public void ProcessRequest(HttpContext context) {
            HttpRequest Request = context.Request;
            HttpResponse Response = context.Response;

            Response.ContentType = "image/jpeg";
            byte[] icon = null;

            if (Request.QueryString[DeviceIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[DeviceIdParamName]);
                DeviceBase device = new DeviceBase(id);
                icon = BCDevice.GetDeviceIcon(device);
            } else if (Request.QueryString[CarrierIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[CarrierIdParamName]);
                CarrierBase carrier = new CarrierBase(id);
                icon = BCCarrier.GetCarrierIcon(carrier);
            } else if (Request.QueryString[BrandIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[BrandIdParamName]);
                DeviceBrandBase brand = new DeviceBrandBase(id);
                icon = BCDeviceBrand.GetDeviceBrandIcon(brand);
            } else if (Request.QueryString[DeviceIdParamNameBigPic] != null) {
                Int32 id = Int32.Parse(Request.QueryString[DeviceIdParamNameBigPic]);
                DeviceBase device = new DeviceBase(id);
                icon = BCDevice.GetDeviceBigIcon(device);
            } else if (Request.QueryString[PaperIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[PaperIdParamName]);
                icon = BCPaper.GetPaperIcon(new PaperBase(id));
            } else if (Request.QueryString[PaperBrandIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[PaperBrandIdParamName]);
                icon = BCPaperBrand.GetPaperBrandIcon(new PaperBrandBase(id));
            } else if (Request.QueryString[RetailerIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[RetailerIdParamName]);
                icon = BCRetailers.GetRetailerIcon(id);
            } else if (Request.QueryString[LinkIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[LinkIdParamName]);
                icon = BCDynamicLink.GetIcon(id);
            } else if (Request.QueryString[LogoIdParamName] != null) {
                Int32 id = Int32.Parse(Request.QueryString[LogoIdParamName]);
                bool hasMenu = Boolean.Parse(Request.QueryString[HasMenuParamName]);
                icon = BCLogo.GetLogo(id, hasMenu);
            }

            if (icon != null) {
                Response.OutputStream.Write(icon, 0, icon.Length);
            }

            Response.End();
        }

        public bool IsReusable {
            get { return true; }
        }
    }
}