using System.Web;
using System.Web.SessionState;

using Neato.Cpt.Business;

namespace Neato.Cpt.WebDesigner.HttpHandlers {
    public class RefreshCacheHandler : IHttpHandler, IRequiresSessionState {
        #region Url
        private const string ModeKey = "type";
        private const string DynamicLinkMode = "DynamicLink";
        private const string RetailerMode = "Retailer";
        private const string PlacePropertyMode = "PlaceProperty";
        private const string LogoMode = "Logo";

        private const string RawUrl = "RefreshCache.aspx";

        public static string UrlRefreshDynamicLinks() {
            return string.Format("{0}?{1}={2}", RawUrl, ModeKey, DynamicLinkMode);
        }

        public static string UrlRefreshRetailers() {
            return string.Format("{0}?{1}={2}", RawUrl, ModeKey, RetailerMode);
        }

        public static string UrlRefreshPlaceProperty() {
            return string.Format("{0}?{1}={2}", RawUrl, ModeKey, PlacePropertyMode);
        }

        public static string UrlRefreshLogo() {
            return string.Format("{0}?{1}={2}", RawUrl, ModeKey, LogoMode);
        }

        public static string PageName() {
            return RawUrl;
        }
        #endregion

        public void ProcessRequest(HttpContext context) {
            string mode = context.Request.QueryString[ModeKey];
            switch (mode) {
                case DynamicLinkMode:
                    BCDynamicLink.ResetDynamicLinks();
                    break;
                case RetailerMode:
                    BCRetailers.ResetRetailers();
                    break;
                case PlacePropertyMode:
                    BCPlaceProperty.ResetPlaceProperty();
                    break;
                case LogoMode:
                    BCLogo.Reset();
                    break;
                default:
                    return;
            }
            
        }

        public bool IsReusable {
            get { return true; }
        }
    }
}