using System;
using System.Web;
using System.Web.SessionState;

using Neato.Cpt.Business;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner.HttpHandlers {
    public class FlashLogoHandler : IHttpHandler, IRequiresSessionState {
        #region Url
        private const string RawUrl = "FlashLogo.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        public void ProcessRequest(HttpContext context) {
            HttpResponse Response = context.Response;

            Response.ContentType = "image/jpeg";
            byte[] jpeg = BCLogo.GetFlashLogo(StorageManager.CurrentRetailer.Id);
            if (jpeg != null)
                Response.OutputStream.Write(jpeg, 0, jpeg.Length);
            
            Response.End();
        }

        public bool IsReusable {
            get { return true; }
        }
    }
}