using System.Globalization;
using System.Web;
using System.Web.SessionState;

using Neato.Cpt.Entity;

namespace Neato.Cpt.WebDesigner.HttpHandlers {
    public class CalibrationHandler:IHttpHandler, IRequiresSessionState {
        #region Url
        private const string RawUrl = "Calibration.aspx";
        #endregion

        public CalibrationHandler() {
        }

        public void ProcessRequest(HttpContext context) {
            PdfCalibration calibration = new PdfCalibration();

            calibration.X = float.Parse(context.Request.QueryString["x"], CultureInfo.InvariantCulture);
            calibration.Y = float.Parse(context.Request.QueryString["y"], CultureInfo.InvariantCulture);
            
            StorageManager.SessionCalibration = calibration;
            StorageManager.CookieCalibration = calibration;
        }

        public bool IsReusable {
            get { return true; }
        }
    }
}