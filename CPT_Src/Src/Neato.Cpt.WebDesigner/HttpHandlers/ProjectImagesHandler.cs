using System;
using System.Web;
using System.Web.SessionState;

using Neato.Cpt.Entity.Helpers;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner.HttpHandlers {
    public class ProjectImagesHandler : IHttpHandler, IRequiresSessionState {
        private const string SessionExpired = "Your session is expired!";

        #region Url
        private const string RawUrl = "ProjectImage.aspx";
        private const string ModeKey = "mode";
        private const string ImageMode = "image";
        private const string IconMode = "icon";
        private const string IdKey = "id";

        public static Uri UrlImage(Guid id) {
            return UrlHelper.BuildUrl(RawUrl, ModeKey, ImageMode, IdKey, id.ToString("N"));
        }

        public static Uri UrlIcon(Guid id) {
            return UrlHelper.BuildUrl(RawUrl, ModeKey, IconMode, IdKey, id.ToString("N"));
        }

        private Guid GetIdParam(HttpContext context) {
            try {
                return new Guid(context.Request.QueryString[IdKey]);
            } catch (FormatException) {
                return Guid.Empty;
            } catch (ArgumentNullException) {
                return Guid.Empty;
            }
        }

        #endregion

        public ProjectImagesHandler() {}

        public void ProcessRequest(HttpContext context) {
            string mode = context.Request.QueryString[ModeKey];
            switch (mode) {
                case ImageMode:
                    this.GetImage(context);
                    break;
                case IconMode:
                    this.GetIcon(context);
                    break;
                default:
                    return;
            }
        }

        public bool IsReusable {
            get { return true; }
        }

        public void GetImage(HttpContext context) {
            HttpResponse Response = context.Response;

            Response.ContentType = "image/jpeg";

            Guid id = GetIdParam(context);
            byte[] image = StorageManager.CurrentProject.GetImage(id);
            Response.OutputStream.Write(image, 0, image.Length);
            Response.End();
        }

        public void GetIcon(HttpContext context) {
            HttpResponse Response = context.Response;
            Guid id = GetIdParam(context);
            try {
                byte[] image = StorageManager.CurrentProject.GetImage(id);
                byte[] icon = ImageHelper.GetIcon(image, 50, 50);
                Response.OutputStream.Write(icon, 0, icon.Length);
                Response.ContentType = "image/jpeg";
            } catch {
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Write(SessionExpired);
            }
            Response.End();
        }
    }
}