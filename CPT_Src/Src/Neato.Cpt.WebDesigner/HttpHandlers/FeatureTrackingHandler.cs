using System;
using System.Web;
using System.Web.SessionState;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Tracking;

namespace Neato.Cpt.WebDesigner.HttpHandlers
{
	public class FeatureTrackingHandler: IHttpHandler, IRequiresSessionState {
        #region Url
        private const string RawUrl = "DesignerTracking.aspx";
        #endregion
	
		public FeatureTrackingHandler(){}
        public void ProcessRequest(HttpContext context) {
            if(context.Request.QueryString["type"] != null ) {
                try {
                    Feature feature = (Feature)Feature.Parse
                        (typeof(Feature),context.Request.
                        QueryString["type"],false);
                    if(feature != Feature.None)
                        BCTrackingFeatures.Add(feature, context.Request.UserHostAddress, StorageManager.CurrentRetailer);
                } catch {}
            }
            HttpResponse Response = context.Response;

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetValidUntilExpires(false);

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "text/xml";
            Response.Write("<?xml version=\"1.0\" encoding=\"utf-8\" ?><Node/>");
            Response.End();
        }

        public bool IsReusable {
            get { return true; }
        }
	}
}
