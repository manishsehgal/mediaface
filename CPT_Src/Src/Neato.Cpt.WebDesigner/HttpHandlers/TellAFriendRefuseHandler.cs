using System;
using System.Web;
using System.Web.SessionState;

using Neato.Cpt.Business;
using Neato.Cpt.Entity.Helpers;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner.HttpHandlers {
    public class TellAFriendRefuseHandler : IHttpHandler, IRequiresSessionState {
        #region Url
        private const string RawUrl = "TellAFriendRefuse.aspx";
        
        public static string TellAFriendRefuseEmail(string path, string email) {
            string str = CryptoHelper.Encrypt(email);
            return string.Format("{0}?{1}", path, HttpUtility.UrlEncode(str));
        }
		public static Uri Url() 
		{
			return UrlHelper.BuildUrl(RawUrl);
		}
        public static string PageName() {
            return RawUrl;
        }

        #endregion

        public bool IsReusable {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context) {
            HttpRequest Request = context.Request;
            HttpResponse Response = context.Response;
            
            string info;
            try {
                string encryptedEmail = HttpUtility.UrlDecode(Request.QueryString.ToString());
                string email = CryptoHelper.Decrypt(encryptedEmail);
                BCMail.InsertTellAFriendRefusedUser(email);
                info = string.Format("A user with email: {0} was removed from further tell-a-friend emails", email);
            } catch {
                info = "The link was incorrect";
            }

            
            Response.Write(info);
        }
    }
}