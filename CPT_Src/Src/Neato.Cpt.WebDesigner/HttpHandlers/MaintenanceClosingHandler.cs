using System;
using System.IO;
using System.Web;
using System.Web.SessionState;

using Neato.Cpt.Business;
using Neato.Cpt.Entity.Helpers;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner.HttpHandlers {
    public class MaintenanceClosingHandler : IHttpHandler, IRequiresSessionState {
        public MaintenanceClosingHandler() {}

        #region Url
        private const string RawUrl = "MaintenanceClosing.aspx";
        public const string RetailerKey = "Retailer";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static string PageName() {
            return RawUrl;
        }
        #endregion

        public void ProcessRequest(HttpContext context) {
            switch (context.Request.RequestType) {
                case "GET":
                    GetMode(context);
                    break;
                case "POST":
                    PostMode(context);
                    break;
            }
        }

        public bool IsReusable {
            get { return true; }
        }

        private void PostMode(HttpContext context) {
            string retailerName = context.Request.QueryString[RetailerKey];
            StreamReader reader = new StreamReader(context.Request.InputStream);
            string htmlSource = reader.ReadToEnd();

            string message = string.Empty;

            try {
                BCRetailers.SaveClosePage(htmlSource, context.Request.PhysicalApplicationPath, retailerName);
                message = "Close page was successfully updated";
            } catch (Exception ex) {
                message = ex.Message;
            }
            byte[] data = ConvertHelper.StringToByteArray(message);
            context.Response.OutputStream.Write(data, 0, data.Length);
        }

        private void GetMode(HttpContext context) {
            string retailerName = context.Request.QueryString[RetailerKey];

            try {
                byte[] data = BCRetailers.LoadClosePage(context.Request.PhysicalApplicationPath, retailerName);
                context.Response.ContentType = "text/html";
                context.Response.OutputStream.Write(data, 0, data.Length);
            } catch (Exception ex) {
                throw ex;
            }
        }
    }
}