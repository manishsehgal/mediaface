using System;
using System.Globalization;
using System.Web;
using System.Web.SessionState;

using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner.HttpHandlers {
    public class SettingsHandler : IHttpHandler, IRequiresSessionState {
        #region Url
        private const string RawUrl = "Settings.aspx";
        #endregion

        public SettingsHandler() {}

        public void ProcessRequest(HttpContext context) {
            HttpResponse Response = context.Response;
            PdfCalibration calibration = StorageManager.CookieCalibration;
            StorageManager.SessionCalibration = calibration;
            PdfCalibration calibr = StorageManager.SessionCalibration;
            
            string xcalibration = (calibr.IsSpecified) ?
                calibr.X.ToString("g", CultureInfo.InvariantCulture): "none";
            string ycalibration = (calibr.IsSpecified) ?
                calibr.Y.ToString("g", CultureInfo.InvariantCulture): "none";
            
            String vars = UrlHelper.BuildParameters(
                    "xcalibration", xcalibration,
                    "ycalibration", ycalibration,
                    "culture", StorageManager.CurrentLanguage,
                    "browser", context.Request.Browser.Browser,
                    "platform", context.Request.Browser.Platform
                );
            Response.Write(vars);
            Response.End();
        }

        public bool IsReusable {
            get { return true; }
        }
    }
}