using System;
using System.Web.UI;

using Neato.Cpt.Business;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner {
    public class ExitPage : Page {
        #region Url
        private const string RawUrl = "ExitPage.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Entity.ExitPage exitPage = BCExitPage.Get(StorageManager.CurrentRetailer.Id);
            Response.Write(exitPage.HtmlText);
            Response.End();
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
        }
        #endregion
    }
}