using System;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner {
    public class UploadProject : Page {
        protected HtmlInputFile ctlFile;
        protected ImageButton btnSubmit;
        //protected Button btnCancel;
        protected HtmlTable ctlContent;
        protected HtmlGenericControl ctlUploadContaner;
        protected Label lblCaption;
        protected Label lblPromptText;
        protected Label lblInstructionHeader;
        protected Label lblBrowseInstruction;
        protected Label lblOkInstruction;
        protected Label lblPathNote;
        protected Label lblUploading;
        protected RequiredFieldValidator vldFileRequired;
        protected RegularExpressionValidator vldFilePath;
        protected RegularExpressionValidator vldFileExt;
        protected CustomValidator vldFileExist;
        protected CustomValidator vldFileStructure;
        protected ValidationSummary validationSummary;
        protected bool isMac;

        #region Url
        private const string PageModeKey = "type";
        private const string UploadMode = "UploadMode";
        private const string UploadContentMode = "UploadContentMode";
        private const string RawUrl = "UploadProject.aspx";
        private const string RawBlankUrl = "UploadBlank.aspx";

        public static Uri UrlUpload() {
            return UrlHelper.BuildUrl(RawUrl, PageModeKey, UploadMode);
        }

        private static Uri UrlUploadContent() {
            return UrlHelper.BuildUrl(RawUrl, PageModeKey, UploadContentMode);
        }
        
        public static Uri UrlUploadBlank() {
            return UrlHelper.BuildUrl(RawBlankUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            if (!IsPostBack) {
                isMac = (Request.Browser.Platform.ToLower().IndexOf("mac") != -1);
                string mode = Request.QueryString[PageModeKey];
                switch (mode) {
                    case UploadMode:
                        GenerateContanerResponse();
                        break;
                    case UploadContentMode:
                    default:
                        GenerateUploadResponse();
                        break;
                }
                DataBind();
            }
        }

        private void GenerateUploadResponse() {
            ctlContent.Visible = true;
            ctlUploadContaner.Visible = false;
        }

        private void GenerateContanerResponse() {
            ctlContent.Visible = false;
            ctlUploadContaner.Visible = true;
            ctlUploadContaner.Attributes["src"] = UrlUploadContent().PathAndQuery;
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.btnSubmit.Click += new ImageClickEventHandler(this.btnSubmit_Click);
            //this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(this.UploadManager_DataBinding);
            this.vldFileExist.ServerValidate += new ServerValidateEventHandler(vldFileExist_ServerValidate);
        }
        #endregion

        private void btnSubmit_Click(object sender, ImageClickEventArgs e) {
            if (!Page.IsValid) return;

            try {
                StorageManager.CurrentProject = BCProject.Import(StorageManager.CurrentCustomer, ctlFile.PostedFile.InputStream);
            } catch (BusinessException ex) {
                vldFileStructure.ErrorMessage = ex.Message;
                vldFileStructure.IsValid = false;
            }
            if (!Page.IsValid) return;
            RegisterClientScriptBlock("close", "<script type='text/javascript' language='javascript'>Exit(true);</script>");
        }

        //private void btnCancel_Click(object sender, EventArgs e) {
            //RegisterClientScriptBlock("close", "<script type='text/javascript' language='javascript'>Exit(false);</script>");
        //}

        private void UploadManager_DataBinding(object sender, EventArgs e) {
            lblCaption.Text = UploadProjectStrings.CaptionText();

            if (isMac) {
                lblBrowseInstruction.Text = UploadProjectStrings.BrowseButtonInstructionTextMac();
            } else {
                lblBrowseInstruction.Text = UploadProjectStrings.BrowseButtonInstructionText();
            }

            lblInstructionHeader.Text = UploadProjectStrings.InstructionHeaderText();
            lblPromptText.Text = UploadProjectStrings.UploadPromptText(" (*." + Configuration.DesignProjectFileType + ")");
            lblOkInstruction.Text = UploadProjectStrings.OKButtonInstructionText();
            lblPathNote.Text = UploadProjectStrings.LongFolderPathNote();
            lblUploading.Text = UploadProjectStrings.UploadingText();

            int maxPathLength = 200;
            int fileNameLengthWithoutExt = maxPathLength - Configuration.DesignProjectFileType.Length - 1;

            vldFileRequired.ErrorMessage = UploadProjectStrings.FileNameEmptyErrorMessage();
            vldFilePath.ErrorMessage = UploadProjectStrings.FilePathTooLongErrorMessage();
            vldFilePath.ValidationExpression = ".{1," + maxPathLength.ToString(CultureInfo.InvariantCulture) + "}";
            vldFileExt.ErrorMessage = UploadProjectStrings.WrongFileFormatErrorMessage();
            
            StringBuilder sb = new StringBuilder();
            foreach (char symbol in Configuration.DesignProjectFileType) {
                sb.AppendFormat(CultureInfo.InvariantCulture, "[{0}{1}]", symbol.ToString().ToLower(), symbol.ToString().ToUpper());
            }
            vldFileExt.ValidationExpression = string.Format(CultureInfo.InvariantCulture,
                                                            @"^(.{{1,{0}}}\.({1})) *$|(.{{{2},}}) *$",
                                                            fileNameLengthWithoutExt, sb.ToString(), maxPathLength + 1);
            vldFileStructure.ErrorMessage = string.Empty;

            //btnSubmit.Text = UploadProjectStrings.UploadSubmitButtonCaption();
            //btnSubmit.Attributes["onclick"] = "HideContent();";
            //btnCancel.Text = UploadProjectStrings.UploadCancelButtonCaption();

        }

        private void vldFileExist_ServerValidate(object source, ServerValidateEventArgs args) {
            vldFileExist.ErrorMessage = UploadProjectStrings.FileNotExistErrorMessage(ctlFile.PostedFile.FileName);
            if (ctlFile.PostedFile != null && ctlFile.PostedFile.ContentLength != 0)
                args.IsValid = true;
            else
                args.IsValid = false;
        }
    }
}