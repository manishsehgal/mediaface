<%@ Page language="c#" Codebehind="Lang.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebDesigner.Lang" %>
<%@ Register TagPrefix="cpt" NameSpace="Neato.Cpt.WebDesigner.Controls" Assembly="Neato.Cpt.WebDesigner" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
        <title>Lang</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <link href="../css/style.css" type="text/css" rel="stylesheet">
  </HEAD>
    <body MS_POSITIONING="GridLayout">
        <form id="Form1" method="post" runat="server">
            <table align="center" class="mainTable">
                <tr>
                    <td class="contentTdStyle">
                        <asp:DropDownList ID="cboLang" Runat="server" CssClass="comboTblField"/>
                        <asp:ImageButton ID="btnSubmit" Runat="server" ImageUrl="../Images/btnStart.jpg" />
                        <div align="center">
                            <cpt:AppVersion id="ctlVersion" runat="server" CssClass="clsVersion"/>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</HTML>
