using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Tracking;
using Neato.Cpt.WebDesigner.Controls;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;

namespace Neato.Cpt.WebDesigner {
	public class Designer : StepPage {
		protected HtmlInputHidden ctlDuplicatedFlash;
		protected HtmlInputHidden ctlDownloadProjectUrl;
		protected HtmlInputHidden ctlUploadUrl;
		protected HtmlInputHidden ctlUploadImageUrl;
		protected HtmlInputHidden ctlUploadBlankUrl;
		protected HtmlInputHidden ctlHomeUrl;
		protected HtmlInputHidden ctlLeaveDesignerMessage;
        protected HtmlInputHidden ctlDuplicatedDesignerMessage;
        protected HtmlInputHidden ctlLocalConnectionId;
		protected PopUpBlockerNotifier ctlNotifier;
		protected Label lblPersonalizeDesign;
		protected Label lblFlashRequired;
        
		#region Url
		private const string RawUrl = "Designer.aspx";
		private const string PaperIdParamName = "PaperId";
        private const string DeviceIdParamName = "DeviceId";

		public static Uri Url() {
			return UrlHelper.BuildUrl(RawUrl);
		}

        public static Uri Url(PaperBase paper,DeviceBase device) 
        {
            return UrlHelper.BuildUrl(RawUrl, PaperIdParamName, paper.Id, DeviceIdParamName, device.Id);
        }
		#endregion

		private void Page_Load(object sender, EventArgs e) {
			if (StorageManager.CurrentProject == null && PaperId == UndefinedPaperId) {
				Response.ClearContent();
				Response.ClearHeaders();
				Response.Redirect(DefaultPage.Url().PathAndQuery, true);
			}
            
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.Cache.SetExpires(DateTime.Now);
			Response.Cache.SetValidUntilExpires(false);

            PdfCalibration calibration = StorageManager.CookieCalibration;
            StorageManager.SessionCalibration = calibration;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);

            DataBind();
			
            if (ctlDuplicatedFlash.Value == "false") {
                ctlDuplicatedFlash.Value = "checked";
                BCTracking.Add(UserAction.Designer, Request.UserHostAddress,StorageManager.CurrentRetailer);
                if(Request.QueryString[DeviceIdParamName]!= null) {
                    try 
                    {
                        DeviceBase devicebase = new DeviceBase(int.Parse(Request.QueryString[DeviceIdParamName]));
                        if(BCPaper.GetPaperList(devicebase).Length < 1 && BCDevice.GetDevice(devicebase)==null)
                            StorageManager.CurrentDevice = null;
                        else 
                            StorageManager.CurrentDevice = BCDevice.GetDevice(devicebase);
                    }
                    catch
                    {}
                }
                LoadFace();
                StorageManager.ChangeLastEditedPaper(StorageManager.CurrentProject.ProjectPaper);
            }
		}

		#region Web Form Designer generated code
		protected override void OnInit(EventArgs e) {
			InitializeComponent();
			base.OnInit(e);
		}

		private void InitializeComponent() {
            rawUrl = RawUrl;
			this.Load += new EventHandler(this.Page_Load);
			this.DataBinding += new EventHandler(this.Designer_DataBinding);

		}
		#endregion

		#region Loading project
		private const int UndefinedPaperId = 0;

		private int PaperId {
			get {
				try {
					return int.Parse(Request.QueryString[PaperIdParamName], CultureInfo.InvariantCulture);
				} catch {
					return UndefinedPaperId;
				}
			}
		}

		// TODO: move to business
		private void LoadFace() {
			if (PaperId != UndefinedPaperId) {
                if(StorageManager.GenerateNewProject || StorageManager.CurrentProject == null) {
                    StorageManager.GenerateNewProject = false;
                    DeviceType dt = DeviceType.Undefined;
                    if (StorageManager.CurrentDevice != null) {
                        dt = StorageManager.CurrentDevice.DeviceType;
                    }
                    StorageManager.CurrentProject = BCProject.GetProject(StorageManager.CurrentCustomer, new PaperBase(PaperId), dt);

                    if(StorageManager.CurrentDevice!=null)
                        BCTrackingPhones.Add(StorageManager.CurrentDevice, Request.UserHostAddress, StorageManager.CurrentRetailer);
                }
			}
		}
		#endregion

		private void Designer_DataBinding(object sender, EventArgs e) {
			ctlUploadUrl.Value = UploadProject.UrlUpload().AbsoluteUri;
			ctlUploadImageUrl.Value = UploadImage.UrlUpload().AbsoluteUri;
			ctlUploadBlankUrl.Value = UploadProject.UrlUploadBlank().AbsoluteUri;
			ctlDownloadProjectUrl.Value = ProjectFileHandler.UrlDownloadProject().AbsoluteUri;
			ctlHomeUrl.Value = DefaultPage.Url().AbsoluteUri;
			ctlNotifier.Message = DesignerStrings.PopUpBlockerMessage();
			lblFlashRequired.Text = DesignerStrings.FlashRequired();
			ctlLeaveDesignerMessage.Value = DesignerStrings.TextLeaveDesignerMessage();
            ctlDuplicatedDesignerMessage.Value = DesignerStrings.TextDuplicatedDesignerMessage();
		}
	}
}