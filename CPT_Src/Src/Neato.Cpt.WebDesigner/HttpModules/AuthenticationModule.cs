using System.Security.Principal;
using System.Threading;
using System.Web;

using Neato.Cpt.Entity;

namespace Neato.Cpt.WebDesigner.HttpModules
{
	public class AuthenticationModule : IHttpModule {
        public AuthenticationModule() : base() {}

        #region IHttpModule members
        public void Init(HttpApplication context) {
            context.AcquireRequestState += new System.EventHandler(context_AcquireRequestState); 
        }
        public void Dispose() {}
        #endregion IHttpModule members

        private void context_AcquireRequestState(object sender, System.EventArgs e) {
            string role;
            string name;
            Customer customer = StorageManager.CurrentCustomer;
            if (customer != null) {
                role = Authentication.RegisteredUser;
                name = customer.Email;
            } else {
                role = Authentication.UnregisteredUser;
                name = Authentication.GuestLogin;
            }

            string[] roles = {role};
            GenericIdentity identity = new GenericIdentity(name);
            Thread.CurrentPrincipal = new GenericPrincipal(identity, roles);
            HttpContext.Current.User = Thread.CurrentPrincipal;
        }
    }
}
