using System;
using System.Globalization;
using System.IO;
using System.Web;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Helpers;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;

namespace Neato.Cpt.WebDesigner.HttpModules {
    public class CheckLocationModule : IHttpModule {
        #region Url
        public const string ModeKey = "mode";
        public const string AdminMode = "Admin";
        public const string RetailerIdKey = "RetailerId";

        public static string AdminUrl(string url, Retailer retailer) {
            string adminUrl;
            if (url.IndexOf("?") > 0)
                adminUrl = string.Format("{0}&{1}={2}&{3}={4}", url, ModeKey, AdminMode, RetailerIdKey, retailer == null ? BCRetailers.defaultId : retailer.Id);
            else
                adminUrl = string.Format("{0}?{1}={2}&{3}={4}", url, ModeKey, AdminMode, RetailerIdKey, retailer == null ? BCRetailers.defaultId : retailer.Id);
            return adminUrl;
        }
        #endregion

        public CheckLocationModule() : base() {}

        #region IHttpModule members
        public void Init(HttpApplication context) {
            context.AcquireRequestState += new EventHandler(context_AcquireRequestState);

        }

        public void Dispose() {}
        #endregion IHttpModule members

        private void context_AcquireRequestState(object sender, EventArgs e) {
            HttpRequest request = HttpContext.Current.Request;

            bool isHomePageVisited = HttpContext.Current.Session != null && HttpContext.Current.Session["IsWasAtHomePage"] != null;

            if (HttpContext.Current.Request.QueryString[ModeKey] == AdminMode)
                HttpContext.Current.Session["IsAdminMode"] = bool.TrueString;

            if (!IsAdminMode && !isHomePageVisited) {
                Uri[] nonCheckedUrl = {DefaultPage.Url(), TellAFriendRefuseHandler.Url(), IconHandler.Url(), ForgotPassword.Url(), DeviceInfo.Url()};
                bool gotoHomePage = true;
                foreach (Uri url in nonCheckedUrl) {
                    if (request.Url.AbsolutePath.ToLower(CultureInfo.InvariantCulture) == url.AbsolutePath.ToLower(CultureInfo.InvariantCulture)) {
                        gotoHomePage = false;
                        break;
                    }
                }
                if (gotoHomePage) {
                    HttpContext.Current.Response.Redirect(DefaultPage.Url().PathAndQuery, true);
                }
            }
            if (request.CurrentExecutionFilePath != DeviceSelect.Url().PathAndQuery
                && request.CurrentExecutionFilePath != IconHandler.Url().PathAndQuery) {
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["FaceSelection"] != null) {
                    HttpContext.Current.Session["FaceSelection"] = null;
                }
            }

            if (!IsHttpHandler
                && (HttpContext.Current.Session.IsNewSession || StorageManager.IsSiteClosed)
                && StorageManager.IsSiteClosed
                && !IsAdminMode
                && !IsDesignerPage) {
                RetailerUrlParser rup = new RetailerUrlParser(HttpContext.Current.Request.Url.AbsoluteUri);
                string closePageUrl = RetailerConstants.RetailersFolder;
                if (rup.NoRetailer) {
                    int retailerId = -1;
                    HttpCookie cookie = HttpContext.Current.Request.Cookies["ASP.NET_SessionId"];
                    if (cookie != null) {
                        Object number = HttpContext.Current.Cache.Get(cookie.Value);
                        if (number != null)
                            retailerId = (int)number;
                    }
                    if (HttpContext.Current.Request.QueryString["RetailerId"] != null) {
                        retailerId = int.Parse(HttpContext.Current.Request.QueryString["RetailerId"]);
                    }

                    string retailer = retailerId == -1 ? string.Empty : BCRetailers.GetRetailerNameById(retailerId);
                    closePageUrl = Path.Combine(closePageUrl, retailer);
                    closePageUrl = Path.Combine(closePageUrl, RetailerConstants.ClosePageFileName);
                } else {
                    closePageUrl = Path.Combine(closePageUrl, rup.Retailer);
                    closePageUrl = Path.Combine(closePageUrl, RetailerConstants.ClosePageFileName);
                }

                if (File.Exists(Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, closePageUrl)))
                    closePageUrl = Path.Combine(HttpContext.Current.Request.ApplicationPath, closePageUrl);
                else
                    closePageUrl = Path.Combine(HttpContext.Current.Request.ApplicationPath, RetailerConstants.ClosePageFileName);

                HttpContext.Current.Session.Abandon();
                HttpContext.Current.Response.Redirect(closePageUrl);
            }
            return;
        }

        public static bool IsAdminMode {
            get {
                bool isAdminMode = HttpContext.Current.Session["IsAdminMode"] == null ? false : bool.Parse((string)HttpContext.Current.Session["IsAdminMode"]);
                return (HttpContext.Current.Request.QueryString[ModeKey] == AdminMode) || isAdminMode;
            }
        }

        public static bool IsDesignerPage {
            get {
                string uploadBlankUrl = UrlHelper.BuildUrl("UploadBlank.aspx").PathAndQuery;
                return (HttpContext.Current.Handler is Print
                    || HttpContext.Current.Handler is UploadImage
                    || HttpContext.Current.Handler is UploadProject
                    || HttpContext.Current.Handler is UploadCheck
                    || HttpContext.Current.Request.CurrentExecutionFilePath.ToLower() == uploadBlankUrl.ToLower());
            }
        }

        public static bool IsHttpHandler {
            get {
                return (HttpContext.Current.Handler is CalibrationHandler
                    || HttpContext.Current.Handler is RefreshCacheHandler
                    || HttpContext.Current.Handler is FaceLayoutHandler
                    || HttpContext.Current.Handler is FeatureTrackingHandler
                    || HttpContext.Current.Handler is FlashLogoHandler
                    || HttpContext.Current.Handler is FontFileHandler
                    || HttpContext.Current.Handler is IconHandler
                    || HttpContext.Current.Handler is ImageLibraryHandler
                    || HttpContext.Current.Handler is ProjectFileHandler
                    || HttpContext.Current.Handler is ProjectHandler
                    || HttpContext.Current.Handler is ProjectImagesHandler
                    || HttpContext.Current.Handler is ProlongSessionHandler
                    || HttpContext.Current.Handler is SessionCountHandler
                    || HttpContext.Current.Handler is SettingsHandler
                    || HttpContext.Current.Handler is MaintenanceClosingHandler
                    || HttpContext.Current.Handler is TellAFriendRefuseHandler);
            }
        }
    }

}