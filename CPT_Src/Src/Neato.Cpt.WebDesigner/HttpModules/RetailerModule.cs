using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Web;
using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.WebDesigner.HttpModules {
    public class RetailerModule : IHttpModule {
        private static string retailerIdParam = "RetailerId";
        private static string redirecredParam = "Redirected";

        public RetailerModule() : base() {}

        #region IHttpModule members

        public void Init(HttpApplication context) {
            context.BeginRequest += new EventHandler(context_BeginRequest);
            context.AcquireRequestState += new EventHandler(context_AcquireRequestState);
        }

        public void Dispose() {}

        #endregion IHttpModule members

        private void context_BeginRequest(object sender, EventArgs e) {
            HttpContext context = HttpContext.Current;
            if (IsHandler(context.Request.Url))
                return;

            RetailerUrlParser rup = new RetailerUrlParser(context.Request.Url.AbsoluteUri);
            string ret;
            if (rup.NoRetailer && rup.VirtualDirectory != string.Empty)
                ret = rup.VirtualDirectory;
            else
                ret = rup.Retailer;

            if (rup.IsError404) {
                RetailerUrlParser rupQuery = new RetailerUrlParser(rup.ErrorRedirectUrl);
                if (rupQuery.NoRetailer && rupQuery.VirtualDirectory != string.Empty)
                    ret = rupQuery.VirtualDirectory;
                else
                    ret = rupQuery.Retailer;
                if (ret != string.Empty) {
                    string newpath = "~/" + ret + "/default.aspx";
                    context.Response.Redirect(newpath);
                }
            }

            Retailer retailer = BCRetailers.GetRetailerByName(ret);
            bool isPostRequest = context.Request.RequestType.ToUpper() == "POST";
            bool wasRedirected = ((string) context.Cache.Get(redirecredParam)) == bool.TrueString;
            if (ret == string.Empty || retailer == null || (!retailer.HasBrandedSite && !wasRedirected && !isPostRequest) || rup.HasPath) {
                int retailerId = -1;
                if (retailer == null) {
                    HttpCookie cookie = context.Request.Cookies["ASP.NET_SessionId"];
                    if (cookie != null) {
                        Object number = context.Cache.Get(cookie.Value);
                        if (number != null)
                            retailerId = (int) number;
                    }
                    if (context.Request.QueryString["RetailerId"] != null) {
                        retailerId = int.Parse(context.Request.QueryString["RetailerId"]);
                    }
                }
                else {
                    retailerId = retailer.Id;
                }

                context.Cache.Insert(redirecredParam, bool.TrueString, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));

                string retailerName = retailerId == -1 ? BCRetailers.GetDefaultRetailerName() : BCRetailers.GetRetailerNameById(retailerId);

                NameValueCollection query = new NameValueCollection(context.Request.QueryString);
                query.Remove(retailerIdParam);

                string filePath = Path.Combine(context.Request.PhysicalApplicationPath, rup.Page);
                string page = File.Exists(filePath) ? rup.Page : "default.aspx";
                string newpath = "~/" + retailerName + "/" + page + (rup.IsError404 || rup.Query == string.Empty ? string.Empty : ("?" + rup.Query));
                context.Response.Redirect(newpath);
            }
            else {
                int retailerId = BCRetailers.GetId(ret);
                NameValueCollection query = new NameValueCollection(context.Request.QueryString);
                query.Add(retailerIdParam, retailerId.ToString());

                if (wasRedirected) {
                    context.Cache.Remove(redirecredParam);
                }

                HttpCookie cookie = context.Request.Cookies["ASP.NET_SessionId"];
                if (cookie != null) {
                    string sessionId = cookie.Value;
                    context.Cache.Insert(sessionId, retailerId, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
                    query.Remove(retailerIdParam);
                }

                string queryString = queryToQueryString(query);
                if (queryString.Length > 0)
                    queryString = queryString.Substring(1);

                string filePath = Path.Combine(context.Request.PhysicalApplicationPath, rup.Page);
                string page = File.Exists(filePath) ? rup.Page : "default.aspx";
                string newpath = "~/" + page;

                context.RewritePath(newpath, "", queryString);
            }
        }

        private string queryToQueryString(NameValueCollection query) {
            int count = query.Count;
            if (count == 0)
                return string.Empty;

            string queryString = string.Empty;
            for (int i = 0; i < count; ++i) {
                queryString += string.Format("{0}{1}={2}",
                                             i > 0 ? "&" : "?",
                                             query.GetKey(i),
                                             query.GetValues(i)[0]);
            }

            return queryString;
        }

        private void context_AcquireRequestState(object sender, EventArgs e) {
            HttpContext context = HttpContext.Current;

            string retailerQuery = context.Request.QueryString[retailerIdParam];
            HttpCookie cookie = context.Request.Cookies["ASP.NET_SessionId"];
            int retailerId = 1;
            string sessionId = cookie.Value;
            if (retailerQuery != null)
                retailerId = int.Parse(retailerQuery);
            else {
                Object number = context.Cache.Get(sessionId);
                if (number != null)
                    retailerId = (int) number;
            }

            context.Cache.Insert(sessionId, retailerId, null, DateTime.MaxValue, TimeSpan.FromMinutes(20));
            StorageManager.CurrentRetailer = BCRetailers.GetRetailer(retailerId);
        }

        private static string[] handlers = null;

        private static string[] EnumHandlers() {
            Assembly WebDesignerAssembly = Global.GetAssembly();
            Type[] types = WebDesignerAssembly.GetExportedTypes();
            ArrayList list = new ArrayList();
            foreach (Type type in types) {
                if (type.Namespace == "Neato.Cpt.WebDesigner.HttpHandlers") {
                    FieldInfo fieldInfo = type.GetField("RawUrl", BindingFlags.NonPublic | BindingFlags.Static);
                    list.Add(fieldInfo.GetValue(null).ToString().ToLower());
                }
            }
            return (string[]) list.ToArray(typeof (string));
        }

        private static bool IsHandler(Uri url) {
            if (handlers == null)
                handlers = EnumHandlers();
            foreach (string handler in handlers) {
                if (url.LocalPath.ToLower().EndsWith(handler))
                    return true;
            }
            return false;
        }
    }
}