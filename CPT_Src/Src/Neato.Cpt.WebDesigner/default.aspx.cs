using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Tracking;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.Logic;

namespace Neato.Cpt.WebDesigner {
    public class DefaultPage : StandardPage {
    
        protected Literal  litCookieDisabled;
		protected Repeater rptDevices;
        protected Panel    divDevices;
						        
        #region Url
        private const string RawUrl = "default.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        public static string PageName() {
            return RawUrl;
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetValidUntilExpires(false);
            Session["IsWasAtHomePage"]=new bool();
            if (!IsPostBack) {
                if (User.IsInRole(Authentication.RegisteredUser)) {
                    BCTracking.Add(UserAction.Login, Request.UserHostAddress,StorageManager.CurrentRetailer);
                }
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            rawUrl = RawUrl;
            isHome = true;

            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
			this.rptDevices.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptDevices_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);
			this.DataBinding += new System.EventHandler(this.Page_DataBinding);

		}
        #endregion

        private void Page_DataBinding(object sender, EventArgs e) {
           
			StorageManager.GenerateNewProject = true;

            litCookieDisabled.Text=DefaultPageStrings.TextCookieDisabled();

            string login = User.Identity.Name;
            SpecialUser[] users = BCSpecialUser.GetSpecialUsers(login, 1);

			ArrayList a_data = new ArrayList();
			
			if (StorageManager.CurrentRetailer.PhoneAvailability != DeviceAvailability.ec_NotAvailable) {
                Hashtable a_subData = new Hashtable();
                a_subData["image"]      = "../images/phone.gif";
                a_subData["image_over"] = "../images/phone_over.gif";
                if (StorageManager.CurrentRetailer.PhoneAvailability == DeviceAvailability.ec_Available || users.Length > 0) {
                    a_subData["URL"] = GetDeviceUrl(DeviceType.Phone);
                }
                a_data.Add(a_subData);
			}

			if (StorageManager.CurrentRetailer.Mp3Availability != DeviceAvailability.ec_NotAvailable) {
				Hashtable a_subData = new Hashtable();
				a_subData["image"]      = "../images/mp3.gif";
				a_subData["image_over"] = "../images/mp3_over.gif";
				if (StorageManager.CurrentRetailer.Mp3Availability == DeviceAvailability.ec_Available || users.Length > 0) {
                    a_subData["URL"] = GetDeviceUrl(DeviceType.MP3Player);
                }
				a_data.Add(a_subData);
			}

			if (StorageManager.CurrentRetailer.StickerAvailability != DeviceAvailability.ec_NotAvailable) {
				Hashtable a_subData = new Hashtable();
				a_subData["image"]      = "../images/stickers.gif";
				a_subData["image_over"] = "../images/stickers_over.gif";
				if (StorageManager.CurrentRetailer.StickerAvailability == DeviceAvailability.ec_Available || users.Length > 0) {
                    a_subData["URL"] = GetDeviceUrl(DeviceType.Sticker);
                }
				a_data.Add(a_subData);
			}

			rptDevices.DataSource = a_data;
			rptDevices.DataBind();

            if (a_data.Count >= 3) {
                divDevices.Style["position"] = "absolute";
                divDevices.Style["top"] = "60px";
                divDevices.Style["left"] = "100px";
            } else {
                divDevices.Style["position"] = "absolute";
                divDevices.Style["top"] = "40px";
                divDevices.Style["left"] = "200px";
            }
        }

		private void rptDevices_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) 
			{
				HyperLink link = (HyperLink) e.Item.FindControl("lnkDevice");
				HtmlImage img  = (HtmlImage) e.Item.FindControl("imgDevice");

				Hashtable table = (Hashtable)e.Item.DataItem;
				if (table.ContainsKey("URL"))
				{
					link.Visible = true;
					img.Visible = false;
					link.NavigateUrl = table["URL"].ToString();
					link.ImageUrl = table["image"].ToString();
				} 
				else 
				{
					link.Visible = false;
					img.Visible = true;
					img.Src = table["image"].ToString();
					img.Attributes["onmouseover"] = "src='" + table["image_over"].ToString() + "'";
					img.Attributes["onmouseout"]  = "src='" + table["image"].ToString() + "'";
				}
			}
		}

        private string GetDeviceUrl(DeviceType deviceType) {
            string url = "";

            Device[] devices = BCDevice.GetDeviceList(deviceType, StorageManager.CurrentRetailer);

            if (devices.Length > 0) {
                if (deviceType == DeviceType.Phone) {
                    if (devices.Length > 1) {
                        url = DeviceSelect.Url(SelectFaceLogic.NodeSelectPhoneByBrandName).PathAndQuery;
                    } else {
                        PaperBase[] papers = BCPaper.GetPaperList(new DeviceBase(devices[0].Id), StorageManager.CurrentRetailer);
                        if (papers.Length > 1) {
                            url = DeviceSelect.Url(SelectFaceLogic.NodeSelectPhonePaperName, devices[0].Id).PathAndQuery;
                        } else {
                            url = Designer.Url(papers[0], devices[0]).PathAndQuery;
                        }
                    }
                }
                else if (deviceType == DeviceType.MP3Player) {
                    if (devices.Length > 1) {
                        url = DeviceSelect.Url(SelectFaceLogic.NodeSelectMediaName).PathAndQuery;
                    } else {
                        PaperBase[] papers = BCPaper.GetPaperList(new DeviceBase(devices[0].Id), StorageManager.CurrentRetailer);
                        if (papers.Length > 1) {
                            url = DeviceSelect.Url(SelectFaceLogic.NodeSelectMediaByPaperName, devices[0].Id).PathAndQuery;
                        } else {
                            url = Designer.Url(papers[0], devices[0]).PathAndQuery;
                        }
                    }
                }
                else if (deviceType == DeviceType.Sticker) {
                    if (devices.Length > 1) {
                        url = DeviceSelect.Url(SelectFaceLogic.NodeSelectStickerName).PathAndQuery;
                    } else {
                        PaperBase[] papers = BCPaper.GetPaperList(new DeviceBase(devices[0].Id), StorageManager.CurrentRetailer);
                        if (papers.Length > 1) {
                            url = DeviceSelect.Url(SelectFaceLogic.NodeSelectStickerPaperName, devices[0].Id).PathAndQuery;
                        } else {
                            url = Designer.Url(papers[0], devices[0]).PathAndQuery;
                        }
                    }
                }
            }

            return url;
        }
    }
}