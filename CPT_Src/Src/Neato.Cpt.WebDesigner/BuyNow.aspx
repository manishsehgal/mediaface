<%@ Page language="c#" Codebehind="BuyNow.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebDesigner.BuyNow" %>
<%@ Register TagPrefix="cpt" TagName="MainMenu" Src="Controls/MainMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="ImageLinkList" Src="Controls/ImageLinkList.ascx" %>
<%@ Register TagPrefix="cpt" TagName="FooterMenu" Src="Controls/FooterMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="Header" Src="Controls/Header.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>Fellowes - BUY NOW</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../css/style.css" type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
       <asp:Label  CssClass="Text YellowHeader" ID="lblCaption" runat="server"/>
               <cpt:ImageLinkList id="ctlRetailers" runat="server" ColumsNum="4"  HyperLinkCssStyle="LinkEnlargeNavy" />
	    </form>
  </body>
</html>