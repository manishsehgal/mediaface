using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Neato.Cpt.WebDesigner {
    public class PrintSetup : Page {
        protected Label lblHdr;
        protected Label lblText;
        protected Image imgScreenShot;

        private const string readerKey = "reader";

        #region ReaderInfo collection
        //collection
        private static Hashtable readerInfos;
        //stetic ctor to fill collection
        static PrintSetup() {
            CreateReaderInfo();
        }

        //class for readr info
        private class ReaderInfo {
            public ReaderInfo(string hdrId, string textId, string image) {
                HdrId = hdrId;
                TextId = textId;
                Image = image;
            }

            public string HdrId;
            public string TextId;
            public string Image;
        }

        //method to fill collection
        private static void CreateReaderInfo() {
            readerInfos = new Hashtable();
            readerInfos["w5"] = new ReaderInfo("ReaderName5", "HelpText", "arw5.gif");
            readerInfos["w6"] = new ReaderInfo("ReaderName6", "HelpText", "arw6.gif");
            readerInfos["w7"] = new ReaderInfo("ReaderName7", "HelpText", "arw7.gif");
            readerInfos["m6"] = new ReaderInfo("ReaderName6", "HelpText", "arm6.gif");
            readerInfos["m7"] = new ReaderInfo("ReaderName7", "HelpText", "arm7.gif");
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            string rd = Request.QueryString[readerKey];
            if (rd == null) return;

            ReaderInfo ri = (ReaderInfo)readerInfos[rd];
            if (ri == null) return;

            lblHdr.Text = PrintSetupStrings.GetString(ri.HdrId);
            lblText.Text = PrintSetupStrings.GetString(ri.TextId);
            imgScreenShot.ImageUrl = "..\\images\\" + ri.Image;
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);

        }
        #endregion
    }
}