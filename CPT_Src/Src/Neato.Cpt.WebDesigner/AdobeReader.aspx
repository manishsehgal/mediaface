<%@ Page language="c#" Codebehind="AdobeReader.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebDesigner.AdobeReader" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Fellowes - Adobe Reader is reqired</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <STYLE TYPE="text/css">
        body {
            background: #000000;
            padding: 5px;
        }
        p {
            color: #FFFFFF;
            font-size: 11pt;
            font-family: Arial;
            text-indent: 10px;
            margin: 10px;
        }
    </STYLE>
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
        <p>
          PDF files on the Custom Printz site require that you have at least version 6 of the Adobe Acrobat reader;
          however, we recommend version 7 of the Acrobat Reader application for the highest level of compatibility.
          Acrobat Reader is an Adobe product. If you encounter any technical difficulties during installation 
          or during normal usage, the Adobe Acrobat Reader support page can be found at
          <a href="http://www.adobe.com/support/products/acrreader.html">http://www.adobe.com/support/products/acrreader.html</a>
          <br><br>
          <a href="http://www.adobe.com/products/acrobat/readstep2.html"><img src="../images/AcrobatReader.png" border="0"></a>
        </p>
    </form>
  </body>
</html>
