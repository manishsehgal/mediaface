<%@ Register TagPrefix="cpt" TagName="Header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="cpt" TagName="LinkList" Src="Controls/LinkList.ascx" %>
<%@ Register TagPrefix="cpt" TagName="FooterMenu" Src="Controls/FooterMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="MainMenu" Src="Controls/MainMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="Faq" Src="Controls/Faq.ascx" %>
<%@ Page language="c#" Codebehind="Support.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebDesigner.Support" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
    <HEAD>
        <title>Fellowes - Help</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <link href="../css/style.css" type="text/css" rel="stylesheet">
    </HEAD>
    <body MS_POSITIONING="GridLayout">
        <form id="Form1" method="post" runat="server">
			<div>
				<div class="Paragraph" id="DynamicContent">
					<asp:Literal id="ctlPageContent" runat="server" />
				</div>
				<div>
					<a ID="hypTellUs" Runat="server" href="Feedback.aspx" class="TellUsLink"/>
					<br/>
				</div>
				<br/>
				<div>
					<asp:Label ID="lblFaq" Runat="server" />
				</div>
				<div>
					<cpt:Faq id="ctlFaq" runat="server"/>
				</div>
			</div>
        </form>
    </body>
</HTML>
