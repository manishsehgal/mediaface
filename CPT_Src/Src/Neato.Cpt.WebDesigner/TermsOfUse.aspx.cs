using System;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner {
    public class TermsOfUse : StandardPage {
        protected Literal ctlPageContent;

        #region Url
        private const string RawUrl = "TermsOfUse.aspx";

        public static string PageName() {
            return RawUrl;
        }

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(TermsOfUse_DataBinding);
            rawUrl = RawUrl;
        }
        #endregion

        #region DataBinding
        private void TermsOfUse_DataBinding(object sender, EventArgs e) {
            string culture = StorageManager.CurrentLanguage;
            LocalizedText pageContent = BCTermsOfUsePage.Get(culture, StorageManager.CurrentRetailer);
            if (pageContent == null)
                pageContent = new LocalizedText(culture, string.Empty);
            ctlPageContent.Text = pageContent.Text;
            headerText = TermsOfUseStrings.TextTermsOfUse();
        }
        #endregion
    }
}