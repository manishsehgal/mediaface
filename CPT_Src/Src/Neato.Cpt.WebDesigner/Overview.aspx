<%@ Page language="c#" Codebehind="Overview.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebDesigner.Overview" %>
<%@ Register TagPrefix="cpt" TagName="MainMenu" Src="Controls/MainMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="FooterMenu" Src="Controls/FooterMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="Header" Src="Controls/Header.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>Fellowes - Overview</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../css/style.css" type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
        <div class="Paragraph" id="DynamicContent">
	        <asp:Literal id="ctlPageContent" runat="server"/>
	    </div>
    </form>
  </body>
</html>
