using System;
using System.ComponentModel;
using System.IO;
using System.Web;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.MaintenanceClosing {
    public class Global : HttpApplication {
        private IContainer components = null;

        public Global() {
            InitializeComponent();
        }

        protected void Application_Start(Object sender, EventArgs e) {
        }

        protected void Session_Start(Object sender, EventArgs e) {
        }

        protected void Application_BeginRequest(Object sender, EventArgs e) {
            HttpContext context = HttpContext.Current;
            RetailerUrlParser rup = new RetailerUrlParser(context.Request.Url.AbsoluteUri);

            string closePageUrl;
            if(rup.NoRetailer) {
                closePageUrl = RetailerConstants.ClosePageFileName;
            }
            else {
                closePageUrl = Path.Combine(rup.Retailer, RetailerConstants.ClosePageFileName);
            }

            if (File.Exists(Path.Combine(Request.PhysicalApplicationPath, closePageUrl)))
                closePageUrl = Path.Combine(Request.ApplicationPath, closePageUrl);
            else
                closePageUrl = Path.Combine(Request.ApplicationPath, RetailerConstants.ClosePageFileName);

            Response.Redirect(closePageUrl);
        }

        protected void Application_EndRequest(Object sender, EventArgs e) {
        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e) {
        }

        protected void Application_Error(Object sender, EventArgs e) {
        }

        protected void Session_End(Object sender, EventArgs e) {
        }

        protected void Application_End(Object sender, EventArgs e) {
        }

        #region Web Form Designer generated code

        private void InitializeComponent() {
            this.components = new Container();
        }

        #endregion
    }
}