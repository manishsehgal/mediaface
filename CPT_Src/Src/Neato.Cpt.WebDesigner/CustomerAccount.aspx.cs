using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.Entity.Tracking;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner {
    public class CustomerAccount : StandardPage {
        
        protected Label lblRequired;
        protected Label lblFirstName;
        protected TextBox txtFirstName;
        protected RequiredFieldValidator vldFirstName;
        protected Label lblLastName;
        protected TextBox txtLastName;
        protected RequiredFieldValidator vldLastName;
        protected Label lblEmailAddress;
        protected TextBox txtEmailAddress;
        protected RequiredFieldValidator vldEmailAddress;
        protected CustomValidator vldValidEmail;
        protected CustomValidator vldUniqueEmail;
        protected Label lblOldPassword;
        protected TextBox txtOldPassword;
        protected RequiredFieldValidator vldOldPassword;
        protected CustomValidator vldWrongOldPassword;
        protected Label lblNewPassword;
        protected TextBox txtNewPassword;
        protected RequiredFieldValidator vldNewPassword;
        protected Label lblConfirmPassword;
        protected TextBox txtConfirmPassword;
        protected RequiredFieldValidator vldConfirmPassword;
        protected CustomValidator vldCompareConfirmPassword;
        protected Label lblEmailOptions;
        protected DropDownList cboEmailOptions;
        protected CheckBox chkReceiveInfo;
        protected ImageButton btnOK;
        protected ImageButton btnCancel;
        protected ImageButton btnClose;
        protected Label lblUnexpectedError;
        protected HtmlTable tblCustomer;
        protected HtmlTable tblConfirmation;
        protected Label lblConfirmation;

        #region Url
        private const string RawUrl = "CustomerAccount.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
            this.ctlAccessToAccount.Logout += new EventHandler(ctlAccessToAccount_Logout);
            this.ctlAccessToAccount.Login += new Neato.Cpt.WebDesigner.Controls.AccessToAccount.AccessToAccountEventHandler(ctlAccessToAccount_Login);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(this.CustomerAccount_DataBinding);
            this.PreRender += new EventHandler(CustomerAccount_PreRender);
            this.btnOK.Click += new ImageClickEventHandler(btnOK_Click);
            this.btnCancel.Click += new ImageClickEventHandler(btnCancel_Click);
            this.btnClose.Click += new ImageClickEventHandler(btnClose_Click);
            this.vldWrongOldPassword.ServerValidate += new ServerValidateEventHandler(vldWrongOldPassword_ServerValidate);
            this.vldUniqueEmail.ServerValidate += new ServerValidateEventHandler(vldUniqueEmail_ServerValidate);
            this.vldValidEmail.ServerValidate += new ServerValidateEventHandler(vldValidEmail_ServerValidate);
            this.vldCompareConfirmPassword.ServerValidate += new ServerValidateEventHandler(vldCompareConfirmPassword_ServerValidate);
            rawUrl = RawUrl;
        }
        #endregion

        #region DataBinding
        private void CustomerAccount_DataBinding(object sender, EventArgs e) {
            headerText = CustomerAccountStrings.TextCustomerAccount();
            lblRequired.Text = CustomerAccountStrings.TextRequired();
            lblFirstName.Text = CustomerAccountStrings.TextFirstName();
            vldFirstName.Text = CustomerAccountStrings.ValidatorFirstName();
            lblLastName.Text = CustomerAccountStrings.TextLastName();
            vldLastName.Text = CustomerAccountStrings.ValidatorLastName();
            lblEmailAddress.Text = CustomerAccountStrings.TextEmailAddress();
            vldEmailAddress.Text = CustomerAccountStrings.ValidatorEmailAddress();
            vldValidEmail.Text = CustomerAccountStrings.ValidatorInvalidEmailAddress();
            lblOldPassword.Text = CustomerAccountStrings.TextOldPassword();
            vldOldPassword.Text = CustomerAccountStrings.ValidatorOldPassword();
            vldWrongOldPassword.Text = CustomerAccountStrings.ValidatorWrongOldPassword();
            lblNewPassword.Text = CustomerAccountStrings.TextPassword();
            vldNewPassword.Text = CustomerAccountStrings.ValidatorPassword();
            lblConfirmPassword.Text = CustomerAccountStrings.TextConfirmPassword();
            vldConfirmPassword.Text = CustomerAccountStrings.ValidatorConfirmPassword();
            vldCompareConfirmPassword.Text = CustomerAccountStrings.ValidatorCompareConfirmPassword();

            lblEmailOptions.Text = CustomerAccountStrings.TextEmailOptions();
            chkReceiveInfo.Text = CustomerAccountStrings.TextReceiveInfo();
            //btnOK.Text = CustomerAccountStrings.TextOK();
            //btnCancel.Text = CustomerAccountStrings.TextCancel();
            vldUniqueEmail.Text = CustomerAccountStrings.ValidatorUniqueEmail();
            lblConfirmation.Text = CustomerAccountStrings.TextCustomerAdded();

            if (cboEmailOptions.Items.Count == 0) {
                cboEmailOptions.Items.Add(new ListItem(CustomerAccountStrings.TextEmailOptionText(), EmailOptions.Text.ToString()));
                cboEmailOptions.Items.Add(new ListItem(CustomerAccountStrings.TextEmailOptionHtml(), EmailOptions.Html.ToString()));
            }
            
            cboEmailOptions.EnableViewState = true;
            chkReceiveInfo.EnableViewState = true;
        }
        #endregion

        private void CustomerAccount_PreRender(object sender, EventArgs e) {
            if (!HttpContext.Current.User.IsInRole(Authentication.RegisteredUser)) {
                foreach (HtmlTableRow row in tblCustomer.Rows) {
                    if (row.ID == "OldPasswordRow")
                        row.Visible = false;
                    if (row.ID == "OldPasswordRowValidator")
                        row.Visible = false;
                }
                return;
            }
            else {
                foreach (HtmlTableRow row in tblCustomer.Rows) {
                    if (row.ID == "NewPasswordRow")
                        row.Cells[0].InnerHtml = string.Empty;
                    if (row.ID == "ConfirmPasswordRow")
                        row.Cells[0].InnerHtml = string.Empty;
                }
            }

            Customer customer = StorageManager.CurrentCustomer;
            Debug.Assert(customer != null);
            if (customer == null)
                return;

            txtFirstName.Text = customer.FirstName;
            txtLastName.Text = customer.LastName;
            txtEmailAddress.Text = customer.Email;
            cboEmailOptions.SelectedValue = customer.EmailOptions.ToString();
            chkReceiveInfo.Checked = customer.ReceiveInfo;
            vldCompareConfirmPassword.Text = CustomerAccountStrings.ValidatorCompareConfirmNewPassword();
            lblNewPassword.Text = CustomerAccountStrings.TextNewPassword();
            vldNewPassword.Text = CustomerAccountStrings.ValidatorNewPassword();
            lblConfirmation.Text = CustomerAccountStrings.TextCustomerUpdated();
        }

        private void btnOK_Click(object sender, ImageClickEventArgs e) {
            vldFirstName.Validate();
            vldLastName.Validate();
            vldEmailAddress.Validate();
            vldValidEmail.Validate();
            vldUniqueEmail.Validate();
            vldCompareConfirmPassword.Validate();

            if (HttpContext.Current.User.IsInRole(Authentication.RegisteredUser)) {
                vldOldPassword.Validate();
                vldWrongOldPassword.Validate();
                if(!(vldOldPassword.IsValid && vldWrongOldPassword.IsValid)) 
                    return;
            }
            else {
                vldNewPassword.Validate();
                vldConfirmPassword.Validate();
            }

            if (!(vldFirstName.IsValid
                 && vldLastName.IsValid
                 && vldEmailAddress.IsValid
                 && vldValidEmail.IsValid
                 && vldUniqueEmail.IsValid
                 && vldNewPassword.IsValid
                 && vldConfirmPassword.IsValid
                 && vldCompareConfirmPassword.IsValid)) {
                return;
            }

            if (HttpContext.Current.User.IsInRole(Authentication.UnregisteredUser)) {
                StorageManager.CurrentCustomer = new Customer();
            }

            StorageManager.CurrentCustomer.FirstName = txtFirstName.Text.Trim();
            StorageManager.CurrentCustomer.LastName = txtLastName.Text.Trim();
            StorageManager.CurrentCustomer.Email = txtEmailAddress.Text.Trim();
            if(HttpContext.Current.User.IsInRole(Authentication.UnregisteredUser))
                StorageManager.CurrentCustomer.Password = txtConfirmPassword.Text;
            else if(txtConfirmPassword.Text != string.Empty)
                StorageManager.CurrentCustomer.Password = txtConfirmPassword.Text;
            if (cboEmailOptions.SelectedValue == EmailOptions.Html.ToString()) {
                StorageManager.CurrentCustomer.EmailOptions = EmailOptions.Html;
            } else {
                StorageManager.CurrentCustomer.EmailOptions = EmailOptions.Text;
            }

            StorageManager.CurrentCustomer.ReceiveInfo = chkReceiveInfo.Checked;

            try {
                if (HttpContext.Current.User.IsInRole(Authentication.RegisteredUser)) {
                    BCCustomer.UpdateCustomer(StorageManager.CurrentCustomer, StorageManager.CurrentRetailer);
                } else {
                    BCCustomer.InsertCustomer(StorageManager.CurrentCustomer,
                        BCLastEditedPaper.ShowPapersAsDataset(
                        StorageManager.CookieLastEditedPapers), StorageManager.CurrentRetailer);
                    BCTracking.Add(UserAction.Registration,
                        StorageManager.CurrentCustomer.Email, Request.UserHostAddress, StorageManager.CurrentRetailer);
                }

                lblUnexpectedError.Visible = false;
                tblCustomer.Visible = false;
                tblConfirmation.Visible = true;
            } catch (BaseBusinessException ex) {
                lblUnexpectedError.Text = ex.Message;
                lblUnexpectedError.Visible = true;
            }
        }

        private void btnCancel_Click(object sender, ImageClickEventArgs e) {
            Response.Redirect(DefaultPage.Url().PathAndQuery);
        }

        private void vldWrongOldPassword_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = txtOldPassword.Text == StorageManager.CurrentCustomer.Password;
        }

        private void vldUniqueEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            Customer customer = BCCustomer.GetCustomerByLogin(txtEmailAddress.Text.Trim(), StorageManager.CurrentRetailer);
            if (HttpContext.Current.User.IsInRole(Authentication.RegisteredUser)) {
                args.IsValid = customer == null || customer.CustomerId == StorageManager.CurrentCustomer.CustomerId;
            } else {
                args.IsValid = customer == null;
            }
        }

        private void vldValidEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = FormatValidator.EnsureValidEmail(txtEmailAddress.Text.Trim());
        }

        private void btnClose_Click(object sender, ImageClickEventArgs e) {
            Response.Redirect(DefaultPage.Url().PathAndQuery);
        }

        private void ctlAccessToAccount_Logout(object sender, EventArgs e) {
            Response.Redirect(DefaultPage.Url().PathAndQuery);
        }

        private void ctlAccessToAccount_Login(Object sender, Neato.Cpt.WebDesigner.Controls.AccessToAccountEventArgs e) {
            Response.Redirect(DefaultPage.Url().PathAndQuery);
        }

        private void vldCompareConfirmPassword_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = txtNewPassword.Text == txtConfirmPassword.Text;
            if(!args.IsValid) {
                if(txtNewPassword.Text.Length == 0) {
                    vldNewPassword.IsValid = false;
                    args.IsValid = true;
                }
                if(txtConfirmPassword.Text.Length == 0) {
                    vldConfirmPassword.IsValid = false;
                    args.IsValid = true;
                }
            }
        }
    }
}
