<%@ Page language="c#" Codebehind="CustomerAccount.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebDesigner.CustomerAccount" %>
<%@ Register TagPrefix="cpt" TagName="MainMenu" Src="Controls/MainMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="FooterMenu" Src="Controls/FooterMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="Header" Src="Controls/Header.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
  <head>
    <title>Fellowes - Customer Account</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../css/style.css" type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
      <table Runat="server" ID="tblCustomer" cellpadding="0" cellspacing="0" class="FormTable" width="390px">
        <tr>
            <td colspan="3">
                <asp:Label ID="lblRequired" Runat="server" CssClass="Text Required"/>
            </td>
        </tr>
        <tr>
            <td>
                <span class="Text Required">&nbsp;*&nbsp;</span>
            </td>
            <td>
                <asp:Label ID="lblFirstName" Runat="server" CssClass="Text Header"/>
            </td>
            <td>
                <asp:TextBox ID="txtFirstName" Runat="server" CssClass="Field100" MaxLength="100"/>                
            </td>
        </tr>
        <tr>
            <td/>
            <td colspan="2">
                <asp:RequiredFieldValidator ControlToValidate = "txtFirstName" Display="Dynamic" CssClass="Text Alert" 
                    Runat="server" ID="vldFirstName" EnableClientScript="False"/>                
            </td>
        </tr>
        <tr>
            <td>
                <span class="Text Required">&nbsp;*&nbsp;</span>
            </td>
            <td>
                <asp:Label ID="lblLastName" Runat="server" CssClass="Text Header"/>
            </td>
            <td>
                <asp:TextBox ID="txtLastName" Runat="server" CssClass="Field100" MaxLength="100"/>
            </td>
        </tr>
        <tr>
            <td/>
            <td colspan="2">
                <asp:RequiredFieldValidator ControlToValidate = "txtLastName" Display="Dynamic" CssClass="Text Alert" 
                    Runat="server" ID="vldLastName" EnableClientScript="False"/>
            </td>
        </tr>
        <tr>
            <td>
                <span class="Text Required">&nbsp;*&nbsp;</span>
            </td>
            <td>
                <asp:Label ID="lblEmailAddress" Runat="server" CssClass="Text Header"/>
            </td>
            <td>
                <asp:TextBox ID="txtEmailAddress" Runat="server" CssClass="Field100" MaxLength="100"/>
            </td>
        </tr>
        <tr>
            <td/>
            <td colspan="2">
				<div>
					<asp:RequiredFieldValidator ControlToValidate = "txtEmailAddress" Display="Dynamic" CssClass="Text Alert" 
						Runat="server" ID="vldEmailAddress" EnableClientScript="False"/>
                </div>
                <div>
					<asp:CustomValidator ControlToValidate="txtEmailAddress" Display="Dynamic" CssClass="Text Alert"
						Runat="server" ID="vldValidEmail" EnableClientScript="False"/>
				</div>
				<div>
					<asp:CustomValidator ControlToValidate = "txtEmailAddress" Display="Dynamic" CssClass="Text Alert" 
						Runat="server" ID="vldUniqueEmail" EnableClientScript="False"/>
				</div>
            </td>
            <td/>
        </tr>
        <tr id="OldPasswordRow">
            <td>
                <span class="Text Required">&nbsp;*&nbsp;</span>
            </td>
            <td>
                <asp:Label ID="lblOldPassword" Runat="server" CssClass="Text Header"/>
            </td>
            <td>
                <asp:TextBox ID="txtOldPassword" Runat="server" CssClass="Password" TextMode="Password" MaxLength="100"/>
            </td>
        </tr>
        <tr id="OldPasswordRowValidator">
            <td/>
            <td colspan="2">
				<div>
					<asp:RequiredFieldValidator ControlToValidate = "txtOldPassword" Display="Dynamic" CssClass="Text Alert" 
						Runat="server" ID="vldOldPassword" EnableClientScript="False"/>
				</div>
				<div>
					<asp:CustomValidator ControlToValidate = "txtOldPassword" Display="Dynamic" CssClass="Text Alert" 
						Runat="server" ID="vldWrongOldPassword" EnableClientScript="False"/>
				</div>
            </td>
        </tr>
        <tr id="NewPasswordRow">
            <td>
                <span class="Text Required">&nbsp;*&nbsp;</span>
            </td>
            <td>
                <asp:Label ID="lblNewPassword" Runat="server" CssClass="Text Header"/>
            </td>
            <td>
                <asp:TextBox ID="txtNewPassword" Runat="server" CssClass="Password" TextMode="Password" MaxLength="100"/>
            </td>
        </tr>
        <tr>
            <td/>
            <td colspan="2">
                <asp:RequiredFieldValidator ControlToValidate = "txtNewPassword" Display="Dynamic" CssClass="Text Alert" 
                    Runat="server" ID="vldNewPassword" EnableClientScript="False"/>
            </td>
        </tr>
        <tr id="ConfirmPasswordRow">
            <td>
                <span class="Text Required">&nbsp;*&nbsp;</span>
            </td>
            <td>
                <asp:Label ID="lblConfirmPassword" Runat="server" CssClass="Text Header"/>
            </td>
            <td>
                <asp:TextBox ID="txtConfirmPassword" Runat="server" CssClass="Password" TextMode="Password" MaxLength="100"/>
            </td>
        </tr>
        <tr>
            <td/>
            <td colspan="2">
				<div>
					<asp:RequiredFieldValidator ControlToValidate = "txtConfirmPassword" Display="Dynamic" CssClass="Text Alert" 
						Runat="server" ID="vldConfirmPassword" EnableClientScript="False"/>
                </div>
                <div>
					<asp:CustomValidator Display="Dynamic" CssClass="Text Alert" 
						Runat="server" ID="vldCompareConfirmPassword" EnableClientScript="False"/>
			    </div>
            </td>
        </tr>
        <tr>
            <td/>
            <td>
                <asp:Label ID="lblEmailOptions" Runat="server" CssClass="Text Header"/>
            </td>
            <td>
                <asp:DropDownList ID="cboEmailOptions" Runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblUnexpectedError" Runat="server" CssClass="Text Alert" Visible="False"/>
            </td>
        </tr>
        <tr>
            <td/>
            <td colspan="2">
                <asp:CheckBox ID="chkReceiveInfo" Runat="server" CssClass="Text" Checked="True"/>
            </td>
        </tr>
        <tr>
            <td/>
            <td align="right" colspan="2">
                <asp:ImageButton Runat="server" ID="btnOK" CssClass="Href"
					CausesValidation="False"
                    ImageUrl="../Images/buttons/ok.gif"
                    onmouseup="src='../images/buttons/ok.gif';"
                    onmousedown="src='../images/buttons/ok_pressed.gif';"
                    onmouseout="src='../images/buttons/ok.gif';"
                />
                <img src="../images/buttons/ok_pressed.gif" style="display: none;">
                <asp:ImageButton Runat="server" ID="btnCancel" CssClass="Href"
                    ImageUrl="../Images/buttons/cancel.gif"
                    onmouseup="src='../images/buttons/cancel.gif';"
                    onmousedown="src='../images/buttons/cancel_pressed.gif';"
                    onmouseout="src='../images/buttons/cancel.gif';"
                    CausesValidation="False"
                    style="margin-left:20px;"
                />
                <img src="../images/buttons/cancel_pressed.gif" style="display: none;">
            </td>
        </tr>
        </table>
        <table Runat="server" ID="tblConfirmation" Visible="False" class="FormTable">
            <tr>
                <td>
					<asp:Label ID="lblConfirmation" Runat="server" CssClass="Text"/>
                </td>
            </tr>
            <tr align="center">
                <td>
                    <asp:ImageButton Runat="server" ID="btnClose" CssClass="Href"
                        ImageUrl="../Images/buttons/close.gif"
                        onmouseup="src='../images/buttons/close.gif';"
                        onmousedown="src='../images/buttons/close_pressed.gif';"
                        onmouseout="src='../images/buttons/close.gif';"/>
                    <img src="../images/buttons/close_pressed.gif" style="display: none;">
                </td>
            </tr>
        </table>
    </form>
  </body>
</html>

