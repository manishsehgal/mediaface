using System;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner {
    public class UploadImage : Page {
        protected HtmlInputFile ctlFile;
        protected ImageButton btnSubmit;
        //protected Button btnCancel;
        protected HtmlTable ctlContent;
        protected HtmlGenericControl ctlUploadContaner;
        protected HtmlGenericControl ctlCheckingContainer;

        protected Label lblCaption;
        protected Label lblPromptText;
        protected Label lblSizeWarning;
        protected Label lblInstructionHeader;
        protected Label lblBrowseInstruction;
        protected Label lblOkInstruction;
        protected Label lblPathNote;
        protected Label lblUploading;
        protected CustomValidator vldUploadSize;
        protected RequiredFieldValidator vldFileRequired;
        protected RegularExpressionValidator vldFilePath;
        protected CustomValidator vldFileExt;
        protected CustomValidator vldFileExist;
        protected string requestSizeKey;

        #region Url
        private const string PageModeKey = "type";
        private const string RequestSizeUrlKey = "key";
        private const string UploadMode = "UploadMode";
        private const string UploadContentMode = "UploadContentMode";
        private const string RawUrl = "UploadImage.aspx";
        protected System.Web.UI.WebControls.ValidationSummary validationSummary;
        private const string OversizeKey = "Oversize";

        private bool Oversize {
            get {
                string oversize = Request.Params[OversizeKey];
                if (oversize != null && oversize == true.ToString()) {
                    return true;
                }
                return false;
            }
        }

        public static Uri UrlUpload() {
            return UrlHelper.BuildUrl(RawUrl, PageModeKey, UploadMode);
        }

        private static Uri UrlUploadContent(string requestSizeKey) {
            return UrlHelper.BuildUrl(RawUrl, PageModeKey, UploadContentMode, RequestSizeUrlKey, requestSizeKey);
        }

        private static Uri UrlOversize(string requestSizeKey) {
            return UrlHelper.BuildUrl(RawUrl, PageModeKey, UploadContentMode, OversizeKey, true, RequestSizeUrlKey, requestSizeKey);
        }

        public static bool IsUrlUploadContent(Uri url) {
            bool samePage = url.AbsolutePath == UrlUploadContent(string.Empty).AbsolutePath;
            int modeIndex = url.Query.IndexOf(PageModeKey);
            int modeValueIndex = url.Query.IndexOf(UploadContentMode);
            bool condition = samePage && modeIndex > 0 && modeValueIndex > 0;
            return condition;
        }

        public static string GetRequestSizeKey(Uri url) {
            string query = url.Query;
            string[] split = query.Split('&', '?');
            foreach (string s in split) {
                if (s.StartsWith(RequestSizeUrlKey)) {
                    return s.Split('=')[1];
                }
            }
            return null;
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetValidUntilExpires(false);

            if (!IsPostBack) {
                requestSizeKey = Request.QueryString[RequestSizeUrlKey];
                if (requestSizeKey == null) {
                    requestSizeKey = Guid.NewGuid().ToString("N");
                }
                string mode = Request.QueryString[PageModeKey];
                switch (mode) {
                    case UploadMode:
                        GenerateContanerResponse();
                        break;
                    case UploadContentMode:
                    default:
                        GenerateUploadResponse(requestSizeKey);
                        DataBind();
                        break;
                }
            }
        }

        private void GenerateUploadResponse(string requestSizeKey) {
            ctlContent.Visible = true;
            ctlUploadContaner.Visible = false;
            vldUploadSize.IsValid = !Oversize;
            btnSubmit.Attributes["onclick"] =
                string.Format(@"OpenUploadCheck('{0}'); return true;", UploadCheck.Url(UrlOversize(requestSizeKey), requestSizeKey).PathAndQuery);
        }

        private void GenerateContanerResponse() {
            ctlContent.Visible = false;
            ctlUploadContaner.Visible = true;
            ctlUploadContaner.Attributes["src"] = UrlUploadContent(requestSizeKey).PathAndQuery;
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.vldFileExist.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(this.vldFileExist_ServerValidate);
            this.btnSubmit.Click += new ImageClickEventHandler(this.btnSubmit_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new System.EventHandler(this.UploadImage_DataBinding);

        }
        #endregion

        private void btnSubmit_Click(object sender, ImageClickEventArgs e) {
            if (!Page.IsValid) {
                return;
            }
            if (ctlFile.PostedFile != null && ctlFile.PostedFile.ContentLength > 0) {
                int size = ctlFile.PostedFile.ContentLength;

                if (!BCProject.IsImageAdditionAllowed(StorageManager.CurrentProject, size)) {
                    vldUploadSize.IsValid = false;
                    return;
                }

                Stream fileData = ctlFile.PostedFile.InputStream;
                string fileExtension = Path.GetExtension(ctlFile.PostedFile.FileName).ToLower();
				fileExtension = System.Text.RegularExpressions.Regex.Replace(fileExtension, @" *$", "");
                if (!BCProject.AllowedImageExtention(fileExtension)) {
                    vldFileExt.IsValid = false;
                    return;
                }

                BCProject.AddImage(StorageManager.CurrentProject, fileData);

                RegisterClientScriptBlock("close", "<script type='text/javascript' language='javascript'>Exit(true);</script>");
            }
        }

        private void UploadImage_DataBinding(object sender, EventArgs e) {
            lblCaption.Text = UploadImageStrings.CaptionText();
            lblBrowseInstruction.Text = UploadImageStrings.BrowseButtonInstructionText();
            lblInstructionHeader.Text = UploadImageStrings.InstructionHeaderText();
            lblPromptText.Text = UploadImageStrings.UploadPromptText();
            lblOkInstruction.Text = UploadImageStrings.OKButtonInstructionText();
            lblPathNote.Text = UploadImageStrings.LongFolderPathNote();
            lblUploading.Text = UploadImageStrings.UploadingText();

            int maxPathLength = 200;
            vldFileRequired.ErrorMessage = UploadImageStrings.FileNameEmptyErrorMessage();
            vldFilePath.ErrorMessage = UploadImageStrings.FilePathTooLongErrorMessage();
            vldFilePath.ValidationExpression = ".{1," + maxPathLength.ToString(CultureInfo.InvariantCulture) + "}";

            //btnSubmit.Text = UploadImageStrings.UploadSubmitButtonCaption();
            //btnCancel.Text = UploadImageStrings.UploadCancelButtonCaption();
            //btnCancel.Attributes["onclick"] = "javascript:Exit(false);";

            int freeSize = Configuration.MaxProjectSize - StorageManager.CurrentProject.Size / 1024;
            lblSizeWarning.Text = UploadImageStrings.SizeWarning(freeSize);
        }

        private void vldFileExist_ServerValidate(object source, ServerValidateEventArgs args) {
            vldFileExist.ErrorMessage = UploadImageStrings.FileNotExistErrorMessage(ctlFile.PostedFile.FileName);
            if (ctlFile.PostedFile != null && ctlFile.PostedFile.ContentLength != 0)
                args.IsValid = true;
            else
                args.IsValid = false;
        }
    }
}