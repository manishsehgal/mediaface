using System;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using System.Web;

using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.HttpHandlers;
using Neato.Cpt.WebDesigner.HttpModules;

namespace Neato.Cpt.WebDesigner {
    public class Global : HttpApplication {
        public static Assembly GetAssembly() {
            return Assembly.GetExecutingAssembly();
        }

        private IContainer components = null;

        public static readonly string Version = GetApplicationVersion();

        private static string GetApplicationVersion() {
            AssemblyName thisAssemblyName = Assembly.GetExecutingAssembly().GetName(false);
            return thisAssemblyName.Version.ToString(4);
        }

        public Global() {
            InitializeComponent();
        }

        protected void Application_Start(Object sender, EventArgs e) {
            StorageManager.IsSiteClosed = false;
            SessionsAdmCount = 0;
            SessionsUsrCount = 0;
        }

        protected void Application_BeginRequest(Object sender, EventArgs e) {
            const string PostWord = "POST";

            if (Request.Url.AbsolutePath == UploadCheck.Url().AbsolutePath) {
                string requestSizeKey = UploadCheck.GetRequestSizeKey(Request.Url);
                int x = 0;
                while (Context.Cache[requestSizeKey] == null && x < 20) {
                    Thread.Sleep(100);
                    ++x;
                }
            }

            if (Request.RequestType == PostWord && Request.ContentLength > 0) {
                if (UploadImage.IsUrlUploadContent(Request.Url)) {
                    string requestSizeKey = UploadImage.GetRequestSizeKey(Request.Url);
                    Context.Cache[requestSizeKey] = Request.ContentLength.ToString();
                    int x = 0;
                    while (Context.Cache[requestSizeKey] != null && x < 20) {
                        Thread.Sleep(100);
                        ++x;
                    }
                    Thread.Sleep(2000);
                }
            }
        }

        protected void Application_EndRequest(Object sender, EventArgs e) {}

        protected void Application_AuthenticateRequest(Object sender, EventArgs e) {}

        protected void Application_Error(Object sender, EventArgs e) {
            ExceptionPublisher.Publish(Server.GetLastError().GetBaseException());
            Server.ClearError();
            Server.Transfer(ErrorPage.Url().PathAndQuery);
        }

        public  int SessionsCount {
            get {
                return SessionsUsrCount;
            }
        }
        protected int SessionsAdmCount {
            get {
                if (Application["WebDesigner.SessionAdm.Count"] != null) {
                    return (int)Application["WebDesigner.SessionAdm.Count"];
                }
                return 0;
            }
            set { 
                Application["WebDesigner.SessionAdm.Count"] = value; 
            }
        }
        protected int SessionsUsrCount {
            get {
                if (Application["WebDesigner.SessionUsr.Count"] != null) {
                    return (int)Application["WebDesigner.SessionUsr.Count"];
                }
                return 0;
            }
            set { 
                Application["WebDesigner.SessionUsr.Count"] = value; 
            }
        }


        protected void Session_Start(Object sender, EventArgs e) {
            if(!CheckLocationModule.IsHttpHandler && !CheckLocationModule.IsAdminMode && !StorageManager.IsSiteClosed) {
                SessionsUsrCount++;
            } else {
                SessionsAdmCount++;
            }
        }

        protected void Session_End(Object sender, EventArgs e) {
            if (SessionsAdmCount > 0) {
                SessionsAdmCount--;
            }else if (SessionsUsrCount > 0) {
                SessionsUsrCount--;
            }
        }

        protected void Application_End(Object sender, EventArgs e) {}

        #region Web Form Designer generated code
        private void InitializeComponent() {
            this.components = new Container();
        }
        #endregion
    }
}