<%@ Page language="c#" Codebehind="PrintSetup.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebDesigner.PrintSetup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
    <HEAD>
        <title>PrintSetup</title>
        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <LINK href="../css/style.css" type="text/css" rel="stylesheet">
    </HEAD>
    <body MS_POSITIONING="GridLayout">
    <center>
        <table runat="server" class="contentTdStyle" style="HEIGHT:100%">
            <tr>
                <td>
                    <h1>
                        <asp:Label ID="lblHdr" Runat="server" style="color:#FDCE10" />
                    </h1>
                    <p>
                        <asp:Label ID="lblText" Runat="server" style="color:#FDCE10" />
                    </p>
                    <p>
                        <asp:Image ID="imgScreenShot" Runat="server" />
                    </p>
                </td>
            </tr>
        </table>
    </center>
    </body>
</HTML>
