<%@ Page language="c#" Codebehind="UploadProject.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebDesigner.UploadProject" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
    <head>
        <title>Fellowes - Upload device skin</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <link href="css/style.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" language="javascript" src="js/UploadProject.js"></script>
    </head>
    <body>
      <center>
        <form id="frmUpload" method="post" runat="server">
            <table id="ctlContent" runat="server" class="contentTdStyle uploadDesign" style="height:100%">
                <tr>
                    <td>
                        <table class="uploadDialog">
                            <tr>
                                <td class="uploadDialogCaption">
                                    <asp:Label ID="lblCaption" Runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="padding10px">
                                    <asp:Label ID="lblPromptText" Runat="server" CssClass="Promt" />
                                </td>
                            </tr>
                            <tr>
                                <td class="padding10px">
                                    <p>
                                        <input type="file" id="ctlFile" runat="server" size="55" class="InputFile">
                                        <asp:RequiredFieldValidator 
                                            id="vldFileRequired" CssClass="Promt" 
                                            runat="server" 
                                            ControlToValidate="ctlFile" 
                                            Display="None"/>
                                        <asp:RegularExpressionValidator
                                            id="vldFilePath" CssClass="Promt"
                                            runat="server"
                                            ControlToValidate="ctlFile"
                                            Display="None"
                                            />
                                        <asp:RegularExpressionValidator 
                                            id="vldFileExt"  CssClass="Promt"
                                            runat="server" 
                                            ControlToValidate="ctlFile" 
                                            Display="None"/>
                                        <asp:CustomValidator 
                                            id="vldFileExist"  CssClass="Promt"
                                            runat="server" 
                                            ControlToValidate="ctlFile" EnableClientScript="False"
                                            Display="None"/>
                                        <asp:CustomValidator 
                                            id="vldFileStructure"  CssClass="Promt"
                                            runat="server" 
                                            ControlToValidate="ctlFile" EnableClientScript="False"
                                            Display="None"/>
                                        <asp:ValidationSummary id="validationSummary" CssClass="Promt" runat="server"
                                            ShowMessageBox="false" ShowSummary="true"
                                            DisplayMode="List"/>
                                    </p>
                                </td>
                            </tr>
                            <tr id="content1">
                                <td class="padding10px">
                                    <div class="uploadInstructions">
                                        <asp:Label ID="lblInstructionHeader" Runat="server" CssClass="TextHeader"/>
                                        <ol>
                                            <li><asp:Label ID="lblBrowseInstruction" Runat="server" /></li>
                                            <li><asp:Label ID="lblOkInstruction" Runat="server" /></li>
                                        </ol>
                                        <p align="center">
                                            <asp:Label ID="lblPathNote" Runat="server" CssClass="Note" />
                                        </p>
                                    </div>
                                    <p style="text-align:center">
                                        <asp:ImageButton Runat="server" ID="btnSubmit"
                                            ImageUrl="images/buttons/ok.gif"
                                            onmouseup="src='images/buttons/ok.gif';"
                                            onmousedown="src='images/buttons/ok_pressed.gif';"
                                            onmouseout="src='images/buttons/ok.gif'"
                                        />
                                        <img src="images/buttons/ok_pressed.gif" style="display: none;">
                                    	<input type="image" name="btnCancel" id="btnCancel"
                                    	    onclick="javascript:Exit(false);"
                                            src="images/buttons/cancel.gif"
                                            onmouseup="src='images/buttons/cancel.gif';"
                                            onmousedown="src='images/buttons/cancel_pressed.gif';"
                                            onmouseout="src='images/buttons/cancel.gif'"
                                    	/>
                                    	<img src="images/buttons/cancel_pressed.gif" style="display: none;">
                                    </p>
                                </td>
                            </tr>
                            <tr id="content2" style="visibility:hidden;">
                                <td class="padding10px">
                                    <asp:Label ID="lblUploading" Runat="server" CssClass="uploadInstructions Note" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <iframe runat="server" id="ctlUploadContaner" align="absmiddle" frameborder="no" scrolling="no" style="WIDTH: 100%; HEIGHT: 100%">
            </iframe>
        </form>
      </center>
    </body>
</html>
