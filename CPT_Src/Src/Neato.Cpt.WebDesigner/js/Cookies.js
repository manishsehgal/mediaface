// Create a cookie with the specified name and value.
// The cookie expires at the end of the 20th century.
function SetCookie(sName, sValue) {
  var today = new Date();
  var expiry = new Date(today.getTime() + 100 * 365 * 24 * 60 * 60 * 1000);

  //document.cookie = sName + "=" + escape(sValue) + "; expires=" + expiry.toGMTString();
  document.cookie = sName + "=" + sValue + "; expires=" + expiry.toGMTString();
}


// Retrieve the value of the cookie with the specified name.
function GetCookie(sName) {
  // cookies are separated by semicolons
  var aCookie = document.cookie.split("; ");
  for (var i=0; i < aCookie.length; i++)
  {
    // a name/value pair (a crumb) is separated by an equal sign
    var cookie = aCookie[i];
    var pos = cookie.indexOf("=");
    if(pos>0) {
       var cName = cookie.substr(0,pos);
       if (sName == cName) 
          return cookie.substr(pos+1);
    }
  }

  // a cookie with the requested name does not exist
  return null;
}

// Delete the cookie with the specified name.
function DelCookie(sName) {
  document.cookie = sName + "=; expires=Fri, 31 Dec 1999 23:59:59 GMT;";
}

function CheckCookies(sText)
{
   SetCookie("CookieTest","Test");
   if(GetCookie("CookieTest")!="Test") {
     document.write(sText);
   }
   DelCookie("CookieTest");
}
