function GetDesignerMovie() {
    var designerObject = document.getElementById('Designer');
    var designerEmbed = document.getElementById('DesignerEmbed');
    return (designerEmbed == null) ? designerObject: designerEmbed;
}

function OpenPdf() {
    var url = document.getElementById('ctlPrintUrl').value;
    OpenWindowUsingBlockerNotifier(url, "CPTPDF");
}

function GoHome() {
    var msg = document.getElementById('ctlLeaveDesignerMessage').value;
    var bConfirmed = window.confirm(msg);
    if (!bConfirmed)
        return;
        
    var url = document.getElementById('ctlHomeUrl').value;
    window.open(url, "_self");
}

function SaveToClient() {
    var time = new Date();
    var url = document.getElementById('ctlDownloadProjectUrl').value+"?time="+time.getTime();
    location.href = url;
}

function LoadFromClient() {
    var msg = document.getElementById('ctlLeaveDesignerMessage').value;
    var bConfirmed = window.confirm(msg);
    if (!bConfirmed)
        return;
        
    var url = document.getElementById('ctlUploadUrl').value;
    OpenDialog(url, ResetDesigner);
}

function LoadImageFromClient() {
    var url = document.getElementById('ctlUploadImageUrl').value;
    OpenDialog(url, ResetDesigner);
}

function ResetDesigner(value) {
	if (value == true) {
	    RefreshDesigner();
	}
}

function RefreshDesigner() {
    setFlashVariables("Designer", "cmd=reload");
}
   
function SaveDesigner() {
    setFlashVariables("Designer", "cmd=save");
}

function setFlashVariables(movieid, flashquery){
	var divcontainer = "flash_setvariables_" + movieid;
	if(document.getElementById(divcontainer) == null) {
		var divholder = document.createElement("div");
		divholder.id = divcontainer;
		document.body.appendChild(divholder);
	}
	document.getElementById(divcontainer).innerHTML = "";
	var divinfo = "<embed src='../Flash/SetVar.swf' FlashVars='lc="+movieid+"&fq="+escape(flashquery)+"' width='0' height='0' type='application/x-shockwave-flash'></embed>";
	document.getElementById(divcontainer).innerHTML = divinfo;
}

function ResizeDesigner() {
    var designerMovie = GetDesignerMovie();
    designerMovie.height = 540;
    designerMovie.width = 904;
}

function ScrolToDesigner() {
    var designerMovie = GetDesignerMovie();
    window.scrollTo(designerMovie.offsetLeft -1, designerMovie.offsetTop - 1);
}

function CheckFlashVer() {
    var result = DetectFlashVer(7, 0, 19);
    if (result) document.getElementById('flashRequiredDiv').style.display = "none";
    else {
		document.getElementById('flashClipDiv').style.visibility = "hidden";
		document.getElementById('flashRequiredDiv').style.display = "block";
	}
}

function GotoStep(args) {
    return;
    var idPrefix = "ctlHeaderStep_ctlStepSelector_btnStep";
    var pathPrefix = "images/";
    var step = "step";
    var inactivePostix = "_inactive.jpg";
    var activePostix = "_active.jpg";
    
	var btnStep1 = document.getElementById(idPrefix+"1");
	var btnStep2 = document.getElementById(idPrefix+"2");
	var btnStep3 = document.getElementById(idPrefix+"3");
    
	var btnStep2Img = document.getElementById(idPrefix+"2").firstChild;
	var btnStep3Img = document.getElementById(idPrefix+"3").firstChild;

    btnStep1.src = pathPrefix + step + "1" + inactivePostix;
    btnStep1.style.cursor = "hand";
    btnStep2Img.src = pathPrefix + step + "2" + inactivePostix;
    btnStep2.style.cursor = "hand";
    btnStep3Img.src = pathPrefix + step + "3" + inactivePostix;
    btnStep3.style.cursor = "hand";

    if (args == "1") {
        btnStep1.src = pathPrefix + step + "1" + activePostix;
        btnStep1.style.cursor = "default";
    } else if (args == "2") {
        btnStep2Img.src = pathPrefix + step + "2" + activePostix;
        btnStep2.style.cursor = "default";
    } else if (args == "3") {
        btnStep3Img.src = pathPrefix + step + "3" + activePostix;
        btnStep3.style.cursor = "default";
    }
}

function SetDuplicatedFlash(isDuplicated) {
    var ctlDuplicatedFlash = document.getElementById('ctlDuplicatedFlash');
    var doPost = false;
    
    if (ctlDuplicatedFlash.value != "checked") {
        doPost = true;
		ctlDuplicatedFlash.value = isDuplicated;
    }
    
    if (isDuplicated == "true") {
        var msg = document.getElementById('ctlDuplicatedDesignerMessage').value;
        alert(msg);
        var url = document.getElementById('ctlHomeUrl').value;
        window.location.href = url;
        return;
    }

    if (doPost)    
        document.forms[0].submit();
}
