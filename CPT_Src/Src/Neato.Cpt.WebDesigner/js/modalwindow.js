var ClipHeight = "540px";

function OpenDialog(url, callbackFunction) {
    var ctlDialogResult = document.getElementById('ctlDialogResult');
    if (ctlDialogResult != null) {
        ctlDialogResult.onclick    = OnCloseDialog;
        ctlDialogResult.ondblclick = callbackFunction;
    }
    
    ShowDialog(true, url);
}

function OnCloseDialog(value) {
	var url = document.getElementById('ctlUploadBlankUrl').value;
    ShowDialog(false, url);
    
    var ctlDialogResult = document.getElementById('ctlDialogResult');
    if (ctlDialogResult != null) {
        ctlDialogResult.ondblclick(value);
    }
}

function ShowDialog(visible, url) {
    var flashClipDiv   = document.getElementById('flashClipDiv');
    var DesignerObj    = document.getElementById('Designer');
    var DesignerEmbed  = document.getElementById('DesignerEmbed');
    var ctlUploadFrame = document.getElementById('ctlUploadFrame');

    if (ctlUploadFrame != null) {
        ctlUploadFrame.src = url;
        ctlUploadFrame.style.visibility = (visible == true ? "visible" : "hidden");
        ctlUploadFrame.style.height = (visible == true ? ClipHeight : "0px");
    }
    
    if (DesignerEmbed != null) {
        DesignerEmbed.style.visibility = (visible == true ? "hidden" : "visible");
        DesignerEmbed.style.height = (visible == true ? "0px" : ClipHeight);
    }
    if (DesignerObj != null) {
        DesignerObj.style.visibility = (visible == true ? "hidden" : "visible");
        DesignerObj.style.height = (visible == true ? "0px" : ClipHeight);
    }
    if (flashClipDiv != null) {
        flashClipDiv.style.visibility = (visible == true ? "hidden" : "visible");
        flashClipDiv.style.height = (visible == true ? "0px" : ClipHeight);
    }
}

