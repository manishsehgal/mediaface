using System;
using System.Globalization;
using System.Web.UI;

namespace Neato.Cpt.WebDesigner.Helpers {
    /// <summary>
    /// Helper functions for display client side message box
    /// </summary>
    public sealed class MsgBox {
        private MsgBox() {}

        /// <summary>
        /// Display simple message box
        /// </summary>
        public static void Alert(Page page, string message) {
            const string AlertScriptFormat = "alert(\"{0}\");";
            const string AlertScriptKey = "Neato.Cpt.AlertScript";
            registerStartupScript(page, AlertScriptKey, AlertScriptFormat, JavaScriptHelper.ReplaceSpecial(message));
        }

        /// <summary>
        /// Display message box then redirect to another page
        /// </summary>
        public static void AlertRedirect(Page page, string message, Uri url) {
            const string AlertRedirectScriptFormat = "alert(\"{0}\");location.href = \"{1}\";";
            const string AlertRedirectScriptKey = "Neato.Cpt.AlertRedirectScript";
            registerStartupScript(page, AlertRedirectScriptKey, AlertRedirectScriptFormat, JavaScriptHelper.ReplaceSpecial(message), url.AbsoluteUri);
        }

        public static void registerStartupScript(Page page, string scriptKey, string scriptFormat, params string[] scriptParams) {
            string script = JavaScriptHelper.SurroundJavaScript(string.Format(CultureInfo.InvariantCulture, scriptFormat, scriptParams));

            if (!page.IsStartupScriptRegistered(scriptKey)) {
                scriptKey = scriptKey + Guid.NewGuid().ToString("N", CultureInfo.InvariantCulture);
            }
            page.RegisterStartupScript(scriptKey, script);
        }
    }
}