using System.Globalization;
using System.Text;
using System.Web.UI;

namespace Neato.Cpt.WebDesigner.Helpers {
    /// <summary>
    /// Helper functions for client scripting and helper client scripts
    /// </summary>
    public sealed class JavaScriptHelper {
        private JavaScriptHelper() {}

        // JavaScript surrount tags
        public static string SurroundJavaScript(string script) {
            return string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript'>{0}</script>", script);
        }

        public static string SurroundJavaScript(string script, string targetControlClientId, string eventName) {
            return string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript' for='{1}' event='{2}'>{0}</script>", script, targetControlClientId, eventName);
        }

        public static string FocusScript(string controlClientId) {
            const string focusScriptFormat = "var controlToFocus = document.getElementById('{0}'); if (controlToFocus && !controlToFocus.disabled) controlToFocus.focus();";
            return SurroundJavaScript(string.Format(CultureInfo.InvariantCulture,
                focusScriptFormat, controlClientId));
        }

        public static void RegisterFocusScript(Page page, string controlClientId) {
            const string FocusScriptKey = "FocusScriptKey";
            page.RegisterStartupScript(FocusScriptKey, FocusScript(controlClientId));
        }

        public static string ConfirmScriptWithValidation(Page page, string confirmFunctionName, params string[] confirmText) {
            RegisterConfirm(confirmFunctionName, page, confirmText);
            const string ReturnFormat = "javascript: return Page_ClientValidate() && {0}();";
            return string.Format(CultureInfo.InvariantCulture, ReturnFormat, confirmFunctionName);
        }

        public static string ConfirmScript(Page page, string confirmFunctionName, params string[] confirmText) {
            RegisterConfirm(confirmFunctionName, page, confirmText);
            const string ReturnFormat = "javascript: return {0}();";
            return string.Format(CultureInfo.InvariantCulture, ReturnFormat, confirmFunctionName);
        }

        public static string ConfirmScriptForPostBack(Page page, string confirmFunctionName, params string[] confirmText) {
            RegisterConfirm(confirmFunctionName, page, confirmText);
            const string ReturnFormat = "javascript: if (!{0}()) return false;";
            return string.Format(CultureInfo.InvariantCulture, ReturnFormat, confirmFunctionName);
        }

        private static void RegisterConfirm(string confirmFunctionName, Page page, string[] confirmText) {
            string scriptKey = string.Format(CultureInfo.InvariantCulture, "ConfirmFunction{0}ScriptKey", confirmFunctionName);           
            if (!page.IsClientScriptBlockRegistered(scriptKey)) {
                const string FunctionFormat = "function {0}() {{ return {1}; }}";

                string confirmValue = GetConfirmValue(confirmText);
                string script = string.Format(CultureInfo.InvariantCulture, FunctionFormat, confirmFunctionName, confirmValue);
                page.RegisterClientScriptBlock(scriptKey, SurroundJavaScript(script));
            }
        }

        public static string GetConfirmValue(params string[] confirmText) {
            const string ConfirmFormat = "confirm('{0}') && ";
            const string ConfirmValueFormat = "{0} true";

            StringBuilder sb = new StringBuilder();
            if (confirmText != null) {
                foreach (string confirm in confirmText) {
                    sb.AppendFormat(ConfirmFormat, ReplaceSpecial(confirm));
                }
            }
            return string.Format(CultureInfo.InvariantCulture, ConfirmValueFormat, sb.ToString());
        }

        public static string ReplaceSpecial(string argument) {
            return argument.Replace("\\", "\\\\").Replace("\"", "'").Replace("\n", "\\n").Replace("\r", "");
        }

    }
}