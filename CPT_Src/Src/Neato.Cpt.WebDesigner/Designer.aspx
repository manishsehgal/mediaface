<%@ Page language="c#" Codebehind="Designer.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebDesigner.Designer" %>
<%@ Register TagPrefix="cpt" NameSpace="Neato.Cpt.WebDesigner.Controls" Assembly="Neato.Cpt.WebDesigner" %>
<%@ Register TagPrefix="cpt" TagName="FooterMenu" Src="Controls/FooterMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="HeaderStep" Src="Controls/HeaderStep.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD>
    <title>Fellowes - Design your device skin</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../css/style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" language="javascript" src="../js/DetectFlashVersion.js"></script>
    <script type="text/javascript" language="javascript" src="../js/modalwindow.js"></script>
    <script type="text/javascript" language="javascript" src="../js/Designer.js"></script>
    <script type="text/javascript" language="javascript" src="../js/Cookies.js"></script>
    <script type="text/javascript" language="javascript">
        // Flash fscommand: Hook for Internet Explorer.
        if (navigator.appName && navigator.appName.indexOf("Microsoft") != -1 && navigator.userAgent.indexOf("Windows") != -1 && navigator.userAgent.indexOf("Windows 3.1") == -1) {
	        document.write("<script type=\"text/vbscript\" >\n");
	        document.write("On Error Resume Next\n");
	        document.write("Sub Designer_FSCommand(ByVal command, ByVal args)\n");
	        document.write("	Call Designer_DoFSCommand(command, args)\n");
	        document.write("End Sub\n");
	        document.write("</script\>\n");
        }

        function Designer_DoFSCommand(command, args) { 
			switch (command) {
	            case "SetDuplicatedFlash":
	            SetDuplicatedFlash(args);
	            break;
	            case "print":
	            OpenPdf();
	            break;
	            case "home":
	            GoHome();
	            break;
	            case "save":
	            SaveToClient();
	            break;
	            case "load":
	            LoadFromClient();
	            break;
	            case "refresh":
	            RefreshDesigner();
	            break;
	            case "loadImage":
	            LoadImageFromClient();
	            break;
	            case "calibration":
	            SetCookie("Calibration", args);
	            break;
	            case "gotoStep":
	            GotoStep(args);
	            break;
	            case "log":
                var log = document.getElementById('txtLog');
	            log.value += command + ": " + args + "\n";
	            break;
	        }
        }
    </script>
    </HEAD>
    <body MS_POSITIONING="GridLayout" bgcolor="#cccccc" onresize="javascript:ResizeDesigner();">
    <form id="Form1" method="post" runat="server">
      <div style="height:540px;">
        <cpt:PopUpBlockerNotifier id="ctlNotifier" runat="server" />
        <input type="hidden" id="ctlDialogResult" name="ctlDialogResult"/>
        <input type="hidden" id="ctlDuplicatedFlash" runat="server"/>
        <input type="hidden" id="ctlUploadUrl" runat="server" NAME="ctlUploadUrl"/>
        <input type="hidden" id="ctlUploadImageUrl" runat="server" NAME="Hidden1"/>
        <input type="hidden" id="ctlUploadBlankUrl" runat="server" NAME="ctlUploadBlankUrl"/>
        <input type="hidden" id="ctlDownloadProjectUrl" runat="server" />
        <input type="hidden" id="ctlHomeUrl" runat="server" />
        <input type="hidden" id="ctlLeaveDesignerMessage" runat="server" />
        <input type="hidden" id="ctlDuplicatedDesignerMessage" runat="server" />
        <textarea id="txtLog" style="DISPLAY: none; VISIBILITY: hidden; WIDTH: 100%" rows="10"></textarea>
        <div id="flashRequiredDiv" style="display: none;">
            <asp:Label id="lblFlashRequired" Runat="server" CssClass="flashRequiredText"/>
        </div>
        <div id="flashClipDiv">
            <OBJECT id="Designer"
                codeBase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0"
                height="540" width="904" align="center" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
                VIEWASTEXT>
                <PARAM NAME="Movie" VALUE="../Flash/Starter.swf">
                <PARAM NAME="Play" VALUE="false">
                <PARAM NAME="Loop" VALUE="false">
                <PARAM NAME="Quality" VALUE="High">
                <PARAM NAME="Menu" VALUE="false">
                <PARAM NAME="AllowScriptAccess" VALUE="sameDomain">
                <PARAM NAME="Scale" VALUE="ShowAll">
                <PARAM NAME="BGColor" VALUE="#282828">
                <PARAM NAME="SeamlessTabbing" VALUE="true">
                <PARAM NAME="SeamlessTabbing" VALUE="true">
                <embed name="Designer" id="DesignerEmbed" src="../Flash/Starter.swf"
                    quality="high" bgcolor="#ffffff" width="904" height="540"
                    swLiveConnect="true" align="middle" allowScriptAccess="sameDomain"
                    menu="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
            </OBJECT>
        </div>
        <iframe id="ctlUploadFrame" name="ctlUploadFrame" align="absmiddle" frameborder="no" scrolling="no" style="WIDTH: 904px; HEIGHT: 0px; visibility:hidden;">
        </iframe>
      </div>
    </form>
    <script type="text/javascript" language="javascript">
        ResizeDesigner();
        ScrolToDesigner();
        CheckFlashVer();
    </script>
</body>
</HTML>
