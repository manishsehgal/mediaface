<%@ Register TagPrefix="cpt" TagName="AccessToAccount" Src="Controls/AccessToAccount.ascx" %>
<%@ Register TagPrefix="cpt" TagName="Announcements" Src="Controls/Announcements.ascx" %>
<%@ Register TagPrefix="cpt" TagName="FooterMenu" Src="Controls/FooterMenu.ascx" %>
<%@ Register TagPrefix="cpt" TagName="MainMenu" Src="Controls/MainMenu.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="StandardPageTemplate.ascx.cs" Inherits="Neato.Cpt.WebDesigner.StandardPageTemplate" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table align="center" border="0" width="900" cellspacing="0" bgcolor="black" cellpadding="0" style="border-collapse:collapse;">
    <tr>
        <td colspan="2">
            <cpt:MainMenu id="ctlMainMenu" runat="server"/>
        </td>
    </tr>
    <tr>
        <td class="LeftPanel" valign="top" bgcolor="#EA7501">
            <img src="../Images/LeftPanelStandard.gif" border="0" GALLERYIMG="no"
                STYLE="position:absolute; z-index:1;">
            <div STYLE="position:absolute; z-index:2;">
                <asp:HyperLink ID="hypHome" Runat="server"
                    STYLE="position:absolute;left:0px;top:-1px; clip:rect(auto 208 100 auto);" />
                <div class="Account" style="overflow: hidden;">
                <div style="padding-left:5px;">
                    <cpt:AccessToAccount ID="ctlAccessToAccount" runat="server" />
                </div>
                </div>
                <div class="Announcements" style="overflow: hidden;">
                <div style="padding-left:5px;">
                    <cpt:Announcements ID="ctlAnnouncements" runat="server"/>
                </div>
                </div>
            </div>
        </td>
        <td STYLE="width:692px;" valign="top">
          <div style="height:59px">&nbsp;</div>
          <div style="position:relative; top:16px;">
            <img src="../Images/StandardBackground.gif" GALLERYIMG="no">
            <asp:Label id="lblHeader" Runat="server" CssClass="HeaderText" style="position:absolute;top:55;left:20"/>
          </div>
          <div align=left style="overflow: hidden; padding: 25px 10px 10px 35px;">
            <asp:PlaceHolder ID="content" Runat="server"/>
          </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <cpt:FooterMenu id="ctlFooterMenu" runat="server"/>
        </td>
    </tr>
</table>
