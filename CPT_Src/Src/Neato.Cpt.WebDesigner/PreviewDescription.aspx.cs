using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Neato.Cpt.WebDesigner {
    public class PreviewDescription : Page {
        protected Label lblHeader;
        protected Label lblInstructionHeader;
        protected Label lblPrintDesignInstruction;
        protected Label lblSaveDesignInstruction;
        protected Label lblChangeDesignInstruction;

        private void Page_Load(object sender, EventArgs e) {
            if(!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(this.PreviewDescription_DataBinding);
        }
        #endregion

        private void PreviewDescription_DataBinding(object sender, EventArgs e) {
            lblHeader.Text = PreviewDescriptionStrings.Header();
            lblInstructionHeader.Text = PreviewDescriptionStrings.InstructionHeader();
            lblPrintDesignInstruction.Text = PreviewDescriptionStrings.PrintDesignInstruction();
            lblSaveDesignInstruction.Text = PreviewDescriptionStrings.SaveDesignInstruction();
            lblChangeDesignInstruction.Text = PreviewDescriptionStrings.ChangeDesignInstruction();
        }
    }
}