using System;
using System.Globalization;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.Controls;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner {
    public class Support : StandardPage {
        protected Literal ctlPageContent;
        protected HtmlAnchor hypTellUs;
        protected Label lblFaq;
        protected Faq ctlFaq;
        
        #region Url
        private const string RawUrl = "Support.aspx";

        public static string PageName() {
            return RawUrl;
        }

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Support_DataBinding);
            rawUrl = RawUrl;
        }
        #endregion

        #region DataBinding
        private void Support_DataBinding(object sender, EventArgs e) {
            string culture = StorageManager.CurrentLanguage;
            
            LocalizedText pageContent = BCSupportPage.Get(culture, StorageManager.CurrentRetailer);
            if (pageContent == null)
                pageContent = new LocalizedText(culture, string.Empty);

            ctlPageContent.Text = pageContent.Text;
            hypTellUs.InnerText = SupportStrings.TextHypTellUs();
            lblFaq.Text = SupportStrings.TextFaq();

            ctlFaq.DataSource = BCFaq.Enum(StorageManager.CurrentRetailer);


            headerText = SupportStrings.TextSupport();
        }
        #endregion
    }
}