using System;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web;

using Neato.Cpt.Entity;

namespace Neato.Cpt.WebDesigner {
    public class StorageManager {
        public static bool GenerateNewProject {
            get {
                if(HttpContext.Current.Session["WebDesigner.Session.GenerateNewProject"] == null)
                      return true;
                return (bool)HttpContext.Current.Session["WebDesigner.Session.GenerateNewProject"];
            }
            set { HttpContext.Current.Session["WebDesigner.Session.GenerateNewProject"] = value; }
        }

        public static bool IsSiteClosed {
            get { return (bool)HttpContext.Current.Application["WebDesigner.Session.IsSiteClosed"]; }
            set { HttpContext.Current.Application["WebDesigner.Session.IsSiteClosed"] = value; }
        }

        public static Project CurrentProject {
            get { return (Project)HttpContext.Current.Session["WebDesigner.Session.Project"]; }
            set { HttpContext.Current.Session["WebDesigner.Session.Project"] = value; }
        }
        public static Device CurrentDevice 
        {
            get { return (Device)HttpContext.Current.Session["WebDesigner.Session.Device"]; }
            set { HttpContext.Current.Session["WebDesigner.Session.Device"] = value; }
        }
    
        public StorageManager() {}

        public static string DefaultCulture = string.Empty;
        public static string CurrentLanguage {
            get {
                if (HttpContext.Current.Session["WebDesigner.Session.CurrentLanguage"] != null) {
                    return (string)HttpContext.Current.Session["WebDesigner.Session.CurrentLanguage"];
                } else {
                    return Thread.CurrentThread.CurrentUICulture.Name;
                }
            }
            set { HttpContext.Current.Session["WebDesigner.Session.CurrentLanguage"] = value; }
        }


        public static Customer CurrentCustomer {
            get { return (Customer)HttpContext.Current.Session["CurrentCustomer"]; }
            set { HttpContext.Current.Session["CurrentCustomer"] = value; }
        }
        public static Retailer CurrentRetailer {
            get { return (Retailer)HttpContext.Current.Session["CurrentRetailer"]; }
            set { HttpContext.Current.Session["CurrentRetailer"] = value; }
        }

        #region Calibration
        public static PdfCalibration CookieCalibration {
            get {
                PdfCalibration calibration = new PdfCalibration();
                HttpCookie objCookie = HttpContext.Current.Request.Cookies["Calibration"];
                if (objCookie != null) {
                    calibration.X = float.Parse(objCookie.Values["x"], CultureInfo.InvariantCulture);
                    calibration.Y = float.Parse(objCookie.Values["y"], CultureInfo.InvariantCulture);
                }
                return calibration;
            }
            set {
                HttpCookie objCookie = new HttpCookie("Calibration");
                objCookie.Expires = DateTime.Now.AddDays(1);
                objCookie.Values.Add("x", value.X.ToString(CultureInfo.InvariantCulture));
                objCookie.Values.Add("y", value.Y.ToString(CultureInfo.InvariantCulture));

                HttpContext.Current.Response.Cookies.Add(objCookie);
            }
        }

        public static PdfCalibration SessionCalibration {
            get { return (PdfCalibration)HttpContext.Current.Session["calibration"]; }
            set { HttpContext.Current.Session["calibration"] = value; }
        }

        #endregion

        public static void ChangeLastEditedPaper(PaperBase newPaper) {
            ArrayList papers = new ArrayList();
            int MaxLastEditedPaperRows = Configuration.MaxLastEditedPaperRows;
            if (MaxLastEditedPaperRows > 0) papers.Add(newPaper);
            PaperBase[] lastEditPapers = CookieLastEditedPapers;

            if (lastEditPapers != null) {
                foreach(PaperBase paper in lastEditPapers) {
                    if (paper.Id != newPaper.Id) {
                        if (papers.Count == MaxLastEditedPaperRows) break;
                        papers.Add(new PaperBase(paper.Id));
                    }
                }
            }

            CookieLastEditedPapers = (PaperBase[])papers.ToArray(typeof(PaperBase));
        }

        public static PaperBase[] CookieLastEditedPapers {
            get {
                ArrayList papers = new ArrayList();

                HttpCookie objCookie = HttpContext.Current.Request.Cookies["LastEditedPapers"];
                if (objCookie != null) {
                    int count = int.Parse(objCookie.Values["count"],
                        CultureInfo.InvariantCulture);

                    for (int i = 0; i < count; i++) {
                        int id = int.Parse(objCookie.Values[i.ToString()],
                            CultureInfo.InvariantCulture);

                        papers.Add(new PaperBase(id));
                    }
                }

                return (PaperBase[])papers.ToArray(typeof(PaperBase));
            }
            set {
                HttpCookie objCookie = new HttpCookie("LastEditedPapers");
                objCookie.Expires = DateTime.Now.AddYears(10);

                int index = 0;
                foreach (PaperBase paper in value) {
                    objCookie.Values.Add(
                        (index++).ToString(),
                        paper.Id.ToString(CultureInfo.InvariantCulture));
                }
                objCookie.Values.Add("count",
                    index.ToString(CultureInfo.InvariantCulture));

                HttpContext.Current.Response.Cookies.Add(objCookie);
            }
        }
    }
}
