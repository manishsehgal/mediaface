using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.WebDesigner.Controls;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner {
    public class Feedback : StandardPage {
        protected Label lblCaption;
        protected Label lblThank;
        protected Label lblMessage;
        protected ImageButton btnSend;
        protected ImageButton btnCancel;
        protected ImageButton btnClose;
        protected TextBox txtEmailText;

        protected Label lblRequired;
        protected Label lblYourEmail;
        protected TextBox txtYourEmail;
        protected RequiredFieldValidator vldEmailAddress;
        protected CustomValidator vldValidEmail;
        protected Panel panFeedbackMessage;
        protected Panel panFeedbackAnswer;

        #region Url
        private const string RawUrl = "Feedback.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
          //  if (!IsPostBack) {
                DataBind();
            //}
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new System.EventHandler(this.Feedback_DataBinding);
            this.btnSend.Click += new System.Web.UI.ImageClickEventHandler(this.btnSend_Click);
            this.btnCancel.Click += new System.Web.UI.ImageClickEventHandler(this.btnCancel_Click);
            this.btnClose.Click += new System.Web.UI.ImageClickEventHandler(this.btnClose_Click);
            this.vldValidEmail.ServerValidate += new ServerValidateEventHandler(vldValidEmail_ServerValidate);
            rawUrl = RawUrl;
        }
        #endregion

        #region DataBinding
        private void Feedback_DataBinding(object sender, EventArgs e) {
            lblCaption.Text = FeedbackStrings.TextCaption();
            lblThank.Text = FeedbackStrings.TextThank();

            lblRequired.Text = FeedbackStrings.TextRequired();
            lblYourEmail.Text = FeedbackStrings.TextYourEmail();
            lblMessage.Text = FeedbackStrings.TextMessage();
            vldEmailAddress.Text = FeedbackStrings.ValidatorEmailAddress();
            vldValidEmail.Text = FeedbackStrings.ValidatorInvalidEmailAddress();

            if (HttpContext.Current.User.IsInRole(Authentication.RegisteredUser)) {
                txtYourEmail.Text = StorageManager.CurrentCustomer.Email;
            }

            SetControlsAvaliability(false);

            
            headerText = FeedbackStrings.TextFeedback();
        }
        #endregion

        private void SetControlsAvaliability(bool isSendPressed) {
            panFeedbackMessage.Visible = !isSendPressed;
            panFeedbackAnswer.Visible = isSendPressed;
        }

        private void btnSend_Click(object sender, ImageClickEventArgs e) {
            vldEmailAddress.Validate();
            vldValidEmail.Validate();
            if (!(vldEmailAddress.IsValid && vldValidEmail.IsValid))
                return;

            int maxLength = Configuration.MaxFeedbackEmailLength;

            string userMessage = txtEmailText.Text;
            if(userMessage.Length > maxLength)
                userMessage = userMessage.Substring(0, maxLength);
            string userEmail = txtYourEmail.Text;
            try {
                BCMail.SendUserFeedbackMail(userEmail, userMessage, StorageManager.CurrentLanguage, true);
            } catch {
                vldValidEmail.IsValid = false;
                return;
            }

            txtEmailText.Text = userMessage;
            SetControlsAvaliability(true);
        }

        private void btnCancel_Click(object sender, ImageClickEventArgs e) {
            Response.Redirect(Support.Url().PathAndQuery);
        }

        private void btnClose_Click(object sender, ImageClickEventArgs e) {
            Response.Redirect(Support.Url().PathAndQuery);
        }

        private void vldValidEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = FormatValidator.EnsureValidEmail(args.Value.Trim());
        }
    }
}