using System;
using System.Globalization;
using System.Threading;

using Neato.Cpt.WebDesigner.Controls;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebDesigner {
    public class ErrorPage : StandardPage {
        protected LinkList ctlErrorMessage;

        #region Url
        private const string RawUrl = "ErrorPage.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(StorageManager.CurrentLanguage);
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Error_DataBinding);
            rawUrl = RawUrl;
        }

        private void Error_DataBinding(object sender, EventArgs e) {
            ctlErrorMessage.HeaderField = ErrorPageStrings.TextErrorMessage();
            headerText = ErrorPageStrings.TextError();
        }
        #endregion
    }
}