#ifndef __NeatoXmlFormatter__
#define __NeatoXmlFormatter__

/* ---------------------------------------------------------------------------

	NeatoXmlFormatter.h
 
	Copyright (c) 1995 Adobe Systems Incorporated
	All Rights Reserved
	
   --------------------------------------------------------------------------- */

#include "AIFileFormat.h"

extern AIErr addFileFormat( SPInterfaceMessage *message );
extern AIErr goGetFormatParms( AIFileFormatMessage *message );
extern AIErr goFileFormat( AIFileFormatMessage *message );
extern AIErr goCheckFormat( AIFileFormatMessage *message );

#endif
