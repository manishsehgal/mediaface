/* plugin.h */

#ifndef __PLUGIN_H__
#define __PLUGIN_H__

#define kPIPL						16000

#define kAboutDialog				16050
#define kErrorDialog				16051

#define kDlgOKButton				1
#define kDlgCancelButton			2
#define kErrorMessageText			3
#define kErrorCodeText				4

#define kErrorStrings				16050

#endif
