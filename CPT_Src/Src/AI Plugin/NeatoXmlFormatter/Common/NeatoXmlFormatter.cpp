/* ---------------------------------------------------------------------------

	NeatoXmlFormatter.cpp
 
	Copyright (c) 1995-6 Adobe Systems Incorporated
	All Rights Reserved
	
   --------------------------------------------------------------------------- */

#include "IllustratorSDK.h"

#include "NeatoXmlFormatter.h"

#include "common.h"
#include "plugin.h"			// for dialog constants
#include <strstream>

AIRealPoint orgRuler;
AIRealRect faceBound;
AIRealPoint currentPos;

const AIReal tollerance = 0.5f;	// precision of approximation for all paths

const char chCutline[] = "cutline";

const AIRealPoint AiToFace(const AIRealPoint& p)
{
	const AIRealPoint rp = 
	{
		p.h + orgRuler.h - faceBound.left,
		orgRuler.v - p.v - faceBound.top
	};
	return rp;
}

void InvalidateCurrentPos() {
	currentPos.h = -1E20f;
	currentPos.h = -1E20f;
}

AIReal GetScalarProduct(const AIRealPoint& p0, const AIRealPoint& p1) {
// return scalar product of tho vectors with origin in (0,0)
	return p0.h*p1.h + p0.v*p1.v;
}

AIReal GetScalarProduct(const AIRealPoint& p0, const AIRealPoint& p1, const AIRealPoint& p2, const AIRealPoint& p3) {
// return scalar product of tho vectors p1-p0 and p3-p2
	return (p1.h-p0.h)*(p3.h-p2.h) + (p1.v-p0.v)*(p3.v-p2.v);
}

AIReal GetDistance(const AIRealPoint& p0, const AIRealPoint& p1) {
// return the distance between two points
	AIReal dh = p1.h - p0.h;
	AIReal dv = p1.v - p0.v;
	return sqrt(dh*dh + dv*dv);
}

// return the middle of a segment define by two points
AIRealPoint GetMiddle(const AIRealPoint& p0, const AIRealPoint& p1)
{
	const AIRealPoint rp =
	{
		(p0.h + p1.h) / 2.0F,
		(p0.v + p1.v) / 2.0F
	};
	return rp;
}

// return a point on a segment [p0, p1] which distance from p0
// is ratio of the length [p0, p1]
AIRealPoint GetPointOnSegment(const AIRealPoint& p0, const AIRealPoint& p1, AIReal ratio)
{
	const AIRealPoint rp =
	{
		p0.h + (p1.h - p0.h) * ratio,
		p0.v + (p1.v - p0.v) * ratio
	};
	return rp;
}

// return middle point of quadratic curve with anchors p0, p1 and control point cp
AIRealPoint GetMiddleOfQuadrCurve(const AIRealPoint& p0, const AIRealPoint& cp, const AIRealPoint& p1) {
	const AIRealPoint rp =
	{
		(p0.h + p1.h + 2*cp.h)*0.25f,
		(p0.v + p1.v + 2*cp.v)*0.25f
	};
	return rp;
}

// return a point of quadratic curve with anchors p0, p1 and control point cp 
//  corresponding to qiven ratio; 
//  ratio = 0  is p0
//  ratio = 1  is p1
//  ratio = 1/2 is middle point 
AIRealPoint GetPointOnQuadrCurve(const AIRealPoint& p0, const AIRealPoint& cp, const AIRealPoint& p1, AIReal ratio) {
	const AIReal rCompl = 1-ratio;
	const AIRealPoint rp =
	{
		(p0.h*ratio*ratio + 2*cp.h*ratio*rCompl + p1.h*rCompl*rCompl),
		(p0.v*ratio*ratio + 2*cp.v*ratio*rCompl + p1.v*rCompl*rCompl)
	};
	return rp;
}

void SaveSegment(std::strstream& strbuf, const AIPathSegment& seg0, const AIPathSegment& seg1)
{
	const AIRealPoint p0 = AiToFace(seg0.p);
	const AIRealPoint p1 = AiToFace(seg0.out);
	const AIRealPoint p2 = AiToFace(seg1.in);
	const AIRealPoint p3 = AiToFace(seg1.p);

	AIReal Dh = p3.h - p0.h;
	AIReal Dv = p3.v - p0.v;

	bool drawLine = false;

	if(	GetDistance(p0,p3) < tollerance &&
		GetDistance(p0,p1) < tollerance &&
		GetDistance(p2,p3) < tollerance ) // segment is too short, so just ignore it
	{
		if(GetDistance(p3,currentPos) < tollerance)
			return;	// we are near last shown point - so just ignore this segment
		else 
			drawLine = true;	// approximate sequence of short segments by a line
	}

	if ( abs((p0.h - p1.h)*Dv - (p0.v - p1.v)*Dh) < tollerance*tollerance &&
		 abs((p0.h - p2.h)*Dv - (p0.v - p2.v)*Dh) < tollerance*tollerance )
		drawLine = true;    // This is a straight line
		
	if(drawLine) 
	{
		strbuf << "\n<LineTo "
			<< "x=\"" << p3.h << "\" "
			<< "y=\"" << p3.v << "\"/>";
	}
	else
	{
		// This is a cubic Bezier curve - translate it to quadratic

		// calculates the useful base points
		const AIRealPoint pa = GetPointOnSegment(p0, p1, 3.0F/4.0F);
		const AIRealPoint pb = GetPointOnSegment(p3, p2, 3.0F/4.0F);

		// get 1/16 of the [p3, p0] segment
		AIReal dx = (p3.h - p0.h)/16.0F;
		AIReal dy = (p3.v - p0.v)/16.0F;

		// calculates control point 1
		const AIRealPoint pc1 = GetPointOnSegment(p0, p1, 3.0F/8.0F);

		// calculates control point 2
		AIRealPoint pc2 = GetPointOnSegment(pa, pb, 3.0F/8.0F);
		pc2.h -= dx;
		pc2.v -= dy;

		// calculates control point 3
		AIRealPoint pc3 = GetPointOnSegment(pb, pa, 3.0F/8.0F);
		pc3.h += dx;
		pc3.v += dy;

		// calculates control point 4
		const AIRealPoint pc4 = GetPointOnSegment(p3, p2, 3.0F/8.0F);

		// calculates the 3 anchor points
		const AIRealPoint pa1 = GetMiddle(pc1, pc2);
		const AIRealPoint pa2 = GetMiddle(pa, pb);
		const AIRealPoint pa3 = GetMiddle(pc3, pc4);

		const AIRealPoint pA = GetPointOnSegment(p0, p1, 3.0F/2.0F);
		const AIRealPoint pB = GetPointOnSegment(p3, p2, 3.0F/2.0F);
		const AIRealPoint pAB = GetMiddle(pA,pB);

		const AIReal dAB = GetDistance(pA,pB);
		const AIReal d03 = GetDistance(p0,p3);

		if(dAB < 0.1f*d03 &&
			GetDistance(pa2,GetMiddleOfQuadrCurve(p0,pAB,p3)) < tollerance &&
			GetDistance(pa1,GetPointOnQuadrCurve(p0,pAB,p3,0.25f)) < tollerance &&
			GetDistance(pa3,GetPointOnQuadrCurve(p0,pAB,p3,0.75f)) < tollerance
			) {
		// draw one quadratic segment
			strbuf << "\n<CurveTo "
				<< "cx=\"" << pAB.h << "\" "
				<< "cy=\"" << pAB.v << "\" "
				<< "x=\"" << p3.h << "\" "
				<< "y=\"" << p3.v << "\"/>";
		}
		else if(dAB < d03 &&
			GetDistance(pa1,GetMiddleOfQuadrCurve(p0,pa,pa2)) < tollerance &&
			GetDistance(pa3,GetMiddleOfQuadrCurve(pa2,pb,p3)) < tollerance
			) {
		// draw two quadratic subsegments
			strbuf << "\n<CurveTo "
				<< "cx=\"" << pa.h << "\" "
				<< "cy=\"" << pa.v << "\" "
				<< "x=\"" << pa2.h << "\" "
				<< "y=\"" << pa2.v << "\"/>";
			strbuf << "\n<CurveTo "
				<< "cx=\"" << pb.h << "\" "
				<< "cy=\"" << pb.v << "\" "
				<< "x=\"" << p3.h << "\" "
				<< "y=\"" << p3.v << "\"/>";
		}
		else {
		// draw the four quadratic subsegments
			strbuf << "\n<CurveTo "
			<< "cx=\"" << pc1.h << "\" "
			<< "cy=\"" << pc1.v << "\" "
			<< "x=\"" << pa1.h << "\" "
			<< "y=\"" << pa1.v << "\"/>";
			strbuf << "\n<CurveTo "
			<< "cx=\"" << pc2.h << "\" "
			<< "cy=\"" << pc2.v << "\" "
			<< "x=\"" << pa2.h << "\" "
			<< "y=\"" << pa2.v << "\"/>";
			strbuf << "\n<CurveTo "
			<< "cx=\"" << pc3.h << "\" "
			<< "cy=\"" << pc3.v << "\" "
			<< "x=\"" << pa3.h << "\" "
			<< "y=\"" << pa3.v << "\"/>";
			strbuf << "\n<CurveTo "
			<< "cx=\"" << pc4.h << "\" "
			<< "cy=\"" << pc4.v << "\" "
			<< "x=\"" << p3.h << "\" "
			<< "y=\"" << p3.v << "\"/>";
		}
	}
	currentPos.h = p3.h;
	currentPos.v = p3.v;
}

void SaveRectangle(std::strstream& strbuf, const AIPathSegment* const pS)
{
	const AIRealPoint& p0 = AiToFace(pS[0].p);
	const AIRealPoint& p1 = AiToFace(pS[1].p);
	const AIRealPoint& p2 = AiToFace(pS[2].p);
	const AIRealPoint& p3 = AiToFace(pS[3].p);
	// find top left corner
	const AIReal x0 = min(min(p0.h, p1.h), min(p2.h, p3.h));
	const AIReal y0 = min(min(p0.v, p1.v), min(p2.v, p3.v));
	// find bottom right corner
	const AIReal x1 = max(max(p0.h, p1.h), max(p2.h, p3.h));
	const AIReal y1 = max(max(p0.v, p1.v), max(p2.v, p3.v));
	// find width and height
	const AIReal w = x1 - x0;
	const AIReal h = y1 - y0;

	strbuf << "\n<Rectangle "
		<< "x=\"" << x0 << "\" "
		<< "y=\"" << y0 << "\" "
		<< "w=\"" << w << "\" "
		<< "h=\"" << h << "\"/>";

	InvalidateCurrentPos();
}

void SaveCircle(std::strstream& strbuf, const AIPathSegment* const pS)
{
	// get the center point
	AIRealPoint p = GetMiddle(pS[0].p, pS[2].p);
	// get point on the cicle
	const AIRealPoint& p0 = pS[0].p;
	// get radius
	const AIReal r = GetDistance(p0,p);
	p = AiToFace(p);
	strbuf << "\n<Circle "
		<< "x=\"" << p.h << "\" "
		<< "y=\"" << p.v << "\" "
		<< "r=\"" << r << "\"/>";

	InvalidateCurrentPos();
}

bool IsRectangle(const AIPathSegment* const pS)
{
	const AIRealPoint& p0 = pS[0].p;
	const AIRealPoint& p1 = pS[1].p;
	const AIRealPoint& p2 = pS[2].p;
	const AIRealPoint& p3 = pS[3].p;
	return
		pS[0].corner && pS[1].corner &&
		pS[2].corner && pS[3].corner &&			// all points are corners
		abs(p0.h - p1.h) < tollerance &&
		abs(p1.v - p2.v) < tollerance &&
		abs(p2.h - p3.h) < tollerance &&
		abs(p3.v - p0.v) < tollerance ||
		abs(p0.v - p1.v) < tollerance &&
		abs(p1.h - p2.h) < tollerance &&
		abs(p2.v - p3.v) < tollerance && 
		abs(p3.h - p0.h) < tollerance ;		// and they are positioned at the same X0, X1, Y0, Y1
}

bool IsCircle(const AIPathSegment* const pS)
{
	const AIPathSegment& s0 = pS[0];
	const AIPathSegment& s1 = pS[1];
	const AIPathSegment& s2 = pS[2];
	const AIPathSegment& s3 = pS[3];

	// get expected center point
	const AIRealPoint pC = GetMiddle(s0.p, s2.p);
	// get expected radius
	const AIReal r = GetDistance(s0.p,pC);

	AIReal dt = r*0.552293f;

	return
		!s0.corner && !s1.corner &&
		!s2.corner && !s3.corner &&			// there are no corners
		GetDistance(pC,GetMiddle(s1.p,s3.p)) < tollerance && // expected centers "almost coincide", so we can speak about "radiuces"
		abs(s0.in.v + s0.out.v - 2* s0.p.v) < tollerance &&
		abs(s0.in.h + s0.out.h - 2* s0.p.h) < tollerance &&	// s0 is symmetrical
		abs(s1.in.v + s1.out.v - 2* s1.p.v) < tollerance &&
		abs(s1.in.h + s1.out.h - 2* s1.p.h) < tollerance &&	// s1 is symmetrical
		abs(s2.in.v + s2.out.v - 2* s2.p.v) < tollerance &&
		abs(s2.in.h + s2.out.h - 2* s2.p.h) < tollerance &&	// s2 is symmetrical
		abs(s3.in.v + s3.out.v - 2* s3.p.v) < tollerance &&
		abs(s3.in.h + s3.out.h - 2* s3.p.h) < tollerance &&	// s3 is symmetrical
		GetScalarProduct(pC,s0.p,pC,s1.p)	< tollerance*tollerance &&	// radiuces are perpendicular
		GetScalarProduct(s0.p,s0.out,pC,s0.p) < tollerance*tollerance && 
		GetScalarProduct(s1.p,s1.out,pC,s1.p) < tollerance*tollerance && 
		GetScalarProduct(s2.p,s2.out,pC,s2.p) < tollerance*tollerance && 
		GetScalarProduct(s3.p,s3.out,pC,s3.p) < tollerance*tollerance && // targets are prependicular to radiuces
		abs(GetDistance(s0.p,s0.out) - dt) < tollerance &&
		abs(GetDistance(s1.p,s1.out) - dt) < tollerance &&
		abs(GetDistance(s2.p,s2.out) - dt) < tollerance &&
		abs(GetDistance(s3.p,s3.out) - dt) < tollerance ;	// lengths of targets are as they should be in circle
}

void SavePath(std::strstream& strbuf, AIArtHandle artHandle)
{
    // get segment count
	short count;
	AIErr error = sAIPath->GetPathSegmentCount(artHandle, &count);
	if (error)
	{
		strbuf << "<Error text =\"GetPathSegmentCount failed\"/>";
		return;
	}

	std::auto_ptr<AIPathSegment> segs(new AIPathSegment[count]);
	error = sAIPath->GetPathSegments(artHandle, 0, count, segs.get());
	if (error)
	{
		strbuf << "<Error text=\"GetPathSegments\"/>";
		return;
	}

	if (count == 0)
	{
		return;
	}

	ai::UnicodeString name;
	ASBoolean isDefaultName;
	error = sAIArt->GetArtName(artHandle, name, &isDefaultName);
	if (error)
	{
		strbuf << "<Error text =\"GetArtName failed\"/>";
		return;
	}
	char chName[_MAX_PATH];
	name.as_Platform(chName, _MAX_PATH);

	AIBoolean closed;
	error = sAIPath->GetPathClosed(artHandle, &closed);
	if (error)
	{
		strbuf << "<Error text =\"GetPathClosed failed\"/>";
		return;
	}
	if (!isDefaultName && stricmp(chName, chCutline) == 0)
	{
		// this is a cutline
		const AIRealPoint p0 = AiToFace(segs.get()[0].p);
		const AIRealPoint p1 = AiToFace(segs.get()[count - 1].p);
		const char* p = 0;
		if (abs(p1.h - p0.h) > abs(p1.v - p0.v))
		{
			p = p1.h > p0.h ? "left" : "right";
		}
		else
		{
			p = p1.v > p0.v ? "top" : "bottom";
		}
		strbuf << "\n<CutLine "
			<< "x1=\"" << p0.h << "\" "
			<< "y1=\"" << p0.v << "\" "
			<< "x2=\"" << p1.h << "\" "
			<< "y2=\"" << p1.v << "\" "
			<< "scissors=\"" << p << "\"/>";
		return;
	}
	else if (closed && count == 4)
	{
		if (IsRectangle(segs.get()))
		{
			SaveRectangle(strbuf, segs.get());
			return;
		}
		else if (IsCircle(segs.get()))
		{
			SaveCircle(strbuf, segs.get());
			return;
		}
	}

	// This is a normal path
	const AIRealPoint p = AiToFace(segs.get()[0].p);

	if(	GetDistance(p,currentPos) >= tollerance) {
		strbuf << "\n<MoveTo "
			<< "x=\"" << p.h << "\" "
			<< "y=\"" << p.v << "\"/>";

		currentPos.h = p.h;
		currentPos.v = p.v;
	}

	for (int i = 1; i < count; ++i)
	{
		SaveSegment(strbuf, segs.get()[i - 1], segs.get()[i]);
	}

	if (closed)
	{
		// close the path - draw the line to the origin
		SaveSegment(strbuf, segs.get()[count - 1], segs.get()[0]);
		InvalidateCurrentPos();
	}
}

void SaveGroup(std::strstream& strbuf, AIArtHandle artHandle)
{
	for (AIErr error = sAIArt->GetArtFirstChild(artHandle, &artHandle);
		artHandle != 0 && error == 0;
		error = sAIArt->GetArtSibling(artHandle, &artHandle))
	{
		short type;
		sAIArt->GetArtType(artHandle, &type);
		if (type == kGroupArt || type == kCompoundPathArt)
		{
			// compound path is tha same as group,
			// but it can only contain path and group arts
			SaveGroup(strbuf, artHandle);
		}
		else if (type == kSymbolArt)
		{
			AIPatternHandle symbolPattern;
			error = sAISymbol->GetSymbolPatternOfSymbolArt(artHandle, &symbolPattern);
			//bResult = GetLayerBounds(artHandle, bounds) || bResult;	// the order is important!
			if (!error) {
				error = sAIPattern->GetPatternArt(symbolPattern, &artHandle);
				if (!error) {
					SaveGroup(strbuf, artHandle);
				}
			}
		}
		else if (type == kPluginArt)
		{
			AIPluginGroupSuite *spPluginGroup;
			error = sSPBasic->AcquireSuite(kAIPluginGroupSuite,
				kAIPluginGroupSuiteVersion, (const void**)&spPluginGroup);
			if (!error)
			{
				AIArtHandle art;/*	this group is invisible
				error = spPluginGroup->GetPluginArtEditArt(artHandle, &art);
				if (!error)
				{
					SaveGroup(strbuf, art);
				}*/
				error = spPluginGroup->GetPluginArtResultArt(artHandle, &art);
				if (!error)
				{
					SaveGroup(strbuf, art);
				}
			}
		}
		else if (type == kPathArt)
		{
			SavePath(strbuf, artHandle);
		}
	}
}

bool GetLayerBounds(AIArtHandle artHandle, AIRealRect *bounds)
{
	bool bResult = false;
	for (AIErr error = sAIArt->GetArtFirstChild(artHandle, &artHandle);
		artHandle != 0 && error == 0;
		error = sAIArt->GetArtSibling(artHandle, &artHandle))
	{
		short type;
		sAIArt->GetArtType(artHandle, &type);
		if (type == kGroupArt || type == kCompoundPathArt)
		{
			// compound path is tha same as group,
			// but it can only contain path and group arts
			bResult = GetLayerBounds(artHandle, bounds) || bResult;	// the order is important!
		}
        else if (type == kSymbolArt)
        {
            AIPatternHandle symbolPattern;
            error = sAISymbol->GetSymbolPatternOfSymbolArt(artHandle, &symbolPattern);
			//bResult = GetLayerBounds(artHandle, bounds) || bResult;	// the order is important!
            if (!error) {
                error = sAIPattern->GetPatternArt(symbolPattern, &artHandle);
                if (!error) {
                    bResult = GetLayerBounds(artHandle, bounds) || bResult;	// the order is important!
                }
            }
        }
		else if (type == kPluginArt)
		{
			AIPluginGroupSuite *spPluginGroup;
			error = sSPBasic->AcquireSuite(kAIPluginGroupSuite,
				kAIPluginGroupSuiteVersion, (const void**)&spPluginGroup);
			if (!error)
			{
				AIArtHandle art;/*	this group is invisible
				error = spPluginGroup->GetPluginArtEditArt(artHandle, &art);
				if (!error)
				{
					bResult = GetLayerBounds(art, bounds) || bResult;	// the order is important!
				}*/
				error = spPluginGroup->GetPluginArtResultArt(artHandle, &art);
				if (!error)
				{
					bResult = GetLayerBounds(art, bounds) || bResult;	// the order is important!
				}
			}
		}
		else if (type == kPathArt)
		{
			ai::UnicodeString name;
			ASBoolean isDefaultName;
			error = sAIArt->GetArtName(artHandle, name, &isDefaultName);
			if (error)
			{
				continue;
			}
			char chName[_MAX_PATH];
			name.as_Platform(chName, _MAX_PATH);
			if (isDefaultName || stricmp(chName, chCutline) != 0)
			{
				AIRealRect rect;
				error = sAIArt->GetArtBounds(artHandle, &rect);
				if (error)
				{
					// nothing can be done here
				}
				else
				{
					bResult = true;
					if (bounds->left > rect.left || _isnan(bounds->left))
					{
						bounds->left = rect.left;
					}
					if (bounds->top < rect.top || _isnan(bounds->top))
					{
						bounds->top = rect.top;
					}
					if (bounds->right < rect.right || _isnan(bounds->right))
					{
						bounds->right = rect.right;
					}
					if (bounds->bottom > rect.bottom || _isnan(bounds->bottom))
					{
						bounds->bottom = rect.bottom;
					}
				}
			}
		}
	}
	return bResult;
}

AIErr ParseTitle(const char* pTitle,
	char* pFace, char* pFaceType, int* piRotation, char* pFaceName)
{
	// Check Face keyword
	static const char chFaceKey[] = "Face";
	if (strncmp(pTitle, chFaceKey, strlen(chFaceKey)) != 0)
	{
		return kErrorCodeText;
	}

	// find face type
	const int iSep = '.';
	char* pType = strchr(pTitle, iSep);
	if (pType == 0)
	{
		return kErrorCodeText;
	}
	size_t iLen = pType - pTitle;
	strncpy(pFace, pTitle, iLen);
	pFace[iLen] = '\0';

	char* pName = strchr(++pType, iSep);
	if (pName == 0)
	{
		return kErrorCodeText;
	}
	iLen = pName - pType;
	strncpy(pFaceType, pType, iLen);
	pFaceType[iLen] = '\0';

	// Check if rotation is present
	static const char chRotationKey[] = "Rotation=";
	if (strncmp(++pName, chRotationKey, strlen(chRotationKey)) == 0)
	{
		pName += strlen(chRotationKey);
		*piRotation = atoi(pName);
		pName = strchr(pName, iSep);
		if (pName == 0)
		{
			return kErrorCodeText;
		}
		++pName;
	}
	else
	{
		*piRotation = 0;
	}

	// get the face name
	strcpy(pFaceName, pName);
	return kNoErr;
}

AIErr SavePaths(std::strstream& strbuf, bool bUniversal)
{
	static const char chUniversal[] = "universal";
	static const char chOutLine[] = "outline";

	strbuf << "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
	strbuf << "\n<Project>";

	AIDocumentSetup setup;
	AIErr error = sAIDocument->GetDocumentSetup(&setup);
	if (error)
	{
		strbuf << "<Error text=\"GetDocumentSetup failed\"/>";
		return error;
	}
	sAIDocument->GetDocumentRulerOrigin(&orgRuler);
	if (error)
	{
		strbuf << "<Error text=\"GetDocumentRulerOrigin failed\"/>";
		return error;
	}
	orgRuler.v = setup.height - orgRuler.v;	// adjust paper height

	AILayerSuite *spLayer;
	error = sSPBasic->AcquireSuite(kAILayerSuite,
		kAILayerSuiteVersion, (const void**)&spLayer);
	if (error)
	{
		strbuf << "<Error text=\"AcquireSuite failed\"/>";
		return error;
	}
	strbuf << "\n<Faces>";
	long iLayerCount;
	spLayer->CountLayers(&iLayerCount);
	for (int i = 0; i < iLayerCount; ++i)
	{
		AILayerHandle layer;
		error = spLayer->GetNthLayer(i, &layer);
		if (error)
		{
			strbuf << "<Error text=\"GetNthLayer failed\"/>";
			continue;
		}
		ai::UnicodeString ustrTitle;
		error = spLayer->GetLayerTitle(layer, ustrTitle);
		if (error)
		{
			strbuf << "<Error text=\"GetLayerTitle failed\"/>";
			continue;
		}
		char chTitle[_MAX_PATH];
		ustrTitle.as_Platform(chTitle, _MAX_PATH);
		char chFace[_MAX_PATH];
		char chFaceType[_MAX_PATH];
		char chFaceName[_MAX_PATH];
		int iRotation;
		if (ParseTitle(chTitle, chFace, chFaceType, &iRotation, chFaceName))
		{
			continue;
		}

		if (stricmp(chFaceType, chUniversal) != 0 && bUniversal ||
			stricmp(chFaceType, chOutLine) != 0 && !bUniversal)
		{
			continue;
		}

		AIArtHandle artHandle = 0;
		error = sAIArt->GetFirstArtOfLayer(layer, &artHandle);
		if (error)
		{
			strbuf << "<Error text=\"GetFirstArtOfLayer failed\"/>";
			continue;
		}

		faceBound.left = (AIReal)sqrt(-1.0);
		faceBound.top = (AIReal)sqrt(-1.0);
		faceBound.right = (AIReal)sqrt(-1.0);
		faceBound.bottom = (AIReal)sqrt(-1.0);

		if (!GetLayerBounds(artHandle, &faceBound))
		{
			// no Path objects found
			strbuf << "<Error text=\"No path objects found on the layer '"
				<< chTitle << "'\"/>";
			continue;
		}

		// convert to the output coordinates
		faceBound.left += orgRuler.h;
		faceBound.top = orgRuler.v - faceBound.top;
		faceBound.right += orgRuler.h;
		faceBound.bottom = orgRuler.v - faceBound.bottom;

		strbuf << "\n<Face id=\"" << chFace << "\" "
			<< "name=\"" << chFaceName << "\" "
			<< "x=\"" << faceBound.left << "\" "
			<< "y=\"" << faceBound.top << "\" ";
		if(iRotation != 0)
		strbuf << "rotation=\"" << iRotation << "\"";
		strbuf << ">";

		strbuf << "\n<Contour "
			<< "x=\"" << 0 << "\" "
			<< "y=\"" << 0 << "\">";

		InvalidateCurrentPos();

		SaveGroup(strbuf, artHandle);	// This has to be the group art

		strbuf << "\n</Contour>";
		strbuf << "\n</Face>";
	}
	strbuf << "\n</Faces>";

	static const char chDieCut[] = "diecut";
	strbuf << "\n<Paper "
		<< "w=\"" << setup.width << "\" "
		<< "h=\"" << setup.height << "\" "
		<< "type=\"" << (bUniversal ? chUniversal : chDieCut)<< "\"/>";

	strbuf << "\n</Project>";

	return error;
}

AIErr addFileFormat(SPInterfaceMessage *message) 
{
	AIErr error;
	PlatformAddFileFormatData affd;
	char title1[] = "Neato Outline XML file";
	affd.type = 'NEAT';
	affd.title = SUctopstr(title1);
	affd.titleOrder = 1;
	affd.extension = "xml";

	error = sAIFileFormat->AddFileFormat(
		message->d.self, "Neato Die Cut XML file format",
		&affd, kFileFormatExport,
		&g->fileFormatOutline);

	char title2[] = "Neato Universal XML file";
	affd.type = 'NEAT';
	affd.title = SUctopstr(title2);
	affd.titleOrder = 2;
	affd.extension = "xml";

	error = sAIFileFormat->AddFileFormat(
		message->d.self, "Neato Universal XML file format",
		&affd, kFileFormatExport,
		&g->fileFormatUniv);

	return error;
}

void ASAPI LayerSelDialogOKButtonProc(ADMItemRef item, ADMNotifierRef notifier)
{
//	ADMItemRef textItem = sADMDialog->GetItem(dialog, kErrorMessageText);
//	sADMItem->GetText(textItem, (char*)errors->err_msg);
//	std::string* strLayer = (std::string*)sADMDialog->GetUserData(dialog);

	// Call the default Notify so the dialog knows the OK button was hit.
	sADMItem->DefaultNotify(item, notifier);
}

ASErr ASAPI LayerSelDialogInitProc(ADMDialogRef dialog)
{
	sADMItem->SetNotifyProc(
		sADMDialog->GetItem(dialog, kDlgOKButton),
		LayerSelDialogOKButtonProc);

	return kNoErr;
}

AIErr goGetFormatParms(AIFileFormatMessage *message) 
{
	// Does the plug-in need any info from user?  It should get it
	// here.  The information may be needed for writing or reading.
	AIErr error = kNoErr;
/*
	if (sADMDialog)
	{
		// "Please select layer name"
		sADMDialog->Modal(message->d.self, "Neato",
			kErrorDialog, kADMModalDialogStyle, 
			LayerSelDialogInitProc, (void*)0, 0);
	}
	else
	{
		error = kCantHappenErr;
	}
*/
	return error;
}

AIErr goFileFormat(AIFileFormatMessage *message) 
{
	char pathName[_MAX_PATH];
	message->GetFilePath().GetFullPath().as_Platform(pathName, _MAX_PATH);

	FILE * fp = fopen(pathName, "w");
	if (fp == 0)
	{
		return kCantHappenErr;
	}

	std::strstream strbuf;
	strbuf.setf(ios_base::fixed);
	(void)strbuf.precision(2);

	AIErr error = SavePaths(strbuf,
		message->fileFormat == g->fileFormatUniv);

	if (!error)
	{
		fwrite(strbuf.str(), 1, strbuf.pcount(), fp);
	}

	fclose(fp);

	return error;
}

AIErr goCheckFormat(AIFileFormatMessage *message) 
{
	// this function is only needed for reading. The file is of a type we can read, but another
	// file format can also read it.  It is an opportunity to check if we can really handle it.
	// If we can we return 0, if not we return 1.  If more than one file format can read 
	// the selected file, the user will be prompted as to which format should actually
	// open the file.
	
	return kNoErr;
}
