#ifndef __AIDocumentList__
#define __AIDocumentList__

/*
 *        Name:	AIDocumentList.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator Document Suite.
 *
 * Copyright (c) 1986-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIRealMath__
#include "AIRealMath.h"
#endif

#ifndef __AIDocument__
#include "AIDocument.h"
#endif

#ifndef __AIActionManager_h__
#include "AIActionManager.h"
#endif

#include "IAIFilePath.hpp"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIDocumentList.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIDocumentListSuite			"AI Document List Suite"
#define kAIDocumentListSuiteVersion3	AIAPI_VERSION(4)

// latest version
#define kAIDocumentListSuiteVersion		kAIDocumentListSuiteVersion3
#define kAIDocumentListVersion			kAIDocumentListSuiteVersion


/*******************************************************************************
 **
 **	Types
 **
 **/

/** Specifies the document color model when creating a new document or opening
	a document. */
typedef enum
{
	kAIUnknownColorModel = -1,
	/** Unimplemented. If specied an RGB document is created. */
	kAIGrayColorModel = 0,
	kAIRGBColorModel,
	kAICMYKColorModel
} AIColorModel;

/** The startup documents. */
typedef enum
{
	/** Document used as a template for creating a new RGB color model document. */
	kAIRGBStartupDocument = 0,
	/** Document used as a template for creating a new CMYK color model document. */
	kAICMYKStartupDocument,
	/** Document defining the PDF portion of a native Illustrator file when saved
		without PDF compatibility. This usually contains some form of warning
		message. */
	kAINoPDFDataStartupDocument
} AIStartupDocumentFlag;




/*******************************************************************************
 **
 **	Suite
 **
 **/

/** The document list suite provides APIs that enumerate and operate on the list
	of open Illustrator documents.
	
	Most APIs do not take a document handle as a parameter. The plugin API has a
	notion of the "current" document and document view on which the APIs act. This
	is usually, but not always, the document and view of the active document window.
	The document list APIs make it possible to change the current document.

	Switching the current document or view commits any modifications that have been made.
	This means that an undo entry is added to the list of undoable operations, any
	deferred notifications are sent and the document windows are updated.
*/
struct AIDocumentListSuite {

	/** Counts the number of open document windows. Note that there may be multiple windows
		open for a single document so this is not a count of the number of open documents. */
	AIAPI AIErr	(*Count)( long* count );
	/** Returns the document handle that identifies the document corresponding to the
		Nth open document window. The index is 0 based. */
	AIAPI AIErr	(*GetNthDocument)( AIDocumentHandle* document, long lIndex );
	/** Create a new document. A window for the new document is opened and becomes the
		frontmost document window. The plugin API switches the current document to be
		the new document. If any of the parameters colorMode, artboardWidth, artboardHeight
		are NULL default values are used. If dialogStatus is kDialogOn then the new document
		dialog will be shown. */
	AIAPI AIErr	(*New)( ai::UnicodeString& title, AIColorModel* colorMode, AIReal* artboardWidth, AIReal* artboardHeight,
						ActionDialogStatus dialogStatus, AIDocumentHandle* document );
	/** Open the indicated file. If the file is already open a new view will be opened
		for the document. If dialogStatus is not kDialogOn no user interaction is
		allowed to take place. The plugin API switches the current document and view
		to the newly opened document window.*/
	AIAPI AIErr	(*Open)( const ai::FilePath &fileSpec, AIColorModel colorMode, 
						ActionDialogStatus dialogStatus, AIDocumentHandle* document );
	/** Saves the current document. If the document has not been saved before the user
		will be prompted for a location and save options. */
	AIAPI AIErr	(*Save)( AIDocumentHandle document );
	/** Close a view for the specified document. If the document has only one open window
		this will close the document. The user will be prompted to save the document if
		it has been modified since last saved. The plugin API switches the current document
		to the active document window regardless of whether it was the current view that
		was closed. */
	AIAPI AIErr	(*Close)( AIDocumentHandle document );
	/** Close all open document windows. This is equivalent to invoking AIDocumentListSuite::Close()
		for each open window. */
	AIAPI AIErr	(*CloseAll)();
	/** The parameter bSetFocus must be true else this is a no-op. Activates a window
		for the specified document. The plugin API switches the current document and view
		to be those for the activated window. */
	AIAPI AIErr	(*Activate)( AIDocumentHandle document, ASBoolean bSetFocus );
	/** Print the specified document. */
	AIAPI AIErr	(*Print)( AIDocumentHandle document, ActionDialogStatus dialogStatus );

	// New in Illustrator 11.0

	/** Get the document handle for one of the startup documents. */
	AIAPI AIErr	(*GetStartupDocument)( AIDocumentHandle *startupDocument, AIStartupDocumentFlag flag );
	/** Make the specified startup document the current document for the plugin API.
		A plugin should not call any APIs that might attempt to modify the current
		document when it is set to the startup document. */
	AIAPI AIErr	(*ActivateStartupDocument)( AIDocumentHandle startupDocument );


};



#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
