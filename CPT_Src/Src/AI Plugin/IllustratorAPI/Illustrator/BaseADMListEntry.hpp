/***********************************************************************/
/*                                                                     */
/* BaseADMListEntry.hpp                                                */
/*                                                                     */
/* Copyright 1998-1999 Adobe Systems Incorporated.                     */
/* All Rights Reserved.                                                */
/*                                                                     */
/* Patents Pending                                                     */
/*                                                                     */
/* NOTICE: All information contained herein is the property of Adobe   */
/* Systems Incorporated. Many of the intellectual and technical        */
/* concepts contained herein are proprietary to Adobe, are protected   */
/* as trade secrets, and are made available only to Adobe licensees    */
/* for their internal use. Any reproduction or dissemination of this   */
/* software is strictly forbidden unless prior written permission is   */
/* obtained from Adobe.                                                */
/*                                                                     */
/* Started by Darin Tomack, 02/02/1998                                 */
/*                                                                     */
/***********************************************************************/

#pragma once

/*
 * Includes
 */

#ifndef __IADMListEntry_hpp__
#include "IADMListEntry.hpp"
#endif

#include "IADMHierarchyList.hpp"
#include "IADMDrawer.hpp"
#include "IADMTracker.hpp"
#include "IADMNotifier.hpp"
#include "ASTypes.h"

class IADMDrawer;
class IADMNotifier;
class IADMTracker;


/*
 * Base Class
 */

template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
class BaseADMListEntryClass : public BaseIADMListEntryClass
{
private:
	static ASErr ASAPI InitProc(ADMListEntryRef entry);
	static void ASAPI DrawProc(ADMListEntryRef entry, ADMDrawerRef drawer);
	static ASBoolean ASAPI TrackProc(ADMListEntryRef entry, ADMTrackerRef tracker);
	static void ASAPI NotifyProc(ADMListEntryRef entry, ADMNotifierRef notifier);
	static void ASAPI DestroyProc(ADMListEntryRef entry);

protected:
	IADMHierarchyList fHierarchyList;
	ADMUserData fUserData;

	virtual ASErr Init();
	virtual void Draw(ADMDrawerClass drawer);
	virtual ASBoolean Track(ADMTrackerClass tracker);
	virtual void Notify(ADMNotifierClass notifier);

	virtual void BaseDraw(ADMDrawerClass drawer);

public:
	BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass, ADMNotifierClass, BaseIADMListEntryClass>(BaseIADMListEntryClass entry, ADMUserData data = NULL);
	BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass, ADMNotifierClass, BaseIADMListEntryClass>(IADMHierarchyList list, ADMUserData entryUserData = NULL);
	virtual ~BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass, ADMNotifierClass, BaseIADMListEntryClass>();

	virtual ADMListEntryRef Create(ADMHierarchyListRef list, ADMUserData data = NULL);

	void SetUserData(ADMUserData userData);
	ADMUserData GetUserData();

	ASErr DoInit();
};


typedef BaseADMListEntryClass<IADMDrawer, IADMTracker, IADMNotifier, IADMListEntry> BaseADMListEntry;


template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
void BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass, ADMNotifierClass, BaseIADMListEntryClass>::SetUserData(ADMUserData userData)
{
	fUserData = userData;
}

template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
ADMUserData BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass,ADMNotifierClass, BaseIADMListEntryClass>::GetUserData()
{
	return fUserData;
}

template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
ASErr BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass,ADMNotifierClass, BaseIADMListEntryClass>::DoInit()
{
	return Init();
}


////////////////////////////////////////
/*
 * Constructor
 */

template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass,ADMNotifierClass, BaseIADMListEntryClass>::BaseADMListEntryClass(BaseIADMListEntryClass entry, ADMUserData data)
: BaseIADMListEntryClass(entry),
  fUserData(data),
  fHierarchyList(GetList())
{
	if (fEntry && fHierarchyList)
	{
		sADMListEntry->SetUserData(fEntry, this);
		sADMHierarchyList->SetDrawProc(fHierarchyList, DrawProc);
		sADMHierarchyList->SetTrackProc(fHierarchyList, TrackProc);
		sADMHierarchyList->SetNotifyProc(fHierarchyList, NotifyProc);
		sADMHierarchyList->SetDestroyProc(fHierarchyList, DestroyProc);
	}
}

template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass,ADMNotifierClass, BaseIADMListEntryClass>::BaseADMListEntryClass(IADMHierarchyList list, ADMUserData entryUserData)
: BaseIADMListEntryClass((ADMListEntryRef) NULL),
  fUserData(entryUserData),
  fHierarchyList(list)
{
	if (fHierarchyList){
		fEntry = sADMListEntry->Create( fHierarchyList );

		if (fEntry)
		{
			sADMListEntry->SetUserData(fEntry, this);
			sADMHierarchyList->SetDrawProc(fHierarchyList, DrawProc);
			sADMHierarchyList->SetTrackProc(fHierarchyList, TrackProc);
			sADMHierarchyList->SetNotifyProc(fHierarchyList, NotifyProc);
			sADMHierarchyList->SetDestroyProc(fHierarchyList, DestroyProc);
		}
	}
}

/*
 * Destructor
 */

template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass,ADMNotifierClass, BaseIADMListEntryClass>::~BaseADMListEntryClass()
{
	if (sADMListEntry)
	{
		sADMListEntry->SetUserData(fEntry, NULL);

		/* We can't do this since other entries depend on these
		   procs.

		sADMHierarchyList->SetDrawProc(fHierarchyList, NULL);
		sADMHierarchyList->SetTrackProc(fHierarchyList, NULL);
		sADMHierarchyList->SetNotifyProc(fHierarchyList, NULL);
		sADMHierarchyList->SetDestroyProc(fHierarchyList, NULL);
		*/

		sADMListEntry->Destroy(*this);
	}
			
//	fEntry = NULL;
}


template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
ADMListEntryRef BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass, ADMNotifierClass, BaseIADMListEntryClass>::Create(ADMHierarchyListRef list, ADMUserData data)								
{
	fHierarchyList = list;
	fUserData = data;

	if (fHierarchyList)
	{
		fEntry = sADMListEntry->Create( fHierarchyList );

		if (fEntry)
		{
			SetUserData(this);
			sADMHierarchyList->SetDrawProc(fHierarchyList, DrawProc);
			sADMHierarchyList->SetTrackProc(fHierarchyList, TrackProc);
			sADMHierarchyList->SetNotifyProc(fHierarchyList, NotifyProc);
			sADMHierarchyList->SetDestroyProc(fHierarchyList, DestroyProc);
		}
	}

	return fEntry;	
}


/*
 * Virtual Functions that call Defaults.
 */
	
template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
ASErr BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass, ADMNotifierClass, BaseIADMListEntryClass>::Init()
{
	return kNoErr;
}

template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
void BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass, ADMNotifierClass, BaseIADMListEntryClass>::Draw(ADMDrawerClass drawer)
{
	sADMListEntry->DefaultDraw(fEntry, drawer);
}

template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
ASBoolean BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass, ADMNotifierClass, BaseIADMListEntryClass>::Track(ADMTrackerClass tracker)
{
	return 	sADMListEntry->DefaultTrack(fEntry, tracker);
}

template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
void BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass, ADMNotifierClass, BaseIADMListEntryClass>::Notify(ADMNotifierClass notifier)
{
	sADMListEntry->DefaultNotify(fEntry, notifier);
}



/*
 * Static Procs that call Virtual Functions.
 */
	
template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
ASErr ASAPI BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass, ADMNotifierClass, BaseIADMListEntryClass>::InitProc(ADMListEntryRef entry)
{
	BaseADMListEntryClass* admEntry = (BaseADMListEntryClass*) sADMListEntry->GetUserData(entry);
	
	admEntry->fEntry = entry;

	if (admEntry)
		return admEntry->Init();
	else
		return kBadParameterErr;
}

template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
void ASAPI BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass, ADMNotifierClass, BaseIADMListEntryClass>::DrawProc(ADMListEntryRef entry, ADMDrawerRef drawer)
{
	BaseADMListEntryClass* admEntry = (BaseADMListEntryClass*) sADMListEntry->GetUserData(entry);
	
	if (admEntry)
		admEntry->BaseDraw(drawer);
}

template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
ASBoolean ASAPI BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass, ADMNotifierClass, BaseIADMListEntryClass>::TrackProc(ADMListEntryRef entry, ADMTrackerRef tracker)
{
	BaseADMListEntryClass* admEntry = (BaseADMListEntryClass*) sADMListEntry->GetUserData(entry);
	
	if (admEntry)
		return admEntry->Track(tracker);
	else
		return false;
}

template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
void ASAPI BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass, ADMNotifierClass, BaseIADMListEntryClass>::NotifyProc(ADMListEntryRef entry, ADMNotifierRef notifier)
{
	BaseADMListEntryClass* admEntry = (BaseADMListEntryClass*) sADMListEntry->GetUserData(entry);
	
	if (admEntry)
		admEntry->Notify(notifier);
}

template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
void ASAPI BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass, ADMNotifierClass, BaseIADMListEntryClass>::DestroyProc(ADMListEntryRef entry)
{
	BaseADMListEntryClass* admEntry = (BaseADMListEntryClass*) sADMListEntry->GetUserData(entry);
	
	if (admEntry)
		delete admEntry;
}


template <class ADMDrawerClass, class ADMTrackerClass, class ADMNotifierClass, class BaseIADMListEntryClass>
void BaseADMListEntryClass<ADMDrawerClass, ADMTrackerClass, ADMNotifierClass, BaseIADMListEntryClass>::BaseDraw(ADMDrawerClass drawer)
{
	Draw(drawer);
}
