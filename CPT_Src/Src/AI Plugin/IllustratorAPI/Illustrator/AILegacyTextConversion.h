#ifndef __AILegacyTextConversion__
#define __AILegacyTextConversion__

/*
 *        Name:	AILegacyTextConversion.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 11.0 Text Frame Object Suite.
 *
 * Copyright (c) 1986-2002 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AILegacyTextConversion.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAILegacyTextConversionSuite			"AI Legacy Text Conversion Suite"
#define kAILegacyTextConversionSuiteVersion1	AIAPI_VERSION(1)
#define kAILegacyTextConversionSuiteVersion		kAILegacyTextConversionSuiteVersion1
#define kAILegacyTextConversionVersion			kAILegacyTextConversionSuiteVersion

// Text Converter errors.


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** Converts legacy text object to native text objects. */
typedef struct {

	/** Convert legacy text object to native Illustrator text art objects.  The return is Group of #kTextFrameArt.
		"art" is not disposed by this function. */
	AIAPI AIErr (*ConvertToNative)(AIArtHandle art, short paintOrder, AIArtHandle prep, AIArtHandle* ret);
	/** ConvertAllToNative will traverse AI tree and converts every #kLegacyTextArt type to #kTextFrameArt type.
		foundAnyLegacy is true if there were any Legacy text in the document. */
	AIAPI AIErr (*ConvertAllToNative)(AIBoolean* foundAnyLegacy);
	/** Check if this legacy art is a copy. A copy is the result of executing conversion command to native
		and keeping the legacy with 40% transparency behind the resultant native art.  
		i.e if user click on legacy art for editing and choose "Copy text object".
	*/
	AIAPI AIErr (*IsACopy)( AIArtHandle LegacyArt , AIBoolean *aCopy);

} AILegacyTextConversionSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
