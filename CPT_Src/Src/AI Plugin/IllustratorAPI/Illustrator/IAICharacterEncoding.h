#ifndef _IAICHARACTERENCODING_H_
#define _IAICHARACTERENCODING_H_

#include <string>
#include "AIUnicodeTypes.h"
#include "AICharacterEncoding.h"

namespace ai {

//------------------------------------------------------------------------
// The traits classes define properties of encodings
//
//	unit_type is the basic type used to represent the data e.g char or AIUTF16
//	encoding is a function returning the AI character encoding type
//	codes_2_units determines the maximum number of elements of unit_type
//		needed to represent a sequence of n character codes
//	units_2_codes determines the maximum number of character codes that
//		could be encoded by n units of unit_type
//------------------------------------------------------------------------

// Platform encoding

struct PlatformTraits {
	typedef char unit_type;
	static AICharacterEncoding encoding ()
		{return kAIPlatformCharacterEncoding;}
	static long codes_2_units (long codes)
		{return codes * 2;}
	static long units_2_codes (long units)
		{return units;}
};

// UTF8 encoding

struct UTF8Traits {
	typedef char unit_type;
	static AICharacterEncoding encoding ()
		{return kAIUTF8CharacterEncoding;}
	static long codes_2_units (long codes)
		{return codes * 4;}
	static long units_2_codes (long units)
		{return units;}
};

// UTF16 encoding

struct UTF16Traits {
	typedef AIUTF16Char unit_type;
	static AICharacterEncoding encoding ()
		{return kAIUTF16CharacterEncoding;}
	static long codes_2_units (long codes)
		{return codes * 2;}
	static long units_2_codes (long units)
		{return units;}
};

// Template class for encoded strings

template <class Traits>
class EncodedString {
public:
	// constructs an empty string
	EncodedString ()
	{}
	// copy constructor
	EncodedString (const EncodedString& s) :
		string(s.string)
	{}
	// construct an encoded string from a basic_string of the same encoding
	explicit EncodedString (const std::basic_string<typename Traits::unit_type>& s) :
		string(s)
	{}
	// construct an encoded string from a null terminated sequence of
	// elements of the same encoding
	explicit EncodedString (const typename Traits::unit_type* s) :
		string(s)
	{}
	// construct an encoded string from a buffer containing elements
	// of the same encoding
	EncodedString (const typename Traits::unit_type* s, int length) :
		string(s, length)
	{}

	// assignment operator
	EncodedString& operator= (const EncodedString& s)
	{
		string = s.string;
		return *this;
	}

#if 1
	// for some reason windows can't handle a templated conversion operator,
	// so we define an explicit cast operator instead. really annoying.
	template <class SrcTraits>
	static EncodedString<Traits> cast (const EncodedString<SrcTraits>& src)
	{
		EncodedString<Traits> dst;
			
		long maxsize = Traits::codes_2_units(SrcTraits::units_2_codes(src.string.size()));
		long maxbytes = maxsize * sizeof(Traits::unit_type);
		Traits::unit_type* b = new Traits::unit_type[maxsize];
		
		try
		{
			long bytes;
			long size;
			AIErr result = sAICharacterEncoding->ConvertBuffer(
					src.string.data(), 
					src.string.size() * sizeof(SrcTraits::unit_type), 
					SrcTraits::encoding(),
					b, maxbytes, Traits::encoding(),
					&bytes, FALSE);
			if (result)
				throw ai::Error(result);
			size = bytes / sizeof(Traits::unit_type);
			
			dst = EncodedString<Traits>(b, size);
			delete [] b;
		}
		catch (std::exception&)
		{
			delete [] b;
			throw;
		}
		
		return dst;
	}
#else
	// implicit conversion to another encoding
	template <class DstTraits>
	operator EncodedString<DstTraits> () const
	{
		EncodedString<DstTraits> dst;
		
		long maxsize = DstTraits::codes_2_units(Traits::units_2_codes(string.size()));
		long maxbytes = maxsize * sizeof(DstTraits::unit_type);
		DstTraits::unit_type* b = new DstTraits::unit_type[maxsize];
		
		try
		{
			long bytes;
			long size;
			AIErr result = sAICharacterEncoding->ConvertBuffer(
					string.data(), string.size(), Traits::encoding(),
					b, maxbytes, DstTraits::encoding(),
					&bytes, FALSE);
			if (result)
				throw ai::Error(result);
			size = bytes / sizeof(DstTraits::unit_type);
			
			dst = EncodedString<DstTraits>(b, size);
			delete [] b;
		}
		catch (std::exception&)
		{
			delete [] b;
			throw;
		}
		
		return dst;
	}
#endif
	
	// the encoded data. direct access is allowed.
	std::basic_string<typename Traits::unit_type> string;
};


// types for strings

typedef EncodedString<PlatformTraits> PlatformString;
typedef EncodedString<UTF8Traits> UTF8String;
typedef EncodedString<UTF16Traits> UTF16String;


}

#endif
