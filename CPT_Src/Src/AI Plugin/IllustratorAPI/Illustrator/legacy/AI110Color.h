#ifndef __AI110Color__
#define __AI110Color__

/*
 *        Name:	AI110Color.h
 *   $Revision: 17 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Color Suite.
 *
 * Copyright (c) 1986-2003 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AIColor.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIColor.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

/** Custom colors define named colors. The appearance of the named color is
	defined by a color of one of these types. */
typedef enum {
	/** Solid ink, expressed in four process colors */
	kAI110CustomFourColor = 0,
	/** RGB colors */
	kAI110CustomThreeColor
} AI110CustomColorTag;


/*******************************************************************************
 **
 **	Types
 **
 **/

/** The union of all possible process colors that can be associated with
	a custom color. */
typedef union {
	AIFourColorStyle	f;
	AIThreeColorStyle	rgb;
	AIGrayColorStyle	gray;
} AI110CustomColorUnion;

/** Custom colors define named colors. These colors may be actual inks or simply
	names for process colors depending on whether the custom color is a spot
	or global process color. In either case the custom color has an associated
	process color. The type of process color is identified by this enumeration. */
typedef struct {
	AI110CustomColorTag		kind;
	AI110CustomColorUnion	c;
	AICustomColorFlags		flag;
	unsigned char			name[32];
} AI110CustomColor;



#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
