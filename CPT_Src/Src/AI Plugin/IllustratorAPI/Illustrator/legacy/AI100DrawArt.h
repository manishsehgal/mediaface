#ifndef __AI100DrawArt__
#define __AI100DrawArt__

/*
 *        Name:	AI100DrawArt.h
 *     Purpose:	Adobe Illustrator 10.0 Draw Art Suite.
 *
 * Copyright (c) 1986-2002 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AIDrawArt.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAI100DrawArtSuite			kAIDrawArtSuite
#define kAIDrawArtSuiteVersion5		AIAPI_VERSION(5)
#define kAI100DrawArtSuiteVersion	kAIDrawArtSuiteVersion5


/*******************************************************************************
 **
 **	Types
 **
 **/

typedef struct AI100DrawArtData {
	short version;
	
	AIDrawArtFlags flags;
	AIDrawArtOutputType type;
	AIRealPoint origin;
	AIRealMatrix matrix;
	AIArtHandle art;
	AIRealRect destClipRect;
	AIBoolean eraseDestClipRect; // erase destClipRect before arts are drawn.
	AIArtHandle interruptedArt;

	AIReal greekThreshold;
	AIExtendedRGBColorRec selectionColor;
	
	union {
		AIDrawArtGWorld gWorld;
		AIDrawArtAGMPort port;
	} output;
} AI100DrawArtData;


typedef struct AI100DrawColorData {
	AIDrawArtOutputType type;

	AIDrawColorStyle style;
	AIDrawColorOptions options;
	AIColor color;
	AIRealRect rect;
	AIReal width;							// when style is frame
	
	union {
		AIDrawArtGWorld gWorld;
		AIDrawArtAGMPort port;
	} output;
} AI100DrawColorData;


/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	AIAPI AIErr (*DrawArt)( AI100DrawArtData *data );
	AIAPI AIErr (*DrawColorSwatch)( AI100DrawColorData *data );
	AIAPI AIErr (*DrawHilite)( AIArtHandle art, AIRGBColor* color );
	AIAPI AIErr (*DrawThumbnail) ( AIArtHandle art, void* port, AIRealRect* dstrect );

} AI100DrawArtSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
