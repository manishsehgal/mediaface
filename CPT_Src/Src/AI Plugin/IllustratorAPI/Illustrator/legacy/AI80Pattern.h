#ifndef __AI80Pattern__
#define __AI80Pattern__

/*
 *        Name:	AI80Pattern.h
 *   $Revision: 10 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Pattern Fill Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/


#ifndef __AIPattern__
#include "AIPattern.h"
#endif

#include "AI110PathStyle.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 ** Constants
 **
 **/
#define kAI80PatternSuite			kAIPatternSuite
#define kAIPatternSuiteVersion5		AIAPI_VERSION(5)
#define kAIPatternSuiteVersion6		AIAPI_VERSION(6)
#define kAI80PatternSuiteVersion	kAIPatternSuiteVersion6





/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	AIAPI AIErr (*NewPattern) ( AIPatternHandle *newPattern );
	AIAPI AIErr (*DeletePattern) ( AIPatternHandle pattern );
	AIAPI AIErr (*GetPattern) ( AIPatternHandle pattern );
	AIAPI AIErr (*SetPattern) ( AIPatternHandle pattern );
	AIAPI AIErr (*CountPatterns) ( long *count );
	AIAPI AIErr (*GetNthPattern) ( long n, AIPatternHandle *pattern );
	AIAPI AIErr (*GetPatternArt) ( AIPatternHandle pattern, AIArtHandle *art );
	AIAPI AIErr (*SetPatternArt) ( AIPatternHandle pattern, AIArtHandle art );
	AIAPI AIErr (*GetPatternName) ( AIPatternHandle pattern, unsigned char *name );
	AIAPI AIErr (*SetPatternName) ( AIPatternHandle pattern, unsigned char *name );
	AIAPI AIErr (*GetPatternByName) ( unsigned char *name, AIPatternHandle *pattern );
	AIAPI AIErr (*IteratePattern) ( AIArtHandle art, AI110PathStyle *style, AIPatternProcPtr proc );
	AIAPI AIErr (*NewPatternName) ( char *name, int maxlen ); //name is modified in place
	AIAPI AIErr (*GetPatternDisplayName) ( char *name ); //name is modified in place
	AIAPI AIErr (*GetPatternTileBounds) ( AIPatternHandle pattern, AIRealRectPtr bounds );

} AI80PatternSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
