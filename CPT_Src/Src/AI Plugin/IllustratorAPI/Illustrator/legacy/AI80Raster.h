#ifndef __AI80Raster__
#define __AI80Raster__

/*
 *        Name:	AI80Raster.h
 *   $Revision: 24 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 8.0 Raster Object Suite.
 *
 * Copyright (c) 1986-1998 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AI110Raster.h"


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAI80RasterSuite		kAIRasterSuite
#define kAIRasterSuiteVersion4	AIAPI_VERSION(4)
#define kAI80RasterSuiteVersion	kAIRasterSuiteVersion4


/*******************************************************************************
 **
 **	Types
 **
 **/

typedef struct _t_AI80RasterRecord	{
	uint16			flags;
	AIRect			bounds;
	int32			byteWidth;
	int16			colorSpace; // look at the colorSpace flags...
	int16			bitsPerPixel;
	int16			originalColorSpace; // if -1 the raster is unconverted, else the color space before conversion
} AI80RasterRecord;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/* Suite supporting embedded images only */
typedef struct AI80RasterSuite {

	AIAPI AIErr (*GetRasterInfo) ( AIArtHandle raster, AI80RasterRecord *info );
	AIAPI AIErr (*SetRasterInfo) ( AIArtHandle raster, AI80RasterRecord *info );
	AIAPI AIErr (*GetRasterFileSpecification) ( AIArtHandle raster, SPPlatformFileSpecification *file );
	AIAPI AIErr (*SetRasterFileSpecification) ( AIArtHandle raster, SPPlatformFileSpecification *file );
	AIAPI AIErr (*GetRasterMatrix) ( AIArtHandle raster, AIRealMatrix *matrix );
	AIAPI AIErr (*SetRasterMatrix) ( AIArtHandle raster, AIRealMatrix *matrix );
	AIAPI AIErr (*GetRasterBoundingBox) ( AIArtHandle raster, AIRealRect *bbox );
	AIAPI AIErr (*SetRasterBoundingBox) ( AIArtHandle raster, AIRealRect *bbox );
	AIAPI AIErr (*GetRasterTile) ( AIArtHandle raster, AISlice *artSlice, AITile *workTile, AISlice *workSlice );
	AIAPI AIErr (*SetRasterTile) ( AIArtHandle raster, AISlice *artSlice, AITile *workTile, AISlice *workSlice );

	AIAPI AIErr (*GetRasterLink) ( AIArtHandle raster, AI110RasterLink *link );
	AIAPI AIErr (*SetRasterLink) ( AIArtHandle raster, AI110RasterLink *link );
	AIAPI AIErr (*ResolveRasterLink) ( AIArtHandle raster, long flags );

	AIAPI AIErr (*GetRasterFileInfoFromArt) ( AIArtHandle raster, SPPlatformFileInfo *pSPFileInfo );
	AIAPI AIErr (*GetRasterFileInfoFromFile) ( AIArtHandle raster, SPPlatformFileInfo *pSPFileInfo );
	AIAPI AIErr (*GetRasterFilePathFromArt) ( AIArtHandle raster, char *pszPath, int iMaxLen );

} AI80RasterSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
