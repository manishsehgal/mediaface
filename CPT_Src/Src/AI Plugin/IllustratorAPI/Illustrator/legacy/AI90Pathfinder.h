/*
 *        Name:	AI90Pathfinder.h
 *     Purpose:	Adobe Illustrator 9.0 Pathfinder Suite.
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifndef __AI90Pathfinder__
#define __AI90Pathfinder__

/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AIPathfinder.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAI90PathfinderSuite		kAIPathfinderSuite
#define kAIPathfinderSuiteVersion3	AIAPI_VERSION(3)
#define kAI90PathfinderSuiteVersion	kAIPathfinderSuiteVersion3
#define kAI90PathfinderVersion		kAI90PathfinderSuiteVersion


/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	AIAPI AIErr (*DoUniteEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoIntersectEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoExcludeEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoBackMinusFrontEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoFrontMinusBackEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoDivideEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoOutlineEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoTrimEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoMergeEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoCropEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoHardEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoSoftEffect) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*DoTrapEffect) ( AIPathfinderData *data, AIFilterMessage *message );

	AIAPI AIErr (*GetHardEffectParameters) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*GetSoftEffectParameters) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*GetTrapEffectParameters) ( AIPathfinderData *data, AIFilterMessage *message );
	AIAPI AIErr (*GetGlobalOptions) ( AIPathfinderData *data, AIFilterMessage *message );

} AI90PathfinderSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
