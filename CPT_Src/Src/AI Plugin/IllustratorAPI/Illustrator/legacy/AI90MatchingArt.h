#ifndef __AI90MatchingArt__
#define __AI90MatchingArt__

/*
 *        Name:	AI90MatchingArt.h
 *   $Revision: 4 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 9.0 Matching Art Suite.
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AIMatchingArt.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAI90MatchingArtSuite		kAIMatchingArtSuite
#define kAIMatchingArtSuiteVersion3	AIAPI_VERSION(3)
#define kAI90MatchingArtVersion		kAIMatchingArtSuiteVersion3


/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	/* Get the art matching the given criteria and on the current layer list. */
	AIAPI AIErr (*GetSelectedArt) ( AIArtHandle ***matches, long *numMatches );
	AIAPI AIErr (*GetMatchingArt) ( AIMatchingArtSpec *specs, short numSpecs,
			AIArtHandle ***matches, long *numMatches );
	
	//new to Illustrator 9.0
	/* Get the art matching the given criteria and on the given layer list. */
	AIAPI AIErr (*GetSelectedArtFromLayerList) ( AILayerList list, AIArtHandle ***matches,
			long *numMatches );
	AIAPI AIErr (*GetMatchingArtFromLayerList) ( AILayerList list, AIMatchingArtSpec *specs,
			short numSpecs, AIArtHandle ***matches, long *numMatches );

} AI90MatchingArtSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
