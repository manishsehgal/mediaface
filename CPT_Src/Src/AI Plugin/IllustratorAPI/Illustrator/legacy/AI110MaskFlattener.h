#ifndef __AI110MaskFlattener__
#define __AI110MaskFlattener__

/*
 *        Name:	AI110MaskFlattener.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 11.0 Mask Flattener Suite.
 *
 * Copyright (c) 1986-2005 Adobe Systems Incorporated, All Rights Reserved.
 *
 */
/*******************************************************************************
 **
 **	Imports
 **
 **/
#ifndef __AIMaskFlattener__
#include "AIMaskFlattener.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AI110MaskFlattener.h */

/*******************************************************************************
 **
 **	Suite name and version
 **
 **/

#define kAIMaskFlattenerSuite					"AI Mask Flattener Suite"
#define kAIMaskFlattenerSuiteVersion2			AIAPI_VERSION(2)
#define kAI110MaskFlattenerSuiteVersion			kAIMaskFlattenerSuiteVersion2

/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	The transparency flattening APIs provide facilities for flattening
	transparent artwork into an equivalent collection of opaque artwork.
 */
typedef struct {

	AIAPI AIErr (*FlattenArt)(AIArtSet artSet, AIFlatteningOptions *options, short paintOrder, AIArtHandle prep);
	AIAPI AIErr (*CreateTemporaryFlatteningLayer)(AILayerHandle *layer);
	AIAPI AIErr (*FlattenDocumentToLayer)(AILayerHandle layer);
	AIAPI AIErr (*RemoveTemporaryFlatteningLayer)();
	AIAPI AIErr (*IsTemporaryFlatteningLayer)(AILayerHandle layer, AIBoolean *flattening);
	AIAPI AIErr (*ReportError)(AIErr error);
	AIAPI AIErr (*CollectDocumentFlatteningInfo)(AIFlatteningInfoReceiver receiver, void* receiverdata);
	
} AI110MaskFlattenerSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
