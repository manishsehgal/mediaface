#ifndef __AI110HitTest__
#define __AI110HitTest__

/*
*        Name:	AIHitTest.h
*   $Revision: 3 $
*      Author:	 
*        Date:	   
*     Purpose:	Adobe Illustrator 11.0 Hit Testing Suite.
*
* Copyright (c) 1986-2003 Adobe Systems Incorporated, All Rights Reserved.
*
*/


/*******************************************************************************
**
**	Imports
**
**/

#include "AIHitTest.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AI110HitTest.h */

/*******************************************************************************
**
** Constants
**
**/

#define kAIHitTestSuiteVersion11	AIAPI_VERSION(2)
#define kAI110HitTestSuiteVersion	kAIHitTestSuiteVersion11
#define kAI110HitTestSuite			kAIHitTestSuite


	/*******************************************************************************
**
**	Suite
**
**/


typedef struct {

	AIAPI AIErr (*HitTest) ( AIArtHandle art, AIRealPoint *point, long option, AIHitRef *hit );
	AIAPI AIErr (*HitTestEx) ( AIArtHandle art, AIRealPoint *point, AIReal tolerance, long option, AIHitRef *hit );
	AIAPI long (*AddRef) ( AIHitRef hit );
	AIAPI long (*Release) ( AIHitRef hit );
	AIAPI AIErr (*GetHitData) ( AIHitRef hit, AIToolHitData *toolHit );
	AIAPI AIBoolean (*IsHit) ( AIHitRef hit );
	AIAPI long (*GetType) ( AIHitRef hit );
	AIAPI AIArtHandle (*GetArt) ( AIHitRef hit );
	AIAPI AIArtHandle (*GetGroup) ( AIHitRef hit );
	AIAPI AIRealPoint (*GetPoint) ( AIHitRef hit );
	AIAPI short (*GetPathSegment) ( AIHitRef hit );
	AIAPI AIReal (*GetPathParameter) ( AIHitRef hit );

} AI110HitTestSuite;



#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif