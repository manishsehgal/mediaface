#ifndef __AI100Folders__
#define __AI100Folders__

/*
 *        Name:	AI100Folders.h
 *     Purpose:	Adobe Illustrator 10.0 Folders Suite.
 *
 * Copyright (c) 1986-2002 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AIFolders.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/*******************************************************************************
 **
 **	Suite name and version
 **
 **/

#define kAI100FoldersSuite				kAIFoldersSuite
#define kAIFoldersSuiteVersion1			AIAPI_VERSION(1)
#define kAI100FoldersSuiteVersion		kAIFoldersSuiteVersion1


 /*******************************************************************************
 **
 **	Suite
 **
 **/


typedef struct {

	// - version 1 api - 
	AIAPI AIErr (*FindFolder)(AIFolderType type, AIBoolean createFolder, SPPlatformFileSpecification *folder);
	AIAPI AIErr (*GetFolderName)(AIFolderType type, char *name); // kMaxPathLength size buffer assumed, folder name only (not path).

} AI100FoldersSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
