#ifndef __AI80PaintStyle__
#define __AI80PaintStyle__

/*
 *        Name:	AI80PaintStyle.h
 *   $Revision: 3 $
 *     Purpose:	AI Paint Style Suite.
 *
 * Copyright (c) 1986-2000 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifndef __AIPaintStyle__
#include "AIPaintStyle.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

// AIPaintStyle Suite
#define kAI80PaintStyleSuiteVersion		AIAPI_VERSION(4)

typedef struct 
{	
	void ASAPI (*SetStrokeActive)();
	ASBoolean ASAPI (*IsStrokeActive)();
	void ASAPI (*SetFillActive)();
	ASBoolean ASAPI (*IsFillActive)();
	void ASAPI (*SetDefaultFillStroke)();
	int ASAPI (*GetActiveGradientStopIndex)();
	ASErr ASAPI (*GetCurrentGradient)(AIColor *color);
	ASErr ASAPI (*GetCurrentColor)(AIColor *color);
	ASErr ASAPI (*ShowGradientDialog)();
	ASErr ASAPI (*ShowColorDialog)();
	ASErr ASAPI (*SetActiveColor)(AIColor *color, ASBoolean useGradientStop);
	ASErr ASAPI (*BeginActiveColor)();
	ASErr ASAPI (*EndActiveColor)();
	ASBoolean ASAPI (*IsCurrentColorStroke)();
	ASErr ASAPI (*SetAIColor)(AIColor *color, AIColorMap *colorMap, ASBoolean isStroke);
	ASErr ASAPI (*SetCurrentGradient)(AIColor *color);
	ASErr ASAPI (*SetCurrentColor)(AIColor *color);
	ASErr ASAPI (*ToggleColorDialog)();
	//version 3, new for 7.0.1
	ASErr ASAPI (*SetColorEditorAIColor)(ADMItemRef colorEditor, AIColor *color, AIColorMap *colorMap);
	ASErr ASAPI (*GetColorEditorAIColor)(ADMItemRef colorEditor, AIColor *color, AIColorMap *colorMap);
	ASErr ASAPI (*SetColorEditorColorSpace)(ADMItemRef colorEditor, ColorSpace space);
	ASErr ASAPI (*GetColorEditorColorSpace)(ADMItemRef colorEditor, ColorSpace *space);
	void ASAPI (*SwapFillStroke)();
} AI80PaintStyleSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif