#ifndef __AI80MatchingArt__
#define __AI80MatchingArt__

/*
 *        Name:	AIMatchingArt.h
 *   $Revision: 4 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Matching Art Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AIMatchingArt__
#include "AIMatchingArt.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAI80MatchingArtSuite		"AI Matching Art Suite"
#define kAIMatchingArtSuiteVersion2	AIAPI_VERSION(2)
#define kAI80MatchingArtVersion		kAIMatchingArtSuiteVersion2


/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	AIAPI AIErr (*GetSelectedArt) ( AIArtHandle ***matches, long *numMatches );  // how 'bout AIArtObject *****matches?
	AIAPI AIErr (*GetMatchingArt) ( AIMatchingArtSpec *specs, short numSpecs, AIArtHandle ***matches, long *numMatches );

} AI80MatchingArtSuite;

#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
