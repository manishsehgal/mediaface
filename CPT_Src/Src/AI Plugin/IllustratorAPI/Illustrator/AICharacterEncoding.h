#ifndef __AICharacterEncoding__
#define __AICharacterEncoding__

/*
 *        Name:	AICharacterEncoding.h
 *		$Id $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 11.0 Character Encoding Suite.
 *
 * Copyright (c) 2003 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AICharacterEncoding.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAICharacterEncodingSuite					"AI Character Encoding Suite"
#define kAICharacterEncodingSuiteVersion3			AIAPI_VERSION(3)
#define kAICharacterEncodingSuiteVersion			kAICharacterEncodingSuiteVersion3
#define kAICharacterEncodingVersion					kAICharacterEncodingSuiteVersion


/** Constants defining the various character encodings supported by this suite.
	The character sets for the specific language encodings (eg. Roman, Japanese, etc.)
	are dictated by the mac and windows platforms' code pages and so may differ across
	platforms. The character sets for the specific East Asian encodings (GB, JISX0213, etc.)
	are dictated by specific standards. */
typedef enum {

	/** The platform character encoding represents the character set currently 
		supported by the system. It is typically the character set that the sytem
		booted up with. */
	kAIPlatformCharacterEncoding = 0,

	kAIUTF8CharacterEncoding,
	kAIUTF16CharacterEncoding,  // used to be kAIUCS2CharacterEncoding
	
	
	// The encodings below are new in Illustrator 11.0
	
	kAIRomanCharacterEncoding = 0x10,
	kAIJapaneseCharacterEncoding, 			// encoded using Shift-JIS
	kAITraditionalChineseCharacterEncoding,
	kAISimplifiedChineseCharacterEncoding,
	kAIKoreanCharacterEncoding,

	kAIArabicCharacterEncoding,
	kAIHebrewCharacterEncoding,
	kAIGreekCharacterEncoding,
	kAICyrillicCharacterEncoding,
	kAICentralEuroRomanCharacterEncoding,	// known in the past as Eastern European Roman
	kAITurkishCharacterEncoding,
	kAIBalticCharacterEncoding,
	
	// Platform-independent encodings - East Asian character standards 
	kAIJapaneseX0213CharacterEncoding = 0x00000100,	// JISX0213:2000 + JISX0208:1997, using Shift-JIS
	kAIChineseGB18030CharacterEncoding,				// GB 18030-2000
	kAIChineseHKSCSCharacterEncoding,				// HKSCS
	
	kAIDummyCharacterEncoding = 0xFFFFFFFF

} AICharacterEncoding;


/** Constants defining the various ways to encode characters 
	in the JIS character set. */
typedef enum {

	kAISJISEncoding,	// Shift-JIS
	kAIJISEncoding,		// JISX0208:1997
	kAIKutenEncoding,	// Kuten
	
	kAIDummyJapaneseEncoding = 0xFFFFFFFF
	
} AIJapaneseEncoding;

/* Error codes */

/** This error is returned from ConvertBuffer() when the conversion requires
	more bytes than the dstBytes parameter passed in. */
#define kDstBufferTooShortErr 'Shrt'

/** This error is returned from ConvertBuffer() when the srcBuffer contains 
	bytes that are invalid in the given srcEncoding. */
#define kSrcHasInvalidBytesErr 'InvB'

/** This error is returned from ConvertBuffer() when the srcBuffer contains 
	bytes that cannot be mapped to the destination encodings. */
#define kSrcCantMapBytesErr         '!Map'

/** This error is returned from ConvertBuffer() when the srcBuffer contains 
	bytes that cannot be mapped exactly. */
#define kSrcCantMapBytesExactlyErr  '~Map'

/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	The character encoding suite provides facilities for translating between different
	encodings of textual data. See also ai::EncodedString which provides a C++ class
	for translating between platform encoding, UTF8 and UTF16.
*/

typedef struct AICharacterEncodingSuite {

	/** Converts the contents of the source buffer from the source encoding to
		the destination encoding. srcBuffer is assumed to contain srcBytes bytes
		of data. dstBuffer is assumed to be large enough to contain dstBytes bytes
		and dstLength returns the actual number of bytes of converted data.
		Unicode 3 values will always be used as the intermediate representation
		during the translation between the source and the destination encodings.
		Byte order of UTF16 unicode values are dictated by the current platform
		(big-endian on mac, little-endian on windows). If some characters do not
		map exactly, a loose or fallback mapping is used. If some characters can not
		be mapped, the source characters are dropped from the destination buffer. 
        The errIfCantMap flag can be set to true if one want error codes
        returned in these cases. */
	AIAPI AIErr (*ConvertBuffer) (
			const void* srcBuffer, long srcBytes, AICharacterEncoding srcEncoding,
			void* dstBuffer, long dstBytes, AICharacterEncoding dstEncoding,
			long* dstLength, AIBoolean errIfCantMap);

	/* New in Illustrator 11.0 */

	/** Converts the given Japanese character (represented as an unsigned short) from
		one Japanese encoding to another. This routine will return a #kBadParameterErr
		if the given character is not within a valid range for the given srcEncoding.
		Note that in particular, when converting from Shift-JIS to another encoding,
		passing in single byte Shift-JIS characters (used to represent ASCII and
		half-width katakana characters) will result in #kBadParameterErr, as this API
		does not support converting such characters to JIS X 0208 or Kuten. */
	AIAPI AIErr (*ConvertJapaneseChar) (
			AIJapaneseEncoding srcEncoding, AIJapaneseEncoding dstEncoding, unsigned short* ch );

	// The following two functions provide limited cross-platform support for classifying
	// unicode characters.
	
	/** Returns true if the given unicode (UTF16) character is categorized as a punctuation
		character. Only supports characters in the BMP (Basic Multilingual Plane). */
	AIAPI bool (*IsPunct)(ASUnicode inChar);

	/** Returns true if the given unicode (UTF16) character is categorized as a spacing
		character. Only supports characters in the BMP (Basic Multilingual Plane). */
	AIAPI bool (*IsSpace)(ASUnicode inChar);

} AICharacterEncodingSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
