#ifndef __AITimer__
#define __AITimer__

/*
 *        Name:	AITimer.h
 *   $Revision: 5 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Timer Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIPlugin__
#include "AIPlugin.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AITimer.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAITimerSuite			"AI Timer Suite"
#define kAITimerSuiteVersion	AIAPI_VERSION(3)
#define kAITimerVersion			kAITimerSuiteVersion


/** @ingroup Callers
	This is the timer caller. */
#define kCallerAITimer 			"AI Timer"

/** @ingroup Selectors
	These are the timer selectors. */
#define kSelectorAIGoTimer		"AI Go"

#define kTicksPerSecond			60


/*******************************************************************************
 **
 ** Types
 **
 **/

/** This is a reference to a timer. It is never dereferenced. */
typedef struct _t_AITimerOpaque *AITimerHandle;

/** The contents of a timer message. The SPMessageData is that which is received
	by all calls to a plug-in. The AITimerHandle is a reference to the timer
	plug-in receiving the message. If more than one timer is installed it can be
	used to determine the instance that should handle the message.*/
typedef struct {
	SPMessageData d;
	AITimerHandle timer;
} AITimerMessage;


/*******************************************************************************
 **
 **	Suite Record
 **
 **/

/** Timers are a plug-in type used to have the Illustrator application notify a
	plug-in that a specified period of time has passed. The functions in this suite
	are used to add timer plug-in instances to the application, turn them on and
	off, and enumerate and access the timer plug-ins installed.

	Timers are most likely to be used in conjunction with other plug-in types, a
	menu or window for instance, as a means of learning when an update is
	needed.

	When a timer "fires" a message is sent to the plug-in's main entry point with
	caller #kCallerAITimer and selector #kSelectorAIGoTimer.

	Timer periods are specified in ticks. There are #kTicksPerSecond ticks in a
	second.
 */
typedef struct {

	/** Use this function at startup to add a timer plug-in to Illustrator. As with
		adding all plug-in type instances, the AddTimer call gives Illustrator a reference
		to the plug-in itself in self and a C style string, name, used for internal
		identification.
		
		The timer period is a long value indicating the number of ticks that should
		pass before the timer is notified (for seconds, use number of seconds multiplied
		by #kTicksPerSecond).
		
		AddTimer returns a reference to a successfully installed timer instance or an
		appropriate error code. The AddTimer call below requests notification once
		a second.

@code
	AIErr error;
	error = sTimer->AddTimer( message->d.self, "Time for Timer", 60, &g->secondTimer );
@endcode

		The returned timer value should be stored if more than one timer is
		installed. A timer reference will be passed back when the timer is triggered
		and the plug-in can compare this value to the stored references to determine
		which action to take.
	*/
	AIAPI AIErr (*AddTimer) ( SPPluginRef self, char *name, long period,
				AITimerHandle *timer );

	/** Returns a pointer to the name of the timer. This is the name value originally
		passed to the AddTimer call. It should not be modified. */
	AIAPI AIErr (*GetTimerName) ( AITimerHandle timer, char **name );
	/** Returns whether or not the timer is active (receiving periodic notifications)
		or not. */
	AIAPI AIErr (*GetTimerActive) ( AITimerHandle timer, AIBoolean *active );
	/** Use this to set whether a time is receiving period notification.
		Note: Timers must be made inactive at #kAISelectorShutdownPlugin. */
	AIAPI AIErr (*SetTimerActive) ( AITimerHandle timer, AIBoolean active );
	/** This returns the interval at which the timer is notified. The period is
		returned in ticks. */
	AIAPI AIErr (*GetTimerPeriod) ( AITimerHandle timer, long *period );
	/** Sets the interval at which the timer is notified. The period is given
		in ticks. */
	AIAPI AIErr (*SetTimerPeriod) ( AITimerHandle timer, long period );
	/** This call will return a reference to the plug-in that installed the timer. The
		AIPluginHandle can then be used with the Plugin Suite functions. */
	AIAPI AIErr (*GetTimerPlugin) ( AITimerHandle timer, SPPluginRef *plugin );

	/** The number of timers available can be obtained using the count call.
		Use this with GetNthTimer() to iterate through the installed timers. */
	AIAPI AIErr (*CountTimers) ( long *count );
	/** This call returns the AITimerHandle at the specified index. Use this
		with CountTimers() to iterate through the installed timers. */
	AIAPI AIErr (*GetNthTimer) ( long n, AITimerHandle *timer );

} AITimerSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
