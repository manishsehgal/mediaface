#ifndef _AINATIVEACTION_H_
#define _AINATIVEACTION_H_

/*
 *        Name:	AINativeAction.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Native File Format Action Parameters.
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifndef __AITypes__
#include "AITypes.h"
#endif

// -----------------------------------------------------------------------------
//	Types.
// -----------------------------------------------------------------------------

typedef enum _AIFileFormatScriptCompatibility {
	kAIFileFormatScriptCompatibleEnglish = 0,
	kAIFileFormatScriptCompatibleCJK
} AIFileFormatScriptCompatibility;

typedef enum _AIFileFormatFlattening {
	kAIFileFormatFlatteningPreservePaths = 0,
	kAIFileFormatFlatteningPreserveAppearance = 1,
	kAIFileFormatFlatteningPreserveAppearanceAndOverprints = 2
} AIFileFormatFlattening;

typedef enum _AIEPSFilePreview {
	kAIEPSFilePreviewNone = 1,
	kAIEPSFilePreviewBWPICT,
	kAIEPSFilePreviewColorPICT,
	kAIEPSFilePreviewBWTIFF,
	kAIEPSFilePreviewColorTIFF,
	kAIEPSFilePreviewSimpleColor
} AIEPSFilePreview;

typedef enum _AIEPSOverprints {
	kAIEPSFileOverprints_Preserve = 1,
	kAIEPSFileOverprints_Discard,
	kAIEPSFileOverprints_Simulate
} AIEPSOverprints;


// -----------------------------------------------------------------------------
// Action: kAISaveDocumentAsAction
// Purpose: These are additional parameters for saving as a native file.
// Parameters:
//	The following parameters are standard to the save as action 
//	- kAISaveDocumentAsNameKey, string: is the file name to save as. if no name
//			is specified a dialog will be presented.
//	- kAISaveDocumentAsFormatKey, string: is the format to save as. use
//			kAINativeFileFormat to save as a native file.
//	- kAISaveDocumentAsGetParamsKey, bool: controls whether the file format
//			presents its options dialog. if true the options dialog is presented.
//			if omitted no dialog will be presented.
//	- kAINativeVersionKey, integer: the file format version to use when saving.
//			see the AIVersion enumeration in AITypes.h for applicable values.
//	- kAINativeScriptKey, integer: language script compatibility of the saved
//			file. see AIFileFormatScriptCompatibility above.
//	- kAINativeIncludeImagesKey, integer: whether to embed any linked images
//			when saving. non-zero causes images to be embedded. zero does not
//			change the embedded/linked status of images.
//	- kAINativeEmbedAllFontsKey, integer: whether to embed all fonts. non-zero
//			causes all fonts to be embedded.
//	- kAINativeSubsetFontsBelowKey, bool: true => fonts using less than the
//			percentage of characters specified by the kAINativeSubsetFontsRatioKey
//			will be subsetted.
//	- kAINativeSubsetFontsRatioKey, integer: see kAINativeSubsetFontsBelowKey.
//			the value is a percentage between 0 and 100.
//	- kAINativeEmbedProfileKey, integer: a non-zero value causes the document
//			color profile to embedded in the file.
//	- kAINativeFlatteningOptionKey, integer: whether or not to flatten when
//			saving a document with transparency to a version that does not
//			support transparency. see AIFileFormatFlattening above.
//	- kAINativeCompressionKey, bool: true => the file contents are compressed
//			false => they are not.
//	- kAINativePDFCompatibilityKey, bool: true => a PDF rendition of the
//			document will be saved together with native roundtrip information.
//			false => only the roundtrip information is saved.
// -----------------------------------------------------------------------------

// file format and extensions for saving as a native file.
#define kAINativeFileFormat								"Adobe Illustrator Any Format Writer"
#define kAINativeFileFormatExtension					"ai"


const ActionParamKeyID kAINativeVersionKey				= 'crtr';
const ActionParamKeyID kAINativeScriptKey				= 'ext.';
const ActionParamKeyID kAINativeIncludeImagesKey		= 'incl';
const ActionParamKeyID kAINativeEmbedAllFontsKey		= 'embd';
const ActionParamKeyID kAINativeSubsetFontsBelowKey		= 'sbst';
const ActionParamKeyID kAINativeSubsetFontsRatioKey		= 'rato';
const ActionParamKeyID kAINativeEmbedProfileKey			= 'prfl';
const ActionParamKeyID kAINativeFlatteningOptionKey		= 'flat';
const ActionParamKeyID kAINativeCompressionKey			= 'cmpr';
const ActionParamKeyID kAINativePDFCompatibilityKey		= 'pdf ';


// -----------------------------------------------------------------------------
// Action: kAISaveDocumentAsAction
// Purpose: These are additional parameters for saving as an EPS file.
// Parameters:
//	The following parameters are standard to the save as action 
//	- kAISaveDocumentAsNameKey, string: is the file name to save as. if no name
//			is specified a dialog will be presented.
//	- kAISaveDocumentAsFormatKey, string: is the format to save as. use
//			kAIEPSFileFormat to save as a native file.
//	- kAISaveDocumentAsGetParamsKey, bool: controls whether the file format
//			presents its options dialog. if true the options dialog is presented.
//			if omitted no dialog will be presented.
//	- kAIEPSFormatKey, integer: the type of preview to include with the EPS
//			see AIEPSFilePreview above.
//	- kAIEPSVersionKey, integer: the file format version to use when saving.
//			see the AIVersion enumeration in AITypes.h for applicable values.
//	- kAIEPSScriptKey, integer: language script compatibility of the saved
//			file. see AIFileFormatScriptCompatibility above.
//	- kAIEPSIncludePlacedKey, bool: whether to embed any linked images and
//			EPS files when saving. true causes images to be embedded. false
//			does not change the embedded/linked status of images.
//	- kAIEPSIncludeThumbnailKey, bool: true => include a thumbnail image of
//			the artwork that is displayed in the file open dialog on Windows.
//			false => the thumbnail is not included.
//	- kAIEPSImageFormatKey, integer: the format for writing raster image data
//			in the EPS. 0 => ASCII hex, 1 => Binary. Only applies when the
//			version is AI8 or earlier.
//	- kAIEPSCMYKPostScriptKey, bool: true => any RGB values are converted to
//			CMYK value in the PostScript using the current color settings.
//			false => RGB values are sent to the printer unconverted.
//	- kAIEPSIncludeCJKFontsKey, bool: true => CJK fonts are included in the
//			PostScript stream. false => CJK fonts will not be included, they
//			must be present on the printer to avoid substitution.
//	- kAIEPSPostScriptLevelKey, integer: PostScript level with which the
//			EPS must be compatible. 1, 2 or 3.
//	- kAIEPSFlatteningOptionKey, integer: whether or not to flatten when
//			saving a document with transparency to a version that does not
//			support transparency. see AIFileFormatFlattening above.
//	- kAIEPSUseDefaultScreensKey, bool: whether Illustrator should include
//			its own halftone screens in the PostScript generated. Only
//			applicable to AI8 and earlier.
// -----------------------------------------------------------------------------

// file format and extensions for saving as a native file.
#define kAIEPSFileFormat								"Adobe Illustrator EPSF"
#define kAIEPSFileFormatExtension						"eps"


const ActionParamKeyID kAIEPSFormatKey					= 'wfmt';
const ActionParamKeyID kAIEPSVersionKey					= 'crtr';
const ActionParamKeyID kAIEPSScriptKey					= 'ext.';
const ActionParamKeyID kAIEPSIncludePlacedKey			= 'iplc';
const ActionParamKeyID kAIEPSIncludeThumbnailKey		= 'ithm';
const ActionParamKeyID kAIEPSCompatibleGradientPrintKey	= 'cgpt';
const ActionParamKeyID kAIEPSImageFormatKey				= 'ifmt';
const ActionParamKeyID kAIEPSCMYKPostScriptKey			= 'cmyk';
const ActionParamKeyID kAIEPSIncludeCJKFontsKey			= 'icjk';
const ActionParamKeyID kAIEPSPostScriptLevelKey			= 'pslv';
const ActionParamKeyID kAIEPSFlatteningOptionKey		= 'flat';
const ActionParamKeyID kAIEPSUseDefaultScreensKey		= 'dfsc';
const ActionParamKeyID kAIEPSOverprintsKey				= 'eopt';


#endif
