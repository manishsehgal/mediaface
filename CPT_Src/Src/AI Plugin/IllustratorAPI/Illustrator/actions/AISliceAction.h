#ifndef _AISLICEACTION_H_
#define _AISLICEACTION_H_

/*
 *        Name:	AISliceAction.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Actions for slicing.
 *
 * Copyright 2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifndef __AIActionManager_h__
#include "AIActionManager.h"
#endif


// -----------------------------------------------------------------------------
// Action: all slice actions have this parameter
// Purpose: defines the action to take
// Parameters: 
//		kAISliceActionType, integer
// -----------------------------------------------------------------------------
const ActionParamKeyID kAISliceActionType					= 'ATYP'; // integer

// for the type parameter
enum
{
	kAINoActionType = 0,
	kAIMakeSliceActionType = 1,					// unimplemented
	kAIReleaseSliceActionType,					// unimplemented
	kAICreateSliceFromGuidesActionType,			// unimplemented
	kAICreateSliceFromSelectionActionType,		// unimplemented
	kAICreateSliceFromTextActionType,			// unimplemented
	kAICreateSliceActionType,			
	kAISelectSliceActionType,					// unimplemented
	kAIDuplicateSliceActionType,				// unimplemented
	kAICombineSliceActionType,					// unimplemented
	kAIDeleteAllSliceActionType,			
	kAIDivideSliceActionType,					// unimplemented
	kAIOptionsSliceActionType,					// unimplemented
	kAIClipToArtboardSliceActionType,
	kAILockSliceActionType						// unimplemented
};



// -----------------------------------------------------------------------------
// Action: kAICreateSliceAction
// Purpose: makes a slice at the specified coordinates
// Parameters: 
//  - kAISliceActionBoundsLeftKey, integer
//  - kAISliceActionBoundsBottomKey, integer
//  - kAISliceActionBoundsRightKey, integer
//  - kAISliceActionBoundsTopKey,integer
// -----------------------------------------------------------------------------

#define kAICreateSliceAction						"AISlice Create Action"

const ActionParamKeyID kAISliceActionBoundsLeftKey		= 'left'; // integer
const ActionParamKeyID kAISliceActionBoundsBottomKey	= 'bttm'; // integer
const ActionParamKeyID kAISliceActionBoundsRightKey		= 'rght'; // integer
const ActionParamKeyID kAISliceActionBoundsTopKey		= 'top '; // integer


// -----------------------------------------------------------------------------
// Action: kAIDeleteAllSliceAction
// Purpose: Deletes all the document slices
// Parameters: 
//		none
// -----------------------------------------------------------------------------

#define kAIDeleteAllSliceAction						"AISlice Delete All Action"


// -----------------------------------------------------------------------------
// Action: kAIClipToArtboardSliceAction
// Purpose: Sets the sliced region to the artboard if true, or the 
//			object bounds if false
// Parameters: 
//  - kAIClipOn, bool
// -----------------------------------------------------------------------------

#define kAIClipToArtboardSliceAction				"AISlice Clip To Artboard Action"
		
const ActionParamKeyID kAIClipOn					= 'clip'; // bool
		
		

#endif // _AISLICEACTION_H_