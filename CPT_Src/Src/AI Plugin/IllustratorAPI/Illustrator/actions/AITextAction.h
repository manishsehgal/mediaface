#ifndef _AICORETEXTACTION_H_
#define _AICORETEXTACTION_H_

/*
 *        Name:	AICoreTextAction.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Actions defined in the core.
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifndef __AIActionManager_h__
#include "AIActionManager.h"
#endif


// -----------------------------------------------------------------------------
// Action: kAISetTextFontAction
// Purpose: Set the text font
// Parameters:
//	- kAISetTextFontKey, string:
//	- kAISetTextFontFamilyKey, string:
//	- kAISetTextFontStyleKey, string:
// Comments:
// Set the text font for subsequently created text or for the selected text.
// All parameters except for 'font' are ignored.
// -----------------------------------------------------------------------------

#define kAISetTextFontAction									"adobe_font"
const ActionParamKeyID kAISetTextFontKey						= 'font'; // string
const ActionParamKeyID kAISetTextFontFamilyKey					= 'fmly'; // string
const ActionParamKeyID kAISetTextFontStyleKey					= 'styl'; // string

// -----------------------------------------------------------------------------
// Action: kAISetTextSizeAction
// Purpose: Set the text size
// Parameters:
//	- kAISetTextSizeKey, real:
// Comments:
// Set the text size for subsequently created text or for the selected text.
// -----------------------------------------------------------------------------

#define kAISetTextSizeAction									"adobe_fontsize"
const ActionParamKeyID kAISetTextSizeKey						= 'size'; // real

// -----------------------------------------------------------------------------
// Action: kAIShowHiddenCharactersAction
// Purpose: Toggles whether hidden characters are displayed
// Parameters:
// -----------------------------------------------------------------------------

#define kAIShowHiddenCharactersAction							"adobe_showHiddenChar"

// -----------------------------------------------------------------------------
// Action: kAISetTextOrientationAction
// Purpose: Sets the text orientation
// Parameters:
//	- kAISetTextOrientationKey, enum: 0 => horizontal, 1 => vertical
// -----------------------------------------------------------------------------

#define kAISetTextOrientationAction								"adobe_textOrientation"
const ActionParamKeyID kAISetTextOrientationKey					= 'horz'; // enum

// -----------------------------------------------------------------------------
// Action: kAICreateOutlinesFromTextAction
// Purpose: Create outlines from the selected text
// Parameters: None
// -----------------------------------------------------------------------------

#define kAICreateOutlinesFromTextAction							"adobe_createOutline"

// -----------------------------------------------------------------------------
// Action: kAIFitHeadlineAction
// Purpose: Fit headline
// Parameters: None
// -----------------------------------------------------------------------------

#define kAIFitHeadlineAction									"adobe_fitHeadline"

// -----------------------------------------------------------------------------
// Action: kAILinkTextBlocksAction
// Purpose: Link text blocks
// Parameters: None
//
// Action: kAIUnlinkTextBlocksAction
// Purpose: Unlink text blocks
// Parameters: None
// -----------------------------------------------------------------------------

#define kAILinkTextBlocksAction									"adobe_linkText"
#define kAIUnlinkTextBlocksAction								"adobe_unlinkText"

// -----------------------------------------------------------------------------
// Action: kAIMakeTextWrapAction
// Purpose: Make text wrap
// Parameters: None
//
// Action: kAIReleaseTextWrapAction
// Purpose: Release text wrap
// Parameters: None
// -----------------------------------------------------------------------------

#define kAIMakeTextWrapAction									"adobe_makeWrap"
#define kAIReleaseTextWrapAction								"adobe_releaseWrap"

// -----------------------------------------------------------------------------
// Action: kAISetParagraphStyleAction
// Purpose: Set paragraph style
// Parameters:
//	- kAISetParagraphStyleAlignmentKey, enum:
//	- kAISetParagraphStyleJustifyLastLineKey, bool:
//
//	- kAISetParagraphStyleHangingPunctuationKey, bool:
//	- kAISetParagraphStyleAutoHyphenationKey, bool:
//
//	- kAISetParagraphStyleLeftIndentKey, real:
//	- kAISetParagraphStyleRightIndentKey, real:
//	- kAISetParagraphStyleIndentFirstKey, real:
//
//	- kAISetParagraphStyleSpaceBeforeKey, real:
//
//	- kAISetParagraphStyleMinimumWordSpacingKey, real:
//	- kAISetParagraphStyleMaximumWordSpacingKey, real:
//	- kAISetParagraphStyleOptimalWordSpacingKey, real:
//
//	- kAISetParagraphStyleMinimumLetterSpacingKey, real:
//	- kAISetParagraphStyleMaximumLetterSpacingKey, real:
//	- kAISetParagraphStyleOptimalLetterSpacingKey, real:
//
//	- kAISetParagraphStyleMinimumCharsBeforeHyphenKey, integer:
//	- kAISetParagraphStyleMinimumCharsAfterHyphenKey, integer:
//	- kAISetParagraphStyleMaximumConsecutiveHyphensKey, integer:
//
//	- kAISetParagraphStyleProcessRepeatCharactersKey, bool:
//	- kAISetParagraphStyleBindingPunctuationKey, bool:
//	- kAISetParagraphStyleCJKBindingPunctuationTypeKey, enum:
//	- kAISetParagraphStyleCJKDontSqueezePeriodKey, bool:
//	- kAISetParagraphStyleCJKBindContinuousCharsKey, bool:
//	- kAISetParagraphStyleCJKDontAdjustContinuousCharsKey, bool:
//	- kAISetParagraphStyleCJKHangCharsAlwaysHangKey, bool:
// -----------------------------------------------------------------------------

#define kAISetParagraphStyleAction								"adobe_paragraphPalette"

const ActionParamKeyID kAISetParagraphStyleAlignmentKey			= 'alin'; // enum
const ActionParamKeyID kAISetParagraphStyleJustifyLastLineKey	= 'last'; // bool

const ActionParamKeyID kAISetParagraphStyleHangingPunctuationKey = 'hang'; // bool
const ActionParamKeyID kAISetParagraphStyleAutoHyphenationKey	= 'auto'; // bool

const ActionParamKeyID kAISetParagraphStyleLeftIndentKey		= 'left'; // real
const ActionParamKeyID kAISetParagraphStyleRightIndentKey		= 'rit.'; // real
const ActionParamKeyID kAISetParagraphStyleIndentFirstKey		= 'flft'; // real

const ActionParamKeyID kAISetParagraphStyleSpaceBeforeKey		= 'pspc'; // real

const ActionParamKeyID kAISetParagraphStyleMinimumWordSpacingKey = 'miws'; // real
const ActionParamKeyID kAISetParagraphStyleMaximumWordSpacingKey = 'maws'; // real
const ActionParamKeyID kAISetParagraphStyleOptimalWordSpacingKey = 'dsws'; // real

const ActionParamKeyID kAISetParagraphStyleMinimumLetterSpacingKey = 'mils'; // real
const ActionParamKeyID kAISetParagraphStyleMaximumLetterSpacingKey = 'mals'; // real
const ActionParamKeyID kAISetParagraphStyleOptimalLetterSpacingKey = 'dsls'; // real

const ActionParamKeyID kAISetParagraphStyleMinimumCharsBeforeHyphenKey = 'lhyp'; // integer
const ActionParamKeyID kAISetParagraphStyleMinimumCharsAfterHyphenKey = 'thyp'; // integer
const ActionParamKeyID kAISetParagraphStyleMaximumConsecutiveHyphensKey = 'hlns'; // integer

const ActionParamKeyID kAISetParagraphStyleProcessRepeatCharactersKey = 'rpet'; // bool
const ActionParamKeyID kAISetParagraphStyleBindingPunctuationKey = 'punc'; // bool
const ActionParamKeyID kAISetParagraphStyleCJKBindingPunctuationTypeKey = 'punc'; // enum
const ActionParamKeyID kAISetParagraphStyleCJKDontSqueezePeriodKey = 'squz'; // bool
const ActionParamKeyID kAISetParagraphStyleCJKBindContinuousCharsKey = 'bind'; // bool
const ActionParamKeyID kAISetParagraphStyleCJKDontAdjustContinuousCharsKey = 'adjs'; // bool
const ActionParamKeyID kAISetParagraphStyleCJKHangCharsAlwaysHangKey = 'adjs'; // bool

// -----------------------------------------------------------------------------
// Action: kAISetCharacterStyleAction
// Purpose: Set character style
// Parameters:
//	-  kAISetCharacterStyleFontKey, string:
//	-  kAISetCharacterStyleSizeKey, real:
//	-  kAISetCharacterStyleTrackingKey, integer:
//	-  kAISetCharacterStyleAutoKerningKey, existence enables autokern:
//	-  kAISetCharacterStyleKerningKey, integer:
//	-  kAISetCharacterStyleAutoLeadingKey, existence enables autolead:
//	-  kAISetCharacterStyleLeadingKey, real:
//	-  kAISetCharacterStyleHorizontalScaleKey, real:
//	-  kAISetCharacterStyleVerticalScaleKey, real:
//	-  kAISetCharacterStyleBaselineShiftKey, real:
//	-  kAISetCharacterStyleLanguageKey, enum:
//	-  kAISetCharacterStyleDirectionKey, enum:
//	-  kAISetCharacterStyleMojiZumeKey, bool:
//	-  kAISetCharacterStyleWariChuKey, bool:
//	-  kAISetCharacterStyleWariChuScaleKey, real:
//	-  kAISetCharacterStyleKumiHanKey, bool:
//	-  kAISetCharacterStyleKumiWaJiKanKey, real:
//	-  kAISetCharacterStyleKumiWaOuKanKey, real:
// -----------------------------------------------------------------------------

#define kAISetCharacterStyleAction								"adobe_characterPalette"

const ActionParamKeyID kAISetCharacterStyleFontKey				= 'font'; // string
const ActionParamKeyID kAISetCharacterStyleSizeKey				= 'size'; // real
const ActionParamKeyID kAISetCharacterStyleTrackingKey			= 'trck'; // integer
const ActionParamKeyID kAISetCharacterStyleAutoKerningKey		= 'atke'; // existence enables autokern
const ActionParamKeyID kAISetCharacterStyleKerningKey			= 'kern'; // integer
const ActionParamKeyID kAISetCharacterStyleAutoLeadingKey		= 'atld'; // existence enables autolead
const ActionParamKeyID kAISetCharacterStyleLeadingKey			= 'lead'; // real
const ActionParamKeyID kAISetCharacterStyleHorizontalScaleKey	= 'hori'; // real
const ActionParamKeyID kAISetCharacterStyleVerticalScaleKey		= 'vert'; // real
const ActionParamKeyID kAISetCharacterStyleBaselineShiftKey		= 'base'; // real
const ActionParamKeyID kAISetCharacterStyleLanguageKey			= 'lang'; // enum
const ActionParamKeyID kAISetCharacterStyleDirectionKey			= 'dirc'; // enum
const ActionParamKeyID kAISetCharacterStyleMojiZumeKey			= 'mono'; // bool
const ActionParamKeyID kAISetCharacterStyleWariChuKey			= 'wari'; // bool
const ActionParamKeyID kAISetCharacterStyleWariChuScaleKey		= 'scal'; // real
const ActionParamKeyID kAISetCharacterStyleKumiHanKey			= 'lyot'; // bool
const ActionParamKeyID kAISetCharacterStyleKumiWaJiKanKey		= 'cjk.'; // real
const ActionParamKeyID kAISetCharacterStyleKumiWaOuKanKey		= 'cjk.'; // real

// -----------------------------------------------------------------------------
// Action: kAISetTabStopsAction
// Purpose: Set tab stops
// Parameters:
//	- kAISetTabStopsPaletteOrientationKey, bool:
//	- kAISetTabStopsNumberOfStopsKey, integer:
//	- kAISetTabStopsAutoStopDeltaKey, real:
// Comments:
//	Each tab stop is specified by two key entries constructed from a pair
//	of characters and the tab stop number 'n' as follows:
//
//	makeLong('a', 'l', n) specifies the alignment
//	makeLong('p', 's', n) specifies the position
//
//	where makeLong is defined to be:
//
//	unsigned long makeLong(char a, char b, int number)
//	{
//		unsigned long rVal;
//
//		if (number > 100 || number < 0)
//			return 0;
//
//		rVal = a << 24;
//		rVal += b << 16;
//		rVal += (char)((number / 10 + 48)) << 8;
//		rVal += (number % 10 + 48);
//
//		return rVal;
//	}
// -----------------------------------------------------------------------------

#define kAISetTabStopsAction									"adobe_tabruler"

const ActionParamKeyID kAISetTabStopsPaletteOrientationKey		= 'horz'; // bool
const ActionParamKeyID kAISetTabStopsNumberOfStopsKey			= 'num.'; // integer
const ActionParamKeyID kAISetTabStopsAutoStopDeltaKey			= 'dlta'; // real

#endif
