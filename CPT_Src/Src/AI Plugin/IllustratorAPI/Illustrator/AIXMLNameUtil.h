#ifndef __AIXMLNameUtil__
#define __AIXMLNameUtil__

/*
 *        Name:	AIXMLNameUtil.h
 *		$Id $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 XML Name Utilties Suite.
 *
 * Copyright (c) 2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIXMLNameUtil.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIXMLNameUtilSuite						"AI XML Name Utilities Suite"
#define kAIXMLNameUtilSuiteVersion2				AIAPI_VERSION(2)
#define kAIXMLNameUtilSuiteVersion				kAIXMLNameUtilSuiteVersion2
#define kAIXMLNameUtilVersion					kAIXMLNameUtilSuiteVersion


/*******************************************************************************
 **
 **	Suite
 **
 **/


/** The XML standard requires that names conform to a specific syntax. For
	example it cannot contain the space character and must start with a
	letter. This suite provides APIs for working with XML names. In particular
	it contains some utilities for converting between general strings and
	names. Illustrator uses these facilities to convert between art object
	names and UIDs (see the AIUIDUtilsSuite).

	See http://www.w3.org/TR/REC-xml#NT-Name for the XML name syntax.
*/
typedef struct AIXMLNameUtilSuite {

	/** Given a base name and number constructs an XML name. The
		constructed name will be no longer than the maximum specified
		length. */
	AIAPI AIErr (*NameFromBaseAndNumber) (const ai::UnicodeString& base, ai::UnicodeString& buffer, int number, ASInt32 maxlength);

	/** Given a possibly uniquified XML name returns the base name. */
	AIAPI AIErr (*BaseFromName) (const ai::UnicodeString& name, ai::UnicodeString& buffer);

	/** Checks that the name is a valid XML name. */
	AIAPI AIBoolean (*IsValidName) (const ai::UnicodeString& name);

	/** Given an arbitrary string constructs a valid XML name.  */
	AIAPI AIErr (*XMLNameFromString) (const ai::UnicodeString& str, ai::UnicodeString& buffer);

	/** Reverse the action of the preceding. */
	AIAPI AIErr (*StringFromXMLName) (const ai::UnicodeString& name, ai::UnicodeString& buffer);

} AIXMLNameUtilSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
