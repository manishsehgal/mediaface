#ifndef __AICMS__
#define __AICMS__

/*
 *        Name:	AICMS.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 7.0 Color Management System (CMS) Suite
 *				Currently just defines a Notifier but would have more
 *				functionality later on.
 *
 * Copyright (c) 1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/
#include "AITypes.h"


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AICMS.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/
#define kAICMSSuite				"AI Color Management System Suite"
#define kAICMSSuiteVersion		AIAPI_VERSION(1)
#define kAICMSVersion			kAICMSSuiteVersion

/** @ingroup Notifiers
	The color calibration changed notifier is sent whenever the color management
	settings for are changed. */
#define kAIColorCalibrationChangedNotifier		"AI Color Calibration Changed Notifier"

/*******************************************************************************
 **
 **	Types
 **
 **/


/*******************************************************************************
 **
 **	Suite
 **
 **/

/*
typedef struct {
	AIAPI AIErr (*NothingDefinedYet) ( void );
} AICMSSuite;
*/


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
