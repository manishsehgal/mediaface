#ifndef __AIFOConversion__
#define __AIFOConversion__

/*
 *        Name:	AIFOConversion.h
 *		$Id $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 11.0 Foreign Object Conversion Suite.
 *
 * Copyright (c) 2002 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIForeignObject__
#include "AIForeignObject.h"
#endif

#ifndef __AIFont__
#include "AIFont.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIFOConversion.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIFOConversionSuite				"AI FO Conversion Suite"
#define kAIFOConversionSuiteVersion1		AIAPI_VERSION(1)
#define kAIFOConversionSuiteVersion			kAIFOConversionSuiteVersion1
#define kAIFOConversionVersion				kAIFOConversionSuiteVersion


/* error codes */
#define kAIFOConversionErr		'!FOc'

/*******************************************************************************
 **
 **	Types
 **
 **/
 
/** This is an AGMColorSpace* clients should wrap it as a CAGMColorSpace
	or some baseclass in order to use it. */
typedef struct _AIAGMColorSpace *AIAGMColorSpaceHandle;


/** When converting a foreign object to native artwork a conversion info
	collector can be supplied. This is a callback function that receives
	information about the conversion process. The conversion info selector
	indicates the type of information being supplied. */
typedef enum AIFOConversionInfoSelector
{
	/** info == NULL */
	kAIFOConversionInfoNone,
	/** info == const char *: font name */
	kAIFOConversionInfoFauxedFont,
	/** info == NULL */
	kAIFOConversionInfoUnknownAxialShading,
	/** info == NULL */
	kAIFOConversionInfoUnknownRadialShading,
	/** info == NULL */
	kAIFOConversionInfoUnknownAIMConstruct,
	/** info == NULL */
	kAIFOConversionInfoTextOutlined,
	/** info == NULL */
	kAIFOConversionInfoTextRasterized

} AIFOConversionInfoSelector;


/** The AIFOConversionSuite::EnumerateContents() API allows the contents
	of a foreign object to be inspected. Information about the contents is
	sent to a callback function. The content info selector indicates the
	type of information being supplied. */
typedef enum AIFOContentInfoSelector
{
	/** info == NULL */
	kAIFOContentInfoNone,
	/** info == const AIBoolean* */
	kAIFOContentInfoHasTransparency,
	/** info == const AIBoolean* */
	kAIFOContentInfoMarksProcessPlates,
	/** info == const AIBoolean [4] c,m,y,k */
	kAIFOContentInfoMarksCMYKPlates,
	/** info == const AIBoolean* */
	kAIFOContentInfoHasOverPrint,
	/** info == const AIBoolean* */
	kAIFOContentInfoHasText,
	/** info == const AIBoolean* */
	kAIFOContentInfoHasGradient,
	/** info == const AIBoolean* */
	kAIFOContentInfoHasGradientMesh,
	/** info == AICustomColorHandle */
	kAIFOContentInfoSpotColor,
	/** info == AIRealMatrix. Each time a drawing operation involves an image
		as the source of color data, this selector is sent providing the
		transformation matrix applied. */
	kAIFOContentInfoRasterTransform,
	/** info == AIRasterRecord. Each time a drawing operation involves an image
		as the source of color data this selector is sent providing information
		about the image. */
	kAIFOContentInfoPaintServerRasterRecord,
	/** info == const AIBoolean*. True if the object contains anything other
		than images and clipping operations. */
	kAIFOContentInfoHasNonImage

} AIFOContentInfoSelector;


/** Flags for conversion of a foreign object. See #AIFOConversionFlagValues */
typedef long AIFOConversionFlags;

/** Flag values that specify options for the conversion of a foreign object to
	native artwork. */
enum AIFOConversionFlagValue {
	/** Preserves full visual fidelity, may create other FOs (preserves spots) */
	kAIFOConversionDefault 				= 0,
	/** Full but possibly lossy conversion will not create other FOs (converts spots to process) 
		and will rasterize objects that cannot be represented natively.  This includes objects filled
		with PDF shading types, device-n (aka spot) rasters, etc. */
	kAIFOConversionFull					= 1 << 0,
	/** Replace paths with stroked version */
	kAIFOConversionStrokePaths			= 1 << 1,
	/** Simplify the paths (no subpaths that intersect) */
	kAIFOConversionSimplifyPaths		= 1 << 2,
	/** Suppress object attributes. Applicable only to ConvertToNative API.  This flags suppresses object-level
		attributes such as visibility, live-effects, transparency, etc.  Intended for clients that want to
		convert only the CONTENTS of the FO (i.e. the display list) and not the FO along with Illustrator
		drawing attributes applied via targetted application.  Note also, this is the default behavior for 
		the OutlineLegacyText API. */
	kAIFOConversionSuppressAttributes	= 1 << 3,
	/** Disallow colorized grayscale images in the output from conversion. */
	kAIFOConversionNoColorizedGray		= 1 << 4,
	/** Forces use of Newell mixing to convert spot colors to process. Newell mixing has the property
		that it preserves the numbers of the spot color alternate when converting to process. */
	kAIFOConversionUseNewellMixing		= 1 << 5
};


/** The AIFOConversionSuite::ConvertToNative() API can provide information about the
	conversion process. The information is supplied to a callback function of this
	type. The selector identifies the type of information being supplied. The
	info parameter provides information specific to the selector. To inspect its
	contents it must be cast to a type dependent on the selector. The data parameter
	is supplied to ConvertToNative by the client and passed back to the callback
	function. */
typedef void (*AIConversionInfoProc)(AIFOConversionInfoSelector selector, void *info, void *data);

/** The AIFOConversionSuite::EnumerateFonts() API enumerates the fonts used by a foreign
	object to a callback function of this type. The data parameter is supplied to
	EnumerateFonts by the client and passed back to the callback function. */
typedef void (*AIVisitFontProc)(AIFontKey fontkey, void *data);

/** The AIFOConversionSuite::EnumerateContents() API provide information about the
	contents of a foreign object. The information is supplied to a callback function of 
	this type. The selector identifies the type of information being supplied. The
	info parameter provides information specific to the selector. To inspect its
	contents it must be cast to a type dependent on the selector. The data parameter
	is supplied to EnumerateContents by the client and passed back to the callback
	function. */
typedef void (*AIVisitContentProc)(AIFOContentInfoSelector selector, void *info, void *data);

/** The AIFOConversionSuite::ConvertToNative() API can provide information about the
	conversion process. To do so the client passes in this structure. */
typedef struct AIFOConversionInfoCollector
{
	/** NULL if no info desired. */
	AIConversionInfoProc proc;
	/** Sent to the proc as the data parameter */
	void *data;

} AIFOConversionInfoCollector;

/** Structure describing options for AIFOConversionSuite::ConvertToNative() */
typedef struct
{
	AIFOConversionFlags flags;
	AIFOConversionInfoCollector info;

	/** Rasterization resolution for high frequency areas (same as in AIMaskFlattener.h).
		Set to zero to pick up document flattener settings. */
	AIReal rasterResolution;
	
	/** Rasterization resolution for low frequency areas (smooth shades) (same as in AIMaskFlattener.h).
		Set to zero to pick up document flattener settings. */
	AIReal meshResolution;

	/** Allows the caller to supply a custom colorspace for blending of spots to process.  Set to NULL to
		pickup up default document colorspace */
	AIAGMColorSpaceHandle conversionCS;
	
} AIFOConversionOptions;



/*******************************************************************************
 **
 **	Suites
 **
 **/

/** The foreign object conversion suite provides APIs for converting artwork and for
	collecting information about its contents. The suite was originally intended
	for use only with foreign objects. However, its APIs can function with any
	type of artwork. For example, the AIFOConversionSuite::ConvertToNative() API
	can be used to convert artwork in such a way that the resulting art does not
	contain self intersecting paths. For more information about foreign objects see the
	AIForeignObjectSuite. */
typedef struct AIFOConversionSuite {

	/** Convert the art object to native illustrator art objects. Note that the
		input object is not deleted.

		- input "art" is any Illustrator art type.  It is important to note however that
			passing clipping objects in (such as clip paths, clipping text, or clipping legacy text)
			will give an empty result.  ConvertToNative(...) can be thought of more as a DrawToNative(...)
			in the sense that the input art is drawn into a "conversion" port that only sees marking commands.
			Since a clip, when drawn, does not do any actual marking no artwork arises from the conversion process.
			To convert legacy text clips to native clips refer to the AILegacyTextConversion suite.
		- options must be supplied
		- paintOrder and prep indicate where the resultant art is to be placed. They
			are identical to the corresponding parameters of AIArtSuite::NewArt()
	*/
	AIAPI AIErr (*ConvertToNative)(AIArtHandle art, AIFOConversionOptions *options, short paintOrder, AIArtHandle prep);
	
	/** Get an instance of the conversion port.  For AGM-savvy clients only.  Must release with AIFOConversionSuite::ReleaseConversionPort() */
	AIAPI AIErr (*GetConversionPort)(AIFOConversionOptions *options, short paintOrder, AIArtHandle prep, AIDisplayPortHandle *port);

	/** Release an instance of the conversion port.  For AGM-savvy clients only.  Must have obtained the port with AIFOConversionSuite::GetConversionPort() */
	AIAPI AIErr (*ReleaseConversionPort)(AIDisplayPortHandle port);

	/** Enumerate the fonts used to draw the object to the callback function. */
	AIAPI AIErr (*EnumerateFonts)(AIArtHandle art, AIVisitFontProc visitor, void *data);
	
	/** Enumerate information about the contents of the object needed in order
		to draw it to the callback function. */
	AIAPI AIErr (*EnumerateContents)(AIArtHandle art, AIVisitContentProc visitor, void *data);

	/** Convert a legacy text object to outlines. This API attempts to perform the
		conversion in such a way that the result of converting the legacy text
		object is identical to the result that would be obtained by doing the
		conversion in the legacy version of the application. It is intended for use
		by plugin groups or live effects that need to execute against legacy text
		to ensure that the results of execution are the same as in the legacy
		version. Note: please see comment on kAIFOConversionSuppressAttributes flag for discussion
		of the application of object-level attributes.  */
	AIAPI AIErr (*OutlineLegacyText)(AIArtHandle art, short paintOrder, AIArtHandle prep);

} AIFOConversionSuite;



#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif // __AIFOConversion__
