#ifndef __AIColor__
#define __AIColor__

/*
 *        Name:	AIColor.h
 *   $Revision: 17 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Color Suite.
 *
 * Copyright (c) 1986-2003 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIRealMath__
#include "AIRealMath.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIColor.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

/** Types of "colors" that can be used to fill or stroke Illustrator art objects.
	Since these include patterns and gradients the term color is something of a
	misnomer. */
typedef enum {
	/** Gray */
	kGrayColor = 0,
	/** Solid ink, expressed in four CMYK process colors */
	kFourColor,
	/** Color with a pattern from the art's pattern list */
	kPattern,
	/** Solid ink, expressed as a custom ink color */
	kCustomColor,
	/** Gradient */
	kGradient,
	/** RGB colors */
	kThreeColor,
	/** None color, NOTE: when painted with kNoneColor, still need to
		set AIPathStyle.fillPaint or strokePaint to false */
	kNoneColor
} AIColorTag;

/** Custom colors define named colors. The appearance of the named color is
	defined by a color of one of these types. */
typedef enum {
	/** Solid ink, expressed in four process colors */
	kCustomFourColor = 0,
	/** RGB colors */
	kCustomThreeColor,
 	/** Lab color. Only valid for spot colors. */
	kCustomLabColor
} AICustomColorTag;

/** Types of gradients. */
enum {
	kLinearGradient = 0,
	kRadialGradient
};

/** Custom colors define named colors. The flags define properties of the
	name color. See #kCustomSpotColor and #kCustomRegistrationColor. */
typedef short AICustomColorFlags;

/** #AICustomColorFlag indicating that a custom color is a spot color rather
	than a global process color. */
#define	kCustomSpotColor			0x0001
/** #AICustomColorFlag indicating that a custom color is a registration color. */
#define kCustomRegistrationColor	0x0002


/** @ingroup Errors
	Attempting to define a spot color with one of the reserved names
	Cyan, Magenta, Yellow, Black, None or All. */
#define kNameInvalidForSpotColorErr		'NA~.'


/*******************************************************************************
 **
 **	Types
 **
 **/

/** Describes a gray color. */
typedef struct {
	AIReal gray;
} AIGrayColorStyle;

/** Describes the valid fields of a gray color. */
typedef struct {
	AIBoolean gray;
} AIGrayColorStyleMap;


/** Describes a CMYK color. */
typedef struct {
	AIReal cyan, magenta, yellow, black;
} AIFourColorStyle;

/** Describes the valid fields of a CMYK color. */
typedef struct {
	AIBoolean cyan, magenta, yellow, black;
} AIFourColorStyleMap;


/** Describes an RGB color. */
typedef struct {
	AIReal red, green, blue;
} AIThreeColorStyle;

/** Describes the valid fields of an RGB color. */
typedef struct {
	AIBoolean red, green, blue;
} AIThreeColorStyleMap;


/** Describes an Lab color. */
typedef struct {
	AIReal l, a, b;
} AILabColorStyle;

/** Describes the valid fields of an Lab color. */
typedef struct {
	AIBoolean l, a, b;
} AILabColorStyleMap;


/** The union of all possible process colors that can be associated with
	a custom color. */
typedef union {
	AIFourColorStyle	f;
	AIThreeColorStyle	rgb;
	AIGrayColorStyle	gray;
	AILabColorStyle		lab;
} AICustomColorUnion;

/** Custom colors define named colors. These colors may be actual inks or simply
	names for process colors depending on whether the custom color is a spot
	or global process color. In either case the custom color has an associated
	process color. The type of process color is identified by this enumeration. */
typedef struct {
	AICustomColorTag	kind;
	AICustomColorUnion	c;
	AICustomColorFlags	flag;
} AICustomColor;

/** Opaque reference to a custom color. */
typedef void *AICustomColorHandle;

/** Describes an instance of a custom color paint. */
typedef struct {
	/** Reference to the custom color which describes instance independent
		parameters for the paint. */
	AICustomColorHandle color;
	/** Amount of the custom color to be applied. */
	AIReal tint;
} AICustomColorStyle;

/** Describes the valid fields of a custom color paint. */
typedef struct {
	AIBoolean color;
	AIBoolean tint;
} AICustomColorStyleMap;


typedef struct {
	AIBoolean not_implemented;
} AIPattern;

/** Opaque reference to a pattern. */
typedef void *AIPatternHandle;

/** The pattern style record defines a pattern instance, or an application of a
	pattern to an art object. The instance includes a reference to the pattern to
	use in the fill or stroke and information about how it is transformed, such
	as scale value. These values are discussed more completely below.

	The pattern itself is an opaque reference accessed through the AIPatternSuite
	functions. These functions get and set a pattern�s name and defining art object,
	which can be a group of several objects. When a pattern is applied to an
	object, this art is tiled to paint it.

	The pattern fill is tiled from the ruler origin rather than relative to the
	object. Before the tiling is done, the transformations in the AIPatternStyle
	record are applied to the pattern art.

	The translation of the pattern art is given by a distance and an angle. The
	distance shiftDist is from the ruler origin at the angle shiftAngle (specified
	in degrees). The scale and rotate transformations are specified in the
	fields of the same name. If the pattern art is reflected, reflect will be true
	and reflectAngle will be the angle about which the reflection is done.
	shearAngle and shearAxis define the angle at which the pattern art is
	skewed and the axis about which it is done.

	The last value of the the AIPatternStyle record is transform, which
	represents the object transformations done by the user.
*/
typedef struct {
	/** Reference to the AIPattern which describes instance independent
		parameters for the paint (the prototype artwork). */
	AIPatternHandle pattern;
	/** Distance to translate the [unscaled] prototype before filling */
	AIReal shiftDist;
	/** Angle to translate the [unscaled] prototype before filling */
	AIReal shiftAngle;
	/** Fraction to scale the prototype before filling */
	AIRealPoint scale;
	/** Angle to rotate the prototype before filling */
	AIReal rotate;
	/** Whether or not the prototype is reflected before filling */
	AIBoolean reflect;
	/** Axis around which to reflect */
	AIReal reflectAngle;
	/** Angle to slant the shear by */
	AIReal shearAngle;
	/** Axis to shear with respect to */
	AIReal shearAxis;
	/** Additional transformation arising from manipulating the path */
	AIRealMatrix transform;
} AIPatternStyle;

/** Describes the valid fields of a pattern paint */
typedef struct {
	AIBoolean pattern;
	AIBoolean shiftDist;
	AIBoolean shiftAngle;
	AIBoolean scale;
	AIBoolean rotate;
	AIBoolean reflect;
	AIBoolean reflectAngle;
	AIBoolean shearAngle;
	AIBoolean shearAxis;
	AIBoolean transform;
} AIPatternStyleMap;

 /** opaque reference to gradient color */
typedef void *AIGradientHandle;

/** The gradient style record defines a gradient instance. The instance includes
	information such as a reference to a gradient definition, and the origin
	point and transformation matrix for the blend. A gradient is sometimes referred
	to as a blend.

	The gradient field of the gradient style record is an opaque reference to the
	information that describes the gradient, including its name and gradient
	ramp. The gradient ramp describes where the color changes of the gradient
	occur. The gradient definition also specifies whether a gradient is linear or
	radial. See the AIGradientSuite.

	The gradientOrigin, gradientAngle, and gradientLength fields of the AIGradientStyle
	structure define the gradient vector, which determines the appearance of the
	gradient within the art object.

	The gradientOrigin value gives the origin of the gradient in page coordinates.
	This corresponds to 0 on the gradient ramp. gradientAngle is the direction in
	degrees of the gradient. The ramp extends from the origin at the angle. The
	angle is with respect to the x axis with positive angles being anti-clockwise.
	The length is the distance over which the gradient ramp is applied. The ramp will
	be scaled so that its 100% value is the end of the length.

	The AIGradientStyle matrix holds the accumulated transformations of the
	gradient. It is not necessarily the same as the transformation matrix of the
	object containing the gradient. When a gradient is first applied to an object,
	it will be set to the identity matrix. When the user transforms the object, the
	user transformation matrix will be concatenated to the gradient instance's
	matrix.

	The gradient vector has a different meaning for a radial gradient. The
	vector origin is the center of the circle containing the radial gradient. The
	vector length in this case is the radius of the that circle. The vector angle is
	not used by radial blends, but is kept and used if the gradient is changed to
	a linear one.

	Radial blends have an additional attribute called a blend hilight. The hilight
	serves at the starting point for the gradient ramp as it expands outward. It is
	still contained within the gradient vector circle.

	The location of the highlight is describe by the hiliteAngle and hiliteLength
	fields of the AIGradientStyle structure. The angle value is the angle to that
	hilight point. The length is distance of the hilight from the origin expressed
	as a fraction of the radius, a value between 0 and 1.
*/
typedef struct {
	/** Reference to the AIGradient which defines instance independent
		parameters for the paint. */
	AIGradientHandle gradient;
	/** Gradient vector origin */
	AIRealPoint gradientOrigin;
	/** Gradient vector angle */
	AIReal gradientAngle;
	/** Gradient vector length */
	AIReal gradientLength;
	/** Transformation applied by xform tools */
	AIRealMatrix matrix;
	/** Gradient hilite vector angle */
	AIReal hiliteAngle;
	/** Length of hiliteVector measured as a fraction
		of the length of the gradient vector (gradientLength) */
	AIReal hiliteLength;
} AIGradientStyle;

/** Describes the valid fields of a gradient paint. */
typedef struct {
	AIBoolean gradient;
	AIBoolean gradientOrigin;
	AIBoolean gradientAngle;
	AIBoolean gradientLength;
	AIBoolean matrix;
	AIBoolean hiliteAngle;
	AIBoolean hiliteLength;
} AIGradientStyleMap;


/** The union of all possible types of color specification. */
typedef union { 
	AIGrayColorStyle g;
	AIFourColorStyle f;
	AIThreeColorStyle rgb;
	AICustomColorStyle c;
	AIPatternStyle p;
	AIGradientStyle b;
} AIColorUnion;

/** The union of all possible types of color specification. */
typedef union { 
	AIGrayColorStyleMap g;
	AIFourColorStyleMap f;
	AIThreeColorStyleMap rgb;
	AICustomColorStyleMap c;
	AIPatternStyleMap p;
	AIGradientStyleMap b;
} AIColorUnionMap;

/** Describes a "color". Since these include patterns and gradients the term color
	is something of a misnomer. Colors may either be fully or partially specified.
	A partially specified color is used when retrieving the common color attributes
	of a collection of objects or when modifying a specific subset of color attributes
	for one or more objects. When a color is partially specified an associated
	AIColorMap structure contains boolean fields indicating which fields are valid. */
typedef struct {
	/** Indicates the type of color being described. Discriminates the appropriate
		member of the color union. */
	AIColorTag kind;
	/** Contains the detailed specification of the color as appropriate for the kind
		discriminant. */
	AIColorUnion c;
} AIColor;

/** Defines the fields of an associated AIColor that have defined values. For
	example, if "kind" is true then the associated AIColor's kind member is
	valid. */
typedef struct {
	AIBoolean kind;
	AIColorUnionMap c;
} AIColorMap;


/** Defines a gradient stop. See the AIGradientSuite for more information about
	gradients.
	
	The rampPoint is a value giving the position of the color stop on the blend
	ramp from 0 to 100. The first rampPoint does not have to be at 0; the last
	does not have to be at 100.

	The midPoint argument is the location between two rampPoints where there
	is a 50/50 mix of the 2 colors. The ramp points used are those of the current
	gradient stop and the next one (n and n+1). It is a percentage of the
	distance between two rampPoints expressed as number between 13 and 87. The
	midPoint for the last color stop is ignored.

	The color argument indicates the type of color for the gradient stop. It is a
	standard color definition for a gray color, a process color, an RGB color or
	a custom color.
*/
typedef struct {
	AIReal midPoint; 				/* midpoint skew value in percent */
	AIReal rampPoint;				/* location of color in the blend in percent */
	AIColor color;
} AIGradientStop;


/** Typedef used by AIPluginGroup.h and AIPluginSelection.h as a callback function for situations
	where one component (such as a Color plugin filter) knows how to modify colors, and another component
	knows how to apply them to a specify kind of object or other color collection such as a swatch book.
	(The typedef very closely parallels the definition and usage of #AIMeshColorMap in AIMesh.h, but
	AIMeshColorMap predates this definition, so couldn't use it.)

	The AIColor* passed to the callback is both input and output. It is expected that a component
	managing a collection of objects or colors will iterate the colors, passing them to the callback,
	and retrieving the modified color and (if *altered is true), applying it to the objects or color
	collection.

	If *result is an error code, the iteration should stop. (The result is a parameter rather than making
	AIMapColorFunc return an AIErr so that the communication can go both ways. If the component handling
	the objects wants the component making the color adjustments to stop processing other objects and
	report an error back to the user, it should set *result to that error code BEFORE calling the
	AIAdjustColorFunc.)
*/
typedef void (*AIAdjustColorFunc) (AIColor *color, void *userData, AIErr *result, AIBoolean *altered);


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
