#ifndef __AIPreferenceKeys__
#define __AIPreferenceKeys__

//
//        Name: AIPreferenceKeys.h
//      Author: Chris Quartetti
//        Date: 2001.08.02 / Illustrator 10
//     Purpose: To define shared preference keys, defaults, and other
//              relevant information.
//       Notes: When sharing preferences between plugins and the main app,
//              either a) use a null prefix, or b) access the preferences from
//              the main app via the AIPreference suite.
//
//
// Copyright (c) 2001 Adobe Systems Incorporated, All Rights Reserved.
//
//

// Use Low Res Proxy for Linked EPS files
const unsigned kUseLowResProxyPrefix= NULL;
#define kUseLowResProxySuffix ((const char *)"useLowResProxy")
const bool kUseLowResProxyDefault= true;

// EPS rasterization resolution for linked EPS/DCS files
const unsigned kEPSResolutionPrefix = NULL;
#define kEPSResolutionSuffix	((const char *)"EPSResolution")
const long kEPSResolutionDefault = 300L;

// Update Linked File options
#define kFileClipboardPrefix ((const char *)"FileClipboard")
#define kLinkOptionsSuffix ((const char *)"linkoptions")
enum UpdateLinkOptions {AUTO, MANUAL, ASKWHENMODIFIED};
const UpdateLinkOptions kLinkOptionsDefault= ASKWHENMODIFIED;

// Enable OPI feature for Linked EPS files
const unsigned kEnableOPIPrefix = NULL;
#define kEnableOPISuffix	((const char *)"enableOPI")
const bool kEnableOPIDefault = false;

// Clipboard preferences
#define kcopyAsPDFSuffix		"copyAsPDF"
#define kcopyAsAICBSuffix		"copyAsAICB"
#define kappendExtensionSuffix	"appendExtension"
#define klowerCaseSuffix		"lowerCase"
#define kflattenSuffix			"flatten"
#define kAICBOptionSuffix	    "AICBOption"

enum  AICBOptions {PRESERVE_PATH, PRESERVE_APPEARANCE_OVERPRINT};
const AICBOptions kAICBOptionsDefault= PRESERVE_APPEARANCE_OVERPRINT;

/** @ingroup PreferenceKeys
	Version Cue preference */
#define kUseVersionCue			"useVersionCue"

/** @ingroup PreferenceKeys
	Determines whether screen display uses a black preserving color transformation
	for converting CMYK to RGB or gray. The black preserving transform maps CMYK
	0,0,0,1 to the darkest black available. While not colorimetrically accurate
	this is sometimes preferrable for viewing CMYK black line art and text. The
	default value is given by #kAIPrefDefaultOnscreenBlackPres. */
#define kAIPrefKeyOnscreenBlackPres			((const char*) "blackPreservation/Onscreen")
/** @ingroup PreferenceKeys
	The default value for #kAIPrefKeyOnscreenBlackPres. */
#define kAIPrefDefaultOnscreenBlackPres		true

/** @ingroup PreferenceKeys
	Determines whether printing and exportuses a black preserving color transformation
	for converting CMYK to RGB or gray. The black preserving transform maps CMYK
	0,0,0,1 to the darkest black available. While not colorimetrically accurate
	this is sometimes preferrable for viewing CMYK black line art and text. The
	default value is given by #kAIPrefDefaultExportBlackPres. */
#define kAIPrefKeyExportBlackPres			((const char*) "blackPreservation/Export")
/** @ingroup PreferenceKeys
	The default value for #kAIPrefKeyExportBlackPres. */
#define kAIPrefDefaultExportBlackPres		true

/** @ingroup PreferenceKeys
	Sets the Guide Style. Currently guide styles can be solid or dashed.
	The default value is given by #kAIPrefDefaultGuideStyle.  */
#define kAIPrefKeyGuideStyle ((const char*)"Guide/Style")
/** @ingroup PreferenceKeys
	The value for solid guides for #kAIPrefKeyGuideStyle. */
#define kAIPrefGuideStyleSolid 0
/** @ingroup PreferenceKeys
	The value for dashed guides for #kAIPrefKeyGuideStyle. */
#define kAIPrefGuideStyleDashed 1
/** @ingroup PreferenceKeys
	The default value for #kAIPrefKeyGuideStyle. */
#define kAIPrefDefaultGuideStyle kAIPrefGuideStyleSolid

/** @ingroup PreferenceKeys
	Sets the red component of the Guide color.
	The default value is given by #kAIPrefDefaultGuideColorRed.  */
#define kAIPrefKeyGuideColorRed ((const char*)"Guide/Color/red")
/** @ingroup PreferenceKeys
	The default value for #kAIPrefKeyGuideColorRed. */
#define kAIPrefDefaultGuideColorRed		(0x4A3D/65535.0f)

/** @ingroup PreferenceKeys
	Sets the green component of the Guide color.
	The default value is given by #kAIPrefDefaultGuideColorGreen.  */
#define kAIPrefKeyGuideColorGreen ((const char*)"Guide/Color/green")
/** @ingroup PreferenceKeys
	The default value for #kAIPrefKeyGuideColorGreen. */
#define kAIPrefDefaultGuideColorGreen		(1.0f)

/** @ingroup PreferenceKeys
	Sets the blue component of the Guide color.
	The default value is given by #kAIPrefDefaultGuideColorBlue.  */
#define kAIPrefKeyGuideColorBlue ((const char*)"Guide/Color/blue")
/** @ingroup PreferenceKeys
	The default value for #kAIPrefKeyGuideColorBlue. */
#define kAIPrefDefaultGuideColorBlue		(1.0f)

#endif
