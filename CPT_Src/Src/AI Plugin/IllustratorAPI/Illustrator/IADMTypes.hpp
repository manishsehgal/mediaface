/***********************************************************************/
/*                                                                     */
/* IADMTypes.hpp                                                        */
/* ADMTypes object wrapper classes                                      */
/*                                                                     */
/* Copyright 1996-2002 Adobe Systems Incorporated.                     */
/* All Rights Reserved.                                                */
/*                                                                     */
/* Patents Pending                                                     */
/*                                                                     */
/* NOTICE: All information contained herein is the property of Adobe   */
/* Systems Incorporated. Many of the intellectual and technical        */
/* concepts contained herein are proprietary to Adobe, are protected   */
/* as trade secrets, and are made available only to Adobe licensees    */
/* for their internal use. Any reproduction or dissemination of this   */
/* software is strictly forbidden unless prior written permission is   */
/* obtained from Adobe.                                                */
/*                                                                     */
/***********************************************************************/

#ifndef __IADMTypes_hpp__
#define __IADMTypes_hpp__

/*
 * Includes
 */
 
#ifndef __IADMFixed_hpp__
#include "IADMFixed.hpp"
#endif

#ifndef __IADMPoint_hpp__
#include "IADMPoint.hpp"
#endif

#ifndef __IADMRect_hpp__
#include "IADMRect.hpp"
#endif

#endif
