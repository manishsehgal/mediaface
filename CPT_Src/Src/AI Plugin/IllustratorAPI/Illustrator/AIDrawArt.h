#ifndef __AIDrawArt__
#define __AIDrawArt__

/*
 *        Name:	AIDrawArt.h
 *   $Revision: $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 11.0 Draw Art Suite.
 *
 * Copyright (c) 1986-1998 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AIColor__
#include "AIColor.h"
#endif

#ifndef __AIColorConversion__
#include "AIColorConversion.h"
#endif

#ifndef __AIRaster__
#include "AIRaster.h"
#endif

#include "ADMAGMTypes.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIDrawArt.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIDrawArtSuite				"AI Draw Art Suite"
#define kAIDrawArtSuiteVersion7		AIAPI_VERSION(7)
#define kAIDrawArtSuiteVersion		kAIDrawArtSuiteVersion7
#define kAIDrawArtVersion			kAIDrawArtSuiteVersion


/** Flag Bit Positions for #AIDrawArtFlags. */
enum AIDrawArtFlagBitPos {
	/** preview if true, artwork if false */
	kAIDrawArtPreviewBit						= 0,
	/** Show images if true */
	kAIDrawArtPreviewImagesBit					= 1,
	/** Show dimmed images if true */
	kAIDrawArtPreviewDimmedImagesBit			= 2,
	kAIDrawArtPreviewPatternsBit				= 3,
	kAIDrawArtSelectObjectsBit					= 4,
	kAIDrawArtShowGuidesBit						= 5,
	kAIDrawArtIgnoreArtTransformBit				= 6,
	kAIDrawArtPreviewCropBit					= 7,
	kAIDrawArtPreviewInterrupt					= 8,
	kAIDrawArtPreviewContinue					= 9,
	kAIDrawArtIgnoreDashedLines					= 10,
	kAIDrawArtDontColorCalibration				= 11,
	kAIDrawArtIsolate							= 12,
	kAIDrawArtIgnoreTopLevelTransparency		= 13,
	kAIDrawArtSimulateColoredPaper				= 14,
	/** If specified any overprint specification on objects is ignored therefore
		colors do not overprint. */
	kAIDrawArtIgnoreOverprint					= 15,
	kAIDrawArtPlayEPS							= 16,
	/** If specified then color conversion uses a faster but possible less accurate
		algorithm. This flag should generally be specified for interactive display
		but not for rasterization. */
	kAIDrawArtFastColorTransform				= 17,
	kAIDrawArtDontCachePatterns					= 18,
	kAIDrawArtIgnoreTopLevelInvisibility		= 19,
	kAIDrawArtKnockoutAlreadyOn					= 20,
	kAIDrawArtRunInBackground					= 21,
	/** When set, client will not attempt to continue an interrupted render.
		Allows for more interruptability. */
	kAIDrawArtPreviewCannotContinue				= 22,
	/** If the art object passed to AIDrawArtSuite::DrawArt() is a clipping object
		then it will not normally be drawn. Setting this flag will cause only clipping
		objects to be drawn. Those objects will establish a clip in the target port. */
	kAIDrawArtClipOnly							= 23,
	/** Causes the artwork to be drawn into an OPP port thus simulating the appearance
		of spot colors. Note that as with kAIDrawArtIsolate this will isolate any
		blending modes from the backdrop. AIDrawArtSuite::BeginDrawArt() can be used
		if multiple calls to DrawArt() are required and the objects drawn should not
		be isolated from one another. The spot plates needed are determined from the
		artwork that is to be drawn. */
	kAIDrawArtOPP								= 24
};

/** Flag Masks for #AIDrawArtFlags */
enum AIDrawArtFlagsValue {
	kAIDrawArtPreviewMask						= (1 << kAIDrawArtPreviewBit),
	kAIDrawArtPreviewImagesMask					= (1 << kAIDrawArtPreviewImagesBit),
	kAIDrawArtPreviewDimmedImagesMask			= (1 << kAIDrawArtPreviewDimmedImagesBit),
	kAIDrawArtPreviewImageOrDimImageMask		= (kAIDrawArtPreviewImagesMask | kAIDrawArtPreviewDimmedImagesMask),
	kAIDrawArtPreviewPatternsMask				= (1 << kAIDrawArtPreviewPatternsBit),
	kAIDrawArtSelectObjectsMask					= (1 << kAIDrawArtSelectObjectsBit),
	kAIDrawArtShowGuidesMask					= (1 << kAIDrawArtShowGuidesBit),
	kAIDrawArtIgnoreArtTransformMask			= (1 << kAIDrawArtIgnoreArtTransformBit),
	kAIDrawArtPreviewCropMask					= (1 << kAIDrawArtPreviewCropBit),
	kAIDrawArtPreviewInterruptMask				= (1 << kAIDrawArtPreviewInterrupt),
	kAIDrawArtPreviewContinueMask				= (1 << kAIDrawArtPreviewContinue),
	kAIDrawArtIgnoreDashedLinesMask				= (1 << kAIDrawArtIgnoreDashedLines),
	kAIDrawArtDontColorCalibrationMask			= (1 << kAIDrawArtDontColorCalibration),
	kAIDrawArtIsolateMask						= (1 << kAIDrawArtIsolate),
	kAIDrawArtIgnoreTopLevelTransparencyMask	= (1 << kAIDrawArtIgnoreTopLevelTransparency),
	kAIDrawArtSimulateColoredPaperMask			= (1 << kAIDrawArtSimulateColoredPaper),
	kAIDrawArtIgnoreOverprintMask				= (1 << kAIDrawArtIgnoreOverprint),
	kAIDrawArtPlayEPSMask						= (1 << kAIDrawArtPlayEPS),
	kAIDrawArtFastColorTransformMask			= (1 << kAIDrawArtFastColorTransform),
	kAIDrawArtDontCachePatternsMask				= (1 << kAIDrawArtDontCachePatterns),
	kAIDrawArtIgnoreTopLevelInvisibilityMask	= (1 << kAIDrawArtIgnoreTopLevelInvisibility),
	kAIDrawArtKnockoutAlreadyOnMask				= (1 << kAIDrawArtKnockoutAlreadyOn),
	kAIDrawArtRunInBackgroundMask				= (1 << kAIDrawArtRunInBackground),
	kAIDrawArtPreviewCannotContinueMask			= (1 << kAIDrawArtPreviewCannotContinue),
	kAIDrawArtClipOnlyMask						= (1 << kAIDrawArtClipOnly),
	kAIDrawArtOPPMask							= (1 << kAIDrawArtOPP)
};


/** Values for #AIDrawArtOutputType */
enum AIDrawArtOutputTypeValue {
	kAIDrawArtUnknownOutput						= 0,
	/** Indicates that a Macintosh GWorld is supplied as the port member of the output
		union in the AIDrawArtData. */
	kAIDrawArtGWorldOutput						= 1,
	/** Indicates that a CAGMPort* is supplied as the port member of the output union
		in the AIDrawArtData. */
	kAIDrawArtAGMPortOutput						= 2,
	/** Indicates that a AGMPort* is supplied as the portV6 member of the output union
		in the AIDrawArtData. */
	kAIDrawArtAGMPortOutputV6					= 3,
	/** Indicates that a AIStructuredPort* is supplied as the structPort member
		of the output union in the AIDrawArtData. */
	kAIDrawArtStructuredPortOutput				= 4
};

/** Values for #AIDrawColorStyle */
enum AIDrawColorStyleValue {
	kAIDrawColorStyleFill						= 0,
	kAIDrawColorStyleFrame						= 1
};


/** Values for #AIDrawColorOptions */
enum AIDrawColorOptionsValue {
	kAIDrawArtGradientForceLinear				= (1 << 0L),
	/** Don't convert colors to the document color model prior to drawing them.
		This allows drawing of out of gamut RGB colors when the current document
		color space is CMYK. */
	kAIDrawColorNoDocCsConversion				= (1 << 1L)
};


/** @ingroup Errors */
#define	kUnknownDrawArtErr					'DHUH'
/** @ingroup Errors */
#define KUnknownDrawArtOutputTypeErr		'DOUT'
/** @ingroup Errors */
#define	kBadDrawArtPreviewMatrixErr			'DMTX'
/** @ingroup Errors */
#define kDrawArtInterruptedErr				'DITR'


/*******************************************************************************
 **
 **	Types
 **
 **/

/** See #AIDrawArtFlagsValue for possible values. */
typedef unsigned long AIDrawArtFlags;
/** See #AIDrawArtOutputTypeValue for possible values. */
typedef long AIDrawArtOutputType;

/** When drawing into a Macintosh GWorld the following structure supplies
	the information needed. */
typedef struct AIDrawArtGWorld {
	void *worldPointer;
} AIDrawArtGWorld;

/** When drawing into an AGM port the following structure supplies
	the information needed. */
typedef struct AIDrawArtAGMPort {
	ADMAGMPort *port;
	AIRect portBounds;
} AIDrawArtAGMPort;

/** When drawing into an AGM port the following structure supplies
	the information needed. */
typedef struct AIDrawArtAGMPortV6 {
	void *port;
	AIRect portBounds;
} AIDrawArtAGMPortV6;

/** The possible output targets for drawing art. */
typedef union {
	AIDrawArtGWorld gWorld;
	AIDrawArtAGMPort port;
	AIDrawArtAGMPortV6 portV6;
	struct AIStructuredPort* structPort;
} AIDrawArtOutputUnion;

/** Structure defining an art object to be drawn, the object it is
	to be drawn into and parameters affecting how it is to be
	drawn. */
typedef struct AIDrawArtData {
	short version;

	/** Combination of flags modifying the behaviour. See AIDrawArt.h for details. */	
	AIDrawArtFlags flags;
	/** The type of object being drawn into. The appropriate field of the "output"
		union must be filled in based on this type. See AIDrawArt.h for
		possible values. */
	AIDrawArtOutputType type;
	AIRealPoint origin;
	AIRealMatrix matrix;
	/** The artwork object that is to be drawn. */
	AIArtHandle art;
	AIRealRect destClipRect;
	/** If true then destClipRect will be erased before the art is drawn. */
 	AIBoolean eraseDestClipRect;
	AIArtHandle interruptedArt;

	AIReal greekThreshold;
	AIExtendedRGBColorRec selectionColor;
	
	AIDrawArtOutputUnion output;
} AIDrawArtData;


/** See #AIDrawColorStyleValue for possible values. */
typedef long AIDrawColorStyle;
/** See #AIDrawColorOptionsValue for possible values. */
typedef long AIDrawColorOptions;

/** Structure defining a color to be drawn, the object it is
	to be drawn into and parameters affecting how it is to be
	drawn. */
typedef struct AIDrawColorData {
	AIDrawArtOutputType type;

	AIDrawColorStyle style;
	AIDrawColorOptions options;
	AIColor color;
	AIRealRect rect;
	AIReal width;							// when style is frame
	
	union {
		AIDrawArtGWorld gWorld;
		AIDrawArtAGMPort port;
		AIDrawArtAGMPortV6 portV6;
	} output;
} AIDrawColorData;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	The draw art suite APIs can be used to draw Illustrator artwork to an
	output port (drawing surface). The port can either be contained within
	a window in the UI or it can be an offscreen drawing surface being used
	to rasterize artwork. There are a considerable number of options for
	controlling the drawing process.
 */
typedef struct {

	/** Draw artwork as specified by the AIDrawArtData parameter. The options parameter
		indicates whether the artwork is being drawn for screen display, for export,
		or simply to play it into the port. This affects the way in which the artwork
		is drawn. In particular:
		
		When drawing for screen display the setting of the screen display preference for
		whether CMYK to RGB/Gray color transformations should map CMYK 0,0,0,1 to the
		darkest black is honored.

		When drawing for export the setting of the print and export preference for
		whether CMYK to RGB/Gray color transformations should map CMYK 0,0,0,1 to the
		darkest black is honored.
	*/
	AIAPI AIErr (*DrawArt)( AIDrawArtData *data, const AIColorConvertOptions& options );
	/** BeginDrawArt() and EndDrawArt() may need to be called around a sequence of calls
		to DrawArt(). They are needed when drawing to a port whose color space does
		not match the document color space. For example drawing a CMYK document to an
		RGB window or an RGB document to a window with a different profile. The begin
		call establishes a temporary drawing surface whose color space matches the
		document space. The end call then copies the contents of the temporary drawing
		surface to the final destination. The options parameter affects color conversions
		done when copying to the destinaiton. The #kAIDrawArtOPP flag may be passed in
		the flags parameter in order to cause drawing to be performed in an OPP port. Note
		that if this flag is passed the call to BeginDrawArt() will temporarily change
		the target port in the data parameter. Calling EndDrawArt() will restore the
		original port. Additionally the set of spot colors needed for the OPP port are
		determined from the art object specified in the data parameter (or the entire
		document if NULL). */
	AIAPI AIErr (*BeginDrawArt)( AIDrawArtData * data, const AIColorConvertOptions& options, AIDrawArtFlags flags );
	/** See BeginDrawArt() for details. */
	AIAPI AIErr (*EndDrawArt)( AIDrawArtData *data, const AIColorConvertOptions& options );
	/** Draw a color swatch as specified by the AIDrawColorData parameter. Color swatches
		are always assumed to be drawn for the purpose of screen display. */
	AIAPI AIErr (*DrawColorSwatch)( AIDrawColorData *data );
	/** This method can only be called from the code that responds to an annotator's draw
		annotations message. It causes the highlighting annotations for the specified art
		object to be drawn using the specified color. */
	AIAPI AIErr (*DrawHilite)( AIArtHandle art, AIRGBColor* color );
	/** A convenience API for drawing a thumbnail of the art object to the AGM port with
		the specified bounds. Thumbnails are always assumed to be drawn for the purpose
		of screen display. */
	AIAPI AIErr (*DrawThumbnail) ( AIArtHandle art, void* port, AIRealRect* dstrect );

} AIDrawArtSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
