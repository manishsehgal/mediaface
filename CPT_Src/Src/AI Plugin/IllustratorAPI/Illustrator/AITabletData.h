#ifndef __AITabletData__
#define __AITabletData__

/*
 *        Name:	AITabletData.h
 *   $Revision: 3 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	
 *
 * Copyright (c) 1986-1998 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AITabletData.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAITabletDataSuite			"AI TabletData Suite"
#define kAITabletDataSuiteVersion	AIAPI_VERSION(1)
#define kAITabletDataVersion		kAITabletDataSuiteVersion


/** Types of data that can be stored with a path. */
typedef enum {
	kTabletPressure = 0,
	kTabletStylusWheel, //aka tangential pressure aka barrel pressure
	kTabletTilt, // aka altitude
	kTabletBearing, // aka azimuth
	kTabletRotation,
	
	kTabletTypeCount //Put all new tablet types before this
} AITabletDataType;


/** Tablet data is described by an array of (offset, value) pairs. Both values are
	in the range [0,1]. Internally the data is stored as an array of small integers
	such that the offset have a maximum resolution of 1/100 and the values have
	a maximum resolution of 1/256.
 */
typedef struct {
	AIReal offset;
	AIReal value;
} AITabletProfile;

/** @ingroup Errors */
#define kNotEnoughSpace		'!spc'

/*******************************************************************************
 **
 **	Suite
 **
 **/

/** Tablet data can be attached to path objects. This suite contains API to inspect
	and manipulate the data.
 */
typedef struct {

	/** Get the tablet data of the specified type attached to the path. The client must supply
		an array in profile and specify the size of the array in count. On return, if no tablet
		data was present then profile is set to NULL and count is set to zero. If the array
		is not large enough then count is set to the size of array needed and #kNotEnoughSpace
		is returned. Otherwise count will be adjusted to the number of samples of the array that
		were filled out. */
	AIAPI AIErr (*GetTabletData) ( AIArtHandle path, AITabletProfile **profile, int *count, AITabletDataType type );
	/** Set the tablet data of the specified type attached to the path. */
	AIAPI AIErr (*SetTabletData) ( AIArtHandle path, AITabletProfile *profile, int count, AITabletDataType type );
	AIAPI AIErr (*GetTabletDataInUse) ( AIArtHandle path, AIBoolean *inUse );
	AIAPI AIErr (*SetTabletDataInUse) ( AIArtHandle path, AIBoolean inUse );
	AIAPI AIErr (*SplitTabletData) ( AITabletProfile *orig, int origCount,
			AITabletProfile **part1, int *part1Count, AITabletProfile **part2, int *part2Count,
			AIReal split );
	AIAPI AIErr (*ExtendTabletData) ( AITabletProfile *orig, int origCount,
			AITabletProfile **extendedProfile, int *extendedCount, AIReal newStart, AIReal newEnd );
	AIAPI AIErr (*JoinTabletData) ( AITabletProfile *part1, int part1Count,
			AITabletProfile *part2, int part2Count, AITabletProfile **joined, int *joinedCount,
			AIReal part1Ends, AIReal part2Begins);

} AITabletDataSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
