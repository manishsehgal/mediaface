#ifndef __AIImageOptimization__
#define __AIImageOptimization__

/*
 *	Name:	AIImageOptimization.h
 *	Purpose:	Adobe Illustrator 10.0 Image Optimization Suite
 *
 *	Copyright (c) 1986-2003 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

/*
 *	Imports
 */

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIDataFilter__
#include "AIDataFilter.h"
#endif

#ifndef __ASTypes__
#include "ASTypes.h"
#endif

#include "AIRasterize.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIImageOptimization.h */

/*
 *	Constants
 */

#define kAIImageOptSuite "AI Image Optimization Suite"
#define kAIImageOptSuiteVersion1 AIAPI_VERSION(1)
#define kAIImageOptSuiteVersion2 AIAPI_VERSION(2)
#define kAIImageOptSuiteVersion3 AIAPI_VERSION(3)
#define kAIImageOptSuiteVersion	kAIImageOptSuiteVersion3

/** @ingroup Errors 
	Generic error that is returned if anything goes wrong. */
#define kAIImageOptErr 'IMer'

/*
 *	AIImageOptSuiteVersion1 Types
 */

/** GIF */
struct AIImageOptGIFParams{

	bool interlaced;
	bool transparencyAvailable;
	ASInt32 numberOfColors;
	ASInt32 transparentIndex;
	ASInt32 loss;

	/** pixels/inch */
	AIFloat resolution;

	bool outAlpha;
	ASInt32 outWidth;
	ASInt32 outHeight;
};

enum AIImageOptJPEGKind {

	AIImageOptJPEGStandard,
	AIImageOptJPEGOptimized,
	AIImageOptJPEGProgressive
};

/** JPEG */
struct AIImageOptJPEGParams{

	ASInt32 quality;
	AIImageOptJPEGKind kind;
	ASInt32 scans;
	AIFloat blurAmount;

	/** pixels/inch */
	AIFloat resolution;

	bool outAlpha;
	ASInt32 outWidth;
	ASInt32 outHeight;
};

/** PNG */
struct AIImageOptPNGParams {

	bool interlaced;
	ASInt32 numberOfColors;
	ASInt32 transparentIndex;

	/** pixels/inch */
	AIFloat resolution;

	bool outAlpha;
	ASInt32 outWidth;
	ASInt32 outHeight;
};

enum AIImageOptZLIBLevel {

	AIImageOptZLIBDefault = -1,
	AIImageOptZLIBNone = 0,
	AIImageOptZLIBFast = 1,
	AIImageOptZLIBSmall = 9
};

/** ZLIB */
struct AIImageOptZLIBParams{

	AIImageOptZLIBLevel level;

	/** pixels/inch */
	AIFloat resolution;

	bool outAlpha;
	ASInt32 outWidth;
	ASInt32 outHeight;
};

/*
 *	AIImageOptSuiteVersion2 Types
 */

/** PNG */
struct AIImageOptPNGParams2 {

	AIImageOptPNGParams versionOneSuiteParams;

	/** if true, text is antialiased when rasterizing. */
	bool antialias;

	/** a cropping box for the AIArtHandle can be specified.
		an empty (or degenerate) crop box is interpret to mean "do not crop". */
	AIRealRect cropBox;

	/** if true, rasterization is done against a transparent background;
		that is, no matte color is applied. */
	bool backgroundIsTransparent;
	
	/** if backgroundIsTransparent is false, rasterization is done against this
		matte color. */
	AIRGBColor matteColor;

#ifdef __cplusplus
	/** Constructor */
	AIImageOptPNGParams2()
		:
			versionOneSuiteParams(),
			antialias(false),	// no text antialiasing
			cropBox(),
			backgroundIsTransparent(true),	// no matte color applied
			matteColor()
	{
		cropBox.top = 0.0; cropBox.left = 0.0; cropBox.bottom = 0.0; cropBox.right = 0.0;
		matteColor.red = 0; matteColor.green = 0; matteColor.blue = 0;
	};
#endif
};




/** Version 3 Stuff. */


/** This is a reference to an opaque color palette object. It is never dereferenced. */
typedef struct _t_AIColorPalette *AIColorPaletteHandle;

/** This is a reference to an opaque inverse color palette object. It is never dereferenced. */
typedef struct _t_AIInverseColorPalette *AIInverseColorPaletteHandle;


/** AIPaletteType an enumeration of the different types of palette construction options available.
*/
typedef short AIPaletteType;
enum AIPaletteTypeValue 
{
	/** Automatically generated palette, base on the input image. */
	kAIAutomaticPalette,
	
	/** Custom palette, based on pre-canned palette. */
	kAICustomPalette
};


/** The AIPaletteOptions structure describes construction options for the palette used
  * in color reduction (posterization) of the image.
  */
typedef struct AIPaletteOptions
{
	AIPaletteType paletteType;
	
	// colorCount is used only if kAIAutomaticPalette is in effect.  if using a custom palette then 
	// the color count is defined by the use of the SetPaletteColor calls.
	long colorCount; 

} AIPaletteOptions;


/** The AIPaletteColor structure.
  */
struct AIPaletteColor
{
    unsigned char alpha;
    unsigned char r;
    unsigned char g;
    unsigned char b;
};


/** The image optimization suite provides APIs that rasterize an art object and
	stream the resulting raster to a datafilter in various image formats. */
typedef struct {

	// AIImageOptSuiteVersion1 functions

	AIAPI AIErr (*AsGIF)(
		AIArtHandle aiArt,
		AIDataFilter* aiDataFilter,
		AIImageOptGIFParams& params);

	AIAPI AIErr (*AsJPEG)(
		AIArtHandle aiArt,
		AIDataFilter* aiDataFilter,
		AIImageOptJPEGParams& params);

	AIAPI AIErr (*AsPNG)(				// as PNG24
		AIArtHandle aiArt,
		AIDataFilter* aiDataFilter,
		AIImageOptPNGParams& params);

	AIAPI AIErr (*AsZLIB)(
		AIArtHandle aiArt,
		AIDataFilter* aiDataFilter,
		AIImageOptZLIBParams& params);

	// AIImageOptSuiteVersion2 functions

	AIAPI AIErr (*MakePNG24)(
		AIArtHandle aiArt,
		AIDataFilter* aiDataFilter,
		AIImageOptPNGParams2& params,
		AIRasterizeProgressProc progressProc);


	// AIImageOptSuiteVersion3 functions

	/** Color palette creation. */
	AIAPI AIErr (*CreatePalette) (AIArtHandle raster, const AIPaletteOptions *options, AIColorPaletteHandle *palette);
	AIAPI AIErr (*DisposePalette) (AIColorPaletteHandle palette);
	AIAPI AIErr (*GetPaletteColor) (AIColorPaletteHandle palette, long index, AIPaletteColor *color);
	AIAPI AIErr (*SetPaletteColor) (AIColorPaletteHandle palette, long index, AIPaletteColor *color);

	/** Color quantization. */
	AIAPI AIErr (*CreateInversePalette) (AIColorPaletteHandle palette, AIInverseColorPaletteHandle *inverse);
	AIAPI AIErr (*DisposeInversePalette) (AIInverseColorPaletteHandle inverse);
	AIAPI AIErr (*GetPaletteIndex) (AIInverseColorPaletteHandle inverse, const AIPaletteColor *color, long *index);

}
AIImageOptSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif	// __AIImageOptimization__
