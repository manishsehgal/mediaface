#ifndef __AIRaster__
#define __AIRaster__

/*
 *        Name:	AIRaster.h
 *   $Revision: 24 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator Raster Object Suite.
 *
 * Copyright (c) 1986-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#include "IAIFilePath.hpp"

extern "C" {

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIRaster.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIRasterSuite		"AI Raster Suite"
#define kAIRasterSuiteVersion6	AIAPI_VERSION(6)
#define kAIRasterSuiteVersion	kAIRasterSuiteVersion6
#define kAIRasterVersion		kAIRasterSuiteVersion


/** Flags that may be specified for the AIRasterRecord::flags member. */
enum AIRasterFlags {
	/** May only be specified for 1-bit gray images. Indicates that the 1-bit data
		is to be used as a mask. In this case the path style of the image indicates
		the color that the bits of the image with a value of 1 are painted. */
	kRasterMaskImageType		= 0x0002,
	/** May only be specified for 1-bit gray images. Reverses the sense of the component
		values so that for a gray image 0=white and 1=black and for an image mask
		the 0 value pixels are painted. */
	kRasterInvertBits			= 0x0004,
	/** May only be specified for 8-bit spot color rasters (16-bit if there is an extra 
	    alpha channel). Indicates the gray component values are to be interpreted in 
		the subtractive color spaces. */
	kRasterGraySubtractive		= 0x0008
};

/** Maximum number of channels per image pixel */
#define kMaxChannels  16


/** Color models for raster data */
enum AIRasterColorSpace {
	/** Flag indicating that the color model has an alpha channel. The alpha
		component appears after the color components. */
	kColorSpaceHasAlpha			= 0x10,

	/** Each pixel value for a gray color space consists of a single component
		describing a grayscale value. The gray color space is additive so the minimum
		value represents black and the maximum represents white. */
	kGrayColorSpace				= 0,
	/** Each pixel value for a RGB color space consists of three components which
		represent red, green and blue respectively. The RGB color space is additive. */
	kRGBColorSpace				= 1,
	/** Each pixel value for a CMYK color space consists of four components which
		represent cyan, magenta, yellow and black respectively. The CMYK color space
		is subtractive. */
	kCMYKColorSpace				= 2,
	/** Not valid as an image type. This image type can only occur in placed linked
		files. See AIPlacedSuite::GetRasterInfo(). */
	kLabColorSpace				= 3,
	/** Not valid as an image type. This image type can only occur in placed linked
		files. See AIPlacedSuite::GetRasterInfo(). */
	kSeparationColorSpace		= 4,
	/** Not valid as an image type. This image type can only occur in placed linked
		files. See AIPlacedSuite::GetRasterInfo(). */
	kNChannelColorSpace			= 5,
	/** Not valid as an image type. This image type can only occur in placed linked
		files. See AIPlacedSuite::GetRasterInfo(). */
	kIndexedColorSpace			= 6,
	
	kAlphaGrayColorSpace		= (kGrayColorSpace | kColorSpaceHasAlpha),
	kAlphaRGBColorSpace			= (kRGBColorSpace  | kColorSpaceHasAlpha),
	kAlphaCMYKColorSpace		= (kCMYKColorSpace | kColorSpaceHasAlpha),
	kAlphaLabColorSpace			= (kLabColorSpace | kColorSpaceHasAlpha),
	kAlphaSeparationColorSpace	= (kSeparationColorSpace | kColorSpaceHasAlpha),
	kAlphaNChannelColorSpace	= (kNChannelColorSpace | kColorSpaceHasAlpha),
	kAlphaIndexedColorSpace		= (kIndexedColorSpace | kColorSpaceHasAlpha),

	kInvalidColorSpace			= 0xFF
};

/** @ingroup Errors */ 
#define kRasterTypeNotSupportedErr			'RTYP'
/** @ingroup Errors
	raster has already been specified */ 
#define kRasterSpecifiedErr					'RSPE'
/** @ingroup Errors
	raster bits per pixel not supported */ 
#define kRasterBitsPerPixelsNotSupportedErr	'RBIT'
/** @ingroup Errors */ 
#define kRasterLinkFileNotFoundErr			'RFNF'
/** @ingroup Errors */ 
#define kRasterLinkPluginNotFoundErr		'RPNF'
/** @ingroup Errors */ 
#define kRasterScratchDiskFullErr			'RFUL'


/** Link states for the AIRasterLink::linkstate member. Direct linking of images is deprecated
	but still supported. All linked objects should be created using the AIPlacedSuite. */
enum AIRasterLinkState {
	/** Value indicating that the image is linked when saving native round trip information. That is
		the image data is not saved into the file. */
	kAIRasterLinked				= 0L,
	/** Value indicating that the image is embedded when saving native round trip information. */
	kAIRasterEmbedded			= 1L
};

/** Data states for the AIRasterLink::datastate member. Direct linking of images is deprecated
	but still supported. All linked objects should be created using the AIPlacedSuite. */
enum AIRasterDataState {
	/** Indicates that no data is available for the image. This happens for example when a
		document is opened and the linked file cannot be found. */
	kAIRasterNoData				= 0L,
	/** Indicates that the data is available and that it is the same data that was read from
		the linked file (i.e it has not been modified since being read). */
	kAIRasterLinkData			= 1L,
	/** Indicates that the data is available and it has been modified since it was read from
		the file. For example a Photoshop filter might have been run on the image. */
	kAIRasterEditedData			= 2L
};

/** Flags for AIRasterSuite::ResolveRasterLink(). Direct linking of images is deprecated
	but still supported. All linked objects should be created using the AIPlacedSuite. */
enum AIResolveRasterLinkFlags {
	/** Only update if the data isn't embedded */
	kAIResolveCheckExternal		= (1L<<0),
	/** Only update if the file has changed */
	kAIResolveCheckFile			= (1L<<1),
	/** Report errors to user and show progress */
	kAIResolveVerbose			= (1L<<2),
	/** Allow a replacement file to be selected in case of error */
	kAIResolveReplace			= (1L<<3),
	/** Do automatic searching for a replacement */
	kAIResolveSearch			= (1L<<4)
};

/*******************************************************************************
 **
 **	Types
 **
 **/

// Ken Abrams - TODO: add this ifdef to source control.
#ifndef BRAVO_TYPES
typedef	signed char		int8;
typedef unsigned char	uint8;
typedef	signed short	int16;
typedef unsigned short	uint16;
typedef signed long		int32;
typedef unsigned long	uint32;
#endif

/**	Slices are used to define a subset of a raster data to be copied or replaced. It
	may be impractical to work with all the information of a raster art object at
	one time, so a slice would be used to work with a subset. The slice of the
	source and destination should be of equal dimension, but may differ in location.

	The top, left, bottom, and right fields of the structure are the rectangle
	containing the pixel data. If a plug-in is iterating through a raster object
	accessing slices of pixels, these values would change, controlling which
	section of the pixel map is being processed. It is likely that these slice values
	for the local pixel data will be the same as the bounding rectangle of the AITile,
	though the local tile slice can also be iterated.

	The front and back of the source slice fields define which of the pixel
	channels should be copied. When copying from the raster object to the plugin
	local data, the raster object slice's front and back fields are used. When
	copying from the plug-in local data, the AITile slice's front and back fields
	are used.
 */
typedef struct AISlice {
	long top, left, bottom, right, front, back;
} AISlice;


/** A tile describes local storage for copying image data to and from. It is used
	with the AIRasterSuite::GetRasterTile() and AIRasterSuite::SetRasterTile() APIs.
	
	The channelInterleave array and the slice information specify changes in the
	order of the bytes making up a pixel when transfering between a raster
	object and the tile�s data. If there is no change in the order of the data
	order the array would be:

@code
	myTile.channelInterleave[0] = 0;
	myTile.channelInterleave[1] = 1;
	myTile.channelInterleave[2] = 2;
	myTile.channelInterleave[3] = 3;
	...
@endcode

	Using this channelInterleave the plug-in would be using the pixel data in
	the same format as it is kept internally.

	If there are changes in the byte order of the color values for some reason,
	the channelInterleave maps the source bytes of the transfer to the destination
	bytes. The component index of the source channel being transferred determines
	the index into the channelInterleave array. The value stored in the array at
	that index determines the destination component index.

	An example of using the channel interleave is transfering from the internal
	RGB format to a Macintosh GWorld PixMap. The internal format uses 3 bytes
	in the order R G B to define a pixel. The GWorld uses 4 bytes in the order A
	R G B, where the A byte is alpha channel information.

	When transfering between the raster art object and the GWorld, the channel
	interleave is used to specify the mapping between the two incompatible
	forms. To transfer from the raster object to the GWorld this mapping would
	be used:

@code
	// Map 24-bit red value to 32-bit red value
	GWorldTile.channelInterleave[0] = 1;
	// Map 24-bit green value to 32bit green value
	GWorldTile.channelInterleave[1] = 2;
	// Map 24-bit blue value to 32-bit blue value
	GWorldTile.channelInterleave[2] = 3;
	// Unused, handled by the slice definition
	GWorldTile.channelInterleave[3] = 0;
@endcode

	Notice that only three channels are mapped since the alpha channel of the
	GWorld receives no information. To transfer from the GWorld back to the
	raster object, this mapping would be used:

@code
	// Alpha channel is ignored, will be handled by the slice definition
	GWorldTile.channelInterleave[0] = 0;
	// Map 32-bit red value to 24-bit red value
	GWorldTile.channelInterleave[1] = 0;
	// Map 32-bit green value to 24bit green value
	GWorldTile.channelInterleave[2] = 1;
	// Map 32-bit blue value to 24-bit blue value
	GWorldTile.channelInterleave[3] = 2;
@endcode

	Here the data in the alpha channel of the GWorld is not transfered back to
	the raster object. Using the slice values to ignore ignore channelInterleave
	values is discussed below.

	The plug-in would set the art object slice's front and back fields to limit
	the channelInterleave array. The front field is the first channel to be affected
	and the back field is the last channel + 1, thus:

@code
	// Map 24-bit red value to 32-bit red value
	GWorldTile.channelInterleave[0] = 1;
	// Map 24-bit green value to 32bit green value
	GWorldTile.channelInterleave[1] = 2;
	// Map 24-bit blue value to 32-bit blue value
	GWorldTile.channelInterleave[2] = 3;
	// Unused, will be handled by the slice definition
	GWorldTile.channelInterleave[3] = 0;
	artSlice.front = 0;
	artSlice.back = 3;
@endcode

	Since artSlice.back is set to 3, only the color information for the first 3
	channels will be copied. When transfering from the GWorld back to the
	raster object, the GWorld's slice controls the channelInterleave. To skip
	the alpha channel, GWorldSlice.front is set to channel 1, causing channel
	0 to be ignored.

@code
	// Alpha channel is ignored, will be handled by the slice definition
	GWorldTile.channelInterleave[0] = 0;
	// Map 32-bit red value to 24-bit red value
	GWorldTile.channelInterleave[1] = 0;
	// Map 32-bit green value to 24bit green value
	GWorldTile.channelInterleave[2] = 1;
	// Map 32-bit blue value to 24-bit blue value
	GWorldTile.channelInterleave[3] = 2;
	GWorldSlice.front = 1;
	GWorldSlice.back = 4;
@endcode
*/
typedef struct AITile {
	/** A pointer to the memory containing the local copy of the pixels. It can be an
		generic block of memory or a platform specific data structure. In any case it
		must be allocated by the plug-in. */
	void		*data;
	/** The bounds field of the tile is the rectangle containing the pixel map, but
		also contains the depth information of the slice. */
	AISlice		bounds;
	/** Specifies the number of bytes in a row of pixel data. */
	long		rowBytes;
	/** The number of bytes used to specify a single pixel. For a raw RGB pixel, it
		would be 3. */
	long		colBytes;
	/** Indicates whether the tile is to receive the pixel data in a chunky mode (all
		color data for a single pixel together) or seperated into planes (all data for
		a color component together). If planeBytes is 0, the pixel data is chunky. If
		it is non-zero, the pixel information is in a planar form and the value is the
		number of bytes making up a single plane. For instance if pixel map were 80 by
		80 pixels and the RGB data were in a planar form, the value of planeBytes would
		be 6400. */
	long		planeBytes;
	/** The channelInterleave array and the slice information specify changes in the
		order of the bytes making up a pixel when transfering between a raster
		object and the tile's data. See the description of the AITile structure for
		more information. */
	short		channelInterleave[kMaxChannels];
} AITile;


/* The two different kinds of color tables supported (rgb and cmyk). */
typedef struct AIRGBColorRec	{
	uint8	mustBeZero;		// MUST BE ZERO!!!!
	uint8	red;
	uint8	green;
	uint8	blue;
} AIRGBColorRec;

typedef struct AIExtendedRGBColorRec {
	uint16	mustBeZero;		// MUST BE ZERO!!!!
	uint16	red;
	uint16	green;
	uint16	blue;
} AIExtendedRGBColorRec;

typedef struct AICMYKColorRec	{
	uint8	cyan;
	uint8	magenta;
	uint8	yellow;
	uint8	black;
} AICMYKColorRec;


/** The AIRasterRecord describes the configuration of the image data. It is used
	primarily with the AIRasterSuite::GetRasterInfo() and AIRasterSuite::SetRasterInfo()
	APIs. */
typedef struct AIRasterRecord	{
	/** Flags defining additional options for the image. See #AIRasterFlags. */
	uint16			flags;
	/** The width and height of the image data in pixels. The top left value of
		the bounds must be zero. The right value gives the width and the bottom
		value the height. */
	AIRect			bounds;
	/** The number of bytes required to store a single row of pixel data. This value
		need not be specified for the AIRasterSuite::SetRasterInfo() API. 1-bit
		data is packed so that there are 8 pixels per byte. */
	int32			byteWidth;
	/** The color model of the pixel data. This may include an alpha channel. See
		#AIRasterColorSpace. */
	int16			colorSpace;
	/** The number of bits per pixel. This is the number of bits per color space
		component times the number of components. */
	int16			bitsPerPixel;
	/** if -1 the raster has not been through the color converter otherwise it is
		the color space before conversion. */
	int16			originalColorSpace;
} AIRasterRecord;


/** Structure describing link information about an image. Direct linking of images is
	deprecated but still supported. All linked objects should be created using the
	AIPlacedSuite. */
struct AIRasterLink {
	/** The source file for the image data (linked file). */
	ai::FilePath file;
	/** The link state. See #AIRasterLinkState. */
	long			linkstate;
	/** The data state. See #AIRasterDataState. */
	long			datastate;
};


/** Parameters for controlling the curve fitting used when outlining a raster. The
	parameters may be passed as nil in which case defaults are used. */
typedef struct AIRasterOutlineParams {
	int noiseFilterDegree;
	double cornerAngleTolerance;
	double smoothnessTolerance;
	double fidelityTolerance;
} AIRasterOutlineParams;

/** When extracting an outline from a raster the curves are enumerated to
	an outline consumer. */
typedef struct AIRasterOutlineConsumer {
	AIAPI AIErr (*BeginComponent) ( void* self, long knots );
	AIAPI AIErr (*PutKnot) ( void* self, AIRealPoint in, AIRealPoint p, AIRealPoint out);
	AIAPI AIErr (*EndComponent) ( void* self );
} AIRasterOutlineConsumer;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	The Raster Suite enables you to access and modify the data associated with
	an art object of type #kRasterArt.

	The functions allow you to work with the art as an object, for instance by
	getting and setting its transformation matrix, or at a pixel level by using the
	get and set tile functions.

	The suite is used in conjunction with at least one additional suite for
	accessing or creating the raster art object reference (the AIArtSuite, the
	AIMatchingArtSuite, or the AIArtSetSuite).

	A raster object is defined by its pixel data and a matrix that positions the
	pixel data in the document. The configuration of the pixel data is defined
	by its AIRasterRecord.

	The bounds of the raster object are initialized to zero when the AIArtSuite::NewArt()
	API is first used to create a raster art object. They should be set to the rectangle
	containing the pixel map using the SetRasterInfo call. This is not necessarily the
	same rectangle as the artwork bounds, which Illustrator will calculate and provide.
	The origin of a raster object is in the top left corner. This means that the coordinate
	system used by a raster object is inverted on the vertical axis compared to the
	Illustrator artboard coordinate system.

	All pixel data in an Adobe Illustrator raster object uses a base resolution of
	72 pixels per inch (ppi). Other resolutions cannot be directly specified.
	Instead, the bounds and the raster matrix are used to set a resolution. For
	a one inch square image at 72 ppi, the scale component of the raster
	matrix is set to 1.0 and the bounds would be 72 x 72. To have a one inch square
	image at 300 ppi, the bounds would be 300 x 300 and the scale component
	of the raster matrix would be 0.24 (72/300).

	To get or set the value of the pixel map data, a plug-in uses the raster suites
	GetRasterTile() and SetRasterTile() functions. The get function retrieves pixels
	from a pixel map while the set function copies pixel information to a pixel
	map. The plug-in's local data to or from which the pixels copied is called a
	tile and is described by the AITile structure.
 */
typedef struct AIRasterSuite {

	/** This call provides the basic information a plug-in needs to use other Raster
		Suite calls, such as GetRasterTile(), on a raster art object. */
	AIAPI AIErr (*GetRasterInfo) ( AIArtHandle raster, AIRasterRecord *info );
	/** When a raster art object is first created, the raster record is unitialized. The
		AIArtSuite::NewArt() call should be followed by SetRasterInfo() to specify the
		objects color space, bounds, etc. */
	AIAPI AIErr (*SetRasterInfo) ( AIArtHandle raster, AIRasterRecord *info );
	/** Returns the value of the file member of the AIRasterLink information returned
		by GetRasterLink(). */
	AIAPI AIErr (*GetRasterFileSpecification) ( AIArtHandle raster, ai::FilePath& );
	/** Returns #kNotImplementedErr. */
	AIAPI AIErr (*SetRasterFileSpecification) ( AIArtHandle raster, const ai::FilePath& );
	/** Use this to get the transformation matrix of a raster art object. The transformation
		matrix of a raster object is the concatenation of all the transforms applied to
		the image after it was created. In addition to an initial translation, it will
		likely have a scale as described in SetRasterMatrix(). */
	AIAPI AIErr (*GetRasterMatrix) ( AIArtHandle raster, AIRealMatrix *matrix );
	/** Use this to set the transformation matrix of a raster art object. In addition
		to using the raster object�s matrix for general transforms, it is used to set
		the pixel resolution of the object. This function will be used to set this and
		other transformations. */
	AIAPI AIErr (*SetRasterMatrix) ( AIArtHandle raster, AIRealMatrix *matrix );
	/** Use this to get the raster object's bounds. This call will return the orginal
		bounding box of a raster object. This is not necessarily the same rectangle as
		the artwork bounds, which Illustrator will calculate and provide. */
	AIAPI AIErr (*GetRasterBoundingBox) ( AIArtHandle raster, AIRealRect *bbox );
	/** Use this to set the raster object�s bounds. This call will set the bounding box
		of a raster object. This is not necessarily the same rectangle as the artwork
		bounds, which Illustrator will calculate and provide. */
	AIAPI AIErr (*SetRasterBoundingBox) ( AIArtHandle raster, AIRealRect *bbox );
	/** Get a tile of data from the raster object. */
	AIAPI AIErr (*GetRasterTile) ( AIArtHandle raster, AISlice *artSlice, AITile *workTile, AISlice *workSlice );
	/** Copy pixel data in a local buffer to all or part of a raster object�s pixel map. */
	AIAPI AIErr (*SetRasterTile) ( AIArtHandle raster, AISlice *artSlice, AITile *workTile, AISlice *workSlice );

	/** Gets the link information specified for the raster object. Direct linking of
		images is deprecated but still supported. All linked objects should be created
		using the AIPlacedSuite. */
	AIAPI AIErr (*GetRasterLink) ( AIArtHandle raster, AIRasterLink &link );
	/** Sets the link information for the raster object. Direct linking of images is
		deprecated but still supported. All linked objects should be created using
		the AIPlacedSuite. */
	AIAPI AIErr (*SetRasterLink) ( AIArtHandle raster, const AIRasterLink &link );
	/** Resolve the link for the raster object. If needed the raster data will be
		reloaded from the source of the link. Note that the embedded and modified
		state of the link data is ignored. If succesful, the raster data will be marked
		as unmodified (i.e the modified flag is cleared), the embed flag is not
		changed.

		Attempting to resolve a link can fail for any of the following reasons:

		- no link source specified
		- link source not found (#kRasterLinkFileNotFoundErr)
		- a plugin which can load the link source data cannot be found
			(#kRasterLinkPluginNotFoundErr)
		- the link source data cannot be read
		- out of memory

		If the flags do not specify that resolution should be forced it will only be
		done if the linked to data is more recent than the current copy.
	*/
	AIAPI AIErr (*ResolveRasterLink) ( AIArtHandle raster, long flags );

	/** The SPPlatformFileInfo contains information about the linked file such as the time
		it was created and last modified. The raster object stores the information from
		the time the file was last read. This API returns the file information stored with
		the raster object. It can be compared against the current information for the file
		itself to determine if the file has changed. */
	AIAPI AIErr (*GetRasterFileInfoFromArt) ( AIArtHandle raster, SPPlatformFileInfo *pSPFileInfo );
	/** The SPPlatformFileInfo contains information about the linked file such as the time
		it was created and last modified. This API returns the file information of the linked
		file itself. It can be compared against the information stored with the raster object
		to determine if the file has changed. */
	AIAPI AIErr (*GetRasterFileInfoFromFile) ( AIArtHandle raster, SPPlatformFileInfo *pSPFileInfo );
	/** Get the path specification for the raster object. A platform specific path is
		returned. */
	AIAPI AIErr (*GetRasterFilePathFromArt) ( AIArtHandle raster, ai::UnicodeString &path );

	/** Raster data is stored in a MIP map. CountLevels() returns the number of levels in
		the map. This is presently hard coded at 6 but may change to be based on the source
		image dimensions in the future. */
	AIAPI AIErr (*CountLevels) ( AIArtHandle raster, long* count );
	/** GetLevelInfo returns an AIRasterRecord describing the format of the image
		data in a given level of the MIP map. The level number is zero based.

		- The flags are always a copy of the flags you'd get if querying the image
			as a whole.
		- The bounds are the bounds of the image at the given level of the MIP map.
			Each level is a power of two less than the previous one with odd dimensions
			being rounded up. Thus for example, levelWidth = (prevLevelWidth + 1) / 2.
			Of course you shouldn't rely on this algorithm. The only important property
			which will be maintained is that any rectangle contained within the base
			image is contained within an image at a given level after scaling to that
			level even if its dimensions are rounded up.
		- The byteWidth field should be ignored.
		- The colorSpace and bitsPerPixel describe the format of the pixel data at
			the given level of the MIP map. At present this is always the same as that
			of the base image but may change in the future. In particular the
			subsampled levels of a bitmap's MIP map are bitmaps--random sampling is
			used to avoid aliasing. In the future this may change to subsample
			into a grayscale image.
	*/
	AIAPI AIErr (*GetLevelInfo) ( AIArtHandle raster, long level, AIRasterRecord *info );
	/** GetLevelTile is equivalent to GetRasterTile but accesses the data from the
		given level of the MIP map. The level number is zero based.
	*/
	AIAPI AIErr (*GetLevelTile) ( AIArtHandle raster, long level, AISlice *artSlice,
			AITile *workTile, AISlice *workSlice );

	/** Extract an outline path from the image data. The params object provides parameters
		for the extraction process. The consumer receives the path data. */
	AIAPI AIErr (*ExtractOutline) ( AIArtHandle raster, const AIRasterOutlineParams* params,
			AIRasterOutlineConsumer* consumer );

	/** Post-concatenate a transformation onto the matrix applied to the placed object. */
	AIAPI AIErr (*ConcatRasterMatrix) ( AIArtHandle raster, AIRealMatrix *concat );

} AIRasterSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

}


#endif
