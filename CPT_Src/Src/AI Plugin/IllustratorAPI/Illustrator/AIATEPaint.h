/*
 *        Name:	AIATEPaint.h
 *   $Revision:
 *      Author:	 
 *        Date:	   
 *     Purpose:	Facilitates access of paint attributes applied to text.
 *				This suite serves as a bridge between ATE::ApplicationPaint
 *				and AIColor
 *
 *				REQUIREMENT: This won't compile without a C++ compiler!
 *
 * Copyright (c) 2002 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifndef __AIATEPAINT__
#define __AIATEPAINT__

/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AITypes.h"

#include "AIColor.h"
#include "AIEntry.h"
#include "AIPathStyle.h"

#include "ATESuites.h"


extern "C" {

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIATEPaint.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIATEPaintSuite			"AIATE Paint Suite"
#define kAIATEPaintSuiteVersion1	AIAPI_VERSION(1)
#define kAIATEPaintSuiteVersion		kAIATEPaintSuiteVersion1
#define kAIATEPaintVersion			kAIATEPaintSuiteVersion

/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	The main purpose of this suite is to connect the Adobe Text Engine (ATE) APIs
	with the Illustrator suites.
	
	The AIATEPaintSuite contains conversion routines that convert an AIColor
	to an ATE::ApplicationPaintRef and vice versa.  Additional methods are
	provided as convenience.  Most notable are the conversion of
	ATE::CharFeatures to AIPathStyle, AIPathStyleMap and vice versa.

	Because paint attributes are often partially defined, this suite REQUIRES
	the use of AIPathStyleMap.  All AIPathStyle instances are treated and
	returned as partial AIPathStyles.  In addition, Illustrator suites and ATE
	suites use slightly different memory management schemes, though both are
	simply reference counting schemes.

	Therefore, please note the following:

	FIRST: If you obtain an AIPathStyle from this suite, you must check its 
	corresponding AIPathStyleMap to determine what information in AIPathStyle 
	is valid.  No guarantees are made for invalid entries.

	SECOND: If you pass in an AIPathStyle and AIPathStyleMap, they must be a correct
	pairing.  Pairs that do not correspond produce undefined results.

	THIRD: The ATE suites do not provide the ability to instantiate, among many others,
	the following structures: ATE::_ApplicationPaint and ATE::_ArrayReal.
	Consequently, this suite contains two suite methods that instantiate these
	as ATE::ApplicationPaintRef and ATE::ArrayRealRef (their respective pointer
	types) as well as perform conversion from Illustrator types.

	Thus, both CreateATELineDashArray() and CreateATEApplicationPaint() expect
	addresses of NULL pointers for struct _ArrayReal and _ApplicationPaint,
	respectively.  In other words, ArrayRealRef and ApplicationPaintRef should
	be NULL; their addresses are passed into the above methods.

	Both suites do not provide protection against memory leaks.  If you pass in
	the address of a "valid" pointer for struct _ApplicationPaint, a.k.a. an
	ApplicationPaintRef, you will have a memory leak.  On the other
	hand, if you pass in an address for a NULL ("invalid") pointer,  
	a "valid" ApplicationPaintRef will be returned.  Below is an example of
	_correct_ usage:

@code
	// ApplicationPaintRef is NULL.  Therefore it is "invalid"
	ATE::ApplicationPaintRef pPaint = NULL;

	// After this call, pPaint will be "valid"
	sAIATEPaintSuite->CreateATEApplicationPaint(&srcAIColor, &pPaint);

	// Use the C++ wrapper (see note below) so we don't have to worry 
	// about ref counting.
	IApplicationPaint paint(pPaint);
	...
@endcode

	FOURTH:	All methods return kBadParameterErr if a bad parameter is passed in.
	These include NULL pointers or source parameters containing data the method was
	unable to convert, e.g. the source AILineCap contained a value that was
	outside its defined set of values.


	EXAMPLES


	To help plugin developers understand how Illustrator suites are used
	with ATE suites, we describe two scenarios:
	
	1.	The user wishes to know the paint attributes for a range of text.
	2.  The user wishes to set the paint attributes for a range of text.

	In Scenario 1, the user will typically receive an ATE::ApplicationPaintRef
	from the ATE suites.  Use of the AIATEPaintSuite allows them to extract
	useful information.  For example, the following code path might occur:

@code
	ATE::TextRangesRef selRef;
	sAIDocument->GetTextSelection(&selRef);
	ITextRanges ranges(selRef);	
	ITextRange range = ranges.getFirst( );

	...

	bool isValid = false;
	AIPathStyle style;
	AIPathStyleMap styleMap;

	ICharFeatures features = range->getUniqueLocalCharFeatures( );
	IApplicationPaint paint = features.getStrokeColor( &isValid );

	if ( isValid )
	{
		error = sAIATEPaintSuite->GetAIPathStyleAndMap(
			features.GetRef(), &style, &styleMap
		);
		...
	}
@endcode

	In Scenario 2, the user will typically start out with an AIPathStyle and
	AIPathStyleMap and wish to set corresponding attribute values in a
	ATE::CharFeaturesRef to set the local overrides of text, for example:

@code
	ITextRange range = ...

	// The C++ wrapper takes care of initialization
	ICharFeatures features;

	error = sAIATEPaintSuite->GetCharFeaturesRef(
		&style, &styleMap, features
	);

	if ( kNoErr == error )
	{
		range.setLocalCharFeatures( features );
	}
@endcode

	NOTE: In the above examples, we have assumed that the plugin developer
	has wisely opted to use the C++ wrapper classes for the ATE suites
	instead of using the suites directly (which entails worrying about
	reference counting among other perils).  Consult the ATE documentation
	for more information.
*/
typedef struct {

	// ATE paint types -> Illustrator paint typess
	// -------------------------------------------------------------------------

	/** Converts an ATE::ApplicationPaintRef to AIColor */
	AIAPI AIErr ( *GetAIColor ) ( ATE::ApplicationPaintRef src, AIColor* dst );

	/** Converts an ATE::LineCapType to AILineCap */
	AIAPI AIErr ( *GetAILineCap ) ( ATE::LineCapType src, AILineCap* dst );

	/** Converts an ATE::LineJoinType to AILineJoin */
	AIAPI AIErr ( *GetAILineJoin ) ( ATE::LineJoinType src, AILineJoin* dst );

	/** Converts an ATE::ArrayReal to array contained in AIDashStyle */
	AIAPI AIErr ( *GetAIDashStyleArray ) ( ATE::ArrayRealRef srcArray, AIDashStyle* dstStyle );

	// Illustrator paint types -> ATE paint types
	// -------------------------------------------------------------------------

	/** Converts an AIColor to an ATE::ApplicationPaintRef. ApplicationPaintRef is
		"instantiated" by this method. */
	AIAPI AIErr ( *CreateATEApplicationPaint ) ( const AIColor* src, ATE::ApplicationPaintRef* dst );

	/** Converts an AILineCap to ATE type */
	AIAPI AIErr ( *GetATELineCap ) ( AILineCap src, ATE::LineCapType* dst );

	/** Converts an AILineJoin to ATE type */
	AIAPI AIErr ( *GetATELineJoin ) ( AILineJoin src, ATE::LineJoinType* dst );

	/** Converts an array contained in AIDashStyle to ATE::ArrayReal. ArrayRealRef is
		"instantiated" by this method. */
	AIAPI AIErr ( *CreateATELineDashArray ) ( const AIDashStyle* srcStyle, ATE::ArrayRealRef* dst );

	// Convenience Methods
	// -------------------------------------------------------------------------

	/** Set fields in AIPathStyle and AIPathStyleMap appropriately by extracting
		corresponding data contained in CharFeaturesRef */
	AIAPI AIErr ( *GetAIPathStyleAndMap ) ( ATE::CharFeaturesRef src, AIPathStyle* pDstPathStyle, AIPathStyleMap* pDstPathStyleMap );

	/** Set fields in CharFeaturesRef appropriately by extracting the
		corresponding data in AIPathStyle and AIPathStyleMap */
	AIAPI AIErr ( *GetCharFeatures ) ( const AIPathStyle* pSrcPathStyle, const AIPathStyleMap* pSrcPathStyleMap, ATE::CharFeaturesRef dst );

	// Transparency Methods
	// -------------------------------------------------------------------------

	/** Given a CharFeatures, extracts blend (transparency) info and adds it to the dictionary.
		If the src contains no blend information, the dictionary is untouched. See
		AIBlendStyleSuite::GetArtAttrs() for a description of the dictionary entries. */
	AIAPI AIErr ( *GetTransparencyAttrs ) ( ATE::CharFeaturesRef pSrc, AIDictionaryRef pDst );

	/** Sets the blend (transparency) attributes in CharFeatures based on the blend values in
		the dictionary. All blend attribute in the dictionary must be known.  If any are unknown,
		then the blend info for CharFeatures is cleared. See AIBlendStyleSuite::GetArtAttrs
		for a description of the dictionary entries. */
	AIAPI AIErr ( *SetTransparencyAttrs ) ( AIDictionaryRef pSrc, ATE::CharFeaturesRef pDst );

} AIATEPaintSuite;

#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

}

#endif // __AIATEPAINT__
