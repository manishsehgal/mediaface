#ifndef __AIRealMath__
#define __AIRealMath__

/*
 *        Name:	AIRealMath.h
 *   $Revision: 28 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 8.0 AIReal AIPoint Math Suite.
 *
 * Copyright (c) 1986-1998 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#include <math.h>	// for floor and ceil
#include <float.h>

#ifdef __cplusplus
extern "C" {
#endif


#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIRealMath.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIRealMathSuite			"AI Real Math Suite"
#define kAIRealMathSuiteVersion		AIAPI_VERSION(1)
#define kAIRealMathVersion			kAIRealMathSuiteVersion

// Most of this stuff is completely unnecessary, and is just here to ease the
// transition between Fixed and AIReal.  We should probably make a later pass
// to clean the unnecessary stuff out.

// common AIReal constants
#define	kAIRealMax					((AIReal)(FLT_MAX / 10.0F))
#define	kAIRealMin					((AIReal)-(FLT_MAX / 10.0F))
#define	kAIRealUnknown				((AIReal)FLT_MAX)

#ifndef kAIRealOne
#define kAIRealOne					((AIReal) 1.0)
#endif
#ifndef kAIRealZero
#define kAIRealZero					((AIReal) 0.0)
#endif
#define kAIRealHundredth			((AIReal) 0.01)
#define kAIRealSixteenth			((AIReal) (1.0/16.0))
#define kAIRealTenth				((AIReal) 0.1)
#define kAIRealEighth				((AIReal) 0.125)

#ifndef kAIRealQuarter
#define kAIRealQuarter 				((AIReal) 0.25)
#endif

#ifndef kAIRealHalf
#define kAIRealHalf 				((AIReal) 0.5)
#endif

#define kAIRealSevenEighths			((AIReal) 0.875)
#define kAIRealOneThird				((AIReal) (1.0/3.0))
#define kAIRealTwoThirds			((AIReal) (2.0/3.0))
#define kAIRealFourThirds			((AIReal) (4.0/3.0))
#define kAIRealThreeHalves			((AIReal) 1.5)

#define kAIPi						3.141592653589793238462643383279502884197169399375
#define kAIDoublePi					((double) kAIPi)
#define kAIRealPi					((AIReal) kAIPi)
#define kAIRealPi4 					((AIReal) (kAIPi/4.0))
#define kAIRealPi2 					((AIReal) (kAIPi/2.0))
#define kAIReal3PiOver2				((AIReal) (kAIRealThreeFourths*kAIPi))
#define kAIRealTwoPi 				((AIReal) (kAIPi*2.0))

#define kAIRealSquareRootOfTwo		((AIReal) 1.4142135623)		/* 2^(1/2) */
#define kAIRealAvogadrosOther		((AIReal) 0.5522847498)		/* (4/3) [2^(1/2) - 1] */
#define kAIRealGolden 				((AIReal) 1.6180339888)		/* [1 + 5^(1/2)]/2 */

#define kSmallestFixedNumberEquivalent_Tolerance		((AIReal) (1.0 / (1 << 16)) )	// 1 / 2^16
#define kAIRealTolerance								kSmallestFixedNumberEquivalent_Tolerance


/*******************************************************************************
 **
 **	Macros
 **
 **/

#if __BUILD_PLUGIN__

// This code used to depend on WIN_ENV.  But fmin, round, and floor are not ANSI,
// and they don't seem to be available on the Mac.

#define	_AIRealMIN(x, y)	((x) > (y) ? (y) : (x))
#define	_AIRealMAX(x, y)	((x) > (y) ? (x) : (y))
#define _AIRealAbs(x)		((x) >= kAIRealZero ? (x) : -(x))

#define _AIRealRound( x)		(AIReal)( (x) > 0 ? floor((x) + 0.5) : ceil((x) - 0.5))
// NOTE:  Trunc() trunc toward 0
#define	_AIRealTrunc( x)		(AIReal)( (x) > 0 ? floor((x)) : ceil((x)))
#define _AIRealFloor( x)		(AIReal)floor( x)

#if 0		// Formerly #ifndef WIN_ENV
	#define	_AIRealMIN( x, y)	fmin( (x), (y))
	#define	_AIRealMAX( x, y)	fmax( (x), (y))

	#define _AIRealRound( x)		(AIReal)round( x)
	#define _AIRealFloor( x)		(AIReal)floor( x)
	#define	_AIRealTrunc( x)		(AIReal)trunc( x)
#endif

// type conversion macros
#define _ShortToAIReal(a)		((AIReal) (a))
#define _AIRealRoundToShort(a)	((short) _AIRealRound(a))
#define _AIRealTruncToShort(a)	((short) _AIRealTrunc(a))

// other macros

#define _AIRealFraction(a)		((AIReal) ((a) - _AIRealTrunc(a)))
#define _AIRealAverage(a, b) 	((AIReal) (((a) + (b)) / 2.0))
#define _AIRealHalf(a) 			((AIReal) ((a) / 2.0))
#define _AIRealOverflow(a)		((a) == kAIRealMax || (a) == kAIRealMin)

#define _AIRealSign(a)			((AIReal) ((a) > 0 ? +1 : ((a) < 0 ? -1 : 0)))

//FIXME: Removed AIRealCeiling, because the definition of its fixed counterpart
//implies that it's meaningless for floats.
//#define _AIRealCeiling(a)		_AIRealFloor((a) + kAIRealOne1)


#endif


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** Coordinates used by Adobe Illustrator are given in AIReal values (an AIReal
	is a C �float�Etype).

	Note: Adobe Illustrator 8.0 changed all coordinate measurements
	from AIFixed to AIReal data types. This change affects
	many suites, in particular, the Fixed Math Suite has been
	superceded by this suite.

	The Real Math Suite contains four general categories of functions. The first
	is for working with straight AIReal math values and includes functions such
	as RealMul and RealSin. The second is for working with AIRealPoints. The
	third group is used to work with AIRealRects. The final group is for manipulating
	AIReal matrices.
 */
typedef struct {

	AIAPI AIReal (*AIRealAdd) ( AIReal a, AIReal b );
	AIAPI AIReal (*AIRealMul) ( AIReal a, AIReal b );
	AIAPI AIReal (*AIRealDiv) ( AIReal a, AIReal b );
	AIAPI AIReal (*ShortRatio) ( short a, short b );
	AIAPI AIReal (*AIRealRatio) ( AIReal a, AIReal b );
	AIAPI AIReal (*AIRealMulAdd) ( AIReal a, AIReal b, AIReal c, AIReal d );
	/** Return "a" rounded to a multiple of "multiple" (which must not be zero.)
		If ceiling is true, we round up, if it is false, we round down.
		(Up and down here are signed, that is, floor -0.5 is -1 and ceiling -0.5 is 0,
		not the other way around.) */
	AIAPI AIReal (*AIRealMultiple) (AIReal a, AIReal multiple, AIBoolean ceiling );

	AIAPI AIReal (*AIRealSin) ( AIReal a );
	AIAPI AIReal (*AIRealCos) ( AIReal a );
	AIAPI AIReal (*AIRealATan) ( AIReal a, AIReal b );
	AIAPI AIReal (*DegreeToRadian) ( AIReal degree );
	AIAPI AIReal (*RadianToDegree) ( AIReal radian );

	AIAPI AIReal (*AIRealSqrt) ( AIReal a );
	/** AIRealSqrt(a*a + b*b) */
	AIAPI AIReal (*AIRealLength) ( AIReal a, AIReal b );
	/** Linear interpolation from b at t=0 to a at t=1 */
	AIAPI AIReal (*AIRealInterpolate) ( AIReal a, AIReal b, AIReal t );

	AIAPI void (*AIRealPointAdd) ( const AIRealPoint *a, const AIRealPoint *b, AIRealPoint *result );
	AIAPI void (*AIRealPointSubtract) ( const AIRealPoint *a, const AIRealPoint *b, AIRealPoint *result );
	/** Tests for exact equality of the points. */
	AIAPI AIBoolean (*AIRealPointEqual) ( const AIRealPoint *a, const AIRealPoint *b );
	/** Tests whether an axis aligned (closed) rectangle of size tolerance can be placed such that
		it contains a and b. */
	AIAPI AIBoolean (*AIRealPointClose) ( const AIRealPoint *a, const AIRealPoint *b, AIReal tolerance );
	/** Returns the angle made by the vector from a to b with the x-axis. */
	AIAPI AIReal (*AIRealPointAngle) ( const AIRealPoint *a, const AIRealPoint *b );
	/** Returns the length of the vector ab */
	AIAPI AIReal (*AIRealPointLength) ( const AIRealPoint *a, const AIRealPoint *b );
	/** Returns the point at distance "length" from the origin at "angle" */
	AIAPI void (*AIRealPointLengthAngle) ( AIReal length, AIReal angle, AIRealPoint *result );
	/** a * t + b * (1-t) */
	AIAPI void (*AIRealPointInterpolate) ( const AIRealPoint *a, const AIRealPoint *b, AIReal t, AIRealPoint *result );

	AIAPI void (*AIRealRectSet) ( AIRealRect *a, AIReal left, AIReal top, AIReal right, AIReal bottom );
	/** Are the two rectangles equal. This uses an inexact equality test that attempts to
		account for errors in floating point numbers. */
	AIAPI AIBoolean (*AIRealRectEqual) ( const AIRealRect *a, const AIRealRect *b );
	/** True if no points lie within the open rectangle defined by a. */
	AIAPI AIBoolean (*AIRealRectEmpty) ( const AIRealRect *a );
	AIAPI void (*AIRealRectInset) ( AIRealRect *a, AIReal h, AIReal v );
	AIAPI void (*AIRealRectOffset) ( AIRealRect *a, AIReal h, AIReal v );
	AIAPI void (*AIRealRectUnion) ( const AIRealRect *a, const AIRealRect *b, AIRealRect *result );
	AIAPI void (*AIRealPointUnion) ( const AIRealPoint *a, const AIRealRect *b, AIRealRect *result );
	/** Do the open rectangles a and b have any points in common. */
	AIAPI AIBoolean (*AIRealRectOverlap) ( const AIRealRect *a, const AIRealRect *b );
	/** Determines the intersection of the rectangles and returns the value of AIRealRectEmpty()
		on the intersection. */
	AIAPI AIBoolean (*AIRealRectIntersect) ( const AIRealRect *a, const AIRealRect *b, AIRealRect *result );
	/** True if the set of points contained by a is also contained by b. Both being considered
		either as open or closed. */
	AIAPI AIBoolean (*AIRealRectInAIRealRect) ( const AIRealRect *a, const AIRealRect *b );
	/** True if the point a is contained in the closed rectangle b. */
	AIAPI AIBoolean (*AIRealPointInAIRealRect) ( const AIRealPoint *a, const AIRealRect *b );
	/** Rounds the rectangles coordinates to integer values such that the result
		rectangle contains the original. Does not necessarily return the smallest such
		since it simply takes the floor of the coordinates and then adds 1 to the top
		and right. */
	AIAPI void (*AIRealRectAlign) ( const AIRealRect *a, AIRealRect *result );

	AIAPI void (*AIRealMatrixSet) ( AIRealMatrix *m, AIReal a, AIReal b, AIReal c, AIReal d, AIReal tx, AIReal ty );
	AIAPI void (*AIRealMatrixSetIdentity) ( AIRealMatrix *m );
	/** Tests for exact equality */
	AIAPI AIBoolean (*AIRealMatrixEqual) ( const AIRealMatrix *m, const AIRealMatrix *n );
	/** Tests for exact equality */
	AIAPI AIBoolean (*AIRealMatrixIdentity) ( const AIRealMatrix *m );
	/** Tests whether the determinant is approximately zero. */
	AIAPI AIBoolean (*AIRealMatrixSingular) ( const AIRealMatrix *m );
	AIAPI void (*AIRealMatrixSetTranslate) ( AIRealMatrix *m, AIReal tx, AIReal ty );
	AIAPI void (*AIRealMatrixSetScale) ( AIRealMatrix *m, AIReal h, AIReal v );
	AIAPI void (*AIRealMatrixSetRotate) ( AIRealMatrix *m, AIReal angle );
	/** m = m * T where T is the translation matrix. */
	AIAPI void (*AIRealMatrixConcatTranslate) ( AIRealMatrix *m, AIReal tx, AIReal ty );
	/** m = m * S where S is the scale matrix. */
	AIAPI void (*AIRealMatrixConcatScale) ( AIRealMatrix *m, AIReal h, AIReal v );
	/** m = m * R where R is the rotation matrix. */
	AIAPI void (*AIRealMatrixConcatRotate) ( AIRealMatrix *m, AIReal angle );
	/** m = m * n */
	AIAPI void (*AIRealMatrixConcat) (const AIRealMatrix *m, const AIRealMatrix *n, AIRealMatrix *result );
	/** Inverts m. Returns -1 if m cannot be inverted. */
	AIAPI short (*AIRealMatrixInvert) (AIRealMatrix *m );
	AIAPI void (*AIRealMatrixXformPoint) ( const AIRealMatrix *m, const AIRealPoint *a, AIRealPoint *result );
	
	AIAPI AIFixed (*AIRealToAIFixed) ( AIReal r );
	AIAPI AIReal (*AIFixedToAIReal) ( AIFixed f );
	AIAPI void (*AIRealPointToAIFixedPoint) ( const AIRealPoint *r, AIFixedPoint *f );
	AIAPI void (*AIFixedPointToAIRealPoint) ( const AIFixedPoint *f, AIRealPoint *r );
	AIAPI void (*AIRealRectToAIFixedRect) ( const AIRealRect *r, AIFixedRect *f );
	AIAPI void (*AIFixedRectToAIRealRect) ( const AIFixedRect *f, AIRealRect *r );
	AIAPI void (*AIRealMatrixToAIFixedMatrix) ( const AIRealMatrix *r, AIFixedMatrix *f );
	AIAPI void (*AIFixedMatrixToAIRealMatrix) ( const AIFixedMatrix *f, AIRealMatrix *r );

	/** Is the value "r" equal to the special value Illustrator uses to represent "unknown"
		quantities. Some parameters can take on "unknown" values. This is used to signal
		a special condition, for example it could be used to signal that a default value
		is to be used. This is only possible for parameters where the unknow value cannot
		be a valid parameter value. The unknown value is #kAIRealUnknown */
	AIAPI AIBoolean (*IsAIRealUnknown) ( AIReal r );
	/** The same as IsAIRealUnknown(). Note that this does not test for NAN. */
	AIAPI AIBoolean (*AIRealIsNAN) ( AIReal r );

	/** Say a > b then this function returns true if abs(a) - abs(b) < abs(b) * percent.
		If b > a then it returns true if abs(a) < abs(b) * percent. */
	AIAPI AIBoolean (*AlmostEqual) (const AIReal a, const AIReal b, const AIReal percent);

	/** Are a and b within tolerance of one another. ( abs(a-b) <= tolerance ) */
	AIAPI AIBoolean (*EqualWithinTol) (const AIReal a, const AIReal b, const AIReal tolerance);

	AIAPI void (*AIRealMatrixGetTranslate) ( const AIRealMatrix *m, AIReal *tx, AIReal *ty );
	AIAPI void (*AIRealMatrixGetScale) ( const AIRealMatrix *m, AIReal *h, AIReal *v );
	AIAPI void (*AIRealMatrixGetRotate) ( const AIRealMatrix *m, AIReal *angle );

	AIAPI void (*AIDoubleRectUnion) ( const AIDoubleRect *a, const AIDoubleRect *b, AIDoubleRect *result );
} AIRealMathSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
