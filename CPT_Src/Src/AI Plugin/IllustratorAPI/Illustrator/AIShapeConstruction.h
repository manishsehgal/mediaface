/**
 
 	AIShapeConstruction.h
 	Copyright (c) 1995 Adobe Systems Incorporated.

 	Adobe Illustrator 8.0 utility routines for some basic shapes.
 	
 **/

#ifndef __AIShapeConstruction__
#define __AIShapeConstruction__


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AITypes.h"
#include "AIArt.h"


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIShapeConstruction.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIShapeConstructionSuite			"AI Shape Construction Suite"
#define kAIShapeConstructionSuiteVersion	AIAPI_VERSION(4)
#define kAIShapeConstructionVersion			kAIShapeConstructionSuiteVersion


/** @ingroup Errors */
#define kNoSegmentsError					'0SEG'
/** @ingroup Errors */
#define kArtworkTooComplexErr				'cmp!'


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** The shape construction suite contains utilities for creating paths whose shapes
	describe a particular geometric figure. All of the utilities take parameters
	that describe the geometric figure and return a new art object. The art object
	is created in the art tree at the current insertion point as returned by
	AIArtSuite::GetInsertionPoint().

	NOTE: The shape suite is supplied by a plugin. The user is at liberty to
	remove this plugin. Clients of the shape suite should fail gracefully if it
	is not available.
 */
typedef struct {

	AIAPI AIErr (*NewPointPointRect)( AIRealPoint fromPoint, AIRealPoint toPoint, 
									  AIBoolean isCentered, AIBoolean isReversed, AIBoolean isConstrained, 
									  AIBoolean honorConstrainAngle, 
									  AIArtHandle *art, AIRealPoint *size );
	AIAPI AIErr (*NewPointSizeRect)( AIRealPoint fromPoint, AIRealPoint size, 
									 AIBoolean isCentered, AIBoolean isReversed, 
									 AIBoolean honorConstrainAngle, 
									 AIArtHandle *art );

	AIAPI AIErr (*NewPointPointOval)( AIRealPoint fromPoint, AIRealPoint toPoint, 
									  AIBoolean isCentered, AIBoolean isReversed, AIBoolean isConstrained, AIBoolean isCircumscribed, 
									  AIBoolean honorConstrainAngle, 
									  AIArtHandle *art, AIRealPoint *size  );
	AIAPI AIErr (*NewPointSizeOval)( AIRealPoint fromPoint, AIRealPoint size, 
									 AIBoolean isCentered, AIBoolean isReversed, 
									 AIBoolean honorConstrainAngle, 
									 AIArtHandle *art );

	AIAPI AIErr (*NewPointPointRoundedRect)( AIRealPoint fromPoint, AIRealPoint toPoint, AIRealPoint radius, 
											 AIBoolean isCentered, AIBoolean isReversed, AIBoolean isConstrained, 
											 AIBoolean honorConstrainAngle, 
											 AIArtHandle *art, AIRealPoint *size  );
	AIAPI AIErr (*NewPointSizeRoundedRect)( AIRealPoint fromPoint, AIRealPoint size, AIRealPoint radius, 
											AIBoolean isCentered, AIBoolean isReversed, 
											AIBoolean honorConstrainAngle, 
											AIArtHandle *art );

	AIAPI AIErr (*NewRect)( AIReal top, AIReal left, 
							AIReal bottom, AIReal right,
	                        AIBoolean reversed, AIArtHandle *newArt);
	AIAPI AIErr (*NewCenteredRect)( AIReal centerX, AIReal centerY, 
									AIReal height, AIReal width,
	                                AIBoolean reversed, AIArtHandle *newArt );

	AIAPI AIErr (*NewRoundedRect)( AIReal top, AIReal left, 
								   AIReal bottom, AIReal right,
                                   AIReal horizRadius, AIReal vertRadius,
	                               AIBoolean reversed, AIArtHandle *newArt );
  	AIAPI AIErr (*NewCenteredRoundedRect)( AIReal centerX, AIReal centerY, 
										   AIReal height, AIReal width,
                                           AIReal horizRadius, AIReal vertRadius,
	                                       AIBoolean reversed, AIArtHandle *newArt );

	AIAPI AIErr (*NewInscribedOval)( AIReal top, AIReal left, 
									 AIReal bottom, AIReal right,
									 AIBoolean reversed, AIArtHandle *newArt );
	AIAPI AIErr (*NewCircumscribedOval)( AIReal top, AIReal left, 
										 AIReal bottom, AIReal right,
										 AIBoolean reversed, AIArtHandle *newArt );
	AIAPI AIErr (*NewInscribedCenteredOval)( AIReal centerX, AIReal centerY, 
											 AIReal height, AIReal width,
											 AIBoolean reversed, AIArtHandle *newArt );
	AIAPI AIErr (*NewCircumscribedCenteredOval)( AIReal centerX, AIReal centerY, 
												 AIReal height, AIReal width,
												 AIBoolean reversed, AIArtHandle *newArt );

	AIAPI AIErr (*NewRegularPolygon)( unsigned short numSides, 
									  AIReal centerX, AIReal centerY, AIReal radius,
	                                  AIBoolean reversed, AIArtHandle *newArt );
	AIAPI AIErr (*NewRegularPolygonPoint)( unsigned short numsides, 
										   AIRealPoint center, AIRealPoint vertex,
	                                       AIBoolean reversed, AIArtHandle *newArt );
	                                         
	AIAPI AIErr (*NewStar)( unsigned short numPoints, 
							AIReal centerX, AIReal centerY, 
							AIReal radius1, AIReal radius2, 
							AIBoolean reversed, AIArtHandle *newArt );
	AIAPI AIErr (*NewStarPoint)( unsigned short numPoints, 
								 AIRealPoint center, AIRealPoint vertex,
	                             AIReal radiusRatio, 
								 AIBoolean reversed, AIArtHandle *newArt );

	AIAPI AIErr (*NewSpiral)( AIRealPoint firstArcCenter, AIRealPoint start, 
							  AIReal decayPercent, short numQuarterTurns,
                              AIBoolean clockwiseFromOutside, AIArtHandle *newArt );

} AIShapeConstructionSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
