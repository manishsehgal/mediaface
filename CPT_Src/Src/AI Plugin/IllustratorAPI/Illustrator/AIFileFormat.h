#ifndef __AIFileFormat__
#define __AIFileFormat__

/*
 *        Name:	AIFileFormat.h
 *   $Revision: 25 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator File Format Suite.
 *
 * Copyright (c) 1986-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIPlugin__
#include "AIPlugin.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AIPlaced__
#include "AIPlaced.h"
#endif

#include "IAIFilePath.hpp"

extern "C" {

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIFileFormat.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIFileFormatSuite				"AI File Format Suite"
#define kAIFileFormatSuiteVersion9		AIAPI_VERSION(9)
#define kAIFileFormatSuiteVersion		kAIFileFormatSuiteVersion9
#define kAIFileFormatVersion			kAIFileFormatSuiteVersion


#define kAIMacFileFormatSuite			"AI Mac File Format Suite"
#define kAIMacFileFormatSuiteVersion	AIAPI_VERSION(2)
#define kAIMacFileFormatVersion			kAIMacFileFormatSuiteVersion


/** @ingroup Callers
	This is the file format caller. */
#define kCallerAIFileFormat 				"AI File Format"

/** @ingroup Selectors
	Sent to file format plugins. The plugin should present any dialogs necessary
	to gather parameters. When reading a file this is sent after #kSelectorAICheckFileFormat.
	Whether reading or writing it is sent before #kSelectorAIGoFileFormat. Note that
	a file format plugin will not always receive this message. For example, when
	re-saving a document via the Save command only the go message is sent. The file
	format is then expected to use the parameters from the last time the document was
	saved. The data is an AIFileFormatMessage. */
#define kSelectorAIGetFileFormatParameters	"AI Get Parameters"
/** @ingroup Selectors
	Sent to file format plugins. Tells the plug�in to "do its thing". A read file
	format can create art according to what is in the specified file, a write file
	format might scan the artwork database and write out appropriately formatted
	information. The data is an AIFileFormatMessage. */
#define kSelectorAIGoFileFormat				"AI Go"
/** @ingroup Selectors
	Sent to file format plugins. Illustrator sends this selector after the user has
	selected a file. This is an opportunity to open the file and check for header
	information (basically to verify that it can be opened). If your plug-in cannot
	read the indicated file, it should return kUnknownFormatErr. The data is an
	AIFileFormatMessage. */
#define kSelectorAICheckFileFormat			"AI Check"
/** @ingroup Selectors
	Sent to file format plugins. This is a request to update a linked object in
	the document. The data is an AIUpdateFileFormatMessage. Note: A plug-in
	should handle only one of kAI110SelectorAIUpdateFileFormat or
	kSelectorAIUpdateFileFormat. */
#define kSelectorAIUpdateFileFormat			"AI Update AI120"
/** @ingroup Selectors
	Sent to file format plugins. The data is an AIFileFormatStreamMessage. */
#define kSelectorAIStreamFileFormat			"AI Stream"


/** @ingroup Notifiers */
#define kAIFileFormatDocumentOpenedNotifier		"AI File Format Document Opened Notifier"
/** @ingroup Notifiers
	The notification data sent is of type AILinkUpdateNotifyData. */
#define kAIFileFormatLinkUpdateNotifier			"AI File Format Link Update Notifier"


/** The capabilities of a file format plug-in are specified by its options. A
	subset of the options are also specified to the plugin in the AIFileFormatMessage.
	The subset indicates the action the plugin is being requested to perform. */
enum AIFileFormatOptions {
	kFileFormatRead	=							(1L<<0),
	kFileFormatExport =							(1L<<1),
	kFileFormatWrite =							(1L<<9),

	kFileFormatImportArt =						(1L<<2),
	kFileFormatPlaceArt	=						(1L<<3),
	kFileFormatConvertTemplate =				(1L<<7),
	kFileFormatLinkArt =						(1L<<8),
	kFileFormatImportStyles =					(1L<<4),

	kFileFormatSuppliesPrintRecordOption =		(1L<<5),
	kFileFormatIsDefaultOption =				(1L<<6),

	/** The plugin will not really respond to the kSelectorAICheckFileFormat selector.
		e.g. PhotoShop adapter plugin always return kNoErr for kSelectorAICheckFileFormat. */
	kFileFormatNoAutoCheckFormat =				(1L<<10),

	kFileFormatCreateTemplateLayer =			(1L<<11),

	kFileFormatHasExtendedData =				(1L<<12),

	/** This file format supplies its own startup objects (colors, patterns, etc); don't
		copy the startup file */
	kFileFormatSkipStartupObjectsOption =		(1L<<13),

	/** Disable warning dialogs */
	kFileFormatNoWarningOption =				(1L<<14),

	kFileFormatSaveCopyOption =					(1L<<15),

	/** Use streaming */
	kFileFormatUseStream =						(1L<<16),

	kFileFormatSequenceWrite =					(1L<<17),
	kFileFormatSequenceDontGenerateFrames =		(1L<<18),
	kFileFormatSequenceDoesntHaveParameters =	(1L<<19),	
	kFileFormatSequenceDoesntPreviewInBrowser =	(1L<<20),	

	kFileFormatSuppressUI =						(1L<<21),	
	kFileFormatWriteAs =						(1L<<22),	

	/** The file format always wants to receive the check message even for operations it
		doesn't support. this gives a format an opportunity to explicitly reject operations
		on files matching its type. */
	kFileFormatCheckAlways =					(1L<<23),

	/** When this option flag is set, it is similar to a normal go message
		but there are parameters in the message.actionParm field.  These should
		*supplement* the existing/default parameters of the file format
		and may not be complete.  It is used, for instance, in a scripting
		or optimizing situation.

		If this is not set, the file format continues as normal.  If it is set but 
		the file format doesn't recognize/respect the option, a normal go occurs.  
		If the plug-in supports this behavior it should set the flag on the options 
		passed when initially adding the file format.  A caller, such as Save For 
		Web, should technically check for the indicated behavior before using the go 
		message with it. */
	kFileFormatContainsPartialParameters =		(1L<<24),

	/** This flag imports only the SLO composite fonts...no artwork or other global objects...no
		font fauxing. */
	kFileFormatImportCompositeFonts	=			(1L<<25),

	/** Treat the file as stationery: open a copy with an Untitled name.
		Only used in conjunction with kFileFormatRead */
	kFileFormatOpenUntitledCopy =				(1L<<26),

	/** The following four flags are used as options only to the native (PGF) AI File Format Writer,
		to cause it to write out only the indicated palettes and the global objects they reference
		directly or indirectly. The reason we don't just register separate writers is that we don't
		want them to show up on a list of possible formats for Save As or Export. The reasons we don't
		make them parameters to the AI File Format are that (1) they are not considered an attribute
		of the file that gets remembered after you write it out (once saved as a Swatch library, a Save
		will still write out the whole file), and (2) the Swatch palette, Styles palette, etc., do
		not have access to the internals of an AI File Format Parameter block, and we don't want them to.
		An option flag is the most opaque way of specifying parameters to the Read/Write process.
		Unfortunately we are getting very close to using up all the available bits. (Most of the newer
		option flags are used only by certain readers or writers.) So we may need a new scheme soon. */
	kFileFormatWriteSwatchLibrary =				(1L<<27),
	kFileFormatWriteBrushLibrary =				(1L<<28),
	kFileFormatWriteStyleLibrary =				(1L<<29),
	kFileFormatWriteSymbolLibrary =				(1L<<30)
};

/** Used to test for any matches to any of the options indicating that only specific palettes should
	be written. */
#define kFileFormatWritePalettesOnly (kFileFormatWriteSwatchLibrary|kFileFormatWriteBrushLibrary|kFileFormatWriteStyleLibrary|kFileFormatWriteSymbolLibrary)


/** Max length of the extension str in PlatformAddFileFormatData struct */
#define kFileFormatExtensionStrMaxLength		31
/** Max length for each extension within the extension str. */
#define kFileFormatExtMaxLength					5

/** @ingroup Errors
	Plugin file format does not recognize the file type. Should be returned from
	check file format if the file does not pass muster. */
#define kUnknownFormatErr						'FMT?'
/** @ingroup Errors
	Plugin file format recognizes the file type but explicitly disallows processing
	it with the options specified. */
#define kInvalidFormatErr						'FMTi'

/** File format priorities */
enum AIFileFormatPriority {
	kAIFileFormatPriorityNative =				10000,
	kAIFileFormatPriorityNormal	=				0,
	kAIFileFormatPriorityLowest	=				-10000
};


/*******************************************************************************
 **
 ** Types
 **
 **/

/** This is a reference to a file format. It is never dereferenced. */
typedef struct _t_AIFileFormatOpaque *AIFileFormatHandle;


/** for kAIFileFormatVersion4 */
typedef struct PlatformAddFileFormatData4 {
	long type;
	unsigned char *title;
	long titleOrder;
} PlatformAddFileFormatData4;

/** The PlatformAddFileFormatData structure contains the information need to
	add a file format to Illustrator. See also PlatformAddFileFormatExData. */
typedef struct PlatformAddFileFormatData {
	/** The variable type is the 4 digit Macintosh file type identifier. Windows
		plug-ins can ignore this. */
	long			type;
	/** The title field contains the Pascal string displayed in the file type menu of
		the Save As and/or Open dialog. */
	unsigned char*	title;
	/** The titleOrder field is used if a plug-in adds multiple file formats and
		designates the order of its menu items. Use 0 for plug-ins that add only
		one filter. */
	long			titleOrder;
	/** Fill extension with a comma delimited list of DOS�style extensions which
		are filtered in the open dialog, or appended to the file name in the save
		as dialog. There is a 31 character limit to this record and a 5 character
		limit per extension. e.g "ai,eps". */
	char*			extension;
} PlatformAddFileFormatData;


/** The PlatformAddFileFormatDataEx structure contains the information need to
	add a file format to Illustrator. It differs from PlatformAddFileFormatData
	by allowing a list of Macintosh file types to be specified. */
typedef struct PlatformAddFileFormatExData {
	/** Number of elements in typeList */
	ASInt32			numTypes;
	/** Address of list of Mac file type */
	long*			typeList;

	unsigned char*	title;
	long			titleOrder;
	/** Max total 31 char. Max 5 char each extension. e.g. "ai,eps" */
	char*			extension;
} PlatformAddFileFormatExData;


/** A Macintosh file creator, type pair */
typedef struct AICreatorTypePair {
	long		creator;
	long		type;
} AICreatorTypePair;


////////////////////////////////////////////////////////////////
// Extend File Format Message with
// usage-specific data type.
////////////////////////////////////////////////////////////////

/** Selector for extended file format datatype */
enum eFFExtType {
	kFFExtNone,
	kFFExtPlaceRequest
};

/** Union of extended file format datatypes. */
union FileFormatExtUnion {
	AIPlaceRequestData *m_pPlaceRequestData;
};

/** Extended file format message data. */
typedef struct FileFormatExtData {
	/** An eFFExtType discriminates the union. */
	ASInt32 m_lExtType;
	union FileFormatExtUnion m_unExt;
} FileFormatExtData;

////////////////////////////////////////////////////////////////
// File Format message structure.
////////////////////////////////////////////////////////////////

/** Message data sent by the file format selectors. */
struct AIFileFormatMessage {
	SPMessageData d;
	/** Identifies the file format for which the message is intended. This can be
		compared against the format handle returned when the format was added. */
	AIFileFormatHandle fileFormat;

	private:
	/** The file to read, write or check. This is deprecated in favor of 'filePath'
	    but must remain in this struct for backwards binary compatibility. This can
	    be removed when AI11 compatibility is no longer needed. */
	SPPlatformFileSpecification file_deprecated;

	public:
	/** The function the plug-in is to perform, (e.g. read or write). */
	long option;
	/** Additional information. Only used if the plugin indicated it supports
		extended data in its options when registering the format. */
	FileFormatExtData *m_pExtData;
	void *actionParm;
	/** The file to read, write or check. */

	private:
	ai::FilePath filePath;
	
	public:
		// Set/GetFilePath ensure 'file_deprecated' stays synchronized with
		// 'filePath' as long as binary support for pre-AI12 plug-ins is required.
	void SetFilePath(const ai::FilePath &fp);
	const ai::FilePath &GetFilePath() const;
};

/** Message structure if kSelectorAIStreamFileFormat */
struct AIFileFormatStreamMessage {
	SPMessageData d;
	AIFileFormatHandle fileFormat;
	AIStream		StreamIn;	
	AIStream		StreamOut;
	long option;
};

/** Message structure for kSelectorAIUpdateFileFormat */
struct AIUpdateFileFormatMessage {
	struct AIFileFormatMessage ffm;
	AIArtHandle art;
	void* data;
};


////////////////////////////////////////////////////////////////
// The following enum and struct can be used to convey
// information in conjunction with the Link Update
// Artwork Notifier.
////////////////////////////////////////////////////////////////

enum eLinkUpdateType {
	kNormalUpdate,
	kLinkReplaced,
	kMissingLinkIgnored
};

/** Notification data sent with the #kAIFileFormatLinkUpdateNotifier. */
struct AILinkUpdateNotifyData
{
	int m_iUpdateType;
	void *m_pvParam;
};


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	Plug-in file formats give Illustrator the ability to read and write new file
	formats. File formats that read a format may be transparent to the user; files
	of their type simply appear in the open dialog. Your file format plug-in
	simply creates artwork in the Illustrator document. Writing file formats
	appear in the Save As dialog as an option for the user to select. These plugins
	walk the artwork tree and write out information on the artwork in the
	appropriate manner. If either needs more parameters, they can use a modal
	dialog to interact with the user.
 
	Plug-in File Formats are used to extend the number of file types Illustrator
	can read and write. Plug-ins tell Illustrator about the file formats they
	support by specifying platform file information: types (e.g 'ART3') under
	Mac OS and extensions (e.g '.AI') under Windows. Plug�ins also indicate
	what read/write capabilities they support.

	One plug-in can register multiple formats. Illustrator will handle parts of the
	file i/o process and have the plug-in do the remainder.
	
	A File Format plug�in receives message actions telling it to:
	
	- check if a file can be read,
	- ask for parameters, and
	- go.
	
	The check selector will only be received by file formats that can read files
	and extends the simple type checking done by the open dialog.
	
	The ask for parameters selector can be ignored if the plug-in does not need
	user input.
	
	The go selector is where the file format actually reads or writes a file.
	If a file format plug�in can read, files of its registered file type will appear in
	the file list of the open dialog. In addition to screening by file type, Illustrator
	will send a selector giving a plug-in a chance to check the file can be
	read. This is useful in the case of text files or files from a different computer.
	If more than one format matches the type, Illustrator will determine which
	should actually open the file, asking the user if necessary.

	If a file format plug�in supports reading and writing, when reading it should
	store any information about the file needed to write the file back to disk in
	the plug-in's globals or a parameter block (for example, the pixel depth of
	an EPS preview). The reference to the block should be attached to the document
	using the AIDocumentSuite::SetDocumentFileFormatParameters() function. Illustrator
	keeps this parameters reference with the document; the plug�in can retrieve
	it when the document is being saved.

	When the user does a Save, Illustrator will automatically default to the file
	format used to read in the file. If the file format does not support saving, the
	Save As... dialog will automatically appear to let the user chose the save format.
	All file format plug-ins that support saving will appear in the format menu of
	this dialog.

	If you need information from the user to open or save a file, you can present
	a dialog when the ask for parameters message is received. When saving files,
	this will be after the Save As dialog has appeared. This information should
	be kept with the document and used for later saves.

	When the go message is received, file formats that read files typically parse
	the input file and create Illustrator artwork using API calls. File formats for
	saving Illustrator artwork traverse the artwork database and write out all
	pertinent information.
*/
struct AIFileFormatSuite {

	/** Call this during startup to install a plug-in file format.

		- The SPPluginRef self is a reference to the plug-in adding the file format.
		- name is a C string identifying the added plug-in file format to Illustrator and
			other plug-ins. Using your company name and a descriptor is one way to
			make it unique. It isn�t displayed to the user and isn�t meant to be localized.
		- The options argument passed to the AddFileFormat function contains
			flags that specify the file format capabilities. When the format is called
			with a #kSelectorAIGoFileFormat selector, one of the flags will be set indicating
			the reason the file format was triggered. See #AIFileFormatOptions
		- The PlatformAddFileFormatData structure contains the information need to
			add the file format to Illustrator.
		- The AIFileFormatHandle returned is the installed plug-in file format. If
			you are installing multiple file formats, this reference must be stored in the
			plug-in�s globals record, as your plug-in will need it to determine which file
			format command is to be processed.
	*/
	AIAPI AIErr (*AddFileFormat) ( SPPluginRef self, const char *name,
				PlatformAddFileFormatData *data, long options, AIFileFormatHandle *fileFormat );

	/** Returns the a pointer to the name of the fileFormat. This is the name value
		originally passed to the AddFileFormat call. It should not be modified. */
	AIAPI AIErr (*GetFileFormatName) ( AIFileFormatHandle fileFormat, char **name );
	/** Use this to get the current options of a file format. */
	AIAPI AIErr (*GetFileFormatOptions) ( AIFileFormatHandle fileFormat, long *options );
	/** Use this to set the current options of a file format. */
	AIAPI AIErr (*SetFileFormatOptions) ( AIFileFormatHandle fileFormat, long options );
	/** This call will return a reference to the plug-in that installed the file format. */
	AIAPI AIErr (*GetFileFormatPlugin) ( AIFileFormatHandle fileFormat,
				SPPluginRef *plugin );

	/** The number of file formats available can be obtained using the count call. */
	AIAPI AIErr (*CountFileFormats) ( long *count );
	/** This call returns the AIFileFormatHandle at the specified zero based index. */
	AIAPI AIErr (*GetNthFileFormat) ( long n, AIFileFormatHandle *fileFormat );

	AIAPI AIErr (*GetFileFormatExtension) ( AIFileFormatHandle fileFormat, char *extension );
	/** Adds a file format using an extended set of information. See AddFileFormat() for
		a description of the parameters. */
	AIAPI AIErr (*AddFileFormatEx) ( SPPluginRef self, const char *name,
				PlatformAddFileFormatExData *dataEx, long options, AIFileFormatHandle *fileFormat );
	AIAPI AIErr (*GetFileFormatTitle) ( AIFileFormatHandle fileFormat, char *szTitle );
	AIAPI AIErr (*GetFileFormatType) ( AIFileFormatHandle fileFormat, ASInt32 *maxNumTypes, long *typeList );

	/** The parameters for AddFileFormat() and AddFileFormatEx() only allow for filtering
		Macintosh files based on their file type. This API allows a filter to be associated
		with a file format based on (creator, type) pairs. The Macintosh wild card value
		'****' can be used for creator or type. */
	AIAPI AIErr (*SetFileFormatFilter) ( AIFileFormatHandle fileFormat, ASInt32 numPairs, AICreatorTypePair *pairList, const char* extensions );
	AIAPI AIErr (*GetFileFormatCreatorType) ( AIFileFormatHandle fileFormat, ASInt32 *maxNumPairs, AICreatorTypePair *pairList );

	/** Write the currrent document to the file using the specified format. Options can be kFileFormatExport,
		kFileFormatWrite, or kFileFormatSequenceWrite */
	AIAPI AIErr (*PutFile) (long options, const unsigned char *dlgTitle, ai::FilePath &fileSpec, AIFileFormatHandle *fileFormat, ASBoolean *good );

	/** Specifies the priority of a file format. When searching for a file format to open a
		file Illustrator first looks through all file formats with priority #kAIFileFormatPriorityNative.
		If a format is found it is used to open the file. If no format is found it repeats the process
		with the next lower priority and so on. Note that only the specific priority values in
		#AIFileFormatPriority are checked. When a file format is added it is assigned the normal
		priority. */
	AIAPI AIErr (*SetFileFormatPriority) ( AIFileFormatHandle fileFormat, int priority );
	
	/** Searches for a file format that can perform the action indicated by the options for the
		specified file. All formats capable of performing the action are returned. */
	AIAPI AIErr (*FindMatchingFileFormats) ( long options, const ai::FilePath &file, long maxMatches, AIFileFormatHandle matchingFormats[], long* numMatches );

	/** This API is only for use when importing artwork into an existing document (e.g place link
		or place embed). It will return an error at other times. When importing artwork the file
		format handler is requested to create it in an empty scratch document. The contents of
		the scratch document are then copied into the destination document.
		
		Conflicts may exist between global resources in the imported artwork and the destination
		document. For example both might contain objects that use a spot color with the same name
		but a different definition.
		
		This API should be called after the file format handler has finished importing the artwork
		from the file and before returning control to the caller. The API will resolve any conflicts
		between custom colors in the document and those in the artwork being imported. For each
		conflict it queries the conflict resolution policy and presents a dialog to the user if
		needed to decide how the conflict should be resolved. Note that the API may return kCanceledErr
		to indicate that the import operation is to be canceled.

		It is intended that the functionality performed by this API will be moved into the standard
		import handling code in a future version. At that time this API will become a no-op.
	*/
	AIAPI AIErr (*ResolveImportConflicts) ();

	/** The use of raster objects that are indicated to be linked files is being deprecated. All
		linked files should be created as placed objects. To support the transition placed linked
		image files can be exported to legacy file formats as raster objects. In order to do so
		the legacy export code needs to know the version number when the file format was switched
		from raster to placed art. This API returns that version. The corresponding API
		SetFirstVersionUsingPlacedArt() sets the version. By default the version is set to
		0 so that placed objects of that file format are not converted on export. */
	AIAPI AIVersion (*GetFirstVersionUsingPlacedArt) (AIFileFormatHandle fileFormat);
	/** Sets the first Illustrator version when the file format handler linked images using
		placed art rather than raster art. See GetFirstVersionUsingPlacedArt() for more
		information. */
	AIAPI AIErr (*SetFirstVersionUsingPlacedArt) (AIFileFormatHandle fileFormat, AIVersion version);
};


/** Obsolete. Plugins should be able to perform all file format operations using
	the AIFileFormatSuite. */
typedef struct {

	AIAPI AIErr (*MacXGetFileFormatTitle) ( AIFileFormatHandle fileFormat, unsigned char *title );
	AIAPI AIErr (*MacXSetFileFormatTitle) ( AIFileFormatHandle fileFormat, const unsigned char *title );
	AIAPI AIErr (*MacXGetFileFormatType) ( AIFileFormatHandle fileFormat, long *type );    
	AIAPI AIErr (*MacXSetFileFormatType) ( AIFileFormatHandle fileFormat, long type );	  

} AIMacFileFormatSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

}

/*******************************************************************************
 **
 **	Inline members
 **
 **/

inline void AIFileFormatMessage::SetFilePath(const ai::FilePath &fp)
{
	filePath = fp;
	fp.GetAsSPPlatformFileSpec(file_deprecated);
}

inline const ai::FilePath &AIFileFormatMessage::GetFilePath() const
{
	return filePath;
}

#endif
