#ifndef __AIEntry__
#define __AIEntry__

/*
 *        Name:	AIEntry.h
 *		$Id $
 *      Author:
 *        Date:
 *     Purpose:	Adobe Illustrator Dictionary Entry Object Suite.
 *
 * Copyright (c) 2001-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif
#ifndef __AIArt__
#include "AIArt.h"
#endif
#ifndef __AILayer__
#include "AILayer.h"
#endif
#ifndef __AIPattern__
#include "AIPattern.h"
#endif
#ifndef __AIGradient__
#include "AIGradient.h"
#endif
#ifndef __AICustomColor__
#include "AICustomColor.h"
#endif
#ifndef __AIObjectSet__
#include "AIObjectSet.h"
#endif
#ifndef __AIPathStyle__
#include "AIPathStyle.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIEntry.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIEntrySuite							"AI Entry Suite"
#define kAIEntrySuiteVersion3					AIAPI_VERSION(4)
#define kAIEntrySuiteVersion					kAIEntrySuiteVersion3
#define kAIEntryVersion							kAIEntrySuiteVersion


/*******************************************************************************
 **
 **	Types
 **
 **/


/**
	A reference to a container entry
 */
typedef struct _AIEntry*						AIEntryRef;

/**
	An array is a heterogeneous sequence of entries indexed by integers
	@see AIArraySuite
 */
typedef struct _AIArray*						AIArrayRef;

/**
	A dictionary is a heterogeneous collection of entries indexed by keys which are character strings
	@see AIDictionarySuite
 */
typedef struct _AIDictionary*					AIDictionaryRef;

/**
	A unique id.
	@see AIUIDSuite
 */
typedef struct _AIUID*							AIUIDRef;

/**
	A reference to a unique id.
	@see AIUIDREFSuite
 */
typedef struct _AIUIDREF*						AIUIDREFRef;

/**
	A reference to an xml element.
 */
typedef struct _AIXMLNode*						AIXMLNodeRef;

/**
	A reference to an SVG filter
 */
typedef struct _t_AISVGFilterOpaque*			AISVGFilterHandle;

/**
	A reference to an art style
 */
typedef struct _t_AIArtStyle*					AIArtStyleHandle;

typedef long  AIEntryType;
/**
	These are the possible container entry types
 */
enum {
	/** value type is unknown to this version of the API */
	UnknownType,
	/** integer (32 bit signed) */
	IntegerType,
	/** boolean */
	BooleanType,
	/** real (single precision floating point) */
	RealType,
	/** string (null terminated sequence of 8-bit values) */
	StringType,
	/** a dictionary. note that an art object is stored in a container by storing its dictionary. */
	DictType,
	/** array */
	ArrayType,
	/** Binary data. if the data is stored to file it is the clients responsibility to
		deal with the endianess of the data. */
	BinaryType,
	/** a convenience over using an ArrayType of two Reals */
	PointType,
	/** a convenience over using an ArrayType of six Reals */
	MatrixType,
	/** a reference to a pattern */
	PatternRefType,
	/** a reference to a brush pattern */
	BrushPatternRefType,
	/** a reference to a custom color (either spot or global process) */
	CustomColorRefType,
	/** a reference to a gradient */
	GradientRefType,
	/** a reference to a plugin global object */
	PluginObjectRefType,
	/** a fill style */
	FillStyleType,
	/** a stroke style */
	StrokeStyleType,
	/** an unique id */
	UIDType,
	/** an unique id reference */
	UIDREFType,
	/** an XML node */
	XMLNodeType,
	/** a SVG filter */
	SVGFilterType,
	/** an art style */
	ArtStyleType,
	/** a symbol definition reference */
	SymbolPatternRefType,
	/** a graph design reference */
	GraphDesignRefType,
	/** a blend style (transpareny attributes) */
	BlendStyleType,
	/** a graphical object */
	GraphicObjectType,
	/** a UnicodeString object */
	UnicodeStringType
};



#define kNoSuchEntry						'EnNS'
/**
	Error code returned when trying to access an entries value and the entry is not of
	the requested type.
 */
#define kWrongEntryType						100
/**
	Error code returned when trying to convert an entry to a particular value type and
	the underlying value of the entry is not convertible. For example, an integer can
	be converted to a real but not to a UID.
 */
#define kNoConversionErr					'NoCV'

/*******************************************************************************
 **
 **	Suite
 **
 **/


/**
	The AIEntry suite contains methods for constructing and inspecting the
	entries stored within heterogeneous containers such as dictionaries and
	arrays.
	
	Some notes on use of entries:

	Entries, like the dictionaries and arrays that contain them, are reference
	counted. This means that when a client is given an entry the count on
	the entry is incremented before it is returned. It is then the clients
	responsibility to decrement the count else leaks will occur. For entries
	these rules could result in some rather unnatural and lengthy code. To
	avoid this the "ToSomeType" methods and the "Set" methods have side
	effects on the reference counts of the entries that are described below.

	The "ToSomeType" methods have the side effect of decrementing the reference
	count of the entry they are passed. Therefore to get the value of an entry
	in a container such as a dictionary it is possible to write:

 		ASBoolean value;
		result = sAIEntry->ToBoolean(sAIDictionary->Get(dict, "MyKey"), &value);

	The "Set" methods of the various container types also have the side effect
	of decrementing the entry's reference count. Hence a value may be set in a
	container such as a dictionary via code like:

		result = sAIDictionary->Set(dict, "MyKey", sAIEntry->FromReal(15));

	"GetType" does not consume its parameter so this can be used to determine the
	type of an entry and act accordingly.
*/

typedef struct {

	/** reference count maintenance. increments the count of references to the entry. */
	AIAPI ASInt32 (*AddRef) ( AIEntryRef entry );
	/** reference count maintenance. decrements the count of references to the entry. */
	AIAPI ASInt32 (*Release) ( AIEntryRef entry );


	/** get the type of the entry */
	AIAPI AIEntryType (*GetType) ( AIEntryRef entry );


	// the various entry types

	/** Get the value of the entry assuming it is boolean. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToBoolean) ( AIEntryRef entry, ASBoolean* value );
	/** Construct an entry from a boolean value. */
	AIAPI AIEntryRef (*FromBoolean) ( ASBoolean value );

	/** Get the value of the entry assuming it is an integer. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToInteger) ( AIEntryRef entry, ASInt32* value );
	/** Construct an entry from an integer value. */
	AIAPI AIEntryRef (*FromInteger) ( ASInt32 value );

	/** Get the value of the entry assuming it is a real. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToReal) ( AIEntryRef entry, ASReal* value );
	/** Construct an entry from a real value. */
	AIAPI AIEntryRef (*FromReal) ( ASReal value );

	/** Get the value of the entry assuming it is a point. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToRealPoint) ( AIEntryRef entry, AIRealPoint* value );
	/** Construct an entry from a point value. */
	AIAPI AIEntryRef (*FromRealPoint) ( const AIRealPoint *value );

	/** Get the value of the entry assuming it is a matrix. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToRealMatrix) ( AIEntryRef entry, AIRealMatrix* value );
	/** Construct an entry from a matrix value. */
	AIAPI AIEntryRef (*FromRealMatrix) ( const AIRealMatrix *value );

	/** Get the value of the entry assuming it is a string. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToString) ( AIEntryRef entry, const char** value );
	/** Construct an entry from a string value. */
	AIAPI AIEntryRef (*FromString) ( const char* value );

	/** Get the value of the entry assuming it is a dictionary. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToDict) ( AIEntryRef entry, AIDictionaryRef* value );
	/** Construct an entry from a dictionary value. */
	AIAPI AIEntryRef (*FromDict) ( AIDictionaryRef value );

	/** Get the value of the entry assuming it is an art object. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToArt) ( AIEntryRef entry, AIArtHandle* art );

	/** Get the value of the entry assuming it is an array. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToArray) ( AIEntryRef entry, AIArrayRef* value );
	/** Construct an entry from an array value. */
	AIAPI AIEntryRef (*FromArray) ( AIArrayRef value );

	/** Get the value of the entry assuming it is binary data. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToBinary) ( AIEntryRef entry, void* value, ASInt32* size );
	/** Construct an entry from a binary value. */
	AIAPI AIEntryRef (*FromBinary) ( void* value, ASInt32 size );

	/** Get the value of the entry assuming it is a pattern. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToPattern) ( AIEntryRef entry, AIPatternHandle* value );
	/** Construct an entry from a pattern value. */
	AIAPI AIEntryRef (*FromPattern) ( AIPatternHandle value );

	/** Get the value of the entry assuming it is a brush pattern. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToBrushPattern) ( AIEntryRef entry, AIPatternHandle* value );
	/** Construct an entry from a brush pattern value. */
	AIAPI AIEntryRef (*FromBrushPattern) ( AIPatternHandle value );

	/** Get the value of the entry assuming it is a gradient. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToGradient) ( AIEntryRef entry, AIGradientHandle* value );
	/** Construct an entry from a gradient value. */
	AIAPI AIEntryRef (*FromGradient) ( AIGradientHandle value );

	/** Get the value of the entry assuming it is a custom color. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToCustomColor) ( AIEntryRef entry, AICustomColorHandle* value );
	/** Construct an entry from a custom color value. */
	AIAPI AIEntryRef (*FromCustomColor) ( AICustomColorHandle value );

	/** Get the value of the entry assuming it is a plugin global object. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToPluginObject) ( AIEntryRef entry, AIObjectHandle* value );
	/** Construct an entry from a plugin global object value. */
	AIAPI AIEntryRef (*FromPluginObject) ( AIObjectHandle value );

	/** Get the value of the entry assuming it is a fill style. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToFillStyle) ( AIEntryRef entry, AIFillStyle *value );
	/** Construct an entry from a fill style value. */
	AIAPI AIEntryRef (*FromFillStyle) ( const AIFillStyle *value );

	/** Get the value of the entry assuming it is a stroke style. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToStrokeStyle) ( AIEntryRef entry, AIStrokeStyle *value );
	/** Construct an entry from a stroke style value. */
	AIAPI AIEntryRef (*FromStrokeStyle) ( const AIStrokeStyle *value );


	/* New in Illustrator 10.0 */

	/** Get the value of the entry assuming it is an UID. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToUID) ( AIEntryRef entry, AIUIDRef* value );
	/** Construct an entry from an UID value. */
	AIAPI AIEntryRef (*FromUID) ( AIUIDRef value );

	/** Get the value of the entry assuming it is an UIDREF. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToUIDREF) ( AIEntryRef entry, AIUIDREFRef* value );
	/** Construct an entry from an UIDREF value. */
	AIAPI AIEntryRef (*FromUIDREF) ( AIUIDREFRef value );

	/** Get the value of the entry assuming it is an XML node. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToXMLNode) ( AIEntryRef entry, AIXMLNodeRef* value );
	/** Construct an entry from an XML node value. */
	AIAPI AIEntryRef (*FromXMLNode) ( AIXMLNodeRef value );

	/** Get the value of the entry assuming it is a SVG filter. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToSVGFilterHandle) ( AIEntryRef entry, AISVGFilterHandle* value );
	/** Construct an entry from a SVG filter value. */
	AIAPI AIEntryRef (*FromSVGFilterHandle) ( AISVGFilterHandle value );

	/** Get the value of the entry assuming it is a symbol pattern. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToSymbolPattern) ( AIEntryRef entry, AIPatternHandle* value );
	/** Construct an entry from a symbol pattern value. */
	AIAPI AIEntryRef (*FromSymbolPattern) ( AIPatternHandle value );

	// type conversion routines.
	// type conversion rules same as that of std::basic_[io]stream rules.

	/**  convert entry to boolean. valid for "1" or "0", or "true" or "false */
	AIAPI AIErr (*AsBoolean) ( AIEntryRef entry, ASBoolean* value );
	/** convert entry to an integer if possible. */
	AIAPI AIErr (*AsInteger) ( AIEntryRef entry, ASInt32* value );
	/** convert entry to a real if possible. */
	AIAPI AIErr (*AsReal) ( AIEntryRef entry, ASReal* value );
	/** convert entry to a string if possible. */
	AIAPI AIErr (*AsString) ( AIEntryRef entry, const char** value );
	/** convert entry to a UID reference it must be of the form: "id('foo')", where "foo" is the uid. */
	AIAPI AIErr (*AsUIDREF) ( AIEntryRef entry, AIUIDREFRef* value );

	/** Get the value of the entry assuming it is an art style. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToArtStyle) ( AIEntryRef entry, AIArtStyleHandle* value );
	/** Construct an entry from an art style value. */
	AIAPI AIEntryRef (*FromArtStyle) ( AIArtStyleHandle value );

	/* New in Illustrator 12.0 */

	/** Get the value of the entry assuming it is an ai::UnicodeString. Returns an error
		if the value is not. Note that this automatically decrements
		the reference count. */
	AIAPI AIErr (*ToUnicodeString) ( AIEntryRef entry, ai::UnicodeString& value );
	/** Construct an entry from an ai::UnicodeString value. */
	AIAPI AIEntryRef (*FromUnicodeString) ( const ai::UnicodeString& value );
	/** convert entry to a UnicodeString if possible. */
	AIAPI AIErr (*AsUnicodeString) ( AIEntryRef entry, ai::UnicodeString& value );

} AIEntrySuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
