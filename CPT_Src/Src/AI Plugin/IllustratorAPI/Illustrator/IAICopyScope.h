#ifndef _IAICOPYSCOPE_H_
#define _IAICOPYSCOPE_H_

/*
 *        Name:	IAICopyScope.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Wrapper class for AIArt copy scope objects.
 *
 * Copyright (c) 2003 Adobe Systems Incorporated, All Rights Reserved.
 *
 */
 
#include "AIArt.h"
#include <exception>
 
namespace ai {

/** @ingroup Errors */
#define kAICopyScopeAlreadyInitialized	'CSAI'

/** CopyScope wrapper class for AIArtSuite AICopyScopeHandle object.

	Copy scopes allow the client to group a collection of art object duplication 
	operations together as parts a single logical copy operation.  This is required
	for Illustrator to maintain some cross object information through a set of copy operations.
	An example of this is the new linked text object information.  For the duplicated
	text frame objects to keep their linked attributes, the linked frames must be duplicated
	in the same copy scope.  A client can achieve this by either ensuring that all of the linked
	objects are copied by a single call to AIArtSuite::DuplicateArt(), or by creating a wrapping
	copy scope and making multiple individual calls to DuplicateArt.
	
	CopyScope objects may not be assigned or copied.
*/
class CopyScope {

public:
	/** 
		Constructs an empty copy scope.
	 */
	CopyScope() AINOTHROW
	: fScope(0)
	{}

	/** 
		Constructs a copy scope of type "kind".
	 */
	explicit CopyScope(AICopyScopeKind kind)
	: fScope(0)
	{
		Initialize(kind);
	}

	/** 
		Destructor will destroy the owned scope if it
		hasn't already been properly terminated.  Does not
		throw an execption.  Clients are encouraged to directly
		call Terminate() themselves in order to determine if there
		was an error terminating the scope.
	 */
	~CopyScope()
	{
		if (IsValid())
		{
			(void) Terminate(std::nothrow);
		}
	}
	
	/** 
		Was this scope initialized.
	*/
	bool IsValid() const AINOTHROW
	{
		return fScope != 0;
	}

	/**
		Initialize an uninitialized copy scope -- non-throwing.
		Attempting to initialize an already initialized scope will
		result in an #kAICopyScopeAlreadyInitialized error.
	 */
	AIErr Initialize(AICopyScopeKind kind, const std::nothrow_t&) AINOTHROW
	{
		AIErr result = kNoErr;
		
		if (!IsValid())
		{
			result = sAIArt->CreateCopyScope(kind, &fScope);
		}
		else
		{
			result = kAICopyScopeAlreadyInitialized;
		}
			
		return result;
	}
	
	/**
		Initialize an uninitialized copy scope -- throws exceptions.
	 */
	void Initialize(AICopyScopeKind kind)
	{
		AIErr result = Initialize(kind, std::nothrow);
		if ( result )
			throw ai::Error(result);
	}

	/** 
		Terminate the copy scope owned by the scope object. It is
		okay to terminate an uninitialized CopyScope object.
		This method does not throw.
	 */
	AIErr Terminate(const std::nothrow_t&) AINOTHROW
	{
		AIErr result = kNoErr;
		
		if (IsValid())
		{
			result = sAIArt->DestroyCopyScope(fScope);
			fScope = 0;
		}

		return result;
	}
	
	/**
		Terminate the copy scope owned by the scope object -- throws exceptions.
	*/
	void Terminate()
	{
		AIErr result = Terminate(std::nothrow);
		if ( result )
			throw ai::Error(result);
	}

private:
	/**
		copying and assignment of scope objects is not allowed
	 */
	CopyScope(const CopyScope& scope);
	CopyScope& operator= (const CopyScope& scope);
	
private:
	AICopyScopeHandle fScope;
};


} // end of namespace ai


#endif	// _IAICOPYSCOPE_H_
