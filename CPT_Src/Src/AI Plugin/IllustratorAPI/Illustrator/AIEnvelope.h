#ifndef __AIEnvelope__
#define __AIEnvelope__

/*
 *        Name:	AIEnvelope.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10.0 Envelope Suite.
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AIDict__
#include "AIDictionary.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIEnvelope.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIEnvelopeSuite						"AI Envelope Suite"
#define kAIEnvelopeSuiteVersion1				AIAPI_VERSION(1)
#define kAIEnvelopeSuiteVersion					kAIEnvelopeSuiteVersion1
#define kAIEnvelopeVersion						kAIEnvelopeSuiteVersion

// Envelope options
#define kDeformExpandAppearance 		(1L << 0)
#define kDeformExpandPatternFills		(1L << 1)
#define kDeformExpandLinearGradients	(1L << 2)

// Additional options for DeformArt call. (These are always done for envelopes.)
#define kDeformExpandText			(1L << 3)
#define kDeformOpacityMasks			(1L << 4)
#define kDeformExpandPluginGroups	(1L << 5)
#define kDeformExpandSymbols		(1L << 6)

#define kDeformAll					(0x0000FFFF)

#define kDeformNonAARaster			(1L << 16)
#define kDeformRasterMakeClip		(1L << 17)


/** @ingroup DictKeys
	Dictionary key for DeformArt call. AIArtHandle of kMeshArt type */
#define kAIDeformMeshKey		"DeformMesh"
/** @ingroup DictKeys
	Dictionary key for DeformArt call. ASInt32 [0..100] */
#define kAIDeformFidelityKey	"DeformFidelity"	// ASInt32  [0..100]
/** @ingroup DictKeys
	Dictionary key for DeformArt call. ASInt32 */
#define kAIDeformOptionsKey		"DeformOptions"		// ASInt32

/*******************************************************************************
 **
 **	Types
 **
 **/

/** Envelope options */
typedef struct 
{
	long envOptions;
	/** range [0..100] */
	long fidelity;

} AIEnvelopeOptions;

/** Pass in current progress; returns false if user cancelled, otherwise true. */
typedef AIAPI AIBoolean (*AIDeformProgressProc)(long current, long total);

/** Canned warps */
typedef enum
{
	kWarpStyleArc = 1,
	kWarpStyleArcLower,
	kWarpStyleArcUpper,
	kWarpStyleArch,
	kWarpStyleBulge,
	kWarpStyleShellLower,
	kWarpStyleShellUpper,
	kWarpStyleFlag,
	kWarpStyleWave,
	kWarpStyleFish,
	kWarpStyleRise,
	kWarpStyleFisheye,
	kWarpStyleInflate,
	kWarpStyleSqueeze,
	kWarpStyleTwist
	
} AIWarpStyle;


/*******************************************************************************
 **
 **	Suite
 **
 **/


/** APIs for creating and managing envelopes. */
typedef struct {

	/** If art is nil or numArt == 0, this function will envelope the current selection on 
		the artboard. Otherwise, it will envelope the art passed in. This function will use
		envelopeObject as the envelope if envelopeObject is not nil. Otherwise, it will use
		the bounding box of the given (or selected) art. Options can be nil if client just
		wants to use default options (ie. those stored in preferences). New envelope is by
		default fully selected and created above the first art passed in. Will not work on
		art stored directly inside dictionaries, but will work on children of art stored
		in dictionaries. */
	AIAPI AIErr (*MakeEnvelope) ( AIArtHandle* art, int numArt, AIArtHandle envelopeObject,
								  AIEnvelopeOptions* options, AIArtHandle* envelope );

	/** Replaces the envelope in the envelope plugin group with one that warps the artwork
		with the given warp style, bend, distortions and rotation. Will only have an effect 
		if EditingMesh would return true. Resulting envelope will by default be fully selected. */
	AIAPI AIErr (*WarpEnvelope) ( AIArtHandle envelope, AIWarpStyle warpStyle, AIReal bend,
								  AIReal horizDistort, AIReal vertDistort, AIBoolean rotate90 );

	/** Releases the envelope from the given envelope plugin group. The original contents are
		moved above where the plugin group was originally. Plugin group is then disposed.
		Released art will by default be selected. Will not work on envelopes stored directly
		inside dictionaries, but will work on children of art stored in dictionaries. */
	AIAPI AIErr (*ReleaseEnvelope) ( AIArtHandle envelope );

	/** Gets the options for an envelope. */
	AIAPI void (*GetEnvelopeOptions) ( AIArtHandle envelope, AIEnvelopeOptions* options );
	/** Sets the options for an envelope. */
	AIAPI void (*SetEnvelopeOptions) ( AIArtHandle envelope, AIEnvelopeOptions* options );
	/** Returns the default options stored in the preferences. New envelopes will by default
		be created using the default options. */
	AIAPI void (*GetCurrentEnvelopeOptions) ( AIEnvelopeOptions* options );
	
	/** Tests whether an arbitrary art object is an envelope. */
	AIAPI AIBoolean (*IsEnvelope) ( AIArtHandle envelope );
	/** Is the mesh defining the distortion being edited? */
	AIAPI AIBoolean (*EditingMesh) ( AIArtHandle envelope );
	/** Are the contents of the mesh being edited? */
	AIAPI AIBoolean (*EditingContents) ( AIArtHandle envelope );
	/** Switch between editing the mesh and the contents of the envelope. */
	AIAPI AIErr (*ToggleEditability) ( AIArtHandle envelope );
	
	/** Get the envelope object for the given envelope plugin group. */
	AIAPI AIErr (*GetEnvelopeObject) ( AIArtHandle envelope, AIArtHandle* envelopeObject );
	/** Set the envelope object for the given envelope plugin group. The envelopeObject
		must be of type kPathArt or kMeshArt and must not be directly stored in a
		dictionary. */
	AIAPI AIErr (*SetEnvelopeObject) ( AIArtHandle envelope, AIArtHandle envelopeObject );
	
	/** Retrieve the objects being deformed by the envelope. Returned object will always
		be a group object, and may be the edit group of the envelope plugin group if
		EditingContents would return true. */
	AIAPI AIErr (*GetObjectsInEnvelope) ( AIArtHandle envelope, AIArtHandle* objects );

	/** Deforms the given art using the parameters in the given dictionary. Does not
		make an envelope, but deforms the art in the same way an envelope would.
		Can pass in nil to srcRect, in which case the bounds of the source art
		will be used as the "source space."
		Can pass in nil to progressProc if you don't care for showing the user a
		progress bar. Will return kCanceledErr if user halts progress bar.
		If this call succeeds, deformedArt is created above artToDeform, which is
		not touched. The caller must be careful in passing in art that has a
		parent pointer. This call will fail if the incoming artToDeform has no
		parent pointer. */
	AIAPI AIErr (*DeformArt) ( AIArtHandle 			artToDeform, 
							   AIDictionaryRef 		deformParams,
							   AIRealRect* 			srcRect,
							   AIArtHandle*			deformedArt,
							   AIDeformProgressProc	progressProc );

	/** (The following function is currently not implemented. It will return kNotImplementedErr
		if called.) */
	AIAPI AIErr (*GenerateDeformMeshFromPath)( AIArtHandle pathArt,	
											   AIArtHandle *meshArt );

} AIEnvelopeSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
