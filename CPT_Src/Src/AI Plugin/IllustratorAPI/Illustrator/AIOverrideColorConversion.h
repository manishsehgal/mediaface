#ifndef __OverrideColorConversion__
#define __OverrideColorConversion__

/*
 *        Name:	AIOverrideColorConversion.h
 *   $Revision: 13 $
 *     Purpose:	Adobe Illustrator Suite to replace internal color
 *              conversion functionality.
 *
 * Copyright (c) 2002 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"          /* Include Adobe Standard Types */
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIOverrideColorConversion.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIOverrideColorConversionSuite			"Override AI Color Conversion Suite"
#define kAIOverrideColorConversionSuiteVersion5	AIAPI_VERSION(5)
#define kAIOverrideColorConversionSuiteVersion	kAIOverrideColorConversionSuiteVersion5
#define kAIOverrideColorConversionVersion		kAIOverrideColorConversionSuiteVersion


// JM - must synch with those in picasso.h
/** Working space references */
enum AIWorkingColorSpace {
	kGrayWS		= 0,
	kRGBWS		= 1,
	kCMYKWS		= 2
};


/** RGB image formats */
enum AIRGBPixelFormat {
	kARGB               = 0,
	kRGB                = 1
};


/* CMYK separations/channels */

#ifndef kCyan
#define kCyan                   0x00000001
#endif

#ifndef kMagenta
#define kMagenta                0x00000002
#endif

#ifndef kYellow
#define kYellow                 0x00000004
#endif

#ifndef kBlack
#define kBlack                  0x00000008
#endif

#ifndef kCMYK
#define kCMYK                   0x0000000f
#endif

/** rendering intents */
enum AIRenderingIntent {
	kICPerceptual				= 0,
	kICRelativeColorimetric	    = 1,
	kICSaturation				= 2,
	kICAbsoluteColorimetric   	= 3
};

/** policies on what to do with a document's profile when it is opened */
enum AIFileOpenColorProfilePolicy {
	/** convert document to working space */
	kConvert	= 0,
	/** remove profile from doc */
	kUntag		= 1,
	/** preserve the documents profile */
	kPreserve	= 2,
	/** user decided to cancel the open */
	kCancel		= 3
};

/** policies on what to do with a document's profile when it is saved */
enum AIFileSaveColorProfilePolicy {
	/** don't save a profile */
	kNoProfile	= 0,
	/** save with the working space profile */
	kWSProfile	= 1,
	/** save with the document profile */
	kDocProfile	= 2
};

/** Color conversion policies inferred from the color settings dialog,
	as to whether or not to actually do color conversions within the same
	colorspace family (such as RGB to RGB, or CMYK to CMYK) */
typedef enum ColorConvPolicy
{
	/** do actual conversion to maintain color appearance */
	kPreserveAppearance,
	/** don't convert, instead maintain numerical values */
	kPreserveNumbers
} ColorConvPolicy;		

/** Constants identifying premade color transforms */
typedef enum InternalColorTransformType
{
	kRGB2RGB=0,
	kRGB2CMYK,
	kCMYK2CMYK,
	kCMYK2RGB,
	kRGB2sRGB,
	kCMYK2sRGB,
	kLAB2RGB,
	
	// The types below are new for AI 11
	//New for JPEG 2000.
	ksGray2RGB,
	ksYCC8_2RGB,
	kesRGB2RGB,
	kROMMRGB2RGB,
	kesYCC8_2RGB,

	//Same as above with 8 < BPC <= 16. Only used by Jpeg2000.
	kCIELab_16Bit2RGB, //Also for CIELab 8 bit.
	ksGray_16Bit2RGB,
	ksYCC8_16Bit2RGB,
	kesRGB_16Bit2RGB,
	kROMMRGB_16Bit2RGB,
	kesYCC8_16Bit2RGB,

	kNumTransforms
} InternalColorTransformType;

/** For used with color conversion #kCIELab_16Bit2RGB */
typedef struct {
	/** Native-Endian versions of the three JPX channel Range and Offset parameters from the EP field
		see JPEG2000 spec, Annex M, sec M.11.7.4 */
	ASUInt32 Range1;
	ASUInt32 Offset1;
	ASUInt32 Range2;
	ASUInt32 Offset2;
	ASUInt32 Range3;
	ASUInt32 Offset3;
	/** Native-Endian version of IL parameter from EP field. Ignored for CIEJab. */    
	ASUInt32 Illuminant;
} JPXLabParams;
   
/**	Ranges for color values */
typedef struct {
	/** Usual value -128 */
	ASReal		min;
	/** Usual value +127 */
	ASReal		max;
} AIRange;


/**	Floating point CIE XYZ color value */
typedef struct  {
	ASReal		X;
	ASReal		Y;
	ASReal		Z;
} AIXYZColor;


/**	Calibration information for constructing a grayscale profile. */
typedef struct {
	AIXYZColor		whitePoint;
	AIXYZColor		blackPoint;
	ASReal		gamma;
} AIGrayCal;

/**	Calibration information for constructing a RGB profile. */
typedef struct {
	AIXYZColor		whitePoint;
	AIXYZColor		blackPoint;
	AIXYZColor		red;
	AIXYZColor		green;
	AIXYZColor		blue;
	ASReal		redGamma;
	ASReal		greenGamma;
	ASReal		blueGamma;
} AIRGBCal;

typedef struct  {
	ASUInt32		bytesPerValue;
	ASUInt32		count;
	void			*data;
} AIToneCurve;

/**	Calibration information for constructing a CMYK profile. */
typedef struct {
	AIXYZColor		whitePoint;
	AIXYZColor		blackPoint;
	AIXYZColor		w;
	AIXYZColor		c;
	AIXYZColor		m;
	AIXYZColor		y;
	AIXYZColor		k;
	AIXYZColor		cm;
	AIXYZColor		cy;
	AIXYZColor		ck;
	AIXYZColor		my;
	AIXYZColor		mk;
	AIXYZColor		yk;
	AIXYZColor		cmy;
	AIXYZColor		cmk;
	AIXYZColor		cyk;
	AIXYZColor		myk;
	AIXYZColor		cmyk;
	
	AIToneCurve	cTRC;
	AIToneCurve	mTRC;
	AIToneCurve	yTRC;
	AIToneCurve	kTRC;

	ASReal		opticalGamma;

} AICMYKCal;
   
/*******************************************************************************
 **
 **	Suite
 **
 **
 */

/** The override color conversion suite provides APIs for managing and performing color
	conversions. See also the AIColorConversionSuite.
 */
typedef struct {

	/** Converts RGB image samples into CMYK to figure out if any of the samples
		were out of gamut. Return value inGamut will be true iff ALL the samples are in
		Gamut. The samples must be 8 bits per component. See #AIRGBPixelFormat for the
		rgbFormat values. */
	AIAPI AIErr ( *GamutRGBToCMYKImage )( void *srcBuf, const ASUInt32 numSamples, 
  										const ASUInt32 rgbFormat, ASBoolean *inGamut );

	/** Creates a transformation using the given source & destination profiles.
		renderIntent can be NULL to use default as selected by OverrideCC dialog.
		Profiles can be obtained using CreateProfileFromTag() or similar API. */
	AIAPI AIErr ( *CreateXform )(const AIColorProfile srcProfile, const AIColorProfile destProfile, ASUInt32 *xform ); 
	/** Frees a transform created with CreateTransform(). */
	AIAPI void  ( *FreeXform )( const ASUInt32 xform );

	/** Applies the given transform to a single color value. The convType parameter defines the
		format and range of the color components. */
	AIAPI AIErr ( *ApplyXformSample )( const ASUInt32 xform, ASFixed *inColor, ASFixed *outColor,
			const InternalColorTransformType convType, ASBoolean isLAB );
	/** Applies the given transform to a buffer of image data. The convType parameter defines the
		format and range of the color components. */
	AIAPI AIErr ( *ApplyXformImage )( const ASUInt32 xform, void *inBuffer, void *outBuffer, const ASUInt32 numPixels,
			const InternalColorTransformType convType, ASBoolean isLAB , ASInt16 format);

	/** Converts an ICC tag into a profile (must be free'ed) */
	AIAPI AIErr ( *CreateProfileFromTag )( void *buffer, const ASUInt32 size, AIColorProfile *profile ); 
	/** Get a current working space profile (must be free'ed). See #AIWorkingColorSpace. */
	AIAPI AIErr ( *GetWSProfile )( const ASUInt32 whichProfile, AIColorProfile *profile );
	/** Once a transform is created, the profile can be free'ed. */
	AIAPI void  ( *FreeProfile )( AIColorProfile profile );

	/** GetOpenPolicy() is intended to be used when opening a document. profile is the color profile
		if any embedded in the document. colorModel is the color model of the document (see 
		#AIDocumentColorModelValue). This API consults the color management policies, putting up
		dialogs to ask the user if need be, to determine how the profile should be used. The
		return value is one of #AIFileOpenColorProfilePolicy. If *profile is NULL and the policies
		indicate that a profile should be assigned then it will be set to the correct profile on
		return. */	
	/** In Zodiac, this will also be used when place-linking file formats that contain profiles.
	    Since link and open behavior is slightly different, the isLink argument differentiates 
		between the two. The linkHasMultipleProfiles argument is used when a linked file has more
		than one profile for a given colorspace. If the linked file contains both RGB and CMYK, this
		 should be called for each colorspace as the policies can be different. */
	AIAPI ASUInt32  ( *GetOpenPolicy )(AIColorProfile *profile, ASUInt32 colorModel, ASBoolean isLink,
		ASBoolean linkHasMultipleProfiles);
	/** GetSavePolicy() is intended to be used when saving a document. whichSpace is the working space
		corresponding to the document color model. See #AIWorkingColorSpace for working spaces and
		#AIDocumentColorModeValue for document color models. On return policy is set to one of the
		values of #AIFileSaveColorProfilePolicy indicating which profile, if any, should be saved with
		the file. The default state of the "embed profile?" checkbox is also returned. If a profile
		is to be embedded then its name is returned in the character buffer profName. */
	AIAPI void  ( *GetSavePolicy )( const ASUInt32 whichSpace, ASUInt32 *policy, ASUInt32 *defaultState, ai::UnicodeString& profName);

	/** If data is null, returns the size of the buffer needed. If data is non-null, puts the profiles
		tag data into the buffer */
	AIAPI AIErr ( *GetProfileData ) (const AIColorProfile profile, ASUInt32 *size, void *data );
	/** Given a profile, returns a description string. */
	AIAPI AIErr ( *GetProfileName) (const AIColorProfile profile, ai::UnicodeString& profileName ) ;

	/** Creates and caches transforms to be used for *all* internal color conversions and in
		particular those done by the AIColorConversionSuite.
		
		- Call CreateInternalTransform() to create a transfrom
		- Call the ColorConversionSuite, with the appropriate src and dest colorspaces
		- Call FreeAllInternalTransforms() when done.

		For example, to do a calibrated CMYK to RGB conversion of a single sample (images work too)
		
		- CreateInternalTransform(CMYK2RGB, cmykProfile, rgbProfile)
		- AIColorConversionSuite::ConvertSampleColor(kAICMYKColorSpace, cmykSample, kAIRGBColorSpace, rgbSample)
		- FreeAllInternalTransforms()

		For the first four color transform types, supply the source and destination profiles.
		
		For the "2sRGB" transforms, if the source profile is null the profile of the top
		document is used. If the top document has no profile, then the corresponding working space
		will be used instead. The destination profile is ignored and will be set internally to sRGB.
		
		For the "LAB2RGB", source RGB data is interpreted as LAB. The source profile is
		ignored and is set internally to LAB. If the destination profile is null, the profile of the
		top document will be used. If that is NULL, the RGB working space will be used instead.
		
		So, to convert a sample in the top document from RGB to sRGB,
		
		- CreateInternalTransform(RGB2sRGB, NULL, NULL)
		- ConvertSampleColor(kAIRGBColorSpace, rgbSample, kAIRGBColorSpace, sRGBSample)
		- FreeAllInternalTransforms()
		
		To convert from LAB to RGB, 
		
		- CreateInternalTransform(LAB2RGB, NULL, NULL)
		- ConvertSampleColor(kAIRGBColorSpace, labSample, kAIRGBColorSpace, rgbSample)
		- FreeAllInternalTransforms()
	*/
	AIAPI AIErr ( *CreateInternalTransform) (const InternalColorTransformType whichOne, AIColorProfile scrProfile, AIColorProfile destProfile);
	/** Frees all cached transforms. Default color conversion is done once xform is free'ed. */
	AIAPI void  ( *FreeAllInternalTransforms) (void);

	/** Returns true if profiles are the same, false if different or are not valid profiles. */
	AIAPI void  ( *ProfsEqual) (const AIColorProfile srcProfile, const AIColorProfile destProfile, ASUInt32 *match); 
	
	/** Gets a transform created by CreateInternalTransform(), so it can be used in ApplyXformSample() or
		ApplyXformImage(). For example,

		- CreateInternalTranform(LAB2RGB, NULL, NULL)
		- GetInternalTransform(LAB2RGB, &xform);
		- ApplyXformSample(xform, incolor, outcolor, KLAB2RGB. false);
		- FreeAllInternalTransforms()

		Note that input LAB data must be scaled from
		
		- L in 0 to 100 => 0 to 255
		- A,B -128 to 127  => 0 to 255
	*/
	AIAPI	void  ( *GetInternalTransform)(const InternalColorTransformType whichOne,  ASUInt32 *xform);
	
	/** Signals that Black Point Compensation should be on for any color transforms subsequently performed.
		(This should be used for ADM drawing operations) */
	AIAPI void  ( *BPCOn ) (void) ;
	/** Set Black Point Compensation back to what it was */
	AIAPI void  ( *RestoreBPC )(void);

	// New in Illustrator 11.0

	/** Construct a profile from grayscale calibration information. */
	AIAPI AIErr ( *MakeProfileFromCalGray)(AIGrayCal *grayCal, AIColorProfile *profile);
	/** Construct a profile from RGB calibration information. */
	AIAPI AIErr ( *MakeProfileFromCalRGB)(AIRGBCal *rgbCal, AIColorProfile *profile);
	/** Construct a profile from CMYK calibration information. */
	AIAPI AIErr ( *MakeProfileFromCalCMYK)(AICMYKCal *cmykCal, AIColorProfile *profile);
	/** This function is similar to function CreateInternalTransform() for creating color transforms. This
		function is used when  source has more than 8 bits per channel to the maximum of 16 bit per channel.
		For source of more than 16bit, caller will have to truncate data to 16 bit himself and then use this
		function. BitDepth is array of size of number of channels, indicating bit depth of each channel.
		JPXLabParams is additional parameter required for kCIELab_16Bit2RGB color conversion and should be NULL
		for others. For CIELab color conversion, source can have 8bits per channel data also. */
	AIAPI AIErr  ( *CreateInternalTransform_16Bit)(const InternalColorTransformType whichOne, AIColorProfile srcProfile, AIColorProfile dstProfile,
			ASUInt8* bitDepth, JPXLabParams* jpxLabParams);

	/** Returns the color conversion policies and the state of "AI6" mode from the color settings dialog */
	AIAPI void   ( *GetColorSettingsPolicies)(ASBoolean *AI6Mode, ColorConvPolicy *rgbPolicy, ColorConvPolicy *cmykPolicy);

	/** returns the render intent from the color settings dialog **/
	AIAPI AIRenderingIntent ( *GetColorSettingsRenderIntent)(void);

} AIOverrideColorConversionSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
