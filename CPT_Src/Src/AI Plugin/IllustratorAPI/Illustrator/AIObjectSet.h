#ifndef __AIObjectSet__
#define __AIObjectSet__

/*
 *        Name:	AIObjectSet.h
 *   $Revision: 17 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 12.0 Pattern Fill Suite.
 *
 * Copyright (c) 1986-2004Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIPlugin__
#include "AIPlugin.h"
#endif

#ifndef __AIColor__
#include "AIColor.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#include "AIUnicodeString.h"


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIObjectSet.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIObjectSetSuite				"AI ObjectSet Suite"
#define kAIObjectSetSuiteVersion2		AIAPI_VERSION(2)
#define kAIObjectSetSuiteVersion		kAIObjectSetSuiteVersion2
#define kAIObjectSetVersion				kAIObjectSetSuiteVersion

/** @ingroup Notifiers */
#define kAIObjectSetChangedNotifier		"AI Object Set Changed Notifier"
/** @ingroup Notifiers */
#define kAIReplaceColorNotifier			"AI Replace Color Notifier"

/** @ingroup Errors */
#define kNameClashErr			'NmCl'
/** @ingroup Errors */
#define kUninitializedDataErr	'init'
/** @ingroup Errors */
#define kCantDoThatNowErr		'!now'

/** @ingroup Callers */
#define kCallerAIObjectSet					"AI Object Set"

/** @ingroup Selectors.
	See AIObjectSetMessage */
#define kSelectorObjectSetDisposeContents "AI Object Set Dispose Contents"
/** @ingroup Selectors.
	See AIObjectSetMessage */
#define kSelectorObjectSetMarkUsage "AI Object Set Mark Usage"
/** @ingroup Selectors.
	See AIObjectSetMessage */
#define kSelectorObjectSetUpdateInternal "AI Object Set Update Internal"
/** @ingroup Selectors.
	See AIObjectSetMessage */
#define kSelectorObjectSetUpdateExternal "AI Object Set Update External"
/** @ingroup Selectors.
	See AIObjectSetMessage */
#define kSelectorObjectSetPurge "AI Object Set Purge"
/** @ingroup Selectors.
	See AIObjectSetMessage */
#define kSelectorObjectSetChange "AI Object Set Change"
/** @ingroup Selectors.
	See AIObjectSetMessage */
#define kSelectorObjectSetChangeIndirect "AI Object Set Change Indirect"
/** @ingroup Selectors.
	See AIObjectSetMessage */
#define kSelectorObjectSetCopy "AI Object Set Copy"
/** @ingroup Selectors.
	See AIObjectSetMessage */
#define kSelectorObjectSetInitializationDone "AI Object Set Initialization Done"


/*******************************************************************************
 **
 **	Types
 **
 **/

/** This is a reference to an object set. It is never dereferenced. */
typedef struct _t_AIObjectSetOpaque *AIObjectSetHandle;
/** This is a reference to an object in an object set. */
typedef struct _t_AIObjectOpaque *AIObjectHandle;

typedef enum {
	kAIObjectSetChangeDelete,
	kAIObjectSetChangeNew, 
	kAIObjectSetChangeChange,
	kAIObjectSetChangeRename
} AIObjectSetChangeType;

/** Types of objects that can be stored in an object set. */
typedef enum {
	kAIObjectMapCustomColor,
	kAIObjectMapPattern,
	kAIObjectMapBrushPattern,
	kAIObjectMapGradient,
	kAIObjectMapPluginObject,
	kAIObjectMapDone = -1
} AIObjectMapType;

/** An object map is a set of type/offset pairs giving the location of each object handle
	in the object definition.  It is terminated by an entry with objectType = kAIObjectMapDone.  A
	particular instance can be nil for a particular instance, which indicates that although some
	instances of the type store an object handle there, this particular one does not.  To paraphrase,
	if the map says there is an object of type xyz at offset abc, then the data at offset abc must
	either be a valid handle of the appropriate type of object, or be nil.
*/
typedef struct {
	/** A value from #AIObjectMapType */
	short objectType;
	short offset;
} AIObjectMap;

/** AIReplaceColorTag is parallel to #AIColorTag except BrushPattern */
typedef enum {
	kRepGrayColor = kGrayColor,				
	kRepFourColor = kFourColor,					
	kRepPattern = kPattern,				
	kRepCustomColor = kCustomColor,			
	kRepGradient = kGradient,				
	kRepThreeColor = kThreeColor,				
	kRepNoneColor = kNoneColor,
	kRepBrushPattern = kNoneColor + 5,

	kRepColorDone = -1,
	kRepColorSameTagAsInAIColor = -2

} AIReplaceColorTag;

/** AIReplaceColor is used in the #kAIReplaceColorNotifier */
typedef struct {
	AIReplaceColorTag kind;			/* Used to determine the color variant */
	AIColorUnion c;
} AIReplaceColor;

/** Data for the #kAIReplaceColorNotifier */
typedef struct {
	/** how many pairs of AIReplaceColor */
	ASInt32			pairs;
	/** Array of AIReplaceColor. Each pair indicates a color to replace and its replacement. */
	AIReplaceColor *colorPtr;
} AIReplaceColorNotifierData;

/** The contents of a message. Message semantics:

	- #kSelectorObjectSetDisposeContents. The object in the message is about to be permanently disposed.
		Do not free the internal or external memory (the suite will do this) but dispose
		of any referred-to memory.  Subobject and change fields are unused.
	- #kSelectorObjectSetMarkUsage. Mark all objects managed by this plugin that are used in the
		current document by calling AIObjectSetSuite::MarkObjectInUse().  If markArt is not nil, just
		mark all objects within that sub-tree.  Set, object, subobject, and change fields are unused.
	- #kSelectorObjectSetUpdateInternal. Fetch the external representation for this object and update
		the internal. Also serves to notify of a new object; this will be the first selector received
		for a new one.  Subobject and change fields are unused.
	- #kSelectorObjectSetUpdateExternal. Fetch the internal representation for this object and update
		the external. Subobject and change fields are unused.
	- #kSelectorObjectSetPurge. The object contains a reference to the object described in the
		subobject fields, and that object is going away.  Remove the reference to it and make any
		necessary changes to the artwork.  Change fields are unused
	- #kSelectorObjectSetChange. The object has been changed as a result of an undo/redo.  Subobject
		fields are unused.
	- #kSelectorObjectSetChangeIndirect. The object contains a reference to an object that has changed.  
		Make any necessary changes to the artwork.  Subobject and change fields are unused.
		(Note, change from AI8, which set subobject).
	- #kSelectorObjectSetCopy. The object has been copied into the current document.
		Subobject and change fields are unused.
	- #kSelectorObjectSetInitializationDone. All UpdateInternal calls for objects that were in the
		document are over with.  Called once per plugin, not per object set.  All fields are unused.
*/
typedef struct {
	SPMessageData d;
	AIObjectSetHandle set;
	AIObjectHandle object;
	/** A value from #AIObjectMapType */
	short subobjectType;
	union {
		/** Pattern or brush pattern */
		AIPatternHandle pattern;
		AIGradientHandle gradient;
		AICustomColorHandle ccolor;
		struct {
			AIObjectSetHandle set;
			AIObjectHandle object;
		} object;
	} subobject;
	AIObjectSetChangeType changeType;
	AIArtHandle markArt;
	/** Added in AI12: see #AIVersion */
	AIVersion aiVersion;
} AIObjectSetMessage;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	Object sets provide a mechanism for attaching global information to a document. Their use is
	deprecated in favor of dictionaries. The document contains a dictionary where arbitrary information
	can be stored. See AIDocumentSuite::GetDictionary() and AIDocumentSuite::GetNonRecordedDictionary().
 */
typedef struct {

	AIAPI AIErr (*NewObjectSet) ( SPPluginRef self, unsigned char *name, AIObjectMap *map, AIObjectSetHandle *set );
	AIAPI AIErr (*CountObjectSets) ( long *count );
	AIAPI AIErr (*GetNthObjectSet) ( long n, AIObjectSetHandle *set );
	AIAPI AIErr (*GetObjectSetName) ( AIObjectSetHandle set, unsigned char *name );
	AIAPI AIErr (*GetObjectSetByName) ( unsigned char *name, AIObjectSetHandle *set );
	AIAPI AIErr (*GetObjectSetMap) ( AIObjectSetHandle set, AIObjectMap **map );
	AIAPI AIErr (*NewObject) ( AIObjectSetHandle setHandle, const ai::UnicodeString& name, void *def, 
		int defLen, AIObjectHandle *object );
	AIAPI AIErr (*DeleteObject) ( AIObjectSetHandle setHandle, AIObjectHandle object);
	AIAPI AIErr (*SetObjectInternal) ( AIObjectSetHandle setHandle, AIObjectHandle object, void *data, long dataLen);
	AIAPI AIErr (*SetObjectExternal) ( AIObjectSetHandle setHandle, AIObjectHandle object, const char *data);
	AIAPI AIErr (*GetObjectInternal) ( AIObjectSetHandle setHandle, AIObjectHandle object, void **data, long *dataLen);
	AIAPI AIErr (*GetObjectExternal) ( AIObjectSetHandle setHandle, AIObjectHandle object, char **data);
	AIAPI AIErr (*CountObjects) ( AIObjectSetHandle setHandle, long *count);
	AIAPI AIErr (*GetNthObject) ( AIObjectSetHandle setHandle, long n, AIObjectHandle *object);
	AIAPI AIErr (*GetObjectName) ( AIObjectSetHandle setHandle, AIObjectHandle object, ai::UnicodeString& name);
	AIAPI AIErr (*SetObjectName) ( AIObjectSetHandle setHandle, AIObjectHandle object, const ai::UnicodeString& name);
	AIAPI AIErr (*GetObjectByName) ( AIObjectSetHandle setHandle, ai::UnicodeString& name, AIObjectHandle *object);
	AIAPI AIErr (*NewObjectName) ( AIObjectSetHandle setHandle, ai::UnicodeString& name );
	AIAPI AIErr (*MarkObjectInUse) ( AIObjectSetHandle setHandle, AIObjectHandle object);
	AIAPI AIErr (*RetargetForCurrentDocument) ( AIObjectHandle object, AIObjectHandle *newObj );

	AIAPI AIErr (*GetExternalObjectName) (AIObjectSetHandle setHandle, AIObjectHandle object, 
		AIVersion aiVersion, char* buf, long* bufLen );

} AIObjectSetSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
