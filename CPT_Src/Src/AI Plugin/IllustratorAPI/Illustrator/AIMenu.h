#ifndef __AIMenu__
#define __AIMenu__

/*
 *        Name:	AIMenu.h
 *      Author:	 
 *        Date:	   
 *     Purpose: Adobe Illustrator Menu Suite.	
 *
 * Copyright (c) 1986-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIPlugin__
#include "AIPlugin.h"
#endif

#ifndef __ASHelp__
#include "ASHelp.h"
#endif

#ifdef MAC_ENV
#ifndef __MENUS__
//Needed for the MenuRef below
#include <Menus.h>
#endif
#endif

#ifndef _IAIUNICODESTRING_H_
#include "IAIUnicodeString.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIMenu.h */

// Mac OS headers #define CountMenuItems, but we want to use it as a name.
#ifdef CountMenuItems
#undef CountMenuItems
#endif

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIMenuSuite					"AI Menu Suite"
#define kAIMenuVersion8					AIAPI_VERSION(8)		// In AI12

// latest version
#define kAIMenuSuiteVersion				kAIMenuVersion8
#define kAIMenuVersion					kAIMenuSuiteVersion


/** @ingroup Notifiers
	The menu changed notifier is received when a menu item is modified using
	the Menu Suite functions. It receives no associated data.
*/
#define kAIMenuChangedNotifier			"AI Menu Changed Notifier"

/** @ingroup Callers
	This is the menu caller. */
#define kCallerAIMenu					"AI Menu"

/** @ingroup Selectors
	When the kSelectorAIGoMenuItem selector is received by a plug-in it indicates that
	one of its installed menu items was selected by the user. If the plug-in has more
	than one menu item installed, it would determine which one was selected by comparing
	the menu handle in the AIMenuMessage to those it had saved in its globals record.
*/
#define kSelectorAIGoMenuItem			"AI Go"
/** @ingroup Selectors
	This selector is passed to the plug-in when there has been a mouse down in the menu
	bar and it is an opportunity for the plug-in to change the appearance of its menu(s)
	before they are displayed. Not all menu plug-ins will receive this selector; it must
	be requested by setting the menu plug-in option #kMenuItemWantsUpdateOption. It is
	the only time the AIMenuSuite::GetUpdateFlags() function will return valid values. If
	the plug-in menu does not use the GetUpdateFlags() function in setting its appearance,
	the menu appearance can be updated at any time.
*/
#define kSelectorAIUpdateMenuItem		"AI Update"


/** These are the options available to a menu item. */
typedef ASInt32 AIMenuItemOption;
enum  {
	kMenuItemNoOptions    				= 0,
	/** Use this option in order to receive the #kSelectorAIUpdateMenuItem selector. */
	kMenuItemWantsUpdateOption			= (1L<<0),
	kMenuItemAlwaysEnabled				= (1L<<1),
	/** Use this option if you are creating a separator. No need to fill in the name in the AIMenuData(US) struct */
	kMenuItemIsSeparator				= (1L<<2),
	/** You cannot create an item with this option, but if you are iterating through items, this will tell you if the item
		you are currently examining is a groupheader. See SetMenuGroupHeader for how to set a menu group header */
	kMenuItemIsGroupHeader				= (1L<<3),
	/** This item ignores the sorted alphabetically option that it's group may have */
	kMenuItemIgnoreSort					= (1L<<4)
};

/** These are the options available to a menu group. */
typedef ASInt32 AIMenuGroupOption;
enum  {
	kMenuGroupNoOptions					 = 0,
	/** If this option is set, menu items in the group will appear alphabetically.
		By default, menu items appear in the order in which they are added. */
	kMenuGroupSortedAlphabeticallyOption = (1L<<0),
	kMenuGroupSeparatorOption			= (1L<<1),
	kMenuGroupSeparatorBelowOption		= (1L<<2),
	/** You cannot create a group with this option, but if you are iterating through groups, this will tell you if the group
		you are currently examining has a header. See SetMenuGroupHeader for how to set a menu group header */
	kMenuGroupHasHeader					= (1L<<3)
};


/** Specifies the action to be performed by the automatic menu update logic.
	Only one action may be specified. See AIMenuSuite::UpdateMenuItemAutomatically(). */
enum AIAutoMenuUpdateAction {
	/** Enables the item if the criteria evaluate to true and disables it otherwise. */
	kAutoEnableMenuItemAction			= (1L<<0),
	/** Puts a check mark next to the item if the criteria evaluate to true removes
		it otherwise. */
	kAutoCheckMenuItemAction			= (1L<<1)
};

/** Bits that identify types of objects that could appear in a document or selection.
	These bits are or'ed together to indicate the types of objects that must be
	present or not present for a menu item action to be performed. See
	AIMenuSuite::UpdateMenuItemAutomatically(). */
enum AIAutoMenuUpdateObjectCriteria {
	kIfGroup							= (1L<<0),
	kIfPath								= (1L<<1),
	kIfPostscript						= (1L<<2),
	kIfPlaced							= (1L<<3),
	kIfText								= (1L<<4),
	kIfTextPath							= (1L<<5),	// repurposed in AI CS to mean a path that has text in or on it
													// Note that kIfTextPath can be on for a selection even if kIfText is off,
													// when a text path is direct-selected
	kIfTextFlow							= (1L<<6),	// obsolete as of AI CS (AI 11)
	kIfTextRun							= (1L<<7),	// obsolete as of AI CS (AI 11)
	kIfGuide							= (1L<<8),
	kIfGraphData						= (1L<<9),
	kIfMask								= (1L<<10),
	kIfEndMask							= (1L<<11),	// obsolete as of AI 9
	kIfNormalGroup						= (1L<<12),
	kIfCompoundGroup					= (1L<<13),
	kIfWrapAroundGroup					= (1L<<14),
	kIfClipGroup						= (1L<<15),
	kIfPointText						= (1L<<16),
	kIfInPathText						= (1L<<17),
	kIfOnPathText						= (1L<<18),
	kIfThreadedText						= (1L<<19),
	kIfRaster							= (1L<<20),
	kMacIfPlugin						= (1L<<21),	// AI8
	kIfMesh								= (1L<<22),	// AI8
	kIfHasStyledArt						= (1L<<23),	// AI9
	kIfSymbol							= (1L<<24),	// AI10
	kIfForeignArt						= (1L<<25),	// AI11
	kIfLegacyTextArt					= (1L<<26),	// AI11
	kIfClassicPath						= (1L<<27),	// AI12: a path that is NOT inside a planar group
	kIfPlanarPath						= (1L<<28), // AI12: a path that is inside a planar group (see AIPlanarObject.h)
	kIfNonTextPath						= (1L<<29), // AI12: a path that does not have text in or on it
	kIfAnyArt							= (0xFFFFFFFFL)	//	AI11
};

/** Bits that identify properties of a document. These bits are or'ed together to
	indicate the properties that must true or false for a menu item action to be
	performed. See AIMenuSuite::UpdateMenuItemAutomatically(). */
enum AIAutoMenuUpdatePropertyCriteria {
	kIfCurrentLayerIsEditable			= (1L<<0),
	kIfOpenTextEdit						= (1L<<1),
	kIfAnyPrintingChars					= (1L<<2),
	kIfAnyHiddenObjects					= (1L<<3),
	kIfAnyLockedObjects					= (1L<<4),
	kIfAnyClipPaths						= (1L<<5),
	kIfOpenDocument						= (1L<<6),
	kIfCMYKDocument						= (1L<<7),		// AI9
	kIfRGBDocument						= (1L<<8),		// AI9
	kIfCurrentArtStyleIsNamed			= (1L<<9),		// AI9
	kIfHasPluginSelection				= (1L<<10)		// AI12
};

/** These are the modifiers available for AIMenuSuite::SetItemCmd() and
	AIMenuSuite::GetItemCmd() to a menu item with a command key. */
enum AIMenuItemModifier {
	kMenuItemCmdShiftModifier			= 1,
	kMenuItemCmdOptionModifier			= 2,
	kMenuItemCmdControlModifier			= 4		// the control key on the Mac.
};


/** @ingroup Errors */
#define kTooManyMenuItemsErr			'>MIT'


/*******************************************************************************
 **
 **	Types
 **
 **/

/** This is a reference to a menu item. It is never dereferenced. */
typedef struct _t_AIMenuItemOpaque	*AIMenuItemHandle;
typedef struct _t_MenuGroupOpaque	*AIMenuGroup;   // new for AI7.0

#if Macintosh
	typedef MenuRef AIPlatformMenuHandle;
#elif MSWindows
	typedef struct WinMenu	**AIPlatformMenuHandle;	 // can cast it to HMENU
#endif

/** parameters used when creating a menu item. */
typedef struct {
	const char *groupName;
	ai::UnicodeString itemText;
} AIPlatformAddMenuItemDataUS;

/** Structure returned by AIMenuSuite::GetPlatformMenuItem() to identify the platform
	specific menu item corresponding to an Illustrator item. */
typedef struct {
	/** HMENU on Windows or MenuRef on Macintosh. */
	AIPlatformMenuHandle menu;
	short item;
} AIPlatformMenuItem;

/** The SPMessageData is that which is received by all messages sent to a plug-in.
	The AIMenuItemHandle is a reference to the menu item receiving the message. If
	more than one menu item is installed it is used to determine the instance that
	should handle the message. */
typedef struct {
	SPMessageData d;
	AIMenuItemHandle menuItem;
} AIMenuMessage;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	The Menu Suite allows you to add menu items to the Illustrator menu structure.
	Menus can be used for a number of purposes: as stand alone interfaces
	to a plug-in feature, or to group a number of such features together. Other
	plug-in types may have an associated plug-in menu item. For instance, a
	window (or non-modal dialog box) will likely add a menu item to the Windows
	menu to hide and show the window.

	This section describes how to install and use plug-in menu items. It discusses
	the caller, selectors and messages that plug-in menus receive, as well as the
	functions provided to add menus, set options, and control menu appearance.
	Simple appearance changes can be set to happen automatically based on the
	artwork.

	Although this suite is completely cross-platform, it provides a set of functions
	that closely parallel the Macintosh toolbox functions for changing
	menu appearance.

	<b>Differences Between Filter and Menu Plug-ins</b>

	Menu plug-ins differ from Filter plug-ins in several ways. While Filters are
	always found under that menu heading, Menu plug-ins can appear in any of
	the Illustrator menus. Filters have a number of features provided automatically,
	including automatic repeat and setting the undo menu item text.
	Menu plug-ins must set the undo menu item text, but can also set command
	keys and control their appearance. Both plug-in types receive a go selector,
	but Filters also receive a selector to get parameters.

	<b>Plug-in Menu Selectors</b>

	The caller for menu plug-ins is #kCallerAIMenu. The selectors that may be
	received are listed below. The caller and selectors would be checked for in
	the plug-in's main function. The message data for each of these selectors
	is defined by the AIMenuMessage structure.

	- #kSelectorAIGoMenuItem
	- #kSelectorAIUpdateMenuItem

	Note: Should a plug-in with a menu become unloaded, it must first be
	reloaded before it can process the update selector. This potentially
	causes a performance lag perceivable to the user when the menu bar
	is clicked. Unless there is a pressing reason for a custom menu update,
	you should use the UpdateMenuItemAutomatically() function
	instead of requesting this selector.

	<b>Menu Groups</b>

	Menu items are installed using AIMenuSuite::AddMenu() function.	Menu items are
	added to menu groups. These are predefined locations within Illustrator's menu
	structure. Some examples of menu groups are: #kOpenMenuGroup, #kArrangeMoveMenuGroup
	and #kTypeSizeMenuGroup. The names are fairly descriptive of their menu location.
	A utilities group is located at the top to a menu group; for instance, the
	type size utilities	menu group is at the top of the type size hierarchical menu.
	The complete list of groups is found in the file AIMenuGroups.h.
	
	You can create new menu groups, to which your plug�in or other plug�ins
	can add menu items. Documentation of such menu groups is the responsibility
	of the developer of the plug-in adding the menu group.
	
	Note: Because the #kFilterUtilities menu is implemented using an
	internal plug-in, to use the menu group a plug-in must acquire the
	Filter Suite.

	<b>Menu Options</b>

	There are two sets of options for menus. The #AIMenuItemOption defines options
	that apply to menu items and the #AIMenuGroupOption defines options that apply
	to menu groups. The menu item options can be set at the time of the AddMenuItem()
	or with SetMenuItemOptions() after the menu item is installed. Similarly the
	options for menu groups can be set with AddMenuGroup() or SetMenuGroupOptions().

	<b>AIMenuHandles and PlatformMenus</b>

	When a plug-in menu is added to Adobe Illustrator, the plug-in is returned
	an AIMenuItemHandle reference. This is Illustrator�s means of keeping track
	of menus. It is an opaque reference that cannot be used to directly make
	changes to the menu item itself.

	The UpdateMenuItemAutomatically() function tells Illustrator to automatically
	make changes to a menu item�s appearance.

	If your plug-in needs to manipulate the menu item directly, the menu suite
	provides the GetPlatformMenuItem() function to obtain a platform specific
	reference to the menu in an AIPlatformMenuItem structure.

	<b>Menu Notifiers</b>
	
	The following notifiers relating to menu events are sent:

	- #kAIMenuChangedNotifier
	- #kAIMenuBeforePluginNotifier
	- #kAIMenuAfterPluginNotifier
	- #kAIMenuBeforeUnknownMenuNotifier
	- #kAIMenuAfterUnknownMenuNotifier

*/
typedef struct {

	/** Use this to install a plug-in menu item.
	
		- self is a reference to the plug-in adding the menu item.
		- inKeyboardShortDictionaryKey is a C string identifying the item in the keyboardshortcut dictionary.
			If you don't want an entry in the dictionary (i.e. your item is dynamic, like a filename or a user setting)
			pass in NULL.
		- data contains the menu group to which the plug-in menu will be added and a Unicode
			string to be used for the menu. Note: to create a menu separator pass in
			kMenuItemIsSeparator in the options flag.
		- options flags are used control the menu item's behavior see #AIMenuItemOption.
		- menuItem identifies the installed plug-in menu. If you are installing multiple menu
			items, this reference must be stored in the plug-in's globals record, as your
			plug-in will need it to determine which menu command is to be processed.
	*/
	AIAPI AIErr (*AddMenuItem) ( SPPluginRef self, const char*inKeyboardShortDictionaryKey,
				AIPlatformAddMenuItemDataUS *data, AIMenuItemOption options,
				AIMenuItemHandle *menuItem );

	/** Same as above, except with a ZString parameter instead of a UnicodeString */
	AIAPI AIErr (*AddMenuItemZString) ( SPPluginRef self, const char*inKeyboardShortDictionaryKey,
				const char *groupName, ZRef itemText, AIMenuItemOption options,
				AIMenuItemHandle *menuItem );
				
	/** Returns name, a pointer to the name of the menuItem. This is the name value
		originally passed to the AddMenuItem call. It should not be modified. */
	AIAPI AIErr (*GetMenuItemKeyboardShortcutDictionaryKey) ( AIMenuItemHandle menuItem, const char**outKey );
	/** Use this to get the current options of a menu item. See #AIMenuItemOption for the
		options. */
	AIAPI AIErr (*GetMenuItemOptions) ( AIMenuItemHandle menuItem, AIMenuItemOption *options );
	/** Use this to set the current options of a menu item. See #AIMenuItemOption for the
		options. */
	AIAPI AIErr (*SetMenuItemOptions) ( AIMenuItemHandle menuItem, AIMenuItemOption options );
	/** Returns the plugin that installed the menu item. */
	AIAPI AIErr (*GetMenuItemPlugin) ( AIMenuItemHandle menuItem,
				SPPluginRef *plugin );

	/** Get the number of installed menu items. */
	AIAPI AIErr (*CountMenuItems) ( long *count );
	/** Get the nth installed menu item. */
	AIAPI AIErr (*GetNthMenuItem) ( long n, AIMenuItemHandle *menuItem );

	/** Use this to get the platform menu item reference associated with menuItem. */
	AIAPI AIErr (*GetPlatformMenuItem) ( AIMenuItemHandle menuItem,
				AIPlatformMenuItem *platformMenuItem );

	/** Use this function to tell Illustrator to update a menu item automatically
		according to the criteria passed to the function. Each criteria is evaluated
		and the results combined to produce a value. This value then determines the
		behaviour of the action. The actions are defined by #AIAutoMenuUpdateAction,
		the object based criteria are defined by #AIAutoMenuUpdateObjectCriteria and
		the document properties based criteria are defined by #AIAutoMenuUpdatePropertyCriteria.

		- The menuItem is the item to be updated.
		- The action variable contains a flag that indicates how the menu is to be
			modified.

		The "positive" criteria are true if any of the properties they specify are
		present. The results of these are then or'ed together to produce a positive
		result. This result is then true if any of the criteria are true.

		- ifObjectIsInArtwork is true if any of the object types identified are
			present in the document.
		- ifObjectIsSelected is true if any of the object types identified are
			present in the current selection.
		- ifIsTrue is true if any of the document properties identified are true.

		The "negative" criteria are true if none of the properties they specify are
		present. The results of these are then and'ed together to produce a negative
		result. This result is then true if none of the criteria are true.

		- ifObjectIsNotInArtwork is true if none of the object types identified
			are present in the document.
		- ifObjectIsNotSelected is true if none of the object types identified are
			present in the current selection.
		- ifIsNotTrueis true if none of the document properties identified are
			true.

		Finally the combined result used to perform the update action is true if
		both the positive and negative results are true. That is, if any of the
		desired properties are present and none of the undesired properties are
		present.
	*/
	AIAPI AIErr (*UpdateMenuItemAutomatically) ( AIMenuItemHandle menuItem,
				long action,
				long ifObjectIsInArtwork, long ifObjectIsNotInArtwork,
				long ifObjectIsSelected, long ifObjectIsNotSelected,
				long ifIsTrue, long ifIsNotTrue );
	/** Use this function to get the artwork and document state information that
		the Illustrator application uses to do it's menu updating. This is quicker than
		scanning the artwork tree or getting specific art, as Illustrator has already
		computed the information for its own use.

		You can use this as a preliminary check to update a menu. The inArtwork
		argument returns flags about the art objects in the current document. The
		isSelected argument returns flags about the selected art objects. The
		isTrue argument returns information about the document state. See
		#AIAutoMenuUpdateObjectCriteria for values for inArtwork and isSelected.
		See #AIAutoMenuUpdatePropertyCriteria for values for isTrue.
		
		This call can only be used when the #kSelectorAIUpdateMenuItem selector
		is received. The information will be invalid at any other time. The
		#kMenuItemWantsUpdateOption option must be set for the plug-in to receive this
		selector.
	*/
	AIAPI AIErr (*GetUpdateFlags)( long *inArtwork, long *isSelected, long *isTrue );

	/*-------------------------------------------------------------------------------------------*/
	
	/** Enable menu item */
	AIAPI AIErr (*EnableItem) ( AIMenuItemHandle menuItem);
	/** Disable menu item */
	AIAPI AIErr (*DisableItem) ( AIMenuItemHandle menuItem);

	/** This call gets the text of a menu item. 

		You can use this after using SetItemText or AddMenuItem. Note: the text returned by
		GetItemText is different than what is returned by GetMenuItemKeyboardShortcutDictionaryKey. */
	AIAPI AIErr (*GetItemText) ( AIMenuItemHandle menuItem, ai::UnicodeString &itemString );
	
	/** Set the text of a menu item. */
	AIAPI AIErr (*SetItemText) ( AIMenuItemHandle menuItem, const ai::UnicodeString &itemString );
	
	/** Set the text of a menu item. */
	AIAPI AIErr (*SetItemTextZString) ( AIMenuItemHandle menuItem, ZRef itemString );

	/** Check or uncheck the menu item. */
	AIAPI AIErr (*CheckItem) ( AIMenuItemHandle menuItem, AIBoolean bCheckIt );

	/** Get the menu group to which the menu item belongs. */
	AIAPI AIErr (*GetItemMenuGroup) ( AIMenuItemHandle menuItem, AIMenuGroup *group );
	/** Creates a new menu group, which you can use to add your plug-in menu.
		
		- name is the new menu group name.
		- options indicate the behavior of the menu group see #AIMenuGroupOption.
		- The new menu group will appear beneath the existing menu group specified by
			nearGroup which may be any available menu group including those created
			by other plugins.
		- A reference to the new group is returned in the group argument.

		A plug-in installing several menu items with a unique function may wish to
		add a menu group. The new menu group can be documented and used by
		other plug-ins installing menu items with similar functions. The menu group
		name can be passed to the AddMenuItem() function just as any of the built in
		menu group names can.
	*/
	AIAPI AIErr (*AddMenuGroup) ( const char* name, AIMenuGroupOption options, const char* nearGroup,
				AIMenuGroup *group );
	/** Illustrator menus use a Menu Group to handle hierarchical menus. Use this
		function to create a hierarchical "submenu" name. It is possible to have
		nested submenus.

		- The options argument indicate the behavior of the menu group see
			#AIMenuGroupOption.
		- The new menu group will appear as a hierarchical menu at the menu
			item indicated by menuItem.
		- An AIMenuGroup reference for the new group is returned in the
			variable group.
		
		To create a hierarchical menu, a plug-in first creates a normal menu item.
		It then calls AddMenuGroupAsSubMenu with the AIMenuItemHandle of this
		menu item and a group name.
	*/
	AIAPI AIErr (*AddMenuGroupAsSubMenu) ( const char* name, AIMenuGroupOption options, AIMenuItemHandle menuItem, AIMenuGroup *group);
	/** Returns a pointer to the name of the group. This is the name value originally
		passed to the AddMenuGroup call. It should not be modified. */
	AIAPI AIErr (*GetMenuGroupName) ( AIMenuGroup group,  const char**name);
	/** Get option flags of a menu group. */
	AIAPI AIErr (*GetMenuGroupOptions) ( AIMenuGroup group, AIMenuGroupOption *options );
	/** Set option flags of a menu group. */
	AIAPI AIErr (*SetMenuGroupOptions) ( AIMenuGroup group, AIMenuGroupOption options );
	/** On the Mac, this call will return a MenuInfo struct for the AIPlatformMenuHandle. On
		Windows, it will return a WinMenu struct for the AIPlatformMenuHandle. It also
		returns the first and last menu indices of the hierarchical menu.*/
	AIAPI AIErr (*GetMenuGroupRange) ( AIMenuGroup group, AIPlatformMenuHandle *theMenu,
				short *firstItem, short *numItems );
	/** Return the number of installed menu groups. */
	AIAPI AIErr (*CountMenuGroups) ( long *count );
	/** Get the nth installed menu group. */
	AIAPI AIErr (*GetNthMenuGroup) ( long n, AIMenuGroup *group );

	/** Returns the command key and modifiers assigned to a menu item. See #AIMenuItemModifier
		for modifiers. Note: Submenus created by plug-ins do not support modified command keys. */
	AIAPI AIErr (*GetItemCmd) ( AIMenuItemHandle menuItem, short *cmdChar, short *modifiers );
	/** Sets the command key and modifiers assigned to a menu item.  See #AIMenuItemModifier
		for modifiers. Note: Submenus created by plug-ins do not support modified command keys. */
	AIAPI AIErr (*SetItemCmd) ( AIMenuItemHandle menuItem, char cmdChar, short modifiers );

	/** This call returns the function key fkey and modifiers modifiers associated
		with the specified menu item. See #AIMenuItemModifier for modifiers. */
	AIAPI AIErr (*GetItemFunctionKey) ( AIMenuItemHandle menuItem, short *fkey, short *modifiers );
	/** This call associates the function key fkey and modifiers modifiers to the specified
		menu item. This function supports fkeys 1-15 and is 1-based. See #AIMenuItemModifier
		for modifiers. */
	AIAPI AIErr (*SetItemFunctionKey) ( AIMenuItemHandle menuItem, short fkey, short modifiers );
	
	/** True if the menu item is enabled */
	AIAPI AIErr (*IsItemEnabled) ( AIMenuItemHandle menuItem, ASBoolean *bEnabled);
	/** True if the menu item is checked */
	AIAPI AIErr (*IsItemChecked) ( AIMenuItemHandle menuItem, AIBoolean *bCheckIt );
	
	AIAPI AIErr (*RemoveMenuItem) ( AIMenuItemHandle menuItem );
	
	/** Sets the menu group header for a menu group. All items in the group will be drawn indented.
		To clear the group header, set it to an empty string */
	AIAPI AIErr (*SetMenuGroupHeader) (AIMenuGroup inGroup, const ai::UnicodeString &inHeader);
	
} AIMenuSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
