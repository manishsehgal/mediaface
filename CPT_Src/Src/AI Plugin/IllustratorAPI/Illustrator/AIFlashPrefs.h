/*
 *        Name:	AIFlashPrefs.h
 *
 * Copyright 1999 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

//-----------------------------------------------------------------------------

#ifndef	AIFLASHPREFS_H
#define	AIFLASHPREFS_H

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

typedef enum _FlashExportOption
{
	exportAIFileToSWFFile = 1,
	exportAILayersToSWFFrames,
	exportAILayersToSWFFiles
	
} FlashExportOption;

typedef enum _FlashOutputSize
{
	outputArtBounds = 1,
	outputPageBounds
} FlashOutputSize;

typedef enum _FlashImageFormat 
{ 
	formatZLIB = 1,
	formatJPEG_			// used to be formatJPEG but that was already defined in SVGExportEnums.h
} FlashImageFormat;

typedef enum _FlashJPEGQuality
{
	qualityLow = 1,
	qualityMedium,
	qualityHigh,
	qualityMaximum
} FlashJPEGQuality;

typedef enum _FlashJPEGMethod
{
	methodBaseline = 1,
	methodOptimized
} FlashJPEGMethod;

typedef enum _FlashOverwriteOption
{
	kAskBeforeReplacingFile = 1, 
	kAlwaysReplaceExistingFile,
	kNeverReplaceExistingFile 
} FlashOverwriteOption;

typedef enum _FlashAnimateBlendOption {
	kAnimateBlendInSequence = 1,
	kAnimateBlendInBuild
} FlashAnimateBlendOption ;

typedef struct FlashOptions
{
	FlashExportOption		m_exportOption;
	float					m_frameRate;
	AIBoolean				m_looping;
	AIBoolean				m_readOnly;
	AIBoolean				m_generateHTML;
	FlashOutputSize			m_outputSize;
	int						m_curveQuality;

	FlashImageFormat		m_imageFormat;
	FlashJPEGQuality		m_jpegQuality;
	FlashJPEGMethod			m_jpegMethod;
	float					m_imageResolution;

	FlashOverwriteOption	m_overwriteOption;

	AIBoolean				m_flattenArtwork;

	//gopinath -begin
	AIBoolean				m_useBackground; 
	int						*m_bgLayerIndex; //what else can we have here?
	AIBoolean				m_animateBlends; 
	FlashAnimateBlendOption m_animateBlendOption;
	AIBoolean				m_orderLayersTopDown; 
	AIRGBColor				m_backgoundColor; 
	AIBoolean				m_exportTextAsOutlines; 
	AIBoolean				m_compressFile; 
	//gopinath -end
} FlashOptions;

//-----------------------------------------------------------------------------

#define	kFlashPrefPrefix					"FlashFormat"

#define	kFlashPrefExportOption				"FlashExportOption"
#define	kFlashPrefDefaultExportOption		exportAIFileToSWFFile

#define	kFlashPrefFrameRate					"FlashFrameRate"
#define	kFlashPrefDefaultFrameRate			12.0

#define	kFlashPrefLooping					"FlashLooping"
#define	kFlashPrefDefaultLooping			false

#define	kFlashPrefReadOnly					"FlashReadOnly"
#define	kFlashPrefDefaultReadOnly			false

#define	kFlashPrefExportHTML				"FlashExportHTML"
#define	kFlashPrefDefaultExportHTML			true

#define	kFlashPrefOutputSize				"FlashOutputSize"
#define	kFlashPrefDefaultOutputSize			outputArtBounds

#define	kFlashPrefCurveQuality				"FlashCurveQuality"
#define	kFlashPrefDefaultCurveQuality		7

#define	kFlashPrefImageFormat				"FlashImageFormat"
#define	kFlashPrefDefaultImageFormat		formatZLIB

#define kFlashPrefJPEGQuality				"FlashJPEGQuality"
#define	kFlashPrefDefaultJPEGQuality		(FlashJPEGQuality)3

#define	kFlashPrefJPEGMethod				"FlashJPEGMethod"
#define	kFlashPrefDefaultJPEGMethod			methodBaseline

#define	kFlashPrefImageResolution			"FlashImageResolution"
#define	kFlashPrefDefaultImageResolution	72.0

#define	kFlashPrefOverwriteOption			"FlashOverwriteOption"
#define	kFlashPrefDefaultOverwriteOption	kAskBeforeReplacingFile

#define	kFlashPrefPreserveAppearance		"FlashPreserveAppearance"
#define	kFlashPrefDefaultPreserveAppearance	false

//gopinath -begin
#define kFlashPrefUseBackground				"FlashUseBackground"
#define kFlashPrefDefaultUseBackground		false

#define kFlashPrefAnimateBlend				"FlashAnimateBlends"
#define kFlashPrefDefaultAnimateBlend		false

#define kFlashPrefAnimateBlendOption		"FlashAnimateBlendOption"
#define kFlashPrefDefaultAnimateBlendOption kAnimateBlendInSequence

#define kFlashPrefOrderLayersOption			"FlashOrderLayersTopDown"
#define kFlashPrefDefaultOrderLayersOption	false

#define kFlashPrefTextAsOutlines			"FlashTextAsOutlines"
#define kFlashPrefDefaultTextAsOutlines		false

#define	kFlashPrefBackgroundColorRed		"FlashBackgroundColorRed"
#define kFlashPrefDefaultBackGroundRed		0xFFFF

#define	kFlashPrefBackgroundColorGreen		"FlashBackgroundColorGreen"
#define kFlashPrefDefaultBackGroundGreen	0xFFFF

#define	kFlashPrefBackgroundColorBlue		"FlashBackgroundColorBlue"
#define kFlashPrefDefaultBackGroundBlue		0xFFFF

#define kFlashPrefCompressFile				"FlashCompressFile"
#define kFlashPrefDefaultCompressFile		false
//gopinath -end

#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif // __cplusplus

#endif