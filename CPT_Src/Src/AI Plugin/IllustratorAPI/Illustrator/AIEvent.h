#ifndef __AIEvent__
#define __AIEvent__

/*
 *        Name:	AIEvent.h
 *   $Revision: 5 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Event Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIEvent.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

/** Modifiers for an event. See AIEventModifersValue. */
typedef ASUInt16 AIEventModifiers;

/** NOTE: the value of the modifier key is the same as the Mac's Events.h */
enum AIEventModifersValue {

/* modifiers */

	aiEventModifiers_activeFlag			= 0x0001,								/* Bit 0 of modifiers for activateEvt and mouseDown events */
	aiEventModifiers_btnState			= 0x0080,								/* Bit 7 of low byte is mouse button state */
	aiEventModifiers_cmdKey				= 0x0100,	/* Ctrl key on MSWindows */ /* Bit 0 of high byte */
	aiEventModifiers_shiftKey			= 0x0200,								/* Bit 1 of high byte */
	aiEventModifiers_alphaLock			= 0x0400,								/* Bit 2 of high byte */
	aiEventModifiers_optionKey			= 0x0800,	/* Alt key on MSWIndows	*/	/* Bit 3 of high byte */
	aiEventModifiers_controlKey			= 0x1000,								/* Bit 4 of high byte */
	aiEventModifiers_rightShiftKey		= 0x2000,								/* Bit 5 of high byte */
	aiEventModifiers_rightOptionKey		= 0x4000,								/* Bit 6 of high byte */
	aiEventModifiers_rightControlKey	= 0x8000,								/* Bit 7 of high byte */
	aiEventModifiers_activeFlagBit		= 0,									/* activate? (activateEvt and mouseDown) */
	aiEventModifiers_btnStateBit		= 7,									/* state of button? */
	aiEventModifiers_cmdKeyBit			= 8,									/* command key down? */
	aiEventModifiers_shiftKeyBit		= 9,									/* shift key down? */
	aiEventModifiers_alphaLockBit		= 10,									/* alpha lock down? */
	aiEventModifiers_optionKeyBit		= 11,									/* option key down? */
	aiEventModifiers_controlKeyBit		= 12,									/* control key down? */
	aiEventModifiers_rightShiftKeyBit	= 13,									/* right shift key down? */
	aiEventModifiers_rightOptionKeyBit	= 14,									/* right Option key down? */
	aiEventModifiers_rightControlKeyBit	= 15,									/* right Control key down? */

/* these modifiers are matching ADM tracker modifiers */

	aiEventModifiers_spaceKey			= 0x0002,								/* Bit 1 of low byte */
	aiEventModifiers_tabKey				= 0x0004,								/* Bit 2 of low byte */
	aiEventModifiers_capsLockOn			= 0x0008,								/* Bit 3 of low byte */
	aiEventModifiers_doubleClick		= 0x0010,								/* Bit 4 of low byte */
	aiEventModifiers_tripleClick		= 0x0020,								/* Bit 5 of low byte */
	aiEventModifiers_spaceKeyBit		= 1,									/* space key down ? */
	aiEventModifiers_tabKeyBit			= 2,									/* tab key down ? */
	aiEventModifiers_capsLcokBit		= 3,									/* caps lock on ? */
	aiEventModifiers_doubleClickBit		= 4,									/* double click ? */
	aiEventModifiers_tripleClickBit		= 5										/* triple click ? */
};









#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
