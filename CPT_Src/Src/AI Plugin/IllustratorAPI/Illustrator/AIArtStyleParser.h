#ifndef __AIArtStyleParser__
#define __AIArtStyleParser__

/*
 *        Name:	AIArtStyleParser.h
 *     Purpose:	Adobe Illustrator Art Style Parser Suite.
 *
 * Copyright (c) 1986-2003 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AIArtStyle__
#include "AIArtStyle.h"
#endif

#ifndef __AILiveEffect__
#include "AILiveEffect.h"
#endif

#ifndef __AIPathStyle__
#include "AIPathStyle.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIArtStyleParser.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIArtStyleParserSuite				"AI Art Style Parser Suite"
#define kAIArtStyleParserSuiteVersion3		AIAPI_VERSION(3)
#define kAIArtStyleParserSuiteVersion		kAIArtStyleParserSuiteVersion3
#define kAIArtStyleParserVersion			kAIArtStyleParserSuiteVersion


/*******************************************************************************
 **
 **	Types
 **
 **/

// Opaque references to data structures inside app.
typedef struct _t_AIStyleParser* AIStyleParser;
typedef struct _t_AIParserLiveEffect* AIParserLiveEffect;
typedef struct _t_AIParserPaintField* AIParserPaintField;
typedef struct _t_AIParserBlendField* AIParserBlendField;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	The AIArtStyleParser suite is useful for manipulating art styles. It provides an API over the
	information which the appearance palette would typically show. 
	
	The parser itself constructs its own data structures when asked to parse an art style. These 
	data structures are tied to the art style it parsed unless the data structures in the parser
	are actually modified. When these data structures are modifed, clients shouldn't make
	assumptions about how the structures in the parser relate to an	actual art style and should
	avoid making calls to SetFocus and EditEffectParameters, which both	assume no modifications 
	were made to the data structures.

	To get a style built from the existing data structures in the parser, clients need to call 
	CreateNewStyle. The typical (or envisioned) workflow is this:
 
@code
	NewParser(&parser)
	ParseStyle(parser, artStyle)
	...
	<Muck with the data structures - reordering, adding, removing, redefining properties, etc.>
	...
	CreateNewStyle(parser, &newArtStyle)
	DisposeParser(parser)
@endcode

	"Pre-Effects" correspond to live effects that show up before any fills and strokes in the
	appearance palette.
	
	"Post-Effects" correspond to live effects that show up after all the fills and strokes in the
	appearance palette.
	 
	A "Blend Field" corresponds to transparency information. Every art style parsed has a "Blend
	Field" which dictates the overall transparency of the art style. This is listed last  in the
	appearance palette.
	
	A "Paint Field" corresponds to either a fill or a stroke in the art style. Each "Paint Field"
	can also contain live effects and a "Blend Field".
*/
typedef struct {

	/** Construct a new parser. */
	AIAPI AIErr (*NewParser) ( AIStyleParser* parser );
	/** Dispose of a parser. */
	AIAPI AIErr (*DisposeParser) ( AIStyleParser parser );

	/** Parse the style contents. */
	AIAPI AIErr (*ParseStyle) ( AIStyleParser parser, AIArtStyleHandle artStyle );
	/** Merge the properties of the given art style into the given parser. Analogous to
		the "Merge Graphic Styles" option in the graphic styles palette flyout menu. */
	AIAPI AIErr (*MergeStyleIntoParser) ( AIStyleParser parser, AIArtStyleHandle artStyle );
	/** Calls to IsStyleParseable make sense only immediately after ParseStyle. Returns
		true if the given parser was able to successfully parse an art style. */
	AIAPI AIBoolean (*IsStyleParseable) ( AIStyleParser parser );
	
	/** Count the number of "Pre-Effects" inside the parser. "Pre-Effects" are live 
		effects that show up before any fills and strokes. */
	AIAPI ASInt32 (*CountPreEffects) ( AIStyleParser parser );
	/** Count the number of "Post-Effects" inside the parser. "Post-Effects" are live 
		effects that show up after all the fills and strokes. */
	AIAPI ASInt32 (*CountPostEffects) ( AIStyleParser parser );
	/** Count the number of "Paint Fields" inside the parser. A "Paint Field" corresponds
		to either a fill or a stroke in the art style. Each "Paint Field" can also
		contain live effects and transparency information. */
	AIAPI ASInt32 (*CountPaintFields) ( AIStyleParser parser );
	/** Count the number of live effects inside the given paint field. */
	AIAPI ASInt32 (*CountEffectsOfPaintField) ( AIParserPaintField paintField );
	
	/** Retrieve nth "Pre-Effect" from the parser. */
	AIAPI AIErr (*GetNthPreEffect) ( AIStyleParser parser, ASInt32 n, AIParserLiveEffect* effect );
	/** Retrieve nth "Post-Effect" from the parser */
	AIAPI AIErr (*GetNthPostEffect) ( AIStyleParser parser, ASInt32 n, AIParserLiveEffect* effect );
	/** Retrieve nth "Paint Field" from the parser */
	AIAPI AIErr (*GetNthPaintField) ( AIStyleParser parser, ASInt32 n, AIParserPaintField* paintField );
	/** Retrieve nth live effect from the given "Paint Field". */
	AIAPI AIErr (*GetNthEffectOfPaintField) ( AIParserPaintField paintField, ASInt32 n,
											  AIParserLiveEffect* effect );
	/** Retrieve the overall art style transparency from the parser in the form of a "Blend Field". */
	AIAPI AIErr (*GetStyleBlendField) ( AIStyleParser parser, AIParserBlendField* blendField );
	
	/** Insert a "Pre-Effect" into the parser at specified index. Use index -1 to insert element
		at the end. */
	AIAPI AIErr (*InsertNthPreEffect) ( AIStyleParser parser, ASInt32 n, AIParserLiveEffect effect );
	/** Insert a "Post-Effect" into the parser at specified index. Use index -1 to insert element
		at the end. */
	AIAPI AIErr (*InsertNthPostEffect) ( AIStyleParser parser, ASInt32 n, AIParserLiveEffect effect );
	/** Insert a "Paint Field" into the parser at specified index. Use index -1 to insert element
		at the end. */
	AIAPI AIErr (*InsertNthPaintField) ( AIStyleParser parser, ASInt32 n, AIParserPaintField paintField );
	/** Insert a live effect into a "Paint Field" in the parser at specified index. Use index -1
		to insert element at the end. */
	AIAPI AIErr (*InsertNthEffectOfPaintField) ( AIStyleParser parser, AIParserPaintField paintField, ASInt32 n,
												 AIParserLiveEffect effect );

	/** Remove given "Pre-Effect" from the parser. The doDelete boolean lets the parser know 
		whether it can dispose of the memory associated with the element. Plugins may not want
		to have the memory disposed if they are moving the elements from one position to another. */
	AIAPI AIErr (*RemovePreEffect) ( AIStyleParser parser, AIParserLiveEffect effect, AIBoolean doDelete );
	/** Remove given "Post-Effect" from the parser. The doDelete boolean lets the parser know 
		whether it can dispose of the memory associated with the element. Plugins may not want
		to have the memory disposed if they are moving the elements from one position to another. */
	AIAPI AIErr (*RemovePostEffect) ( AIStyleParser parser, AIParserLiveEffect effect, AIBoolean doDelete );
	/** Remove given "Paint Field" from the parser. The doDelete boolean lets the parser know 
		whether it can dispose of the memory associated with the element. Plugins may not want
		to have the memory disposed if they are moving the elements from one position to another. */
	AIAPI AIErr (*RemovePaintField) ( AIStyleParser parser, AIParserPaintField paintField, AIBoolean doDelete );
	/** Remove a live effect from a "Paint Field" in the parser. The doDelete boolean lets the parser know 
		whether it can dispose of the memory associated with the element. Plugins may not want
		to have the memory disposed if they are moving the elements from one position to another. */
	AIAPI AIErr (*RemoveEffectOfPaintField) ( AIStyleParser parser, AIParserPaintField paintField, 
											  AIParserLiveEffect effect, AIBoolean doDelete );
	
	/** Removes all the live effects in the parser. */
	AIAPI AIErr (*RemoveAllEffects) ( AIStyleParser parser );
	/** Removes all the effects and all the paint fields except for one fill and one stroke.
		The fill and stroke that are kept are the ones that would show in the fill and stroke
		proxies in the tool and color palettes. They are the ones that the user is currently 
		editing, and they are referred to as the focus fill and stroke in the API. 
		Simplify also sets all transparency back to normal 100% opaque and always places the
		focus stroke on top of the focus fill in the stacking order. */
	AIAPI AIErr (*Simplify) ( AIStyleParser parser );
	
	/** Retrieve the focus fill from the parser. If an art style has multiple fills, the user
		can only edit one at a time. The focus fill is the fill the user can currently edit, 
		and it is shown in the fill/stroke proxies on the tool and color palettes. */
	AIAPI AIErr (*GetFocusFill) ( AIStyleParser parser, AIParserPaintField* paintField );
	/** Retrieve the focus stroke from the parser. If an art style has multiple strokes, the user
		can only edit one at a time. The focus stroke is the stroke the user can currently edit, 
		and it is shown in the fill/stroke proxies on the tool and color palettes. */
	AIAPI AIErr (*GetFocusStroke) ( AIStyleParser parser, AIParserPaintField* paintField );
	/** Set the focus fill or focus stroke for this parser, depending on whether the given
		"Paint Field" corresponds to a fill or stroke. */
	AIAPI AIErr (*SetParserFocus) ( AIStyleParser parser, AIParserPaintField paintField );
	/** Analogous to SetParserFocus but rather than just modifying the parser's data structures,
		this API also modifies the art style from which the parser's data structures were constructed.
		SetFocus should be called when the data structures in the given parser have not been
		modified since the call to ParseStyle. The artStyle passed in should be the same one
		previously passed into ParseStyle. */
	AIAPI AIErr (*SetFocus) ( AIArtStyleHandle artStyle, AIStyleParser parser, AIParserPaintField paintField );

	/** Art styles have a concept of "Group Contents", which affects the generation of styled art
		when applied to art objects like groups or text. "Group Contents" controls where the original
		contents inside the group are rendered relative to the stacking order of the fills and strokes
		in the art styles. 
		
		This API returns the position in the stacking order (from top to bottom) where the group
		contents would be rendered. */
	AIAPI ASInt32 (*GetGroupContentsPosition) ( AIStyleParser parser );
	/** Move the group contents position in the parser. See GetGroupContentsPosition() for an
		explanation of "Group Contents". */
	AIAPI AIErr (*MoveGroupContentsPosition) ( AIStyleParser parser, ASInt32 position );
	
	/** CreateNewStyle constructs and returns a new art style handle based on the elements
		in the given parser. ParseStyle would still need to be called on the new style if
		a plugin wanted to call SetFocus or EditEffectParameters afterward. */
	AIAPI AIErr (*CreateNewStyle) ( AIStyleParser parser, AIArtStyleHandle* artStyle );
	
	/** A style is "invisible" if there are no effects, all the paint fields have
		a none color, and the overall transparency contains default values. */
	AIAPI AIBoolean (*IsStyleVisible) ( AIStyleParser parser );
	/** Returns true if the parser contains any non-empty fill or stroke values. */
	AIAPI AIBoolean (*ContainsPaint) ( AIStyleParser parser );
	/** Returns true if the parser contains any live effects. */
	AIAPI AIBoolean (*ContainsEffects) ( AIStyleParser parser );
	/** Returns true if the parser contains any non-default transparency information. */
	AIAPI AIBoolean (*ContainsTransparency) ( AIStyleParser parser );
	
	/** Retrieve the AILiveEffectHandle (see AILiveEffect.h) that corresponds to the given
		live effect data structure in the parser. */
	AIAPI AIErr (*GetLiveEffectHandle) ( AIParserLiveEffect effect, AILiveEffectHandle* liveEffectHandle );
	/** Retrieve the AILiveEffectParameters (see AILiveEffect.h) that corresponds to the given
		live effect data structure in the parser. */
	AIAPI AIErr (*GetLiveEffectParams) ( AIParserLiveEffect effect, AILiveEffectParameters* params );
	/** Set the AILiveEffectHandle (see AILiveEffect.h) for the given live effect data structure
		in the parser. */
	AIAPI AIErr (*SetLiveEffectHandle) ( AIParserLiveEffect effect, AILiveEffectHandle liveEffectHandle );
	/** Set the AILiveEffectParameters (see AILiveEffect.h) for the given live effect data structure
		in the parser. */
	AIAPI AIErr (*SetLiveEffectParams) ( AIParserLiveEffect effect, AILiveEffectParameters params );
	/** Copy the given live effect data structure into a newly created equivalent structure. Used by
		the appearance palette when users duplicate a live effect through that palette. */
	AIAPI AIErr (*CloneLiveEffect) ( AIParserLiveEffect effect, AIParserLiveEffect* clonedEffect );
	/** Brings up the given live effect's editing dialog, allowing the user to edit the parameters
		of the given effect in the given art style. Changes made by the user then result in a new
		art style that is applied to all the art objects currently targeted in the document.

		EditEffectParameters should only be called when the given live effect data structure was
		retrieved from a style parser whose data structues have not been modified since the call
		to ParseStyle. The artStyle	passed in to this API should be the same one previously passed
		into ParseStyle. 

		Because of the nature of its implementation, it is urged to use this API very sparingly. */
	AIAPI AIErr (*EditEffectParameters) ( AIArtStyleHandle artStyle, AIParserLiveEffect effect );
	
	/** Returns true if the given "Paint Field" corresponds to a fill. */
	AIAPI AIBoolean (*IsFill) ( AIParserPaintField paintField );
	/** Returns true if the given "Paint Field" corresponds to a stroke. */
	AIAPI AIBoolean (*IsStroke) ( AIParserPaintField paintField );
	/** Retrive the fill information, in the form of a #AIFillStyle, from the given "Paint Field".
		See #AIArtStylePaintData for more information on the paintData parameter. */
	AIAPI AIErr (*GetFill) ( AIParserPaintField paintField, AIFillStyle* fill, AIArtStylePaintData* paintData );
	/** Retrive the stroke information, in the form of a #AIStrokeStyle, from the given "Paint Field".
		The paintData parameter is currently ignored in this API. */
	AIAPI AIErr (*GetStroke) ( AIParserPaintField paintField, AIStrokeStyle* stroke, AIArtStylePaintData* paintData );
	/** Set the fill information, in the form of a #AIFillStyle, for the given "Paint Field".
		See #AIArtStylePaintData for more information on the paintData parameter. */
	AIAPI AIErr (*SetFill) ( AIParserPaintField paintField, AIFillStyle* fill, AIArtStylePaintData* paintData );
	/** Set the stroke information, in the form of a #AIStrokeStyle, for the given "Paint Field".
		The paintData parameter is currently ignored in this API. */
	AIAPI AIErr (*SetStroke) ( AIParserPaintField paintField, AIStrokeStyle* stroke, AIArtStylePaintData* paintData );
	/** Retrieve the transparency information for the given "Paint Field". See AIMask.h for the 
		keys that clients can use to get to the info inside the dictionary. */
	AIAPI AIErr (*GetPaintBlendDictionary) ( AIParserPaintField paintField, AIDictionaryRef blendDict );
	/** Set the transparency information for the given "Paint Field". See AIMask.h for the 
		keys that clients can use to set to the info inside the dictionary. */
	AIAPI AIErr (*SetPaintBlendDictionary) ( AIParserPaintField paintField, AIDictionaryRef blendDict );
	/** Inside a "Paint Field", there can exist live effects that affect only that "Paint Field" 
		and not the entire art style. The order of the effects matters, as effects are executed
		one after the other, and the output of the first effect then becomes the input of the
		second effect. Additionally, within this order, the "painting" (ie. filling or stroking)
		of the object also occurs. The color position refers to the spot in the Paint Field's 
		effect order that the painting occurs. This API retrieves tha color position. */
	AIAPI ASInt32 (*GetColorPosn) ( AIParserPaintField paintField );
	/** Set the color position of the given "Paint Field". See GetColorPosn() for a description of
		the color position. */
	AIAPI AIErr (*SetColorPosn) ( AIParserPaintField paintField, ASInt32 colorPosn );
	/** Copy the given "Paint Field" data structure into a newly created equivalent structure. Used by
		the appearance palette when users duplicate a fill or stroke through that palette. */
	AIAPI AIErr (*ClonePaintField) ( AIParserPaintField paintField, AIParserPaintField* clonedPaintField );
	/** Paint Fields can contain live effects which substitute for the built-in fill and stroke behaviors.
		The only current example of this that ships with Illustrator is brushes. Brushes are implemented
		as a live effect that replaces the default stroking behavior. This API retrieves both the
		AILiveEffectHandle and AILiveEffectParameters (see AILiveEffect.h) that substitute for the
		built-in fill or stroke behavior. */
	AIAPI AIErr (*GetPaintLiveEffectInfo) ( AIParserPaintField paintField, AILiveEffectHandle* liveEffectHandle,
											AILiveEffectParameters* params );
	/** Paint Fields can contain live effects which substitute for the built-in fill and stroke behaviors.
		The only current example of this that ships with Illustrator is brushes. Brushes are implemented
		as a live effect that replaces the default stroking behavior. With this API, clients can set
		both the AILiveEffectHandle and AILiveEffectParameters (see AILiveEffect.h) that will
		substitute for the built-in fill or stroke behavior. */
	AIAPI AIErr (*SetPaintLiveEffectInfo) ( AIParserPaintField paintField, AILiveEffectHandle liveEffectHandle,
											AILiveEffectParameters params );

	/** Every art style parsed has a "Blend	Field" which dictates the overall transparency of the
		art style. This transparency info is listed last in the appearance palette. This API retrieves
		the transparency information for that "Blend Field". See AIMask.h for the keys that clients 
		can use to get to the info inside the dictionary. */
	AIAPI AIErr (*GetBlendDictionary) ( AIParserBlendField blendField, AIDictionaryRef blendDict );
	/** Every art style parsed has a "Blend	Field" which dictates the overall transparency of the
		art style. This transparency info is listed last in the appearance palette. This API sets
		the transparency information for that "Blend Field". See AIMask.h for the keys that clients 
		can use to set to the info inside the dictionary. */
	AIAPI AIErr (*SetBlendDictionary) ( AIParserBlendField blendField, AIDictionaryRef blendDict );

	// NEW FOR AI10

	/** Retrieve the even-odd fill rule for the given "Paint Field". Returns an error if the given 
		"Paint Field" does not correspond to a fill. (See the evenodd field inside the #AIPathStyle 
		struct for a description of the even-odd fill rule.) */
	AIAPI AIErr (*GetEvenOdd) ( AIParserPaintField paintField, AIBoolean* evenodd );
	/** Set the even-odd fill rule for the given "Paint Field". Returns an error if the given "Paint
		Field" does not correspond to a fill. (See the evenodd field inside the #AIPathStyle struct 
		for a description of the even-odd fill rule.) */
	AIAPI AIErr (*SetEvenOdd) ( AIParserPaintField paintField, AIBoolean evenodd );

	/** Construct a new "Paint Field" with the given fill (represented as a AIFillStyle), even-odd 
		fill rule, and gradient vector information (represented in the paintData). The paintData
		parameter is allowed to be NULL. The resulting "Paint Field" is unassociated with any parser
		after construction. */
	AIAPI AIErr (*NewPaintFieldFill) ( const AIFillStyle* fill, AIBoolean evenOdd, const AIArtStylePaintData* paintData,
									   AIParserPaintField* paintField );
	/** Construct a new "Paint Field" with the given stroke (represented as a AIStrokeStyle). The
		paintData parameter is currently ignored. The resulting "Paint Field" is unassociated with
		any parser after construction. */
	AIAPI AIErr (*NewPaintFieldStroke) ( const AIStrokeStyle* stroke, const AIArtStylePaintData* paintData,
										 AIParserPaintField* paintField );
	/** Free the memory associated with the given "Paint Field". Be sure _not_ to use this API on
		"Paint Fields" that are retrieved from or inserted into a parser. Otherwise, the parser may
		attempt to reference freed memory. Disposing the parser that contains these structures will
		automatically dispose these structures as well. */
	AIAPI AIErr (*DisposePaintField) ( AIParserPaintField paintField );

	/** Construct a new live effect parser data structure with the given AILiveEffectHandle and
		AILiveEffectParameters (see AILiveEffect.h). The resulting data structure is unassociated
		with any parser after construction. */
	AIAPI AIErr (*NewParserLiveEffect) ( AILiveEffectHandle liveEffectHandle, AILiveEffectParameters params,
										 AIParserLiveEffect* effect );
	/** Free the memory associated with the given live effect parser data structure. Be sure _not_
		to use this API on structures that are retrieved from or inserted into a parser. Otherwise, 
		the parser may attempt to reference freed memory. Disposing the parser that contains these
		structures will	automatically dispose these structures as well. */
	AIAPI AIErr (*DisposeParserLiveEffect) ( AIParserLiveEffect effect );
	
	
	/** The following call is unrelated to parsing styles. This call merely returns the style that is currently
		selected in the Graphic Styles palette. It will return null if no style is selected and will return the
		first style selected if multiple styles are selected in the palette. */
	AIAPI AIErr (*GetCurrentArtStyle) ( AIArtStyleHandle* style );

} AIArtStyleParserSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
