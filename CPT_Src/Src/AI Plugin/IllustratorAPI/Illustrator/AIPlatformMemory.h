
#ifndef __AIPlatformMemory__
#define __AIPlatformMemory__

/*
 *        Name:	AIPlatformMemory.h
 *   $Revision: 1 $
 *      Author:	Dave Lazarony
 *        Date: 7/1/96
 *     Purpose:	Wrapper suite for calling the Mac or Windows platform 
 *				memory manager.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __ASTypes__
#include "ASTypes.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIPlatformMemory.h */

/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIPlatformMemorySuite				"AI Platform Memory Suite"
#define kAIPlatformMemorySuiteVersion		AIAPI_VERSION(1)
#define kAIPlatformMemoryVersion			kAIPlatformMemorySuiteVersion


/*******************************************************************************
 **
 **	Suite Record
 **
 **/

/** Plugins should not use these APIs unless absolutely necessary. They should
	use the AIBlockSuite or SPBlocksSuite to allocate memory.
 */
typedef struct {

	ASAPI ASErr (*New)(unsigned long size, ASHandle *handle);
	ASAPI ASErr (*Delete)(ASHandle handle);
	ASAPI ASErr (*GetSize)(ASHandle handle, unsigned long *size);
	ASAPI ASErr (*Resize)(ASHandle *handle, unsigned long newSize);
	ASAPI ASErr (*Lock)(ASHandle handle, void **dataPtr);
	ASAPI ASErr (*UnLock)(ASHandle handle);

} AIPlatformMemorySuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif