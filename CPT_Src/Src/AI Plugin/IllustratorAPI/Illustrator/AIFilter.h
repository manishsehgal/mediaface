#ifndef __AIFilter__
#define __AIFilter__

/*
 *        Name:	AIFilter.h
 *   $Revision: 6 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Filter Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIPlugin__
#include "AIPlugin.h"
#endif

#ifndef __AIMenu__
#include "AIMenu.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIFilter.h */

/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIFilterSuite		"AI Filter Suite"
#define kAIFilterSuiteVersion	AIAPI_VERSION(2)
#define kAIFilterVersion	kAIFilterSuiteVersion

/** @ingroup Callers
	This is the filter caller. see AIFilterSuite */
#define kCallerAIFilter					"AI Filter"

/** @ingroup Selectors
	
	Illustrator sends a kSelectorAIGetFilterParameters message when the
	user chooses a filter from the Filter menu. The filter should get any user
	parameters, which are returned to Illustrator in the message structure. The
	filter plug-in can ignore this selector if it does not solicit any user
	input. A filter might want to check the artwork state on receiving this
	selector to know whether it can proceed with the go selector. See
	AIFilterMessage for the message structure.
*/
#define kSelectorAIGetFilterParameters	"AI Get Parameters"
/** @ingroup Selectors
	The kSelectorAIGoFilter message tells your filter to execute. The filter should
	not query for parameters in the processing of this message. It is passed any
	parameters it in the message structure. If the user selects the filter from
	the menu the parametes will be the values from an immediately preceding
	#kSelectorAIGetFilterParameters. If the user chooses to apply the last filter
	the get parameters message is not sent. Instead the filter immediately
	receives a go message with the last parameters used. See AIFilterMessage
	for the message structure. */
#define kSelectorAIGoFilter				"AI Go"


/*******************************************************************************
 **
 ** Types
 **
 **/

// This is a reference to a filter. It is never dereferenced.
typedef struct _t_AIFilterOpaque *AIFilterHandle;


#if Macintosh || MSWindows
typedef void *PlatformFilterParameters;

/** The PlatformAddFilterData structure contains the category to which the plug-in
	filter will be added and a name to be used for the filter submenu. Both of
	these are Pascal strings. Windows plug-ins support menu access keys (also known
	as the underlined letter) in menu items. Precede an access key character with
	an ampersand character "&". Illustrator will automatically create the underline
	and add support for access key functionality. Make sure the access keys for
	your category or title do not conflict with other menu items.

	category and name may be any text; filters that are added to the same category,
	either by you or by another plug-in, are grouped together in the category�s
	submenu. Give some thought to which category your filter belongs. Pick a filter
	name that is an action, such as �Rotate� or �Unite�. Consider how the name reads
	when it�s placed by itself at the top of the Filter menu, out of its category.
	If your filter asks for parameters, be sure to include three periods (not an
	ellipsis) after the name.
*/
typedef struct {
	unsigned char *category;
	unsigned char *title;
} PlatformAddFilterData;
#endif

/** When your plug-in receives a filter message action, it can expect the
	message data to be this structure.

	The SPPluginData is the data which is received by all calls to a plug-in.
	The AIFilterHandle is a reference to the filter plug-in receiving the message.
	If the plug-in has more than one filter installed, it would determine which
	one was selected by comparing the filter handle in the message to those it
	has saved in it globals structure, for instance:

@code
	if ( message->filter == globals->twirlFilter ) {
	// do whatever twirlFilter does...
	} else if ( message->filter == globals->swirlFilter ) {
	// do whatever swirlFilter does...
	}
@endcode

	The parameters field is where the reference to the user specified control
	values for the filter are passed in and returned. It is a platform Handle, so
	space allocated for any parameters structure should be allocated with the
	Machine Dependent Suite call, AIMdMemorySuite::MdMemoryNewHandle().
 */
typedef struct {
	SPMessageData d;
	AIFilterHandle filter;
	PlatformFilterParameters parameters;
} AIFilterMessage;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** Plug-in filters allow a plug-in to add menu items and categories to the Illustrator
	filter menu.

	While Filters can be used for a variety of purposes, they generally present a
	modal dialog to the user and then process the document�s artwork in some
	fashion.

	Filter plug-ins differ from menu plug-ins several ways. While Filters are
	always found under that menu heading, Menu plug-ins can appear in any of
	the Illustrator menus. Filters have a number of features provided automatically,
	including automatic repeat and setting the undo menu item text.
	Menu plug-ins must set the undo menu item text, but can also set command
	keys and control their appearance. Both plug-in types receive a go selector,
	but Filters also receive a selector to get parameters. The user interface for a
	filter will generally be a modal dialog, while a menu plug-in may interact
	with other plug-in types, or trigger other events.

	Filter plug-ins receive two message actions:
	- #kSelectorAIGetFilterParameters
	- #kSelectorAIGoFilter

	While Illustrator will set up the undo and redo text for a successful Filter,
	the plug-in needs to remove its undo context if the user cancels it. Use the
	AIUndoSuite::UndoChanges() function for this. This will remove any changes to
	the artwork the plug-in made, but the plug-in will still need to do any other
	necessary clean up, such as disposing of allocated memory.
 */
typedef struct {

	/** Call this during startup to place the name of your filter in the Filter menu.
		The SPPluginHandle self is a reference to the plug-in adding the menu
		item. name is a C string identifying the added plug-in filter to Illustrator and
		other plug-ins. It is an internal identifier for the plug-in and does not need
		to be translated; it should be unique, however. The PlatformAddFilterData
		structure contains the category to which the plug-in filter will be added
		and a name to be used for the filter submenu. The options flags are used to
		control the menu item�s behavior. Currently there are no options for filters.
		Pass 0 by default.
		
		The plug�in manager returns an AIFilterHandle in filter. If you are
		installing multiple filters, this reference must be stored in the plug-in�s
		globals record, as your plug-in will need it to determine which filter
		command is to be processed.
	*/
	AIAPI AIErr (*AddFilter) ( SPPluginRef self, char *name,
				PlatformAddFilterData *data, long options, AIFilterHandle *filter );

	/** Returns a pointer to the name of the filter. This is the name value originally
		passed to the AddFilter call. It should not be modified. */
	AIAPI AIErr (*GetFilterName) ( AIFilterHandle filter, char **name );
	/** Use this to get the current options of a filter. Pass the AIFilterHandle from
		the filter message to get options for the current filter being called or use
		GetNthFilter to get a filter by index. */
	AIAPI AIErr (*GetFilterOptions) ( AIFilterHandle filter, long *options );
	/** Use this to set the current options of a filter. Pass the AIFilterHandle
		from the filter message to set options for the filter being called, or use
		GetNthFilter to set a filter by index. */
	AIAPI AIErr (*SetFilterOptions) ( AIFilterHandle filter, long options );
	/** A reference to the last used parameter block is kept for each filter. This
		function will return that stored reference. */
	AIAPI AIErr (*GetFilterParameters) ( AIFilterHandle filter, PlatformFilterParameters *parameters );
	/** The plug�in manager keeps a reference to the last used parameter block for
		each filter. This function is used to store that reference. */
	AIAPI AIErr (*SetFilterParameters) ( AIFilterHandle filter, PlatformFilterParameters parameters );
	/** GetFilterMenuItem returns an AIMenuItemHandle to the filter menu
		item. Your plug-in can then modify its menu appearance using the AIMenuSuite
		functions. */
	AIAPI AIErr (*GetFilterMenuItem) ( AIFilterHandle filter, AIMenuItemHandle *menuItem );
	/** This call will return a reference to the plug-in that installed the filter. The
		SPPluginRef can then be used with the SPPluginSuite functions. */
	AIAPI AIErr (*GetFilterPlugin) ( AIFilterHandle filter, SPPluginRef *plugin );

	/** Use CountFilters in conjunction with GetNthFilter to iterate through
		the installed filters. */
	AIAPI AIErr (*CountFilters) ( long *count );
	/** Use GetNthFilter to iterate through the installed filters. */
	AIAPI AIErr (*GetNthFilter) ( long n, AIFilterHandle *filter );

} AIFilterSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
