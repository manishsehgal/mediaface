#ifndef __AISFWUTILITIES__
#define __AISFWUTILITIES__

/*
 *        Name:	AISFWUtilities.h
 *     Purpose:	Adobe Illustrator 10.0 Save For Web Utilities Suite
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif
#ifndef __AIDataFilter__
#include "AIDataFilter.h"
#endif
#ifndef __AIFileFormat__
#include "AIFileFormat.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AISFWUtilities.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAISFWUtilitiesSuite			"AI SFW Utilities Suite"
#define kAISFWUtilitiesSuiteVersion	AIAPI_VERSION(1)

/*******************************************************************************
 **
 **	Suite
 **
 **/

/** Save for web utilities.
 */
typedef struct {

	AIAPI AIErr (*GetSaveSettingsDirectory)	(SPPlatformFileSpecification* outFileSpec);
	AIAPI AIErr (*ShowSliceOptions)	(ASInt32 inSliceID);

} AISFWUtilitiesSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif

