#ifndef __AIGrid__
#define __AIGrid__

/*
 *        Name:	AIGrid.h
 *   $Revision: 13 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator Grid Suite.
 *
 * Copyright (c) 1986-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/


#ifndef __AITypes__
#include "AITypes.h"
#endif
#ifndef __AIArt__
#include "AIArt.h"
#endif
#ifndef __AIDocumentView__
#include "AIDocumentView.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIGrid.h */

/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIGridSuite			"AI Grid Suite"
#define kAIGridSuiteVersion3	AIAPI_VERSION(3)
#define kAIGridSuiteVersion		kAIGridSuiteVersion3
#define kAIGridVersion			kAIGridSuiteVersion


/** These constants identify the style of the grid. */
enum {
	kAIGridStyleGraph = 0,
	kAIGridStyleDotted
};


/*******************************************************************************
 **
 **	Types
 **
 **/
 
/** Grid spacing information. */
typedef struct {
	/** Major increments specified in points */
	AIFloat				spacing;
	/** Minor step: number of steps (subdivisions) per major increment */
	long				ticks;
} AIGridSpacing;

/** Grid settings information used by AIGridSuite::SetGridSettings() and
	AIGridSuite::GetGridSettings().*/
typedef struct {
	/** TRUE := draw grid in back */
	AIBoolean			isinback;
	/** Grid drawing style: #kAIGridStyleGraph or #kAIGridStyleDotted */
	long				style;
	/** Grid spacing information. */
	AIGridSpacing		spacing;
	/** Color for heavy lines. */
	AIRGBColor			darkcolor;
	/** Color for light lines. */
	AIRGBColor			litecolor;
} AIGridSettings;

/** The AIGridSuite::EnumGridLines() API enumerates the grid lines that intersect
	a given rectangle to a callback function of this type.  The data parameter 
	passed to EnumGridLines by the client is passed back to the callback
	function */
typedef ASErr ASAPI (*AIGridLineProc)	(void* data,
										AIRealPoint p0,
										AIRealPoint p1);
/** The AIGridSuite::EnumPageLines() API enumerates the page tile lines that intersect
	a given rectangle to a callback function of this type.  The data parameter 
	passed to EnumGridLines by the client is passed back to the callback
	function */
typedef ASErr ASAPI (*AIPageLineProc)	(void* data,
										AIRealPoint p0,
										AIRealPoint p1);


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** The Grid suite may be used to set and get information about the grid settings
	for the current document. */
typedef struct {

	/** Set the grid settings for the current document. */
	AIAPI AIErr (*GetGridSettings)		(AIGridSettings* settings);
	/** Get the grid settings for the current document. */
	AIAPI AIErr (*SetGridSettings) 		(AIGridSettings* settings);

	/** Enumerate the grid lines that intersect the passed rectangle to the 
		callback function. The data parameter is passed through to the callback
		function. */
	AIAPI AIErr	(*EnumGridLines)		(AIDocumentViewHandle view,
										AIRealRect *rect,
										AIGridLineProc proc,
										void* data);
	/** Enumerate the page tile lines that intersect the passed rectangle to
		the callback function.  The data parameter is passed through to the callback
		function. */
	AIAPI AIErr	(*EnumPageLines)		(AIDocumentViewHandle view,
										AIRealRect *rect,
										AIPageLineProc proc,
										void* data);

	/** Returns an AIBoolean specifying whether the grid is visible in the 
		view passed as a parameter. */
	AIAPI AIBoolean (*IsVisible)		(AIDocumentViewHandle view);
	/** Returns an AIBoolean specifying if snapping is on in the view 
		passed as a parameter. */
	AIAPI AIBoolean (*IsSnapOn)			(AIDocumentViewHandle view);

} AIGridSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
