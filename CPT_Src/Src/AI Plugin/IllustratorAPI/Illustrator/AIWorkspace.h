#ifndef __AIWorkspace__
#define __AIWorkspace__

/*
 *        Name:	AIWorkspace.h
 *		$Id $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 12.0 Workspace Suite.
 *
 * Copyright (c) 2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/
#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __SPAccess__
#include "SPAccess.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIWorkspace.h */
/*******************************************************************************
 **
 **	Constants
 **
 **/

/** @ingroup Constants */
#define kAIWorkspaceSuite					"AI Workspace Suite"
#define kAIWorkspaceSuiteVersion			AIAPI_VERSION(1)
#define kAIWorkspaceVersion					kAIWorkspaceSuiteVersion

/** The workspace name must be shorter than 64 bytes including null terminator.
*/
#define kMaxWSStringLength			(64)


/** @ingroup Callers */
/**	Caller id for messages sent to plugins from the workspace manager. 
*/
#define kAIWorkspaceCaller					"Workspace Manager"

/** @ingroup Selectors */
/**	kAIWSWriteSelector/kAIWSRestoreSelector sent to plugins owning tab palettes when requesting 
	a plugin to save or restore the current dialog's specific configuration such as list view 
	option, show option, and etc. 	
	kAIWSLocation, kAIWSDockCode, kAIWSDockGroup, kAIWSSize and kAIWSVisiblity will be saved or
	restored in the Workspace plug-in for all tab palettes.

	kWSDefaultSelector is to reset to hard-coded default values defined in each plug-in.  
	When A tab palette plug-in receives kAIWSDefaultSelector, the plug-in will be responsible to 
	configure the palette layout to the initial default. 
	This include kAIWSLocation, kAIWSDockCode, kAIWSDockGroup, kAIWSSize, and kAIWSVisiblity
	in addition to a palette's specific configuration(i.e. show/hide option, list view).
*/
#define kAIWSWriteSelector					"Write Woskspace"
#define kAIWSRestoreSelector				"Restore Woskspace"
#define kAIWSDefaultSelector				"Default Woskspace"

/** @ingroup Errors */
/** kWorkspaceNameTooLongErr indicates the workspace name is longer than kMaxWSStringLength.*/
#define kWorkspaceNameTooLongErr			'WSNL'

/** When A tab palette plug-in receives kAIWSDefaultSelector, the plug-in will be responsible to 
	configure the palette layout to the default AIWorkspaceDefaultValue.
*/
enum AIWorkspaceDefaultValue {
	kAIWSDefaultPreset = 1,
	kAIWSAICSPreset
};

/*******************************************************************************
 **
 **	Types
 **
 **/
/** @ingroup Types */
typedef struct _AIWorkspace*			AIWorkspaceHandle; 	

/** The SPMessageData is that which is received by all messages sent to a plug-in.
	The AIWorkspaceHandle is a reference to the workspace receiving the message.
	When a plug-in owns multiple palettes, check the dialog name. */
typedef struct {	
	SPMessageData d;
	AIWorkspaceHandle workspace;
	const char *dialogName; 
	AIBoolean restore; /* This will be set to true when kAIWSDefaultSelector 
						message is sent to recover kAIWSRestoreSelector call. */
	AIWorkspaceDefaultValue flag;
} AIWorkspaceMessage;

/*******************************************************************************
 **
 **	Suites
 **
 **/

// -----------------------------------------------------------------------------
/** 
Workspace: A workspace is the layout of palettes on the screen. 
You can save current size, visibility, physical location, docking information, 
and any other palette's specific options (e.g., the list view option in the brushes 
palette) in a workspace.

Palettes: They are floating tab palettes, the Tool palette, and the Control palette. 

Please note that the persistence (open at start behavior) of libraries of Brush, Graphic 
Style, Swatch, and Symbol isn't controled by the workspace. */

typedef struct {

	/** The following APIs are to get and set values of the basic types.
		@param workspace is a reference to the workspace receiving the message. 
		@param prefix is the palette's name that sADMDialog->GetDialogName(fDialog) returns.
		@param suffix names a specific configuration item in the plug-in such as "ListView", "ShowOption". etc.
		@param value for the preference item. */
	AIAPI AIErr (*GetIntegerValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, int *value);
	AIAPI AIErr (*SetIntegerValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, int value);
	AIAPI AIErr (*GetRealValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, AIReal *value);
	AIAPI AIErr (*SetRealValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, AIReal value);
	AIAPI AIErr (*GetBooleanValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, AIBoolean *value);
	AIAPI AIErr (*SetBooleanValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, AIBoolean value);
	AIAPI AIErr (*GetStringValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, char *value, int length);
	AIAPI AIErr (*SetStringValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, const char *value);
	AIAPI AIErr (*GetPointValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, AIPoint *value);
	AIAPI AIErr (*SetPointValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, AIPoint value);
	AIAPI AIErr (*GetRealPointValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, AIRealPoint *value);
	AIAPI AIErr (*SetRealPointValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, AIRealPoint value);
	AIAPI AIErr (*GetRectValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, AIRect *value);
	AIAPI AIErr (*SetRectValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, AIRect value);
	AIAPI AIErr (*GetRealRectValue)(AIWorkspaceHandle collectioRef, const char *prefix, const char *suffix, AIRealRect *value);
	AIAPI AIErr (*SetRealRectValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, AIRealRect value);
	AIAPI AIErr (*GetRealMatrixValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, AIRealMatrix *value);
	AIAPI AIErr (*SetRealMatrixValue)(AIWorkspaceHandle workspace, const char *prefix, const char *suffix, AIRealMatrix value);

} AIWorkspaceSuite;



#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif // __AIWorkspace__
