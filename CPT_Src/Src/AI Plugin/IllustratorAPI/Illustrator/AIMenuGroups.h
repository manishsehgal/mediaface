#ifndef __AIMenuGroups__
#define __AIMenuGroups__

/*
 *        Name:	AIMenuGroups.h
 *   $Revision: 20 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Built-in Menu Groups.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

/** @file AIMenuGroups.h */


/**	@ingroup MenuGroups */
/** @{ */

#define kAboutMenuGroup 		"About"

#define kOpenMenuGroup 			"Open Document"
#define kRecentMenuGroup		"Recent Files"
#define kCloseMenuGroup 		"Close Document"
#define kSaveMenuGroup 			"Save Document"
#define kSaveForMenuGroup		"Save For"
#define kImportMenuGroup 		"Import"
#define kPlaceMenuGroup 		"Place Document"
#define kExportMenuGroup 		"Export Document"
#define kDocumentUtilsMenuGroup "Document Utilities"
#define kDocumentInterchangeMenuGroup "Document Interchange"
#define kPrintMenuGroup 		"Print"	
#define kSendMenuGroup 			"Send Document"	

#define kAppUtilsMenuGroup 		"Application Utilities"
#define kQuitMenuGroup 			"Quit"

#define kUndoMenuGroup 			"Undo"
#define kPasteUtilsMenuGroup 	"Paste Utilities"
#define kSelectMenuGroup 		"Select"			// Select menu- internal commands
#define kSelectMenuExternalGroup "SelectExternal"	// Select menu- external commands
#define kEditTextMenuGroup		"Edit Text"			// Find/Replace, Spell Check
#define kEditMenuGroup 			"Edit"
#define kPresetsMenuGroup		"Presets Group"		// PDF, Transparency, Print presets

#define kSameMenuGroup			"Same"
#define kSelectObjectMenuGroup	"SelectObject"

#define kPrefsMenuGroup 		"Preferences"
#define kEditUtilsMenuGroup 	"Edit Utilities"
//#define kPublishingMenuGroup 	"Publishing"
#define kClipboardMenuGroup 	"Clipboard"

#define kRepeatMenuGroup 		"Repeat"
#define kArrangeTransformMenuGroup "Arrange Transform"
#define kArrangeMoveMenuGroup 	"Arrange Move"
#define kArrangeGroupMenuGroup 	"Arrange Group"
#define kArrangeAttribsMenuGroup "Arrange Attributes"

#define kViewModeMenuGroup 		"View Mode"
#define kViewAdornmentsMenuGroup "View Adornments"
#define kViewMenuGroup 			"View"
#define kViewUtilsMenuGroup 	"View Utilities"
#define kViewExtMenuGroup 		"View Extension"

#define kObjectAttribsMenuGroup "Object Attributes"
#define kObjectUtilsMenuGroup 	"Object Utilities"
#define kObjectsMenuGroup 		"Objects"
#define kObjectPathsMenuGroup 		"Objects Paths"
#define kObjectPathsPopupMenuGroup 	"Objects Paths Popup"
#define kLockMenuGroup			"Lock"
#define kHideMenuGroup			"Hide"

#define kGuidesMenuGroup 		"Guides"
#define kMaskMenuGroup 			"Masks"
#define kCompoundPathsMenuGroup "Compound Paths"
#define kCropMarksMenuGroup 	"Crop Marks"
#define kGraphsMenuGroup 		"Graphs"
#define kBlocksMenuGroup 		"Blocks"
#define kWrapMenuGroup 			"Wrap"
#define kTextPathTypeGroup		"Text Path Type"
#define kTypeAttribsMenuGroup 	"Type Attributes"
#define kTypePalettesMenuGroup 	"Type Palettes"
#define kTypeLayoutMenuGroup 	"Type Layout"
#define kTypeTabsMenuGroup 		kTypePalettesMenuGroup
#define kTypeUtilsMenuGroup 	"Type Utilities"
#define kTypePluginsMenuGroup1	"Type Plugins1"
#define kTypePluginsMenuGroup2	"Type Plugins2"
#define kTypeAsianOptionsGroup	"Type Asian Options"

#define kTypeSizeUtilsMenuGroup "Type Size Utilities"
#define kTypeSizeMenuGroup 		"Type Size"
#define kTypeLeadingUtilsMenuGroup "Type Leading Utilities"
#define kTypeLeadingMenuGroup 	"Type Leading"
#define kTypeAlignmentMenuGroup "Type Alignment"

#define kFilterUtilities	"Filter Utilities"

#define kEffectsMenuGroup	"Effects"

#define kHelpMenuGroup 			"Help Menu"

// The following groups do not show up in the menu bar. They only
// show up in the keyboard shortcuts dialog.
#define kHidddenOtherSelectMenuGroup	"Hidden Other Select"
#define kHiddenOtherTextMenuGroup		"Hidden Other Text"
#define kHiddenOtherObjectMenuGroup		"Hidden Other Object"
#define kHiddenOtherPaletteMenuGroup	"Hidden Other Palette"
#define kHiddenOtherMiscMenuGroup		"Hidden Other Misc"


#define kWindowUtilsMenuGroup 			"Window Utilities"
#define kToolPalettesMenuGroup			"Tool Palettes"
#define kWindowLibariesMenuGroup		"Window Libraries"


#define kPaintPalettesMenuGroup 			kToolPalettesMenuGroup 
#define kObjectInfoPalettesMenuGroup		kToolPalettesMenuGroup 
#define kAttributePalettesMenuGroup 		kToolPalettesMenuGroup 
#define kOtherPalettesMenuGroup 			kToolPalettesMenuGroup 
#define kPaintAttributesPaletteMenuGroup	kToolPalettesMenuGroup 
#define kSVGPaletteMenuGroup				kToolPalettesMenuGroup 
#define kWindowObjectUtilsMenuGroup			kToolPalettesMenuGroup
// Note the pre-AI10 definitions of these menu groups as shown below are mapped to kToolPalettesMenuGroup
//#define kPaintPalettesMenuGroup 			 "Paint Palettes"
//#define kObjectInfoPalettesMenuGroup		 "Object Info Palettes"
//#define kAttributePalettesMenuGroup 		 "Attribute Palettes"
//#define kOtherPalettesMenuGroup 			 "Palettes"
//#define kPaintAttributesPaletteMenuGroup	 "Paint Attributes Palettes"
//#define kSVGPaletteMenuGroup				 "SVG Palette"
//#define kWindowObjectUtilsMenuGroup		 "Window Object Utilities Palettes"


/////////////////////////////////////////////////
//
// The menus below are added by plug-ins, not by
// the application.  If you intend to use one, your
// plug-in should add it (adding a menu group twice
// is ok; the second call simply returns the existing
// group reference).
//
// When adding a group, be sure to use the option 
// indicated in below the group
//
/////////////////////////////////////////////////

#define kDocInfoMenuGroup				"AIPlugin Document Info"
//#define kDocInfoMenuGroupNearGroup
//#define kDocInfoMenuGroupOptions		kMenuGroupSeparatorOption

#define kObjectRasterMenuGroup			"AIPlugin Object Raster"
//#define kObjectFlattenMenuGroup			"AIPlugin Object Flatten"
//#define kDocInfoMenuGroupNearGroup
//#define kObjectRasterMenuGroupOptions	kMenuGroupSeparatorOption

#define kArrangeTransformMultipleMenuGroup "Arrange Multiple Transform"
#define kATMMenuGroupNearGroup 			kArrangeTransformMenuGroup
//#define kArrangeTransformMultipleMenuGroupOptions kMenuGroupSeparatorOption

#define kObjectPathsPopoutPluginMenuGroup	"More Menus in the Object Path Popout"
#define kOPPPMenuGroupNearGroup 			kObjectPathsPopupMenuGroup
//#define kOPPPMenuGroupOptions 			kMenuGroupSeparatorOption

#define kDocumentSupportMenuGroup		"AIPlugin Document Support"

#define kAssetMgmtMenuGroup		"Adobe Plugin Asset Mgmt"
#define kWorkgroupMenuGroup		kDocumentSupportMenuGroup

//Scripting Plugin
#define kScriptsMenuGroup 		"ScriptsMenuGroup"

// Workspaces
#define kWorkspacesMenuGroup		"WorkspacesMenuGroup"
#define kWorkspacesCustomMenuGroup	"WorkspacesCustomMenuGroup"

// end of the MenuGroups documentation group
/** @} */


#endif

