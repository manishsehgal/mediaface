#ifndef __AIClipboard__
#define __AIClipboard__

/*
 *        Name:	AIClipboard.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 9.0 Clipboard Suite.
 *
 * Copyright (c) 1986-1999 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIDragDropTypes__
#include "AIDragDropTypes.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIClipboard.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIClipboardSuite			"AI Clipboard Suite"
#define kAIClipboardSuiteVersion1		AIAPI_VERSION(1)	// In AI 9.0
// latest version
#define kAIClipboardSuiteVersion		kAIClipboardSuiteVersion1
#define kAIClipboardVersion			kAIClipboardSuiteVersion

/** @ingroup Callers
	This is the Clipboard caller. */
#define kCallerAIClipboard 				"AI Clipboard"

// These are the Clipboard selectors.
/** @ingroup Selectors
	Look at AIClipboardMessage.option to decide weather read or write. */
#define kSelectorAIGoClipboard				"AI Go"
/** @ingroup Selectors
	Check if you can copy this data. Return #kNoErr if you can and #kCantCopyErr
	if you cannot. */
#define kSelectorAICanCopyClipboard		"AI Can Copy"
/** @ingroup Selectors
	Make a duplicate of your AIClipboardMessage.ClipboardData */
#define kSelectorAICloneClipboard			"AI Clone"
/** @ingroup Selectors
	Dispose your data AIClipboardMessage.ClipboardData */
#define kSelectorAIDisposeClipboard		"AI Dispose"

/** Options that can be specified when registering a clipboard format handler.
	These options indicate the capabilities of the handler. */
enum AIClipboardFormatOptions {
	/** This format knows how to copy to the clipboard. */
	kClipboardCopy						=	(1L<<1),
	/** This format knows how to paste from the clipboard. */
	kClipboardPaste						=	(1L<<2),
	/** The clipboard preferences allow copying of certain formats to be enabled
		and disabled. This is done by getting and setting the clipboard format
		options. This option is a flag that indicates that the format is incapable
		of copying data to the clipboard and so the kClipboardCopy option should
		never be set. */
	kClipboardCannotCopy				=	(1L<<3)
};

/** @ingroup Errors
	Return in case you can't perform copy (return of kSelectorAICanCopyClipboard) */
#define kCantCopyErr		'CLP?'


/*******************************************************************************
 **
 ** Types
 **
 **/

/** Identifies a regitered clipboard format handler. */
typedef struct _t_AIClipboardOpaque *AIClipboardHandle;

/** Data used to register a clipboard format handler. */
typedef struct {
	/** "myCompany File Format" or "Rich Text Format" etc... */
	char* clipboardName;
	/** CF_TEXT CF_BITMAP etc...(Windows only) */
	long OleType;
	/** 'TEXT' 'PICT' etc... make sure it is unique (check AIDragDropTypes.h).
		This uniqueType represents ResType on the Mac side. */
	DDType uniqueType;
} AIClipboardData;


/** This is the message structure received when the clipboard format handler's main
	entry point receives a message with caller kCallerAIClipboard and selector one
	of #kSelectorAIGoClipboard, #kSelectorAICanCopyClipboard, #kSelectorAICloneClipboard
	or #kSelectorAIDisposeClipboard. */
typedef struct {
	SPMessageData d;
	AIClipboardHandle Clipboard;
	AIStream ClipboardData;
	long option;
} AIClipboardMessage;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	The clipboard suite enables plugins to register new clipboard format handlers.
	The handler's main entry point will then receive messages requesting it to
	copy data to and from the clipboard as needed. The messages have caller
	#kCallerAIClipboard. The possible selectors are:

	- #kSelectorAIGoClipboard
	- #kSelectorAICanCopyClipboard
	- #kSelectorAICloneClipboard
	- #kSelectorAIDisposeClipboard
*/
typedef struct {

	/** Register a new clipboard format handler. The data parameter identifies
		the types of clipboard data handled. The options provide information about
		the capabilities of the format handler. See #AIClipboardFormatOptions. */
	AIAPI AIErr (*AddClipboard) ( SPPluginRef self, AIClipboardData *data, 
								long options, AIClipboardHandle *Clipboard );
	/** Unregister a clipboard format handler. */
	AIAPI AIErr (*RemoveClipboard) (AIClipboardHandle Clipboard);
	/** Get the registered name of the clipboard format handler. */
	AIAPI AIErr (*GetClipboardName) (AIClipboardHandle Clipboard, const char* *name);
	/** Get the Windows type of the data handled by the format handler. */
	AIAPI AIErr (*GetClipboardOleType) (AIClipboardHandle Clipboard, long* OleType);
	/** Get the plugin reference for the plugin supplying the format handler. */
	AIAPI AIErr (*GetClipboardPlugin) (AIClipboardHandle Clipboard, SPPluginRef *plugin);
	/** Get the Macintosh type of the data handled by the format handler. */
	AIAPI AIErr (*GetClipboardType) (AIClipboardHandle Clipboard, DDType *uniqueType);
	/** Get the options describing the capabilities of the format handler. See
		#AIClipboardFormatOptions. */
	AIAPI AIErr (*GetClipboardOptions) (AIClipboardHandle Clipboard, long *options);
	/** Set the options describing the capabilities of the format handler. See
		#AIClipboardFormatOptions. */
	AIAPI AIErr (*SetClipboardOptions) (AIClipboardHandle Clipboard, long options);
	/** Count the number of registered clipboard format handlers. */
	AIAPI AIErr (*CountClipboards) (long *count);
	/** Get the Nth registered clipboard format handler. Zero based. */
	AIAPI AIErr (*GetNthClipboard) (long n, AIClipboardHandle *Clipboard);

} AIClipboardSuite;

#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
