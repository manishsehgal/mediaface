#ifndef __AITypes__
#define __AITypes__

/*
 *        Name:	AITypes.h
 *   $Revision: 17 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 through 10.0 core Type definitions.
 *
 * Copyright (c) 1986-2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 *
 *  Includes
 *
 */
#ifdef __cplusplus
#include <exception>
#endif

#ifndef __ASTypes__
#include "ASTypes.h"
#endif

#include "ADMTypes.h"

#ifndef __SPFiles__
#include "SPFiles.h"
#endif

#ifndef __AIPragma__
#include "AIPragma.h"
#endif


#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AITypes.h */

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 *
 *  Constants
 *
 */

#define kPluginInterfaceVersion6001		0x06000001  // 6.0x1
#define kPluginInterfaceVersion6002		0x06000020

#define kPluginInterfaceVersion7001		0x07000001	// AI 7.0

#define kPluginInterfaceVersion6021		0x06000021  // Joe's AI6-based prototype (only needed to run his plugins)
#define kPluginInterfaceVersion8001		0x08000001	// AI 8.0
#define kPluginInterfaceVersion9001		0x09000001	// AI 9.0
#define kPluginInterfaceVersion10001	0x10000001	// AI 10.0
#define kPluginInterfaceVersion11001	0x11000001	// AI 11.0
#define kPluginInterfaceVersion12001	0x12000001	// AI 12.0

#define kPluginInterfaceVersion			kPluginInterfaceVersion12001

#ifdef MAC_ENV
#define AIAPI_VERSION(x) (x + 1000)
#endif

#ifdef WIN_ENV
#define AIAPI_VERSION(x) (x)
#endif

#define kPlatformUnloadedSuiteProc	NULL

#if !defined(__BUILD_PLUGIN__)
#if defined(ILLUSTRATOR_H)
#define __BUILD_PLUGIN__ 0
#else
#define __BUILD_PLUGIN__ 1
#endif
#endif

//These are no longer needed. Consider them deprecated
#define PUSH_GLOBALS
#define POP_GLOBALS

// Determine the platform.
#if defined(MAC_ENV)
#define Macintosh 1
#else
#define Macintosh 0
#endif

#if !defined(MSWindows)
#if defined(WIN_ENV)
#define MSWindows 1
#else
#define MSWindows 0
#endif
#endif

// Some common error codes.
#define kCanceledErr			'STOP'
#define kNoDocumentErr			'DOC?'
#define kSelectorClashErr		'CLSH'
#define kNameNotFoundErr		'NAM?'
#define kNameInUseErr			'NAM2'
#define kInvalidNameErr			'NA*!'
#define kNameTooLongErr			'NAL!'


/*******************************************************************************
 *
 *  Types
 *
 */

#define AIAPI ASAPI

/** An opaque reference to an art object. */
typedef struct ArtObject *AIArtHandle;
/** An opaque reference to a layer. */
typedef struct _t_AILayerOpaque *AILayerHandle;

typedef ASFixed AIFixed;
typedef ASFract AIFract;
typedef unsigned long AIUFract;
typedef	ASReal AIReal, *AIRealPtr;				
typedef float AIFloat;
typedef double AIDouble;
typedef long	AIStream;

typedef ASFixedRect AIFixedRect;
typedef ASFixedPoint AIFixedPoint;
typedef ASFixedMatrix AIFixedMatrix;
typedef ASRealRect AIRealRect, *AIRealRectPtr;
typedef ASRealPoint AIRealPoint, *AIRealPointPtr;
typedef ASRealMatrix AIRealMatrix, *AIRealMatrixPtr;

typedef ASBoolean AIBoolean;

typedef ADMRect AIRect;
typedef ADMPoint AIPoint;

struct AIDoubleRect {
	AIDouble left, top, right, bottom;
};

#ifdef MAC_ENV
/** AIPortRef is the same as a Macintosh CGrafPtr. */
typedef CGrafPtr AIPortRef;

/** AIWindowRef is the same as a Macintosh WindowPtr. */
typedef WindowRef AIWindowRef;

/** AIDialogRef is the same as a Macintosh DialogPtr. */
typedef DialogRef AIDialogRef;	
#endif

#ifdef WIN_ENV
/** AIPortRef is the same as a Windows HDC. */
typedef void* AIPortRef;				

/** AIWindowRef is the same as a Windows HWND. */
typedef void* AIWindowRef;			

/** AIDialogRef is the same as a Windows HWND. */
typedef void* AIDialogRef;			
#endif

typedef ADMRGBColor AIRGBColor;

typedef ADMEvent AIEvent;

typedef ASErr AIErr;

/** A Color Profile. Please use the AIColorProfile type instead of a ACEProfile* */
class ACEProfile;
typedef ACEProfile *AIColorProfile;

/** An Illustrator Command ID. */
typedef unsigned long AICommandID;

/** This is a structure for enforcing the passing of ZStrings.
	Since ZStrings are just char*, it's easy to pass a non-zstring into
	a function expecting a zstring by accident. By putting "ZREF" into your
	API, it 
	a) tells the user of the API that you only expect ZStrings in
	and
	b) forces them to use the ZSTR macro (and think about actually ZStringing whatever it is)
	Inside the actual function, you can just cast it back to a char* if you need to look inside
	it for some reason. */
typedef const struct ZREFStruct *ZRef;
#define ZREF(x) (ZRef)x

/** Every art object has a paint order, which is its position in the back to front
	ordering of the objects. The paint order determines which objects are drawn
	first, and so it determines which objects obscure others when their bounds
	overlap. If the objects do not overlap the paint order is irrelevant in
	rendering.
	
	Some functions in the API require a paint order specifier. The paint order
	specifier indicates the position of one object with respect to another when
	creating or moving it. */
typedef enum AIPaintOrder {
	kPlaceDefault = 0,
	kPlaceAbove = 1,
	kPlaceBelow,
	kPlaceInsideOnTop,
	kPlaceInsideOnBottom,
	kPlaceAboveAll,
	kPlaceBelowAll
} AIPaintOrder;

/** Paint can be applied to fills, strokes, or both. Some functions in the API
	require the ability to distinguish between these cases. */
typedef enum AIPaintUsage {
	kAINotUsed = 0x0000,
	kAIUsedOnFill = 0x0001, 
	kAIUsedOnStroke = 0x0002,
	kAIUsedOnBoth = 0x0003
} AIPaintUsage;

/** These constants identify the various shipping versions of Illustrator. */
typedef enum AIVersion {
	kAIVersion1 = 1,
	kAIVersion88,
	kAIVersion3,
	kAIVersion4,
	kAIVersion5,
	kAIVersion6,
	kAIVersion7,
	kAIVersion8,
	kAIVersion9,
	kAIVersion10,
	kAIVersion11,
	kAIVersion12
} AIVersion;

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
namespace ai {

/** this macro is used to decorate C++ functions and methods that are
	guaranteed not to throw. due to the current state of compiler support
	for throw specifications it is defined to be empty. */
#define AINOTHROW


/** Exception class thrown by C++ classes */
class Error : public std::exception {
public:
	Error (AIErr _err) : err(_err)
		{}
	operator AIErr () const
		{return err;}

protected:
	AIErr err;
};

} // end namespace ai
#endif


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END


#endif
