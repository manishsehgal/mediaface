#ifndef _IAIREF_H_
#define _IAIREF_H_

/*
 *        Name:	IAIRef.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Reference counted objects.
 *
 * Copyright (c) 1986-1999 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#include	"AICountedObject.h"

namespace ai {
// start of namespace ai


/**
	no instance of this class exists. it is used only to limit the potential
	for namespace collisions for the declaration of Replace
 */
class RefReplaceParam;

/**
	The function needs to be defined by each plugin that uses this header to return
	a valid AICounterObjectSuite* for the IAIRefs to work with.
	You must add the appropriate IAIRef<blah>.cpp file to your project to get things to work.
	If you define yet another way to access suites, please add a new IAIRef<blah>.cpp.
*/

extern AICountedObjectSuite *GetAICountedObjectSuitePtr();

/**
	Many object types are reference counted. Examples are dictionaries and arrays:
	AIDictionaryRef and AIArrayRef respectively. When a plugin acquires one of
	these objects its reference count is incremented and the plugin must be sure
	to "Release" the object else a memory leak will result.

	A plugin can relieve itself of the burden of having to worry about this memory
	management by using the Ref<X> template class. This is a "smart pointer"
	class which automatically takes care of incrementing and decrementing the
	count of the referenced object. For example the following code could be used
	to manipulate a hypothetical art object's dictionary without any need to
	worry about the reference counting.

	@code
		Ref<AIDictionaryRef> dict1, dict2;
		sAIArt->GetDictionary(art, dict1 << Replace);
		sAIDictionary->CreateDictionary(dict2 << Replace);
		sAIDictionary->SetBooleanEntry(dict2, "my boolean", true);
		sAIDictionary->SetDictEntry(dict1, "first dictionary", dict2);
		sAIDictionary->CreateDictionary(dict2 << Replace);
		sAIDictionary->SetBooleanEntry(dict2, "my boolean", false);
		sAIDictionary->SetDictEntry(dict1, "second dictionary", dict2);
	@endcode

	@see AICountedObject
 */

template <class X> class Ref {
public:
	/**
		construct a null reference
	 */
	Ref () : x(0) {}
	/**
		construct a reference to an object. this will increment the reference
		count.
	 */
	Ref (const X& x);
	/**
		copy construct a reference from another reference. this will increment
		the reference count.
	 */
	Ref (const Ref<X>& ref);

	/**
		destructor. this will decrement the reference count.
	 */
	~Ref ();

	/**
		implicit conversion to an X allows a Ref<X> to be used wherever an X
		can appear.
	 */
	operator X () const
		{return x;}
	
	/**
		assignment operators decrement the reference count of the current
		object and increments that of the one newly assigned.	
	 */
	Ref<X>& operator= (const X& x);
	/**
		assignment operators decrement the reference count of the current
		object and increments that of the one newly assigned.	
	 */
	Ref<X>& operator= (const Ref<X>& ref);

	/**
		comparison operators test whether the same objects are identified
		by both references
	 */
	bool operator== (const Ref<X>& ref) const
		{return x == ref.x;}
	/**
		comparison operators test whether the same objects are identified
		by both references
	 */
	bool operator!= (const Ref<X>& ref) const
		{return x != ref.x;}

	/**
		this allows a Ref<X> to be passed to a plugin API which takes an
		X*. it releases the current referenced object and expects the API
		function to assign a new object to the pointer and increment its
		reference count. use the function "Replace" for the dummy
		function parameter. 
	 */
	X* operator<< (void (*f)(const RefReplaceParam& p));

	/**
		this allows a Ref<X> to be passed to the AIEntry suite ToXXX
		functions.  it bumps the reference count to account for the 
		decremented count on the API side, keeping the count correct and 
		allowing the destructor on the plug-in side to dispose of the object
	 */
	Ref<X>& to();

protected:
	X x;
};


inline void Replace (const RefReplaceParam&)
{
}

template <class X> Ref<X>::Ref (const X& _x) : x(_x)
{
	AICountedObjectSuite* theSuite = GetAICountedObjectSuitePtr();
	theSuite->AddRef(x);
}

template <class X> Ref<X>::Ref (const Ref<X>& ref) : x(ref.x)
{
	AICountedObjectSuite* theSuite = GetAICountedObjectSuitePtr();
	theSuite->AddRef(x);
}

template <class X> Ref<X>::~Ref ()
{
	AICountedObjectSuite* theSuite = GetAICountedObjectSuitePtr();
	theSuite->Release(x);
}

template <class X> Ref<X>& Ref<X>::operator= (const X& _x)
{
	if (x != _x)
	{
		AICountedObjectSuite* theSuite = GetAICountedObjectSuitePtr();
		theSuite->Release(x);
		x = _x;
		theSuite->AddRef(x);
	}
	return *this;
}

template <class X> Ref<X>& Ref<X>::operator= (const Ref<X>& ref)
{
	if (x != ref.x)
	{
		AICountedObjectSuite* theSuite = GetAICountedObjectSuitePtr();
		theSuite->Release(x);
		x = ref.x;
		theSuite->AddRef(x);
	}
	return *this;
}

template <class X> X* Ref<X>::operator<< (void (*)(const RefReplaceParam&))
{
	AICountedObjectSuite* theSuite = GetAICountedObjectSuitePtr();
	theSuite->Release(x);
	x = 0;
	return &x;
}

template <class X> Ref<X>& Ref<X>::to()
{
	AICountedObjectSuite* theSuite = GetAICountedObjectSuitePtr();
	theSuite->AddRef(x);

	return *this;
}

// end of namespace ai
}

#if !AICOUNTEDOBJECTSUITE_DEFINED

extern "C" 
{	
	extern AICountedObjectSuite *sAICountedObject;
}

/**
	This implements the GetAICountedObjectSuitePtr for "standard" naked suite pointers.
*/

inline AICountedObjectSuite *ai::GetAICountedObjectSuitePtr()
{
	return sAICountedObject;
}

#endif //AICOUNTEDOBJECTSUITE_DEFINED

#endif
