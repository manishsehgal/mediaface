#ifndef __AICommandManager__
#define __AICommandManager__

/*
 *        Name:	AICommandManager.h
 *   $Revision: 6 $
 *      Author:	Grace Ge
 *        Date:	   
 *     Purpose: .	
 *
 * Copyright (c) 1986-1997 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIPlugin__
#include "AIPlugin.h"
#endif

#include "AIUnicodeString.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AICommandManager.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAICommandManagerSuite			"AI Command Manager Suite"
#define kAICommandManagerSuiteVersion1	AIAPI_VERSION(1)
#define kAICommandManagerSuiteVersion2	AIAPI_VERSION(2)
#define kAICommandManagerSuiteVersion3	AIAPI_VERSION(3)
#define kAICommandManagerSuiteVersion	kAICommandManagerSuiteVersion3
#define kAICommandManagerVersion		kAICommandManagerSuiteVersion

/*******************************************************************************
 **
 **	Types
 **
 **/

/** The contents of a pluginCommand message. */
typedef struct {
	SPMessageData d;
	/** zero for plugins */
	AICommandID commandID;
	/** short cut key */
	int key;
	/** adm style modifiers */
	unsigned long admModifiers;
	/** command id string */
	char *commandStr;
	/** menu item string */
	char *localizedStr;
} AICommandMessage;

/*
 * PluginCommand Message
 */

/** @ingroup Callers */
#define kCallerAICommand					"Plugin Command Manager"
/** @ingroup Selectors */
#define kSelectorAICommand					"Do Plugin Command"

// key command context
#define kAICommandNormalContext				0x0000
#define kAICommandTextEditContext			0x0001
#define kAICommandToolBoxContext			0x0002
#define kAICommandPluginContext				0x0004

/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	/** Add a command. This adds an association between a command id, a name and a 
		localized name. */
	AIAPI AIErr (*AddCommand) (AICommandID commandID, const char *name, const ai::UnicodeString& localizedName);	

	/** Given the non-localized name for a command, get the command id. */
	AIAPI AIErr (*GetCommandIDFromName) (const char *name, AICommandID *commandID);
	/** Given the localized name for a command, get the command id. */
	AIAPI AIErr (*GetCommandIDFromLocalizedName) (const ai::UnicodeString& , AICommandID *commandID);
	/** Given the command id get its non-localized name. */
	AIAPI AIErr (*GetCommandName) (AICommandID commandID, char *name);
	/** Given the command id get its localized name. */
	AIAPI AIErr (*GetCommandLocalizedName) (AICommandID commandID, ai::UnicodeString& localizedName);

	/** Not implemented */
	AIAPI AIErr (*TestFunctionKey) (int fkey, unsigned long admModifiers, AIBoolean *used);
	/** Not implemented */
	AIAPI AIErr (*SetActionFunctionKey) (int fkey, unsigned long admModifiers);
	/** Not implemented */
	AIAPI AIErr (*ClearActionFunctionKey) (int fkey, unsigned long admModifiers);

	/** Given a keyboard shortcut, retrieve the command id for the corresponding command.
		Assumes the keyboard shortcut is a valid shortcut for a command (ie. it has the
		kADMMenuKeyDownModifier). Returns kBadParameterErr if no command id exists for
		this shortcut. */
	AIAPI AIErr (*GetCommandIDFromShortCutKey) (int key, unsigned long admModifiers, AICommandID *commandID);

	AIAPI AIErr (*SearchCommandLocalizedName) (const ai::UnicodeString& partialName, ai::UnicodeString& localizedName);

	/** Not implemented */
	AIAPI AIErr (*GetShortCutKeyFromCommandID) (AICommandID commandID, int context, int *key, unsigned long *admModifiers, int *represent);
	/** Not implemented */
	AIAPI AIErr (*SetShortCutKeyFromCommandID) (AICommandID commandID, int context, int key, unsigned long admModifiers, int represent);
	/** Not implemented */
	AIAPI AIErr (*DeleteShortCutKeyFromCommandID) (AICommandID commandID, int context, int key, unsigned long admModifiers, int represent);

	/** Not implemented */
	AIAPI AIErr (*GetShortCutKeyOfCommandEx) (AICommandID commandID, int context, char *commandStr, int *key, unsigned long *admModifiers, int *represent);
	/** Not implemented */
	AIAPI AIErr (*SetShortCutKeyOfCommandEx) (AICommandID commandID, int context, int key, unsigned long admModifiers, int represent,
												char *commandStr, char *localizedStr, SPPluginRef pluginRef);
	/** Not implemented */
	AIAPI AIErr (*DeleteShortCutKeyOfCommandEx) (AICommandID commandID, int context, char *commandStr, int key, unsigned long admModifiers, int represent);
	/** Not implemented */
	AIAPI AIErr (*GetCommandExFromShortCutKey) (int key, unsigned long admModifiers, int context, AICommandID *commandID,
												SPPluginRef *pluginRef, char *commandStr, int maxCommandStr, char *localizedStr, int maxLocalizedStr);

	/** Not implemented */
	AIAPI AIErr (*SetActionFunctionKeyEx) (int fkey, unsigned long admModifiers, char *commandStr);

	/** Not implemented */
	AIAPI AIErr (*CountCommands)(int *totalCommands);
	/** Not implemented */
	AIAPI AIErr (*GetNthCommandInfo)(int index, AICommandID *commandID, int *context, int *key, int *represent, unsigned long *admModifiers,
									SPPluginRef *pluginRef, char *commandStr, int maxCommandStr, char *localizedStr, int maxLocalizedStr);

	/** Remove the associations for the given command id. */
	AIAPI AIErr (*DeleteCommand) (AICommandID commandID);

} AICommandManagerSuite;

#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
