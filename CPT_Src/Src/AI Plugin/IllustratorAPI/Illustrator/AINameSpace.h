#ifndef __AINameSpace__
#define __AINameSpace__

/*
 *        Name:	AINameSpace.h
 *   $Revision: 4 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Name Space Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIDataFilter__
#include "AIDataFilter.h"
#endif

#ifndef __AIStringPool__
#include "AIStringPool.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AINameSpace.h */

/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAINameSpaceSuite		"AI Name Space Suite"
#define kAINameSpaceSuiteVersion		AIAPI_VERSION(2)
#define kAINameSpaceVersion		kAINameSpaceSuiteVersion

/** @ingroup Errors */
#define kNameSpaceErr	  		'NAME'


/*******************************************************************************
 **
 **	Types
 **
 **/

typedef struct NameSpace AINameSpace;

#if __BUILD_PLUGIN__
#define kNSMaxNameLength		(300)
#define kNSMaxPathLength		((kNSMaxNameLength + 1) * 5)
#define kNSPathSeparator		'/'
typedef char NSPath[kNSMaxPathLength + 1];
typedef char NSName[kNSMaxNameLength + 1];
#endif


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	A namespace is essentially a global dictionary. It is not associated with any particular
	document. Its keys are strings called "paths" and the associated values can be of various
	types such as integers, reals and strings.
	
	Unlike dictionaries the contents of a namespace can be viewed as having a hierarchical
	organization. The hierarchy is imposed by thinking of the keys as being paths, similar to
	directory paths in a file system. The paths are divided into components by '/' separators.
	For example the key "/root/entry" has two components "root" and "entry".

	Several APIs allow partial paths to be specified. These APIs refer to all keys that
	are prefixed with that path. When a partial path is specified the trailing '/' separator
	should be included. The path "/" corresponds to the root.

	A path may not be more than 505 characters in length. Each component of a path (excluding
	separators) may not be more than 100 characters in length.

	The APIs for getting and setting values in a namespace take a type name indicating the
	value type and a variable sequence of parameters for the actual value. The type names
	and associated values are listed below.

	- "integer": long value
	- "real": double value
	- "string": char* value
	- "raw": long length, char* value
	- "unicodeString": ai::UnicodeString* value
 */
typedef struct {

	/** Create a new name space. The strings for the namespace path components will be allocated
		in the supplied string pool. In this case it is the clients responsibility to dispose of
		the string pool and this must be done after the namespace is disposed. If the string pool
		is NULL then the global string pool is used. See AIStringPoolSuite. */
	AIErr (*AllocateNameSpace) ( AIStringPool *pool, AINameSpace **space );
	/**  Dispose of a name space. */
	AIErr (*DisposeNameSpace) ( AINameSpace *space );

	/** Set the value stored at the specified point in the namespace. */
	AIErr (*SetValue) ( const AINameSpace *space, const char *path, const char *type, ... );
	/** Get the value stored at the specified point in the namespace. */
	AIErr (*GetValue) ( const AINameSpace *space, const char *path, const char *type, ... );

	/** Get the type of the value stored at the specified point in the namespace. */
	AIErr (*GetType) ( const AINameSpace *space, const char *path, char **type );
	/** Get a count of the number of times the value stored at the specified point in the
		namespace has been changed. Each time the value is set the change count is incremented.
		If the path indicates an intermediate node in the namespace then the count will
		represent the number of times all values stored under that node have changed. */
	AIErr (*GetChangeCount) ( const AINameSpace *space, const char *path, long *count );
	/** Removes the value stored at the specified path. If a partial path is specified all
		values under that path are removed. */
	AIErr (*RemoveValue) ( const AINameSpace *space, const char *path );
	/** Returns a count of the number of unique components that can be appended to the path to
		produce paths or partial paths in the namespace. Thinking of the namespace as a hierarchical
		directory this corresponds to the number of entries in the directory element specified
		by the path. */
	AIErr (*CountPaths) ( const AINameSpace *space, const char *path, long *count );
	/** Returns the nth unique component that can be appended to the path to produce a new
		path or partial path in the namespace. The component returned will include the trailing
		separator if it identifies a partial path. */
	AIErr (*GetNthPath) ( const AINameSpace *space, const char *path, long n, char *nthPath );

	/** De-serializes namespace entries that were serialized using FlushValue(). The
		specified path is used as the prefix for the de-serialized keys. */
	AIErr (*ParseValue) ( const AINameSpace *space, const char *path, AIDataFilter *filter );
	/** Serializes the namespace entries identified by the partial path to the data filter.
		The entries can be de-serialized by the ParseValue() API. Only the key suffixes are
		stored hence they can be de-serialized with a new prefix. */
	AIErr (*FlushValue) ( const AINameSpace *space, const char *path, AIDataFilter *filter );

} AINameSpaceSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
