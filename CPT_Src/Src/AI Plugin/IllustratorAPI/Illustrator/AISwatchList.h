#ifndef __AISwatchList__
#define __AISwatchList__

/*
 *        Name:	AISwatchList.h
 *   $Revision: 7 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 7.0 Swatch list management
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIColor__
#include "AIColor.h"
#endif

#ifndef __AIDocument__
#include "AIDocument.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AISwatchList.h */

/*******************************************************************************
 **
 ** Constants
 **
 **/
#define kAISwatchListSuite					"AI Swatch List Suite"
#define kAISwatchListSuiteVersion5			AIAPI_VERSION(5)
#define kAISwatchListSuiteVersion			kAISwatchListSuiteVersion5
#define kAISwatchListVersion				kAISwatchListSuiteVersion

/** @ingroup Notifier
	This notifier is sent whenever a change is made to any swatch list for any
	open document (including libraries). These changes include insertion and
	deletion of a swatch into a swatch list and redefinition of a swatch color or
	name. There is no data sent with the notifier, so the plug-in must determine
	what change occured using the Swatch List Suite functions. */
#define kAISwatchListChangedNotifier				"AI Swatch List Changed Notifier"
/** @ingroup Notifier
	The ChangedInternally notifier is sent when some object in the swatch list has changed its
	definition.  It may be followed by a SwatchListChanged notifier if the swatch list itself has also
	changed.  Both will have the same refStamp as their notifier data if sent as a pair */
#define kAISwatchListChangedInternallyNotifier		"AI Swatch List Changed Internally Notifier"
/** @ingroup Notifier */
#define kAISwatchReplaceColorNotifier				"AI Swatch Replace Color Notifier"	// used only by SwatchLib
/** @ingroup Notifier */
#define kAICreateNewSwatchNotifier					"AI Create New Swatch Notifier"	// used only by PaintStyle

/** @ingroup Errors */
#define kCantDeleteSwatchErr				'!XDS'

/*******************************************************************************
 **
 **	Types
 **
 **/

/** Opaque reference to a swatch */
typedef struct _AISwatchOpaque *AISwatchRef;
/** Opaque reference to a swatch list */
typedef struct _AISwatchListOpaque *AISwatchListRef;

/*******************************************************************************
 **
 **	Suite
 **
 **/

/** Each Illustrator document has an associated swatch list which contains the
	document�s swatches as seen in the Swatches palette. The routines in the
	Swatch List suite are used to access and modify swatch lists and swatches.

	Most functions in the Swatch List Suite take an AISwatchListRef argument.
	This is an opaque reference to a document's swatch list, and can be
	obtained using the GetSwatchList() function or from the AISwatchLibrariesSuite.

	However, plug�ins are usually only concerned with the current document's
	swatch list, so for convenience all of the functions which require an
	AISwatchListRef will accept NULL to indicate the current document's
	swatch list.

	The elements in a swatch list are of type AISwatchRef, which is an opaque
	reference to a swatch. A swatch is simply an AIColor struct and an associated
	name. For AIColor types that are themselves defined by a named object such
	as custom colors, patterns and gradients the swatch name is always the
	same as the name of the object. For other types of AIColor the swatch name
	is independent.

	Note: Indexing is zero based, for example index 2 means 3rd swatch.
	You can pass -1 for index while inserting a swatch in which
	case swatch will be appended to the list.

	There are three notifiers associated with swatch lists:

	- #kAISwatchListChangedNotifier
	- #kAISwatchListChangedInternallyNotifier
	- #kAISwatchReplaceColorNotifier
*/
typedef struct {

	/** Get number of swatches in a swatch list */
	int ASAPI (*CountSwatches)				(AISwatchListRef list);

	/** Get first swatch in a swatch list */
	AISwatchRef ASAPI (*GetFirstSwatch)		(AISwatchListRef list);
	/** Get next swatch in a swatch list */
	AISwatchRef ASAPI (*GetNextSwatch)		(AISwatchRef prev);
	/** Get nth swatch in a swatch list. This is more efficient than GetNextSwatch() for
		iterating through the list.  */
	AISwatchRef ASAPI (*GetNthSwatch)		(AISwatchListRef list, int index);
	/** Find swatch with the specified name. Names are ai::UnicodeString strings. If no match is found,
		a NULL AISwatchRef is returned by the function. */
	AISwatchRef ASAPI (*GetSwatchByName)	(AISwatchListRef list, const ai::UnicodeString& name);

	/** This function creates a new swatch and places it in the specified list at
		position N. The swatch previously at position N and all swatches following it in
		the list are bumped down the list by one position. To append a swatch specify
		-1 for N.

		You must set the name and color of the swatch after it has been inserted
		using SetAIColor and SetSwatchName. */
	AISwatchRef ASAPI (*InsertNthSwatch)	(AISwatchListRef list, int index);
	/** This function removes the nth swatch from the list. All swatches in the list
		from position N+1 on are moved up the list one position. The error #kCantDeleteSwatchErr
		is returned if an improper index is specified. */
	ASErr ASAPI (*RemoveNthSwatch)			(AISwatchListRef list, int index);

	/** This function reads the color definition from the specified swatch and fills in
		aicolor with the corresponding values. The AIColor can be any color type
		supported by Illustrator. */
	ASErr ASAPI (*GetAIColor)				(AISwatchRef swatch, AIColor *aicolor);
	/** This function takes the values from aicolor and puts them in the swatch.
		The AIColor can be any color type supported by Illustrator. */
	ASErr ASAPI (*SetAIColor)				(AISwatchRef swatch, AIColor *aicolor);

	/** Returns an ai::UnicodeString string containing the swatch name. */
	ASErr ASAPI (*GetSwatchName)			(AISwatchRef swatch, ai::UnicodeString& name);
	/** Set the name of the swatch. If the swatch identifies a custom color, pattern or gradient then
		this changes the name of the underlying object. */
	ASErr ASAPI (*SetSwatchName)			(AISwatchRef swatch, const ai::UnicodeString& name);

	/** Get document's swatch list. Specify NULL for the AIDocumentHandle to indicate the current
		document. An AIDocumentHandle may be obtained from the AIDocumentListSuite or the
		AIPathStyleSuite::ImportStyles() API. */
	ASErr ASAPI (*GetSwatchList)			(AIDocumentHandle document, AISwatchListRef *list);
	
	/** Returns the first swatch with a color matching the input color. For patterns and gradients
		this API only checks that the gradient or pattern objects match, it does not check that
		the other parameters such as the gradient matrix also match. For all other types all fields
		are checked for a match. */
	AISwatchRef ASAPI (*GetSwatchByColor)	(AISwatchListRef list, const AIColor *aicolor);
	/** Returns the first swatch in the list with the given name and color type. */
	AISwatchRef ASAPI (*GetSwatchByNameAndType)(AISwatchListRef list, const ai::UnicodeString& name, AIColorTag type);
	
	/** Remove the swatch from the list. */
	ASErr ASAPI (*RemoveSwatch)				(AISwatchListRef list, AISwatchRef swatch, ASBoolean deleteCustomColor);

} AISwatchListSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
