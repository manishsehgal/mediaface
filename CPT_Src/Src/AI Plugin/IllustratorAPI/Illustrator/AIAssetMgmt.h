#ifndef __AIAssetMgmt__
#define __AIAssetMgmt__

/*
 *        Name:	AIAssetMgmt.h
 *		$Id $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 12.0 Asset Management Suite.
 *
 * Copyright (c) 1999-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#include "IAIFilePath.hpp"

extern "C" {

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIAssetMgmt.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIAssetMgmtSuite				"AI Asset Management Suite"
#define kAIAssetMgmtSuiteVersion5		AIAPI_VERSION(5)
#define kAIAssetMgmtSuiteVersion		kAIAssetMgmtSuiteVersion5
#define kAIAssetMgmtVersion				kAIAssetMgmtSuiteVersion

#define kAILaunchProjectBridgeNotifier	"AI Launch Project Bridge Notifier"

/*******************************************************************************
 **
 **	Types
 **
 **/
/** This is xmp session object.  You must have xmp data to create this session object.  Use this xmp session object to manipulate 
	data in the xmp buffer.*/
typedef struct _AIXMPSession *AIXMPSession;

/** Data struture passed in as an argument for updating the XMP metadata. 
    See also UpdateDynamicMetadataFields.
 */
typedef struct AIXMPDataFieldMap {
    /** name of the XMP property, such as "CreateDate", "CreatorTool", "Format", ... */
    const char *name;
    /** new value of the XMP property. A null pointer value means to
        remove the XMP property from the XMP packet if the replace flag is true. This can be used
        for example to remove the XMP "Thumbnails" data from the original packet. */
    const char *value;
    /** whether to reset the value of the XMP property if it already
        exists in the input XMP metadata buffer. */
	AIBoolean	replace;
} AIXMPDataFieldMap;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** The asset managment suite is a suite that exposes some simple XMP metadata
    manipulation routines and the Adobe Version Cue(TM) functionalities. 
 */

typedef struct AIAssetMgmtSuite {
	/** Is Adobe Version Cue(TM) enabled?
    */
	AIAPI AIBoolean (*IsAssetMgmtEnabled)();

	/** Manipulate the input XMP metadata buffer: update the XMP property name/value pair
        as specified in the input XMP fields array, and creates a new XMP metadata buffer
        upon output. The caller is responsible for releasing the returned output buffer.
        For details on the XMP properties, please refer to "XMP Specification Version 1.6", 
        August 15, 2002, Adobe Systems Inc. See also AIDocumentSuite::GetDocumentXAP() and
        AIDocumentSuite::SetDocumentXAP() for getting and setting the XMP metadata buffer
        for the current document.
    */
	AIAPI AIErr (*UpdateDynamicMetadataFields) (const char *xapIn, 
												const AIXMPDataFieldMap *fields, const int length, 
												char **xapOut);

	/** 
		@mimeType		- param to Update the "Format" property (MIME-type) of the XMP metadata for the current document.
		@managedURL		- URL of the file whose metadata is being updated.Is NULL for local(unmanaged) files.
							Updates "ManageTo" field of kXMP_NS_XMP_MM namespace.
		@preserveDocID	- This param if true ensures that a old DocumentID(128-bit  uuid) is retained
		in the XMP Packet, otherwise a new "DocumentID" is associated with(created in) XMP Packet.
		preserveDocID is generally set to true when a Document "Save" is done.
		preserveDocID may be set to false for the case of Document=>
		"Save As...","Save As Copy" and All Export operations. 
		The API is currently used only by Illustrator core and it updates AI document's XMP packet.
    */
	AIAPI AIErr (*UpdateDynamicMetadata) (const char *mimeType, const char* managedURL /* = NULL */, AIBoolean	preserveDocID);

	/** Progress callback for Adobe Version Cue(TM) related network operation. Legacy API for
        internal Illustrator use only. 
	    Return true to continue the operation, false to cancel. 
    */
	AIAPI AIBoolean (*Progress) ( ASInt32 bytesTransferred, ASInt32 contentLength,
								  char *server, char *uri, char *method,
								  char *contentType, const ai::FilePath &localFile);

	/** Create a new XMP session object given xmpIn buffer.  Use this object to manipulate xmp data.
		xmpIn is the input buffer containing xmp data, encoded in UTF-8.  If xmpIn is null, a session object is still created and can be used with SetProperty() API.
		Return error if out of memory.
	*/
	AIAPI AIErr (*CreateXMPSession) (const char *xmpIn , AIXMPSession* xmp);
	/** Dispose the xmp session object that was created from CreateXMPSession().*/
	AIAPI AIErr (*DisposeXMPSession) (AIXMPSession xmp);
	/** Return the size of this xmp session object.  Use this size to allocate memory before calling GetXMPSessionBuffer()*/
	AIAPI AIErr (*GetXMPSessionSize) (AIXMPSession xmp , int* xmpSize);
	/** Return the buffer of the xmp session object.  The buffer size should be enough and can be determined by GetXMPSessionSize().*/
	AIAPI AIErr (*GetXMPSessionBuffer) (AIXMPSession xmp , char *xmpOut);
	/** Get the size of a certain key.  
		Return error kNoSuchKey if key doesn't exist.*/
	AIAPI AIErr (*GetPropertySize) (AIXMPSession xmp, const char *nameSpace, const char* key, int* valueSize);
	/** Get the value if a given key.  Make sure to call GetPropertySize() to get the proper size of the returned value.*/
	AIAPI AIErr (*GetProperty) (AIXMPSession xmp, const char *nameSpace, const char* key , char* value);
	/** Set the value of a given key.*/
	AIAPI AIErr (*SetProperty) (AIXMPSession xmp, const char *nameSpace, const char* key , const char* value);
	/** Remove the given key from xmp data.*/
	AIAPI AIErr (*DeleteProperty) (AIXMPSession xmp, const char *nameSpace, const char* key);

	/** 
	1. Show FileInfo Dialog for a given XMP packet. Currently used by Stock-Photo.
	2. Memory to xmpOut is allocated by "SPBasic::AllocateBlock". Clients, Please use "SPBasic::FreeBlock" appropriately.
	3. If you do not want to get a copy of new updated XMP pass "xmpOut = NULL". In this case a read-only FileInfo dialog will be shown to user.
	4. This API does not check whether UserInteraction is turned on/off. So, Clients should do that at their end before calling this API.
	5. The dialogTitle (UTF-8 string) is typically the name of the artwork where the xmpIn is coming from. When NULL pointer is passed in,
	   the generic dialog title "File Info" will be used.
	*/
	AIAPI AIErr (*ShowMetadataDialog) (const char *xmpIn, char **xmpOut, const char *dialogTitle);

} AIAssetMgmtSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

}


#endif
