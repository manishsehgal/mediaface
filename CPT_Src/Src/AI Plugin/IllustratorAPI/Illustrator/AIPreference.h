#ifndef __AIPreference__
#define __AIPreference__

/*
 *        Name:	AIPreference.h
 *   $Revision: 6 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator Preference Suite.
 *
 * Copyright (c) 1986-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIFixedMath__
#include "AIFixedMath.h"
#endif

#ifndef __SPPlugins__
#include "SPPlugs.h"
#endif

#ifndef __ADMTypes__
#include "ADMTypes.h"
#endif

#ifndef __AIMenu__
#include "AIMenu.h"
#endif

#include "IAIUnicodeString.h"
#include "IAIFilePath.hpp"

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIPreference.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIPreferenceSuite		"AI Preference Suite"
#define kAIPreferenceSuiteVersion4	AIAPI_VERSION(4)
#define kAIPreferenceSuiteVersion	kAIPreferenceSuiteVersion4
#define kAIPreferenceVersion		kAIPreferenceSuiteVersion

/** @ingroup Notifiers
	This notifier is sent whenever a change is made to the preferences.
*/
#define kAIPreferenceChangedNotifier	"AI Preference Changed Notifier"


/** @ingroup Callers */
#define kAIPreferenceCaller				"AI Preference"
/** @ingroup Selectors */
#define kAIPreferenceInitSelector		"Init Preference Panel"
/** @ingroup Selectors */
#define kAIPreferenceOKSelector			"Preference Dialog OK"
/** @ingroup Selectors */
#define kAIPreferenceCancelSelector		"Preference Dialog Cancel"
/** @ingroup Selectors */
#define kAIPreferenceUpdateSelector		"Preference Dialog Upadate"

/** When you want to add a checkbox in alert dialog to disable it, you need to call
	PutBooleanPreference(kAIPreferenceTurnOffWarningPrefix, "your plugin unique string", value)
	to set it into preference. So we can clear it from General Preference. */
#define kAIPreferenceTurnOffWarningPrefix	"DontShowWarningAgain"

/*******************************************************************************
 **
 **	Types
 **
 **/

/** This is a reference to a preference itemGroup. It is never dereferenced. */
typedef struct _t_AIPreferenceItemGroupOpaque	*AIPreferenceItemGroupHandle;

/** Type for a date/time value stored in preferences */
typedef unsigned long AIPreferenceDateTime;

/** Message structure sent by the preference selectors. */
struct AIPreferencePanelMessage {
	SPMessageData d;
	AIPreferenceItemGroupHandle itemGroup;
};

/*******************************************************************************
 **
 **	Suite
 **
 **/

/** The Preference Suite allows you to store information in the Adobe Illustrator
	preferences file.

	The Preference Suite provides a cross-platform way to store your plug-in�s
	preferences. You can restore your preferences during startup, and save them
	during shutdown. The Preference Suite stores information in the Illustrator
	Preferences file.

	Each function in the suite takes three arguments:

	- a prefix, which is generally your plug�in�s name,
	- a suffix names a specific preference item in your plug�in, and
	- a value for the preference item.
	
	The Preference Suite supports a number of basic types: boolean, integer,
	real, C string, block, fixed, fract, Pascal string, date/time stamp, point, rect,
	and file spec. For each data type, the suite contains a Get and a Put function
	to read or write the corresponding value for a prefix/suffix pair.

	This example shows saving preferences for a window position and visibility
	status:

@code
	// save preferences for our dialog
	error = sPref->PutBooleanPreference( kPluginName, "dialogShowing", g->dialogShowing );
	error = sPref->PutPointPreference( kPluginName, "dialogPosition", &g->dialogPosition );
@endcode

	When you want to retrieve your preference info, use the corresponding Get
	calls. If a prefix/suffix pair isn't found in the preferences file, the third argument
	(value) will be unchanged. You should preset the default value for the
	preference you are retrieving.

@code
	//retrieve preferences for our dialog
	g->dialogShowing = false; // default is not visible
	error = sPref->GetBooleanPreference( kPluginName, "dialogShowing", &g->dialogShowing );
	g->dialogPosition = { 100 , 100 }; // default position
	error = sPref->GetPointPreference( kPluginName, "dialogPosition", &g->dialogPosition );
@endcode

	Generally, you read your prefs during the startup message action and write
	them at shutdown.

	You can get the current application preferences at any time. For instance,
	the constrain angle used by the Illustrator tools is an application preference.

	Note: The Illustrator preferences file is read once when the application is
	first launched and written once when the application quits. The Preference
	suite functions interact with the preferences stored in memory, not with the
	preferences file on disk.

	To get an application preference, use nil for the prefix. Use the paths
	derived from the preference file without the leading '/' to make the suffix.
	There is no direct way to determine the data type of an application preference,
	however, you should be able to determine the data type based on your
	knowledge of Illustrator and the appearance of the value in the preference
	file. For example:

	The following is from the preference file:

@code
	/constrain {
		/sin 0.0
		/cos 16.0
		/angle 0.0
	}
@endcode

	The values in this example indicate AIReal values. To retrieve the value of
	sin:

@code
	AIReal r;
	error = sPref->GetRealPreference( nil, "constrain/sin", &r );
@endcode

	The following is from the preference file:

@code
	/snapToPoint 1
@endcode

	This option is displayed as a checkbox in the General Preferences dialog,
	which suggests it is a boolean value. To get the snap to point setting:

@code
	AIBoolean snap;
	error = sPref->GetBooleanPreference( nil, "snapToPoint", &snap );
@endcode
*/
typedef struct AIPreferenceSuite {

	AIAPI AIErr (*GetBooleanPreference) ( const char *prefix, const char *suffix, AIBoolean *value );
	AIAPI AIErr (*PutBooleanPreference) ( const char *prefix, const char *suffix, AIBoolean value );
	AIAPI AIErr (*GetIntegerPreference) ( const char *prefix, const char *suffix, long *value );
	AIAPI AIErr (*PutIntegerPreference) ( const char *prefix, const char *suffix, long value );
	AIAPI AIErr (*GetRealPreference) ( const char *prefix, const char *suffix, double *value );
	AIAPI AIErr (*PutRealPreference) ( const char *prefix, const char *suffix, double value );
	AIAPI AIErr (*GetStringPreference) ( const char *prefix, const char *suffix, char *value );
	AIAPI AIErr (*PutStringPreference) ( const char *prefix, const char *suffix, const char *value );
	AIAPI AIErr (*GetUnicodeStringPreference) ( const char *prefix, const char *suffix, ai::UnicodeString& value );
	AIAPI AIErr (*PutUnicodeStringPreference) ( const char *prefix, const char *suffix, const ai::UnicodeString& value );
	/** Since the preferences file is an ASCII text file that can be edited avoid using raw
		block data unless absolutely necessary. */
	AIAPI AIErr (*GetBlockPreference) ( const char *prefix, const char *suffix, void *address, long size );
	/** Since the preferences file is an ASCII text file that can be edited avoid using raw
		block data unless absolutely necessary. */
	AIAPI AIErr (*PutBlockPreference) ( const char *prefix, const char *suffix, void *address, long size );

	/** For compatibility with AI7 and earlier. */	
	AIAPI AIErr (*GetFixedPreference) ( const char *prefix, const char *suffix, AIFixed *value );
	/** For compatibility with AI7 and earlier. */	
	AIAPI AIErr (*PutFixedPreference) ( const char *prefix, const char *suffix, AIFixed value );
	/** For compatibility with AI7 and earlier. */	
	AIAPI AIErr (*GetFractPreference) ( const char *prefix, const char *suffix, AIFract *value );
	/** For compatibility with AI7 and earlier. */	
	AIAPI AIErr (*PutFractPreference) ( const char *prefix, const char *suffix, AIFract value );
	AIAPI AIErr (*GetPascalPreference) ( const char *prefix, const char *suffix, unsigned char *value );
	AIAPI AIErr (*PutPascalPreference) ( const char *prefix, const char *suffix, const unsigned char *value );
	AIAPI AIErr (*GetDateTimePreference) ( const char *prefix, const char *suffix, AIPreferenceDateTime *value );
	AIAPI AIErr (*PutDateTimePreference) ( const char *prefix, const char *suffix, AIPreferenceDateTime *value );
	
	AIAPI AIErr (*GetPointPreference) ( const char *prefix, const char *suffix, AIPoint *value );
	AIAPI AIErr (*PutPointPreference) ( const char *prefix, const char *suffix, AIPoint *value );
	AIAPI AIErr (*GetRectPreference) ( const char *prefix, const char *suffix, AIRect *value );
	AIAPI AIErr (*PutRectPreference) ( const char *prefix, const char *suffix, AIRect *value );
	AIAPI AIErr (*GetFileSpecificationPreference) ( const char *prefix, const char *suffix, SPPlatformFileSpecification *value );
	AIAPI AIErr (*PutFileSpecificationPreference) ( const char *prefix, const char *suffix, SPPlatformFileSpecification *value );
	AIAPI AIErr (*GetFilePathSpecificationPreference) ( const char *prefix, const char *suffix, ai::FilePath &value );
	AIAPI AIErr (*PutFilePathSpecificationPreference) ( const char *prefix, const char *suffix, const ai::FilePath &value );

	AIAPI AIErr (*RemovePreference) ( const char *prefix, const char *suffix );

	AIAPI AIErr (*AddPreferencePanel) (SPPluginRef pluginRef, unsigned char *itemText, int dialogID, int options,
			AIPreferenceItemGroupHandle *prefItemGroup, AIMenuItemHandle *menuItem);
	AIAPI AIErr (*GetPreferencePanelItemRef) (AIPreferenceItemGroupHandle prefItemGroup, ADMItemRef *itemRef);
	AIAPI AIErr (*GetPreferencePanelBaseItemIndex) (AIPreferenceItemGroupHandle prefItemGroup, int *baseIndex);
	AIAPI AIErr (*ShowPreferencePanel) (AIPreferenceItemGroupHandle prefItemGroup);

} AIPreferenceSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
