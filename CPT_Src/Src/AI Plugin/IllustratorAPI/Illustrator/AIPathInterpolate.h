#ifndef __AIPathInterp__
#define __AIPathInterp__

/*
 *      Name:		AIPathInterpolate.h
 * 		Purpose:	Adobe Illustrator Path Interpolation Suite.
 *
 * Copyright (c) 1986-2003 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIPathInterpolate.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIPathInterpolateSuite				"AI Path Interpolate Suite"
#define kAIPathInterpolateSuiteVersion2		AIAPI_VERSION(2)
#define kAIPathInterpolateSuiteVersion		kAIPathInterpolateSuiteVersion2
#define kAIPathInterpolateVersion			kAIPathInterpolateSuiteVersion



/*******************************************************************************
 **
 **	Types
 **
 **/


/** An AIFlattenedPath (and its component, AIKnot) represent a path after flattening.
*/
typedef struct {
	AIReal lengthFromLastKnot;
	AIReal fractionalLengthAlongPath;
} AIKnot;


/** An AIFlattenedPath (and its component, AIKnot) represent a path after flattening.
*/
typedef struct {
	AIReal		totalPathLength;
	short		ctKnots;
	AIKnot		knots[1];
} AIFlattenedPath;


/** AIAnchorIdentifier redundantly specifies in two different ways which point
	along a path is meant.

	The first way is as an AIReal in which the integer part is an anchor point
	index (into the AIPathSegments of a path - an AIPathSegment is misnamed, since
	it is really an anchor point with in and out control handles, not a bezier),
	and the fractional part is a t-value along the bezier between that anchor
	point and the next one. This is the same semantics that the "offset" has
	in AITextPath's Get/SetTextPathOffset. In most cases, the fractional part
	will be zero.

	The second way is also an AIReal, but only the fractional part is relevant.
	It is a "t-value" along the entire flattened path length.

    An AIAnchorIdentifier array would be a very inefficient way of describing a
	fully selected path. It is recommended that this only be used for remembering
    which points are selected on a partially selected path, under situations in
    which the same description is to be used after the path has been modified.

	The reason that AIPathInterpolate specifies the anchor point redundantly
	is so that the path object can be edited in such a way as to add, delete,
	or move anchor points, and we will still be able to, in many cases, reasonably
	map the description onto what the user will view as the "same" anchor points.

	A segmentOffset alone is not adequate to maintain identity across additions
	and deletions of points. E.g., if a point is inserted between points N and N+1,
	then the index of every old anchor point after N is incremented.

	On the other hand, a fractionalOffset alone will not stay invariant when
	anchor points or control points (handles) are moved. If point N is stretched
	way out, it and every point after it gets a higher t-value from the path
	beginning, but they are still perceived as the "same" point.

	So we remember the "match" points of an interpolation in dual representations,
	and we also remember how many points each path had when the match points were
	specified.

	If the number of points after an edit is the same as the number of points
	when the match was specified, then the segmentOffset representation is used.
	This will give the expected results for transformations or distortions of
	the whole path, as well as edits which move some or all of the points.

	If the number of points after an edit is different than the number of points
	when the match was specified, then the flattenedOffset representation is
	used. This will give the expected results for edits which add or delete
	points without changing the geometry of the path.
*/
typedef struct {
	AIReal segmentOffset;
	AIReal fractionalOffset;
} AIAnchorIdentifier;


/** Installation data for AIPathInterpolateSuite::InsertInterpolants():

	When InsertInterpolants is first called, the parent, paintOrder and refChild
	are initialized to describe where to install the first interpolated path.

	If refChild is non-null, it must be a child of parent, and the paintOrder
	should be #kPlaceBelow or #kPlaceAbove.

	If refChild is null, paintOrder must be #kPlaceInsideOnTop or #kPlaceInsideOnBottom.

	If clientCallback is non-null, after each path created by InsertInterpolants
	is installed, the clientCallback will be passed the AIArtHandle for the
	new path, and the InstallData, and it will be the clientCallback's responsibility
	to update parent, refChild and paintOrder to describe where the NEXT interpolated
	path should be installed.

	If clientCallback is null, the default behavior is to link each path to the
	previously created one. If endOnBottom is true, each path will be linked as the
	sibling of the previous one, so that the last created path, the one most like
	endPath, is on the bottom; if it is false, the previous path will become the
	sibling of the next one created (so that the last created path, the one most
	like endPath, is on the top.) The endOnBottom field is ignored if clientCallback
	is non-null.

	The clientCallback also gives the client the opportunity to abort the interpolation,
	e.g., due to a user interrupt, or to do other post-processing.
*/ 	
typedef struct
{
	AIArtHandle		parent;
	AIArtHandle		refChild;
	short			paintOrder;
	AIBoolean		endOnBottom;

	void			*clientData;
} AIPathInstallData;

/** Callback function for AIPathInterpolateSuite::InsertInterpolants() */
typedef ASErr (*PostInstallCB)( AIArtHandle art, AIPathInstallData *installData );


// Opaque references to data structures inside app.
typedef struct _t_AIStyleInterpolator* AIStyleInterpolator;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** APIs for interpolating paths and styles.
 */
typedef struct {

	/**	Return the default number of blend steps between startPath and endPath, based
    	primarily on the maximum distance in any color channel between the two paint
    	styles.

		By "primarily" is meant that if the objects are not appropriate for shading,
		either because there is no color channel difference (the objects are painted with
		the same color), or because they are painted with multiple colors (such as gradients
		and patterns), or because they have effects or their outline is too complex for
		shading (such as type objects, groups and symbols), then a number of steps is
		calculated based on the separation between objects instead, so that the space
		between them gets "filled up" with objects that either almost overlap or barely
		overlap.

    	If densityLimit is zero or negative, then it is ignored.

    	If it is positive, then we also calculate the maximum distance from a bounding
    	box edge of one path to the corresponding bounding box edit of the other path,
    	and treat densityLimit as a bound on the number of paths per point that will be
    	placed in that distance. E.g., if the maximum distance between the two blended
    	paths is 4 points, and the density limit is 8, then steps will be at most 32
    	no matter how different the colors are. This is used to keep from getting hundreds
    	of paths all smashed into a tiny distance, when the blend would look the same
    	with a much smaller number of paths. 

	*/
	AIAPI AIErr (*SmoothColorBlendSteps) ( AIArtHandle startPath, AIArtHandle endPath,
										AIReal densityLimit, short *steps );

	/** Return the number of segments of the path which are in kSegmentPointSelected state.
    	(This could logically be in the AIPath suite, but it is mainly used to prepare for
    	PathSelectionToAnchorIDs or PathSelectionToSegNumbers, so it is here.)
	*/
	AIAPI AIErr (*GetPathSelectedSegmentCount) ( AIArtHandle path, short *count );

	/** Translation from a partially selected path to arrays recording which points are
    	selected. (Non-p0 selections are ignored.) These comments describe the anchorId
    	translation, but the SegNumber case is parallel. (The segNumber representation
    	is used to communicate with InsertInterpolants, when the number of segments is
    	known. The AnchorID representation is stored across events that may insert or
    	delete anchor points.)
   
    	Fill in the anchorIDs array (storage to be allocated and freed by the client)
    	with the offsets to any selected points. All fractional parts of the segmentOffsets
    	will be zero.

    	The input count is the amount of space available for storing AIAnchorIdentifiers
    	(the length of the anchorIDs array.)

    	If the count passed to PathSelectionToAnchorIDs is less than the number of selected
    	points in the path, only the first "count" selected points will be translated. This
    	is not considered an error, in fact, if two paths are partially selected but the
    	number of selected points differs, the default behavior is to treat both paths
    	as if they had the lower of the two selected point counts.
 
 		If the count passed to PathSelectionToAnchorIDs is greater than the number of
 		selected points in the path, then the AIAnchorIdentifiers after the end will be
 		filled in with both offsets being 0.
	*/
	AIAPI AIErr (*PathSelectionToAnchorIDs) ( AIArtHandle path, short count, AIAnchorIdentifier *anchorIDs );
	/** See PathSelectionToAnchorIDs() */
	AIAPI AIErr (*PathSelectionToSegNumbers) ( AIArtHandle path, short count, short *selectedPoints );

	AIAPI AIErr (*SelectAnchorPoints) ( AIArtHandle path, short count, short *selectPoints );

	/** Create and install the paths that represent an interpolation from startPath
 		to endPath, with "steps" paths in between.

		Either startPath or endPath may be null, but not both. If one of them is null,
		then the other is blended down to where all points gather at *substituteCenter.
		substituteCenter is ignored if both paths are non-null.
	
		useStyleFromPath is only non-null if one of the two paths is null and the
		other one is part of a compound path (an "extra hole"), in which case it is
		the backmost object in the *other* compound path, and will be used for the
		path style interpolation. (We do it this way instead of passing in an AIPathStyle
		pointer to avoid all the conversion.) Vanishing objects from non-compound paths
		maintain their own path style through all the interpolated steps.

		See comments on typedef of AIPathInstallData for how the paths are linked into
		the artwork.

		If numMatchPairs is positive, then the two segNumber arrays will first be matched up. 
		
		If numMatchPairs is zero or less, then the two matches array pointers are
		ignored. The point matching will start at the top left corner of both paths
		for a closed path, or the leftmost endpoint for an open path, and proceed clockwise.
		If both paths are fully selected, or if either path is not selected at all, then
		the selection state of the anchor points is ignored, and both paths are entirely
		flattened to get the point correspondences. If at least one path is partially selected,
		and the other is fully or partially selected, then AIAnchorIndentifier arrays will
		be internally built corresponding to the partial selections, and matching will
		proceed.
	*/
	AIAPI AIErr (*InsertInterpolants) ( AIArtHandle startPath,
										AIArtHandle endPath,
										AIRealPoint	*substituteCenter,
										AIArtHandle useStyleFromPath,
										short steps,
										short numMatchPairs, short *matches1, short *matches2,
										AIPathInstallData *installData,
										PostInstallCB clientCallback);

	/** This does NOT allocate memory for the segment length array.
	*/
	AIAPI AIErr (*MeasurePathSegments) ( AIArtHandle path,
										short ixStart, short count,
										AIReal *segLengths,
										AIReal flatness );

	// New for AI 9:

	/** Allocate and initialize a style interpolator. The input data are AIArtHandles rather than AIArtStyleHandles
		because it also interpolates object-specific data about how the style is applied to a particular object
		(random seeds, scale factors, gradient matrices, etc.) that is not encapsulated in the AIArtStyleHandle
		(which only carries the generic attributes of the style.)

		It is not necessary to use a StyleInterpolator if you are interpolating paths using InsertInterpolants.
		InsertInterpolants internally interpolates the styles of all paths. It is only necessary to create an
		AIStyleInterpolator if you are creating the intermediate objects yourself, which means they probably
		are not paths. (For example, to apply the interpolated styles to group objects or text objects, or if
		you are interpolating path outlines in a different way than InsertInterpolants does.) Going by suite
		names, this API is thus somewhat misplaced in a suite labelled as "AIPathInterpolate", but since it
		does not take AIArtStyleHandles, it also would seem misplaced in one of the AIArtStyle suites, and it
		is anticipated that the only clients will always be clients of AIPathInterpolate as well. If this suite
		had originally been named AIArtInterpolate, it would fit just fine here. So I am not going to let a name 
		get in my way of putting it where it is most useful.

		It is OK for one of the styleSource handles to be null. In that case, the interpolator always
		applies a copy of the other art style. (This frees the client from the necessity of special-casing
		an interpolation down to nothing.) If both styleSources are null a kBadParameterErr will be
		returned.
	*/
	AIAPI AIErr (*NewStyleInterpolator) ( AIStyleInterpolator* interpolator, AIArtHandle styleSource0, AIArtHandle styleSource1 );

	/** Free memory for a style interpolator.
	*/
	AIAPI AIErr (*DisposeStyleInterpolator) ( AIStyleInterpolator interpolator );

	/** Use a style interpolator to create and apply a new style. If fraction is 0, the style
		applied is that of styleSource0; if fraction is 1, the style applied is that of styleSource1.
		If fraction is a value in between, the art styles of styleSource0 and styleSource1 are
		interpolated at that fraction. If the art styles are incomparable, the style applied is
		a duplicate of that attached to styleSource0.
	*/
	AIAPI AIErr (*ApplyInterpolatedStyle) ( AIStyleInterpolator interpolator, AIArtHandle targetArt, AIReal fraction );

	/** Similar to NewStyleInterpolator(), except that it takes style definitions.
	*/
	AIAPI AIErr (*NewArtStyleInterpolator) ( AIStyleInterpolator* interpolator, AIArtStyleHandle style0, AIArtStyleHandle style1 );

	/** Similar to ApplyInterpolatedStyle, except that it returns the interpolated style definition
		directly.
	*/
	AIAPI AIErr (*GetInterpolatedArtStyle) ( AIStyleInterpolator interpolator, AIReal fraction, AIArtStyleHandle* result );

	// New for AI 11:

	/** Reports whether the two objects are appropriate for smooth color blending. They must
		both be either paths or compound paths, be painted with simple solid colors (not gradients,
		patterns, or active styles), and those solid colors must be different. They can have
		transparency, though.
	*/
	AIAPI AIBoolean (*AppropriateForSmoothColor) ( AIArtHandle art1, AIArtHandle art2 );

	/** Like SmoothColorBlendSteps(), but if the objects are NOT appropriate for smooth color
		spacing, so that the steps must be calculated based on separation instead, then the
		specified pathDistance is used to calculate that separation, instead of the object bounds.
		The pathDistance is also used in the densityLimit calculations. This variant is meant
		for blends along non-default spine paths.
	*/
	AIAPI AIErr (*SmoothColorBlendStepsAlongPath) ( AIArtHandle startPath, AIArtHandle endPath,
													AIReal densityLimit, short *steps, AIReal pathDistance );

} AIPathInterpolateSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
