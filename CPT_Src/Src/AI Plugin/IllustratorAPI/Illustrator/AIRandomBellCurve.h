#ifndef __AIRandomBellCurve__
#define __AIRandomBellCurve__

/*
 *        Name:	AIRandomBellCurve.h
 *   $Revision: 4 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Random Number Bell Curve Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIRandomBellCurve.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIRandomBellCurveSuite		"AI Random Bell Curve Suite"
#define kAIRandomBellCurveSuiteVersion	AIAPI_VERSION(2)
#define kAIRandomBellCurveVersion		kAIRandomBellCurveSuiteVersion


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** There are often times when a plug-in wants to add a degree of noise to the
	effect it is implementing. Illustrator provides two suites that aid in doing
	this: the AIRandomSuite and the AIRandomBellCurveSuite. The Random Suite
	gives an even distibution while the Random Bell Curve Suite yields numbers
	on a standard probability curve.
*/
typedef struct {

	/** Returns Returns a random number with normal probability distribution centered on
		zero and with a standard deviation of one.

		The value returned by FixedRndBellCurve will be in the range -1 to +1 about
		70% of the time (see table below) and the distribution of values in this
		range is close to linear. The probability of a value occuring outside this
		range drops off quickly the farther you get from the origin.
	
		<TABLE>
			<TR><TD>Output Range</TD>		<TD>% result in this range</TD>
			<TR><TD>-0.5 to +0.5</TD>		<TD> about 40%</TD>
			<TR><TD>-1.0 to +1.0</TD>		<TD> about 70%</TD>
			<TR><TD>-1.5 to +1.5</TD>		<TD> about 86%</TD>
			<TR><TD>-2.0 to +2.0</TD>		<TD> about 96%</TD>
			<TR><TD>-3.0 to +3.0</TD>		<TD> >99%</TD>
			<TR><TD>-5.0 to +5.0</TD>		<TD> 100%</TD>
			<TR><TD>-infinity to 0.0</TD>	<TD> 50%</TD>
			<TR><TD>0.0 to +infinity</TD>	<TD> 50%</TD>
		</TABLE>

		In theory, this routine could occasionally generate very large positive or
		negative values but in practice, no numbers outside the range -5 to +5 will
		be generated.
	*/
	AIAPI AIFixed (*FixedRndBellCurve) ( void );

	/** Use the value zero for a random seed based on today�s date and time. */
	AIAPI void (*SetRndSeedBellCurve) ( long seed );

	/** Returns the current seed for FixedRndBellCurve�s random number generator.
		These two random loops will generate the same sequence:

@code
	AIReal r;
	short i;
	long seed = sRandomBell->GetRndSeedBellCurve();
	for ( i = 0; i < 10; ++i ) {
		r = sRandomBell->FixedRndBellCurve();
	}
	sRandom->SetRndSeedBellCurve( seed );
	for ( i = 0; i < 10; ++i ) {
		r = sRandomBell->FixedRndBellCurve();
	}
@endcode
	*/
	AIAPI long (*GetRndSeedBellCurve) ( void );

} AIRandomBellCurveSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
