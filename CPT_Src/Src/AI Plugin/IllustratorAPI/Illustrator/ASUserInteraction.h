#ifndef __ASUserInteraction__
#define __ASUserInteraction__

/*
 *        Name:	ASUserInteraction.h
 *   $Revision: 1 $
 *      Author:	 Andy Bachorski
 *        Date:	   
 *     Purpose:	Adobe Standard User Interaction Suite
 *
 * Copyright (c) 2001 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __ASTypes__
#include "ASTypes.h"
#endif

#ifndef __ASPragma__
#include "ASPragma.h"
#endif

#include "SPBasic.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma PRAGMA_ALIGN_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kASUserInteractionSuite				"AS User Interaction Suite"
#define kASUserInteractionSuiteVersion1		1
#define kASUserInteractionSuiteVersion		kASUserInteractionSuiteVersion1
#define kASUserInteractionVersion			kASUserInteractionSuiteVersion

/*******************************************************************************
 **
 **	Error codes
 **
 **/
 
#define kErrUnknowInteractionLevel		'UILv'


/*******************************************************************************
 **
 **	Types
 **
 **/

//	Note:	For Windows clients, the only valid interaction settings are 
//			kASInteractWithNone & kASInteractWithAll.  Interaction levels other 
//			than kASInteractWithNone it will produce the same results as a 
//			setting of kASInteractWithAll.

typedef enum _t_ASInteractionAllowed {
    kASInteractWithNone         = -1,		//	Don't interact
    kASInteractWithSelf         = 0,		//	Interact for self-send messages
    kASInteractWithLocal        = 1,		//	Interact for local & self-send messages (not remote messages)
    kASInteractWithAll          = 2			//	Interact for all messages
} ASInteractionAllowed;


/*******************************************************************************
 **
 **	Suite
 **
 **/

typedef struct {

	//	The allowable level of interaction for the application (server).  
	//	This property is the maximum level of interaction allowed with regard 
	//	to displaying modal dialogs & alerts.  Value is persistent until 
	//	explicitly changed.  
	//
	//	Will be exposed in application's scripting interface as a property of 
	//	the application.  Could be exposed in application preference dialog.
	//
	//	Default value - kASInteractWithAll
	//
	ASAPI ASInteractionAllowed	(*GetInteractionAllowed) ( void );
	ASAPI ASErr					(*SetInteractionAllowed) ( ASInteractionAllowed allowed );
	

	//	Check to see if it is allowable to interact with the user in the current 
	//	application context.  Value returned is determined by the settings of 
	//	InteractionAllowed and by the current script execution context.
	//
	ASAPI ASBoolean 			(*InteractWithUser) ( void );

} ASUserInteractionSuite;

//
// Stack based class for checking the user interaction level. 
//
class ASUserInteractionChecker
{
public:
	ASUserInteractionChecker(SPBasicSuite *spBasic)
	{
		m_SPBasic = spBasic;
		m_SuitePtr = NULL;
		if (m_SPBasic)
		{
			ASErr error = m_SPBasic->AcquireSuite(kASUserInteractionSuite, 
										kASUserInteractionVersion, 
										(const void **)&m_SuitePtr);
			if(error != kNoErr)
			{
				m_SuitePtr = NULL;
			}
		}
	}
	ASBoolean InteractWithUser(void)
	{
		if (m_SuitePtr)
			return m_SuitePtr->InteractWithUser();
		else
			return TRUE;
	}
	~ASUserInteractionChecker()
	{
		if (m_SuitePtr && m_SPBasic)
		{
			m_SPBasic->ReleaseSuite(kASUserInteractionSuite, kASUserInteractionVersion);
			m_SuitePtr = NULL;
		}
	}

private:
	SPBasicSuite *m_SPBasic;
	ASUserInteractionSuite *m_SuitePtr;
};

#pragma PRAGMA_IMPORT_END
#pragma PRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif //	__ASUserInteraction__

