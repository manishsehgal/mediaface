#ifndef __AIXMLElement__
#define __AIXMLElement__

/*
 *        Name:	AIXMLNodeRef.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 10 XML node suite.
 *
 * Copyright (c) 1986-1999 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIEntry__
#include "AIEntry.h"
#endif

#ifndef __AIDict__
#include "AIDictionary.h"
#endif

#ifndef __AIArray__
#include "AIArray.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIXMLElement.h */

/*******************************************************************************
 **
 **	Suite name and version
 **
 **/

#define kAIXMLNodeSuite						"AI XML Node Suite"
#define kAIXMLNodeSuiteVersion3				AIAPI_VERSION(3)
#define kAIXMLNodeSuiteVersion				kAIXMLNodeSuiteVersion3
#define kAIXMLNodeVersion					kAIXMLNodeSuiteVersion

#define kAIXMLDocumentSuite					"AI XML Document Suite"
#define kAIXMLDocumentSuiteVersion2			AIAPI_VERSION(2)
#define kAIXMLDocumentSuiteVersion			kAIXMLDocumentSuiteVersion2
#define kAIXMLDocumentVersion				kAIXMLDocumentSuiteVersion

#define kAIXMLElementSuite					"AI XML Element Suite"
#define kAIXMLElementSuiteVersion2			AIAPI_VERSION(2)
#define kAIXMLElementSuiteVersion			kAIXMLElementSuiteVersion2
#define kAIXMLElementVersion				kAIXMLElementSuiteVersion

#define kAIXMLNodeListSuite					"AI XML Node List Suite"
#define kAIXMLNodeListSuiteVersion1			AIAPI_VERSION(1)
#define kAIXMLNodeListSuiteVersion			kAIXMLNodeListSuiteVersion1
#define kAIXMLNodeListVersion				kAIXMLNodeListSuiteVersion

#define kAIXMLNamedNodeMapSuite				"AI XML Named Node Map Suite"
#define kAIXMLNamedNodeMapSuiteVersion2		AIAPI_VERSION(2)
#define kAIXMLNamedNodeMapSuiteVersion		kAIXMLNamedNodeMapSuiteVersion2
#define kAIXMLNamedNodeMapVersion			kAIXMLNamedNodeMapSuiteVersion

/*******************************************************************************
 **
 **	Constants
 **
 **/

/** @ingroup Errors */
#define kAIXMLIndexSizeErr			'xInd'
/** @ingroup Errors */
#define kAIXMLDOMStringSizeErr		'xDSt'
/** @ingroup Errors */
#define kAIXMLHierarchyRequestErr	'xHer'
/** @ingroup Errors */
#define kAIXMLWrongDocumentErr		'xDoc'
/** @ingroup Errors */
#define kAIXMLInvalidCharacterErr	'xChr'
/** @ingroup Errors */
#define kAIXMLNoDataAllowedErr		'x!dt'
/** @ingroup Errors */
#define kAIXMLNoModifyAllowedErr	'x!mo'
/** @ingroup Errors */
#define kAIXMLNotFoundErr			'x!fd'
/** @ingroup Errors */
#define kAIXMLNotSupportedErr		'x!sp'
/** @ingroup Errors */
#define kAIXMLInUseAttributeErr		'xInU'


/*******************************************************************************
 **
 **	Types
 **
 **/
typedef struct _AIXMLNodeList *AIXMLNodeListRef;
typedef struct _AIXMLNamedNodeMap *AIXMLNamedNodeMapRef;
typedef struct _AIXMLName *AIXMLName;
typedef AIEntryRef AIXMLString; 		// entry of type string.

/** Node type. See #AIXMLNodeTypeValue for possible values. */
typedef long  AIXMLNodeType;
enum AIXMLNodeTypeValue {
	kAIXMLUnknownNode,
	kAIXMLElementNode,
	kAIXMLAttributeNode,
	kAIXMLTextNode,
	kAIXMLCDATASectionNode,
	kAIXMLCommentNode,
	/** not supported */
	kAIXMLEntityReferenceNode,
	/** not supported */
	kAIXMLEntityNode,
	/** not supported */
	kAIXMLProcessingInstructionNode,
	/** not supported */
	kAIXMLDocumentNode,
	/** not supported */
	kAIXMLDocumentTypeNode,
	/** not supported */
	kAIXMLDocumentFragmentNode,
	/** not supported */
	kAIXMLNotationNode
};



/*******************************************************************************
 **
 **	Notifier
 **
 **/

/** @ingroup Notifiers
	Subscribe to this notifier if you keep information in the document metadata
	element that may not always be current.  It will be sent when an operation
	requiring up to date metadata is about to occur.  For instance, it will be
	sent before any file format is called/written.  If you add some function that
	assumes that the meta data is current, you should send this notifier
	(e.g. if someone wrote a metadata browser). */
#define kAIMetadataSyncNotifier			"AI Metadata Sync Notifier"


/*******************************************************************************
 **
 **	Suites
 **
 **/

//  ------ AIXMLNodeSuite -------------------------
/**
	The node suite provides an approximate implementation of the XML Level 1
	DOM interface for nodes. See http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html#ID-1950641247

	The values of "NodeName", "NodeValue", and "Attributes" vary according to 
	the node type as follows: 

	<table>
	<tr><td>				</td><td>NodeName			</td><td>NodeValue			</td><td>Attributes</td></tr>

	<tr><td>Element			</td><td>tag name			</td><td>null				</td><td>NamedNodeMap</td></tr>
	<tr><td>Attr			</td><td>attribute name		</td><td>attribute value	</td><td>null</td></tr>
	<tr><td>Text			</td><td>#text				</td><td>text value			</td><td>null</td></tr>
	<tr><td>CDATASection	</td><td>#cdata-section		</td><td>CDATA contents 	</td><td>null</td></tr>
	<tr><td>Comment			</td><td>#comment			</td><td>comment string		</td><td>null</td></tr>

	<tr><td>EntityReference	</td><td>entity ref name	</td><td>null				</td><td>null</td></tr>
	<tr><td>Entity			</td><td>entity name		</td><td>null				</td><td>null</td></tr>
	<tr><td>P.Instruction	</td><td>target name		</td><td>content			</td><td>null</td></tr>
	<tr><td>Document		</td><td>#document			</td><td>null				</td><td>null</td></tr>
	<tr><td>DocumentType	</td><td>document type name	</td><td>null				</td><td>null</td></tr>
	<tr><td>DocumentFrag	</td><td>#document-fragment	</td><td>null				</td><td>null</td></tr>
	<tr><td>Notation		</td><td>notation name		</td><td>null				</td><td>null</td></tr>
	</table>

	An AIXMLName is an abstract name.  Currently it can be converted to/from a 
	simple c-string.  In the future it will be possible to convert to/from a UTF-16 
	encoded string.
*/
typedef struct {

	// -- nodes -- 

	/** Nodes are reference counted. Call Release once you are done with a node
		to free its memory. */
	AIAPI ASInt32 (*AddRef) (AIXMLNodeRef node);
	/** Nodes are reference counted. Call Release once you are done with a node
		to free its memory. */
	AIAPI ASInt32 (*Release) (AIXMLNodeRef node);
	
	/** Make an exact duplicate of the source node. a deep copy is performed. */
	AIAPI AIErr (*Clone) (AIXMLNodeRef src, AIXMLNodeRef* dst );
	/** Make a copy of the source node replacing the current contents of the node */
	AIAPI AIErr (*Copy) (AIXMLNodeRef node, AIXMLNodeRef src);

	/** Get the type of an node. */
	AIAPI AIErr (*GetNodeType)(AIXMLNodeRef node, AIXMLNodeType *type);
	
	/** Get the name of an node. */
	AIAPI AIErr (*GetNodeName)(AIXMLNodeRef node, AIXMLName *name);
	/** Set the name of an node. */
	AIAPI AIErr (*SetNodeName)(AIXMLNodeRef node, AIXMLName name);
	
	/** Get the value of an node. */
	AIAPI AIErr (*GetNodeValue)(AIXMLNodeRef node, AIXMLString *value);
	/** Set the name of an node. */
	AIAPI AIErr (*SetNodeValue)(AIXMLNodeRef node, AIXMLString value);
		
	/** Get the node list of the child nodes. */
	AIAPI AIErr (*GetChildNodes)(AIXMLNodeRef node, AIXMLNodeListRef *nodes);

	/** Get the attributes associated with the node (in named node list form). */
	AIAPI AIErr (*GetAttributes)(AIXMLNodeRef node, AIXMLNamedNodeMapRef *attributes);

	/** Insert newchild in front of refchild within node. If refchild is NULL then
		insert it at the end. */
	AIAPI AIErr (*InsertBefore)(AIXMLNodeRef node, AIXMLNodeRef newchild, AIXMLNodeRef refchild);
	/** Replace oldchild with newchild. */
	AIAPI AIErr (*ReplaceChild)(AIXMLNodeRef node, AIXMLNodeRef newchild, AIXMLNodeRef oldchild);
	/** Remove oldchild */
	AIAPI AIErr (*RemoveChild)(AIXMLNodeRef node, AIXMLNodeRef oldchild);
	/** Append newchild to the node. */
	AIAPI AIErr (*AppendChild)(AIXMLNodeRef node, AIXMLNodeRef newchild);
	/** True if the node has any children. */
	AIAPI AIErr (*HasChildNodes)(AIXMLNodeRef node, AIBoolean *haschildren);

	// -- names --
	
	/** Convert the string into a "name". */
	AIAPI AIXMLName (*NameFromString) (const char* string);
	/** Convert the "name" into a string. */
	AIAPI const char* (*StringFromName) (AIXMLName name);

	/** Convert the ai::UnicodeString into a "name". */
	AIAPI AIXMLName (*NameFromUnicodeString) (const ai::UnicodeString& string);
	/** Convert the "name" into an ai::UnicodeString. */
	AIAPI AIErr (*UnicodeStringFromName) (AIXMLName name, ai::UnicodeString& string);
	
	// -- private data --
	
	/** Deprecated. Get private data associated with the node. This is not a part
		of the XML DOM specification. */
	AIAPI AIErr (*GetData) (AIXMLNodeRef node, AIDictKey key, AIEntryRef *value);
	/** Deprecated. Set private data associated with the node. This is not a part
		of the XML DOM specification. */
	AIAPI AIErr (*SetData) (AIXMLNodeRef node, AIDictKey key, AIEntryRef value);
	/** Deprecated. Remove private data associated with the node. This is not a part
		of the XML DOM specification. */
	AIAPI AIErr (*RemoveData) (AIXMLNodeRef node, AIDictKey key);

	// -- utilities --

	/** Compare two nodes for equality optionally performing a deep comparison.
		Nodes are equal if they have the same type, name, and value and they
		each have the same attributes with the same values. They are deeply
		equal if furthermore their sequences of child nodes are equal.
		
		When comparing values an attempt is first made to convert them to real
		numbers. If this succedes the numerical values are compared. If it fails
		the string values are compared.
		
		This is not a part of the XML DOM specification. */
	AIAPI AIErr (*Compare) (AIXMLNodeRef node1, AIXMLNodeRef node2, AIBoolean deep, long *result);

} AIXMLNodeSuite;




//  ------ AIXMLDocumentSuite -------------------------
/** The document suite provides an approximate implementation of the XML Level 1
	DOM interface for documents. See http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html#i-Document
 */
typedef struct {

	/** Each Illustrator document may store in its dictionary a document XML element.
		This API returns that element creating it if needed.
		
		This is not a part of the XML DOM specification. */
	AIAPI AIErr (*GetDocumentElement)(AIXMLNodeRef *element);
	/** Each Illustrator document may store in its dictionary a document XML element. This
		may in turn contain a "metadata" element. This API returns that element creating
		it if needed. When an Illustrator document is exported to SVG the "metadata" element
		is written to the SVG file. When reading an SVG file the "metadata" in the SVG becomes
		the document metadata element. Prior to writing the file the #kAIMetadataSyncNotifier
		is sent to ensure that the metadata is up to date. 
		
		This is not a part of the XML DOM specification. */
	AIAPI AIErr (*GetDocumentMetadata)(AIXMLNodeRef *element);

	/** Create an element with the specified name. */
	AIAPI AIErr (*CreateElement)(AIXMLName name, AIXMLNodeRef *element);
	/** Create an attribute with the specified name and value. */
	AIAPI AIErr (*CreateAttribute)(AIXMLName name, AIXMLString value, AIXMLNodeRef *attribute);
	/** Create a text node from the given string. */
	AIAPI AIErr (*CreateTextNode)(AIXMLString string, AIXMLNodeRef *text);
	/** Create a comment node from the given string. */
	AIAPI AIErr (*CreateComment)(AIXMLString string, AIXMLNodeRef *comment);
	/** Create CDATA node from the given string. */
	AIAPI AIErr (*CreateCDATASection)(AIXMLString string, AIXMLNodeRef *cdata);
	
	/** On input the client is responsible for supplying an array of AIXMLNodeRef in matches
		and setting count to a value no greater than the size of that array. The API then fills
		the match array with up to count elements that match the given name and returns the
		actual number of entries in count. The matching elements are determined by a pre-order
		traversal. The special name "*" matches all elements.

		This differs from the XML DOM specification. */
	AIAPI AIErr (*GetElementsByTagName)(AIXMLNodeRef node, AIXMLName name, long *count, AIXMLNodeRef *match);
	
} AIXMLDocumentSuite;



//  ------ AIXMLElementSuite -------------------------
/** The element suite provides an approximate implementation of the XML Level 1
	DOM interface for elements. See http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html#ID-745549614
 */
typedef struct {

	// -- attributes --
	
	/** Get the value of an node's named attribute. */
	AIAPI AIErr (*GetAttribute)(AIXMLNodeRef element, AIXMLName name, AIXMLString *value);
	/** Set the value of an node's named attribute. */
	AIAPI AIErr (*SetAttribute)(AIXMLNodeRef element, AIXMLName name, AIXMLString value);

	/** remove named attribute associated with the node. */
	AIAPI AIErr (*RemoveAttribute)(AIXMLNodeRef element, AIXMLName name);

} AIXMLElementSuite;


//  ------ AIXMLNodeListSuite -------------------------
/** The node list suite provides an approximate implementation of the XML Level 1
	DOM interface for node lists. See http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html#ID-536297177
 */
typedef struct {

	/** Get the number of items in the list. */
	AIAPI AIErr (*GetLength)(AIXMLNodeListRef nodes, long *length);
	/** Get the indexed item in the list. */
	AIAPI AIErr (*GetItem)(AIXMLNodeListRef nodes, long index, AIXMLNodeRef *node);

	// -- utilities --

	/** Swap the node at position1 in list1 with the node at position2 in list2.
	
		This is not a part of the XML DOM specification. */
	AIAPI AIErr (*SwapNodes) (AIXMLNodeListRef list1, AIXMLNodeListRef list2, ASInt32 position1, ASInt32 position2);

} AIXMLNodeListSuite;


//  ------ AIXMLNamedNodeMapSuite -------------------------
/** The named node map suite provides an approximate implementation of the XML Level 1
	DOM interface for named node maps. See http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html#ID-1780488922
 */
typedef struct {

	/** Get the size of the node map */
	AIAPI AIErr (*GetLength)(AIXMLNamedNodeMapRef map, long *length);
	/** Get the indexed item in the node map */
	AIAPI AIErr (*GetItem)(AIXMLNamedNodeMapRef map, long index, AIXMLNodeRef *node);
	/** Get the item with the specified name from the map */
	AIAPI AIErr (*GetNamedItem)(AIXMLNamedNodeMapRef map, AIXMLName name, AIXMLNodeRef *node);
	/** Set the item with the specified name to the map. Replaces it if already present. */
	AIAPI AIErr (*SetNamedItem)(AIXMLNamedNodeMapRef map, AIXMLNodeRef node);
	/** Remove the named item from the map. The item need not exist. */
	AIAPI AIErr (*RemoveNamedItem)(AIXMLNamedNodeMapRef map, AIXMLNodeRef node);

} AIXMLNamedNodeMapSuite;



#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
