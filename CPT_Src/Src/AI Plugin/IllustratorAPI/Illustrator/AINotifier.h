#ifndef __AINotifier__
#define __AINotifier__

/*
 *        Name:	AINotifier.h
 *   $Revision: 5 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 6.0 Notifier Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIPlugin__
#include "AIPlugin.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AINotifier.h */


/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAINotifierSuite		"AI Notifier Suite"
#define kAINotifierSuiteVersion		AIAPI_VERSION(2)
#define kAINotifierVersion			kAINotifierSuiteVersion

/** @ingroup Callers */
#define kCallerAINotify 		"AI Notifier"
/** @ingroup Selectors */
#define kSelectorAINotify		"AI Notify"


/*******************************************************************************
 **
 ** Types
 **
 **/

/** This is a reference to a notifier. It is never dereferenced. */
typedef struct _t_AINotifierOpaque *AINotifierHandle;

/** The contents of a notifier message. */
typedef struct AINotifierMessage {
	SPMessageData d;
	/** The notifier handle returned from AINotifierSuite::AddNotifier(). If more than
		one notifier is installed it can be used to determine the instance that should
		handle the message. */
	AINotifierHandle notifier;
	/** The type variable is a pointer to a C string indicating the type of event for
		which the plug-in is being notified. If the plug-in only installs one notifier,
		this string will be the same passed to the AINotifierSuite::AddNotifier()
		function. If more than one notifier type is installed, it can be used alone or
		in conjunction with the notifier reference to determine the best way to handle
		the call. */
	char *type;
	/** The contents of the notifyData record will depend on the type of notification
		being sent. For instance, notifiers related to plug�in tools receive
		data; the notifyData field is the AIToolHandle to which the notifier
		message refers. */
	void *notifyData;
} AINotifierMessage;


/*******************************************************************************
 **
 **	Suite Record
 **
 **/

/**
	Notifiers are used by a plug-in to have Illustrator inform it of certain defined
	events. The functions in this suite are used to add requests to receive notifications,
	turn them on and off, and find out which plug-ins are listening to notifications.

	Notifiers can be used by themselves as a background process or in conjunction
	with other plug-in types, such as a menu or window, as a means of learning when an
	update is needed. The actual notifier type definitions are not a part of this suite,
	but are found in the suites to which they are related. For instance, the AIArtSuite
	defines the #kAIArtSelectionChangedNotifier and #kAIArtPropertiesChangedNotifier.

	Notifications are sent to plug-ins by sending a message to their main entry point.
	The message has caller #kCallerAINotify and selector #kSelectorAINotify. The
	message data is defined by AINotifierMessage.
	
	Notifications are sometimes sent during an idle loop, so a plug-in should not rely
	on receiving them synchronously with a document state change. Also, some notifications
	are sent when something may have changed; they do not guarantee that something has
	changed.
  */
typedef struct {

	/** Use this function at startup to register interest in notifications. As with
		adding all plug-in type instances, the AddNotifier call passes to Illustrator a
		reference to the plug-in itself, self, and a string used internally for
		identification, name. The notifier type is a pointer to a string identifying the
		type of notification to be sent. The returned notifier value should be stored if
		more than one notifier is installed. A notifier reference will be passed back
		when the notifier is triggered and the plug-in can compare this value to the
		stored references to determine which notifier to process. */
	AIAPI AIErr (*AddNotifier) ( SPPluginRef self, const char *name, const char *type,
				AINotifierHandle *notifier );

	/** Returns the a pointer to the name of the notifier. This is the name value
		originally passed to the AddNotifier call. It should not be modified. */
	AIAPI AIErr (*GetNotifierName) ( AINotifierHandle notifier, char **name );
	/** Returns a pointer to the type string of the notifier. This is the type value
		originally passed to the AddNotifier call. It should not be modified. */
	AIAPI AIErr (*GetNotifierType) ( AINotifierHandle notifier, char **type );
	/** Returns whether a notifier is active (wants to be sent events) or inactive
		(doesn�t want events). */
	AIAPI AIErr (*GetNotifierActive) ( AINotifierHandle notifier, AIBoolean *active );
	/** Use this to turn a notifier on (wants to receive notifications) and off (doesn�t
		want to receive notifications). */
	AIAPI AIErr (*SetNotifierActive) ( AINotifierHandle notifier, AIBoolean active );
	/** This call will return a reference to the plug-in that installed the notifier. The
		AIPluginHandle can then be used with the Plug-in Suite functions. */
	AIAPI AIErr (*GetNotifierPlugin) ( AINotifierHandle notifier, SPPluginRef *plugin );

	/** Return a count of the number of registerations of interest in receiving
		notifications. */
	AIAPI AIErr (*CountNotifiers) ( long *count );
	/** Return the nth registeration of interest in receiving notifications. */
	AIAPI AIErr (*GetNthNotifier) ( long n, AINotifierHandle *notifier );

	/** Broadcast a notification to all the plug-ins that have subscribed to the
		specified type (see AddNotifer). You are free to use this mechanism for inter
		plug�in communication. */
	AIAPI AIErr (*Notify) ( const char *type, void *notifyData );

} AINotifierSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
