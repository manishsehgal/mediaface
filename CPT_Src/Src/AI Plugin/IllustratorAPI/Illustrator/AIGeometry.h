/**

	AIGeometry.h
	Copyright (c) 1996 Adobe Systems Incorporated.

	Adobe Illustrator 7.0 Geometry Suite.

 **/

#ifndef __AIGeometry__
#define __AIGeometry__


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AIArt.h"
#include "AIPathStyle.h"
#include "AIRaster.h"


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIGeometry.h */

/*******************************************************************************
 **
 ** Constants
 **
 **/

#define kAIGeometrySuite		"AI Geometry Suite"
#define kAIGeometryVersion		AIAPI_VERSION(3)
#define kAIGeometrySuiteVersion	kAIGeometryVersion


/** @ingroup Errors */
#define kCancelledErr			'CAN!'
/** @ingroup Errors */
#define kFormatErr				'FRM!'


/** Organization types*/
typedef enum {

	kAIGeometryOrganizeNull = 0,
	kAIGeometryOrganizePath,
	kAIGeometryOrganizeGroup,
	kAIGeometryOrganizeCompound,
	kAIGeometryOrganizePlaced,
	kAIGeometryOrganizeRaster,
	/** As of AI11 the text organization callbacks are called for each text frame. */
	kAIGeometryOrganizeText,
	/** As of AI11 this is called if the text frame has a confining path. This is the
		case if it is area text or text on a path. */
	kAIGeometryOrganizeTextPath,
	/** This callback is not invoked by AI11. */
	kAIGeometryOrganizeTextLine,
	kAIGeometryOrganizeClipGroup,
	kAIGeometryOrganizeMask,
	kAIGeometryOrganizeMesh,
	kAIGeometryOrganizeSymbol,
	/** New in AI11. Indicates that foreign objects should be enumerated. */
	kAIGeometryOrganizeForeignObject,
	/** New in AI11. Indicates that foreign objects should be enumerated. */
	kAIGeometryOrganizeLegacyText,

	kAIGeometryOrganizeDummy = 0xFFFFFFFF

} AIGeometryOrganizationType;


/*******************************************************************************
 **
 **	Types
 **
 **/

typedef void *AIGeometryUserData;

typedef short (*AIGeometryBeginProc)(AIGeometryUserData userData, AIArtHandle object);
typedef short (*AIGeometryEndProc)(AIGeometryUserData userData, AIArtHandle object);

typedef short (*AIGeometryMoveToProc)(AIGeometryUserData userData, AIRealPoint *p);
typedef short (*AIGeometryLineToProc)(AIGeometryUserData userData, AIRealPoint *p);
typedef short (*AIGeometryCurveToProc)(AIGeometryUserData userData, AIRealPoint *p1, AIRealPoint *p2, AIRealPoint *p3);
typedef short (*AIGeometryClosePathProc)(AIGeometryUserData userData);

typedef short (*AIGeometryFillProc)(AIGeometryUserData userData);
typedef short (*AIGeometryStrokeProc)(AIGeometryUserData userData);
typedef short (*AIGeometryClipProc)(AIGeometryUserData userData);
typedef short (*AIGeometryImageProc)(AIGeometryUserData userData, AIArtHandle raster);
typedef short (*AIGeometryImageMaskProc)(AIGeometryUserData userData, AIArtHandle raster);

typedef short (*AIGeometrySetGrayProc)(AIGeometryUserData userData, AIReal gray);
typedef short (*AIGeometrySetCMYKColorProc)(AIGeometryUserData userData, AIReal cyan, AIReal magenta, AIReal yellow, AIReal black);
typedef short (*AIGeometrySetRGBColorProc)(AIGeometryUserData userData, AIReal red, AIReal green, AIReal blue);
typedef short (*AIGeometrySetAlphaProc)(AIGeometryUserData userData, AIReal alpha);
typedef short (*AIGeometrySetCustomColorProc)(AIGeometryUserData userData, AICustomColorHandle customColor, AIReal tint);
typedef short (*AIGeometrySetGradientColorProc)(AIGeometryUserData userData, 
												AIGradientHandle gradient, 
												AIRealPoint *gradientOrigin, 
												AIReal gradientAngle, 
												AIReal gradientLength, 
												AIReal hiliteAngle, 
												AIReal hiliteLength, 
												AIRealMatrix *matrix);
typedef short (*AIGeometrySetPatternColorProc)(AIGeometryUserData userData, 
											   AIPatternHandle pattern, 
											   AIReal shiftDistance, 
											   AIReal shiftAngle, 
											   AIRealPoint *scale, 
											   AIReal rotateAngle, 
											   AIBoolean reflect, 
											   AIReal reflectAngle, 
											   AIReal shearAngle, 
											   AIReal shearAxis, 
											   AIRealMatrix *matrix);

typedef short (*AIGeometryImageCallback)(AIGeometryUserData userData, 
										 AIArtHandle raster, 
										 AISlice *artSlice, 
										 AITile *workTile, 
										 AISlice *workSlice);

typedef short (*AIGeometrySetLineWidthProc)(AIGeometryUserData userData, AIReal lineWidth);
typedef short (*AIGeometrySetLineCapProc)(AIGeometryUserData userData, AILineCap lineCap);
typedef short (*AIGeometrySetLineJoinProc)(AIGeometryUserData userData, AILineJoin lineJoin);
typedef short (*AIGeometrySetMiterLimitProc)(AIGeometryUserData userData, AIReal miterLimit);
typedef short (*AIGeometrySetDashProc)(AIGeometryUserData userData, AIReal offset, short length, AIReal array[]);


/** The geometry organization procs are passed as a pointer to a list of organization
	types with associated begin/end procs. The list is terminated by an entry with
	an organization type of 0. */
typedef struct {

	/** the organization type */
	AIGeometryOrganizationType type;
	/** called before enumerating an object of this type */
	AIGeometryBeginProc begin;
	/** called after enumerating an object of this type */
	AIGeometryEndProc end;

} AIGeometryOrganizationProcs;

typedef struct {

	AIGeometryMoveToProc moveTo;
	AIGeometryLineToProc lineTo;
	AIGeometryCurveToProc curveTo;
	AIGeometryClosePathProc closePath;

} AIGeometryConstructionProcs;

typedef struct {

	AIGeometryFillProc fill;
	AIGeometryFillProc eofill;
	AIGeometryStrokeProc stroke;
	AIGeometryClipProc clip;
	AIGeometryClipProc eoclip; /* ? */
	AIGeometryImageProc image;
	AIGeometryImageMaskProc imageMask;

} AIGeometryPaintingProcs;

typedef struct {

	AIGeometrySetGrayProc setWhiteColor;
	AIGeometrySetGrayProc setBlackColor;
	AIGeometrySetGrayProc setGrayColor;
	AIGeometrySetCMYKColorProc setCMYKColor;
	AIGeometrySetRGBColorProc setRGBColor; /* unimplemented */
	AIGeometrySetAlphaProc setAlpha; /* unimplemented */
	AIGeometrySetCustomColorProc setCustomColor;
	AIGeometrySetGradientColorProc setGradientColor;
	AIGeometrySetPatternColorProc setPatternColor;
	
	AIGeometrySetLineWidthProc setLineWidth;
	AIGeometrySetLineCapProc setLineCap;
	AIGeometrySetLineJoinProc setLineJoin;
	AIGeometrySetMiterLimitProc setMiterLimit;
	AIGeometrySetDashProc setDash;

} AIGeometryStateProcs;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** The AIGeometrySuite provides APIs for enumerating the geometry and properties
	of artwork to a set of callback functions. */
typedef struct {

	/** Iterate subtree rooted at the input art object supplying information to the
		callback procedures. The organizationProcs receive information about the
		organization of the artwork into objects. The constructionProcs receive a
		geometric description of each leaf art object. The paintingProcs receive information
		about how each leaf object is painted. The stateProcs receive additional
		parameters to the painting operations. A client need only supply those
		callbacks it is interested in. All callbacks return a short.  If zero is
		returned, processing continues; if nonzero, the current artwork element being
		processed is aborted. Processing continues at the next artwork element. This
		can be used to prune traversal of the tree. */
	ASAPI AIErr (*GeometryIterate) ( AIArtHandle art,
									 AIGeometryOrganizationProcs *organizationProcs, 
									 AIGeometryConstructionProcs *constructionProcs, 
									 AIGeometryPaintingProcs *paintingProcs, 
									 AIGeometryStateProcs *stateProcs, 
									 AIGeometryUserData userData );

} AIGeometrySuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
