#ifndef __AIRasterize__
#define __AIRasterize__

/*
 *        Name:	AIRasterize.h
 *   $Revision: 8 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 11.0 Rasterize Suite.
 *
 * Copyright (c) 1986-2003 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIArt__
#include "AIArt.h"
#endif

#ifndef __AIArtSet__
#include "AIArtSet.h"
#endif

#ifndef __AIColorConversion__
#include "AIColorConversion.h"
#endif

#ifndef __AIFixedMath__
#include "AIFixedMath.h"
#endif

#ifndef __AIRaster__
#include "AIRaster.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIRasterize.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIRasterizeSuite				"AI Rasterize Suite"
#define kAIRasterizeSuiteVersion		AIAPI_VERSION(7)


/**
	The default anti-aliasing factor when rasterizing with antialiasing.
 */
#define kAIRasterizeDefaultAntialiasing	2

/**
	@name Dictionary keys for live effect rasterization

	These keys correspond to the parameter dictionary potentially stored
	inside the document dictionary. These parameters dictate what the default settings
	will be whenever an object needs to be automatically rasterized as part of a 
	live effect.

	@{
*/
/** Key for parameter dictionary stored in document dictionary. */
#define kAIAutoRasterSettingsKey		"AI Auto Rasterize"
/** IntegerEntry ( enum AIRasterizeType ) */
#define kDefaultRasterizeType			"colr"
/** IntegerEntry */
#define kDefaultRasterizeDPI			"dpi."
/** BooleanEntry */
#define kDefaultRasterizeAlias			"alis"
/** BooleanEntry -- Note: This value may be undefined.  Clients should consider
	an undefined value as false. */
#define kDefaultRasterizePreserveSpot	"spot"
/** BooleanEntry */
#define kDefaultRasterizeMask			"mask"
/** RealEntry */
#define kDefaultRasterizePadding		"padd"
/**
	@}
*/

/**
	If the document does not have the auto-rasterize settings specified in the
	document dictionary, then plugin effects should use the values stored in the
	application preferences under this key.
*/
#define kRasterizePrefPrefix			"Rasterize/Defaults"


/**
	kAIRasterizeTooWideErr is returned when attempting to rasterize artwork
	with settings that exceed the maximum width possible. Note that the
	maximum width is affected both by the antialiasing factor and the color
	model of the target raster. This is due to the fact that there is a limit
	on the maximum byte width of the image buffer that the artwork is drawn
	into.
*/
#define kAIRasterizeTooWideErr			'R2Wd'


/*******************************************************************************
 **
 **	Types
 **
 **/

/**
	Specifies the type of raster desired and whether it has opacity.
*/
enum AIRasterizeType {
	/** RGB no alpha */
	kRasterizeRGB, 
	/** CMYK no alpha */
	kRasterizeCMYK, 
	/** Grayscale no alpha */
	kRasterizeGrayscale, 
	/** opaque bitmap */
	kRasterizeBitmap,
	/** RGB with alpha */
	kRasterizeARGB, 
	/** CMYK with alpha */
	kRasterizeACMYK, 
	/** Grayscale with alpha */
	kRasterizeAGrayscale,
	/** bitmap with transparent 0-pixels */
	kRasterizeABitmap,
	/** Separation no alpha */
	kRasterizeSeparation,
	/** Separation with alpha */
	kRasterizeASeparation
};

/**
	Options for how the rasterization is done. One or more options can
	be specified by adding them together.
*/
enum AIRasterizeOptions {
	kRasterizeOptionsNone = 0,
	/** normally rasterize ignores layers in the input set */
	kRasterizeOptionsDoLayers = 1,
	/** rasterize against a black background instead of white */
	kRasterizeOptionsAgainstBlack = 2,		
	/** don't align 72 dpi images to the pixel grid */
	kRasterizeOptionsDontAlign = 4,
	/** convert type to outlines before rasterizing it */
	kRasterizeOptionsOutlineText = 8,
	/** don't supersample; preserve type hinting */
	kRasterizeOptionsHinted = 16,	
	/** use the Document Raster Effects Settings resolution */
	kRasterizeOptionsUseEffectsRes = 32,
	/** use a minimum of 5 tiles when rasterizing with antialias on (useful for better user feedback on progress bars) */
	kRasterizeOptionsUseMinTiles = 64,
	/** matte transparency with CMYK white */
	kRasterizeOptionsCMYKWhiteMatting = 128,
	/** rasterize to spot color raster when a single separation channel is sufficient */
	kRasterizeOptionsSpotColorRasterOk = 256
};

/**
	The rasterization settings are collected into a single structure
	describing the type of raster desired, resolution, amount of
	anti-aliasing and other options.
*/
struct AIRasterizeSettings {
	AIRasterizeSettings () :
		type(kRasterizeGrayscale),
		resolution(0),
		antialiasing(1),
		options(kRasterizeOptionsNone),
		ccoptions(AIColorConvertOptions::kDefault),
		preserveSpotColors(false)
	{
	}
	AIRasterizeSettings (const AIRasterizeSettings& src) :
		type(src.type),
		resolution(src.resolution),
		antialiasing(src.antialiasing),
		options(src.options),
		ccoptions(src.ccoptions),
		preserveSpotColors(src.preserveSpotColors)
	{
	}
	AIRasterizeSettings& operator= (const AIRasterizeSettings& src)
	{
		type = src.type;
		resolution = src.resolution;
		antialiasing = src.antialiasing;
		options = src.options;
		ccoptions = src.ccoptions;
		preserveSpotColors = src.preserveSpotColors;
		return *this;
	}

	/** color model and bit depth of raster to be produced */
	AIRasterizeType type;
	/** 0 or 72 provides a resolution of 72 dpi */
	AIReal resolution;
	/** < 2 for none, 2+ for antialiasing. artwork is supersampled at this factor */
 	short antialiasing;
	/** additional options for how rasterization is performed */
	AIRasterizeOptions options;
	/** the color conversion options */
	AIColorConvertOptions ccoptions;
	/** preserve spot colors when possible */
	AIBoolean preserveSpotColors;
};

/**
	Describes the image resampling method to be used by the resampling
	API.
*/
enum AIResamplingType {

	/** Default (anti-aliasing off). Fast, lowest quality */
	kSubsample = 0,
	/** Averaging sample (anti-aliasing on). Slower, better quality */
	kDownsample,
	/** Weighted average sample. Slowest, best quality */
	kBicubicDownsample

};

/**
	Progress callback procedure for rasterization. This procedure is supplied
	by the client. It is called with the two values representing how far
	rasterization has progressed and the value that will be reached on completion.
	The procedure should return false to cancel rasterization and true to
	continue.
*/
typedef AIAPI AIBoolean (*AIRasterizeProgressProc)(long current, long total);


/*******************************************************************************
 **
 **	Suite
 **
 **/


/**
	The rasterize suite provides facilities for rasterizing collections
	of art objects creating a new raster art object as a result.
 */
typedef struct {

/**
	ComputeArtBounds()
	
	This returns the art bounds for the entire artSet.
	If honorCropBox is true, and there is a crop box, then it will 
	return only what's inside the Illustrator crop box (which is 
	created only if Cropmarks->Make was previously done.)
*/
	AIAPI AIErr (*ComputeArtBounds) ( AIArtSet artSet, 
									  AIRealRect *artBounds, 
									  AIBoolean honorCropBox );

/**
	Rasterize()
	
	This creates a raster object (type kRasterArt) from the art contained
	in artSet. You must pass in the artBounds which you can get from 
	calling ComputeArtBounds(). paintOrder and prepArt determine where
	the new kRasterArt object will be created. A pointer to the AIArtHandle
	is returned as raster.
*/
	AIAPI AIErr (*Rasterize) ( AIArtSet artSet, 
							   AIRasterizeSettings *settings, 
							   AIRealRect *artBounds, 
							   short paintOrder, 
							   AIArtHandle prepArt, 
							   AIArtHandle *raster,
							   AIRasterizeProgressProc progressProc );

/**
	CreateMask()
	
	This will create a clip group containing:
	   1. A unionized version of whatever was in artSet (this is the clipArt)
	   2. Whatever kRasterArt you pass as rasterArt
	After the clip group is created, it is placed in another group and 
	this group is returned as maskGroup. You specify paintOrder to indicate
	where the maskGroup should be placed. The default value for paintOrder 
	should be kPlaceAbove.
*/
	AIAPI AIErr (*CreateMask) ( AIArtSet artSet, 
								short paintOrder, 
								AIArtHandle rasterArt, 
								AIArtHandle *maskGroup );

/**
	RasterizeWithPadding()
	
	Almost identical to the Rasterize call above, except adds the
	specified padding to resulting raster. The padding parameter is in
	document points.
*/
	AIAPI AIErr (*RasterizeWithPadding) ( AIArtSet artSet, 
										  AIRasterizeSettings *settings, 
										  AIRealRect *artBounds, 
										  short paintOrder, 
										  AIArtHandle prepArt, 
										  AIArtHandle *raster,
										  AIRasterizeProgressProc progressProc,
										  AIReal padding );

/**
	RasterizeDocument()
	
	This creates a raster object (type kRasterArt) for the current
	document. paintOrder and prepArt determine where the new kRasterArt
	object will be created. A pointer to the AIArtHandle is returned
	as raster.
*/
	AIAPI AIErr (*RasterizeDocument) ( AIRasterizeSettings *settings, 
							   short paintOrder, 
							   AIArtHandle prepArt, 
							   AIArtHandle *raster,
							   AIRasterizeProgressProc progressProc,
							   long options );

/**
	ImageResample()
	
	This downsamples a raster object. paintOrder and prepArt determine 
	where the new resampled raster object will be created. Up-sampling 
	is not implemented.(kNotImplementedErr will be returned)
*/

	AIAPI AIErr (*ImageResample) (AIArtHandle rasterArt, 
									const AIResamplingType flag, 
									const AIReal resolution,
									const short paintOrder, 
									AIArtHandle prepArt, 
									AIArtHandle *newRasterArt);

/**
	CheckSpotColorPreservation()
	
	This checks whether the rasterization can preserve the spot color(s) when rasterizing the art set.
*/

	AIAPI AIErr (*CheckSpotColorPreservation) (AIArtSet artSet, 
									AIBoolean *canPreserveSpotColor); 

} AIRasterizeSuite;

#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
