#ifndef __AIArt__
#define __AIArt__

/*
 *        Name:	AIArt.h
 *   $Revision: 25 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 9.0 Artwork Object Suite.
 *
 * Copyright (c) 1986-2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#include "AITypes.h"
#include "AILayer.h"


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIArt.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIArtSuite				"AI Art Suite"
#define kAIArtSuiteVersion11	AIAPI_VERSION(12)
#define kAIArtSuiteVersion		kAIArtSuiteVersion11
#define kAIArtVersion			kAIArtSuiteVersion


/** @ingroup Notifiers
	The kAIArtSelectionChangedNotifier notifier is sent when either a change in the
	selected art objects occurs or an artwork modification such as moving a point on
	a path occurs. In other words EITHER something was selected or deselected or
	targeted or untargeted, OR some aspect of the current selected object(s) changed.
	These two conditions are not discriminated. There is no notifier than means just
	one or the other. In particular, this notifier does NOT mean that the set of object
	handles returned by AIMatchingArtSuite::GetSelectedArt() will be different.
*/
#define kAIArtSelectionChangedNotifier		"AI Art Selection Changed Notifier"
/** @ingroup Notifiers
	The kAIArtPropertiesChangedNotifier notifier is sent when an object attribute such
	as the fill or stroke of an object changes.

	This notifier was originally intended for the use of the AI 5 paint style palette,
	which displayed a combination of what is now on the Swatch, Color, Stroke and
	Gradient palettes. It meant, "either some style-related aspect of the selection
	has changed, or some global colors, patterns or gradients have been added, deleted
	or modified." It was not supposed to be sent if you selected a different object
	that had exactly the same style as the previously selected object, or if you altered
	the shape of a selected object without changing the style. Over the years
	the meaning of this notifier has mutated considerably, so that it now means almost
	exactly the same thing as kArtSelectionChanged, except that it is additionally sent
	if any global object sets change.
*/
#define kAIArtPropertiesChangedNotifier		"AI Art Properties Changed Notifier"

/** The types of art objects

	If new types are added here, also update the AIStyleFilterPreferedInputArtType
	enum in AILiveEffect.h */

enum AIArtType {
	/** The special type kAnyArt is never returned as an art object type, but is used
		as a parameter to the Matching Art suite function GetMatchingArt. */
	kAnyArt = -1,
	/** The type kUnknownArt is reserved for objects that are not supported in the
		plug-in interface. You should anticipate unknown art objects and ignore
		them gracefully. For example graph objects return kUnkownType.

		If a plug-in written for an earlier version of the plug-in API calls GetArt-
		Type with an art object of a type unknown in its version, this function will
		map the art type to either an appropriate type or to kUnknownArt. */
	kUnknownArt = 0,

	kGroupArt,
	kPathArt,
	kCompoundPathArt,

	/** Pre-AI11 text art type. No longer supported but remains as a place holder so
		that the values for other art types remain the same. */
	kTextArtUnsupported,
	/** Pre-AI11 text art type. No longer supported but remains as a place holder so
		that the values for other art types remain the same. */
	kTextPathArtUnsupported,
	/** Pre-AI11 text art type. No longer supported but remains as a place holder so
		that the values for other art types remain the same. */
	kTextRunArtUnsupported,
	
	kPlacedArt,
	/** The special type kMysteryPathArt is never returned as an art object type, it is
		an obsolete parameter to GetMatchingArt. It used to match paths inside text
		objects without matching the text objects themselves. In AI11 and later the
		kMatchTextPaths flag is used to indicate that text paths should be returned. */
	kMysteryPathArt,
	kRasterArt,

	kPluginArt,
	kMeshArt,

	kTextFrameArt,
	kSymbolArt,
	/** A foreign object is a "black box" containing drawing commands. Construct using
		AIForeignObjectSuite::New(...) rather than AIArtSuite::NewArt(...). See
		AIForeignObjectSuite. */
	kForeignArt,
	/** A text object read from a legacy file (AI10, AI9, AI8 ....) */
	kLegacyTextArt
};


/** Return values from GetArtOrder. (This is the order they would be encountered in
	a GetArtSibling tree traversal. For most objects, "before" means above in the paint
	order, and "after" means behind in the paint order, but there are exceptions. E.g.,
	the text paths of a text object are linked from bottom to top.)
*/
enum AIArtOrder {
	kUnknownOrder = 0,
	kFirstBeforeSecond = 1,
	kSecondAfterFirst = kFirstBeforeSecond,
	kFirstAfterSecond,
	kSecondBeforeFirst = kFirstAfterSecond,
	kFirstInsideSecond,
	kSecondInsideFirst
};


/** whichAttr selectors for Get/SetArtUserAttr, GetMatchingArt, MatchingArtSet, etc.

	The values returned and set are also these attribute bits. E.g., to hide
	an object or ask whether it is hidden, you do a [Set/Get]ArtUserAttr(object,
	kArtHidden, kArtHidden), NOT [Set/Get]ArtUserAttr(object, kArtHidden, true).
	Unfortunately, there is a LOT of client code that passes "true" and "false"
	to SetArtUserAttr or GetMatchingArt as the value for the kArtSelected attribute,
	so it won't be safe for kArtSelected to ever be other than 0x0001. (The clients
	of kArtHidden and kArtLocked have already had to figure out that the attribute
	value paramter is not an AIBoolean, but the clients of kArtSelected, which are
	MUCH more numerous, have been sheltered from this awareness.)

	Notes on kArtSelected vs kArtFullySelected:

	kArtFullySelected is a new attribute added for AI 7. It is mainly needed
	to detect the difference between a text path object which is fully selected,
	and one whose path is selected but whose text isn't. (In both of these
	cases, kArtSelected will be on for both the text path object and the
	path object, but kArtFullySelected will be off for the text path object
	if the text is not selected.) It is also a great accelerator for distinguishing
	other fully selected from partially selected objects, but not absolutely
	necessary for other cases, since they could be detected by scanning the
	object descendants or path segments	looking for one that wasn't selected.
	Text selections, however, are not indicated by object state of the text runs,
	so that method wouldn't work.

	Passing either of these to SetArtUserAttr will fully select the object,
	even if it is a container. The only way to partially select a container
	is to select the descendants you want to have selected, and if you need
	to have the container state synchronized before your plugin returns, then
	call RedrawDocument from the AIDocument suite. (Otherwise the container
	state will be justified by Illustrator after the plugin returns.)

    These attributes thus differ only for Get and Match. kArtSelected is true
	for (and matches) partially selected containers, such as groups with some
	selected components, as well as partially selected paths. kArtFullySelected
	for paths only matches those all of whose segments are selected, and for
	container objects (groups, compound paths, text objects, etc.) only matches
	containers all of whose descendants are selected.

	The "shortcut" routines GetSelectedArt (in AIMatchingArt) and SelectedArtSet
	(in AIArtSet) are equivalent to using kArtSelected as the attribute selector.
*/
enum AIArtUserAttr {
	kArtSelected		= 0x00000001,
	kArtLocked			= 0x00000002,
	kArtHidden			= 0x00000004,
	kArtFullySelected	= 0x00000008,
	/** Valid only for groups and plugin groups. Indicates whether the contents of the object
		are expanded in the layers palette. */
	kArtExpanded		= 0x00000010,
	kArtTargeted		= 0x00000020,
	/** Indicates that the object defines a clip mask. This can only be set on paths, compound paths, and
		text frame objects. This property can only be set on an object if the object is already
		contained within a clip group. */
	kArtIsClipMask		= 0x00001000,
	/** Indicates that text is to wrap around the object. This property cannot be set on an object that is
		part of compound group, it will return kBadParameterErr. kArtIsTextWrap has to be set to the
		ancestor compound group in this case.*/
	kArtIsTextWrap		= 0x00010000,

	/** Meaningful only to GetMatchingArt passing to SetArtUserAttr will cause an error. Only one
		of kArtSelectedTopLevelGroups, kArtSelectedLeaves or kArtSelectedTopLevelWithPaint can
		be passed into GetMatchingArt, and they cannot be combined with anything else. When
		passed to GetMatchingArt causes only fully selected top level objects to be returned
		and not their children. */
	kArtSelectedTopLevelGroups		= 0x00000040,
	/** Meaningful only to GetMatchingArt passing to SetArtUserAttr will cause an error. When passed
		to GetMatchingArt causes only leaf selected objects to be returned and not their containers.
		See also kArtSelectedTopLevelGroups */
	kArtSelectedLeaves				= 0x00000080,
	/** Meaningful only to GetMatchingArt passing to SetArtUserAttr will cause an error. When passed
		to GetMatchingArt causes only top level selected objects that have a stroke or fill to be
		returned. See also kArtSelectedTopLevelGroups */
	kArtSelectedTopLevelWithPaint 	= 0x00000100,	// Top level groups that have a stroke or fill, or leaves
	
	/** Valid only for GetArtUserAttr and GetMatchingArt passing to SetArtUserAttr will cause
		an error. true if the art object has a simple style. */
	kArtHasSimpleStyle	= 0x00000200,
	/** Valid only for GetArtUserAttr and GetMatchingArt passing to SetArtUserAttr will cause
		an error. true if the art object has an active style. */
	kArtHasActiveStyle	= 0x00000400,
	/** Valid only for GetArtUserAttr and GetMatchingArt passing to SetArtUserAttr will cause
		an error. true if the art object is a part of a compound path. */
	kArtPartOfCompound  = 0x00000800,

	// notice that 0x00001000 is in use for kArtIsClipMask 
	
	/** Meaningful only to GetMatchingArt passing to SetArtUserAttr will cause an error. Causes
		art object dictionaries to be included in the search for matching art. */
	kMatchDictionaryArt	= 0x00002000,
	/** Meaningful only to GetMatchingArt passing to SetArtUserAttr will cause an error. Causes
		the contents of graph objects to be included in the search for matching art. */
	kMatchArtInGraphs	= 0x00004000,
	/** Meaningful only to GetMatchingArt passing to SetArtUserAttr will cause an error. Causes
		the result art of plugin groups to be included in the search for matching art. */
	kMatchArtInResultGroups = 0x00008000,

	// notice that 0x00010000 is in use for kArtIsTextWrap 
	
	/** Meaningful only to GetMatchingArt passing to SetArtUserAttr will cause an error. Causes
		the defining paths of text frame objects to be included in the search for matching art. */
	kMatchTextPaths = 0x00020000,

	/** On GetArtUserAttr, reports whether the object has an art style that is pending re-execution.
		On SetArtUserAttr, marks the art style dirty without making any other changes to the art
		or to the style.
	*/
	kArtStyleIsDirty = 0x00040000
};

/** Flags for GetArtTransformBounds and GetArtRotatedBounds:

	kVisibleBounds is the default. Since it is 0 it can't strictly speaking be "set";
	the absence of kControlBounds is interpreted as kVisibleBounds. "Visible" is somewhat
	of a misnomer since it by default includes hidden or unpainted objects as long as they
	aren't guides. Mostly it is just saying that the direction handles are to be ignored
	and only "actual" object bounds considered. But whether those "actual" bounds should
	include strokes and other appearance attributes, or just the raw bezier outlines,
	depends on other options. 

	If kControlBounds is off, kNoStrokeBounds, kNoExtendedBounds and kExcludeUnpaintedObjectBounds
	can be OR'ed in to ignore certain aspects of the visual bounds. "kNoStrokeBounds" means that
	strokes, effects and other appearance attributes that may make an object extend past the bezier
	outlines are ignored. "kNoExtendedBounds" implies kNoStrokeBounds, but additionally
	excludes text characters for text on a path or area text, measuring only the path.
	kExcludeUnpaintedObjectBounds excludes objects with no fill and no stroke.

	kWStrokeDependsOnPreference means to use the setting of the "Use Preview Bounds" preference
	to control whether the strokes and other appearance attributes are included. Either kNoStrokeBounds
	or kWStrokeDependsOnPreference can be set. BUT they cannot both be set (a kBadParameterErr will
	be returned.)

	The kExcludeHiddenObjectBounds and kExcludeGuideBounds flags can be set either when doing
	a kVisibleBounds or kControlBounds.

	GetArtBounds takes no flags, and has the same behavior as GetArtTransformBounds with
	a null transform and kVisibleBounds|kExcludeGuideBounds. Thus, GetArtTransformBounds is often
	called with a null transform just to be able to specify non-default flags.
*/
enum AIArtBoundsOptions {
	kVisibleBounds				= 0x0000,
	kControlBounds				= 0x0001,
	kNoStrokeBounds				= 0x0002,
	kNoExtendedBounds			= 0x0004,
	kWStrokeDependsOnPreference	= 0x0008,
	kExcludeHiddenObjectBounds	= 0x0010,
	kExcludeUnpaintedObjectBounds = 0x0020,
	kExcludeGuideBounds			= 0x0040
};

/** The values for the action parameter to ModifyTargetedArtSet */
enum AIArtTargettingAction {
	/** Replace current set with this set */
	kReplaceAction				= 0x0001,
	/** Append this set to current set */
	kAppendAction				= 0x0002,
	/** Remove this set from the current set */
	kRemoveAction				= 0x0003,
	/** (ignores list parameters) moves targeting up one level */
	kUpAction					= 0x0004,
	/** (ignores list parameters) moves targeting down one level */
	kDownAction					= 0x0005,
	/** (ignores list parameters) moves targeting down to leaves */
	kDownToLeafAction			= 0x0006,
	/** (ignores list parameters) moves targeting up to layers */
	kUpToLayersAction			= 0x0007
};

/** Flags that can be used to specify which attributes are transferred
	by TransferAttributes. These are the attributes that are common to
	all art objects. */
enum AIArtTransferAttrsOptions {
	/** Transfer the unique id. Since an art object's name is stored
		as its unique id this will also transfer the name. */
	kTransferID					= 1L << 0,
	/** Transfer the style of the object. */
	kTransferStyle				= 1L << 1,
	/** Transfer the opacity mask of the object. */
	kTransferOpacityMask		= 1L << 2,
	/** Transfer the text wrap properties--whether or not the object is a text
		wrap object together with the wrap offset and invert properties. */
	kTransferTextWrapProperties	= 1L << 3,
	/** Transfer the compound shape mode used to combine the object geometry with
		others when it is a part of a compound shape. */
	kTransferCompoundShapeMode	= 1L << 4,
	/** Transfer saved selections. This is information that describes the
		selection state for the object when a named selection is chosen from
		the "Select" menu. The information is a map from selection ids to the
		corresponding selection state for the object. The selection state
		information can include information about specific parts of the object
		that are selected. Clearly this information can be nonsense if the
		target object is not an identical copy of the original. At present no
		attempt is made to deal with such nonsense cases. */
	kTransferSavedSelections	= 1L << 5,
	/** Transfer slicing attributes. The slicing attributes define properties
		for exporting the object as a slice forming part of an HTML file. See
		the AISlicingSuite. */
	kTransferSlicingAttributes	= 1L << 6,
	/** Transfer the locked attribute. See #AIArtUserAttr. */
	kTransferArtLocked			= 1L << 7,
	/** Transfer the hidden attribute. See #AIArtUserAttr. */
	kTransferArtHidden			= 1L << 8,
	/** Transfer information imported from and exported to SVG files. This
		includes the SVG interactivity attributes. */
	kTransferSVGProperties		= 1L << 9,
	/** Transfer tags that Live Paint uses to identify paths as being the "same".
		This should only be used if one path or set of paths are intended as being
		replacements for an existing path, similar to distorting it, scissoring it,
		etc. Does nothing if the destination art is not the same object type as the
		source art, so it will be ignored when the destination art is something like
		a group that expands the styled art of a path.
	*/
	kTransferLivePaintPathTags	= 1L << 10,
	/** Transfer all object attributes. Note that this causes all properties
		associated with the object in the current Illustrator version to be
		transferred regardless of the API version a plug-in is built against. */
	kTransferAll				= 0xFFFFFFFF
};

/** Opaque handle to a copy scope. */
typedef struct _t_AICopyScopeOpaque* AICopyScopeHandle;
/** Copy scope kind:  to be used as argument to CreateCopyScope. */
enum AICopyScopeKind {
	/** Scope that groups together a sequence of copy operations that corresponds to
		duplication of the copied objects within a single document. */
	kAICopyScopeForDuplicate = 0,
	/** Scope that groups together a sequence of calls to AIPathStyleSuite::RetargetForCurrentDoc().
		This is needed so that any decisions made about how to handle conflicts between
		global objects in the source and destination documents can apply to the entire set
		of objects being retargeted. For example, when there are global color name conflicts
		and the user chooses to apply their choice for handling one conflict (add or merge)
		to all future conflicts. */
	kAICopyScopeForRetarget,
	kDummyScopeKind = 0xffffffff
};

/** @ingroup Errors */
#define kUnknownArtTypeErr			'ART?'
/** @ingroup Errors */
#define kUnknownPaintOrderTypeErr	'ORD?'
/** @ingroup Errors */
#define kUntouchableArtObjectErr	'ELIO'
/** @ingroup Errors */
#define kTooDeepNestingErr			'NEST'
/** @ingroup Errors */
#define kUntouchableLayerErr  		'NESS'
/** @ingroup Errors */
#define kInvalidArtTypeForDestErr  	'~VAT'	// attempt to insert an object type not allowed in the insertion location


/*******************************************************************************
 **
 **	Suite
 **
 **/


/**
	The functions in the Art Suite are used to access and modify the artwork in
	Illustrator documents. It provides functions to create and delete objects,
	rearrange objects, and get and set information about objects. The Art Suite
	is fundamental to implementing most plug-ins.

	An Illustrator document consists of a collection of art objects referenced by
	an AIArtHandle. AIArtHandles are opaque pointers to art objects in the
	document�s artwork database. All access to the fields of an art object is
	made through various suite functions, passing an AIArtHandle as an argument.

	Art objects can be individual entities, such as paths, blocks of text and
	placed images. An art object can also be a group of objects, such as a collection
	of paths. The art type identifier (see #AIArtType) indicates the type of an art
	object.
*/

typedef struct AIArtSuite {

	/** Create a new art object, specifying the object�s type, paint order, and prep
		object. See #AIArtType for the types of art objects and #AIPaintOrder for the
		possible paint orders. If the new object type is a path then it is typically
		given the path style that would be assigned to a path created by a tool (e.g the
		pen tool). Tool plugins can depend on this. Plugins that are not tools should
		make no assumptions about the style assigned to paths. 
		
		Change for AI 11 - new path art created when the "prep" parameter is styled art
		will get a path style with a black fill, no stroke.  This should be true if and only
		if the art is being created during the execution of an effect.  This prevents effect
		executions from accidentally using the current path style creating their result
		art.  */
	AIAPI AIErr (*NewArt) ( short type, short paintOrder, AIArtHandle prep, AIArtHandle *newArt );
	/** Remove the art object from the art tree. Disposing a group or compound path
		also removes their children. After calling DisposeArt, the AIArtHandle is no
		longer valid. */
	AIAPI AIErr (*DisposeArt) ( AIArtHandle art );
	/** This function moves an object around in the paint order. It is also used to
		put paths inside groups and compound paths, or to move them out. The paintOrder
		and prep parameters describe the new location for thisArt. See #AIPaintOrder
		for the possible paint orders. */
	AIAPI AIErr (*ReorderArt) ( AIArtHandle thisArt, short paintOrder, AIArtHandle prep );
	/** Make a duplicate of the art object. The paintOrder and prep parameters describe the
		location for the new art object. See #AIPaintOrder for the possible paint orders.*/
	AIAPI AIErr (*DuplicateArt) ( AIArtHandle thisArt, short paintOrder, AIArtHandle prep, AIArtHandle *newArt );

	/** This function returns a layer�s first art object, which is the group that
		contains all of the art on the layer. You can then use the GetArtFirstChild,
		GetArtParent, GetArtSibling, and GetArtPriorSibling functions to traverse
		the group returned by this function. Specify nil for the layer argument to
		indicate the current layer. */
	AIAPI AIErr (*GetFirstArtOfLayer) ( AILayerHandle layer, AIArtHandle *art );
	/** Returns the layer that contains the art object if any. Art that is stored
		in a dictionary or an array is not considered to be on a layer even though
		the dictionary may belong to an art object that is on a layer. */
	AIAPI AIErr (*GetLayerOfArt) ( AIArtHandle art, AILayerHandle *layer );
	/** Returns the type of the art object. Before you begin manipulating an object�s
		type specific attributes you need to know what kind of art object it is. For
		example, if the AIArtHandle is a path, you can call the path functions. If it
		is a text object, you can call the text functions. See #AIArtType for more
		information about art types. */
	AIAPI AIErr (*GetArtType) ( AIArtHandle art, short *type );
	/** Gets the user attributes, see #AIArtUserAttr. The whichAttr argument is a mask
		describing the attributes you�re interested in. GetArtUserAttr returns in attr
		the flags in the same positions as the mask bits indicating whether that
		attribute is on or off. */
	AIAPI AIErr (*GetArtUserAttr) ( AIArtHandle art, long whichAttr, long *attr );
	/** Sets the user attributes, see #AIArtUserAttr. The whichAttr argument is a mask
		describing the attributes you wish to set. The attr argument contains flags
		in the same positions as the mask bits indicating whether that attribute
		should be on or off.*/
	AIAPI AIErr (*SetArtUserAttr) ( AIArtHandle art, long whichAttr, long attr );
	/** Get the parent group of an art object. If art is the topmost group of a layer,
		or topmost object stored in a container such as a dictonary or array it will
		return nil. */
	AIAPI AIErr (*GetArtParent) ( AIArtHandle art, AIArtHandle *parent );
	/**	Returns the first child of the object. This function is applicable to container
		objects such as groups, graphs and text frame objects. If art is not a
		container, or is an empty container, the function returns nil. */
	AIAPI AIErr (*GetArtFirstChild) ( AIArtHandle art, AIArtHandle *child );
	/** GetArtSibling can be used with GetArtFirstChild and GetArtParent to look at
		all of the objects in a container object such as a group. If art is the
		last member of the container, sibling will be nil. */
	AIAPI AIErr (*GetArtSibling) ( AIArtHandle art, AIArtHandle *sibling );
	/** GetArtPriorSibling is the opposite of GetArtSibling. GetArtSibling
		returns the next member of a container; GetArtPriorSibling
		returns the previous member. If art is the first, member, GetArtPriorSibling
		sets sibling to nil. */
	AIAPI AIErr (*GetArtPriorSibling) ( AIArtHandle art, AIArtHandle *sibling );

	/** The bounds returned are the geometric bounds of art. This is not the same
		as the imageable bounds. The bounds of a path, for example, do not include
		the stroke width or mitering. */
	AIAPI AIErr (*GetArtBounds) ( AIArtHandle art, AIRealRect *bounds );
	/** SetArtBounds needs to be called before querying (via GetArtBounds) the
		bounds of an object that has been newly created or that has just been modified.
		This is needed because the bounds information is cached and is not normally
		updated until the plugin returns control to the application. */
	AIAPI AIErr (*SetArtBounds) ( AIArtHandle art );

	/** Returns whether or not the center point of the object is shown when it
		is selected. */
	AIAPI AIErr (*GetArtCenterPointVisible) ( AIArtHandle art, AIBoolean *visible );
	/** Sets whether or not the center point of the object is shown when it
		is selected. */
	AIAPI AIErr (*SetArtCenterPointVisible) ( AIArtHandle art, AIBoolean visible );

	/* New with Illustrator 7.0 */

	/** Returns the bounds of the art after the optional transformation matrix transform
		is applied. The type of bounds returned are specified by the flags. see
		#AIArtBoundsOptions for details. */
	AIAPI AIErr (*GetArtTransformBounds) ( AIArtHandle art, AIRealMatrix *transform, long flags, AIRealRect *bounds );
	/** Checks to see if any linked objects (linked images or placed objects)
		contained in the subtree of the input art need updating. Normally a linked
		object needs updating if its file has been modified since the contents were
		last read. If this is the case then the objects are updated. If the force
		parameter is true then the objects are updated regardless of whether they
		have changed. The optional parameter updated is set to true or false to
		indicate if any objects were updated. */
	AIAPI AIErr (*UpdateArtworkLink) ( AIArtHandle art, AIBoolean force, AIBoolean *updated );

	/* New in Illustrator 8.0 */

	/** Tests whether an art handle is valid. If this function returns true then it
		is guaranteed that the art handle identifies an object in the document. It is
		not necessarily the case that if it returns false that the art object does not
		exist in the document. This is because the function tests for validity by
		searching for the object in the tree. Not all possible locations are searched.
		If searchAllLayerLists is true, then this does a search through all layers in
		all layer lists. Otherwise, it only does a search on the current layer list. */
	AIAPI AIBoolean (*ValidArt) ( AIArtHandle art, AIBoolean searchAllLayerLists );
	/** This is the order they would be encountered in a GetArtSibling tree
		traversal. For most objects, "before" means above in the paint order, and
		"after" means behind in the paint order. See #AIArtOrder for the possible
		ordering values. */
	AIAPI AIErr (*GetArtOrder) ( AIArtHandle art1, AIArtHandle art2, short *order );
	/** Iterate over all the artwork in layer, selecting those objects whose note
		matches name. If the name is the empty string then all leaf objects without
		notes are matched. Partial matches can be used if the flag matchWholeWord is set
		to false. */
	AIAPI AIErr (*SelectNamedArtOfLayer) ( AILayerHandle layer, const ai::UnicodeString& name, 
		AIBoolean matchWholeWord, AIBoolean caseSensitive );
	/** Get the bounds of the object after rotation by the angle. The flags specify
		options for what bounds are returned. See #AIArtBoundsOptions for details. */
	AIAPI AIErr (*GetArtRotatedBounds) ( AIArtHandle art, AIReal angle, long flags, AIRealRect *bounds );

	/** Returns true if the art object or some descendent object draws something that
		should be considered a fill. The distinguishing property of a fill is that
		it is drawn behind all other objects when applied to an object that is part
		of a clipping mask. */
	AIAPI AIBoolean (*ArtHasFill) ( AIArtHandle art );
	/** Returns true if the art object or some descendent object draws something that
		should be considered a stroke. The distinguishing property of a stroke is that
		it is drawn in front of all other objects when applied to an object that is part
		of a clipping mask. */
	AIAPI AIBoolean (*ArtHasStroke) ( AIArtHandle art );
	/** Returns true if the two objects contain nothing other than groups, paths and
		compound paths and they have identical geometry. */
	AIAPI AIBoolean (*ArtsHaveEqualPaths) ( AIArtHandle art1, AIArtHandle art2 );
	/** This API is unimplemented in AI11 and later. The implementation has always contained
		bugs. It returns kNotImplementedErr. */
	AIAPI AIErr (*ArtCopyFillStyleIfEqualPaths) ( AIArtHandle dstArt, AIArtHandle srcArt );
	/** This API is unimplemented in AI11 and later. The implementation has always contained
		bugs. It returns kNotImplementedErr. */
	AIAPI AIErr (*ArtCopyStrokeStyleIfEqualPaths) ( AIArtHandle dstArt, AIArtHandle srcArt );

	/* New in Illustrator 9.0 */
	
	/** Get the insertion point. This is the position in the art tree that a drawing
		tool should create new art. It is specified by an art object and a position
		relative to that object. See #AIPaintOrder for the possible positions. The
		parameter "editable" returns whether it is actually valid to create art in
		the returned position. For example, it would not be valid to do so if it were
		on a locked layer. Any of these parameters may be NULL if not needed. */
	AIAPI AIErr (*GetInsertionPoint) ( AIArtHandle *art, short *paintorder, ASBoolean *editable );
	/** Sets the insertion point. This is the position in the art tree that a drawing
		tool should create new art. The new insertion point will be a position relative
		to the specified art object. If the object is a group then it will be
		kPlaceInsideOnTop for other object types it will be kPlaceAbove. If the
		object is in a position where an insertion point cannot be set (for example
		inside a plugin group) the nearest parent defining a valid insertion point is
		used. If no valid insertion point can be determined an error is returned. */
	AIAPI AIErr (*SetInsertionPoint) ( AIArtHandle art );

	/** Get the key object for object alignment. If there is no key object then NULL
		is returned. The key object is the one to which other objects are aligned. It
		is usually the last object clicked on with the select tool. */
	AIAPI AIErr (*GetKeyArt) ( AIArtHandle *art );	 
	/** Clear the key object for object alignment. The key object is the one to which
		other objects are aligned. It is usually the last object clicked on with the
		select tool. */
	AIAPI AIErr (*CancelKeyArt) (void);

	/** Returns the dictionary associated with the art object. Arbitrary data can be
		attached to the object in its dictionary. See the AIDictionarySuite for more
		information about dictionaries. The same dictionary is accessed by the AITag
		suite but that suite is only capable of setting and getting string values. Note
		that dictionaries are reference counted. You must call sAIDictionary->Release()
		when you're done with it. */
	AIAPI AIErr (*GetDictionary) ( AIArtHandle art, struct _AIDictionary** dictionary );

	/** Set the name of the art object. This is the name that appears in the layers
		palette. If name the empty string the art object's name is reset
		to the default for the object type. */
	AIAPI AIErr (*SetArtName) (AIArtHandle art, const ai::UnicodeString& name);

	/** Get the name of the art object. This is the name that appears in the layers
		palette. If there is no user assigned name, a default descriptive name will be
		returned. *isDefaultName will be set to reflect this. isDefaultName
		may be null. */
	AIAPI AIErr (*GetArtName) (AIArtHandle art, ai::UnicodeString& name, ASBoolean *isDefaultName);

	/** True if art is a group and it corresponds to a layer or a sublayer. */
	AIAPI AIErr (*IsArtLayerGroup) (AIArtHandle art, ASBoolean *isLayerGroup);

	/** Release elements of a layer, group, or plugin group to separate layers.
		Plugin groups may be inside styled art, and will be treated as follows: If
		the artstyle consists of only the plugin group, the plugin group will be
		expanded and the original art deleted. If there is more than one plugin group
		or other element in the styled art, the remaining styled art will be left
		intact and 'art' will be left in place.

		If 'build' is true the new layers will be built upon one another such that
		the topmost released layer contains everything in the original art and
		the bottommost released layer contains only the highest object in the
		stacking order. This order was chosen because the Flash exporter takes the
		bottommost layer as the first animation frame and the topmost layer as the
		last frame. */
	AIAPI AIErr (*ReleaseToLayers) (const AIArtHandle art, ASBoolean build);
	
	/** Modify the set of targeted objects in the document according to the action.
		See #AIArtTargettingAction for the possible actions. Some actions require
		a set of objects as an additional parameter. In that case the objects are
		defined by the list and count. */
	AIAPI AIErr (*ModifyTargetedArtSet) (AIArtHandle *list, long count, long action);

	/** Return true if art is part of the styled art of another object */
	AIAPI AIBoolean (*IsArtStyledArt) (AIArtHandle art);

	/** Return true if 'art' adds to clipping. Should only be called on
		descendents of a clipGroup. A return of TRUE indicates 'art' is either an
		appropriate art object with the isClip flag on or a non-clip group with a
		clipping descendent that isn't bounded by another clipGroup. */
	AIAPI AIBoolean (*IsArtClipping) (AIArtHandle art);


	/* New in Illustrator 10.0 */

	/** Transfers attributes from the source art to the destination. This is intended
		to be used when the destination art is to replace the source art in the art
		tree. For example when an art object is converted from one type to another.
		The set of attributes to be transferred is defined by the which parameter. See
		#AIArtTransferAttrsOptions for details. */
	AIAPI AIErr (*TransferAttributes) (AIArtHandle srcart, AIArtHandle dstart, unsigned long which);

	/* New in Illustrator 11.0 */

	/**	Returns the last child of the object. This function is applicable to container
		objects such as groups, graphs and text frame objects. If art is not a
		container, or is an empty container, the function returns nil. */
	AIAPI AIErr (*GetArtLastChild) ( AIArtHandle art, AIArtHandle *child );

	/** Set the properties that affect how text wraps around a text wrap object. Returns
		kBadParameterErr if the object does not have the kArtIsTextWrap attribute set.
		Use Get/SetArtUserAttr to inspect and set the text wrap attribute. The offset is
		a distance in points from the geometric outline of the object that defines an
		offset for wrapping. If invert is false text wraps around the outside of the offset
		object. If it is true then text wraps inside the outline of the offset object. */
	AIAPI AIErr (*SetArtTextWrapProperty) ( AIArtHandle art, AIReal offset, AIBoolean invert );
	/** Get the properties that affect how text wraps around a text wrap object. Returns
		kBadParameterErr if the object does not have the kArtIsTextWrap attribute set.
		See SetArtTextWrapProperty() for more information. */
	AIAPI AIErr (*GetArtTextWrapProperty) ( AIArtHandle art, AIReal *offset, AIBoolean *invert );
	
	/** Creates and instates a copy scope of type "kind". Returns handle
		to the created scope in "scope".  See the ai::CopyScope class for more information. */
	AIAPI AIErr (*CreateCopyScope) (enum AICopyScopeKind kind, AICopyScopeHandle* scope);
	/** De-instates "scope" and destroys it. */
	AIAPI AIErr (*DestroyCopyScope) (AICopyScopeHandle scope);

	/* New in Illustrator 12.0 */

	/** Check to see if it is OK to create or insert an object of artType in the indicated insertion point.
		Returns kNoErr if it is OK, and #kInvalidArtTypeForDestErr if it is not. */ 
	AIAPI AIErr (*InsertionPointBadForArtType) (short paintOrder, AIArtHandle prep, short artType);

	/** Check to see if it is OK to insert the candidateArt object or a duplicate copy of it at the indicated
		insertion point, based on attributes of the insertion context and the candidateArt, without actually
		attempting the insertion. Returns kNoErr if it is OK. Possible non-OK returns are #kTooDeepNestingErr,
		#kInvalidArtTypeForDestErr, and #kUntouchableArtObjectErr. This is mainly intended for use during drag-drop
		mouseover, to give proper cursor feedback, and so that an insertion is not attempted if it is doomed to fail.
	*/ 
	AIAPI AIErr (*PreinsertionFlightCheck) (AIArtHandle candidateArt, short paintOrder, AIArtHandle prep);

	/** Set the note attribute on the given art object.  The note is 
		meant for text entered by the user. To attach private data to an art object plug-ins
		should use the art dictionary interface (see AIArtSuite::GetDictionary()) rather than
		using the note.
		@param art the art object to modify.
		@param inNote the new text for the note.*/
	AIAPI AIErr (*SetNote) (AIArtHandle art, const ai::UnicodeString& inNote);
	/** Get the note attribute text for the given art object. See SetArtNote() for
	    more information.  If the art object has no note, the returned string's contents
		will be empty.
		@param art the art object to retrieve the note from.
		@param outNote the contents of the note will be written to this UnicodeString.*/
	AIAPI AIErr (*GetNote) (AIArtHandle art, ai::UnicodeString& outNote);
	/** Check if an art object has a note attached.
		@param art the art object to examine.
		@return true is returned if there is a note attached. */
	AIAPI AIBoolean (*HasNote) (AIArtHandle art);
	/** Delete the note attached to art if there is one.
		@param art the art object to remove the note from. */
	AIAPI void (*DeleteNote) (AIArtHandle art);

	/** Returns the size of the xmp data associated with the art.  Size zero indicates no xmp data 
	*/
	AIAPI AIErr (*GetArtXMPSize)(AIArtHandle art, long* size);
	/** Fills a pointer to the xml packet associated with the art XMP data. The format is UTF-8
		unicode 
	*/
	AIAPI AIErr (*GetArtXMP)(AIArtHandle art, char *xmp, long size);
	/** Replace any existing XAP data and assign this xml XMP packet to the art. Format must be UTF-8
        unicode. A null xmp pointer or empty one will remove the existing xmp.
	*/
	AIAPI AIErr (*SetArtXMP)(AIArtHandle art, const char *xmp);

 	AIAPI AIErr (*GetPreciseArtTransformBounds) ( AIArtHandle art, AIRealMatrix *transform, long flags, AIDoubleRect *bounds );

	/** Removes the art object from the art tree. Any checks for validity of the input
		are minimized in order to maximize performance. Disposing a group or compound path
		also removes their children. After calling DisposeArt, the AIArtHandle is no
		longer valid. */
	AIAPI AIErr (*UncheckedDisposeArt) ( AIArtHandle art );

} AIArtSuite;

#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __AIArt__
