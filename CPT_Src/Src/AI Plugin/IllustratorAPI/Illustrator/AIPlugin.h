#ifndef __AIPlugin__
#define __AIPlugin__

/*
 *        Name:	AIPlugin.h
 *   $Revision: 10 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 7.0 Standard Plug-in Includes and Definitions
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif


#ifndef __SPAccess__
#include "SPAccess.h"
#endif

#ifndef __SPInterf__
#include "SPInterf.h"
#endif

#ifndef __SPProps__
#include "SPProps.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN


/*******************************************************************************
 **
 ** Constants
 **
 **/
 
#define kAIPluginSuite				"AI Plugin Suite"
#define kAIPluginSuiteVersion		AIAPI_VERSION(3)

/** @ingroup Notifiers
	You will receive the notification after all plug-ins
	have received and porcessed kAIApplicationStartedNotifier.
*/
#define kAIAllPluginStartedNotifier		"AI All Plug-ins Started Notifier"

/** Sent before ADM::HostEnd() gets called and the plugins are sent the application stopped notifier. 
*/
#define kAIPluginStoppingNotifier		"AI All Plug-ins Stopping Notifier"



/** These are the options available to a plug-in. */
enum AIPluginOptions {
	/** The state of the auto selected results option determines what artwork is
		selected when the plug-in finishes running. By default, Illustrator automatically
		selects any artwork a plug-in modifies while running. Any artwork that
		was in a selected state on entry but not touched by a plug-in will be deselected.
		
		You can disable this functionality by unsetting the kPluginWantsResultsAutoSelectedOption
		if you want to handle within your plug-in what artwork is selected and deselected.
		By default, this option is set for Plug-in Filters and cleared for all other plug-in
		types.
	*/
	kPluginWantsResultsAutoSelectedOption	= (1L<<1)
};

/** Note:  The original version of this suite, version 2, has been largely replaced by 
	the SPPluginSuite.  That suite is used, for instance, to access the plug-in list, 
	identify their type and location.  It is a core suite available in other applications.  
	The AIPluginSuite, on the other hand, allows access to the only Illustrator 
	specific aspects of AI7.0 plug-ins, the options.  As in Adobe Illustrator 6.0, 
	there is only one option.

	The CallPlugin() API makes it possible for one plugin to send a message to the main
	entry point of another plugin. An example for how to do this is shown below:

@code
	// somePlugin is the plugin ref for the plugin we want to call.
	SPPluginRef somePlugin;
	// AISomePluginMessage is a plugin message structure. All such structures must
	// have an SPMessageData as their first field.
	AISomePluginMessage message;

	// here would be the code to fill out the message structure

	// now send the message to the plugin
	error = sAIPlugin->SetupPluginData(somePlugin, &message.d);
	if (!error)
		error = sAIPlugin->CallPlugin(somePlugin, kSomePluginSelector, &message );
	if (!error)
		error = sAIPlugin->EmptyPluginData(somePlugin, &message.d);
@endcode
*/
typedef struct {

	/** Get options for the plugin. See #AIPluginOptions. */
	AIAPI AIErr (*GetPluginOptions) ( SPPluginRef plugin, long *options );
	/** Set options for the plugin. See #AIPluginOptions. */
	AIAPI AIErr (*SetPluginOptions) ( SPPluginRef plugin, long options );
	
	/** Set up data for sending a message to a plugin. */
	AIAPI AIErr (*SetupPluginData) ( SPPluginRef plugin, void *data );
	/** Send a message to a plugin. */
	AIAPI AIErr (*CallPlugin) ( SPPluginRef plugin, const char *selector, void *message );
	/** Tear down data for sending a message to a plugin. */
	AIAPI AIErr (*EmptyPluginData) ( SPPluginRef plugin, void *data );
} AIPluginSuite;



#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif
