#ifndef __AICustomColor__
#define __AICustomColor__

/*
 *        Name:	AICustomColor.h
 *   $Revision: 10 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 8.0 Custom Color Fill Suite.
 *
 * Copyright (c) 1986-1997 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __AIColor__
#include "AIColor.h"
#endif

#ifndef __AIColorConversion__
#include "AIColorConversion.h"
#endif

#include "IAIUnicodeString.h"


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AICustomColor.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAICustomColorSuite					"AI Custom Color Suite"
#define kAICustomColorSuiteVersion			AIAPI_VERSION(6)
#define kAICustomColorVersion				kAICustomColorSuiteVersion

/** @ingroup Notifiers
	If a change to a custom color occurs, including adding a new color or
	deleting an existing one, a plug-in can receive this notification.
	There is no data sent with the notifier, so the plug-in must determine what
	change occured using the suite functions. */
#define kAIArtCustomColorChangedNotifier	"AI Art Custom Color Changed Notifier"


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** Custom colors represent both spot colors and global process colors. The
	functions in this suite are used to access custom colors, allowing a plug-in to
	create, delete, and modify them.
	
	Note that the PostScript/PDF language has special meanings for the names
	All, None, Cyan, Magenta, Yellow and Black. These names cannot be used for
	spot colors. */
typedef struct {

	/** Make a new custom color. The color is not added to the swatch list. */
	AIAPI AIErr (*NewCustomColor) ( AICustomColor *color, const ai::UnicodeString& name, AICustomColorHandle *customColor );
	/** Delete a custom color. This will delete it from the swatches palette if present
		and replace any usages by the process equivalent. */
	AIAPI AIErr (*DeleteCustomColor) ( AICustomColorHandle customColor );

	AIAPI AIErr (*GetCustomColor) ( AICustomColorHandle customColor, AICustomColor *color );
	AIAPI AIErr (*SetCustomColor) ( AICustomColorHandle customColor, AICustomColor *color );

	AIAPI AIErr (*GetCustomColorName) ( AICustomColorHandle customColor, ai::UnicodeString& name );
	AIAPI AIErr (*SetCustomColorName) ( AICustomColorHandle customColor, const ai::UnicodeString& name );

	AIAPI AIErr (*CountCustomColors) ( long *count );
	AIAPI AIErr (*GetNthCustomColor) ( long n, AICustomColorHandle *customColor );
	AIAPI AIErr (*GetCustomColorByName) ( const ai::UnicodeString& name,  AICustomColorHandle *customColor );
	/** Generates a unique name for a custom color. On input "name" should be the
		candidate name for the new custom color. On return it will have been modified
		if need be to ensure that it is unique. */
	AIAPI AIErr (*NewCustomColorName) ( ai::UnicodeString& name );
	/** This function is a no-op. An earlier version of Illustrator stored different names
		for custom colors internally from those displayed to the user. */
	AIAPI AIErr (*GetCustomColorDisplayName) ( ai::UnicodeString& name );

	AIAPI AIErr (*GetCurrentRegistrationColor) ( AICustomColorHandle *customColor );
	AIAPI AIErr (*NewRegistrationColor)(AICustomColorHandle *customColor, AIReal cyan /*red*/, 
		AIReal magenta /*green*/,AIReal yellow/*blue*/, AIReal black, AICustomColorTag kind);
	
	/** Checks that the supplied custom color handle represents a valid custom
		color in the current document. */
	AIAPI AIBoolean (*ValidateCustomColor)(AICustomColorHandle customColor);
	/** Returns true if the custom color is a spot color that is used by a linked file.
		If this is the case it cannot be modified. */
	AIAPI AIBoolean (*HasExternalUses)(AICustomColorHandle customColor);
	
	/** Converts a tint of a custom color to its closest non-global approximation.
		If the custom color is a global process color then this is the process
		equivalent. If it is a spot color then it is the process approximation
		of the appearance of the spot color. If it is a registration color then
		it is the process color used to represent the registration color. The
		resulting color will be consistent with the document color model. For
		example, in a CMYK document, Lab and RGB based spot colors get converted
		to CMYK. */
	AIAPI AIErr (*ConvertToNonGlobal)(AICustomColorHandle customColor, AIReal tint, AIColor* color);
	
	/** Converts a tint of a custom color to its best appearance approximation.
		The appearance approximation is a color that approximates the way the
		custom color looks. For global process colors it is just the process
		color determined by the tint. For spot colors it is a color that approximates
		the appearance of the tint of the spot ink. This differs from
		ConvertToNonGlobal() in that the resulting color need not be consistent
		with the document color model. */
	AIAPI AIErr (*GetAppearanceApproximation)(AICustomColorHandle customColor, AIReal tint,
			long* space, AISampleComponent* values);

} AICustomColorSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
