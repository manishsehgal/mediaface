#ifndef __AIPaintStyle__
#define __AIPaintStyle__

/*
 *        Name:	AIPaintStyle.h
 *   $Revision: 17 $
 *      Author:	Dave Lazarony
 *        Date:	6/17/96
 *     Purpose:	AI Paint Style Suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifndef __ADMItem__
#include "ADMItem.h"
#endif

#ifndef __ASTypes__
#include "ASTypes.h"
#endif

#ifndef __AIColor__
#include "AIColor.h"
#endif

#ifndef __AIPathStyle__
#include "AIPathStyle.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIPaintStyle.h */


/*******************************************************************************
 **
 **	Constants
 **
 **/

// AIPaintStyle Suite
#define kAIPaintStyleSuite			"AI Paint Style Suite"
#define kAIPaintStyleSuiteVersion6	AIAPI_VERSION(6)
#define kAIPaintStyleSuiteVersion	kAIPaintStyleSuiteVersion6


/** @ingroup Notifiers
	When the paint style plugin receives this notifier it updates all it's proxies. If the data for
	the notifier is not null then it should point to the AIUpdatePathStyleNotifyData;  The plugin
	uses the pathStyle and  pathStyleMap to update the proxies with.  This is currently sent by
	the eyedropper. */
#define kAIUpdatePathStyleNotifier					"AI Update PathStyle Notifier"
/** @ingroup Notifiers */
#define kAIPaintStyleFillStrokeChangedNotifier		"AI Paint Style Fill Stroke Changed Notifier"
/** @ingroup Notifiers */
#define kAIPaintStyleGradientStopChangedNotifier	"AI Paint Style Gradient Stop Changed Notifier"

/** @ingroup Selectors
	Sets the stroke width.  Used by the Control Palette.  Use the related data type AIStrokeWidthMessage
	to indicate the new stroke width.  If there is no stroke currently applied, and a non-zero stroke width
	is set through this selector, then the previously applied stroke color is restored. */
#define kAIPaintStyleSetStrokeWidthSelector			"AI Paint Style Set Stroke Width Selector"
typedef struct {	
	SPMessageData d;
	float newStrokeWidth;
} AIStrokeWidthMessage;

// AIPaintStyle Indicator Type for ADM.
#define kAIPaintStyleIndicatorType					"AI Paint Style Indicator Type"
#define kAIPaintStyleIndicatorSmallType				"AI Paint Style Indicator Small Type"

// AIPaintStyle Color Editor Type for ADM.
#define kAIPaintStyleColorEditorType				"AI Paint Style Color Editor Type"

// New for AI10: we do not allow dashes or dash gaps with lengths strictly between 0 and kMinStrokeDashLength.
#define kMinStrokeDashLength		0.01


/*******************************************************************************
 **
 **	Types
 **
 **/

/** Data for the #kAIUpdatePathStyleNotifier */
typedef struct
{
	AIPathStyle	pathStyle;
	AIPathStyleMap pathStyleMap;
} AIUpdatePathStyleNotifyData;


/** Color spaces that can be displayed by the color palette for editing colors. */
typedef enum
{
	kUnknownSpace = 0,
	kGrayscaleSpace,
	kRGBSpace,
	kHSVSpace,			
	kCMYKSpace,			
	kWebSpace,
	kTintSpace,
	kLabSpace
} ColorSpace;

/** Defines options that modify the behaviour of a color editor control. See
	the AIPaintStyleSuite for general information on color editors. */
typedef struct
{
	/** If true then colors are converted to the document color space for
		display in the color indicator. This gamut limits them to the
		document color space. The default value is true. */
	AIBoolean showSampleInDocCs;
	/** If true then Lab is allowed as a color model in the color editor.
		The default value is false. */
	AIBoolean allowLabColorModel;
} AIColorEditorOptions;

/** Possible color spaces for specifying colors to a color editor */
typedef enum
{
	kAIColorEditorColorUnknown = 0,
	kAIColorEditorColorGray,
	kAIColorEditorColorRGB,
	kAIColorEditorColorCMYK,
	kAIColorEditorColorLab,
	kAIColorEditorColorDummy = 0xFFFFFFFF
} AIColorEditorColorSpace;

/** Structure for specifying colors to a color editor */
typedef struct
{
	AIColorEditorColorSpace space;
	AIReal values[4];
	AIBoolean defined[4]; 
} AIColorEditorColor;

/** Options for color editor read only mode. See AIPaintStyleSuite::SetColorEditorReadOnly(). */
typedef enum
{
	kAIColorEditorReadOnlyDefault = 0
} AIColorEditorReadOnlyOption;


/*******************************************************************************
 **
 **	Suite
 **
 **/

/** The paint style suite provides APIs for interacting with the palettes that display
	and modify the paint style of the current selection. These include the color
	palette and the gradient palette.

	It also contains APIs for interacting with color editors. A color editor is the
	set of controls that allow entry of a color. They consist of a popup menu for
	selecting the color model, the sliders and edit boxes for the color values, the
	proxy for the current color and the in gamut and web safe color selectors. A
	color editor is instantiated by putting a special control type into a dialog
	resource. Instantiating the dialog then instantiates the color editor. The APIs
	in the AIPaintStyleSuite can then be used to interact with the control.
 */
typedef struct 
{	
	void ASAPI (*SetStrokeActive)();
	ASBoolean ASAPI (*IsStrokeActive)();
	void ASAPI (*SetFillActive)();
	ASBoolean ASAPI (*IsFillActive)();
	void ASAPI (*SetDefaultFillStroke)();
	int ASAPI (*GetActiveGradientStopIndex)();
	ASErr ASAPI (*GetCurrentGradient)(AIColor *color);
	ASErr ASAPI (*GetCurrentColor)(AIColor *color);
	ASErr ASAPI (*ShowGradientDialog)();
	ASErr ASAPI (*ShowColorDialog)();
	/** Updates the active gradient stop, fill or stroke color.  Only updates the proxies if
		caching is started. */
	ASErr ASAPI (*SetActiveColor)(AIColor *color, ASBoolean useGradientStop);
	/** Starts caching the current PathStyle. */
	ASErr ASAPI (*BeginActiveColor)();
	/** Finishes caching the current PathStyle. */
	ASErr ASAPI (*EndActiveColor)();
	ASBoolean ASAPI (*IsCurrentColorStroke)();
	ASErr ASAPI (*SetAIColor)(AIColor *color, AIColorMap *colorMap, ASBoolean isStroke);
	ASErr ASAPI (*SetCurrentGradient)(AIColor *color);
	ASErr ASAPI (*SetCurrentColor)(AIColor *color);
	ASErr ASAPI (*ToggleColorDialog)();

	/** Set the color displayed by a color editor control. */
	ASErr ASAPI (*SetColorEditorAIColor)(ADMItemRef colorEditor, AIColor *color, AIColorMap *colorMap);
	/** Get the color displayed by a color editor control. */
	ASErr ASAPI (*GetColorEditorAIColor)(ADMItemRef colorEditor, AIColor *color, AIColorMap *colorMap);
	/** Set the current color space of a color editor control. */
	ASErr ASAPI (*SetColorEditorColorSpace)(ADMItemRef colorEditor, ColorSpace space);
	/** Get the current color space of a color editor control. */
	ASErr ASAPI (*GetColorEditorColorSpace)(ADMItemRef colorEditor, ColorSpace *space);
	/** Sets options that affect the behaviour of a color editor control group. */
	ASErr ASAPI (*SetColorEditorOptions)(ADMItemRef colorEditor, const AIColorEditorOptions& options);
	/** Gets the current options for a color editor control group. */
	ASErr ASAPI (*GetColorEditorOptions)(ADMItemRef colorEditor, AIColorEditorOptions& options);

	void ASAPI (*SwapFillStroke)();
	/** Puts up a dialog that allows the user to choose a color. Returns the color in
		currentColor and true if the the dialog was OK'ed. */
	ASBoolean ASAPI (*DisplayColorPicker)(AIColor *currentColor);
	
	/** Set the color displayed by a color editor control. */
	ASErr ASAPI (*SetColorEditorColor)(ADMItemRef colorEditor, AIColorEditorColor *color);
	/** Get the color displayed by a color editor control. */
	ASErr ASAPI (*GetColorEditorColor)(ADMItemRef colorEditor, AIColorEditorColor *color);
	/** Sets whether the editor is in read only mode or not. In read only mode
		the editor displays the color and its values but does not allow it to
		be modified. The options allow additional configuration of what is shown
		in read only mode. See #AIColorEditorReadOnlyOption. */
	ASErr ASAPI (*SetColorEditorReadOnly)(ADMItemRef colorEditor, ASBoolean readOnly, ASInt32 options);
} AIPaintStyleSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif