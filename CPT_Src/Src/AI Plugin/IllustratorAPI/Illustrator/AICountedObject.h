#ifndef __AICountedObject__
#define __AICountedObject__

/*
 *        Name:	AICountedObject.h
 *		$Id $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 9.0 Counted Object Suite.
 *
 * Copyright (c) 1999 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AICountedObject.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAICountedObjectSuite				"AI Counted Object Suite"
#define kAICountedObjectSuiteVersion1		AIAPI_VERSION(1)
#define kAICountedObjectSuiteVersion		kAICountedObjectSuiteVersion1
#define kAICountedObjectVersion				kAICountedObjectSuiteVersion


/*******************************************************************************
 **
 **	Suite
 **
 **/


/**
	A number of object types returned through plugin APIs rely on reference
	counting to determine their lifetime. The conventions for reference
	counted objects are as follows:

	- An API that returns a pointer to a reference counted object increments
	the count before it is returned. It is the callers responsible to
	decrement the count when the object is no longer needed.
	- If a reference counted object is passed as a field in a message to
	a plugin or as a parameter to a callback function then its reference
	count is incremented and decremented by the caller. The plugin should
	not decrement the count.
	- If a plugin passes a reference counted object to an API that needs
	to hold onto the object beyond the duration of the call then the API
	will increment the count. The plugin should still decrement its count
	if necessary when done with the object.

	Suites for object types that are reference counted provide their own
	APIs for incrementing and decrementing the count. The counted object suite
	provides APIs that will increment or decrement the count of any reference
	counted object. A client may use either set of APIs.

	Correctly managing reference counts can be difficult. Failure to do
	so will result in memory leaks. C++ client code is strongly advised
	to use the ai::Ref template class to automatically manage these counts.

	@see ai::Ref
	@see AIEntrySuite for an exception to the rules above.
*/

typedef struct {

	/** 
		Increment the object reference count returning the number of references
		after the increment. Object may be null.
	*/
	AIAPI ASInt32 (*AddRef) ( void* object );

	/** 
		Decrement the reference count returning the number of references after
		the decrement. Object may be null.
	*/
	AIAPI ASInt32 (*Release) ( void* object );

} AICountedObjectSuite;


#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
