/***********************************************************************/
/*                                                                     */
/* ASTypes.h                                                           */
/* Adobe Standard Types                                                */
/*                                                                     */
/* Copyright 1996-1999 Adobe Systems Incorporated.                     */
/* All Rights Reserved.                                                */
/*                                                                     */
/* Patents Pending                                                     */
/*                                                                     */
/* NOTICE: All information contained herein is the property of Adobe   */
/* Systems Incorporated. Many of the intellectual and technical        */
/* concepts contained herein are proprietary to Adobe, are protected   */
/* as trade secrets, and are made available only to Adobe licensees    */
/* for their internal use. Any reproduction or dissemination of this   */
/* software is strictly forbidden unless prior written permission is   */
/* obtained from Adobe.                                                */
/*                                                                     */
/* Started by Dave Lazarony, 01/26/1996                                */
/*                                                                     */
/***********************************************************************/

#ifndef __ASTypes__
#define __ASTypes__

/*
 * Includes
 */
 
#ifndef __ASConfig__
#include "ASConfig.h"
#endif

#ifndef __ASPragma__
#include "ASPragma.h"
#endif

#ifdef MAC_ENV
	#ifndef __QUICKDRAW__
		#include <Quickdraw.h> //for bool and GrafPtr Def'n
	#endif
	#ifndef __DIALOGS__
		#include <Dialogs.h>	//for DialogRef Def'n
	#endif
#endif //#ifdef MAC_ENV

#ifdef __cplusplus
extern "C" {
#endif


#pragma PRAGMA_IMPORT_BEGIN
//We need everything to be properly aligned on Windows
#ifdef WIN_ENV
#pragma PRAGMA_ALIGN_BEGIN
#endif

/*
 * Constants
 */

// true and false

#ifndef __cplusplus	
#ifndef true
#define true	1
#endif

#ifndef false
#define false	0
#endif
#endif // __cplusplus

#ifdef TRUE
#undef TRUE
#endif
#define TRUE	true

#ifdef FALSE
#undef FALSE
#endif
#define FALSE	false

// error codes
#define kNoErr					0
#define kOutOfMemoryErr			'!MEM'
#define kBadParameterErr		'PARM'
#define kNotImplementedErr		'!IMP'
#define kCantHappenErr			'CANT'


// NULL and nil

#ifndef NULL

#ifdef MAC_ENV
#if !defined(__cplusplus) && (defined(__SC__) || defined(THINK_C))
#define NULL	((void *) 0)
#else
#define NULL	0
#endif
#endif

#ifdef WIN_ENV
#ifdef __cplusplus
#define NULL    0
#else
#define NULL    ((void *)0)
#endif
#endif

#endif

// dhearst 8/11/99 - we now specifically prefer NULL, so nil
// is obsolete. We no longer provide it, but can't enforce this
// policy because platform headers often provide nil.
#ifndef nil
#define nil NULL
#endif


// AMPAPI  Adobe Standard Plugin API calling convention.

#ifndef AMPAPI
#ifdef MAC_ENV
#define ASAPI
#endif
#ifdef WIN_ENV
#define ASAPI
#endif
#endif

// C calling convention for those places that need it.
// This doesn't really do anything, but is  more for
// an explicity declaration when it matters.
#define ASCAPI	



/*
 * Types
 */

// Integer Types

#ifndef _H_CoreExpT
typedef signed char ASInt8;
typedef signed short ASInt16;
typedef signed long ASInt32;
#endif

typedef unsigned char ASUInt8;
typedef unsigned short ASUInt16;
typedef unsigned long ASUInt32;

typedef long ASErr;

// Storage Types

typedef unsigned char ASByte;
typedef ASByte* ASBytePtr;

// Unicode Types
typedef ASUInt16 ASUnicode;

// Pointer Types

typedef void* ASPtr;
typedef void** ASHandle;

// Fixed Types

typedef long ASFract;
typedef float ASReal;

#ifndef _AS_FIXED_TYPE_
#define _AS_FIXED_TYPE_ 1

typedef	long ASFixed;

typedef struct _t_ASFixedPoint {
	ASFixed h, v;
} ASFixedPoint, *ASFixedPointP;

typedef struct _t_ASFixedRect {
	ASFixed left, top, right, bottom;
} ASFixedRect, *ASFixedRectP;

typedef struct _t_ASFixedMatrix {
	ASFixed a, b, c, d, tx, ty;
} ASFixedMatrix, *ASFixedMatrixP;

#endif/* _AS_FIXED_TYPE_ */

typedef struct _t_ASRealPoint {
	ASReal h, v;
} ASRealPoint;

typedef struct _t_ASRealRect {
	ASReal left, top, right, bottom;
} ASRealRect;

typedef struct _t_ASRealMatrix {
	ASReal a, b, c, d, tx, ty;
} ASRealMatrix;

// This is a generic reference to a resource/plugin file.  If not otherwise stated,
// it is assumed to be equivalent to an SPPluginRef (see "SPPlugs.h")
typedef struct ASAccess* ASAccessRef;


//
//
// Platform dependant natively aligned structures
//
//

#ifdef MAC_ENV
// Size type for Macintosh Rect and Point.
typedef short ASSize;

// ASBoolean is the same a Macintosh Boolean.
typedef unsigned char ASBoolean; 
#endif

#ifdef WIN_ENV
// ASBoolean is the same a Windows BOOL.
typedef int ASBoolean;

// Size type for Windows Rect and Point.
typedef long ASSize;
#endif


//
//
// Platform dependant fixed alignment (68K on Mac, 4 Byte on Windows) structures
//
//
#ifdef MAC_ENV
#pragma PRAGMA_ALIGN_BEGIN
#endif
// Platform Structures

#ifdef MAC_ENV

// ASPortRef is the same as a Macintosh CGrafPtr.
typedef CGrafPtr ASPortRef;

// ASWindowRef is the same as a Macintosh WindowPtr.
typedef WindowRef ASWindowRef;

// ASDialogRef is the same as a Macintosh DialogPtr.
typedef DialogRef ASDialogRef;	

// ASRect is the same size and layout as a Macintosh Rect.
/*typedef struct _t_ASRect {
	ASSize top, left, bottom, right;
} ASRect;*/

// ASPoint is the same size and layout as a Macintosh Point.
/*typedef struct _t_ASPoint {
	ASSize v, h;
} ASPoint;*/

#endif


#ifdef WIN_ENV

// ASPortRef is the same as a Windows HDC.
typedef void* ASPortRef;				

// ASWindowRef is the same as a Windows HWND.
typedef void* ASWindowRef;			

// ASDialogRef is the same as a Windows HWND.
typedef void* ASDialogRef;			

// ASRect is the same size and layout as a Windows RECT.
/*typedef struct _t_ASRect {
	ASSize left, top, right, bottom;
} ASRect;*/

// ASPoint is the same size and layout as a Windows POINT.
/*typedef struct _t_ASPoint  {
	ASSize h, v;
} ASPoint;*/

#endif

// ASRGBColor is the same as a Macintosh RGBColor on Macintosh and Windows.
/*typedef struct _t_ASRGBColor {
	unsigned short red, green, blue;
} ASRGBColor;*/


// AIEvent is the same as a Macintosh EventRecord on Macintosh and Windows.
/*typedef struct _t_ASEvent {
	unsigned short	what;
	unsigned long	message;
	unsigned long	when;
	ASPoint			where;
	unsigned short	modifiers;

} ASEvent;*/

#pragma PRAGMA_ALIGN_END

//
//
// End of Aligned Structures
//
//

#pragma PRAGMA_IMPORT_END

#ifdef __cplusplus
}
#endif


#endif
