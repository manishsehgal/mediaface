#ifndef __AIApplication__
#define __AIApplication__

/*
 *        Name:	AIApplication.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Adobe Illustrator 9.0 Document Suite.
 *
 * Copyright (c) 1986-1999 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


/*******************************************************************************
 **
 **	Imports
 **
 **/

#ifndef __AITypes__
#include "AITypes.h"
#endif

#ifndef __DocumentList__
#include "AIDocumentList.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#pragma AIPRAGMA_ALIGN_BEGIN
#pragma AIPRAGMA_ENUM_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

/** @file AIApplication.h */

/*******************************************************************************
 **
 **	Constants
 **
 **/

#define kAIApplicationSuite					"AI Application Suite"
#define kAIApplicationSuiteVersion1	AIAPI_VERSION(1)

// latest version
#define kAIApplicationSuiteVersion		kAIApplicationSuiteVersion1
#define kAIApplicationVersion				kAIApplicationSuiteVersion


/*******************************************************************************
 **
 **	Types
 **
 **/


/*******************************************************************************
 **
 **	Suite
 **
 **/

/**
	The application suite provides APIs for querying and controlling the state
	of the host application.
 */
typedef struct {

	/** Instructs the application to quit. This is identical to the user issuing
		a quit command. */
	AIAPI AIErr	(*Quit)	();
	/** Is this the first time the application has ever been run? Not implemented.
		Always returns false. */
	AIAPI ASBoolean	(*IsFirstTime)();
	/** Is the application visible. Only implemented on windows. */
	AIAPI ASBoolean	(*IsVisible)();
	/** Hide or show the application. Not implemented. */
	AIAPI AIErr	(*SetVisible)(ASBoolean bVisible);

} AIApplicationSuite;



#pragma PRAGMA_IMPORT_END
#pragma AIPRAGMA_ENUM_END
#pragma AIPRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif


#endif
