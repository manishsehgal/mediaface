/*
 *        Name:	IAIUnicodeString.h
 *   $Revision: 1 $
 *      Author:	 
 *        Date:	   
 *     Purpose:	Utility templates and convenience functions for use with 
 *				the ai::UnicodeString wrapper class.
 *
 * Copyright (c) 2004 Adobe Systems Incorporated, All Rights Reserved.
 *
 */


#ifndef _IAIUNICODESTRINGUTILS_H_
#define _IAIUNICODESTRINGUTILS_H_

#pragma once

#include "IAIUnicodeString.h"


/** @file IAIUnicodeStringUtils.h */

namespace ai {

//----------------------------------------------------------------------
/** @name ADM Get/Set UnicodeString wrappers. */
//----------------------------------------------------------------------
//@{
/** Uses the passed ADM suite to get the text of the passed ADM object.  This is 
	essentially a convenience wrapper around the ADM GetTextW method of the 
	passed suite.
	@param s ADM suite pointer the suite.
	@param obj ADM object reference; e.g. ADMItemRef
	@param out ai::UnicodeString to deliver the text in. */
template<typename ADMSuitePtr, typename ADMObjRef> void GetADMTextUS (ADMSuitePtr s, const ADMObjRef& obj, ai::UnicodeString& out)
{
	ADMInt32 utfCount = s->GetTextLengthW(obj) + 1;
	ai::AutoBuffer<ADMUnicode> buffer(utfCount);

	s->GetTextW(obj, buffer, utfCount);

	out = ai::UnicodeString(buffer.GetBuffer());
}

/** Non-throwing version of GetADMTextUS().
	See GetADMTextUS(). */
template<typename ADMSuitePtr, typename ADMObjRef> AIErr GetADMTextUS (ADMSuitePtr s, const ADMObjRef& obj, ai::UnicodeString& out,
											  const std::nothrow_t&) AINOTHROW
{
	AIErr result = kNoErr;
	try
	{
		GetADMTextUS(s, obj, out);
	}
	catch(ai::Error& e)
	{
		result = e;
	}
	return result;
}

/** Use the passed ADM suite to set the text of the passed ADM object.  This is 
	essentially a convenience wrapper around the ADM SetTextW method of the 
	passed suite.
	@param s ADM suite pointer the suite.
	@param obj ADM object reference; e.g. ADMItemRef
	@param in ai::UnicodeString containing the text to set the ADM object's contents to. */
template<typename ADMSuitePtr, typename ADMObjRef> void SetADMTextUS (ADMSuitePtr s, const ADMObjRef& obj, const ai::UnicodeString& in)
{
	s->SetTextW(obj, in.as_ASUnicode().c_str());
}

/** Non-throwing version of SetADMTextUS().
	See SetADMTextUS(). */
template<typename ADMSuitePtr, typename ADMObjRef> AIErr SetADMTextUS (ADMSuitePtr s, const ADMObjRef& obj, 
											const ai::UnicodeString& in, const std::nothrow_t&) AINOTHROW
{
	AIErr result = kNoErr;
	try
	{
		SetADMTextUS(s, obj, in);
	}
	catch(ai::Error& e)
	{
		result = e;
	}
	return result;
}

template<typename ADMSuitePtr> void GetADMIndexString (ADMSuitePtr s, SPPluginRef inPluginRef, ADMInt32 inStringID,
													   ADMInt32 inStringIndex, ai::UnicodeString& out, ADMInt32 inMaxLen)
{
	ai::AutoBuffer<ADMUnicode> buffer(inMaxLen);

	s->GetIndexStringW(inPluginRef, inStringID, inStringIndex, buffer, inMaxLen);

	out = ai::UnicodeString(buffer.GetBuffer());
}

template<typename ADMSuitePtr> AIErr GetADMIndexString (ADMSuitePtr s, SPPluginRef inPluginRef, ADMInt32 inStringID,
													   ADMInt32 inStringIndex, ai::UnicodeString& out, ADMInt32 inMaxLen, const std::nothrow_t&) AINOTHROW
{
	AIErr result = kNoErr;
	try
	{
		GetADMIndexString(s, inPluginRef, inStringID, inStringIndex, out, inMaxLen);
	}
	catch(ai::Error& e)
	{
		result = e;
	}
	return result;
}

template<typename ADMSuitePtr> void ADMLookUpZString (ADMSuitePtr s, SPPluginRef inPluginRef,
				const char* inString, ai::UnicodeString& out)
{
	const ADMUInt32 kInitBufferLen = 256;
	ai::AutoBuffer<ADMUnicode> buffer(kInitBufferLen);
	ADMUInt32 bufferLen = kInitBufferLen;

	s->LookUpZStringW(inPluginRef, inString, buffer, &bufferLen);
	if ( bufferLen > kInitBufferLen )
	{
		buffer.Resize(bufferLen);
		s->LookUpZStringW(inPluginRef, inString, buffer, &bufferLen);
	}

	out = ai::UnicodeString(buffer.GetBuffer());
}

template<typename ADMSuitePtr> AIErr ADMLookUpZString (ADMSuitePtr s, SPPluginRef inPluginRef,
				const char* inString, ai::UnicodeString& out, const std::nothrow_t&) AINOTHROW
{
	AIErr result = kNoErr;
	try
	{
		ADMLookUpZString(s, inPluginRef, inString, out);
	}
	catch(ai::Error& e)
	{
		result = e;
	}
	return result;
}


//@}

template<typename IADMObj> void SetIADMTextUS (IADMObj& o, const ai::UnicodeString& in)
{
	o.SetTextW(in.as_ASUnicode().c_str());
}

template<typename IADMObj> void SetIADMTextUS (IADMObj& o, const ai::UnicodeString& in, const std::nothrow_t&) AINOTHROW
{
	AIErr result = kNoErr;
	try
	{
		SetIADMTextUS(o, in);
	}
	catch(ai::Error& e)
	{
		result = e;
	}
}

template<typename IADMObj> void GetIADMTextUS (IADMObj& o, ai::UnicodeString& out)
{
	ADMInt32 utfCount = o.GetTextLengthW() + 1;
	ai::AutoBuffer<ADMUnicode> buffer(utfCount);

	o.GetTextW(buffer, utfCount);

	out = ai::UnicodeString(buffer.GetBuffer());
}

template<typename IADMObj> void GetIADMTextUS (IADMObj& o, ai::UnicodeString& out, const std::nothrow_t&) AINOTHROW
{
	AIErr result = kNoErr;
	try
	{
		GetIADMTextUS(o, out);
	}
	catch(ai::Error& e)
	{
		result = e;
	}
}

} // namespace ai

#endif // _IAIUNICODESTRINGUTILS_H_