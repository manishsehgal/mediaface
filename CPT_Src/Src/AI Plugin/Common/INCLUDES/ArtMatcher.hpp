////////////////////////////////////////////////////////////////////////
//
//       Name: ArtMatcher.hpp
//  $Revision: $
//     Author: David Elson
//       Date: 02/06/98
//    Purpose: Encapsulates AIMatchingArt Suite.
//
// Copyright (c) 1986-1997 Adobe Systems Incorporated, All Rights Reserved.
//
////////////////////////////////////////////////////////////////////////

#ifndef _ARTMATCHER_HPP_
#define _ARTMATCHER_HPP_

#include "AIArt.h"
#include "AIMatchingArt.h"

#ifdef __cplusplus
extern "C"
{
#endif

#pragma PRAGMA_ALIGN_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

class ArtMatcher
{
public:
	ArtMatcher(int iNumSpecs);
	virtual ~ArtMatcher(void);
	//
	ASErr SetSpec(int iX, short shType, long lWhichAttr, long lAttr);
	//
	ASErr GetMatchingArt(void);
	int GetNumMatches(void);
	AIArtHandle GetMatchedArt(int iX);
	//
	enum eArtMatcherErrors
	{
		AM_OK = kNoErr,
		AM_InvalidIndex,
		AM_InvalidSuite,
		AM_NumErrs
	};
	//
private:
	int m_iNumSpecs;
	AIMatchingArtSpec *m_pSpecs;
	//
	ASInt32 m_lNumMatches;
	AIArtHandle **m_hMatches;
};

#pragma PRAGMA_IMPORT_END
#pragma PRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif // _ARTMATCHER_HPP_

