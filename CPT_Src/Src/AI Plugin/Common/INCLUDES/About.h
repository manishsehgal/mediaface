/**

	about.h
	Copyright (c) 1995 Adobe Systems Incorporated.

	Adobe Illustrator 7.0 Useful Plug-in Utilities.

 **/


#include "SPInterf.h"


/**-----------------------------------------------------------------------------
 **
 **	Types
 **
 **/


#define kBadSelectionErr  '!sel'


/**-----------------------------------------------------------------------------
 **
 **	Functions
 **
 **/
typedef	AIErr FXErr;

extern AIErr goAbout( SPInterfaceMessage *message );
