#ifndef __Suites_hpp__
#define __Suites_hpp__

#include "ASTypes.h"

/*
	A dumb "acquire all these suites when I instantiate a Suites object and
	release them when it's destroyed" class.
*/

typedef struct
{
	char *name;
	int version;
	void *suite;
} ImportSuite;

// The fAcquiredCount field is not necessarily maintained across unload/reloads;
// in fact it probably isn't.  Whenever we first create a Suites object after
// being loaded or reloaded, it's important to call InitializeRefCount on it.

class Suites
{
public:
	Suites();
	~Suites();
	ASErr Error() { return fError; }

	void InitializeRefCount() { fAcquiredCount = 1; }

private:
	static int fAcquiredCount;
	ASErr fError;

	ASErr AcquireSuites();
	ASErr ReleaseSuites(); 
	ASErr AcquireSuite(ImportSuite *suite);
	ASErr ReleaseSuite(ImportSuite *suite);
};

#endif