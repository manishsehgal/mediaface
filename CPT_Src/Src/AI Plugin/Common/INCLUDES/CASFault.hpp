////////////////////////////////////////////////////////////////////////
//
//       Name: CASFault.hpp
//  $Revision: $
//     Author: Paul George
//       Date: 04/05/98
//    Purpose: For plugin exception handling.
//
// Copyright (c) 1986-1997 Adobe Systems Incorporated, All Rights Reserved.
//
////////////////////////////////////////////////////////////////////////

#ifndef _CASFAULT_HPP_
#define _CASFAULT_HPP_

#ifdef __cplusplus
extern "C"
{
#endif

#pragma PRAGMA_ALIGN_BEGIN
#pragma PRAGMA_IMPORT_BEGIN

////////////////////////////////////////////////////////////////////////
//
// A very simple exception class which allows Illustrator error codes to be
// returned as exceptions.
//
// Example of use:
//
// ASErr
// Foo(void)
// {
//     ASErr result = kNoErr;
//     ...
//     try
//     {
//         ...
//         throw CASFault(kBadParameterErr);
//         ...
//     }
//     catch (CASFault &f) // catch by (C++) reference
//     {
//         // assigns the ASErr field
//         result = f;
//     }
//
//     return result;
// }
//
////////////////////////////////////////////////////////////////////////

class CASFault {
public:
	CASFault (ASErr id)
		{fId = id;}

	operator ASErr()
		{return fId;}

protected:
	ASErr fId;
};

#pragma PRAGMA_IMPORT_END
#pragma PRAGMA_ALIGN_END

#ifdef __cplusplus
}
#endif

#endif // _CASFAULT_HPP_

