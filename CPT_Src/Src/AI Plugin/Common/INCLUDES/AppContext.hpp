#ifndef __AppContext_hpp__
#define __AppContext_hpp__

/*
 *        Name:	AppContext.hpp
 *   $Revision: $
 *      Author:	Dave Lazarony 
 *        Date:	6/11/96					  
 *     Purpose:	Creates a Application Context for setting up the globals in the applications suite.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

#ifndef __SPAccess__
#include "SPAccess.h"
#endif

class AppContext
{
protected:
	void *fAppContext;

public:
	AppContext(SPPluginRef plugin);
	~AppContext();
};

#endif