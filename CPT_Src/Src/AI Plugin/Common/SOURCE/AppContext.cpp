/*
 *        Name:	AppContext.cpp
 *   $Revision: 1 $
 *      Author:	Dave Lazarony 
 *        Date:	6/11/96
 *     Purpose:	Creates a Application Context for setting up the globals in the applications suite.
 *
 *		  NOTE:	This implementation is Illustrator Specific.
 *
 * Copyright (c) 1986-1996 Adobe Systems Incorporated, All Rights Reserved.
 *
 */

/*
	NOTE: this file was yanked out of the PaintStyle plugin and stuck in the GlobLists
	experimental plugin.  If this code is ever stuck into the real PlugInDev tree, this
	file should be shared with the corresponding file in PaintStyle.
	THERE IS NO GLOBLISTS SPECIFIC CODE HERE
*/

#include "AI90Context.h"
#include "AppContext.hpp"

extern "C" AI90AppContextSuite *sAIAppContext;

AppContext::AppContext(SPPluginRef pluginRef)
{
	fAppContext = nil;
	if (sAIAppContext)
		sAIAppContext->PushAppContext(pluginRef, (AIAppContextHandle *)&fAppContext);
}

AppContext::~AppContext()
{
	if (sAIAppContext)
		sAIAppContext->PopAppContext((AIAppContextHandle)fAppContext);
}
