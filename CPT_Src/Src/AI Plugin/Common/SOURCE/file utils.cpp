// Getting a Full Pathname
// Utilities from the Apple Q&A Stack showing how to get the full
// pathname of a file.  Note that this is NOT the recommended way
// to specify a file to Toolbox routines.  These routines should be
// used for displaying full pathnames only.

#include "SPConfig.h"
#ifdef MAC_ENV

#include "Files.h"
#include "file utils.h"
#include "stringUtils.h"

// Assumes inclusion of <MacHeadersCarbon>
#define haveAUX() 0


void PathNameFromDirID(long dirID, short vRefNum, unsigned char *fullPathName)
{
	CInfoPBRec pb;
	DirInfo *block = (DirInfo *)&pb;
	Str255	directoryName;
	OSErr	err;

	fullPathName[0] = '\0';

	block->ioDrParID = dirID;
	block->ioNamePtr = directoryName;
	do {
			block->ioVRefNum = vRefNum;
			block->ioFDirIndex = -1;
			block->ioDrDirID = block->ioDrParID;
			err = PBGetCatInfoSync( &pb );
			if (haveAUX()) {
					if (directoryName[1] != '/')
							/* If this isn't root (i.e. "/"), append a slash ('/') */
							SUpstrcat(directoryName, (StringPtr)"\p/");
			}
			else
					SUpstrcat(directoryName, (StringPtr)"\p:");
			SUpstrinsert(fullPathName, directoryName);
	} while (block->ioDrDirID != 2);
}



#endif