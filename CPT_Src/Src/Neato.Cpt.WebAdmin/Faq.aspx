<%@ Page language="c#" Codebehind="Faq.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.Faq" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Faq</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
	
    <form id="Form1" method="post" runat="server">
    
        <h3 align="center">FAQ content</h3>   
        <br>
        <asp:Button ID="btnNew" Runat="server" Text="New section"/>
        <br><br>
        <asp:DataGrid ID="grdFaq" Runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateColumn HeaderText="Question">
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="270px"/>
                    <ItemTemplate>
                        <asp:TextBox ID="txtQuestion" Runat="server" TextMode="MultiLine" Rows="5" MaxLength="100" style="width:100%" />
                        <asp:RequiredFieldValidator ID="vldEmptyQuestion"
                         Runat="server" Display="Dynamic"
                         ControlToValidate="txtQuestion"
                         style="font-size: 12pt;"
                         Text="* required" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Answer">
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="270px"/>
                    <ItemTemplate>
                        <asp:TextBox ID="txtAnswer" Runat="server" TextMode="MultiLine" Rows="5" MaxLength="1000" style="width:100%" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Action">
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="150px"/>
                    <ItemTemplate>
                        <asp:ImageButton
                            Runat="server"
                            ID="btnItemEdit"
                            ImageUrl="images/edit.gif"
                            CommandName="Edit"
                            CausesValidation="False"
                            AlternateText="Edit" />
                        <asp:ImageButton
                            Runat="server"
                            ID="btnItemDelete"
                            ImageUrl="images/remove.gif"
                            CommandName="Delete"
                            CausesValidation="False"
                            AlternateText="Remove" />
                        <asp:ImageButton
                            Runat="server"
                            ID="btnItemUp"
                            ImageUrl="images/up.gif"
                            CommandName="Up"
                            CausesValidation="False"
                            AlternateText="Up" />
                        <asp:ImageButton
                            Runat="server"
                            ID="btnItemDown"
                            ImageUrl="images/down.gif"
                            CommandName="Down"
                            CausesValidation="False"
                            AlternateText="Down" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:ImageButton
                            Runat="server"
                            ID="btnItemUpdate"
                            ImageUrl="images/save.gif"
                            CommandName="Update"
                            CausesValidation="False"
                            AlternateText="Save" />
                        <asp:ImageButton
                            Runat="server"
                            ID="btnItemCancel"
                            ImageUrl="images/cancel.gif"
                            CommandName="Cancel"
                            CausesValidation="False"
                            AlternateText="Cancel" />
                    </EditItemTemplate>                    
                <ItemStyle Wrap="False" HorizontalAlign="Center"/>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
     </form>
	
  </body>
</html>
