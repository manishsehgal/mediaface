using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;

namespace Neato.Cpt.WebAdmin {
    public class TestAddImage : Page {
        protected Button btnSubmit;
        protected Label lblId;
        protected HtmlInputFile ctlFile;

        private void Page_Load(object sender, EventArgs e) {
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            btnSubmit.Click += new EventHandler(btnSubmit_Click);

        }
        #endregion

        private void btnSubmit_Click(object sender, EventArgs e) {
            if (ctlFile.PostedFile != null && ctlFile.PostedFile.ContentLength > 0) {
                int id = BCImageLibrary.TestAddImage(ctlFile.PostedFile.InputStream);
                lblId.Text = id.ToString();
            }
        }
    }
}