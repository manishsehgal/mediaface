<%@ Page language="c#" Codebehind="ManageMaintenanceClosing.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.ManageMaintenanceClosing" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>ManageMaintenanceClosing</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <script type="text/javascript" language="javascript" src="js/Overview.js"></script>
  </head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<h3 align="center">Maintenance Closing</h3>
			<asp:Panel Runat="server" ID="panMaintenanceClosing">
				<div>Text/HTML:</div>
				<div>
					<asp:TextBox Runat="server" ID="txtText" TextMode="MultiLine" Rows="10" style="width:100%" />
				</div>
				</div>
					<button ID="btnPreview" onclick="javascript:PreviewPage()">Preview</button>
					<asp:Button ID="btnSubmit" Runat="server" Text="Apply to Site"/>
					<asp:Label ID="lblResponse" Runat="server" />
				</div>
				<iframe Runat="server" ID="ctlPreviewContaner" align="absmiddle" frameborder="yes" scrolling="auto" style="width:100%;height:745px;" />
			</asp:Panel>
			<asp:Panel Runat="server" ID="panMessage">
				<h4 align="center">Site is closing for maintenance. Please, open site, if you want to manage close pages.</h4>
			</asp:Panel>
		</form>
	</body>
</html>
