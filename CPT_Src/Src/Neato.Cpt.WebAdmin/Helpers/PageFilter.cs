using System;
using System.Collections;
using System.Collections.Specialized;

namespace Neato.Cpt.WebAdmin.Helpers {
    [Serializable]
    public class PageFilter {
        private NameValueCollection filters = new NameValueCollection();

        public virtual string this[string name] {
            get { return filters[name]; }
            set { filters[name] = value; }
        }

        public virtual string[] GetFiltersWithValue(string val, params string[] filterNames) {
            string[] names = chooseFilterKeys(filterNames);

            ArrayList filterList = new ArrayList();
            foreach (string name in names) {
                if (this[name] == val) {
                    filterList.Add(name);
                }
            }
            return (string[])filterList.ToArray(typeof(string));
        }

        public virtual string[] GetFiltersWithoutValue(string filterValue, params string[] filterNames) {
            string[] names = chooseFilterKeys(filterNames);
                
            ArrayList filterList = new ArrayList();
            foreach (string name in names) {
                if (this[name] != filterValue) {
                    filterList.Add(name);
                }
            }
            return (string[])filterList.ToArray(typeof(string));
        }

        public virtual void ReplaceFilterValue(string oldValue, string newValue, params string[] filterNames) {
            string[] names = chooseFilterKeys(filterNames);
                
            foreach (string name in names) {
                if (this[name] == oldValue) {
                    this[name] = newValue;
                }
            }
        }

        private string[] chooseFilterKeys(string[] filterNames) {
            string[] names;
            if (filterNames.Length > 0) {
                names = filterNames;
            } else {
                names = filters.AllKeys;
            }
            return names;
        }

        public bool FiltersHaveSameValue(string filterValue, params string[] filterNames) {
            string[] names = chooseFilterKeys(filterNames);
            foreach (string name in names) {
                if (this[name] != filterValue) {
                    return false;
                }
            }
            return true;
        }
    }
}