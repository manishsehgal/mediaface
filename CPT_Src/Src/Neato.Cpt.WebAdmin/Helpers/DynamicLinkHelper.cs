using System;
using System.Collections;
using System.Net;
using System.Reflection;

using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.HttpHandlers;
using Neato.Cpt.WebDesigner.HttpModules;

namespace Neato.Cpt.WebAdmin.Helpers {
    public sealed class DynamicLinkHelper {
        private static string ResetDynamicLinksUrl = CheckLocationModule.AdminUrl(Configuration.IntranetDesignerSiteUrl + RefreshCacheHandler.UrlRefreshDynamicLinks(), Neato.Cpt.WebAdmin.StorageManager.CurrentRetailer);
        private DynamicLinkHelper() {}

        public static void ResetDesignerDynamicLinks() {
            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(ResetDynamicLinksUrl);
            WebResponse response= request.GetResponse();
            response.Close();
        }

        public static string[] GetMenuPagesWithFooter(string className) {
            ArrayList pages = new ArrayList();

            Assembly WebDesignerAssembly = Neato.Cpt.WebDesigner.Global.GetAssembly();
            Type[] types = WebDesignerAssembly.GetExportedTypes();
            foreach(Type type in types) {
                if (type.Namespace != "Neato.Cpt.WebDesigner")
                    continue;

                FieldInfo fieldInfo = type.GetField("RawUrl", BindingFlags.NonPublic|BindingFlags.Static);
                if (fieldInfo == null)
                    continue;

                bool hasMenu = false;
                FieldInfo[] fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
                foreach(FieldInfo field in fields) {
                    if (field.FieldType.Name == className) {
                        hasMenu = true;
                        break;
                    }
                }

                if (!hasMenu)
                    continue;

                pages.Add(fieldInfo.GetValue(null));
            }

            return (string[])pages.ToArray(typeof(string));
        }
    }
}