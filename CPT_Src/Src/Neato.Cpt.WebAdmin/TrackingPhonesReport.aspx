<%@ Page language="c#" Codebehind="TrackingPhonesReport.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.TrackingPhonesReport" %>
<%@ Register TagPrefix="cpt" TagName="DateInterval" Src="Controls/DateInterval.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Tracking Devices Report</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">

    
  </head>
  <body MS_POSITIONING="GridLayout">
	
    <form id="Form1" method="post" runat="server">
        <h3 align="center"><asp:Label Runat=server ID="lblTitle"/></h3>
            <table>
               <tr>
                 <td valign="top">
                    Retailer: 
                 </td>
                 <td> 
                    <asp:DropDownList ID="cbRetailers" Runat=server width="120px" />
                 </td>
                 <td style="width:50;"></td>
                 <td rowspan=3>
                    <cpt:DateInterval id="dtiDates" runat="server" CaptionWidth="90px" DateControlWidth="120px" />
                 </td>
               </tr>
               <tr>
                 <td> 
                    Manufacturer:&nbsp;&nbsp;&nbsp;
                 </td>
                 <td> 
                   <asp:DropDownList ID="cbManufacturers" Runat=server width="120px" />
                 </td>
               <tr>
                 <td></td>
                 <td align="right">
                    <asp:Button ID="btnSubmit" text="Search" Runat="server" />
                 </td>
               </tr>
            </table>
            <br>
        
        <asp:DataGrid ID="grdReport" Runat="server"
            AllowPaging="False" AllowSorting="False"
            AutoGenerateColumns="True"
            BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1"
            CellPadding="1" CellSpacing="1" 
            Width ="100%" HeaderStyle-Font-Bold=True HeaderStyle-HorizontalAlign=Center
            />
        <asp:Label Runat=server ID="lblStatus" Visible="False" />
     </form>
	
  </body>
</html>
