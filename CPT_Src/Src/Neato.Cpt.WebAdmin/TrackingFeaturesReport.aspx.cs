using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Tracking;
using Neato.Cpt.WebAdmin.Controls;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebAdmin {
    public class TrackingFeaturesReport : Page {
        #region Url
        private const string RawUrl = "TrackingFeaturesReport.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        protected Button btnSubmit;
        protected DateInterval dtiDates;
        protected DataGrid grdReport;
        protected DropDownList cbRetailers;

        private string currentRetailer = "All";

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(TrackingFeaturesReport_DataBinding);
            btnSubmit.Click += new EventHandler(btnSubmit_Click);
            cbRetailers.DataBinding +=new EventHandler(cbRetailers_DataBinding);
        }
        #endregion

        private void TrackingFeaturesReport_DataBinding(object sender, EventArgs e) {}

        private void btnSubmit_Click(object sender, EventArgs e) {
            grdReport.Visible = false;
            if (IsValid) {
                currentRetailer = cbRetailers.SelectedItem.Text;
                Retailer retailer = null;
                if(cbRetailers.SelectedIndex>0)
                    retailer = BCRetailers.GetRetailers()[cbRetailers.SelectedIndex-1];
                DataSet data = BCTrackingFeatures.GetFeaturesListByDate(dtiDates.StartDate, dtiDates.EndDate, retailer);
                bindGrid(data);
                DataBind();
            }
        }

        private void bindGrid(DataSet data) {
            grdReport.DataSource = data;
            grdReport.Visible = true;
            DataTable resultTable = new DataTable();
            resultTable.Columns.Add("Feature");
            resultTable.Columns.Add("Count");
            string[] features = Feature.GetNames(typeof(Feature));
            foreach (DataRow row in data.Tables[0].Rows) {
                DataRow workrow = resultTable.NewRow();
                string feature = (string)row[0];
                workrow["Feature"] = decodeFeature(feature);
                workrow["Count"] = row[1];
                resultTable.Rows.Add(workrow);
                for (int i = 0; i < features.Length; i++) {
                    string f = features[i];
                    if (f == feature) {
                        features[i] = null;
                        break;
                    }
                }

            }
            foreach (string f in features) {
                if (f == null)
                    continue;
                if (f != "None") {
                    DataRow workrow = resultTable.NewRow();
                    workrow["Feature"] = decodeFeature(f);
                    workrow["Count"] = 0;
                    resultTable.Rows.Add(workrow);
                }

            }
            grdReport.DataSource = resultTable;
        }

        private string decodeFeature(string feature) {
            string res = "";
            switch (feature) {
                case "ColorFill":
                    res = "Color Fill";
                    break;
                case "AddImage":
                    res = "Add Image from Library";
                    break;
                case "AddCustomImage":
                    res = "Upload Image";
                    break;
                case "AddText":
                    res = "Add Text";
                    break;
                case "Paint":
                    res = "Paint";
                    break;
                case "AddShape":
                    res = "Add shape";
                    break;
                case "Clear":
                    res = "Clear";
                    break;
                case "UsePreviousDesign":
                    res = "Use previous design";
                    break;
                case "SaveDesign":
                    res = "Save Design";
                    break;
                case "QuickTools":
                    res = "Quick tools";
                    break;
                case "Preview":
                    res = "Preview";
                    break;
                case "PrintPDF":
                    res = "Print (PDF)";
                    break;
                case "CopyDesign":
                    res = "Copy design";
                    break;
                default:
                    break;
            }
            return res;
        }
        private void cbRetailers_DataBinding(object sender, EventArgs e)
        {
            cbRetailers.Items.Clear();
            
            cbRetailers.Items.Insert(0,"All");
            foreach (Retailer retailer in BCRetailers.GetRetailers()) 
            {
                ListItem item = new ListItem(retailer.Name, retailer.Id.ToString());
                cbRetailers.Items.Add(item);
                if (item.Text == currentRetailer)
                    item.Selected = true;
            }
           
        }

    }
}