<%@ Page language="c#" Codebehind="CustomerAccount.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.CustomerAccount" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
    <HEAD>
        <title>Customer Account</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <link href="css/admin.css" type="text/css" rel="stylesheet">
    </HEAD>
    <body MS_POSITIONING="GridLayout">
        <form id="Form1" method="post" runat="server">
            <h3 align="center">Customer Information</h3>
            <span class="requiredText">Requred *</span>
            <br>
            <table>
                <tr>
                    <td>First Name:</td>
                    <td>
                        <asp:TextBox ID="txtFirstName" Runat="server" CssClass="inputTblField" MaxLength="100" />
                    </td>
                    <td>
                        <span class="requiredText">*</span>
                        <asp:RequiredFieldValidator ControlToValidate="txtFirstName" Display="Dynamic" CssClass="alert" ErrorMessage="First Name is required"
                            Runat="server" ID="vldFirstName" EnableClientScript="False" />
                    </td>
                </tr>
                <tr>
                    <td>Last Name:</td>
                    <td>
                        <asp:TextBox ID="txtLastName" Runat="server" CssClass="inputTblField" MaxLength="100" />
                    </td>
                    <td>
                        <span class="requiredText">*</span>
                        <asp:RequiredFieldValidator ControlToValidate="txtLastName" Display="Dynamic" CssClass="alert" ErrorMessage="Last Name is required"
                            Runat="server" ID="vldLastName" EnableClientScript="False" />
                    </td>
                </tr>
                <tr>
                    <td>E-mail:</td>
                    <td>
                        <asp:TextBox ID="txtEmailAddress" Runat="server" CssClass="inputTblField" MaxLength="100" />
                    </td>
                    <td>
                        <span class="requiredText">*</span>
                        <asp:RequiredFieldValidator ControlToValidate="txtEmailAddress" Display="Dynamic" CssClass="alert" ErrorMessage="E-Mail Address is required"
                            Runat="server" ID="vldEmailAddress" EnableClientScript="False" />
                        <asp:CustomValidator ControlToValidate="txtEmailAddress" Display="Dynamic" CssClass="alert" ErrorMessage="Invalid E-Mail address"
                            Runat="server" ID="vldValidEmail" EnableClientScript="False" />
                        <asp:CustomValidator ControlToValidate="txtEmailAddress" Display="Dynamic" CssClass="alert" ErrorMessage="E-Mail is not unique for this retailer!"
                            Runat="server" ID="vldUniqueEmail" EnableClientScript="False" />
                    </td>
                </tr>
                <tr>
                    <td>Retailer:</td>
                    <td colspan="2">
                        <asp:DropDownList ID="cboRetailers" Runat="server" CssClass="comboTblField" />
                    </td>
                </tr>
                <tr>
                    <td>E-Mail Options:</td>
                    <td colspan="2">
                        <asp:DropDownList ID="cboEmailOptions" Runat="server" CssClass="comboTblField" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:CheckBox ID="chkReceiveInfo" Runat="server" CssClass="checkboxTblField" Checked="True" Text="I would like to receive information about<br>special offers and new products from Fellowes" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Button Runat="server" ID="btnResetPassword" Text="Reset Password" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblMessage" Runat="server" CssClass="alert" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button Runat="server" ID="btnSave" Text="Save" />
                    </td>
                    <td align="right">
                        <asp:Button Runat="server" ID="btnClose" Text="Close" CausesValidation="False" />
                    </td>
                    <td />
                </tr>
            </table>
        </form>
    </body>
</HTML>
