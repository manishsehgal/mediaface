<%@ Page language="c#" Codebehind="ManagePaperBrand.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.ManagePaperBrand" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
    <head>
        <title>Paper Brand Management</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name=vs_defaultClientScript content="JavaScript">
        <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    </head>
    <body MS_POSITIONING="GridLayout">
        <form id="Form1" method="post" runat="server">
            <h3 align="center">Paper Brand Management</h3>
            <asp:Button ID="btnNew" Runat="server" Text="New Paper Brand" CausesValidation="False" />
            <br>
            <br>
            <asp:DataGrid Runat="server" ID="grdPaperBrands" AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateColumn HeaderText="Icon">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" Width="250px" />
                        <ItemTemplate>
                            <asp:Image ID="imgIcon" Runat="server" BorderStyle="Solid" BorderWidth="1" BorderColor="#d4d0c8" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <input ID="ctlIconFile" Runat="server" type="file" NAME="ctlIconFile" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Paper Brand Name">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="160px" />
                        <ItemTemplate>
                            <asp:Label ID="lblName" Runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtName" Runat="server" MaxLength="30" style="width:100%" />
                            <asp:RequiredFieldValidator ID="vldNameRequired" Runat="server"
                                ControlToValidate="txtName" Display="Dynamic"
                                ErrorMessage="Enter Paper Brand name" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Action">
                        <ItemTemplate>
                            <asp:ImageButton Runat="server" ID="btnEdit" ImageUrl="images/edit.gif"
                                CommandName="Edit" CausesValidation="False"
                                ImageAlign="AbsMiddle" AlternateText="Edit" />
                            <asp:ImageButton Runat="server" ID="btnDelete" ImageUrl="images/remove.gif"
                                CommandName="Delete" CausesValidation="False"
                                ImageAlign="AbsMiddle" AlternateText="Remove" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton Runat="server" ID="btnUpdate" ImageUrl="images/save.gif"
                                CommandName="Update" CausesValidation="True"
                                ImageAlign="AbsMiddle" AlternateText="Save" />
                            <asp:ImageButton Runat="server" ID="btnCancel" ImageUrl="images/cancel.gif"
                                CommandName="Cancel" CausesValidation="False"
                                ImageAlign="AbsMiddle" AlternateText="Cancel" />
                        </EditItemTemplate>
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="100px" />
                        <ItemStyle Wrap="False" />
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </form>
    </body>
</html>
