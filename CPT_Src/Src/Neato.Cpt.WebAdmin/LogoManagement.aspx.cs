using System;
using System.IO;
using System.Net;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;
using Neato.Cpt.WebDesigner.HttpModules;

namespace Neato.Cpt.WebAdmin {
    public class LogoManagement : Page {
        private const string RefreshScript = "RefreshScript";

        protected Image imgLogo;
        protected Button btnDelete;
        protected Button btnNew;
        protected Button btnUpload;
        protected Button btnCancel;
        protected HtmlInputFile ctlLogoFile;
        protected Panel pnlManage;
        protected Panel pnlUpload;

        private int retailerId {
            get {return StorageManager.CurrentRetailer.Id;}
        }

        #region Url
        private static string RefreshLogoUrl = CheckLocationModule.AdminUrl(Configuration.IntranetDesignerSiteUrl + RefreshCacheHandler.UrlRefreshLogo(), null);
        private const string RawUrl = "LogoManagement.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(LogoManagement_DataBinding);
            this.btnDelete.Click += new EventHandler(btnDelete_Click);
            this.btnNew.Click += new EventHandler(btnNew_Click);
            this.btnUpload.Click += new EventHandler(btnUpload_Click);
            this.btnCancel.Click += new EventHandler(btnCancel_Click);
        }
        #endregion

        private void LogoManagement_DataBinding(object sender, EventArgs e) {
            ShowManagePanel(true);
            string imageUrl = IconHandler.ImageUrlForLogo(retailerId, true).AbsoluteUri;
            UriBuilder appRoot = new UriBuilder(Request.Url);
            appRoot.Path = Request.ApplicationPath;
            appRoot.Query = string.Empty;
            imageUrl = imageUrl.Replace(appRoot.Uri.AbsoluteUri.TrimEnd('\\', '/'), Configuration.IntranetDesignerSiteUrl.TrimEnd('\\', '/'));
            imgLogo.ImageUrl = imageUrl;
        }

        private void btnDelete_Click(object sender, EventArgs e) {
            try {
                BCLogo.Delete(retailerId);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            RefreshLogo();
        }

        private void btnNew_Click(object sender, EventArgs e) {
            ShowManagePanel(false);
        }

        private void btnUpload_Click(object sender, EventArgs e) {
            Stream logo = ctlLogoFile.PostedFile.InputStream;
            byte[] buffer = new byte[logo.Length];
            logo.Read(buffer, 0, (int)logo.Length);
            try {
                BCLogo.Update(retailerId, buffer);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            RefreshLogo();
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            ShowManagePanel(true);
        }

        private void ShowManagePanel(bool value) {
            pnlManage.Visible = value;
            pnlUpload.Visible = !value;
        }

        private void RefreshLogo() {
            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(RefreshLogoUrl);
            WebResponse response= request.GetResponse();
            response.Close();

            if (!IsStartupScriptRegistered(RefreshScript)) {
                RegisterStartupScript(RefreshScript, "<script type='text/javascript'>RefreshContent()</script>");
            }
        }
    }
}