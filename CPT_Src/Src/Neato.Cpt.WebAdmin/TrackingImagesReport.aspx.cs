using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebAdmin.Controls;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;

namespace Neato.Cpt.WebAdmin {
    public class TrackingImagesReport : Page {
        private const string imgIconId = "imgIcon";
        private const string lblLinksId = "lblLinks";
        private const string litCountId = "litCount";

        #region Url
        private const string RawUrl = "TrackingImagesReport.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private struct ImageTrackingReportTableItem {
            public int imageId;
            public ArrayList foldersList;
            public int count;
        }

        private struct FolderItem {
            public int folderId;
            public string folderName;
        }

        protected Button btnSubmit;
        protected Label lblTitle;
        protected Label lblStatus;
        protected DateInterval dtiDates;
        protected DataGrid grdReport;
        protected DropDownList cbRetailers;
        private string currentRetailer;

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                currentRetailer = StorageManager.CurrentRetailer.Name;
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(TrackingImagesReport_DataBinding);
            btnSubmit.Click += new EventHandler(btnSubmit_Click);
            cbRetailers.DataBinding += new EventHandler(cbRetailers_DataBinding);
            grdReport.ItemDataBound += new DataGridItemEventHandler(grdReport_ItemDataBound);
        }
        #endregion

        private void TrackingImagesReport_DataBinding(object sender, EventArgs e) {
            lblTitle.Text = "What images have been chosen";
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            grdReport.Visible = false;
            if (IsValid) {
                currentRetailer = cbRetailers.SelectedItem.Text;
                Retailer retailer = null;
                retailer = BCRetailers.GetRetailers()[cbRetailers.SelectedIndex];
                DataSet data = BCTrackingImages.GetImagesListByDate(dtiDates.StartDate, dtiDates.EndDate, retailer);

                bindGrid(data);
                DataBind();
            }
        }

        private void bindGrid(DataSet data) {
            ArrayList grdDataList = new ArrayList();
            foreach (DataRow itemRow in data.Tables[0].Rows) {
                ImageTrackingReportTableItem item = new ImageTrackingReportTableItem();
                item.imageId = (int)itemRow["ImageId"];
                item.count = (int)itemRow["Count"];
                item.foldersList = new ArrayList();
                foreach (DataRow folderRow in data.Tables[1].Rows) {
                    if ((int)folderRow["ImageId"] == item.imageId) {
                        FolderItem folderItem = new FolderItem();
                        folderItem.folderId = (int)folderRow["FolderId"];
                        folderItem.folderName = (string)folderRow["Caption"];
                        item.foldersList.Add(folderItem);
                    }
                }
                grdDataList.Add(item);
            }
            //DataTable grdData = getTable(grdDataList);

            if (grdDataList.Count < 1) {
                lblStatus.Text = "There are no results matching your criteria";
                grdReport.DataSource = null;
            } else {
                lblStatus.Text = "";
                grdReport.Visible = true;
                grdReport.DataSource = grdDataList;
            }
        }

        /*private DataTable getTable(ArrayList arr) {
            DataTable result = new DataTable();
            result.Columns.Add("Image",typeof(string));
            
            result.Columns.Add("Count",typeof(string));
            
            foreach(ImageTrackingReportTableItem item in arr) {
               DataRow workRow = result.NewRow() ;
               /*workRow["Count"]=item.count.ToString();
               workRow["Image"]=renderImageCell(item);
               result.Rows.Add(workRow
            }
            return result;
        }
    /*    private string renderImageCell(ImageTrackingReportTableItem item) {
            string res = "";
            res += "<table><tr><td><img src=\""+ImageLibraryHandler.UrlGetIcon(item.imageId).PathAndQuery + "\"></td><td>";
            res += "<table border=0 cellspacing=2 cellpadding=2><tr><td rowspan=\""+(item.foldersList.Count+1).ToString();
            res += "\" valign=top>Directories: </td></tr>";
            foreach(FolderItem folder in item.foldersList) 
                res+="<tr><td><a href=\""+ImageLibraryContent.Url(folder.folderId,BCRetailers.GetRetailerByName(currentRetailer)).PathAndQuery+"\">"+folder.folderName+"</a></td></tr>";
            res+="</table></td></tr></table>";          
            return res;
        }*/

        private void cbRetailers_DataBinding(object sender, EventArgs e) {
            cbRetailers.Items.Clear();

            foreach (Retailer retailer in BCRetailers.GetRetailers()) {
                ListItem item = new ListItem(retailer.Name, retailer.Id.ToString());
                cbRetailers.Items.Add(item);
                if (item.Text == currentRetailer)
                    item.Selected = true;
            }

        }

        private void ItemDataBound(DataGridItem item) {
            ImageTrackingReportTableItem itm = (ImageTrackingReportTableItem)item.DataItem;
            Image img = (Image)item.FindControl(imgIconId);
            Label lit = (Label)item.FindControl(lblLinksId);
            Literal cnt = (Literal)item.FindControl(litCountId);
            img.ImageUrl = ImageLibraryHandler.UrlGetIcon(itm.imageId).PathAndQuery;
            foreach (FolderItem folder in itm.foldersList) {
                HyperLink link = new HyperLink();
                link.NavigateUrl = ImageLibraryContent.Url(folder.folderId, BCRetailers.GetRetailerByName(currentRetailer)).PathAndQuery;
                link.Text = folder.folderName;
                Literal l = new Literal();
                l.Text = "<br>";
                lit.Controls.Add(link);
                lit.Controls.Add(l);
            }
            cnt.Text = itm.count.ToString();

        }

        private void grdReport_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    break;
            }
        }
    }
}