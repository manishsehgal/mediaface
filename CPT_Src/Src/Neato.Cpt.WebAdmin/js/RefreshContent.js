function RefreshContent() {
	if(window.parent == null)
		return;
	var contentWindow = window.parent.frames("Content");
	if(contentWindow == null)
		return;
	contentWindow.location.href = contentWindow.location.pathname;
}
function RefreshContentForContentPages(){
	if(window.parent == null)
		return;
	var contentWindow = window.parent.frames("Content");
	if(contentWindow == null)
		return;
    if( contentWindow.location.href.indexOf("Announcements.aspx") > -1 ||
        contentWindow.location.href.indexOf("LogoManagement.aspx") > -1 ||
        contentWindow.location.href.indexOf("HeaderMenu.aspx") > -1 ||
        contentWindow.location.href.indexOf("Categories.aspx") > -1 ||
        contentWindow.location.href.indexOf("Overview.aspx") > -1 ||
        contentWindow.location.href.indexOf("BuyMoreSkins.aspx") > -1 ||
        contentWindow.location.href.indexOf("TermsOfUse.aspx") > -1 ||
        contentWindow.location.href.indexOf("Support.aspx") > -1 ||
        contentWindow.location.href.indexOf("Faq.aspx") > -1 ||
        contentWindow.location.href.indexOf("FooterMenu.aspx") > -1 ||
        contentWindow.location.href.indexOf("ImageLibraryContent.aspx") > -1 ||
        contentWindow.location.href.indexOf("ExitPage.aspx") > -1 ||
        contentWindow.location.href.indexOf("ManageMaintenanceClosing.aspx") > -1
        )
	contentWindow.location.href = contentWindow.location.pathname;

}

function RefreshMenu() {
	if(window.parent == null)
		return;
	var menuWindow = window.parent.frames("Menu");
	if(menuWindow == null)
		return;
	menuWindow.location.href = menuWindow.location.href;
}

