using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.WebAdmin.Controls;
using Neato.Cpt.WebAdmin.Helpers;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;

namespace Neato.Cpt.WebAdmin {
    public class ManagePaper : Page {
        private const string LblNameId = "lblName";
        private const string ImgIconId = "imgIcon";
        private const string TxtNameId = "txtName";
        private const string BtnDeleteId = "btnDelete";
        private const string LblPaperBrandId = "lblPaperBrand";
        private const string CboPaperBrandId = "cboPaperBrand";
        private const string VldPaperBrandRequiredId = "vldPaperBrandRequired";
        private const string VldPaperXmlFileRequiredId = "vldPaperXmlFileRequired";
        private const string TxtFacesId = "txtFaces";
        private const string CtlPaperXmlFileId = "ctlPaperXmlFile";
        private const string HypIconViewId = "hypIconView";
        private const string CtlDevicesId = "ctlDevices";
        private const string TxtDevicesId = "txtDevices";
        private const string LblPaperTypeId = "lblPaperType";
        private const string LblPaperStateId = "lblPaperState";
        private const string CboPaperStateId = "cboPaperState";
        private const string LblRetailersId = "lblRetailers";
        private const string CtlRetailersId = "ctlRetailers";

        private const int NumberOfColumns = 9;

        private const string AllValue = "-1";
        private const string SelectValue = "-2";

        protected Button btnNew;
        protected DataGrid grdPapers;
        protected DropDownList cboPaperBrandFilter;
        protected DropDownList cboDeviceFilter;
        protected DropDownList cboDeviceTypeFilter;
        protected DropDownList cboPaperStateFilter;
        protected DropDownList cbRetailers;
        protected TextBox txtPaperName;
        protected Button btnSearch;

        private ArrayList brands;
        private ArrayList deviceTypes;
        private ArrayList allDevices;
        private ArrayList devices;
        private ArrayList papers;
        private ArrayList retailers;

        private const string PaperBrandFilterKey = "PaperBrandFilterKey";
        private const string DeviceFilterKey = "DeviceFilterKey";
        private const string DeviceTypeFilterKey = "DeviceTypeFilterKey";
        private const string PaperStateFilterKey = "PaperStateFilterKey";
        private const string RetailerFilterKey = "RetailerFilterKey";

        private PageFilter CurrentFilter {
            get { return (PageFilter)ViewState["CurrentFilter"]; }
            set { ViewState["CurrentFilter"] = value; }
        }

        private bool NewMode {
            get { return (bool)ViewState["NewModeKey"]; }
            set { ViewState["NewModeKey"] = value; }
        }

        #region Url
        private const string RawUrl = "ManagePaper.aspx";

        public static Uri Url(string brandFilter, string deviceFilter, string deviceTypeFilter) {
            return UrlHelper.BuildUrl(RawUrl,
                                      PaperBrandFilterKey, brandFilter,
                                      DeviceFilterKey, deviceFilter,
                                      DeviceTypeFilterKey, deviceTypeFilter);
        }

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                InitFilters();
                NewMode = false;
                DataBind();
            }

        }

        private void InitFilters() {
            CurrentFilter = new PageFilter();

            CurrentFilter[PaperBrandFilterKey] = Request.QueryString[PaperBrandFilterKey];
            if (CurrentFilter[PaperBrandFilterKey] == null) CurrentFilter[PaperBrandFilterKey] = SelectValue;

            CurrentFilter[DeviceFilterKey] = Request.QueryString[DeviceFilterKey];
            if (CurrentFilter[DeviceFilterKey] == null) CurrentFilter[DeviceFilterKey] = SelectValue;

            CurrentFilter[DeviceTypeFilterKey] = Request.QueryString[DeviceTypeFilterKey];
            if (CurrentFilter[DeviceTypeFilterKey] == null) CurrentFilter[DeviceTypeFilterKey] = SelectValue;

            CurrentFilter[PaperStateFilterKey] = Request.QueryString[PaperStateFilterKey];
            if (CurrentFilter[PaperStateFilterKey] == null) CurrentFilter[PaperStateFilterKey] = SelectValue;
            CurrentFilter[RetailerFilterKey] = Request.QueryString[RetailerFilterKey];
            if (CurrentFilter[RetailerFilterKey] == null) CurrentFilter[RetailerFilterKey] = SelectValue;
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManageDevice_DataBinding);
            grdPapers.ItemDataBound += new DataGridItemEventHandler(grdPapers_ItemDataBound);
            grdPapers.EditCommand += new DataGridCommandEventHandler(grdPapers_EditCommand);
            grdPapers.DeleteCommand += new DataGridCommandEventHandler(grdPapers_DeleteCommand);
            grdPapers.UpdateCommand += new DataGridCommandEventHandler(grdPapers_UpdateCommand);
            grdPapers.CancelCommand += new DataGridCommandEventHandler(grdPapers_CancelCommand);
            grdPapers.ItemCreated += new DataGridItemEventHandler(grdPapers_ItemCreated);
            btnNew.Click += new EventHandler(btnNew_Click);
            btnSearch.Click += new EventHandler(btnSearch_Click);
            cboPaperBrandFilter.DataBinding += new EventHandler(cboPaperBrandFilter_DataBinding);
            cboDeviceFilter.DataBinding += new EventHandler(cboDeviceFilter_DataBinding);
            cboDeviceTypeFilter.DataBinding += new EventHandler(cboDeviceTypeFilter_DataBinding);
            cboPaperStateFilter.DataBinding += new EventHandler(cboPaperStateFilter_DataBinding);
            cbRetailers.DataBinding += new EventHandler(cbRetailers_DataBinding);
            this.PreRender += new EventHandler(ManagePaper_PreRender);
        }
        #endregion

        private void ManageDevice_DataBinding(object sender, EventArgs e) {
            brands = new ArrayList(BCPaperBrand.GetPaperBrandList());

            deviceTypes = new ArrayList(Enum.GetNames(typeof(DeviceType)));
            deviceTypes.Remove(DeviceType.Undefined.ToString());

            allDevices = new ArrayList(BCDevice.GetDeviceList());
            allDevices.Sort(Device.GetFullModelComparer(CultureInfo.InvariantCulture));

            retailers = new ArrayList(BCRetailers.GetRetailers());

            if (txtPaperName.Text != string.Empty &&
                cboPaperBrandFilter.SelectedValue == SelectValue &&
                cboDeviceFilter.SelectedValue == SelectValue &&
                cboDeviceTypeFilter.SelectedValue == SelectValue &&
                cboPaperStateFilter.SelectedValue == SelectValue && cbRetailers.SelectedValue == SelectValue) {
                cboPaperBrandFilter.SelectedValue = AllValue;
                cboDeviceFilter.SelectedValue = AllValue;
                cboDeviceTypeFilter.SelectedValue = AllValue;
                cboPaperStateFilter.SelectedValue = "ait";
                cbRetailers.SelectedValue = AllValue;
            }
            if (cboPaperStateFilter.SelectedValue == SelectValue)
                cboPaperStateFilter.SelectedValue = "ait";

            if (IsPostBack) {
                CurrentFilter[PaperBrandFilterKey] = cboPaperBrandFilter.SelectedValue;
                CurrentFilter[DeviceFilterKey] = cboDeviceFilter.SelectedValue;
                CurrentFilter[DeviceTypeFilterKey] = cboDeviceTypeFilter.SelectedValue;
                CurrentFilter[PaperStateFilterKey] = cboPaperStateFilter.SelectedValue;
                CurrentFilter[RetailerFilterKey] = cbRetailers.SelectedValue;
            }
            if (!CurrentFilter.FiltersHaveSameValue(SelectValue)) {
                if (CurrentFilter.GetFiltersWithoutValue(SelectValue).Length > 0) {
                    CurrentFilter.ReplaceFilterValue(SelectValue, AllValue);
                }

                int brandId = int.Parse(CurrentFilter[PaperBrandFilterKey], CultureInfo.InvariantCulture.NumberFormat);
                PaperBrandBase paperBrand = null;
                if (brandId > 0) {
                    paperBrand = new PaperBrandBase(brandId);
                }

                DeviceBase device = null;
                int deviceId = int.Parse(CurrentFilter[DeviceFilterKey], CultureInfo.InvariantCulture.NumberFormat);
                if (deviceId > 0) {
                    device = new DeviceBase(deviceId);
                }

                DeviceType deviceType =
                    (CurrentFilter[DeviceTypeFilterKey] == AllValue) ?
                        DeviceType.Undefined :
                        (DeviceType)Enum.Parse(typeof(DeviceType), CurrentFilter[DeviceTypeFilterKey]);

                Retailer retailer = null;
                if (CurrentFilter[RetailerFilterKey] != AllValue) {
                    string retailerName = CurrentFilter[RetailerFilterKey];
                    Retailer ret = BCRetailers.GetRetailerByName(retailerName);
                    if (ret.Id > 0)
                        retailer = ret;
                }

                string paperState = CurrentFilter[PaperStateFilterKey];

                papers = new ArrayList(BCPaper.GetPapers(paperBrand, deviceType, device, txtPaperName.Text, paperState, retailer));
            } else {
                papers = new ArrayList();
            }

            if (NewMode) {
                papers.Insert(0, BCPaper.NewPaper());
                grdPapers.EditItemIndex = 0;
                devices = new ArrayList();
            } else if (grdPapers.EditItemIndex >= 0) {
                int paperId = (int)grdPapers.DataKeys[grdPapers.EditItemIndex];
                PaperBase paper = new PaperBase(paperId);
                int index = papers.IndexOf(paper);
                grdPapers.EditItemIndex = index;
                devices = new ArrayList(BCDevice.GetDeviceListByPaper(paper));
            }
            grdPapers.DataSource = papers;
            grdPapers.DataKeyField = Paper.IdField;
        }

        private void cboPaperBrandFilter_DataBinding(object sender, EventArgs e) {
            cboPaperBrandFilter.Items.Clear();
            foreach (PaperBrand brand in brands) {
                ListItem item = new ListItem(brand.Name, brand.Id.ToString());
                cboPaperBrandFilter.Items.Add(item);
                if (item.Value == CurrentFilter[PaperBrandFilterKey]) item.Selected = true;
            }
            cboPaperBrandFilter.Items.Insert(0, new ListItem("All", AllValue));
            if (CurrentFilter[PaperBrandFilterKey] == SelectValue) {
                cboPaperBrandFilter.Items.Insert(0, new ListItem("Select", SelectValue));
            }
        }

        private void cboDeviceFilter_DataBinding(object sender, EventArgs e) {
            cboDeviceFilter.Items.Clear();
            foreach (Device device in allDevices) {
                ListItem item = new ListItem(device.FullModel, device.Id.ToString());
                cboDeviceFilter.Items.Add(item);
                if (item.Value == CurrentFilter[DeviceFilterKey]) item.Selected = true;
            }
            cboDeviceFilter.Items.Insert(0, new ListItem("All", AllValue));
            if (CurrentFilter[DeviceFilterKey] == SelectValue) {
                cboDeviceFilter.Items.Insert(0, new ListItem("Select", SelectValue));
            }
        }

        private void cboDeviceTypeFilter_DataBinding(object sender, EventArgs e) {
            cboDeviceTypeFilter.Items.Clear();
            foreach (string deviceType in deviceTypes) {
                ListItem item = new ListItem(deviceType);
                cboDeviceTypeFilter.Items.Add(item);
                if (item.Value == CurrentFilter[DeviceTypeFilterKey]) item.Selected = true;
            }
            cboDeviceTypeFilter.Items.Insert(0, new ListItem("All", AllValue));
            if (CurrentFilter[DeviceTypeFilterKey] == SelectValue) {
                cboDeviceTypeFilter.Items.Insert(0, new ListItem("Select", SelectValue));
            }
        }

        private void cboPaperStateFilter_DataBinding(object sender, EventArgs e) {
            cboPaperStateFilter.Items.Clear();
            cboPaperStateFilter.Items.Insert(0, new ListItem("All", "ait"));
            cboPaperStateFilter.Items.Insert(1, new ListItem("Active", "a"));
            cboPaperStateFilter.Items.Insert(2, new ListItem("Inactive", "i"));
            cboPaperStateFilter.Items.Insert(3, new ListItem("Test Mode", "t"));
            cboPaperStateFilter.Items.Insert(4, new ListItem("Inactive & Test Mode", "it"));
            if (CurrentFilter[PaperStateFilterKey] == SelectValue) {
                cboPaperStateFilter.Items.Insert(0, new ListItem("Select", SelectValue));
            }
        }

        private void grdPapers_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void ItemDataBound(DataGridItem item) {
            Paper paper = (Paper)item.DataItem;
            Label lblName = (Label)item.FindControl(LblNameId);
            lblName.Text = HttpUtility.HtmlEncode(paper.Name);

            Label lblPaperType = (Label)item.FindControl(LblPaperTypeId);
            lblPaperType.Text = HttpUtility.HtmlEncode(BCPaperType.GetName(paper.PaperType));

            Image imgIcon = (Image)item.FindControl(ImgIconId);

            string imageUrl = IconHandler.ImageUrl(paper).AbsoluteUri;
            UriBuilder appRoot = new UriBuilder(Request.Url);
            appRoot.Path = Request.ApplicationPath;
            appRoot.Query = string.Empty;
            imageUrl = imageUrl.Replace(appRoot.Uri.AbsoluteUri.TrimEnd('\\', '/'), Configuration.IntranetDesignerSiteUrl.TrimEnd('\\', '/'));

            imgIcon.ImageUrl = imageUrl;

            HyperLink hypIconView = (HyperLink)item.FindControl(HypIconViewId);
            hypIconView.NavigateUrl = imageUrl;
            hypIconView.Target = HtmlHelper.Blank;

            Label lblPaperBrand = (Label)item.FindControl(LblPaperBrandId);
            lblPaperBrand.Text = HttpUtility.HtmlEncode(paper.Brand.Name);

            Label lblPaperState = (Label)item.FindControl(LblPaperStateId);
            string stateText = "Active";
            if (paper.PaperState == "i") {
                stateText = "Inactive";
            } else if (paper.PaperState == "t") {
                stateText = "Test Mode";
            }
            lblPaperState.Text = stateText;

            TextBox txtFaces = (TextBox)item.FindControl(TxtFacesId);
            StringBuilder sb = new StringBuilder();
            foreach (string faceName in paper.GetFaceNames(WebDesigner.StorageManager.DefaultCulture)) {
                if (sb.Length == 0) {
                    sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(faceName));
                } else {
                    sb.Append(Environment.NewLine);
                    sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(faceName));
                }
            }
            txtFaces.Text = sb.ToString();

            TextBox txtDevices = (TextBox)item.FindControl(TxtDevicesId);
            sb = new StringBuilder();
            foreach (Device device in BCDevice.GetDeviceListByPaper(paper)) {
                if (sb.Length == 0) {
                    sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(device.FullModel));
                } else {
                    sb.Append(Environment.NewLine);
                    sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(device.FullModel));
                }
            }
            txtDevices.Text = sb.ToString();

            Button btnDelete = (Button)item.FindControl(BtnDeleteId);
            string warning = "Do you want to remove this paper?";
            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", warning);

            Label lblRetailers = (Label)item.FindControl(LblRetailersId);
            sb = new StringBuilder();
            foreach (Retailer retailer in BCRetailers.GetRetailers(paper)) {
                if (sb.Length == 0) {
                    sb.Append(HttpUtility.HtmlEncode(retailer.Name));
                } else {
                    sb.Append(HtmlHelper.BrTag);
                    sb.Append(HttpUtility.HtmlEncode(retailer.Name));
                }
            }
            lblRetailers.Text = sb.ToString();
        }

        private void EditItemDataBound(DataGridItem item) {
            Paper paper = (Paper)item.DataItem;
            TextBox txtName = (TextBox)item.FindControl(TxtNameId);
            txtName.Text = HttpUtility.HtmlEncode(paper.Name);
            JavaScriptHelper.RegisterFocusScript(this, txtName.ClientID);

            DropDownList cboPaperBrand = (DropDownList)item.FindControl(CboPaperBrandId);
            cboPaperBrand.DataSource = brands;
            cboPaperBrand.DataValueField = DeviceBrand.IdField;
            cboPaperBrand.DataTextField = DeviceBrand.NameField;
            cboPaperBrand.DataBind();

            int brandIndex = brands.IndexOf(paper.Brand);
            if (brandIndex < 0) {
                cboPaperBrand.Items.Insert(0, new ListItem("SELECT", SelectValue));
                cboPaperBrand.SelectedIndex = 0;
            } else {
                cboPaperBrand.SelectedIndex = brandIndex;
            }
            RequiredFieldValidator vldPaperBrandRequired = (RequiredFieldValidator)item.FindControl(VldPaperBrandRequiredId);
            vldPaperBrandRequired.InitialValue = SelectValue;

            RequiredFieldValidator vldPaperXmlFileRequired = (RequiredFieldValidator)item.FindControl(VldPaperXmlFileRequiredId);
            vldPaperXmlFileRequired.Enabled = NewMode;

            Selector ctlDevices = (Selector)item.FindControl(CtlDevicesId);
            ctlDevices.DataSource = allDevices;
            ctlDevices.DataValueField = Device.IdField;
            ctlDevices.DataTextField = Device.FullModelField;
            ctlDevices.DataBind();
            ctlDevices.SelectedIndices = GetIndices(allDevices, devices);

            DropDownList cboPaperState = (DropDownList)item.FindControl(CboPaperStateId);
            cboPaperState.Items.Insert(0, new ListItem("Active", "a"));
            cboPaperState.Items.Insert(1, new ListItem("Inactive", "i"));
            cboPaperState.Items.Insert(2, new ListItem("Test Mode", "t"));
            if (paper.PaperState == "a") {
                cboPaperState.SelectedIndex = 0;
            } else if (paper.PaperState == "i") {
                cboPaperState.SelectedIndex = 1;
            } else if (paper.PaperState == "t") {
                cboPaperState.SelectedIndex = 2;
            } else {
                cboPaperState.SelectedIndex = 0;
            }

            Selector ctlRetailers = (Selector)item.FindControl(CtlRetailersId);
            ctlRetailers.DataSource = BCRetailers.GetRetailers();
            ctlRetailers.DataValueField = Retailer.IdField;
            ctlRetailers.DataTextField = Retailer.NameField;
            ctlRetailers.DataBind();
            ctlRetailers.SelectedIndices = GetIndices(new ArrayList(BCRetailers.GetRetailers()),
                                                      new ArrayList(BCRetailers.GetRetailers(paper)));
        }

        // TODO: Refactoring: Create Helper 
        private ICollection GetIndices(ArrayList allItems, ArrayList items) {
            ArrayList indices = new ArrayList();
            foreach (object device in items) {
                int index = allItems.IndexOf(device);
                if (index >= 0) {
                    indices.Add(index);
                }
            }
            return indices;
        }

        private void grdPapers_EditCommand(object source, DataGridCommandEventArgs e) {
            grdPapers.EditItemIndex = e.Item.ItemIndex;
            NewMode = false;
            DataBind();
        }

        private void grdPapers_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdPapers.EditItemIndex = -1;
            int paperId = (int)grdPapers.DataKeys[e.Item.ItemIndex];
            PaperBase paper = new PaperBase(paperId);
            try {
                BCPaper.Delete(paper);
                Response.Redirect(Url(CurrentFilter[PaperBrandFilterKey], CurrentFilter[DeviceFilterKey], CurrentFilter[DeviceTypeFilterKey]).PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewMode = false;
            DataBind();
        }

        private void grdPapers_UpdateCommand(object source, DataGridCommandEventArgs e) {
            Selector ctlDevices = (Selector)e.Item.FindControl(CtlDevicesId);
            if (!IsValid) {
                allDevices = new ArrayList(BCDevice.GetDeviceList());
                allDevices.Sort(Device.GetFullModelComparer(CultureInfo.InvariantCulture));
                ctlDevices.DataSource = allDevices;
                ArrayList list = new ArrayList(ctlDevices.SelectedIndices);
                ctlDevices.DataBind();
                ctlDevices.SelectedIndices = list;
                return;
            }

            HtmlInputFile ctlPaperXmlFile = (HtmlInputFile)e.Item.FindControl(CtlPaperXmlFileId);
            Stream paperXml = ctlPaperXmlFile.PostedFile.InputStream;

            int paperId = (int)grdPapers.DataKeys[e.Item.ItemIndex];

            Paper paper = null;
            if (paperXml == null || paperXml.Length == 0) {
                if (NewMode) {
                    paper = BCPaper.NewPaper();
                } else {
                    paper = BCPaper.GetPaper(new PaperBase(paperId));
                }
            } else {
                paper = BCPaperImport.ParsePaper(paperXml);
            }
            paper.Id = paperId;

            TextBox txtName = (TextBox)e.Item.FindControl(TxtNameId);
            paper.Name = txtName.Text.Trim();

            DropDownList cboPaperBrand = (DropDownList)e.Item.FindControl(CboPaperBrandId);
            int brandId = int.Parse(cboPaperBrand.SelectedValue, CultureInfo.InvariantCulture.NumberFormat);
            string brandName = cboPaperBrand.SelectedItem.Text;
            paper.Brand = new PaperBrand(brandId, brandName);

            DropDownList cboPaperState = (DropDownList)e.Item.FindControl(CboPaperStateId);
            string state = cboPaperState.SelectedValue;
            string oldState = paper.PaperState;
            string newState = state;
            paper.PaperState = state;

            ArrayList deviceList = new ArrayList(ctlDevices.SelectedItemValues.Count);
            foreach (object itemValue in ctlDevices.SelectedItemValues) {
                if (itemValue is string) {
                    int deviceId = int.Parse((string)itemValue, CultureInfo.InvariantCulture.NumberFormat);
                    deviceList.Add(new DeviceBase(deviceId));
                }
            }
            DeviceBase[] newDevices = (DeviceBase[])deviceList.ToArray(typeof(DeviceBase));

            Selector ctlRetailers = (Selector)e.Item.FindControl(CtlRetailersId);
            ArrayList retailerList = new ArrayList(ctlRetailers.SelectedItemValues.Count);
            foreach (object itemValue in ctlRetailers.SelectedItemValues) {
                if (itemValue is string) {
                    int retailerId = int.Parse((string)itemValue, CultureInfo.InvariantCulture.NumberFormat);
                    retailerList.Add(new Retailer(retailerId));
                }
            }
            Retailer[] retailers = (Retailer[])retailerList.ToArray(typeof(Retailer));

            try {
                BCPaper.Save(paper, null, newDevices, retailers);
                if (oldState != newState) {
                    BCMail.SendPaperStateChangeNotification(paper.Name, oldState, newState, "");
                }
                Response.Redirect(Url(CurrentFilter[PaperBrandFilterKey], CurrentFilter[DeviceFilterKey], CurrentFilter[DeviceTypeFilterKey]).PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }

            grdPapers.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdPapers_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdPapers.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewMode = true;
            DataBind();
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            grdPapers.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void ManagePaper_PreRender(object sender, EventArgs e) {
            grdPapers.Visible = grdPapers.Items.Count > 0;
        }

        protected void vldPaperXml_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = true;
            if (grdPapers.EditItemIndex >= 0) {
                DataGridItem item = grdPapers.Items[grdPapers.EditItemIndex];

                HtmlInputFile ctlPaperXmlFile = (HtmlInputFile)item.FindControl(CtlPaperXmlFileId);
                if (ctlPaperXmlFile.PostedFile.InputStream.Length != 0) {
                    string[] errors = BCPaperImport.Validate(ctlPaperXmlFile.PostedFile.InputStream);
                    args.IsValid = (errors.Length == 0);

                    StringBuilder sb = new StringBuilder();
                    foreach (string error in errors) {
                        if (sb.Length == 0) {
                            sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(error));
                        } else {
                            sb.Append(HtmlHelper.BrTag);
                            sb.AppendFormat("{0} {1}", HtmlHelper.BlackCircleChar, HttpUtility.HtmlEncode(error));
                        }
                    }
                    ((CustomValidator)source).ErrorMessage = sb.ToString();
                } else {
                    string fileName = ctlPaperXmlFile.Value;
                    if (fileName != string.Empty) {
                        args.IsValid = false;
                        string errorMessageFormat = "Can't open file '{0}'";
                        ((CustomValidator)source).ErrorMessage =
                            string.Format(errorMessageFormat, fileName);
                    }
                }
            }
        }

        private void grdPapers_ItemCreated(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            if (item.ItemType != ListItemType.EditItem)
                return;

            GridTransformationHelper.EditItemCreationForm(item, grdPapers.Columns, NumberOfColumns);
        }

        private void cbRetailers_DataBinding(object sender, EventArgs e) {
            cbRetailers.Items.Clear();

            foreach (Retailer retailer in retailers) {
                ListItem item = new ListItem(retailer.Name);
                cbRetailers.Items.Add(item);
                if (item.Value == CurrentFilter[RetailerFilterKey]) item.Selected = true;
            }
            cbRetailers.Items.Insert(0, new ListItem("All", AllValue));
            if (CurrentFilter[RetailerFilterKey] == SelectValue) {
                cbRetailers.Items.Insert(0, new ListItem("Select", SelectValue));
            }
        }
    }
}