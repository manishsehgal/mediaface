using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.WebAdmin.Controls;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebAdmin {
    public class ImageLibraryContent : Page {
        protected LinkButton btnRoot;
        protected Literal ctlFolderTree;
        protected RequiredFieldValidator vldFolderName;
        protected ImageButton btnFolderUp;
        protected ImageButton btnFolderDown;

        protected TextBox txtFolderName;
        protected Button btnAddFolder;
        protected Button btnEditFolder;
        protected Button btnDeleteFolder;

        protected ImageList ctlImages;
        protected HtmlInputFile ctlAddImage;
        protected Button btnAddImage;
        protected Button btnDelSelectedImages;

        protected HtmlInputHidden ctlCurrentFolderId;
        protected HtmlInputHidden ctlImagesTryDelete;
        protected HtmlInputHidden ctlIsRootSelected;

        private StringWriter stringWriter;
        private HtmlTextWriter writer;

        private Library library {
            get { return (Library)ViewState["library"]; }
            set { ViewState["library"] = value; }
        }

        private int currentFolderId {
            get {
                string id = ctlCurrentFolderId.Value;
                return (id == string.Empty)
                    ? int.MinValue : Convert.ToInt32(id);
            }
            set {
                ctlCurrentFolderId.Value =
                    (value == int.MinValue)
                    ? string.Empty : value.ToString();
            }
        }

        private bool isAnyFolderSelected {
            get { return ctlCurrentFolderId.Value != string.Empty; }
        }

        private bool isImagesTryDelete {
            get { return ctlImagesTryDelete.Value != string.Empty; }
        }

        private bool isRootSelected {
            get { return ctlIsRootSelected.Value != string.Empty; }
        }

        private Retailer retailer {
            get {
                if (Request.QueryString[RetailerIdParam] != null) {
                    int retId = int.Parse(Request.QueryString[RetailerIdParam]);
                    return BCRetailers.GetRetailer(retId);
                } else
                    return StorageManager.CurrentRetailer;
            }
        }

        private const string DivIdPrefix = "ctlDivFolder";

        private const string OpenDivsScriptKey = "OpenDivsScriptKey";
        private const string HighlightCurrentLinkScriptKey = "HighlightCurrentLinkScriptKey";
        private const string UnselectCurrentFolderScriptKey = "UnselectCurrentFolderScriptKey";
        private const string ImagesTryDeleteFlagClearScriptKey = "ImagesTryDeleteFlagClearScriptKey";
        private const string SelectRootNodeScriptKey = "SelectRootNodeScriptKeyScriptKey";
        private const string RefreshMenuScriptKey = "RefreshMenuScriptKey";

        #region Url
        private const string RawUrl = "ImageLibraryContent.aspx";
        private const string FolderIdParam = "FolderId";
        private const string RetailerIdParam = "RetailerId";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri Url(int folder) {
            return UrlHelper.BuildUrl(RawUrl, FolderIdParam, folder);
        }

        public static Uri Url(int folder, Retailer retailer) {
            return UrlHelper.BuildUrl(RawUrl, FolderIdParam, folder, RetailerIdParam, retailer == null ? BCRetailers.defaultId : retailer.Id);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                if (retailer != StorageManager.CurrentRetailer) {
                    StorageManager.CurrentRetailer = retailer;
                    MsgBox.registerStartupScript(this, RefreshMenuScriptKey, "RefreshMenu();");
                    ctlCurrentFolderId.Value = string.Empty;
                }
                library = BCImageLibrary.GetImageLibrary
                    (WebDesigner.StorageManager.CurrentLanguage, retailer);

                if (library.XmlData.FirstChild != null &&
                    library.XmlData.FirstChild.ChildNodes.Count > 0 /*&&
                    !isRootSelected*/
                    ) {
                    string rootId = library.XmlData.FirstChild.
                        ChildNodes[0].Attributes["id"].Value;
                    ctlCurrentFolderId.Value = rootId;
                    if (Request.QueryString[FolderIdParam] != null)
                        currentFolderId = int.Parse(Request.QueryString[FolderIdParam]);
                }
            }

            if (!isImagesTryDelete) {
                LoadCurrentFolderImages();
            }

            PageDataBind();
        }

        private void LoadCurrentFolderImages() {
            ctlImages.DataSource = BCImageLibrary.
                ImageLibItemEnumByFolderId(currentFolderId);
            ctlImages.DataBind();
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnDelSelectedImages.Click += new EventHandler(this.btnDelSelectedImages_Click);
            this.btnDeleteFolder.Click += new EventHandler(this.btnDeleteFolder_Click);
            this.btnAddFolder.Click += new EventHandler(this.btnAddFolder_Click);
            this.btnEditFolder.Click += new EventHandler(this.btnEditFolder_Click);
            this.btnAddImage.Click += new EventHandler(this.btnAddImage_Click);
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(this.ImageLibrary_DataBinding);
            this.btnFolderUp.Click += new ImageClickEventHandler(btnFolderUp_Click);
            this.btnFolderDown.Click += new ImageClickEventHandler(btnFolderDown_Click);
        }
        #endregion

        #region DataBinding
        private string GetFolderPrefix(int count) {
            string result = string.Empty;
            for (int i = 0; i < 4 * count; i++) {
                result += HtmlHelper.NbspTag;
            }
            return result + "|_";
        }

        private void RenderFolder(XmlNode node, int level) {
            if (node.Name == Library.FolderName) {
                string id = node.Attributes["id"].Value;
                string name = node.Attributes["name"].Value;
                string ctlDivId = DivIdPrefix + id;

                writer.Write(GetFolderPrefix(level));

                HyperLink link = new HyperLink();
                link.NavigateUrl = HtmlHelper.EmptyLink;
                link.ID = id;
                link.Text = name;
                link.Attributes[HtmlHelper.OnClick] = string.Format
                    (@"DoFolderTreePostback('{0}', '{1}', '{2}');",
                    ctlCurrentFolderId.ClientID, id, ctlDivId);

                link.RenderControl(writer);

                writer.Write(HtmlHelper.BrTag);

                writer.Write(string.Format
                    ("<div id='{0}' style='{1}: {2}'>",
                    ctlDivId, HtmlHelper.Display, HtmlHelper.DisplayInline));
            }

            foreach (XmlNode child in node.ChildNodes) {
                RenderFolder(child, level + 1);
            }

            if (node.Name == Library.FolderName) {
                writer.Write("</div>");
            }
        }

        private void ImageLibrary_DataBinding(object sender, EventArgs e) {
            PageDataBind();
        }

        private void PageDataBind() {
            btnAddFolder.Text = "Add";
            btnEditFolder.Text = "Rename";
            btnDeleteFolder.Text = "Delete";
            btnAddImage.Text = "Add image";
            btnDelSelectedImages.Text = "Delete selected images";
            btnRoot.Text = "Root";
            btnRoot.Attributes[HtmlHelper.OnClick] =
                string.Format
                (@"IsRootSelectedFlagSet('{0}');
                   UnselectCurrentFolder('{1}');",
                ctlIsRootSelected.ClientID,
                ctlCurrentFolderId.ClientID);

            stringWriter = new StringWriter();
            writer = new HtmlTextWriter(stringWriter);

            RenderFolder(library.XmlData.FirstChild, 0);
            ctlFolderTree.Text = stringWriter.ToString();

            if (isAnyFolderSelected) {
                XmlNode currentFolder =
                    library.SelectFolderById(currentFolderId);
                if (currentFolder != null) {
                    MsgBox.registerStartupScript(
                        this, OpenDivsScriptKey, "OpenDivs([{0}]);",
                        library.GetPathToCurrentLink(currentFolder, DivIdPrefix));

                    MsgBox.registerStartupScript(
                        this, HighlightCurrentLinkScriptKey,
                        "HighlightCurrentLink('{0}', '{1}', '{2}', '{3}');",
                        currentFolderId.ToString(),
                        ctlCurrentFolderId.ClientID,
                        txtFolderName.ClientID,
                        btnDeleteFolder.ClientID);

                    MsgBox.registerStartupScript(
                        this, ImagesTryDeleteFlagClearScriptKey,
                        "ImagesTryDeleteFlagClear('{0}');",
                        ctlImagesTryDelete.ClientID);
                }
            }

            string flag = (isRootSelected && !isAnyFolderSelected) ? "true" : "false";
            MsgBox.registerStartupScript(
                this, SelectRootNodeScriptKey,
                "SelectRootNode('{0}', '{1}', '{2}');",
                flag, ctlIsRootSelected.ClientID, btnRoot.ClientID);

            btnDeleteFolder.Enabled =
                (isAnyFolderSelected &&
                ! library.IsFolderHasChildren(currentFolderId));

            btnEditFolder.Enabled = btnAddImage.Enabled =
                btnDelSelectedImages.Enabled = (isAnyFolderSelected);

            btnAddFolder.Enabled = true;

            btnDelSelectedImages.Attributes[HtmlHelper.OnClick] =
                string.Format("return ConfirmDelSelectedImages('{0}', '{1}')",
                ImageLibraryContentStrings.TextDelSelectedImages(),
                ctlImagesTryDelete.ClientID);
        }
        #endregion

        #region Actions
        private void btnAddFolder_Click(object sender, EventArgs e) {
            if (!vldFolderName.IsValid)
                return;

            object id = (currentFolderId == int.MinValue)
                ? null : (object)currentFolderId;
            int newFolderId = BCImageLibrary.InsertImageLibFolder(
                id, txtFolderName.Text, WebDesigner.StorageManager.DefaultCulture, retailer);

            library.AddFolder(id, newFolderId, txtFolderName.Text);
            currentFolderId = newFolderId;

            LoadCurrentFolderImages();
            PageDataBind();
        }

        private void btnEditFolder_Click(object sender, EventArgs e) {
            if (!vldFolderName.IsValid)
                return;

            BCImageLibrary.UpdateImageLibFolder(
                currentFolderId, txtFolderName.Text,
                WebDesigner.StorageManager.DefaultCulture);

            library.EditFolder(currentFolderId, txtFolderName.Text);

            PageDataBind();
        }

        private void btnDeleteFolder_Click(object sender, EventArgs e) {
            BCImageLibrary.DeleteImageLibFolder(currentFolderId);

            int parentFolderId = library.DeleteFolder(currentFolderId);
            currentFolderId = parentFolderId;

            if (parentFolderId == int.MinValue) {
                MsgBox.registerStartupScript(
                    this, UnselectCurrentFolderScriptKey,
                    "UnselectCurrentFolder('{0}');",
                    ctlCurrentFolderId.ClientID);
            }

            LoadCurrentFolderImages();
            PageDataBind();
        }

        private void btnAddImage_Click(object sender, EventArgs e) {
            if (ctlAddImage.PostedFile != null &&
                ctlAddImage.PostedFile.ContentLength > 0) {
                try {
                    int imageId = BCImageLibrary.ImageLibItemIns(
                        currentFolderId, ctlAddImage.PostedFile.InputStream);
                    library.AddImage(currentFolderId, imageId);
                    Response.Redirect(Url(currentFolderId).PathAndQuery);
                } catch (BaseBusinessException ex) {
                    MsgBox.Alert(this, ex.Message);
                }

                LoadCurrentFolderImages();
                PageDataBind();
            }
        }

        private void btnDelSelectedImages_Click(object sender, EventArgs e) {
            ImageLibItem[] list = ctlImages.SelectedItems;
            BCImageLibrary.ImageLibItemsDel(list);
            library.DeleteImages(list);

            LoadCurrentFolderImages();
            PageDataBind();
        }

        private void btnFolderUp_Click(object sender, ImageClickEventArgs e) {
            int id = library.GetPrevFolderId(currentFolderId);
            if (id > 0) {
                BCImageLibrary.ImageLibFolderSortOrderSwap
                    (currentFolderId, id);
                library.FolderSortOrderSwap(id, currentFolderId);
                PageDataBind();
            }
        }

        private void btnFolderDown_Click(object sender, ImageClickEventArgs e) {
            int id = library.GetNextFolderId(currentFolderId);
            if (id > 0) {
                BCImageLibrary.ImageLibFolderSortOrderSwap
                    (currentFolderId, id);
                library.FolderSortOrderSwap(currentFolderId, id);
                PageDataBind();
            }
        }
        #endregion
    }
}