using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.Entity.Helpers;
using Neato.Cpt.WebAdmin.Controls;
using Neato.Cpt.WebAdmin.Helpers;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;
using Neato.Cpt.WebDesigner.HttpModules;

namespace Neato.Cpt.WebAdmin {
    public class ManageRetailers : Page {
        private const string LblNameId = "lblName";
        private const string LblDisplayNameId = "lblDisplayName";
        private const string LblUrlId = "lblUrl";
        private const string ImgIconId = "imgIcon";
        private const string CtlIconFileId = "ctlIconFile";
        private const string TxtNameId = "txtName";
        private const string TxtDisplayNameId = "txtDisplayName";
        private const string TxtUrlId = "txtUrl";
        private const string ChkHasBrandedSiteViewId = "chkHasBrandedSiteView";
        private const string ChkHasBrandedSiteEditId = "chkHasBrandedSiteEdit";
        private const string CtlRetailersId = "ctlRetailers";
        private const string LblRetailersId = "lblRetailers";

        private const string BtnDeleteId = "btnDelete";
        private const string LblDefaultRetailerId = "lblDefaultRetailer";
        private const string VldExistNameId = "vldExistName";
        protected Button btnNew;
        protected DataGrid grdRetailers;

        private const string RefreshMenuScript = "RefreshMenuScript";

        private bool refreshMenuValue = false;

        private bool RefreshMenu {
            get { return refreshMenuValue; }
            set { refreshMenuValue = value; }
        }

        private bool NewRetailerMode {
            get { return (bool)ViewState["NewRetailerModeKey"]; }
            set { ViewState["NewRetailerModeKey"] = value; }
        }

        #region Url
        private static string RefreshRetailersUrl = CheckLocationModule.AdminUrl(Configuration.IntranetDesignerSiteUrl + RefreshCacheHandler.UrlRefreshRetailers(), null);

        private const string RawUrl = "ManageRetailers.aspx";
        private const string RefreshMenuKey = "RefreshMenu";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri Url(bool refreshMenu) {
            return UrlHelper.BuildUrl(RawUrl, RefreshMenuKey, refreshMenu);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                NewRetailerMode = false;
                DataBind();
            }
            if (Request.QueryString[RefreshMenuKey] != null)
                if (bool.Parse(Request.QueryString[RefreshMenuKey]))
                    RefreshMenu = true;
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManageRetailers_DataBinding);
            grdRetailers.ItemDataBound += new DataGridItemEventHandler(grdRetailers_ItemDataBound);
            grdRetailers.EditCommand += new DataGridCommandEventHandler(grdRetailers_EditCommand);
            grdRetailers.DeleteCommand += new DataGridCommandEventHandler(grdRetailers_DeleteCommand);
            grdRetailers.UpdateCommand += new DataGridCommandEventHandler(grdRetailers_UpdateCommand);
            grdRetailers.CancelCommand += new DataGridCommandEventHandler(grdRetailers_CancelCommand);
            btnNew.Click += new EventHandler(btnNew_Click);
            this.PreRender += new EventHandler(ManageRetailers_PreRender);
            grdRetailers.ItemCreated += new DataGridItemEventHandler(grdRetailers_ItemCreated);
        }
        #endregion

        private void ManageRetailers_DataBinding(object sender, EventArgs e) {
            ArrayList retailers = new ArrayList(BCRetailers.GetRetailers());
            if (NewRetailerMode) {
                retailers.Insert(0, new Retailer());
                grdRetailers.EditItemIndex = 0;
            } else if (grdRetailers.EditItemIndex >= 0) {
                int retailerId = (int)grdRetailers.DataKeys[grdRetailers.EditItemIndex];
                Retailer tmpRetailer = new Retailer();
                tmpRetailer.Id = retailerId;
                int index = retailers.IndexOf(tmpRetailer);
                grdRetailers.EditItemIndex = index;
            }
            grdRetailers.DataSource = retailers;
            grdRetailers.DataKeyField = Retailer.IdField;
        }

        private void grdRetailers_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void grdRetailers_EditCommand(object source, DataGridCommandEventArgs e) {
            grdRetailers.EditItemIndex = e.Item.ItemIndex;
            NewRetailerMode = false;
            DataBind();
        }

        private void grdRetailers_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdRetailers.EditItemIndex = -1;
            int retailerId = (int)grdRetailers.DataKeys[e.Item.ItemIndex];
            if (retailerId == BCRetailers.defaultId) {
                MsgBox.Alert(this, "You can not remove default retailer!");
                return;
            }
            try {
                BCRetailers.DeleteRetailer(new Retailer(retailerId));
                Response.Redirect(Url(true).PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewRetailerMode = false;
            DataBind();
        }

        private void grdRetailers_UpdateCommand(object source, DataGridCommandEventArgs e) {
            Page.Validate();
            if (!IsValid) return;

            Retailer retailer = new Retailer();

            retailer.Id = (int)grdRetailers.DataKeys[e.Item.ItemIndex];

            Retailer oldRetailer = BCRetailers.FindRetailer(retailer.Id);
            if (oldRetailer != null) {
                retailer.PhoneAvailability   = oldRetailer.PhoneAvailability;
                retailer.Mp3Availability     = oldRetailer.Mp3Availability;
                retailer.StickerAvailability = oldRetailer.StickerAvailability;
            }

            TextBox txtName = (TextBox)e.Item.FindControl(TxtNameId);
            retailer.Name = txtName.Text.Trim();

            TextBox txtDisplayName = (TextBox)e.Item.FindControl(TxtDisplayNameId);
            retailer.DisplayName = txtDisplayName.Text;

            TextBox txtUrl = (TextBox)e.Item.FindControl(TxtUrlId);
            retailer.Url = txtUrl.Text.Trim();

            CheckBox chkHasBrandedSiteEdit = (CheckBox)e.Item.FindControl(ChkHasBrandedSiteEditId);
            retailer.HasBrandedSite = chkHasBrandedSiteEdit.Checked;

            CustomValidator vldExistName = (CustomValidator)e.Item.FindControl(VldExistNameId);
            HtmlInputFile ctlIconFile = (HtmlInputFile)e.Item.FindControl(CtlIconFileId);

            Stream icon = ctlIconFile.PostedFile.InputStream;
            vldExistName.IsValid = true;

            Selector ctlRetailers = (Selector)e.Item.FindControl(CtlRetailersId);
            ArrayList retailerList = new ArrayList(ctlRetailers.SelectedItemValues.Count);
            foreach (object itemValue in ctlRetailers.SelectedItemValues) {
                if (itemValue is string) {
                    int retailerId = int.Parse((string)itemValue, CultureInfo.InvariantCulture.NumberFormat);
                    retailerList.Add(new Retailer(retailerId));
                }
            }
            Retailer[] buyNowRetailers = (Retailer[])retailerList.ToArray(typeof(Retailer));

            try {
                if (NewRetailerMode) {
                    if (BCRetailers.IsUniqueRetailerName(retailer.Name))
                        BCRetailers.InsertRetailer(retailer, ConvertHelper.StreamToByteArray(icon), buyNowRetailers);
                    else {
                        vldExistName.IsValid = false;
                        return;
                    }
                } else {
                    if ((BCRetailers.GetId(retailer.Name) != retailer.Id && !BCRetailers.IsUniqueRetailerName(retailer.Name))) {
                        vldExistName.IsValid = false;
                        return;
                    }
                    BCRetailers.Update(retailer, buyNowRetailers);
                    if (icon.Length > 0)
                        BCRetailers.UpdateIcon(retailer, ConvertHelper.StreamToByteArray(icon));
                }
                RefreshRetailers();
                Response.Redirect(Url(true).PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }

            grdRetailers.EditItemIndex = -1;
            NewRetailerMode = false;
            DataBind();
        }

        private void grdRetailers_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdRetailers.EditItemIndex = -1;
            NewRetailerMode = false;
            DataBind();
        }

        private void ItemDataBound(DataGridItem item) {
            Retailer retailer = (Retailer)item.DataItem;
            Label lblName = (Label)item.FindControl(LblNameId);
            Label lblDisplayName = (Label)item.FindControl(LblDisplayNameId);
            Label lblUrl = (Label)item.FindControl(LblUrlId);
            lblName.Text = HttpUtility.HtmlEncode(retailer.Name);
            lblDisplayName.Text = HttpUtility.HtmlEncode(retailer.DisplayName);
            lblUrl.Text = HttpUtility.HtmlEncode(retailer.Url);
            CheckBox chkHasBrandedSiteView = (CheckBox)item.FindControl(ChkHasBrandedSiteViewId);
            chkHasBrandedSiteView.Checked = retailer.HasBrandedSite;
            Image imgIcon = (Image)item.FindControl(ImgIconId);

            // TODO: refactoring: extract method to helper
            string imageUrl = IconHandler.ImageUrl(retailer).AbsoluteUri;
            UriBuilder appRoot = new UriBuilder(Request.Url);
            appRoot.Path = Request.ApplicationPath;
            appRoot.Query = string.Empty;
            imageUrl = imageUrl.Replace(appRoot.Uri.AbsoluteUri.TrimEnd('\\', '/'), Configuration.IntranetDesignerSiteUrl.TrimEnd('\\', '/'));

            imgIcon.ImageUrl = imageUrl;

            ImageButton btnDelete = (ImageButton)item.FindControl(BtnDeleteId);
            string warning = "All customer accounts and Image Library relating to the retailer will be deleted.\nAre you sure to continue?";
            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", warning);

            Label lblDefaultRetailer = (Label)item.FindControl(LblDefaultRetailerId);
            if (retailer.Id == BCRetailers.defaultId) {
                btnDelete.Visible = false;
                lblDefaultRetailer.Visible = true;
            }

            Label lblRetailers = (Label)item.FindControl(LblRetailersId);
            StringBuilder sb = new StringBuilder();
            foreach (Retailer ret in BCRetailers.BuyNowRetailerEnum(retailer)) {
                if (sb.Length == 0) {
                    sb.Append(HttpUtility.HtmlEncode(ret.Name));
                } else {
                    sb.Append(HtmlHelper.BrTag);
                    sb.Append(HttpUtility.HtmlEncode(ret.Name));
                }
            }
            lblRetailers.Text = sb.ToString();
        }

        private void EditItemDataBound(DataGridItem item) {
            Retailer retailer = (Retailer)item.DataItem;
            TextBox txtName = (TextBox)item.FindControl(TxtNameId);
            TextBox txtDisplayName = (TextBox)item.FindControl(TxtDisplayNameId);
            TextBox txtUrl = (TextBox)item.FindControl(TxtUrlId);
            txtName.Text = retailer.Name;
            txtDisplayName.Text = retailer.DisplayName;
            txtUrl.Text = retailer.Url;
            CheckBox chkHasBrandedSiteEdit = (CheckBox)item.FindControl(ChkHasBrandedSiteEditId);
            chkHasBrandedSiteEdit.Checked = retailer.HasBrandedSite;
            if (retailer.Id == BCRetailers.defaultId)
                chkHasBrandedSiteEdit.Enabled = false;

            ArrayList allRetailers = new ArrayList(BCRetailers.GetRetailers());
            ArrayList retailers = new ArrayList(BCRetailers.BuyNowRetailerEnum(retailer));
            Selector ctlRetailers = (Selector)item.FindControl(CtlRetailersId);
            ctlRetailers.DataSource = allRetailers;
            ctlRetailers.DataValueField = Retailer.IdField;
            ctlRetailers.DataTextField = Retailer.NameField;
            ctlRetailers.DataBind();
            ctlRetailers.SelectedIndices = GetIndices(allRetailers, retailers);
            
            JavaScriptHelper.RegisterFocusScript(this, txtName.ClientID);
        }

        private ICollection GetIndices(ArrayList allRetailers, ArrayList retailers) {
            ArrayList indices = new ArrayList();
            foreach (object retailer in retailers) {
                int index = allRetailers.IndexOf(retailer);
                if (index >= 0) {
                    indices.Add(index);
                }
            }
            return indices;
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewRetailerMode = true;
            DataBind();
        }

        public static void RefreshRetailers() {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(RefreshRetailersUrl);
            WebResponse response = request.GetResponse();
            response.Close();
        }

        private void ManageRetailers_PreRender(object sender, EventArgs e) {
            if (!IsStartupScriptRegistered(RefreshMenuScript) && RefreshMenu) {
                RegisterStartupScript(RefreshMenuScript, "<script type='text/javascript'>RefreshMenu()</script>");
                RefreshMenu = false;
            }
        }

        private void grdRetailers_ItemCreated(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            if (item.ItemType != ListItemType.EditItem)
                return;

            GridTransformationHelper.EditItemCreationForm(item, grdRetailers.Columns, 7);
        }
    }
}