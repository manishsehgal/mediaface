using System;
using System.Collections;
using System.Web.UI.WebControls;

using Neato.Cpt.Entity;
using Neato.Cpt.Business;
using Neato.Cpt.WebAdmin.Controls;
using Neato.Cpt.WebAdmin.Helpers;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebAdmin {
	public class PhoneRequests : System.Web.UI.Page {
        protected DataGrid grdPhoneRequests;
        protected Label lblMessage;
        
        protected Button btnSubmit;
        protected Button btnSelectAll;
        protected Button btnSendNotification;
        protected Button btnRemoveSelected;

        protected DropDownList cboCarrierFilter;
        protected DropDownList cboManufacturerFilter;
        protected DropDownList cboModelFilter;

        protected DateInterval dtiDates;

        protected CustomValidator vldRecipientSelection;
        protected CustomValidator vldFilter;
        protected DropDownList cbRetailers;

        private string currentRetailer = All;

        private const string All = "All";
        
        private const string LblFirstName = "lblFirstName";
        private const string TxtFirstName = "txtFirstName";
        private const string LblLastName = "lblLastName";
        private const string TxtLastName = "txtLastName";
        private const string LblEmailAddress = "lblEmailAddress";
        private const string TxtEmailAddress = "txtEmailAddress";
        private const string LblCarrier = "lblCarrier";
        private const string TxtCarrier = "txtCarrier";
        private const string LblPhoneManufacturer = "lblPhoneManufacturer";
        private const string TxtPhoneManufacturer = "txtPhoneManufacturer";
        private const string LblPhoneModel = "lblPhoneModel";
        private const string TxtPhoneModel = "txtPhoneModel";
        private const string TxtCommentView = "txtCommentView";
        private const string TxtComment = "txtComment";
        
        private const string BtnItemDeleteName = "btnItemDelete";
        private const string BtnItemEditName = "btnItemEdit";
        private const string BtnItemUpdateName = "btnItemUpdate";
        private const string BtnItemCancelName = "btnItemCancel";
        private const string ChkSelected = "chkSelected";
        private const string PhoneRequestName = "PhoneRequests";

        private const int PhoneRequestPageSize = 25;
        private const int NumberOfColumns = 7;

        [Serializable]
        private class PhoneRequestData {
            private DeviceRequest deviceRequestValue;
            private bool selectedValue;
            public DeviceRequest DeviceRequest {
                get { return deviceRequestValue; }
                set { deviceRequestValue = value; }
            }
            public bool Selected {
                get { return selectedValue; }
                set { selectedValue = value; }
            }
        }

        private PhoneRequestData[] PhoneRequestDataSource {
            get { return (PhoneRequestData[])ViewState[PhoneRequestName]; }
            set { ViewState[PhoneRequestName] = value; }
        }

        private bool SelectState {
            get {return (bool)ViewState["SelectState"]; }
            set {ViewState["SelectState"] = value; }
        }

		private void Page_Load(object sender, System.EventArgs e) {
            if(!IsPostBack) {
                GetPhoneRequests();
                SelectState = true;
                DataBind();
            }
		}

        private void GetPhoneRequests() {
            string carrier = null;
            string phoneManufacturer = null;
            string phoneModel = null;

            bool otherCarrier = false;
            bool otherPhoneManufacturer = false;
            bool otherPhoneModel = false;
            
            if(cboCarrierFilter.SelectedItem != null) {
                if (cboCarrierFilter.SelectedIndex == (cboCarrierFilter.Items.Count - 1)) {
                    carrier = cboCarrierFilter.SelectedItem.Text;
                    otherCarrier = true;
                }
                else if (cboCarrierFilter.SelectedIndex != 0)
                    carrier = cboCarrierFilter.SelectedItem.Text;
            }

            if(cboManufacturerFilter.SelectedItem != null) {
                if (cboManufacturerFilter.SelectedIndex == (cboManufacturerFilter.Items.Count - 1)) {
                    phoneManufacturer = cboManufacturerFilter.SelectedItem.Text;
                    otherPhoneManufacturer = true;
                }
                else if (cboManufacturerFilter.SelectedIndex != 0)
                    phoneManufacturer = cboManufacturerFilter.SelectedItem.Text;
            }

            if(cboModelFilter.SelectedItem != null) {
                if (cboModelFilter.SelectedIndex == (cboModelFilter.Items.Count - 1)) {
                    phoneModel = cboModelFilter.SelectedItem.Text;
                    otherPhoneModel = true;
                }
                else if (cboModelFilter.SelectedIndex != 0)
                    phoneModel = cboModelFilter.SelectedItem.Text;
            }
            currentRetailer = cbRetailers.SelectedItem != null ? cbRetailers.SelectedItem.Text : currentRetailer;
            Retailer retailer = null;
            if(cbRetailers.SelectedIndex>0)
                retailer = BCRetailers.GetRetailers()[cbRetailers.SelectedIndex-1];
            DateTime timeStart = dtiDates.StartDate;
            DateTime timeEnd = dtiDates.EndDate;
            DeviceRequest[] deviceRequests = BCDeviceRequest.GetPhoneRequestsByCarrierPhoneManufacturerPhoneModel(carrier, otherCarrier, phoneManufacturer, otherPhoneManufacturer, phoneModel, otherPhoneModel,timeStart, timeEnd, retailer);
            ArrayList list = new ArrayList(deviceRequests.Length);
            foreach(DeviceRequest phoneRequest in deviceRequests) {
                PhoneRequestData ppd = new PhoneRequestData();
                ppd.DeviceRequest = phoneRequest;
                ppd.Selected = false;
                list.Add(ppd);
            }
            PhoneRequestDataSource = (PhoneRequestData[])list.ToArray(typeof(PhoneRequestData));
        }

        #region Url
        private const string RawUrl = "PhoneRequests.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent() {
            this.cboCarrierFilter.DataBinding += new System.EventHandler(this.cboCarrierFilter_DataBinding);
            //this.cboCarrierFilter.SelectedIndexChanged += new System.EventHandler(this.cboCarrierFilter_SelectedIndexChanged);
            this.cboManufacturerFilter.DataBinding += new System.EventHandler(this.cboManufacturerFilter_DataBinding);
            this.cboManufacturerFilter.SelectedIndexChanged += new System.EventHandler(this.cboManufacturerFilter_SelectedIndexChanged);
            this.cboModelFilter.DataBinding += new System.EventHandler(this.cboModelFilter_DataBinding);
//            this.cboModelFilter.SelectedIndexChanged += new System.EventHandler(this.cboModelFilter_SelectedIndexChanged);

            this.btnSubmit.Click +=new EventHandler(btnSubmit_Click);

            this.vldRecipientSelection.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(this.vldRecipientSelection_ServerValidate);
            this.vldFilter.ServerValidate += new ServerValidateEventHandler(vldFilter_ServerValidate);
            
            this.grdPhoneRequests.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.grdPhoneRequests_PageIndexChanged);
            this.grdPhoneRequests.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdPhoneRequests_CancelCommand);
            this.grdPhoneRequests.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdPhoneRequests_EditCommand);
            this.grdPhoneRequests.DataBinding += new System.EventHandler(this.grdPhoneRequests_DataBinding);
            this.grdPhoneRequests.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdPhoneRequests_UpdateCommand);
            this.grdPhoneRequests.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdPhoneRequests_DeleteCommand);
            this.grdPhoneRequests.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdPhoneRequests_ItemDataBound);
            this.grdPhoneRequests.ItemCreated += new DataGridItemEventHandler(grdPhoneRequests_ItemCreated);
            
            this.btnSelectAll.DataBinding += new System.EventHandler(this.btnSelectAll_DataBinding);
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            this.btnSendNotification.Click += new System.EventHandler(this.btnSendNotification_Click);
            this.btnRemoveSelected.Click += new System.EventHandler(this.btnRemoveSelected_Click);

            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new System.EventHandler(this.PhoneRequests_DataBinding);
            cbRetailers.DataBinding +=new EventHandler(cbRetailers_DataBinding);
        }
        #endregion

        private void PhoneRequests_DataBinding(object sender, EventArgs e) {
            btnRemoveSelected.Attributes["onclick"] = "javascript:return window.confirm('Are you sure?');";
        }

        private void grdPhoneRequests_DataBinding(object sender, EventArgs e) {
            grdPhoneRequests.PageSize = PhoneRequestPageSize;
            grdPhoneRequests.DataSource = PhoneRequestDataSource;

            int maxIndex = (PhoneRequestDataSource.Length - 1) / grdPhoneRequests.PageSize;
            maxIndex = Math.Max(0, maxIndex);
            grdPhoneRequests.CurrentPageIndex = Math.Min(grdPhoneRequests.CurrentPageIndex, maxIndex);
        }

        private void grdPhoneRequests_ItemDataBound(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            switch (item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                    ItemBound(item);
                    break;
                case ListItemType.EditItem:
                    EditItemBound(item);
                    break;
                default:
                    break;
            }
        }

        private void ItemBound(DataGridItem item) {
            Label lblFirstName = (Label)item.FindControl(LblFirstName);
            Label lblLastName = (Label)item.FindControl(LblLastName);
            Label lblEmailAddress = (Label)item.FindControl(LblEmailAddress);
            Label lblCarrier = (Label)item.FindControl(LblCarrier);
            Label lblPhoneManufacturer = (Label)item.FindControl(LblPhoneManufacturer);
            Label lblPhoneModel = (Label)item.FindControl(LblPhoneModel);
            TextBox txtCommentView = (TextBox)item.FindControl(TxtCommentView);
            CheckBox chkSelected = (CheckBox)item.FindControl(ChkSelected);

            PhoneRequestData phoneRequestData = (PhoneRequestData)item.DataItem;
            DeviceRequest deviceRequest = phoneRequestData.DeviceRequest;
            lblFirstName.Text = deviceRequest.FirstName;
            lblFirstName.Text = deviceRequest.FirstName;
            lblLastName.Text = deviceRequest.LastName;
            lblEmailAddress.Text = deviceRequest.EmailAddress;
            lblCarrier.Text = deviceRequest.Carrier;
            lblPhoneManufacturer.Text = deviceRequest.DeviceBrand;
            lblPhoneModel.Text = deviceRequest.Device;
            txtCommentView.Text = deviceRequest.Comment;
            chkSelected.Checked = phoneRequestData.Selected;

            ImageButton btnEdit = (ImageButton)item.FindControl(BtnItemEditName);
            ImageButton btnUpdate = (ImageButton)item.FindControl(BtnItemUpdateName);
            ImageButton btnCancel = (ImageButton)item.FindControl(BtnItemCancelName);
            ImageButton btnDelete = (ImageButton)item.FindControl(BtnItemDeleteName);

            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", "Are you sure?");

            bool edit = item.ItemType == ListItemType.EditItem;
            btnEdit.Visible = !edit;
            btnDelete.Visible = !edit;
            btnUpdate.Visible = edit;
            btnCancel.Visible = edit;
        }
        private void EditItemBound(DataGridItem item) {
            TextBox txtFirstName = (TextBox)item.FindControl(TxtFirstName);
            TextBox txtLastName = (TextBox)item.FindControl(TxtLastName);
            TextBox txtEmailAddress = (TextBox)item.FindControl(TxtEmailAddress);
            TextBox txtCarrier = (TextBox)item.FindControl(TxtCarrier);
            TextBox txtPhoneManufacturer = (TextBox)item.FindControl(TxtPhoneManufacturer);
            TextBox txtPhoneModel = (TextBox)item.FindControl(TxtPhoneModel);
            TextBox txtComment = (TextBox)item.FindControl(TxtComment);
            CheckBox chkSelected = (CheckBox)item.FindControl(ChkSelected);

            PhoneRequestData phoneRequestData = (PhoneRequestData)item.DataItem;
            DeviceRequest deviceRequest = phoneRequestData.DeviceRequest;
            txtFirstName.Text = deviceRequest.FirstName;
            txtLastName.Text = deviceRequest.LastName;
            txtEmailAddress.Text = deviceRequest.EmailAddress;
            txtCarrier.Text = deviceRequest.Carrier;
            txtPhoneManufacturer.Text = deviceRequest.DeviceBrand;
            txtPhoneModel.Text = deviceRequest.Device;
            txtComment.Text = deviceRequest.Comment;
            chkSelected.Checked = phoneRequestData.Selected;

            ImageButton btnEdit = (ImageButton)item.FindControl(BtnItemEditName);
            ImageButton btnUpdate = (ImageButton)item.FindControl(BtnItemUpdateName);
            ImageButton btnCancel = (ImageButton)item.FindControl(BtnItemCancelName);
            ImageButton btnDelete = (ImageButton)item.FindControl(BtnItemDeleteName);
            
            bool edit = item.ItemType == ListItemType.EditItem;
            btnEdit.Visible = !edit;
            btnDelete.Visible = !edit;
            btnUpdate.Visible = edit;
            btnCancel.Visible = edit;
            JavaScriptHelper.RegisterFocusScript(this, txtFirstName.ClientID);
        }

        private void grdPhoneRequests_EditCommand(object source, DataGridCommandEventArgs e) {
            grdPhoneRequests.EditItemIndex = e.Item.ItemIndex;
            grdPhoneRequests.DataBind();
        }

        private void grdPhoneRequests_UpdateCommand(object source, DataGridCommandEventArgs e) {
            DataGridItem item = e.Item;

            TextBox txtFirstName = (TextBox)item.FindControl(TxtFirstName);
            TextBox txtLastName = (TextBox)item.FindControl(TxtLastName);
            TextBox txtEmailAddress = (TextBox)item.FindControl(TxtEmailAddress);
            TextBox txtCarrier = (TextBox)item.FindControl(TxtCarrier);
            TextBox txtPhoneManufacturer = (TextBox)item.FindControl(TxtPhoneManufacturer);
            TextBox txtPhoneModel = (TextBox)item.FindControl(TxtPhoneModel);
            TextBox txtComment = (TextBox)item.FindControl(TxtComment);
            
            DeviceRequest deviceRequest = PhoneRequestDataSource[e.Item.DataSetIndex].DeviceRequest;
            deviceRequest.FirstName = txtFirstName.Text;
            deviceRequest.LastName = txtLastName.Text;
            deviceRequest.EmailAddress = txtEmailAddress.Text;
            deviceRequest.Carrier = txtCarrier.Text;
            deviceRequest.DeviceBrand = txtPhoneManufacturer.Text;
            deviceRequest.Device = txtPhoneModel.Text;
            deviceRequest.Comment = txtComment.Text;

            BCDeviceRequest.UpdatePhoneRequest(deviceRequest,Neato.Cpt.WebAdmin.StorageManager.CurrentRetailer);
            grdPhoneRequests.EditItemIndex = -1;
            grdPhoneRequests.DataBind();
        }

        private void grdPhoneRequests_DeleteCommand(object source, DataGridCommandEventArgs e) {
            DeviceRequest deviceRequest = PhoneRequestDataSource[e.Item.ItemIndex].DeviceRequest;
            BCDeviceRequest.DeletePhoneRequest(deviceRequest.RequestDeviceId);
            GetPhoneRequests();
            if((grdPhoneRequests.Items.Count - 1) == 0)
                grdPhoneRequests.CurrentPageIndex = 0;
            grdPhoneRequests.EditItemIndex = -1;
            grdPhoneRequests.DataBind();
        }

        private void grdPhoneRequests_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdPhoneRequests.EditItemIndex = -1;
            grdPhoneRequests.DataBind();
        }

        private void grdPhoneRequests_PageIndexChanged(object source, DataGridPageChangedEventArgs e) {
            grdPhoneRequests.EditItemIndex = -1;
            grdPhoneRequests.CurrentPageIndex = e.NewPageIndex;
            grdPhoneRequests.DataBind();
        }

        private void cboCarrierFilter_DataBinding(object sender, EventArgs e) {
            Carrier[] carriers = BCCarrier.GetCarrierListByDeviceType(DeviceType.Phone, StorageManager.CurrentRetailer);
            cboCarrierFilter.Items.Clear();
            cboCarrierFilter.Items.Add(new ListItem("SELECT"));
            foreach(Carrier carrier in carriers) {
                cboCarrierFilter.Items.Add(new ListItem(carrier.Name, carrier.Id.ToString()));
            }
            cboCarrierFilter.Items.Add(new ListItem("Other"));
        }

        private void cboManufacturerFilter_DataBinding(object sender, EventArgs e) {
            DeviceType[] deviceTypes = new DeviceType[] {DeviceType.Phone, DeviceType.MP3Player};
            ArrayList manufactureList = new ArrayList();
            foreach(DeviceType deviceType in deviceTypes)
                manufactureList.AddRange(BCDeviceBrand.GetDeviceBrandList(deviceType));

            manufactureList.Sort(new DeviceBrand.DeviceBrandNameCompare());
            DeviceBrand[] deviceBrands = (DeviceBrand[])manufactureList.ToArray(typeof(DeviceBrand));

            cboManufacturerFilter.Items.Clear();
            cboManufacturerFilter.Items.Add(new ListItem("SELECT"));
            foreach(DeviceBrand deviceBrand in deviceBrands) {
                cboManufacturerFilter.Items.Add(new ListItem(deviceBrand.Name, deviceBrand.Id.ToString()));
            }
            cboManufacturerFilter.Items.Add(new ListItem("Other"));
        }

        private void cboModelFilter_DataBinding(object sender, EventArgs e) {
            cboModelFilter.Items.Clear();
            cboModelFilter.Enabled = false;

            int index = cboManufacturerFilter.SelectedIndex;
            if(index != 0 && index != (cboManufacturerFilter.Items.Count - 1)) {
                int brandId = int.Parse(cboManufacturerFilter.Items[index].Value);
                Device[] devices = BCDevice.GetDeviceList(new DeviceBrandBase(brandId), DeviceType.Phone);

                if(devices.Length != 0) {
                    cboModelFilter.Items.Add(new ListItem("SELECT"));
                    foreach(Device device in devices) {
                        cboModelFilter.Items.Add(new ListItem(device.Model, device.Id.ToString()));
                    }
                    cboModelFilter.Items.Add(new ListItem("Other"));
                    cboModelFilter.Enabled = true;
                }
            }
        }



        private void cboManufacturerFilter_SelectedIndexChanged(object sender, EventArgs e) {

            cboModelFilter.DataBind();
        }
        private void btnSelectAll_DataBinding(object sender, EventArgs e) {
            this.btnSelectAll.Text = SelectState ? "Select all" : "Deselect all";
        }

        private void btnSelectAll_Click(object sender, EventArgs e) {
            foreach(PhoneRequestData phoneRequestData in PhoneRequestDataSource) {
                phoneRequestData.Selected = SelectState;
            }
            SelectState = !SelectState;
            grdPhoneRequests.DataBind();
            btnSelectAll.DataBind();
        }

        private void btnSendNotification_Click(object sender, EventArgs e) {
            foreach(DataGridItem item in grdPhoneRequests.Items) {
                PhoneRequestData phoneRequestData = PhoneRequestDataSource[item.DataSetIndex];
                if(phoneRequestData.Selected) {
                    string siteLink = string.Format("{0}{1}/{2}",
                        Configuration.PublicDesignerSiteUrl,
                        BCRetailers.GetRetailerNameById(phoneRequestData.DeviceRequest.RetailerId),
                        WebDesigner.DefaultPage.PageName());

                    BCMail.SendPhoneRequestNotificationMail(
                        phoneRequestData.DeviceRequest, siteLink, string.Empty);
                }
            }            
        }

        private void btnRemoveSelected_Click(object sender, EventArgs e) {
            int count = 0;
            foreach(DataGridItem item in grdPhoneRequests.Items) {
                PhoneRequestData phoneRequestData = PhoneRequestDataSource[item.DataSetIndex];
                if(phoneRequestData.Selected) {
                    ++ count;
                    DeviceRequest deviceRequest = PhoneRequestDataSource[item.DataSetIndex].DeviceRequest;
                    BCDeviceRequest.DeletePhoneRequest(deviceRequest.RequestDeviceId);
                }
            }
            GetPhoneRequests();
            if((grdPhoneRequests.Items.Count - count) == 0)
                grdPhoneRequests.CurrentPageIndex = 0;
            grdPhoneRequests.EditItemIndex = -1;
            grdPhoneRequests.DataBind();
        }

        protected void chkSelected_CheckedChanged(object sender, EventArgs e) {
            CheckBox chkSelected = sender as CheckBox;
            DataGridItem item = (DataGridItem)chkSelected.Parent.Parent;
            PhoneRequestData phoneRequestData = PhoneRequestDataSource[item.DataSetIndex];
            phoneRequestData.Selected = chkSelected.Checked;
        }

        private void vldRecipientSelection_ServerValidate(object source, ServerValidateEventArgs args) {
            int count = 0;
            foreach(DataGridItem item in grdPhoneRequests.Items) {
                PhoneRequestData phoneRequestData = PhoneRequestDataSource[item.DataSetIndex];
                if(phoneRequestData.Selected)
                    ++count;
            }
            args.IsValid = count > 0;
        }

        private void vldFilter_ServerValidate(object source, ServerValidateEventArgs args) {
            if(PhoneRequestDataSource.Length == 0) {
                args.IsValid = false;
                grdPhoneRequests.Visible = false;
                btnSelectAll.Visible = false;
                btnSendNotification.Visible = false;
                btnRemoveSelected.Visible = false;
            }
            else {
                args.IsValid = true;
                grdPhoneRequests.Visible = true;
                btnSelectAll.Visible = true;
                btnSendNotification.Visible = true;
                btnRemoveSelected.Visible = true;
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e){
           // cboModelFilter.DataBind();
            GetPhoneRequests();
            grdPhoneRequests.DataBind();
            vldFilter.Validate();
            vldRecipientSelection.IsValid = true;
            
        }

        private void grdPhoneRequests_ItemCreated(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            if(item.ItemType != ListItemType.EditItem)
                return;

            GridTransformationHelper.EditItemCreationForm(item, grdPhoneRequests.Columns, NumberOfColumns);
        }
        private void cbRetailers_DataBinding(object sender, EventArgs e)
        {
            cbRetailers.Items.Clear();
            
            cbRetailers.Items.Insert(0, All);
            foreach (Retailer retailer in BCRetailers.GetRetailers()) 
            {
                ListItem item = new ListItem(retailer.Name, retailer.Id.ToString());
                cbRetailers.Items.Add(item);
                if (item.Text == currentRetailer)
                    item.Selected = true;
            }
           
        }
    }
}
