<%@ Page language="c#" Codebehind="BuyMoreSkins.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.BuyMoreSkins" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Where can I buy more skins text editing</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <script type="text/javascript" language="javascript" src="js/Overview.js"></script>
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
      <h3 align="center">Where can I buy more skins</h3>
      <span>Text/HTML:</span>
      <asp:TextBox ID="txtText" Runat="server" TextMode="MultiLine" Rows="10" MaxLength="2048" style="width:100%"></asp:TextBox>
      <br>
      <asp:Button ID="btnPreview" Runat="server" Text="Preview"/>
      <asp:Button ID="btnSubmit" Runat="server" Text="Apply to Site"/>
      <br><br>
      <iframe Runat="server" ID="ctlPreviewContaner" align="absmiddle" frameborder="yes" scrolling="auto" style="width:100%;height:745px;"/>
    </form>
  </body>
</html>
