using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebAdmin.Helpers;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebAdmin {
    public class AdminMenu : Page {
        protected HyperLink hypAnnouncements;
        protected HyperLink hypLogoManagement;
        protected HyperLink hypHeaderMenu;
        protected HyperLink hypCategories;
        protected HyperLink hypFooterMenu;
        protected HyperLink hypOverview;
        protected HyperLink hypBuyMoreSkins;
        protected HyperLink hypTermsOfUse;
        protected HyperLink hypSupport;
        protected HyperLink hypFaq;
        protected HyperLink hypMaintenanceClosing;

        protected HyperLink hypAddPhone;
        protected HyperLink hypSiteClosing;
        protected HyperLink hypManageCarrier;
        protected HyperLink hypTrackUserAction;
        protected HyperLink hypTrackPhones;
        protected HyperLink hypTrackImages;
        protected HyperLink hypTrackFeatures;
        protected HyperLink hypNotifications;
        protected HyperLink hypCustomerProfiles;
        protected HyperLink hypManageDeviceBrand;
        protected HyperLink hypManagePaperBrand;
        protected HyperLink hypManageDevice;
        protected HyperLink hypManagePaper;
        protected HyperLink hypManageRetailers;
        protected HyperLink hypExitPage;

        protected HyperLink hypImageLibrary;
        protected HyperLink hypPhoneRequestReportByDatesAndBrand;
        protected HyperLink hypPhoneRequestEmailsReportByDates;
        protected HyperLink hypGoogleTracking;
        protected HyperLink hypManageIPFilter;
        protected HyperLink hypManageSpecialUser;

        protected DropDownList cboRetailers;

        private const string RefreshScript = "RefreshScript";
        private const string RetailerFilterKey = "RetailerFilterKey";

        private PageFilter CurrentFilter {
            get { return (PageFilter)ViewState["CurrentFilter"]; }
            set { ViewState["CurrentFilter"] = value; }
        }

        private void InitFilters() {
            CurrentFilter = new PageFilter();

            CurrentFilter[RetailerFilterKey] = Request.QueryString[RetailerFilterKey];
            if (CurrentFilter[RetailerFilterKey] == null) CurrentFilter[RetailerFilterKey] = StorageManager.CurrentRetailer.Id.ToString();
        }

        #region Url
        private const string RawUrl = "AdminMenu.aspx";
        private const string Target = "Content";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                InitFilters();
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new System.EventHandler(this.AdminMenu_DataBinding);
            this.cboRetailers.DataBinding += new EventHandler(cboRetailers_DataBinding);
            this.cboRetailers.SelectedIndexChanged += new EventHandler(cboRetailers_SelectedIndexChanged);
        }
        #endregion

        private void AdminMenu_DataBinding(object sender, EventArgs e) {
            if (IsPostBack) 
                CurrentFilter[RetailerFilterKey] = cboRetailers.SelectedValue;

            hypAnnouncements.NavigateUrl = Announcements.Url().PathAndQuery;
            hypAnnouncements.Target = Target;
            hypLogoManagement.NavigateUrl = LogoManagement.Url().PathAndQuery;
            hypLogoManagement.Target = Target;
            hypHeaderMenu.NavigateUrl = HeaderMenu.Url().PathAndQuery;
            hypHeaderMenu.Target = Target;
            hypCategories.NavigateUrl = Categories.Url().PathAndQuery;
            hypCategories.Target = Target;
            hypFooterMenu.NavigateUrl = FooterMenu.Url().PathAndQuery;
            hypFooterMenu.Target = Target;
            hypOverview.NavigateUrl = Overview.Url().PathAndQuery;
            hypOverview.Target = Target;
            hypBuyMoreSkins.NavigateUrl = BuyMoreSkins.Url().PathAndQuery;
            hypBuyMoreSkins.Target = Target;
            hypTermsOfUse.NavigateUrl = TermsOfUse.Url().PathAndQuery;
            hypTermsOfUse.Target = Target;
            hypSupport.NavigateUrl = Support.Url().PathAndQuery;
            hypSupport.Target = Target;
            hypFaq.NavigateUrl = Faq.Url().PathAndQuery;
            hypFaq.Target = Target;
            hypMaintenanceClosing.NavigateUrl = ManageMaintenanceClosing.Url().PathAndQuery;
            hypMaintenanceClosing.Target = Target;

            hypAddPhone.NavigateUrl = PhoneRequests.Url().PathAndQuery;
            hypAddPhone.Target = Target;
            hypSiteClosing.NavigateUrl = SiteClosing.Url().PathAndQuery;
            hypSiteClosing.Target = Target;
            hypManageCarrier.NavigateUrl = ManageCarrier.Url().PathAndQuery;
            hypManageCarrier.Target = Target;
            hypTrackUserAction.NavigateUrl = TrackingUserActionReport.Url().PathAndQuery;
            hypTrackUserAction.Target = Target;
            hypTrackPhones.NavigateUrl = TrackingPhonesReport.Url().PathAndQuery;
            hypTrackPhones.Target = Target;
            hypTrackImages.NavigateUrl = TrackingImagesReport.Url().PathAndQuery;
            hypTrackImages.Target = Target;
            hypTrackFeatures.NavigateUrl = TrackingFeaturesReport.Url().PathAndQuery;
            hypTrackFeatures.Target = Target;
            hypNotifications.NavigateUrl = Notifications.Url().PathAndQuery;
            hypNotifications.Target = Target;
            hypCustomerProfiles.NavigateUrl = CustomerList.Url().PathAndQuery;
            hypCustomerProfiles.Target = Target;
            hypManageDeviceBrand.NavigateUrl = ManageDeviceBrand.Url().PathAndQuery;
            hypManageDeviceBrand.Target = Target;
            hypManagePaperBrand.NavigateUrl = ManagePaperBrand.Url().PathAndQuery;
            hypManagePaperBrand.Target = Target;
            hypManagePaper.NavigateUrl = ManagePaper.Url().PathAndQuery;
            hypManagePaper.Target = Target;
            hypManageRetailers.NavigateUrl = ManageRetailers.Url().PathAndQuery;
            hypManageRetailers.Target = Target;
            hypExitPage.NavigateUrl = ExitPage.Url().PathAndQuery;
            hypExitPage.Target = Target;
            hypManageDevice.NavigateUrl = ManageDevice.Url().PathAndQuery;
            hypManageDevice.Target = Target;
            hypImageLibrary.NavigateUrl = ImageLibraryContent.Url().PathAndQuery;
            hypImageLibrary.Target = Target;
            hypPhoneRequestReportByDatesAndBrand.NavigateUrl = PhoneRequestReportByDatesAndBrand.Url().PathAndQuery;
            hypPhoneRequestReportByDatesAndBrand.Target = Target;
            hypPhoneRequestEmailsReportByDates.NavigateUrl = PhoneRequestEmailsReportByDates.Url().PathAndQuery;
            hypPhoneRequestEmailsReportByDates.Target = Target;
            hypGoogleTracking.NavigateUrl = GoogleTracking.Url().PathAndQuery;
            hypGoogleTracking.Target = Target;
            hypManageIPFilter.NavigateUrl = ManageIPFilter.Url().PathAndQuery;
            hypManageIPFilter.Target = Target;
            hypManageSpecialUser.NavigateUrl = ManageSpecialUser.Url().PathAndQuery;
            hypManageSpecialUser.Target = Target;
        }

        private void cboRetailers_DataBinding(object sender, EventArgs e) {
            cboRetailers.Items.Clear();
            foreach (Retailer retailer in BCRetailers.GetRetailers()) {
                ListItem item = new ListItem(retailer.Name, retailer.Id.ToString());
                cboRetailers.Items.Add(item);
                if (item.Value == CurrentFilter[RetailerFilterKey])
                    item.Selected = true;
            }
        }

        private void cboRetailers_SelectedIndexChanged(object sender, EventArgs e) {
            StorageManager.CurrentRetailer = BCRetailers.GetRetailers()[cboRetailers.SelectedIndex];
            if (!IsStartupScriptRegistered(RefreshScript)) {
                RegisterStartupScript(RefreshScript, "<script type='text/javascript'>RefreshContentForContentPages()</script>");
            }
            
       }
    }
}
