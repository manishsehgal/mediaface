<%@ Register TagPrefix="cpt" TagName="DateInterval" Src="Controls/DateInterval.ascx" %>
<%@ Page language="c#" Codebehind="TrackingFeaturesReport.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.TrackingFeaturesReport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
    <HEAD>
        <title>TrackingFeaturesReport</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    </HEAD>
    <body MS_POSITIONING="GridLayout">
        <form id="Form1" method="post" runat="server">
            <h3 align="center">What features have been choosen</h3>
            <table>
               <tr>
                 <td valign="top">
                    Retailer:&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="cbRetailers" Runat=server width="120px" />
                 </td>
                 <td style="width:50;"></td>
                 <td rowspan=2>
                    <cpt:DateInterval id="dtiDates" runat="server" CaptionWidth="90px" DateControlWidth="120px" />
                 </td>
               </tr>
               <tr>
                 <td align="right">
                    <asp:Button ID="btnSubmit" text="Search" Runat="server" />
                 </td>
               </tr>
            </table>
            <br>
            <asp:DataGrid ID="grdReport" Runat="server"
            AllowPaging="False" AllowSorting="False"
            AutoGenerateColumns="True"
            BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1"
            CellPadding="1" CellSpacing="1" 
            Width ="100%" HeaderStyle-Font-Bold=True HeaderStyle-HorizontalAlign=Center
            />
        </form>
    </body>
</HTML>
