using System;
using System.Collections;
using System.Net;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Web;
//using System.Web.SessionState;
//using System.Web.UI;
using System.Web.UI.WebControls;
//using System.Web.UI.HtmlControls;
//using Neato.Cpt.WebAdmin.Helpers;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.WebDesigner.HttpHandlers;
using Neato.Cpt.WebDesigner.HttpModules;


namespace Neato.Cpt.WebAdmin
{
	public class Categories : System.Web.UI.Page
	{
        protected DataGrid grdCats;

        private class CatItem {
            public string idValue;
            public string Name;
            public DeviceAvailability State;
            public CatItem(string id, string name, DeviceAvailability state) {
                idValue = id;
                Name = name;
                State = state;
            }
            public string Id {
                get { return idValue; }
            }
        }

        #region Url
        private static string RefreshRetailersUrl = CheckLocationModule.AdminUrl(Configuration.IntranetDesignerSiteUrl + RefreshCacheHandler.UrlRefreshRetailers(), null);
        private const string RawUrl = "Categories.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private int retailerId {
            get {return StorageManager.CurrentRetailer.Id;}
        }

		private void Page_Load(object sender, System.EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(this.Page_DataBinding);
            this.grdCats.ItemDataBound += new DataGridItemEventHandler(this.grdCats_ItemDataBound);
            this.grdCats.EditCommand   += new DataGridCommandEventHandler(this.grdCats_EditCommand);
            this.grdCats.UpdateCommand += new DataGridCommandEventHandler(this.grdCats_UpdateCommand);
            this.grdCats.CancelCommand += new DataGridCommandEventHandler(this.grdCats_CancelCommand);
		}
		#endregion

        private void Page_DataBinding(object sender, EventArgs e) {
            Retailer retailer = BCRetailers.GetRetailer(retailerId);
            ArrayList cats = new ArrayList();
            cats.Add(new CatItem("P", "Phone", retailer.PhoneAvailability));
            cats.Add(new CatItem("M", "MP3 Player", retailer.Mp3Availability));
            cats.Add(new CatItem("S", "Sticker", retailer.StickerAvailability));
            grdCats.DataSource = cats;
            grdCats.DataKeyField = "Id";
        }

        private void grdCats_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void ItemDataBound(DataGridItem item) {
            CatItem cat = (CatItem)item.DataItem;

            Label lblItemName = (Label)item.FindControl("lblItemName");
            lblItemName.Text = cat.Name;

            Label lblItemState = (Label)item.FindControl("lblItemState");
            string text = "Not Available";
            if (cat.State == DeviceAvailability.ec_Available) text = "Available";
            if (cat.State == DeviceAvailability.ec_ComingSoon) text = "Coming Soon";
            lblItemState.Text = text;
        }

        private void EditItemDataBound(DataGridItem item) {
            CatItem cat = (CatItem)item.DataItem;
            Label lblItemName = (Label)item.FindControl("lblItemNameEdit");
            lblItemName.Text = cat.Name;

            if (cat.State == DeviceAvailability.ec_NotAvailable) {
                RadioButton btnItemState = (RadioButton)item.FindControl("btnItemState1");
                btnItemState.Checked = true;
            }
            else if (cat.State == DeviceAvailability.ec_Available) {
                RadioButton btnItemState = (RadioButton)item.FindControl("btnItemState2");
                btnItemState.Checked = true;
            }
            else if (cat.State == DeviceAvailability.ec_ComingSoon) {
                RadioButton btnItemState = (RadioButton)item.FindControl("btnItemState3");
                btnItemState.Checked = true;
            }
        }

        private void grdCats_EditCommand(object source, DataGridCommandEventArgs e) {
            grdCats.EditItemIndex = e.Item.ItemIndex;
            DataBind();
        }

        private void grdCats_UpdateCommand(object source, DataGridCommandEventArgs e) {
            //CatItem cat = (CatItem)e.Item.DataItem;
            string catId = (string)grdCats.DataKeys[e.Item.ItemIndex];

            RadioButton btnItemState1 = (RadioButton)e.Item.FindControl("btnItemState1");
            RadioButton btnItemState2 = (RadioButton)e.Item.FindControl("btnItemState2");
            RadioButton btnItemState3 = (RadioButton)e.Item.FindControl("btnItemState3");
            DeviceAvailability state = DeviceAvailability.ec_NotAvailable;
            if (btnItemState1.Checked == true) {
                state = DeviceAvailability.ec_NotAvailable;
            } else if (btnItemState2.Checked == true) {
                state = DeviceAvailability.ec_Available;
            } else if (btnItemState3.Checked == true) {
                state = DeviceAvailability.ec_ComingSoon;
            }

            try {
                Retailer retailer = BCRetailers.GetRetailer(retailerId);
                Retailer[] bnRetailers = BCRetailers.BuyNowRetailerEnum(retailer);

                if (catId == "P") {
                    retailer.PhoneAvailability = state;
                } else if (catId == "M") {
                    retailer.Mp3Availability = state;
                } else if (catId == "S") {
                    retailer.StickerAvailability = state;
                }

                BCRetailers.Update(retailer, bnRetailers);
                RefreshRetailers();
            } 
            catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            grdCats.EditItemIndex = -1;
            DataBind();
        }

        private void grdCats_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdCats.EditItemIndex = -1;
            DataBind();
        }

        public static void RefreshRetailers() {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(RefreshRetailersUrl);
            WebResponse response = request.GetResponse();
            response.Close();
        }
	}
}
