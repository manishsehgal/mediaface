using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebAdmin.Controls;
using Neato.Cpt.WebAdmin.Helpers;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebAdmin {
    public class CustomerList : Page {
        #region Constants
        private const string LblFirstNameItem = "lblFirstNameItem";
        private const string LblLastNameItem = "lblLastNameItem";
        private const string LblEmailItem = "lblEmailItem";
        private const string LblEmailOptions = "lblEmailOptions";
        private const string ChkReceiveInfo = "chkReceiveInfo";
        private const string LblCreated = "lblCreated";
        
        private const string RetailerFilterKey = "RetailerFilterKey";
        private const string AllValue = "-1";
        private const string SelectValue = "-2";
        #endregion

        protected TextBox txtEmail;
        protected TextBox txtFirstName;
        protected TextBox txtLastName;
        protected Label lblResultsCount;
        protected Button btnNewCustomer;
        protected Button btnCustomerSearch;
        protected DataGrid grdCustomers;
        protected DateInterval dtiDates;
        protected DropDownList cbRetailers;

        private ArrayList retailers;
        private Retailer retailer {
            get {
                if(CurrentFilter[RetailerFilterKey] != AllValue &&
                    CurrentFilter[RetailerFilterKey] != SelectValue) {
                    string retailerName =  CurrentFilter[RetailerFilterKey];
                    return BCRetailers.GetRetailerByName(retailerName);
                }
                return null;
            }
        }

        private PageFilter CurrentFilter {
            get { return (PageFilter)ViewState["CurrentFilter"]; }
            set { ViewState["CurrentFilter"] = value; }
        }

        #region Url
        private const string RawUrl = "CustomerList.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                InitFilters();
                DataBind();
            }
        }

        private void InitFilters() {
            CurrentFilter = new PageFilter();
            CurrentFilter[RetailerFilterKey] = Request.QueryString[RetailerFilterKey];
            if (CurrentFilter[RetailerFilterKey] == null) CurrentFilter[RetailerFilterKey] = SelectValue;
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.btnCustomerSearch.Click += new System.EventHandler(this.btnCustomerSearch_Click);
            this.btnNewCustomer.Click += new System.EventHandler(this.btnNewCustomer_Click);
            this.grdCustomers.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.grdCustomers_PageIndexChanged);
            this.grdCustomers.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdCustomers_EditCommand);
            this.grdCustomers.DataBinding += new System.EventHandler(this.grdCustomers_DataBinding);
            this.grdCustomers.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdCustomers_DeleteCommand);
            this.grdCustomers.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdCustomers_ItemDataBound);
            this.cbRetailers.DataBinding +=new EventHandler(cbRetailers_DataBinding);
            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(CustomerList_DataBinding);
        }
        #endregion

        private void btnCustomerSearch_Click(object sender, EventArgs e) {
            DataBind();
        }

        private void btnNewCustomer_Click(object sender, EventArgs e) {
            Response.Redirect(CustomerAccount.Url().PathAndQuery);
        }
        
        private void grdCustomers_PageIndexChanged(object source, DataGridPageChangedEventArgs e) {
            grdCustomers.CurrentPageIndex = e.NewPageIndex;
            grdCustomers.DataBind();
        }

        private void grdCustomers_DataBinding(object sender, EventArgs e) {
            ArrayList customers = (CurrentFilter[RetailerFilterKey] == SelectValue)
                ? new ArrayList() : new ArrayList(
                BCCustomer.CustomerSearch(
                txtFirstName.Text.Trim(),
                txtLastName.Text.Trim(),
                txtEmail.Text.Trim(),
                dtiDates.StartDate,
                dtiDates.EndDate,
                retailer));
            grdCustomers.DataSource = customers;
            grdCustomers.DataKeyField = Customer.FieldId;

            int maxIndex = (customers.Count - 1) / grdCustomers.PageSize;
            maxIndex = Math.Max(0, maxIndex);

            grdCustomers.CurrentPageIndex = Math.Min(grdCustomers.CurrentPageIndex, maxIndex);

            if (customers.Count > 0) {
                lblResultsCount.Visible = true;
                lblResultsCount.Text = string.Format
                    ("{0} customer[s] found.", customers.Count);
            } else {
                lblResultsCount.Visible = false;
            }
        }

        private void grdCustomers_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
            }
        }

        private void ItemDataBound(DataGridItem item) {
            Customer customer = (Customer)item.DataItem;
            Label lblFirstName = (Label)item.FindControl(LblFirstNameItem);
            Label lblLastName = (Label)item.FindControl(LblLastNameItem);
            Label lblEmail = (Label)item.FindControl(LblEmailItem);
            Label lblEmailOptions = (Label)item.FindControl(LblEmailOptions);
            CheckBox chkReceiveInfo = (CheckBox)item.FindControl(ChkReceiveInfo);
            Label lblCreated = (Label)item.FindControl(LblCreated);

            lblFirstName.Text = HttpUtility.HtmlEncode(customer.FirstName);
            lblLastName.Text = HttpUtility.HtmlEncode(customer.LastName);
            lblEmail.Text = HttpUtility.HtmlEncode(customer.Email);
            lblEmailOptions.Text = customer.EmailOptions.ToString();
            chkReceiveInfo.Checked = customer.ReceiveInfo;
            chkReceiveInfo.Enabled = false;
            lblCreated.Text = customer.Created.ToShortDateString();
        }

        private void grdCustomers_EditCommand(object source, DataGridCommandEventArgs e) {
            int customerId = (int)grdCustomers.DataKeys[e.Item.ItemIndex];
            Response.Redirect(CustomerAccount.Url(customerId).PathAndQuery);
        }

        private void grdCustomers_DeleteCommand(object source, DataGridCommandEventArgs e) {
            int customerId = (int)grdCustomers.DataKeys[e.Item.ItemIndex];
            BCCustomer.DeleteCustomer(customerId);
            DataBind();
        }

        private void cbRetailers_DataBinding(object sender, EventArgs e) {
            cbRetailers.Items.Clear();
            
            foreach (Retailer retailer in retailers) {
                ListItem item = new ListItem(retailer.Name);
                cbRetailers.Items.Add(item);
                if (item.Value == CurrentFilter[RetailerFilterKey]) item.Selected = true;
            }
            cbRetailers.Items.Insert(0, new ListItem("All", AllValue));
            if (CurrentFilter[RetailerFilterKey] == SelectValue) {
                cbRetailers.Items.Insert(0, new ListItem("Select", SelectValue));
            }
        }

        private void CustomerList_DataBinding(object sender, EventArgs e) {
            retailers = new ArrayList(BCRetailers.GetRetailers());

            if (IsPostBack) {
                CurrentFilter[RetailerFilterKey ] = cbRetailers.SelectedValue;
            }
        }
    }
}