<%@ Page language="c#" Codebehind="TrackingUserActionReport.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.TrackingUserActionReport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
<title>TrackingUserActionReport</title>
    <meta name=vs_snapToGrid content="True">
<meta name=vs_showGrid content="True">
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </HEAD>
  <body MS_POSITIONING="GridLayout">
	
    <form id="Form1" method="post" runat="server"><br>
    Retailer:  <asp:DropDownList ID="cbRetailers" Runat=server width="120px" AutoPostBack="True"/>
    <br>
        <asp:DataGrid ID="grdReport" Runat="server"
            AllowSorting="False"
            AutoGenerateColumns="True"
            BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1"
            CellPadding="1" CellSpacing="1" 
            Width ="100%"
            AllowPaging="True" 
            PagerStyle-Mode="NumericPages"
            />
     </form>
	
  </body>
</HTML>
