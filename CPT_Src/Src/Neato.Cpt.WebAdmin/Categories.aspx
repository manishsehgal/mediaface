<%@ Page language="c#" Codebehind="Categories.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.Categories" %>
<%@ Register TagPrefix="cpt" NameSpace="Neato.Cpt.WebAdmin.Controls" Assembly="Neato.Cpt.WebAdmin" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
    <head>
        <title>Categories</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name=vs_defaultClientScript content="JavaScript">
        <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
        <link type="text/css" rel="stylesheet" href="css/admin.css" />
    </head>
    <body MS_POSITIONING="GridLayout">
	
        <form id="Form1" method="post" runat="server">
            <h3 align="center">Categories</h3>
            
            <asp:DataGrid Runat="server" ID="grdCats" AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateColumn HeaderText="Category">
                        <HeaderStyle Wrap="True" Width="200px" HorizontalAlign="Center" />
                        <ItemStyle Wrap="True" Width="200px" HorizontalAlign="Left"/>
                        <ItemTemplate>
                            <asp:Label ID="lblItemName" Runat="server" style="font-family:Arial"/>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="lblItemNameEdit" Runat="server" style="font-family:Arial"/>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    
                    <asp:TemplateColumn HeaderText="State">
                        <HeaderStyle Wrap="True" Width="200px" HorizontalAlign="Center" />
                        <ItemStyle Wrap="True" Width="200px" HorizontalAlign="Left"/>
                        <ItemTemplate>
                            <asp:Label ID="lblItemState" Runat="server" style="font-family:Arial"/>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:RadioButton id="btnItemState1" Text="Not Available" Checked="False" GroupName="StateGroup" runat="server" />
                            <br>
                            <asp:RadioButton id="btnItemState2" Text="Available" Checked="False" GroupName="StateGroup" runat="server" />
                            <br>
                            <asp:RadioButton id="btnItemState3" Text="Coming Soon" Checked="False" GroupName="StateGroup" runat="server" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    
                    <asp:TemplateColumn HeaderText="Action">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                        <ItemStyle Wrap="False" Width="70px" />
                        <ItemTemplate>
                            <asp:ImageButton Runat="server" ID="btnEdit" ImageUrl="images/edit.gif" CommandName="Edit" CausesValidation="False" ImageAlign="AbsMiddle" AlternateText="Edit" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton Runat="server" ID="btnUpdate" ImageUrl="images/save.gif" ImageAlign="AbsMiddle" CommandName="Update" CausesValidation="False" AlternateText="Save" />
                            <br>
                            <asp:ImageButton Runat="server" ID="btnCancel" ImageUrl="images/cancel.gif" ImageAlign="AbsMiddle" CommandName="Cancel" CausesValidation="False" AlternateText="Cancel" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>

        </form>
	
    </body>
</html>
