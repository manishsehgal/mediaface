<%@ Register TagPrefix="cpt" TagName="DateInterval" Src="Controls/DateInterval.ascx" %>
<%@ Page language="c#" Codebehind="PhoneRequestReportByDatesAndBrand.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.PhoneRequestReportByDatesAndBrand" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
    <HEAD>
        <title>DeviceRequestReportByDatesAndBrand</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    </HEAD>
    <body MS_POSITIONING="GridLayout">
        <form id="Form1" method="post" runat="server">
            <h3 align="center">�Can�t find my device� requests statistic (devices by date range)</h3>
           <table>
               <tr>
                 <td valign="top">
                    Retailer: 
                 </td>
                 <td> 
                    <asp:DropDownList ID="cbRetailers" Runat=server width="120px" />
                 </td>
                 <td style="width:50;"></td>
                 <td rowspan=3>
                    <cpt:DateInterval id="dtiDates" runat="server" CaptionWidth="90px" DateControlWidth="120px" />
                 </td>
               </tr>
               <tr>
                 <td> 
                    Manufacturer:&nbsp;&nbsp;&nbsp;
                 </td>
                 <td> 
                   <asp:DropDownList ID="cboManufacturerFilter" Runat=server width="120px" />
                 </td>
               <tr>
                 <td></td>
                 <td align="right">
                    <asp:Button ID="btnSearch" text="Search" Runat="server" />
                 </td>
               </tr>
            </table>
            <br>
            <asp:Label ID="lblNoData" Runat="server" Visible="False" ForeColor="Red" />
            <asp:DataGrid ID="grdReport" Runat="server" AutoGenerateColumns="True" />
        </form>
    </body>
</HTML>
