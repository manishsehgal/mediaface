using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebAdmin {
    public class TrackingUserActionReport : Page {
        protected DataGrid grdReport;
        protected DropDownList cbRetailers;
        private static int TableItemsCountPerPage = 25;
        private string currentRetailer = "All";
        #region Url
        private const string RawUrl = "TrackingUserActionReport.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(TrackingUserActionReport_DataBinding);
            grdReport.DataBinding +=new EventHandler(grdReport_DataBinding);
            grdReport.PageIndexChanged +=new DataGridPageChangedEventHandler(grdReport_PageIndexChanged);       
            cbRetailers.DataBinding +=new EventHandler(cbRetailers_DataBinding);
            cbRetailers.SelectedIndexChanged +=new EventHandler(cbRetailers_SelectedIndexChanged);

        }
        #endregion

        private void TrackingUserActionReport_DataBinding(object sender, EventArgs e) {
            grdReport.DataBind();

        }

        private void grdReport_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            grdReport.CurrentPageIndex = e.NewPageIndex;
            grdReport.DataBind();
        }

        private void grdReport_DataBinding(object sender, EventArgs e)
        {
            Retailer retailer = null;
            if(cbRetailers.SelectedIndex>0)
                retailer = BCRetailers.GetRetailers()[cbRetailers.SelectedIndex-1];
            
            DataSet data = BCTracking.GetList(retailer);
            grdReport.DataSource = data;
            grdReport.PageSize = TableItemsCountPerPage;
        }

        private void cbRetailers_DataBinding(object sender, EventArgs e)
        {
            cbRetailers.Items.Clear();
            
            cbRetailers.Items.Insert(0,"All");
            foreach (Retailer retailer in BCRetailers.GetRetailers()) 
            {
                ListItem item = new ListItem(retailer.Name, retailer.Id.ToString());
                cbRetailers.Items.Add(item);
                if (item.Text == currentRetailer)
                    item.Selected = true;
            }
           
        }

        private void cbRetailers_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentRetailer = cbRetailers.SelectedItem.Text;
            grdReport.CurrentPageIndex = 0;
            DataBind();
        }
    }
}