using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebAdmin {
    public class ManageIPFilter : System.Web.UI.Page 
    {
        protected DataGrid grdIPFilter;
        protected Button btnNewIPFilter;

        private const string LblBeginIPAddress = "lblBeginIPAddress";
        private const string LblEndIPAddress = "lblEndIPAddress";
        private const string LblDescription = "lblDescription";
        private const string TxtBeginIPAddress = "txtBeginIPAddress";
        private const string TxtEndIPAddress = "txtEndIPAddress";
        private const string TxtDescription = "txtDescription";

        protected CustomValidator vldBeginIPAddress;
        protected CustomValidator vldEndIPAddress;
        protected CustomValidator vldBeginIPAddressRequired;
        protected CustomValidator vldIPRange;
        
        private const string BtnItemDeleteName = "btnDelete";
        private const string BtnItemEditName = "btnEdit";
        private const string BtnItemUpdateName = "btnUpdate";
        private const string BtnItemCancelName = "btnCancel";

        #region Url
        private const string RawUrl = "ManageIPFilter.aspx";

        public static Uri Url() 
        {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private bool NewFilterMode {
            get { return (bool)ViewState["NewFilterModeKey"]; }
            set { ViewState["NewFilterModeKey"] = value; }
        }


        private void Page_Load(object sender, System.EventArgs e) {
            if(!this.IsPostBack) {
                NewFilterMode = false;
                this.DataBind();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }
		
        private void InitializeComponent() 
        { 
            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManageIPFilter_DataBinding);
            this.btnNewIPFilter.Click += new EventHandler(btnNewIPFilter_Click);
            this.grdIPFilter.ItemDataBound += new DataGridItemEventHandler(grdIPFilter_ItemDataBound);
            this.grdIPFilter.EditCommand += new DataGridCommandEventHandler(grdIPFilter_EditCommand);
            this.grdIPFilter.CancelCommand += new DataGridCommandEventHandler(grdIPFilter_CancelCommand);
            this.grdIPFilter.DeleteCommand += new DataGridCommandEventHandler(grdIPFilter_DeleteCommand);
            this.grdIPFilter.UpdateCommand += new DataGridCommandEventHandler(grdIPFilter_UpdateCommand);
        }
        #endregion

        private void ManageIPFilter_DataBinding(object sender, EventArgs e) {
            ArrayList filters = new ArrayList(BCIPFilter.EnumIPFilters());
            if (NewFilterMode) {
                filters.Insert(0, BCIPFilter.NewIPFilter());
                grdIPFilter.EditItemIndex = 0;
            } 
            /*else if (grdIPFilter.EditItemIndex >= 0) {
                int carrierId = (int)grdIPFilter.DataKeys[grdIPFilter.EditItemIndex];
                int index = filters.IndexOf(new CarrierBase(carrierId));
                grdIPFilter.EditItemIndex = index;
            }*/
            grdIPFilter.DataSource = filters;
            grdIPFilter.DataKeyField = IPFilter.IdField;
        }

        private void grdIPFilter_ItemDataBound(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            switch(item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                    ItemBound(item);
                    break;
                case ListItemType.EditItem:
                    EditItemBound(item);
                    break;
                default:
                    break;
            }
        }
        
        private void ItemBound(DataGridItem item) {
            IPFilter ipFilter = (IPFilter)item.DataItem;

            Label lblBeginIPAddress = (Label)item.FindControl(LblBeginIPAddress);
            Label lblEndIPAddress = (Label)item.FindControl(LblEndIPAddress);
            Label lblDescription = (Label)item.FindControl(LblDescription);
            ImageButton btnEdit = (ImageButton)item.FindControl(BtnItemEditName);
            ImageButton btnUpdate = (ImageButton)item.FindControl(BtnItemUpdateName);
            ImageButton btnCancel = (ImageButton)item.FindControl(BtnItemCancelName);
            ImageButton btnDelete = (ImageButton)item.FindControl(BtnItemDeleteName);

            lblBeginIPAddress.Text = ipFilter.BeginIPAddress.ToString();
            lblEndIPAddress.Text = ipFilter.EndIPAddress.ToString();
            lblDescription.Text = ipFilter.Description;

            bool edit = item.ItemType == ListItemType.EditItem;
            btnEdit.Visible = !edit;
            btnDelete.Visible = !edit;
            btnUpdate.Visible = edit;
            btnCancel.Visible = edit;
        }

        private void EditItemBound(DataGridItem item) {
            IPFilter ipFilter = (IPFilter)item.DataItem;

            TextBox txtBeginIPAddress = (TextBox)item.FindControl(TxtBeginIPAddress);
            TextBox txtEndIPAddress = (TextBox)item.FindControl(TxtEndIPAddress);
            TextBox txtDescription = (TextBox)item.FindControl(TxtDescription);
            ImageButton btnEdit = (ImageButton)item.FindControl(BtnItemEditName);
            ImageButton btnUpdate = (ImageButton)item.FindControl(BtnItemUpdateName);
            ImageButton btnCancel = (ImageButton)item.FindControl(BtnItemCancelName);
            ImageButton btnDelete = (ImageButton)item.FindControl(BtnItemDeleteName);

            txtBeginIPAddress.Text = ipFilter.BeginIPAddress.ToString();
            txtEndIPAddress.Text = ipFilter.EndIPAddress.ToString();
            txtDescription.Text = ipFilter.Description;

            bool edit = item.ItemType == ListItemType.EditItem;
            btnEdit.Visible = !edit;
            btnDelete.Visible = !edit;
            btnUpdate.Visible = edit;
            btnCancel.Visible = edit;
        }

        private void btnNewIPFilter_Click(object sender, EventArgs e) {
            NewFilterMode = true;
            DataBind();
        }

        private void grdIPFilter_EditCommand(object source, DataGridCommandEventArgs e) {
            grdIPFilter.EditItemIndex = e.Item.ItemIndex - (NewFilterMode ? 1 : 0);
            NewFilterMode = false;
            DataBind();
        }

        private void grdIPFilter_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdIPFilter.EditItemIndex = -1;
            NewFilterMode = false;
            DataBind();
        }

        private void grdIPFilter_DeleteCommand(object source, DataGridCommandEventArgs e) {
            IPFilter ipFilter = BCIPFilter.NewIPFilter();
            ipFilter.Id = (int)grdIPFilter.DataKeys[e.Item.ItemIndex];
            BCIPFilter.DeleteIPFilter(ipFilter);

            grdIPFilter.EditItemIndex = -1;
            NewFilterMode = false;
            DataBind();
        }

        private void grdIPFilter_UpdateCommand(object source, DataGridCommandEventArgs e) {
            DataGridItem item = e.Item;

            if(!(vldBeginIPAddress.IsValid && vldEndIPAddress.IsValid && vldBeginIPAddressRequired.IsValid && vldIPRange.IsValid)) {
                return;
            }

            TextBox txtBeginIPAddress = (TextBox)item.FindControl(TxtBeginIPAddress);
            TextBox txtEndIPAddress = (TextBox)item.FindControl(TxtEndIPAddress);
            TextBox txtDescription = (TextBox)item.FindControl(TxtDescription);

            IPFilter ipFilter = BCIPFilter.NewIPFilter();
            ipFilter.BeginIPAddress = IPAddress.ToIPAddress(txtBeginIPAddress.Text);
            ipFilter.EndIPAddress = IPAddress.ToIPAddress(txtEndIPAddress.Text != string.Empty ? txtEndIPAddress.Text : txtBeginIPAddress.Text);
            ipFilter.Description = txtDescription.Text;

            if(NewFilterMode) {
                BCIPFilter.InsertIPFilter(ipFilter);
            }
            else {
                ipFilter.Id = (int)grdIPFilter.DataKeys[e.Item.ItemIndex];
                BCIPFilter.UpdateIPFilter(ipFilter);
            }

            grdIPFilter.EditItemIndex = -1;
            NewFilterMode = false;
            DataBind();
        }

        protected void BeginIPAddressRequiredServerValidate(object source, ServerValidateEventArgs args) {
            TextBox txtBeginIPAddress = (TextBox)grdIPFilter.Items[grdIPFilter.EditItemIndex].FindControl(TxtBeginIPAddress);
            args.IsValid = txtBeginIPAddress.Text.Length != 0;
        }

        protected void BeginIPAddressServerValidate(object source, ServerValidateEventArgs args) {
            TextBox txtBeginIPAddress = (TextBox)grdIPFilter.Items[grdIPFilter.EditItemIndex].FindControl(TxtBeginIPAddress);
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(IPAddress.IPMatchExpression);
            args.IsValid = regex.IsMatch(txtBeginIPAddress.Text);
        }

        protected void EndIPAddressServerValidate(object source, ServerValidateEventArgs args) {
            TextBox txtEndIPAddress = (TextBox)grdIPFilter.Items[grdIPFilter.EditItemIndex].FindControl(TxtEndIPAddress);
            if(txtEndIPAddress.Text.Length != 0) {
                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(IPAddress.IPMatchExpression);
                args.IsValid = regex.IsMatch(txtEndIPAddress.Text);
            }
        }

        protected void IPRangeServerValidate(object source, ServerValidateEventArgs args) {
            if(!(vldBeginIPAddress.IsValid && vldEndIPAddress.IsValid && vldBeginIPAddressRequired.IsValid)) 
                return;

            TextBox txtBeginIPAddress = (TextBox)grdIPFilter.Items[grdIPFilter.EditItemIndex].FindControl(TxtBeginIPAddress);
            TextBox txtEndIPAddress = (TextBox)grdIPFilter.Items[grdIPFilter.EditItemIndex].FindControl(TxtEndIPAddress);
            IPAddress beginIPIddress = IPAddress.ToIPAddress(txtBeginIPAddress.Text);
            IPAddress endIPIddress;
            if(txtEndIPAddress.Text.Length == 0)
                endIPIddress = IPAddress.ToIPAddress(txtBeginIPAddress.Text);
            else
                endIPIddress = IPAddress.ToIPAddress(txtEndIPAddress.Text);
            args.IsValid = endIPIddress.CompareTo(beginIPIddress) >= 0;
        }
    }
}
