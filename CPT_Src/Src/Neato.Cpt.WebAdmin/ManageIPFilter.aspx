<%@ Page language="c#" Codebehind="ManageIPFilter.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.ManageIPFilter" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>ManageIPFilter</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
	
    <form id="Form1" method="post" runat="server">
      <h3 align="center">IP filters</h3>
      <br>
		<asp:Button Runat="server" ID="btnNewIPFilter" Text="New IP Filter" CausesValidation="False" />
		<div>
			<asp:CustomValidator Runat="server" ID="vldBeginIPAddressRequired"
				OnServerValidate="BeginIPAddressRequiredServerValidate"
				ErrorMessage="Start IP address is required" />
		</div>
		<div>
			<asp:CustomValidator Runat="server" ID="vldBeginIPAddress"
				OnServerValidate="BeginIPAddressServerValidate"
				ErrorMessage="Start IP address is not valid"/>
		</div>
		<div>
			<asp:CustomValidator Runat="server" ID="vldEndIPAddress"
				OnServerValidate="EndIPAddressServerValidate"
				ErrorMessage="End IP address is not valid" />
		</div>
		<div>
			<asp:CustomValidator Runat="server" ID="vldIPRange"
				OnServerValidate="IPRangeServerValidate"
				ErrorMessage="Defined IP range is not valid" />
		</div>
		<p>Define range (specify IP):</p>
		<asp:DataGrid Runat="server" ID="grdIPFilter" AutoGenerateColumns="False"
		    AllowPaging="False" AllowSorting="False"
            BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1"
            CellPadding="1" CellSpacing="1" 
            Width ="100%" HeaderStyle-Font-Bold="True" HeaderStyle-HorizontalAlign="Center">
			<Columns>
				<asp:TemplateColumn HeaderText="Start from">
					<ItemTemplate>
						<asp:Label Runat="server" ID="lblBeginIPAddress" />
					</ItemTemplate>
					<EditItemTemplate>
						<asp:TextBox Runat="server" ID="txtBeginIPAddress" />
					</EditItemTemplate>
				</asp:TemplateColumn>
				<asp:TemplateColumn HeaderText="Up to">
					<ItemTemplate>
						<asp:Label Runat="server" ID="lblEndIPAddress" />
					</ItemTemplate>
					<EditItemTemplate>
						<asp:TextBox Runat="server" ID="txtEndIPAddress" />
					</EditItemTemplate>
				</asp:TemplateColumn>
				<asp:TemplateColumn HeaderText="Description">
					<ItemTemplate>
						&nbsp;<asp:Label Runat="server" ID="lblDescription" />
					</ItemTemplate>
					<EditItemTemplate>
						<asp:TextBox Runat="server" ID="txtDescription" />
					</EditItemTemplate>
				</asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Action">
                    <ItemTemplate>
                        <asp:ImageButton Runat="server" ID="btnEdit"
                            ImageUrl="images/edit.gif" CommandName="Edit" CausesValidation="False"
                            ImageAlign="AbsMiddle" AlternateText="Edit" />
                        <asp:ImageButton Runat="server" ID="btnDelete" ImageUrl="images/remove.gif" CommandName="Delete"
                            CausesValidation="False" ImageAlign="AbsMiddle" AlternateText="Remove" />
                        <asp:ImageButton Runat="server" ID="btnUpdate"
                            ImageUrl="images/save.gif" CommandName="Update" CausesValidation="True"
                            ImageAlign="AbsMiddle" AlternateText="Save" />
                        <asp:ImageButton Runat="server" ID="btnCancel"
                            ImageUrl="images/cancel.gif" CommandName="Cancel"
                            CausesValidation="False" ImageAlign="AbsMiddle" AlternateText="Cancel" />
                    </ItemTemplate>
                </asp:TemplateColumn>
			</Columns>
		</asp:DataGrid>
     </form>
	
  </body>
</html>
