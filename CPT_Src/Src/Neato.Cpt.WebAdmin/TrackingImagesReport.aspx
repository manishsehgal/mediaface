<%@ Page language="c#" Codebehind="TrackingImagesReport.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.TrackingImagesReport" %>
<%@ Register TagPrefix="cpt" TagName="DateInterval" Src="Controls/DateInterval.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Tracking Images Report</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
	
    <form id="Form1" method="post" runat="server">
        <h3 align="center"><asp:Label Runat=server ID="lblTitle"/></h3>
            <table>
               <tr>
                 <td valign="top">
                    Retailer:&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="cbRetailers" Runat=server width="120px" />
                 </td>
                 <td style="width:50;"></td>
                 <td rowspan=2>
                    <cpt:DateInterval id="dtiDates" runat="server" CaptionWidth="90px" DateControlWidth="120px" />
                 </td>
               </tr>
               <tr>
                 <td align="right">
                    <asp:Button ID="btnSubmit" text="Search" Runat="server" />
                 </td>
               </tr>
            </table>
            <br>
            <asp:DataGrid ID="grdReport" Runat="server"
            AllowPaging="False" AllowSorting="False"
            BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1" AutoGenerateColumns ="False"
            CellPadding="1" CellSpacing="1" HeaderStyle-Font-Bold=True HeaderStyle-HorizontalAlign=Center
            Width ="80%"
            > 
             <Columns>
                    <asp:TemplateColumn HeaderText="Image">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                        <ItemTemplate>
                        <table><tr>
                        <td align ="left" valign="center">
                            <asp:Image ID="imgIcon" Runat="server" BorderStyle="Solid" BorderWidth="1" BorderColor="#d4d0c8"
                                    CssClass="imageIcon" />
                           </td><td align ="left" valign="center"> Directories:
                            </td>
                            <td align ="left" valign="center"><asp:Label ID = "lblLinks" Runat ="server"/></td>
                            </tr>
                        </table>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Image">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Literal ID = "litCount" Runat ="server"/>
                        </ItemTemplate>
                    </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
        <br/>
        <asp:Label Runat=server ID="lblStatus"/>
     </form>
	
  </body>
</html>
