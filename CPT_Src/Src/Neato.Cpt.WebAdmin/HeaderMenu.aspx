<%@ Page language="c#" Codebehind="HeaderMenu.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.HeaderMenu" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Top menu management</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
    <h3 align="center">Top menu</h3>
      <table>
        <tr>
          <td>
            Background color: 
          </td>
          <td>
            <asp:TextBox ID="txtBackgroundColor" Runat="server" MaxLength="7"/>
          </td>
          <td>
            <asp:Button ID="btnApplyBackgroundColor" Runat="server" Text="Apply background color"/>
          </td>
        </tr>
        <tr>
          <td/>
          <td>
            <asp:RequiredFieldValidator ID="reqBackgroundColor" Runat="server" ControlToValidate="txtBackgroundColor"
                Display="Dynamic"
                ErrorMessage="Background color is required"
                />
            <asp:RegularExpressionValidator ID="vldBackgroundColor" Runat="server" ControlToValidate="txtBackgroundColor"
                Display="Dynamic"
                ValidationExpression="#([0-9a-fA-F]{6})"
                ErrorMessage="use format #xxxxxx"
                />
          </td>
          <td/>  
        </tr>
      </table>
      <br>
      <span>Page URL:</span>
      <asp:DropDownList Runat="server" ID="cboPages" AutoPostBack="True" />
      &nbsp;
      <asp:CheckBox Runat="server" ID="chkUseDefaultMenu" Text="Use Default Top Menu" AutoPostBack="True" />
      <br><br>

      <asp:Button ID="btnNew" Runat="server" Text="New Link" CausesValidation="False"/>
      <br><br>
      <asp:DataGrid
        Runat="server"
        ID="grdLinks"
        AutoGenerateColumns="False">
        <Columns>
          <asp:TemplateColumn HeaderText="Link Name">
            <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="150px" />
            <ItemTemplate>
                <asp:Label ID="lblLinkName" Runat="server" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtLinkName" Runat="server" MaxLength="100" style="width:100%" />
            </EditItemTemplate>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Link URL">
            <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="150px" />
            <ItemTemplate>
                <asp:Label ID="lblLinkUrl" Runat="server" />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="txtLinkUrl" Runat="server" MaxLength="100" style="width:100%" />
            </EditItemTemplate>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Open in New Window">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:CheckBox ID="chkIsNewWindow" Runat="server" />
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center"/>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Show at Public Site">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:CheckBox ID="chkIsVisible" Runat="server" />
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center"/>
          </asp:TemplateColumn>
          <asp:TemplateColumn HeaderText="Action">
            <ItemTemplate>
              <asp:ImageButton
                Runat="server"
                ID="btnItemEdit"
                ImageUrl="images/edit.gif"
                CommandName="Edit"
                CausesValidation="False"
                ImageAlign="AbsMiddle"
                AlternateText="Edit" />
              <asp:ImageButton
                Runat="server"
                ID="btnItemUpdate"
                ImageUrl="images/save.gif"
                ImageAlign="AbsMiddle"
                CommandName="Update"
                CausesValidation="False"
                AlternateText="Save" />
              <asp:ImageButton
                Runat="server"
                ID="btnItemCancel"
                ImageUrl="images/cancel.gif"
                ImageAlign="AbsMiddle"
                CommandName="Cancel"
                CausesValidation="False"
                AlternateText="Cancel" />
              <asp:ImageButton
                Runat="server"
                ID="btnItemDelete"
                ImageUrl="images/remove.gif"
                ImageAlign="AbsMiddle"
                CommandName="Delete"
                CausesValidation="False"
                AlternateText="Remove" />
              <asp:ImageButton
                Runat="server"
                ID="btnItemUp"
                ImageUrl="images/up.gif"
                ImageAlign="AbsMiddle"
                OnClick="btnItemUp_Click"
                CausesValidation="False"
                AlternateText="Up" />
              <asp:ImageButton
                Runat="server"
                ID="btnItemDown"
                ImageUrl="images/down.gif"
                ImageAlign="AbsMiddle"
                OnClick="btnItemDown_Click"
                CausesValidation="False"
                AlternateText="Down" />
            </ItemTemplate>
            <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="100px"/>
            <ItemStyle Wrap="False"/>
          </asp:TemplateColumn>
        </Columns>
      </asp:DataGrid>    
    </form>
  </body>
</html>
