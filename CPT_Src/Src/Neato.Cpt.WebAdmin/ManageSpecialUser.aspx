<%@ Page language="c#" Codebehind="ManageSpecialUser.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.ManageSpecialUser" %>
<%@ Register TagPrefix="cpt" NameSpace="Neato.Cpt.WebAdmin.Controls" Assembly="Neato.Cpt.WebAdmin" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
    <head>
        <title>ManageSpecialUser</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name=vs_defaultClientScript content="JavaScript">
        <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
        <link type="text/css" rel="stylesheet" href="css/admin.css" />
    </head>
    <body MS_POSITIONING="GridLayout">
	
        <form id="Form1" method="post" runat="server">
            <h3 align="center">Special User Management</h3>
            <table border="0" cellpadding="4" cellspacing="0">
                <tr valign="top">
                    <td valign="center">User E-Mail :<br>(substring)</td>
                    <td>
                        <asp:TextBox ID="txtEmail" Runat="server" style="width:250px" />
                    </td>
                </tr>                
                <tr>
                     <td></td>
                     <td align="right">
                        <asp:Button runat="server" ID="btnSearch" Text="Search" />
                    </td>
                </tr>
            </table>
            <br>
            <asp:Button ID="btnNew" Runat="server" Text="New Special User" CausesValidation="False" />
            <br>
            <br>
            
            <asp:DataGrid Runat="server" ID="grdUsers" AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateColumn HeaderText="E-Mail">
                        <HeaderStyle Wrap="True" Width="300px" HorizontalAlign="Center" />
                        <ItemStyle Wrap="True" Width="300px" HorizontalAlign="Center"/>
                        <ItemTemplate>
                            <asp:Label ID="lblItemEmail" Runat="server" style="font-family:Arial"/>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtItemEmail" Runat="server" MaxLength="100" style="width:100%" />
        					<asp:RequiredFieldValidator ControlToValidate = "txtItemEmail" Display="Dynamic" 
		        			    ErrorMessage="E-Mail field can't be empty!"
				        		Runat="server" ID="vldEmailAddress" EnableClientScript="False"/>
					        <asp:CustomValidator ControlToValidate="txtItemEmail" Display="Dynamic" 
					            ErrorMessage="Incorrect E-Mail format!" OnServerValidate="vldValidEmail_ServerValidate"
        						Runat="server" ID="vldValidEmail" EnableClientScript="False"/>
		        			<asp:CustomValidator ControlToValidate = "txtItemEmail" Display="Dynamic" 
        					    ErrorMessage="This E-Mail is already exists!" OnServerValidate="vldUniqueEmail_ServerValidate"
		        				Runat="server" ID="vldUniqueEmail" EnableClientScript="False"/>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    
                    <asp:TemplateColumn HeaderText="View papers (all modes: active/inactive/test)">
                        <HeaderStyle Wrap="True" Width="100px" HorizontalAlign="Center" />
                        <ItemStyle Wrap="True" Width="100px" HorizontalAlign="Center"/>
                        <ItemTemplate>
                            &nbsp;<asp:Label ID="lblItemShowPaper" Runat="server" style="font-size:20pt; font-weight:bold"/>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkItemShowPaper" Runat="server"/>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    
                    <asp:TemplateColumn HeaderText="Notify">
                        <HeaderStyle Wrap="True" Width="100px" HorizontalAlign="Center" />
                        <ItemStyle Wrap="True" Width="100px" HorizontalAlign="Center"/>
                        <ItemTemplate>
                            &nbsp;<asp:Label ID="lblItemNotify" Runat="server" style="font-size:20pt; font-weight:bold"/>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkItemNotify" Runat="server"/>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    
                    <asp:TemplateColumn HeaderText="Action">
                        <ItemTemplate>
                            <asp:Button Runat="server" ID="btnEdit" CommandName="Edit" CausesValidation="False"
                                Text="Edit" Tooltip="Edit paper" />
                            <br>
                            <asp:Button Runat="server" ID="btnDelete" CommandName="Delete"
                                CausesValidation="False" Text="Remove" Tooltip="Remove paper" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button Runat="server" ID="btnUpdate" Text="Save" CommandName="Update" CausesValidation="True"
                                Tooltip="Save paper" />
                            <br>
                            <asp:Button Runat="server" ID="btnCancel" CommandName="Cancel" Text="Cancel"
                                CausesValidation="False" Tooltip="Cancel" />
                        </EditItemTemplate>
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                        <ItemStyle Wrap="False" Width="70px" />
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
            
        </form>
	
    </body>
</html>
