<%@ Page language="c#" Codebehind="Notifications.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.Notifications" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Notifications</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
	
    <form id="Form1" method="post" runat="server">
    
        <h3 align="center">E-mail content</h3>   
        <asp:DataGrid ID="grdMailFormat" Runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateColumn HeaderText="Notification name">
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="100px"/>
                    <ItemTemplate>
                        <asp:Label ID="lblEvent" Runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Subject">
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="120px"/>
                    <ItemTemplate>
                        <asp:Label ID="lblSubject" Runat="server" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtSubject" Runat="server" TextMode="MultiLine" Rows="5" MaxLength="100" style="width:100%" />
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Text">
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="270px"/>
                    <ItemTemplate>
                        <asp:TextBox ID="txtBody" Runat="server" TextMode="MultiLine" Rows="5" MaxLength="1000" ReadOnly="True" style="width:100%" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBodyEdit" Runat="server" TextMode="MultiLine" Rows="5" MaxLength="1000" style="width:100%" />
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Available variables">
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="170px"/>
                    <ItemTemplate>
                        <asp:TextBox ID="txtVariables" Runat="server" TextMode="MultiLine" Rows="5" ReadOnly="True" style="width:100%" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                
                <asp:TemplateColumn HeaderText="Action">
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="100px"/>
                    <ItemTemplate>
                        <asp:ImageButton
                        Runat="server"
                        ID="btnItemEdit"
                        ImageUrl="images/edit.gif"
                        CommandName="Edit"
                        CausesValidation="False"
                        AlternateText="Edit" />
                        <asp:ImageButton
                        Runat="server"
                        ID="btnItemTest"
                        ImageUrl="images/test.gif"
                        CommandName="Test"
                        CausesValidation="False"
                        AlternateText="Test" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:ImageButton
                        Runat="server"
                        ID="btnItemUpdate"
                        ImageUrl="images/save.gif"
                        CommandName="Update"
                        CausesValidation="False"
                        AlternateText="Save" />
                        <asp:ImageButton
                        Runat="server"
                        ID="btnItemCancel"
                        ImageUrl="images/cancel.gif"
                        CommandName="Cancel"
                        CausesValidation="False"
                        AlternateText="Cancel" />
                    </EditItemTemplate>                    
                <ItemStyle Wrap="False" HorizontalAlign="Center"/>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
        
        <br>
        Enter E-mail for test:&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtTestEmail" Runat="server" Width="200px" />
        <p style="COLOR: #CC3300">Note: you can use variables in template for the body of messages. 
        <br>Variables that can be used in each template are enumerated in "Available variables" column.</p>
     </form>
	
  </body>
</html>
