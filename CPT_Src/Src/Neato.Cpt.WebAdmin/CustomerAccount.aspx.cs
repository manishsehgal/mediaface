using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.Entity.Tracking;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebAdmin {
    public class CustomerAccount : Page {
        #region Constants
        private const string CustomerIdKey = "customerId";
        #endregion

        protected TextBox txtFirstName;
        protected RequiredFieldValidator vldFirstName;
        protected TextBox txtLastName;
        protected RequiredFieldValidator vldLastName;
        protected TextBox txtEmailAddress;
        protected RequiredFieldValidator vldEmailAddress;
        protected CustomValidator vldValidEmail;
        protected CustomValidator vldUniqueEmail;
        protected DropDownList cboEmailOptions;
        protected CheckBox chkReceiveInfo;
        protected Button btnResetPassword;
        protected Button btnSave;
        protected Button btnClose;
        protected Label lblMessage;
        protected DropDownList cboRetailers;
        private ArrayList retailers;


        private Customer customer {
            get { return (Customer)(ViewState[CustomerIdKey]); }
            set { ViewState[CustomerIdKey] = value; }
        }

        private Retailer CustomerRetailer {
            get {
                return new Retailer(int.Parse(cboRetailers.SelectedItem.Value));
            }
        }

        #region Url
        private const string RawUrl = "CustomerAccount.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri Url(int customerId) {
            return UrlHelper.BuildUrl(RawUrl, CustomerIdKey, customerId);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.vldValidEmail.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(this.vldValidEmail_ServerValidate);
            this.vldUniqueEmail.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(this.vldUniqueEmail_ServerValidate);
            this.btnResetPassword.Click += new System.EventHandler(this.btnResetPassword_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new System.EventHandler(this.CustomerAccount_DataBinding);

        }
        #endregion

        private void CustomerAccount_DataBinding(object sender, EventArgs e) {
            cboEmailOptions.Items.Clear();
            cboEmailOptions.Items.Add(EmailOptions.Text.ToString());
            cboEmailOptions.Items.Add(EmailOptions.Html.ToString());

            int customerId = Convert.ToInt32(Request.QueryString[CustomerIdKey]);
            customer = BCCustomer.GetCustomerById(customerId);
            if (customer == null)
                customer = new Customer();

            retailers = new ArrayList(BCRetailers.GetRetailers());
            cboRetailers.Items.Clear();
            cboRetailers.DataSource = retailers;
            cboRetailers.DataTextField = "Name";
            cboRetailers.DataValueField = "Id";

            int index = 0;
            foreach (Retailer retailer in retailers) {
                if (retailer.Id == customer.RetailerId)
                    break; else index++;
            }

            cboRetailers.SelectedIndex = (index < retailers.Count) ? index : 0;

            txtFirstName.Text = customer.FirstName;
            txtLastName.Text = customer.LastName;
            txtEmailAddress.Text = customer.Email;
            cboEmailOptions.SelectedValue = customer.EmailOptions.ToString();
            chkReceiveInfo.Checked = customer.ReceiveInfo;
        }

        private void btnSave_Click(object sender, EventArgs e) {
            lblMessage.Visible = false;
            if (!IsValid) {
                return;
            }

            customer.FirstName = txtFirstName.Text.Trim();
            customer.LastName = txtLastName.Text.Trim();
            customer.Email = txtEmailAddress.Text.Trim();
            if (cboEmailOptions.SelectedValue == EmailOptions.Html.ToString()) {
                customer.EmailOptions = EmailOptions.Html;
            } else {
                customer.EmailOptions = EmailOptions.Text;
            }
            customer.ReceiveInfo = chkReceiveInfo.Checked;

            try {
                if (customer.CustomerId != 0) {
                    BCCustomer.UpdateCustomer(customer, CustomerRetailer);
                    lblMessage.Text = "Account was successfully updated!";
                } else {
                    BCCustomer.InsertCustomer(customer, null, CustomerRetailer);
                    BCTracking.Add(UserAction.Registration, customer.Email, Request.UserHostAddress, CustomerRetailer);
                    lblMessage.Text = "Account was successfully added!";
                }
            } catch (BusinessException ex) {
                lblMessage.Text = ex.Message;
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            lblMessage.Visible = true;
        }

        private void btnClose_Click(object sender, EventArgs e) {
            Response.Redirect(CustomerList.Url().PathAndQuery);
        }

        private void btnResetPassword_Click(object sender, EventArgs e) {
            lblMessage.Visible = false;
            if (!IsValid) {
                return;
            }

            try {
                string email = txtEmailAddress.Text.Trim();
                BCMail.SendForgotPasswordMail(Configuration.PublicDesignerSiteUrl,
                    WebDesigner.ForgotPassword.PageName(),
                    email.Trim(), BCDynamicLink.DefaultCulture,
                    CustomerRetailer);
                lblMessage.Text = "E-Mail was successfully sent!";
            } catch (BaseBusinessException ex) {
                lblMessage.Text = ex.Message;
            }
            lblMessage.Visible = true;
        }

        private void vldUniqueEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            Customer found = BCCustomer.GetCustomerByLogin
                (txtEmailAddress.Text.Trim(), CustomerRetailer);
            if (customer.CustomerId != 0) {
                args.IsValid = found == null || found.CustomerId == customer.CustomerId;
            } else {
                args.IsValid = found == null;
            }

        }

        private void vldValidEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = FormatValidator.EnsureValidEmail(txtEmailAddress.Text.Trim());
        }
    }
}
