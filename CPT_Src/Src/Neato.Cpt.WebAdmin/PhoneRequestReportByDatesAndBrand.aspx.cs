using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebAdmin.Controls;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebAdmin {
    public class PhoneRequestReportByDatesAndBrand : Page {
        protected DropDownList cboManufacturerFilter;
        protected DateInterval dtiDates;
        protected Button btnSearch;
        protected Label lblNoData;
        protected DataGrid grdReport;
        protected DropDownList cbRetailers;

        private string currentRetailer = "All";

        public int ManufacturerId {
            get { return Convert.ToInt32
                      (cboManufacturerFilter.SelectedValue); }
        }

        public string ManufacturerName {
            get { return (ManufacturerId == 0) ? null :
                      cboManufacturerFilter.SelectedItem.Text; }
        }

        public bool OtherManufacturer {
            get { return (ManufacturerId == -1); }
        }


        #region Url
        private const string RawUrl = "PhoneRequestReportByDatesAndBrand.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

 
        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new System.EventHandler(this.PhoneRequestReportByDatesAndBrand_DataBinding);
            this.btnSearch.Click += new EventHandler(btnSearch_Click);
            cbRetailers.DataBinding +=new EventHandler(cbRetailers_DataBinding);

        }
        #endregion

        private void PhoneRequestReportByDatesAndBrand_DataBinding(object sender, EventArgs e) {
            ArrayList brands =
                new ArrayList(BCDeviceBrand.GetDeviceBrandList());

            cboManufacturerFilter.Items.Add(new ListItem("All", "0"));

            foreach(DeviceBrand brand in brands) {
                cboManufacturerFilter.Items.Add
                    (new ListItem(brand.Name, brand.Id.ToString()));

            }
            cboManufacturerFilter.Items.Add(new ListItem("Other", "-1"));
            lblNoData.Text = "There are no results matching your criteria.";
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            if (IsValid) {
                lblNoData.Visible = false;
                grdReport.Visible = false;
                currentRetailer = cbRetailers.SelectedItem.Text;
                Retailer retailer = null;
                if(cbRetailers.SelectedIndex>0)
                    retailer = BCRetailers.GetRetailers()[cbRetailers.SelectedIndex-1];
                DataSet data = BCDeviceRequest.
                    GetPhoneRequestReportByDatesAndBrand(
                    ManufacturerName, OtherManufacturer,
                    dtiDates.StartDate, dtiDates.EndDate, retailer);

                bool dataExisting = (data.Tables[0].Rows.Count > 0);

                if (dataExisting) grdReport.DataSource = data;
                grdReport.Visible = dataExisting;
                lblNoData.Visible = !dataExisting;

                DataBind();
            }
        }
        private void cbRetailers_DataBinding(object sender, EventArgs e)
        {
            cbRetailers.Items.Clear();
            
            cbRetailers.Items.Insert(0,"All");
            foreach (Retailer retailer in BCRetailers.GetRetailers()) 
            {
                ListItem item = new ListItem(retailer.Name, retailer.Id.ToString());
                cbRetailers.Items.Add(item);
                if (item.Text == currentRetailer)
                    item.Selected = true;
            }
           
        }
    }
}