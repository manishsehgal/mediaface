<%@ Page language="c#" Codebehind="LogoManagement.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.LogoManagement" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Logo management</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <script type="text/javascript" language="javascript" src="js/RefreshContent.js"></script>
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
    <h3 align="center">Logo management</h3>
    <br><br>
    <table>
      <tr>
        <td>Current logo:</td>
        <td>
          <asp:Image ID="imgLogo" Runat="server" BorderStyle="Solid" BorderWidth="1" BorderColor="#d4d0c8" />
        </td>
      </tr>
      <tr>
        <td align ="center" colspan="2">
          <asp:Panel ID="pnlManage" Runat="server">
            <asp:Button ID="btnDelete" Runat="server" Text="Delete" />
            <asp:Button ID="btnNew" Runat="server" Text="New" />
          </asp:Panel>
          <asp:Panel ID="pnlUpload" Runat="server">
            <input ID="ctlLogoFile" Runat="server" type="file" />
            <asp:Button ID="btnUpload" Runat="server" Text="Upload" />
            <asp:Button ID="btnCancel" Runat="server" Text="Cancel" />
          </asp:Panel>
        </td>
      </tr>
    </table>
    </form>
  </body>
</html>
