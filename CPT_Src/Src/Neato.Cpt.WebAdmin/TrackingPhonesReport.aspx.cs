using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebAdmin.Controls;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebAdmin {
    public class TrackingPhonesReport : Page {
        protected DataGrid grdReport;
        protected DropDownList cbManufacturers;
        protected Label lblTitle;
        protected Label lblStatus;
        protected DateInterval dtiDates;
        protected DropDownList cbRetailers;

        protected Button btnSubmit;
        private string currentRetailer = "All";

        #region Url
        private const string RawUrl = "TrackingPhonesReport.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(TrackingPhonesReport_DataBinding);
            btnSubmit.Click += new EventHandler(btnSubmit_Click);
            cbRetailers.DataBinding +=new EventHandler(cbRetailers_DataBinding);
        }
        #endregion

        private void TrackingPhonesReport_DataBinding(object sender, EventArgs e) {
            Retailer retailer = null;
            if(cbRetailers.SelectedIndex>0)
                retailer = BCRetailers.GetRetailers()[cbRetailers.SelectedIndex-1];
            DataSet Carriersdata = BCTrackingPhones.GetCarriersList(retailer);

            lblTitle.Text = "What models have been chosen";

            int selIndex = cbManufacturers.SelectedIndex;
            cbManufacturers.Items.Clear();
            cbManufacturers.Items.Add("All");
            foreach (DataTable Tables in Carriersdata.Tables) {
                foreach (DataRow Rows in Tables.Rows) {
                    foreach (DataColumn Column in Tables.Columns) {
                        cbManufacturers.Items.Add((string)Rows[Column]);
                    }
                }
            }
            if (selIndex >= 0)
                cbManufacturers.SelectedIndex = selIndex;
            lblStatus.Text = "There are no results matching your criteria.";
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            if (IsValid) {
                DeviceBrand brand;
                if (cbManufacturers.SelectedItem.Text == "All")
                    brand = null;
                else {
                    brand = new DeviceBrand(0, cbManufacturers.SelectedItem.Text);

                }
                currentRetailer = cbRetailers.SelectedItem.Text;

                lblStatus.Visible = false;
                grdReport.Visible = false;
                Retailer retailer = null;
                if(cbRetailers.SelectedIndex>0)
                    retailer = BCRetailers.GetRetailers()[cbRetailers.SelectedIndex-1];
                DataSet data = BCTrackingPhones.GetListByBrandAndDate
                    (brand, dtiDates.StartDate, dtiDates.EndDate, retailer);

                bool dataExisting = (data.Tables[0].Rows.Count > 0);

                if (dataExisting) grdReport.DataSource = data;
                grdReport.Visible = dataExisting;
                lblStatus.Visible = !dataExisting;

                DataBind();
            }
        }
        private void cbRetailers_DataBinding(object sender, EventArgs e)
        {
            cbRetailers.Items.Clear();
            
            cbRetailers.Items.Insert(0,"All");
            foreach (Retailer retailer in BCRetailers.GetRetailers()) 
            {
                ListItem item = new ListItem(retailer.Name, retailer.Id.ToString());
                cbRetailers.Items.Add(item);
                if (item.Text == currentRetailer)
                    item.Selected = true;
            }
           
        }
    }
}