using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.WebAdmin.Controls;
using Neato.Cpt.WebAdmin.Helpers;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;

namespace Neato.Cpt.WebAdmin {
    public class ManageDevice : Page {
        private const string LblModelId = "lblModel";
        private const string ImgIconId = "imgIcon";
        private const string ImgBigIconId = "imgBigIcon";
        private const string CtlIconFileId = "ctlIconFile";
        private const string CtlBigIconFileId = "ctlBigIconFile";
        private const string TxtModelId = "txtModel";
        private const string BtnDeleteId = "btnDelete";
        private const string LblManufacturerId = "lblManufacturer";
        private const string CboManufacturerId = "cboManufacturer";
        private const string LblRatingId = "lblRating";
        private const string CboRatingId = "cboRating";
        private const string LblDeviceTypeId = "lblDeviceType";
        private const string CboDeviceTypeId = "cboDeviceType";
        private const string HypIconViewId = "hypIconView";
        private const string HypIconBigViewId = "hypIconBigView";
        private const string CtlCarriersId = "ctlCarriers";
        private const string LblCarriersId = "lblCarriers";
        private const string ManufactureFilterName = "Manufacture";
        private const string CarrierFilterName = "Carrier";
        private const string DeviceTypeFilterName = "DeviceType";

        private const int NumberOfColumns = 7;

        protected Button btnNew;
        protected DataGrid grdDevices;
        protected DropDownList cboManufacturerFilter;
        protected DropDownList cboCarrierFilter;
        protected DropDownList cboDeviceTypeFilter;
        protected Button btnDeviceSearch;

        private ArrayList brands;
        private ArrayList ratings;
        private ArrayList deviceTypes;
        private ArrayList allCarriers;
        private ArrayList carriers;

        private bool NewMode {
            get { return (bool)ViewState["NewModeKey"]; }
            set { ViewState["NewModeKey"] = value; }
        }

        #region Url
        private const string RawUrl = "ManageDevice.aspx";

        public static Uri Url(int manufacture, int carrier, string deviceType) {
            return UrlHelper.BuildUrl(RawUrl,
                                      ManufactureFilterName, manufacture,
                                      CarrierFilterName, carrier, 
                                      DeviceTypeFilterName, deviceType);
        }

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                BindFilterContols();
                NewMode = false;
                DataBind();
            }
        }

        private int ManufactureFilter {
            get {return int.Parse(cboManufacturerFilter.SelectedValue, CultureInfo.InvariantCulture.NumberFormat);}
            set {cboManufacturerFilter.SelectedValue = value.ToString();}
        }

        private int CarrierFilter {
            get {return int.Parse(cboCarrierFilter.SelectedValue, CultureInfo.InvariantCulture.NumberFormat);}
            set {cboCarrierFilter.SelectedValue = value.ToString();}
        }

        private string DeviceTypeFilter {
            get {return cboDeviceTypeFilter.SelectedValue;}
            set {cboDeviceTypeFilter.SelectedValue = value;}
        }

       
        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManageDevice_DataBinding);
            grdDevices.ItemDataBound += new DataGridItemEventHandler(grdDevices_ItemDataBound);
            grdDevices.EditCommand += new DataGridCommandEventHandler(grdDevices_EditCommand);
            grdDevices.DeleteCommand += new DataGridCommandEventHandler(grdDevices_DeleteCommand);
            grdDevices.UpdateCommand += new DataGridCommandEventHandler(grdDevices_UpdateCommand);
            grdDevices.CancelCommand += new DataGridCommandEventHandler(grdDevices_CancelCommand);
            grdDevices.ItemCreated += new DataGridItemEventHandler(grdDevices_ItemCreated);
            btnNew.Click += new EventHandler(btnNew_Click);
            btnDeviceSearch.Click += new EventHandler(btnDeviceSearch_Click);
        }
        #endregion

        private void BindFilterContols() {
            bool found = false;
            int brandId = Convert.ToInt32(Request[ManufactureFilterName]);
            cboManufacturerFilter.Items.Insert(0, new ListItem("All", "0"));
            DeviceBrand[] devBrands = BCDeviceBrand.GetDeviceBrandList();
            foreach(DeviceBrand brand in devBrands) {
                cboManufacturerFilter.Items.Add(new ListItem(brand.Name, brand.Id.ToString()));
                if (brandId == brand.Id)
                    found = true;
            }
            if (found)
                cboManufacturerFilter.SelectedValue = brandId.ToString();
            else
                cboManufacturerFilter.SelectedIndex = devBrands.Length > 0 ? 1: 0;
            
            found = false;
            int carrierId = Convert.ToInt32(Request[CarrierFilterName]);
            cboCarrierFilter.Items.Insert(0, new ListItem("All", "0"));
            Carrier[] carriers= BCCarrier.GetCarrierList();
            foreach(Carrier carrier in carriers) {
                cboCarrierFilter.Items.Add(new ListItem(carrier.Name, carrier.Id.ToString()));
                if (carrierId == carrier.Id)
                    found = true;
            }
            if (found)
                cboCarrierFilter.SelectedValue = carrierId.ToString();
            else
                cboCarrierFilter.SelectedIndex = 0;
            
            found = false;
            string deviceType = Convert.ToString(Request[DeviceTypeFilterName]);
            ArrayList devTypes = new ArrayList(Enum.GetNames(typeof(DeviceType)));
            foreach(string devType in devTypes) {
                if (devType == DeviceType.Undefined.ToString())
                    cboDeviceTypeFilter.Items.Add(new ListItem("All", devType));
                else 
                    cboDeviceTypeFilter.Items.Add(new ListItem(devType));
                if (deviceType == devType)
                    found = true;
            }
            if (found)
                cboDeviceTypeFilter.SelectedValue = deviceType;
            else
                cboDeviceTypeFilter.SelectedIndex = 0;
        }

        private void ManageDevice_DataBinding(object sender, EventArgs e) {
            brands = new ArrayList(BCDeviceBrand.GetDeviceBrandList());
            ratings = new ArrayList(BCDevice.GetRatingList());

            deviceTypes = new ArrayList(Enum.GetNames(typeof(DeviceType)));
            deviceTypes.Remove(DeviceType.Undefined.ToString());

            allCarriers = new ArrayList(BCCarrier.GetCarrierList());

            ArrayList devices = new ArrayList();
            int brandId = ManufactureFilter;
            DeviceBrand devBrand = null;
            if (brandId > 0) {
                string brandName = cboManufacturerFilter.SelectedItem.Text;
                devBrand = new DeviceBrand(brandId, brandName);
            }
            CarrierBase carrier = null;
            int carrierId = CarrierFilter;
            if (carrierId > 0) {
                carrier = new CarrierBase(carrierId);
            }
            DeviceType devType = (DeviceType)Enum.Parse(typeof(DeviceType), DeviceTypeFilter);
            
            devices = new ArrayList(BCDevice.GetDeviceList(devBrand, devType, carrier));
            
            if (NewMode) {
                devices.Insert(0, BCDevice.NewDevice());
                grdDevices.EditItemIndex = 0;
                carriers = new ArrayList();
            } else if (grdDevices.EditItemIndex >= 0) {
                int deviceId = (int)grdDevices.DataKeys[grdDevices.EditItemIndex];
                DeviceBase device = new DeviceBase(deviceId);
                int index = devices.IndexOf(device);
                grdDevices.EditItemIndex = index;
                carriers = new ArrayList(BCCarrier.GetCarrierList(device, null));
            }
            grdDevices.DataSource = devices;
            grdDevices.DataKeyField = Device.IdField;
        }

        private void grdDevices_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void ItemDataBound(DataGridItem item) {
            Device device = (Device)item.DataItem;
            Label lblModel = (Label)item.FindControl(LblModelId);
            lblModel.Text = HttpUtility.HtmlEncode(device.Model);

            Image imgIcon = (Image)item.FindControl(ImgIconId);

            string imageUrl = IconHandler.ImageUrl(device, false).AbsoluteUri;
            UriBuilder appRoot = new UriBuilder(Request.Url);
            appRoot.Path = Request.ApplicationPath;
            appRoot.Query = string.Empty;
            imageUrl = imageUrl.Replace(appRoot.Uri.AbsoluteUri.TrimEnd('\\', '/'), Configuration.IntranetDesignerSiteUrl.TrimEnd('\\', '/'));

            imgIcon.ImageUrl = imageUrl;

            HyperLink hypIconView = (HyperLink)item.FindControl(HypIconViewId);
            hypIconView.NavigateUrl = imageUrl;
            hypIconView.Target = HtmlHelper.Blank;

            Image imgBigIcon = (Image)item.FindControl(ImgBigIconId);

            string imageBigUrl = IconHandler.ImageUrl(device, true).AbsoluteUri;
            imageBigUrl = imageBigUrl.Replace(appRoot.Uri.AbsoluteUri.TrimEnd('\\', '/'), Configuration.IntranetDesignerSiteUrl.TrimEnd('\\', '/'));

            imgBigIcon.ImageUrl = imageBigUrl;

            HyperLink hypIconBigView = (HyperLink)item.FindControl(HypIconBigViewId);
            hypIconBigView.NavigateUrl = imageBigUrl;
            hypIconBigView.Target = HtmlHelper.Blank;

            Label lblManufacturer = (Label)item.FindControl(LblManufacturerId);
            lblManufacturer.Text = HttpUtility.HtmlEncode(device.Brand.Name);

            Label lblRating = (Label)item.FindControl(LblRatingId);
            lblRating.Text = HttpUtility.HtmlEncode(device.Rating.ToString(CultureInfo.InvariantCulture));

            Label lblDeviceType = (Label)item.FindControl(LblDeviceTypeId);
            lblDeviceType.Text = HttpUtility.HtmlEncode(device.DeviceType.ToString(CultureInfo.InvariantCulture));

            Label lblCarriers = (Label)item.FindControl(LblCarriersId);
            StringBuilder sb = new StringBuilder();
            foreach (Carrier carrier in BCCarrier.GetCarrierList(device, null)) {
                if (sb.Length == 0) {
                    sb.Append(HttpUtility.HtmlEncode(carrier.Name));
                } else {
                    sb.Append(HtmlHelper.BrTag);
                    sb.Append(HttpUtility.HtmlEncode(carrier.Name));
                }
            }
            lblCarriers.Text = sb.ToString();

            ImageButton btnDelete = (ImageButton)item.FindControl(BtnDeleteId);
            string warning = "Do you want to remove this device?";
            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", warning);
        }

        private void EditItemDataBound(DataGridItem item) {
            Device device = (Device)item.DataItem;
            TextBox txtModel = (TextBox)item.FindControl(TxtModelId);
            txtModel.Text = HttpUtility.HtmlEncode(device.Model);
            JavaScriptHelper.RegisterFocusScript(this, txtModel.ClientID);

            DropDownList cboManufacturer = (DropDownList)item.FindControl(CboManufacturerId);
            cboManufacturer.DataSource = brands;
            cboManufacturer.DataValueField = DeviceBrand.IdField;
            cboManufacturer.DataTextField = DeviceBrand.NameField;
            cboManufacturer.DataBind();

            int brandIndex = brands.IndexOf(device.Brand);
            if (brandIndex < 0) {
                cboManufacturer.Items.Insert(0, new ListItem("SELECT", "0"));
                cboManufacturer.SelectedIndex = 0;
            } else {
                cboManufacturer.SelectedIndex = brandIndex;
            }

            DropDownList cboRating = (DropDownList)item.FindControl(CboRatingId);
            cboRating.DataSource = ratings;
            cboRating.DataBind();
            cboRating.SelectedIndex = device.Rating;

            DropDownList cboDeviceType = (DropDownList)item.FindControl(CboDeviceTypeId);
            cboDeviceType.DataSource = deviceTypes;
            cboDeviceType.DataBind();

            int deviceTypeIndex = deviceTypes.IndexOf(device.DeviceType.ToString());
            if (deviceTypeIndex < 0) {
                cboDeviceType.Items.Insert(0, new ListItem("SELECT", "0"));
                cboDeviceType.SelectedIndex = 0;
            } else {
                cboDeviceType.SelectedIndex = deviceTypeIndex;
            }

            Selector ctlCarriers = (Selector)item.FindControl(CtlCarriersId);
            ctlCarriers.DataSource = allCarriers;
            ctlCarriers.DataValueField = Carrier.IdField;
            ctlCarriers.DataTextField = Carrier.NameField;
            ctlCarriers.DataBind();
            ctlCarriers.SelectedIndices = GetIndices(allCarriers, carriers);
        }

        private ICollection GetIndices(ArrayList allCarriers, ArrayList carriers) {
            ArrayList indices = new ArrayList();
            foreach (object carrier in carriers) {
                int index = allCarriers.IndexOf(carrier);
                if (index >= 0) {
                    indices.Add(index);
                }
            }
            return indices;
        }

        private void grdDevices_EditCommand(object source, DataGridCommandEventArgs e) {
            grdDevices.EditItemIndex = e.Item.ItemIndex;
            NewMode = false;
            DataBind();
        }

        private void grdDevices_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdDevices.EditItemIndex = -1;
            int deviceId = (int)grdDevices.DataKeys[e.Item.ItemIndex];
            DeviceBase device = new DeviceBase(deviceId);
            try {
                BCDevice.Delete(device);
                Response.Redirect(Url(ManufactureFilter, CarrierFilter, DeviceTypeFilter).PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewMode = false;
            DataBind();
        }

        private void grdDevices_UpdateCommand(object source, DataGridCommandEventArgs e) {
            Page.Validate();
            if (!IsValid) return;

            Device device = BCDevice.NewDevice();

            device.Id = (int)grdDevices.DataKeys[e.Item.ItemIndex];

            TextBox txtModel = (TextBox)e.Item.FindControl(TxtModelId);
            device.Model = txtModel.Text.Trim();

            HtmlInputFile ctlIconFile = (HtmlInputFile)e.Item.FindControl(CtlIconFileId);
            Stream icon = ctlIconFile.PostedFile.InputStream;

            HtmlInputFile ctlBigIconFile = (HtmlInputFile)e.Item.FindControl(CtlBigIconFileId);
            Stream bigIcon = ctlBigIconFile.PostedFile.InputStream;

            DropDownList cboManufacturer = (DropDownList)e.Item.FindControl(CboManufacturerId);
            int brandId = int.Parse(cboManufacturer.SelectedValue, CultureInfo.InvariantCulture.NumberFormat);
            string brandName = cboManufacturer.SelectedItem.Text;
            device.Brand = new DeviceBrand(brandId, brandName);

            DropDownList cboRating = (DropDownList)e.Item.FindControl(CboRatingId);
            device.Rating = int.Parse(cboRating.SelectedValue, CultureInfo.InvariantCulture.NumberFormat);

            DropDownList cboDeviceType = (DropDownList)e.Item.FindControl(CboDeviceTypeId);
            device.DeviceType = (DeviceType)Enum.Parse(typeof(DeviceType), cboDeviceType.SelectedValue);

            Selector ctlCarriers = (Selector)e.Item.FindControl(CtlCarriersId);
            ArrayList carrierList = new ArrayList(ctlCarriers.SelectedItemValues.Count);
            foreach (object itemValue in ctlCarriers.SelectedItemValues) {
                if (itemValue is string) {
                    int carrierId = int.Parse((string)itemValue, CultureInfo.InvariantCulture.NumberFormat);
                    carrierList.Add(new CarrierBase(carrierId));
                }
            }
            CarrierBase[] carriers = (CarrierBase[])carrierList.ToArray(typeof(CarrierBase));

            try {
                BCDevice.Save(device, icon, bigIcon, carriers);
                Response.Redirect(Url(ManufactureFilter, CarrierFilter, DeviceTypeFilter).PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }

            grdDevices.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdDevices_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdDevices.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewMode = true;
            DataBind();
        }

        private void btnDeviceSearch_Click(object sender, EventArgs e) {
            grdDevices.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdDevices_ItemCreated(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            if(item.ItemType != ListItemType.EditItem)
                return;

            GridTransformationHelper.EditItemCreationForm(item, grdDevices.Columns, NumberOfColumns);
        }
    }
}