<%@ Page language="c#" Codebehind="AdminMenu.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.AdminMenu" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>AdminMenu</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <script type="text/javascript" language="javascript" src="js/RefreshContent.js"></script>
  </HEAD>
  <body MS_POSITIONING="GridLayout">
	
    <form id="Form1" method="post" runat="server">
        <ul>
            <B>Content</B>
            <br>
       		&nbsp;Retailer:&nbsp;<asp:DropDownList Runat="server" ID="cboRetailers" AutoPostBack="True" />
            <br>

            <li><asp:HyperLink ID="hypAnnouncements" Runat="server" Text="Announcements"/></li>
            <li><asp:HyperLink ID="hypLogoManagement" Runat="server" Text="Logo management"/></li>
            <li><asp:HyperLink ID="hypHeaderMenu" Runat="server" Text="Top menu"/></li>
            <li><asp:HyperLink ID="hypCategories" Runat="server" Text="Categories"/></li>
            <li><asp:HyperLink ID="hypOverview" Runat="server" Text="Overview"/></li>
            <li><asp:HyperLink ID="hypBuyMoreSkins" Runat="server" Text="Where can I buy more skins"/></li>
            <li><asp:HyperLink ID="hypTermsOfUse" Runat="server" Text="Terms of Use"/></li>
            <li><asp:HyperLink ID="hypSupport" Runat="server" Text="Support page"/></li>
            <li><asp:HyperLink ID="hypFaq" Runat="server" Text="FAQ"/></li>
            <li><asp:HyperLink ID="hypFooterMenu" Runat="server" Text="Footer"/></li>
            <li><asp:HyperLink ID="hypImageLibrary" Runat="server" Text="Image library"/></li>
            <li><asp:HyperLink ID="hypExitPage" Runat="server" Text="Exit Page"/></li>
            <li><asp:HyperLink ID="hypMaintenanceClosing" Runat="server" Text="Close for Maintenance"/></li>
        </ul>
        <ul>
           <B> Notifications</B>
            <li><asp:HyperLink ID="hypNotifications" Runat="server" Text="Edit e-mail content"/></li>
        </ul>
        <ul>
            <B>Maintenance</B>
            <li><asp:HyperLink ID="hypAddPhone" Runat="server" Text="&quot;Can't find my device&quot; requests"/></li>
            <li><asp:HyperLink ID="hypSiteClosing" Runat="server" Text="Site closing"/></li>
        </ul>
        <ul>
            <B>Management</B>
            <li><asp:HyperLink ID="hypManageCarrier" Runat="server" Text="Carriers"/></li>
            <li><asp:HyperLink ID="hypManageDeviceBrand" Runat="server" Text="Manufacturers"/></li>
            <li><asp:HyperLink ID="hypManagePaperBrand" Runat="server" Text="Paper Brands"/></li>
            <li><asp:HyperLink ID="hypManageDevice" Runat="server" Text="Devices"/></li>
            <li><asp:HyperLink ID="hypManagePaper" Runat="server" Text="Papers"/></li>
            <li><asp:HyperLink ID="hypManageRetailers" Runat="server" Text="Retailers"/></li>
            <li><asp:HyperLink ID="hypCustomerProfiles" Runat="server" Text="Customer Profiles"/></li>
            <li><asp:HyperLink ID="hypGoogleTracking" Runat="server" Text="Google Tracking"/></li>
            <li><asp:HyperLink ID="hypManageIPFilter" Runat="server" Text="IP filters"/></li>
            <li><asp:HyperLink ID="hypManageSpecialUser" Runat="server" Text="Special Users"/></li>
        </ul>
        <ul>
            <B>Reports</B>
            <li><asp:HyperLink ID="hypTrackUserAction" Runat="server" Text="User action tracking report"/></li>
            <li><asp:HyperLink ID="hypTrackPhones" Runat="server" Text="Devices tracking report"/></li>
            <li><asp:HyperLink ID="hypTrackImages" Runat="server" Text="Images tracking report"/></li>
            <li><asp:HyperLink ID="hypTrackFeatures" Runat="server" Text="Features tracking report"/></li>
            <li><asp:HyperLink ID="hypPhoneRequestReportByDatesAndBrand" Runat="server" Text="�Can�t find my device� (devices by date range)"/></li>
            <li><asp:HyperLink ID="hypPhoneRequestEmailsReportByDates" Runat="server" Text="�Can�t find my device� (email addresses by date range)"/></li>
        </ul>
    </form>
	
  </body>
</HTML>
