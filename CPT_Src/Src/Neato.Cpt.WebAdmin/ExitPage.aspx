<%@ Page language="c#" Codebehind="ExitPage.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.ExitPage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>ExitPage</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
    <form id="Form1" method="post" runat="server">
      <h3 align="center">Exit Page</h3>
      <div>Text/HTML:</div>
	  <asp:TextBox Runat="server" ID="txtText" TextMode="MultiLine" Rows="10" style="width:100%" />
	  <br>
	  <asp:CheckBox ID="chkEnable" Runat="server" Text="Enabled" style="margin-right:100px;"/>
      <asp:Button ID="btnSubmit" Runat="server" Text="Apply to Site"/>
    </form>
  </body>
</html>
