using System;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.WebAdmin.Helpers;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;
namespace Neato.Cpt.WebAdmin {
    public class ManageDeviceBrand : Page {
        private const string LblNameId = "lblName";
        private const string ImgIconId = "imgIcon";
        private const string CtlIconFileId = "ctlIconFile";
        private const string TxtNameId = "txtName";
        private const string BtnDeleteId = "btnDelete";
        protected Button btnNew;
        protected DataGrid grdManufacturers;


        private bool NewMode {
            get { return (bool)ViewState["NewModeKey"]; }
            set { ViewState["NewModeKey"] = value; }
        }

        #region Url
        private const string RawUrl = "ManageDeviceBrand.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                NewMode = false;
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManageDeviceBrand_DataBinding);
            grdManufacturers.ItemDataBound += new DataGridItemEventHandler(grdManufacturers_ItemDataBound);
            grdManufacturers.EditCommand += new DataGridCommandEventHandler(grdManufacturers_EditCommand);
            grdManufacturers.DeleteCommand += new DataGridCommandEventHandler(grdManufacturers_DeleteCommand);
            grdManufacturers.UpdateCommand += new DataGridCommandEventHandler(grdManufacturers_UpdateCommand);
            grdManufacturers.CancelCommand += new DataGridCommandEventHandler(grdManufacturers_CancelCommand);
            btnNew.Click += new EventHandler(btnNew_Click);
        }
        #endregion

        private void ManageDeviceBrand_DataBinding(object sender, EventArgs e) {
            ArrayList deviceBrands = new ArrayList(BCDeviceBrand.GetDeviceBrandList());
            if (NewMode) {
                deviceBrands.Insert(0, BCDeviceBrand.NewDeviceBrand());
                grdManufacturers.EditItemIndex = 0;
            } else if (grdManufacturers.EditItemIndex >= 0) {
                int deviceBrandId = (int)grdManufacturers.DataKeys[grdManufacturers.EditItemIndex];
                int index = deviceBrands.IndexOf(new DeviceBrandBase(deviceBrandId));
                grdManufacturers.EditItemIndex = index;
            }
            grdManufacturers.DataSource = deviceBrands;
            grdManufacturers.DataKeyField = DeviceBrand.IdField;
        }

        private void grdManufacturers_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void grdManufacturers_EditCommand(object source, DataGridCommandEventArgs e) {
            grdManufacturers.EditItemIndex = e.Item.ItemIndex;
            NewMode = false;
            DataBind();
        }

        private void grdManufacturers_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdManufacturers.EditItemIndex = -1;
            int deviceBrandId = (int)grdManufacturers.DataKeys[e.Item.ItemIndex];
            DeviceBrandBase brand = new DeviceBrandBase(deviceBrandId);
            try {
                BCDeviceBrand.Delete(brand);
                Response.Redirect(Url().PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewMode = false;
            DataBind();
        }

        private void grdManufacturers_UpdateCommand(object source, DataGridCommandEventArgs e) {
            Page.Validate();
            if (!IsValid) return;

            DeviceBrand brand = BCDeviceBrand.NewDeviceBrand();

            brand.Id = (int)grdManufacturers.DataKeys[e.Item.ItemIndex];
            TextBox txtName = (TextBox)e.Item.FindControl(TxtNameId);

            brand.Name = txtName.Text.Trim();
            HtmlInputFile ctlIconFile = (HtmlInputFile)e.Item.FindControl(CtlIconFileId);

            Stream icon = ctlIconFile.PostedFile.InputStream;

            try {
                BCDeviceBrand.Save(brand, icon);
                Response.Redirect(Url().PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }

            grdManufacturers.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdManufacturers_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdManufacturers.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void ItemDataBound(DataGridItem item) {
            DeviceBrand brand = (DeviceBrand)item.DataItem;
            Label lblName = (Label)item.FindControl(LblNameId);
            lblName.Text = HttpUtility.HtmlEncode(brand.Name);

            Image imgIcon = (Image)item.FindControl(ImgIconId);
            

            // TODO: refactoring: extract method to helper
            string imageUrl = IconHandler.ImageUrl(brand).AbsoluteUri;
            UriBuilder appRoot = new UriBuilder(Request.Url);
            appRoot.Path = Request.ApplicationPath;
            appRoot.Query = string.Empty;
            imageUrl = imageUrl.Replace(appRoot.Uri.AbsoluteUri.TrimEnd('\\', '/'), Configuration.IntranetDesignerSiteUrl.TrimEnd('\\', '/'));
            
            imgIcon.ImageUrl = imageUrl;

            ImageButton btnDelete = (ImageButton)item.FindControl(BtnDeleteId);
            string warning1 = "Do you want to remove this manufacturer?"; 
            string warning2 = "All devices of this manufacturer will be removed. Are you sure?"; 
            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", warning1, warning2);
        }

        private void EditItemDataBound(DataGridItem item) {
            DeviceBrand brand = (DeviceBrand)item.DataItem;
            TextBox txtName = (TextBox)item.FindControl(TxtNameId);
            txtName.Text = brand.Name;
            JavaScriptHelper.RegisterFocusScript(this, txtName.ClientID);
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewMode = true;
            DataBind();
        }    
    }
}