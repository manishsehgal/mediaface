using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpModules;

namespace Neato.Cpt.WebAdmin {
    public class Support : Page {
        #region Constants
        private const string LocalizedTextName = "LocalizedText";
        private const string DefaultCultureName = "";
        private const string JSPreview = "return Preview()";
        #endregion

        protected TextBox txtText;
        protected Button btnSubmit;
        protected Button btnPreview;
        protected HtmlControl ctlPreviewContaner;
        
        private LocalizedText PageContent {
            get { return (LocalizedText)ViewState[LocalizedTextName]; }
            set { ViewState[LocalizedTextName] = value; }
        }

        #region Url
        private const string RawUrl = "Support.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(Support_DataBinding);
            this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        }
        #endregion

        private void Support_DataBinding(object sender, EventArgs e) {
            string url = CheckLocationModule.AdminUrl(Configuration.IntranetDesignerSiteUrl + WebDesigner.Support.PageName(), Neato.Cpt.WebAdmin.StorageManager.CurrentRetailer);
            ctlPreviewContaner.Attributes["src"] = url;
            btnPreview.Attributes["onclick"] = JSPreview;
            PageContent = BCSupportPage.Get(DefaultCultureName, Neato.Cpt.WebAdmin.StorageManager.CurrentRetailer);
            if (PageContent == null)
                PageContent = new LocalizedText(DefaultCultureName, string.Empty);
               
            txtText.Text = PageContent.Text;

        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            PageContent.Text = txtText.Text;
            BCSupportPage.Update(PageContent, Neato.Cpt.WebAdmin.StorageManager.CurrentRetailer);
        }
    }
}