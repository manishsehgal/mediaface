using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.WebAdmin.Controls;
using Neato.Cpt.WebAdmin.Helpers;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;

namespace Neato.Cpt.WebAdmin {
    public class ManageCarrier : Page {
        private const string LblNameId = "lblName";
        private const string ImgIconId = "imgIcon";
        private const string CtlIconFileId = "ctlIconFile";
        private const string TxtNameId = "txtName";
        private const string BtnDeleteId = "btnDelete";
        private const string LblRetailersId = "lblRetailers";
        private const string CtlRetailersId = "ctlRetailers";
        protected Button btnNew;
        protected DataGrid grdCarriers;

        private const int NumberOfColumns = 3;

        private bool NewCarrierMode {
            get { return (bool)ViewState["NewCarrierModeKey"]; }
            set { ViewState["NewCarrierModeKey"] = value; }
        }

        #region Url
        private const string RawUrl = "ManageCarrier.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                NewCarrierMode = false;
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManageCarrier_DataBinding);
            grdCarriers.ItemDataBound += new DataGridItemEventHandler(grdCarriers_ItemDataBound);
            grdCarriers.EditCommand += new DataGridCommandEventHandler(grdCarriers_EditCommand);
            grdCarriers.DeleteCommand += new DataGridCommandEventHandler(grdCarriers_DeleteCommand);
            grdCarriers.UpdateCommand += new DataGridCommandEventHandler(grdCarriers_UpdateCommand);
            grdCarriers.CancelCommand += new DataGridCommandEventHandler(grdCarriers_CancelCommand);
            grdCarriers.ItemCreated += new DataGridItemEventHandler(grdCarriers_ItemCreated);
            btnNew.Click += new EventHandler(btnNew_Click);
        }
        #endregion

        private void ManageCarrier_DataBinding(object sender, EventArgs e) {
            ArrayList carriers = new ArrayList(BCCarrier.GetCarrierList());
            if (NewCarrierMode) {
                carriers.Insert(0, BCCarrier.NewCarrier());
                grdCarriers.EditItemIndex = 0;
            } else if (grdCarriers.EditItemIndex >= 0) {
                int carrierId = (int)grdCarriers.DataKeys[grdCarriers.EditItemIndex];
                int index = carriers.IndexOf(new CarrierBase(carrierId));
                grdCarriers.EditItemIndex = index;
            }
            grdCarriers.DataSource = carriers;
            grdCarriers.DataKeyField = Carrier.IdField;
        }

        private void grdCarriers_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void grdCarriers_EditCommand(object source, DataGridCommandEventArgs e) {
            grdCarriers.EditItemIndex = e.Item.ItemIndex;
            NewCarrierMode = false;
            DataBind();
        }

        private void grdCarriers_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdCarriers.EditItemIndex = -1;
            int carrierId = (int)grdCarriers.DataKeys[e.Item.ItemIndex];
            CarrierBase carrier = new CarrierBase(carrierId);
            try {
                BCCarrier.Delete(carrier);
                Response.Redirect(Url().PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
            NewCarrierMode = false;
            DataBind();
        }

        private void grdCarriers_UpdateCommand(object source, DataGridCommandEventArgs e) {
            Page.Validate();
            if (!IsValid) return;

            Carrier carrier = BCCarrier.NewCarrier();

            carrier.Id = (int)grdCarriers.DataKeys[e.Item.ItemIndex];
            TextBox txtName = (TextBox)e.Item.FindControl(TxtNameId);

            carrier.Name = txtName.Text.Trim();
            HtmlInputFile ctlIconFile = (HtmlInputFile)e.Item.FindControl(CtlIconFileId);

            Stream icon = ctlIconFile.PostedFile.InputStream;

            Selector ctlRetailers = (Selector)e.Item.FindControl(CtlRetailersId);
            ArrayList retailerList = new ArrayList(ctlRetailers.SelectedItemValues.Count);
            foreach (object itemValue in ctlRetailers.SelectedItemValues) {
                if (itemValue is string) {
                    int retailerId = int.Parse((string)itemValue, CultureInfo.InvariantCulture.NumberFormat);
                    retailerList.Add(new Retailer(retailerId));
                }
            }
            Retailer[] retailers = (Retailer[])retailerList.ToArray(typeof(Retailer));

            try {
                BCCarrier.Save(carrier, icon, retailers);
                Response.Redirect(Url().PathAndQuery);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }

            grdCarriers.EditItemIndex = -1;
            NewCarrierMode = false;
            DataBind();
        }

        private void grdCarriers_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdCarriers.EditItemIndex = -1;
            NewCarrierMode = false;
            DataBind();
        }

        private void ItemDataBound(DataGridItem item) {
            Carrier carrier = (Carrier)item.DataItem;
            Label lblName = (Label)item.FindControl(LblNameId);
            lblName.Text = HttpUtility.HtmlEncode(carrier.Name);

            Image imgIcon = (Image)item.FindControl(ImgIconId);
            

            // TODO: refactoring: extract method to helper
            string imageUrl = IconHandler.ImageUrl(carrier).AbsoluteUri;
            UriBuilder appRoot = new UriBuilder(Request.Url);
            appRoot.Path = Request.ApplicationPath;
            appRoot.Query = string.Empty;
            imageUrl = imageUrl.Replace(appRoot.Uri.AbsoluteUri.TrimEnd('\\', '/'), Configuration.IntranetDesignerSiteUrl.TrimEnd('\\', '/'));
            
            imgIcon.ImageUrl = imageUrl;

            ImageButton btnDelete = (ImageButton)item.FindControl(BtnDeleteId);
            string warning = "All devices will be removed from the Carrier." + Environment.NewLine + "Are you sure?";
            btnDelete.Attributes[HtmlHelper.OnClick] = JavaScriptHelper.ConfirmScript(this, "DeleteWarning", warning);

            Label lblRetailers = (Label)item.FindControl(LblRetailersId);
            StringBuilder sb = new StringBuilder();
            foreach (Retailer retailer in BCRetailers.GetRetailers(carrier)) {
                if (sb.Length == 0) {
                    sb.Append(HttpUtility.HtmlEncode(retailer.Name));
                } 
                else {
                    sb.Append(HtmlHelper.BrTag);
                    sb.Append(HttpUtility.HtmlEncode(retailer.Name));
                }
            }
            lblRetailers.Text = sb.ToString();
        }

        private void EditItemDataBound(DataGridItem item) {
            Carrier carrier = (Carrier)item.DataItem;
            TextBox txtName = (TextBox)item.FindControl(TxtNameId);
            txtName.Text = carrier.Name;
            JavaScriptHelper.RegisterFocusScript(this, txtName.ClientID);

            Selector ctlRetailers = (Selector)item.FindControl(CtlRetailersId);
            ctlRetailers.DataSource = BCRetailers.GetRetailers();
            ctlRetailers.DataValueField = Retailer.IdField;
            ctlRetailers.DataTextField = Retailer.NameField;
            ctlRetailers.DataBind();
            ctlRetailers.SelectedIndices = GetIndices(new ArrayList(BCRetailers.GetRetailers()),
                                                      new ArrayList(BCRetailers.GetRetailers(carrier)));
        }

        private ICollection GetIndices(ArrayList allRetailers, ArrayList retailers) {
            ArrayList indices = new ArrayList();
            foreach (object retailer in retailers) {
                int index = allRetailers.IndexOf(retailer);
                if (index >= 0) {
                    indices.Add(index);
                }
            }
            return indices;
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewCarrierMode = true;
            DataBind();
        }

        private void grdCarriers_ItemCreated(object sender, DataGridItemEventArgs e){
            DataGridItem item = e.Item;
            if(item.ItemType != ListItemType.EditItem)
                return;

            GridTransformationHelper.EditItemCreationForm(item, grdCarriers.Columns, NumberOfColumns);
        }
    }
}