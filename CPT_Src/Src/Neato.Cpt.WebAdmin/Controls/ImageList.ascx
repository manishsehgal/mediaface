<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ImageList.ascx.cs" Inherits="Neato.Cpt.WebAdmin.Controls.ImageList" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<input type="hidden" id="ctlSelectState"/>
<script type=text/javascript>
    function SwitchSelectMode() {
        var ctl = document.getElementById('ctlSelectState');
        var state = (ctl.value == '' ||
                    ctl.value == false ||
                    ctl.value == 'false') ? true : false;

        for(i = 0; i < document.all.length; i++) {
            var elem = document.all[i];
            if (elem.type == 'checkbox') elem.checked = state;
        }

        ctl.value = state;
        return false; 
    }
</script>

<table border="0">
    <tr>
        <td height="90%">&nbsp;
            <div style="OVERFLOW: auto; HEIGHT: 365px">
                <asp:DataList ID="grdImages"
                    Runat="server" 
                    BorderColor="black"
                    CellPadding="5"
                    CellSpacing="20"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Table"
                    RepeatColumns="6"
                    EnableViewState="true">
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                    <ItemTemplate>
			            <img ID="imgItem" Runat="server"/><br>
	                    <input id="chkSelect" type="checkbox"
	                    Runat="server" NAME="chkSelect" CausesValidation="False"/>
                    </ItemTemplate>
                </asp:DataList>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="btnSwitchSelectMode" runat="server" 
            Text="Select/unselect all images" CausesValidation="False"/>
        </td>
    </tr>
</table>

