<%@ Register TagPrefix="cpt" TagName="DatePicker" Src="DatePicker.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="DateInterval.ascx.cs" Inherits="Neato.Cpt.WebAdmin.Controls.DateInterval" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<table>
    <tr>
        <td id="tdStartDate" runat="server" width="150px">
            Start Date
        </td>
        <td>
            <cpt:DatePicker id="dtpStartDate" runat="server" ReadOnly="False" Width="300px" />
        </td>
    </tr>
    <tr>
        <td id="tdEndDate" runat="server" width="150px">
            End Date
        </td>
        <td>
            <cpt:DatePicker id="dtpEndDate" runat="server" ReadOnly="False" Width="300px" />
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <asp:Label ID="lblDateFormatWarning" Runat="server"/>
        </td>
    </tr>
    <tr>
        <td>
            <asp:CustomValidator ID="vldDateFormat" Runat="server"
            ErrorMessage="Wrong date format" Display="Dynamic"/>
        </td>
    </tr>
    <tr>
        <td>
            <asp:CustomValidator ID="vldDateRange" Runat="server"
            ErrorMessage="End Date must be >= Start Date" Display="Dynamic"/>
        </td>
    </tr>
</table>