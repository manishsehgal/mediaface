
function OpenCalendar(cal, field, anchor, div, dateFormat) {
	if(!document.getElementById(field).disabled) {
		if(!cal) {
			var c = new CalendarPopup(div);
			c.select(document.getElementById(field), anchor, dateFormat);
		} else cal.select(document.getElementById(field), anchor, dateFormat);
	}
	return false;
}
