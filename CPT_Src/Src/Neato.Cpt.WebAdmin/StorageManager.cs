using System.Web;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;

namespace Neato.Cpt.WebAdmin {
    public class StorageManager {
        public StorageManager() {}

        public const string CurrentRetailerKey = "CurrentRetailer";

        public static Retailer CurrentRetailer {
            get {
                object retailer = HttpContext.Current.Session[CurrentRetailerKey];
                if (retailer == null) {
                    retailer = BCRetailers.GetRetailerByName(BCRetailers.GetDefaultRetailerName());
                    HttpContext.Current.Session[CurrentRetailerKey] = retailer;
                }

                return (Retailer)retailer;
            }
            set { HttpContext.Current.Session[CurrentRetailerKey] = value; }
        }

    }
}