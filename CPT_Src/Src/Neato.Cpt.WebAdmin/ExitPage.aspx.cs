using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.WebDesigner.Helpers;

namespace Neato.Cpt.WebAdmin {
    public class ExitPage : Page {
        #region Url
        private const string RawUrl = "ExitPage.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        protected TextBox txtText;
        protected Button btnSubmit;
        protected CheckBox chkEnable;
        
        private int retailerId {
            get {return StorageManager.CurrentRetailer.Id;}
        }

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ExitPage_DataBinding);
            this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        }
        #endregion

        private void ExitPage_DataBinding(object sender, EventArgs e) {
            Entity.ExitPage exitPage = BCExitPage.Get(retailerId);
            if (exitPage != null) {
                txtText.Text = exitPage.HtmlText;
                chkEnable.Checked = exitPage.Enabled;
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            Entity.ExitPage exitPage = new Entity.ExitPage(
                retailerId,
                txtText.Text,
                chkEnable.Checked);

            try {
                BCExitPage.Update(exitPage);
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
        }
    }
}