using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;
using Neato.Cpt.WebDesigner.HttpModules;

namespace Neato.Cpt.WebAdmin {
    public class ManageMaintenanceClosing : Page {
        protected TextBox txtText;
        protected Button btnPreview;
        protected Button btnSubmit;
        protected HtmlControl ctlPreviewContaner;
        protected Label lblResponse;
        protected Panel panMessage;
        protected Panel panMaintenanceClosing;

        private string closePageSource;
        private ManualResetEvent allDone = new ManualResetEvent(false);

        private const string FrameContentLoadScript = "FrameContentLoad";

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Url

        private const string RawUrl = "ManageMaintenanceClosing.aspx";
        private const string TextKey = "Text";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }

        public static Uri Url(string text) {
            return UrlHelper.BuildUrl(RawUrl, TextKey, text);
        }

        public static string CurrentRetailerUrl(string url) {
            string retailerUrl;
            if (url.IndexOf("?") > 0)
                retailerUrl = string.Format("{0}&{1}={2}", url, MaintenanceClosingHandler.RetailerKey, StorageManager.CurrentRetailer.Name);
            else
                retailerUrl = string.Format("{0}?{1}={2}", url, MaintenanceClosingHandler.RetailerKey, StorageManager.CurrentRetailer.Name);
            return retailerUrl;
        }

        private string RetailerClosePageHandlerUri {
            get {
                return CurrentRetailerUrl(CheckLocationModule.AdminUrl(
                             Path.Combine(Configuration.IntranetDesignerSiteUrl, MaintenanceClosingHandler.PageName())
                           , StorageManager.CurrentRetailer));
            }
        }

        private string RetailerClosePageUri {
            get {
                string retailerFolder = Path.Combine(Configuration.IntranetDesignerSiteUrl, RetailerConstants.RetailersFolder);
                string retailerClosePage = Path.Combine(StorageManager.CurrentRetailer.Name, RetailerConstants.ClosePageFileName);
                return Path.Combine(retailerFolder, retailerClosePage);
            }
        }

        #endregion

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(ManageMaintenanceClosing_DataBinding);
            this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
            this.txtText.DataBinding += new EventHandler(txtText_DataBinding);
            this.PreRender += new EventHandler(ManageMaintenanceClosing_PreRender);
            this.ctlPreviewContaner.DataBinding += new EventHandler(ctlPreviewContaner_DataBinding);
            this.lblResponse.DataBinding += new EventHandler(lblResponse_DataBinding);
        }

        #endregion

        private void btnSubmit_Click(object sender, EventArgs e) {
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(RetailerClosePageHandlerUri);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "POST";

            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(txtText.Text);
            writer.Close();

            request.BeginGetRequestStream(new AsyncCallback(ResponseCallback), request);
            allDone.WaitOne();

            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
            if (request.HaveResponse) {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string result = reader.ReadToEnd();
                Response.Redirect(Url(result).PathAndQuery);
            }
            lblResponse.Text = "Can't get response from " + RetailerClosePageHandlerUri;
        }

        private void ResponseCallback(IAsyncResult asynchronousResult) {
            HttpWebRequest request = (HttpWebRequest) asynchronousResult.AsyncState;
            request.EndGetRequestStream(asynchronousResult);
            allDone.Set();
        }

        private void ManageMaintenanceClosing_DataBinding(object sender, EventArgs e) {
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(RetailerClosePageHandlerUri);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "GET";

            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
            if (request.HaveResponse) {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                closePageSource = reader.ReadToEnd();

                reader.Close();
                response.Close();
            }
        }

        private void txtText_DataBinding(object sender, EventArgs e) {
            txtText.Text = closePageSource;
        }

        private void ManageMaintenanceClosing_PreRender(object sender, EventArgs e) {
            if (BCMaintenanceClosing.SiteClosed) {
                panMaintenanceClosing.Visible = false;
                panMessage.Visible = true;
            }
            else {
                panMaintenanceClosing.Visible = true;
                panMessage.Visible = false;
            }
        }

        private void ctlPreviewContaner_DataBinding(object sender, EventArgs e) {
            if (closePageSource != string.Empty) {
                ctlPreviewContaner.Attributes["src"] = RetailerClosePageUri;
                if (!IsStartupScriptRegistered(FrameContentLoadScript))
                    RegisterStartupScript(FrameContentLoadScript, "<script type='text/javascript'>PreviewPage()</script>");
            }
        }

        private void lblResponse_DataBinding(object sender, EventArgs e) {
            if (Request.QueryString[TextKey] != null)
                lblResponse.Text = Request.QueryString[TextKey];
        }
    }
}