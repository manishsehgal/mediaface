<%@ Page language="c#" Codebehind="ManageRetailers.aspx.cs" AutoEventWireup="false" Inherits="Neato.Cpt.WebAdmin.ManageRetailers" %>
<%@ Register TagPrefix="cpt" NameSpace="Neato.Cpt.WebAdmin.Controls" Assembly="Neato.Cpt.WebAdmin" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
    <HEAD>
        <title>Carrier Management</title>
        <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
        <meta name="CODE_LANGUAGE" Content="C#">
        <meta name="vs_defaultClientScript" content="JavaScript">
        <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	    <script type="text/javascript" language="javascript" src="js/RefreshContent.js"></script>
    </HEAD>
    <body MS_POSITIONING="GridLayout">
        <form id="Form1" method="post" runat="server">
            <h3 align="center">Retailer Management</h3>
            <asp:Button ID="btnNew" Runat="server" Text="New Retailer" CausesValidation="False" />
            <br>
            <br>
            <asp:DataGrid Runat="server" ID="grdRetailers" AutoGenerateColumns="False">
                <Columns>
                    <asp:TemplateColumn HeaderText="Icon">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Image ID="imgIcon" Runat="server" BorderStyle="Solid" BorderWidth="1" BorderColor="#d4d0c8" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <input ID="ctlIconFile" Runat="server" type="file" NAME="ctlIconFile"/>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Retailer Name">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="70px" />
                        <ItemTemplate>
                            <asp:Label ID="lblName" Runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtName" Runat="server" MaxLength="40" style="width:100%" />
                            <asp:RequiredFieldValidator
                                ID="vldNameRequired" Runat="server"
                                ControlToValidate="txtName"
                                Display="Dynamic"
                                ErrorMessage="Enter the retailer name" />
                            <asp:RegularExpressionValidator Runat="server"
                                ID="vldAlphanumeric" Display="Dynamic" ControlToValidate="txtName"
                                ValidationExpression="^[_0-9a-zA-Z]*"
                                ErrorMessage="Alphanumeric value is required" />
                            <asp:CustomValidator Runat="server"
                                ID="vldExistName" Display="Dynamic" 
                                ErrorMessage="Retailer with this name already exist" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Retailer Display Name">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="100px" />
                        <ItemTemplate>
                            <asp:Label ID="lblDisplayName" Runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDisplayName" Runat="server" MaxLength="40" style="width:100%" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Retailer Url">
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="100px" />
                        <ItemTemplate>
                            <asp:Label ID="lblUrl" Runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUrl" Runat="server" MaxLength="200" style="width:100%" />
                            <asp:RequiredFieldValidator
                                ID="Requiredfieldvalidator1" Runat="server"
                                ControlToValidate="txtUrl"
                                Display="Dynamic"
                                ErrorMessage="Enter the retailer url" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Has Branded Site">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" Width="60px" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkHasBrandedSiteView" Runat="server" style="width:100%" Enabled="False"/>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkHasBrandedSiteEdit" Runat="server" style="width:100%" />
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center"/>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Retailers on 'Buy Now' page">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" Width="150px" />
                        <ItemTemplate>
                            <asp:Label ID="lblRetailers" Runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <cpt:Selector ID="ctlRetailers" runat="server" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Action">
                        <ItemTemplate>
                            <asp:ImageButton Runat="server" ID="btnEdit"
                                ImageUrl="images/edit.gif" CommandName="Edit" CausesValidation="False"
                                ImageAlign="AbsMiddle" AlternateText="Edit" /><br>
                            <asp:ImageButton Runat="server" ID="btnDelete" ImageUrl="images/remove.gif" CommandName="Delete"
                                CausesValidation="False" ImageAlign="AbsMiddle" AlternateText="Remove" />
                            <asp:Label ID="lblDefaultRetailer" Runat="server" Visible="False">Default&nbsp;retailer</asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton Runat="server" ID="btnUpdate"
                                ImageUrl="images/save.gif" CommandName="Update" CausesValidation="True"
                                ImageAlign="AbsMiddle" AlternateText="Save" /><br>
                            <asp:ImageButton Runat="server" ID="btnCancel"
                                ImageUrl="images/cancel.gif" CommandName="Cancel"
                                CausesValidation="False" ImageAlign="AbsMiddle" AlternateText="Cancel" />
                        </EditItemTemplate>
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" Width="80px" />
                        <ItemStyle Wrap="False" />
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </form>
    </body>
</HTML>