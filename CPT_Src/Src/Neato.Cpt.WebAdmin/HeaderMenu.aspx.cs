using System;
using System.Collections;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.WebAdmin.Helpers;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;
using Neato.Cpt.WebDesigner.HttpModules;

namespace Neato.Cpt.WebAdmin {
    public class HeaderMenu : Page {
        #region Constants
        private const string LinkItemsName = "LINKITEMS";

        private const string LblLinkName = "lblLinkName";
        private const string TxtLinkName = "txtLinkName";
        private const string LblLinkUrl = "lblLinkUrl";
        private const string TxtLinkUrl = "txtLinkUrl";
        private const string ChkIsNewWindow = "chkIsNewWindow";
        private const string ChkIsVisible = "chkIsVisible";

        private const string BtnItemDeleteName = "btnItemDelete";
        private const string BtnItemEditName = "btnItemEdit";
        private const string BtnItemUpdateName = "btnItemUpdate";
        private const string BtnItemCancelName = "btnItemCancel";

        private const string SetFocusScriptKey = "SET_FOCUS_SCRIPT_KEY";
        private const string SetFocusScript = "<script language='javascript'>var element = document.getElementById('{0}');element.focus();element.select();</script>";
        #endregion

        protected DropDownList cboPages;
        protected DataGrid grdLinks;
        protected Button btnNew;
        protected CheckBox chkUseDefaultMenu;
        protected TextBox txtBackgroundColor;
        protected Button btnApplyBackgroundColor;

        private ArrayList LinkItems {
            get { return (ArrayList)ViewState[LinkItemsName]; }
            set { ViewState[LinkItemsName] = value; }
        }

        #region Url
        private static string RefreshPlacePropertyUrl = CheckLocationModule.AdminUrl(Configuration.IntranetDesignerSiteUrl + RefreshCacheHandler.UrlRefreshPlaceProperty(), null);

        private const string RawUrl = "HeaderMenu.aspx";

        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

        private int retailerId {
            get {return StorageManager.CurrentRetailer.Id;}
        }

        private int pageId {
            get {
                int id = BCDynamicLink.GetPageIdFromPageUrl(retailerId, cboPages.SelectedValue);
                if (id == BCDynamicLink.NotFoundPageId)
                    id = BCDynamicLink.AddPage(retailerId, cboPages.SelectedValue);
                return id;
            }
        }

        private void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                DataBind();
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e) {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent() {
            this.Load += new EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(HeaderMenu_DataBinding);
            this.cboPages.SelectedIndexChanged += new EventHandler(cboPages_SelectedIndexChanged);
            this.grdLinks.DataBinding += new EventHandler(grdLinks_DataBinding);
            this.grdLinks.ItemDataBound += new DataGridItemEventHandler(grdLinks_ItemDataBound);
            this.grdLinks.EditCommand += new DataGridCommandEventHandler(grdLinks_EditCommand);
            this.grdLinks.UpdateCommand += new DataGridCommandEventHandler(grdLinks_UpdateCommand);
            this.grdLinks.CancelCommand += new DataGridCommandEventHandler(grdLinks_CancelCommand);
            this.grdLinks.DeleteCommand += new DataGridCommandEventHandler(grdLinks_DeleteCommand);
            this.btnNew.Click += new EventHandler(btnNew_Click);
            this.chkUseDefaultMenu.CheckedChanged += new EventHandler(chkUseDefaultMenu_CheckedChanged);
            this.btnApplyBackgroundColor.Click += new EventHandler(btnApplyBackgroundColor_Click);
        }
        #endregion

        private void HeaderMenu_DataBinding(object sender, EventArgs e) {
            string[] pages = DynamicLinkHelper.GetMenuPagesWithFooter("MainMenu");
            cboPages.Items.Clear();
            cboPages.Items.Add(new ListItem("Default Top Menu", BCDynamicLink.DefaultMenuPage));
            foreach (string page in pages) {
                cboPages.Items.Add(new ListItem(page, page));
            }
            cboPages.SelectedIndex = 0;

            LinkItems = new ArrayList();
            LinkItems.AddRange(BCDynamicLink.GetLinks(pageId, Place.Header, BCDynamicLink.DefaultCulture));
            chkUseDefaultMenu.Visible = false;
            PlaceProperty placeProperty = BCPlaceProperty.Get(retailerId, Place.Header);
            if (placeProperty != null)
                txtBackgroundColor.Text = placeProperty.BackgroundColor;
        }

        private void chkUseDefaultMenu_CheckedChanged(object sender, EventArgs e) {
            bool useDefaultMenu = chkUseDefaultMenu.Checked;
            btnNew.Visible = !useDefaultMenu;
            grdLinks.Visible = !useDefaultMenu;

            if (!chkUseDefaultMenu.Visible)
                return;

            if (useDefaultMenu) {
                BCDynamicLink.DynamicLinkDelByPageId(pageId);
                LinkItems = new ArrayList();
                grdLinks.DataBind();
            } else {
                BCDynamicLink.AddPage(retailerId, cboPages.SelectedItem.Text);
            }
        }

        private void cboPages_SelectedIndexChanged(object sender, EventArgs e) {
            LinkItems = new ArrayList();
            LinkItems.AddRange(BCDynamicLink.GetLinks(pageId, Place.Header, BCDynamicLink.DefaultCulture));
            grdLinks.EditItemIndex = -1;
            grdLinks.DataBind();
            chkUseDefaultMenu.Visible = cboPages.SelectedValue != BCDynamicLink.DefaultMenuPage;
            chkUseDefaultMenu.Checked = LinkItems.Count == 0;
            chkUseDefaultMenu_CheckedChanged(null, null);
        }

        private void grdLinks_DataBinding(object sender, EventArgs e) {
            grdLinks.DataSource = LinkItems;
        }

        private void grdLinks_ItemDataBound(object sender, DataGridItemEventArgs e) {
            DataGridItem item = e.Item;
            switch (item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.EditItem:
                    ItemBound(e.Item);
                    break;
            }
        }

        private void ItemBound(DataGridItem item) {
            LinkItem link = (LinkItem)LinkItems[item.ItemIndex];

            ImageButton btnEdit = (ImageButton)item.FindControl(BtnItemEditName);
            ImageButton btnUpdate = (ImageButton)item.FindControl(BtnItemUpdateName);
            ImageButton btnCancel = (ImageButton)item.FindControl(BtnItemCancelName);
            ImageButton btnDelete = (ImageButton)item.FindControl(BtnItemDeleteName);

            Label lblLinkName = (Label)item.FindControl(LblLinkName);
            TextBox txtLinkName = (TextBox)item.FindControl(TxtLinkName);
            Label lblLinkUrl = (Label)item.FindControl(LblLinkUrl);
            TextBox txtLinkUrl = (TextBox)item.FindControl(TxtLinkUrl);
            CheckBox chkIsNewWindow = (CheckBox)item.FindControl(ChkIsNewWindow);
            CheckBox chkIsVisible = (CheckBox)item.FindControl(ChkIsVisible);

            bool edit = item.ItemType == ListItemType.EditItem;

            if (edit) {
                txtLinkName.Text = link.Text;
                if (!IsStartupScriptRegistered(SetFocusScriptKey))
                    RegisterStartupScript(SetFocusScriptKey, string.Format(SetFocusScript, txtLinkName.ClientID));
                txtLinkUrl.Text = link.Url;
            } else {
                lblLinkName.Text = link.Text;
                lblLinkUrl.Text = link.Url;
            }

            chkIsNewWindow.Checked = link.IsNewWindow;
            chkIsNewWindow.Enabled = edit;
            chkIsVisible.Checked = link.IsVisible;
            chkIsVisible.Enabled = edit;
            btnEdit.Visible = !edit;
            btnDelete.Visible = !edit;
            btnUpdate.Visible = edit;
            btnCancel.Visible = edit;
        }

        private void btnNew_Click(object sender, EventArgs e) {
            LinkItem item = new LinkItem();
            item.Align = Align.Right;

            if (LinkItems.Count > 0) {
                LinkItem lastItem = (LinkItem)LinkItems[LinkItems.Count - 1];
                item.SortOrder = lastItem.SortOrder + 1;
            }
            BCDynamicLink.AddDynamicLink(retailerId, pageId, Place.Header, BCDynamicLink.DefaultCulture, item);
            LinkItems.Add(item);
            grdLinks.EditItemIndex = LinkItems.Count - 1;
            grdLinks.DataBind();
            DynamicLinkHelper.ResetDesignerDynamicLinks();
        }

        private void grdLinks_EditCommand(object source, DataGridCommandEventArgs e) {
            grdLinks.EditItemIndex = e.Item.ItemIndex;
            grdLinks.DataBind();
        }

        private void grdLinks_UpdateCommand(object source, DataGridCommandEventArgs e) {
            LinkItem link = (LinkItem)LinkItems[e.Item.ItemIndex];
            if (link != null) {
                TextBox txtLinkName = (TextBox)e.Item.FindControl(TxtLinkName);
                TextBox txtLinkUrl = (TextBox)e.Item.FindControl(TxtLinkUrl);
                CheckBox chkIsNewWindow = (CheckBox)e.Item.FindControl(ChkIsNewWindow);
                CheckBox chkIsVisible = (CheckBox)e.Item.FindControl(ChkIsVisible);
                link.Text = txtLinkName.Text;
                link.Url = txtLinkUrl.Text;
                link.IsNewWindow = chkIsNewWindow.Checked;
                link.IsVisible = chkIsVisible.Checked;
                BCDynamicLink.UpdateDynamicLink(retailerId, pageId, Place.Header, BCDynamicLink.DefaultCulture, link);
                DynamicLinkHelper.ResetDesignerDynamicLinks();
            }

            grdLinks.EditItemIndex = -1;
            grdLinks.DataBind();
        }

        private void grdLinks_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdLinks.EditItemIndex = -1;
            grdLinks.DataBind();
        }

        private void grdLinks_DeleteCommand(object source, DataGridCommandEventArgs e) {
            LinkItem link = (LinkItem)LinkItems[e.Item.ItemIndex];
            if (link != null) {
                BCDynamicLink.DeleteDynamicLink(pageId, link.LinkId, Place.Header);
                LinkItems.RemoveAt(e.Item.ItemIndex);
                DynamicLinkHelper.ResetDesignerDynamicLinks();
            }
            grdLinks.EditItemIndex = -1;
            grdLinks.DataBind();
        }

        protected void btnItemUp_Click(object sender, ImageClickEventArgs e) {
            DataGridItem grdItem = (DataGridItem)((ImageButton)sender).Parent.Parent;
            if (grdItem.ItemIndex > 0) {
                LinkItem item = (LinkItem)LinkItems[grdItem.ItemIndex];
                LinkItem itemPrev = (LinkItem)LinkItems[grdItem.ItemIndex - 1];
                int sortOrder = itemPrev.SortOrder;
                itemPrev.SortOrder = item.SortOrder;
                item.SortOrder = sortOrder;

                LinkItems.RemoveAt(grdItem.ItemIndex);
                LinkItems.Insert(grdItem.ItemIndex - 1, item);
                BCDynamicLink.UpdateDynamicLink(retailerId, pageId, Place.Header, BCDynamicLink.DefaultCulture, item);
                BCDynamicLink.UpdateDynamicLink(retailerId, pageId, Place.Header, BCDynamicLink.DefaultCulture, itemPrev);
                DynamicLinkHelper.ResetDesignerDynamicLinks();
            }
            grdLinks.EditItemIndex = -1;
            grdLinks.DataBind();
        }

        protected void btnItemDown_Click(object sender, ImageClickEventArgs e) {
            DataGridItem grdItem = (DataGridItem)((ImageButton)sender).Parent.Parent;
            if (grdItem.ItemIndex < LinkItems.Count - 1) {
                LinkItem item = (LinkItem)LinkItems[grdItem.ItemIndex];
                LinkItem itemNext = (LinkItem)LinkItems[grdItem.ItemIndex + 1];
                int sortOrder = itemNext.SortOrder;
                itemNext.SortOrder = item.SortOrder;
                item.SortOrder = sortOrder;

                LinkItems.RemoveAt(grdItem.ItemIndex);
                LinkItems.Insert(grdItem.ItemIndex + 1, item);
                BCDynamicLink.UpdateDynamicLink(retailerId, pageId, Place.Header, BCDynamicLink.DefaultCulture, item);
                BCDynamicLink.UpdateDynamicLink(retailerId, pageId, Place.Header, BCDynamicLink.DefaultCulture, itemNext);
                DynamicLinkHelper.ResetDesignerDynamicLinks();
            }
            grdLinks.EditItemIndex = -1;
            grdLinks.DataBind();
        }

        private void btnApplyBackgroundColor_Click(object sender, EventArgs e) {
            if (!IsValid)
                return;

            try {
                BCPlaceProperty.Update(retailerId, Place.Header, new PlaceProperty(retailerId, Place.Footer.ToString(), txtBackgroundColor.Text));
                HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(RefreshPlacePropertyUrl);
                WebResponse response= request.GetResponse();
                response.Close();
            } catch (BaseBusinessException ex) {
                MsgBox.Alert(this, ex.Message);
            }
        }
    }
}