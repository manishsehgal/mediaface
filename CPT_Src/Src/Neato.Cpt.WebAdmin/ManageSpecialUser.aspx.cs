using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Exceptions;
using Neato.Cpt.WebAdmin.Controls;
using Neato.Cpt.WebAdmin.Helpers;
using Neato.Cpt.WebDesigner;
using Neato.Cpt.WebDesigner.Helpers;
using Neato.Cpt.WebDesigner.HttpHandlers;

namespace Neato.Cpt.WebAdmin {

	public class ManageSpecialUser : System.Web.UI.Page {

        private const string LblItemEmail     = "lblItemEmail";
        private const string TxtItemEmail     = "txtItemEmail";
        private const string VldEmailAddress  = "vldEmailAddress";
        private const string VldValidEmail    = "vldValidEmail";
        private const string VldUniqueEmail   = "vldUniqueEmail";
        private const string LblItemShowPaper = "lblItemShowPaper";
        private const string ChkItemShowPaper = "chkItemShowPaper";
        private const string LblItemNotify    = "lblItemNotify";
        private const string ChkItemNotify    = "chkItemNotify";

        protected TextBox txtEmail;
        protected Button  btnSearch;

        protected Button   btnNew;
        protected DataGrid grdUsers;


        private bool NewMode {
            get { return (bool)ViewState["NewModeKey"]; }
            set { ViewState["NewModeKey"] = value; }
        }

        #region Url
        private const string RawUrl = "ManageSpecialUser.aspx";
        /*
        public static Uri Url(string brandFilter, string deviceFilter, string deviceTypeFilter) {
            return UrlHelper.BuildUrl(RawUrl,
                                      PaperBrandFilterKey, brandFilter,
                                      DeviceFilterKey, deviceFilter,
                                      DeviceTypeFilterKey, deviceTypeFilter);
        }
        */
        public static Uri Url() {
            return UrlHelper.BuildUrl(RawUrl);
        }
        #endregion

		private void Page_Load(object sender, System.EventArgs e) {
            if (!IsPostBack) {
                InitFilters();
                NewMode = false;
                DataBind();
            }
		}

        private void InitFilters() {
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			InitializeComponent();
			base.OnInit(e);
		}
		private void InitializeComponent() {    
			this.Load += new System.EventHandler(this.Page_Load);
            this.DataBinding += new EventHandler(this.Page_DataBinding);
            this.grdUsers.ItemDataBound += new DataGridItemEventHandler(this.grdUsers_ItemDataBound);
            this.grdUsers.EditCommand += new DataGridCommandEventHandler(this.grdUsers_EditCommand);
            this.grdUsers.DeleteCommand += new DataGridCommandEventHandler(this.grdUsers_DeleteCommand);
            this.grdUsers.UpdateCommand += new DataGridCommandEventHandler(this.grdUsers_UpdateCommand);
            this.grdUsers.CancelCommand += new DataGridCommandEventHandler(this.grdUsers_CancelCommand);
            this.btnNew.Click += new EventHandler(this.btnNew_Click);
            this.btnSearch.Click += new EventHandler(this.btnSearch_Click);
            this.PreRender += new EventHandler(this.Page_PreRender);
		}
		#endregion

        private void Page_DataBinding(object sender, EventArgs e) {
            ArrayList users = new ArrayList(BCSpecialUser.GetSpecialUsers());

            string email = txtEmail.Text.Trim();
            if (email.Length > 0)
            {
                string sub = email.ToLower();
                for (int i=0; i<users.Count; i++) {
                    SpecialUser user = (SpecialUser)users[i];
                    if (user.Email.IndexOf(sub) == -1) {
                        users.RemoveAt(i--);
                    }
                }
            }
            txtEmail.Text = "";

            if (NewMode) {
                users.Insert(0, BCSpecialUser.NewSpecialUser());
                grdUsers.EditItemIndex = 0;
            } else if (grdUsers.EditItemIndex >= 0) {
                int userId = (int)grdUsers.DataKeys[grdUsers.EditItemIndex];
                SpecialUserBase user = new SpecialUserBase(userId);
                int index = users.IndexOf(user);
                grdUsers.EditItemIndex = index;
            }
            grdUsers.DataSource = users;
            grdUsers.DataKeyField = SpecialUser.IdField;
        }

        private void grdUsers_ItemDataBound(object sender, DataGridItemEventArgs e) {
            switch (e.Item.ItemType) {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                case ListItemType.SelectedItem:
                    ItemDataBound(e.Item);
                    break;
                case ListItemType.EditItem:
                    EditItemDataBound(e.Item);
                    break;
            }
        }

        private void ItemDataBound(DataGridItem item) {
            SpecialUser user = (SpecialUser)item.DataItem;

            Label lblItemEmail = (Label)item.FindControl(LblItemEmail);
            lblItemEmail.Text = HttpUtility.HtmlEncode(user.Email);

            Label lblItemShowPaper = (Label)item.FindControl(LblItemShowPaper);
            lblItemShowPaper.Text = user.ShowPaper ? "+" : "";

            Label lblItemNotify = (Label)item.FindControl(LblItemNotify);
            lblItemNotify.Text = user.NotifyPaperState ? "+" : "";
        }

        private void EditItemDataBound(DataGridItem item) {
            SpecialUser user = (SpecialUser)item.DataItem;

            TextBox txtItemEmail = (TextBox)item.FindControl(TxtItemEmail);
            txtItemEmail.Text = HttpUtility.HtmlEncode(user.Email);
            JavaScriptHelper.RegisterFocusScript(this, txtItemEmail.ClientID);

            CheckBox chkItemShowPaper = (CheckBox)item.FindControl(ChkItemShowPaper);
            chkItemShowPaper.Checked = user.ShowPaper;

            CheckBox chkItemNotify = (CheckBox)item.FindControl(ChkItemNotify);
            chkItemNotify.Checked = user.NotifyPaperState;
        }

        private void grdUsers_EditCommand(object source, DataGridCommandEventArgs e) {
            grdUsers.EditItemIndex = e.Item.ItemIndex;
            NewMode = false;
            DataBind();
        }

        private void grdUsers_DeleteCommand(object source, DataGridCommandEventArgs e) {
            grdUsers.EditItemIndex = -1;
            int userId = (int)grdUsers.DataKeys[e.Item.ItemIndex];
            SpecialUserBase user = new SpecialUserBase(userId);
            BCSpecialUser.Delete(user);
            NewMode = false;
            DataBind();
        }

        private void grdUsers_UpdateCommand(object source, DataGridCommandEventArgs e) 
        {
            RequiredFieldValidator vldEmailAddress = (RequiredFieldValidator)e.Item.FindControl(VldEmailAddress);
            if (!vldEmailAddress.IsValid) return;

            CustomValidator vldValidEmail = (CustomValidator)e.Item.FindControl(VldValidEmail);
            if (!vldValidEmail.IsValid) return;

            int userId = (int)grdUsers.DataKeys[e.Item.ItemIndex];

            SpecialUser user = null;
            if (NewMode) {
                user = BCSpecialUser.NewSpecialUser();
                user.Id = userId;
            } else {
                user = BCSpecialUser.GetSpecialUserById(new SpecialUserBase(userId));
            }
            if (user == null) return;

            TextBox  txtItemEmail     = (TextBox) e.Item.FindControl(TxtItemEmail);
            CheckBox chkItemShowPaper = (CheckBox)e.Item.FindControl(ChkItemShowPaper);
            CheckBox chkItemNotify    = (CheckBox)e.Item.FindControl(ChkItemNotify);

            if (NewMode || txtItemEmail.Text.ToLower() != user.Email.Trim().ToLower()) {
                CustomValidator vldUniqueEmail = (CustomValidator)e.Item.FindControl(VldUniqueEmail);
                if (!vldUniqueEmail.IsValid) return;
            }

            user.Email = txtItemEmail.Text.Trim();
            user.ShowPaper = chkItemShowPaper.Checked;
            user.NotifyPaperState = chkItemNotify.Checked;

            BCSpecialUser.Save(user);

            grdUsers.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void grdUsers_CancelCommand(object source, DataGridCommandEventArgs e) {
            grdUsers.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void btnNew_Click(object sender, EventArgs e) {
            NewMode = true;
            DataBind();
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            grdUsers.EditItemIndex = -1;
            NewMode = false;
            DataBind();
        }

        private void Page_PreRender(object sender, EventArgs e) {
            grdUsers.Visible = grdUsers.Items.Count > 0;
        }

        public void vldValidEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            args.IsValid = FormatValidator.EnsureValidEmail(args.Value.Trim());
        }
        public void vldUniqueEmail_ServerValidate(object source, ServerValidateEventArgs args) {
            ArrayList users = new ArrayList(BCSpecialUser.GetSpecialUsers(args.Value.Trim()));
            args.IsValid = users.Count == 0;
        }
    }
}
