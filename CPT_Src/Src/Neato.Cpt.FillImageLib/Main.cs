using System;

using Neato.Cpt.Business;
using Neato.Cpt.Entity;
using Neato.Cpt.Entity.Helpers;

namespace Neato.Cpt.FillImageLib {
    /// <summary>
    /// Console application for filling Image Library
    /// </summary>
    internal class MainApplication {
        private static string helpString = @"
Usage: -p ""%ImagePath"" [-f ""%FolderName""] [-r ""%RetailerName""]

[] - optional parameters

-p %ImagePath: the dir path to images will be copying into database

-f %FolderName: the name of the target folder in database
(if parameter not present, a bulk of folders and images will be
created in root node)

-r %RetailerName: define Retailer. If parameter is absent, pictures will place in the default retailer.

!!! connectionString to DB locate in Neato.Cpt.FillImageLib.exe.config file

Example:
1)
Neato.Cpt.FillImageLib.exe -p ""C:\Temp\BulkImages"" -f ""Main""
- copy all images from ""c:\Temp\BulkImages"" to image folder ""Main""
(default retailer)

2)
Neato.Cpt.FillImageLib.exe -p ""BulkImages\Temp""
- copy all images from ""BulkImages\Temp"" to root
(default retailer)

3)
Neato.Cpt.FillImageLib.exe -p ""BulkImages\Temp""  -r ""MyRetailerName""
- copy all images from ""BulkImages\Temp"" to root
(retailer with name MyRetailerName)
";

        [STAThread]
        private static void Main(string[] args) {
            try {
                if (
                    args.Length == 0 ||
                    args[0] == "/?" ||
                    args[0] == "-?" ||
                    args[0] == "?" ||
                    args[0] == "/help" ||
                    args[0] == "-help" ||
                    args[0] == "help" ||
                    args[0] == "/h" ||
                    args[0] == "-h" ||
                    args[0] == "h" ||
                    args.Length > 6) {
                    Console.Write(helpString);
                    return;
                }

                string imagePath = CommandLineParser.GetValueByKey(args, "-p");
                string folderName = CommandLineParser.GetValueByKey(args, "-f");
                bool insertIntoRoot = !CommandLineParser.IsKeyPresent(args, "-f");
                bool clearTargetFolder = false;//CommandLineParser.IsKeyPresent(args, "-c");
                string retailerName = CommandLineParser.GetValueByKey(args, "-r");

                Console.WriteLine(@"
imagePath = {0},
folderName = {1},
insertIntoRoot = {2},
retailerName = {3}
",
                    imagePath, folderName, insertIntoRoot
                    /*, clearTargetFolder*/, retailerName);

                Retailer retailer = BCRetailers.GetRetailerByName(retailerName);
                BCImageLibrary.AddContentRecursively(imagePath,
                    folderName, insertIntoRoot, clearTargetFolder, retailer);
                Console.WriteLine("Process complete succesifully");
            } catch (Exception ex) {
                Console.WriteLine("!!! ERROR !!! Data rollback...");
                Console.WriteLine("Error message = " + ex.Message);
                Console.Write(helpString);
            }
        }
    }
}