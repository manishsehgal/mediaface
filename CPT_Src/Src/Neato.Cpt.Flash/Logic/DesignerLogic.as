﻿class Logic.DesignerLogic {
	private static var LocalListener:Object;
	private function DesignerLogic() {
		trace("DesignerLogic .ctor");
		LocalListener = new Object();
	}

	public static function SynchronizeToolbar() {
		ToolbarContent.GetInstance().Synchronize();
	}
	
	public static function FillFace() {
		trace("FaceFill");
		UnselectCurrentUnit();
		ToolPropertiesContainer.GetInstance().ShowFillProperties();
	}

	public static function AddTextUnit() {
		trace("AddTextUnit");
		UnselectCurrentUnit();
		var unit = _global.CurrentFace.AddText(true);
		var bounds = _global.CurrentFace.ContourBounds;
		unit.X = Math.random() * (bounds.xMax-bounds.xMin); 
		unit.Y = Math.random() * (bounds.yMax-bounds.yMin); 
		SATracking.AddText();
		ToolPropertiesContainer.GetInstance().ShowTextProperties();
		// TODO: move code to ChangeUnit handler when created
		QuickToolContent.GetInstance().EnableButtons();
	}
	
	public static function AddImageUnit() {
		trace("AddImageUnit");
		UnselectCurrentUnit();
		ToolPropertiesContainer.GetInstance().ShowImageProperties();
	}

	public static function AddPaintUnit() {
		UnselectCurrentUnit();
		_global.CurrentFace.CreateNewPaintUnit(); //sets onclick to edit area
		QuickToolContent.GetInstance().DisableButtons();
		ToolPropertiesContainer.GetInstance().ShowPaintProperties();
	}

	public static function AddShapeUnit() {
		trace("AddShapeUnit");
		UnselectCurrentUnit();
		ToolPropertiesContainer.GetInstance().ShowShapeProperties();
	}

	public static function AddPlaylistUnit() {
		trace("AddPlaylistUnit");
		UnselectCurrentUnit();
		ToolPropertiesContainer.GetInstance().ShowPlaylistProperties();
	}
	
	public static function ChooseLayout() {
		trace("ChooseLayout");
		UnselectCurrentUnit();
		ToolPropertiesContainer.GetInstance().ShowChooseLayoutProperties();
	}	

	public static function AddGradientUnit() {
		trace("AddGradientUnit");
		UnselectCurrentUnit();
		//ToolPropertiesContainer.GetInstance().ShowGradientProperties();
	}
	
	public static function UnselectCurrentUnit() {
		if (_global.CurrentUnit != null) {
			_global.RessurectEmptyLayoutItem();
			if (_global.CurrentUnit.Text == "" || _global.CurrentUnit.Empty()) {
				_global.CurrentFace.DeleteUnit(_global.CurrentUnit);
				trace("UnselectCurrentUnit: deleted empty text or paint");
			}
			if (_global.Mode == _global.PaintMode) _global.CurrentUnit.ExitPaint();
			_global.SelectionFrame.DetachUnit();
			_global.CurrentUnit = null;
			_global.Mode = _global.InactiveMode;
			// TODO: move logic into QuickToolContent class
			//Selection.setFocus(_root.pnlMainLayer.content.pnlQuickTool.content.ctlToolbarContent.btnMoveRight);//hack
			_global.ChangeUnit();
		} else //PLaylist temporary added while PLUnit not implemented
		if (_global.Mode != _global.FillMode && _global.Mode != _global.PlaylistMode){
			ToolPropertiesContainer.GetInstance().ShowFillProperties();
		}
		QuickToolContent.GetInstance().DisableButtons();
	}
    
    public static function ShowPreviousPanel() {
      switch (_global.Mode) {
        case _global.PaintMode:
            AddPaintUnit(); break;
        case _global.PaintEditMode:
            AddPaintUnit(); break;
        case _global.ImageEditMode:
            AddImageUnit(); break;
        case _global.ShapeEditMode:
            AddShapeUnit(); break;
		default: FillFace();
        }        
    }
	
	public static function ShowUnitProperties() {
		trace("DesignerLogic.ShowUnitProperties()");
		ToolPropertiesContainer.GetInstance().ShowUnitProperties();
	}
}