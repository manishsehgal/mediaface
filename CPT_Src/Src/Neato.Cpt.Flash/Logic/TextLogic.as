﻿class Logic.TextLogic {
	static function ConvertTextToEffect(unit,effect){
		trace("ConvertTextToEffect");
		//if(unit instanceof TextEffectUnit || !(unit instanceof TextUnit))
			//return;
		if(unit instanceof TextEffectUnit )	{
			unit.SetTextEffect(effect);
			unit.Draw();
			return;
		} 
		trace("Unit = "+unit+" mc = "+unit.mc);
		var node:XML = unit.GetXmlNode();
		_global.CurrentFace.CreateEffectUnitFromText(node,effect);
	/*	ClearMovieClip(unit,"TextEffectUnit");
		unit = new TextEffectUnit(unit.mc,node);
		trace("Unit = "+unit);
		_global.CurrentUnit = unit;
		_global.CurrentFace.RegisterOnUnitClickHandler(unit);
		unit.SetTextEffect(effect);
		unit.Draw();*/
	}
	static function ConvertEffectToText(unit){
		if(!(unit instanceof TextEffectUnit))
		{
			trace("ConvertEffectToText: unit not of type TextEffectUnit");
			return;
		}
		/*if(unit instanceof TextUnit)
		{	
			trace("ConvertEffectToText: unit already is instance of type TextUnit");
			return
			
		}*/
		trace("ConvertEffectToText");
		var node:XML = unit.GetXmlNode()
		_global.CurrentFace.CreateTextUnitFromEffect(node);
		/*ClearMovieClip(unit,"TextUnit");
		unit = new TextUnit(unit.mc,node);
		_global.CurrentUnit = unit;
		_global.CurrentFace.RegisterOnUnitClickHandler(unit);
		unit.RestoreText();
		unit.Draw();*/
	}
	/*static function ClearMovieClip(unit,unittype:String){
		var parent = unit.mc._parent;
		var depth:Number = parent.getNextHighestDepth();
		unit.mc.removeMovieClip();
		
		//unit = new Object();
		unit.mc=parent.createEmptyMovieClip(unittype+depth,depth);
	}*/
	
}