﻿var preloader:MovieClip = _root.createEmptyMovieClip("preloader2", this.getNextHighestDepth());
var designer:MovieClip = _root.createEmptyMovieClip("designer", this.getNextHighestDepth());

preloader._x = 455;
preloader._y = 220;
preloader.loadMovie("../Flash/Preloader.swf");


// Check Duplacted flash
var doPost:Boolean = true;
lc1 = new LocalConnection();
lc2 = new LocalConnection();
lc1.connect("starter");
lc1.starter = function() {
	if (doPost) {
		getURL("javascript:Designer_DoFSCommand('SetDuplicatedFlash', 'false');");
		var url:String = "../Flash/Designer.swf";
		if (SALocalWork.IsLocalWork)
			url = "Designer.swf";
			
		designer.loadMovie(url);
	}
	doPost = false;
	lc2.close();
	lc1.send("next", "next");
};

lc2.connect("next");
lc2.next = function() {
	lc2.close();
	getURL("javascript:Designer_DoFSCommand('SetDuplicatedFlash', 'true');");
};

lc1.send("starter", "starter");