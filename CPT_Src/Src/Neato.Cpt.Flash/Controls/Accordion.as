﻿import mx.core.UIObject;
[IconFile("Controls/Icon/Accordion.png")]
class Controls.Accordion extends mx.containers.Accordion {
    function Accordion() {
    }

    [Inspectable(defaultValue="MenuButtonTrueUpSkin")]
    var trueUpSkin:String = "MenuButtonTrueUpSkin";

    [Inspectable(defaultValue="MenuButtonTrueOverSkin")]
    var trueOverSkin:String = "MenuButtonTrueOverSkin";

    [Inspectable(defaultValue="MenuButtonTrueDownSkin")]
    var trueDownSkin:String = "MenuButtonTrueDownSkin";

    [Inspectable(defaultValue="MenuButtonTrueDisabledSkin")]
    var trueDisabledSkin:String = "MenuButtonTrueDisabledSkin";

    [Inspectable(defaultValue="MenuButtonFalseUpSkin")]
    var falseUpSkin:String = "MenuButtonFalseUpSkin";

    [Inspectable(defaultValue="MenuButtonFalseOverSkin")]
    var falseOverSkin:String = "MenuButtonFalseOverSkin";

    [Inspectable(defaultValue="MenuButtonFalseDownSkin")]
    var falseDownSkin:String = "MenuButtonFalseDownSkin";

    [Inspectable(defaultValue="MenuButtonFalseDisabledSkin")]
    var falseDisabledSkin:String = "MenuButtonFalseDisabledSkin";
}
