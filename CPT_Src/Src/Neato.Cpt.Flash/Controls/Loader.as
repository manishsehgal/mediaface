﻿import mx.utils.Delegate;
[TagName("Loader")]
[IconFile("Controls/Icon/Loader.png")]
class Controls.Loader extends mx.controls.Loader {
	function Loader() {
	}

	public function RegisterOnCompleteHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("complete", Delegate.create(scopeObject, callBackFunction));
    }
}
