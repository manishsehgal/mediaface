﻿import mx.core.UIObject;
import mx.utils.Delegate;
[IconFile("Controls/Icon/Tree.png")]
class Controls.Tree extends mx.controls.Tree {
    function Tree() {
        super();
        vScrollPolicy = "auto";
		setStyle("indentation", 10);
		setStyle("disclosureClosedIcon", "EmptyContent");
		setStyle("disclosureOpenIcon", "EmptyContent");
		setStyle("folderClosedIcon", "TreeFolderClosed");
		setStyle("folderOpenIcon", "TreeFolderOpen");
    }

	public function RegisterOnChangeHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("change", Delegate.create(scopeObject, callBackFunction));
    }

	public function RegisterOnNodeOpenHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("nodeOpen", Delegate.create(scopeObject, callBackFunction));
    }

	public function RegisterOnNodeCloseHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("nodeClose", Delegate.create(scopeObject, callBackFunction));
    }
}
