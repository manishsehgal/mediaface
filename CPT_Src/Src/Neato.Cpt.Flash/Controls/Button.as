﻿import mx.utils.Delegate;
[IconFile("Controls/Icon/Button.png")]
[Event("click")]
[Event("press")]
[Event("rollOver")]
[Event("rollOut")]
class Controls.Button extends mx.controls.Button {

	[Inspectable(defaultValue="ButtonTrueUpSkin")]
	var trueUpSkin:String = "ButtonTrueUpSkin";
	[Inspectable(defaultValue="")]
	var trueUpIcon:String = "";
	[Inspectable(defaultValue="")]
	var trueUpStyleName:String = "";

	[Inspectable(defaultValue="ButtonTrueOverSkin")]
	var trueOverSkin:String = "ButtonTrueOverSkin";
	[Inspectable(defaultValue="")]
	var trueOverStyleName:String = "";
	[Inspectable(defaultValue="")]
	var trueOverIcon:String = "";

	[Inspectable(defaultValue="ButtonTrueDownSkin")]
	var trueDownSkin:String = "ButtonTrueDownSkin";
	[Inspectable(defaultValue="")]
	var trueDownStyleName:String = "";
	[Inspectable(defaultValue="")]
	var trueDownIcon:String = "";

	[Inspectable(defaultValue="ButtonTrueDisabledSkin")]
	var trueDisabledSkin:String = "ButtonTrueDisabledSkin";
	[Inspectable(defaultValue="")]
	var trueDisabledStyleName:String = "";
	[Inspectable(defaultValue="")]
	var trueDisabledIcon:String = "";

	[Inspectable(defaultValue="ButtonFalseUpSkin")]
	var falseUpSkin:String = "ButtonFalseUpSkin";
	[Inspectable(defaultValue="")]
	var falseUpStyleName:String = "";
	[Inspectable(defaultValue="")]
	var falseUpIcon:String = "";

	[Inspectable(defaultValue="ButtonFalseOverSkin")]
	var falseOverSkin:String = "ButtonFalseOverSkin";
	[Inspectable(defaultValue="")]
	var falseOverStyleName:String = "";
	[Inspectable(defaultValue="")]
	var falseOverIcon:String = "";

	[Inspectable(defaultValue="ButtonFalseDownSkin")]
	var falseDownSkin:String = "ButtonFalseDownSkin";
	[Inspectable(defaultValue="")]
	var falseDownStyleName:String = "";
	[Inspectable(defaultValue="")]
	var falseDownIcon:String = "";

	[Inspectable(defaultValue="ButtonFalseDisabledSkin")]
	var falseDisabledSkin:String = "ButtonFalseDisabledSkin";
	[Inspectable(defaultValue="")]
	var falseDisabledStyleName:String = "";
	[Inspectable(defaultValue="")]
	var falseDisabledIcon:String = "";

	[Inspectable(defaultValue=false)]
	var centerContent:Boolean = false;
	[Inspectable(defaultValue=true)]
	var useHandCursor:Boolean = true;

	function Button() {
		super();
		if (this.enabled) {
			this.setStyle("styleName", _global.styles[this.toggle ? this.trueUpStyleName : this.falseUpStyleName]);
		} else {
			this.setStyle("styleName", _global.styles[this.toggle ? this.trueDisabledStyleName : this.falseDisabledStyleName]);
		}
	}

	function init(Void):Void {
		var tempUseHandCursor = useHandCursor;
		super.init();
		useHandCursor = (tempUseHandCursor == true);
	}
	
	public function RegisterOnRollOverHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("rollOver", Delegate.create(scopeObject, callBackFunction));
    }

	function onRollOver() {
		super.onRollOver.call(this);
		if (this.enabled) {
			this.setStyle("styleName", _global.styles[this.toggle ? this.trueOverStyleName : this.falseOverStyleName]);
		} else {
			this.setStyle("styleName", _global.styles[this.toggle ? this.trueDisabledStyleName : this.falseDisabledStyleName]);
		}
		var eventObject = {type:"rollOver", target:this};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnRollOutHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("rollOut", Delegate.create(scopeObject, callBackFunction));
    }

	function onRollOut() {
		super.onRollOut.call(this);
		if (this.enabled) {
			this.setStyle("styleName", _global.styles[this.toggle ? this.trueUpStyleName : this.falseUpStyleName]);
		} else {
			this.setStyle("styleName", _global.styles[this.toggle ? this.trueDisabledStyleName : this.falseDisabledStyleName]);
		}
		var eventObject = {type:"rollOut", target:this};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnPressHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("press", Delegate.create(scopeObject, callBackFunction));
    }

	function onPress() {
		super.onPress.call(this);
		if (this.enabled) {
			this.setStyle("styleName", _global.styles[this.toggle ? this.trueDownStyleName : this.falseDownStyleName]);
		} else {
			this.setStyle("styleName", _global.styles[this.toggle ? this.trueDisabledStyleName : this.falseDisabledStyleName]);
		}
		var eventObject = {type:"press", target:this};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnReleaseHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("release", Delegate.create(scopeObject, callBackFunction));
    }

	function onRelease() {
		super.onRelease.call(this);
		if (this.enabled) {
			this.setStyle("styleName", _global.styles[this.toggle ? this.trueUpStyleName : this.falseUpStyleName]);
		} else {
			this.setStyle("styleName", _global.styles[this.toggle ? this.trueDisabledStyleName : this.falseDisabledStyleName]);
		}
		var eventObject = {type:"release", target:this};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnDragOutHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("dragOut", Delegate.create(scopeObject, callBackFunction));
    }

	function onDragOut() {
		super.onDragOut.call(this);
		var eventObject = {type:"dragOut", target:this};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnClickHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("click", Delegate.create(scopeObject, callBackFunction));
    }
}