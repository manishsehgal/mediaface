﻿import mx.utils.Delegate;
[TagName("ImageHolder")]
[Event("click")]
[Event("doubleclick")]
[Event("contentLoaded")]
class Controls.ImageHolder extends Controls.Holder {
	static var symbolName:String = "ImageHolder";
	static var symbolOwner:Object = Object(Controls.ImageHolder);
	var className:String = "ImageHolder";
	var serializeDataBinds:Boolean = true;

	private var ctlImage;
	
	function ImageHolder() {
	}

	function init():Void {
		super.init();
	}

	function createChildren():Void {
		super.createChildren();
		if (ctlImage == undefined) {
			ctlImage =  this.createObject("LoaderEx", "ctlImage", this.getNextHighestDepth());
			ctlImage.autoLoad = false;
			ctlImage.scaleContent = true;
		}
	}

	//region Data Binding
	public function DataBind() {
		ctlImage.load(DataSource.url);
	}
	//endregion Data Binding

	function size():Void {
		super.size();
		ctlImage.setSize(this.width * 0.9, this.height * 0.9);
		ctlImage.move(this.width * 0.1 / 2, this.height * 0.1 / 2);
	}

	private function onLoad() {
		ctlImage.RegisterOnCompleteHandler(this, ctlImage_OnComplete);
	}
	private function ctlImage_OnComplete() {
		size();
		onContentLoaded();
	}
}