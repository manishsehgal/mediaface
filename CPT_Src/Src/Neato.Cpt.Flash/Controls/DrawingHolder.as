﻿import mx.utils.Delegate;
[TagName("VectorGraphicsHolder")]
[Event("click")]
[Event("doubleclick")]
[Event("contentLoaded")]
class Controls.DrawingHolder extends Controls.Holder {
	static var symbolName:String = "DrawingHolder";
	static var symbolOwner:Object = Object(Controls.DrawingHolder);
	var className:String = "DrawingHolder";
	static var serializeDataBinds:Boolean = true;

	private var ctlContent;
	
	function DrawingHolder() {
	}

	function init():Void {
		super.init();
	}

	function createChildren():Void {
		super.createChildren();
		if (ctlContent == undefined) {
			ctlContent = this.createEmptyMovieClip("ctlContent", this.getNextHighestDepth());
		}
	}

	//region Data Binding
	public function DataBind() {
		var x:Number = 0;
		var y:Number = 0;
		var rotation:Number = 0;
		var color:Number = 0xf4d217;
		var thickness:Number = (DataSource.type == "shape") ? 5 : 1;
		var filled:Boolean = false;
		
		if (DataSource.type == "brush") {
			trace("brushbrushbrushbrushbrushbrushbrushbrushbrushbrushbrushbrushbrushbrush");
			filled = DataSource.filled;
			TooltipHelper.SetTooltip(this, "ToolProperties", DataSource.tipID);
		}
		ctlContent.clear();
		Logic.DrawingHelper.DrawSequence(DataSource.drawSequence, ctlContent, x, y, rotation, color, thickness, filled);
		var layoutItemColor:Number = 0xFF6666;
  		Logic.DrawingHelper.DrawSequence(DataSource.textSequence, ctlContent, x, y, rotation, layoutItemColor, thickness, true);
		size();
		onContentLoaded();
	}
	//endregion Data Binding
	
	function size():Void {
		super.size();
		var width = this.width;
		var height = this.height;

		ctlContent._xscale = 100;
		ctlContent._yscale = 100;

		var xscale = (width * 0.9) / ctlContent._width * 100;
		var yscale = (height * 0.9) / ctlContent._height * 100;

		var scale = xscale < yscale ? xscale : yscale;
		if (scale > 100) {
			scale = 100;
		}
		ctlContent._xscale = scale;
		ctlContent._yscale = scale;

		ctlContent._x = 0;
		ctlContent._y = 0;
		var bounds = ctlContent.getBounds(ctlContent._parent);

		ctlContent._x = (width - ctlContent._width) / 2 - bounds.xMin;
		ctlContent._y = (height - ctlContent._height) / 2 - bounds.yMin;
	}
}