﻿[Event("update")]
class Controls.MiniPreview extends mx.core.UIComponent {
	private var buttons:Array;
	private var mc:MovieClip;

	var mcWidth:Number = 0;
	var mcHeight:Number = 0;
	var fillCurrent:Number = 0;
	var fillNoncurrent:Number = 0;

	function MiniPreview(mc:MovieClip) {
		trace("Mini preview");
		/*
		MiniPreviewWidth=width;
		MiniPreviewHeight=height;
		this._width=width;
		this._height=height;
		*/
		this.mc = mc;
		buttons = new Array();
		RegenerateView();
		mcWidth = mc._width;
		mcHeight = mc._height;
	}

	function RegenerateView():Void {
		var count = _global.Faces.length;
		var isFirstCall:Boolean = (buttons.length == 0 ? true : false);
		var parent = this;
		var currentFace:Number = -1;
		for (var i = 0; i < count; ++i) {
			var face:FaceUnit = _global.Faces[i];
			if (face.Id == _global.CurrentFace.Id) {
				currentFace = i;
			}
			var depth:Number = i;
			var mcFaceMask:MovieClip;
			if (isFirstCall == true) {
				mcFaceMask = mc.createEmptyMovieClip("FaceMask" + i, depth + 1);
			} else {
				mcFaceMask = buttons[i];
				mcFaceMask.clear();
			}
			mcFaceMask.lineStyle(0, 0xFFFFFF);
			mcFaceMask.beginFill(i == currentFace ? fillCurrent : fillNoncurrent, 100);
			face.DrawContour(mcFaceMask, true);
			mcFaceMask.endFill();
			mcFaceMask._x = face.PaperX;
			mcFaceMask._y = face.PaperY;
			mcFaceMask.FaceNum = i;
			isFirstCall ? buttons.push(mcFaceMask) : i = i;
		}
		mc.lineStyle(undefined, 0xFF0000);
		//
		mc.moveTo(0, 0);
		mc.lineTo(0, mc._width);
		mc.lineTo(mc._height, mc._width);
		mc.lineTo(mc._height, 0);
		mc.lineTo(0, 0);
		trace("minipreview:_xscale = " + mc._xscale + " |||||| _yscale = " + mc._yscale);
		trace("minipreview:_width = " + mc._width + " |||||| _heigth = " + mc._height);
	}

	function MC():MovieClip {
		return mc;
	}

	function GetButtonByNum(num:Number):MovieClip {
		return buttons[num];
	}

	function GetCount():Number {
		return buttons.length;
	}

	function UpdateSize(width:Number, height:Number) {
		//var mcWidth:Number=this.width;
		//var mcHeight:Number=this.height;
		var k:Number = mcWidth / mcHeight;
		var kN:Number = width / height;
		if (k >= 1) {
			if (kN >= 1) {
				mcWidth = width;
				mcHeight = width / k;
			} else {
				mcWidth = height * k;
				mcHeight = height;
			}
		} else {
			if (kN <= 1) { 
				mcWidth = width;
				mcHeight = width / k;
			} else {
				mcWidth = height * k;
				mcHeight = height;
			}
		}
		/*
		if(mcWidth > width) {
				var k2:Number=width / mcWidth;
				mcWidth = width;
				mcHeight *= k2;
		}
		if(mcHeight > height)
		{
				var k2:Number = height / mcHeight;
				mcWidth *= k2;
				mcHeight = height;
				
		}
		*/
		mc._width = mcWidth;
		mc._height = mcHeight;
		trace("minipreview:_xscale = " + mc._xscale + " |||||| _yscale = " + mc._yscale);
		trace("minipreview:_width = " + mc._width + " |||||| _heigth = " + mc._height);
	}
}