﻿import mx.utils.Delegate;
import mx.core.UIObject;
import mx.controls.*;
[Event("exit")]
[Event("EndPainting")]
class PaintProperties extends UIObject {
	private var chkKeepProportions:CheckBox;
	private var btnDelete:MenuButton;
	private var btnRestoreProportions:MenuButton;
	private var lblPaintEdit:Label;
	private var lblBrushColor:Label;
	private var btnContinueAndClose:MenuButton;
	private var btnUndo:MenuButton;
	private var ctlBrushType;
	private var scrollBrushWeight:UIScrollBar;
	private var defaultBrushes:Array;
	private var selectedBrushesGroup:Array;
	private var lblBrushWeight:Label;
		
	private var lineColorTool:Tools.ColorSelectionTool.ColorTool;
	//private var lineColor:Number;
	
	public function PaintProperties() {
		this.btnDelete.setStyle("styleName", "ToolPropertiesActiveButton");
		this.lblPaintEdit.setStyle("styleName", "ToolPropertiesCaption");
		this.btnContinueAndClose.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnUndo.setStyle("styleName", "ToolPropertiesActiveButton");
	}
	
	private function onLoad() {
		trace("PaintProperties.onLoad");
		btnDelete.RegisterOnClickHandler(this, _global.DeleteCurrentUnit);
		chkKeepProportions.addEventListener("click", Delegate.create(this, chkKeepProportions_OnClick));
		btnRestoreProportions.addEventListener("click", Delegate.create(this, btnRestoreProportions_OnClick));
		btnContinueAndClose.addEventListener("click", Delegate.create(this, btnContinueAndClose_OnClick));
		btnUndo.addEventListener("click", Delegate.create(this, btnUndo_OnClick));
		lineColorTool.RegisterOnSelectHandler(this, lineColorTool_OnSelect);
		scrollBrushWeight.addEventListener("scroll", Delegate.create(this, scrollBrushWeight_OnScroll));
		
		if (ctlBrushType == undefined) {
			ctlBrushType = this.createObject("HList", "ctlBrushType", this.getNextHighestDepth());
		}
			
		defaultBrushes = _global.Brushes.GetBrushDefaultsArray();
		ctlBrushType.RegisterOnChangeHandler(this, ctlBrushType_OnChange);
		//ctlBrushType.RegisterOnApplyHandler(this, ctlBrushType_OnApply);

		DataBind();
		InitLocale();
	}
	
	public function DataBind():Void {
		trace("PaintProperties.DataBind Mode = " + _global.Mode);
		chkKeepProportions.selected = _global.CurrentUnit.KeepProportions;
		chkKeepProportions.enabled =_global.CurrentUnit.IsNonScalable;
		
		var lastUsedBrushIndex = 0;
		if (ctlBrushType.SelectedItemIndex>0) lastUsedBrushIndex = ctlBrushType.SelectedItemIndex;
		
		ctlBrushType.DataSource = defaultBrushes;
		ctlBrushType.ItemLinkageName = "DrawingHolder";
		ctlBrushType.DataBind();
		ctlBrushType.SelectedItemIndex = lastUsedBrushIndex;
		
		if (lineColorTool.RGB == undefined) lineColorTool.SetColorState(0);
		
		//set brush and color for new paint unit
		_global.CurrentUnit.PaintColor = lineColorTool.RGB;
		_global.CurrentUnit.Brush = defaultBrushes[ctlBrushType.SelectedItemIndex];
	}

	private function InitLocale() {
		//_global.LocalHelper.LocalizeInstance(btnDelete, "ToolProperties", "IDS_BTNDELETE");
		_global.LocalHelper.LocalizeInstance(chkKeepProportions, "ToolProperties", "IDS_CHKKEEPPROPORTIONS");
		_global.LocalHelper.LocalizeInstance(btnRestoreProportions, "ToolProperties", "IDS_BTNRESTOREPROPORTIONS");
		_global.LocalHelper.LocalizeInstance(lblPaintEdit, "ToolProperties", "IDS_LBLPAINTEDIT");
		_global.LocalHelper.LocalizeInstance(lblBrushColor, "ToolProperties", "IDS_LBLBRUSHCOLOR");
		//_global.LocalHelper.LocalizeInstance(btnContinueAndClose, "ToolProperties", "IDS_BTNCONTINUE");
		//_global.LocalHelper.LocalizeInstance(btnUndo, "ToolProperties", "IDS_BTNUNDO");
		_global.LocalHelper.LocalizeInstance(lblBrushWeight, "ToolProperties", "IDS_LBLBRUSHWEIGHT");
		
		TooltipHelper.SetTooltip(btnDelete, "ToolProperties", "IDS_TOOLTIP_DELETE");
		TooltipHelper.SetTooltip(btnContinueAndClose, "ToolProperties", "IDS_TOOLTIP_CONTINUE");
		TooltipHelper.SetTooltip(btnUndo, "ToolProperties", "IDS_TOOLTIP_UNDO");
		TooltipHelper.SetTooltip(chkKeepProportions, "ToolProperties", "IDS_TOOLTIP_KEEPPROPORTIONS");
		TooltipHelper.SetTooltip(btnRestoreProportions, "ToolProperties", "IDS_TOOLTIP_RESTOREPROPORTIONS");
    }
	
	private function lineColorTool_OnSelect(eventObject) {
		_global.CurrentUnit.PaintColor = eventObject.color;
	}
	
	//Actions
	private function ctlBrushType_OnChange(eventObject) {
		//find default brush for the group and set it
		var brush = defaultBrushes[ctlBrushType.SelectedItemIndex];
		_global.CurrentUnit.Brush = brush;
		
		//fill current group
		selectedBrushesGroup = _global.Brushes.GetBrushesArrayByGroup(brush.group);
		
		//set up size control
		scrollBrushWeight.setScrollProperties(1, 0, selectedBrushesGroup.length-1);
		var positionInGroup = brush.positionInGroup;
		if (positionInGroup == undefined) {
			positionInGroup = GetBrushIndexInArray(selectedBrushesGroup, brush);
		}
		scrollBrushWeight.scrollPosition = positionInGroup;
	}
	
	private function GetBrushIndexInArray(brushesArray:Array, brush:Object):Number {
		for (var i:Number=0; i<brushesArray.length; ++i) {
			if (brushesArray[i] == brush) return i;
		}
		return 0;
	}
	
	private function scrollBrushWeight_OnScroll(eventObject) {
		var selectedBrush = selectedBrushesGroup[scrollBrushWeight.scrollPosition];
		selectedBrush.positionInGroup = scrollBrushWeight.scrollPosition;
		_global.CurrentUnit.Brush = selectedBrush;
		//change default in HList control
		defaultBrushes[ctlBrushType.SelectedItemIndex] = selectedBrush;
		ctlBrushType.RebindItem(ctlBrushType.SelectedItemIndex);
	}
	
	private function btnContinueAndClose_OnClick(eventObject) {
		_global.CurrentUnit.ExitPaint();
		OnEndPainting();
	}
	
	private function btnUndo_OnClick(eventObject) {
		_global.CurrentUnit.Undo();
	}
	
	private function chkKeepProportions_OnClick(eventObject)	{
		_global.CurrentUnit.KeepProportions = chkKeepProportions.selected;
	}
	
	private function btnRestoreProportions_OnClick(eventObject) {
		_global.CurrentUnit.ScaleX = _global.CurrentUnit.InitialScale;
		_global.CurrentUnit.ScaleY = _global.CurrentUnit.InitialScale;
		_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
	}
	
	//~Actions
	
	//region EndPainting event
	private function OnEndPainting() {
		var eventObject = {type:"EndPainting", target:this};
		this.dispatchEvent(eventObject);
		if (_global.CurrentUnit.Empty())
			_global.DeleteCurrentUnit();
	};

	public function RegisterOnEndPaintingHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("EndPainting", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion

	//region Exit event
	private function OnExit() {
		var eventObject = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
}
