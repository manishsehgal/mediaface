﻿class UnitResizer {
	var leftUpper:Object;
	var rightUpper:Object;
	var leftBottom:Object;
	var rightBottom:Object;
	
	function UnitResizer(mc:MovieClip) {
		var bounds = mc.getBounds(mc);
		
		leftUpper = {x:bounds.xMin, y:bounds.yMin};
		rightUpper = {x:bounds.xMax, y:bounds.yMin};
		leftBottom = {x:bounds.xMin, y:bounds.yMax};
		rightBottom = {x:bounds.xMax, y:bounds.yMax};

		mc.localToGlobal(leftUpper);
		mc.localToGlobal(rightUpper);
		mc.localToGlobal(leftBottom);
		mc.localToGlobal(rightBottom);
	}
	
	private function NewSegmentLengthFromStartToProjectionPoint(segmentBegin:Object, segmentEnd:Object, point:Object):Number {
		var length:Number = SegmentLength(segmentBegin, segmentEnd);
		var newLength:Number = ((point.x - segmentBegin.x) * (segmentEnd.x - segmentBegin.x) + (point.y - segmentBegin.y) * (segmentEnd.y - segmentBegin.y)) / length;
		return newLength;
	}

	private function SegmentLength(segmentBegin:Object, segmentEnd:Object):Number {
		var length:Number = Math.sqrt(Math.pow(segmentEnd.x - segmentBegin.x, 2) + Math.pow(segmentEnd.y - segmentBegin.y, 2));
		return length;
	}
	
	public function get CurrentWidth():Number {
		return SegmentLength(leftUpper, rightUpper);
	}

	public function get CurrentHeight():Number {
		return SegmentLength(leftUpper, leftBottom);
	}

	public function GetProportionalFactor(newPositionX:Number, newPositionY:Number):Number {
		var newPosition:Object = {x:newPositionX, y:newPositionY};
		
		var diagonal:Number = SegmentLength(leftUpper, rightBottom);
		var newDiagonal:Number = NewSegmentLengthFromStartToProjectionPoint(leftUpper, rightBottom, newPosition);
		return (diagonal == 0) ? 0 : newDiagonal / diagonal;
	}

	public function GetFreeXFactor(newPositionX:Number, newPositionY:Number):Number {
		var newPosition:Object = {x:newPositionX, y:newPositionY};

		var oldWidth:Number = CurrentWidth;
		var newWidth:Number = NewSegmentLengthFromStartToProjectionPoint(leftUpper, rightUpper, newPosition);
		var kx:Number = (oldWidth == 0) ? 0 : newWidth / oldWidth;
		return kx;
	}

	public function GetFreeYFactor(newPositionX:Number, newPositionY:Number):Number {
		var newPosition:Object = {x:newPositionX, y:newPositionY};

		var oldHeight:Number = CurrentHeight;
		var newHeight:Number = NewSegmentLengthFromStartToProjectionPoint(leftUpper, leftBottom, newPosition);
		var ky:Number = (oldHeight == 0) ? 0 : newHeight / oldHeight;
		return ky;
	}
}