﻿class RotateHandler extends MovieClip {

	private var centerX:Number = 0;
	private var centerY:Number = 0;
	private var angleD:Number = 0;
	
	
	function RotateHandler() {
		this.onPress = drag;
		this.onMouseMove = null;
		this.onMouseUp = mouseUp;
		this.onRollOver = rollOver;
		this.onRollOut = rollOut;
		GotoAndStop(1);
	}
	
	function drag() {
		var w = _parent.frameBorder.getBounds(_parent).xMin + _parent.frameBorder._width/2-1;
		var h = _parent.frameBorder.getBounds(_parent).yMin + _parent.frameBorder._height/2-1;
		var r = Math.sqrt(w*w + h*h);
		var angle = Math.atan(h/Math.abs(w));
		if (w<0) angle = Math.PI - angle;
		angle += _parent._rotation * Math.PI/180;
		centerX = _parent._x + r * Math.cos(angle);
		centerY = _parent._y + r * Math.sin(angle);
		//trace("Rotate: relCenterX " + r * Math.cos(angle) + " relCenterY " + r * Math.sin(angle));
		
		w = _parent.frameBorder._width/2-1  + _width/2;
		h = _parent.frameBorder._height/2-1 + _height/2;
		angleD = Math.atan(h/Math.abs(w));
		if (w<0) angleD = Math.PI - angleD;
		
		trace("rotate drag");
		this.onMouseMove = mouseMove;
		GotoAndStop(3);
	}
	
	/*
	function tracePointMC(mc:MovieClip, x:Number, y:Number, color) {
		mc.lineStyle(5, color);
		mc.moveTo(x-10, y-10);
		mc.lineTo(x+10, y+10);
		mc.moveTo(x-10, y+10);
		mc.lineTo(x+10, y-10);
	}
	*/
	
	function mouseMove() {
		var x = _parent._parent._xmouse;
		var y = _parent._parent._ymouse;
		
		if (Math.abs(centerX - x) < 2 && Math.abs(centerY - y) < 2) {
			return;
		}
			
		var a1 = Math.atan((centerX - x)/(centerY - y));
		if (y > centerY) {
			a1 += Math.PI;
		}
		var a2 = (Math.PI/2 - a1 - angleD) * 180 / Math.PI;;
		
		_parent.Angle = a2;
		
		updateAfterEvent();
	}
	
	function noDrag() {
		trace("rotate noDrag");
		this.onMouseMove = null;
		GotoAndStop(1);
	}
	
	function rollOver() {
		GotoAndStop(2);
	}
	
	function rollOut() {
		GotoAndStop(1);
	}
		
	function mouseUp() {
		if (onMouseMove != null)
			noDrag();
	}
}