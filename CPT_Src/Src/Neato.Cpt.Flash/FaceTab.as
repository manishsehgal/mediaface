﻿
import mx.controls.Label;
import mx.core.UIObject;

class FaceTab extends UIObject {
	private var lblFront:Label;
	private var lblBack:Label;

	private var btnFaceTab0:MovieClip;
	private var btnFaceTab1:MovieClip;
	private var btnCopyToBack:MovieClip;
	private var btnCopyToFront:MovieClip;
	private var mcContour0:MovieClip;
	private var mcContour1:MovieClip;
	
	function FaceTab() {
	}
	
	public function NormalizeState() {
		var isFirstFaceActive:Boolean = (_global.Faces[0].Id == _global.CurrentFace.Id);
		
		if (isFirstFaceActive) {
			btnFaceTab0.gotoAndStop(2);
			btnFaceTab1.gotoAndStop(1);
			lblFront.setStyle("color", "0x000000");
			lblBack.setStyle("color", "0xFF9900");
		} else {
			btnFaceTab0.gotoAndStop(1);
			btnFaceTab1.gotoAndStop(2);
			lblFront.setStyle("color", "0xFF9900");
			lblBack.setStyle("color", "0x000000");
		}
			
		btnCopyToBack.visible = isFirstFaceActive;
		btnCopyToFront.visible = ! isFirstFaceActive;
		
		btnFaceTab0.enabled = ! isFirstFaceActive;
		btnFaceTab1.enabled = isFirstFaceActive;
		
		if(_global.Faces.length == 1) {
			lblBack._visible = false;
			btnFaceTab1._visible = false;
			btnCopyToFront._visible = false;
			btnCopyToBack._visible = false;
		}
		else if(_global.Faces.length == 2) {
			lblBack._visible = true;
			btnFaceTab1._visible = true;
		}
	}
	
	function FaceTab_OnSelect(index) {
		var destinationFace = _global.Faces[index];				
		Logic.DesignerLogic.UnselectCurrentUnit();
		_global.CurrentFace.Hide();
		_global.CurrentFace = destinationFace;
		destinationFace.Show();
		NormalizeState();
	}

	function onLoad() {
		//_root.pnlMainLayer.content.pnlFaceTab.setStyle("styleName", "FaceTabPanel");
		this.lblFront.setStyle("styleName", "FaceTabPanelFaceFront");
		this.lblBack.setStyle("styleName", "FaceTabPanelFaceBack");
		_global.LocalHelper.LocalizeInstance(lblFront, "FaceTab", "IDS_LBLFRONT");
		_global.LocalHelper.LocalizeInstance(lblBack, "FaceTab", "IDS_LBLBACK");
		
		btnFaceTab0.onRelease =  function() {
			this._parent.FaceTab_OnSelect(0);
		}
		
		btnFaceTab1.onRelease =  function() {
			this._parent.FaceTab_OnSelect(1);
		}
		
		btnCopyToBack.onRelease =  function() {
			ToolPropertiesContainer.GetInstance().ShowTransferProperties(1);
		}
		
		btnCopyToFront.onRelease =  function() {
			ToolPropertiesContainer.GetInstance().ShowTransferProperties(0);
		}
	}
	
	function FillContourClip(index, mc:MovieClip) {
		mc.clear();
		mc._xscale = 100;
		mc._yscale = 100;
		var face = _global.Faces[index];				
		mc.lineStyle(0, 0x00, 100);
		face.DrawContour(mc, true, true);
		var w:Number = mc._width;
		var h:Number = mc._height;
		var scaleFactor = (100 * 25) / (w > h ? w : h);
		mc._xscale = mc._yscale = scaleFactor;
		if (index == 0) {
			mc._x = lblFront._x;
		}
		if (index == 1) {
			mc._x = lblBack._x + lblBack._width - 14;
		}
		var isRotated = face.RotationDeg == 90 || face.RotationDeg == -90;
		if (isRotated == true) {
			mc._x += mc._width;
		}
	}
	
	public function DataBind():Void {
		FillContourClip(0, mcContour0);
		FillContourClip(1, mcContour1);
	}
}
