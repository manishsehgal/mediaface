﻿import mx.core.UIObject;
import mx.controls.*;
import mx.utils.Delegate;
[Event("exit")]
class FillProperties extends UIObject {
	private var lblFillColor:Label;
	private var colorTool:Tools.ColorSelectionTool.ColorTool;
	private var btnContinue:MenuButton;
	
	function FillProperties() {
		this.lblFillColor.setStyle("styleName", "ToolPropertiesCaption");
		this.btnContinue.setStyle("styleName", "ToolPropertiesActiveButton");
	}
	
	function onLoad() {
		setStyle("styleName", "ToolPropertiesPanel");
		colorTool.RegisterOnSelectHandler(this, colorTool_OnSelect);
		colorTool.RegisterOnClickHandler(this, colorTool_OnClick);
		btnContinue.RegisterOnClickHandler(this,btnContinue_OnClick);
				
		//_global.LocalHelper.LocalizeInstance(btnContinue, "ToolProperties", "IDS_BTNCONTINUE");
		_global.LocalHelper.LocalizeInstance(lblFillColor, "ToolProperties", "IDS_LBLFILLCOLOR");

		TooltipHelper.SetTooltip(btnContinue, "ToolProperties", "IDS_TOOLTIP_CONTINUE");
	}
	
	function ShowFillProperties():Void {
		_global.Mode = _global.FillMode;
		Logic.DesignerLogic.UnselectCurrentUnit();
		this._visible = true;
		/*_parent.TextPropertiesClip._visible = false;
		_parent.ImageEditPropertiesClip._visible = false;
		_parent.TransferPropertiesClip._visible = false;		
		_parent.ClearFacePropertiesClip._visible = false;
		_parent.ChooseLayoutPropertiesClip._visible = false;*/
		
		var selectedColor = _global.CurrentFace.bgColor;
		if (selectedColor == undefined) selectedColor = 0xFFFFFF;
		colorTool.SetColorState(selectedColor);
	}
	
	private function colorTool_OnClick(eventObject) {
		trace("colorTool_OnClick");
		SATracking.ColorFill();
	}
	private function colorTool_OnSelect(eventObject) {
		_global.CurrentFace.Fill(eventObject.color);
	}
	private function btnContinue_OnClick() {
		OnExit();
	}
	
	
	//region Exit event
	private function OnExit() {
		var eventObject = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
}