﻿import mx.core.UIObject;

class MiniBar extends UIObject {
	private var buttonDrawHelper:ButtonDrawHelper;
	private var buttons:Array;
	private static var instance;
	
	function MiniBar() {
		_root.pnlMainLayer.content.pnlMinibar.setStyle("styleName", "MiniBarPanel");
		instance = this;
		buttons = new Array();
		buttonDrawHelper = new ButtonDrawHelper();
	}
	
	static function getInstance():MiniBar {
		return instance;
	}
	
	function NormalizeState(){
		trace("btndrawhelper"+buttonDrawHelper._x);
		trace("btndrawhelper"+buttonDrawHelper._y);
		trace("btndrawhelper"+buttonDrawHelper._width);
		trace("btndrawhelper"+buttonDrawHelper._height);
		
		var buttSpace:Number = 15; 
		var buttonFaceWidth = 40;
		var panelWidth:Number = _root.pnlMainLayer.content.pnlMinibar.width;
		var widthsumm:Number = (panelWidth - (buttonFaceWidth + buttSpace) * (buttons.length+0.5)) / 2;
		for(var i = 0; i < buttons.length; i++)
		{
			if (_global.Faces[i].Id == _global.CurrentFace.Id) {
				buttons[i].gotoAndStop(2);
			} else buttons[i].gotoAndStop(1);
			 var k:Number =  buttons[i]._width/ buttons[i]._height;
			 buttons[i]._height = 90; //magic:) for situate in minibar
			 buttons[i]._width = 90 * k; // ---------//---------
			 buttons[i]._x = widthsumm;
			 widthsumm+=buttons[i]._width + buttSpace;
		}
	}
	
	function CreateButtons() {
		for (var i = 0; i < buttons.length; i++) {
			var fm = buttons[i];
			if (fm != undefined) {
				fm.removeMovieClip();
			}
		}
		buttons = new Array();
		
		var panelWidth = _root.pnlMainLayer.content.pnlMinibar.width/2;
		var buttonFaceWidth = 64;
		var gap = ( panelWidth - _global.Faces.length * buttonFaceWidth )
		/ (_global.Faces.length + 1);

		buttonDrawHelper.Initialize(this, gap, 8, gap);
		
		for (var i = 0; i < _global.Faces.length; i++) { 
			var face:FaceUnit = _global.Faces[i];
			var btn:MovieClip = buttonDrawHelper.CreateOneButton(face);
			btn.index = i;
			
			btn.onRelease = function () {
				var destinationFace = _global.Faces[this.index];
				
				Logic.DesignerLogic.UnselectCurrentUnit();
				_global.CurrentFace.Hide();
				_global.CurrentFace = destinationFace;
				destinationFace.Show();
				
				ToolPropertiesContainer.GetInstance().ShowFillProperties();
				this._parent.NormalizeState();
			}
			
			buttons.push(btn);
		}
		NormalizeState();
	}
	
	function onLoad() {
		_global.projectXml.onLoad += CreateButtons();
	}
}
