﻿import mx.core.UIComponent;
//import mx.controls.Loader;
import mx.controls.Label;

class FontCellRenderer extends UIComponent
{
	static public var symbolName:String = "FontCellRenderer";
	static public var symbolOwner:Object = FontCellRenderer;
	public var className:String = "FontCellRenderer";
	

    private var multiLineLabel; // The label to be used for text.
    private var owner; // The row that contains this cell.
    private var listOwner; // The List, data grid or tree containing this cell.

    // Cell height offset from the row height total and preferred cell width.
    private static var PREFERRED_HEIGHT_OFFSET = 0; 
    private static var PREFERRED_WIDTH = 140;

//	private var image_ldr:Loader;
	private var label_lbl;//:Label;
	
	
	public var getDataLabel:Function;
	

    public function FontCellRenderer()
    { }

    public function createChildren():Void
    {
		label_lbl = createObject("Label", "label_lbl", 1, {styleName:this, owner:this});
		label_lbl.text = " ";
    }

    public function doLayout():Void
	{
		label_lbl.move(0, 0);
		var w:Number = width - label_lbl.x;
		label_lbl.setSize(w, 22);
	}

	public function getPreferredHeight():Number
    {
		return owner.__height - PREFERRED_HEIGHT_OFFSET;
    }

    public function setValue(suggestedValue:String, item:Object, selected:Boolean):Void
    {
		
		if(item == undefined)
		{
            _visible = false;
			return;
        }
        _visible = true;
		var lbl:String = getDataLabel();

		var textFormat:TextFormat=label_lbl.labelField.getTextFormat();
		//textFormat.color=Math.round(Math.random()*256*256*256-1);
		//var a:TextField;
		
		if(_global.Fonts.TestLoadedSmallFont()){
			label_lbl.labelField.embedFonts=true;
			textFormat.font=item[lbl];
			textFormat.size=16;
		}
		textFormat.color = (selected == "selected" || selected == "highlighted") ? 0 : 0xFFFFFF;
		label_lbl.labelField.setTextFormat(textFormat);
		label_lbl.labelField.setNewTextFormat(textFormat);
		label_lbl.text = item[lbl];
		doLayout();
		_visible = true;
	
    }
    // function getPreferredWidth :: only for menus and DataGrid headers
    // function getCellIndex :: not used in this cell renderer

}

