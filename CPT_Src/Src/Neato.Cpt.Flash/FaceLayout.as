﻿class FaceLayout {
	public var Id:Number;
	public var FaceId:Number;
	public var Items:Array;
	public var drawSequence:Array;
	public var textSequence:Array;
	
	function FaceLayout() {
		Items = new Array();
		drawSequence = new Array();
		textSequence = new Array();
	}
}