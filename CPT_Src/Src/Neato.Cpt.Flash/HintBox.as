﻿import mx.controls.Label;
import mx.core.UIObject;

class HintBox extends UIObject {
	private var lblModelName:Label;
	private static var instances:Array;
	
	function HintBox() {
		if(instances == undefined)
			instances = new Array();
		instances.push( this);
		trace("HintBox:Constructor; instances.lenght = "+instances.length)
	}
	
	static function getInstance(j:Number):HintBox {
		instances[0].Update();
		
		return instances[j];
	}
	static function updateAll(){
		trace("HintBox:updateAll; instances.lenght = "+instances.length)
		for(var obj in instances)
		{
			obj.Update();
			trace("Object = "+obj);
		}
	}
	
	function onLoad() {
		_root.pnlMainLayer.content.pnlHintBox.setStyle("styleName", "HintBoxPanel");
		_root.pnlPreviewLayer.content.pnlHintBoxPreview.setStyle("styleName","HintBoxPanelPreview");
		lblModelName.text = _global.ModelName;
	}
	
	function Update() {
		lblModelName.text = _global.ModelName;
	}
}