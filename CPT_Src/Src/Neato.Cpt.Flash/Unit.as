﻿class Unit {
	public var id:String;
	private var mc:MovieClip;
	
	function Unit(mc:MovieClip, node:XML) {
		this.mc = mc;
		if (node != null) {
			id = String(node.attributes.id);
			this.mc._x = Number(node.attributes.x);
			this.mc._y = Number(node.attributes.y);
		} else {
			id = mc._name;
			this.mc._x = 0;
			this.mc._y = 0;
		}
	}

	public function set X(value:Number):Void {
		mc._x = value;
	}
	
	public function get X():Number {
		return mc._x;
	}
	
	public function set Y(value:Number):Void {
		mc._y = value;
	}
	
	public function get Y():Number {
		return mc._y;
	}
	
	function GetXmlNodeBase():XMLNode {
		var node:XMLNode = new XMLNode(1, "BaseNode");
		node.attributes.id = id;
		node.attributes.x = Math.round(this.mc._x*1000)/1000;
		node.attributes.y = Math.round(this.mc._y*1000)/1000;
		return node;
	}
}