﻿class SATracking {
	private static var url:String = "../DesignerTracking.aspx?type=";
	private static var ColorFillText:String = "ColorFill";
    private static var AddImageText:String = "AddImage";
	private static var AddCustomImageText:String = "AddCustomImage";
    private static var AddTextText:String = "AddText";
    private static var PaintText:String = "Paint";
    private static var AddShapeText:String = "AddShape";
    private static var ClearText:String = "Clear";
    private static var UsePreviousDesignText:String = "UsePreviousDesign";
    private static var SaveDesignText:String = "SaveDesign";
    private static var QuickToolsText:String = "QuickTools";
    private static var PreviewText:String = "Preview";
    private static var PrintPDFText:String = "PrintPDF";
    private static var CopyDesignText:String = "CopyDesign";
	
	public static function TrackAction(action:String) {
		var xml:XML = new XML();
		var currentDate:Date = new Date();
		xml.load(url+action+"&time="+currentDate.getTime());
		xml.onLoad = function(success) {
			trace("action "+action+" tracked ok");
		}
	}
	public static function ColorFill(){
		TrackAction(ColorFillText);
	}
	public static function AddImage(){
		TrackAction(AddImageText);
	}
	public static function AddCustomImage(){
		TrackAction(AddCustomImageText);
	}
	public static function AddText(){
		TrackAction(AddTextText);
	}
	public static function Paint(){
		TrackAction(PaintText);
	}
	public static function AddShape(){
		TrackAction(AddShapeText);
	}
	public static function Clear(){
		TrackAction(ClearText);
	}
	public static function UsePreviousDesign(){
		TrackAction(UsePreviousDesignText);
	}
	public static function SaveDesign(){
		TrackAction(SaveDesignText);
	}
	public static function QuickTools(){
		TrackAction(QuickToolsText);
	}
	public static function Preview(){
		TrackAction(PreviewText);
	}
	public static function PrintPDF(){
		TrackAction(PrintPDFText);
	}
	public static function CopyDesign(){
		TrackAction(CopyDesignText);
	}
}