﻿class ImageUnit extends MoveableUnit {
	private var url:String;
	private var mclListener:Object;
	private var image_mcl:MovieClipLoader;
	private var isNewImage:Boolean = false;
	
	public function get IsNewImage():Boolean {
		return isNewImage;
	}
	
	function ImageUnit(mc:MovieClip, node:XML, urlFormat:String) {
		super(mc, node);
		keepProportions = false;
		url = urlFormat+node.attributes.id;
		trace("ImageUnit url="+ url);
		isNewImage = node.attributes.isNewImage != undefined && node.attributes.isNewImage != null;

		if (node.attributes.isCurrentUnit == "True") {
			_global.CurrentFace.ChangeCurrentUnit(this);
		}
		
		mclListener = new Object();
		image_mcl = new MovieClipLoader();
		var ref = this;
		Preloader.Show();
		mclListener.onLoadInit = function(target_mc:MovieClip) {
			Preloader.Hide();
			if (_global.CurrentUnit == ref) {
				_global.CurrentFace.ChangeCurrentUnit(ref);
			}
		};
		image_mcl = new MovieClipLoader();
		image_mcl.addListener(mclListener);
	}
	function Draw():Void {
		DrawMC(mc);
	}
	function DrawMC(mc:MovieClip):Void {
		mc.clear();
		trace("DrawMC image");
		var mcImage:MovieClip = mc.createEmptyMovieClip("mcImage", 0);

		image_mcl.loadClip(url, mcImage);

		mc._xscale = this.scaleXValue;
		mc._yscale = this.scaleYValue;
	}

	function GetXmlNode():XMLNode {
		var node:XMLNode = super.GetXmlNode();
		node.nodeName = "Image";
		return node;
	}

	function getMode():String {
		return _global.ImageEditMode;
	}
	

	private function Resize(kx:Number, ky:Number) {
		this.ScaleX = this.ScaleX * kx;
		this.ScaleY = this.ScaleY * ky;
	}
}
