﻿class ResizeHandler extends mx.core.UIObject {
	private var mousePositionDelta:Object;
	
	function ResizeHandler() {
		this.onPress = drag;
		this.onMouseMove = null;
		this.onMouseUp = mouseUp;
		this.onRollOver = rollOver;
		this.onRollOut = rollOut;
		gotoAndStop(1);
	}
	
	function drag() {
		trace("resize drag");
		this.onMouseMove = mouseMove;
		
		var startPosition:Object = {x:this._x, y:this._y};
		this._parent.localToGlobal(startPosition);
		this.globalToLocal(startPosition);

		mousePositionDelta = {x:this._xmouse - startPosition.x, y:this._ymouse - startPosition.y};
		
		gotoAndStop(3);
	}
	
	function noDrag() {
		trace("resize noDrag");
		this.onMouseMove = null;
		gotoAndStop(1);
	}
	
	function mouseMove() {
		var newPosition:Object = {x:_xmouse - mousePositionDelta.x, y:_ymouse - mousePositionDelta.y};
		this.localToGlobal(newPosition);
		_global.CurrentUnit.ResizeToPosition(newPosition.x, newPosition.y);
		_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
	}
	
	function rollOver() {
		gotoAndStop(2);
	}
	
	function rollOut() {
		gotoAndStop(1);
	}
		
	function mouseUp() {
		if (onMouseMove != null)
			noDrag();
	}
}