﻿import mx.controls.TextInput;
import mx.utils.Delegate;

[Event("SelectionChange")]
[Event("TextChanged")]
class SelectionTextInput extends TextInput {
	static var symbolName:String = "SelectionTextInput";
    static var symbolOwner = SelectionTextInput;
	private var keyListener:Object;
	private var focusListener:Object;
	private var mouseListener:Object;;
	
	function SelectionTextInput() {
	}
	
	function onLoad() {
		keyListener = new Object();
		keyListener.parent = this;
		keyListener.keyUp = function(eventObject) {
			if (_global.Mode == _global.TextMode) {
				switch(eventObject.code) {
					case Key.LEFT:
					case Key.RIGHT:
					case Key.HOME:
					case Key.END:
					case Key.PGDN:
					case Key.PGUP:
					case Key.UP:
					case Key.DOWN:
						this.parent.OnSelectionChange();
						break;
					case Key.ENTER:
						break;
				}
			}
		}
		addEventListener("keyUp", keyListener);
		
		focusListener = new Object();
		focusListener.parent = this;
		focusListener.focusIn = function(eventObject) {}
		addEventListener("focusIn", focusListener);
		
		mouseListener = new Object();
		mouseListener.parent = this;
		mouseListener.onMouseDown = function() {
			if (this.parent.hitTest(_root._xmouse, _root._ymouse, false)
				&& this.parent.getFocus() == this.parent.label
				&& _global.Mode == _global.TextMode)
			{
				this.flag = true;
				this.parent.OnSelectionChange();
			}
		}
		mouseListener.onMouseMove = function() {
			if (this.flag) {
				if (Selection.getBeginIndex() != this.parent.beginIndex || Selection.getEndIndex() != this.parent.endIndex) {
					this.parent.OnSelectionChange();
				}
			}
		}
		mouseListener.onMouseUp = function() {
			if (this.flag) {
				this.flag = false;
				this.parent.OnSelectionChange();
			}
		}
		Mouse.addListener(mouseListener);
		
		this.addEventListener("change", Delegate.create(this, SelectionTextInput_OnChange));
	}
	
	function get text():String {
		return super.text;
	}

	function set text(value:String):Void {
		oldLength = value.length;
		beginIndex = 0;
		endIndex = 0;
		caretIndex = 0;

		super.text = value;
	}

	private var beginIndex:Number = 0;
	private var endIndex:Number = 0;
	private var caretIndex:Number = 0;
	private var oldLength:Number = 0;
	
	private function SelectionTextInput_OnChange(eventObject:Object) {
		OnTextChanged();
		OnSelectionChange();
	}
	
	public function RegisterOnSelectionChangeHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("SelectionChange", Delegate.create(scopeObject, callBackFunction));
    }
	
	private function OnSelectionChange():Void {
		caretIndex = Selection.getCaretIndex();
		beginIndex = Selection.getBeginIndex();
		endIndex = Selection.getEndIndex();

		SelectionTrick();

		var eventObject = {type:"SelectionChange", target:this, beginIndex:beginIndex , endIndex:endIndex, caretIndex:caretIndex};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnTextChangedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("TextChanged", Delegate.create(scopeObject, callBackFunction));
    }

	private function SelectionTrick() {
		var currentSelection:Object = Selection;
		currentSelection.lastBeginIndex = beginIndex;
		currentSelection.lastEndIndex = endIndex;
	}
	
	private function OnTextChanged():Void {
		caretIndex = Selection.getCaretIndex();
		var begin:Number = 0;
		var end:Number = 0;
		var insertedText:String;

		if(beginIndex==endIndex) {
			if (this.length < oldLength) {
				if (beginIndex > caretIndex) {
					insertedText = "";
					begin = beginIndex - (oldLength - this.length);
					end = beginIndex;
				} else {
					insertedText = "";
					begin = beginIndex;
					end = beginIndex + (oldLength - this.length);
				}
			} else {
				insertedText = this.text.substring(beginIndex, beginIndex + this.length - oldLength);
				begin = beginIndex;
				end = endIndex;
			}
		} else {
			if (beginIndex==caretIndex) {
				insertedText = "";
			} else {
				insertedText = this.text.substring(beginIndex, caretIndex);
			}
			begin = beginIndex;
			end = endIndex;
		}

		beginIndex = Selection.getBeginIndex();
		endIndex = Selection.getEndIndex();
		oldLength = this.length;
		
		SelectionTrick();

		var eventObject = {type:"TextChanged", target:this, oldBegin:begin , oldEnd:end, insertedText:insertedText, caretIndex:caretIndex};
		this.dispatchEvent(eventObject);

	}

	public function SelectAllText() {
		SetSelection(0, this.length);
		OnSelectionChange();
	}
	
	public function RestoreSelection() {
		SetSelection(beginIndex, endIndex);
	}
	
	private function SetSelection(begin:Number, end:Number) {
		Selection.setFocus(this.label);
		SelectionTrick();
		Selection.setSelection(begin, end);
	}
	
	public function get SelectionBeginIndex() {
		return this.beginIndex;
	}

	public function get SelectionEndIndex() {
		return this.endIndex;
	}
}