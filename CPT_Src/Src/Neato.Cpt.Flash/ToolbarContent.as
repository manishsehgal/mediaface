﻿import mx.controls.Label;
import mx.core.UIObject;

class ToolbarContent extends UIObject {
	private var btnFill:ToolbarButton;
	private var btnText:ToolbarButton;
	private var btnImage:ToolbarButton;
	private var btnPaint:ToolbarButton;
	private var btnChooseLayout:ToolbarButton;
	private var btnAddShape:ToolbarButton;
	
	private var btnPlaylist:ToolbarButton;
	private var btnGradient:ToolbarButton;

	private var lblDesignTools:Label;// = _parent._parent.lblDesignTools;

	private var speed:Number = 50;
	private var intUp:Number;
	private var intDown:Number;
	private var intLeft:Number;
	private var intRight:Number;
	private var intRotateLeft:Number;
	private var intRotateRight:Number;
	private var intResizeMore:Number;
	private var intResizeLess:Number;
    
	private static var instance;
	public static function GetInstance():ToolbarContent {
		return instance;
	}

	private function ToolbarContent() {
		trace("ToolbarContent constructor");

		Preloader.Show();
		lblDesignTools =   _parent.lblDesignTools;
		_parent._parent.setStyle("styleName", "ToolbarPanel");
		lblDesignTools.setStyle("styleName","TitleText");
		instance = this;
	}

	private function onLoad() {
		InitLocale();

		btnFill.RegisterOnClickHandler(this, btnFill_OnClick);
		btnText.RegisterOnClickHandler(this, btnText_OnClick);
		btnImage.RegisterOnClickHandler(this, btnImage_OnClick);
		btnPaint.RegisterOnClickHandler(this, btnPaint_OnClick);
		btnChooseLayout.RegisterOnClickHandler(this, btnChooseLayout_OnClick);
		btnPlaylist.RegisterOnClickHandler(this, btnPlaylist_OnClick);
		btnAddShape.RegisterOnClickHandler(this, btnAddShape_OnClick);
		btnGradient.RegisterOnClickHandler(this, btnGradient_OnClick);
		
		btnChooseLayout.visible = false;
		btnPlaylist.visible = false;
		btnGradient.visible = false;
		var appName = _global.projectXml.firstChild.attributes.appName;
		if (appName == "DOL") {
			btnChooseLayout.visible = true;
			btnPlaylist.visible = true;
			btnGradient.visible = true;
		}
		
		Preloader.Hide();
		Synchronize();
	}

	private function InitLocale() {
		//_global.LocalHelper.LocalizeInstance(this.btnFill, "Toolbar", "IDS_LBLFILL");
		//_global.LocalHelper.LocalizeInstance(this.btnText, "Toolbar", "IDS_LBLTEXT");
		//_global.LocalHelper.LocalizeInstance(this.btnAddShape, "Toolbar", "IDS_LBLADDSHAPE");
		//_global.LocalHelper.LocalizeInstance(this.btnPaint, "Toolbar", "IDS_LBLPAINT");
		//_global.LocalHelper.LocalizeInstance(this.btnImage, "Toolbar", "IDS_LBLIMAGE");		
		_global.LocalHelper.LocalizeInstance(this.btnPlaylist,"Toolbar","IDS_LBLADDPLAYLIST");		
		_global.LocalHelper.LocalizeInstance(this.btnChooseLayout, "Toolbar", "IDS_LBLCHOOSELAYOUT");
		
		//_global.LocalHelper.LocalizeInstance(this.lblDesignTools,"Toolbar","IDS_LBLDESIGNTOOLS");
		
		TooltipHelper.SetTooltip(this.btnFill, "Toolbar", "IDS_TOOLTIP_FILL");
		TooltipHelper.SetTooltip(this.btnText, "Toolbar", "IDS_TOOLTIP_TEXT");
		TooltipHelper.SetTooltip(this.btnAddShape, "Toolbar", "IDS_TOOLTIP_ADDSHAPE");
		TooltipHelper.SetTooltip(this.btnPaint, "Toolbar", "IDS_TOOLTIP_PAINT");
		TooltipHelper.SetTooltip(this.btnImage, "Toolbar", "IDS_TOOLTIP_IMAGE");
		TooltipHelper.SetTooltip(this.btnPlaylist,"Toolbar","IDS_TOOLTIP_ADDPLAYLIST");
		TooltipHelper.SetTooltip(this.btnChooseLayout, "Toolbar", "IDS_TOOLTIP_CHOOSELAYOUT");
	}

	public function Synchronize():Void {
		switch (_global.Mode) {
			case _global.TextMode:
			case _global.TextEffectMode:
				changeButtonsState(btnText);
				break;
			case _global.FillMode:
				changeButtonsState(btnFill);
				break;
			case _global.ImageMode:
			case _global.ImageEditMode:
				changeButtonsState(btnImage);
				break;
			case _global.ShapeMode:
			case _global.ShapeEditMode:
				changeButtonsState(btnAddShape);
				break;
			case _global.PaintMode:
			case _global.PaintEditMode:
				changeButtonsState(btnPaint);
				break;
			case _global.ChooseLayoutMode:
			case _global.InactiveMode:
			case _global.PlaylistMode:
			case _global.PlaylistEditMode:
			case _global.GradientMode:
			case _global.GradientEditMode:
			case _global.TransferMode:
			case _global.ClearFaceMode:
			default:
				changeButtonsState(null);
				break;
		}
	}
	
	private function changeButtonsState(selectedButton:Object) {
		btnAddShape.value = (selectedButton == btnAddShape);
		btnPaint.value = (selectedButton == btnPaint);
		btnImage.value = (selectedButton == btnImage);
		btnText.value = (selectedButton == btnText);
		btnFill.value = (selectedButton == btnFill);
	}
	
	private function btnFill_OnClick(eventObject) {
		changeButtonsState(eventObject.target);
		Logic.DesignerLogic.FillFace();
	}
	
	private function btnText_OnClick(eventObject) {
		changeButtonsState(eventObject.target);
		Logic.DesignerLogic.AddTextUnit();
	}

	private function btnImage_OnClick(eventObject) {
		changeButtonsState(eventObject.target);
		Logic.DesignerLogic.AddImageUnit();
	}

	private function btnPaint_OnClick(eventObject) {
		changeButtonsState(eventObject.target);
		Logic.DesignerLogic.AddPaintUnit();
	}

	private function btnAddShape_OnClick(eventObject) {
		changeButtonsState(eventObject.target);
		Logic.DesignerLogic.AddShapeUnit();
	}

	private function btnPlaylist_OnClick(eventObject) {
		changeButtonsState(eventObject.target);
		Logic.DesignerLogic.AddPlaylistUnit();
	}
	
	private function btnChooseLayout_OnClick(eventObject) {
		changeButtonsState(eventObject.target);
		Logic.DesignerLogic.ChooseLayout();
	}	
	
	private function btnGradient_OnClick(eventObject) {
		changeButtonsState(eventObject.target);
		Logic.DesignerLogic.AddGradientUnit();
	}	
}
