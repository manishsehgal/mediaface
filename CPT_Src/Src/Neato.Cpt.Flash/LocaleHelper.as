﻿class LocaleHelper {
	private var defaultLang:String;
	private var longLang:String;
	private var shortLang:String;
	private var stringMap:Object = new Object();
	
	private var parseLang:String;
	private var parseFla:String;
	
	function LocaleHelper(language:String, defaultLanguage:String) {
		longLang = language;
		shortLang = longLang.substr(0,2);
		defaultLang = defaultLanguage;
	}
	
	public function AddStringsXML(language:String, flaName:String, doc:XML):Void {
		parseLang = language;
		parseFla = flaName;
		parseStringsXML(doc);
	}

	private function parseStringsXML(doc:XML):Void {
		if(doc.childNodes.length > 0 && doc.childNodes[0].nodeName == "xliff") {
			parseXLiff(doc.childNodes[0]);
		}
	}

	private function parseXLiff(node:XMLNode):Void {
		if(node.childNodes.length > 0 && node.childNodes[0].nodeName == "file") {
			parseFile(node.childNodes[0]);
		}
	}

	private function parseFile(node:XMLNode):Void {
		if(node.childNodes.length > 1 && node.childNodes[1].nodeName == "body") {
			parseBody(node.childNodes[1]);
		}
	}

	private function parseBody(node:XMLNode):Void {
		for(var i:Number = 0; i < node.childNodes.length; i++) {
			if(node.childNodes[i].nodeName == "trans-unit") {
				parseTransUnit(node.childNodes[i]);
			}
		}
	}

	private function parseTransUnit(node:XMLNode):Void {
		var id:String = node.attributes.resname;
		if(id.length > 0 && node.childNodes.length > 0 &&
				node.childNodes[0].nodeName == "source") {
			var value:String = parseSource(node.childNodes[0]);
			if(value.length > 0) {
				if (stringMap[parseLang] == undefined)
					stringMap[parseLang] = new Object();
				if (stringMap[parseLang][parseFla] == undefined)
					stringMap[parseLang][parseFla] = new Object();
				if (stringMap[parseLang][parseFla][id] == undefined)
					stringMap[parseLang][parseFla][id] = new Object();
					
				stringMap[parseLang][parseFla][id] = value;
			}
		}
	}
	
	private function parseSource(node:XMLNode):String {
		if(node.childNodes.length > 0) {
			return node.childNodes[0].nodeValue;
		}

		return "";
	}
	
	public function GetString(flaName:String, stringID:String):String {
		var localizedString:String;
		localizedString = stringMap[longLang][flaName][stringID];
		
		if (localizedString == undefined)
			localizedString = stringMap[shortLang][flaName][stringID];
		
		if (localizedString == undefined)
			localizedString = stringMap[defaultLang][flaName][stringID];
			
		return localizedString;		
	}
	
	public function LocalizeInstance(instance, flaName:String, stringID:String) {
		var localizedString:String = GetString(flaName, stringID);
		
		if (localizedString == undefined)
			return;
			
		if (instance.text != undefined) {
			instance.text = localizedString;
			return;
		}
			
		if (instance.label != undefined)
			instance.label = localizedString;
	}
}