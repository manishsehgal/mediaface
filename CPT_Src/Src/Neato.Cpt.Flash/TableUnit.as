﻿class TableUnit extends MoveableUnit {
	var columns:Array;
	var columnsOrder:Array;
	static var columnsNum:Number = 4;
	private var layoutPosition:Number; 
	private var emptyLayoutItem:Boolean;
	var italic:Boolean = false;
	var bold:Boolean = false;
	var underline:Boolean = false;
	var fontSize:Number = 12;
	var font:String = "Arial";
	var strokeInterval:Number = 0;
	public var frameLinkageName = "TableFrame";
	public static var UnitClassName = "TableUnit";
	function TableUnit(mc:MovieClip, node:XML, bPlaylist:Boolean,bNew:Boolean) {
		super(mc, node);
		keepProportions = false;
		columns=new Array(columnsNum);
		columnsOrder=new Array(columnsNum);
		var currWidth:Number = 0;
		trace("TableUnit constructor");
		this.Visible = true;
		if(bPlaylist == undefined)
			bPlaylist = false;
		//var bNew:Boolean = false;
		trace(node);
		if(node == null)
				bNew = true;
		if (!bNew && !bPlaylist) {
			
			this.italic = node.attributes.italic.toString() == "true" ? true : false;
			this.bold = node.attributes.bold.toString() == "true" ? true : false;
			this.underline = node.attributes.underline.toString() == "true" ? true : false;
			this.fontSize = parseInt(node.attributes.fontSize);
			this.font = node.attributes.font;
			
			this.strokeInterval = parseInt(node.attributes.strokeInterval);
			for (var j = 0; j < node.childNodes[j].childNodes.length;j++) {
				if(node.childNodes[j].nodeName == "Columns")
				for (var i = 0; i < columnsNum/*node.childNodes.length*/; ++i) {
					columns[i]=new Object({
						text:String, 
						color:Number,
						columnOrder:Number,
						id:String,
						align:String,
						width:Number,
						height:Number,
						textField:TextField,
						visible:Boolean,
						fontFormat:TextFormat});
						//trace(childNode);
					var childNode:XML = node.childNodes[j].childNodes[i]; //Column
					var col = columns[i];
					col.text = childNode.attributes.text;
					col.color = parseInt(childNode.attributes.color.substr(1), 16);
					col.columnOrder = parseInt(childNode.attributes.columnOrder,10);
					if(col.visible ==  true)
						columnsOrder[col.columnOrder] = i;
				
					col.id = childNode.attributes.id;
					col.align = childNode.attributes.align;
					col.width = parseInt(childNode.attributes.width,10);
					col.visible =  childNode.attributes.visible.toString() == "true" ? true : false;
					
				}
			}
		} else if (bNew && !bPlaylist) {
			for (var i = 0; i < columnsNum/*node.childNodes.length*/; ++i) {
					columns[i]=new Object({
						text:String, 
						color:Number,
						columnOrder:Number,
						id:String,
						align:String,
						width:Number,
						height:Number,
						textField:TextField,
						visible:Boolean,
						fontFormat:TextFormat});
					
					var col = columns[i];
					col.text ="" ;
					col.color = 0;
					col.columnOrder = i;
					col.id = "col"+i;
					col.align = "center";
					
					col.width = (i == 0 || i == 3)?40:80;
					col.visible = true;
			}
		} else if (bPlaylist) {
					trace("Playlist received");
					var Tracks:XMLNode=node.firstChild;
					var dta:Array = new Array( //{
						"",//TrNumber:"",
						"",//TrArtist:"",
						"",//TrTitle:"",
						""//TrDuration:""
						);//})
					var j:Number = 1;
					var retline = String.fromCharCode(13);
					for(var TrckNode:XMLNode=Tracks.firstChild;TrckNode!=null;TrckNode=TrckNode.nextSibling)
					{
						
						dta[0] += j + retline;
						
						dta[1] += TrckNode.attributes.Artist + retline;
						
						dta[2] += TrckNode.attributes.Title + retline;
						
						dta[3] += TrckNode.attributes.Duration + retline;
						j++;
					}
					for (var i = 0; i < columnsNum/*node.childNodes.length*/; ++i) {
					columns[i]=new Object({
						text:String, 
						color:Number,
						columnOrder:Number,
						id:String,
						align:String,
						width:Number,
						height:Number,
						textField:TextField,
						visible:Boolean,
						fontFormat:TextFormat});
					
					var col = columns[i];
					col.text = dta[i];
					col.color = 0;
					col.columnOrder = i;
					col.id = "col"+i;
					col.align = "center";
					col.width = (i == 0 || i == 3)?40:80;
					col.visible = true;
				    }
		}
		if( node.attributes.visible == undefined || node.attributes.visible.toString() == "true"){
			this.Visible = true;
		}
		else 
			this.Visible = false;
		trace("Table unit:this.visible = "+this.Visible);
		if(id == undefined)	
			id = this.mc._name;
		width = currWidth;

		SortColumns();
	}
	function ReinitTable(node:XMLNode,bLayout:Boolean,bEmpty:Boolean,bRewriteFormatting:Boolean){
		var Tracks:XMLNode=node.firstChild;
		var oldcolumns = columns;
		columns = new Array();
		var dta:Array = new Array( //{
			"",//TrNumber:"",
			"",//TrArtist:"",
			"",//TrTitle:"",
			""//TrDuration:""
				);//})
		if(bLayout == undefined){
			bLayout = false;
		}
		if(bEmpty == undefined){
			bEmpty = false;
		}
		if(bRewriteFormatting == undefined){
			bRewriteFormatting = false;
		}
		
		
		var retline = String.fromCharCode(13);
		trace("Reinit table "+node+" blayout "+bLayout + "  bEmpty "+bEmpty);
		if(columns == undefined)
			columns = new Array();
		if(!bEmpty && node == null && bLayout){
			trace("emptyLayoutItem = "+emptyLayoutItem);
			trace("GetAllText().length <1  = "+GetAllText().length);
			if(GetAllText().length > 0 )
				return;
				
			for (var i = 0; i < columnsNum; ++i) {
				columns[i]=new Object({
						text:String, 
						color:Number,
						columnOrder:Number,
						id:String,
						align:String,
						width:Number,
						height:Number,
						textField:TextField,
						visible:Boolean,
						fontFormat:TextFormat});
				
				var col = columns[i];
				col.color = bRewriteFormatting?0:oldcolumns[i].color;
				col.text = "";
				col.columnOrder = i;
				col.id = "col"+i;
				col.align = bRewriteFormatting?"center":oldcolumns[i].align;
				col.width = (i == 0 || i == 3)?40:80;
				col.visible = true;
			}
			for(var i=0;i<4;i++){
				var no:Number = i+1;
				columns[0].text+=no+retline;
				columns[1].text+="Artist"+retline;
				columns[2].text+="Track "+no+retline;
				columns[3].text+="0:00"+retline;
			}
			
			
		}
		
		if(bEmpty && node == null && !bLayout) {
			if(GetAllText().length > 0 && LayoutPosition == undefined)
				return;
			for (var i = 0; i < columnsNum/*node.childNodes.length*/; ++i) {
				columns[i]=new Object({
						text:String, 
						color:Number,
						columnOrder:Number,
						id:String,
						align:String,
						width:Number,
						height:Number,
						textField:TextField,
						visible:Boolean,
						fontFormat:TextFormat});					
				var col = columns[i];
				col.color = bRewriteFormatting?0:oldcolumns[i].color;
				col.text = "";
				col.columnOrder = i;
				col.id = "col"+i;
				col.align = bRewriteFormatting?"center":oldcolumns[i].align;
					
					col.width = (i == 0 || i == 3)?40:80;
					col.visible = true;
			}
		}
		if(!bEmpty && !bLayout){
			var j:Number = 1;
			
			for(var TrckNode:XMLNode=Tracks.firstChild;TrckNode!=null;TrckNode=TrckNode.nextSibling)
			{
				
				dta[0] += j + retline;
				
				dta[1] += TrckNode.attributes.Artist + retline;
				
				dta[2] += TrckNode.attributes.Title + retline;
				
				dta[3] += TrckNode.attributes.Duration + retline;
				j++;
			}
			columns = new Array();
			for (var i = 0; i < columnsNum/*node.childNodes.length*/; ++i) {
				columns[i]=new Object({
						text:String, 
						color:Number,
						columnOrder:Number,
						id:String,
						align:String,
						width:Number,
						height:Number,
						textField:TextField,
						visible:Boolean,
						fontFormat:TextFormat});
		
				var col = columns[i];
				col.text = dta[i];
				col.color = bRewriteFormatting?0:oldcolumns[i].color;
				col.columnOrder = i;
				col.id = "col"+i;
				col.align = bRewriteFormatting?"center":oldcolumns[i].align;
				col.width = (i == 0 || i == 3)?40:80;
				col.visible = true;
			}
		}
		Draw();
	}
	
	function GetXmlNode():XMLNode {
		if(GetAllText().length < 1 || EmptyLayoutItem != undefined || LayoutPosition !=undefined)
			return null;
		trace("GetXMLnode after layout verification "+GetAllText().length +" = GetAllText().length ");
		trace("EmptyLayoutItem = " +EmptyLayoutItem);
		trace("LayoutPosition = " +LayoutPosition);
		trace ("TableUnit:GetXmlNode");
		var node:XMLNode = super.GetXmlNode();
		node.nodeName = "Playlist";
		node.attributes.italic = this.italic;
		node.attributes.bold = this.bold;
		node.attributes.underline = this.underline;
		node.attributes.fontSize = this.fontSize;
		node.attributes.font = this.font ;
		node.attributes.strokeInterval = this.strokeInterval;
		node.attributes.height = GetUnitHeight();
		node.attributes.width = GetUnitWidth();
		node.attributes.layoutPosition = layoutPosition;
		node.attributes.visible = this.Visible;
		node.attributes.emptyLayoutItem = this.EmptyLayoutItem;
		var columnsNode:XMLNode = new XMLNode(1,"Columns");
		for(var i = 0; i < columnsNum; i++) {
			var columnNode:XMLNode = new XMLNode(1,"Column");
			columnNode.attributes.text = columns[i].text;
			columnNode.attributes.color = _global.GetColorForXML(columns[i].color);
			columnNode.attributes.columnOrder = columns[i].columnOrder;
			columnNode.attributes.id = columns[i].id;
			columnNode.attributes.visible = columns[i].visible;
			columnNode.attributes.align = columns[i].align;
			columnNode.attributes.width = columns[i].width;
			columnsNode.appendChild(columnNode);
		}
		node.appendChild(columnsNode);
		node.appendChild(super.GetXmlNodeRotate());
				

		return node;
	}
	function Draw():Void {
		
		trace ("TableUnit:Draw");
		DrawMC(mc,true);
	}
	function DrawMC(mc:MovieClip,bDrawOnWorkarea:Boolean):Void {
		SortColumns();
		trace ("TableUnit:DrawMC");
		mc.clear();
		//trace("TableUnit:this.Visible "+this.Visible);
		//trace("TableUnit:this.EmptyLayoutItem "+EmptyLayoutItem);
		
		var currWidth:Number = 0;
		var currHeight:Number = 0;
		if(bDrawOnWorkarea == undefined)
			bDrawOnWorkarea = false;
		//trace("_global.DesignMode"+_global.DesignMode);
		trace("bDrawOnWorkarea = "+bDrawOnWorkarea);
		trace("EmptyLayoutItem = "+EmptyLayoutItem);
		trace("LayoutPosition = "+LayoutPosition);
		//if(/*_global.DesignMode == "Preview"*/ bDrawOnWorkarea == false && EmptyLayoutItem != undefined && LayoutPosition !=undefined)//bDrawOnWorkarea == false && EmptyLayoutItem!=undefined && LayoutPosition!=undefined)	
		if(bDrawOnWorkarea == false && !(EmptyLayoutItem == undefined || LayoutPosition ==undefined))
		{
			mc._visible = false;
			trace("this.Visible = false");
		}
		else {
			mc._visible = true;
			trace("this.Visible = true");
		}
	
		trace(_root.pnlPreviewLayer._visible +" _root.pnlPreviewLayer._visible");
		for(var i:Number = 0;i < columnsOrder.length;i++) {
				if(columnsOrder[i]== undefined || columnsOrder[i] == null )
					continue;
				var col = columns[columnsOrder[i]];
				mc[col.id].removeTextField();
				//
				//	continue;
				trace("col.visible"+col.visible);
				mc.createTextField(col.id , mc.getNextHighestDepth(), currWidth, 0, col.width, 0);
				col.textField = mc[col.id];
				col.textField.embedFonts = true;
				col.textField.autoSize = true;
				col.textField.wordWrap = false;
				col.textField.multiline = true;
				col.textField.html = false;
				col.textField.selectable = false;
				col.fontFormat = col.textField.getNewTextFormat();
				col.fontFormat.font = font;
				col.fontFormat.size = fontSize;
				col.fontFormat.bold = this.bold;
				col.fontFormat.italic = this.italic;
				col.fontFormat.underline = this.underline;
				col.fontFormat.color = col.color;
				col.fontFormat.align = col.align;
				col.fontFormat.leading = strokeInterval;
				
				col.textField._x= currWidth;
				col.textField.text = col.text;
				col.textField.setTextFormat(col.fontFormat);
				col.textField.setNewTextFormat(col.fontFormat);
				if (!_global.Fonts.TestFont(col.fontFormat))
					_global.Fonts.LoadFont(col.fontFormat, this);
				col.height = col.textField._height;
				currHeight = Math.max(col.height,currHeight);
				col.textField.autoSize = false;
				col.textField._height = col.height;
				col.textField._width = col.width;
				if(col.textField._width > col.width) {
					col.width = col.textField._width;
				}
				
				if(col.visible == false) {
					col.textField._width = 0;
				}
							
				col.textField._height += 2;
				col.textField._visible = col.visible;
				if(col.visible != false) {
					currWidth += col.width;
				}
		}
		/*for(var i = 0; i<columnsNum; i++) {
			columns[i].textField._visible = this.Visible&&!EmptyLayoutItem;
		}*/
		mc.beginFill(0, 0);
		mc.moveTo(0, 0);
		mc.lineTo(currWidth, 0);
		mc.lineTo(currWidth, GetUnitHeight());
		mc.lineTo(0, currHeight);
		mc.endFill();
		if(_global.CurrentUnit == this)
			Attach();
		trace("Unit height:"+GetUnitHeight()+" "+this.mc._height);
		trace("Unit width:"+GetUnitWidth()+" "+this.mc._width);
	
	}
	function getMode():String {
		//trace ("TableUnit:GetMode");
		return _global.PlaylistEditMode;
	}
	
	function GetColumnsWidth():Array {
		var result = new Array();
		for(var i = 0; i < columnsNum; i++) {
			if(columnsOrder[i] != undefined)
				if(columns[columnsOrder[i]].visible == true)
					result.push( columns[columnsOrder[i]].width) ;
		}
		return result;
	}
	
	
	function SortColumns() {
		columnsOrder = new Array(columnsNum);
		for(var i = 0; i < columnsNum ; i++) {
		//	if(columns[i].visible == true)
				columnsOrder[columns[i].columnOrder]=i;
		}	
		trace("Col order"+columnsOrder);
	}
	
	public function ResizeByStep(isPositive:Boolean) {
		var step:Number = isPositive ? 1 : -1;
		var k = (this.Size + step) / this.Size;
		var size = this.Size;
		Resize(1, k);
		if (size != this.Size) {
			Resize(k, 1);
		}
	}
	
	private function Resize(kx:Number, ky:Number) {
 		var newSize  = Math.round(this.Size * ky);

 		if (newSize >=4 && newSize <= 127 && newSize != this.fontSize) {
			if(GetAllText().length >0)
 			this.fontSize = newSize;
				//TODO: refcatoring need
 			ToolPropertiesContainer.GetInstance().ctlPlaylistEditProperties.RefreshFontSize();
 		}
		
		var allWidth:Number = GetUnitWidth();
		if(allWidth < 40 && kx < 1) {
			kx = 1;
			allWidth = 40.1;
		}

		var currWidth:Number = 0;
		for(var i = 0; i < columnsNum ; i++) {
			var colnum:Number = columnsOrder[i];
			if(colnum != undefined && columns[colnum].visible != false)
			{	
				columns[colnum].width = kx * columns[colnum].width ;
				if(columns[colnum].width <10){
					columns[colnum].width = 10; 
				}
				columns[colnum].textField._x = currWidth;
				currWidth += columns[colnum].width;
			}
		}
		Draw();
	}

	
	function ResizeColumnWidth(colNum:Number,deltaX:Number) {
		var showed:Number = 0;
		trace(colNum);
		for(var i = 0; i < columnsNum; i++) {
			if(columns[columnsOrder[i]].visible == true){
				
				if(showed == colNum){
					if ((columns[columnsOrder[i]].width < 10 && deltaX < 0) || ((columns[columnsOrder[i]].width + deltaX )< 10))
							return;
					columns[columnsOrder[i]].width += deltaX;
					if(columns[columnsOrder[i]].width < 10)
						columns[columnsOrder[i]].width = 10;
					break;
				}
				showed++;
			}
		}
	//	if (columns[columnsOrder[colNum]].width < 10 && deltaX < 0)
		//	return;
		//columns[columnsOrder[colNum]].width += deltaX;

		Draw();
		Attach();
		
	}
	function GetUnitWidth():Number {
		var res:Number = 0;
		for(var i = 0; i < columnsNum; i++) {
			if(columns[i].visible != false)
				res += columns[i].width;
		}
		return res;
	}
	function GetUnitHeight():Number {
		var res:Number = 0;
		for(var i = 0; i < columnsNum; i++) {
		//	if(columns[i].visible != false)
				res = Math.max( columns[i].textField._height,res);
		}
		return res;
	}

	function SetLeading(pos:String, deltaY:Number) {
		
		var delta:Number = Math.ceil(deltaY / 12);
		if(GetAllText().length <1)
			return;
		if(pos == "bottomleft") 
			strokeInterval += delta;
		else if(pos == "topright")
			strokeInterval -= delta;
		var oldHeight:Number = GetUnitHeight();
		var oldX = this.mc._x;
		var oldY = this.mc._y;
		if(strokeInterval < 0)
		strokeInterval = 0;
			
		
		Draw();
		var newHeight:Number = GetUnitHeight();
		trace("Angle = "+Angle);
		var angle:Number = Angle*Math.PI/180;
		trace("angle = "+angle);
		if(pos == "topright") 
		{
			_global.SelectionFrame.Y -= (newHeight-oldHeight) * Math.cos(angle);
			_global.SelectionFrame.X += (newHeight-oldHeight) * Math.sin(Math.PI-angle);
			
		}
		
	}
	
	public function get Size ():Number {
		return fontSize;
	}
	public function set Size (value) {
		fontSize = value;
		Draw();
		Attach();
	}
	public function get Bold ():Boolean {
		return bold;
	}
	public function set Bold (value) {
		bold = value;
		Draw();
		Attach();
	}
	
	public function get Italic ():Boolean {
		return italic;
	}
	public function set Italic (value) {
		italic = value;
		Draw();
		Attach();
	}
	public function get Underline ():Boolean {
		return underline;
	}
	public function set Underline (value) {
		underline = value;
		Draw();
		Attach();
	}
	public function get Font ():String {
		return font;
	}
	public function set Font (value) {
		font = value;
		Draw();
		Attach();
	}
	
	function SetColumnOrder(colNum:Number, colNewOrder:Number) {
		var oldPos:Number = columns[colNum].columnOrder ;
		
		for(var i = 0; i < columnsNum; i++) {
			if(colNum == i)
				columns[i].columnOrder = colNewOrder;
			else {
				if(colNewOrder > oldPos) {
					if(columns[i].columnOrder <= colNewOrder && columns[i].columnOrder > oldPos)
						columns[i].columnOrder--;
				}
				else if (colNewOrder < oldPos) {
					if(columns[i].columnOrder >= colNewOrder && columns[i].columnOrder < oldPos)
						columns[i].columnOrder++;
				}
			}
		}
		/*var oldPos:Number = columns[colNum].columnOrder ;
		for(var i = 0; i < columnsNum; i++) {
			if(colNum == i)
				columns[i].columnOrder = colNewOrder;
			else {
				if(colNewOrder > oldPos ) {
					if(columns[i].columnOrder >= colNewOrder)
						columns[i].columnOrder++; 
					else if( columns[i].columnOrder > oldPos  )
						columns[i].columnOrder--;
				} else if(colNewOrder < oldPos ) {
					if(columns[i].columnOrder >= colNewOrder && columns[i].columnOrder < oldPos)
						columns[i].columnOrder ++;
				}
				
				
			}
		}*/
		SortColumns();
		Draw();
		Attach();
	}
	
	public function ApplyLayoutFormat(layout:FaceLayoutItem, putNewText:Boolean):Void {		
		var format:TextFormat = new TextFormat();
		this.font = layout.Font;
		this.fontSize = layout.Size;
		this.bold = layout.Bold;
		this.italic = layout.Italic;
		for(var i = 0; i < columnsNum; i++) {
			columns[i].color = layout.Color;
			columns[i].align = layout.Align;
		}

/*		var actualText = (putNewText) ? layout.Text : Text;
		ReplaceText(0, actualText.length, actualText, format);*/
		
		this.X = layout.X;
		this.Y = layout.Y;
		this.Angle = layout.Angle;
		this.EmptyLayoutItem = layout.EmptyLayoutItem;
		trace(this.EmptyLayoutItem + " this.empty.layout.item");
		Draw();
	}
	
	
	function GetColumnOrder(colNum:Number):Number {
		return columns[colNum].columnOrder ;
	}
	
	function SetColumnColor(colNum:Number, colColor:Number) {
		columns[colNum].color = colColor;
		Draw();
		Attach();
	}
	
	function GetColumnColor(colNum:Number):Number {
		return columns[colNum].color ;
	}
	
	function SetColumnAlign(colNum:Number, colAlign:String) {
		columns[colNum].align = colAlign;
		Draw();
		Attach();
	}
	
	public function GetColumnVisible(colNum:Number):Boolean {
		return columns[colNum].visible;
	}
	
	public function SetColumnVisible(colNum:Number, bVisible:Boolean) {
		bVisible = (bVisible == "true" || bVisible == true) ? true : false;
		var num:Number = 0;
		if(bVisible == false)
		{
			if( IsCanHideAnyOne() == false)
				return;
			
			mc[columns[colNum].id].removeTextField();
			
		}
		columns[colNum].visible = bVisible;
		Draw();
		Attach();
	}
	function IsCanHideAnyOne():Boolean {
			
			var num:Number = 0;
			for(var i:Number = 0; i < columnsNum; i++) {
				if(columns[i].visible == true)
					num += 1;
				trace(num);
			}
			if(num>1)
				return true;
			else 
				return false;
	}
	function GetColumnAlign(colNum:Number):String {
		return columns[colNum].align ;
	}
	
	function SetColumnText(colNum:Number, colText:String) {
		EmptyLayoutItem = undefined;
		LayoutPosition = undefined;
		columns[colNum].text = colText;
		Draw();
		Attach();
	}
	
	function GetColumnText(colNum:Number):String {
		return columns[colNum].text ;
	}
	function Attach(){
		_global.SelectionFrame.AttachUnit(this);
	}
	function GetAllText():String {
		var res:String="";
			for(var i:Number = 0; i < columnsNum; i++) {
				res+=columns[i].text;
			}
		return res;
		
	}
		
	public function get EmptyLayoutItem():Boolean {
		return emptyLayoutItem;
	}
	public function set EmptyLayoutItem(value:Boolean):Void {
		trace("Set emptylayoutitem:"+value);
		emptyLayoutItem = value;
	}
	
	
	public function get LayoutPosition():Number {
		return layoutPosition;
	}
	public function set LayoutPosition(value:Number):Void {
		layoutPosition = value;
	}
	public function get Visible():Boolean {
		return mc._visible;
	}
	public function set Visible(value:Boolean):Void {
		mc._visible = value;
	}
	
	
	
}
