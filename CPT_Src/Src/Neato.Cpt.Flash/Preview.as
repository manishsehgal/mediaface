﻿class Preview extends MovieClip {
	private var paper:Paper;

	function Preview() {
		this["preview"] = true;
	}
	
	function Hide() {
		trace("Preview Hide");
		_root.pnlMainLayer._visible = true;
		_root.pnlPreviewLayer._visible = false;
	}
	
	function Show() {
		trace("Preview Show");
		_root.pnlPreviewLayer._visible = true;
		_root.pnlMainLayer._visible = false;
		_root.pnlPreviewLayer.content.pnlPreview.spContentHolder.ctlPreview.Init();
		paper.Draw();

		var margin:Number = 0;
		var descrWidth:Number = 300;
		var menuHeight:Number = 30;
		var designerWidth:Number = 904;
		var designerHeight:Number = 540;
		var maxHeight:Number = 390;
		trace("descrWidth = "+descrWidth);
		trace("menuHeight = "+menuHeight);
		trace("designerWidth = "+designerWidth);
		trace("designerHeight = "+designerHeight);
		
		var originalWidth = designerWidth - descrWidth - 2*margin;
		var originalHeight = designerHeight - menuHeight - 2*margin;
		trace("originalWidth = "+originalWidth);
		trace("originalHeight = "+originalHeight);
		
		var xF=originalWidth/paper.Width*100;
		//var yF=originalHeight/paper.Height*100;
		var yF = (paper.Height >= maxHeight ? maxHeight : originalHeight) / paper.Height*100;
		var scaleFactor = (xF<yF)?xF:yF;	//Calculate min. scale
		
		trace("paper.Width = "+paper.Width);
		trace("paper.Height = "+paper.Height);
		
		paper._xscale = paper._yscale = scaleFactor;
		paper._x = descrWidth + margin + (originalWidth - paper.Width * scaleFactor/100) / 2;
		paper._y = menuHeight + margin + (originalHeight - paper.Height * scaleFactor/100) / 2;
		trace("paper._xscale = "+paper._xscale);
		trace("paper._x = "+paper._x);
		trace("paper._y = "+paper._y);
	}		
}