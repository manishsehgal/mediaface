﻿import mx.core.UIObject;
import mx.controls.*;
import mx.utils.Delegate;
import SAPlugins;

class Tools.AddPlaylistTool.WMPTool extends UIObject  {
	
	private var lblPlayer:Label;
	private var cboPlayer:ComboBox;
	private var lblPlaylist:Label;
	private var cboPlaylist:ComboBox;
	
	private var pluginsService:SAPlugins;
	
	function WMPTool(){
		pluginsService = SAPlugins.GetInstance();
		pluginsService.RegisterOnInvokedHandler(this, onInvoked);
	}

	function onLoad()
	{
		setStyle("styleName", "ToolPropertiesPanel");
		
		this.cboPlayer.setStyle("styleName", "ToolPropertiesCombo");
		cboPlayer.addItem("Select player...", "");
		cboPlayer.addItem("Windows Media Player", "wmp");
		cboPlayer.addItem("iTunes", "itunes");
		cboPlayer.selectedIndex = 0;
		cboPlayer.addEventListener("change", Delegate.create(this, cboPlayer_onChange));
		
		_global.LocalHelper.LocalizeInstance(lblPlaylist,"ToolProperties","IDS_LBLSELECTPLAYLIST");
		
		this.cboPlaylist.setStyle("styleName", "ToolPropertiesCombo");
		cboPlaylist.enabled = false;
	}
	
	function Show() {
		this._visible = true;
		OnAvaible(false, "");
	}
	
	private function cboPlayer_onChange(eventObject) {
		OnAvaible(false, "");
		
		var cmdName:String;
		if (cboPlayer.selectedItem.data == "wmp") {
			cmdName = "WMP_GET_CATEGORIES";
		} else if (cboPlayer.selectedItem.data == "itunes") {
			cmdName = "ITUNES_GET_CATEGORIES";
		} else {
			cboPlaylist.removeAll();
			cboPlaylist.enabled = false;
			return;
		}
		if (SAPlugins.IsAvailable() == false) return;
		
		var cmd:XML = new XML();
		var CmdInfo:XMLNode = cmd.createElement("CmdInfo");
		CmdInfo.attributes.Type = "Request";
		cmd.appendChild(CmdInfo);
		
		var Request:XMLNode = cmd.createElement("Request");
		Request.attributes.Name = cmdName;
		CmdInfo.appendChild(Request);
		
		pluginsService.InvokeCommand(cmd);
	}
	
	function onInvoked(eventObject) {
		try {
			var reply:XML = new XML();
			reply.parseXML(eventObject.result);
		    
			var CmdInfo:XMLNode = reply.firstChild;
			var Response:XMLNode = GetChild(CmdInfo, "Response");
			if (Response == undefined) throw "Not available";
			if (Response.attributes.Error != "0") throw Response.attributes.ErrorDesc;
			var Params:XMLNode = GetChild(Response, "Params");
			if (Params == undefined) throw "Not available";
		    
			if (CmdInfo.attributes.Name == "WMP_GET_CATEGORIES" ||
			    CmdInfo.attributes.Name == "ITUNES_GET_CATEGORIES") {
				
				cboPlaylist.removeAll();
				var Cats:XMLNode = GetChild(Params, "Categories");
				if (Cats != undefined) {
					cboPlaylist.enabled = true;
					for(var node:XMLNode=Cats.firstChild; node!=null; node=node.nextSibling) {
						if (node.nodeName == "Category") {
							cboPlaylist.addItem(node.attributes.Name);
						}
					}
				}
				if (cboPlaylist.length == 0) {
					cboPlaylist.enabled = false;
					OnAvaible(false, "No playlists");
				} else {
					cboPlaylist.selectedIndex = 0;
					OnAvaible(true,"");
				}
			} else if(CmdInfo.attributes.Name=="WMP_GET_PLAYLIST" ||
			          CmdInfo.attributes.Name == "ITUNES_GET_PLAYLIST") {
				var Playlist:XMLNode = GetChild(Params, "PlayList");
				OnAdd(Playlist);
			}
		} catch(err:String) {
			OnAvaible(false, err);
		}
	}
	
	//Get first child node of 'parNode' with given name 'childName'
	function GetChild(parNode:XMLNode, childName:String) : XMLNode {
		for(var node:XMLNode=parNode.firstChild; node!=null; node=node.nextSibling){
			if (node.nodeName == childName) return node;
		}
		return undefined;
	}
	
	function Retrieve() {
		if(cboPlaylist.enabled == false || cboPlaylist.length < 1){
			return;
		}
		
		var cmdName:String;
		var catId:String;
		if (cboPlayer.selectedItem.data == "wmp") {
			cmdName = "WMP_GET_PLAYLIST";
			catId = cboPlaylist.selectedIndex.toString();
		} else if (cboPlayer.selectedItem.data == "itunes") {
			cmdName = "ITUNES_GET_PLAYLIST";
			catId = cboPlaylist.selectedItem.label;
		} else {
			return;
		}
		
		var cmd:XML = new XML();
		var CmdInfo:XMLNode = cmd.createElement("CmdInfo");
		CmdInfo.attributes.Type = "Request";
		cmd.appendChild(CmdInfo);
		
		var Request:XMLNode = cmd.createElement("Request");
		Request.attributes.Name = cmdName;
		CmdInfo.appendChild(Request);
		
		var Params:XMLNode = cmd.createElement("Params");
		Request.appendChild(Params);
		
		var Cat:XMLNode = cmd.createElement("Category");
		Cat.attributes.CatId = catId;
		Params.appendChild(Cat);
		
		pluginsService.InvokeCommand(cmd);
	}
	
	private function OnAdd(node:XMLNode) {
		var playlistNode = node;
		var eventObject = {type:"add", target:this, playlistNode:playlistNode};
		trace("OnAdd"+eventObject.playlistNode);
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnAddHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("add", Delegate.create(scopeObject, callBackFunction));
    }
	
	//region Avaible
	private function OnAvaible(bAvaible:Boolean,err:String) {
		var avaible:Boolean = (bAvaible == false || bAvaible == undefined) ? false : true;
		var errorDesk:String = (bAvaible == false || bAvaible == undefined) ? err : "";
		var eventObject = {type:"avaible", target:this, avaible:avaible, errorDesk:errorDesk};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnAvaibleHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("avaible", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
}