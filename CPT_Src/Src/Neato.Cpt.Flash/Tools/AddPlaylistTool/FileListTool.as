﻿
import mx.core.UIObject;
import mx.controls.*;
import mx.utils.Delegate;
import SAPlugins;

class Tools.AddPlaylistTool.FileListTool extends UIObject {
	
	private var pluginsService:SAPlugins;
	private var mode:String;//current tool mode: "folder" or "file"
	private var curFolder:String;
	
	private var lblPath:TextArea;
	private var btnUp:Button;
	private var lstFolders:List;
	
	function FileListTool(){
		this.lblPath.setStyle("styleName", "ToolPropertiesInputText");
		this.btnUp.setStyle("styleName", "ToolPropertiesActiveButton");
        this.lstFolders.setStyle("styleName", "ToolPropertiesCombo");
		
		pluginsService = SAPlugins.GetInstance();
		pluginsService.RegisterOnInvokedHandler(this, onResponse);
		mode = "file";
		curFolder = "";
	}
	
	public function set Mode(val:String):Void {
		this.mode = val;
	}
	
	function Show() {
		this._visible = true;
		DoFileList(curFolder);
		OnAvaible(false, "");
	}
	
    function onLoad() {
        this.btnUp.addEventListener("click", Delegate.create(this, onUp));
        this.lstFolders.addEventListener("change", Delegate.create(this, onFolderClick));
    }
    
	function DoFileList(folder:String):Void {
		if (SAPlugins.IsAvailable() == false) return;
		
		var cmd:XML = new XML();
		var CmdInfo:XMLNode = cmd.createElement("CmdInfo");
		CmdInfo.attributes.Type = "Request";
		cmd.appendChild(CmdInfo);
		
		var Request:XMLNode = cmd.createElement("Request");
		Request.attributes.Name = "FILELIST_GET_FOLDER_CONTENT";
		CmdInfo.appendChild(Request);
		
		var Params:XMLNode = cmd.createElement("Params");
		Request.appendChild(Params);
		
		var FileEl:XMLNode = cmd.createElement("Folder");
		FileEl.attributes.Path = folder;
		Params.appendChild(FileEl);
		
		pluginsService.InvokeCommand(cmd);
	}
	
	function DoPlayList(file:String):Void {
		if (SAPlugins.IsAvailable() == false) return;
		
		var cmd:XML = new XML();
		var CmdInfo:XMLNode = cmd.createElement("CmdInfo");
		CmdInfo.attributes.Type = "Request";
		cmd.appendChild(CmdInfo);
		
		var Request:XMLNode = cmd.createElement("Request");
		Request.attributes.Name = "FILELIST_GET_PLAYLIST";
		CmdInfo.appendChild(Request);
		
		var Params:XMLNode = cmd.createElement("Params");
		Request.appendChild(Params);
		
		var FileEl:XMLNode = cmd.createElement("File");
		FileEl.attributes.Name = file;
		Params.appendChild(FileEl);
		
		pluginsService.InvokeCommand(cmd);
	}
	
	function DoScanFolder(folder:String):Void {
		if (SAPlugins.IsAvailable() == false) return;
		
		var cmd:XML = new XML();
		var CmdInfo:XMLNode = cmd.createElement("CmdInfo");
		CmdInfo.attributes.Type = "Request";
		cmd.appendChild(CmdInfo);
		
		var Request:XMLNode = cmd.createElement("Request");
		Request.attributes.Name = "FILELIST_GET_PLAYLIST";
		CmdInfo.appendChild(Request);
		
		var Params:XMLNode = cmd.createElement("Params");
		Request.appendChild(Params);
		
		var FileEl:XMLNode = cmd.createElement("Folder");
		FileEl.attributes.Name = folder;
		FileEl.attributes.GetSubFolders = "true";
		Params.appendChild(FileEl);
		
		pluginsService.InvokeCommand(cmd);
	}
	
	function onResponse(eventObject) {
		try {
		var reply:XML = new XML();
		reply.parseXML(eventObject.result);
		
		var CmdInfo:XMLNode = reply.firstChild;
		var Response:XMLNode = GetChild(CmdInfo, "Response");
		if (Response == undefined) throw "Not available";
		if (Response.attributes.Error != "0") throw Response.attributes.ErrorDesc;
		var Params:XMLNode = GetChild(Response, "Params");
		if (Params == undefined) throw "Not available";
		
		if (CmdInfo.attributes.Name == "FILELIST_GET_FOLDER_CONTENT") {
			var Cont:XMLNode = GetChild(Params, "FolderContent");
			if (Cont == undefined) throw "Not available";
			
			curFolder = Cont.attributes.Path;
			lblPath.text = curFolder;
			
			lstFolders.removeAll();
			var Folders:XMLNode = GetChild(Cont, "Folders");
			if (Folders != undefined) {
				for(var node:XMLNode=Folders.firstChild; node!=null; node=node.nextSibling){
					if (node.nodeName == "Folder") {
						lstFolders.addItem(node.attributes.Name, "d");
					}
				}
			}
			var Files:XMLNode = GetChild(Cont, "Files");
			if (Files != undefined) {
				for(var node:XMLNode=Files.firstChild; node!=null; node=node.nextSibling){
					if (node.nodeName == "File") {
						lstFolders.addItem(node.attributes.Name, "f");
					}
				}
			}
			//lstFolders.selectedIndex = 0;
			OnAvaible((mode == "file" ? false : true), "");
		}
		else if (CmdInfo.attributes.Name == "FILELIST_GET_PLAYLIST") {
			var PL:XMLNode = GetChild(Params, "PlayList");
			if (PL == undefined) throw "Not available";
			OnAdd(PL);
		}
		} catch(err:String) {
			OnAvaible(false, err);
		}
	}
	
	//Get first child node of 'parNode' with given name 'childName'
	function GetChild(parNode:XMLNode, childName:String) : XMLNode {
		for(var node:XMLNode=parNode.firstChild; node!=null; node=node.nextSibling){
			if (node.nodeName == childName) {
				return node;
			}
		}
		return undefined;
	}
	
    function onUp(eventObject) {
        DoFileList(curFolder + "..");
    }
    
    function onFolderClick(eventObject) {
    	var item = this.lstFolders.selectedItem;
       	if (item == undefined) return;
       	if (item.data == "d") {
   			DoFileList(curFolder + item.label + "\\");
   		} else {
   			OnAvaible(true, "");
   		}
    }
    
	function Retrieve() {
		if (mode == "file") {
	    	var item = this.lstFolders.selectedItem;
	    	if (item != undefined && item.data == "f") {
        		DoPlayList(curFolder + item.label);
        	}
        } else if (mode == "folder") {
	        DoScanFolder(curFolder);
        }
    }
    
	private function OnAdd(node:XMLNode) {
		var playlistNode = node;
		var eventObject = {type:"add", target:this, playlistNode:playlistNode};
		this.dispatchEvent(eventObject);
	};
	
	public function RegisterOnAddHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("add", Delegate.create(scopeObject, callBackFunction));
    }
	
	private function OnAvaible(bAvaible:Boolean, err:String) {
		var avaible:Boolean = (bAvaible == false || bAvaible == undefined) ? false : true;
		var errorDesc:String = err;
		var eventObject = {type:"avaible", target:this, avaible:avaible, errorDesk:errorDesc};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnAvaibleHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("avaible", Delegate.create(scopeObject, callBackFunction));
    }
}
