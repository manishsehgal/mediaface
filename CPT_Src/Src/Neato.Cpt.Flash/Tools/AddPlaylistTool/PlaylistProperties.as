﻿import mx.core.UIObject;
import mx.utils.Delegate;
import mx.controls.*;
import Tools.AddPlaylistTool.*;
class Tools.AddPlaylistTool.PlaylistProperties extends UIObject {
	//private var cboSource:ComboBox;
	//public var txtResponse:TextArea;
	private var lblPlaylist:Label;
	private var lblError:Label;
	private var listenerCboSource:Object;
	private var CDTextToolClip:CDTextTool;
	private var AudioCDToolClip:AudioCDTool;
	private var WMPToolClip:WMPTool;
	private var FileListToolClip:FileListTool;
	private var keyListener:Object;
	private var btnCDText:MenuButton;
	private var btnAudio:MenuButton;
	private var btnFolder:MenuButton;
	private var btnPlayer:MenuButton;
	private var btnFile:MenuButton;
	private var btnManual:MenuButton;
	private var btnCancel:MenuButton;
	private var btnCreate:MenuButton;
		
	private var currentClip;
	function PlaylistProperties() {
		currentClip = null;
		setStyle("styleName", "ToolPropertiesPanel");
		//this.cboSource.setStyle("styleName", "ToolPropertiesCombo");
		//this.txtResponse.setStyle("styleName", "ToolPropertiesInputText");
		this.btnCancel.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnCreate.setStyle("styleName", "ToolPropertiesActiveButton");
		this.lblError.setStyle("styleName", "ToolPropertiesNote");
		btnCDText.setStyle("styleName", "ToolPropertiesRadioButton");
		btnAudio.setStyle("styleName", "ToolPropertiesRadioButton");
		btnFolder.setStyle("styleName", "ToolPropertiesRadioButton");
		btnPlayer.setStyle("styleName", "ToolPropertiesRadioButton");
		btnFile.setStyle("styleName", "ToolPropertiesRadioButton");
		btnManual.setStyle("styleName", "ToolPropertiesRadioButton");

		CDTextToolClip._visible=false;
		AudioCDToolClip._visible=false;
		WMPToolClip._visible=false;
		FileListToolClip._visible=false;
	}
	
	function onLoad() {
		lblError.text = "";
		//btnManual.selected = true;
		
		CDTextToolClip.RegisterOnAvaibleHandler(this,OnAvaible);
		AudioCDToolClip.RegisterOnAvaibleHandler(this,OnAvaible);
		WMPToolClip.RegisterOnAvaibleHandler(this,OnAvaible);
		FileListToolClip.RegisterOnAvaibleHandler(this,OnAvaible);
		
		CDTextToolClip.RegisterOnAddHandler(this,OnPlaylistReceive);
		AudioCDToolClip.RegisterOnAddHandler(this,OnPlaylistReceive);
		WMPToolClip.RegisterOnAddHandler(this,OnPlaylistReceive);
		FileListToolClip.RegisterOnAddHandler(this,OnPlaylistReceive);
		
		_global.LocalHelper.LocalizeInstance(lblPlaylist,"ToolProperties","IDS_LBLADDPLAYLISTTEXT");
		_global.LocalHelper.LocalizeInstance(btnCancel, "ToolProperties", "IDS_BTNCANCEL");
		_global.LocalHelper.LocalizeInstance(btnAudio, "ToolProperties", "IDS_BTNAUDIO");
		_global.LocalHelper.LocalizeInstance(btnCDText, "ToolProperties", "IDS_BTNCDTEXT");
		_global.LocalHelper.LocalizeInstance(btnFolder, "ToolProperties", "IDS_BTNFOLDER");
		_global.LocalHelper.LocalizeInstance(btnPlayer, "ToolProperties", "IDS_BTNPLAYER");
		_global.LocalHelper.LocalizeInstance(btnFile, "ToolProperties", "IDS_BTNFILE");
		_global.LocalHelper.LocalizeInstance(btnManual, "ToolProperties", "IDS_BTNMANUAL");
		_global.LocalHelper.LocalizeInstance(btnCreate, "ToolProperties", "IDS_BTNCREATENEW");
		
		TooltipHelper.SetTooltip(btnCancel, "ToolProperties", "IDS_TOOLTIP_CANCEL");
		TooltipHelper.SetTooltip(btnAudio, "ToolProperties", "IDS_BTNAUDIO");
		TooltipHelper.SetTooltip(btnCDText, "ToolProperties", "IDS_BTNCDTEXT");
		TooltipHelper.SetTooltip(btnFolder, "ToolProperties", "IDS_BTNFOLDER");
		TooltipHelper.SetTooltip(btnPlayer, "ToolProperties", "IDS_BTNPLAYER");
		TooltipHelper.SetTooltip(btnFile, "ToolProperties", "IDS_BTNFILE");
		TooltipHelper.SetTooltip(btnManual, "ToolProperties", "IDS_BTNMANUAL");
		TooltipHelper.SetTooltip(btnCreate, "ToolProperties", "IDS_BTNCREATENEW");
		
		btnCancel.RegisterOnClickHandler(this,	btnCancel_OnClick);
		btnCreate.RegisterOnClickHandler(this, Retrieve);
		
		btnCDText.addEventListener("click", Delegate.create(this, btnCDText_OnClick));//.RegisterOnClickHandler(this,	btnCDText_OnClick);
		btnAudio.addEventListener("click", Delegate.create(this, btnAudio_OnClick));//.RegisterOnClickHandler(this,	btnAudio_OnClick);
		btnFolder.addEventListener("click", Delegate.create(this, btnFolder_OnClick));//.RegisterOnClickHandler(this,	btnFolder_OnClick);
		btnPlayer.addEventListener("click", Delegate.create(this, btnPlayer_OnClick));//.RegisterOnClickHandler(this,		btnPlayer_OnClick);
		btnFile.addEventListener("click", Delegate.create(this, btnFile_OnClick));//.RegisterOnClickHandler(this,	btnFile_OnClick);
		btnManual.addEventListener("click", Delegate.create(this, btnManual_OnClick));//.RegisterOnClickHandler(this,	btnManual_OnClick);
	}
	
	function ShowPlaylistProperties():Void {
	
		this._visible = true;
		HideAllClips();
		btnManual.selected = true;
		btnManual.selected = false;
		currentClip = null;
		btnCreate.enabled = true; 
	}
	
	private function Retrieve(eventObject) {
		if(currentClip != null)
			currentClip.Retrieve();
		else {
			OnAdd(null,true);
		}
	}
	private function btnCDText_OnClick(eventObject) {
		trace("btnCDText_OnClick");
		HideAllClips();
		if (SAPlugins.IsAvailable() == false){
			_global.LocalHelper.LocalizeInstance(lblError, "ToolProperties", "IDS_LBLNOPLUGIN");
			return;
		}
		currentClip = CDTextToolClip;
		currentClip.Show();
	}
	private function btnAudio_OnClick(eventObject) {
		trace("btnAudio_OnClick");
		HideAllClips();
		if (SAPlugins.IsAvailable() == false){
			_global.LocalHelper.LocalizeInstance(lblError, "ToolProperties", "IDS_LBLNOPLUGIN");
			return;
		}
		currentClip = AudioCDToolClip;
		currentClip.Show();
	}
	private function btnFolder_OnClick(eventObject) {
		trace("btnFolder_OnClick");
		HideAllClips();
		if (SAPlugins.IsAvailable() == false){
			_global.LocalHelper.LocalizeInstance(lblError, "ToolProperties", "IDS_LBLNOPLUGIN");
			return;
		}
		currentClip = FileListToolClip;
		currentClip.Mode = "folder";
		currentClip.Show();
	}
	private function btnPlayer_OnClick(eventObject) {
		trace("btnPlayer_OnClick");
		HideAllClips();
		if (SAPlugins.IsAvailable() == false){
			_global.LocalHelper.LocalizeInstance(lblError, "ToolProperties", "IDS_LBLNOPLUGIN");
			return;
		}
		currentClip = WMPToolClip;
		currentClip.Show();
	}
	private function btnFile_OnClick(eventObject) {
		HideAllClips();
		if (SAPlugins.IsAvailable() == false){
			_global.LocalHelper.LocalizeInstance(lblError, "ToolProperties", "IDS_LBLNOPLUGIN");
			return;
		}
		currentClip = FileListToolClip;
		currentClip.Mode = "file";
		currentClip.Show();
	}
	private function btnManual_OnClick(eventObject) {
		trace("btnManual_OnClick");
		currentClip = null;
		HideAllClips();
		btnCreate.enabled = true; 
	}
	
	private function btnCancel_OnClick(eventObject) {
		OnExit();
	}
	
	
	private function HideAllClips() {
		CDTextToolClip._visible = false;
		AudioCDToolClip._visible = false;
		WMPToolClip._visible = false;
		FileListToolClip._visible = false;
		btnCreate.enabled = false; 
		lblError.text = "";
	}
	private function OnPlaylistReceive(eventObject) {
		trace("OnPlaylistReceive))"+eventObject.playlistNode);
		var playlistNode:XMLNode = eventObject.playlistNode;
		OnAdd(playlistNode)
	}
	
	private function OnAvaible(eventObject) {
		trace("OnAvaible")
		var bAvaible:Boolean = eventObject.avaible;
		var err:String = eventObject.errorDesk;
		btnCreate.enabled = bAvaible; 
		lblError.text = err;
	}
	

	//region Exit event
	private function OnExit() {
		var eventObject = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
	
	
	private function OnAdd(node:XMLNode,bAddNew:Boolean) {
		var playlistNode = node;
		var bNew:Boolean = bAddNew;
		var eventObject = {type:"add", target:this, playlistNode:playlistNode,bNew:bNew};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnAddHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("add", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
	
}