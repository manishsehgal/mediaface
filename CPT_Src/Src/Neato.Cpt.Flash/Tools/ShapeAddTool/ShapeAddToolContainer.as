﻿import mx.utils.Delegate;
[Event("upload")]
[Event("add")]
class Tools.ShapeAddTool.ShapeAddToolContainer extends mx.core.UIComponent {
	static var symbolName:String = "ShapeAddToolContainer";
	static var symbolOwner:Object = Tools.ShapeAddTool.ShapeAddToolContainer;
	var className:String = "ShapeAddToolContainer";

	private var boundingBox_mc:MovieClip;
	private var ctlShapeType;
	private var lblAddShapeText;
	private var lblAddShapeHintText;
	private var btnCancel/*:MenuButton*/;
	

	private var xGap:Number = 4;
	private var yGap:Number = 4;

	function ShapeAddToolContainer() {
		super();
	}

	function init():Void {
		super.init();

		if (boundingBox_mc != undefined) {
			boundingBox_mc._width = 0;
			boundingBox_mc._height = 0;
			boundingBox_mc._visible = false;
		}
	}

	function createChildren():Void {
		super.createChildren();
		if (ctlShapeType == undefined) {
			ctlShapeType = this.createObject("HList", "ctlShapeType", this.getNextHighestDepth());
		}
		/*if (btnCancel == undefined) {
			btnCancel = this.createObject("MenuButton", "btnCancel", this.getNextHighestDepth());
		}*/
	}

	private function onLoad() {
		lblAddShapeText.setStyle("styleName", "ToolPropertiesCaption");
		lblAddShapeHintText.setStyle("styleName", "TransferPropertiesText");
		
		ctlShapeType.RegisterOnChangeHandler(this, ctlShapeType_OnChange);
		
		this.btnCancel.setStyle("styleName", "ToolPropertiesActiveButton");
		btnCancel.RegisterOnClickHandler(this,	btnCancel_OnClick);

		DataBind();
		InitLocale();
	}

    private function InitLocale() {
		_global.LocalHelper.LocalizeInstance(lblAddShapeText, "ToolProperties", "IDS_LBLADDSHAPETEXT");
		_global.LocalHelper.LocalizeInstance(lblAddShapeHintText, "ToolProperties", "IDS_LBLADDSHAPEHINTTEXT");
		//_global.LocalHelper.LocalizeInstance(btnCancel, "ToolProperties", "IDS_BTNCANCEL");
		
		TooltipHelper.SetTooltip(btnCancel, "ToolProperties", "IDS_TOOLTIP_CANCEL");
    }

	public var DataSource:Array;

	public function DataBind() {
		trace("ShapeAddTool DataBind");
		ctlShapeType.DataSource = DataSource;
		ctlShapeType.ItemLinkageName = "DrawingHolder";
		ctlShapeType.DataBind();
	}
	//region Actions

	private function ctlShapeType_OnChange(eventObject) {
		if (!isNaN(eventObject.index)) {
			OnAdd(eventObject.index);
		}
	}
	//endregion

	//region add Shape event
	private function OnAdd(index) {
		var shapeType = DataSource[index].name;
		var eventObject = {type:"add", target:this, shapeType:shapeType };
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnAddHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("add", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
	
	private function btnCancel_OnClick(eventObject) {
		OnExit();
	}
	
	
	//region Exit event
	private function OnExit() {
		var eventObject = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
}
