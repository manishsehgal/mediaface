﻿import mx.utils.Delegate;
[Event("upload")]
[Event("exit")]
[Event("add")]
class Tools.AddImageTool.AddImageToolContainer extends mx.core.UIComponent {
	static var symbolName:String = "AddImageToolContainer";
	static var symbolOwner:Object = Tools.AddImageTool.AddImageToolContainer;
	var className:String = "AddImageToolContainer";

	private var boundingBox_mc:MovieClip;
	private var ctlLibrary:Tools.AddImageTool.LibraryContent;
	private var ctlUpload:Tools.AddImageTool.UploadImageContent;

	private var lblAddImageText:mx.controls.Label;
	private var btnShowLibrary:Controls.Button;
	private var btnShowUpload:Controls.Button;

	private var xGap:Number = 4;
	private var yGap:Number = 4;

	function AddImageToolContainer() {
		super();
	}

	function init():Void {
		super.init();

		if (boundingBox_mc != undefined) {
			boundingBox_mc._width = 0;
			boundingBox_mc._height = 0;
			boundingBox_mc._visible = false;
		}
	}

	function createChildren():Void {
		super.createChildren();
	}

	private function onLoad() {
		lblAddImageText.setStyle("styleName", "ToolPropertiesText");
		lblAddImageText.labelField.multiline = true;
		lblAddImageText.labelField.autoSize = true;
		lblAddImageText.labelField.wordWrap = true;

		ctlLibrary.RegisterOnAddHandler(this, ctlLibrary_OnAdd);
		ctlLibrary.RegisterOnChangeHandler(this, ctlLibrary_OnChange);
		ctlUpload.RegisterOnUploadHandler(this, ctlUpload_OnUpload);
		
		btnShowLibrary.addEventListener("click", Delegate.create(this, btnShowLibrary_OnClick));
		btnShowUpload.addEventListener("click", Delegate.create(this, btnShowUpload_OnClick));
		ctlLibrary._visible = true;
		ctlUpload._visible = false;

		DataBind();
		InitLocale();
	}

    private function InitLocale() {
		//_global.LocalHelper.LocalizeInstance(ctlLibrary, "ToolProperties", "IDS_LIBRARYHEADER");
		_global.LocalHelper.LocalizeInstance(lblAddImageText, "ToolProperties", "IDS_LBLADDIMAGETEXT");
		lblAddImageText.labelField.autoSize = true;
    }

	public var LibraryDataSource:XML;
	public var LibraryIconUrlFormat:String;

	public function DataBind() {
		LibraryDataSource.ignoreWhite = true;
		ctlLibrary.DataSource = LibraryDataSource;
		ctlLibrary.ImageIconUrlFormat = LibraryIconUrlFormat;
		ctlLibrary.DataBind();

		imageSelectedInLibrary = undefined;

		btnShowUpload.value = ctlUpload._visible;
		btnShowLibrary.value = ctlLibrary._visible;
	}
	//region Actions

	private var imageSelectedInLibrary;

	private function ctlLibrary_OnAdd(eventObject) {
		imageSelectedInLibrary = eventObject.id;
		OnAdd();
	}

	private function ctlLibrary_OnChange(eventObject) {
		imageSelectedInLibrary = eventObject.id;
	}

	private function ctlUpload_OnUpload(eventObject) {
		OnUpload();
	}

	private function btnShowUpload_OnClick(eventObject) {
		ctlLibrary._visible = true;
		btnShowLibrary.value = true;
		ctlUpload._visible = false;
		btnShowUpload.value = false;
		OnUpload();
	}

	private function btnShowLibrary_OnClick(eventObject) {
		ctlLibrary._visible = true;
		btnShowLibrary.value = true;
		ctlUpload._visible = false;
		btnShowUpload.value = false;
	}
	//endregion


	//region uploadImage event
	private function OnUpload() {
		var eventObject = {type:"upload", target:this};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnUploadHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("upload", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
	
	//region Exit event
	private function OnExit() {
		var eventObject = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion

	//region addImage event
	private function OnAdd() {
		var id = imageSelectedInLibrary;
		var eventObject = {type:"add", target:this, id:id };
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnAddHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("add", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
}
