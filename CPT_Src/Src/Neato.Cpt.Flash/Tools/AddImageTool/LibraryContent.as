﻿import mx.utils.Delegate;
import mx.skins.RectBorder;
import mx.managers.DepthManager;
[Event("add")]
[Event("change")]
class Tools.AddImageTool.LibraryContent extends mx.core.View {
    static var symbolName:String = "LibraryContent";
    static var symbolOwner:Object = Object(Tools.AddImageTool.LibraryContent);
    var className:String = "LibraryContent";

    private var boundingBox_mc:MovieClip;
	private var border_mc:RectBorder;
	private var createClassChildAtDepth:Function;

    private var btnAdd:Controls.Button;
	private var ctlImageList:Controls.HList;
    private var ctlFolders:Controls.Tree;

    function LibraryContent() {
    }

    function init():Void {
        super.init();

        if (boundingBox_mc != undefined) {
            boundingBox_mc._width = 0;
            boundingBox_mc._height = 0;
            boundingBox_mc._visible = false;
        }
    }

    function createChildren():Void {
        super.createChildren();
		if (border_mc == undefined)
			border_mc = createClassChildAtDepth(_global.styles.rectBorderClass, DepthManager.kBottom, {styleName : this});
    }

    private var xGap:Number = 4;
    private var yGap:Number = 4;

    function size():Void {
        super.size();
		border_mc.setSize(width, height);
		border_mc.move(0, 0);
        ctlFolders.invalidate();
        ctlImageList.invalidate();
    }

    private function onLoad() {
        setStyle("styleName", "ToolPropertiesSubPanel");
		btnAdd.setStyle("styleName", "ToolPropertiesActiveButtonSmall");
		
        ctlImageList.RegisterOnChangeHandler(this, ctlImageList_OnChange);
        ctlImageList.RegisterOnApplyHandler(this, ctlImageList_OnApply);
        ctlFolders.RegisterOnChangeHandler(this, ctlFolders_OnChange);
        ctlFolders.RegisterOnNodeCloseHandler(this, ctlFolders_OnNodeClose);
		btnAdd.RegisterOnClickHandler(this, btnAdd_OnClick);

		//_global.LocalHelper.LocalizeInstance(btnAdd, "ToolProperties", "IDS_BTNADDIMAGE");
		TooltipHelper.SetTooltip(btnAdd, "ToolProperties", "IDS_TOOLTIP_ADDIMAGE");

        DataBind();
    }

    //region Data Binding
    public var DataSource:XML;
    public var ImageIconUrlFormat:String;

    public function DataBind() {
        var folderData:XML = new XML("<node></node>");
        folderData.ignoreWhite = true;
        CreateTreeData(folderData.firstChild, DataSource.firstChild, folderData);

        ctlFolders.dataProvider = folderData.firstChild;
        ctlFolders.setIsOpen(ctlFolders.getTreeNodeAt(0), true);
        ctlFolders.selectedNode = ctlFolders.getTreeNodeAt(0);

        var ctlTempItem = this.createObject("ImageHolderTrueUpSkin", "ctlTempItem", this.getNextHighestDepth());
        ctlImageList.ItemHeight = ctlTempItem.height;
        ctlImageList.ItemWidth = ctlTempItem.width;
        this.destroyObject("ctlTempItem");

        var node:XMLNode = FindFolder(DataSource.firstChild, ctlFolders.selectedNode.attributes.data);
        ctlImageList.DataSource = CreateImageListData(node);
		ctlImageList.ItemLinkageName = "ImageHolder";
        ctlImageList.DataBind();
		ChangeAddButtonActivity();
    }
    //endregion Data Binding

    // TODO: move to entity (ImageLib)
    private function CreateTreeData(targetNode:XMLNode, sourceNode:XMLNode, doc:XML) {
        for (var node:XMLNode = sourceNode.firstChild; node != null; node = node.nextSibling) {
            if (node.nodeName == "folder") {
            	var child:XMLNode = doc.createElement("node");
            	child.attributes.label = node.attributes.name;
            	child.attributes.data = node.attributes.id;
            	child.attributes.isBranch = "true";
            	targetNode.appendChild(child);
                CreateTreeData(child, node, doc);
            }
        }
    }

    // TODO: move to entity (ImageLib)
    private function FindFolder(parentNode:XMLNode, id):XMLNode {
        for (var node:XMLNode = parentNode.firstChild; node != null; node = node.nextSibling) {
            if (node.nodeName == "folder" && node.attributes["id"] == id) {
                return node;
            } else {
                var child:XMLNode = FindFolder(node, id);
                if (child != undefined) {
                    return child;
                }
            }
        }
    }

    // TODO: move to entity (ImageLib)
    private function CreateImageListData(node:XMLNode):Array {
        var itemData:Array = new Array();
        for (var child:XMLNode = node.firstChild; child != null; child = child.nextSibling) {
            if (child.nodeName == "item") {
                itemData.push({name:child.attributes["name"], id:child.attributes["id"], url:ImageIconUrlFormat + child.attributes["id"] });
            }
        }
        return itemData;
    }

    private function ctlImageList_OnChange(eventObject) {
        OnChange(eventObject.index);
    }

    private function ctlImageList_OnApply(eventObject) {
        OnChange(eventObject.index);
        OnAdd(eventObject.index);
    }

    private function ctlFolders_OnChange(eventObject) {
		for (var siblingNode:XMLNode = ctlFolders.selectedNode.parentNode.firstChild; siblingNode != null; siblingNode = siblingNode.nextSibling) {
			ctlFolders.setIsOpen(siblingNode, false);
		}
		ctlFolders.setIsOpen(ctlFolders.selectedNode, true);
		for (var child:XMLNode = ctlFolders.selectedNode.firstChild; child != null; child = child.nextSibling) {
			ctlFolders.setIsOpen(child, false);
		}
        var node:XMLNode = FindFolder(DataSource.firstChild, ctlFolders.selectedNode.attributes.data);
        ctlImageList.DataSource = CreateImageListData(node);
		ctlImageList.ItemLinkageName = "ImageHolder"
        ctlImageList.DataBind();
    }

	private function ctlFolders_OnNodeClose(eventObject) {
		if (ctlFolders.selectedNode == undefined) {
			if (eventObject.node.parentNode != ctlFolders.getTreeNodeAt(0).parentNode) {
				ctlFolders.selectedNode = eventObject.node.parentNode;
			} else {
				ctlFolders.selectedNode = eventObject.node;
			}
			ctlFolders_OnChange(eventObject);
		}
    }
	
    //region Add Image event
	private function btnAdd_OnClick(eventObject) {
		OnAdd(ctlImageList.SelectedItemIndex);
	}

    private function OnAdd(index) {
        var eventObject = {type:"add", target:this, id:ctlImageList.DataSource[index].id  };
        this.dispatchEvent(eventObject);
    };

    public function RegisterOnAddHandler(scopeObject:Object, callBackFunction:Function) {
        this.addEventListener("add", Delegate.create(scopeObject, callBackFunction));
    }
    //endregion

    //region change event
    private function OnChange(index) {
        var eventObject = {type:"change", target:this, id:ctlImageList.DataSource[index].id };
        this.dispatchEvent(eventObject);
		ChangeAddButtonActivity();
    }

    public function RegisterOnChangeHandler(scopeObject:Object, callBackFunction:Function) {
        this.addEventListener("change", Delegate.create(scopeObject, callBackFunction));
    }

    //endregion

	private function ChangeAddButtonActivity() {
		btnAdd.enabled = ctlImageList.SelectedItemIndex != undefined;
	}
}