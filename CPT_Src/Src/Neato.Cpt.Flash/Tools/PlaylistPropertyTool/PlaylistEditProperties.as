﻿import mx.utils.Delegate;
import mx.core.UIObject;
import mx.controls.*;
import Tools.PlaylistPropertyTool.*;

[Event("exit")]
[Event("reinit")]
class Tools.PlaylistPropertyTool.PlaylistEditProperties extends UIObject {
    private var lblPlaylistEdit:Label;
    private var btnDelete:MenuButton;
    private var lblFont:Label;
    private var lblSize:Label;
    private var cbFont:ComboBox;
    private var cbFontSize:ComboBox;
    private static var fontSizes:Array = [4,8,9,10,12,14,16,18,20,24,28,32,36,40,44,48,54,60,66,72,80,88,96,106,117,127];
    private var oldFontSize:Number;
    private var chkBold:CheckBox;
    private var chkItalic:CheckBox;
    private var lblColumns:Label;
    private var columnNames:Array = ["Number", "Artist", "Song", "Time"];
    private var lstColumns:List;
    private var btnUp:MenuButton;
    private var btnDown:MenuButton;
    private var btnHide:MenuButton;
	private var btnImport:MenuButton;
    private var pnlColumnText:PlaylistColumnText;
    private var pnlColumnProps:PlaylistColumnProperties;
    private var columnEditMode:String;
    
    function PlaylistEditProperties() {
        this.lblPlaylistEdit.setStyle("styleName", "ToolPropertiesCaption");
        this.btnDelete.setStyle("styleName", "ToolPropertiesActiveButton");
        this.cbFont.setStyle("styleName", "ToolPropertiesCombo");
        this.cbFontSize.setStyle("styleName", "ToolPropertiesCombo");
        this.chkBold.setStyle("styleName", "ToolPropertiesCheckBoxBold");
        this.chkItalic.setStyle("styleName", "ToolPropertiesCheckBox");
        this.lstColumns.setStyle("styleName", "ToolPropertiesCombo");
        this.btnUp.setStyle("styleName", "ToolPropertiesActiveButton");
        this.btnDown.setStyle("styleName", "ToolPropertiesActiveButton");
        this.btnHide.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnImport.setStyle("styleName", "ToolPropertiesActiveButton");
        
        this.pnlColumnText.btnEditColProps.setStyle("styleName", "ToolPropertiesActiveButton");
        this.pnlColumnText.txtText.setStyle("styleName", "ToolPropertiesInputText");
        
        this.pnlColumnProps.btnEditColText.setStyle("styleName", "ToolPropertiesActiveButton");
        this.pnlColumnProps.btnLeft.setStyle("styleName", "ToolPropertiesCheckBox");
        this.pnlColumnProps.btnCenter.setStyle("styleName", "ToolPropertiesCheckBox");
        this.pnlColumnProps.btnRight.setStyle("styleName", "ToolPropertiesCheckBox");
    }
    
    function onLoad() {
        InitLocale();
        
        btnDelete.RegisterOnClickHandler(this, _global.DeleteCurrentUnit);
        btnImport.RegisterOnClickHandler(this, OnReinit);
        var FontsArray:Array = _global.Fonts.GetFontsArray();
		cbFont.dropdown.cellRenderer = "FontCellRenderer";
        for (var i in FontsArray) {
            this.cbFont.addItem(FontsArray[i]);
        }
        this.cbFont.sortItems(upperCaseFunc);
        this.cbFont.selectedIndex = 0;
        this.cbFont.addEventListener("change", Delegate.create(this, OnChangeFont));
        
        for (var i in fontSizes) {
            this.cbFontSize.addItem(fontSizes[i]);
        }
        this.cbFontSize.sortItems(numberFunc);
        this.cbFontSize.selectedIndex = 0;
        this.cbFontSize.textField.restrict="0-9";
        this.cbFontSize.textField.maxChars=3;
        this.oldFontSize = 4;
        this.cbFontSize.addEventListener("change", Delegate.create(this, OnChangeFontSize));
        this.cbFontSize.textField.addEventListener("enter", Delegate.create(this, ChangeFontSize));
        
        this.chkBold.addEventListener("click", Delegate.create(this, OnBold));
        this.chkItalic.addEventListener("click", Delegate.create(this, OnItalic));
        
        this.lstColumns.addItem("Number", 0);
        this.lstColumns.addItem("Artist", 1);
        this.lstColumns.addItem("Song",   2);
        this.lstColumns.addItem("Time",   3);
        this.lstColumns.selectedIndex = 0;
        this.lstColumns.vScrollPolicy = "auto";
        this.lstColumns.addEventListener("change", Delegate.create(this, OnChangeColumn));
        
        this.btnUp.RegisterOnClickHandler(this, OnColumnUp);
        this.btnDown.RegisterOnClickHandler(this, OnColumnDown);
        this.btnHide.RegisterOnClickHandler(this, OnColumnHide);
        
        this.pnlColumnText.btnEditColProps.RegisterOnClickHandler(this, OnEditColProps);
        this.pnlColumnText.txtText.addEventListener("change", Delegate.create(this, OnChangeText));

        
        this.pnlColumnProps.btnEditColText.RegisterOnClickHandler(this, OnEditColText);
        this.pnlColumnProps.colorTool.RegisterOnSelectHandler(this, OnColor);
        this.pnlColumnProps.btnLeft.addEventListener("click", Delegate.create(this, OnAlign));
        this.pnlColumnProps.btnCenter.addEventListener("click", Delegate.create(this, OnAlign));
        this.pnlColumnProps.btnRight.addEventListener("click", Delegate.create(this, OnAlign));
        
        pnlColumnText._visible = true;
        pnlColumnProps._visible = false;
        columnEditMode = "Text";
        
        //DataBind();
    }
    
    private function InitLocale() {
        _global.LocalHelper.LocalizeInstance(btnDelete, "ToolProperties", "IDS_BTNDELETE");
        _global.LocalHelper.LocalizeInstance(lblFont, "ToolProperties", "IDS_LBLFONT");
        _global.LocalHelper.LocalizeInstance(lblSize, "ToolProperties", "IDS_LBLSIZE");
        _global.LocalHelper.LocalizeInstance(chkBold, "ToolProperties", "IDS_CHKBOLD");
        _global.LocalHelper.LocalizeInstance(chkItalic, "ToolProperties", "IDS_CHKITALIC");
        _global.LocalHelper.LocalizeInstance(lblColumns, "ToolProperties", "IDS_LBL_COLUMNS");
        _global.LocalHelper.LocalizeInstance(btnUp, "ToolProperties", "IDS_BTN_COLUMN_UP");
        _global.LocalHelper.LocalizeInstance(btnDown, "ToolProperties", "IDS_BTN_COLUMN_DOWN");
        _global.LocalHelper.LocalizeInstance(btnHide, "ToolProperties", "IDS_BTN_HIDE");
        
        _global.LocalHelper.LocalizeInstance(this.pnlColumnText.btnEditColProps, "ToolProperties", "IDS_BTN_EDIT_COLUMN_PROPS");
        _global.LocalHelper.LocalizeInstance(this.pnlColumnProps.btnEditColText, "ToolProperties", "IDS_BTN_EDIT_COLUMN_TEXT");
    }
    
    static function upperCaseFunc(a, b) {
        return a.label.toUpperCase() > b.label.toUpperCase();
    }
    static function numberFunc(a, b) {
        return parseInt(a.label) > parseInt(b.label);
    }
    
    function DataBind() {
        for (var i = 0; i < this.cbFont.length; ++i) {
            if (this.cbFont.getItemAt(i).label.toLowerCase() == _global.CurrentUnit.Font) {
                this.cbFont.selectedIndex = i;
                break;
            }
        }
        
        RefreshFontSize();
        
        this.chkBold.selected   = _global.CurrentUnit.Bold;
        this.chkItalic.selected = _global.CurrentUnit.Italic;
		if (_global.CurrentUnit.EmptyLayoutItem) {
			_global.CurrentUnit.ReinitTable(null,false,true);
			_global.CurrentUnit.EmptyLayoutItem = false;
		}
		
		for (var i=0; i<4; i++) {
			var order = _global.CurrentUnit.GetColumnOrder(i);
			this.lstColumns.getItemAt(order).label = columnNames[i];
			this.lstColumns.getItemAt(order).data = i;
		}
		//this.lstColumns.redraw();
        
        var selInd = this.lstColumns.selectedIndex;
		this.lstColumns.selectedIndex = selInd;
        if (selInd == undefined) selInd = 0;
        var id = this.lstColumns.getItemAt(selInd).data;
        DataBindColumn(id);
    }
    
    function DataBindColumn(id) {
    	var isVisible = _global.CurrentUnit.GetColumnVisible(id);
    	this.btnHide.enabled = _global.CurrentUnit.IsCanHideAnyOne() || !isVisible;
        _global.LocalHelper.LocalizeInstance(this.btnHide, "ToolProperties", isVisible ? "IDS_BTN_HIDE" : "IDS_BTN_UNHIDE");
        this.btnHide.setStyle("styleName", "ToolPropertiesActiveButton");

        this.pnlColumnText.txtText.text = _global.CurrentUnit.GetColumnText(id);

        var selectedColor = _global.CurrentUnit.GetColumnColor(id);
        if (selectedColor == undefined) selectedColor = 0;
        this.pnlColumnProps.colorTool.SetColorState(selectedColor);
        
        if (_global.CurrentUnit.GetColumnAlign(id) == "right") {
            this.pnlColumnProps.btnRight.selected = true;
        } else if (_global.CurrentUnit.Align == "center") {
            this.pnlColumnProps.btnCenter.selected = true;
        } else {
            this.pnlColumnProps.btnLeft.selected = true;
        }
    }
    
    function RefreshFontSize() {
        oldFontSize = _global.CurrentUnit.Size;
        for (var i = 0; i < this.cbFontSize.length; ++i) {
            if (this.cbFontSize.getItemAt(i).label == oldFontSize) {
                this.cbFontSize.selectedIndex = i;
                break;
            }
        }
        this.cbFontSize.text = oldFontSize.toString();
    }
    
    function OnChangeFont(eventObject) {
        _global.CurrentUnit.Font = eventObject.target.selectedItem.label;
    }
    
    function OnChangeFontSize(eventObject) {
        //if (cbFontSize.textField.getFocus() != null)
        //    return;
        ChangeFontSize(eventObject);
    }
    
    function ChangeFontSize(eventObject) {
        var newSize = parseInt(eventObject.target.text);
        if (newSize > 3 && newSize < 128) {
            oldFontSize = newSize;
            _global.CurrentUnit.Size = newSize;
        } else {
            cbFontSize.text = oldFontSize.toString();
        }
    }
    
    function OnBold(eventObject) {
        _global.CurrentUnit.Bold = eventObject.target.selected;
    }
    
    function OnItalic(eventObject) {
        _global.CurrentUnit.Italic = eventObject.target.selected;
    }
    
    function OnChangeColumn(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined) return;
        var id = this.lstColumns.getItemAt(selInd).data;
        DataBindColumn(id);
    }
    
    function OnColumnUp(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined || selInd == 0) return;
        var tmpLabel = this.lstColumns.getItemAt(selInd).label;
        var tmpData = this.lstColumns.getItemAt(selInd).data;
        this.lstColumns.getItemAt(selInd).label = this.lstColumns.getItemAt(selInd-1).label;
        this.lstColumns.getItemAt(selInd).data = this.lstColumns.getItemAt(selInd-1).data;
        this.lstColumns.getItemAt(selInd-1).label = tmpLabel;
        this.lstColumns.getItemAt(selInd-1).data = tmpData;
        this.lstColumns.selectedIndex = selInd-1;
        _global.CurrentUnit.SetColumnOrder(tmpData, selInd-1);
    }
    
    function OnColumnDown(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined || selInd == this.lstColumns.length-1) return;
        var tmpLabel = this.lstColumns.getItemAt(selInd).label;
        var tmpData = this.lstColumns.getItemAt(selInd).data;
        this.lstColumns.getItemAt(selInd).label = this.lstColumns.getItemAt(selInd+1).label;
        this.lstColumns.getItemAt(selInd).data = this.lstColumns.getItemAt(selInd+1).data;
        this.lstColumns.getItemAt(selInd+1).label = tmpLabel;
        this.lstColumns.getItemAt(selInd+1).data = tmpData;
        this.lstColumns.selectedIndex = selInd+1;
        _global.CurrentUnit.SetColumnOrder(tmpData, selInd+1);
    }
    
    function OnColumnHide(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined) return;
        var id = this.lstColumns.getItemAt(selInd).data;
        var vis = _global.CurrentUnit.GetColumnVisible(id);
        _global.CurrentUnit.SetColumnVisible(id, !vis);
        _global.LocalHelper.LocalizeInstance(this.btnHide, "ToolProperties", !vis ? "IDS_BTN_HIDE" : "IDS_BTN_UNHIDE");
        this.btnHide.setStyle("styleName", "ToolPropertiesActiveButton");
    }
    
    function OnEditColProps(eventObject) {
        this.pnlColumnText._visible = false;
        this.pnlColumnProps._visible = true;
        this.columnEditMode = "Props";
    }
    
    function OnChangeText(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined) return;
        var id = this.lstColumns.getItemAt(selInd).data;
        _global.CurrentUnit.SetColumnText(id, eventObject.target.text);
    }
    
    function OnEditColText(eventObject) {
        pnlColumnText._visible = true;
        pnlColumnProps._visible = false;
        columnEditMode = "Text";
    }
    
    function OnColor(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined) return;
        var id = this.lstColumns.getItemAt(selInd).data;
        _global.CurrentUnit.SetColumnColor(id, eventObject.color);
    }
    
    function OnAlign(eventObject) {
        var selInd = this.lstColumns.selectedIndex;
        if (selInd == undefined) return;
        var id = this.lstColumns.getItemAt(selInd).data;
        _global.CurrentUnit.SetColumnAlign(id, eventObject.target.data);
    }
    
    private function OnReinit() {
        var eventObject = {type:"reinit", target:this};
        this.dispatchEvent(eventObject);
    };

    public function RegisterOnReinitHandler(scopeObject:Object, callBackFunction:Function) {
        this.addEventListener("reinit", Delegate.create(scopeObject, callBackFunction));
    }
    
/*    
     private function chkNoFill_OnClick(eventObject)    {
         _global.CurrentUnit.SetFill(fillColor, chkNoFill.selected);
       }

    private function chkKeepProportions_OnClick(eventObject)    {
        _global.CurrentUnit.KeepProportions = chkKeepProportions.selected;
        RefreshLineWeight();
    }
    
    private function btnRestoreProportions_OnClick(eventObject) {
        //    _global.CurrentUnit.ScaleX = _global.CurrentUnit.InitialScale;
        //_global.CurrentUnit.ScaleY = _global.CurrentUnit.InitialScale;
        _global.CurrentUnit.RestoreProportions();
        RefreshLineWeight();
        _global.SelectionFrame.AttachUnit(_global.CurrentUnit);
    }
    
    private function nsLineWeight_OnChange(eventObject)    {
        trace("nsLineWeight_OnChange");
        trace(eventObject.target.value);
        _global.CurrentUnit.SetBorderWidth(eventObject.target.value)
        RefreshLineWeight();
        _global.SelectionFrame.AttachUnit(_global.CurrentUnit);
    }
    private function nsLineWeight_OnFocusOut(eventObject)    {
        trace ("nsLineWeight_OnFocusOut");
        trace(eventObject.target.value);
        
        var newVal = parseFloat(eventObject.target.inputField.label.value);
        var maxBorderWidth = _global.CurrentUnit.GetMaxBorderWidth();

        if(maxBorderWidth > newVal)
            _global.CurrentUnit.SetBorderWidth(newVal);
        else
            _global.CurrentUnit.SetBorderWidth(maxBorderWidth);
        RefreshLineWeight();
        _global.SelectionFrame.AttachUnit(_global.CurrentUnit);
    }

    public function DataBind():Void {
        chkKeepProportions.selected = _global.CurrentUnit.KeepProportions;
        chkKeepProportions.enabled = _global.CurrentUnit.IsNonScalable ? false : true;
        chkNoFill.selected = _global.CurrentUnit.Transparency;
        fillColor=_global.CurrentUnit.GetFillColor();
        lineColor=_global.CurrentUnit.GetBorderColor();
        RefreshLineWeight();
        fillColorTool.SetColorState(fillColor);
        lineColorTool.SetColorState(lineColor);
        nsLineWeight.inputField.maxChars = 5;
    }
*/
/*    
    private function fillColorTool_OnSelect(eventObject) {
        fillColor = eventObject.color;
        _global.CurrentUnit.SetFill(fillColor, chkNoFill.selected);

    }
    
    private function lineColorTool_OnSelect(eventObject) {
        lineColor = eventObject.color;
        _global.CurrentUnit.SetBorderColor(lineColor);
    }
    function RefreshLineWeight(){
        trace("RefreshLineWeight");
        nsLineWeight.maximum=_global.CurrentUnit.GetMaxBorderWidth();
        nsLineWeight.value=_global.CurrentUnit.GetBorderWidth();
        nsLineWeight.stepSize=0.5;
        trace("nsLineWeight.maximum="+nsLineWeight.maximum);
        trace("nsLineWeight.value="+nsLineWeight.value);
    }
    
    private function btnContinue_OnClick() {
        OnExit();
    }
    
    
    //region Exit event
    private function OnExit() {
        var eventObject = {type:"exit", target:this};
        this.dispatchEvent(eventObject);
    };

    public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
        this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
    //endregion
*/    
}
