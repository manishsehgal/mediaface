﻿import mx.core.UIObject;

[Event("exit")]
class Tools.PlaylistPropertyTool.PlaylistColumnProperties extends UIObject {
	var btnEditColText:MenuButton;
	var colorTool:Tools.ColorSelectionTool.ColorTool;
	var btnLeft:MenuButton;
	var btnCenter:MenuButton;
	var btnRight:MenuButton;
}
