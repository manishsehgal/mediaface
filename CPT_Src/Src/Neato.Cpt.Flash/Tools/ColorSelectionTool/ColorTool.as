﻿import mx.utils.Delegate;
[Event("select")]
class Tools.ColorSelectionTool.ColorTool extends mx.core.UIComponent {
    static var symbolName:String = "ColorTool";
    static var symbolOwner:Object = Object(Tools.ColorSelectionTool.ColorTool);
    var className:String = "ColorTool";

	//private var btnChangePanel;
	private var colorPicker;
	private var colorSelector;
	private var rgb:Number;
	private var captionSimpleColors:String;
	private var captionMoreColors:String;

	private function onLoad(){
		colorPicker._visible = true;
		colorSelector._visible = false;
		
		/*_global.LocalHelper.LocalizeInstance(btnChangePanel, "ToolProperties", "IDS_LBLSIMPLECOLORS");
		captionSimpleColors = btnChangePanel.text;
		_global.LocalHelper.LocalizeInstance(btnChangePanel, "ToolProperties", "IDS_LBLMORECOLORS");
		captionMoreColors = btnChangePanel.text;
		btnChangePanel.setStyle("styleName", "ToolPropertiesActiveButtonSmall");
		
		TooltipHelper.SetTooltip(btnChangePanel, "ToolProperties", "IDS_TOOLTIP_CHANGECOLORS");
		
		btnChangePanel.onPress = function() {
			var flag = _parent.colorPicker._visible;
			_parent.colorPicker._visible = !flag;
			_parent.colorSelector._visible = flag;
			this.text = (flag)
			? _parent.captionSimpleColors
			: _parent.captionMoreColors;
					
			if (flag) _parent.colorSelector.SetColorState(_parent.rgb);
			else _parent.colorPicker.SetColorState(_parent.rgb);
			this.setStyle("styleName", "ToolPropertiesActiveButtonSmall");
		}
		*/
	}
	
		
	public function colorPanels_OnChange() {		
			var flag = _parent._parent.colorPicker._visible;
			
			_parent._parent.colorPicker._visible = !flag;
			_parent._parent.colorSelector._visible = flag;
					
			if (flag) _parent._parent. colorSelector.SetColorState(_parent._parent.rgb);
			else _parent._parent.colorPicker.SetColorState(_parent._parent.rgb);
	}
	
    public function RegisterOnSelectHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("select", Delegate.create(scopeObject, callBackFunction));
		colorPicker.RegisterOnSelectHandler(this, colorTool_OnSelect);
		colorSelector.RegisterOnSelectHandler(this, colorTool_OnSelect);
    }
	
	function OnControlClick(){
		var eventObject = {type:"click", target:this};
		this.dispatchEvent(eventObject);
	}
	public function RegisterOnClickHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("click", Delegate.create(scopeObject, callBackFunction));
		colorSelector.RegisterOnClickHandler(this, OnControlClick);
		colorPicker.RegisterOnClickHandler(this, OnControlClick);
    }
	
	private function colorTool_OnSelect(eventObject) {
		rgb = eventObject.color;
		this.dispatchEvent(eventObject);
	}
	
	public function SetColorState(selectedColor:Number) {
		rgb = selectedColor;
		colorPicker.SetColorState(selectedColor);
		colorSelector.SetColorState(selectedColor);
	}
		
	public function get RGB():Number {
		return rgb;
	}
}