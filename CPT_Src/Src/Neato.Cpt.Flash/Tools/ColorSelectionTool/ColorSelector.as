﻿import mx.utils.Delegate;
import mx.controls.Label;
[Event("select")]
class Tools.ColorSelectionTool.ColorSelector extends mx.core.UIComponent {
    static var symbolName:String = "ColorSelector";
    static var symbolOwner:Object = Object(Tools.ColorSelectionTool.ColorSelector);
    var className:String = "ColorSelector";

	private var picker:MovieClip;
	private var palette:MovieClip;
	private var slider:MovieClip;
	private var slideBar:MovieClip;
	private var lblBrightness:Label;
	private var btnSimpleColors;

	private var pickerColor:Color;
	private var paletteColor:Color;
	private var slideBarPanelColor:Color;
	private var h, s, b:Number;
	
	function onLoad() {
		_global.LocalHelper.LocalizeInstance(lblBrightness, "ToolProperties", "IDS_LBLBRIGHTNESS");
		TooltipHelper.SetTooltip(btnSimpleColors, "ToolProperties", "IDS_TOOLTIP_CHANGECOLORS");
	}
	
	function ColorSelector() {
		pickerColor = new Color(picker);
		paletteColor = new Color(palette);
		slideBarPanelColor = new Color(slideBar.panelColor);
		
		palette.onPress = function() {
			this._parent.h = 359 * this._parent._xmouse / this._width;
			this._parent.s = 100 * this._parent._ymouse / this._height;
			this._parent.picker._x = this._parent._xmouse;
			this._parent.picker._y = this._parent._ymouse;
			this._parent.ChangeColor();
			this._parent.AttachPicker();
			this._parent.OnClick();
		}
		
		picker.onPress = function() {
			this._parent.AttachPicker();
			this._parent.OnClick();
		}
		
		slideBar.onPress = function() {
			this._parent.b = 100 - 100 * (this._parent._xmouse - this._x) / this._width;
			this._parent.slider._x = this._parent._xmouse;
			this._parent.ChangeColor();
			this._parent.AttachSlider();
			this._parent.OnClick();
		}
		
		slider.onPress = function() {
			this._parent.AttachSlider();
			this._parent.OnClick();
		}
		
		btnSimpleColors.onRelease = _parent.colorPanels_OnChange;
	}
	
	private function AttachSlider() {
		Mouse.addListener(slider);
		
		slider.onMouseMove = function() {
			var xm = this._parent._xmouse;
			var sliderX = this._parent.slideBar._x;
			var sliderW = this._parent.slideBar._width;
			
			this._x = xm;
			if (xm < sliderX) this._x = sliderX;
			if (xm > sliderX + sliderW) this._x = sliderX + sliderW;
			
			this._parent.b = 100 - 100 * (this._x - sliderX) / sliderW;
			this._parent.ChangeColor();
		}
	
		slider.onMouseUp = function() {
			this.onMouseMove = null;
			this.onMouseUp = null;
			Mouse.removeListener(this);
		}
	}
	
	private function AttachPicker() {
		Mouse.addListener(picker);
		
		picker.onMouseMove = function() {
			this._x = this._parent._xmouse;
			this._y = this._parent._ymouse;
			if (this._parent._xmouse < 0) this._x = 0;
			if (this._parent._ymouse < 0) this._y = 0;
			if (this._parent._xmouse > this._parent.palette._width) this._x = this._parent.palette._width;
			if (this._parent._ymouse > this._parent.palette._height) this._y = this._parent.palette._height;
			this._parent.h = 359 * this._x / this._parent.palette._width;
			this._parent.s = 100 * this._y / this._parent.palette._height;
			this._parent.ChangeColor();
		}
	
		picker.onMouseUp = function() {
			this.onMouseMove = null;
			this.onMouseUp = null;
			Mouse.removeListener(this);
		}
	}

	// Methods for color transform (HSB<>RGB) - BEGIN
	private function HSBtoRGB(H, S, Br) {
		var R, G, B;
		// Нормируем Br, так чтобы значение яркости изменялось в диапазоне от 0 до 255
		Br = Br/100 * 255;
		// При нулевой насыщенности - ахроматический цвет (оттенок серого). Все цветовые составляющие имеют одинаковое значение, определяемое яркостью
		if (S==0){
			R=G=B=Br;
		}
		// Если насыщенность отлична от нуля - хроматический цвет
		else {
			// Вычисляем долю 60-градусного сектора цветового круга, отделяющую заданный тон от границы этого сектора
			var resH = H/60 - Math.floor(H/60);
			//trace(H + "  " + H/60 + "  "+ "res " + resH);
			 // Определяем минимальное значение цветовой составляющей
			var bot=Br*(1-S/100);
			 // Определяем промежуточное значение цветовой составляющей на убывающей части графика
			var dec=Br*(1-(S*resH)/100);
			 // Определяем промежуточное значение цветовой составляющей на возрастающей части графика
			var inc=Br*(1-(S*(1-resH)/100));
			// В зависимости от принадлежности заданного тона к определенному сектору цветового круга, устанавливаем значения цветовых составляющих в соответствии с графиком
			switch(Math.floor(H/60)){
			  case 0: // Красный цвет
				R = Br; 
				G = inc;
				B = bot;
				break;
			  case 1: // Желтый цвет
				R = dec; 
				G = Br; 
				B = bot;
				break;
			  case 2: // Зеленый цвет
				R = bot; 
				G = Br; 
				B = inc;
				break;
			  case 3: // Голубой цвет
				R = bot; 
				G = dec; 
				B = Br;
				break;
			  case 4: // Синий цвет
				R = inc; 
				G = bot;
				B = Br;
				break;
			  case 5: // Пурпурный цвет
				R = Br; 
				G = bot;
				B = dec;
				break;
			}
		}
		// Возвращаем шестнадцатеричное значение цвета
		return Math.round(R)<<16|Math.round(G)<<8|Math.round(B);
	}

	private function RGBtoHSB (rgb:Number):Array{
		var H, S, Br;
		// Выделяем цветовые составляющие
		var R=(rgb&0xff0000)>>16; 
		var G=(rgb&0xff00)>>8;
		var B=(rgb&0xff);
		// Яркость определяется как максимальная интенсивность составляющей (от 0 до 255)
		Br=max(R, G, B);
		// Определяем минимальное значение интенсивности составляющей в цвете
		var minVal=min(R, G, B); 
		// Диапазон значений, соответствующих насыщенности цвета
		var delta=Br-minVal;
		// Если интенсивность каждой составляющей равна нулю, получаем черный цвет, насыщенность которого равна нулю
		if (Br==0){
		S=0;
		}
		// При ненулевой яркости насыщенность определяется как отношение разности максимальной и минимальной интенсивности составляющих цвета к его яркости (от 0 до 255), умноженное на 100
		else {
		S=delta / Br * 100;
		}
		// При нулевой насыщенности - ахроматический цвет, поэтому параметру Hue присвоим значение 0 (строго говоря тон неопределен)
		if (S==0)H=0;
		// При ненулевой насыщенности - хроматический цвет. Определяем тон как угол(в градусах) на цветовом круге
		else {
		// Если красная составляющая имеет максимальную интенсивность, то цветовой тон лежит между пурпурным и желтым
		if (R==Br){
		  H=60*(G-B)/delta; 
		}
		else{
			// Если зеленая составляющая имеет максимальную интенсивность, то цветовой тон лежит между голубым и желтым
		  if (G==Br){
			H=120+60*(B-R)/delta;
			}	
			// Если синяя составляющая имеет максимальную интенсивность, то цветовой тон лежит между пурпурным и голубым
			  else {
				H=240+60*(R-G)/delta;
					}	
				}
			} 
		// Если тон получился отрицательным, добавляем полный оборот для получения положительного значения
		  if (H<0) H += 360;
		// Возвращаем массив с полученными значенями Hue, Saturation и Brightness
		  return ([Math.round(H), Math.round(S), Math.round(Br/255*100)]);
	}
	
	//Функция возвращает максимальное из переданных ей значений
	function max():Number {
		var max = -Infinity;
		for (var i = 0; i < arguments.length; i++) {
			max = Math.max(max, arguments[i]);
		}
		return max;
	}

	//Функция возвращает минимальное из переданных ей значений
	function min():Number {
		var min = Infinity;
		for (var i = 0; i < arguments.length; i++) {
			min = Math.min(min, arguments[i]);
		}
		return min;
	}

	// Methods for color transform (HSB<>RGB) - END

	// Working methods
	
    private function OnSelect(color) {
        var eventObject = {type:"select", target:this, color:color };
        this.dispatchEvent(eventObject);
    };

    public function RegisterOnSelectHandler(scopeObject:Object, callBackFunction:Function) {
        this.addEventListener("select", Delegate.create(scopeObject, callBackFunction));
    }
	
	private function OnClick() {
		trace("ColorSelector on click");
        var eventObject = {type:"click", target:this};
        this.dispatchEvent(eventObject);
    };

    public function RegisterOnClickHandler(scopeObject:Object, callBackFunction:Function) {
        this.addEventListener("click", Delegate.create(scopeObject, callBackFunction));
    }
	
	private function ChangeColor() {
		RedrawControls();
		OnSelect(HSBtoRGB(h, s, b));
	}
	
	private function RedrawControls() {
		slideBarPanelColor.setRGB(HSBtoRGB(h, s, 100));
		paletteColor.setTransform
			({ra: b, rb: '0', ga: b, gb: '0', ba: b, bb: '0', aa: '100', ab: '0'});
		var oppositeHue = (h < 180)? 180 + h: h - 180;
		pickerColor.setRGB(HSBtoRGB(oppositeHue, 100, 100));
	}
	
	public function SetColorState(selectedColor:Number) {
		var hsb:Array = RGBtoHSB(selectedColor);
		h = hsb[0]; s = hsb[1]; b = hsb[2];
		
		var x =  h * palette._width / 359;
		var y =  s * palette._height / 100;
		var z =  slideBar._width * (100 - b) / 100;
		
		picker._x = x; picker._y = y; slider._x = z + slideBar._x; 
		RedrawControls();
	}
}
