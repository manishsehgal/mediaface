﻿class FrameEx extends Frame {
	
	public var frameBorder:FrameBorderEx;
	private var checkPointHandlers:Array;
	private var effect;
	private var checkHandler0:CheckPointHandler;
	function FrameEx() {
		super.Frame();
		
		checkPointHandlers = new Array();
		checkPointHandlers.push(checkHandler0);
		//effect=_global.Effects.GetEffect("BottomArc");//to refactor
		checkHandler0.id = 0;
		checkHandler0.RegisterOnUpdateHandler(this,OnHandlerUpdate);
		
	}
	function onLoad() {
		trace("FrameEx:OnLoad");
		checkHandler0.id = 0;
		checkHandler0.RegisterOnUpdateHandler(this,OnHandlerUpdate);
		checkHandler0.RegisterOnFinishDragHandler(this,OnUpdate);
	}
	function AttachUnit(mu:MoveableUnit) {
		effect = _global.CurrentUnit.getEffect();
		trace("Effect "+effect.name);
		/*if(effect == null) {
			super.AttachUnit(mu);
			return;
		}*/
		trace("Attach [Frame To] Unit: " + mu["mc"]);
		unit = mu;
		super.AttachUnit(mu);
		_visible = true;

		
		_x = unit.X;
		_y = unit.Y;
		_rotation = unit.Angle;
		saveAngle = NaN;
		frameBorder._rotation = rotateHandler._rotation = resizeHandler._rotation = 0;
		frameBorder._x = 0;
		frameBorder._y = 0;
		frameBorder.Draw(unit);
		var cnt:Number =0;
		checkPointsCountUpdate(effect.refPoints.length);
		trace("Effect + "+effect);
		trace("Effectref + "+effect.refPoints);
		for(var i=0 ;i<effect.refPoints.length ;i++) {
			checkPointHandlers[i]._visible = true;
			checkPointHandlers[i]._x = effect.refPoints[i].x*ScaleX/100;
			checkPointHandlers[i]._y = effect.refPoints[i].y*ScaleY/100;
			checkPointHandlers[i].id = effect.refPoints[i].id;
		}
		
		
		var fbUpperLeftX = frameBorder.getBounds(this).xMin;
		var fbUpperLeftY = frameBorder.getBounds(this).yMin;
		rotateHandler._x = fbUpperLeftX;
		rotateHandler._y = fbUpperLeftY;
		resizeHandler._x = fbUpperLeftX + frameBorder._width;
		resizeHandler._y = fbUpperLeftY + frameBorder._height;
		
				
	}
	
	function DetachUnit() {
		_visible = false;
		unit = undefined;
	}
	private function OnUpdate(){
		_global.CurrentUnit.Draw();
	}
	private function OnHandlerUpdate(eventObject) {
		effect.refPoints[eventObject.id].x = eventObject.X / ScaleX * 100;
		effect.refPoints[eventObject.id].y = eventObject.Y / ScaleY * 100;
		AttachUnit(unit);
		
	}
	public function set HandlersScale(value:Number):Void {
		rotateHandler._xscale = rotateHandler._yscale = 
		resizeHandler._xscale = resizeHandler._yscale = 
		frameBorder._xscale = frameBorder._yscale = value;
	}
	private function checkPointsCountUpdate(num:Number) {
		var len:Number = checkPointHandlers.length;
		for(var i = 0; i < len; i++) {
			checkPointHandlers[i]._visible = false;
		}
		if(num <= len)
			return;
		else {
			
			for( var i:Number = 0; i < num - len; i++) {
				var id:Number = checkPointHandlers.length;
				var res:MovieClip = this.attachMovie("CheckPointHandler","checkHand"+id, this.getNextHighestDepth());
				res.RegisterOnUpdateHandler(this,OnHandlerUpdate);
				res.RegisterOnFinishDragHandler(this,OnUpdate);
				checkPointHandlers.push(res);
			}
		}
	}
}