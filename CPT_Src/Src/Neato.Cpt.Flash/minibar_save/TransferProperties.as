﻿import mx.core.UIObject;
import mx.controls.*;
import mx.utils.Delegate;
import Controls.*;

[Event("exit")]
class TransferProperties extends UIObject {
	private var lblTransfer:Label;
	private var lblNote:TextArea;
	private var lblSelect:TextArea;
	private var lblClick:TextArea;
	private var btnCancel:MenuButton;
	private var buttonDrawHelper:ButtonDrawHelper;
	private var miniPreviewWidth:Number = 150;
	private var miniPreviewHeight:Number = 140;
	private var miniPreviewYPos:Number = 215;
	var ctlMiniPreview:MiniPreview;

	function TransferProperties() {
		ToolPropertiesContainer.GetInstance().TransferPropertiesClip.setStyle("styleName", "TransferProperties");
		this.lblTransfer.setStyle("styleName", "ToolPropertiesCaption");
		this.lblNote.setStyle("styleName", "ToolPropertiesNote");
		this.lblSelect.setStyle("styleName", "TransferPropertiesText");
		this.lblClick.setStyle("styleName", "TransferPropertiesText");
		this.btnCancel.setStyle("styleName", "ToolPropertiesActiveButton");
		buttonDrawHelper = new ButtonDrawHelper();
	}

	function onLoad() {
		btnCancel.RegisterOnClickHandler(this, btnCancel_OnClick);
		_global.LocalHelper.LocalizeInstance(btnCancel, "ToolProperties", "IDS_BTNCANCEL");
	}

	function ctlMiniPreview_OnUpdate() {
		ctlMiniPreview.RegenerateView();
	}

	function Show():Void {
		var clip:MovieClip = createEmptyMovieClip("clip", 20);
		ctlMiniPreview = new MiniPreview(clip);
		ctlMiniPreview.fillNoncurrent = 0xF7C938;
		ctlMiniPreview.fillCurrent = 0x0F0F0F;
		_global.UpdateMiniPreviews();
		clip._y = miniPreviewYPos;
		ctlMiniPreview.UpdateSize(miniPreviewWidth, miniPreviewHeight);
		clip._x = 20;//(/*_parent._width*/212-clip._width)/2;
		trace("Clip widht" + this._width + " " + clip._width + " " + clip._x);
		var parent = this;
		var count:Number = ctlMiniPreview.GetCount();
		for (var i = 0; i < count; i++) {
			var mcButt:MovieClip = ctlMiniPreview.GetButtonByNum(i);
			mcButt.FaceNum = i;
			trace("Minibar:mcButt" + i + mcButt);
			var face:FaceUnit = _global.Faces[i];
			if (face.Id != _global.CurrentFace.Id) {
				mcButt.onRelease = function() {
					var destinationFace = _global.Faces[this.FaceNum];
					_global.CurrentFace.Hide();
					var faceXml:XMLNode = new XMLNode(1, "Face");
					var fakeNode:XMLNode = new XMLNode(1, "fakeNode");
					faceXml.appendChild(fakeNode);
					_global.CurrentFace.AddUnitNodes(faceXml);
					destinationFace.Clear();
					destinationFace.ParseXMLUnits(faceXml);
					destinationFace.Draw();
					destinationFace.Show();
					destinationFace.ChangeUnitCoordinatesUnderTransfer(_global.CurrentFace.ScaleFactor);
					_global.CurrentFace = destinationFace;
					//MiniBar.getInstance().NormalizeState();
					_global.UpdateMiniPreviews();
					parent.OnExit();
				};
			}
		}
		/*
		var buttonFaceWidth = 40;
		var gap = ( this.width - (_global.Faces.length - 1) * buttonFaceWidth )
		/ _global.Faces.length;
		
		buttonDrawHelper.Initialize(clip, gap, 215, gap);
		var parent=this;
		for (var i = 0; i < _global.Faces.length; i++) {
			var face:FaceUnit = _global.Faces[i];
			if (face.Id != _global.CurrentFace.Id) { 
				var btn:MovieClip = buttonDrawHelper.CreateOneButton(face);         
				btn.index = i;
				
				btn.onRelease = function () {
					var destinationFace = _global.Faces[this.index];

					_global.CurrentFace.Hide();                 
					var faceXml:XMLNode = new XMLNode(1, "Face");
					var fakeNode:XMLNode = new XMLNode(1, "fakeNode");
					faceXml.appendChild(fakeNode);
					
					_global.CurrentFace.AddUnitNodes(faceXml);
					
					destinationFace.Clear();
					destinationFace.ParseXMLUnits(faceXml);
					destinationFace.Draw();                 
					destinationFace.Show();
					destinationFace.ChangeUnitCoordinatesUnderTransfer
					(_global.CurrentFace.ScaleFactor);                  
					
					_global.CurrentFace = destinationFace;                  
					//MiniBar.getInstance().NormalizeState();
					parent.OnExit();
				}
			}
		}
		*/
	}

	private function btnCancel_OnClick() {
		OnExit();
	}

	//region Exit event
	private function OnExit() {
		var eventObject = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	}
	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
	}
	//endregion
}
