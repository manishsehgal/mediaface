﻿import mx.core.UIObject;
import Controls.*;
class MiniBar extends UIObject {
	private static var instance;
	//private static btnMiniBar;
	private var btnClear:UIObject;
	private var btnTransfer:UIObject;
	private var mcPreview:MovieClip;
	var ctlMiniPreview:MiniPreview;

	private var miniPreviewWidth:Number = 150;
	private var miniPreviewHeight:Number = 140;
	private var currentFace:Number = 0;
	private var thisWidth:Number = 212;

	function MiniBar() {
		_root.pnlMainLayer.content.pnlMinibar.setStyle("styleName", "MiniBarPanel");
		this.btnTransfer.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnClear.setStyle("styleName", "ToolPropertiesActiveButton");
		instance = this;
		//buttonDrawHelper = new ButtonDrawHelper();
		this.btnClear.onRelease = function() {
			trace("btnClear");
			_parent.CommandClear();
		};
		this.btnTransfer.onRelease = function() {
			trace("btnTransfer");
			_parent.CommandTransfer();
		};
	}

	static function getInstance():MiniBar {
		return instance;
	}

	function CreateMiniPreview() {
		/*ctlMiniPreview=new MiniPreview(miniPreviewWidth,miniPreviewHeight)*/
		trace(_parent._parent);
		mcPreview = this.createEmptyMovieClip("mcPreview", 0);
		ctlMiniPreview = new MiniPreview(mcPreview);
		ctlMiniPreview.fillCurrent = 0xF7C938;
		ctlMiniPreview.fillNoncurrent = 0x4F4F4F;
		_global.UpdateMiniPreviews();
		//mcPreview._x = 0;//temporary
		mcPreview._y = 2;
		ctlMiniPreview.UpdateSize(miniPreviewWidth, miniPreviewHeight);
		mcPreview._x = (_parent._parent._width - mcPreview._width - 16) / 2;
		var parent = this;
		trace(mcPreview);
		trace(mcPreview._x);
		trace(mcPreview._y);
		trace(mcPreview._width);
		trace(mcPreview._height);
		trace(mcPreview._xscale);
		trace(mcPreview._yscale);
		trace(_parent._parent);
		trace(_parent._parent._x);
		trace(_parent._parent._y);
		trace(_parent._parent._width);
		trace(_parent._parent._height);
		trace(this._x);
		trace(_parent._x);
		var count:Number = ctlMiniPreview.GetCount();
		for (var i = 0; i < count; i++) {
			var mcButt:MovieClip = ctlMiniPreview.GetButtonByNum(i);
			mcButt.FaceNum = i;
			trace("Minibar:mcButt" + i + mcButt);
			mcButt.onRelease = function() {
				var destinationFace = _global.Faces[this.FaceNum];
				Logic.DesignerLogic.UnselectCurrentUnit();
				_global.CurrentFace.Hide();
				_global.CurrentFace = destinationFace;
				destinationFace.Show();
				_global.UpdateMiniPreviews();
			};
		}
		trace("Minibar:CreateMiniPreview");
	}

	function CommandClear() {
		trace("CommandClear");
		ToolPropertiesContainer.GetInstance().ShowClearFaceProperties();
	}

	function CommandTransfer() {
		trace("CommandTransfer");
		ToolPropertiesContainer.GetInstance().ShowTransferProperties();
	}

	function onLoad() {
		_global.projectXml.onLoad += CreateMiniPreview();
		_global.LocalHelper.LocalizeInstance(this.btnClear, "Minibar", "IDS_BTNCLEAR");
		_global.LocalHelper.LocalizeInstance(this.btnTransfer, "Minibar", "IDS_BTNTRANSFER");
	}
}
