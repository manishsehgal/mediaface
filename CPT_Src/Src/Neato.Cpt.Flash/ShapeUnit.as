﻿class ShapeUnit extends MoveableUnit {
	private var shapeType:String;
	private var borderWidth:Number = 1;
	private var borderColor:Number = 0;
	private var fillColor:Number = 0x0066FF;
	private var transparency:Boolean = false;
	private var fillType:String;

	public var currentWidth:Number=100;
	public var currentHeight:Number=100;
	
	private var isNonScalable:Boolean=false;
	private var width:Number=100;
	private var height:Number=100;
	private var maxSize:Number=100;
	
	private var shape:Object;
	
	function ShapeUnit(mc:MovieClip, node:XML, shapeType:String, size:Number) {
		super(mc, node);
		this.shapeType = shapeType;
		shape = _global.Shapes.GetShape(this.shapeType);
		transparency = shape.transparency;
		
		if (shape.width > shape.height) {
			currentWidth = size;
			currentHeight = shape.height / (shape.width / currentWidth);
		} else {
			currentHeight = size;
			currentWidth = shape.width / (shape.height / currentHeight);
		}
		maxSize = size;
		
		if(!isNaN(shape.fillColor))
			fillColor = shape.fillColor;
		if(!isNaN(shape.borderColor))
			borderColor = shape.borderColor;
		if(!isNaN(shape.borderWidth))
			borderWidth = shape.borderWidth;
			
		if (node != null) {
			this.shapeType = node.attributes.type;
			currentWidth = Number(node.attributes.currentWidth);
			currentHeight = Number(node.attributes.currentHeight);
			if (node.attributes.maxSize != undefined) {
				maxSize = Number(node.attributes.maxSize);
			}
			for (var i = 0; i < node.childNodes.length; ++i) {
				var childNode:XML = node.childNodes[i];
				switch(childNode.nodeName) {
					case "Fill":
						fillType = new String(childNode.attributes.type)
						fillColor = parseInt(childNode.attributes.color.substr(1), 16);
						transparency = childNode.attributes.transparency.toString() == "true" ? true : false;						
					break;
					case "Border":
						borderWidth=Number(childNode.attributes.width);
						borderColor=parseInt(childNode.attributes.color.substr(1), 16);
					break;
					case "Type":
						shape = _global.Shapes.CreateShapeFromXml(childNode);
					default:
					break;
				}
			}
		}
		width = Number(shape.width);
		height = Number(shape.height);
		isNonScalable = shape.isNonScalable;
		KeepProportions = true;
	}

	function GetXmlNode():XMLNode {
		var node:XMLNode = super.GetXmlNode();
		node.nodeName = "Shape";
		node.attributes.type=shapeType;
		node.attributes.currentWidth=currentWidth;
		node.attributes.currentHeight=currentHeight;
		node.attributes.maxSize=maxSize;

		node.appendChild(shape.originalXmlNode.cloneNode(true));
		
		var fillNode:XMLNode = new XMLNode(1, "Fill");
		fillNode.attributes.type=fillType;
		fillNode.attributes.color=_global.GetColorForXML(fillColor);
		fillNode.attributes.transparency=transparency;
		node.appendChild(fillNode);
		
		var borderNode:XMLNode = new XMLNode(1, "Border");
		borderNode.attributes.width=borderWidth;
		borderNode.attributes.color=_global.GetColorForXML(borderColor);
		node.appendChild(borderNode);
		
		node.appendChild(super.GetXmlNodeRotate());
		return node;
	}
	function Draw():Void {
		trace ("ShapeUnit:Draw");
		DrawMC(mc);
	}
	function DrawMC(mc:MovieClip):Void {
		mc.clear();
		if (transparency) {
			mc.beginFill(0, 0);
		} else {
			mc.beginFill(fillColor, 100);
		  }
		
		mc.lineStyle(borderWidth > 0 ? borderWidth : 0.01, borderColor, borderWidth > 0 ? 100 : 0);
		for (var i = 0; i < shape.drawSequence.length; ++i) {
			var primitive:IDraw = shape.drawSequence[i];
			primitive.Draw(mc, 0, 0, 0, currentWidth / width , currentHeight / height);
		}
		mc.endFill();
		
		var bounds = mc.getBounds();
		var dX = -bounds.xMin / (currentWidth  / width);
		var dY = -bounds.yMin / (currentHeight / height);
		
		mc.clear();
		if (transparency) {
			mc.beginFill(0, 0);
		} else {
			mc.beginFill(fillColor, 100);
		}
		mc.lineStyle(borderWidth > 0 ? borderWidth : 0.01, borderColor, borderWidth > 0 ? 100 : 0);
		for (var i = 0; i < shape.drawSequence.length; ++i) {
			var primitive:IDraw = shape.drawSequence[i];
			primitive.Draw(mc, dX, dY, 0, currentWidth / width , currentHeight / height);
		}
		mc.endFill();
	}
	function getMode():String {
		trace ("ShapeUnit:GetMode");
		return _global.ShapeEditMode;
	}
	
	function SetBorderColor(color:Number){
		borderColor=color;
		Draw();
	}
	function GetBorderColor():Number{
		return borderColor;
	}
	function SetBorderWidth(width:Number){
		borderWidth=width;
		Draw();
	}
	function GetBorderWidth():Number{
		return Math.min(borderWidth,GetMaxBorderWidth());
	}
	function SetFill(color:Number, transparencyFill:Boolean){
		fillColor=color;
		transparency = transparencyFill;
		Draw();
	}
	function GetFillColor():Number{
		return fillColor;
	}
	function get Transparency():Boolean{
		return transparency;
	}
	function set IsNonScalable(value:Boolean):Void {
		isNonScalable = value;
	}
	function get IsNonScalable():Boolean {
		return isNonScalable;
	}
	function RestoreProportions():Void	{
		//currentWidth = width;
		//currentHeight = height;
		if (width > height) {
			currentWidth = maxSize;
			currentHeight = height / (width / currentWidth);
		} else {
			currentHeight = maxSize;
			currentWidth = width / (height / currentHeight);
		}
		Draw();
	}
	function GetMaxBorderWidth():Number {
		return Math.floor(Math.min(currentWidth,currentHeight)/2);
	}

	private function Resize(kx:Number, ky:Number) {
		var borderWidth:Number = GetBorderWidth();

		var newWidth:Number = currentWidth * kx;
		var newHeight:Number = currentHeight * ky;

		if (newHeight <= borderWidth * 2 || newWidth <= borderWidth * 2) {
			newWidth = currentWidth;
			newHeight = currentHeight;
		}

		this.ScaleX = 100;
		this.ScaleY = 100;
	
		this.currentWidth = newWidth;
		this.currentHeight = newHeight;
		this.Draw();
		ToolPropertiesContainer.GetInstance().ShapeEditPropertiesClip.RefreshLineWeight();
	}
}
