﻿
import mx.core.UIObject;

class FaceTabSticker extends UIObject {
	
	private var btnNavigatorClosed : MovieClip;
	private var btnNavigatorOpened : MovieClip;
	private var mcNavigatorPanel   : MovieClip;
	
	function FaceTabSticker() {
	}
	
	function onLoad() {
		mcNavigatorPanel._visible = false;
		btnNavigatorClosed._visible = true;
		btnNavigatorOpened._visible = false;
		btnNavigatorClosed.onRelease =  function() {
			this._parent.onSelect();
		}
		btnNavigatorOpened.onRelease =  function() {
			this._parent.onSelect();
		}
	}
	
	function onSelect() {
		if (mcNavigatorPanel._visible == true) {
			mcNavigatorPanel._visible = false;
			btnNavigatorClosed._visible = true;
			btnNavigatorOpened._visible = false;
		} else {
			btnNavigatorClosed._visible = false;
			btnNavigatorOpened._visible = true;
			mcNavigatorPanel._visible = true;
			mcNavigatorPanel.DataBind();
		}
	}
}
