﻿import mx.controls.Label;
class PreviewMenu extends CommonMenu {
	private var btnBack:Button;
	private var lblModelName:Label;
	private static var instance;

	function PreviewMenu() {
		trace("PreviewMenu");
		instance = this;
		Preloader.Show();

		_parent._parent.setStyle("styleName", "MenuPanelPreview");
		lblModelName.setStyle("styleName", "MenuPanelModelName");

		this.btnBack.onRelease = GotoDesigner;
	}
	
	static function getInstance() : PreviewMenu {
		return instance;
	}
	
	function onLoad() {
		_global.LocalHelper.LocalizeInstance(this.btnBack, "PreviewMenu", "IDS_BTNBACK");
		_global.LocalHelper.LocalizeInstance(this.btnPrint, "PreviewMenu", "IDS_BTNPRINT");
		_global.LocalHelper.LocalizeInstance(this.btnSave, "PreviewMenu", "IDS_BTNSAVE");
		
		TooltipHelper.SetTooltip(this.btnBack, "PreviewMenu", "IDS_TOOLTIP_BACK");
		TooltipHelper.SetTooltip(this.btnPrint, "PreviewMenu", "IDS_TOOLTIP_PRINT");
		TooltipHelper.SetTooltip(this.btnSave, "PreviewMenu", "IDS_TOOLTIP_SAVE");
		
		lblModelName.text = _global.ModelName;
		
		Preloader.Hide();
	}
	
	static function GotoDesigner() {
		_root.pnlPreviewLayer.content.Hide();
		Header.getInstance().SetActiveStep(2);
		var count = _global.Faces.length;
		for (var i = 0; i < count; ++i) {
			var face:FaceUnit = _global.Faces[i];
			face.RedrawPaintUnits();
		}
	}
	
	function Update() {
		lblModelName.text = _global.ModelName;
	}
}