﻿_global.Mode;
_global.TextMode="Text", _global.FillMode="Fill",
_global.PlaylistMode="Playlist", _global.PlaylistEditMode="PlaylistEdit",
_global.GradientMode="Gradient", _global.GradientEditMode="GradientEdit",
_global.TransferMode="Transfer", _global.ClearFaceMode="ClearFace",
_global.ImageMode="Image", _global.ImageEditMode="ImageEdit",
_global.ShapeMode="Shape", _global.ShapeEditMode="ShapeEdit", 
_global.PaintMode="Paint", _global.PaintEditMode="PaintEdit", 
_global.ChooseLayoutMode="ChooseLayoutMode",_global.InactiveMode="InactiveMode",
_global.TextEffectMode = "TextEffect";

_global.ThumbailAutofit = "Thubail";
_global.HorizontalAutofit = "Horizontal";
_global.VerticalAutofit = "Vertical";
_global.FitToFaceAutofit = "Fill";
_global.NoneAutofit = "None";

this.pnlPreviewLayer._visible = false;

/*var eventObject:Object = {type:"complete"};
this.pnlMainLayer.dispatchEvent(eventObject);
this.pnlPreviewLayer.dispatchEvent(eventObject);
*/

_global.Fonts = new FontHelper(_global.fontsXml);
_global.Brushes = new PaintHelper(_global.brushesXml);
_global.Shapes = new ShapeHelper(_global.shapesXml);
_global.Effects = new TextEffectHelper(_global.effectsXml);

_global.EditLeft = _root.pnlMainLayer.content.WorkSpace._x;
_global.EditTop = _root.pnlMainLayer.content.WorkSpace._y;
_global.EditWidth = _root.pnlMainLayer.content.WorkSpace._width;
_global.EditHeight = _root.pnlMainLayer.content.WorkSpace._height;

_global.ReadFaces(_root.pnlMainLayer.content.WorkSpace, _global.projectXml);
//BERK: !!! Now images loading through Project.ProjectXml
//_global.ReadImages(_global.imagesXml);

SASession.Initialize();

_global.keyListener = new Object();
_global.keyListener.onKeyDown = function() {
	if (eval(Selection.getFocus()) instanceof TextField)
		return;
		
	switch (Key.getCode()) {
		case Key.LEFT :
			_global.SelectionFrame.X -= 1;
			break;
		case Key.UP :
			_global.SelectionFrame.Y -= 1;
			break;
		case Key.RIGHT :
			_global.SelectionFrame.X += 1;
			break;
		case Key.DOWN :
			_global.SelectionFrame.Y += 1;
			break;
	};
	if (Key.isDown(Key.DELETEKEY)) {
		_global.DeleteCurrentUnit();
	}
};
Key.addListener(_global.keyListener);


//----------Calibration----------
_global.SetXCalibration = function(value:Number) {
	_global.xcalibration = value;
	_global.IsCalibrationSpecified = true;
	}
_global.SetYCalibration = function(value:Number) {
	_global.ycalibration = value;
	_global.IsCalibrationSpecified = true;
	}
//----------Calibration----------