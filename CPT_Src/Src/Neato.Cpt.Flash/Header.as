﻿import mx.controls.Label;
import mx.core.UIObject;

class Header extends UIObject {
	private var mcLogo:MovieClip;
	private var mcStep1:Step;
	private var mcStep2:Step;
	private var mcStep3:Step;
	private static var instance:Header;
	
	function Header() {
		instance = this;
		Preloader.Show();
		
		mcLogo.onPress = function() {
			getURL("default.aspx", "_self");
		}
	}
	
	static function getInstance() : Header {
		return instance;
	}
	
	private function onLoad() {
		var currentDate:Date = new Date();
		mcLogo.logoContainer.contentPath = "FlashLogo.aspx?time="+currentDate.getTime();
		mcLogo.logoContainer.refreshPane();
		
		InitLocale();
		Preloader.Hide();
		
		SetActiveStep(2);
		
		mcStep1.RegisterOnPressHandler(this, mcStep1_Press);
		mcStep2.RegisterOnPressHandler(this, mcStep2_Press);
		mcStep3.RegisterOnPressHandler(this, mcStep3_Press);
		mcStep3.RegisterOnActivePressHandler(this, mcStep3_ActivePress);
	}
	
	private function InitLocale() {
		mcStep1.SetText(_global.LocalHelper.GetString("Header", "IDS_STEP1"));
		mcStep2.SetText(_global.LocalHelper.GetString("Header", "IDS_STEP2"));
		mcStep3.SetText(_global.LocalHelper.GetString("Header", "IDS_STEP3"));
	}

	private function mcStep1_Press(eventObject) {
		SetActiveStep(1);
		getURL("default.aspx", "_self");
	}
	
	private function mcStep2_Press(eventObject) {
		PreviewMenu.GotoDesigner();
	}
	
	private function mcStep3_Press(eventObject) {
		DesignerMenu.GotoPreview();
		SetActiveStep(3);
	}
	
	public function SetActiveStep(step:Number) {
		mcStep1.SetActive(false);
		mcStep2.SetActive(false);
		mcStep3.SetActive(false);
		
		if (step == 1)
			mcStep1.SetActive(true);
		else if (step == 2)
			mcStep2.SetActive(true);
		else if (step == 3)
			mcStep3.SetActive(true);
	}
	
	public function mcStep3_ActivePress(eventObject) {
		PrintHelper.Print();
	}
}