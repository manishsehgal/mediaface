﻿import mx.controls.Button;
import mx.utils.Delegate;
class MenuButton extends Button {
	static var symbolName:String = "MenuButton";
	static var symbolOwner = MenuButton;

	var falseUpSkin:String = "MenuButtonFalseUpSkin";
	var falseDownSkin:String = "MenuButtonFalseDownSkin";
	var falseOverSkin:String = "MenuButtonFalseOverSkin";
	var falseDisabledSkin:String = "MenuButtonFalseDisabledSkin";
	var textFormat:TextFormat;
	function MenuButton() {
		//textFormat = new TextFormat();
		useHandCursor = true;

		this.labelPath.html = true;
		this.labelPath.multiline = true;
		this.labelPath.autoSize = true;
		
		/*textFormat.align = "center";
		this.labelPath.setNewTextFormat(textFormat);*/

		 
	}
	function SetHTMLStyle(){
		textFormat = this.labelPath.getTextFormat();
		textFormat.align = "center";
		this.labelPath.setTextFormat(textFormat);
	}
	function get text():String {
		return this.labelPath.htmlText;
	}

	function set text(label:String):Void {
		this.labelPath.htmlText = label;
	    
	}
	
	public function RegisterOnClickHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("click", Delegate.create(scopeObject, callBackFunction));
    }
}
