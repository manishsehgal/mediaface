﻿import mx.controls.Button
import mx.utils.Delegate;
[Event("press")]
[Event("rollOver")]
[Event("rollOut")]
class ToolbarButton extends Button {
    static var symbolName:String = "ToolbarButton";
    static var symbolOwner = ToolbarButton;
   
	[Inspectable(defaultValue="ToolbarButtonTrueUpSkin")]
	var trueUpSkin:String = "ToolbarButtonTrueUpSkin";

	[Inspectable(defaultValue="ToolbarButtonTrueOverSkin")]
	var trueOverSkin:String = "ToolbarButtonTrueOverSkin";

	[Inspectable(defaultValue="ToolbarButtonTrueDownSkin")]
	var trueDownSkin:String = "ToolbarButtonTrueDownSkin";

	[Inspectable(defaultValue="ToolbarButtonTrueDisabledSkin")]
	var trueDisabledSkin:String = "ToolbarButtonTrueDisabledSkin";

	[Inspectable(defaultValue="ToolbarButtonFalseUpSkin")]
	var falseUpSkin:String = "ToolbarButtonFalseUpSkin";

	[Inspectable(defaultValue="ToolbarButtonFalseOverSkin")]
	var falseOverSkin:String = "ToolbarButtonFalseOverSkin";

	[Inspectable(defaultValue="ToolbarButtonFalseDownSkin")]
	var falseDownSkin:String = "ToolbarButtonFalseDownSkin";

	[Inspectable(defaultValue="ToolbarButtonFalseDisabledSkin")]
	var falseDisabledSkin:String = "ToolbarButtonFalseDisabledSkin";

	public function ToolbarButton() {
		this.useHandCursor = true;
		this.centerContent = false;
		this.setStyle("styleName", _global.styles.ToolbarButtonUp);
	}
	
	public function RegisterOnRollOverHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("rollOver", Delegate.create(scopeObject, callBackFunction));
    }

	function onRollOver() {
		this.setStyle("styleName", _global.styles.ToolbarButtonOver);
		super.onRollOver.call(this);
		var eventObject = {type:"rollOver", target:this};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnRollOutHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("rollOut", Delegate.create(scopeObject, callBackFunction));
    }

	function onRollOut() {
		this.setStyle("styleName", _global.styles.ToolbarButtonUp);
		super.onRollOut.call(this);
		var eventObject = {type:"rollOut", target:this};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnPressHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("press", Delegate.create(scopeObject, callBackFunction));
    }

	function onPress() {
		super.onPress.call(this);
		var eventObject = {type:"press", target:this};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnReleaseHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("release", Delegate.create(scopeObject, callBackFunction));
    }

	function onRelease() {
		super.onRelease.call(this);
		var eventObject = {type:"release", target:this};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnDragOutHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("dragOut", Delegate.create(scopeObject, callBackFunction));
    }

	function onDragOut() {
		super.onDragOut.call(this);
		var eventObject = {type:"dragOut", target:this};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnClickHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("click", Delegate.create(scopeObject, callBackFunction));
    }
}