﻿class FrameBorder extends MovieClip {

	function Draw(mu):Void {
		var bounds = mu.mc.getBounds(mu.mc);

		var kx = _parent.ScaleX/100;
		var ky = _parent.ScaleY/100;

		var xMin = bounds.xMin*kx * 100 / this._xscale;
		var yMin = bounds.yMin*ky * 100 / this._yscale;
		var xMax = bounds.xMax*kx * 100 / this._xscale;
  		var yMax = bounds.yMax*ky * 100 / this._yscale;
		
		this.clear();
			
		this.moveTo(xMin, yMin);
		var count:Number = 0;
		var step:Number = 3;
		lineStyle(1, 0xFFFFFF*(count%2));
		for(var i = yMin; i < yMax; i += step, ++count) {
			this.lineTo(xMin, i);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		for(var i = xMin; i < xMax; i += step, ++count) {
			this.lineTo(i, yMax);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		for(var i = yMax; i > yMin; i -= step, ++count) {
			this.lineTo(xMax, i);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		for(var i = xMax; i > xMin; i -= step, ++count) {
			this.lineTo(i, yMin);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		this.lineTo(xMin, yMin);
	}
}
