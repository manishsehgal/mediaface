﻿class Paper extends MovieClip {
	private var width:Number;
	private var height:Number;
	private var widthCopy:Number;
	private var heightCopy:Number;

	function Paper() {
		width = 100;
		height = 100;
	}

	function ParseXml(node:XML) {
		if (node != null) {
			width = Number(node.attributes.w);
			height = Number(node.attributes.h);
		}
 		for(var i in this) {
 			if (this[i] instanceof MovieClip)
 				this[i].removeMovieClip();
 		}
	}

	public function get Width():Number {
		return width;
	}

	public function get Height():Number {
		return height;
	}

	function Draw() {
		this._xscale = this._yscale = 100;
		clear();
		
		//draw border
		lineStyle(0);
		beginFill(0xFFFFFF, 100);
		moveTo(0, 0);
		lineTo(width, 0);
		lineTo(width, height);
		lineTo(0, height);
		lineTo(0, 0);
		endFill();
		
		var count = _global.Faces.length;
		for (var i = 0; i < count; ++i) {
			var face:FaceUnit = _global.Faces[i];
			var depth:Number = 2 * i;
			var mcFace:MovieClip = this.createEmptyMovieClip("Face" + i, depth);
			face.DrawMC(mcFace);
			mcFace._x = face.PaperX;
			mcFace._y = face.PaperY;
			var mcFaceMask = this.createEmptyMovieClip("FaceMask" + i, depth + 1);
			face.DrawContour(mcFaceMask, false, false);
			mcFaceMask._x = face.PaperX;
			mcFaceMask._y = face.PaperY;
			mcFace.setMask(mcFaceMask);
			var mcFaceContour= this.createEmptyMovieClip("FaceContour" + i, 2*count + i);
			mcFaceContour.lineStyle(0);
			face.DrawContour(mcFaceContour, true);
			face.DrawCutLines(mcFaceContour);
			mcFaceContour._x = face.PaperX;
			mcFaceContour._y = face.PaperY;
		}
		
		widthCopy = this._width;
 		heightCopy = this._height;
		var mcFaceMask = this.createEmptyMovieClip("FaceMask", 10000);
		mcFaceMask.lineStyle(0);
		mcFaceMask.beginFill(0xFFFFFF);
		mcFaceMask.moveTo(-1, -1);
		mcFaceMask.lineTo(widthCopy+1, -1);
		mcFaceMask.lineTo(widthCopy+1, heightCopy+1);
		mcFaceMask.lineTo(-1, heightCopy+1);
		mcFaceMask.lineTo(-1, -1);
		mcFaceMask.endFill();
		this.setMask(mcFaceMask);
		
		//draw border again
		var mcBorder = this.createEmptyMovieClip("mcBorder", this.getNextHighestDepth());
		mcBorder.lineStyle(0, 0);
		mcBorder.moveTo(0, 0);
		mcBorder.lineTo(width, 0);
		mcBorder.lineTo(width, height);
		mcBorder.lineTo(0, height);
		mcBorder.lineTo(0, 0);
		
		calibrate(_global.xcalibration, _global.ycalibration);
	}

	function calibrate(x:Number, y:Number) {
		var count = _global.Faces.length;
		for (var i = 0; i < count; ++i) {
			var face:FaceUnit = _global.Faces[i];
			var mcFace:MovieClip = this["Face" + i];
			mcFace._x = face.PaperX + x;
			mcFace._y = face.PaperY + y;
			var mcFaceMask = this["FaceMask" + i];
			mcFaceMask._x = face.PaperX + x;
			mcFaceMask._y = face.PaperY + y;
			//Nice feature
			/*var mcFaceContour = this["FaceContour" + i];
			mcFaceContour._x = face.PaperX + x;
			mcFaceContour._y = face.PaperY + y;
			*/
		}
	}
}