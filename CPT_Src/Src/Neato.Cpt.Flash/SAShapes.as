﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;
[Event("onLoaded")]
class SAShapes {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SAShapes.prototype);

	private var types:XML;
	
	public static function GetInstance(types:XML):SAShapes {
		return new SAShapes(types);
	}
	
	public function get IsLoaded():Boolean {
		return types.loaded;
	}
	
	private function SAShapes(types:XML) {
		var parent = this;
		
		this.types = types;
		this.types.onLoad = function(success) {
			var eventObject = {type:"onLoaded", target:parent};
			trace("SAShapes: dispatch types onLoaded");
			parent.dispatchEvent(eventObject);
		};
	}

	public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }

	public function BeginLoad() {
		var currentDate:Date = new Date();
		var url:String = "../ShapeTypes.xml?time=" + currentDate.getTime();
		if (SALocalWork.IsLocalWork)
			url = "../Neato.Cpt.WebDesigner/ShapeTypes.xml";
		trace("SAShapes before load:"+url)
		BrowserHelper.InvokePageScript("log", url);
		
		this.types.load(url);
		trace("SAShapes after load:"+url)
		BrowserHelper.InvokePageScript("log", "SAShapes.as: types loaded");
    }
}