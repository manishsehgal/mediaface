﻿import mx.utils.Delegate;
[Event("PanelChanged")]
class ToolPropertiesContainer extends mx.core.UIComponent {
	static var symbolName:String = "ToolPropertiesContainer";
	static var symbolOwner:Object = ToolPropertiesContainer;
	var className:String = "ToolPropertiesContainer";

	private var boundingBox_mc:MovieClip;
	private var lblToolOptions;
	
	public var TextPropertiesClip:TextProperties;
	
	public var ctlFillProperties:FillProperties;
	
	private var ctlAddImageTool:Tools.AddImageTool.AddImageToolContainer;
	private var ImageEditPropertiesClip:ImageEditProperties;
	
	private var ctlPlaylistProperties:Tools.AddPlaylistTool.PlaylistProperties;
	public var ctlPlaylistEditProperties:Tools.PlaylistPropertyTool.PlaylistEditProperties;
	
	public var TransferPropertiesClip:TransferProperties;
	
	private var ClearFacePropertiesClip:ClearFaceProperties;
	
	private var ctlAddShapeTool:Tools.ShapeAddTool.ShapeAddToolContainer;
	public var ShapeEditPropertiesClip:ShapeEditProperties;
	
	private var PaintEditPropertiesClip:PaintEditProperties;
	private var ctlAddPaintTool:PaintProperties;
	
	private var ctlTextEffect:TextEffectControl;
	private var ChooseLayoutPropertiesClip:ChooseLayoutProperties;

	private static var instance;
	public static function GetInstance():ToolPropertiesContainer {
		return instance;
	}

	function ToolPropertiesContainer() {
		super();
		instance = this;
		imageLibraryXml = new XML();
	}

	function init():Void {
		super.init();

		if (boundingBox_mc != undefined) {
			boundingBox_mc._width = 0;
			boundingBox_mc._height = 0;
			boundingBox_mc._visible = false;
		}
	}

	function createChildren():Void {
		super.createChildren();
	}

	private function onLoad() {
		_parent._parent.setStyle("styleName", "ToolPropertiesPanel");
		lblToolOptions.setStyle("styleName","TitleText");

		ctlAddImageTool.RegisterOnExitHandler(this, HidePanel);
		ctlFillProperties.RegisterOnExitHandler(this, HidePanel);
		
		ImageEditPropertiesClip.RegisterOnExitHandler(this, HidePanel);
		ctlPlaylistProperties.RegisterOnExitHandler(this,HidePanel);
		ClearFacePropertiesClip.RegisterOnExitHandler(this, HidePanel);
		ctlAddShapeTool.RegisterOnExitHandler(this, HidePanel);
		ShapeEditPropertiesClip.RegisterOnExitHandler(this, HidePanel);
		PaintEditPropertiesClip.RegisterOnExitHandler(this, HidePanel);
		ctlAddPaintTool.RegisterOnExitHandler(this, HidePanel);
		ctlAddPaintTool.RegisterOnEndPaintingHandler(this, ctlAddPaintTool_EndPainting);
		ChooseLayoutPropertiesClip.RegisterOnExitHandler(this, HidePanel);
		
		ctlAddImageTool.RegisterOnUploadHandler(this, UploadCustomImage);
		ctlAddImageTool.RegisterOnAddHandler(this, AddImage);
		
		TextPropertiesClip.RegisterOnTextEffectHandler(this, ShowTextEffectProperties);
		ctlPlaylistEditProperties.RegisterOnReinitHandler(this, ShowImportPlaylist);
		ctlTextEffect.RegisterOnEditTextHandler(this, ShowTextProperties);
		
		ctlAddShapeTool.RegisterOnAddHandler(this, AddShape);
		ctlPlaylistProperties.RegisterOnAddHandler(this, AddPlaylist);

		TransferPropertiesClip.RegisterOnExitHandler(this,HidePanel);

		DataBind();
		InitLocale();
	}

    private function InitLocale() {
		_global.LocalHelper.LocalizeInstance(this.lblToolOptions, "ToolProperties", "IDS_LBLTOOLOPTIONS");
    }

	public function DataBind() {
		var currentUnitMode = _global.CurrentUnit.getMode();
		if (currentUnitMode != undefined) {
			_global.Mode = currentUnitMode;
		} else {
			_global.Mode = _global.InactiveMode;
		}
		HideAllProperties();
		ShowUnitProperties();
	}
	
	private function HideAllProperties():Void {
		TextPropertiesClip._visible = false;
		ctlAddImageTool._visible = false;
		ImageEditPropertiesClip._visible = false;
		ctlPlaylistProperties._visible = false;
		ctlPlaylistEditProperties._visible = false;
		TransferPropertiesClip._visible = false;
		ClearFacePropertiesClip._visible = false;
		ctlFillProperties._visible = false;
		ctlAddShapeTool._visible = false;
		ShapeEditPropertiesClip._visible = false;
		PaintEditPropertiesClip._visible = false;
		ctlAddPaintTool._visible = false;
		ChooseLayoutPropertiesClip._visible = false;
		ctlTextEffect._visible = false;
	}
	
	public function HidePanel()	{
		HideAllProperties();
		_global.Mode = _global.InactiveMode;
		if (_global.CurrentUnit != null) Logic.DesignerLogic.UnselectCurrentUnit();
		_global.SelectionFrame.DetachUnit();
		_global.CurrentUnit = null;
		OnPanelChanged();
		ShowFillProperties();
	}
	
	public function ShowTextProperties():Void {
		_global.Mode = _global.TextMode;
		HideAllProperties();
		TextPropertiesClip._visible = true;
		TextPropertiesClip.DataBind();
		OnPanelChanged();
	}
	
	public function ShowFillProperties():Void {
		HideAllProperties();
		_global.Mode = _global.FillMode;
		Logic.DesignerLogic.UnselectCurrentUnit();
		_global.SelectionFrame.DetachUnit();
		ctlFillProperties._visible = true;

		ctlFillProperties.ShowFillProperties();
		OnPanelChanged();
	}
	
	public function ShowPlaylistProperties():Void {
		_global.Mode = _global.PlaylistMode;
		Logic.DesignerLogic.UnselectCurrentUnit();
		_global.Mode = _global.PlaylistMode;
		HideAllProperties();
		ctlPlaylistProperties.ShowPlaylistProperties();
		OnPanelChanged();
	}
	public function ShowImportPlaylist():Void {
		_global.Mode = _global.PlaylistMode;
		HideAllProperties();
		ctlPlaylistProperties.ShowPlaylistProperties();
		OnPanelChanged();
	}
	
	public function ShowPlaylistEditProperties():Void {
		_global.Mode = _global.PlaylistEditMode;
		HideAllProperties();
		
		ctlPlaylistEditProperties._visible = true;
		ctlPlaylistEditProperties.DataBind();
		OnPanelChanged();
	}

	private var imageLibraryXml:XML;
	
	public function ShowImageProperties():Void {
		Logic.DesignerLogic.UnselectCurrentUnit();
		_global.Mode = _global.ImageMode;
		HideAllProperties();
		ctlAddImageTool._visible = true;
		var imageLibariryService:SAImages = SAImages.GetInstance(this.imageLibraryXml);
		imageLibariryService.RegisterOnLoadedHandler(this, BindImageLibrary);
		imageLibariryService.BeginLoad();
		OnPanelChanged();
	}
	
	public function BindImageLibrary(eventObject) {
		ctlAddImageTool.LibraryDataSource = imageLibraryXml;
		ctlAddImageTool.LibraryIconUrlFormat = _global.LibraryImageIconUrlFormat;
	
		ctlAddImageTool.DataBind();
		OnPanelChanged();
	}
	
	public function ShowImageEditProperties():Void {
		_global.Mode = _global.ImageEditMode;
		HideAllProperties();
		ImageEditPropertiesClip._visible = true;
		ImageEditPropertiesClip.DataBind();
		OnPanelChanged();
	}
	
	public function ShowTransferProperties(destinationFaceIndex:Number):Void {
		Logic.DesignerLogic.UnselectCurrentUnit();
	
		_global.Mode = _global.TransferMode;
		HideAllProperties();
		TransferPropertiesClip._visible = true;
		
		TransferPropertiesClip.Show(destinationFaceIndex);
		OnPanelChanged();
	}
	
	public function ShowClearFaceProperties():Void {
		_global.Mode = _global.ClearFaceMode;
		HideAllProperties();
		ClearFacePropertiesClip._visible = true;
		OnPanelChanged();
	}

	public function ShowShapeProperties():Void {
		_global.Mode = _global.ShapeMode;
		HideAllProperties();
		ctlAddShapeTool._visible = true;
		ctlAddShapeTool.DataSource = _global.Shapes.GetShapesArray();
		ctlAddShapeTool.DataBind();
		OnPanelChanged();
	}
	
	public function ShowTextEffectProperties():Void {
		_global.Mode = _global.TextEffectMode;
		HideAllProperties();
		ctlTextEffect._visible = true;
		ctlTextEffect.DataSource = _global.Effects.GetEffectsArray();
		ctlTextEffect.DataBind(true);
		OnPanelChanged();
	}

	public function ShowShapeEditProperties():Void {
		_global.Mode = _global.ShapeEditMode;
		HideAllProperties();
		ShapeEditPropertiesClip._visible = true;
		ShapeEditPropertiesClip.DataBind();
		//ShowPlaylistEditProperties();
		OnPanelChanged();
	}
	
	public function ShowPaintEditProperties():Void {
		_global.Mode = _global.PaintEditMode;
		HideAllProperties();
		PaintEditPropertiesClip._visible = true;
		PaintEditPropertiesClip.DataBind();
		OnPanelChanged();
	}
	
	public function ShowPaintProperties():Void {
		trace("ShowPaintProperties");
		_global.Mode = _global.PaintMode;
		HideAllProperties();
		ctlAddPaintTool._visible = true;
		ctlAddPaintTool.DataBind();
		OnPanelChanged();
	}
	
	public function ShowChooseLayoutProperties():Void {
		_global.Mode = _global.ChooseLayoutMode;
		HideAllProperties();
		ChooseLayoutPropertiesClip._visible = true;
		ChooseLayoutPropertiesClip.DataBind();
		OnPanelChanged();
	}
	
	public function ShowUnitProperties():Void {
		switch (_global.Mode) {
			case _global.TextMode:
			case _global.TextEffectMode:
			//	if (_global.CurrentUnit != null)
				ShowTextProperties();
				break;
			/*
				ShowTextEffectProperties();
				break;*/
			case _global.ImageMode:
				ShowImageProperties();
				break;
			case _global.ImageEditMode:
				ShowImageEditProperties();
				break;
			case _global.ShapeMode:
				ShowShapeProperties();
				break;
			case _global.ShapeEditMode:
				ShowShapeEditProperties();
				break;
			case _global.PlaylistEditMode:
				ShowPlaylistEditProperties();
				break;
			case _global.PaintEditMode:
				ShowPaintEditProperties();
				break;
			case _global.PaintMode:
				ShowPaintProperties();
				break;
			case _global.InactiveMode:
				trace("Inactive mode");
				HidePanel();
				break;
		}
		OnPanelChanged();
	}
	
	private function UploadCustomImage(eventObject:Object) {
		_global.GenerateProjectXml();
		var projectService:SAProject = SAProject.GetInstance(_global.projectXml);
		projectService.RegisterOnSavedHandler(this, CommandUploadImage);
		projectService.BeginSave();
	}
	
	private function CommandUploadImage(eventObject:Object) {
		SATracking.AddCustomImage();
		BrowserHelper.InvokePageScript("loadImage");
	}
	
	// Add image from library
	private var imageId = 0;
	private function AddImage(eventObject:Object) {
		_global.GenerateProjectXml();
		this.imageId = eventObject.id;
		
		var projectService:SAProject = SAProject.GetInstance(_global.projectXml);
		projectService.RegisterOnSavedHandler(this, this.CommandAddLibraryImage);
		projectService.BeginSave();

		ShowImageEditProperties();
	}

	private function CommandAddLibraryImage(eventObject:Object) {
		SATracking.AddImage();
		var imageLibariryService:SAImages = SAImages.GetInstance(new XML());
		imageLibariryService.RegisterOnAddedToProjectHandler(this, this.RefreshDesigner);
		imageLibariryService.BeginAddImageToProject(this.imageId);
	}

	private function RefreshDesigner(eventObject:Object) {
		BrowserHelper.InvokePageScript("refresh");
	}

	private function ctlAddPaintTool_EndPainting(eventObject:Object):Void {
		_global.Mode = _global.PaintEditMode;
		//set current unit as active and selected
		_global.CurrentFace.ChangeCurrentUnit(_global.CurrentUnit);
	}
	
	private function AddShape(eventObject) {
		SATracking.AddShape();
		_global.Mode = _global.ShapeEditMode;
		var shapeType = eventObject.shapeType;
		_global.CurrentFace.CreateNewShapeUnit(shapeType);
		ShowUnitProperties();
	}

	private function AddPlaylist(eventObject) {
		_global.Mode = _global.PlaylistEditMode;
		var playlist = eventObject.playlistNode;
		var bNew:Boolean = eventObject.bNew;
		if(_global.CurrentUnit instanceof TableUnit)
			_global.CurrentUnit.ReinitTable(playlist, false, false, false);
		else
			_global.CurrentFace.AddNewPlaylist(playlist,bNew);
		ShowUnitProperties();
	}
	
	public function RegisterOnPanelChangedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("PanelChanged", Delegate.create(scopeObject, callBackFunction));
    }
	
	private function OnPanelChanged() {
		var eventObject = {type:"PanelChanged", target:this};
		this.dispatchEvent(eventObject);
		Logic.DesignerLogic.SynchronizeToolbar();
	}
	
}