﻿class BrowserHelper {
	
	public static function InvokePageScript(cmd:String, param:String) {
		var browser:String = _global.browser;
		browser = browser.toLowerCase();
		var platform:String = _global.platform;
		platform = platform.toLowerCase();
		
		if (browser == undefined || platform == undefined) {
			fscommand(cmd, param);
		}
		else if (browser == "ie" && platform.indexOf("win") != -1) {
			fscommand(cmd, param);
		}
		else {
			getURL("javascript:Designer_DoFSCommand('" + cmd + "', '" + param + "');");
		}
	}
}
