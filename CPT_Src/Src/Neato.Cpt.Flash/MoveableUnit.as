﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;

[Event("onClick")]
[Event("AutofitCancel")]
class MoveableUnit extends Unit {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	
	public  var frameLinkageName = "Frame";
	
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(MoveableUnit.prototype);
	
	private var width:Number = 0;
	private var height:Number = 0;
	private var scaleXValue:Number = 100;
	private var scaleYValue:Number = 100;
	private var initScale:Number = 100;
	
	private var deltaX:Number = 0;
	private var deltaY:Number = 0;
	
	private var keepProportions:Boolean = true;
	private var autofitType:String;
	
	function MoveableUnit(mc:MovieClip, node:XML) {
		super(mc, node);
		
		if (node.attributes.scale != undefined) {
			scaleXValue = node.attributes.scale;
			scaleYValue = node.attributes.scale;
		}
		if (node.attributes.scaleX != undefined) {
			scaleXValue = node.attributes.scaleX;
		}
		if (node.attributes.scaleY != undefined) {
			scaleYValue = node.attributes.scaleY;
		}
		if(node.attributes.autofit != undefined) {
			AutofitType = node.attributes.autofit;
		}
		else {
			AutofitType = _global.ThumbailAutofit;
		}

		initScale = (node.attributes.initScale == undefined) ? scaleXValue : node.attributes.initScale;
		mc._rotation = (node.attributes.rotation == undefined) ? 0 : node.attributes.rotation;
		mc._xscale = scaleXValue;
		mc._yscale = scaleYValue;
		
		setAttributesFromXML(node);		
		
		mc.unitRef = this;
		mc.onPress = press;
		mc.onMouseUp = null;
		mc.onMouseMove = null;
	}
	
	public function RegisterOnClickHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onClick", Delegate.create(scopeObject, callBackFunction));
    }
	
	public static function order(a, b):Number {
		return a.ZOrder > b.ZOrder ? 1 : -1;
	}
	
	public function get ZOrder():Number {
		return mc.getDepth();
	}
	
	public function set X(value:Number):Void {
		if(X != value) {
			OnAutofitCancel();
		}
		super.X = value;
	}
	
	public function get X():Number {
		return super.X;
	}
	
	public function set Y(value:Number):Void {
		if(Y != value) {
			OnAutofitCancel();
		}
		super.Y = value;
	}
	
	public function get Y():Number {
		return super.Y;
	}
	
	public function set Angle(value:Number):Void {
		if(Angle != value) {
			OnAutofitCancel();
		}
		mc._rotation = value;
	}
	
	public function get Angle():Number {
		return mc._rotation;
	}
	
	public function set ScaleX(value:Number):Void {
		if(ScaleX != value) {
			OnAutofitCancel();
		}
		mc._xscale = scaleXValue = value;
	}
	
	public function get ScaleX():Number {
		return scaleXValue;
	}

	public function set ScaleY(value:Number):Void {
		if(ScaleY != value) {
			OnAutofitCancel();
		}
		mc._yscale = scaleYValue = value;
	}
	
	public function Offset(dx:Number, dy:Number):Void {
		X += dx * Math.cos(mc._rotation * Math.PI / 180);
		Y += dx * Math.sin(mc._rotation * Math.PI / 180);
		//TODO: offset by 'dy'
	}
	
 	public function DrawHitArea() {
 		var bounds = mc.getBounds(mc);
 		var xMin = bounds.xMin;
 		var yMin = bounds.yMin;
 		var xMax = bounds.xMax;
 		var yMax = bounds.yMax;
 		
 		mc.lineStyle(0,0,0);
 		mc.beginFill(0, 0);
 		mc.moveTo(xMin, yMin);
 		mc.lineTo(xMin, yMax);
 		mc.lineTo(xMax, yMax);
 		mc.lineTo(xMax, yMin);
 		mc.endFill();
 	}	
	
	public function get ScaleY():Number {
		return scaleYValue;
	}
	
	public function set KeepProportions(value:Boolean):Void {
		keepProportions = value;
	}
	
	public function get KeepProportions():Boolean {
		return keepProportions;
	}

	public function set AutofitType(value:String):Void {
		autofitType = value;
	}
	
	public function get AutofitType():String {
		return autofitType;
	}
	
	public function get IsAutofited():Boolean {
		return AutofitType != _global.NoneAutofit;
	}

	public function get InitialScale():Number {
		return initScale;
	}

	function setAttributesFromXML(node:XML):Void {
		for (var i = 0; i < node.childNodes.length; ++i) {
			var childNode:XMLNode = node.childNodes[i];
				switch(childNode.nodeName) {
					case "Rotate":
						Angle = childNode.attributes.angle;
						break;
					default:
						//trace("Unexpected tag name! " + childNode.nodeName);
				}
		}
	}
	
	function GetXmlNodeRotate():XMLNode {
		var node:XMLNode = new XMLNode(1, "Rotate");
		node.attributes.angle = Math.round(mc._rotation*1000)/1000;
		return node;
	}

	function GetXmlNode():XMLNode {
		var node:XMLNode = super.GetXmlNodeBase();
		node.attributes.scaleX = scaleXValue;
		node.attributes.scaleY = scaleYValue;
		node.attributes.initScale = initScale;
		node.attributes.rotation = Math.round(mc._rotation*1000)/1000;
		node.attributes.autofit = AutofitType;
		return node;
	}

	function press():Void {
		if (_global.Mode == _global.PaintMode) return;
		var mc = MovieClip(this);
		var unit = mc.unitRef;

		var eventObject = {type:"onClick", target:unit};
		unit.dispatchEvent(eventObject);
		
		mc.onMouseMove = unit.mouseMove;
		mc.onMouseUp = unit.noDrag;
		deltaX = _global.SelectionFrame._parent._xmouse - _global.SelectionFrame.X;
		deltaY = _global.SelectionFrame._parent._ymouse - _global.SelectionFrame.Y;
	}
	
	function mouseMove() {
		_global.SelectionFrame.X = _global.SelectionFrame._parent._xmouse - deltaX;
		_global.SelectionFrame.Y = _global.SelectionFrame._parent._ymouse - deltaY;
	}
	
	function noDrag() {
		var mc = MovieClip(this);
		mc.onMouseMove = null;
		mc.onMouseUp = null;
	}

	function getMode():String {
		return "undefined";
	}
	
	
	public function ResizeByStep(isPositive:Boolean) {
		var step:Number = isPositive ? 1 : -1;

		var resizer:UnitResizer = new UnitResizer(this.mc);
		var deltaX:Number = (resizer.rightBottom.x - resizer.leftUpper.x) > 0 ? step : -step;
		var deltaY:Number = (resizer.rightBottom.y - resizer.leftUpper.y) > 0 ? step : -step;
		
		var newPositionX:Number = resizer.rightBottom.x + deltaX;
		var newPositionY:Number = resizer.rightBottom.y + deltaY; 
		
		var k:Number = resizer.GetProportionalFactor(newPositionX, newPositionY);
		
		var newWidth = 	resizer.CurrentWidth * k;
		var newHeight = resizer.CurrentHeight * k;

		if (newHeight <= 4 || newWidth <= 4) {
			k = 1;
		}
		Resize(k, k);
	}
	
	public function ResizeToPosition(globalPositionX:Number, globalPositionY:Number) {
		var resizer:UnitResizer = new UnitResizer(this.mc);

		var kx:Number = 1;
		var ky:Number = 1;
		if (this.KeepProportions == true) {
			var k:Number = resizer.GetProportionalFactor(globalPositionX, globalPositionY);
			kx = k;
			ky = k;
		} else {
			kx = resizer.GetFreeXFactor(globalPositionX, globalPositionY);
			ky = resizer.GetFreeYFactor(globalPositionX, globalPositionY);
		}

		var newWidth = 	resizer.CurrentWidth * kx;
		var newHeight = resizer.CurrentHeight * ky;

		if (newHeight <= 4 || newWidth <= 4) {
			kx = ky = 1;
		}

		Resize(kx, ky);
	}
	
	private function Resize(kx:Number, ky:Number) {
	}
	
	private function OnAutofitCancel() {
		if(IsAutofited) {
			var eventObject = {type:"AutofitCancel", target:this};
			this.dispatchEvent(eventObject);
		}
	};

	public function RegisterOnAutofitCancelHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("AutofitCancel", Delegate.create(scopeObject, callBackFunction));
   }

	public function UnregisterOnAutofitCancelHandler(scopeObject:Object, callBackFunction:Function) {
		this.removeEventListener("AutofitCancel", Delegate.create(scopeObject, callBackFunction));
    }
}