﻿class ContourUnit extends Unit {
	private var primitives:Array;
	private var cutLines:Array;
	private var bgColor:Number;
	private var isFilled:Boolean;
		
	function ContourUnit(mc:MovieClip, node:XML) {
		super(mc, node);
		primitives = new Array();
		cutLines = new Array();
		for (var i = 0; i < node.childNodes.length; ++i) {
			var childNode:XML = node.childNodes[i];
			if (childNode.nodeName == "CutLine")
				cutLines.push(Logic.DrawingHelper.CreatePrimitive(childNode));
			else
				primitives.push(Logic.DrawingHelper.CreatePrimitive(childNode));
		}
	}
	
	function CreateXmlNode():XML {
		return new XML();
	}
	
	function SetBgColor(IsFilled:Boolean, BgColor:Number):Void {
		isFilled = IsFilled;
		bgColor = BgColor;
		if (!isFilled)
			bgColor = 0xFFFFFF;
	}
	
	function SetBgAndDraw(IsFilled:Boolean, BgColor:Number, angle:Number):Void {
		mc.clear();
		SetBgColor(IsFilled, BgColor);
		Draw(angle);
	}
	
	function Draw(angle:Number):Void {
		trace("Contour Draw");
		trace(mc);
		DrawMC(mc, false, angle);
	}
	
	function DrawMC(mc:MovieClip, transparent:Boolean, angle:Number):Void {
		if (!isFilled)
			bgColor = 0xFFFFFF;
			
		if (!transparent)
			mc.beginFill(bgColor, 100);

		for (var i = 0; i < primitives.length; ++i) {
			var primitive:IDraw = primitives[i];
			primitive.Draw(mc, 0, 0, angle);
		}
		
		if (!transparent)
			mc.endFill();
	}
	
	function DrawMask(mc:MovieClip, angle:Number):Void {
		var workspace:MovieClip = mc._parent._parent;
		var scale:Number = workspace._xscale / 100;
		if (scale == undefined || scale < 1)
			scale = 1;
		scale *= 5;
		var topLeft:Object = {x:-_global.EditWidth, y:-_global.EditHeight};
		//to do: must be refactored!
		var rightBottom:Object = {x:topLeft.x + scale*_global.EditWidth, y:topLeft.y + scale*_global.EditHeight};
		mc.globalToLocal(topLeft);
		mc.globalToLocal(rightBottom);
			
		mc.clear();
		mc.beginFill(_global.styles.MainLayerPanel.backgroundColor, 90);
		
		mc.moveTo(topLeft.x, topLeft.y);
		mc.lineTo(rightBottom.x, topLeft.y);
		mc.lineTo(rightBottom.x, rightBottom.y);
		mc.lineTo(topLeft.x, rightBottom.y);
		mc.lineTo(topLeft.x, topLeft.y);
		
		for (var i = 0; i < primitives.length; ++i) {
			var primitive:IDraw = primitives[i];
			primitive.Draw(mc, 0, 0, angle);
		}
		mc.endFill();
	}
	
	function DrawHoles(mc:MovieClip, angle:Number):Void {
		var bounds = mc.getBounds(mc);
		var mcHoles:MovieClip = mc.createEmptyMovieClip("Holes", mc.getNextHighestDepth());
		
		mcHoles.beginFill(0xFFFFFF, 100);
		
		var xMin = bounds.xMin - 1;
		var yMin = bounds.yMin - 1;
		var xMax = bounds.xMax + 1;
		var yMax = bounds.yMax + 1;

		mcHoles.moveTo(xMin, yMin);
		mcHoles.lineTo(xMin, yMax);
		mcHoles.lineTo(xMax, yMax);
		mcHoles.lineTo(xMax, yMin);
		mcHoles.lineTo(xMin, yMin);
		
		for (var i = 0; i < primitives.length; ++i) {
			var primitive:IDraw = primitives[i];
			primitive.Draw(mcHoles, 0, 0, angle);
		}
		mcHoles.endFill();
		mc.setMask(mcHoles);
	}
	
	function DrawCutLines(mc:MovieClip, angle:Number):Void {
		trace("DrawCutLines");
		var mcCutLines:MovieClip = mc.createEmptyMovieClip("CutLines", mc.getNextHighestDepth());
		mcCutLines.lineStyle(1);
		for (var i = 0; i < cutLines.length; ++i) {
			var primitive:IDraw = cutLines[i];
			primitive.Draw(mcCutLines, 0, 0, angle);
		}
	}
}