﻿class CurveToCommand implements IDraw {
	private var x:Number;
	private var y:Number;
	private var cx:Number;
	private var cy:Number;
	
	function CurveToCommand(node:XML) {
		x = Number(node.attributes.x);
		y = Number(node.attributes.y);
		cx = Number(node.attributes.cx);
		cy = Number(node.attributes.cy);
	}
	
	function Draw(mc:MovieClip, originalX:Number, originalY:Number, rotationAngle:Number, scaleX:Number, scaleY:Number):Void {
		if(scaleX==undefined)scaleX=1.0;
		if(scaleY==undefined)scaleY=1.0;
		var a = new Object({x:originalX+x,  y:originalY+y});
		var c = new Object({x:originalX+cx, y:originalY+cy});
		//rotate if needed
		if (rotationAngle > 0 || rotationAngle < 0) {
			a = _global.RotateAboutCenter(a.x, a.y, originalX, originalY, rotationAngle);
			c = _global.RotateAboutCenter(c.x, c.y, originalX, originalY, rotationAngle);
		}
		mc.curveTo(c.x*scaleX, c.y*scaleY, a.x*scaleX, a.y*scaleY);
	}	
}
