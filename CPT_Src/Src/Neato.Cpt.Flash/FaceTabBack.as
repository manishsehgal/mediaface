﻿import mx.controls.Label;
import mx.core.UIObject;

class FaceTabBack extends UIObject {
	
	private static var instance;
	
	private var mcFaceTab : MovieClip;
	private var mcFaceTabSticker : MovieClip;
	
	function FaceTabBack() {
		instance = this;
	}
	
	static function getInstance():FaceTabBack {
		return instance;
	}
	
	function onLoad() {
		NormalizeState();
	}
	
	public function NormalizeState() {
		var deviceType = _global.projectXml.firstChild.attributes.deviceType;
		if (deviceType == "Sticker") {
			mcFaceTab.visible = false;
			mcFaceTabSticker.visible = true;
		} else {
			mcFaceTabSticker.visible = false;
			mcFaceTab.visible = true;
			mcFaceTab.NormalizeState();
			mcFaceTab.DataBind();
		}
	}
	
	
}