﻿import mx.core.UIObject;
import mx.controls.CheckBox;
import mx.controls.ComboBox;
import mx.controls.Label;
import mx.utils.Delegate;
[Event("exit")]
class TextProperties extends UIObject {
	private var cbFont:ComboBox;
	private var cbFontSize:ComboBox;
	private var txtText:SelectionTextInput;
	private var chkBold:CheckBox;
	private var chkItalic:CheckBox;
	//private var chkAllText:CheckBox;
	private var listenerCbFont:Object;
	private var oldFontSize:Number;
	private var listenerChkBold:Object;
	private var listenerChkItalic:Object;
	//private var listenerChkAllText:Object;
	/*private var lblAlign:Label;
	private var btnAlignLeft:RadioButton;
	private var btnAlignCenter:RadioButton;
	private var btnAlignRight:RadioButton;
	private var listenerAlign:Object;*/
	private var btnDelete:MenuButton;
	private var lblEditText:Label;
	private var lblFont:Label;
	private var lblSize:Label;
	private var lblText:Label;
	private var colorTool:Tools.ColorSelectionTool.ColorTool;
	private var btnTextEffect:MenuButton;
	private var bTextChanged:Boolean = false;

	function TextProperties() {
		setStyle("styleName", "ToolPropertiesPanel");
		this.cbFont.setStyle("styleName", "ToolPropertiesCombo");
		this.cbFontSize.setStyle("styleName", "ToolPropertiesCombo");
		this.txtText.setStyle("styleName", "ToolPropertiesInputText");
		this.chkBold.setStyle("styleName", "ToolPropertiesCheckBoxBold");
		this.chkItalic.setStyle("styleName", "ToolPropertiesCheckBox");
		/*this.btnAlignLeft.setStyle("styleName", "ToolPropertiesCheckBox");
		this.btnAlignCenter.setStyle("styleName", "ToolPropertiesCheckBox");
		this.btnAlignRight.setStyle("styleName", "ToolPropertiesCheckBox");*/
		this.btnDelete.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnTextEffect.setStyle("styleName", "ToolPropertiesActiveButton");
		this.lblEditText.setStyle("styleName", "ToolPropertiesCaption");
	}
	
	function onLoad() {
		_global.Fonts.LoadSmallFont();
				
		_global.LocalHelper.LocalizeInstance(chkBold, "ToolProperties", "IDS_CHKBOLD");
		_global.LocalHelper.LocalizeInstance(chkItalic, "ToolProperties", "IDS_CHKITALIC");
		//_global.LocalHelper.LocalizeInstance(chkAllText, "ToolProperties", "IDS_CHK_ALL_TEXT");
		_global.LocalHelper.LocalizeInstance(lblEditText, "ToolProperties", "IDS_LBLEDITTEXT");
		_global.LocalHelper.LocalizeInstance(lblFont, "ToolProperties", "IDS_LBLFONT");
		_global.LocalHelper.LocalizeInstance(lblSize, "ToolProperties", "IDS_LBLSIZE");
		/*_global.LocalHelper.LocalizeInstance(lblAlign, "ToolProperties", "IDS_LBLALIGN");
		_global.LocalHelper.LocalizeInstance(btnAlignLeft, "ToolProperties", "IDS_BTN_ALIGN_LEFT");
		_global.LocalHelper.LocalizeInstance(btnAlignCenter, "ToolProperties", "IDS_BTN_ALIGN_CENTER");
		_global.LocalHelper.LocalizeInstance(btnAlignRight, "ToolProperties", "IDS_BTN_ALIGN_RIGHT");*/
		_global.LocalHelper.LocalizeInstance(lblText, "ToolProperties", "IDS_LBLTEXT");
		//_global.LocalHelper.LocalizeInstance(btnDelete, "ToolProperties", "IDS_BTNDELETE");
		//_global.LocalHelper.LocalizeInstance(btnTextEffect, "ToolProperties", "IDS_BTNTEXTEFFECT");
		this.cbFont.dropdown.cellRenderer = "FontCellRenderer";
		TooltipHelper.SetTooltip(btnDelete, "ToolProperties", "IDS_TOOLTIP_DELETE");

		colorTool.RegisterOnSelectHandler(this, colorTool_OnSelect);

		btnDelete.RegisterOnClickHandler(this, _global.DeleteCurrentUnit);
		btnTextEffect.RegisterOnClickHandler(this, OnTextEffectClick);
		btnTextEffect._visible = false;
		this.txtText.RegisterOnSelectionChangeHandler(this, OnSelectionChanged);
		this.txtText.RegisterOnTextChangedHandler(this, ChangeText);

		this.txtText.tabIndex = 1;
		var FontsArray:Array = new Array();
		FontsArray = _global.Fonts.GetFontsArray();
		
		this.cbFont.dropdownWidth = 180;
		for (var i in FontsArray)
			this.cbFont.addItem(FontsArray[i]);
		
		
		trace(cbFontSize);
		cbFontSize.addItem(4);
		cbFontSize.addItem(8);
		cbFontSize.addItem(9);
		cbFontSize.addItem(10);
		cbFontSize.addItem(12);
		cbFontSize.addItem(14);
		cbFontSize.addItem(16);
		cbFontSize.addItem(18);
		cbFontSize.addItem(20);
		cbFontSize.addItem(24);
		cbFontSize.addItem(28);
		cbFontSize.addItem(32);
		cbFontSize.addItem(36);
		cbFontSize.addItem(40);
		cbFontSize.addItem(44);
		cbFontSize.addItem(48);
		cbFontSize.addItem(54);
		cbFontSize.addItem(60);
		cbFontSize.addItem(66);
		cbFontSize.addItem(72);
		cbFontSize.addItem(80);
		cbFontSize.addItem(88);
		cbFontSize.addItem(96);
		cbFontSize.addItem(106);
		cbFontSize.addItem(117);
		cbFontSize.addItem(127);		
		
		this.cbFont.sortItems(upperCaseFunc);
		this.cbFont.selectedIndex = 0;
		
		this.cbFontSize.addEventListener("change", Delegate.create(this, OnChangeFontSize));
		this.cbFontSize.textField.addEventListener("enter", Delegate.create(this, ChangeFontSize));
		this.cbFontSize.textField.restrict="0-9";
		this.cbFontSize.textField.maxChars=3;
		this.chkBold.label += " ";
		var parent = this;
		
		/*
		listenerCbFont = new Object();
		listenerCbFont.change = function(eventObject) {
			var format:TextFormat = new TextFormat();
			format.font = eventObject.target.selectedItem.label;
			eventObject.target._parent.ChangeFontFormat(format);
		};
		this.cbFont.addEventListener("change", listenerCbFont);
		*/
		this.cbFont.addEventListener("change", Delegate.create(this, onFontChanged));

		
		listenerChkBold = new Object();
		listenerChkBold.click = function(eventObject) {
			var format:TextFormat = new TextFormat();
			format.bold = eventObject.target.selected;
			eventObject.target._parent.ChangeFontFormat(format);
		};
		this.chkBold.addEventListener("click", listenerChkBold);
		
		listenerChkItalic = new Object();
		listenerChkItalic.click = function(eventObject) {
			var format:TextFormat = new TextFormat();
			format.italic = eventObject.target.selected;
			eventObject.target._parent.ChangeFontFormat(format);
		};
		this.chkItalic.addEventListener("click", listenerChkItalic);
		
		//trace("this.cbFont = "+this.cbFont);
		//trace("this.cbFont.dropdown = "+this.cbFont.dropdown);
		//trace("this.cbFont.dropdown.cellRenderer = "+this.cbFont.dropdown.cellRenderer);
		/*listenerChkAllText = new Object();
		listenerChkAllText.click = function(eventObject) {
			eventObject.target._parent.ChangeFontFormat();
		};
		this.chkAllText.addEventListener("click", listenerChkAllText);*/
		
		/*listenerAlign = new Object();
		listenerAlign.click = function(eventObject) {
			eventObject.target._parent.ChangeAlign(eventObject);
		};
		this.btnAlignLeft.addEventListener("click", listenerAlign);
		this.btnAlignCenter.addEventListener("click", listenerAlign);
		this.btnAlignRight.addEventListener("click", listenerAlign);*/
		var appName = _global.projectXml.firstChild.attributes.appName;
		if (appName == "DOL" || SALocalWork.IsLocalWork == true) {
			btnTextEffect._visible=true;
		}
		else {
			btnTextEffect._visible=false;
		}
		
	}

	static function upperCaseFunc(a, b) {
		return a.label.toUpperCase() > b.label.toUpperCase();
	}
	
	function OnChangeFontSize(eventObject) {
		if (cbFontSize.textField.getFocus() != null)
			return;
				
		ChangeFontSize(eventObject);
	}
	
	private var inHandler : Boolean = false;
	function onFontChanged(eventObject) {
		if (inHandler == true) return;
		inHandler = true;
		var format:TextFormat = new TextFormat();
		format.font = eventObject.target.selectedItem.label;
		ChangeFontFormat(format);
		inHandler = false;
	}
	
	function OnTextEffectClick(eventObject) {
		if(bTextChanged == true && _global.CurrentUnit instanceof TextEffectUnit) {
			_global.CurrentUnit.UpdateTextEffect();
			bTextChanged = false;
			_global.LocalHelper.LocalizeInstance(btnTextEffect, "ToolProperties", "IDS_BTNTEXTEFFECT");
		} else {
			OnTextEffect();
		}
	}
	private function OnTextEffect() {
		var eventObject = {type:"texteffect", target:this};
		this.dispatchEvent(eventObject);
	}

	public function RegisterOnTextEffectHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("texteffect", Delegate.create(scopeObject, callBackFunction));
    }
	
	/*function ChangeAlign(eventObject) {
		_global.CurrentUnit.Align = eventObject.target.data;
	}*/
	
	function ChangeFontSize(eventObject) {
		var newSize = parseInt(eventObject.target.text);
		if (newSize > 3 && newSize < 128) {
			oldFontSize = newSize;
			var format:TextFormat = new TextFormat();
			format.size = newSize;
			ChangeFontFormat(format);
		} else {
			cbFontSize.text = oldFontSize.toString();
		}
	}
	
	private function colorTool_OnSelect(eventObject) {
		var format:TextFormat = new TextFormat();
		format.color = colorTool.RGB;
		ChangeFontFormat(format);
	}
	
	function ChangeText(eventObject) {
		bTextChanged = true;

		if(_global.CurrentUnit instanceof TextEffectUnit) {
			_global.LocalHelper.LocalizeInstance(btnTextEffect, "ToolProperties", "IDS_BTNAPPLYCHANGES");
		}
		
		var format:TextFormat = new TextFormat();
		format.font = cbFont.selectedItem.label;
		format.size = oldFontSize;
		format.bold = chkBold.selected;
		format.italic = chkItalic.selected;
		format.color = colorTool.RGB;
		
		_global.CurrentUnit.ReplaceText(eventObject.oldBegin, eventObject.oldEnd, eventObject.insertedText, format);

		if(_global.CurrentUnit instanceof TextEffectUnit) {
			 ChangeFontFormat(format);
			_global.LocalHelper.LocalizeInstance(btnTextEffect, "ToolProperties", "IDS_BTNAPPLYCHANGES");
		}
		
		btnTextEffect.enabled = txtText.length > 0 ? true : false;
	}

	function ChangeFontFormat(format:TextFormat) {
		trace("ChangeFontFormat");
		bTextChanged = true;

		
		//_global.CurrentUnit.AllText = chkAllText.selected;
		if (format == undefined)
			return;
		/*if (chkAllText.selected)
			_global.CurrentUnit.ChangeFontFormat(0, txtText.length, format);
		else {*/
		
		if(!(_global.CurrentUnit instanceof TextEffectUnit)) {
			_global.CurrentUnit.ChangeFontFormat(txtText.SelectionBeginIndex, txtText.SelectionEndIndex, format);
		} else {
			_global.CurrentUnit.ChangeFontFormat(0, txtText.length, format);
		}
		
		btnTextEffect.enabled = txtText.length > 0 ? true : false;
		if (_global.CurrentUnit instanceof TextEffectUnit) {
			_global.LocalHelper.LocalizeInstance(btnTextEffect, "ToolProperties", "IDS_BTNAPPLYCHANGES");
		}
		//}
		txtText.RestoreSelection();
	}
	
	function OnSelectionChanged() {
		BindTextFormat();
		
		bTextChanged = true;
		if(_global.CurrentUnit instanceof TextEffectUnit)
			_global.LocalHelper.LocalizeInstance(btnTextEffect, "ToolProperties", "IDS_BTNAPPLYCHANGES");
	}
	
	public function DataBind() {
		bTextChanged = false;
		_global.LocalHelper.LocalizeInstance(btnTextEffect, "ToolProperties", "IDS_BTNTEXTEFFECT");
	
		if (_global.CurrentUnit.EmptyLayoutItem) {
			var fmt = _global.CurrentUnit.GetFontFormat(0);
			var len = _global.CurrentUnit.Text.length;
			_global.CurrentUnit.ReplaceText(0, len, "", fmt);
			_global.CurrentUnit.EmptyLayoutItem = false;
		}
		
		this.txtText.text = _global.CurrentUnit.Text;
		this.txtText.SelectAllText();

		btnTextEffect.enabled = txtText.length > 0 ? true : false;

		BindTextFormat();
		
		//this.chkAllText.selected = _global.CurrentUnit.AllText;
		
		/*if (_global.CurrentUnit.Align == "right") {
			this.btnAlignRight.selected = true;
		} else if (_global.CurrentUnit.Align == "center") {
			this.btnAlignCenter.selected = true;
		} else {
			this.btnAlignLeft.selected = true;
		}*/
	}
	
	private function BindTextFormat() {
		var index:Number;
		
		if (txtText.SelectionBeginIndex == txtText.SelectionEndIndex) {
			index = (txtText.SelectionBeginIndex <= 0) ? 0 : txtText.SelectionBeginIndex - 1;
		} else {
			index = txtText.SelectionBeginIndex;
		}
		
		var format:TextFormat = _global.CurrentUnit.GetFontFormat(index);

		if (this.cbFont.value.toLowerCase() != format.font.toLowerCase()) {
			for (var i = 0; i < this.cbFont.length; ++i) {
				if (this.cbFont.getItemAt(i).label.toLowerCase() == format.font.toLowerCase()) {
		BrowserHelper.InvokePageScript("log", "BindTextFormat");
					this.cbFont.selectedIndex = i;
					break;
				}
			}
		}

		var fontSize:Number = format.size;
		if (fontSize != null && fontSize != undefined && fontSize != this.cbFontSize.value) {
			oldFontSize = fontSize;
			for (var i = 0; i < this.cbFontSize.length; ++i) {
				if (this.cbFontSize.getItemAt(i).label == oldFontSize) {
					this.cbFontSize.selectedIndex = i;
					break;
				}
			}
			this.cbFontSize.text = oldFontSize.toString();
		}
		
		var selectedColor = format.color;
		if (selectedColor == undefined) selectedColor = 0;
		colorTool.SetColorState(selectedColor);

		this.chkBold.selected = format.bold;
		this.chkItalic.selected = format.italic;
	}
	
	function RefreshFontSize(){
		this.cbFontSize.text = _global.CurrentUnit.Size.toString();
	}
}