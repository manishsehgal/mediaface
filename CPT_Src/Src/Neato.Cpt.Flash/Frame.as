﻿class Frame extends MovieClip {
	private var unit:MoveableUnit;
	private var frameBorder:FrameBorder;
	private var rotateHandler:MovieClip;
	private var rotateLabel:MovieClip;
	private var resizeHandler:MovieClip;
	private var resizeLabel:MovieClip;
	private var saveAngle:Number;
	private var saveX:Number;
	private var saveY:Number;
	private var handlerSize:Number;
	
	
	function Frame() {
		unit = undefined;
		trackAsMenu = true;
		saveAngle = NaN;
	}
	function AttachUnit(mu:MoveableUnit) {
		unit = mu;
		
		_visible = true;
		frameBorder.Draw(unit);
		
		_x = unit.X;
		_y = unit.Y;
		_rotation = unit.Angle;
		saveAngle = NaN;
		frameBorder._rotation = rotateHandler._rotation = resizeHandler._rotation = 0;
		frameBorder._x = 0;
		frameBorder._y = 0;
		
		var fbUpperLeftX = frameBorder.getBounds(this).xMin;
		var fbUpperLeftY = frameBorder.getBounds(this).yMin;
		
		rotateHandler._x = fbUpperLeftX;
		rotateHandler._y = fbUpperLeftY;
		
		resizeHandler._x = fbUpperLeftX + frameBorder._width;
		resizeHandler._y = fbUpperLeftY + frameBorder._height;
		
		handlerSize = rotateHandler._height;
		
		MoveHandlerLabels();
	}
	
	function MoveHandlerLabels() {
		// Move Rotate Label
		rotateLabel._rotation = -_rotation;
		var lblW = rotateLabel._width;
		var lblH = rotateLabel._height;
		var dx = 0;
		var dy = 0;
		var angle = -_rotation * Math.PI / 180;

		if(_rotation >= 0 && _rotation <= 90) {
			dx = 0;
			dy = - handlerSize * Math.cos(angle);
		}
		else if(_rotation >= 90 && _rotation <= 180) {
			dx = - lblW * Math.cos(angle);
			dy = 0;
		}
		else if (_rotation >= -180 && _rotation < -90) {
			dx = - lblW * Math.cos(angle) + lblH * Math.sin(angle) - handlerSize * Math.sin(angle) - 0.5 * handlerSize * Math.cos(angle) * Math.sin(angle);
			dy = - 2 * handlerSize * Math.sin(angle) - 0.5 * handlerSize * Math.cos(angle) * Math.sin(angle);
		}
		else if(_rotation >= -90 && _rotation < 0) {
			dx = handlerSize * Math.sin(angle);
			dy = - handlerSize * Math.cos(angle) - lblH * Math.sin(angle);
		}
		rotateLabel._x = rotateHandler._x + dx;
		rotateLabel._y = rotateHandler._y + dy;
		
		// Move Resize Label
		resizeLabel._rotation = -_rotation;
		lblW = resizeLabel._width;
		lblH = resizeLabel._height;
		
		if(_rotation >= 0 && _rotation <= 90) {
			dx = - lblW * Math.cos(angle) + lblH * Math.sin(angle) - handlerSize * Math.sin(angle) + 0.5 * handlerSize * Math.cos(angle) * Math.sin(angle);
			dy = - 2 * handlerSize * Math.sin(angle) + 0.5 * handlerSize * Math.cos(angle) * Math.sin(angle);
		}
		else if(_rotation >= 90 && _rotation <= 180) {
			dx = handlerSize * Math.sin(angle);
			dy = - handlerSize * Math.cos(angle) - lblH * Math.sin(angle);
		}
		else if (_rotation >= -180 && _rotation < -90) {
			dx = 0;
			dy = - handlerSize * Math.cos(angle);
		}
		else if(_rotation >= -90 && _rotation < 0) {
			dx = - lblW * Math.cos(angle);
			dy = 0;
		}
		resizeLabel._x = resizeHandler._x + dx;
		resizeLabel._y = resizeHandler._y + dy;
	}
	
	function DetachUnit() {
		_visible = false;
		unit = undefined;
	}
	
	public function set X(value:Number):Void {
		_x = value;
		unit.X = value;
		saveAngle = NaN;
	}
	
	public function get X():Number {
		return _x;
	}
	
	public function set Y(value:Number):Void {
		_y = value;
		unit.Y = value;
		saveAngle = NaN;
	}
	
	public function get Y():Number {
		return _y;
	}
	
	public function set Angle(value:Number):Void {
		var rot = _rotation;
		var xS = _x;
		var yS = _y;
		if (!isNaN(saveAngle)) {
			rot = saveAngle;
			xS = saveX;
			yS = saveY;
		}
			
		
		var w = frameBorder.getBounds(this).xMin + frameBorder._width/2-1;
		var h = frameBorder.getBounds(this).yMin + frameBorder._height/2-1;
		var r = Math.sqrt(w*w + h*h);
		
		var angle = Math.atan(h/Math.abs(w));
		if (w<0) angle = Math.PI - angle;
		angle += rot * Math.PI/180;
		var x0 = w - r * Math.cos(angle);
		var y0 = h - r * Math.sin(angle);
		
		angle = Math.atan(h/Math.abs(w));
		if (w<0) angle = Math.PI - angle;
		angle += value * Math.PI/180;
		var x = w - r * Math.cos(angle);
		var y = h - r * Math.sin(angle);
		
		if (isNaN(saveAngle)) {
			saveAngle = _rotation;
			saveX = _x;
			saveY = _y;
		}
		
		_x     = xS + (x - x0);
		unit.X = _x;
		_y     = yS + (y - y0);
		unit.Y = _y;

		_rotation = value;
		unit.Angle = value;
		
		MoveHandlerLabels();
	}
	
	public function get Angle():Number {
		return _rotation;
	}
	
	public function set ScaleX(value:Number):Void {
		unit.ScaleX = value;
		saveAngle = NaN;
	}
	
	public function get ScaleX():Number {
		return unit.ScaleX;
	}
	
	public function set ScaleY(value:Number):Void {
		unit.ScaleY = value;
		saveAngle = NaN;
	}
	
	public function get ScaleY():Number {
		return unit.ScaleY;
	}
	
	public function set HandlersScale(value:Number):Void {
		rotateHandler._xscale = rotateHandler._yscale = 
		resizeHandler._xscale = resizeHandler._yscale = 
		rotateLabel._xscale = rotateLabel._yscale =
		resizeLabel._xscale = resizeLabel._yscale =
		frameBorder._xscale = frameBorder._yscale = value;
	}
}