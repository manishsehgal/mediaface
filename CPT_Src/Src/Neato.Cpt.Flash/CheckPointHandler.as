﻿import mx.utils.Delegate;
class CheckPointHandler extends mx.core.UIObject {
	private var xMouseBegin:Number = 0;
	private var yMouseBegin:Number = 0;
	public var pos:String;
	public var id:Number;
	function CheckPointHandler() {
		this.onPress = drag;
		this.onMouseMove = null;
		this.onMouseUp = mouseUp;
		this.onRollOver = rollOver;
		this.onRollOut = rollOut;
		GotoAndStop(1);
	}
	
	function drag() {
		trace("resize drag");
		this.onMouseMove = mouseMove;
		xMouseBegin = _xmouse;
		yMouseBegin = _ymouse;
		GotoAndStop(3);
	}
	
	function noDrag() {
		trace("resize noDrag");
		this.onMouseMove = null;
		GotoAndStop(1);
		OnFinishDrag();
	}
	
	function mouseMove() {
		this._x = _parent._xmouse;
		this._y = _parent._ymouse;
		trace("id = "+id);
		OnUpdate( _parent._xmouse,_parent._ymouse)
	}
	
	function rollOver() {
		GotoAndStop(2);
	}
	
	function rollOut() {
		GotoAndStop(1);
	}
		
	function mouseUp() {
		if (onMouseMove != null)
			noDrag();
	}
	//region Exit event
	private function OnUpdate(X:Number,Y:Number) {
		var eventObject = {type:"update", target:this, id:this.id, X:X, Y:Y};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnUpdateHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("update", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion	//region finishdrag event
	private function OnFinishDrag(X:Number,Y:Number) {
		var eventObject = {type:"finishdrag", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnFinishDragHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("finishdrag", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
}