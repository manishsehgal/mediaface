﻿import mx.controls.Alert;

class SASession{
	static private var vars:LoadVars;
	static private var vars2:LoadVars;
	static private var mouseListener:Object;
	static private var alertShown:Boolean;
	
	static public function Initialize():Void {
		alertShown = false;
		vars = new LoadVars();
		vars2 = new LoadVars();
		mouseListener = new Object();
		
		mouseListener.onMouseDown = mouseListener_MouseDown;
		Mouse.addListener(mouseListener);
		vars.onLoad = vars_LoadHandler;
	}
	
	static function mouseListener_MouseDown() {
		var currentDate:Date = new Date();
		var url:String = "../ProlongSession.aspx?time=" + currentDate.getTime();
		vars.load(url);
	}
	
	static function vars_LoadHandler(success:Boolean) {
		if (vars.sessionExpired == "True")
			ShowAlert();
	}
	
	static function ShowAlert():Void {
		Mouse.removeListener(_global.mouseListener);
		if (alertShown == false) {
			alertShown = true;
			Alert.show("Your session is expired. You will be redirected to home page.",
				"Warning", Alert.OK, null, alertclickHandler);
		}
	}
	
	static function alertclickHandler(evt) {
		getURL("default.aspx");
	}
	
	static public function WaitForSessionChecked(callBackFunction:Function):Void {
		Preloader.Show();
		vars2.onLoad = function(success:Boolean) {
			Preloader.Hide();
			mouseListener_MouseDown();
			if (!success) {
				ShowAlert();
				return;
			}
			var intervalObject = new Object();
			var interval:Number = setInterval(checkLoaded, 200, intervalObject, callBackFunction);
			intervalObject["interval"] = interval;
		}
		
		var currentDate:Date = new Date();
		var url:String = "../ProlongSession.aspx?time=" + currentDate.getTime();
		vars2.load(url);
		vars.load(url);
	}
	
	static private function checkLoaded(intervalObject:Object, callBackFunction:Function) {
		clearInterval(intervalObject.interval);
		if (vars.sessionExpired != "True") 
				callBackFunction.call();
	}
}