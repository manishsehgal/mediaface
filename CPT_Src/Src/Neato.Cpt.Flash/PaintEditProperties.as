﻿import mx.utils.Delegate;
import mx.core.UIObject;
import mx.controls.*;
[Event("exit")]
class PaintEditProperties extends UIObject {
	private var chkKeepProportions:CheckBox;
	private var btnDelete:MenuButton;
	private var btnRestoreProportions:MenuButton;
	private var lblPaintEdit:Label;
	private var btnContinue:MenuButton;
	private var lblBrushWeight:Label;
	function PaintEditProperties() {
		this.btnDelete.setStyle("styleName", "ToolPropertiesActiveButton");
		this.lblPaintEdit.setStyle("styleName", "ToolPropertiesCaption");
		this.btnContinue.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnRestoreProportions.setStyle("styleName", "ToolPropertiesActiveButtonSmall");
	}
	
	function onLoad() {
		trace("PaintEditProperties.onLoad");
		btnDelete.RegisterOnClickHandler(this, _global.DeleteCurrentUnit);
		chkKeepProportions.addEventListener("click", Delegate.create(this, chkKeepProportions_OnClick));
		btnRestoreProportions.addEventListener("click", Delegate.create(this, btnRestoreProportions_OnClick));
		btnContinue.addEventListener("click",	Delegate.create(this, btnContinue_OnClick));
		DataBind();
		InitLocale();
	}
	
	private function chkKeepProportions_OnClick(eventObject)	{
		_global.CurrentUnit.KeepProportions = chkKeepProportions.selected;
	}
	
	private function btnRestoreProportions_OnClick(eventObject) {
		_global.CurrentUnit.ScaleX = _global.CurrentUnit.InitialScale;
		_global.CurrentUnit.ScaleY = _global.CurrentUnit.InitialScale;
		_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
	}
	
	public function DataBind():Void {
		trace("PaintEditProperties.DataBind Mode = " + _global.Mode);
		chkKeepProportions.selected = _global.CurrentUnit.KeepProportions;
		chkKeepProportions.enabled =_global.CurrentUnit.IsNonScalable;
	}

	private function InitLocale() {
		//_global.LocalHelper.LocalizeInstance(btnDelete, "ToolProperties", "IDS_BTNDELETE");
		_global.LocalHelper.LocalizeInstance(chkKeepProportions, "ToolProperties", "IDS_CHKKEEPPROPORTIONS");
		//_global.LocalHelper.LocalizeInstance(btnRestoreProportions, "ToolProperties", "IDS_BTNRESTOREPROPORTIONS");
		_global.LocalHelper.LocalizeInstance(lblPaintEdit, "ToolProperties", "IDS_LBLPAINTEDIT");
		//_global.LocalHelper.LocalizeInstance(btnContinue, "ToolProperties", "IDS_BTNCONTINUE");
		_global.LocalHelper.LocalizeInstance(lblBrushWeight, "ToolProperties", "IDS_LBLBRUSHWEIGHT");
		
		
		TooltipHelper.SetTooltip(btnDelete, "ToolProperties", "IDS_TOOLTIP_DELETE");
		TooltipHelper.SetTooltip(btnContinue, "ToolProperties", "IDS_TOOLTIP_CONTINUE");
		TooltipHelper.SetTooltip(chkKeepProportions, "ToolProperties", "IDS_TOOLTIP_KEEPPROPORTIONS");
		TooltipHelper.SetTooltip(btnRestoreProportions, "ToolProperties", "IDS_TOOLTIP_RESTOREPROPORTIONS");
    }
	private function btnContinue_OnClick() {
		OnExit();
	}
	//region Exit event
	private function OnExit() {
		var eventObject = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
}