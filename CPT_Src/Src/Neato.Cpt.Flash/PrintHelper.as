﻿import mx.controls.Alert;

class PrintHelper {
	private static var instance:PrintHelper=undefined;
	static function Print():Void {
		//if (!SessionHelper.IsSessionChecked())
		//	return;
				
		if (instance == undefined)
			instance = new PrintHelper();
			
		_global.GenerateProjectXml();
		var projectService:SAProject = SAProject.GetInstance(_global.projectXml);
		projectService.RegisterOnSavedHandler(instance, OnSaveProject);
		projectService.BeginSave();
	}
	
	static private function OnSaveProject(eventObject) {
		CheckCalibration();
	}
	
	static private function ShowPDF():Void {
		SATracking.PrintPDF();
		var currentDate:Date = new Date();
		var url:String = "Print.aspx?time=" + currentDate.getTime();
		getURL(url, "_blank");
	}

	static private function onAlertShow(evt) {
		if (evt.detail == Alert.CANCEL) {
			ShowPDF();
		}
	}
	
	static private function CheckCalibration() {
		var alertOkLabel:String = Alert.okLabel;
		var alertCancelLabel:String = Alert.cancelLabel;
		var alertButtonWidth:Number = Alert.buttonWidth;
			
		var alertMessage:String = _global.LocalHelper.GetString("PreviewDescription", "IDS_ALERTMESSAGE");
		var alertCaption:String = _global.LocalHelper.GetString("PreviewDescription", "IDS_ALERTCAPTION");
		var alertButtonWidth:Number = Alert.buttonWidth;
		
		Alert.okLabel = _global.LocalHelper.GetString("PreviewDescription", "IDS_ALERTOK");;
		Alert.cancelLabel = _global.LocalHelper.GetString("PreviewDescription", "IDS_ALERTCANCEL");
		Alert.buttonWidth = 175;
		
		if (_global.IsCalibrationSpecified == false) {
			Alert.show(alertMessage, alertCaption, Alert.OK | Alert.CANCEL, null, onAlertShow);
		} else {
			ShowPDF();
		}
		
		Alert.okLabel = alertOkLabel;
		Alert.cancelLabel = alertCancelLabel;
		Alert.buttonWidth = alertButtonWidth;
	}
}