﻿class TooltipHelper {
	private static function showHint(obj, hintMessage) {
		var depth = _root.DepthManager.kTooltip;
		clearInterval(obj.interval);
		var hint:MovieClip = _root.createEmptyMovieClip("hint", depth);
		hint._x = _root._xmouse;
		hint._y = _root._ymouse;
		hint.createTextField("hintText", depth, 0, 0, 200, 50);
		var hintStyleObj:Object = StylesHelper.GetStyle(".Tooltip");
		var hintFormat:TextFormat = new TextFormat();
		hintFormat.font = hintStyleObj.fontFamily;
		hintFormat.size = hintStyleObj.fontSize;
		hintFormat.color = hintStyleObj.color;
		hintFormat.align = "center";
		var indent:Number = 5;
		with (hint.hintText) {
			selectable = false;
			htmlText = hintMessage;
			setTextFormat(hintFormat);
			autoSize = true;
		}
		if (hint.hintText._width > 200) {
			indent = 0;
			hint.hintText.multiline = true;
			hint.hintText.wordWrap = true;
			hint.hintText._width = 200;
			hint.hintText.setTextFormat(hintFormat);
		}
		var s:Number = 10;
		//shift
		var c:Number = 15;
		//corner;
		var w:Number = hint.hintText._width+s + indent*2;
		var h:Number = hint.hintText._height+s;
		var topRight:Boolean = _root.hitTest(_root._xmouse+w+s, _root._ymouse-h-s, true);
		var bottomRight:Boolean = _root.hitTest(_root._xmouse+w+s, _root._ymouse+h+s, true);
		var topLeft:Boolean = _root.hitTest(_root._xmouse-w-s, _root._ymouse-h-s, true);
		var bottomLeft:Boolean = _root.hitTest(_root._xmouse-w-s, _root._ymouse+h+s, true);
		if (!(topRight || bottomRight || topLeft || bottomLeft)) {
			topRight = true;
		}
		
		hint.lineStyle(0);
		hint.beginFill(hintStyleObj.backgroundColor);
		hint.moveTo(0, 0);
		if (topRight) {
			hint.curveTo(s, -s-s/2, s, -s-s/2);
			hint.curveTo(s, -s-h+c, s, -s-h+c);
			hint.curveTo(s, -s-h, s+c, -s-h);
			hint.curveTo(s+w-c, -s-h, s+w-c, -s-h);
			hint.curveTo(s+w, -s-h, s+w, -s-h+c);
			hint.curveTo(s+w, -s-c, s+w, -s-c);
			hint.curveTo(s+w, -s, s+w-c, -s);
			hint.curveTo(s+s/2, -s, s+s/2, -s);
			hint.hintText._x = s+s/2 + indent;
			hint.hintText._y = -s-h+s/2;
		} else if (bottomRight) {
			hint.curveTo(s, s+s/2, s, s+s/2);
			hint.curveTo(s, s+h-c, s, s+h-c);
			hint.curveTo(s, s+h, s+c, s+h);
			hint.curveTo(s+w-c, s+h, s+w-c, s+h);
			hint.curveTo(s+w, s+h, s+w, s+h-c);
			hint.curveTo(s+w, s+c, s+w, s+c);
			hint.curveTo(s+w, s, s+w-c, s);
			hint.curveTo(s+s/2, s, s+s/2, s);
			hint.hintText._x = s+s/2 + indent;
			hint.hintText._y = s+s/2;
		} else if (topLeft) {
			hint.curveTo(-s, -s-s/2, -s, -s-s/2);
			hint.curveTo(-s, -s-h+c, -s, -s-h+c);
			hint.curveTo(-s, -s-h, -s-c, -s-h);
			hint.curveTo(-s-w+c, -s-h, -s-w+c, -s-h);
			hint.curveTo(-s-w, -s-h, -s-w, -s-h+c);
			hint.curveTo(-s-w, -s-c, -s-w, -s-c);
			hint.curveTo(-s-w, -s, -s-w+c, -s);
			hint.curveTo(-s-s/2, -s, -s-s/2, -s);
			hint.hintText._x = -s-w+s/2 + indent;
			hint.hintText._y = -s-h+s/2;
		} else {
			hint.curveTo(-s, s+s/2, -s, s+s/2);
			hint.curveTo(-s, s+h-c, -s, s+h-c);
			hint.curveTo(-s, s+h, -s-c, s+h);
			hint.curveTo(-s-w+c, s+h, -s-w+c, s+h);
			hint.curveTo(-s-w, s+h, -s-w, s+h-c);
			hint.curveTo(-s-w, s+c, -s-w, s+c);
			hint.curveTo(-s-w, s, -s-w+c, s);
			hint.curveTo(-s-s/2, s, -s-s/2, s);
			hint.hintText._x = -s-w+s/2 + indent;
			hint.hintText._y = s+s/2;
		}
		hint.lineTo(0, 0);
		hint.endFill();
	}
	private static function addTextHint(obj:Object, hintMessage:String) {
		var toolTipAssigned = obj["toolTipAssigned"];
		if (toolTipAssigned)
			return;
			
		obj["toolTipAssigned"] = true;
		var showHintFunc:Function = showHint;
		var fnRollOver = obj.onRollOver;
		obj.onRollOver = function() {
			if (this.hitTest(_root._xmouse, _root._ymouse, true)) {
				this.interval = setInterval(showHintFunc, 300, this, hintMessage);
			}
			fnRollOver.call(this);
		};
		var fnRollOut = obj.onRollOut;
		obj.onRollOut = function() {
			clearInterval(this.interval);
			_root["hint"].removeMovieClip();
			fnRollOut.call(this);
		};
		var fnDragOut = obj.onDragOut;
		obj.onDragOut = function() {
			clearInterval(this.interval);
			_root["hint"].removeMovieClip();
			fnDragOut.call(this);
		};
		var fnPress = obj.onPress;
		obj.onPress = function() {
			clearInterval(this.interval);
			_root["hint"].removeMovieClip();
			fnPress.call(this);
		};
	}
	static function SetTooltip(obj:Object, flaName:String, stringID:String) {
		var hintMessage:String = _global.LocalHelper.GetString(flaName, stringID);
		addTextHint(obj, hintMessage);
	}
}