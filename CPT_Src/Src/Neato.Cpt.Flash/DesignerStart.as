﻿#include "MainLogic.as"
_root.preloader.removeMovieClip(); //remove starter preloader
this._lockroot = true;
_global.Fonts.LoadSmallFont();
BrowserHelper.InvokePageScript("log", "DesignerStart.as");
stop();

_global.pluginsService = SAPlugins.GetInstance();
_global.pluginsService.StartDiscovery();

var projectService:SAProject = SAProject.GetInstance(_global.projectXml);
projectService.RegisterOnLoadedHandler(this, onLoaded);

var fontsService:SAFont = SAFont.GetInstance(_global.fontsXml);
fontsService.RegisterOnLoadedHandler(this, onLoaded);

var paintService:SAPaint = SAPaint.GetInstance(_global.brushesXml);
paintService.RegisterOnLoadedHandler(this, onLoaded);

var stylesService:SAStyles = SAStyles.GetInstance();
stylesService.RegisterOnLoadedHandler(this, onLoaded);

var imagesService:SAImages = SAImages.GetInstance(_global.imagesXml);
imagesService.RegisterOnLoadedHandler(this, onLoaded);

var settingsService:SASettings = SASettings.GetInstance();
settingsService.RegisterOnLoadedHandler(this, onLoaded);

var localeService:SALocale = SALocale.GetInstance();
localeService.RegisterOnLoadedHandler(this, onLoaded);

var shapeService:SAShapes = SAShapes.GetInstance(_global.shapesXml);
shapeService.RegisterOnLoadedHandler(this, onLoaded);

var effectService:SATextEffects = SATextEffects.GetInstance(_global.effectsXml);
effectService.RegisterOnLoadedHandler(this, onLoaded);

function onLoaded(eventObject) {
	if (eventObject.target == settingsService) {
		localeService.BeginLoad();
	}
	
	if (projectService.IsLoaded &&
		fontsService.IsLoaded &&
		stylesService.IsLoaded &&
		settingsService.IsLoaded &&
		localeService.IsLoaded &&
		shapeService.IsLoaded &&
		effectService.IsLoaded)
	{
		nextFrame();
	}
}

projectService.BeginLoad();
fontsService.BeginLoad();
paintService.BeginLoad();
stylesService.BeginLoad();
settingsService.BeginLoad();
shapeService.BeginLoad();
effectService.BeginLoad();