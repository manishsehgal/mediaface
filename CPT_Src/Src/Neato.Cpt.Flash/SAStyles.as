﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;
[Event("onLoaded")]
class SAStyles {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SAStyles.prototype);
	private var isLoaded:Boolean;

	private var styleObj:TextField.StyleSheet;
	
	public static function GetInstance():SAStyles {
		return new SAStyles();
	}
	
	public function get IsLoaded():Boolean {
		return styleObj.getStyleNames().length > 0;
		//return true;
	}
	
	private function SAStyles() {
		isLoaded = false;
		var parent = this;
		
		styleObj = new TextField.StyleSheet();
		styleObj.onLoad = function(success) {
			StylesHelper.ApplyStyles(this);
			var eventObject = {type:"onLoaded", target:parent, status:success};
			trace("SAStyles: dispatch styles onLoaded");
			parent.dispatchEvent(eventObject);
		};
	}

	public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }

	public function BeginLoad() {
		var currentDate:Date = new Date();
		var url:String = "../css/flash.css?time=" + currentDate.getTime();
		if (SALocalWork.IsLocalWork)
			url = "../Neato.Cpt.WebDesigner/css/flash.css";
		BrowserHelper.InvokePageScript("log", url);
		styleObj.clear();
		styleObj.load(url);
		BrowserHelper.InvokePageScript("log", "SAStyles.as: css loaded");
    }
}