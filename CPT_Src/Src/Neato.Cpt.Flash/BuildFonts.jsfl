﻿var fontInfoList = new Array (
	//Times New Roman
    {
        fontName:"Times New Roman",
		symbolName:"times_n",
        bold:false,
        italic:false,
        embedRanges:"5"
    },
	{
        fontName:"Times New Roman",
		symbolName:"times_i",
        bold:false,
        italic:true,
        embedRanges:"5"
    },
	{
        fontName:"Times New Roman",
		symbolName:"times_b",
        bold:true,
        italic:false,
        embedRanges:"5"
    },
	{
        fontName:"Times New Roman",
		symbolName:"times_bi",
        bold:true,
        italic:true,
        embedRanges:"5"
    },
	//Courier New
	{
        fontName:"Courier New",
		symbolName:"cour_n",
        bold:false,
        italic:false,
        embedRanges:"5"
    },
    {
        fontName:"Courier New",
		symbolName:"cour_i",
        bold:false,
        italic:true,
        embedRanges:"5"
    },
    {
        fontName:"Courier New",
		symbolName:"cour_b",
        bold:true,
        italic:false,
        embedRanges:"5"
    },
    {
        fontName:"Courier New",
		symbolName:"cour_bi",
        bold:true,
        italic:true,
        embedRanges:"5"
    },
	//Arial
	{
        fontName:"Arial",
		symbolName:"arial_n",
        bold:false,
        italic:false,
        embedRanges:"5"
    },
    {
        fontName:"Arial",
		symbolName:"arial_i",
        bold:false,
        italic:true,
        embedRanges:"5"
    },
    {
        fontName:"Arial",
		symbolName:"arial_b",
        bold:true,
        italic:false,
        embedRanges:"5"
    },
    {
        fontName:"Arial",
		symbolName:"arial_bi",
        bold:true,
        italic:true,
        embedRanges:"5"
    },
	//Arial Narrow
	{
        fontName:"Arial Narrow",
		symbolName:"arialn_n",
        bold:false,
        italic:false,
        embedRanges:"5"
    },
    {
        fontName:"Arial Narrow",
		symbolName:"arialn_i",
        bold:false,
        italic:true,
        embedRanges:"5"
    },
    {
        fontName:"Arial Narrow",
		symbolName:"arialn_b",
        bold:true,
        italic:false,
        embedRanges:"5"
    },
    {
        fontName:"Arial Narrow",
		symbolName:"arialn_bi",
        bold:true,
        italic:true,
        embedRanges:"5"
    },
	//Arial Black
	{
        fontName:"Arial Black",
		symbolName:"ariblk_n",
        bold:false,
        italic:false,
        embedRanges:"5"
    },
	//Comic Sans MS
	{
        fontName:"Comic Sans MS",
		symbolName:"comic_n",
        bold:false,
        italic:false,
        embedRanges:"5"
    },
    {
        fontName:"Comic Sans MS",
		symbolName:"comic_b",
        bold:true,
        italic:false,
        embedRanges:"5"
    },
	//Impact
	{
        fontName:"Impact",
		symbolName:"impact_n",
        bold:false,
        italic:false,
        embedRanges:"5"
    },
	//Monotype Corsiva
	{
        fontName:"Monotype Corsiva",
		symbolName:"mtcorsva_n",
        bold:false,
        italic:false,
        embedRanges:"5"
    },
	//Symbol
	{
        fontName:"Symbol",
		symbolName:"symbol_n",
        bold:false,
        italic:false,
        embedRanges:"5"
    },
	//Bookman Old Style
	{
        fontName:"Bookman Old Style",
		symbolName:"bookos_n",
        bold:false,
        italic:false,
        embedRanges:"5"
    },
    {
        fontName:"Bookman Old Style",
		symbolName:"bookos_i",
        bold:false,
        italic:true,
        embedRanges:"5"
    },
    {
        fontName:"Bookman Old Style",
		symbolName:"bookos_b",
        bold:true,
        italic:false,
        embedRanges:"5"
    },
    {
        fontName:"Bookman Old Style",
		symbolName:"bookos_bi",
        bold:true,
        italic:true,
        embedRanges:"5"
    },
	//Century Schoolbook
    {
        fontName:"Century Schoolbook",
		symbolName:"schlbk_n",
        bold:false,
        italic:false,
        embedRanges:"5"
    },
    {
        fontName:"Century Schoolbook",
		symbolName:"schlbk_i",
        bold:false,
        italic:true,
        embedRanges:"5"
    },
    {
        fontName:"Century Schoolbook",
		symbolName:"schlbk_b",
        bold:true,
        italic:false,
        embedRanges:"5"
    },
    {
        fontName:"Century Schoolbook",
		symbolName:"schlbk_bi",
        bold:true,
        italic:true,
        fileName:"schlbkbi.fla",
        embedRanges:"5"
    },
	//Trebuchet MS
    {
        fontName:"Trebuchet MS",
		symbolName:"trebuc_n",
        bold:false,
        italic:false,
        embedRanges:"5"
    },
    {
        fontName:"Trebuchet MS",
		symbolName:"trebuc_i",
        bold:false,
        italic:true,
        embedRanges:"5"
    },
    {
        fontName:"Trebuchet MS",
		symbolName:"trebuc_b",
        bold:true,
        italic:false,
        embedRanges:"5"
    },
    {
        fontName:"Trebuchet MS",
		symbolName:"trebuc_bi",
        bold:true,
        italic:true,
        embedRanges:"5"
    },
	//Verdana
    {
        fontName:"Verdana",
		symbolName:"verdana_n",
        bold:false,
        italic:false,
        embedRanges:"5"
    },
    {
        fontName:"Verdana",
		symbolName:"verdana_i",
        bold:false,
        italic:true,
        embedRanges:"5"
    },
    {
        fontName:"Verdana",
		symbolName:"verdana_b",
        bold:true,
        italic:false,
        embedRanges:"5"
    },
    {
        fontName:"Verdana",
		symbolName:"verdana_bi",
        bold:true,
        italic:true,
        embedRanges:"5"
    }
);

var designerDir = "../Neato.Cpt.WebDesigner/Flash/Fonts/";
var testDir = "Flash/Fonts/";

for(var info in fontInfoList){
    publishFont(info, designerDir);
    publishFont(info, testDir);
}

function publishFont(info, dir) {
    var doc = CreateDoc(info);
			
	var symbolName = fontInfoList[info].symbolName;
	var flaName = symbolName + "_lib.fla";
	var linkageUrl = symbolName + "_lib.swf";

	var lib = doc.library;
	lib.items[0].linkageExportForAS = true;
	lib.items[0].linkageExportForRS = true;
	lib.items[0].linkageURL = linkageUrl;

    doc.importPublishProfile("file:///FontPublishProfile.xml");
    doc.currentPublishProfile = "FontPublishProfile";
    doc.exportSWF("file:///" + dir + flaName, true);
	doc.close(false);
	
	doc = CreateDoc(info);
	flaName = symbolName + ".fla";

	lib = doc.library;
	lib.items[0].linkageImportForRS = true;
	lib.items[0].linkageURL = "../Flash/Fonts/" + linkageUrl;

	doc.importPublishProfile("file:///FontPublishProfile.xml");
    doc.currentPublishProfile = "FontPublishProfile";
	doc.exportSWF("file:///" + dir + flaName, true);
	doc.close(false);
}

function CreateDoc(info) {
	var doc = flash.createDocument("timeline");
	
	doc.addNewText({left:0, top:0, right:300, bottom:50});
    doc.setTextString(fontInfoList[info].fontName);
	var txt = doc.selection[0];
    txt.textType = "dynamic";
    txt.setTextAttr("face", fontInfoList[info].fontName);
    txt.setTextAttr("size", 24);
    txt.setTextAttr("bold", fontInfoList[info].bold);
    txt.setTextAttr("italic", fontInfoList[info].italic);
    txt.embedRanges = fontInfoList[info].embedRanges;
	
	doc.convertToSymbol("movie clip", fontInfoList[info].symbolName, "top left");
	
	return doc;
}

