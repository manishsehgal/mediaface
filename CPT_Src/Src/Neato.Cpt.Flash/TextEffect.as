﻿import Geometry.*
import mx.utils.Delegate;
import mx.core.UIObject;

[event("redraw")]
class TextEffect extends UIObject {
	
	private var pluginsService:SAPlugins;
	public var effect;
	private var mc:MovieClip;
	public var text:String;
	public var color:Number;
	public var bold:Boolean;
	public var italic:Boolean;
	public var contourNode:XMLNode;
	public var bChanged:Boolean = true;
	public var fontTTF:String;
	public var size:Number = 12;
	private var owner;
	private var needFontName:String;
	private var saFont:SAFont;
	private var fontFile:XML;
	
	
	function TextEffect(mc:MovieClip,owner){
		pluginsService = SAPlugins.GetInstance();
		pluginsService.RegisterOnInvokedHandler(this, onResponse);
		needFontName = "";
		this.mc = mc;
		this.owner = owner;
		bold = false;
		italic = false;
		textString = " ";
		contourNode = null;
		fontTTF = "SCHLBKB.TTF";
	}
	
	function Draw():Void {
		trace("TextEffect.draw begin");
					
		if ((SAPlugins.IsAvailable() == false ||  owner.bChanged == false) && owner.contour != null)
		{
			mc.clear();
			DrawByNode(owner.contour);
			return;
		}
		mc.clear();
		/*trace("Plugin avaible");
		mc.clear();
		
		var cmd:XML = new XML();
		var CmdInfo:XMLNode = cmd.createElement("CmdInfo");
		CmdInfo.attributes.Type = "Request";
		cmd.appendChild(CmdInfo);
		
		var Request:XMLNode = cmd.createElement("Request");
		Request.attributes.Name = "TEXTEFFECT_GET_CONTOUR";
		CmdInfo.appendChild(Request);
		
		var Params:XMLNode = cmd.createElement("Params");
		Request.appendChild(Params);
		
		var Text:XMLNode = cmd.createElement("Text");
		var TextValue:XMLNode = cmd.createTextNode(text);
		Text.appendChild(TextValue);
		//Text.attributes.text = text;
		trace("TextEffect.as:"+text);
		var Style:XMLNode = cmd.createElement("Style");
		Style.attributes.font = "SCHLBKB.TTF";
		Style.attributes.bold = bold == true ? 1 : 0;
		Style.attributes.italic = italic == true ? 1 : 0;
		Text.appendChild(Style);
		
		Params.appendChild(Text.cloneNode(true));
		
		var Desc:XMLNode = cmd.createElement("EffectDesc");
		Desc.attributes.Type = "Curved";
		var guides:String = effect.EncodeGuideLines(effect);
		trace("Encoded guides:"+guides);
		var DescText:XMLNode = cmd.createTextNode(guides);//"A:0:50:300:200.57:600:50:A:0:300:300:360:600:300:");
		Desc.appendChild(DescText);
		Params.appendChild(Desc);
		
		//if(contourNode==null || guides!=lastGuides || guides == null || lastText != text)

		//if(owner.bChanged)
		//{
			//lastGuides = guides;
			//lastText = text;
			pluginsService.InvokeCommand(cmd);
		//}
		//else 
			*/
		
		
		var cmd:XML = new XML();
		var CmdInfo:XMLNode = cmd.createElement("CmdInfo");
		CmdInfo.attributes.Type = "Request";
		cmd.appendChild(CmdInfo);
		
		var Request:XMLNode = cmd.createElement("Request");
		Request.attributes.Name = "TEXTEFFECT_GET_CONTOUR";
		CmdInfo.appendChild(Request);
		
		var Params:XMLNode = cmd.createElement("Params");
		Request.appendChild(Params);
		
		var TextEl:XMLNode = cmd.createElement("Text");
		var Text:XMLNode = cmd.createTextNode(text);
		TextEl.appendChild(Text);
		
		var Style:XMLNode = cmd.createElement("Style");
		Style.attributes.font = SALocalWork.IsLocalWork?"SCHLBKB.TTF":fontTTF;
		//Style.attributes.font = "arial.ttf";
		//Style.attributes.font = "tahomabd.ttf";
		
		Style.attributes.bold =  bold == true ? 1 : 0;;
		Style.attributes.italic =  italic == true ? 1 : 0;;
		Style.attributes.size = size;
		TextEl.appendChild(Style);
		
		Params.appendChild(TextEl);
		
		var Desc:XMLNode = cmd.createElement("EffectDesc");
		Desc.attributes.Type = effect.type;
		var bounds = mc.getBounds();
		Desc.attributes.Width = bounds.xMax - bounds.xMin;
		Desc.attributes.Height = bounds.yMax - bounds.yMin;
		
		var guides:String = effect.EncodeGuideLines(effect);
		trace("Encoded guides:"+guides);
		var DescText:XMLNode = cmd.createTextNode(guides);//"L:0:50:600:50:B:0:300:200:660:400:-40:600:300:");
		
		Desc.appendChild(DescText);
		Params.appendChild(Desc);
		pluginsService.InvokeCommand(cmd);
		Preloader.Show();
		trace("TextEffect.draw end");
	}

	function onResponse(eventObject) {

		var reply:XML = new XML();
		reply.parseXML(eventObject.result);
		
		
		var CmdInfo:XMLNode = reply.firstChild;
		var Response:XMLNode = GetChild(CmdInfo, "Response");
		if (Response == undefined) return;
		if (CmdInfo.attributes.Name == "TEXTEFFECT_GET_CONTOUR") {
			if (Response.attributes.Error != "0") return;
			
			var Params:XMLNode = GetChild(Response, "Params");
			if (Params == undefined) return;
			
			var Status:XMLNode = GetChild(Params, "Status");
			if (Status == undefined) return;
			
			//==================================================
			if (Status.attributes.status == "Ok") {
				
				var Contour:XMLNode = GetChild(Params, "Contour");
				if (Contour == undefined) return;
				Preloader.Hide();
				DrawByNode(Contour);
				owner.contour= Contour;
				owner.bChanged = false;
			
			}
			else if (Status.attributes.status == "NeedFont") {
				if (this.needFontName == Status.attributes.font) return;
				this.needFontName = Status.attributes.font;
				this.fontFile = new XML();
				this.saFont = SAFont.GetInstance(this.fontFile);
			    this.saFont.RegisterOnLoadedHandler(this, onFontFileLoaded);
				this.saFont.BeginLoadFontFile(Status.attributes.font);
				Preloader.Show();
			}
		}
		else if (CmdInfo.attributes.Name == "TEXTEFFECT_SET_FONT") {
			Preloader.Hide();
			if (Response.attributes.Error != "0") return;
			this.needFontName = "";
			Draw();
		}
		
		
	}
	function DrawByNode(Contour:XMLNode) {
				trace("drawByNode");
				mc.beginFill(color);
				var cont:String = Contour.firstChild.nodeValue;

				
				var pos:Number = 0;
				var p0:Number = 0;
				var p1:Number = 0;
				if (cont.length<=1) return;
				while (true) {
					var code:String = cont.charAt(pos);
					pos++;
					if (code == "E") break;
					
					switch(code) {
						case "M": {
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var x = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var y = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
							mc.moveTo(x, y);
							break;
						}
						case "L": {
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var x = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var y = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
							mc.lineTo(x, y);
							break;
						}
						case "C": {
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var cx = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var cy = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var x = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var y = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
							mc.curveTo(cx, cy, x, y);
							break;
						}
						case "B": {
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var x0 = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var y0 = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var x1 = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var y1 = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var x2 = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var y2 = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var x3 = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
							p0 = pos + 1;
							p1 = cont.indexOf(":", p0);
							if (p1 == -1) break;
							var y3 = parseFloat(cont.substr(p0, p1-p0));
							pos = p1;
							
						    GeometryHelper.DrawCubicBezier2(mc,{x:x0, y:y0}, {x:x1, y:y1}, {x:x2, y:y2}, {x:x3, y:y3});
							break;
						}
					}
				}
				//race("Contour.nodeValue"+);
				
				
				//Contour.
			/*	for(var node:XMLNode=Contour.firstChild; node!=null; node=node.nextSibling){
					switch(node.nodeName) {
						case "M": {
							var x:Number = parseFloat(node.attributes.x);
							var y:Number = parseFloat(node.attributes.y);
							mc.moveTo(x, y);
							break;
						}
						case "L": {
							var x:Number = parseFloat(node.attributes.x);
							var y:Number = parseFloat( node.attributes.y);
							mc.lineTo(x, y);
							break;
						}
						case "C": {
							var x:Number = parseFloat(node.attributes.x);
							var y:Number = parseFloat(node.attributes.y);
							var cx:Number = parseFloat(node.attributes.cx);
							var cy:Number = parseFloat(node.attributes.cy);
							mc.curveTo(cx, cy, x, y);
							break;
						}
						case "B": {
							var x0:Number = parseFloat(node.attributes.x0);
							var y0:Number = parseFloat(node.attributes.y0);
							var x1:Number = parseFloat(node.attributes.x1);
							var y1:Number = parseFloat(node.attributes.y1);
							var x2:Number = parseFloat(node.attributes.x2);
							var y2:Number = parseFloat(node.attributes.y2);
							var x3:Number = parseFloat(node.attributes.x3);
							var y3:Number = parseFloat(node.attributes.y3);
							GeometryHelper.DrawCubicBezier2(mc,{x:x0, y:y0}, {x:x1, y:y1}, {x:x2, y:y2}, {x:x3, y:y3});
							break;
						}
					}
				}*/
				mc.endFill();
				/*mc.lineStyle(1,0x007FFF);
				var a = mc.getBounds(mc);
				mc.moveTo(a.xMin,a.yMin);
				mc.lineTo(a.xMin,a.yMax);
				mc.lineTo(a.xMax,a.yMax);
				mc.lineTo(a.xMax,a.yMin);
				mc.lineTo(a.xMin,a.yMin);
				mc.moveTo(1,1);
				mc.lineTo(mc._width-1,1);
				mc.lineTo(mc._width-1,mc._height-1);
				mc.lineTo(1,mc._height-1);
				mc.lineTo(1,1);*/
				

		OnRedraw();
		
	}
	public function set textString(value) {
		//lastGuides == null;
		text = value;
		owner.bChanged = true;
	}
	function GetChild(parNode:XMLNode, childName:String) : XMLNode {
		for(var node:XMLNode=parNode.firstChild; node!=null; node=node.nextSibling){
			if (node.nodeName == childName) {
				return node;
			}
		}
		return undefined;
	}

	function onFontFileLoaded(eventObject) {
		if (this.fontFile.firstChild.attributes.status != "Ok") return;
		
		var bin:String = this.fontFile.firstChild.firstChild.toString();
		if (bin == undefined || bin.length == 0) return;
		
		var cmd:XML = new XML();
		var CmdInfo:XMLNode = cmd.createElement("CmdInfo");
		CmdInfo.attributes.Type = "Request";
		cmd.appendChild(CmdInfo);
		
		var Request:XMLNode = cmd.createElement("Request");
		Request.attributes.Name = "TEXTEFFECT_SET_FONT";
		CmdInfo.appendChild(Request);
		
		var Params:XMLNode = cmd.createElement("Params");
		Request.appendChild(Params);
		
		var Font:XMLNode = cmd.createElement("Font");
		Font.attributes.Name = this.needFontName;
		Params.appendChild(Font);
		
		var Bin:XMLNode = cmd.createTextNode(bin);
		Font.appendChild(Bin);
		
		pluginsService.InvokeCommand(cmd);
	}
	private function OnRedraw() {
		var eventObject = {type:"redraw", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnRedrawHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("redraw", Delegate.create(scopeObject, callBackFunction));
    }

}
