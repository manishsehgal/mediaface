﻿var swfList = new Array (
    {src:"file:///SkinLib.fla"},
    {src:"file:///Toolbar.fla"},
    {src:"file:///Minibar.fla"},
    {src:"file:///Menu.fla"},
    {src:"file:///PreviewMenu.fla"},
    {src:"file:///DesignerMenu.fla"},
    {src:"file:///ToolProperties.fla"},
    {src:"file:///HintBox.fla"},
    {src:"file:///FaceTab.fla"},
    {src:"file:///PreviewDescription.fla"},
	{src:"file:///ZoomTool.fla"},
    {src:"file:///QuickTool.fla"},
    {src:"file:///Preloader.fla"},
    {src:"file:///Header.fla"},
    {src:"file:///SetVar.fla"},
    {src:"file:///Designer.fla"},
    {src:"file:///Starter.fla"}
);

for(var info in swfList){
    fl.openDocument(swfList[info].src);
    var doc = fl.documents[0];
    fl.trace(doc.name);
    for (profileIndex in doc.publishProfiles) {
        var profile = doc.publishProfiles[profileIndex];
        doc.currentPublishProfile = profile.toString();
        fl.trace("profile:"+ doc.currentPublishProfile);
        doc.publish();
    }
    doc.close();
}