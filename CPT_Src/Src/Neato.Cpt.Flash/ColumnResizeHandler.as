﻿import mx.utils.Delegate;
class ColumnResizeHandler extends mx.core.UIObject {
	private var xMouseBegin:Number = 0;
	private var yMouseBegin:Number = 0;
    var colNum:Number;
	function ColumnResizeHandler() {
		this.onPress = drag;
		this.onMouseMove = null;
		this.onMouseUp = mouseUp;
		this.onRollOver = rollOver;
		this.onRollOut = rollOut;
		GotoAndStop(1);
	}
	
	function drag() {
		trace("resize drag");
		this.onMouseMove = mouseMove;
		xMouseBegin = _xmouse;
		yMouseBegin = _ymouse;
		GotoAndStop(3);
	}
	
	function noDrag() {
		trace("resize noDrag");
		this.onMouseMove = null;
		GotoAndStop(1);
	}
	
	function mouseMove() {
		var keepProportionOnly:Boolean = false;
		_global.CurrentUnit.ResizeColumnWidth(this.colNum,_xmouse - xMouseBegin);
		_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
	}
	
	function rollOver() {
		GotoAndStop(2);
	}
	
	function rollOut() {
		GotoAndStop(1);
	}
		
	function mouseUp() {
		if (onMouseMove != null)
			noDrag();
	}
}