﻿class StylesHelper {
	static private var styleGlobal:TextField.StyleSheet;
	
	static function ApplyStyles(styleSheetGlobal:TextField.StyleSheet) {
		styleGlobal = styleSheetGlobal;
		ApplyFlashControlStyles();
		ApplyGenericStyles();
		ApplyMenuPanelStyles();
		ApplyMiniBarPanelStyles();
		ApplyToolbarPanelStyles();
		ApplyQuickToolPanelStyles();
		ApplyToolbarButtonStyles();
		ApplyToolPropertiesPanelStyles();
		ApplyTransferPropertiesStyles();
		ApplyHintBoxPanelStyles();
		ApplyFaceTabPanelStyles();
		ApplyPreviewPanelStyles();
		ApplyMainLayerPanelStyles();
		ApplySliderStyles();
		ApplyTitleStyles();
		ApplyHeaderControlStyles();
	}
	
	private static function CopyStyles(source:Object, destination:Object) {
		for (var styleName in source) {
			destination.setStyle(styleName, source[styleName]);
		}
	}
	
	private static function ApplyFlashControlStyles() {
		//ComboBox
		if (_global.styles.ComboBox == undefined) {
		  _global.styles.ComboBox = new mx.styles.CSSStyleDeclaration();
		}
		var styleObj = _global.styles.ComboBox;
		CopyStyles(styleGlobal.getStyle(".ComboBox"), styleObj);

		//NumericStepper
		if (_global.styles.NumericStepper == undefined) {
		  _global.styles.NumericStepper = new mx.styles.CSSStyleDeclaration();
		}
		styleObj = _global.styles.NumericStepper;
		CopyStyles(styleGlobal.getStyle(".NumericStepper"), styleObj);

		//TextInput
		if (_global.styles.TextInput == undefined) {
		  _global.styles.TextInput = new mx.styles.CSSStyleDeclaration();
		}
		styleObj = _global.styles.TextInput;
		CopyStyles(styleGlobal.getStyle(".TextInput"), styleObj);

		//CheckBox
		if (_global.styles.CheckBox == undefined) {
		  _global.styles.CheckBox = new mx.styles.CSSStyleDeclaration();
		}
		styleObj = _global.styles.CheckBox;
		CopyStyles(styleGlobal.getStyle(".CheckBox"), styleObj);
		
		
		//CheckBoxBold
		if (_global.styles.CheckBoxBold == undefined) {
		  _global.styles.CheckBoxBold = new mx.styles.CSSStyleDeclaration();
		}
		styleObj = _global.styles.CheckBoxBold;
		CopyStyles(styleGlobal.getStyle(".CheckBoxBold"), styleObj);

		//Button
		if (_global.styles.Button == undefined) {
		  _global.styles.Button = new mx.styles.CSSStyleDeclaration();
		}
		styleObj = _global.styles.Button;
		CopyStyles(styleGlobal.getStyle(".Button"), styleObj);

		//Tree
		if (_global.styles.Tree == undefined) {
		  _global.styles.Tree = new mx.styles.CSSStyleDeclaration();
			for (var style in _global.styles.ScrollSelectList) {
			  _global.styles.Tree.setStyle(style, _global.styles.ScrollSelectList.getStyle(style));
			}
		}
		styleObj = _global.styles.Tree;
		CopyStyles(styleGlobal.getStyle(".Tree"), styleObj);
		
		//Accordion
		if (_global.styles.Accordion == undefined) {
		  _global.styles.Accordion = new mx.styles.CSSStyleDeclaration();
		}
		styleObj = _global.styles.Accordion;
		CopyStyles(styleGlobal.getStyle(".Accordion"), styleObj);

		//HList
		if (_global.styles.HList == undefined) {
		  _global.styles.HList = new mx.styles.CSSStyleDeclaration();
		}
		styleObj = _global.styles.HList;
		CopyStyles(styleGlobal.getStyle(".HList"), styleObj);
		
		//Alert
		if (_global.styles.Alert == undefined) {
		  _global.styles.Alert = new mx.styles.CSSStyleDeclaration();
		}
		styleObj = _global.styles.Alert;
		CopyStyles(styleGlobal.getStyle(".Alert"), styleObj);
	}

	private static function ApplyGenericStyles() {
		//GenericPanel
		var styleObj:Object = _global.styles.GenericPanel = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".GenericPanel"), styleObj);

		//Panel
		styleObj = _global.styles.Panel = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".Panel"), styleObj);

		//HeaderText
		styleObj = _global.styles.HeaderText = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".HeaderText"), styleObj);

		//Caption
		styleObj = _global.styles.Caption = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".Caption"), styleObj);

		//Note
		styleObj = _global.styles.Note = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".Note"), styleObj);
	}
	
	private static function ApplyMainLayerPanelStyles() {
		var styleObj = _global.styles.MainLayerPanel = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".MainLayerPanel"), styleObj);
	}
	
	private static function ApplyMenuPanelStyles() {
		var styleObj:Object = _global.styles.MenuPanel = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".MenuPanel"), styleObj);
		
		styleObj = _global.styles.MenuPanelLargeButton = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".MenuPanel"), styleObj);
		CopyStyles(styleGlobal.getStyle(".MenuPanel.LargeButton"), styleObj);
		
		styleObj = _global.styles.MenuPanelModelName = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".MenuPanel"), styleObj);
		CopyStyles(styleGlobal.getStyle(".MenuPanel.ModelName"), styleObj);
		
		styleObj = _global.styles.MenuPanelPreview = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".MenuPanel"), styleObj);
		CopyStyles(styleGlobal.getStyle(".MenuPanel.Preview"), styleObj);
	}
	
	private static function ApplyMiniBarPanelStyles() {
		var styleObj = _global.styles.MiniBarPanel = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".MiniBarPanel"), styleObj);
	}
	
	private static function ApplyToolbarPanelStyles() {
		var styleObj = _global.styles.ToolbarPanel = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolbarPanel"), styleObj);
	}
	
	private static function ApplyQuickToolPanelStyles() {
		var styleObj = _global.styles.QuickToolPanel = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".QuickToolPanel"), styleObj);
	}
	
	private static function ApplyToolbarButtonStyles() {
		var styleObj = _global.styles.ToolbarButton = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".Button"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolbarButton"), styleObj);

		styleObj = _global.styles.ToolbarButtonUp = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolbarButton"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolbarButton.Up"), styleObj);

		styleObj = _global.styles.ToolbarButtonDown = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolbarButton"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolbarButton.Down"), styleObj);

		styleObj = _global.styles.ToolbarButtonOver = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolbarButton"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolbarButton.Over"), styleObj);

		styleObj = _global.styles.ToolbarButtonDisabled = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolbarButton"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolbarButton.Disabled"), styleObj);
	}
	
	private static function ApplyHintBoxPanelStyles() {
		var styleObj = _global.styles.HintBoxPanel = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".HintBoxPanel"), styleObj);
		styleObj = _global.styles.HintBoxPanelPreview = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".HintBoxPanelPreview"), styleObj);
	}
	
	private static function ApplyFaceTabPanelStyles() {
		var styleObj = _global.styles.FaceTabPanel = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".FaceTabPanel"), styleObj);

		styleObj = _global.styles.FaceTabPanelFaceFront = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".FaceTabPanel.FaceFront"), styleObj);

		styleObj = _global.styles.FaceTabPanelFaceBack = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".FaceTabPanel.FaceBack"), styleObj);
	}
	
	private static function ApplyPreviewPanelStyles() {
		var styleObj = _global.styles.PreviewPanel = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".Preview"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Preview.Panel"), styleObj);
		
		styleObj = _global.styles.PreviewCaptionText = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".Preview"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Preview.CaptionText"), styleObj);
		
		styleObj = _global.styles.PreviewDescriptionText = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".Preview"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Preview.DescriptionText"), styleObj);
		
		styleObj = _global.styles.PreviewABValueCaption = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".Preview"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Preview.ABValueCaption"), styleObj);
		
		styleObj = _global.styles.PreviewABValueInput = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".Preview"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Preview.ABValueInput"), styleObj);
		
		styleObj = _global.styles.PreviewAdvancedCalibrationText = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".Preview"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Preview.AdvancedCalibrationText"), styleObj);
		
		styleObj = _global.styles.PreviewButtons = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".Preview"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Preview.Buttons"), styleObj);
	}
	
	private static function ApplyToolPropertiesPanelStyles() {
		var styleObj:Object = _global.styles.ToolProperties = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);

		styleObj = _global.styles.ToolPropertiesPanel = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Panel"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.Panel"), styleObj);

		styleObj = _global.styles.ToolPropertiesSubPanel = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Panel"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.Panel"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.Panel.SubPanel"), styleObj);

		styleObj = _global.styles.ToolPropertiesText = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Text"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.Text"), styleObj);

		styleObj = _global.styles.ToolPropertiesHeaderText = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".HeaderText"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.HeaderText"), styleObj);
		
		styleObj = _global.styles.ToolPropertiesCombo = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ComboBox"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.ComboBox"), styleObj);
		
		styleObj = _global.styles.ToolPropertiesSpin = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".NumericStepper"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.NumericStepper"), styleObj);
		
		styleObj = _global.styles.ToolPropertiesInputText = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".TextInput"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.TextInput"), styleObj);
		
		styleObj = _global.styles.ToolPropertiesSearchText = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".TextInput"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.TextInput"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.TextSearch"), styleObj);
		
		styleObj = _global.styles.ToolPropertiesCheckBox = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".CheckBox"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.CheckBox"), styleObj);
		
		styleObj = _global.styles.ToolPropertiesCheckBoxBold = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".CheckBoxBold"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.CheckBoxBold"), styleObj);

		styleObj = _global.styles.ToolPropertiesButton = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Button"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.Button"), styleObj);
		
		styleObj = _global.styles.ToolPropertiesActiveButton = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Button"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.Button"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.Button.ActiveButton"), styleObj);
		
		styleObj = _global.styles.ToolPropertiesActiveButtonSmall = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Button"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.Button"), styleObj);
		CopyStyles(styleGlobal.getStyle(".ToolProperties.Button.ActiveButtonSmall"), styleObj);
		
		styleObj = _global.styles.ToolPropertiesCaption = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Caption"), styleObj); 
		CopyStyles(styleGlobal.getStyle(".ToolProperties.Caption"), styleObj); 
		
		styleObj = _global.styles.ToolPropertiesNote = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Note"), styleObj); 
		CopyStyles(styleGlobal.getStyle(".ToolProperties.Note"), styleObj); 

		styleObj = _global.styles.ToolPropertiesTree = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".ToolProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".Tree"), styleObj); 
		CopyStyles(styleGlobal.getStyle(".ToolProperties.Tree"), styleObj); 
	}
	
	private static function ApplyTransferPropertiesStyles() {
		var styleObj:Object = _global.styles.TransferProperties = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".TransferProperties"), styleObj);
		
		styleObj = _global.styles.TransferPropertiesText = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".TransferProperties"), styleObj);
		CopyStyles(styleGlobal.getStyle(".TransferProperties.Text"), styleObj); 
	}

	
	private static function ApplySliderStyles() {
		var styleObj:Object = _global.styles.SliderTitleText = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".SliderTitleText"), styleObj);
		
		styleObj = _global.styles.SliderInputText = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".SliderInputText"), styleObj);
		
		styleObj = _global.styles.CalibrationText = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".CalibrationText"), styleObj);
		
		styleObj = _global.styles.CalibrationLabel = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".CalibrationLabel"), styleObj); 
	}
	
	private static function ApplyTitleStyles() {
		var styleObj:Object = _global.styles.TitleText = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".TitleText"), styleObj);
	}
	
	private static function ApplyHeaderControlStyles() {
		var styleObj:Object = _global.styles.HeaderControlTextNormal = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".HeaderControlTextNormal"), styleObj);
		
		styleObj = _global.styles.HeaderControlTextBold = new mx.styles.CSSStyleDeclaration();
		CopyStyles(styleGlobal.getStyle(".HeaderControlTextBold"), styleObj);
	}
	
	public static function GetStyle(styleName:String) {
		return styleGlobal.getStyle(styleName);
	}
}
