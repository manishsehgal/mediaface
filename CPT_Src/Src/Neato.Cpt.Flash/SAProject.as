﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;
[Event("onSaved")]
[Event("onLoaded")]
class SAProject {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SAProject.prototype);

	private var project:XML;
	private var saveResultXml:XML;

	public static function GetInstance(project:XML):SAProject {
		return new SAProject(project);
	}
	
	public function get IsLoaded():Boolean {
		return project.loaded;
	}

	private function SAProject(project:XML) {
		var parent = this;
		
		this.project = project;
		this.project.onLoad = function(success) {
			var eventObject = {type:"onLoaded", target:parent, result:this};
			trace("SAProject: dispatch project onLoaded");
			parent.dispatchEvent(eventObject);
		};
		
		this.saveResultXml = new XML();
		this.saveResultXml.onLoad = function(success) {
			var eventObject = {type:"onSaved", target:parent, result:this};
			trace("SAProject: dispatch project onSaved");
			parent.dispatchEvent(eventObject);
		}
	}

	public function BeginLoad() {
        trace("SAProject BeginLoad");
		var currentDate:Date = new Date();
		var url:String = "../Project.aspx?type=XmlProject&time=" + currentDate.getTime();
		if (SALocalWork.IsLocalWork)
			url = "Motorola_V635.xml";
		BrowserHelper.InvokePageScript("log", url);
        this.project.load(url);
    }

    public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }

	public function SessionChecked() {
		var currentDate:Date = new Date();
		var url:String = "../Project.aspx?type=SaveXmlProject&time=" + currentDate.getTime();
		project.contentType = "text/xml";
		project.sendAndLoad(url, saveResultXml);
	}
	
    public function BeginSave() {
		trace("SAProject BeginSave");
		SASession.WaitForSessionChecked(Delegate.create(this,SessionChecked));
    }

	public function RegisterOnSavedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onSaved", Delegate.create(scopeObject, callBackFunction));
    }
}
