﻿class Geometry.GeometryHelper {

	public static var M_RAD_2_DEG:Number = 57.295779;
	public static var M_DEG_2_RAD:Number = 1.0 / M_RAD_2_DEG;
	/////////////////////////////////////////////////////////////////////////
	//
	// Add a drawCubicBezier2 to the movieClip prototype based on a MidPoint 
	// simplified version of the midPoint algorithm by Helen Triolo
	//
	/////////////////////////////////////////////////////////////////////////
	
	// This function will trace a cubic approximation of the cubic Bezier
	// It will calculate a serie of [control point/Destination point] which 
	// will be used to draw quadratic Bezier starting from P0
	public static function DrawCubicBezier(mc:MovieClip, x1, y1, x2, y2, x3, y3, x4, y4) {
		DrawCubicBezier2(mc, {x : x1, y : y1 },
							 {x : x2, y : y2 },
							 {x : x3, y : y3},
							 {x : x4, y : y4 });
	}
	public static function DrawCubicBezier2(mc:MovieClip,P0, P1, P2, P3) {

	// calculates the useful base points
		var PA = getPointOnSegment(P0, P1, 3/4);
		var PB = getPointOnSegment(P3, P2, 3/4);
		
		// get 1/16 of the [P3, P0] segment
		var dx = (P3.x - P0.x)/16;
		var dy = (P3.y - P0.y)/16;
	
		// calculates control point 1
		var Pc_1 = getPointOnSegment(P0, P1, 3/8);
	
		// calculates control point 2
		var Pc_2 = getPointOnSegment(PA, PB, 3/8);
		Pc_2.x -= dx;
		Pc_2.y -= dy;
		
		// calculates control point 3
		var Pc_3 = getPointOnSegment(PB, PA, 3/8);
		Pc_3.x += dx;
		Pc_3.y += dy;
	
		// calculates control point 4
		var Pc_4 = getPointOnSegment(P3, P2, 3/8);
	
		// calculates the 3 anchor points
		var Pa_1 = getMiddle(Pc_1, Pc_2);
		var Pa_2 = getMiddle(PA, PB);
		var Pa_3 = getMiddle(Pc_3, Pc_4);

		// draw the four quadratic subsegments
		mc.curveTo(Pc_1.x, Pc_1.y, Pa_1.x, Pa_1.y);
		mc.curveTo(Pc_2.x, Pc_2.y, Pa_2.x, Pa_2.y);
		mc.curveTo(Pc_3.x, Pc_3.y, Pa_3.x, Pa_3.y);
		mc.curveTo(Pc_4.x, Pc_4.y, P3.x, P3.y);
	}
	
	// return the distance between two points
	private static function distance(P0, P1):Number {
		var dx:Number = 0, dy:Number = 0;
		
		if(P0.x != undefined){
			dx = P0.x - P1.x;
			dy = P0.y - P1.y;
			return Math.sqrt(dx*dx + dy*dy);
		}
		else if(P0.X != undefined) {
			dx = P0.X - P1.X;
			dy = P0.Y - P1.Y;
			return Math.sqrt(dx*dx + dy*dy);
		}
	}

	// return the middle of a segment define by two points
	private static function getMiddle(P0, P1) {
		return {x: ((P0.x + P1.x) / 2), y: ((P0.y + P1.y) / 2)};
	}


	// return a point on a segment [P0, P1] which distance from P0
	// is ratio of the length [P0, P1]
	private static function getPointOnSegment(P0, P1, ratio) {
		return {x: (P0.x + ((P1.x - P0.x) * ratio)), y: (P0.y + ((P1.y - P0.y) * ratio))};
	}
	
	public static function DrawArcFrom3Points(mc, ax ,ay ,bx ,by ,cx ,cy) {
		//Private Sub FindCircle(ByVal ax As Single, ByVal ay As _
    //Single, ByVal bx As Single, ByVal by As Single, ByVal _
    //cx As Single, ByVal cy As Single, ByRef ox As Single, _
    //y As Single, ByRef radius As Single)
		/*var x1:Number;
		var y1:Number; 
		var dx1:Number;
		var dy1:Number;
		var x2:Number;
		var y2:Number; 
		var dx2:Number;
		var dy2:Number;
		var x3:Number;
		var y3:Number; 
		var dx:Number;
		var dy:Number;
		var x:Number;
		var y:Number;
		var radius:Number;
		var startAngle:Number=  0;
		var endAngle:Number=  0;
		//' Get the perpendicular bisector of (x1, y1) and (x2,
		//' y2).
		x1 = (bx + ax) / 2;
		trace(" x1 "+x1);
		y1 = (by + ay) / 2;
		trace(" y1 "+y1);
		dy1 = bx - ax;
		trace("dy1 "+dy1);
		dx1 = -(by - ay);
		trace("dx1 "+dx1);
	
		//' Get the perpendicular bisector of (x2, y2) and (x3,
		//' y3).
		x2 = (cx + bx) / 2;
		trace("x2 "+x2);
		y2 = (cy + by) / 2;
		trace("y2 "+y2);
		
		dy2 = cx - bx;
		trace("dy2 "+dy2);
		dx2 = -(cy - by);
		trace("dx2 "+dx2);
	
		x3 = (cx + ax) / 2;
		trace("x3 "+x3);
		y3 = (cy + ay) / 2;
		trace("y3 "+y3);
		var dy3:Number = cx - ax;
		trace("dy3 "+dy3);
		var dx3:Number = -(cy - ay);
		trace("dx3 "+dx3);
		
		//' See where the lines intersect.
		x = (y1 * dx1 * dx2 + x2 * dx1 * dy2 - x1 * dy1 * dx2 
			- y2 * dx1 * dx2) 
			/ (dx1 * dy2 - dy1 * dx2);
			trace("x "+x);
		y = (x - x1) * dy1 / dx1 + y1;
		trace("y "+y);
		dx = x - ax;
		trace("dx "+dx);
		
		dy = y - ay;
		trace("dy "+dy);
		radius = Math.sqrt(dx * dx + dy * dy);
		trace("radius "+radius);
		var tAngle:Number = 0;
		var a:Number = Math.sqrt(dx1 * dx1 + dy1 * dy1);
		trace("a = "+a);
		var b:Number = Math.sqrt(dx3 * dx3 + dy3 * dy3) / 2;// points 1 to 3 middle
		trace("b = "+b);
		tAngle = (Math.abs(Math.asin(b / a))* 180 / Math.PI ) ;
		trace("tAngle = "+tAngle);
		//tAngle = 45;
		//region calc angles
		//var tAngle = Math.asin(Math.sqrt(Math.pow(dx3 - x1,2) + Math.pow(dy3 - y1 , 2)) /
		//Math.sqrt(Math.pow(dx3 - x,2) + Math.pow(dy3 - y , 2)));*/
	var x0:Number = ax, x1:Number = bx, x2:Number = cx, X:Number;
	var y0:Number = ay, y1:Number = by, y2:Number = cy, Y:Number;
	var m_ptCenter = new Object({X:Number,Y:Number});
	var radius:Number = 0;
	if (x1 == x0)
	{
		// renumerating
		x1 = ax;
		x2 = bx;
		x0 = cx;
		y1 = ay;
		y2 = by;
		y0 = cy;
	}
	Y = 2 * ((y1 - y2) * (x1 - x0) - (y0 - y1) * (x2 - x1));
/*	if (Y == 0)
	{
		radius = 0;
		return;
	}*/
	Y = ((y2 + y1) * (y1 - y2) * (x1 - x0) - (y1 + y0) * (y0 - y1) * (x2 - x1) + (x0 - x2) * (x1 - x0) * (x2 - x1)) / Y;
	X = (Y - 0.5 * (y1 + y0)) * (y0 - y1) / (x1 - x0) + 0.5 * (x1 + x0);
	m_ptCenter.X = X;
	m_ptCenter.Y = Y;
	radius = distance(m_ptCenter, {X : ax, Y : ay});

	//VectorF vec0(m_pptArc[0] - m_ptCenter), vec1(m_pptArc[1] - m_ptCenter), vec2(m_pptArc[2] - m_ptCenter);
	var vec0=new Object();
	vec0.X = ax - m_ptCenter.X;
	vec0.Y = ay - m_ptCenter.Y;
	
	var vec1=new Object();
	vec1.X = bx - m_ptCenter.X;
	vec1.Y = by - m_ptCenter.Y;
	
	var vec2=new Object();
	vec2.X = cx - m_ptCenter.X;
	vec2.Y = cy - m_ptCenter.Y;
	
	var m_fStartAngle:Number = M_RAD_2_DEG * Math.atan2(vec0.Y, vec0.X);
	var m_fSweepAngle:Number = M_RAD_2_DEG * Math.atan2(vec2.Y, vec2.X);
	var fMidAngle:Number = M_RAD_2_DEG * Math.atan2(vec1.Y, vec1.X);
	trace("m_fStartAngle = "+m_fStartAngle+";m_fSweepAngle = " + m_fSweepAngle+";fMidAngle = " + fMidAngle);
	if (m_fStartAngle < 0)
		m_fStartAngle += 360.0;
	if (m_fSweepAngle < 0)
		m_fSweepAngle += 360.0;
	if (fMidAngle < 0)
		fMidAngle += 360.0;

	/*m_fSweepAngle -= m_fStartAngle;
	fMidAngle -= m_fStartAngle;

	if (!(m_fSweepAngle > 0 && 0 <= fMidAngle && fMidAngle <= m_fSweepAngle) && !(m_fSweepAngle <= 0 && m_fSweepAngle <= fMidAngle && fMidAngle <= 0))
	{
		if (m_fSweepAngle > 0)
			m_fSweepAngle -= 360;
		else
			m_fSweepAngle += 360;
	}*/
	//m_fStartAngle = m_fStartAngle % 360;
	//m_fSweepAngle = m_fSweepAngle % 360;
	trace("m_fStartAngle = "+m_fStartAngle+";m_fSweepAngle = " + m_fSweepAngle+";fMidAngle = " + fMidAngle);
	trace("X = "+vec1.Y+";Y = "+vec1.Y)
	var bInv:Boolean = (Math.sqrt(Math.pow(vec0.X - vec1.X, 2)+Math.pow(vec0.Y - vec1.Y, 2)) <= radius);
	if((m_fStartAngle > m_fSweepAngle && bInv) || (fMidAngle<m_fStartAngle)){
		var tmp:Number = m_fStartAngle;
		m_fStartAngle = m_fSweepAngle;
		m_fSweepAngle = tmp;
	}
	/*if(m_fSweepAngle-m_fStartAngle < 90) {
		var tmp:Number = m_fStartAngle;
		m_fStartAngle = m_fSweepAngle;
		m_fSweepAngle = tmp;
	}*/
	if(m_fStartAngle > 360)
		m_fStartAngle -= 360;
	if(m_fSweepAngle > 360)
		m_fSweepAngle -= 360;
	
	//m_fLength = Math.abs(M_DEG_2_RAD * m_fSweepAngle * m_fRadius);
		//endregion calc angles
		if(radius < 15000)
			DrawArc(mc, m_ptCenter.X , m_ptCenter.Y, radius, m_fStartAngle, m_fSweepAngle);
		else {
			mc.moveTo(ax, ay);
			mc.lineTo(cx, cy);
		}
		
	}
	/*public static function DrawArc(mc, x, y, radius, arc, startAngle, yRadius){
		if (yRadius == undefined) {
			yRadius=radius;
		}
		
		if(Math.abs(arc) > 360) {
			arc = 360;
		}
		segs = Math.ceil(Math.abs(arc) / 45);
		segAngle = arc / segs;
		theta =- (segAngle / 180) * Math.PI;
		angle =- (startAngle / 180) * Math.PI;
		ax = x - Math.cos(angle) * radius;
		ay = y - Math.sin(angle) * yRadius;
		if(segs > 0) {
			for(i = 0 ; i < segs; i++)	{
				angle += theta;
				angleMid = angle - (theta / 2);
				bx = ax + Math.cos(angle) * radius;
				by = ay + Math.sin(angle) * yRadius;
				cx = ax + Math.cos(angleMid) * (radius / Math.cos(theta / 2));
				cy = ay + Math.sin(angleMid) * (yRadius / Math.cos(theta / 2));
				mc.curveTo(cx, cy, bx, by)
				}
			}
		}*/
	public static function DrawArc (mc, x, y, radius, bA, eA) {
		if (eA < bA) eA += 360;
		var r = radius;
		var degToRad = Math.PI / 180 ;
		var n = Math.ceil((eA - bA) / 45);
		var theta = ((eA - bA) / n) * degToRad;
		var cr = radius/Math.cos(theta/2);
		var angle = bA*degToRad;
		var cangle = angle-theta/2;
		mc.moveTo(x+r*Math.cos(angle), y+r*Math.sin(angle));
		for (var i=0;i < n;i++) 
		{
			angle += theta;
			cangle += theta;
			var endX = r*Math.cos (angle);
			var endY = r*Math.sin (angle);
			var cX = cr*Math.cos (cangle);
			var cY = cr*Math.sin (cangle);
			mc.curveTo(x+cX,y+cY, x+endX,y+endY);
		}
		trace("Exit");

	}	
}