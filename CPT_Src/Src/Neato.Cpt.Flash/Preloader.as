﻿class Preloader {
	private static var counter:Number = 0;
	public static function Show() {
		++counter;
		trace("Preloader counter = "+counter);
		if (counter > 0) {
			_root.preloader._visible = true;
			//Mouse.hide();
		}
	}
	public static function Hide() {
		if (counter > 0)
			--counter;
		trace("Preloader counter = "+counter);
		if (counter < 1) {
			_root.preloader._visible = false;
			//Mouse.show();
		}
	}
}