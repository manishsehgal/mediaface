﻿#include "Utils.as"

BrowserHelper.InvokePageScript("log", "MainLogic.as");

_global.Faces;
_global.CurrentFace;
_global.CurrentUnit;
_global.Fonts;
_global.SelectionFrame;
_global.ModelName;
_global.Frames;
_global.DesignMode="Designer";

_global.ReadFaces = function(mc:MovieClip, projectXml:XML):Void {
	
	for (var i = 0; i < _global.Faces.length; i++) {
		var inst = _global.Faces[i]["mc"];
		if (inst != undefined) {
			inst.removeMovieClip();
		}
	}
	
	_global.Faces = new Array();
	
	var facesXml:XML = projectXml.childNodes[0].childNodes[0]; //Must be refactored
	var propertiesNode:XMLNode = projectXml.childNodes[0].childNodes[2]; //Must be refactored
	var currentFaceId:String = propertiesNode.attributes.currentFaceId;
	
	trace("Faces creation");
	for (var i = 0; i < facesXml.childNodes.length; ++i) {
		var mcFace = mc.createEmptyMovieClip("Face" + i, mc.getNextHighestDepth());
		var face:FaceUnit = new FaceUnit(mcFace, facesXml.childNodes[i]);
		trace("face mc="+mcFace);
		Faces.push(face);
		if (face.Id == currentFaceId) _global.CurrentFace = face;
	}
	trace("Faces="+_global.Faces);
	if (currentFaceId == undefined)
		_global.CurrentFace = _global.Faces[0];
	
	if(_global.Frames != undefined) {
	   for(var i in _global.Frames)
	        _global.Frames[i].removeMovieClip();
	}
	_global.Frames=new Array(3);
	_global.Frames["Frame"] = mc.attachMovie("Frame", "frame", mc.getNextHighestDepth());
	_global.Frames["TableFrame"] = mc.attachMovie("TableFrame", "tableframe", mc.getNextHighestDepth());
	_global.Frames["FrameEx"] = mc.attachMovie("FrameEx", "frameex", mc.getNextHighestDepth());
		if (_global.CurrentUnit == undefined) {
		_global.Frames["Frame"].DetachUnit();
		_global.Frames["TableFrame"].DetachUnit();
		_global.Frames["FrameEx"].DetachUnit();
		
	} else {
		_global.Frames["Frame"]._visible = false;
		_global.Frames["TableFrame"]._visible = false;	
		_global.Frames["FrameEx"]._visible = false;	
	}
	_global.SelectionFrame = _global.Frames["Frame"];

	_global.CurrentFace.Show();
	
	var paperXml:XML = projectXml.childNodes[0].childNodes[1]; //Must be refactored
	_root.pnlPreviewLayer.content.paper.ParseXml(paperXml);
	
	_global.ModelName = projectXml.childNodes[0].childNodes[2].attributes.name; //Must be refactored
	trace("exit project costructor");
}

_global.fontsXml = new XML();
_global.fontsXml.ignoreWhite = true;

_global.projectXml = new XML();
_global.projectXml.ignoreWhite = true;

_global.imagesXml = new XML();
_global.imagesXml.ignoreWhite = true;

_global.brushesXml = new XML();
_global.brushesXml.ignoreWhite = true;

_global.shapesXml = new XML();
_global.shapesXml.ignoreWhite = true;

_global.effectsXml = new XML();
_global.effectsXml.ignoreWhite = true;


_global.ChangeUnit = function ():Void {
	ToolPropertiesContainer.GetInstance().ShowUnitProperties();
	// TODO: move code to ChangeUnit handler when created
	QuickToolContent.GetInstance().EnableButtons();
}

_global.RessurectEmptyLayoutItem = function ():Void {
	trace("RessurectEmptyLayoutItem");
	if (_global.CurrentUnit instanceof TextUnit &&
		_global.CurrentUnit.Text.length == 0 &&
		_global.CurrentUnit.EmptyLayoutItem == false) {
		_global.CurrentUnit.EmptyLayoutItem = true;
		var fmt = _global.CurrentUnit.GetFontFormat(0);
		var len = _global.CurrentUnit.Text.length;
		var defaultText = "Text " + _global.CurrentUnit.LayoutPosition;
		_global.CurrentUnit.ReplaceText(0, 0, defaultText, fmt);
	}
	trace(_global.CurrentUnit.GetAllText().length + " = _global.CurrentUnit.GetAllText() " +_global.CurrentUnit.EmptyLayoutItem);
	if (_global.CurrentUnit instanceof TableUnit &&
		_global.CurrentUnit.GetAllText().length < 1 &&
		_global.CurrentUnit.EmptyLayoutItem == false) {
			trace("In resurect table Unit");
		
		_global.CurrentUnit.EmptyLayoutItem = true;
		_global.CurrentUnit.ReinitTable(null,true,false);
	}
}

_global.DeleteCurrentUnit = function (eventObject):Void {
	_global.CurrentFace.DeleteCurrentUnit();
    Logic.DesignerLogic.ShowPreviousPanel();
}

_global.RotateToRightAngle = function(antiClockwise:Boolean):Void {
		var currentAngle = _global.SelectionFrame.Angle >= 0
        ? _global.SelectionFrame.Angle
        : _global.SelectionFrame.Angle + 360;
		var angle = currentAngle % 90;
		angle = antiClockwise
        ? - (angle + 90 * (angle == 0))
        : (90 - angle);
		_global.SelectionFrame.Angle = currentAngle + angle;
	}

_global.GenerateProjectXml = function() {
	//Create a copy of current XML file
	var xml:XML = new XML(_global.projectXml.firstChild.toString());
	//remove CalibrationNode
	for (i=0; i<xml.childNodes[0].childNodes.length; i++) {
		var node:XMLNode = xml.childNodes[0].childNodes[i];
		if (node.nodeName == "Calibration")
			node.removeNode();
	}
	
	var calibrationNode:XMLNode = new XMLNode(1, "Calibration");
	calibrationNode.attributes.x = _global.xcalibration;
	calibrationNode.attributes.y = _global.ycalibration;
	xml.childNodes[0].appendChild(calibrationNode);		
	
	//Delete from XML all nodes from FACES, except CONTOURS
	var facesXml:XML = xml.childNodes[0].childNodes[0];
	for (i=0; i<facesXml.childNodes.length; i++) {
		var faceNode:XMLNode = facesXml.childNodes[i];
		for(j=faceNode.childNodes.length-1; j>=0; j--) {
			var faceSubNode:XMLNode = faceNode.childNodes[j];
			if (!(faceSubNode.nodeName == "Contour" || faceSubNode.nodeName == "Localization"))
				faceSubNode.removeNode();
			}
		//Add others units (texts, circles, rectangles, etc.) into XML document
		_global.Faces[i].AddUnitNodes(faceNode);
	}

	var propertiesNode:XMLNode = xml.childNodes[0].childNodes[2]; //Must be refactored
	propertiesNode.attributes.currentFaceId = _global.CurrentFace.Id;

	_global.projectXml = xml;
}


if (SALocalWork.IsLocalWork) {
	_global.ProjectImageUrlFormat = "../Neato.Cpt.WebDesigner/Images/P800.jpg";
	_global.LibraryImageIconUrlFormat = "../Neato.Cpt.WebDesigner/Images/P800.jpg";
} else {
	_global.ProjectImageUrlFormat = "ProjectImage.aspx?mode=image&id=";
	_global.LibraryImageIconUrlFormat = "ImageLibrary.aspx?mode=icon&id=";
}

function onReloadProject(eventObject) {
	_global.ReadFaces(_root.pnlMainLayer.content.WorkSpace, _global.projectXml);
	HintBox.getInstance(0).Update();
	HintBox.getInstance(1).Update();
//	PreviewMenu.getInstance().Update();
	FaceTabBack.getInstance().NormalizeState();
//	FaceTabBack.getInstance().DataBind();
}

function onSaveCompleted(eventObject) {
	trace("saveCompleted");
}

if (_global.jslisten_init == undefined) {
	_global.jsvarlistener = new LocalConnection();
	_global.jsvarlistener.setVariables = function(query) {
		var i, values;
		var chunk = query.split("&");
		for (i in chunk) {
			values = chunk[i].split("=");
			if (values[0] == "cmd") {
				if(values[1] == "reload") {
					var projectService:SAProject = SAProject.GetInstance(_global.projectXml);
					projectService.RegisterOnLoadedHandler(this, onReloadProject);
					projectService.BeginLoad();
				}
				else if(values[1] == "save") {
					_global.GenerateProjectXml();
					var projectService:SAProject = SAProject.GetInstance(_global.projectXml);
					projectService.RegisterOnSavedHandler(this, onSaveCompleted);
					projectService.BeginSave();
				}
			} else if (values[0] == "gotoStep") {
				var stepNumber = values[1];
				if (stepNumber == "2") {
					PreviewMenu.GotoDesigner();
				} else if (stepNumber == "3") {
					DesignerMenu.GotoPreview();
				}
			}
		}
	}
    _global.jsvarlistener.connect("Designer");
	_global.jslisten_init = true;
}
