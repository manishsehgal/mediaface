﻿import mx.behaviors.DepthControl;

class FaceUnit extends Unit {
	private var bgColor:Number;
	private var isFilled:Boolean;
	private var contours:Array;
	private var units:Array; 
	private var caption:String = ""; 
	private var dbId:String; 

	private var mcContour:MovieClip;
	private var mcCanvas:MovieClip;
	private var mcContourTop:MovieClip;
	
	private var scaleFactor:Number;	
	private var rotation:Number = 0;

	private var paperXValue:Number = 0;
	public function get PaperX():Number {
		return paperXValue;
	}

	private var paperYValue:Number = 0;
	public function get PaperY():Number {
		return paperYValue;
	}

	function FaceUnit(mc:MovieClip, node:XML) {
		super(mc, node);
	
		paperXValue = Number(node.attributes.x);
		paperYValue = Number(node.attributes.y);
		if (node.attributes.rotation != null && node.attributes.rotation != undefined)
			this.rotation = node.attributes.rotation;
		X = 0;
		Y = 0;
		
		dbId = String(node.attributes.dbId);

		isFilled = false;
		contours = new Array();
		this.mcContour = mc.createEmptyMovieClip("mcContour", mc.getNextHighestDepth());
		this.mcCanvas = mc.createEmptyMovieClip("mcCanvas", mc.getNextHighestDepth());
		for (var i = 0; i<node.childNodes.length; ++i) {
			var childNode:XML = node.childNodes[i];
			var depth = mcCanvas.getNextHighestDepth();
			switch (childNode.nodeName) {
			case "Contour" :
				var mcContourUnit = mcContour.createEmptyMovieClip("ContourUnit"+depth, depth);
				var contourUnit = new ContourUnit(mcContourUnit, childNode);
				contours.push(contourUnit);
				break;
			case "Localization" :
				ParseFaceCaption(childNode);
				break;
			default :
				trace("Unknown unit name! "+childNode.nodeName);
			}
		}
		
		var mcMask = mc.createEmptyMovieClip("mcMask", mc.getNextHighestDepth());
		this.mcContourTop = mc.createEmptyMovieClip("mcContourTop", mc.getNextHighestDepth());
		DrawContours();
		ParseXMLUnits(node);
		Draw();
		DrawMask(mcMask);

		//mcContour.onPress = OnContourClick;
		mcContour.useHandCursor = false;
		//_root.pnlMainLayer.content.WorkSpaceClickArea.onPress = OnContourClick;
		_root.pnlMainLayer.content.WorkSpaceClickArea.useHandCursor = false;
		Hide();
	}
	
	public function get RotationRad() {
		return rotation * Math.PI / 180;;
	}
	
	public function get RotationDeg() {
		return rotation;
	}
	
	public function ParseXMLUnits(node:XMLNode) {
		units = new Array();
		
		for (var i = 0; i<node.childNodes.length; ++i) {
			var childNode:XML = node.childNodes[i];
			var depth = mcCanvas.getNextHighestDepth();
			switch (childNode.nodeName) {
			case "Text" :
				var bEffect:Boolean = false;
				trace("Text + "+node);
				/* To Sashs Firsov
				// There are some problems with upload images
				for(var i=0;i<childNode.childNodes.length;i++)
					if(childNode.childNodes[i].nodeName == "TextEffect")
						{bEffect = true;break;	}
				*/
				var mcText = mcCanvas.createEmptyMovieClip("TextUnit"+depth, depth);
				var textUnit;
				trace("bEffect  = "+bEffect);
				if(!bEffect)
					textUnit = new TextUnit(mcText, childNode);
				else 
					textUnit = new TextEffectUnit(mcText, childNode);
				textUnit.RegisterOnClickHandler(this, onUnitClick);
				units.push(textUnit);
				break;
			case "Paint" :
				var mcPaint = mcCanvas.createEmptyMovieClip("PaintUnit"+depth, depth);
				var paintUnit = new PaintUnit(mcPaint, childNode);
				paintUnit.RegisterOnClickHandler(this, onUnitClick);
				units.push(paintUnit);
				break;
			case "Image" :			
				AddImageUnit(childNode);
				break;
			case "Shape" :
			    AddShapeUnit(childNode, undefined);
				break;
			case "Playlist":
				AddPlaylistUnit(childNode);
				break;
			case "Fill" :
				bgColor = parseInt(childNode.attributes.color.substr(1), 16);
				isFilled = true;
				break;
			}
		}		
	}
	public function RegisterOnUnitClickHandler(unit:MoveableUnit){
		unit.RegisterOnClickHandler(this, onUnitClick);
	}
	private function onUnitClick(eventObject) {
		var unit:MoveableUnit = eventObject.target;
		trace("unit.getMode()="+unit.getMode());
 		ChangeCurrentUnit(unit);
 	}
	function UpdateSelection(unit:MoveableUnit) {
		if (_global.CurrentUnit != null) {
			var frameParent:MovieClip = _global.SelectionFrame._parent;
			_global.SelectionFrame.DetachUnit() ;
			_global.SelectionFrame = _global.Frames[_global.CurrentUnit.frameLinkageName];
			//trace("_global.CurrentUnit.frameLinkageName = "+_global.CurrentUnit.frameLinkageName);
			_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
		}
	}
	function ChangeCurrentUnit(unit:MoveableUnit) {
 		if (_global.CurrentUnit != unit) {
 			if (_global.CurrentUnit != null) {
 				Logic.DesignerLogic.UnselectCurrentUnit();
 			}
			_global.Mode = unit.getMode();
			trace("onUnitClick: new mode " + _global.Mode);
 			_global.CurrentUnit = unit;
 		}
		else {
			_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
			_global.ChangeUnit();	
			return;
		}
		var frameParent:MovieClip = _global.SelectionFrame._parent;
		_global.SelectionFrame.DetachUnit() ;
		_global.SelectionFrame = _global.Frames[_global.CurrentUnit.frameLinkageName];
		_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
		_global.ChangeUnit();	
	}
	
	public function get ContourBounds() {
		return mcContour.getBounds(mcContour);
	}
	
	private function AddImageUnit(unitNode) {
		var depth = mcCanvas.getNextHighestDepth();
		var mcImage = mcCanvas.createEmptyMovieClip("ImageUnit"+depth, depth);
		var imageUnit:ImageUnit = new ImageUnit(mcImage, unitNode, _global.ProjectImageUrlFormat);
		if (imageUnit.IsNewImage) {
			imageUnit.X = Math.random() * (mcContour._width); 
			imageUnit.Y = Math.random() * (mcContour._height); 
		}
		imageUnit.RegisterOnClickHandler(this, onUnitClick);
		imageUnit.RegisterOnAutofitCancelHandler(this, onAutofitCancel);
		units.push(imageUnit);
		return imageUnit;
	}
	private function AddShapeUnit(unitNode, shapeType){
		trace("FaceUnit: AddShapeUnit");
		var depth = mcCanvas.getNextHighestDepth();
		var mcShape = mcCanvas.createEmptyMovieClip("ShapeUnit"+depth, depth);
		var shapeUnit:ShapeUnit = new ShapeUnit(mcShape, unitNode, shapeType, Math.min(mcContour._width, mcContour._height)/3);
		shapeUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(shapeUnit);
		shapeUnit.Draw();
		return shapeUnit;
	}
	
	private function AddPlaylistUnit(unitNode, playlist){
		trace("FaceUnit: AddPlaylistUnit");
		var depth = mcCanvas.getNextHighestDepth();
		var mcPlaylist = mcCanvas.createEmptyMovieClip("PlaylistUnit"+depth, depth);
		var playlistUnit:TableUnit= new TableUnit(mcPlaylist, (unitNode != null?unitNode:playlist),(unitNode != null?false:true));
		playlistUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(playlistUnit);
		playlistUnit.Draw();
		//ChangeCurrentUnit(playlistUnit);
		return playlistUnit;
	}
	private function AddNewPlaylist(playlist,bNew:Boolean,bSelect:Boolean){
		trace("FaceUnit: AddPlaylistUnit");
		var depth = mcCanvas.getNextHighestDepth();
		if(bSelect == undefined)
			bSelect = true;
		var mcPlaylist = mcCanvas.createEmptyMovieClip("PlaylistUnit"+depth, depth);
		var playlistUnit:TableUnit= new TableUnit(mcPlaylist, playlist,!bNew,bNew);
		playlistUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(playlistUnit);
		playlistUnit.Draw();
		if(bSelect)
			ChangeCurrentUnit(playlistUnit);
		return playlistUnit;
	}
	
	
	public function CreateNewShapeUnit(shapeType){
		trace("FaceUnit: CreateNewShapeUnit");
		var shapeUnit:ShapeUnit = AddShapeUnit(null, shapeType);
		shapeUnit.X = Math.random() * (mcContour._width); 
		shapeUnit.Y = Math.random() * (mcContour._height); 
		ChangeCurrentUnit(shapeUnit);
		return shapeUnit;
	}
	
	public function DuplicateImageUnit(unitNode) {
		var imageUnit = AddImageUnit(unitNode);
		imageUnit.Angle = 0;
		imageUnit.X = Math.random() * 10;
		imageUnit.Y = Math.random() * 70;
		imageUnit.ScaleX = imageUnit.InitialScale;
		imageUnit.ScaleY = imageUnit.InitialScale;
		imageUnit.Draw();
		//TODO: dispatch even to change current unit
		ChangeCurrentUnit(imageUnit);
	}
	public function CreateTextUnitFromEffect(unitNode){
		DeleteCurrentUnit();
		var depth = mcCanvas.getNextHighestDepth();
		var mcText = mcCanvas.createEmptyMovieClip("TextUnit"+depth, depth);
		var textUnit = new TextUnit(mcText, unitNode);
		textUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(textUnit);
		ChangeCurrentUnit(textUnit)
		textUnit.Draw();
		break;
	}
	public function CreateEffectUnitFromText(unitNode,effect){
		DeleteCurrentUnit();
		var depth = mcCanvas.getNextHighestDepth();
		var mcEffect = mcCanvas.createEmptyMovieClip("TextUnit"+depth, depth);
		var effectUnit = new TextEffectUnit(mcEffect,unitNode);
		effectUnit.SetTextEffect(effect);
		effectUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(effectUnit);
		ChangeCurrentUnit(effectUnit)
		effectUnit.Draw();
		break;
	}

	public function FindImageUnit(id) {
		for(var index in units) {
			var unit = units[index];
			if (unit.getMode() == _global.ImageEditMode && unit.id == id) {
				return unit;
			}
		}
		return undefined;
	}
	
	function Hide():Void {
		mc._visible = false;
	}
	
	function Show():Void {
		var xF:Number =_global.EditWidth / mc.mcContour._width;
		var yF:Number =_global.EditHeight / mc.mcContour._height;
		scaleFactor = (xF < yF) ? xF : yF;
		scaleFactor *= 0.8;
		
		var bounds = mc.mcContour.getBounds(mc.mcContour);
 		var xMin = bounds.xMin;
 		var yMin = bounds.yMin;
 		var xMax = bounds.xMax;
 		var yMax = bounds.yMax;
		
		mc._parent._xscale = mc._parent._yscale = scaleFactor * 100;
		mc._parent._x = _global.EditLeft + (_global.EditWidth  - scaleFactor*(xMax + xMin))/2;
		mc._parent._y = _global.EditTop  + (_global.EditHeight - scaleFactor*(yMax + yMin))/2;
		
		 _global.Frames["Frame"].HandlersScale = _global.Frames["TableFrame"].HandlersScale = 100 / scaleFactor;
		
		mc._visible = true;
	}
	
	public function get ScaleFactor():Number {
		return scaleFactor;
	}
	function ChangeUnitCoordinatesUnderTransfer(sourceFaceScaleFactor:Number):Void {
		var k:Number  = (scaleFactor - sourceFaceScaleFactor)/scaleFactor;
		for(var i = 0; i < units.length; i++) {
			var unit:Unit = units[i];
			unit.X *= (1-k);
			unit.Y *= (1-k);
		}		
	}	

	function SmartXMLAppendChild(parentNode:XMLNode, child:XMLNode):Void {
		if (parentNode.childNodes.length<2) {
			parentNode.appendChild(child);
		} else {
			parentNode.insertBefore(child, parentNode.childNodes[1]);
		}
	}
	
	function GetFillXMLNode():XMLNode {
		var node:XMLNode = new XMLNode(1, "Fill");
		node.attributes.color = _global.GetColorForXML(bgColor);
		return node;
	}
	
	function AddUnitNodes(parentNode:XMLNode):Void {		
		for (var i = units.length-1; i>=0; i--) {
			SmartXMLAppendChild(parentNode, units[i].GetXmlNode());
		}
		if (isFilled) {
			SmartXMLAppendChild(parentNode, GetFillXMLNode());
		}
	}
	
	function Fill(color:Number):Void {
		bgColor = color;
		isFilled = true;
  		for (var i in contours) {
  			var contour = contours[i];
  			contour.SetBgAndDraw(isFilled, bgColor, RotationRad);
  		}
	}
	
	function RedrawPaintUnits():Void {
		trace("FaceUnit.RedrawPaintUnits()");
		for (var i in units) {
			var unit = units[i];
			var isPaintMode = unit.getMode() == _global.PaintMode || unit.getMode() == _global.PaintEditMode;
			if(isPaintMode) {
				trace("isPaintMode == true");
				unit.Draw();
				unit.mc._xscale = unit.ScaleX;
				unit.mc._yscale = unit.ScaleY;
			}
		}
	}
	
	function DrawContours():Void {
		for (var i in contours) {
			var contour = contours[i];
			contour.SetBgAndDraw(isFilled, bgColor, RotationRad);

			mc.mcContourTop.lineStyle(0);
			contour.DrawMC(mc.mcContourTop, true, RotationRad);			
		}
	}
	
	function DrawUnits():Void {
		trace("FaceUnit.Draw: draw " + units.length + " units");
		for (var i in units) {
			var unit = units[i];
			unit.Draw(RotationRad);
			unit.mc._xscale = unit.ScaleX;
			unit.mc._yscale = unit.ScaleY;
		}
	}
	
	function Draw():Void {
		DrawContours();
		DrawUnits();
	}
	
	function DrawMC(movie:MovieClip):Void {
		trace("FaceUnit.DrawMC");
		for (var i in contours) {
			var contour = contours[i];
			contour.SetBgColor(isFilled, bgColor);
			contour.DrawMC(movie, false, RotationRad);
		}
		
		var count = units.length;
		for (var j = 0; j<count; ++j) {
			var unit = units[j];
			
			var mcUnit = movie.createEmptyMovieClip("Unit"+j, j);
			mcUnit.unitRef = unit;
			unit.DrawMC(mcUnit);
			mcUnit._x = unit.X;
			mcUnit._y = unit.Y;
			mcUnit._rotation = unit.Angle;
			mcUnit._xscale = unit.ScaleX;
			mcUnit._yscale = unit.ScaleY;
		}
		
		var ctr:MovieClip = movie.createEmptyMovieClip("Contour", movie.getNextHighestDepth());
		
		for (var i in contours) {
			var contour = contours[i];
			contour.DrawMC(ctr, true, RotationRad);
			contour.DrawHoles(movie, RotationRad);
		}
		movie._rotation = - RotationDeg;
	}
	function DrawContour(movie:MovieClip, transparent:Boolean, rotate:Boolean):Void {
		var angle = rotate == null || rotate == undefined || rotate == false ? 0 : RotationRad;
		for (var i in contours) {
			var contour = contours[i];
			contour.DrawMC(movie, transparent, angle);
		}
	}
	function DrawCutLines(movie:MovieClip):Void {
		for (var i in contours) {
			var contour = contours[i];
			contour.DrawCutLines(movie);
		}
	}
	function DrawMask(movie:MovieClip):Void {
		for (var i in contours) {
			var contour = contours[i];
			contour.DrawMask(movie, RotationRad);
		}
	}
	function AddText(selectUnit:Boolean):TextUnit {
		trace("FaceUnit.AddText");
		var depth = mc.mcCanvas.getNextHighestDepth();
		var mcText = mc.mcCanvas.createEmptyMovieClip("TextUnit"+depth, depth);
		var textUnit:TextUnit = new TextUnit(mcText, null);
		textUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(textUnit);
		
		if (selectUnit) {
			ChangeCurrentUnit(textUnit);
		}
			
		textUnit.Draw();
		return textUnit;
	}
	function CreateNewPaintUnit() {
		trace("FaceUnit.CreateNewPaintUnit");
		var depth = mc.mcCanvas.getNextHighestDepth();
		var mcPaint = mc.mcCanvas.createEmptyMovieClip("PaintUnit"+depth, depth);
		var paintUnit = new PaintUnit(mcPaint, null);
		paintUnit.RegisterOnClickHandler(this, onUnitClick);
		units.push(paintUnit);
		ChangeCurrentUnit(paintUnit);
		paintUnit.mc.onMouseDown = paintUnit.PaintUnit_onMouseDown;
	}	
	
	function Clear() {
		trace("Clear");
		_global.SelectionFrame.DetachUnit();
		_global.CurrentUnit = null;
		isFilled = false;
		for (var i in units) {
			var unit = units[i];
			unit.mc.removeMovieClip();
		}
		units = new Array();
		Draw();
	}

	function DeleteCurrentUnit() {
		trace("FaceUnit:DeleteCurrentUnit");
		if (_global.CurrentUnit == null) return;
		DeleteUnit(_global.CurrentUnit);
		_global.CurrentUnit = null;
		_global.SelectionFrame.DetachUnit();
		
	}
	
	function DeleteUnit(deletedUnit) {
		trace("FaceUnit:DeleteUnit");
		for (var i = 0; i<units.length; ++i) {
			var unit = units[i];
			if (unit == deletedUnit) {
				unit.mc.removeMovieClip();
				units.splice(i, 1);
				break;
			}
		}
	}
	
	public function get Id():String {
		return id;
	}
	
	public function get DbId():String {
		return dbId;
	}
	
	function ParseFaceCaption(childNode:XMLNode) {
		var captionSpecificCulture;
		var captionDefaultCulture;
		
		for(var i = 0; i < childNode.firstChild.childNodes.length; i++) {
			var cultureNode = childNode.firstChild.childNodes[i];
			if (cultureNode.attributes.key == _global.culture) {
				captionSpecificCulture = cultureNode.attributes.value
				}
			if (cultureNode.attributes.key == "") {
				captionDefaultCulture = cultureNode.attributes.value
				}
		}
			
		if (captionSpecificCulture != undefined) {
			caption = captionSpecificCulture;
		} else if (captionDefaultCulture != undefined) {
			caption = captionDefaultCulture;
		}
	}
	
	public function get Caption():String {
		return caption;
	}
	
	public function ToBackCurrentUnit() {
		if (_global.CurrentUnit != null) {
			DepthControl.sendBackward(_global.CurrentUnit.mc);
 			units.sort(MoveableUnit.order);
 		}
	}
	
	public function ToBackAllCurrentUnit() {
		if (_global.CurrentUnit != null) {
			DepthControl.sendToBack(_global.CurrentUnit.mc);
 			units.sort(MoveableUnit.order);
 		}
	}
	
	public function ToFrontCurrentUnit() {
		if (_global.CurrentUnit != null) {
			DepthControl.bringForward(_global.CurrentUnit.mc);
 			units.sort(MoveableUnit.order);
 		}
	}
	
	public function ToFrontAllCurrentUnit() {
		if (_global.CurrentUnit != null) {
			DepthControl.bringToFront(_global.CurrentUnit.mc);
 			units.sort(MoveableUnit.order);
 		}
	}
	
	public function AutofitCurrentUnit(autofitType:String) {
		if(_global.Mode != _global.ImageEditMode)
			return;
		
		_global.CurrentUnit.AutofitType = _global.NoneAutofit;
			
		_global.CurrentUnit.ScaleX = 100;
		_global.CurrentUnit.ScaleY = 100;
		
		var angle = (_global.SelectionFrame.Angle >= 0 ? _global.SelectionFrame.Angle : _global.SelectionFrame.Angle + 360) % 90;
		if(angle != 0) {
			var antiClockwise = angle < 45;
			_global.RotateToRightAngle(antiClockwise);
		}

		var bounds = mcContour.getBounds(_global.CurrentFace.mcContour);
		var width = bounds.xMax - bounds.xMin;
		var height = bounds.yMax - bounds.yMin;

		var kx = width / _global.CurrentUnit.mc._width;
		var ky = height / _global.CurrentUnit.mc._height;
		
		switch(autofitType) {
			case _global.FitToFaceAutofit:
				if(_global.SelectionFrame.Angle == 90 || _global.SelectionFrame.Angle == -90) {
					var temp = kx;
					kx = ky;
					ky = temp;
				}
			break;
			case _global.HorizontalAutofit:
				ky = kx;
			break;
			case _global.VerticalAutofit:
				kx = ky;
			break;
			case _global.ThumbailAutofit:
				_global.CurrentUnit.ScaleX = _global.CurrentUnit.InitialScale;
				_global.CurrentUnit.ScaleY = _global.CurrentUnit.InitialScale;
				kx = ky = 1;
			break;
		}
		_global.CurrentUnit.Resize(kx, ky);
		
		var dx = (_global.CurrentUnit.mc._width - width) / 2;
		var dy = (_global.CurrentUnit.mc._height - height) / 2;
		var offsetX = - dx;
		var offsetY = - dy;
		switch(_global.SelectionFrame.Angle) {
			case 90:
				offsetX += _global.CurrentUnit.mc._width;
			break;
			case 180:
				offsetX += _global.CurrentUnit.mc._width;
				offsetY += _global.CurrentUnit.mc._height;
			break;
			case -90:
				offsetY += _global.CurrentUnit.mc._height;
			break;
		}
		_global.CurrentUnit.X = _global.CurrentFace.ContourBounds.xMin + _global.CurrentFace.mcContour._x + offsetX;
		_global.CurrentUnit.Y = _global.CurrentFace.ContourBounds.yMin + _global.CurrentFace.mcContour._y + offsetY;
		
		_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
		_global.CurrentUnit.AutofitType = autofitType;
	}
	
	private function onAutofitCancel(eventObject) {
		var unit = eventObject.target;
		unit.AutofitType = _global.NoneAutofit;
		Logic.DesignerLogic.ShowUnitProperties();
	}
}
