﻿class TextEffectUnit extends TextUnit {
	private var effect;
	private var effmc:TextEffect;
	private var contour:XMLNode = null;
	private var effScaleX:Number = 100.0;
	private var effScaleY:Number = 100.0;
	public var frameLinkageName = "FrameEx";
	public var bChanged:Boolean = true;
	//private var contour:XMLNode = null;
	public static var UnitClassName = "TextEffectUnit";
	function TextEffectUnit(mc:MovieClip, node:XML) {
		super(mc,node);
		trace("TextEffectUnit constructor")
		effect = null;
		effmc = null;
		var fontFormat:TextFormat = textField.getNewTextFormat();
		/*fontFormat.font = "arial";
		fontFormat.size = 16;
		fontFormat.bold = false;
		fontFormat.italic = false;
		fontFormat.color = 0x000000;*/
		if (node != null) {
			trace("node = "+node);
			for (var i = 0; i < node.childNodes.length; ++i) {
				for (var i = 0; i < node.childNodes.length; ++i) {
					var childNode:XML = node.childNodes[i];
					switch(childNode.nodeName) {
						case "TextEffect":
							bChanged = true;
							effect = _global.Effects.CreateTextEffectFromXml(childNode);
							frameLinkageName = SAPlugins.IsAvailable() ? "FrameEx" : "Frame";
							break;
						case "Contour":
							contour = childNode;
							fontFormat.color = parseInt(childNode.attributes.color.substr(1),16);
							fontFormat.italic = childNode.attributes.italic.toString() == "true" ? true : false;
							fontFormat.bold =  childNode.attributes.bold.toString() == "true" ? true : false;
							textField.setTextFormat(0,textField.text.length,fontFormat);
							textField.setNewTextFormat(fontFormat);
							break;
						default:
							
					}
				}
			}
		}
		
		
	}
	function GetXmlNode():XMLNode {
		
		var node:XMLNode = super.GetXmlNode();
			//var effectNode:XMLNode = new XMLNode(1,"TextEffect");
		node.appendChild(_global.Effects.GetXML(effect));
		var cont:XMLNode = contour;
		contour.attributes.color = _global.GetColorForXML(textField.getTextFormat(0).color);
		//trace("GetXML: color:"+contour.attributes.color);
		//trace("GetXML: color:"+_global.GetColorForXML(textField.getTextFormat().color));
		contour.attributes.width = mc.getBounds(mc).xMax - mc.getBounds(mc).xMin;
		contour.attributes.height = mc.getBounds(mc).yMax - mc.getBounds(mc).yMin;
		//trace("contour.attributes.height "+contour.attributes.height);
		contour.attributes.x = mc.getBounds(mc).xMin;
		contour.attributes.y = mc.getBounds(mc).yMin;
		node.appendChild(cont);
		return node;
		//trace("GetXML effect activated");
			
		
	}
	
	
	function SetTextEffect(effectName) {
			effect = _global.Effects.GetEffect(effectName); //to refactor;
			//effmc.textString = textField.text;
			this.ScaleX = 100;
			this.ScaleY = 100;
			bChanged = true;
			
			
			textField._visible = false;
			
			textField.autoSize = false;
			textField._width = 1;
			textField._height = 1;
			mc.txt.removeTextField();
			
			mc.textField._visible = false;
			mc.textField.autoSize = false;
			mc.textField._width = 1;
			mc.textField._height = 1;
			
			mc.textField._x = effect.refPoints[0].x;
			mc.textField._y = effect.refPoints[0].y;
			
			frameLinkageName = "FrameEx";
		
	}
	
	public function UpdateTextEffect() {
		
			bChanged = true;
			Draw();
		
	}
	function getEffect() {
		return effect;
	}
	function Draw():Void {
		DrawMC(mc, true);
	}
	

	function DrawMC(mc:MovieClip, drawOnWorkArea:Boolean):Void {
	
			mc.txt._visible = false;
			effmc = new TextEffect(mc,this);
			effmc.RegisterOnRedrawHandler(this,OnEffectRedraw);
			effmc.effect = effect;
			effmc.textString = textField.text;
			effmc.color = textField.getTextFormat(0).color;
			trace("Color = "+effmc.color);
			effmc.bold = textField.getTextFormat(0).bold;
			effmc.italic = textField.getTextFormat(0).italic;
			effmc.size = textField.getTextFormat(0).size;
			effmc.fontTTF = _global.Fonts.GetTTFFileName(textField.getTextFormat(0).font.toString().toLowerCase()+"_"+effmc.italic.toString()+"_"+effmc.bold.toString());
			if(effmc.fontTTF == undefined)
				effmc.fontTTF = _global.Fonts.GetTTFFileName(textField.getNewTextFormat().font.toString().toLowerCase()+"_"+effmc.italic.toString()+"_"+effmc.bold.toString());
			if(effmc.fontTTF == undefined)
				effmc.fontTTF = _global.Fonts.GetTTFFileName(textField.getNewTextFormat().font.toString().toLowerCase()+"_false_false");
			
			//block - bug with left border workaround
			mc.textField._visible = false;
			mc.textField.autoSize = false;
			mc.textField._width = 1;
			mc.textField._height = 1;
			mc.textField._x = effect.refPoints[0].x;
			mc.textField._y = effect.refPoints[0].y;
			
			effmc.Draw();
			bChanged = false;
			if (_global.CurrentUnit == this)
				_global.CurrentFace.UpdateSelection(this);
	}
	public function ResizeByStep(isPositive:Boolean) {
		var mu:MoveableUnit = this;
		mu.ResizeByStep(isPositive);
	}

	private function Resize(kx:Number, ky:Number) {
		
			var b=mc.getBounds(mc);
			var dX:Number=0,dY:Number=0;
			
			this.ScaleX = this.ScaleX * kx;
			this.ScaleY = this.ScaleY * ky;
			_global.SelectionFrame.scaleX = this.ScaleX * kx;
			_global.SelectionFrame.scaleY = this.ScaleY * ky;
			_global.SelectionFrame.frameBorder.scaleX = this.ScaleX * kx;
			_global.SelectionFrame.frameBorder.scaleY = this.ScaleY * ky;
			
		
	}
	public function ChangeFontFormat(indexBegin:Number, indexEnd:Number, format:TextFormat):Void {
		textField.setNewTextFormat(format);
		textField.setTextFormat(format);
		/*for(var index:Number = indexBegin; index <= indexEnd; ++index) {
			var begin:Number = (index == indexBegin) ? index : index - 1;
			var end:Number =  index;
			textField.setTextFormat(begin, end, format);
		}*/

		
			bChanged = true;
		
		
	}
	public function ReplaceText(indexBegin:Number, indexEnd:Number, newSubsrting:String, format:TextFormat):Void {
		// Any property of textFormat that is set to null will not be applied
		textField.setNewTextFormat(format);
			
		if (newSubsrting.length > 0) {
			textField.replaceText(indexBegin, indexEnd, newSubsrting);
			textField.setTextFormat(indexBegin, indexBegin+newSubsrting.length, format);
			if (!_global.Fonts.TestLoadedFont(format)) {
				_global.Fonts.LoadFont(format, this);
			}
		} else if (indexBegin == 0 && indexEnd == textField.text.length) { 
			textField.text = newSubsrting;
		} else { 
			//removing range
			var txt:String = textField.text.substr(0, indexBegin);
			txt += textField.text.substr(indexEnd, textField.length);
			
			var fontFormats:Array = new Array();
			
			for (var i = 0; i < indexBegin; ++i) {
				fontFormats.push(textField.getTextFormat(i));
			}
			
			for (var i = indexEnd; i < textField.length; ++i) {
				fontFormats.push(textField.getTextFormat(i));
			}
			
			textField.text = txt;
			for (var i = 0; (i < fontFormats.length) || (i == 0 && textField.length == 0); ++i) {
				textField.setTextFormat(i, fontFormats[i]);
			}
		}
			
			effmc.textString = textField.text;
			bChanged = true;
	
	}
	public function OnEffectRedraw(eventObject) {
		//hiding text field
		textField._visible = false;
		textField.autoSize = false;
		textField._width = 1;
		textField._height = 1;
	
		mc.textField._visible = false;
		mc.textField.autoSize = false;
		mc.textField._width = 1;
		mc.textField._height = 1;
		mc.textField._x = effect.refPoints[0].x;
		mc.textField._y = effect.refPoints[0].y;
		mc.txt.removeTextField();
		//filling contour
		mc.beginFill(0, 0);
		var b=mc.getBounds();
		mc.moveTo(b.xMin, b.yMin);
		mc.lineTo(b.xMin, b.yMax);
		mc.lineTo(b.xMax, b.yMax);
		mc.lineTo(b.xMax, b.yMin);
		mc.lineTo(b.xMin, b.yMin);
		mc.endFill();
		if(_global.CurrentUnit == this) {
			trace("INNN!!!!!");
				_global.SelectionFrame.AttachUnit(this);
				
		}
		
	}
	function getMode():String {
		return _global.TextMode;
	}
	
}