﻿var fontInfoList = new Array (
   //Times New Roman
	{
        fontName:"Times New Roman",
		fontChars:"Times NwRoan"
	
    },
	//Courier New
	{
        fontName:"Courier New",
		fontChars:"Courie Nw"
    },
  
	//Arial
	{
        fontName:"Arial",
		fontChars:"Arial"
    },

	//Arial Narrow
	{
        fontName:"Arial Narrow",
		fontChars:"Arial Now"
		
    },
   
	//Arial Black
	{
        fontName:"Arial Black",
		fontChars:"Arial Bck"
		
    },
	//Comic Sans MS
	{
        fontName:"Comic Sans MS",
		fontChars:"Comic SansM"
		
    },
   
	//Impact
	{
        fontName:"Impact",
		fontChars:"Impact"
		
    },
	//Monotype Corsiva
	{
        fontName:"Monotype Corsiva",
		fontChars:"Montype Crsiva"
		
       
    },
	//Symbol
	{
        fontName:"Symbol",
		fontChars:"Symbol"
		
    },
	//Bookman Old Style
	{
        fontName:"Bookman Old Style",
		fontChars:"Bokman OldStye"
		
    },
   
	//Century Schoolbook
    {
        fontName:"Century Schoolbook",
		fontChars:"Century Scholbk"
		
    },
   
	//Trebuchet MS
    {
        fontName:"Trebuchet MS",
		fontChars:"Trebucht MS"
		
    },
  
	//Verdana
    {
        fontName:"Verdana",
		fontChars:"Verdan"
    }
   

)
var designerDir = "../Neato.Cpt.WebDesigner/Flash/Fonts/";
var testDir = "Flash/Fonts/";
publishSmallFont(fontInfoList,designerDir);
publishSmallFont(fontInfoList,testDir);

function publishSmallFont(info, dir) {
    var doc = CreateDoc(info);
			
	var symbolName = "smallFont";
	var flaName = symbolName + "_lib.swf";
	var linkageUrl = symbolName + "_lib.swf";

	var lib = doc.library;
	lib.items[0].linkageExportForAS = true;
	lib.items[0].linkageExportForRS = true;
	lib.items[0].linkageURL = linkageUrl;

    doc.importPublishProfile("file:///FontPublishProfile.xml");
    doc.currentPublishProfile = "FontPublishProfile";
    doc.exportSWF("file:///" + dir + flaName, true);
    doc.close(false);
	
	doc = CreateDoc(info);
	flaName = symbolName + ".swf";

	lib = doc.library;
	lib.items[0].linkageImportForRS = true;
	lib.items[0].linkageURL = "../Flash/Fonts/" + linkageUrl;
	
	doc.importPublishProfile("file:///FontPublishProfile.xml");
    doc.currentPublishProfile = "FontPublishProfile";
    doc.exportSWF("file:///" + dir + flaName, true);
    doc.close(false);
}

function CreateDoc(info) {
	var doc = flash.createDocument("timeline");
	for(var i in info){
		doc.addNewText({left:0, top:0, right:300, bottom:50});
   	        doc.setTextString(info[i].fontName);
		var txt = doc.selection[0];
		txt.textType = "dynamic";
 		txt.setTextAttr("face", info[i].fontName);
		txt.setTextAttr("size", 12);
		txt.setTextAttr("fillColor", 0x0);
		txt.setTextAttr("bold", false);
		txt.setTextAttr("italic", false);
	    	txt.embeddedCharacters  = info[i].fontChars;
		txt.autoExpand = true;
	}	
	
	doc.convertToSymbol("movie clip", "smallFont", "top left");
	
	return doc;
}

