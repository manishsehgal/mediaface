﻿import mx.core.UIObject;
import mx.controls.*;
import mx.utils.Delegate;
[Event("exit")]
class ChooseLayoutProperties extends UIObject {
	private var lblChooseLayout:Label;
	private var lblApply:TextArea;
	private var btnCancel:MenuButton;
	private var btnContinue:MenuButton;
	private var ctlLayoutList;

	private var dataSource:Array;
	private var layoutData:XML;
	private var faceLayouts:Array;
	
	function ChooseLayoutProperties() {
		Preloader.Show();
		layoutData = new XML();
		faceLayouts = new Array();
		dataSource =  new Array();
		this.lblChooseLayout.setStyle("styleName", "ToolPropertiesCaption");
		this.btnCancel.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnContinue.setStyle("styleName", "ToolPropertiesActiveButton");
		this.lblApply.setStyle("styleName", "TransferPropertiesText");
	}
	
	function onLoad() {
		ctlLayoutList.RegisterOnChangeHandler(this, ctlLayoutList_OnChange);
		ctlLayoutList.ItemWidth = ctlLayoutList.ItemHeight = 95;//set in design time
		btnCancel.RegisterOnClickHandler(this,	btnCancel_OnClick);
		btnContinue.RegisterOnClickHandler(this,	btnContinue_OnClick);
		InitLocale();
		LoadLayout();
	}
	
	function ContinueShow(b:Boolean)
	{
		btnContinue._visible = b;
		btnCancel._visible = !b;
		
	}
	private function InitLocale() {
		_global.LocalHelper.LocalizeInstance(lblChooseLayout, "ToolProperties", "IDS_LBLCHOOSELAYOUT");
		_global.LocalHelper.LocalizeInstance(lblApply, "ToolProperties", "IDS_LBLAPPLY");
		_global.LocalHelper.LocalizeInstance(btnCancel, "ToolProperties", "IDS_BTNCANCEL");
		_global.LocalHelper.LocalizeInstance(btnContinue, "ToolProperties", "IDS_BTNCONTINUE");
		
		TooltipHelper.SetTooltip(btnCancel, "ToolProperties", "IDS_TOOLTIP_CANCEL");
		TooltipHelper.SetTooltip(btnContinue, "ToolProperties", "IDS_TOOLTIP_CONTINUE");
	}

	private function LoadLayout() {
		var parent = this;
		layoutData.onLoad = function(success) {
			parent.ParseLayoutData();
			parent.DataBind();
			trace("!!!!!Layout data on load");
		};
		
		var currentDate:Date = new Date();
		//var url:String = "FaceLayout.aspx?time=" + currentDate.getTime();
		var url:String = "facelayout.xml";
		trace("Load layout yrl "+url);
		layoutData.load(url);
	}
	
	private function ctlLayoutList_OnChange(eventObject) {
		if (eventObject.index == undefined) return;
		var currentFaceLayout:FaceLayout = dataSource[eventObject.index];
		ContinueShow(true);
		HideOddUnitsFromLastLayout(currentFaceLayout);
		ApplyFormattingToExistentLayoutUnits(currentFaceLayout);
		CreateNonExistentLayoutUnits(currentFaceLayout);
	}
	
	private function GetLayoutPrimitives(layout:FaceLayout):Array {
		var list:Array = new Array;
		
		for(var i = 0; i < layout.Items.length; i++) {
			var item:FaceLayoutItem  = layout.Items[i];
			var rectangle:RectanglePrimitive = new RectanglePrimitive();
			
			rectangle.X = parseFloat(item.X.toString());
			rectangle.Y = parseFloat(item.Y.toString());
			rectangle.W = parseInt(item.Width.toString());
			rectangle.H = parseInt(item.Height.toString());
			
			var angle = (item.Angle) * 2 * Math.PI / 360;
			rectangle.Angle = parseFloat(angle.toString());
			
			list.push(rectangle);
		}
		
		return list;
	}
	
	private function GetFacePrimitives(face):Array {
		var list:Array = new Array();
		
		for (var i = 0; i < face.contours.length; i++) {
			list = list.concat(face.contours[i].primitives);			
		}
		
		return list;
	}
	
	public function DataBind():Void {
		ContinueShow(false);
		dataSource.splice(0);
		
		var defaultLayout:FaceLayout = new FaceLayout();
		defaultLayout.drawSequence = GetFacePrimitives(_global.CurrentFace);
		faceLayouts.push(defaultLayout);
		dataSource.push(defaultLayout);

		for (var i = 0; i < faceLayouts.length; i++) {
			var layout:FaceLayout = faceLayouts[i];
			trace("Layoutid "+layout.FaceId+" dbFaceid"+_global.CurrentFace.DbId);
			if (layout.FaceId == _global.CurrentFace.DbId) {
				trace(_global.CurrentFace.DbId);
				layout.drawSequence = GetFacePrimitives(_global.CurrentFace);
				layout.textSequence = GetLayoutPrimitives(layout);
				dataSource.push(layout);
			}
		}
		
		ctlLayoutList.DataSource = dataSource;
		ctlLayoutList.ItemLinkageName = "DrawingHolder";
		ctlLayoutList.DataBind();
		Preloader.Hide();
	}
	
	private function ParseFaceLayouts():Void {
		for (var i = 0; i < layoutData.firstChild.childNodes.length; i++) {
			var node:XMLNode = layoutData.firstChild.childNodes[i];
			
			if (node.nodeName == "FaceLayout") {
				var layout:FaceLayout = new FaceLayout();
				layout.Id = node.childNodes[1].childNodes[0].nodeValue;
				layout.FaceId = node.childNodes[3].childNodes[0].nodeValue;
				faceLayouts.push(layout);
			}
		}
	}
	
	private function ParseFaceLayoutItems():Void {
		for (var k = 0; k < faceLayouts.length; k++) {
			var layout:FaceLayout = faceLayouts[k];
			
			for (var i = 0; i < layoutData.firstChild.childNodes.length; i++) {
				var node:XMLNode = layoutData.firstChild.childNodes[i];
				var faceLayoutId = node.childNodes[3].childNodes[0].nodeValue;
				
				if (node.nodeName == "FaceLayoutItem" && layout.Id == faceLayoutId) {
					var item:FaceLayoutItem = new FaceLayoutItem();
					item.Id = node.childNodes[1].childNodes[0].nodeValue;
					item.FaceLayoutId = faceLayoutId;
					item.X = node.childNodes[5].childNodes[0].nodeValue;
					item.Y = node.childNodes[7].childNodes[0].nodeValue;
					item.Position = node.childNodes[9].childNodes[0].nodeValue;
					item.Text = node.childNodes[11].childNodes[0].nodeValue;
					item.Font = node.childNodes[13].childNodes[0].nodeValue;
					item.Size = node.childNodes[15].childNodes[0].nodeValue;
					item.Bold = node.childNodes[17].childNodes[0].nodeValue == "true" ? true : false;
					item.Italic = node.childNodes[19].childNodes[0].nodeValue == "true" ? true : false;
					item.Color = node.childNodes[21].childNodes[0].nodeValue;
					item.Angle = node.childNodes[23].childNodes[0].nodeValue;
					item.Height = node.childNodes[25].childNodes[0].nodeValue;
					item.Width = node.childNodes[27].childNodes[0].nodeValue;
					item.Align = node.childNodes[29].childNodes[0].nodeValue;
					item.UnitType = node.childNodes[31].childNodes[0].nodeValue;
					
					if (item.Text == undefined) {
						item.EmptyLayoutItem = true;
						item.Text = "Text " + item.Position;
					} 					

					layout.Items.push(item);
				}
			}
		}
	}

	private function ParseLayoutData():Void {
		ParseFaceLayouts();
		ParseFaceLayoutItems();
	}
		
	private function HideOddUnitsFromLastLayout(currentFaceLayout:FaceLayout):Void {
		var bNoPlaylist:Boolean=true;
		for (var j = 0; j < currentFaceLayout.Items.length; j++) {
			if(currentFaceLayout.Items[i].UnitType == "TableUnit")
			{
				bNoPlaylist = false;
			}
		}
		for (var i = _global.CurrentFace.units.length - 1; i >= 0; i--) {
			var unit = _global.CurrentFace.units[i];
			
			if ((unit instanceof TextUnit || unit instanceof TableUnit)  && !isNaN(unit.LayoutPosition)) {
				if(unit instanceof TableUnit)
					trace("Hide TAble unit +"+unit.LayoutPosition);
				var foundItem = false;
				for (var j = 0; j < currentFaceLayout.Items.length; j++) {
					var layoutItem:FaceLayoutItem = currentFaceLayout.Items[j];
					if (unit.LayoutPosition == layoutItem.Position && unit.UnitClassName == layoutItem.UnitType) {
						foundItem = true;
						break;
					}
				}

				if (!foundItem) {
					unit.Visible = false;
					//_global.CurrentFace.DeleteUnit(unit);
				}
			}
		}
	}		
	
	private function ApplyFormattingToExistentLayoutUnits(currentFaceLayout:FaceLayout):Void {
		var bWasPlaylist:Boolean = false;
		for (var i = 0; i < currentFaceLayout.Items.length; i++) {
			var layoutItem:FaceLayoutItem = currentFaceLayout.Items[i];
			
			for (var j = 0; j < _global.CurrentFace.units.length; j++) {
				var unit = _global.CurrentFace.units[j];
				
				if (unit instanceof TextUnit && layoutItem.UnitType == "TextUnit" && unit.LayoutPosition == layoutItem.Position) {
					unit.Visible = true;
					layoutItem.EmptyLayoutItem = unit.EmptyLayoutItem;
					unit.ApplyLayoutFormat(layoutItem, false);
					break;
				}
				if (unit instanceof TableUnit && layoutItem.UnitType == "TableUnit" && !bWasPlaylist) {
					unit.Visible = true;
					//if(unit.EmptyLayoutItem == true)
						//layoutItem.EmptyLayoutItem = unit.EmptyLayoutItem;
					unit.ApplyLayoutFormat(layoutItem, false);
					bWasPlaylist = true;
					break;
				}
			}
		}
	}
	
	private function CreateNonExistentLayoutUnits(currentFaceLayout:FaceLayout):Void {
		for (var i = 0; i < currentFaceLayout.Items.length; i++) {
			var layoutItem:FaceLayoutItem = currentFaceLayout.Items[i];
			trace("CreateNonExistentLayoutUnits UT:"+layoutItem.UnitType);
			if(layoutItem.UnitType == "TextUnit"){
				var foundItem = false;
				for (var j = 0; j < _global.CurrentFace.units.length; j++) {
					var unit = _global.CurrentFace.units[j];
				
					if (unit instanceof TextUnit && unit.LayoutPosition == layoutItem.Position) {
						foundItem = true;
						break;
					}
				}
			
				if (!foundItem) {
					var textUnit:TextUnit = _global.CurrentFace.AddText(false);
					textUnit.ApplyLayoutFormat(layoutItem, true);
					textUnit.LayoutPosition = layoutItem.Position;
				}
			}
			else if(layoutItem.UnitType == "TableUnit") {
				var foundItem = false;
				for (var j = 0; j < _global.CurrentFace.units.length; j++) {
					var unit = _global.CurrentFace.units[j];
				
					if (unit instanceof TableUnit ) {
						foundItem = true;
						trace("Table unit found +"+unit);
						break;
					}
				}
			
				if (!foundItem) {
					trace("No playlist");
					var tableUnit:TableUnit= _global.CurrentFace.AddNewPlaylist(null,true,false);
					tableUnit.ReinitTable(null,true,false);
					tableUnit.ApplyLayoutFormat(layoutItem, true);
					tableUnit.LayoutPosition = layoutItem.Position;
				}
			}
		}
	}
	private function btnCancel_OnClick(){
		OnExit();
	}
	private function btnContinue_OnClick(){
		OnExit();
	}
	//region Exit event
	private function OnExit() {
		var eventObject = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
}
