﻿import mx.core.UIObject;
import mx.utils.Delegate;
import mx.controls.*;

class TransferProperties extends UIObject {
	private var lblTransfer:Label;
	private var lblNote:TextArea;
	private var lblSelect:TextArea;
	private var btnYes:MenuButton;
	private var btnNo:MenuButton;
	private var destinationFaceIndexValue:Number;

	function TransferProperties() {
		this.setStyle("styleName", "TransferProperties");
		this.lblTransfer.setStyle("styleName", "ToolPropertiesCaption");
		this.lblNote.setStyle("styleName", "TransferPropertiesText");
		this.lblSelect.setStyle("styleName", "TransferPropertiesText");
		this.btnYes.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnNo.setStyle("styleName", "ToolPropertiesActiveButton");
	}
	
	function onLoad() {
		btnYes.RegisterOnClickHandler(this, btnYes_OnClick);
		btnNo.RegisterOnClickHandler(this, btnNo_OnClick);
		_global.LocalHelper.LocalizeInstance(lblTransfer, "ToolProperties", "IDS_LBLTRANSFER");
		_global.LocalHelper.LocalizeInstance(lblNote, "ToolProperties", "IDS_LBLNOTE");
		_global.LocalHelper.LocalizeInstance(lblSelect, "ToolProperties", "IDS_LBLSELECT");
		TooltipHelper.SetTooltip(btnYes, "ToolProperties", "IDS_TOOLTIP_YES");
		TooltipHelper.SetTooltip(btnNo, "ToolProperties", "IDS_TOOLTIP_NO");
	}
	
	function Show(destinationFaceIndex:Number):Void {
		destinationFaceIndexValue= destinationFaceIndex;
	}
	
	private function btnYes_OnClick() {
		SATracking.CopyDesign();
		var destinationFace = _global.Faces[destinationFaceIndexValue];
	
		_global.CurrentFace.Hide();					
		var faceXml:XMLNode = new XMLNode(1, "Face");
		var fakeNode:XMLNode = new XMLNode(1, "fakeNode");
		faceXml.appendChild(fakeNode);
		
		_global.CurrentFace.AddUnitNodes(faceXml);
		
		destinationFace.Clear();
		destinationFace.ParseXMLUnits(faceXml);
		destinationFace.Draw();					
		destinationFace.Show();
		destinationFace.ChangeUnitCoordinatesUnderTransfer
		(_global.CurrentFace.ScaleFactor);					
		
		_global.CurrentFace = destinationFace;					
		FaceTabBack.getInstance().NormalizeState();
		OnExit();
  	}
	
	private function btnNo_OnClick() {
		ToolPropertiesContainer.GetInstance().ShowFillProperties();
  		OnExit();
  	}
  
  	//region Exit event
  	private function OnExit() {
  		var eventObject = {type:"exit", target:this};
  		this.dispatchEvent(eventObject);
  	}
  	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
  		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
  	}
  	//endregion
}