﻿//
import mx.events.EventDispatcher;
import mx.utils.Delegate;

[Event("onInvoked")]
class SAPlugins {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SAPlugins.prototype);
	
	public static var isDiscovered:Boolean = false;
	public static var isAvailable:Boolean = false;
	public static var port:Number = 0;
    public var socket:XMLSocket;
    
	public var test_port:Number;
	public var test_count:Number;

	public static function GetInstance():SAPlugins {
		return new SAPlugins();
	}
	
	public static function IsAvailable():Boolean {
		return SAPlugins.isAvailable;
	}
	
	private function SAPlugins() {
        socket = new XMLSocket();
	}
	
    public function StartDiscovery()
    {
        test_port = 1212;
        test_count = 0;
		TryConnect();
		
		var parent = this;
		socket.onConnect = function(status) {
			if (status) {
				var xml:XML = new XML();
				xml.parseXML("<Discovery />");
				parent.socket.send(xml);
				SAPlugins.isDiscovered = true;
				SAPlugins.isAvailable = true;
				SAPlugins.port = parent.test_port;
				var eventObject = {type:"onDiscovered", target:parent, result:SAPlugins.isAvailable};
				parent.dispatchEvent(eventObject);
				parent.socket.close();
			}
			else {
	        	parent.test_port += 1234;
    	    	parent.test_count += 1;
    	    	if (parent.test_count == 10) {
					SAPlugins.isDiscovered = true;
					var eventObject = {type:"onDiscovered", target:parent, result:SAPlugins.isAvailable};
					parent.dispatchEvent(eventObject);
    	    	}
			}
			parent.TryConnect();
		};
    }
    private function TryConnect()
    {
    	if (SAPlugins.isDiscovered) {
    		socket = null;
    		return;
    	}
		System.security.loadPolicyFile("xmlsocket://localhost:" + test_port.toString());
		var res:Boolean = socket.connect("localhost", test_port);
    }
    
    public function InvokeCommand(cmd:XML): Boolean {
    	if (!SAPlugins.isAvailable) return false;
    	
	    if (!socket.connect("localhost", SAPlugins.port)) {
	        return false;
	    }
	    
	    socket.send(cmd);
		
        var parent = this;
        socket.onXML = function(recvXML) {
			var eventObject = {type:"onInvoked", target:parent, result:recvXML};
		    parent.socket.close();
			parent.dispatchEvent(eventObject);
	    };
	    return true;
    }
    
    public function RegisterOnDiscoveredHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onDiscovered", Delegate.create(scopeObject, callBackFunction));
    }
    public function RegisterOnInvokedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onInvoked", Delegate.create(scopeObject, callBackFunction));
    }
    
}

/* Usage example:

//class variable:
var pluginsService:SAPlugins;

//constructor:
{
	pluginsService = SAPlugins.GetInstance();
	pluginsService.RegisterOnInvokedHandler(this, onInvoked);
}

//to send command:
{
	var test:XML = new XML();
	var root = test.createElement("Test");
	test.appendChild(root);
	pluginsService.InvokeCommand(test);
}

//to receive response:
function onInvoked(eventObject) 
{
	//eventObject.result contains received xml
}

*/
