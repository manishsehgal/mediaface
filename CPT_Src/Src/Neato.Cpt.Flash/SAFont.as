﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;
[Event("onLoaded")]
class SAFont {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SAFont.prototype);

	private var fonts:XML;
	
	public static function GetInstance(fonts:XML):SAFont {
		return new SAFont(fonts);
	}
	
	public function get IsLoaded():Boolean {
		return fonts.loaded;
	}
	
	private function SAFont(fonts:XML) {
		var parent = this;
		
		this.fonts = fonts;
		this.fonts.onLoad = function(success) {
			var eventObject = {type:"onLoaded", target:parent};
			trace("SAFont: dispatch fonts onLoaded");
			parent.dispatchEvent(eventObject);
		};
	}

	public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }

	public function BeginLoad() {
		var currentDate:Date = new Date();
		var url:String = "../fonts.xml?time=" + currentDate.getTime();
		if (SALocalWork.IsLocalWork)
			url = "../Neato.Cpt.WebDesigner/fonts.xml";
		
		fonts.load(url);
    }
    
	public function BeginLoadFontFile(name:String) {
		var url:String = "../FontFile.aspx?name=" + name;
		fonts.load(url);
    }
}