﻿class TableFrameBorder extends FrameBorder {

	function Draw(mu):Void {
		//super.Draw(mu);
		
		var bounds = mu.mc.getBounds(mu.mc);

		var widthes:Array = _global.CurrentUnit.GetColumnsWidth();
		
		var kx = _parent.ScaleX/100;
		var ky = _parent.ScaleY/100;

		var xMin = bounds.xMin*kx * 100 / this._xscale;
		var yMin = bounds.yMin*ky * 100 / this._yscale;
		var xMax = bounds.xMax*kx * 100 / this._xscale;
  		var yMax = bounds.yMax*ky * 100 / this._yscale;
		
		var currWidth:Number = 0;
		
		var step:Number = 3;
		this.clear();
			
		
		var count:Number = 0;
		
		for(var j = 0; j < widthes.length; j++)
		{
			
			var count:Number = 1;
			currWidth += widthes[j];
			trace("Widthes: width: "+currWidth);
			this.moveTo(currWidth+xMin,yMin);
			for(var i = yMin; i < yMax; i += step, ++count) {
				this.lineTo(currWidth+xMin, i);
				this.lineStyle(1, 0xFFFFFF*(count%2));
			}	
		}
		xMax = currWidth * kx * 100 / this._xscale;
		var step:Number = 3;
		this.moveTo(xMin, yMin);
		lineStyle(1, 0xFFFFFF*(count%2));
		for(var i = yMin; i < yMax; i += step, ++count) {
			this.lineTo(xMin, i);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		for(var i = xMin; i < xMax; i += step, ++count) {
			this.lineTo(i, yMax);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		for(var i = yMax; i > yMin; i -= step, ++count) {
			this.lineTo(xMax, i);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		for(var i = xMax; i > xMin; i -= step, ++count) {
			this.lineTo(i, yMin);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		this.lineTo(xMin, yMin);
		
	
	}
}
