﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;
[Event("onLoaded")]
class SALocale {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SALocale.prototype);
	private var isLoaded:Boolean = false;
	
	private var defaultLang:String;
	private var longLang:String;
	private var shortLang:String;
	
	private var flaNames:Array;
	
	private var count:Number;
	private var maxCount:Number;
	
	public static function GetInstance():SALocale {
		return new SALocale();
	}
	
	public function get IsLoaded():Boolean {
		return isLoaded;
	}

	private function SALocale() {
		isLoaded = false;
		
		flaNames = new Array("PreviewDescription", 
							 "PreviewMenu", 
							 "DesignerMenu",
							 "Toolbar",
							 "ToolProperties",
							 "FaceTab",
							 "QuickTool",
							 "Header");
	}

	public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }
	
	function onload(success:Boolean, lang:String, fla:String, doc:XML) {
		if (success) {
			_global.LocalHelper.AddStringsXML(lang, fla, doc);
		}

		++count;
		if (count == maxCount) {
			isLoaded = true;
			var eventObject = {type:"onLoaded", target:this, status:true};
			trace("SALocale: dispatch styles onLoaded");
			dispatchEvent(eventObject);
		}
	}
	
	public function LoadXml(language:String, flaName:String) {
		var url:String = "../Flash/";
		url += language;
		url += "/";
		url += flaName;
		url += "_";
		url += language;
		url += ".xml";
		
		var parent = this;
		var xml:XML = new XML();
		xml.ignoreWhite = true;
		xml.onLoad = function(success) {
			parent.onload(success, language, flaName, xml);
		};
		xml.load(url);
	}
	
	public function BeginLoad() {
		defaultLang = "en";
		if (_global.culture != undefined && _global.culture.length > 1)
			longLang = _global.culture;
		else
			longLang = defaultLang;
		shortLang = longLang.substr(0,2);

		_global.LocalHelper = new LocaleHelper(longLang, defaultLang);
		
		count = 0;
		if (longLang == shortLang && shortLang == defaultLang) {
			maxCount = flaNames.length;
			for (var i in flaNames) {
				LoadXml(defaultLang, flaNames[i]);
			}
		} else if (longLang == shortLang) {
			maxCount = flaNames.length * 2;
			for (var i in flaNames) {
				LoadXml(shortLang, flaNames[i]);
				LoadXml(defaultLang, flaNames[i]);
			}
		} else {
			maxCount = flaNames.length * 3;
			for (var i in flaNames) {
				LoadXml(longLang, flaNames[i]);
				LoadXml(shortLang, flaNames[i]);
				LoadXml(defaultLang, flaNames[i]);
			}
		}
    }
}