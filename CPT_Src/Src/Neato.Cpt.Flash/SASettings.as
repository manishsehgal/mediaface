﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;
[Event("onLoaded")]
class SASettings {
	public var addEventListener:Function;
	public var removeEventListener:Function;
	private var dispatchEvent:Function;
	private static var _event_mixin = EventDispatcher.initialize(SASettings.prototype);

	private var settings:LoadVars;
	private var isLoaded:Boolean;

	public static function GetInstance():SASettings {
		return new SASettings();
	}
	
	public function get IsLoaded():Boolean {
		return isLoaded;
	}
	
	private function SASettings() {
		isLoaded = false;
		var parent = this;
		settings = new LoadVars();
		
		settings.onLoad = function(success) {
			parent.ApplyVars();
			var eventObject = {type:"onLoaded", target:parent, status:success};
			trace("SASettings: dispatch styles onLoaded");
			parent.dispatchEvent(eventObject);
		};
	}

	public function RegisterOnLoadedHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("onLoaded", Delegate.create(scopeObject, callBackFunction));
    }

	public function BeginLoad() {
		var currentDate:Date = new Date();
		var url:String = "Settings.aspx?time=" + currentDate.getTime();
		settings.load(url);
		BrowserHelper.InvokePageScript("log", "SASettings.as: css loaded");
    }

	public function ApplyVars() {
		isLoaded = true;
		
		_global.IsCalibrationSpecified = true;	
		_global.xcalibration = parseFloat(settings.xcalibration);
		_global.ycalibration = parseFloat(settings.ycalibration);
		_global.culture = settings.culture;
		_global.browser = settings.browser;
		_global.platform = settings.platform;

		if (isNaN(_global.xcalibration)) {
			_global.xcalibration = 0;
			_global.IsCalibrationSpecified = false;
		}
		if (isNaN(_global.ycalibration)) {
			_global.ycalibration = 0;
			_global.IsCalibrationSpecified = false;
		}
		if (_global.culture == undefined)
			_global.culture = "en";
	}
}