﻿import mx.controls.Label;
import mx.controls.TextArea;
import mx.controls.TextInput;

class Calibration extends MovieClip {
	private var xSlider:Slider;
	private var ySlider:Slider;

	private var lblCaption:Label;
	private var lblDescription:TextArea;
	private var lblAValue:Label;
	private var lblBValue:Label;
	private var txtAValue:TextInput;
	private var txtBValue:TextInput;
	private var lblAdvancedCalibration:Label;
	private var btnOpenTestPage:MenuButton;
	private var btnSaveSettings:MenuButton;
	
	private var zeroPointX:Number = 45;
	private var zeroPointY:Number = 135;
	private var maxShift:Number = 45;
	private var safeShift:Number = 5;
	private var maxShiftForSliders:Number = 10.5;
	private var coeffMMtoPoints:Number = 72/25.4;
	private var coeffABtoMM:Number = 25.4/32;
		
	function Calibration() {
		lblCaption.setStyle("styleName", "PreviewCaptionText");
		lblDescription.setStyle("styleName", "PreviewDescriptionText");
		lblAValue.setStyle("styleName", "PreviewABValueCaption");
		lblBValue.setStyle("styleName", "PreviewABValueCaption");
		txtAValue.setStyle("styleName", "PreviewABValueInput");
		txtBValue.setStyle("styleName", "PreviewABValueInput");
		lblAdvancedCalibration.setStyle("styleName", "PreviewAdvancedCalibrationText");

		btnOpenTestPage.setStyle("styleName", "PreviewButtons");
		btnSaveSettings.setStyle("styleName", "PreviewButtons");
		
		btnOpenTestPage.onRelease = function() {
			getURL("Calibration.pdf", "_blank");
		};
		
		btnSaveSettings.onRelease = onSaveSettings;
		
		lblCaption._visible = false;
		lblAValue._visible = false;
		lblBValue._visible = false;
		txtAValue._visible = false;
		txtBValue._visible = false;
		lblAdvancedCalibration._visible = false;
		btnOpenTestPage._visible = false;
		btnSaveSettings._visible = false;
	}
	
	function onLoad() {
		updateSliders();
		updateAbControls();
		_global.LocalHelper.LocalizeInstance(xSlider.lblLeft,"PreviewDescription","IDS_LBL_LEFT");
		_global.LocalHelper.LocalizeInstance(xSlider.lblRight,"PreviewDescription","IDS_LBL_RIGHT");
		_global.LocalHelper.LocalizeInstance(ySlider.lblLeft,"PreviewDescription","IDS_LBL_UP");
		_global.LocalHelper.LocalizeInstance(ySlider.lblRight,"PreviewDescription","IDS_LBL_DOWN");
		_global.LocalHelper.LocalizeInstance(xSlider.lblTitle,"PreviewDescription","IDS_SHIFT_HORIZONTALY_TEXT");
		_global.LocalHelper.LocalizeInstance(ySlider.lblTitle,"PreviewDescription","IDS_SHIFT_VERTICALLY_TEXT");
			
		xSlider.RegisterOnChangeHandler(this, onXChanged);
		ySlider.RegisterOnChangeHandler(this, onYChanged);
		
		var id = "IDS_LBLCALIBRATEDESC";
		if (_global.platform.toLowerCase().indexOf("mac") != -1) {
			id = "IDS_LBLCALIBRATEDESC_MAC";
		}
		_global.LocalHelper.LocalizeInstance(lblDescription,"PreviewDescription",id);
		
		_global.LocalHelper.LocalizeInstance(lblCaption,"PreviewDescription","IDS_LBLCALIBRATECAPTION");
		_global.LocalHelper.LocalizeInstance(lblAValue,"PreviewDescription","IDS_LBLAVALUE");
		_global.LocalHelper.LocalizeInstance(lblBValue,"PreviewDescription","IDS_LBLBVALUE");
		_global.LocalHelper.LocalizeInstance(lblAdvancedCalibration,"PreviewDescription","IDS_LBLADVANCEDCALIBRATION");
		_global.LocalHelper.LocalizeInstance(btnOpenTestPage,"PreviewDescription","IDS_BTNOPENTESTPAGE");
		_global.LocalHelper.LocalizeInstance(btnSaveSettings,"PreviewDescription","IDS_BTNSAVESETTINGS");

		//TooltipHelper.SetTooltip(_parent.ctlCalibration.btnOpenTestPage, "PreviewMenu", "IDS_TOOLTIP_PRINT");
		//TooltipHelper.SetTooltip(_parent.ctlCalibration.btnSaveSettings, "PreviewMenu", "IDS_TOOLTIP_SAVE");		
		
		//-------------lblDescription-------------
		var styleObject:Object = StylesHelper.GetStyle(".Preview.DescriptionText");
		var htmlStyle:Object = new Object();
		for(var i in styleObject)
			htmlStyle[i] = styleObject[i];
		
		var styles = new TextField.StyleSheet();
		styles.setStyle("html", htmlStyle);
		
		lblDescription.styleSheet = styles;
	}
	
	function onXChanged() {
		_global.SetXCalibration(xSlider.Points);
		applyCalibration();	
		updateAbControls();
		xSlider.sliderHandle._visible = true;
		xSlider.lblValue._visible = true;
	}
	
	function onYChanged() {
		_global.SetYCalibration(ySlider.Points);
		applyCalibration();
		updateAbControls();
		ySlider.sliderHandle._visible = true;
		ySlider.lblValue._visible = true;
	}
	
	function applyCalibration() {
		_root.pnlPreviewLayer.content.paper.calibrate(_global.xcalibration, _global.ycalibration);
		var vars:String = "x=" + _global.xcalibration+ "&y=" + _global.ycalibration;
		BrowserHelper.InvokePageScript("calibration", vars);
	}
	
	function onSaveSettings() {
		var xc:Number = parseFloat(_parent.txtAValue.text);
		var yc:Number = parseFloat(_parent.txtBValue.text);
		
		var xPrev:Number = (Math.round(10 * _global.xcalibration / (_parent.coeffMMtoPoints * _parent.coeffABtoMM)) / 10) + _parent.zeroPointX;
		var yPrev:Number = (Math.round(10 * _global.ycalibration / (_parent.coeffMMtoPoints * _parent.coeffABtoMM)) / 10) + _parent.zeroPointY;
		
		var xMin:Number = _parent.zeroPointX - _parent.maxShift + _parent.safeShift;
		var xMax:Number = _parent.zeroPointX + _parent.maxShift;
		var yMin:Number = _parent.zeroPointY - _parent.maxShift + _parent.safeShift;
		var yMax:Number = _parent.zeroPointY + _parent.maxShift;
		
		if (isNaN(xc)) {
			xc = xPrev; _parent.txtAValue.text = xc.toString();
		} else if (xc < xMin) {
			xc = xMin; _parent.txtAValue.text = xc.toString();
			} else if (xc > xMax) {
				xc = xMax; _parent.txtAValue.text = xc.toString();
				}
		
		if (isNaN(yc)) {
			yc = yPrev; _parent.txtBValue.text = yc.toString();
		} if (yc < yMin) {
			yc = yMin; _parent.txtBValue.text = yc.toString();
		} if (yc > yMax) {
			yc = yMax; _parent.txtBValue.text = yc.toString();
		}

		_global.SetXCalibration((xc - _parent.zeroPointX) * _parent.coeffABtoMM * _parent.coeffMMtoPoints);
		_global.SetYCalibration((yc - _parent.zeroPointY) * _parent.coeffABtoMM * _parent.coeffMMtoPoints);
		_parent.applyCalibration();
		_parent.updateSliders();
	}
	
	function updateAbControls() {
		txtAValue.text = ((Math.round(10 * _global.xcalibration / (coeffMMtoPoints * coeffABtoMM)) / 10) + zeroPointX).toString();
		txtBValue.text = ((Math.round(10 * _global.ycalibration / (coeffMMtoPoints * coeffABtoMM)) / 10) + zeroPointY).toString();
	}
	
	function checkSlidersVisibility() {
		xSlider.sliderHandle._visible = true;
		ySlider.sliderHandle._visible = true;
		xSlider.lblValue._visible = true;
		ySlider.lblValue._visible = true;
		
		if ((_global.xcalibration / coeffMMtoPoints) < - maxShiftForSliders ||
			(_global.xcalibration / coeffMMtoPoints) > + maxShiftForSliders) {
			xSlider.sliderHandle._visible = false;
			xSlider.lblValue._visible = false;
		}
		
		if ((_global.ycalibration / coeffMMtoPoints) < - maxShiftForSliders ||
			(_global.ycalibration / coeffMMtoPoints) > + maxShiftForSliders) {
			ySlider.sliderHandle._visible = false;
			ySlider.lblValue._visible = false;
		}
	}
	
	function updateSliders() {
		xSlider.Points = _global.xcalibration;
		ySlider.Points = _global.ycalibration;
		
		checkSlidersVisibility();
	}
}