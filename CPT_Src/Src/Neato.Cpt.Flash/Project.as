﻿class Project {
	public function Project() {}
	public static function DuplicateImageUnit(id) {
		for(var index = 0; index < _global.Faces.length; index++) {
			var imageUnit = _global.Faces[index].FindImageUnit(id);
			if (imageUnit != undefined) {
				var unitNode:XMLNode = imageUnit.GetXmlNode();
				imageUnit = _global.CurrentFace.DuplicateImageUnit(unitNode);
				return;
			}
		}
	}
}