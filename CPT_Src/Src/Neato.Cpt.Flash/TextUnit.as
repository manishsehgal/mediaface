﻿class TextUnit extends MoveableUnit {
	private var textField:TextField;
	public var margin2px:Number = 2;
	public var letterCount:Number = 0;
	//private var allText:Boolean = true;
	private var align:String = "left";
	private var layoutPosition:Number; 
	private var emptyLayoutItem:Boolean;
	

	public var frameLinkageName = "Frame";
	
	public static var UnitClassName = "TextUnit";
	function TextUnit(mc:MovieClip, node:XML) {
		super(mc, node);
		if(mc.textField == undefined)
			mc.createTextField("textField", 0, 0, 0, 1, 1);
		
		textField = mc.textField;
		textField.embedFonts = true;
		textField.autoSize = true;
		textField._visible = false;
		
		var fontFormat:TextFormat = textField.getNewTextFormat();
		fontFormat.font = "arial";
		fontFormat.size = 16;
		fontFormat.bold = false;
		fontFormat.italic = false;
		fontFormat.color = 0x000000;
		if (!_global.Fonts.TestLoadedFont(fontFormat))
			_global.Fonts.LoadFont(fontFormat, this);
			
		textField.setTextFormat(fontFormat);
		textField.setNewTextFormat(fontFormat);
		textField.text = " ";
				
		if (node != null) {
			//trace("node = "+node);
			textField.text = node.attributes.text;
			layoutPosition = node.attributes.layoutPosition;
			align = node.attributes.align;
			this.Visible = node.attributes.visible.toString() == "true" ? true : false;
			this.EmptyLayoutItem = node.attributes.emptyLayoutItem.toString() == "true" ? true : false;
			for (var i = 0; i < node.childNodes.length; ++i) {
				var childNode:XML = node.childNodes[i];
				switch(childNode.nodeName) {
					case "Style":
						//fontFormat = new TextFormat();
						fontFormat.font = childNode.attributes.font;
						fontFormat.size = Number(childNode.attributes.size);
						fontFormat.bold = childNode.attributes.bold.toString() == "true" ? true : false;
						fontFormat.italic = childNode.attributes.italic.toString() == "true" ? true : false;
						fontFormat.color = parseInt(childNode.attributes.color.substr(1), 16);
						if (!_global.Fonts.TestLoadedFont(fontFormat))
							_global.Fonts.LoadFont(fontFormat, this);
						textField.setTextFormat(i,fontFormat);
						textField.newTextFormat(fontFormat);
						break;
					
					default:
						//trace("Unexpected tag name! " + childNode.nodeName);
				}
			}
		} else {
			textField.text = "";
			width  = 31;
			height = 31;
		}		
	}
	
	function GetXmlNode():XMLNode {
		if (textField.text == "" && layoutPosition == undefined) {
			return null;
		}
		var node:XMLNode = super.GetXmlNode();
		node.nodeName = "Text";
		node.attributes.text = textField.text;
		node.attributes.layoutPosition = layoutPosition;
		node.attributes.align = align;
		node.attributes.visible = this.Visible;
		node.attributes.emptyLayoutItem = this.EmptyLayoutItem;
		for (var index in node.attributes) {
			if (node.attributes[index] == "scaleX" ||
				node.attributes[index] == "scaleY" ||
				node.attributes[index] == "initScale")
				node.attributes.splice(index, 1);
		}
		
		for (var i = 0; i < textField.length; ++i) {
			var styleNode:XMLNode = new XMLNode(1, "Style");
			var fontFormat:TextFormat = textField.getTextFormat(i);
			styleNode.attributes.font = fontFormat.font;
			styleNode.attributes.size = fontFormat.size;
			styleNode.attributes.bold = fontFormat.bold;
			styleNode.attributes.italic = fontFormat.italic;
			styleNode.attributes.color = _global.GetColorForXML(fontFormat.color);
			node.appendChild(styleNode);
		}

		
		
		node.appendChild(super.GetXmlNodeRotate());
		
		
		return node;
	}
	
	public function ChangeFontFormat(indexBegin:Number, indexEnd:Number, format:TextFormat):Void {
		// Any property of textFormat that is set to null will not be applied
		if (!_global.Fonts.TestLoadedFont(format)) {
			_global.Fonts.LoadFont(format, this);
		}
		textField.setNewTextFormat(format);

		for(var index:Number = indexBegin; index <= indexEnd; ++index) {
			var begin:Number = (index == indexBegin) ? index : index - 1;
			var end:Number =  index;
			textField.setTextFormat(begin, end, format);
		}

		CalcSize(mc, true, true);
		DrawMC(mc, true);
		
		
	}
	
	public function ReplaceText(indexBegin:Number, indexEnd:Number, newSubsrting:String, format:TextFormat):Void {
		// Any property of textFormat that is set to null will not be applied
		textField.setNewTextFormat(format);
			
		if (newSubsrting.length > 0) {
			textField.replaceText(indexBegin, indexEnd, newSubsrting);
			textField.setTextFormat(indexBegin, indexBegin+newSubsrting.length, format);
			if (!_global.Fonts.TestLoadedFont(format)) {
				_global.Fonts.LoadFont(format, this);
			}
		} else if (indexBegin == 0 && indexEnd == textField.text.length) { 
			textField.text = newSubsrting;
		} else { 
			//removing range
			var txt:String = textField.text.substr(0, indexBegin);
			txt += textField.text.substr(indexEnd, textField.length);
			
			var fontFormats:Array = new Array();
			
			for (var i = 0; i < indexBegin; ++i) {
				fontFormats.push(textField.getTextFormat(i));
			}
			
			for (var i = indexEnd; i < textField.length; ++i) {
				fontFormats.push(textField.getTextFormat(i));
			}
			
			textField.text = txt;
			for (var i = 0; (i < fontFormats.length) || (i == 0 && textField.length == 0); ++i) {
				textField.setTextFormat(i, fontFormats[i]);
			}
		}
			
		
		CalcSize(mc, true, true);
		DrawMC(mc, true);
		
	}
	

	public function GetFontFormat(index:Number):TextFormat {
		if (textField.length == 0) 
			return textField.getNewTextFormat();
		return textField.getTextFormat(index);
	}
	
	public function get Text():String {
		return textField.text;
	}

	function RestoreText() {
		trace("simple text");
			this.ScaleX = 100;
			this.ScaleY = 100;
			mc.textField._x = 0;
			mc.textField._y = 0;
			textField.autoSize = false;
			textField._visible = true;		
			
					
		
		Draw();
		
	}	
	
	function Draw():Void {
		DrawMC(mc, true);
	}
	

	function DrawMC(mc:MovieClip, drawOnWorkArea:Boolean):Void {
		
		
		//	mc.txt._visible = true;
		//for(var i in mc)
			
			
			CalcSize(mc, false, drawOnWorkArea);
			mc.beginFill(0, 0);
			mc.moveTo(0, 0);
			mc.lineTo(width, 0);
			mc.lineTo(width, height);
			mc.lineTo(0, height);
			mc.endFill();
		
 		if (_global.CurrentUnit == this)
 			_global.CurrentFace.UpdateSelection(this);
	}
	
	function CalcSize(mc:MovieClip, doOffset:Boolean, drawOnWorkArea:Boolean):Void {
		mc.clear();
		mc.txt.removeTextField();
		mc.createTextField("txt", 1, -margin2px, -margin2px, 100, 100);
		var txt:TextField = mc.txt;
		txt._visible = this.Visible && (drawOnWorkArea || !EmptyLayoutItem);
		txt.embedFonts = true;
		txt.autoSize = true;
		txt.text = " " + textField.text + " ";
		txt.selectable = false;
		
		var oldW:Number = width;
		
		if (textField.length == 0) {
			width = 31;
			height = 31;
		} else {
			var fontFormat:TextFormat = textField.getTextFormat(0);
			TestFontFormat(fontFormat);
			txt.setTextFormat(0, fontFormat);
			fontFormat = textField.getTextFormat(textField.length - 1); //first space
			TestFontFormat(fontFormat);
			txt.setTextFormat(textField.length + 1, fontFormat); //last space
			for (var i = 0; i < textField.length; ++i) {
				fontFormat = textField.getTextFormat(i);
				TestFontFormat(fontFormat);
				txt.setTextFormat(i+1, fontFormat);
			}
			
			width = txt._width - margin2px - margin2px;
			height = txt._height - margin2px - margin2px;
		}
		
		var deltaW:Number = oldW - width;
		if (deltaW != 0 && doOffset == true) {
			if (align == "center") {
				Offset(deltaW/2, 0);
			}
			else if (align == "right") {
				Offset(deltaW, 0);
			}
		}
	}
	
	private function TestFontFormat(fontFormat:TextFormat):Void {
		if (!_global.Fonts.TestFont(fontFormat.font, false, true))
			fontFormat.bold = false;
		if (!_global.Fonts.TestFont(fontFormat.font, true, false))
			fontFormat.italic = false;
		if (!_global.Fonts.TestLoadedFont(fontFormat))
			_global.Fonts.LoadFont(fontFormat, this);
	}
	
	function getMode():String {
		return _global.TextMode;
	}
	
	function get Size():Number {
		var size:Number = 0;
		var fontFormat:TextFormat;
		for (var i = 0; i < textField.length; ++i) {
			fontFormat = textField.getTextFormat(i);
			size = Math.max(size, fontFormat.size);
		}
  		return size;
	}

	function set Size(value:Number):Void {
		var oldSize:Number = Size;
		if (oldSize == value)
			return;
			
		var fontFormat:TextFormat;
		var minSize:Number = 127;
		for (var i = 0; i < textField.length; ++i) {
			fontFormat = textField.getTextFormat(i);
			minSize = Math.min(minSize, fontFormat .size);
		}

		var delta:Number = value - oldSize;
		if (delta <= 4 - minSize)
			delta = 4 - minSize;
		if (delta >= 127 - oldSize)
			delta = 127 - oldSize;
			
		for (var i = 0; i < textField.length; ++i) {
			fontFormat = textField.getTextFormat(i);
			fontFormat.size += delta;
			fontFormat.size = Math.max(fontFormat.size, 4);
			fontFormat.size = Math.min(fontFormat.size, 127);
			textField.setTextFormat(i, fontFormat);
		}

		Draw(mc);
	}
	
	public function ApplyLayoutFormat(layout:FaceLayoutItem, putNewText:Boolean):Void {		
		var format:TextFormat = new TextFormat();
		format.font = layout.Font;
		format.size = layout.Size;
		format.bold = layout.Bold;
		format.italic = layout.Italic;
		format.color = layout.Color;

		var actualText = (putNewText) ? layout.Text : Text;
		ReplaceText(0, actualText.length, actualText, format);
		
		this.X = layout.X;
		this.Y = layout.Y;
		this.Angle = layout.Angle;
		this.Align = layout.Align;
		this.EmptyLayoutItem = layout.EmptyLayoutItem;
	}

	public function ResizeByStep(isPositive:Boolean) {
		
			this.ScaleX = 100;
			this.ScaleY = 100;
			var step:Number = isPositive ? 1 : -1;
			var k = 1 + step / this.Size; 
			Resize(1, k);
		
	}

	private function Resize(kx:Number, ky:Number) {
		
			this.ScaleX = 100;
			this.ScaleY = 100;
			var newSize  = Math.round(this.Size * ky);
			if (this.Text.length > 0 &&	newSize >=4 && newSize <= 127 && newSize != this.Size) {
				this.Size = newSize;
				ToolPropertiesContainer.GetInstance().TextPropertiesClip.RefreshFontSize();
			}
		
		
	}

	public function get Align():String {
		return align;
	}
	public function set Align(value:String):Void {
		align = value;
	}
	
	public function get Visible():Boolean {
		return mc._visible;
	}
	public function set Visible(value:Boolean):Void {
		trace("TextUnit:SetVisible: value = "+value);
		mc._visible = value;
	}
	
	public function get EmptyLayoutItem():Boolean {
		return emptyLayoutItem;
	}
	public function set EmptyLayoutItem(value:Boolean):Void {
		emptyLayoutItem = value;
	}
	
	public function get LayoutPosition():Number {
		return layoutPosition;
	}
	public function set LayoutPosition(value:Number):Void {
		layoutPosition = value;
	}
	
	
}
