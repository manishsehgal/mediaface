﻿import mx.core.UIObject;

class ButtonDrawHelper extends UIObject {
	private	var CountourColor:Number = 0xFFFFFF;
	private	var xStartPos:Number;
	private	var yStartPos:Number;
	private	var shapeGap:Number;
	private	var scaleReduceFactor:Number = 0.8;
	private	var widthEmptyButton:Number = 0;
	private	var heightEmptyButton:Number = 0;
	private var index:Number = 0;
	private var parentMC:MovieClip;
	
	function ButtonDrawHelper() {}
	
	function Initialize(parentClip:MovieClip, xStart:Number, yStart:Number, gap:Number){
		index = 0;
		parentMC = parentClip;
		xStartPos = xStart;
		yStartPos = yStart;
		shapeGap = gap;
	}
	
	function CreateButton(face:FaceUnit):MovieClip {
		var id = "btn" + index;
		
		var btn:MovieClip = parentMC.attachMovie("BtnMiniBar", id, parentMC.getNextHighestDepth());
		
		btn.id = id;
		btn._x = xStartPos + index * (btn._width + shapeGap);
		btn._y = yStartPos;

		index++;
		return btn;
	}

	function CreateContour(face:FaceUnit, btn:MovieClip):MovieClip {
		var contour:MovieClip = btn.createEmptyMovieClip("mcContour", btn.getNextHighestDepth());
		contour.lineStyle(0, CountourColor);
		face.DrawContour(contour, true);
		return contour;
	}
	
	function ReduceContour(contour:MovieClip, btn:MovieClip):Void {
		var bounds = contour.getBounds(parentMC);		
		var xMin = bounds["xMin"] - btn._x;
		var yMin = bounds["yMin"] - btn._y;
	
		var widthContour = bounds["xMax"]-bounds["xMin"];
		var heightContour = bounds["yMax"]-bounds["yMin"];
	
		var xF=widthEmptyButton/widthContour*100;
		var yF=heightEmptyButton/heightContour*100;
		var scaleFactor = (xF<yF)?xF:yF;
	
		contour._xscale = contour._yscale = scaleFactor*scaleReduceFactor;
	
		//Move contour to left-high corner of MiniBarButton
		contour._x -= xMin * scaleFactor*scaleReduceFactor/100;
		contour._y -= yMin * scaleFactor*scaleReduceFactor/100;
	
		var bounds2 = contour.getBounds(parentMC);
	
		var widthContour2 = bounds2["xMax"]-bounds2["xMin"];
		var heightContour2 = bounds2["yMax"]-bounds2["yMin"];
		
		//Move contour to center of MiniBarButton
		contour._x += ((widthEmptyButton-widthContour2)/2);	
		contour._y += ((heightEmptyButton-heightContour2)/2);
		
		contour._y -= 2;	//MAGIC :-)
	}
	
	function DrawFaceCaption(faceCaption:String, btn:MovieClip) {
		btn.createObject("Label", "txtFaceCaption", btn.getNextHighestDepth());
		
		/*getURL("javascript:alert('btn._x="+btn._x+"')");
		getURL("javascript:alert('btn._width="+btn._width+"')");
		getURL("javascript:alert('widthEmptyButton="+widthEmptyButton+"')");*/
		
		btn.txtFaceCaption._x = -4;
		btn.txtFaceCaption._y = heightEmptyButton;
		btn.txtFaceCaption._width = widthEmptyButton+8;
		btn.txtFaceCaption._height = 22;

		btn.txtFaceCaption.text = faceCaption;
		btn.txtFaceCaption.setStyle("styleName","TitleText");
	}
	
	function CreateOneButton(face:FaceUnit):MovieClip {
		var btn:MovieClip = CreateButton(face);

		widthEmptyButton = btn._width;
		heightEmptyButton = btn._height;
		
		var contour:MovieClip = CreateContour(face, btn);		
		ReduceContour(contour, btn);		
		DrawFaceCaption(face.Caption, btn);
		
		return btn;
	}
}