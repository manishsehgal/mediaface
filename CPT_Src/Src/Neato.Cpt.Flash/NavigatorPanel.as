﻿
import mx.core.UIObject;

class NavigatorPanel extends UIObject {
	
	private var btnCopy      : MovieClip;
	private var btnPaste     : MovieClip;
	private var mcViewBorder : MovieClip;
	private var mcPreview    : MovieClip;
	private var maxWidth     : Number;
	private var maxHeight    : Number;
	private var faceXml      : XMLNode;
	private var scaleFactor  : Number;
	
	function NavigatorPanel() {
		faceXml = undefined;
	}
	
	function onLoad() {
		maxWidth  = mcViewBorder._width;
		maxHeight = mcViewBorder._height;
		
		btnCopy.onRelease =  function() {
			this._parent.onCopy();
		}
		btnPaste.onRelease =  function() {
			this._parent.onPaste();
		}
	}
	
	public function DataBind():Void {
		Draw();
	}
	
	function ChangeFace(index:Number){
		var destinationFace = _global.Faces[index];				
		Logic.DesignerLogic.UnselectCurrentUnit();
		_global.CurrentFace.Hide();
		_global.CurrentFace = destinationFace;
		destinationFace.Show();
		DataBind();
	}
	
	function Draw():Void {
		var w:Number = _root.pnlPreviewLayer.content.paper.Width;
		var h:Number = _root.pnlPreviewLayer.content.paper.Height;
		PaperHelper.DrawContours(mcPreview, this, ChangeFace, false, true, _global.Faces, w, h);
		
		var kx:Number = maxWidth  / mcPreview._width;
		var ky:Number = maxHeight / mcPreview._height;
		var k = kx <= ky ? kx : ky;
		mcPreview._xscale = mcPreview._yscale = k * 100;

		mcPreview.lineStyle(2 / k, 0x999999);
		mcPreview.moveTo(0, 0);
		mcPreview.lineTo(0, h);
		mcPreview.lineTo(w, h);
		mcPreview.lineTo(w, 0);
		mcPreview.lineTo(0, 0);
		
		mcPreview._x = mcViewBorder._x + (mcViewBorder._width  -  mcPreview._width)  / 2;
		mcPreview._y = mcViewBorder._y + (mcViewBorder._height -  mcPreview._height) / 2;
	}
	
	function onCopy() {
		faceXml = new XMLNode(1, "Face");
		var fakeNode:XMLNode = new XMLNode(1, "fakeNode");
		faceXml.appendChild(fakeNode);
		_global.CurrentFace.AddUnitNodes(faceXml);
		scaleFactor = _global.CurrentFace.ScaleFactor;
	}
	
	function onPaste() {
		if (faceXml == undefined) return;
		var destinationFace = _global.CurrentFace;
		destinationFace.Clear();
		destinationFace.ParseXMLUnits(faceXml);
		destinationFace.Draw();					
		destinationFace.Show();
		destinationFace.ChangeUnitCoordinatesUnderTransfer(scaleFactor);
	}
}