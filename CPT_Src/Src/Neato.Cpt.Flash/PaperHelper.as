﻿
class PaperHelper {    
    
	public static function Clear(mc:MovieClip):Void {
		for(var i in mc) {
			if (mc[i] instanceof MovieClip)
				mc[i].removeMovieClip();
		}
		mc.clear();
	}

	public static function DrawBackground(mc:MovieClip, width:Number,
        height:Number, color:Number, alpha:Number):Void {
		mc.lineStyle();
		mc.beginFill(color, alpha);
		mc.moveTo(0, 0);
		mc.lineTo(width, 0);
		mc.lineTo(width, height);
		mc.lineTo(0, height);
		mc.lineTo(0, 0);
		mc.endFill();
	}
	
	public static function DrawFaceContour(mc:MovieClip, obj:Object,
    										callBackFunction:Function, 
    										showFrameAroundSelectedFace:Boolean,
    										selectCurrentFace:Boolean, 
    										faces:Array):Void {
		
		var count = faces.length;
		var depth:Number = 2 * count;
		for (var i = 0; i < count; ++i) {
			var face:FaceUnit = faces[i];
						
			var mcFaceContour= mc.createEmptyMovieClip("FaceContour" + i, depth + i);

			if(obj != null && obj != undefined ) {
				DrawBackground(mcFaceContour, mcFaceContour._width, mcFaceContour._height, 0x0, 0);
				mcFaceContour.index = i;
				if (callBackFunction != null && callBackFunction != undefined) {
					mcFaceContour.onPress = function() {
						callBackFunction.call(obj, this.index);
						if (showFrameAroundSelectedFace) DrawFaceAroundFrame(mc, faces[this.index]);
					}
				}
				if(face == _global.CurrentFace && selectCurrentFace)
					mcFaceContour.beginFill(0xFF9900, 100);
				else
					mcFaceContour.beginFill(0xffffff, 0);
		
				if (face == _global.CurrentFace && showFrameAroundSelectedFace) {
					DrawFaceAroundFrame(mc, face);
				}
			}
			mcFaceContour.lineStyle(0, 0xcccccc);
			face.DrawContour(mcFaceContour, true);
			face.DrawCutLines(mcFaceContour);
			mcFaceContour._x = face.PaperX;
			mcFaceContour._y = face.PaperY;
			mcFaceContour.endFill();
            
            var bounds = mcFaceContour.getBounds(mcFaceContour);
            face["width"] = bounds.xMax - bounds.xMin;
            face["height"] = bounds.yMax - bounds.yMin;
		}
	}
	
	public static function DrawMask(mc:MovieClip, width:Number, height:Number):Void {
		var mcFaceMask = mc.createEmptyMovieClip("FaceMask", mc.getNextHighestDepth());
		mcFaceMask.lineStyle(0);
		mcFaceMask.beginFill(0xFFFFFF);
		mcFaceMask.moveTo(-1, -1);
		mcFaceMask.lineTo(width + 1, -1);
		mcFaceMask.lineTo(width + 1, height + 1);
		mcFaceMask.lineTo(-1, height + 1);
		mcFaceMask.lineTo(-1, -1);
		mcFaceMask.endFill();
		mc.setMask(mcFaceMask);
	}
	
	public static function DrawContours(mc:MovieClip, obj:Object, callBackFunction:Function,
        								showFrameAroundSelectedFace:Boolean,
        								selectCurrentFace:Boolean, faces:Array, 
        								width:Number, height:Number):Void {
		mc._xscale = mc._yscale = 100;
		PaperHelper.Clear(mc);
		PaperHelper.DrawBackground(mc, width, height, 0xffffff, 100);
		DrawFaceContour(mc, obj, callBackFunction, showFrameAroundSelectedFace, selectCurrentFace, faces);
		DrawMask(mc, width, height);
	}
	
	public static function DrawFaceAroundFrame(mc:MovieClip, face:FaceUnit):Void {
        var mcFrame:MovieClip = mc.createEmptyMovieClip("mcFrame", 100);		
        mcFrame._x = face.PaperX;
        mcFrame._y = face.PaperY;
        mcFrame.clear();
		
		var xMin = 0;
		var yMin = 0;
		var xMax = face["width"];
  		var yMax = face["height"];
		var frameColor = 0xFF0000;
		var addColor = 0x00FFFF;
		
		mcFrame.moveTo(xMin, yMin);
		var count:Number = 0;
		var step:Number = 20;
		mcFrame.lineStyle(0, frameColor + addColor*(count%2));
		for(var i = yMin; i < yMax; i += step, ++count) {
			mcFrame.lineTo(xMin, i);
			mcFrame.lineStyle(0, frameColor + addColor*(count%2));
		}
		for(var i = xMin; i < xMax; i += step, ++count) {
			mcFrame.lineTo(i, yMax);
			mcFrame.lineStyle(0, frameColor + addColor*(count%2));
		}
		for(var i = yMax; i > yMin; i -= step, ++count) {
			mcFrame.lineTo(xMax, i);
			mcFrame.lineStyle(0, frameColor + addColor*(count%2));
		}
		for(var i = xMax; i > xMin; i -= step, ++count) {
			mcFrame.lineTo(i, yMin);
			mcFrame.lineStyle(0, frameColor + addColor*(count%2));
		}
		mcFrame.lineTo(xMin, yMin);		
    }
}
