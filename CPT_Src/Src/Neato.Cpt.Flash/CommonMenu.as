﻿import mx.core.UIObject;
class CommonMenu extends UIObject {
	private var btnPrint:Button;
	private var btnSave:Button;
	function CommonMenu() {
		trace("CommonMenu constructor");
		_parent._parent.setStyle("styleName", "MenuPanel");
		this.btnPrint.onRelease = function() {
			trace("btnPrint");
			PrintHelper.Print();
		};
		this.btnSave.onRelease = function() {
			trace("btnSave");
			this._parent.SaveProject(this._parent.CommandSave);
		};
	}
	
	function SaveProject(callBackFunction:Function) {
		trace("SaveProject");
		_global.GenerateProjectXml();
		var projectService:SAProject = SAProject.GetInstance(_global.projectXml);
		projectService.RegisterOnSavedHandler(this, callBackFunction);
		projectService.BeginSave();
	}
	
	function CommandPrint(eventObject) {
		trace("CommandPrint");
		SATracking.PrintPDF();
		_parent._parent._parent._parent.content.pnlPreview.spContentHolder.ctlPreview.Show();
	}
	
	function CommandSave(eventObject) {
		trace("CommandSave");
		SATracking.SaveDesign();
		BrowserHelper.InvokePageScript("save", "");
	}
}
