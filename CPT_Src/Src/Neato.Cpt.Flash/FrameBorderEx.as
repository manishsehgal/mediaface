﻿import Geometry.*;
class FrameBorderEx extends MovieClip {

	function Draw(mu):Void {
		//var bounds = mu.mc.getBounds(mu.mc);
		//var bounds= _parent.getBounds(_parent);
		
		
		var kx = _parent.ScaleX/100;
		var ky = _parent.ScaleY/100;

		// +_parent.rotateHandler._width;
		// + _parent.rotateHandler._height;
		// - _parent.resizeHandler._width;
  		// - _parent.resizeHandler._height;
		//this._xscale = kx * 100;
		//this._yscale = ky * 100;
		//_parent._xscale = kx * 100;
		//_parent._yscale = ky * 100;
		
		
		this.clear();
		
		
		
		
		lineStyle(1,0x007FFF);
		var effect = _parent.effect;
		trace("FrameBorderEx:Draw:effect.type="+effect.type+";effect.subtype="+effect.subtype);
		var fontSize:Number = effect.fontSize;
		if(effect.type == "Curved" && effect.subtype == "DoubleArch") {
			for(var i=0 ;i<effect.refPoints.length ; i += 4) {
				moveTo(effect.refPoints[i].x*kx,effect.refPoints[i].y*ky);
				GeometryHelper.DrawCubicBezier(this,
											effect.refPoints[i].x*kx,   effect.refPoints[i].y*ky,
											effect.refPoints[i+1].x*kx, effect.refPoints[i+1].y*ky,
											effect.refPoints[i+2].x*kx, effect.refPoints[i+2].y*ky,
											effect.refPoints[i+3].x*kx, effect.refPoints[i+3].y*ky
											);
			
				//curveTo(effect.refPoints[i+1].x,effect.refPoints[i+1].y,effect.refPoints[i+2].x,effect.refPoints[i+2].y);
			}
			if(effect.refPoints.length == 8) { //refactor:update logic for diff. effects
				moveTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);
				lineTo(effect.refPoints[4].x*kx,effect.refPoints[4].y*ky);
				moveTo(effect.refPoints[3].x*kx,effect.refPoints[3].y*ky);
				lineTo(effect.refPoints[7].x*kx,effect.refPoints[7].y*ky);
			}
		} else if(effect.type == "Curved" && effect.subtype == "Perspective"){
				moveTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);
				lineTo(effect.refPoints[1].x*kx,effect.refPoints[1].y*ky);
				lineTo(effect.refPoints[3].x*kx,effect.refPoints[3].y*ky);
				lineTo(effect.refPoints[2].x*kx,effect.refPoints[2].y*ky);
				lineTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);
		} else if(effect.type == "Curved" && effect.subtype == "Balloon"){
			trace("Here: curved + Balloon");
				moveTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);
				GeometryHelper.DrawArcFrom3Points(this,
					effect.refPoints[0].x*kx,effect.refPoints[0].y*ky,
					effect.refPoints[1].x*kx,effect.refPoints[1].y*ky,
					effect.refPoints[2].x*kx,effect.refPoints[2].y*ky
					);
				moveTo(effect.refPoints[2].x*kx,effect.refPoints[2].y*ky);	
				lineTo(effect.refPoints[5].x*kx,effect.refPoints[5].y*ky);
				GeometryHelper.DrawArcFrom3Points(this,
					effect.refPoints[3].x*kx,effect.refPoints[3].y*ky,
					effect.refPoints[4].x*kx,effect.refPoints[4].y*ky,
					effect.refPoints[5].x*kx,effect.refPoints[5].y*ky
					);
				//curveTo(effect.refPoints[2].x,effect.refPoints[2].y,effect.refPoints[1].x,effect.refPoints[1].y);
				//lineTo(effect.refPoints[2].x,effect.refPoints[2].y);
				moveTo(effect.refPoints[3].x*kx,effect.refPoints[3].y*ky);
				lineTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);
		} else if(effect.type == "Curved" && effect.subtype == "BottomArc"){
				moveTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);
				lineTo(effect.refPoints[1].x*kx,effect.refPoints[1].y*ky);
				lineTo(effect.refPoints[4].x*kx,effect.refPoints[4].y*ky);
				moveTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);	
				lineTo(effect.refPoints[2].x*kx,effect.refPoints[2].y*ky);	
				GeometryHelper.DrawArcFrom3Points(this,
					effect.refPoints[2].x*kx,effect.refPoints[2].y*ky,
					effect.refPoints[3].x*kx,effect.refPoints[3].y*ky,
					effect.refPoints[4].x*kx,effect.refPoints[4].y*ky
					);
		}else if(effect.type == "Curved" && effect.subtype == "TopArc"){
				moveTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);
				GeometryHelper.DrawArcFrom3Points(this,
					effect.refPoints[0].x*kx,effect.refPoints[0].y*ky,
					effect.refPoints[1].x*kx,effect.refPoints[1].y*ky,
					effect.refPoints[2].x*kx,effect.refPoints[2].y*ky
					);
				moveTo(effect.refPoints[2].x*kx,effect.refPoints[2].y*ky);
				lineTo(effect.refPoints[4].x*kx,effect.refPoints[4].y*ky);
				lineTo(effect.refPoints[3].x*kx,effect.refPoints[3].y*ky);
				lineTo(effect.refPoints[0].x*kx,effect.refPoints[0].y*ky);	
		} else if((effect.type == "Guided" && (effect.subtype == "LineAngleDown" || effect.subtype == "LineAngleUp"))
					||(effect.type == "GuidedFit" && effect.subtype == "fit")) {
			var x0 = effect.refPoints[0].x*kx;
			var y0 = effect.refPoints[0].y*ky;
			var x1 = effect.refPoints[1].x*kx;
			var y1 = effect.refPoints[1].y*ky;
			moveTo((effect.refPoints[0].x)*kx ,(effect.refPoints[0].y) *ky);
			lineTo((effect.refPoints[1].x)*kx ,(effect.refPoints[1].y)*ky);

			//var diffAngle:Number = Math.PI/8;
			//var angl:Number = Math.atan2((x1-x0),(y1-y0));///Math.sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0))) ;
			//if(effect.subtype == "LineAngleDown")
				//angl -= diffAngle;
			//else
				//angl += diffAngle;
				//moveTo((effect.refPoints[0].x)*kx - fontSize * Math.sin(angl),(effect.refPoints[0].y) *ky- fontSize * Math.cos(angl));
				//lineTo((effect.refPoints[1].x)*kx - fontSize * Math.sin(angl),(effect.refPoints[1].y)*ky - fontSize * Math.cos(angl));
				//lineTo((effect.refPoints[1].x)*kx + fontSize * Math.sin(angl),(effect.refPoints[1].y)*ky + fontSize * Math.cos(angl));
				//lineTo((effect.refPoints[0].x)*kx + fontSize * Math.sin(angl),(effect.refPoints[0].y)*ky + fontSize * Math.cos(angl));
				//lineTo((effect.refPoints[0].x)*kx - fontSize * Math.sin(angl),(effect.refPoints[0].y)*ky - fontSize * Math.cos(angl));
		} else if(effect.type == "Guided" && effect.subtype == "Curve4" ) {
			
			var x0:Number = effect.refPoints[0].x*kx;
			var y0:Number = effect.refPoints[0].y*ky;
			var x1:Number = effect.refPoints[1].x*kx;
			var y1:Number = effect.refPoints[1].y*ky;
			var x2:Number = effect.refPoints[2].x*kx;
			var y2:Number = effect.refPoints[2].y*ky;
			var x3:Number = effect.refPoints[3].x*kx;
			var y3:Number = effect.refPoints[3].y*ky;
			
			var angl1:Number = Math.atan2((y1-y0),(x1-x0));///Math.sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0))) ;
			var angl2:Number = Math.atan2((y2-y1),(x2-x1));
			var angl3:Number = Math.atan2((y3-y2),(x3-x2));
			moveTo(x0,y0);
			GeometryHelper.DrawCubicBezier(this,
						(x0),(y0),
						(x1),(y1),
						(x2),(y2),
						(x3),(y3)
						);			
			 //+ 10 * sin(angl1), y0 + 10 * cos(angl1));
			
		//	lineTo(x1,y1);
			//lineTo(x2,y2);
			//lineTo(x3,y3);
			/*moveTo(x0 + fontSize * Math.sin(angl1), y0 - fontSize * Math.cos(angl1) );
			GeometryHelper.DrawCubicBezier(this,
		
						(x0 + fontSize * Math.sin(angl1))*kx, (y0 - fontSize * Math.cos(angl1))*ky,
						(x1 + fontSize * Math.sin((angl1+angl2) / 2))*kx, (y1 - fontSize * Math.cos((angl1 + angl2) / 2))*ky,
						(x2 + fontSize * Math.sin((angl2 + angl3) / 2))*kx,( y2 - fontSize * Math.cos((angl2+angl3) / 2))*ky,
						(x3 + fontSize * Math.sin(angl3))*kx, (y3 - fontSize * Math.cos(angl3))*ky
						);			
			lineTo((x3 - fontSize * Math.sin(angl3))*kx, (y3 + fontSize * Math.cos(angl3))*ky);
			moveTo((x0 - fontSize * Math.sin(angl1))*kx, (y0 + fontSize * Math.cos(angl1))*ky);
			GeometryHelper.DrawCubicBezier(this,
		
						(x0 - fontSize * Math.sin(angl1))*kx, (y0 + fontSize * Math.cos(angl1))*ky,
						(x1 - fontSize * Math.sin((angl1+angl2) / 2))*kx, (y1 + fontSize * Math.cos((angl1 + angl2) / 2))*ky,
						(x2 - fontSize * Math.sin((angl2 + angl3) / 2))*kx, (y2 + fontSize * Math.cos((angl2+angl3) / 2))*ky,
						(x3 - fontSize * Math.sin(angl3))*kx, (y3 + fontSize * Math.cos(angl3))*ky
						);			
			moveTo((x0 - fontSize * Math.sin(angl1))*kx,( y0 + fontSize * Math.cos(angl1))*ky);
			lineTo((x0 + fontSize * Math.sin(angl1))*kx, (y0 - fontSize * Math.cos(angl1))*ky);
			*/
			
		} else if(effect.type == "Circle" || effect.type == "Button") {
			var x0 = effect.refPoints[0].x *kx;
			var y0 = effect.refPoints[0].y*ky;
			var x1 = effect.refPoints[1].x*kx;
			var y1 = effect.refPoints[1].y*ky;
			var x2 = effect.refPoints[2].x*kx;
			var y2 = effect.refPoints[2].y*ky;
			var x3 = effect.refPoints[3].x*kx;
			var y3 = effect.refPoints[3].y*ky;
			var tmpXml:XML = new XML("<Obj/>");
			
			if(x2<x0)x2 = x0;
			if(y3<y1)y3 = y0;
			tmpXml.attributes.x = (x2+x0) / 2;
			tmpXml.attributes.y = (y3+y1) / 2;
			tmpXml.attributes.rH = (x2-x0) / 2;
			tmpXml.attributes.rV = (y3-y1) / 2;
			
			var circle = new EllipsePrimitive(tmpXml);
			effect.refPoints[0].y = (y3+y1) / 2;
			effect.refPoints[2].y = (y3+y1) / 2;
			effect.refPoints[1].x = (x2+x0) / 2;
			effect.refPoints[3].x = (x2+x0) / 2;
			circle.Draw(this,0,0,0);

		} else if(effect.type == "GuidedFit" && effect.subtype == "CurveArc" ) {
					GeometryHelper.DrawArcFrom3Points(this,
						effect.refPoints[0].x*kx,effect.refPoints[0].y*ky,
						effect.refPoints[1].x*kx,effect.refPoints[1].y*ky,
						effect.refPoints[2].x*kx,effect.refPoints[2].y*ky
					);
		}
		
		var bounds = mu.mc.getBounds(mu.mc);
		var boundsThis = this.getBounds(this);
		var boundsParent = _parent.getBounds(this);
		
		
		var xMin = bounds.xMin*kx * 100 / this._xscale;
		var yMin = bounds.yMin*ky * 100 / this._yscale;
		var xMax = bounds.xMax*kx * 100 / this._xscale;
		var yMax = bounds.yMax*ky * 100 / this._yscale;
				
		var xMin1 = boundsThis.xMin;
		var yMin1 = boundsThis.yMin;
		var xMax1 = boundsThis.xMax;
		var yMax1 = boundsThis.yMax;
		
		var xMin2 = boundsParent.xMin+_parent.rotateHandler._width;
		var yMin2 = boundsParent.yMin+_parent.rotateHandler._width;
		var xMax2 = boundsParent.xMax-_parent.resizeHandler._width;
		var yMax2 = boundsParent.yMax-_parent.resizeHandler._width;
		
		xMin = xMin<xMin1?xMin:xMin1;
		yMin = yMin<yMin1?yMin:yMin1;
		xMax = xMax>xMax1?xMax:xMax1;
		yMax = yMax>yMax1?yMax:yMax1;
		
		/*xMin = xMin<xMin2?xMin:xMin2;
		yMin = yMin<yMin2?yMin:yMin2;
		xMax = xMax>xMax2?xMax:xMax2;
		yMax = yMax>yMax2?yMax:yMax2;*/

		
		this.moveTo(xMin, yMin);
		var count:Number = 0;
		var step:Number = 3;
		this.lineStyle(1, 0xFFFFFF*(count%2));
		for(var i = yMin; i < yMax; i += step, ++count) {
			this.lineTo(xMin, i);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		for(var i = xMin; i < xMax; i += step, ++count) {
			this.lineTo(i, yMax);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		for(var i = yMax; i > yMin; i -= step, ++count) {
			this.lineTo(xMax, i);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		for(var i = xMax; i > xMin; i -= step, ++count) {
			this.lineTo(i, yMin);
			this.lineStyle(1, 0xFFFFFF*(count%2));
		}
		this.lineTo(xMin, yMin);
		
		
		
		
	}
}
