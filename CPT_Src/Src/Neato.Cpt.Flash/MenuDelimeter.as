﻿import mx.controls.Label

class MenuDelimeter extends Label
{
    static var symbolName:String = "MenuDelimeter";
    static var symbolOwner = MenuDelimeter;

	function MenuDelimeter() {
		this.setStyle("textDecoration", "none");
		this._y += (this.__height - this.labelField.height) / 2;
	}
}