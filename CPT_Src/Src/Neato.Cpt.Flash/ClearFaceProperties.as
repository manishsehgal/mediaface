﻿import mx.core.UIObject;
import mx.controls.*;
import mx.utils.Delegate;
[Event("exit")]
class ClearFaceProperties extends UIObject {
	private var lblClearFace:Label;
	private var lblQuestion:Label;
	private var btnYes:MenuButton;
	private var btnNo:MenuButton;
    
	function ClearFaceProperties() {
		this.lblClearFace.setStyle("styleName", "ToolPropertiesCaption");
		this.lblQuestion.setStyle("styleName", "TransferPropertiesText");
		this.btnYes.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnNo.setStyle("styleName", "ToolPropertiesActiveButton");
	}
	
	function onLoad() {
		btnYes.RegisterOnClickHandler(this, CommandClearFace);
		btnNo.RegisterOnClickHandler(this, btnNo_OnClick);

		_global.LocalHelper.LocalizeInstance(lblClearFace, "ToolProperties", "IDS_LBLCLEARFACE");
		_global.LocalHelper.LocalizeInstance(lblQuestion, "ToolProperties", "IDS_LBLQUESTION");
		//_global.LocalHelper.LocalizeInstance(btnYes, "ToolProperties", "IDS_BTNYES");
		//_global.LocalHelper.LocalizeInstance(btnNo, "ToolProperties", "IDS_BTNNO");
		TooltipHelper.SetTooltip(btnYes, "ToolProperties", "IDS_TOOLTIP_YES");
		TooltipHelper.SetTooltip(btnNo, "ToolProperties", "IDS_TOOLTIP_NO");
	}
	
	function CommandClearFace(){
		_global.CurrentFace.Clear();
		OnExit();
	}
	function btnNo_OnClick() {
		OnExit();
	}
	
	private function OnExit() {
		var eventObject = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
	
}