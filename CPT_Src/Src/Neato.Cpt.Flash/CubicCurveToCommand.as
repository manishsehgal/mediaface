﻿import Geometry.*;
class CubicCurveToCommand implements IDraw {
	private var x0:Number;
	private var y0:Number;
	private var x1:Number;
	private var y1:Number;
	private var x2:Number;
	private var y2:Number;
	private var x3:Number;
	private var y3:Number;
	
	function CubicCurveToCommand(node:XML) {
		x0 = Number(node.attributes.x0);
		y0 = Number(node.attributes.y0);
		x1 = Number(node.attributes.x1);
		y1 = Number(node.attributes.y1);
		x2 = Number(node.attributes.x2);
		y2 = Number(node.attributes.y2);
		x3 = Number(node.attributes.x3);
		y3 = Number(node.attributes.y3);
	}
	
	function Draw(mc:MovieClip, originalX:Number, originalY:Number, rotationAngle:Number, scaleX:Number, scaleY:Number):Void {
		if(scaleX==undefined)scaleX=1.0;
		if(scaleY==undefined)scaleY=1.0;
		GeometryHelper.DrawCubicBezier(mc,x0+originalX,y0+originalY,
						 x1+originalX,y1+originalY,
						 x2+originalX,y2+originalY,
						 x3+originalX,y3+originalY);
	}	
}
