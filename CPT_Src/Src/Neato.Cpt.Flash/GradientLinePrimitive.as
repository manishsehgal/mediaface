﻿class GradientLinePrimitive implements IDraw {
	private var x1:Number;
	private var y1:Number;
	private var x2:Number;
	private var y2:Number;
	private var w:Number;
	private var colors:Array;
	private var a:Number;
	private var cosa:Number;
	private var sina:Number;
	
	function GradientLinePrimitive(node:XML) {
		x1 = Number(node.attributes.x1);
		y1 = Number(node.attributes.y1);
		x2 = Number(node.attributes.x2);
		y2 = Number(node.attributes.y2);
		w = Number(node.attributes.w);
		a = Math.atan2(y2 - y1, x2 - x1);
		cosa = Math.cos(a);
		sina = Math.sin(a);
	}
	
	function Draw(mc:MovieClip, originalX:Number, originalY:Number, rotationAngle:Number, scaleX:Number, scaleY:Number):Void {
		if(scaleX==undefined)scaleX=1.0;
		if(scaleY==undefined)scaleY=1.0;
				
		var begin = new Object({x:originalX+x1, y:originalY+y1});
		var center = new Object({x:originalX+x1+(x2-x1)/2, y:originalY+y1+(y2-y1)/2});
		var end = new Object({x:originalX+x2, y:originalY+y2});
  		if (rotationAngle > 0 || rotationAngle < 0) {
  			begin = _global.RotateAboutCenter(begin.x, begin.y, originalX, originalY, rotationAngle);
			center = _global.RotateAboutCenter(center.x, center.y, originalX, originalY, rotationAngle);
			end = _global.RotateAboutCenter(end.x, end.y, originalX, originalY, rotationAngle);
  		}
		var absX1:Number = begin.x*scaleX;
		var absY1:Number = begin.y*scaleY;
		var absW:Number = w*(scaleX<=scaleY?scaleX:scaleY);
		colors = [mc.color, mc.color];
		var deltaX:Number = absW / 2 * sina;
		var deltaY:Number = absW / 2 * cosa;
  		mc.lineStyle(undefined);
		
		var alphas:Array;
		var ratios:Array;

		colors = [mc.color, mc.color, mc.color];
		alphas = [0, 50, 0];
		ratios = [0x0, 0x7f, 0xff];
		var matrix:Object = {matrixType:"box", x:center.x - absW/2, y:center.y - absW/2, w:absW, h:absW, r:Math.PI/2 + a};
		mc.beginGradientFill("linear", colors, alphas, ratios, matrix);
		
		mc.moveTo(absX1 - deltaX, absY1 + deltaY);
		mc.lineTo(absX1 + deltaX, absY1 - deltaY);
				
		var absX2 = end.x*scaleX;
		var absY2 = end.y*scaleY;
		
		mc.lineTo(absX2 + deltaX, absY2 - deltaY);
		mc.lineTo(absX2 - deltaX, absY2 + deltaY);
		mc.lineTo(absX1 - deltaX, absY1 + deltaY);
		mc.endFill();
		
		colors = [mc.color, mc.color];
		alphas = [50, 0];
		ratios = [0x0, 0xff];

		matrix = {matrixType:"box", x:absX1 - absW/2, y:absY1-absW/2, w:absW, h:absW, r:0};
		mc.beginGradientFill("radial", colors, alphas, ratios, matrix);
		mc.moveTo(absX1 - deltaX, absY1 + deltaY);
		mc.lineTo(absX1 + deltaX, absY1 - deltaY);
		mc.curveTo(absX1 - deltaY + deltaX, absY1 - deltaY - deltaX, absX1 - deltaY, absY1 - deltaX);
		mc.curveTo(absX1 - deltaY - deltaX, absY1 + deltaY - deltaX, absX1 - deltaX, absY1 + deltaY);
		mc.endFill();
		
		matrix = {matrixType:"box", x:absX2 - absW/2, y:absY2-absW/2, w:absW, h:absW, r:0};
		mc.beginGradientFill("radial", colors, alphas, ratios, matrix);
		mc.moveTo(absX2 - deltaX, absY2 + deltaY);
		mc.lineTo(absX2 + deltaX, absY2 - deltaY);
		mc.curveTo(absX2 + deltaY + deltaX, absY2 - deltaY + deltaX, absX2 + deltaY, absY2 + deltaX);
		mc.curveTo(absX2 + deltaY - deltaX, absY2 + deltaY + deltaX, absX2 - deltaX, absY2 + deltaY);
		mc.endFill();
		
	}
}