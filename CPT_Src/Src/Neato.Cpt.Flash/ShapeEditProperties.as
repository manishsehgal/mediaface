﻿import mx.utils.Delegate;
import mx.core.UIObject;
import mx.controls.*;
[Event("exit")]
class ShapeEditProperties extends UIObject {
	private var chkKeepProportions:CheckBox;
	private var chkNoFill:CheckBox;
	private var btnDelete:MenuButton;
	private var btnRestoreProportions:MenuButton;
	private var lblShapeEdit:Label;
	private var lblFillColor:Label;
	private var lblLineColor:Label;
	private var lblLineWeight:Label;
	private var nsLineWeight:NumericStepper;
	private var btnContinue:MenuButton;
	
	private var fillColorTool:Tools.ColorSelectionTool.ColorTool;
	private var lineColorTool:Tools.ColorSelectionTool.ColorTool;
	private var fillColor:Number;
	private var lineColor:Number;
	
	function ShapeEditProperties() {
		this.btnDelete.setStyle("styleName", "ToolPropertiesActiveButton");
		this.lblShapeEdit.setStyle("styleName", "ToolPropertiesCaption");
		this.btnContinue.setStyle("styleName", "ToolPropertiesActiveButton");
		this.btnRestoreProportions.setStyle("styleName", "ToolPropertiesActiveButtonSmall");
	}
	
	function onLoad() {
		btnDelete.RegisterOnClickHandler(this, _global.DeleteCurrentUnit);
		chkKeepProportions.addEventListener("click", Delegate.create(this, chkKeepProportions_OnClick));
		chkNoFill.addEventListener("click", Delegate.create(this, chkNoFill_OnClick));
		btnRestoreProportions.addEventListener("click", Delegate.create(this, btnRestoreProportions_OnClick));
		nsLineWeight.addEventListener("change", Delegate.create(this, nsLineWeight_OnChange));
		nsLineWeight.addEventListener("focusOut", Delegate.create(this, nsLineWeight_OnFocusOut));
		fillColorTool.RegisterOnSelectHandler(this, fillColorTool_OnSelect);
		lineColorTool.RegisterOnSelectHandler(this, lineColorTool_OnSelect);
		btnContinue.addEventListener("click", Delegate.create(this, btnContinue_OnClick));
		
//		nsLineWeight.inputField.addEventListener("change", Delegate.create(this, nsLineWeight_OnChange1));
		
		DataBind();
		InitLocale();
	}
	
	
 	private function chkNoFill_OnClick(eventObject)	{
 		_global.CurrentUnit.SetFill(fillColor, chkNoFill.selected);
   	}

	private function chkKeepProportions_OnClick(eventObject)	{
		_global.CurrentUnit.KeepProportions = chkKeepProportions.selected;
		RefreshLineWeight();
	}
	
	private function btnRestoreProportions_OnClick(eventObject) {
		//	_global.CurrentUnit.ScaleX = _global.CurrentUnit.InitialScale;
		//_global.CurrentUnit.ScaleY = _global.CurrentUnit.InitialScale;
		_global.CurrentUnit.RestoreProportions();
		RefreshLineWeight();
		_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
	}
	
	private function nsLineWeight_OnChange(eventObject)	{
		trace("nsLineWeight_OnChange");
		trace(eventObject.target.value);
		_global.CurrentUnit.SetBorderWidth(eventObject.target.value)
		RefreshLineWeight();
		_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
	}
	/*
	private function nsLineWeight_OnChange1(eventObject)	{
		trace("nsLineWeight_OnChange1");
		trace(eventObject.target.value);
		_global.CurrentUnit.SetBorderWidth(eventObject.target.value)
		RefreshLineWeight();
		_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
	}
	*/
	private function nsLineWeight_OnFocusOut(eventObject)	{
		trace ("nsLineWeight_OnFocusOut");
		trace(eventObject.target.value);
		/*trace("+++++++++++++++++++++++++++++++++++++++++++");
		for(var i in eventObject.target.inputField.label)
			trace(i+"->"+eventObject.target.inputField.label[i]);
		trace("________________________________________");
		*/
		//trace("text ="+eventObject.target.inputField.label.text);
		//trace("Value ="+eventObject.target.inputField.label.value);
		
		var newVal = parseFloat(eventObject.target.inputField.label.value);
		var maxBorderWidth = _global.CurrentUnit.GetMaxBorderWidth();

		if(maxBorderWidth > newVal)
			_global.CurrentUnit.SetBorderWidth(newVal);
		else
			_global.CurrentUnit.SetBorderWidth(maxBorderWidth);
		RefreshLineWeight();
		_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
	}

	public function DataBind():Void {
		chkKeepProportions.selected = _global.CurrentUnit.KeepProportions;
		chkKeepProportions.enabled = _global.CurrentUnit.IsNonScalable ? false : true;
		chkNoFill.selected = _global.CurrentUnit.Transparency;
		fillColor=_global.CurrentUnit.GetFillColor();
		lineColor=_global.CurrentUnit.GetBorderColor();
		RefreshLineWeight();
		fillColorTool.SetColorState(fillColor);
		lineColorTool.SetColorState(lineColor);
		nsLineWeight.inputField.maxChars = 5;
	}

	private function InitLocale() {
		//_global.LocalHelper.LocalizeInstance(btnDelete, "ToolProperties", "IDS_BTNDELETE");
		_global.LocalHelper.LocalizeInstance(chkKeepProportions, "ToolProperties", "IDS_CHKKEEPPROPORTIONS");
		//_global.LocalHelper.LocalizeInstance(btnRestoreProportions, "ToolProperties", "IDS_BTNRESTOREPROPORTIONS");
		_global.LocalHelper.LocalizeInstance(lblShapeEdit, "ToolProperties", "IDS_LBLSHAPEEDIT");
		_global.LocalHelper.LocalizeInstance(lblFillColor, "ToolProperties", "IDS_LBLSHAPEFILLCOLOR");
		_global.LocalHelper.LocalizeInstance(lblLineColor, "ToolProperties", "IDS_LBLLINECOLOR");
		_global.LocalHelper.LocalizeInstance(lblLineWeight, "ToolProperties", "IDS_LBLLINEWEIGHT");
		_global.LocalHelper.LocalizeInstance(chkNoFill, "ToolProperties", "IDS_CHKNOFILL");
		//_global.LocalHelper.LocalizeInstance(btnContinue, "ToolProperties", "IDS_BTNCONTINUE");

		TooltipHelper.SetTooltip(btnDelete, "ToolProperties", "IDS_TOOLTIP_DELETE");
		TooltipHelper.SetTooltip(btnContinue, "ToolProperties", "IDS_TOOLTIP_CONTINUE");
		TooltipHelper.SetTooltip(chkKeepProportions, "ToolProperties", "IDS_TOOLTIP_KEEPPROPORTIONS");
		TooltipHelper.SetTooltip(btnRestoreProportions, "ToolProperties", "IDS_TOOLTIP_RESTOREPROPORTIONS");
		TooltipHelper.SetTooltip(chkNoFill, "ToolProperties", "IDS_TOOLTIP_NOFILL");
    }
	
	private function fillColorTool_OnSelect(eventObject) {
		chkNoFill.selected = false;
		fillColor = eventObject.color;
		_global.CurrentUnit.SetFill(fillColor, chkNoFill.selected);

	}
	
	private function lineColorTool_OnSelect(eventObject) {
		lineColor = eventObject.color;
		_global.CurrentUnit.SetBorderColor(lineColor);
	}
	function RefreshLineWeight(){
		trace("RefreshLineWeight");
		nsLineWeight.maximum=_global.CurrentUnit.GetMaxBorderWidth();
		nsLineWeight.value=_global.CurrentUnit.GetBorderWidth();
		nsLineWeight.stepSize=0.5;
		trace("nsLineWeight.maximum="+nsLineWeight.maximum);
		trace("nsLineWeight.value="+nsLineWeight.value);
	}
	
	private function btnContinue_OnClick() {
		OnExit();
	}
	
	
	//region Exit event
	private function OnExit() {
		var eventObject = {type:"exit", target:this};
		this.dispatchEvent(eventObject);
	};

	public function RegisterOnExitHandler(scopeObject:Object, callBackFunction:Function) {
		this.addEventListener("exit", Delegate.create(scopeObject, callBackFunction));
    }
	//endregion
	
}
