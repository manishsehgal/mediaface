﻿import mx.controls.Label;
import mx.core.UIObject;

class QuickToolContent extends UIObject {
	private var lblMove:Label;
	private var lblTransform:Label;
	private var lblArrange:Label;

	private var lblLeft:Label;
	private var lblRight:Label;
	private var lblUp:Label;
	private var lblDown:Label;
	private var lblResize:Label;
	private var lblRotate:Label;
	private var lblFront:Label;
	private var lblForward:Label;
	private var lblBack:Label;
	private var lblBackward:Label;
	
	private var btnMoveUp:ToolbarButton;
	private var btnMoveLeft:ToolbarButton;
	private var btnMoveRight:ToolbarButton;
	private var btnMoveDown:ToolbarButton;

	private var btnRotateToLeft:ToolbarButton;
	private var btnRotateToRight:ToolbarButton;
	private var btnRotateToLeft90:ToolbarButton;
	private var btnRotateToRight90:ToolbarButton;

	private var btnResizeMore:ToolbarButton;
	private var btnResizeLess:ToolbarButton;
	
	private var btnToBack:ToolbarButton;
	private var btnToBackAll:ToolbarButton;
	private var btnToFront:ToolbarButton;
	private var btnToFrontAll:ToolbarButton;

	private var speed:Number = 50;
	private var intUp:Number;
	private var intDown:Number;
	private var intLeft:Number;
	private var intRight:Number;
	private var intRotateLeft:Number;
	private var intRotateRight:Number;
	private var intResizeMore:Number;
	private var intResizeLess:Number;

	private static var instance;
	public static function GetInstance():QuickToolContent {
		return instance;
	}

	function QuickToolContent() {
		trace("QuickToolContent constructor");
		Preloader.Show();
		_parent._parent.setStyle("styleName", "QuickToolPanel");
		lblMove.setStyle("styleName","TitleText");
		lblTransform.setStyle("styleName","TitleText");
		lblArrange.setStyle("styleName","TitleText");
		trace("Quick tool content after set style");
		this.btnMoveUp._focusrect = false;
		this.btnMoveLeft._focusrect = false;
		this.btnMoveDown._focusrect = false;
		this.btnMoveRight._focusrect = false;
		this.btnRotateToLeft._focusrect = false;
		this.btnRotateToLeft90._focusrect = false;
		trace("quick tool:between focusrect");
		this.btnRotateToRight._focusrect = false;
		this.btnRotateToRight90._focusrect = false;
		this.btnResizeMore._focusrect = false;
		this.btnResizeLess._focusrect = false;
		this.btnToBack._focusrect = false;		
		trace(btnToBack+" btnToBack");
		instance = this;
	}

	function onLoad() {
		InitLocale();

		trace("Quick tool:onload");

		btnToBack.RegisterOnClickHandler(this, ToBackUnit);
		btnToBackAll.RegisterOnClickHandler(this, ToBackAllUnit);
		btnToFront.RegisterOnClickHandler(this, ToFrontUnit);
		btnToFrontAll.RegisterOnClickHandler(this, ToFrontAllUnit);

		btnMoveUp.RegisterOnPressHandler(this, StartMoveUp);
		btnMoveUp.RegisterOnReleaseHandler(this, StopMoveUp);
		btnMoveUp.RegisterOnDragOutHandler(this, StopMoveUp);

		btnMoveDown.RegisterOnPressHandler(this, StartMoveDown);
		btnMoveDown.RegisterOnReleaseHandler(this, StopMoveDown);
		btnMoveDown.RegisterOnDragOutHandler(this, StopMoveDown);

		btnMoveLeft.RegisterOnPressHandler(this, StartMoveLeft);
		btnMoveLeft.RegisterOnReleaseHandler(this, StopMoveLeft);
		btnMoveLeft.RegisterOnDragOutHandler(this, StopMoveLeft);

		btnMoveRight.RegisterOnPressHandler(this, StartMoveRight);
		btnMoveRight.RegisterOnReleaseHandler(this, StopMoveRight);
		btnMoveRight.RegisterOnDragOutHandler(this, StopMoveRight);

		btnRotateToLeft.RegisterOnPressHandler(this, StartRotateLeft);
		btnRotateToLeft.RegisterOnReleaseHandler(this, StopRotateLeft);
		btnRotateToLeft.RegisterOnDragOutHandler(this, StopRotateLeft);

		btnRotateToLeft90.RegisterOnClickHandler(this, btnRotateToLeft90_OnClick);

		btnRotateToRight.RegisterOnPressHandler(this, StartRotateRight);
		btnRotateToRight.RegisterOnReleaseHandler(this, StopRotateRight);
		btnRotateToRight.RegisterOnDragOutHandler(this, StopRotateRight);

		btnRotateToRight90.RegisterOnClickHandler(this, btnRotateToRight90_OnClick);

		btnResizeMore.RegisterOnPressHandler(this, StartResizeMore);
		btnResizeMore.RegisterOnReleaseHandler(this, StopResizeMore);
		btnResizeMore.RegisterOnDragOutHandler(this, StopResizeMore);

		btnResizeLess.RegisterOnPressHandler(this, StartResizeLess);
		btnResizeLess.RegisterOnReleaseHandler(this, StopResizeLess);
		btnResizeLess.RegisterOnDragOutHandler(this, StopResizeLess);

		DisableButtons();
		Preloader.Hide();
	}

	private function InitLocale() {
		_global.LocalHelper.LocalizeInstance(this.lblMove, "QuickTool", "IDS_LBLMOVE");
		_global.LocalHelper.LocalizeInstance(this.lblTransform, "QuickTool", "IDS_LBLTRANSFORM");
		_global.LocalHelper.LocalizeInstance(this.lblArrange, "QuickTool", "IDS_LBLARRANGE");

		_global.LocalHelper.LocalizeInstance(this.lblLeft, "QuickTool", "IDS_LBLLEFT");
		_global.LocalHelper.LocalizeInstance(this.lblRight, "QuickTool", "IDS_LBLRIGHT");
		_global.LocalHelper.LocalizeInstance(this.lblUp, "QuickTool", "IDS_LBLUP");
		_global.LocalHelper.LocalizeInstance(this.lblDown, "QuickTool", "IDS_LBLDOWN");
		_global.LocalHelper.LocalizeInstance(this.lblResize, "QuickTool", "IDS_LBLRESIZE");
		_global.LocalHelper.LocalizeInstance(this.lblRotate, "QuickTool", "IDS_LBLROTATE");
		_global.LocalHelper.LocalizeInstance(this.lblFront, "QuickTool", "IDS_LBLFRONT");
		_global.LocalHelper.LocalizeInstance(this.lblForward, "QuickTool", "IDS_LBLFORWARD");
		_global.LocalHelper.LocalizeInstance(this.lblBack, "QuickTool", "IDS_LBLBACK");
		_global.LocalHelper.LocalizeInstance(this.lblBackward, "QuickTool", "IDS_LBLBACKWARD");

		TooltipHelper.SetTooltip(this.btnMoveUp, "QuickTool", "IDS_TOOLTIP_MOVEUP");
		TooltipHelper.SetTooltip(this.btnMoveLeft, "QuickTool", "IDS_TOOLTIP_MOVELEFT");
		TooltipHelper.SetTooltip(this.btnMoveDown, "QuickTool", "IDS_TOOLTIP_MOVEDOWN");
		TooltipHelper.SetTooltip(this.btnMoveRight, "QuickTool", "IDS_TOOLTIP_MOVERIGHT");
		TooltipHelper.SetTooltip(this.btnResizeLess, "QuickTool", "IDS_TOOLTIP_RESIZELESS");
		TooltipHelper.SetTooltip(this.btnResizeMore, "QuickTool", "IDS_TOOLTIP_RESIZEMORE");
		TooltipHelper.SetTooltip(this.btnRotateToLeft, "QuickTool", "IDS_TOOLTIP_ROTATELEFT");
		TooltipHelper.SetTooltip(this.btnRotateToRight, "QuickTool", "IDS_TOOLTIP_ROTATERIGHT");
		TooltipHelper.SetTooltip(this.btnRotateToLeft90, "QuickTool", "IDS_TOOLTIP_ROTATELEFT90");
		TooltipHelper.SetTooltip(this.btnRotateToRight90, "QuickTool", "IDS_TOOLTIP_ROTATERIGHT90");
		TooltipHelper.SetTooltip(this.btnToBack, "QuickTool", "IDS_TOOLTIP_TOBACK");
		TooltipHelper.SetTooltip(this.btnToBackAll, "QuickTool", "IDS_TOOLTIP_TOBACKALL");
		TooltipHelper.SetTooltip(this.btnToFront, "QuickTool", "IDS_TOOLTIP_TOFRONT");
		TooltipHelper.SetTooltip(this.btnToFrontAll, "QuickTool", "IDS_TOOLTIP_TOFRONTALL");
	}

	private function Move(Direction:String) {
		if (Direction == "right") {
			_global.SelectionFrame.X += 1;
		}
		if (Direction == "left") {
			_global.SelectionFrame.X -= 1;
		}
		if (Direction == "up") {
			_global.SelectionFrame.Y -= 1;
		}
		if (Direction == "down") {
			_global.SelectionFrame.Y += 1;
		}
		//_global.CurrentUnit.Draw();
		updateAfterEvent();
	}

	private function Rotate(Direction:String) {
		if (Direction == "right") {
			_global.SelectionFrame.Angle += 1;
		}
		if (Direction == "left") {
			_global.SelectionFrame.Angle -= 1;
		}
		updateAfterEvent();
	}

	private function Resize(Zoom:String) {
		var isPositive:Boolean;
		if (Zoom == "Up") {
			isPositive = true;
		} else if (Zoom == "Down") {
			isPositive = false;
		}
		_global.CurrentUnit.ResizeByStep(isPositive);
		_global.SelectionFrame.AttachUnit(_global.CurrentUnit);
		updateAfterEvent();
	}


	private function ToBackUnit(eventObject) {
		SATracking.QuickTools();
		trace("ToBackUnit");
		_global.CurrentFace.ToBackCurrentUnit();
	}
	
	private function ToBackAllUnit(eventObject) {
		SATracking.QuickTools();
		trace("ToBackAllUnit");
		_global.CurrentFace.ToBackAllCurrentUnit();
	}
	
	private function ToFrontUnit(eventObject) {
		SATracking.QuickTools();
		trace("ToFrontUnit");
		_global.CurrentFace.ToFrontCurrentUnit();
	}
	
	private function ToFrontAllUnit(eventObject) {
		SATracking.QuickTools();
		trace("ToFrontAllUnit");
		_global.CurrentFace.ToFrontAllCurrentUnit();
	}
	
	private function StartMoveUp(eventObject) {
		SATracking.QuickTools();
		trace("StartMoveUp");
		intUp = setInterval(Move, speed, "up");
	}

	private function StopMoveUp(eventObject) {
		trace("StopMoveUp");
		clearInterval(intUp);
		Selection.setFocus(this);
	}

	private function StartMoveDown(eventObject) {
		SATracking.QuickTools();
		trace("StartMoveDown");
		intDown = setInterval(Move, speed, "down");
	}

	private function StopMoveDown(eventObject) {
		trace("StopMoveDown");
		clearInterval(intDown);
		Selection.setFocus(this);
	}

	private function StartMoveLeft(eventObject) {
		SATracking.QuickTools();
		trace("StartMoveLeft");
		intLeft = setInterval(Move, speed, "left");
	}

	private function StopMoveLeft(eventObject) {
		trace("StopMoveLeft");
		clearInterval(intLeft);
		Selection.setFocus(this);
	}

	private function StartMoveRight(eventObject) {
		SATracking.QuickTools();
		trace("StartMoveRight");
		intRight = setInterval(Move, speed, "right");
	}

	private function StopMoveRight(eventObject) {
		trace("StopMoveRight");
		clearInterval(intRight);
		Selection.setFocus(this);
	}

	private function StartRotateLeft(eventObject) {
		SATracking.QuickTools();
		trace("StartRotateLeft");
		intRotateLeft = setInterval(Rotate, speed, "left");
	}

	private function StopRotateLeft(eventObject) {
		trace("StopRotateLeft");
		clearInterval(intRotateLeft);
		Selection.setFocus(this);
	}

	private function StartRotateRight(eventObject) {
		SATracking.QuickTools();
		trace("StartRotateRight");
		intRotateRight = setInterval(Rotate, speed, "right");
	}

	private function StopRotateRight(eventObject) {
		trace("StopRotateRight");
		clearInterval(intRotateRight);
		Selection.setFocus(this);
	}

	private function btnRotateToLeft90_OnClick(eventObject) {
		SATracking.QuickTools();
		_global.RotateToRightAngle(true);
	}
	
	private function btnRotateToRight90_OnClick(eventObject) {
		SATracking.QuickTools();
		_global.RotateToRightAngle(false);
	}

	private function StartResizeMore(eventObject) {
		SATracking.QuickTools();
		trace("StartResizeMore");
		intResizeMore = setInterval(Resize, speed, "Up");
	}

	private function StopResizeMore(eventObject) {
		trace("StopResizeMore");
		trace("eventObject.type="+eventObject.type);
		clearInterval(intResizeMore);
		Selection.setFocus(this);
	}

	private function StartResizeLess(eventObject) {
		SATracking.QuickTools();
		trace("StartResizeLess");
		intResizeLess = setInterval(Resize, speed, "Down");
	}

	private function StopResizeLess(eventObject) {
		trace("StopResizeLess");
		trace("eventObject.type="+eventObject.type);
		clearInterval(intResizeLess);
		Selection.setFocus(this);
	}
	
	private function ChangeButtonsActivity(isActive:Boolean) {
		btnMoveUp.enabled = isActive;
		btnMoveLeft.enabled = isActive;
		btnMoveRight.enabled = isActive;
		btnMoveDown.enabled = isActive;
		
		btnRotateToLeft.enabled = isActive;
		btnRotateToRight.enabled = isActive;
		btnRotateToLeft90.enabled = isActive;
		btnRotateToRight90.enabled = isActive;
		
		btnResizeMore.enabled = isActive;
		btnResizeLess.enabled = isActive;
		
		btnToBack.enabled = isActive;
		btnToBackAll.enabled = isActive;
		btnToFront.enabled = isActive;
		btnToFrontAll.enabled = isActive;
	}
	public function DisableButtons() {
		ChangeButtonsActivity(false);
	}

	public function EnableButtons() {
		ChangeButtonsActivity(true);
	}
}