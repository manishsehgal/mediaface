﻿class PaintUnit extends MoveableUnit {
	private var points:Array;
	public function Empty() {
		return (points.length == 0);
	}
	
	private var currentBrush:Object;
	private var lastBrush:Object;
	
	public function set Brush(value) {
		currentBrush = value;
	}
	
	private var lastColorValue:Number = -1;
	private var currentColorValue:Number = 0;
	public function set PaintColor(value:Number) {
		currentColorValue = value;
	}
	public function get PaintColor():Number {
		return currentColorValue;
	}
	
	private var segmentStart:Object = new Object({x:0,y:0});
	private var distance:Number = 0;
	private var currentAngle:Number = 0;
	
	function PaintUnit(mc:MovieClip, node:XML) {
		super(mc, node);
		
		points = new Array();
		
		if (node != null) {
			for (var i = 0; i < node.childNodes.length; ++i) {
				var childNode:XML = node.childNodes[i];
				switch(childNode.nodeName) {
					case "Brush":
						//transfer brush xml node into brush object
						var brush = PaintHelper.CreateBrushFromXml(childNode);
						//var brush = _global.Brushes.GetBrush(childNode.attributes.type);
						points.push(brush);
						break;
					case "Color":
						var color = new Object({color:0, type:"color"});
						color.color = parseInt(childNode.attributes.color.substr(1), 16);
						points.push(color);
						break;
					case "Point":
						var point = new Object({x:0, y:0, rotation:0, type:"point"});
						point.x = Number(childNode.attributes.x);
						point.y = Number(childNode.attributes.y);
						point.rotation = Number(childNode.attributes.rotation);
						if (!point.rotation) point.rotation = 0;
						points.push(point);
						break;
					default:
						trace("Unexpected tag: " + childNode.nodeName);
				}
			}
		}
	}
	
	function GetXmlNode():XMLNode {
		var node:XMLNode = super.GetXmlNode();
		node.nodeName = "Paint";
		
		for (var i = 0; i < points.length; ++i) {
			if (points[i].type =="point") {
				var pointNode:XMLNode = new XMLNode(1, "Point");
				pointNode.attributes.x = Math.round(points[i].x*1000)/1000;
				pointNode.attributes.y = Math.round(points[i].y*1000)/1000;
				var rotation:Number = points[i].rotation;
				if (rotation>0 || rotation<0)
					pointNode.attributes.rotation = rotation;
				node.appendChild(pointNode);
			} 
			else if (points[i].type == "brush") {
				var brushNode:XMLNode = new XML(points[i].originalXmlNode.toString());
				brushNode = brushNode.firstChild;
				brushNode.nodeName = "Brush"; //in PaintTypes it is "type"
				node.appendChild(brushNode);
			} 
			else if (points[i].type == "color") {
				var colorNode:XMLNode = new XMLNode(1, "Color");
				colorNode.attributes.color = _global.GetColorForXML(points[i].color);
				node.appendChild(colorNode);
			}
			else {
				trace("Unexpected node type: " + points[i].type);
			}
		}
		
		node.appendChild(super.GetXmlNodeRotate());
		trace("");
		trace("paint xml node: ");
		trace(node.toString());
		trace("");
		return node;
	}
	
	function Draw():Void {
		DrawMC(mc);
	}
	
	function DrawMC(mc:MovieClip):Void {
		mc.clear();
        var drawingBrush;
		
		for (var i = 0; i < points.length; ++i) {
			var element = points[i];
			switch (element.type) {
				case "brush":
                    drawingBrush = element;
					break;
				case "point":
					Logic.DrawingHelper.DrawSequence(drawingBrush.drawSequence, mc, element.x, element.y, element.rotation, currentColorValue, 1, drawingBrush.filled);
					break;
				case "color":
					currentColorValue = element.color;
					break;
			}
		}
		
		DrawHitArea();
		if (_global.CurrentUnit == this && _global.Mode == _global.PaintEditMode)
			_global.SelectionFrame.AttachUnit(this);
	}
	
	//segmentStart and distance are modified inside
	private function DrawSegmentMC(mc:MovieClip, segmentEnd:Object) {
		var segDeltaX:Number = segmentEnd.x - segmentStart.x;
		var segDeltaY:Number = segmentEnd.y - segmentStart.y;
		var segmentLength = Math.sqrt(segDeltaX*segDeltaX + segDeltaY*segDeltaY);
		//increment distance until it's bigger than segment length
		var count = 0;
		while (distance<segmentLength) {
			//trace(" segment: " + segmentLength + " distance: " + distance);
			var distanceRatio:Number = distance/segmentLength;
			
			//calculate brush position and angle
			var distanceDeltaX:Number = segDeltaX * distanceRatio;
			var distanceDeltaY:Number = segDeltaY * distanceRatio;
			var angle:Number = getAngle(); //=currentAngle;
			
			//memorize brush and draw it
			var point = new Object({x:0, y:0, rotation:0, type:"point"});
			point.x = segmentStart.x + distanceDeltaX;
			point.y = segmentStart.y + distanceDeltaY;
			point.rotation = angle;
			points.push(point);
			Logic.DrawingHelper.DrawSequence(currentBrush.drawSequence, mc, point.x, point.y, point.rotation, currentColorValue, 1, currentBrush.filled);
			
			//increment distance with step
			distance += currentBrush.step;
			//currentAngle += rotate; if (currentAngle>Math.PI) currentAngle = currentAngle - Math.PI*2;
			++count;
		}
		if (count>0) {
			distance = distance-segmentLength;
			segmentStart.x = segmentEnd.x;
			segmentStart.y = segmentEnd.y;
		}
	}
	
	private function getAngle() {
		if (currentBrush.mode == "RandomRotate") {
			return Math.random()*Math.PI*2 - Math.PI;
		}
		return 0;
	}
	
	public function get IsDrawing():Boolean {
		if (mc.onMouseDown != null && mc.onMouseDown != undefined) return true;
		return false;
	}
	function getMode():String {
		if (IsDrawing) return _global.PaintMode;
		return _global.PaintEditMode;
	}
	
	//Event handlers
	
	function PaintUnit_onMouseDown() {
		var mc = MovieClip(this);
		mc.unitRef.PaintMouseDown();
	}
	
	function PaintUnit_onMouseMove() {
		var mc = MovieClip(this);
		//trace("PaintUnit_onMouseMove: mouse x " + mc._xmouse + ", y " + mc._ymouse);
		mc.unitRef.PaintMouseMove();
	}
	
	function PaintUnit_onMouseUp() {
		var mc = MovieClip(this);
		trace("PaintUnit_onMouseUp: mouse x " + mc._xmouse + ", y " + mc._ymouse);
		//mc.onMouseDown = null;
		mc.onMouseMove = null;
	}
	
	function PaintMouseDown() {
		//check if we are within draw area
		var bounds = _root.pnlMainLayer.content.WorkSpaceClickArea.getBounds(mc);
		if (mc._xmouse<bounds.xMin || mc._xmouse>bounds.xMax || 
			mc._ymouse<bounds.yMin || mc._ymouse>bounds.yMax) {
			trace("PaintUnit.StartPaint: click outside WorkSpace");
			return;
		}
		trace("PaintUnit.StartPaint: click inside WorkSpace");
		
		if (points.length == 0) { //first click
			SATracking.Paint();
			this.X = mc._xmouse;
			this.Y = mc._ymouse;
		}
		mc.onMouseMove = PaintUnit_onMouseMove;
		mc.onMouseUp = PaintUnit_onMouseUp;

		if (currentBrush != lastBrush) {
			points.push(currentBrush);
			lastBrush = currentBrush;
		}
		if (currentColorValue != lastColorValue) {
			var color = new Object({color:currentColorValue, type:"color"});
			points.push(color);
		}
		var point = new Object({x:mc._xmouse, y:mc._ymouse, rotation:getAngle(), type:"point"});
		points.push(point);
		Logic.DrawingHelper.DrawSequence(currentBrush.drawSequence, mc, point.x, point.y, point.rotation, currentColorValue, 1, currentBrush.filled);
		distance = currentBrush.step;
		segmentStart.x = mc._xmouse;
		segmentStart.y = mc._ymouse;
	}
	
	function PaintMouseMove() {
		var endNode = new Object({x:0,y:0});
		endNode.x = mc._xmouse;
		endNode.y = mc._ymouse;
		DrawSegmentMC(mc, endNode);
	}
	
	function ExitPaint() {
		mc.onMouseDown = null;
		mc.onMouseMove = null;
		mc.onMouseUp = null;
		if (points.length==0) return;
		//todo: shift coords and recalculate points array to have 0,0 in upper left
		//todo: then it's possible to remove rotation and shift parts from resize handler
		DrawHitArea();
	}

	private function Resize(kx:Number, ky:Number) {
		var oldBounds:Object = this.mc.getBounds(this.mc);
		var oldLeftUpper:Object = {x:oldBounds.xMin, y:oldBounds.yMin};
		this.mc.localToGlobal(oldLeftUpper);
		this.mc._parent.globalToLocal(oldLeftUpper);

		this.ScaleX = this.ScaleX * kx;
		this.ScaleY = this.ScaleY * ky;

		var newBounds:Object = this.mc.getBounds(this.mc);
		var newLeftUpper:Object = {x:newBounds.xMin, y:newBounds.yMin};
		this.mc.localToGlobal(newLeftUpper);
		this.mc._parent.globalToLocal(newLeftUpper);

		var originPoint:Object = {x:this.X, y:this.Y};

		var dx:Number = newLeftUpper.x - oldLeftUpper.x;
		var dy:Number = newLeftUpper.y - oldLeftUpper.y;

		originPoint.x = originPoint.x - dx;
		originPoint.y = originPoint.y - dy;
		
		this.X = originPoint.x;
		this.Y = originPoint.y;
	}
    
    private function SetLastBrushAfterUndo():Void {
        if (Empty()) lastBrush = undefined;

        for (var i = points.length - 1; i >= 0; i--){
            if (points[i].type == "brush"){
                lastBrush = points[i]; break;
                }
            }
    }
    
    private function GetDelPointsCountForUndo():Number {
        for (var i = points.length - 1; i >= 0; i--){
            if (points[i].type == "brush"){
                var MiddlePointSurface = 6;
                var count = Math.round(MiddlePointSurface/points[i].step);
                return (count == 0) ? 1 : count;
                }
            }
        return 1;
    }
    
    function Undo():Void {
        var delPointsCount:Number = GetDelPointsCountForUndo();
        
        //del count of points
        while (points.length > 0 && delPointsCount > 0) {
           if (points[points.length-1].type == "brush") break;
           if (points[points.length-1].type == "point") delPointsCount--;
           points.pop();
       }
        
        //del "non-points" nodes before deleted points
        while (points.length > 0 && points[points.length-1].type != "point") {
           points.pop();
       }
       
        SetLastBrushAfterUndo();       
        var currentColorValueCopy = currentColorValue;
        Draw();
        currentColorValue = currentColorValueCopy;
   }
}