﻿class DesignerMenu extends CommonMenu {
	private var btnPreview:MenuButton;
	private var btnNew:MenuButton;
	private var btnClear:MenuButton;
	private var btnUpload:MenuButton;
	private var btnSave:MenuButton;
	
	function DesignerMenu() {
		trace("DesignerMenu ");
		Preloader.Show();
		
		this.btnUpload.setStyle("styleName", "MenuPanelLargeButton");
		this.btnPreview.onRelease = GotoPreview;
				
		this.btnNew.onRelease = function() {
			trace("btnNew");
			this._parent.CommandNew();
		};
				
		this.btnClear.onRelease = function() {
			SATracking.Clear();
			trace("btnClear");
			this._parent.CommandClear();
		};
				
		this.btnUpload.onRelease = function() {
			trace("btnUpload");
			SATracking.UsePreviousDesign();
			this._parent.CommandUpload();
		};
	}
	
	static function GotoPreview() {
		trace("btnPreview");
		SATracking.Preview();
		_root.pnlPreviewLayer.content.Show();
		Header.getInstance().SetActiveStep(3);
	}
	
	
	function CommandNew() {
		trace("CommandNew");
		BrowserHelper.InvokePageScript("home", "");
	}
	
	function CommandClear() {
		trace("CommandClear");
		ToolPropertiesContainer.GetInstance().ShowClearFaceProperties();
	}
	
	function CommandUpload() {
		trace("CommandUpload");
		Logic.DesignerLogic.UnselectCurrentUnit();
		ToolPropertiesContainer.GetInstance().ShowFillProperties();
		BrowserHelper.InvokePageScript("load", "");
	}
	
	function onLoad() {
		_global.LocalHelper.LocalizeInstance(this.btnPreview, "DesignerMenu", "IDS_BTNPREVIEW");
		_global.LocalHelper.LocalizeInstance(this.btnPrint, "DesignerMenu", "IDS_BTNPRINT");
		_global.LocalHelper.LocalizeInstance(this.btnSave, "DesignerMenu", "IDS_BTNSAVE");
		_global.LocalHelper.LocalizeInstance(this.btnNew, "DesignerMenu", "IDS_BTNNEW");
		_global.LocalHelper.LocalizeInstance(this.btnClear, "DesignerMenu", "IDS_BTNCLEAR");
		_global.LocalHelper.LocalizeInstance(this.btnUpload, "DesignerMenu", "IDS_BTNUPLOAD");
		
		this.btnPreview.SetHTMLStyle();
		this.btnUpload.SetHTMLStyle();
		this.btnSave.SetHTMLStyle();
		this.btnClear.SetHTMLStyle();
		
		TooltipHelper.SetTooltip(this.btnPreview, "DesignerMenu", "IDS_TOOLTIP_PREVIEW");
		TooltipHelper.SetTooltip(this.btnPrint, "DesignerMenu", "IDS_TOOLTIP_PRINT");
		TooltipHelper.SetTooltip(this.btnSave, "DesignerMenu", "IDS_TOOLTIP_SAVE");
		TooltipHelper.SetTooltip(this.btnNew, "DesignerMenu", "IDS_TOOLTIP_NEW");
		TooltipHelper.SetTooltip(this.btnClear, "DesignerMenu", "IDS_TOOLTIP_CLEAR");
		TooltipHelper.SetTooltip(this.btnUpload, "DesignerMenu", "IDS_TOOLTIP_UPLOAD");
		
		Preloader.Hide();
	}
}