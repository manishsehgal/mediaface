﻿class FontHelper {
	private var names:Array;
	private var urls:Object;
	private var ttfnames:Array;
	private var smallFontUrl:String;
	public function FontHelper(fontsXml:XML) {
		trace("FontHelper");
		
		names = new Array();
		ttfnames = new Array();
		urls = new Object();
		var fontName:String;
		smallFontUrl = fontsXml.firstChild.attributes.smallFontUrl;
		for (var itemNode:XMLNode = fontsXml.firstChild.firstChild; itemNode != null; itemNode = itemNode.nextSibling) {
			fontName = itemNode.attributes.name;
			names.push(fontName);
			
			for (var styleNode:XMLNode = itemNode.firstChild; styleNode != null; styleNode = styleNode.nextSibling) {
				var italic:Boolean = false;
				var bold:Boolean = false;
				switch (styleNode.nodeName) {
					case "italic" :
						italic = true;
						break;
					case "bold" :
						bold = true;
						break;
					case "bold-italic" :
						italic = true;
						bold = true;
						break;
					case "normal" :
					default :
						italic = false;
						bold = false;
						break;
				}
				urls[CreateKey(fontName, italic, bold)] = styleNode.attributes.url;
				ttfnames[CreateKey(fontName,italic,bold)] = styleNode.attributes.ttf;
			}
		}
	}
	
	private function GetFontsMC():MovieClip {
		if (_root.fonts_mc == undefined) {
			_root.createEmptyMovieClip("fonts_mc", _root.getNextHighestDepth());
			_root.fonts_mc._y = 200;
			_root.fonts_mc._visible = false;
		}
		
		return _root.fonts_mc;
	}
	
	private function CreateKey(fontId:String, italic:Boolean, bold:Boolean):String {
		return fontId.toLowerCase()+"_"+italic.toString()+"_"+bold.toString();
	}
	public function GetFontsArray():Array {
		return names;
	}
	public function GetTTFFileName(key:String):String {
		var res:String = ttfnames[key];
		var inx:Number = res.lastIndexOf("/");
		if(inx==-1)
			return res;
		else
			return res.substring(inx+1);
			
	}
	public function TestFont(fontId:String, italic:Boolean, bold:Boolean):Boolean {
		var key:String = CreateKey(fontId, italic, bold);
		return urls[key] != undefined;
	}
	public function TestLoadedFont(fontFormat:TextFormat):Boolean {
		var key:String = CreateKey(fontFormat.font, fontFormat.italic, fontFormat.bold);
		return GetFontsMC()[key]._width > 0;
	}
	public function TestLoadedSmallFont(){
		return (GetFontsMC()["smallFont"]._width > 100 && GetFontsMC()["smallFont"]._height > 10);
	}
	public function LoadSmallFont()
	{
		var key:String = "smallFont";
		urls[key]=smallFontUrl;
		if (urls[key] == undefined)
			return;
		if (GetFontsMC()[key]._width > 0)
			return;
			
		var mcFont:MovieClip = GetFontsMC()[key];
		if (!mcFont) {
			Preloader.Show();
			var i:Number = GetFontsMC().getNextHighestDepth();
			mcFont = GetFontsMC().createEmptyMovieClip(key, i);
			var url:String = urls[key];
			mcFont.loadMovie(url);
		}
		var intervalObject = new Object();
		var interval:Number = setInterval(checkLoaded, 500, mcFont, key, intervalObject);
		intervalObject["interval"] = interval;
	}
	public function LoadFont(fontFormat:TextFormat, target):Void {
		var key:String = CreateKey(fontFormat.font, fontFormat.italic, fontFormat.bold);
		if (urls[key] == undefined)
			return;
		if (GetFontsMC()[key]._width > 0)
			return;
			
		var mcFont:MovieClip = GetFontsMC()[key];
		if (!mcFont) {
			Preloader.Show();
			var i:Number = GetFontsMC().getNextHighestDepth();
			mcFont = GetFontsMC().createEmptyMovieClip(key, i);
			var url:String = urls[key];
			mcFont.loadMovie(url);
		}
		var intervalObject = new Object();
		var interval:Number = setInterval(checkLoaded, 500, mcFont, key, intervalObject, target);
		intervalObject["interval"] = interval;
	}
	
	static private function checkLoaded(mcFont:MovieClip, key, intervalObject, target) {
		if (mcFont._width > 0) {
			Preloader.Hide();
			clearInterval(intervalObject.interval);
			target.Draw();
		}
	}
}
