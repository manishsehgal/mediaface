 /* Changes
- add cascade delete ImageLibFolderItem
- add trigger for delete from table ImageLibFolderItem
 (delete linked row from ImageLibItem)
 
- add RetailerBuyNow table
*/

BEGIN TRANSACTION
--add cascade delete

if exists (select * from dbo.sysobjects where id = object_id(N'FK_ImageLibFolderItem_ImageLibFolder') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[ImageLibFolderItem] DROP CONSTRAINT FK_ImageLibFolderItem_ImageLibFolder
ALTER TABLE [dbo].[ImageLibFolderItem]
    ADD CONSTRAINT [FK_ImageLibFolderItem_ImageLibFolder]
    FOREIGN KEY (
		    [FolderId]
	    ) REFERENCES [dbo].[ImageLibFolder] (
		    [FolderId]
	    ) ON DELETE CASCADE
	    
GO	    

--add trigger
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TrgImageLibFolderItem]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[TrgImageLibFolderItem]
GO

CREATE TRIGGER TrgImageLibFolderItem ON [dbo].[ImageLibFolderItem] 
FOR DELETE 
AS
BEGIN
CREATE TABLE #t(Id INT)

INSERT INTO #t
SELECT ItemId FROM deleted

DELETE FROM [dbo].[ImageLibItem]
WHERE [dbo].[ImageLibItem].ItemId IN (SELECT Id FROM #t)

DROP TABLE #t
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RetailerBuyNow]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[RetailerBuyNow] (
	[RetailerId] [int] NOT NULL ,
	[VisibleRetailerId] [int] NOT NULL 
) ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_RetailerBuyNow_Retailers]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[RetailerBuyNow] ADD 
	CONSTRAINT [FK_RetailerBuyNow_Retailers] FOREIGN KEY 
	(
		[RetailerId]
	) REFERENCES [dbo].[Retailers] (
		[Id]
	)
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_RetailerBuyNow_Retailers1]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[RetailerBuyNow] ADD 
    CONSTRAINT [FK_RetailerBuyNow_Retailers1] FOREIGN KEY 
	(
		[VisibleRetailerId]
	) REFERENCES [dbo].[Retailers] (
		[Id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrRetailersGetIdByName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrRetailersGetIdByName]
GO

-- add column Retailers.HasBrandedSite
EXEC sp_columns @table_name = 'Retailers', @table_owner='dbo', @column_name = 'HasBrandedSite'
IF @@ROWCOUNT = 0
BEGIN
    ALTER TABLE dbo.Retailers ADD HasBrandedSite BIT NOT NULL
    CONSTRAINT DF_Retailers_HasBrandedSite DEFAULT (0)
    
    ALTER TABLE dbo.Retailers DROP COLUMN Enabled
END
GO
COMMIT