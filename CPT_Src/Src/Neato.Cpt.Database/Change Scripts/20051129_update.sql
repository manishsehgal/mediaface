BEGIN TRANSACTION

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Customer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
ALTER TABLE [dbo].[Customer] ALTER COLUMN [FirstName] NVARCHAR(100) NOT NULL
ALTER TABLE [dbo].[Customer] ALTER COLUMN [LastName] NVARCHAR(100) NOT NULL
ALTER TABLE [dbo].[Customer] ALTER COLUMN [Email] VARCHAR(100) NOT NULL
ALTER TABLE [dbo].[Customer] ALTER COLUMN [Password] VARCHAR(100) NOT NULL
END

COMMIT TRANSACTION