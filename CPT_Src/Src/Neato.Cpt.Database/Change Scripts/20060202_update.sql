/*
    Changes:
        - modify table: LinkPlace
        - add table: Announcement
        - add table: LastEditedPaper
*/

DELETE FROM [dbo].[LinkPlace] WHERE ( [LinkId] =  28 OR [LinkId] = 29)
UPDATE [dbo].[LinkPlace] SET [LinkId] = 1 WHERE [PageId] = 13 AND [LinkId] = 36

IF Not Exists (SELECT * FROM [dbo].[LinkPlace] WHERE [PageId] = 13 AND [LinkId] = 12)
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (13, 12, 2, 'R', 'Footer', 'N')

GO

--Table Announcement
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Announcement]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Announcement] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Text] [text] COLLATE Latin1_General_BIN NULL ,
	[Time] [datetime] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].[Announcement] WITH NOCHECK ADD 
	CONSTRAINT [PK_Announcement] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 

SET IDENTITY_INSERT dbo.Announcement ON

INSERT INTO dbo.Announcement ([Id], [Text], [Time]) 
  VALUES (1, '<span style="color:red;">Announcement<span style="color:blue">1</span></span>', '2006-02-02 12:36:00.000')

INSERT INTO dbo.Announcement ([Id], [Text], [Time]) 
  VALUES (2, '<span style="color:red;">Announcement<span style="color:blue">2</span></span>', '2006-02-02 12:37:00.000')

INSERT INTO dbo.Announcement ([Id], [Text], [Time]) 
  VALUES (3, '<span style="color:red;">Announcement<span style="color:blue">3</span></span>', '2006-02-02 12:38:00.000')

INSERT INTO dbo.Announcement ([Id], [Text], [Time]) 
  VALUES (4, '<span style="color:red;">Announcement<span style="color:blue">4</span></span>', '2006-02-02 12:39:00.000')

SET IDENTITY_INSERT dbo.Announcement OFF 
END

GO

--Table LastEditedPaper
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LastEditedPaper]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[LastEditedPaper]
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LastEditedPaper]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[LastEditedPaper] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[CustomerId] [int] NOT NULL ,
	[PaperId] [int] NOT NULL 
) ON [PRIMARY]
END

GO

ALTER TABLE [dbo].[LastEditedPaper] WITH NOCHECK ADD 
	CONSTRAINT [PK_LastEditedPaper] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[LastEditedPaper] ADD 
	CONSTRAINT [FK_LastEditedPaper_Customer] FOREIGN KEY 
	(
		[CustomerId]
	) REFERENCES [dbo].[Customer] (
		[CustomerId]
	) ON DELETE CASCADE  ON UPDATE CASCADE ,
	CONSTRAINT [FK_LastEditedPaper_Paper] FOREIGN KEY 
	(
		[PaperId]
	) REFERENCES [dbo].[Paper] (
		[Id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO
