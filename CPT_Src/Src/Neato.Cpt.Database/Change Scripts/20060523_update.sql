/* Changes
- add retailers to dynamic links
*/

BEGIN TRANSACTION

EXEC sp_columns @table_name = 'Page', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Page ADD
	[RetailerId] [int] NULL
END
GO

UPDATE dbo.Page
SET
    RetailerId = 1
WHERE
    RetailerId IS NULL
    
ALTER TABLE dbo.Page
    ALTER COLUMN [RetailerId] [int] NOT NULL
GO
    
if not exists (select * from dbo.sysobjects where id = object_id(N'FK_Page_Retailers') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Page] ADD CONSTRAINT [FK_Page_Retailers] FOREIGN KEY ([RetailerId]) REFERENCES [dbo].[Retailers] ([Id])
GO

EXEC sp_columns @table_name = 'LinkPlace', @table_owner='dbo', @column_name = 'Icon'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.LinkPlace ADD
	[Icon] [image] NOT NULL CONSTRAINT DF_LinkPlace_Icon DEFAULT 0x00
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'DF_LinkPlace_Icon') and OBJECTPROPERTY(id, N'IsDefaultCnst') = 1)
ALTER TABLE [dbo].[LinkPlace] DROP CONSTRAINT [DF_LinkPlace_Icon]
GO

COMMIT



-- BEGIN PaperToRetailer
BEGIN TRANSACTION
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PaperToRetailer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE dbo.PaperToRetailer
	(
	PaperId int NOT NULL,
	RetailerId int NOT NULL
	)  ON [PRIMARY]
GO
if not exists (select * from dbo.sysobjects where id = object_id(N'FK_PaperToRetailer_Retailers') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.PaperToRetailer ADD CONSTRAINT
	FK_PaperToRetailer_Retailers FOREIGN KEY
	(
	RetailerId
	) REFERENCES dbo.Retailers
	(
	Id
	) ON UPDATE CASCADE
	 ON DELETE CASCADE
	
GO
if not exists (select * from dbo.sysobjects where id = object_id(N'FK_PaperToRetailer_Paper') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.PaperToRetailer ADD CONSTRAINT
	FK_PaperToRetailer_Paper FOREIGN KEY
	(
	PaperId
	) REFERENCES dbo.Paper
	(
	Id
	) ON UPDATE CASCADE
	 ON DELETE CASCADE
	
GO
COMMIT
-- END PaperToRetailer

if not exists(select * from [dbo].[PaperToRetailer])
insert into PaperToRetailer
            select Paper.Id, Retailers.Id from Paper, Retailers
            where Retailers.Id = 1

if not exists(select * from [dbo].[CarrierToRetailer])
begin
insert into CarrierToRetailer
            select Retailers.Id,Carrier.Id from Carrier, Retailers
            where Retailers.Id = 1
end
