if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PhoneRequest]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table PhoneRequest
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Tracking_Phones]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table Tracking_Phones
GO

if exists (select * from dbo.sysobjects where id = object_id(N'PK_PhoneRequest') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[DeviceRequest] DROP CONSTRAINT PK_PhoneRequest
GO

if exists (select * from dbo.sysobjects where id = object_id(N'PK_PhonesTracking') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[Tracking_Devices] DROP CONSTRAINT PK_PhonesTracking
GO

if exists (select * from dbo.sysobjects where id = object_id(N'DF_PhoneRequest_Created') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
ALTER TABLE [dbo].[DeviceRequest] DROP CONSTRAINT DF_PhoneRequest_Created
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DeviceRequest]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[DeviceRequest] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[FirstName] [nvarchar] (100) COLLATE Latin1_General_BIN NOT NULL ,
	[LastName] [nvarchar] (100) COLLATE Latin1_General_BIN NOT NULL ,
	[EmailAddress] [nvarchar] (100) COLLATE Latin1_General_BIN NOT NULL ,
	[DeviceBrand] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Device] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Comment] [nvarchar] (1000) COLLATE Latin1_General_BIN NOT NULL ,
	[Carrier] [nvarchar] (100) COLLATE Latin1_General_BIN NOT NULL ,
	[Created] [datetime] NOT NULL 
) ON [PRIMARY]
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'PK_DeviceRequest') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[DeviceRequest] DROP CONSTRAINT PK_DeviceRequest
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_DeviceRequest') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[DeviceRequest] WITH NOCHECK ADD 
	CONSTRAINT [PK_DeviceRequest] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'DF_DeviceRequest_Created') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
ALTER TABLE [dbo].[DeviceRequest] DROP CONSTRAINT DF_DeviceRequest_Created
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'DF_DeviceRequest_Created') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
ALTER TABLE [dbo].[DeviceRequest] ADD 
	CONSTRAINT [DF_DeviceRequest_Created] DEFAULT (getdate()) FOR [Created]
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Tracking_Devices]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[Tracking_Devices] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Device] [varchar] (200) COLLATE Latin1_General_BIN NOT NULL ,
	[DeviceBrand] [varchar] (200) COLLATE Latin1_General_BIN NOT NULL ,
	[User] [varchar] (200) COLLATE Latin1_General_BIN NOT NULL ,
	[Time] [datetime] NOT NULL 
) ON [PRIMARY]
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'PK_Tracking_Devices') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[Tracking_Devices] DROP CONSTRAINT PK_Tracking_Devices
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_Tracking_Devices') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[Tracking_Devices] WITH NOCHECK ADD 
	CONSTRAINT [PK_Tracking_Devices] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[IPFilter]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[IPFilter] (
	[Id] [int] NOT NULL ,
	[BeginField1] [tinyint] NOT NULL ,
	[BeginField2] [tinyint] NOT NULL ,
	[BeginField3] [tinyint] NOT NULL ,
	[BeginField4] [tinyint] NOT NULL ,
	[EndField1] [tinyint] NOT NULL ,
	[EndField2] [tinyint] NOT NULL ,
	[EndField3] [tinyint] NOT NULL ,
	[EndField4] [tinyint] NOT NULL ,
	[Description] [nvarchar] (100) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]
END
GO
