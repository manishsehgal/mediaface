/*
Added new table:
    - Tracking for user actions
*/


if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Tracking_UserAction]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Tracking_UserAction] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Action] [varchar] (200) COLLATE Latin1_General_BIN NOT NULL ,
	[Login] [varchar] (200) COLLATE Latin1_General_BIN NOT NULL ,
	[Time] [datetime] NOT NULL 
) ON [PRIMARY]

ALTER TABLE [dbo].[Tracking_UserAction] WITH NOCHECK ADD 
	CONSTRAINT [PK_UserActionTracking] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 

END
GO 