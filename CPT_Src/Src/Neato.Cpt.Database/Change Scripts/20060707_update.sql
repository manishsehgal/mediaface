BEGIN TRANSACTION
EXEC sp_columns @table_name = 'FaceToPaper', @table_owner='dbo', @column_name = 'Rotation'
IF @@ROWCOUNT = 0
ALTER TABLE dbo.FaceToPaper ADD
	Rotation real NOT NULL CONSTRAINT DF_FaceToPaper_Rotation DEFAULT 0
GO
COMMIT
BEGIN TRANSACTION
if exists (select * from dbo.sysobjects where id = object_id(N'DF_FaceToPaper_Rotation') and OBJECTPROPERTY(id, N'IsDefaultCnst') = 1)
ALTER TABLE dbo.FaceToPaper
	DROP CONSTRAINT DF_FaceToPaper_Rotation
GO
COMMIT