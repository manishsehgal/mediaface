/*
 - Phone: Motorola Razr - V3, V3c, V3i - contour and position updated   
 - Delete link "SiteMap", add link "HOME" in header
*/

-------------------------------
DECLARE @ptrContour binary(16)

BEGIN TRANSACTION

--- Phone: Motorola Razr - V3, V3c, V3i ---
SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 3
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
	<MoveTo x="142.75" y="218.45" />
	<LineTo x="142.9" y="10" />
	<LineTo x="138.85" y="8.35" />
	<CurveTo cx="133.8" cy="6.35" x="117.3" y="3.5" />
	<CurveTo cx="102.05" cy="0.9" x="95.1" y="0.45" />
	<LineTo x="87.05" y="0" />
	<LineTo x="88.35" y="8.05" />
	<CurveTo cx="90.25" cy="20.4" x="91.45" y="25.85" />
	<LineTo x="91.75" y="27.15" />
	<LineTo x="53.65" y="27.15" />
	<LineTo x="53.9" y="25.85" />
	<LineTo x="57.05" y="8.05" />
	<LineTo x="58.35" y="0" />
	<LineTo x="50.25" y="0.45" />
	<CurveTo cx="43.05" cy="0.9" x="27.25" y="3.45" />
	<CurveTo cx="9.6" cy="6.3" x="4.55" y="8.35" />
	<LineTo x="0.45" y="10" />
	<LineTo x="0.25" y="218.45" />
	<MoveTo x="0.25" y="218.45" />
	<LineTo x="0" y="255.5" />
	<LineTo x="4.25" y="257.05" />
	<CurveTo cx="15" cy="261.1" x="36.35" y="264.15" />
	<CurveTo cx="56.55" cy="267.05" x="71.3" y="267.05" />
	<CurveTo cx="86.1" cy="267.05" x="106.25" y="264.15" />
	<CurveTo cx="127.6" cy="261.1" x="138.4" y="257.05" />
	<LineTo x="142.6" y="255.5" />
	<LineTo x="142.75" y="218.45" />
	<MoveTo x="123.55" y="64.25" />
	<LineTo x="123.55" y="130.2" />
	<CurveTo cx="123.6" cy="133.1" x="123.15" y="133.9" />
	<CurveTo cx="122.55" cy="134.9" x="119.95" y="135.45" />
	<CurveTo cx="113.25" cy="136.75" x="99.25" y="138.1" />
	<CurveTo cx="83.1" cy="139.7" x="72.4" y="139.7" />
	<CurveTo cx="61.45" cy="139.7" x="45.5" y="138.15" />
	<CurveTo cx="31.65" cy="136.8" x="24.8" y="135.45" />
	<CurveTo cx="22.45" cy="135" x="21.75" y="134.1" />
	<CurveTo cx="21.2" cy="133.4" x="21.2" y="131.25" />
	<LineTo x="21.2" y="64.25" />
	<CurveTo cx="21.2" cy="60.25" x="25.3" y="60.25" />
	<LineTo x="119.45" y="60.25" />
	<CurveTo cx="123.55" cy="60.25" x="123.55" y="64.25" />
</Contour>
'

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 4
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
	<MoveTo x="145" y="147.35" />
	<LineTo x="144.4" y="142.95" />
	<CurveTo cx="144.2" cy="141.5" x="144.45" y="139.45" />
	<CurveTo cx="144.85" cy="135.35" x="146.9" y="132.2" />
	<CurveTo cx="153.35" cy="122.3" x="153.35" y="108.1" />
	<LineTo x="153.35" y="0" />
	<LineTo x="145.25" y="2.05" />
	<CurveTo cx="137.7" cy="3.9" x="125.45" y="5.7" />
	<CurveTo cx="101" cy="9.35" x="76.7" y="9.35" />
	<CurveTo cx="52.4" cy="9.35" x="27.95" y="5.7" />
	<LineTo x="8.1" y="2.05" />
	<LineTo x="0.05" y="0.05" />
	<LineTo x="0" y="108.1" />
	<CurveTo cx="0" cy="122.2" x="6.45" y="132.2" />
	<CurveTo cx="9.05" cy="136.2" x="9.05" y="141.45" />
	<CurveTo cx="9.05" cy="142.95" x="8.95" y="142.95" />
	<LineTo x="8.65" y="147.3" />
	<MoveTo x="8.65" y="147.3" />
	<LineTo x="9.1" y="228.85" />
	<LineTo x="8.85" y="251" />
	<LineTo x="13.35" y="252.5" />
	<CurveTo cx="38.25" cy="260.65" x="76.95" y="260.65" />
	<CurveTo cx="115.4" cy="260.65" x="140.1" y="252.65" />
	<LineTo x="144.55" y="251.2" />
	<LineTo x="145" y="147.35" />
	<MoveTo x="58.45" y="230.65" />
	<LineTo x="95.45" y="230.65" />
	<LineTo x="95.45" y="239.35" />
	<LineTo x="58.45" y="239.35" />
	<LineTo x="58.45" y="230.65" />
</Contour>
'


UPDATE dbo.Paper SET [Width] = 612, [Height] = 360 WHERE [Name] = N'Motorola Razr - V3, V3c, V3i'

UPDATE dbo.FaceToPaper SET FaceX = 97.2, FaceY = 65.4 WHERE FaceId = 3 AND PaperId = 2;

UPDATE dbo.FaceToPaper SET FaceX = 371.9, FaceY = 64 WHERE FaceId = 4 AND PaperId = 2;

COMMIT TRANSACTION
 
 
--Delete link "SiteMap", add link "HOME" in header
BEGIN TRANSACTION

UPDATE dbo.LinkPlace
SET LinkId = 1, SortOrder = 0
WHERE PageId = 1 AND LinkId = 6

UPDATE dbo.LinkPlace
SET SortOrder = 1
WHERE PageId = 1 AND LinkId = 2

UPDATE dbo.LinkPlace
SET SortOrder = 2
WHERE PageId = 1 AND LinkId = 3

DELETE dbo.LinkPlace
WHERE LinkId = 12

DELETE dbo.LinkLocalization
WHERE LinkId = 12

DELETE dbo.Link
WHERE LinkId = 12 AND PageId = 6

COMMIT TRANSACTION
