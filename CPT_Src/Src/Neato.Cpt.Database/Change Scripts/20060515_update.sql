/*
Change retailers table
*/

BEGIN TRANSACTION

EXEC sp_columns @table_name = 'Retailers', @table_owner='dbo', @column_name = 'DisplayName'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Retailers ADD
    [DisplayName] [nvarchar] (50) COLLATE Latin1_General_BIN NULL

ALTER TABLE dbo.Retailers
    ALTER COLUMN [Name] [varchar] (50) COLLATE Latin1_General_CI_AI NOT NULL


CREATE  UNIQUE  INDEX [IX_Retailers] ON [dbo].[Retailers]([Name]) ON [PRIMARY]
END

COMMIT