/* Changes
- new tables: MailVariable, MailTypeVariable
  and data for theirs
*/

BEGIN TRANSACTION

--Table: MailVariable
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MailVariable]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[MailVariable] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]

ALTER TABLE [dbo].[MailVariable] WITH NOCHECK ADD 
	CONSTRAINT [PK_MailVariable] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
END

--Table: MailTypeVariable
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MailTypeVariable]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[MailTypeVariable] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[MailId] [int] NOT NULL ,
	[MailVariableId] [int] NOT NULL 
) ON [PRIMARY]

ALTER TABLE [dbo].[MailTypeVariable] WITH NOCHECK ADD 
	CONSTRAINT [PK_MailTypeVariable] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
END

--ForeignKeys
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_MailTypeVariable_MailType]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
BEGIN
    ALTER TABLE [dbo].[MailTypeVariable] ADD 
	CONSTRAINT [FK_MailTypeVariable_MailType] FOREIGN KEY 
	(
		[MailId]
	) REFERENCES [dbo].[MailType] (
		[MailId]
	)
END	

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_MailTypeVariable_MailVariable]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
BEGIN
    ALTER TABLE [dbo].[MailTypeVariable] ADD 
	CONSTRAINT [FK_MailTypeVariable_MailVariable] FOREIGN KEY 
	(
		[MailVariableId]
	) REFERENCES [dbo].[MailVariable] (
		[Id]
	) 
END

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_MailTypeVariable_MailVariable]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
BEGIN
    ALTER TABLE [dbo].[MailTypeVariable] ADD 
	CONSTRAINT [FK_MailTypeVariable_MailType] FOREIGN KEY 
	(
		[MailId]
	) REFERENCES [dbo].[MailType] (
		[MailId]
	)
END

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_MailTypeVariable_MailVariable]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
BEGIN
    ALTER TABLE [dbo].[MailTypeVariable] ADD 
	CONSTRAINT [FK_MailTypeVariable_MailVariable] FOREIGN KEY 
	(
		[MailVariableId]
	) REFERENCES [dbo].[MailVariable] (
		[Id]
	)
END

--Data: MailVariable
if not exists (select * from [dbo].[MailVariable])
BEGIN
SET IDENTITY_INSERT [dbo].[MailVariable] ON
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (1, '%UserName')
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (2, '%NewPasswordLink')
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (3, '%MailText')
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (4, '%PrintzHomeLink')
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (5, '%UnsubscribeLink')
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (6, '%DeviceName')
SET IDENTITY_INSERT [dbo].[MailVariable] OFF
END

--Data: MailTypeVariable
if not exists (select * from [dbo].[MailTypeVariable])
BEGIN
SET IDENTITY_INSERT [dbo].[MailTypeVariable] ON
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (1, 1, 1)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (2, 1, 2)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (3, 2, 3)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (4, 2, 4)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (5, 2, 5)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (6, 5, 6)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (7, 5, 4)
SET IDENTITY_INSERT [dbo].[MailTypeVariable] OFF
END

COMMIT

-- Overview Content
IF Not Exists(SELECT * FROM dbo.OverviewPage WHERE Culture='')
BEGIN
INSERT INTO OverviewPage ([Culture], [HtmlText])
VALUES('', '')
END

UPDATE OverviewPage
SET HtmlText = '<table width="600px" cellspacing="10px" class="Paragraph">
<tr>
  <td valign="top" width="40%">
    Printz™ skins are made with a durable vinyl material that conforms to the shape of your device and protects it from bumps and scratches. Printz™ skins also feature a residue-free adhesive, and are water-resistant so your design won’t rub off if it gets wet.
  </td>
  <td>
    <strong>To personalize your Printz™ skin:</strong>
    <DL>
      <DT>Design It!
      <DD>Create your unique design by using our online design studio.<BR>Make your skin match your style! 
      <DT>Print It!
      <DD>Enjoy a professional, photo quality finish right from your inkjet printer at home.<BR>It’s quick and convenient. 
      <DT>Stick It!
      <DD>Applying and removing your skin is simple – just peel and press for a custom fit.
    </DL>
  </td>
</tr>
</table>
'
WHERE Culture=''

-- Support Content
IF Not Exists(SELECT * FROM dbo.SupportPage WHERE Culture='')
BEGIN
INSERT INTO SupportPage ([Culture], [HtmlText])
VALUES('', '')
END

UPDATE SupportPage
SET HtmlText = '<table width="600px" class="Paragraph" cellpadding="10px">
  <tr>
    <td valign="top" width="50%">
At Fellowes-NEATO, one of our top goals is helping you -our customer- find the support and service that you need and deserve. Our extensive online support section provides a number of self-service options as well as several ways for you to get in touch with us.
    </td>
    <td valign="top">
<b>Contact Us</b>
<br><br>
Corporate Headquarters<br>
Fellowes, Inc.<br>
1789 Norwood Avenue<br>
Itasca, IL 60143<br>
1-630-893-1600<br>
<p>
<b>Customer Support</b>
<br>
For website, customer service and technical support, call us Monday through Friday between 7:30 AM and 5:00 PM CT, or send an email anytime!  
</p>
Call 1-888-312-7561 or email <a href=mailto:printz@neato.com>printz@neato.com</a>.
    </td>
  </tr>
</table>
'
WHERE Culture=''