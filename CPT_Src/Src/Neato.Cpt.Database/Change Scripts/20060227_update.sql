-- alter columns begin
alter TABLE [Carrier] 
	ALTER COLUMN [Name] [nvarchar] (40) COLLATE Latin1_General_BIN NOT NULL
alter TABLE [DeviceBrand] 
	ALTER COLUMN [Name] [nvarchar] (30) COLLATE Latin1_General_BIN NOT NULL
alter TABLE [Device] 
	ALTER COLUMN [Model] [nvarchar] (50) COLLATE Latin1_General_BIN NOT NULL
-- alter columns end