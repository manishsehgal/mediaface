/*
    Remove old procedures:
        - PrDeviceIconGetByDeviceId
        - PrDeviceIconBigGetByDeviceId
        - PrDevicesEnumerate
*/

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceIconGetByDeviceId]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PrDeviceIconGetByDeviceId]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceIconBigGetByDeviceId]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PrDeviceIconBigGetByDeviceId]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDevicesEnumerate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PrDevicesEnumerate]
GO
