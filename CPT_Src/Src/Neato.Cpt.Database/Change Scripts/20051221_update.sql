/*
Rename table Brand To DeviceBrand:
*/

-- create new table DeviceBrand
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DeviceBrand]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[DeviceBrand] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (30) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Icon] [image] NULL
) ON [PRIMARY]
END

GO 

-- set primary key for DeviceBrand
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DeviceBrand]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
ALTER TABLE [dbo].[DeviceBrand] WITH NOCHECK ADD 
	CONSTRAINT [PK_DeviceBrand] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
END

GO 

-- drop foreign key for Device table (to Brand table)
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Device_Brand]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Device] DROP CONSTRAINT FK_Device_Brand

GO 

SET IDENTITY_INSERT DeviceBrand ON
INSERT INTO DeviceBrand (Id, Name)
   SELECT Id, Name FROM Brand
SET IDENTITY_INSERT DeviceBrand OFF
   
GO 

-- set foreign key for Device table (to DeviceBrand table)
ALTER TABLE [dbo].[Device] ADD 
	CONSTRAINT [FK_Device_DeviceBrand] FOREIGN KEY 
	(
		[BrandId]
	) REFERENCES [dbo].[DeviceBrand] (
		[Id]
	) ON UPDATE CASCADE

GO 

-- drop Brand table
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Brand]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Brand]

GO


/*
Create new tables:
*/

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Carrier]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Carrier] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (30) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Icon] [image] NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Carrier] WITH NOCHECK ADD 
	CONSTRAINT [PK_Carrier] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DeviceToCarrier]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[DeviceToCarrier] (
	[DeviceId] [int] NOT NULL ,
	[CarrierId] [int] NOT NULL 
) ON [PRIMARY]
END

ALTER TABLE [dbo].[DeviceToCarrier] ADD 
	CONSTRAINT [FK_DeviceToCarrier_Device] FOREIGN KEY 
	(
		[DeviceId]
	) REFERENCES [dbo].[Device] (
		[Id]
	) ON UPDATE CASCADE ,
	CONSTRAINT [FK_DeviceToCarrier_Carrier] FOREIGN KEY 
	(
		[CarrierId]
	) REFERENCES [dbo].[Carrier] (
		[Id]
	) ON UPDATE CASCADE 

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Face]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Face] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Contour] [image] NULL ,
	[Icon] [image] NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Face] WITH NOCHECK ADD 
	CONSTRAINT [PK_Face] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FaceToDevice]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[FaceToDevice] (
	[FaceId] [int] NOT NULL ,
	[DeviceId] [int] NOT NULL 
) ON [PRIMARY]
ALTER TABLE [dbo].[FaceToDevice] ADD 
	CONSTRAINT [FK_FaceToDevice_Face] FOREIGN KEY 
	(
		[FaceId]
	) REFERENCES [dbo].[Face] (
		[Id]
	) ON UPDATE CASCADE ,
	CONSTRAINT [FK_FaceToDevice_Device] FOREIGN KEY 
	(
		[DeviceId]
	) REFERENCES [dbo].[Device] (
		[Id]
	) ON UPDATE CASCADE 
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FaceLocalization]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[FaceLocalization] (
	[FaceId] [int] NOT NULL ,
	[Culture] [nvarchar] (10) COLLATE Latin1_General_BIN NOT NULL ,
	[FaceName] [nvarchar] (255) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]
ALTER TABLE [dbo].[FaceLocalization] ADD 
	CONSTRAINT [FK_FaceLocalization_Face] FOREIGN KEY 
	(
		[FaceId]
	) REFERENCES [dbo].[Face] (
		[Id]
	)
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PaperBrand]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[PaperBrand] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (30) COLLATE Latin1_General_CI_AS NOT NULL ,
	[Icon] [image] NULL
) ON [PRIMARY]
END

ALTER TABLE [dbo].[PaperBrand] WITH NOCHECK ADD 
	CONSTRAINT [PK_PaperBrand] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Paper]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Paper] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [nvarchar] (30) COLLATE Latin1_General_CI_AS NOT NULL ,
	[BrandId] [int] NOT NULL ,
	[Icon] [image] NULL ,
	[PaperW] [varchar] (8) NOT NULL ,
	[PaperH] [varchar] (8) NOT NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Paper] WITH NOCHECK ADD 
	CONSTRAINT [PK_Paper] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
	
ALTER TABLE [dbo].[Paper] ADD 
	CONSTRAINT [FK_Paper_PaperBrand] FOREIGN KEY 
	(
		[BrandId]
	) REFERENCES [dbo].[PaperBrand] (
		[Id]
	) ON UPDATE CASCADE 
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FaceToPaper]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[FaceToPaper] (
	[FaceId] [int] NOT NULL,
	[PaperId] [int] NOT NULL, 
	[FaceX] [varchar] (8) NOT NULL,
	[FaceY] [varchar] (8) NOT NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[FaceToPaper] ADD 
	CONSTRAINT [FK_FaceToPaper_Face] FOREIGN KEY 
	(
		[FaceId]
	) REFERENCES [dbo].[Face] (
		[Id]
	) ON UPDATE CASCADE ,
	CONSTRAINT [FK_FaceToPaper_Paper] FOREIGN KEY 
	(
		[PaperId]
	) REFERENCES [dbo].[Paper] (
		[Id]
	) ON UPDATE CASCADE 
END

GO

/*
ReFill Device.Faces column
*/

DECLARE @ptrFaces binary(16)

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 1
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="AudioVox 8940"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 2
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="Kyocera k433"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 3
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="LG 4NE1"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 4
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="LG 4010"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 5
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="LG 6000"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 6
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="LG F9100"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 7
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="Motorola A840"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 8
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="Motorola C330"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 9
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="Motorola C350"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 10
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="Motorola E398"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 11
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="612" h="396"/> 
    <Properties name="Motorola v180"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 12
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="Motorola V878"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 13
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="Nokia 3220"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 14
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="Nokia 6010"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 15
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="Samsung A700"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 16
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="Samsung S307"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 17
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="Samsung V205"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 18
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="Samsung X450"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 19
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="Samsung X600"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 20
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="Siemens C55"/>
</Project>
'

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 21
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="Siemens M46"/>
</Project>
'

set identity_insert dbo.Device on

INSERT INTO dbo.Device
  (Id, Model, BrandId, DeviceTypeId, Icon, Faces)
  VALUES (22, N'Motorola Razor V3', 2, 1, 0xFFD8FFE000104A46494600010200000100010000FFE100E645786966000049492A00080000000500120103000100000001000000310102001C0000004A00000032010200140000006600000013020300010000000100000069870400010000007A000000000000004143442053797374656D73204469676974616C20496D6167696E6700323030353A31323A32312031323A35313A313100050000900700040000003032323090920200040000003933320002A00400010000005100000003A00400010000006100000005A0040001000000BC0000000000000002000100020004000000523938000200070004000000303130300000000002000000FFC00011080061005103012100021101031101FFDB00840003020202020103020202030303030407040404040409060605070A090B0B0A090A0A0C0D110E0C0C100C0A0A0F140F1011121313130B0E151615121611121312010405050605060D07070D1B120F121B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1BFFC4009700010002020301010000000000000000000008090506030407020110000103030105050407050900000000000100020304051106070809122113223141511471A1D116233252618191154292B1B42533629394A3C1C4F1010100030101010000000000000000000000030405020107110002010402000504030000000000000000010203041112213105133241516171C1D1A1B1F0FFDA000C03010002110311003F00B4D44011004401100440110044061359EB6D25B3BD9B56EB0D73A8686C765B7343EAABAB6611C5102E0D193EA5C40007524801793B77E1DD41C0166DBAC0F69190F6099CDC7BC478439724BB34BD63C4C7745D1FA9596CFA7B5B7A73E31219AD16A9AA216927EC9790D1CDE7819C642E1D2FC4E7747D4DAD22B2BF5ADCECEE9413ED175B44B053B4819C39E39B949F019F3E9E6879BA37A977E1DD420A63349B6DB088DA325E1B3168FCC4785E91B35DAB6CEB6C3B3A3AB3665ABEDFA8AD22A1F4AEA9A290B9AC9598E6638100B5C0107040E841F0287AA49F46D688741100440443E29F512537092BB4911667F6D5B5BDE8C3BC66C646474233907C410A9DDB4B46E039A2ED0FDE91C5E7E2A7A6B2B92AD56D3E0E29AD1054DEE39E632B61859DC6D396B5DCF9CE7BC08C2F892D1147AA9F594A6630C8C21EDA82D2E3E18C72803C974E270A7C60FD7515387F48BB327CD9DD3F0569DC1EADCE8F742D757733F37B4EA76D3727A7654CC393EA4F68067FC214535825A6DB64FD4519602200880887C53E2ED3844DEBBC4725E6D8EF1C67EBC7EAA99EBEED351DF9F450D3C27B300973F2492467C8A922F58B657A8B69619D476AAAC89E7EA69323D4BBE6BE3E98D613D29E8F3EF77CD3CD673E5239ADFA86A2B6FF001D2CD49035B2BB97B84E47C55B1707BA82EDCCF5D527943ABCBBF375243F25E49ED8C9DC16B2C227D22E09C2200880891C5227A587844DF9B52C91CE96ED6D8A1E4C74799C609FC07554B37D8F1B44A86FA72FF20BBE342097ACB15DDB376EA5D5FB85E8ABB4DB3BB6560BF5B854BEE535B5B2C8CE5964EA1D8FDEC341CE7231E0B39B58DD3A0B26C42E55947A0281F243A6AA2E354E86D918F6693B29096921BD0B4342FA7DA78A5A428C2936BAFA71C631FEF9294F54B6DB9CF455D69DEF6ADB7F9F348CF8856EDC1F636B7746D7EF07A9D5D823DD4B1FCD7CBDF45F5EB27DA2E4942200880885C53E17CDC256E7C98C36FB6C7BB3E827F2F82A71D4D6BAF66D06B2AA3A1A87C0DE51DAB227166797C320614AA2DC195A6D2A8BEDFA2556C8B8955EF63DBAA698D9647B22A3B947A668BD89B58EBDC903A76F3B9D92CEC5C1A7BDEA7C16675C715ED49ACB66979D34DD8BDBE862BB5B27B687B2FF00249D88962747CD8EC46701D9C67AE3C55985C46386E2654FC35CE4E4A78CBF8FE0837A62071D6B6E631AE708E56E481E43CD5B9F082EEEEA5B428FEEEAFF00FAB12A6FD26C2F593E51724C1100440446E29357534DC266E91D3CAE60A9BEDAE0931FBCC338C8FCF0AAFAA28BEBDD96F5C91D7C55FB65B45A332F1E248EA43A51F7BBDC34545697D755C8EE58618A232C8E77A340C92BAB7BD06ED3D7F75BAFBA6A4B7D646D05D0555318DE01F03CAE0A7942392AA9CF07053DBE0A7388628D99F26B70AC6B8421C6ED3B488FEEEAD1FD333E4A9D75848BB6CF3264FC4554D0088022022371498F9F84D5CDF8388EFF006979FF0052D1FF002AB766A16BE6763C0B8AD1B4E99917EF0E26E7B1AD5965D9EEDA3F6C6A0A3A97D054D14B4134D4ACE69E9C3F1DF60E87C0729C1CE1C7195DADE4B695A6B699AF6D1F45A9EB64A5B4D2C913ABABA3ECE6AA7C8FE72037248634E79738FB4EE8000BB9519BAEA7EC578D58F95AFB9E3ADA6699803F82B03E110797613B5087EE6AC67F4EDF9286E56122DDA3CC993F515135022008808ADC4D29FDA7847EA3606E4B6E96B7019C75F6C8FCD564C57FB44240B952DDA826EA1CD752FB4B41F7C64BBF56057AD67A67832EF61BB587C9F5537FD233C7837D7C24107229EA617B48FC7B3042C74B76D30CA72C8AF52D538BB3931544AECFE6CF72BAEAD3EFF666AA5552C63FA31CFAF63E6FECFB6DC2B08EBFDC185BFABF07E0A7D70888EAE1D9AED5E2AC8DD13CEA2A590C44FD82EA77123D7FF151B996C9611A5691D64F2F92C151523502200880F03DFAF4A55EAFE16FAD6828207CD3D1C54D736B18DC92DA7A98E57FE8C6B8FE4AB8A3D47A62E9DEAAA6A691A40239E36B81F82B96D249B5F6FC991E2116F56BEBF813C3A1BB17385A689A5DE00340C7E8B1D3BB4946710DB2907AF7010AF3714652DDFB98AADBCDAE9B229638A368EB88DA07F20A7370BAD39594BB01D75ACDF1E28F50EA260A57F948208035E47E01CF23DED3E8B3EEA69B8A46BD8C1A6DB26CA2AA6A84401101F13C10D4D1C94D530B258A56963D8F68735CD2304107A104792F16D4DB976EC9AA6E32D6566C9ED943512F5325AA69ADF83EBCB0B9ADF82EA3271E8E2508CD6248D0ABB86E6EF7572B9D4D70D6B421DE0D82F61E1BFE646E3F15C143C34F77CA5979AB2F1AE2E233F667BCB231FEDC4D3F1523AAD90AB6A6BD8DCB4F6E23BACE9EA96540D974174998E0E0FBB5754568FE07BCB0FF000AF70B2D8AC9A6F4CC165D3B67A2B5DBE95BCB0525153B6086219CE1AC68007524F40A26F24F18A8F4779178741100440110044011004401100440110044011004401101FFD9, 0x00)

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 22
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="612" h="396"/> 
    <Properties name="Motorola Razor V3"/>
</Project>
'


--- Phone: Samsung X495/497 ---

INSERT INTO dbo.Device
  (Id, Model, BrandId, DeviceTypeId, Icon, Faces)
  VALUES (23, N'Samsung X495/497', 5, 1, 0xFFD8FFE000104A46494600010200000100010000FFE100E645786966000049492A00080000000500120103000100000001000000310102001C0000004A00000032010200140000006600000013020300010000000100000069870400010000007A000000000000004143442053797374656D73204469676974616C20496D6167696E6700323030353A31323A32312031323A35323A323400050000900700040000003032323090920200040000003533390002A00400010000002F00000003A00400010000004C00000005A0040001000000BC0000000000000002000100020004000000523938000200070004000000303130300000000000000000FFC0001108004C002F03012100021101031101FFDB00840003020202020103020202030303030407040404040409060605070A090B0B0A090A0A0C0D110E0C0C100C0A0A0F140F1011121313130B0E151615121611121312010405050605060D07070D1B120F121B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1B1BFFC40092000001050101010100000000000000000007000405060908030102100001020405010406090501000000000001020300040511060708122113143141510922718191A1153242556192A2B1C1435262B2C2D1010003010101000000000000000000000000010203050411000201010605040300000000000000000001021103123132334104214281C1225171F061D1E1FFDA000C03010002110311003F00D4D80D1D4FE0CAA668CD612C1D26FD62664BA9D59A5B825E5494292956D5904AB950F017B1B5E0DAA6739A81352B99388A6D7BBB3D05849FB25E79C23DE0089895C6159572F228CE8F243EE367E60C45EFC12A6CF439954693C532D47ADB6A927E6D214D381C0EB26EA29E542C4723C47888B7C59A464A4426379F552B272B95242B6AA569CFBA93FE41B36F9DA33FF468C4FD5F33B1ACDCC39BE508644B94125236B8B4A813DDBB806C2E4022F6BDA296079ADB9CD1D8B4EA5A5207113D2D228000B4494900BD54D5E670A54309559873A6C75261A98DD7095025BB006D6DC0F3636B806DDD68E9EA0CE7D2181E9D3FBB77699469EBF9EE403FCC36167CA72457B3782CE9971206D4127E8F73BC5EE3C47BC473268DB0FA2434A7235308DA67A6A74A8F9D9E047F306C45AEA20CCAC10E4DD5173231B622612A99ED01A626425091BD4AD9DD7DBEB5ADE400F0895A760C7E566D974E37C40E869495143930928584941B286DE6FB4FE6315797B095973ADE60C75912899BD37A145370CBC17F878FFE41D72B9C2EE9C30C2CFDD32E3E0D8119EE6D0D467CCD347534EB8851E720E7ED001C900BC37E8BC92ACB0B0D3B254EAA4EA167B92A487160FB8A41F74375BAE844F517C19413DAA5D52D56826B3299ED8F768DCB703752E934909B6ED9B6C4DB727803BAF0EE5B53DAB5C374A9AAA54B3C731992CB3D46549A9879B4A81207512AB803701C5B911D4B2E06AAFB8B714AAF0AE1F7B1E4971114EEA6AF569BFDFE9AA39C3539FC57E8D5A0E24AB2D2ECF54A894E9F9A5A5010953AECBA56B200E002A51E077474065524A74DB86527EEB63FD44727A9FDF73DD0CEC73986D75B23EB8D7F748B9FB405F2DB0E4C56FD17F2B86A4109ED153A154A4D9055B46F752EA1373E1CA8731A2C099EA76F264DA7451AB6C3F85DDA7399138A1D71D05B2A96926A66D6D9CA569738B940E6C4103C61E553497AD0C4786D74B564963073B493B82E49A974A8DC9F5C95803D623F0E018EC478B70B276719726A9B1E17C329DA29B583A9A9199B8767B0EFA31695872AAD25B9EA3D069B233294A82825D6984A16011C1F59245E0E99688E9E9F30E23CA96C0FD02387D6FB793A31CE48E2967B465C5518B5F7CA383F4C0DF2399E969530E309FE9B2FA7E0F28468B014B517C7E8BE74D3DE5223F694240FAA224B061A9017D2B5593C7ADD248FCC60A58259ECF93D4462DF529EC0FD0227A9F6F2259C939E6FAD467D93F6DA527E20C0DF27190CE46CA4A91632D3336CFB2CFAE345803D45F0FC17522C7910A24A059A8E3BB4ECB950799A9C97640F3BAE0C94B604AE1B949602C1A6108F824089EA147331C900A6C6073976D991557E8AE0295C9D5DE56D3DFB1DB2D27D879F845AC184B3AEE58D54F051303B5CC0ED0E75090E7D4E00B27C871F384CC92DA990E2A7E61C03BD2A22DFB4372AEC392ABAD4A0675C9AAACC613A0B492A5CFE2162E902F7422EB51F60007C60C40002C3BA22828E66288E99A3B6AA9B93F2850D4C3C90970A85D2E5BBAF6E6E2E79865B551AAD9ABB62C69EDB9F8B7303FE808F006BCB7C21142D80FDB766D0123F2DCFCA1D111EAF61C4A61FDF5B62AB575B4F4D4B050974B693B19DD6DC45F92A361CF1EC89A84525414280A14280050A003FFFD9, 0x00)

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 23
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
    </Faces>
    <Paper w="612" h="396"/> 
    <Properties name="Samsung X495/497"/>
</Project>
'

set identity_insert dbo.Device off

GO

/*
Fill Face table:
*/

set identity_insert dbo.Face on
DECLARE @ptrContour binary(16)

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (1, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 1
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="26.95" y="17.75" />
                <LineTo x="65.55" y="17.70" />
                <LineTo x="84.85" y="17.65" />
                <LineTo x="104.15" y="17.65" />
                <LineTo x="105.70" y="17.65" />
                <CurveTo cx="108.85" cy="17.65" x="108.85" y="14.05" />
                <LineTo x="108.85" y="13.25" />
                <LineTo x="108.85" y="8.75" />
                <LineTo x="108.85" y="6.50" />
                <LineTo x="108.90" y="4.25" />
                <LineTo x="108.90" y="2.90" />
                <CurveTo cx="108.85" cy="0.00" x="112.65" y="0.55" />
                <LineTo x="112.75" y="0.55" />
                <CurveTo cx="121.45" cy="1.85" x="124.70" y="7.05" />
                <CurveTo cx="127.90" cy="12.15" x="127.90" y="24.60" />
                <LineTo x="127.90" y="106.05" />
                <CurveTo cx="127.90" cy="131.40" x="126.25" y="154.25" />
                <CurveTo cx="124.55" cy="177.60" x="120.90" y="202.10" />
                <CurveTo cx="120.30" cy="205.95" x="119.80" y="208.45" />
                <CurveTo cx="117.45" cy="219.40" x="112.00" y="224.65" />
                <CurveTo cx="105.75" cy="230.60" x="92.35" y="232.55" />
                <LineTo x="35.00" y="232.55" />
                <CurveTo cx="21.25" cy="230.95" x="14.90" y="225.65" />
                <CurveTo cx="9.25" cy="221.00" x="6.80" y="210.90" />
                <CurveTo cx="2.50" cy="193.00" x="0.95" y="168.40" />
                <CurveTo cx="0.35" cy="158.50" x="0.15" y="146.50" />
                <CurveTo cx="0.00" cy="137.85" x="0.00" y="125.25" />
                <LineTo x="0.15" y="74.45" />
                <CurveTo cx="0.20" cy="46.10" x="0.20" y="23.10" />
                <CurveTo cx="0.20" cy="10.55" x="4.45" y="5.60" />
                <CurveTo cx="8.30" cy="1.15" x="18.30" y="0.25" />
                <CurveTo cx="21.25" cy="0.00" x="22.05" y="0.45" />
                <CurveTo cx="23.15" cy="1.15" x="23.15" y="4.05" />
                <LineTo x="23.15" y="13.30" />
                <CurveTo cx="23.15" cy="13.95" x="23.10" y="14.95" />
                <CurveTo cx="23.05" cy="17.75" x="26.80" y="17.75" />
                <LineTo x="26.95" y="17.75" />

                <MoveTo x="35.15" y="67.75" />
                <LineTo x="91.95" y="67.75" />
                <CurveTo cx="96.25" cy="67.75" x="96.25" y="72.20" />
                <LineTo x="96.25" y="111.85" />
                <CurveTo cx="96.25" cy="116.25" x="91.95" y="116.25" />
                <LineTo x="35.15" y="116.25" />
                <CurveTo cx="30.90" cy="116.25" x="30.90" y="111.85" />
                <LineTo x="30.90" y="72.20" />
                <CurveTo cx="30.90" cy="67.75" x="35.15" y="67.75" />

                <MoveTo x="63.35" y="186.95" />
                <CurveTo cx="68.55" cy="186.95" x="72.20" y="183.25" />
                <CurveTo cx="75.90" cy="179.55" x="75.90" y="174.35" />
                <CurveTo cx="75.90" cy="169.20" x="72.20" y="165.50" />
                <CurveTo cx="68.55" cy="161.80" x="63.35" y="161.80" />
                <CurveTo cx="58.15" cy="161.80" x="54.45" y="165.50" />
                <CurveTo cx="50.75" cy="169.15" x="50.75" y="174.35" />
                <CurveTo cx="50.75" cy="179.55" x="54.45" y="183.25" />
                <CurveTo cx="58.15" cy="186.95" x="63.35" y="186.95" />

                <MoveTo x="45.45" y="206.60" />
                <LineTo x="81.85" y="206.60" />
                <CurveTo cx="83.85" cy="206.60" x="85.25" y="208.00" />
                <CurveTo cx="86.65" cy="209.40" x="86.65" y="211.40" />
                <CurveTo cx="86.65" cy="213.40" x="85.25" y="214.80" />
                <CurveTo cx="83.85" cy="216.20" x="81.85" y="216.20" />
                <LineTo x="45.45" y="216.20" />
                <CurveTo cx="43.45" cy="216.20" x="42.05" y="214.80" />
                <CurveTo cx="40.65" cy="213.40" x="40.65" y="211.40" />
                <CurveTo cx="40.65" cy="209.40" x="42.05" y="208.00" />
                <CurveTo cx="43.45" cy="206.60" x="45.45" y="206.60" />

            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Front"/>
                    <Culture key="en" value="Front"/>
                    <Culture key="ru" value="&#x0424;&#x0430;&#x0441;&#x0430;&#x0434;"/>
                </Caption>
            </Localization>            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (2, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 2
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="13.50" y="203.80" />
                <LineTo x="15.95" y="203.75" />
                <LineTo x="19.60" y="203.65" />
                <LineTo x="23.05" y="203.55" />
                <CurveTo cx="28.90" cy="203.45" x="32.60" y="203.55" />
                <CurveTo cx="38.15" cy="203.70" x="42.10" y="204.35" />
                <CurveTo cx="45.35" cy="204.90" x="45.20" y="208.85" />
                <CurveTo cx="45.15" cy="209.65" x="45.05" y="210.80" />
                <CurveTo cx="44.80" cy="213.10" x="46.20" y="214.30" />
                <LineTo x="84.60" y="215.30" />
                <CurveTo cx="87.95" cy="212.25" x="87.60" y="207.45" />
                <CurveTo cx="87.30" cy="203.45" x="91.80" y="203.75" />
                <CurveTo cx="93.45" cy="203.85" x="94.45" y="203.85" />
                <LineTo x="103.10" y="203.95" />
                <LineTo x="111.90" y="204.05" />
                <CurveTo cx="112.85" cy="204.05" x="114.85" y="203.90" />
                <CurveTo cx="122.70" cy="203.40" x="122.70" y="207.60" />
                <CurveTo cx="122.70" cy="221.05" x="111.75" y="224.90" />
                <CurveTo cx="105.10" cy="227.25" x="86.70" y="226.85" />
                <CurveTo cx="81.05" cy="226.70" x="78.25" y="226.70" />
                <LineTo x="51.65" y="226.70" />
                <CurveTo cx="50.05" cy="226.60" x="45.70" y="226.50" />
                <CurveTo cx="38.25" cy="226.30" x="34.05" y="225.95" />
                <CurveTo cx="26.85" cy="225.30" x="21.65" y="223.70" />
                <CurveTo cx="7.60" cy="219.35" x="5.95" y="207.60" />
                <CurveTo cx="5.95" cy="203.75" x="11.30" y="203.80" />
                <CurveTo cx="12.70" cy="203.85" x="13.50" y="203.80" />
                <MoveTo x="53.80" y="0.05" />
                <LineTo x="107.35" y="0.05" />
                <LineTo x="109.25" y="0.05" />
                <CurveTo cx="116.55" cy="0.00" x="118.80" y="1.05" />
                <CurveTo cx="122.35" cy="2.70" x="122.35" y="8.55" />
                <LineTo x="122.35" y="38.60" />
                <CurveTo cx="122.35" cy="44.25" x="121.30" y="45.30" />
                <CurveTo cx="120.20" cy="46.40" x="114.50" y="46.40" />
                <LineTo x="52.35" y="46.40" />
                <CurveTo cx="47.25" cy="46.40" x="45.85" y="45.20" />
                <CurveTo cx="44.30" cy="43.90" x="44.30" y="39.15" />
                <LineTo x="44.30" y="6.65" />
                <CurveTo cx="44.30" cy="0.05" x="53.80" y="0.05" />
                <MoveTo x="9.80" y="51.50" />
                <LineTo x="120.90" y="51.50" />
                <CurveTo cx="131.05" cy="51.50" x="131.05" y="61.50" />
                <CurveTo cx="131.05" cy="77.30" x="130.30" y="96.80" />
                <CurveTo cx="129.85" cy="108.45" x="128.65" y="131.70" />
                <CurveTo cx="127.55" cy="152.25" x="127.10" y="162.55" />
                <CurveTo cx="126.40" cy="179.75" x="126.25" y="193.65" />
                <CurveTo cx="126.20" cy="199.40" x="122.65" y="201.00" />
                <CurveTo cx="120.40" cy="202.05" x="113.20" y="201.95" />
                <LineTo x="110.70" y="201.95" />
                <LineTo x="17.85" y="201.95" />
                <LineTo x="16.75" y="201.95" />
                <CurveTo cx="10.65" cy="201.95" x="8.50" y="201.15" />
                <CurveTo cx="5.00" cy="199.85" x="4.85" y="195.45" />
                <LineTo x="0.00" y="60.55" />
                <CurveTo cx="0.00" cy="51.50" x="9.80" y="51.50" />

                <MoveTo x="84.45" y="5.00" />
                <LineTo x="113.15" y="5.00" />
                <CurveTo cx="117.10" cy="5.00" x="117.10" y="9.00" />
                <LineTo x="117.10" y="39.20" />
                <CurveTo cx="117.10" cy="43.20" x="113.15" y="43.20" />
                <LineTo x="84.45" y="43.20" />
                <CurveTo cx="80.45" cy="43.20" x="80.45" y="39.20" />
                <LineTo x="80.45" y="9.00" />
                <CurveTo cx="80.45" cy="5.00" x="84.45" y="5.00" />

            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Back"/>
                    <Culture key="en" value="Back"/>
                    <Culture key="ru" value="&#x041E;&#x0431;&#x043E;&#x0440;&#x043E;&#x0442;"/>
                </Caption>
            </Localization>            
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (3, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 3
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 3" x="50" y="420">
            <Contour x="0" y="0">
                <MoveTo x="42.40" y="0.00" />
                <LineTo x="76.50" y="0.00" />
                <CurveTo cx="97.80" cy="0.95" x="107.90" y="5.10" />
                <CurveTo cx="118.15" cy="9.50" x="120.45" y="23.50" />
                <LineTo x="124.20" y="72.95" />
                <CurveTo cx="125.25" cy="86.90" x="125.25" y="102.50" />
                <LineTo x="125.25" y="197.10" />
                <LineTo x="125.25" y="200.65" />
                <CurveTo cx="125.35" cy="217.20" x="123.20" y="223.05" />
                <CurveTo cx="119.70" cy="232.65" x="108.15" y="232.65" />
                <LineTo x="107.20" y="232.65" />
                <CurveTo cx="104.20" cy="232.65" x="104.35" y="230.35" />
                <CurveTo cx="104.40" cy="229.30" x="104.40" y="228.60" />
                <LineTo x="104.45" y="220.70" />
                <LineTo x="104.50" y="212.75" />
                <CurveTo cx="104.50" cy="210.20" x="102.60" y="210.25" />
                <CurveTo cx="101.75" cy="210.25" x="101.25" y="210.25" />
                <LineTo x="24.40" y="209.95" />
                <CurveTo cx="19.95" cy="209.95" x="20.00" y="214.05" />
                <LineTo x="20.05" y="216.50" />
                <CurveTo cx="20.15" cy="224.65" x="20.00" y="228.75" />
                <LineTo x="20.00" y="228.90" />
                <CurveTo cx="19.95" cy="232.65" x="16.30" y="232.65" />
                <LineTo x="15.30" y="232.65" />
                <LineTo x="14.45" y="232.60" />
                <LineTo x="13.55" y="232.55" />
                <LineTo x="12.55" y="232.55" />
                <CurveTo cx="6.55" cy="232.55" x="3.75" y="225.00" />
                <CurveTo cx="1.80" cy="219.70" x="0.65" y="207.20" />
                <CurveTo cx="0.05" cy="200.55" x="0.00" y="191.70" />
                <CurveTo cx="0.00" cy="186.60" x="0.20" y="177.65" />
                <CurveTo cx="0.35" cy="172.35" x="0.35" y="170.40" />
                <LineTo x="0.45" y="134.15" />
                <LineTo x="0.55" y="113.30" />
                <LineTo x="0.60" y="92.90" />
                <CurveTo cx="0.70" cy="77.50" x="1.70" y="62.65" />
                <LineTo x="2.75" y="48.45" />
                <LineTo x="3.25" y="41.35" />
                <LineTo x="3.75" y="34.25" />
                <CurveTo cx="4.30" cy="25.25" x="5.40" y="20.55" />
                <CurveTo cx="6.80" cy="14.70" x="9.80" y="11.40" />
                <CurveTo cx="17.15" cy="2.10" x="42.40" y="0.00" />

                <MoveTo x="57.85" y="11.20" />
                <LineTo x="66.60" y="11.20" />
                <CurveTo cx="67.45" cy="11.20" x="68.05" y="11.80" />
                <CurveTo cx="68.65" cy="12.40" x="68.65" y="13.25" />
                <CurveTo cx="68.65" cy="14.10" x="68.05" y="14.65" />
                <CurveTo cx="67.45" cy="15.25" x="66.60" y="15.25" />
                <LineTo x="57.85" y="15.25" />
                <CurveTo cx="57.05" cy="15.25" x="56.45" y="14.65" />
                <CurveTo cx="55.85" cy="14.10" x="55.85" y="13.25" />
                <CurveTo cx="55.85" cy="12.40" x="56.45" y="11.80" />
                <CurveTo cx="57.00" cy="11.20" x="57.85" y="11.20" />

                <MoveTo x="39.25" y="22.20" />
                <LineTo x="85.40" y="22.20" />
                <CurveTo cx="87.40" cy="22.20" x="88.75" y="23.60" />
                <CurveTo cx="90.15" cy="25.00" x="90.15" y="26.95" />
                <CurveTo cx="90.15" cy="28.90" x="88.75" y="30.30" />
                <CurveTo cx="87.40" cy="31.70" x="85.40" y="31.70" />
                <LineTo x="39.25" y="31.70" />
                <CurveTo cx="37.30" cy="31.70" x="35.90" y="30.30" />
                <CurveTo cx="34.50" cy="28.90" x="34.50" y="26.95" />
                <CurveTo cx="34.50" cy="25.00" x="35.90" y="23.60" />
                <CurveTo cx="37.30" cy="22.20" x="39.25" y="22.20" />

                <MoveTo x="23.80" y="59.75" />
                <LineTo x="100.40" y="59.75" />
                <CurveTo cx="107.65" cy="59.75" x="107.65" y="67.00" />
                <LineTo x="107.65" y="163.90" />
                <CurveTo cx="107.65" cy="171.20" x="100.40" y="171.20" />
                <LineTo x="23.80" y="171.20" />
                <CurveTo cx="16.50" cy="171.20" x="16.50" y="163.90" />
                <LineTo x="16.50" y="67.00" />
                <CurveTo cx="16.50" cy="59.75" x="23.80" y="59.75" />

            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Inside 1"/>
                    <Culture key="en" value="Inside 1"/>
                    <Culture key="ru" value="&#x0412;&#x043D;&#x0443;&#x0442;&#x0440;&#x0438;&#x00A0;&#x0031;"/>
                </Caption>
            </Localization>            
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (4, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 4
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 4" x="300" y="420">
            <Contour x="0" y="0">
                <MoveTo x="19.85" y="0.00" />
                <LineTo x="107.10" y="0.00" />
                <CurveTo cx="121.55" cy="0.00" x="127.85" y="6.05" />
                <CurveTo cx="134.30" cy="12.20" x="134.30" y="26.65" />
                <CurveTo cx="134.30" cy="42.50" x="133.90" y="62.25" />
                <CurveTo cx="133.70" cy="74.15" x="133.10" y="97.85" />
                <CurveTo cx="132.45" cy="122.55" x="132.25" y="134.90" />
                <CurveTo cx="131.85" cy="155.50" x="131.90" y="172.00" />
                <CurveTo cx="131.95" cy="195.90" x="126.05" y="204.85" />
                <CurveTo cx="118.30" cy="216.65" x="95.65" y="215.40" />
                <LineTo x="61.10" y="215.20" />
                <LineTo x="57.40" y="215.20" />
                <CurveTo cx="49.50" cy="215.15" x="45.55" y="215.00" />
                <CurveTo cx="38.95" cy="214.75" x="33.75" y="213.95" />
                <CurveTo cx="19.40" cy="211.70" x="13.20" y="205.70" />
                <CurveTo cx="5.25" cy="199.90" x="2.95" y="187.40" />
                <CurveTo cx="1.35" cy="179.05" x="1.35" y="156.90" />
                <CurveTo cx="1.35" cy="138.65" x="1.15" y="119.75" />
                <CurveTo cx="1.00" cy="108.40" x="0.70" y="87.25" />
                <CurveTo cx="0.35" cy="65.80" x="0.20" y="54.20" />
                <CurveTo cx="0.00" cy="34.85" x="0.00" y="16.00" />
                <CurveTo cx="0.00" cy="8.70" x="6.10" y="4.20" />
                <CurveTo cx="11.75" cy="0.00" x="19.85" y="0.00" />

                <MoveTo x="66.90" y="202.05" />
                <CurveTo cx="67.40" cy="202.05" x="67.75" y="202.45" />
                <CurveTo cx="68.15" cy="202.80" x="68.15" y="203.30" />
                <LineTo x="68.15" y="209.85" />
                <CurveTo cx="68.15" cy="210.35" x="67.75" y="210.75" />
                <CurveTo cx="67.40" cy="211.10" x="66.90" y="211.10" />
                <CurveTo cx="66.40" cy="211.10" x="66.00" y="210.75" />
                <CurveTo cx="65.65" cy="210.40" x="65.65" y="209.85" />
                <LineTo x="65.65" y="203.30" />
                <CurveTo cx="65.65" cy="202.80" x="66.00" y="202.45" />
                <CurveTo cx="66.40" cy="202.05" x="66.90" y="202.05" />

                <MoveTo x="52.80" y="176.45" />
                <CurveTo cx="67.55" cy="177.25" x="80.75" y="176.45" />
                <LineTo x="80.75" y="194.40" />
                <CurveTo cx="66.45" cy="195.40" x="52.80" y="194.40" />
                <LineTo x="52.80" y="176.45" />

                <MoveTo x="51.90" y="148.10" />
                <CurveTo cx="65.45" cy="149.35" x="81.35" y="148.10" />
                <LineTo x="81.35" y="166.15" />
                <CurveTo cx="66.75" cy="167.55" x="51.90" y="166.15" />
                <LineTo x="51.90" y="148.10" />

                <MoveTo x="51.90" y="120.10" />
                <CurveTo cx="65.55" cy="121.35" x="81.55" y="120.10" />
                <LineTo x="81.55" y="138.30" />
                <CurveTo cx="66.50" cy="139.65" x="51.90" y="138.30" />
                <LineTo x="51.90" y="120.10" />

                <MoveTo x="51.90" y="92.25" />
                <CurveTo cx="65.80" cy="93.55" x="82.05" y="92.25" />
                <LineTo x="82.05" y="111.20" />
                <CurveTo cx="66.75" cy="112.65" x="51.90" y="111.20" />
                <LineTo x="51.90" y="92.25" />

                <MoveTo x="16.95" y="172.70" />
                <LineTo x="42.70" y="174.25" />
                <LineTo x="42.70" y="192.30" />
                <LineTo x="39.10" y="192.30" />
                <CurveTo cx="19.95" cy="191.25" x="16.95" y="180.70" />
                <LineTo x="16.95" y="172.70" />

                <MoveTo x="115.50" y="173.65" />
                <LineTo x="90.05" y="175.30" />
                <LineTo x="90.05" y="192.15" />
                <LineTo x="93.65" y="192.15" />
                <CurveTo cx="109.55" cy="191.35" x="114.15" y="184.05" />
                <CurveTo cx="115.10" cy="182.55" x="115.35" y="180.95" />
                <CurveTo cx="115.50" cy="179.95" x="115.50" y="177.75" />
                <LineTo x="115.50" y="176.90" />
                <LineTo x="115.50" y="173.65" />

                <MoveTo x="117.85" y="145.35" />
                <LineTo x="90.55" y="148.00" />
                <LineTo x="90.55" y="165.60" />
                <LineTo x="116.45" y="162.90" />
                <LineTo x="117.85" y="145.35" />

                <MoveTo x="118.25" y="116.70" />
                <LineTo x="91.50" y="119.45" />
                <LineTo x="91.50" y="137.55" />
                <LineTo x="117.45" y="134.75" />
                <LineTo x="118.25" y="116.70" />

                <MoveTo x="92.10" y="91.95" />
                <LineTo x="109.80" y="91.30" />
                <LineTo x="110.20" y="91.30" />
                <CurveTo cx="113.35" cy="91.15" x="114.45" y="91.60" />
                <CurveTo cx="116.05" cy="92.20" x="117.50" y="94.80" />
                <CurveTo cx="120.00" cy="99.55" x="119.20" y="108.00" />
                <LineTo x="92.10" y="110.40" />
                <LineTo x="92.10" y="91.95" />

                <MoveTo x="41.55" y="91.90" />
                <LineTo x="24.30" y="91.25" />
                <LineTo x="23.85" y="91.25" />
                <CurveTo cx="20.85" cy="91.15" x="19.80" y="91.60" />
                <CurveTo cx="18.20" cy="92.20" x="16.85" y="94.70" />
                <CurveTo cx="14.40" cy="99.15" x="15.20" y="107.40" />
                <LineTo x="41.55" y="109.70" />
                <LineTo x="41.55" y="91.90" />

                <MoveTo x="14.60" y="116.90" />
                <LineTo x="41.45" y="119.65" />
                <LineTo x="41.45" y="137.70" />
                <LineTo x="15.05" y="134.85" />
                <LineTo x="14.60" y="116.90" />

                <MoveTo x="16.10" y="145.05" />
                <LineTo x="42.00" y="147.70" />
                <LineTo x="42.00" y="165.45" />
                <LineTo x="16.10" y="162.60" />
                <LineTo x="16.10" y="145.05" />

                <MoveTo x="33.40" y="25.25" />
                <LineTo x="33.85" y="58.50" />
                <CurveTo cx="36.50" cy="65.15" x="40.90" y="66.50" />
                <LineTo x="40.90" y="79.60" />
                <CurveTo cx="29.25" cy="80.45" x="22.55" y="78.15" />
                <CurveTo cx="15.20" cy="75.60" x="13.60" y="69.10" />
                <LineTo x="13.60" y="35.00" />
                <CurveTo cx="16.00" cy="28.75" x="21.70" y="26.45" />
                <CurveTo cx="26.30" cy="24.60" x="33.40" y="25.25" />

                <MoveTo x="100.45" y="25.55" />
                <LineTo x="100.05" y="58.80" />
                <CurveTo cx="97.45" cy="65.50" x="93.25" y="66.75" />
                <LineTo x="93.25" y="79.90" />
                <CurveTo cx="104.50" cy="80.75" x="110.90" y="78.40" />
                <CurveTo cx="117.95" cy="75.85" x="119.55" y="69.40" />
                <LineTo x="119.55" y="35.30" />
                <CurveTo cx="117.25" cy="29.00" x="111.75" y="26.75" />
                <CurveTo cx="107.30" cy="24.90" x="100.45" y="25.55" />

                <MoveTo x="54.10" y="69.70" />
                <LineTo x="79.30" y="69.70" />
                <CurveTo cx="81.00" cy="69.70" x="82.25" y="70.80" />
                <CurveTo cx="83.50" cy="71.90" x="83.50" y="73.45" />
                <LineTo x="83.50" y="77.70" />
                <CurveTo cx="83.50" cy="79.25" x="82.25" y="80.35" />
                <CurveTo cx="81.00" cy="81.45" x="79.30" y="81.45" />
                <LineTo x="54.10" y="81.45" />
                <CurveTo cx="52.40" cy="81.45" x="51.15" y="80.35" />
                <CurveTo cx="49.90" cy="79.25" x="49.90" y="77.70" />
                <LineTo x="49.90" y="73.45" />
                <CurveTo cx="49.90" cy="71.90" x="51.15" y="70.80" />
                <CurveTo cx="52.40" cy="69.70" x="54.10" y="69.70" />

                <MoveTo x="50.45" y="12.05" />
                <LineTo x="85.00" y="12.05" />
                <CurveTo cx="93.70" cy="12.05" x="93.70" y="21.50" />
                <LineTo x="93.70" y="52.95" />
                <CurveTo cx="93.70" cy="62.40" x="85.00" y="62.40" />
                <LineTo x="50.45" y="62.40" />
                <CurveTo cx="41.75" cy="62.40" x="41.75" y="52.95" />
                <LineTo x="41.75" y="21.50" />
                <CurveTo cx="41.75" cy="12.05" x="50.45" y="12.05" />

            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Inside 2"/>
                    <Culture key="en" value="Inside 2"/>
                    <Culture key="ru" value="&#x0412;&#x043D;&#x0443;&#x0442;&#x0440;&#x0438;&#x00A0;&#x0032;"/>
                </Caption>
            </Localization>            
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (5, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 5
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="58.35" y="0.00" />
                <LineTo x="73.15" y="0.00" />
                <CurveTo cx="99.35" cy="0.00" x="114.55" y="13.75" />
                <CurveTo cx="130.05" cy="27.80" x="130.75" y="53.30" />
                <CurveTo cx="131.45" cy="77.70" x="131.60" y="98.90" />
                <CurveTo cx="131.75" cy="122.90" x="131.30" y="144.75" />
                <CurveTo cx="130.20" cy="194.05" x="125.60" y="237.70" />
                <CurveTo cx="124.05" cy="252.45" x="121.85" y="261.90" />
                <CurveTo cx="119.15" cy="273.90" x="114.65" y="281.75" />
                <CurveTo cx="104.10" cy="300.15" x="80.50" y="300.15" />
                <LineTo x="51.70" y="300.15" />
                <CurveTo cx="28.10" cy="300.15" x="17.40" y="281.05" />
                <CurveTo cx="12.90" cy="272.95" x="10.20" y="260.70" />
                <CurveTo cx="8.00" cy="250.90" x="6.65" y="236.30" />
                <CurveTo cx="2.50" cy="192.05" x="1.20" y="146.50" />
                <CurveTo cx="0.00" cy="102.65" x="1.45" y="56.85" />
                <CurveTo cx="2.30" cy="30.85" x="17.35" y="15.55" />
                <CurveTo cx="32.65" cy="0.00" x="58.35" y="0.00" />

                <MoveTo x="26.10" y="50.00" />
                <CurveTo cx="67.05" cy="48.50" x="106.85" y="50.00" />
                <CurveTo cx="110.15" cy="50.15" x="110.45" y="53.20" />
                <CurveTo cx="111.55" cy="65.95" x="111.50" y="79.45" />
                <CurveTo cx="111.40" cy="91.10" x="110.45" y="106.15" />
                <CurveTo cx="110.35" cy="108.60" x="106.85" y="108.60" />
                <CurveTo cx="66.50" cy="110.10" x="26.10" y="108.60" />
                <CurveTo cx="23.35" cy="108.60" x="23.05" y="106.65" />
                <CurveTo cx="21.40" cy="93.40" x="21.50" y="79.30" />
                <CurveTo cx="21.60" cy="66.95" x="23.05" y="52.50" />
                <CurveTo cx="23.25" cy="50.15" x="26.10" y="50.00" />

                <MoveTo x="51.70" y="175.00" />
                <LineTo x="58.25" y="180.90" />
                <LineTo x="58.15" y="180.70" />
                <CurveTo cx="58.15" cy="184.10" x="60.45" y="186.70" />
                <CurveTo cx="62.85" cy="189.40" x="66.15" y="189.40" />
                <CurveTo cx="74.25" cy="189.40" x="74.25" y="180.90" />
                <LineTo x="74.20" y="180.95" />
                <LineTo x="80.30" y="175.25" />
                <LineTo x="80.20" y="175.35" />
                <CurveTo cx="88.75" cy="175.35" x="88.75" y="167.30" />
                <CurveTo cx="88.75" cy="159.70" x="80.70" y="159.70" />
                <LineTo x="74.05" y="152.85" />
                <LineTo x="74.05" y="152.90" />
                <CurveTo cx="74.05" cy="144.95" x="66.00" y="144.95" />
                <CurveTo cx="58.00" cy="144.95" x="58.00" y="153.15" />
                <LineTo x="57.95" y="153.05" />
                <LineTo x="52.20" y="159.60" />
                <CurveTo cx="43.70" cy="159.60" x="43.70" y="167.10" />
                <CurveTo cx="43.70" cy="175.00" x="51.75" y="175.00" />
                <LineTo x="51.70" y="175.00" />

                <MoveTo x="32.25" y="165.20" />
                <CurveTo cx="35.65" cy="165.20" x="38.10" y="162.80" />
                <CurveTo cx="40.55" cy="160.35" x="40.55" y="156.95" />
                <CurveTo cx="40.55" cy="153.50" x="38.10" y="151.10" />
                <CurveTo cx="35.65" cy="148.65" x="32.25" y="148.65" />
                <CurveTo cx="28.85" cy="148.65" x="26.40" y="151.10" />
                <CurveTo cx="23.95" cy="153.50" x="23.95" y="156.95" />
                <CurveTo cx="23.95" cy="160.35" x="26.40" y="162.80" />
                <CurveTo cx="28.85" cy="165.20" x="32.25" y="165.20" />

                <MoveTo x="99.50" y="164.90" />
                <CurveTo cx="102.95" cy="164.90" x="105.40" y="162.45" />
                <CurveTo cx="107.80" cy="160.00" x="107.80" y="156.60" />
                <CurveTo cx="107.80" cy="153.20" x="105.40" y="150.75" />
                <CurveTo cx="102.95" cy="148.35" x="99.50" y="148.35" />
                <CurveTo cx="96.10" cy="148.35" x="93.65" y="150.75" />
                <CurveTo cx="91.25" cy="153.20" x="91.25" y="156.60" />
                <CurveTo cx="91.25" cy="160.00" x="93.65" y="162.45" />
                <CurveTo cx="96.10" cy="164.90" x="99.50" y="164.90" />

                <MoveTo x="20.80" y="170.35" />
                <LineTo x="32.35" y="170.35" />
                <CurveTo cx="35.55" cy="170.35" x="37.80" y="172.55" />
                <CurveTo cx="40.10" cy="174.80" x="40.10" y="177.90" />
                <CurveTo cx="40.10" cy="181.00" x="37.80" y="183.20" />
                <CurveTo cx="35.55" cy="185.40" x="32.35" y="185.40" />
                <LineTo x="20.80" y="185.40" />
                <CurveTo cx="17.60" cy="185.40" x="15.35" y="183.20" />
                <CurveTo cx="13.10" cy="181.00" x="13.10" y="177.90" />
                <CurveTo cx="13.10" cy="174.80" x="15.35" y="172.55" />
                <CurveTo cx="17.60" cy="170.35" x="20.80" y="170.35" />

                <MoveTo x="99.70" y="170.25" />
                <LineTo x="111.35" y="170.25" />
                <CurveTo cx="114.55" cy="170.25" x="116.85" y="172.45" />
                <CurveTo cx="119.15" cy="174.70" x="119.15" y="177.80" />
                <CurveTo cx="119.15" cy="180.90" x="116.85" y="183.15" />
                <CurveTo cx="114.55" cy="185.35" x="111.35" y="185.35" />
                <LineTo x="99.70" y="185.35" />
                <CurveTo cx="96.50" cy="185.35" x="94.20" y="183.15" />
                <CurveTo cx="91.90" cy="180.90" x="91.90" y="177.80" />
                <CurveTo cx="91.90" cy="174.70" x="94.20" y="172.45" />
                <CurveTo cx="96.50" cy="170.25" x="99.70" y="170.25" />

                <MoveTo x="109.45" y="192.30" />
                <LineTo x="95.55" y="192.30" />
                <CurveTo cx="92.00" cy="192.30" x="89.50" y="194.40" />
                <CurveTo cx="86.95" cy="196.50" x="86.95" y="199.40" />
                <CurveTo cx="86.95" cy="202.35" x="89.50" y="204.45" />
                <CurveTo cx="92.00" cy="206.55" x="95.55" y="206.55" />
                <LineTo x="109.45" y="206.55" />
                <CurveTo cx="113.00" cy="206.55" x="115.55" y="204.45" />
                <CurveTo cx="118.05" cy="202.35" x="118.05" y="199.40" />
                <CurveTo cx="118.05" cy="196.45" x="115.55" y="194.40" />
                <CurveTo cx="113.00" cy="192.30" x="109.45" y="192.30" />

                <MoveTo x="21.25" y="192.40" />
                <LineTo x="35.15" y="192.40" />
                <CurveTo cx="38.65" cy="192.40" x="41.20" y="194.50" />
                <CurveTo cx="43.70" cy="196.60" x="43.70" y="199.55" />
                <CurveTo cx="43.70" cy="202.50" x="41.20" y="204.60" />
                <CurveTo cx="38.65" cy="206.70" x="35.15" y="206.70" />
                <LineTo x="21.25" y="206.70" />
                <CurveTo cx="17.70" cy="206.70" x="15.15" y="204.60" />
                <CurveTo cx="12.65" cy="202.50" x="12.65" y="199.55" />
                <CurveTo cx="12.65" cy="196.60" x="15.15" y="194.50" />
                <CurveTo cx="17.70" cy="192.40" x="21.25" y="192.40" />

                <MoveTo x="107.25" y="212.35" />
                <LineTo x="94.30" y="212.35" />
                <CurveTo cx="91.00" cy="212.35" x="88.65" y="214.40" />
                <CurveTo cx="86.30" cy="216.40" x="86.30" y="219.25" />
                <CurveTo cx="86.30" cy="222.10" x="88.65" y="224.15" />
                <CurveTo cx="91.00" cy="226.15" x="94.30" y="226.15" />
                <LineTo x="107.25" y="226.15" />
                <CurveTo cx="110.55" cy="226.15" x="112.90" y="224.15" />
                <CurveTo cx="115.25" cy="222.10" x="115.25" y="219.25" />
                <CurveTo cx="115.25" cy="216.40" x="112.90" y="214.40" />
                <CurveTo cx="110.55" cy="212.35" x="107.25" y="212.35" />

                <MoveTo x="23.45" y="212.45" />
                <LineTo x="36.40" y="212.45" />
                <CurveTo cx="39.70" cy="212.45" x="42.05" y="214.50" />
                <CurveTo cx="44.40" cy="216.55" x="44.40" y="219.35" />
                <CurveTo cx="44.40" cy="222.20" x="42.05" y="224.25" />
                <CurveTo cx="39.70" cy="226.30" x="36.40" y="226.30" />
                <LineTo x="23.45" y="226.30" />
                <CurveTo cx="20.15" cy="226.30" x="17.80" y="224.25" />
                <CurveTo cx="15.40" cy="222.20" x="15.40" y="219.35" />
                <CurveTo cx="15.40" cy="216.55" x="17.80" y="214.50" />
                <CurveTo cx="20.15" cy="212.45" x="23.45" y="212.45" />

                <MoveTo x="104.40" y="232.60" />
                <LineTo x="92.70" y="232.60" />
                <CurveTo cx="89.75" cy="232.60" x="87.65" y="234.55" />
                <CurveTo cx="85.55" cy="236.50" x="85.55" y="239.30" />
                <CurveTo cx="85.55" cy="242.10" x="87.65" y="244.05" />
                <CurveTo cx="89.75" cy="246.05" x="92.70" y="246.05" />
                <LineTo x="104.40" y="246.05" />
                <CurveTo cx="107.35" cy="246.05" x="109.45" y="244.05" />
                <CurveTo cx="111.55" cy="242.10" x="111.55" y="239.30" />
                <CurveTo cx="111.55" cy="236.50" x="109.45" y="234.55" />
                <CurveTo cx="107.35" cy="232.60" x="104.40" y="232.60" />

                <MoveTo x="26.30" y="232.75" />
                <LineTo x="38.00" y="232.75" />
                <CurveTo cx="40.95" cy="232.75" x="43.05" y="234.70" />
                <CurveTo cx="45.15" cy="236.65" x="45.15" y="239.45" />
                <CurveTo cx="45.15" cy="242.25" x="43.05" y="244.20" />
                <CurveTo cx="40.95" cy="246.20" x="38.00" y="246.20" />
                <LineTo x="26.30" y="246.20" />
                <CurveTo cx="23.35" cy="246.20" x="21.25" y="244.20" />
                <CurveTo cx="19.15" cy="242.25" x="19.15" y="239.45" />
                <CurveTo cx="19.15" cy="236.65" x="21.25" y="234.70" />
                <CurveTo cx="23.35" cy="232.75" x="26.30" y="232.75" />

                <MoveTo x="101.70" y="251.65" />
                <LineTo x="90.95" y="251.65" />
                <CurveTo cx="88.20" cy="251.65" x="86.25" y="253.70" />
                <CurveTo cx="84.30" cy="255.75" x="84.30" y="258.60" />
                <CurveTo cx="84.30" cy="261.50" x="86.25" y="263.50" />
                <CurveTo cx="88.20" cy="265.55" x="90.95" y="265.55" />
                <LineTo x="101.70" y="265.55" />
                <CurveTo cx="104.45" cy="265.55" x="106.35" y="263.50" />
                <CurveTo cx="108.30" cy="261.50" x="108.30" y="258.60" />
                <CurveTo cx="108.30" cy="255.75" x="106.35" y="253.70" />
                <CurveTo cx="104.45" cy="251.65" x="101.70" y="251.65" />

                <MoveTo x="29.00" y="251.80" />
                <LineTo x="39.75" y="251.80" />
                <CurveTo cx="42.50" cy="251.80" x="44.45" y="253.85" />
                <CurveTo cx="46.40" cy="255.90" x="46.40" y="258.75" />
                <CurveTo cx="46.40" cy="261.60" x="44.45" y="263.65" />
                <CurveTo cx="42.50" cy="265.70" x="39.75" y="265.70" />
                <LineTo x="29.00" y="265.70" />
                <CurveTo cx="26.25" cy="265.70" x="24.35" y="263.65" />
                <CurveTo cx="22.40" cy="261.60" x="22.40" y="258.75" />
                <CurveTo cx="22.40" cy="255.90" x="24.35" y="253.85" />
                <CurveTo cx="26.25" cy="251.80" x="29.00" y="251.80" />

                <MoveTo x="58.20" y="194.90" />
                <LineTo x="73.45" y="194.90" />
                <CurveTo cx="77.30" cy="194.90" x="80.10" y="196.95" />
                <CurveTo cx="82.85" cy="198.95" x="82.85" y="201.80" />
                <CurveTo cx="82.85" cy="204.70" x="80.10" y="206.70" />
                <CurveTo cx="77.30" cy="208.75" x="73.45" y="208.75" />
                <LineTo x="58.20" y="208.75" />
                <CurveTo cx="54.35" cy="208.75" x="51.60" y="206.70" />
                <CurveTo cx="48.85" cy="204.70" x="48.85" y="201.80" />
                <CurveTo cx="48.85" cy="198.95" x="51.60" y="196.95" />
                <CurveTo cx="54.35" cy="194.90" x="58.20" y="194.90" />

                <MoveTo x="58.35" y="214.15" />
                <LineTo x="72.65" y="214.15" />
                <CurveTo cx="76.30" cy="214.15" x="78.90" y="216.25" />
                <CurveTo cx="81.50" cy="218.35" x="81.50" y="221.30" />
                <CurveTo cx="81.50" cy="224.25" x="78.90" y="226.30" />
                <CurveTo cx="76.30" cy="228.40" x="72.65" y="228.40" />
                <LineTo x="58.35" y="228.40" />
                <CurveTo cx="54.70" cy="228.40" x="52.10" y="226.30" />
                <CurveTo cx="49.50" cy="224.25" x="49.50" y="221.30" />
                <CurveTo cx="49.50" cy="218.35" x="52.10" y="216.25" />
                <CurveTo cx="54.70" cy="214.15" x="58.35" y="214.15" />

                <MoveTo x="58.90" y="233.75" />
                <LineTo x="72.35" y="233.75" />
                <CurveTo cx="75.75" cy="233.75" x="78.20" y="235.85" />
                <CurveTo cx="80.65" cy="237.95" x="80.65" y="240.85" />
                <CurveTo cx="80.65" cy="243.80" x="78.20" y="245.90" />
                <CurveTo cx="75.75" cy="248.00" x="72.35" y="248.00" />
                <LineTo x="58.90" y="248.00" />
                <CurveTo cx="55.50" cy="248.00" x="53.05" y="245.90" />
                <CurveTo cx="50.60" cy="243.80" x="50.60" y="240.85" />
                <CurveTo cx="50.60" cy="237.95" x="53.05" y="235.85" />
                <CurveTo cx="55.50" cy="233.75" x="58.90" y="233.75" />

                <MoveTo x="59.00" y="252.95" />
                <LineTo x="71.55" y="252.95" />
                <CurveTo cx="74.75" cy="252.95" x="77.00" y="254.95" />
                <CurveTo cx="79.30" cy="256.90" x="79.30" y="259.70" />
                <CurveTo cx="79.30" cy="262.50" x="77.00" y="264.45" />
                <CurveTo cx="74.75" cy="266.45" x="71.55" y="266.45" />
                <LineTo x="59.00" y="266.45" />
                <CurveTo cx="55.80" cy="266.45" x="53.55" y="264.45" />
                <CurveTo cx="51.30" cy="262.50" x="51.30" y="259.70" />
                <CurveTo cx="51.30" cy="256.90" x="53.55" y="254.95" />
                <CurveTo cx="55.80" cy="252.95" x="59.00" y="252.95" />

                <MoveTo x="60.10" y="8.85" />
                <LineTo x="70.80" y="8.85" />
                <CurveTo cx="71.40" cy="8.85" x="71.85" y="9.25" />
                <CurveTo cx="72.30" cy="9.70" x="72.15" y="10.20" />
                <LineTo x="71.60" y="12.30" />
                <CurveTo cx="71.45" cy="12.80" x="70.70" y="13.15" />
                <CurveTo cx="70.00" cy="13.50" x="69.40" y="13.50" />
                <LineTo x="61.50" y="13.50" />
                <CurveTo cx="60.90" cy="13.50" x="60.35" y="13.25" />
                <CurveTo cx="59.80" cy="12.95" x="59.60" y="12.45" />
                <LineTo x="58.75" y="10.20" />
                <CurveTo cx="58.55" cy="9.70" x="59.05" y="9.25" />
                <CurveTo cx="59.50" cy="8.85" x="60.10" y="8.85" />

                <MoveTo x="55.60" y="14.15" />
                <CurveTo cx="54.95" cy="13.40" x="56.40" y="13.10" />
                <CurveTo cx="57.85" cy="12.80" x="58.85" y="13.40" />
                <CurveTo cx="59.35" cy="13.75" x="59.90" y="14.20" />
                <LineTo x="60.30" y="14.55" />
                <LineTo x="60.60" y="14.85" />
                <CurveTo cx="61.00" cy="15.25" x="61.80" y="15.25" />
                <LineTo x="69.45" y="15.20" />
                <CurveTo cx="70.55" cy="15.25" x="71.15" y="14.60" />
                <LineTo x="72.00" y="14.05" />
                <CurveTo cx="73.10" cy="12.80" x="74.80" y="13.10" />
                <CurveTo cx="76.50" cy="13.35" x="75.75" y="14.25" />
                <LineTo x="73.35" y="17.20" />
                <CurveTo cx="72.65" cy="18.25" x="71.50" y="18.15" />
                <LineTo x="60.65" y="18.10" />
                <CurveTo cx="58.95" cy="18.25" x="58.30" y="17.10" />
                <LineTo x="55.60" y="14.15" />

                <MoveTo x="62.70" y="282.90" />
                <LineTo x="68.75" y="282.90" />
                <CurveTo cx="69.30" cy="282.90" x="69.70" y="283.35" />
                <CurveTo cx="70.10" cy="283.85" x="70.10" y="284.50" />
                <CurveTo cx="70.10" cy="285.20" x="69.70" y="285.65" />
                <CurveTo cx="69.30" cy="286.15" x="68.75" y="286.15" />
                <LineTo x="62.70" y="286.15" />
                <CurveTo cx="62.15" cy="286.15" x="61.75" y="285.65" />
                <CurveTo cx="61.35" cy="285.20" x="61.35" y="284.50" />
                <CurveTo cx="61.35" cy="283.85" x="61.75" y="283.35" />
                <CurveTo cx="62.15" cy="282.90" x="62.70" y="282.90" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (6, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 6
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="20.35" y="1.45" />
                <CurveTo cx="30.65" cy="7.85" x="52.25" y="7.50" />
                <CurveTo cx="74.05" cy="7.10" x="81.35" y="0.00" />
                <CurveTo cx="92.85" cy="6.20" x="97.35" y="14.50" />
                <CurveTo cx="100.80" cy="20.90" x="101.95" y="33.45" />
                <CurveTo cx="102.00" cy="35.70" x="102.35" y="43.55" />
                <CurveTo cx="103.10" cy="60.55" x="103.35" y="71.50" />
                <CurveTo cx="103.85" cy="90.85" x="103.50" y="106.50" />
                <CurveTo cx="103.00" cy="126.40" x="101.15" y="140.50" />
                <CurveTo cx="98.90" cy="157.20" x="94.75" y="165.55" />
                <CurveTo cx="93.05" cy="168.45" x="87.80" y="171.90" />
                <CurveTo cx="83.20" cy="175.00" x="80.15" y="175.90" />
                <CurveTo cx="66.25" cy="180.25" x="51.25" y="180.35" />
                <CurveTo cx="36.90" cy="180.45" x="23.35" y="176.60" />
                <CurveTo cx="19.30" cy="175.45" x="16.30" y="173.60" />
                <CurveTo cx="12.15" cy="170.95" x="10.25" y="166.90" />
                <CurveTo cx="6.25" cy="158.25" x="3.80" y="142.10" />
                <CurveTo cx="1.70" cy="128.35" x="0.80" y="109.25" />
                <CurveTo cx="0.05" cy="94.20" x="0.00" y="75.45" />
                <CurveTo cx="0.00" cy="64.80" x="0.25" y="47.70" />
                <CurveTo cx="0.40" cy="37.65" x="0.35" y="35.00" />
                <CurveTo cx="0.45" cy="23.20" x="4.40" y="16.00" />
                <CurveTo cx="8.95" cy="7.75" x="20.35" y="1.45" />

                <MoveTo x="26.35" y="18.50" />
                <CurveTo cx="39.65" cy="17.25" x="51.45" y="17.15" />
                <CurveTo cx="64.55" cy="17.10" x="76.55" y="18.50" />
                <CurveTo cx="81.65" cy="19.05" x="83.70" y="24.65" />
                <LineTo x="85.25" y="28.85" />
                <CurveTo cx="86.00" cy="31.00" x="82.75" y="32.45" />
                <CurveTo cx="80.15" cy="33.55" x="76.55" y="33.80" />
                <CurveTo cx="63.40" cy="34.65" x="51.70" y="34.80" />
                <CurveTo cx="38.90" cy="35.00" x="26.80" y="34.40" />
                <CurveTo cx="23.55" cy="34.25" x="20.70" y="32.55" />
                <CurveTo cx="17.45" cy="30.65" x="18.20" y="28.45" />
                <LineTo x="19.55" y="24.65" />
                <CurveTo cx="21.50" cy="18.95" x="26.35" y="18.50" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (7, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 7
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="115.15" y="13.10" />
                <CurveTo cx="123.10" cy="58.75" x="123.95" y="98.45" />
                <CurveTo cx="124.85" cy="142.30" x="117.25" y="181.80" />
                <CurveTo cx="115.50" cy="182.20" x="112.15" y="182.85" />
                <CurveTo cx="102.25" cy="184.80" x="96.50" y="187.10" />
                <CurveTo cx="86.85" cy="190.90" x="75.60" y="199.35" />
                <CurveTo cx="70.65" cy="202.20" x="61.95" y="202.00" />
                <CurveTo cx="53.60" cy="201.80" x="48.65" y="199.20" />
                <CurveTo cx="36.00" cy="189.90" x="24.75" y="185.95" />
                <CurveTo cx="18.50" cy="183.75" x="11.20" y="183.05" />
                <CurveTo cx="7.80" cy="182.70" x="7.50" y="181.95" />
                <CurveTo cx="0.00" cy="141.30" x="1.60" y="96.35" />
                <CurveTo cx="3.05" cy="54.85" x="12.20" y="10.75" />
                <CurveTo cx="12.00" cy="6.35" x="28.00" y="3.30" />
                <CurveTo cx="42.95" cy="0.40" x="63.30" y="0.20" />
                <CurveTo cx="84.65" cy="0.00" x="98.75" y="3.00" />
                <CurveTo cx="114.95" cy="6.45" x="115.15" y="13.10" />
                <MoveTo x="12.95" y="197.80" />
                <LineTo x="21.75" y="200.45" />
                <CurveTo cx="27.35" cy="202.15" x="38.10" y="208.05" />
                <CurveTo cx="46.10" cy="212.40" x="50.05" y="214.00" />
                <CurveTo cx="56.65" cy="216.60" x="61.95" y="216.35" />
                <CurveTo cx="66.50" cy="216.15" x="73.05" y="213.50" />
                <CurveTo cx="77.00" cy="211.95" x="85.25" y="207.85" />
                <CurveTo cx="98.35" cy="201.35" x="104.30" y="199.95" />
                <LineTo x="112.70" y="197.95" />
                <CurveTo cx="115.05" cy="197.45" x="114.95" y="203.30" />
                <LineTo x="113.50" y="305.95" />
                <CurveTo cx="113.35" cy="316.05" x="111.25" y="316.30" />
                <LineTo x="100.30" y="317.45" />
                <LineTo x="100.00" y="303.60" />
                <CurveTo cx="95.95" cy="303.40" x="87.25" y="302.75" />
                <CurveTo cx="77.20" cy="302.00" x="72.20" y="301.75" />
                <CurveTo cx="63.85" cy="301.40" x="57.80" y="301.70" />
                <LineTo x="22.25" y="303.55" />
                <LineTo x="22.50" y="317.30" />
                <LineTo x="13.75" y="316.30" />
                <CurveTo cx="12.55" cy="316.15" x="11.20" y="312.30" />
                <CurveTo cx="10.05" cy="308.90" x="10.00" y="307.10" />
                <LineTo x="8.75" y="201.25" />
                <CurveTo cx="8.75" cy="199.85" x="10.20" y="198.65" />
                <CurveTo cx="11.65" cy="197.40" x="12.95" y="197.80" />

                <MoveTo x="62.95" y="199.55" />
                <CurveTo cx="73.90" cy="199.55" x="81.70" y="192.90" />
                <CurveTo cx="89.50" cy="186.20" x="89.50" y="176.80" />
                <CurveTo cx="89.50" cy="167.40" x="81.70" y="160.70" />
                <CurveTo cx="73.90" cy="154.00" x="62.95" y="154.00" />
                <CurveTo cx="51.95" cy="154.00" x="44.20" y="160.70" />
                <CurveTo cx="36.40" cy="167.40" x="36.40" y="176.80" />
                <CurveTo cx="36.40" cy="186.20" x="44.20" y="192.90" />
                <CurveTo cx="51.95" cy="199.55" x="62.95" y="199.55" />

                <MoveTo x="38.45" y="154.50" />
                <LineTo x="22.20" y="155.30" />
                <CurveTo cx="14.35" cy="155.65" x="13.30" y="162.10" />
                <CurveTo cx="12.15" cy="169.40" x="23.30" y="178.20" />
                <CurveTo cx="25.45" cy="179.45" x="26.80" y="179.75" />
                <CurveTo cx="28.65" cy="180.15" x="29.35" y="178.95" />
                <CurveTo cx="31.45" cy="162.00" x="39.85" y="158.25" />
                <CurveTo cx="41.70" cy="154.75" x="38.45" y="154.50" />

                <MoveTo x="86.65" y="154.20" />
                <LineTo x="102.95" y="155.00" />
                <CurveTo cx="110.80" cy="155.35" x="111.85" y="161.75" />
                <CurveTo cx="113.00" cy="169.05" x="101.85" y="177.90" />
                <CurveTo cx="99.75" cy="179.15" x="98.30" y="179.45" />
                <CurveTo cx="96.45" cy="179.85" x="95.80" y="178.65" />
                <CurveTo cx="93.60" cy="161.65" x="85.30" y="157.95" />
                <CurveTo cx="83.35" cy="154.45" x="86.65" y="154.20" />

                <MoveTo x="23.80" y="59.65" />
                <CurveTo cx="64.85" cy="57.55" x="102.35" y="59.65" />
                <CurveTo cx="105.80" cy="59.85" x="105.80" y="63.00" />
                <LineTo x="105.80" y="126.75" />
                <CurveTo cx="105.80" cy="129.85" x="102.35" y="130.10" />
                <CurveTo cx="64.70" cy="132.90" x="23.80" y="130.10" />
                <CurveTo cx="20.40" cy="129.80" x="20.40" y="126.75" />
                <LineTo x="20.40" y="63.00" />
                <CurveTo cx="20.40" cy="59.85" x="23.80" y="59.65" />

                <MoveTo x="60.20" y="18.65" />
                <CurveTo cx="64.05" cy="17.70" x="67.85" y="18.65" />
                <CurveTo cx="68.40" cy="18.80" x="68.70" y="19.10" />
                <CurveTo cx="69.10" cy="19.40" x="69.10" y="19.90" />
                <CurveTo cx="69.10" cy="20.45" x="68.70" y="20.85" />
                <CurveTo cx="68.30" cy="21.25" x="67.85" y="21.15" />
                <CurveTo cx="64.05" cy="20.10" x="60.20" y="21.15" />
                <CurveTo cx="59.75" cy="21.25" x="59.35" y="20.85" />
                <CurveTo cx="58.95" cy="20.45" x="58.95" y="19.90" />
                <CurveTo cx="58.95" cy="19.40" x="59.35" y="19.10" />
                <CurveTo cx="59.65" cy="18.80" x="60.20" y="18.65" />

                <MoveTo x="54.05" y="32.90" />
                <CurveTo cx="53.15" cy="29.10" x="54.20" y="25.25" />
                <CurveTo cx="54.30" cy="24.70" x="54.60" y="24.40" />
                <CurveTo cx="54.95" cy="24.05" x="55.45" y="24.05" />
                <CurveTo cx="55.95" cy="24.05" x="56.35" y="24.45" />
                <CurveTo cx="56.80" cy="24.85" x="56.65" y="25.30" />
                <CurveTo cx="55.60" cy="29.15" x="56.55" y="32.95" />
                <CurveTo cx="56.65" cy="33.40" x="56.25" y="33.80" />
                <CurveTo cx="55.80" cy="34.20" x="55.30" y="34.20" />
                <CurveTo cx="54.80" cy="34.15" x="54.45" y="33.80" />
                <CurveTo cx="54.15" cy="33.45" x="54.05" y="32.90" />

                <MoveTo x="74.60" y="32.90" />
                <CurveTo cx="75.50" cy="29.00" x="74.45" y="25.25" />
                <CurveTo cx="74.35" cy="24.70" x="74.05" y="24.35" />
                <CurveTo cx="73.70" cy="24.00" x="73.20" y="24.00" />
                <CurveTo cx="72.70" cy="24.00" x="72.30" y="24.40" />
                <CurveTo cx="71.85" cy="24.80" x="72.00" y="25.25" />
                <CurveTo cx="73.05" cy="29.10" x="72.10" y="32.90" />
                <CurveTo cx="72.00" cy="33.35" x="72.45" y="33.75" />
                <CurveTo cx="72.85" cy="34.15" x="73.35" y="34.15" />
                <CurveTo cx="73.85" cy="34.10" x="74.20" y="33.75" />
                <CurveTo cx="74.50" cy="33.45" x="74.60" y="32.90" />

                <MoveTo x="60.00" y="38.95" />
                <CurveTo cx="63.85" cy="40.00" x="67.65" y="38.95" />
                <CurveTo cx="68.20" cy="38.85" x="68.55" y="38.55" />
                <CurveTo cx="68.90" cy="38.20" x="68.90" y="37.75" />
                <CurveTo cx="68.90" cy="37.20" x="68.55" y="36.80" />
                <CurveTo cx="68.15" cy="36.40" x="67.65" y="36.50" />
                <CurveTo cx="63.85" cy="37.50" x="60.00" y="36.50" />
                <CurveTo cx="59.55" cy="36.40" x="59.15" y="36.80" />
                <CurveTo cx="58.80" cy="37.20" x="58.80" y="37.75" />
                <CurveTo cx="58.80" cy="38.20" x="59.15" y="38.55" />
                <CurveTo cx="59.45" cy="38.85" x="60.00" y="38.95" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (8, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 8
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="9.05" y="0.00" />
                <LineTo x="94.85" y="0.00" />
                <CurveTo cx="103.90" cy="0.00" x="103.70" y="8.85" />
                <LineTo x="100.45" y="171.15" />
                <CurveTo cx="100.25" cy="180.00" x="94.85" y="180.00" />
                <LineTo x="9.05" y="180.00" />
                <CurveTo cx="5.70" cy="180.00" x="4.25" y="177.00" />
                <CurveTo cx="3.10" cy="174.70" x="3.00" y="170.35" />
                <LineTo x="0.20" y="8.85" />
                <CurveTo cx="0.00" cy="0.00" x="9.05" y="0.00" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (9, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 9
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 3" x="50" y="420">
            <Contour x="0" y="0">
                <MoveTo x="102.30" y="0.95" />
                <CurveTo cx="76.15" cy="5.30" x="52.65" y="5.40" />
                <CurveTo cx="27.05" cy="5.45" x="2.95" y="0.55" />
                <CurveTo cx="0.00" cy="0.00" x="0.00" y="3.30" />
                <LineTo x="0.00" y="97.85" />
                <CurveTo cx="0.00" cy="99.75" x="1.90" y="100.30" />
                <CurveTo cx="2.85" cy="100.60" x="8.40" y="98.85" />
                <CurveTo cx="12.35" cy="97.65" x="15.45" y="96.45" />
                <CurveTo cx="20.30" cy="94.60" x="28.80" y="89.65" />
                <CurveTo cx="36.80" cy="84.95" x="41.15" y="83.30" />
                <CurveTo cx="48.30" cy="80.55" x="55.60" y="81.10" />
                <CurveTo cx="61.15" cy="81.50" x="66.75" y="84.10" />
                <CurveTo cx="70.15" cy="85.60" x="76.65" y="89.65" />
                <CurveTo cx="83.55" cy="93.95" x="87.55" y="95.80" />
                <LineTo x="87.95" y="96.00" />
                <CurveTo cx="92.30" cy="98.15" x="94.20" y="98.85" />
                <CurveTo cx="97.10" cy="99.85" x="100.55" y="100.05" />
                <CurveTo cx="102.05" cy="100.15" x="103.50" y="99.75" />
                <CurveTo cx="105.25" cy="99.20" x="105.25" y="98.20" />
                <LineTo x="105.25" y="3.65" />
                <CurveTo cx="105.25" cy="0.40" x="102.30" y="0.95" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (10, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 10
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 4" x="300" y="420">
            <Contour x="0" y="0">
                <MoveTo x="2.30" y="0.00" />
                <LineTo x="2.50" y="0.00" />
                <CurveTo cx="11.00" cy="0.80" x="15.15" y="1.80" />
                <CurveTo cx="22.00" cy="3.40" x="27.25" y="7.05" />
                <CurveTo cx="44.00" cy="18.70" x="57.15" y="18.15" />
                <CurveTo cx="67.80" cy="17.65" x="79.90" y="8.40" />
                <CurveTo cx="86.00" cy="3.70" x="93.00" y="1.85" />
                <CurveTo cx="98.60" cy="0.40" x="106.65" y="0.45" />
                <CurveTo cx="107.25" cy="16.60" x="107.55" y="26.85" />
                <CurveTo cx="108.00" cy="41.35" x="108.05" y="53.35" />
                <CurveTo cx="108.20" cy="82.65" x="106.20" y="106.20" />
                <LineTo x="95.45" y="106.15" />
                <LineTo x="95.45" y="104.15" />
                <CurveTo cx="75.60" cy="101.20" x="54.15" y="100.95" />
                <CurveTo cx="31.65" cy="100.70" x="12.85" y="103.55" />
                <LineTo x="12.85" y="105.40" />
                <LineTo x="3.00" y="105.40" />
                <CurveTo cx="0.15" cy="79.75" x="0.10" y="52.95" />
                <CurveTo cx="0.00" cy="28.05" x="2.30" y="0.00" />

                <MoveTo x="54.30" y="36.70" />
                <CurveTo cx="60.60" cy="36.70" x="65.10" y="34.40" />
                <CurveTo cx="69.55" cy="32.05" x="69.55" y="28.75" />
                <CurveTo cx="69.55" cy="25.50" x="65.10" y="23.15" />
                <CurveTo cx="60.60" cy="20.85" x="54.30" y="20.85" />
                <CurveTo cx="48.00" cy="20.85" x="43.55" y="23.15" />
                <CurveTo cx="39.05" cy="25.50" x="39.05" y="28.75" />
                <CurveTo cx="39.05" cy="32.05" x="43.55" y="34.40" />
                <CurveTo cx="48.00" cy="36.70" x="54.30" y="36.70" />

                <MoveTo x="18.80" y="73.50" />
                <CurveTo cx="24.90" cy="73.50" x="29.25" y="71.25" />
                <CurveTo cx="33.60" cy="69.00" x="33.60" y="65.85" />
                <CurveTo cx="33.60" cy="62.65" x="29.25" y="60.40" />
                <CurveTo cx="24.90" cy="58.15" x="18.80" y="58.15" />
                <CurveTo cx="12.70" cy="58.15" x="8.35" y="60.40" />
                <CurveTo cx="4.00" cy="62.65" x="4.00" y="65.85" />
                <CurveTo cx="4.00" cy="69.00" x="8.35" y="71.25" />
                <CurveTo cx="12.70" cy="73.50" x="18.80" y="73.50" />

                <MoveTo x="91.85" y="74.00" />
                <CurveTo cx="97.95" cy="74.00" x="102.30" y="71.75" />
                <CurveTo cx="106.60" cy="69.50" x="106.60" y="66.35" />
                <CurveTo cx="106.60" cy="63.15" x="102.30" y="60.90" />
                <CurveTo cx="97.95" cy="58.65" x="91.85" y="58.65" />
                <CurveTo cx="85.75" cy="58.65" x="81.40" y="60.90" />
                <CurveTo cx="77.05" cy="63.15" x="77.05" y="66.35" />
                <CurveTo cx="77.05" cy="69.50" x="81.40" y="71.75" />
                <CurveTo cx="85.75" cy="74.00" x="91.85" y="74.00" />

                <MoveTo x="21.60" y="11.95" />
                <CurveTo cx="27.65" cy="13.50" x="31.30" y="16.70" />
                <CurveTo cx="34.90" cy="19.85" x="34.05" y="22.80" />
                <CurveTo cx="33.15" cy="25.75" x="28.25" y="26.70" />
                <CurveTo cx="23.40" cy="27.60" x="17.35" y="26.05" />
                <CurveTo cx="11.35" cy="24.45" x="7.70" y="21.30" />
                <CurveTo cx="4.10" cy="18.10" x="4.95" y="15.20" />
                <CurveTo cx="5.85" cy="12.30" x="10.75" y="11.35" />
                <CurveTo cx="15.60" cy="10.35" x="21.60" y="11.95" />

                <MoveTo x="89.00" y="12.45" />
                <CurveTo cx="83.00" cy="14.05" x="79.35" y="17.20" />
                <CurveTo cx="75.75" cy="20.40" x="76.60" y="23.30" />
                <CurveTo cx="77.45" cy="26.20" x="82.35" y="27.20" />
                <CurveTo cx="87.25" cy="28.15" x="93.25" y="26.55" />
                <CurveTo cx="99.30" cy="24.95" x="102.90" y="21.80" />
                <CurveTo cx="106.55" cy="18.60" x="105.65" y="15.70" />
                <CurveTo cx="104.80" cy="12.80" x="99.90" y="11.85" />
                <CurveTo cx="95.05" cy="10.85" x="89.00" y="12.45" />

                <MoveTo x="20.80" y="96.80" />
                <CurveTo cx="26.80" cy="95.95" x="30.75" y="93.25" />
                <CurveTo cx="34.65" cy="90.55" x="34.20" y="87.50" />
                <CurveTo cx="33.75" cy="84.50" x="29.20" y="82.95" />
                <CurveTo cx="24.60" cy="81.40" x="18.65" y="82.25" />
                <CurveTo cx="12.65" cy="83.05" x="8.75" y="85.80" />
                <CurveTo cx="4.80" cy="88.55" x="5.25" y="91.55" />
                <CurveTo cx="5.70" cy="94.60" x="10.25" y="96.15" />
                <CurveTo cx="14.80" cy="97.65" x="20.80" y="96.80" />

                <MoveTo x="89.85" y="97.30" />
                <CurveTo cx="83.85" cy="96.50" x="79.95" y="93.75" />
                <CurveTo cx="76.00" cy="91.00" x="76.45" y="88.00" />
                <CurveTo cx="76.90" cy="84.95" x="81.45" y="83.45" />
                <CurveTo cx="86.00" cy="81.90" x="92.00" y="82.75" />
                <CurveTo cx="98.00" cy="83.60" x="101.95" y="86.30" />
                <CurveTo cx="105.85" cy="89.00" x="105.40" y="92.05" />
                <CurveTo cx="104.95" cy="95.05" x="100.40" y="96.60" />
                <CurveTo cx="95.80" cy="98.15" x="89.85" y="97.30" />

                <MoveTo x="19.80" y="35.05" />
                <CurveTo cx="25.80" cy="35.70" x="29.85" y="38.35" />
                <CurveTo cx="33.85" cy="41.00" x="33.50" y="44.05" />
                <CurveTo cx="33.10" cy="47.10" x="28.60" y="48.80" />
                <CurveTo cx="24.10" cy="50.45" x="18.10" y="49.80" />
                <CurveTo cx="12.05" cy="49.10" x="8.05" y="46.50" />
                <CurveTo cx="4.10" cy="43.85" x="4.45" y="40.80" />
                <CurveTo cx="4.80" cy="37.75" x="9.30" y="36.05" />
                <CurveTo cx="13.80" cy="34.40" x="19.80" y="35.05" />

                <MoveTo x="90.80" y="35.55" />
                <CurveTo cx="84.80" cy="36.25" x="80.80" y="38.85" />
                <CurveTo cx="76.80" cy="41.50" x="77.15" y="44.55" />
                <CurveTo cx="77.55" cy="47.60" x="82.00" y="49.30" />
                <CurveTo cx="86.55" cy="50.95" x="92.55" y="50.30" />
                <CurveTo cx="98.55" cy="49.65" x="102.55" y="47.00" />
                <CurveTo cx="106.55" cy="44.35" x="106.20" y="41.30" />
                <CurveTo cx="105.80" cy="38.25" x="101.35" y="36.55" />
                <CurveTo cx="96.80" cy="34.90" x="90.80" y="35.55" />

                <MoveTo x="54.45" y="97.40" />
                <CurveTo cx="60.75" cy="97.40" x="65.25" y="95.10" />
                <CurveTo cx="69.70" cy="92.75" x="69.70" y="89.50" />
                <CurveTo cx="69.70" cy="86.25" x="65.25" y="83.90" />
                <CurveTo cx="60.75" cy="81.60" x="54.45" y="81.60" />
                <CurveTo cx="48.15" cy="81.60" x="43.70" y="83.90" />
                <CurveTo cx="39.20" cy="86.25" x="39.20" y="89.50" />
                <CurveTo cx="39.20" cy="92.75" x="43.70" y="95.10" />
                <CurveTo cx="48.15" cy="97.40" x="54.45" y="97.40" />

                <MoveTo x="54.30" y="76.90" />
                <CurveTo cx="60.60" cy="76.90" x="65.10" y="74.60" />
                <CurveTo cx="69.55" cy="72.25" x="69.55" y="69.00" />
                <CurveTo cx="69.55" cy="65.75" x="65.10" y="63.40" />
                <CurveTo cx="60.60" cy="61.10" x="54.30" y="61.10" />
                <CurveTo cx="48.00" cy="61.10" x="43.55" y="63.40" />
                <CurveTo cx="39.05" cy="65.75" x="39.05" y="69.00" />
                <CurveTo cx="39.05" cy="72.25" x="43.55" y="74.60" />
                <CurveTo cx="48.00" cy="76.90" x="54.30" y="76.90" />

                <MoveTo x="54.45" y="56.60" />
                <CurveTo cx="60.75" cy="56.60" x="65.25" y="54.30" />
                <CurveTo cx="69.70" cy="51.95" x="69.70" y="48.70" />
                <CurveTo cx="69.70" cy="45.45" x="65.25" y="43.10" />
                <CurveTo cx="60.75" cy="40.80" x="54.45" y="40.80" />
                <CurveTo cx="48.15" cy="40.80" x="43.70" y="43.10" />
                <CurveTo cx="39.20" cy="45.45" x="39.20" y="48.70" />
                <CurveTo cx="39.20" cy="51.95" x="43.70" y="54.30" />
                <CurveTo cx="48.15" cy="56.60" x="54.45" y="56.60" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (11, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 11
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="11.10" y="5.65" />
                <CurveTo cx="34.70" cy="0.00" x="60.95" y="0.15" />
                <CurveTo cx="87.55" cy="0.25" x="112.85" y="6.30" />
                <CurveTo cx="119.10" cy="7.75" x="119.40" y="12.05" />
                <CurveTo cx="121.00" cy="34.05" x="121.45" y="58.15" />
                <CurveTo cx="121.85" cy="80.30" x="121.30" y="104.70" />
                <CurveTo cx="120.80" cy="126.60" x="119.50" y="151.05" />
                <CurveTo cx="118.40" cy="172.00" x="116.60" y="196.50" />
                <CurveTo cx="116.60" cy="199.90" x="114.95" y="203.65" />
                <CurveTo cx="113.05" cy="207.95" x="110.50" y="208.50" />
                <CurveTo cx="63.85" cy="218.65" x="11.55" y="209.05" />
                <CurveTo cx="9.00" cy="208.55" x="7.00" y="204.00" />
                <CurveTo cx="5.20" cy="199.90" x="5.20" y="196.50" />
                <CurveTo cx="0.60" cy="150.25" x="0.30" y="103.90" />
                <CurveTo cx="0.00" cy="55.60" x="4.40" y="11.30" />
                <CurveTo cx="4.85" cy="7.10" x="11.10" y="5.65" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (12, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 12
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="4.75" y="0.00" />
                <LineTo x="98.70" y="0.00" />
                <CurveTo cx="103.45" cy="0.00" x="103.45" y="4.75" />
                <LineTo x="103.45" y="152.55" />
                <CurveTo cx="103.45" cy="157.40" x="98.70" y="158.35" />
                <CurveTo cx="50.15" cy="167.05" x="4.75" y="158.85" />
                <CurveTo cx="0.00" cy="158.00" x="0.00" y="152.55" />
                <LineTo x="0.00" y="4.75" />
                <CurveTo cx="0.00" cy="0.00" x="4.75" y="0.00" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (13, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 13
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 3" x="50" y="420">
            <Contour x="0" y="0">
                <MoveTo x="17.05" y="199.50" />
                <LineTo x="24.30" y="199.75" />
                <LineTo x="24.20" y="216.15" />
                <LineTo x="95.15" y="216.00" />
                <LineTo x="95.15" y="199.65" />
                <LineTo x="102.80" y="199.30" />
                <CurveTo cx="114.15" cy="198.80" x="115.20" y="189.40" />
                <CurveTo cx="119.90" cy="146.95" x="119.00" y="107.20" />
                <CurveTo cx="118.15" cy="69.85" x="112.00" y="25.15" />
                <LineTo x="111.85" y="24.10" />
                <CurveTo cx="111.20" cy="19.30" x="110.35" y="15.95" />
                <CurveTo cx="103.40" cy="16.15" x="101.90" y="5.80" />
                <CurveTo cx="82.80" cy="0.00" x="59.65" y="0.05" />
                <CurveTo cx="36.40" cy="0.10" x="17.10" y="6.10" />
                <CurveTo cx="16.95" cy="11.15" x="15.10" y="14.10" />
                <CurveTo cx="12.95" cy="17.45" x="8.95" y="17.25" />
                <CurveTo cx="8.30" cy="19.60" x="8.10" y="22.35" />
                <CurveTo cx="8.00" cy="24.15" x="7.80" y="25.45" />
                <CurveTo cx="1.45" cy="70.20" x="0.75" y="108.05" />
                <CurveTo cx="0.00" cy="147.30" x="5.00" y="189.70" />
                <CurveTo cx="6.10" cy="199.10" x="17.05" y="199.50" />

                <MoveTo x="24.20" y="71.80" />
                <LineTo x="95.95" y="71.80" />
                <CurveTo cx="100.10" cy="71.80" x="100.10" y="76.25" />
                <LineTo x="100.10" y="161.85" />
                <CurveTo cx="100.10" cy="166.30" x="95.95" y="166.30" />
                <LineTo x="24.20" y="166.30" />
                <CurveTo cx="20.00" cy="166.30" x="20.00" y="161.85" />
                <LineTo x="20.00" y="76.25" />
                <CurveTo cx="20.00" cy="71.80" x="24.20" y="71.80" />

                <MoveTo x="56.10" y="32.45" />
                <CurveTo cx="60.20" cy="35.00" x="64.50" y="32.45" />
                <LineTo x="64.50" y="36.00" />
                <CurveTo cx="62.35" cy="37.20" x="60.15" y="37.15" />
                <CurveTo cx="58.20" cy="37.05" x="56.10" y="36.00" />
                <LineTo x="56.10" y="32.45" />

                <MoveTo x="53.50" y="21.30" />
                <CurveTo cx="50.90" cy="25.35" x="53.50" y="29.70" />
                <LineTo x="49.95" y="29.70" />
                <CurveTo cx="48.75" cy="27.55" x="48.80" y="25.35" />
                <CurveTo cx="48.85" cy="23.40" x="49.95" y="21.25" />
                <LineTo x="53.50" y="21.30" />

                <MoveTo x="66.90" y="30.20" />
                <CurveTo cx="69.75" cy="26.35" x="67.50" y="21.80" />
                <LineTo x="71.05" y="22.05" />
                <CurveTo cx="72.15" cy="24.30" x="71.90" y="26.50" />
                <CurveTo cx="71.70" cy="28.45" x="70.45" y="30.45" />
                <LineTo x="66.90" y="30.20" />

                <MoveTo x="56.05" y="18.75" />
                <CurveTo cx="60.15" cy="16.20" x="64.50" y="18.75" />
                <LineTo x="64.50" y="15.20" />
                <CurveTo cx="62.35" cy="14.00" x="60.15" y="14.05" />
                <CurveTo cx="58.20" cy="14.10" x="56.05" y="15.20" />
                <LineTo x="56.05" y="18.75" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (14, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 14
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 4" x="300" y="420">
            <Contour x="0" y="0">
                <MoveTo x="3.65" y="8.00" />
                <LineTo x="24.15" y="9.35" />
                <LineTo x="24.15" y="0.00" />
                <LineTo x="93.60" y="0.00" />
                <LineTo x="93.60" y="9.35" />
                <LineTo x="114.35" y="8.00" />
                <CurveTo cx="118.05" cy="50.50" x="116.95" y="99.80" />
                <CurveTo cx="116.45" cy="121.65" x="115.05" y="145.80" />
                <CurveTo cx="113.75" cy="167.15" x="111.65" y="191.60" />
                <CurveTo cx="111.65" cy="198.80" x="102.60" y="200.50" />
                <CurveTo cx="77.85" cy="205.15" x="59.70" y="205.10" />
                <CurveTo cx="44.05" cy="205.05" x="16.75" y="201.00" />
                <CurveTo cx="12.60" cy="200.40" x="9.75" y="198.35" />
                <CurveTo cx="6.45" cy="195.95" x="6.45" y="192.45" />
                <CurveTo cx="4.35" cy="166.75" x="3.15" y="146.00" />
                <CurveTo cx="1.70" cy="121.45" x="1.15" y="99.95" />
                <CurveTo cx="0.00" cy="50.50" x="3.65" y="8.00" />

                <MoveTo x="25.90" y="15.95" />
                <CurveTo cx="33.15" cy="15.15" x="38.30" y="16.05" />
                <CurveTo cx="43.45" cy="16.90" x="43.45" y="18.95" />
                <CurveTo cx="43.45" cy="20.95" x="38.30" y="22.95" />
                <CurveTo cx="33.20" cy="24.90" x="25.90" y="25.70" />
                <CurveTo cx="18.65" cy="26.45" x="13.55" y="25.55" />
                <CurveTo cx="8.40" cy="24.65" x="8.40" y="22.65" />
                <CurveTo cx="8.40" cy="20.65" x="13.55" y="18.70" />
                <CurveTo cx="18.70" cy="16.70" x="25.90" y="15.95" />

                <MoveTo x="92.55" y="15.95" />
                <CurveTo cx="85.30" cy="15.20" x="80.20" y="16.10" />
                <CurveTo cx="75.05" cy="16.95" x="75.05" y="18.95" />
                <CurveTo cx="75.05" cy="20.95" x="80.20" y="22.95" />
                <CurveTo cx="85.30" cy="24.90" x="92.55" y="25.70" />
                <CurveTo cx="99.80" cy="26.50" x="104.95" y="25.60" />
                <CurveTo cx="110.10" cy="24.75" x="110.10" y="22.70" />
                <CurveTo cx="110.10" cy="20.70" x="104.95" y="18.70" />
                <CurveTo cx="99.85" cy="16.75" x="92.55" y="15.95" />

                <MoveTo x="48.30" y="30.75" />
                <CurveTo cx="48.90" cy="26.60" x="51.05" y="21.70" />
                <CurveTo cx="53.20" cy="16.80" x="55.75" y="13.75" />
                <CurveTo cx="59.30" cy="9.50" x="62.85" y="13.75" />
                <CurveTo cx="65.80" cy="17.25" x="67.75" y="22.00" />
                <CurveTo cx="69.45" cy="26.15" x="70.05" y="30.30" />
                <CurveTo cx="70.70" cy="32.20" x="67.40" y="33.30" />
                <CurveTo cx="64.50" cy="34.25" x="59.80" y="34.25" />
                <CurveTo cx="55.20" cy="34.25" x="51.90" y="33.35" />
                <CurveTo cx="48.25" cy="32.35" x="48.30" y="30.75" />

                <MoveTo x="76.20" y="37.10" />
                <CurveTo cx="80.40" cy="37.65" x="85.30" y="39.75" />
                <CurveTo cx="90.30" cy="41.85" x="93.30" y="44.35" />
                <CurveTo cx="97.55" cy="47.90" x="93.40" y="51.50" />
                <CurveTo cx="89.95" cy="54.45" x="85.20" y="56.45" />
                <CurveTo cx="81.05" cy="58.25" x="76.90" y="58.85" />
                <CurveTo cx="75.00" cy="59.50" x="73.90" y="56.20" />
                <CurveTo cx="72.90" cy="53.30" x="72.85" y="48.65" />
                <CurveTo cx="72.80" cy="44.05" x="73.65" y="40.70" />
                <CurveTo cx="74.60" cy="37.05" x="76.20" y="37.10" />

                <MoveTo x="48.20" y="65.30" />
                <CurveTo cx="48.75" cy="69.45" x="50.95" y="74.40" />
                <CurveTo cx="53.05" cy="79.30" x="55.60" y="82.30" />
                <CurveTo cx="59.20" cy="86.60" x="62.75" y="82.30" />
                <CurveTo cx="65.70" cy="78.80" x="67.65" y="74.10" />
                <CurveTo cx="69.35" cy="69.90" x="69.90" y="65.80" />
                <CurveTo cx="70.55" cy="63.90" x="67.25" y="62.80" />
                <CurveTo cx="64.35" cy="61.85" x="59.65" y="61.85" />
                <CurveTo cx="55.05" cy="61.85" x="51.75" y="62.70" />
                <CurveTo cx="48.15" cy="63.70" x="48.20" y="65.30" />

                <MoveTo x="43.05" y="58.50" />
                <CurveTo cx="38.85" cy="58.25" x="33.75" y="56.50" />
                <CurveTo cx="28.65" cy="54.80" x="25.45" y="52.55" />
                <CurveTo cx="20.95" cy="49.35" x="24.85" y="45.45" />
                <CurveTo cx="28.05" cy="42.20" x="32.60" y="39.85" />
                <CurveTo cx="36.60" cy="37.80" x="40.70" y="36.85" />
                <CurveTo cx="42.55" cy="36.05" x="43.90" y="39.25" />
                <CurveTo cx="45.10" cy="42.05" x="45.50" y="46.75" />
                <CurveTo cx="45.90" cy="51.30" x="45.30" y="54.70" />
                <CurveTo cx="44.65" cy="58.40" x="43.05" y="58.50" />

                <MoveTo x="59.45" y="57.70" />
                <CurveTo cx="63.55" cy="57.70" x="66.40" y="55.00" />
                <CurveTo cx="69.30" cy="52.30" x="69.30" y="48.45" />
                <CurveTo cx="69.30" cy="44.65" x="66.40" y="41.95" />
                <CurveTo cx="63.55" cy="39.25" x="59.45" y="39.25" />
                <CurveTo cx="55.35" cy="39.25" x="52.50" y="41.95" />
                <CurveTo cx="49.60" cy="44.65" x="49.60" y="48.45" />
                <CurveTo cx="49.60" cy="52.30" x="52.50" y="55.00" />
                <CurveTo cx="55.35" cy="57.70" x="59.45" y="57.70" />

                <MoveTo x="25.60" y="59.25" />
                <CurveTo cx="31.65" cy="62.30" x="34.85" y="67.05" />
                <CurveTo cx="38.05" cy="71.75" x="36.60" y="75.40" />
                <CurveTo cx="35.15" cy="79.05" x="29.80" y="79.45" />
                <CurveTo cx="24.50" cy="79.85" x="18.50" y="76.80" />
                <CurveTo cx="12.45" cy="73.75" x="9.25" y="69.05" />
                <CurveTo cx="6.05" cy="64.35" x="7.50" y="60.70" />
                <CurveTo cx="8.95" cy="57.05" x="14.25" y="56.65" />
                <CurveTo cx="19.55" cy="56.20" x="25.60" y="59.25" />

                <MoveTo x="92.90" y="58.70" />
                <CurveTo cx="86.85" cy="61.75" x="83.65" y="66.45" />
                <CurveTo cx="80.45" cy="71.15" x="81.90" y="74.80" />
                <CurveTo cx="83.40" cy="78.45" x="88.70" y="78.85" />
                <CurveTo cx="94.00" cy="79.25" x="100.00" y="76.20" />
                <CurveTo cx="106.00" cy="73.15" x="109.20" y="68.45" />
                <CurveTo cx="112.40" cy="63.75" x="110.95" y="60.10" />
                <CurveTo cx="109.50" cy="56.45" x="104.20" y="56.05" />
                <CurveTo cx="98.90" cy="55.65" x="92.90" y="58.70" />

                <MoveTo x="12.40" y="86.90" />
                <CurveTo cx="18.40" cy="87.45" x="23.00" y="88.70" />
                <CurveTo cx="27.70" cy="89.95" x="32.70" y="92.50" />
                <CurveTo cx="36.05" cy="94.20" x="37.05" y="95.65" />
                <CurveTo cx="38.35" cy="97.65" x="36.20" y="99.95" />
                <CurveTo cx="33.45" cy="102.85" x="29.90" y="103.85" />
                <CurveTo cx="26.90" cy="104.70" x="22.90" y="104.30" />
                <CurveTo cx="15.50" cy="103.50" x="11.65" y="100.20" />
                <CurveTo cx="8.75" cy="97.70" x="7.05" y="92.70" />
                <CurveTo cx="5.80" cy="89.00" x="7.20" y="87.70" />
                <CurveTo cx="8.40" cy="86.55" x="12.40" y="86.90" />

                <MoveTo x="105.65" y="87.15" />
                <CurveTo cx="100.00" cy="87.60" x="95.50" y="89.00" />
                <CurveTo cx="91.60" cy="90.20" x="86.35" y="92.85" />
                <CurveTo cx="83.00" cy="94.50" x="82.00" y="95.95" />
                <CurveTo cx="80.70" cy="97.90" x="82.85" y="100.20" />
                <CurveTo cx="85.55" cy="103.10" x="89.10" y="104.10" />
                <CurveTo cx="92.05" cy="104.90" x="96.00" y="104.50" />
                <CurveTo cx="103.25" cy="103.75" x="107.25" y="99.80" />
                <CurveTo cx="110.15" cy="96.95" x="111.90" y="91.65" />
                <CurveTo cx="113.05" cy="88.20" x="111.30" y="87.35" />
                <CurveTo cx="110.05" cy="86.75" x="105.65" y="87.15" />

                <MoveTo x="13.90" y="155.70" />
                <CurveTo cx="19.85" cy="155.00" x="24.70" y="155.35" />
                <CurveTo cx="29.50" cy="155.70" x="34.90" y="157.15" />
                <CurveTo cx="38.55" cy="158.20" x="39.80" y="159.40" />
                <CurveTo cx="41.50" cy="161.10" x="39.85" y="163.75" />
                <CurveTo cx="37.70" cy="167.20" x="34.40" y="168.90" />
                <CurveTo cx="31.65" cy="170.35" x="27.65" y="170.70" />
                <CurveTo cx="20.25" cy="171.40" x="15.80" y="168.90" />
                <CurveTo cx="12.50" cy="167.00" x="9.80" y="162.45" />
                <CurveTo cx="7.85" cy="159.05" x="8.95" y="157.55" />
                <CurveTo cx="9.90" cy="156.15" x="13.90" y="155.70" />

                <MoveTo x="104.45" y="156.15" />
                <CurveTo cx="98.60" cy="155.45" x="93.95" y="155.80" />
                <CurveTo cx="89.20" cy="156.10" x="84.00" y="157.55" />
                <CurveTo cx="80.45" cy="158.55" x="79.25" y="159.75" />
                <CurveTo cx="77.60" cy="161.35" x="79.20" y="163.95" />
                <CurveTo cx="81.25" cy="167.30" x="84.50" y="168.95" />
                <CurveTo cx="87.20" cy="170.35" x="91.05" y="170.70" />
                <CurveTo cx="98.30" cy="171.35" x="102.60" y="168.95" />
                <CurveTo cx="105.80" cy="167.10" x="108.40" y="162.65" />
                <CurveTo cx="110.30" cy="159.40" x="109.25" y="157.90" />
                <CurveTo cx="108.30" cy="156.60" x="104.45" y="156.15" />

                <MoveTo x="13.00" y="133.20" />
                <CurveTo cx="19.05" cy="132.85" x="23.80" y="133.40" />
                <CurveTo cx="28.60" cy="133.95" x="33.90" y="135.65" />
                <CurveTo cx="37.50" cy="136.75" x="38.65" y="138.00" />
                <CurveTo cx="40.25" cy="139.65" x="38.45" y="142.05" />
                <CurveTo cx="36.15" cy="145.10" x="32.75" y="146.55" />
                <CurveTo cx="29.85" cy="147.75" x="25.90" y="147.85" />
                <CurveTo cx="18.45" cy="148.10" x="14.20" y="145.55" />
                <CurveTo cx="10.95" cy="143.60" x="8.55" y="139.20" />
                <CurveTo cx="6.80" cy="136.00" x="7.95" y="134.65" />
                <CurveTo cx="9.00" cy="133.40" x="13.00" y="133.20" />

                <MoveTo x="105.60" y="133.60" />
                <CurveTo cx="99.65" cy="133.25" x="94.95" y="133.85" />
                <CurveTo cx="90.25" cy="134.45" x="85.05" y="136.20" />
                <CurveTo cx="81.55" cy="137.35" x="80.40" y="138.65" />
                <CurveTo cx="78.85" cy="140.40" x="80.60" y="142.90" />
                <CurveTo cx="82.85" cy="146.10" x="86.20" y="147.60" />
                <CurveTo cx="89.00" cy="148.85" x="92.90" y="148.95" />
                <CurveTo cx="100.25" cy="149.20" x="104.45" y="146.55" />
                <CurveTo cx="107.60" cy="144.55" x="109.95" y="139.95" />
                <CurveTo cx="111.70" cy="136.55" x="110.55" y="135.15" />
                <CurveTo cx="109.50" cy="133.85" x="105.60" y="133.60" />

                <MoveTo x="12.40" y="110.60" />
                <CurveTo cx="18.40" cy="110.30" x="23.20" y="110.90" />
                <CurveTo cx="28.00" cy="111.45" x="33.30" y="113.15" />
                <CurveTo cx="36.85" cy="114.30" x="38.05" y="115.55" />
                <CurveTo cx="39.60" cy="117.25" x="37.80" y="119.65" />
                <CurveTo cx="35.50" cy="122.75" x="32.10" y="124.20" />
                <CurveTo cx="29.25" cy="125.40" x="25.25" y="125.50" />
                <CurveTo cx="17.75" cy="125.75" x="13.50" y="123.15" />
                <CurveTo cx="10.30" cy="121.25" x="7.90" y="116.75" />
                <CurveTo cx="6.15" cy="113.45" x="7.35" y="112.05" />
                <CurveTo cx="8.40" cy="110.85" x="12.40" y="110.60" />

                <MoveTo x="106.15" y="111.05" />
                <CurveTo cx="100.30" cy="110.70" x="95.60" y="111.35" />
                <CurveTo cx="90.90" cy="111.95" x="85.70" y="113.70" />
                <CurveTo cx="82.20" cy="114.85" x="81.00" y="116.20" />
                <CurveTo cx="79.45" cy="117.90" x="81.25" y="120.45" />
                <CurveTo cx="83.55" cy="123.65" x="86.85" y="125.15" />
                <CurveTo cx="89.65" cy="126.40" x="93.55" y="126.50" />
                <CurveTo cx="100.90" cy="126.75" x="105.05" y="124.05" />
                <CurveTo cx="108.25" cy="122.05" x="110.55" y="117.40" />
                <CurveTo cx="112.30" cy="113.95" x="111.15" y="112.55" />
                <CurveTo cx="110.10" cy="111.25" x="106.15" y="111.05" />

                <MoveTo x="75.70" y="94.95" />
                <CurveTo cx="75.70" cy="98.70" x="70.75" y="102.05" />
                <CurveTo cx="65.65" cy="105.45" x="59.50" y="105.45" />
                <CurveTo cx="53.35" cy="105.45" x="48.35" y="102.05" />
                <CurveTo cx="43.50" cy="98.75" x="43.50" y="94.95" />
                <CurveTo cx="43.50" cy="92.80" x="48.55" y="91.60" />
                <CurveTo cx="53.00" cy="90.55" x="59.60" y="90.60" />
                <CurveTo cx="66.10" cy="90.65" x="70.65" y="91.75" />
                <CurveTo cx="75.70" cy="93.00" x="75.70" y="94.95" />

                <MoveTo x="75.55" y="161.50" />
                <CurveTo cx="75.55" cy="165.25" x="70.60" y="168.60" />
                <CurveTo cx="65.50" cy="172.00" x="59.35" y="172.00" />
                <CurveTo cx="53.20" cy="172.00" x="48.20" y="168.60" />
                <CurveTo cx="43.35" cy="165.30" x="43.35" y="161.50" />
                <CurveTo cx="43.35" cy="159.35" x="48.40" y="158.15" />
                <CurveTo cx="52.85" cy="157.10" x="59.45" y="157.15" />
                <CurveTo cx="65.95" cy="157.20" x="70.50" y="158.30" />
                <CurveTo cx="75.55" cy="159.55" x="75.55" y="161.50" />

                <MoveTo x="75.75" y="139.55" />
                <CurveTo cx="75.75" cy="143.35" x="70.80" y="146.65" />
                <CurveTo cx="65.75" cy="150.05" x="59.55" y="150.05" />
                <CurveTo cx="53.40" cy="150.05" x="48.40" y="146.65" />
                <CurveTo cx="43.55" cy="143.35" x="43.55" y="139.55" />
                <CurveTo cx="43.55" cy="137.40" x="48.60" y="136.20" />
                <CurveTo cx="53.05" cy="135.15" x="59.65" y="135.20" />
                <CurveTo cx="66.15" cy="135.25" x="70.70" y="136.35" />
                <CurveTo cx="75.75" cy="137.60" x="75.75" y="139.55" />

                <MoveTo x="75.55" y="117.90" />
                <CurveTo cx="75.55" cy="121.65" x="70.60" y="125.00" />
                <CurveTo cx="65.50" cy="128.40" x="59.35" y="128.40" />
                <CurveTo cx="53.20" cy="128.40" x="48.20" y="125.00" />
                <CurveTo cx="43.35" cy="121.70" x="43.35" y="117.90" />
                <CurveTo cx="43.35" cy="115.75" x="48.40" y="114.55" />
                <CurveTo cx="52.85" cy="113.50" x="59.45" y="113.55" />
                <CurveTo cx="65.95" cy="113.60" x="70.50" y="114.70" />
                <CurveTo cx="75.55" cy="115.95" x="75.55" y="117.90" />

                <MoveTo x="103.20" y="192.95" />
                <CurveTo cx="104.70" cy="192.95" x="105.75" y="191.90" />
                <CurveTo cx="106.80" cy="190.85" x="106.80" y="189.40" />
                <CurveTo cx="106.80" cy="187.90" x="105.75" y="186.85" />
                <CurveTo cx="104.70" cy="185.80" x="103.20" y="185.80" />
                <CurveTo cx="101.75" cy="185.80" x="100.70" y="186.85" />
                <CurveTo cx="99.65" cy="187.90" x="99.65" y="189.40" />
                <CurveTo cx="99.65" cy="190.85" x="100.70" y="191.90" />
                <CurveTo cx="101.75" cy="192.95" x="103.20" y="192.95" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (15, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 15
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="49.50" y="0.00" />
                <LineTo x="83.80" y="0.00" />
                <CurveTo cx="94.75" cy="0.00" x="103.50" y="2.85" />
                <CurveTo cx="113.35" cy="6.00" x="119.30" y="12.55" />
                <CurveTo cx="124.45" cy="18.15" x="127.20" y="27.25" />
                <CurveTo cx="129.65" cy="35.45" x="129.65" y="44.50" />
                <LineTo x="129.65" y="160.85" />
                <CurveTo cx="131.80" cy="193.50" x="111.15" y="212.90" />
                <CurveTo cx="92.70" cy="230.20" x="65.25" y="230.15" />
                <CurveTo cx="37.75" cy="230.10" x="19.95" y="212.65" />
                <CurveTo cx="0.00" cy="193.15" x="3.35" y="160.50" />
                <LineTo x="3.65" y="44.50" />
                <CurveTo cx="3.70" cy="34.85" x="5.75" y="27.10" />
                <CurveTo cx="8.15" cy="17.80" x="13.25" y="12.30" />
                <CurveTo cx="19.20" cy="5.90" x="29.50" y="2.75" />
                <CurveTo cx="38.45" cy="0.00" x="49.50" y="0.00" />

                <MoveTo x="86.80" y="214.45" />
                <CurveTo cx="92.35" cy="214.45" x="96.30" y="210.35" />
                <CurveTo cx="100.20" cy="206.30" x="100.20" y="200.55" />
                <CurveTo cx="100.20" cy="194.75" x="96.30" y="190.70" />
                <CurveTo cx="92.35" cy="186.60" x="86.80" y="186.60" />
                <CurveTo cx="81.30" cy="186.60" x="77.35" y="190.70" />
                <CurveTo cx="73.45" cy="194.80" x="73.45" y="200.55" />
                <CurveTo cx="73.45" cy="206.30" x="77.35" y="210.35" />
                <CurveTo cx="81.30" cy="214.45" x="86.80" y="214.45" />

                <MoveTo x="45.35" y="54.65" />
                <LineTo x="87.65" y="54.65" />
                <CurveTo cx="93.85" cy="54.65" x="93.85" y="63.60" />
                <LineTo x="93.85" y="82.10" />
                <CurveTo cx="93.85" cy="91.05" x="87.65" y="91.05" />
                <LineTo x="45.35" y="91.05" />
                <CurveTo cx="39.15" cy="91.05" x="39.15" y="82.10" />
                <LineTo x="39.15" y="63.60" />
                <CurveTo cx="39.15" cy="54.65" x="45.35" y="54.65" />

                <MoveTo x="42.95" y="214.15" />
                <CurveTo cx="48.45" cy="214.15" x="52.40" y="210.05" />
                <CurveTo cx="56.35" cy="206.00" x="56.35" y="200.25" />
                <CurveTo cx="56.35" cy="194.50" x="52.40" y="190.40" />
                <CurveTo cx="48.45" cy="186.30" x="42.95" y="186.30" />
                <CurveTo cx="37.45" cy="186.30" x="33.50" y="190.40" />
                <CurveTo cx="29.55" cy="194.50" x="29.55" y="200.25" />
                <CurveTo cx="29.55" cy="206.00" x="33.50" y="210.05" />
                <CurveTo cx="37.45" cy="214.15" x="42.95" y="214.15" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (16, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 16
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="6.90" y="0.00" />
                <LineTo x="95.05" y="0.00" />
                <CurveTo cx="102.05" cy="0.00" x="101.95" y="6.85" />
                <LineTo x="101.10" y="136.55" />
                <CurveTo cx="101.05" cy="145.05" x="85.25" y="150.15" />
                <CurveTo cx="71.05" cy="154.70" x="50.50" y="154.85" />
                <CurveTo cx="30.00" cy="155.00" x="15.80" y="150.70" />
                <CurveTo cx="0.00" cy="145.95" x="0.00" y="137.70" />
                <LineTo x="0.00" y="6.85" />
                <CurveTo cx="0.00" cy="0.00" x="6.90" y="0.00" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (17, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 17
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 3" x="50" y="420">
            <Contour x="0" y="0">
                <MoveTo x="62.50" y="224.60" />
                <LineTo x="66.00" y="224.60" />
                <LineTo x="116.25" y="224.35" />
                <CurveTo cx="124.30" cy="220.20" x="127.10" y="203.60" />
                <CurveTo cx="132.15" cy="173.70" x="130.85" y="136.15" />
                <CurveTo cx="130.35" cy="120.90" x="128.70" y="101.45" />
                <CurveTo cx="127.70" cy="89.65" x="125.30" y="65.45" />
                <LineTo x="125.15" y="64.00" />
                <CurveTo cx="122.10" cy="34.00" x="106.95" y="17.20" />
                <CurveTo cx="91.40" cy="0.00" x="66.00" y="0.00" />
                <LineTo x="62.50" y="0.00" />
                <CurveTo cx="38.85" cy="0.00" x="22.55" y="18.90" />
                <CurveTo cx="7.10" cy="36.75" x="4.20" y="64.00" />
                <CurveTo cx="0.10" cy="103.15" x="0.05" y="134.35" />
                <CurveTo cx="0.00" cy="171.75" x="5.65" y="204.55" />
                <CurveTo cx="8.65" cy="221.95" x="16.20" y="224.35" />
                <LineTo x="62.50" y="224.60" />

                <MoveTo x="26.60" y="87.15" />
                <LineTo x="104.20" y="87.15" />
                <CurveTo cx="108.45" cy="87.15" x="108.45" y="91.40" />
                <LineTo x="108.45" y="192.95" />
                <CurveTo cx="108.45" cy="197.20" x="104.20" y="197.20" />
                <LineTo x="26.60" y="197.20" />
                <CurveTo cx="22.35" cy="197.20" x="22.35" y="192.95" />
                <LineTo x="22.35" y="91.40" />
                <CurveTo cx="22.35" cy="87.15" x="26.60" y="87.15" />

                <MoveTo x="77.85" y="28.80" />
                <LineTo x="80.95" y="27.15" />
                <CurveTo cx="87.85" cy="31.55" x="87.45" y="35.90" />
                <CurveTo cx="87.15" cy="39.70" x="81.20" y="43.75" />
                <LineTo x="78.55" y="42.30" />
                <CurveTo cx="83.80" cy="37.25" x="83.90" y="34.85" />
                <CurveTo cx="84.05" cy="31.95" x="77.85" y="28.80" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (18, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 18
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 4" x="300" y="420">
            <Contour x="0" y="0">
                <MoveTo x="126.45" y="27.15" />
                <CurveTo cx="127.50" cy="47.05" x="127.95" y="62.35" />
                <CurveTo cx="128.40" cy="80.00" x="128.20" y="95.60" />
                <CurveTo cx="127.75" cy="129.35" x="123.65" y="164.70" />
                <CurveTo cx="120.50" cy="191.80" x="102.75" y="210.05" />
                <CurveTo cx="85.10" cy="228.20" x="63.80" y="226.65" />
                <CurveTo cx="40.50" cy="226.80" x="23.40" y="208.75" />
                <CurveTo cx="6.60" cy="191.10" x="3.80" y="164.70" />
                <CurveTo cx="0.00" cy="129.25" x="0.05" y="95.25" />
                <CurveTo cx="0.10" cy="80.30" x="0.80" y="62.40" />
                <CurveTo cx="1.35" cy="48.90" x="2.60" y="27.25" />
                <CurveTo cx="3.50" cy="19.10" x="4.80" y="13.50" />
                <CurveTo cx="6.40" cy="6.75" x="8.55" y="3.95" />
                <CurveTo cx="10.70" cy="1.15" x="15.10" y="0.40" />
                <CurveTo cx="17.70" cy="0.00" x="23.70" y="0.25" />
                <CurveTo cx="26.50" cy="0.40" x="27.85" y="0.40" />
                <LineTo x="106.05" y="0.55" />
                <LineTo x="107.70" y="0.55" />
                <CurveTo cx="111.70" cy="0.50" x="113.75" y="0.95" />
                <CurveTo cx="117.25" cy="1.75" x="119.25" y="4.35" />
                <CurveTo cx="122.35" cy="8.25" x="124.40" y="15.50" />
                <CurveTo cx="126.15" cy="21.60" x="126.45" y="27.15" />

                <MoveTo x="52.65" y="133.75" />
                <CurveTo cx="64.05" cy="132.80" x="74.35" y="133.80" />
                <CurveTo cx="77.90" cy="134.00" x="79.05" y="134.95" />
                <CurveTo cx="80.10" cy="135.80" x="79.95" y="137.95" />
                <LineTo x="79.60" y="142.70" />
                <CurveTo cx="79.20" cy="148.10" x="74.80" y="149.65" />
                <CurveTo cx="68.95" cy="152.25" x="63.75" y="152.35" />
                <CurveTo cx="58.35" cy="152.45" x="52.30" y="149.90" />
                <CurveTo cx="52.00" cy="149.55" x="51.05" y="148.95" />
                <CurveTo cx="50.20" cy="148.45" x="49.90" y="148.15" />
                <CurveTo cx="48.95" cy="147.20" x="48.80" y="145.50" />
                <LineTo x="47.95" y="137.95" />
                <CurveTo cx="47.75" cy="136.55" x="48.45" y="135.55" />
                <CurveTo cx="49.55" cy="133.95" x="52.65" y="133.75" />

                <MoveTo x="88.25" y="56.60" />
                <LineTo x="89.50" y="35.35" />
                <CurveTo cx="89.50" cy="25.35" x="82.40" y="18.25" />
                <CurveTo cx="75.25" cy="11.10" x="65.25" y="11.10" />
                <LineTo x="64.70" y="11.10" />
                <CurveTo cx="54.70" cy="11.10" x="47.55" y="18.25" />
                <CurveTo cx="40.45" cy="25.35" x="40.45" y="35.35" />
                <LineTo x="41.65" y="55.25" />
                <CurveTo cx="41.85" cy="58.60" x="49.15" y="60.20" />
                <CurveTo cx="55.40" cy="61.65" x="65.05" y="61.45" />
                <CurveTo cx="74.05" cy="61.30" x="80.95" y="59.95" />
                <CurveTo cx="88.15" cy="58.50" x="88.25" y="56.60" />

                <MoveTo x="36.95" y="17.05" />
                <CurveTo cx="33.10" cy="23.15" x="32.40" y="32.95" />
                <CurveTo cx="31.95" cy="39.95" x="33.10" y="53.35" />
                <CurveTo cx="33.15" cy="56.00" x="30.95" y="54.70" />
                <CurveTo cx="30.50" cy="54.45" x="30.30" y="54.35" />
                <LineTo x="19.65" y="47.45" />
                <CurveTo cx="13.85" cy="43.55" x="14.45" y="34.80" />
                <CurveTo cx="14.50" cy="33.85" x="14.50" y="32.65" />
                <CurveTo cx="14.45" cy="31.25" x="14.65" y="30.15" />
                <CurveTo cx="15.00" cy="28.45" x="16.25" y="24.80" />
                <CurveTo cx="17.50" cy="21.05" x="21.75" y="18.90" />
                <CurveTo cx="24.25" cy="17.60" x="29.80" y="16.20" />
                <CurveTo cx="32.40" cy="15.50" x="33.55" y="15.15" />
                <LineTo x="33.95" y="15.00" />
                <CurveTo cx="36.10" cy="14.25" x="36.90" y="14.35" />
                <CurveTo cx="38.45" cy="14.50" x="36.95" y="17.05" />

                <MoveTo x="92.95" y="16.75" />
                <CurveTo cx="96.80" cy="22.95" x="97.50" y="32.90" />
                <CurveTo cx="97.95" cy="40.00" x="96.80" y="53.65" />
                <CurveTo cx="96.75" cy="56.25" x="98.90" y="55.00" />
                <CurveTo cx="99.35" cy="54.75" x="99.55" y="54.65" />
                <LineTo x="110.20" y="47.65" />
                <CurveTo cx="116.00" cy="43.70" x="115.40" y="34.80" />
                <CurveTo cx="115.35" cy="33.85" x="115.35" y="32.65" />
                <CurveTo cx="115.40" cy="31.20" x="115.20" y="30.05" />
                <CurveTo cx="114.85" cy="28.30" x="113.60" y="24.60" />
                <CurveTo cx="112.35" cy="20.80" x="108.15" y="18.60" />
                <CurveTo cx="105.65" cy="17.30" x="100.05" y="15.85" />
                <CurveTo cx="97.45" cy="15.20" x="96.30" y="14.80" />
                <LineTo x="95.90" y="14.65" />
                <CurveTo cx="93.80" cy="13.90" x="92.95" y="14.00" />
                <CurveTo cx="91.45" cy="14.15" x="92.95" y="16.75" />

                <MoveTo x="45.05" y="64.10" />
                <CurveTo cx="54.75" cy="66.80" x="63.65" y="66.85" />
                <CurveTo cx="73.45" cy="66.90" x="82.95" y="63.75" />
                <CurveTo cx="85.30" cy="63.35" x="86.40" y="65.40" />
                <CurveTo cx="87.40" cy="67.30" x="86.50" y="69.15" />
                <LineTo x="83.50" y="75.55" />
                <CurveTo cx="82.70" cy="77.30" x="78.55" y="78.60" />
                <CurveTo cx="74.95" cy="79.70" x="72.00" y="79.75" />
                <LineTo x="55.50" y="79.90" />
                <CurveTo cx="52.20" cy="79.95" x="48.75" y="78.30" />
                <CurveTo cx="45.85" cy="76.95" x="44.95" y="75.55" />
                <LineTo x="41.45" y="70.35" />
                <CurveTo cx="40.05" cy="68.30" x="41.80" y="65.95" />
                <CurveTo cx="43.45" cy="63.65" x="45.05" y="64.10" />

                <MoveTo x="52.40" y="86.00" />
                <CurveTo cx="63.35" cy="87.40" x="74.10" y="86.05" />
                <CurveTo cx="77.65" cy="85.60" x="78.85" y="86.55" />
                <CurveTo cx="79.90" cy="87.45" x="79.70" y="90.30" />
                <LineTo x="79.35" y="95.25" />
                <CurveTo cx="78.95" cy="100.80" x="74.55" y="102.40" />
                <CurveTo cx="63.35" cy="107.65" x="52.00" y="102.65" />
                <CurveTo cx="51.70" cy="102.25" x="50.75" y="101.65" />
                <CurveTo cx="49.95" cy="101.15" x="49.65" y="100.85" />
                <CurveTo cx="48.70" cy="99.85" x="48.55" y="98.15" />
                <LineTo x="47.70" y="90.30" />
                <CurveTo cx="47.50" cy="88.55" x="48.20" y="87.50" />
                <CurveTo cx="49.25" cy="85.95" x="52.40" y="86.00" />

                <MoveTo x="52.90" y="109.15" />
                <CurveTo cx="64.45" cy="108.20" x="74.65" y="109.25" />
                <CurveTo cx="78.15" cy="109.40" x="79.30" y="110.40" />
                <CurveTo cx="80.35" cy="111.25" x="80.20" y="113.50" />
                <LineTo x="79.85" y="118.40" />
                <CurveTo cx="79.45" cy="123.95" x="75.05" y="125.55" />
                <CurveTo cx="63.90" cy="130.75" x="52.55" y="125.85" />
                <CurveTo cx="52.25" cy="125.45" x="51.30" y="124.85" />
                <CurveTo cx="50.45" cy="124.35" x="50.15" y="124.00" />
                <CurveTo cx="49.20" cy="122.95" x="49.05" y="121.30" />
                <LineTo x="48.20" y="113.50" />
                <CurveTo cx="48.00" cy="112.05" x="48.70" y="111.00" />
                <CurveTo cx="49.80" cy="109.40" x="52.90" y="109.15" />

                <MoveTo x="52.65" y="157.25" />
                <CurveTo cx="64.05" cy="156.30" x="74.35" y="157.30" />
                <CurveTo cx="77.90" cy="157.50" x="79.05" y="158.45" />
                <CurveTo cx="80.10" cy="159.30" x="79.95" y="161.45" />
                <LineTo x="79.60" y="166.25" />
                <CurveTo cx="79.20" cy="171.65" x="74.80" y="173.25" />
                <CurveTo cx="63.70" cy="178.30" x="52.30" y="173.50" />
                <CurveTo cx="51.95" cy="173.10" x="51.00" y="172.50" />
                <CurveTo cx="50.20" cy="172.05" x="49.90" y="171.75" />
                <CurveTo cx="48.95" cy="170.75" x="48.80" y="169.10" />
                <LineTo x="47.95" y="161.45" />
                <CurveTo cx="47.75" cy="160.05" x="48.45" y="159.05" />
                <CurveTo cx="49.55" cy="157.45" x="52.65" y="157.25" />

                <MoveTo x="52.90" y="180.00" />
                <CurveTo cx="58.65" cy="179.55" x="63.75" y="179.55" />
                <CurveTo cx="69.40" cy="179.55" x="74.60" y="180.10" />
                <CurveTo cx="78.15" cy="180.30" x="79.30" y="181.25" />
                <CurveTo cx="80.35" cy="182.10" x="80.20" y="184.25" />
                <LineTo x="79.85" y="189.10" />
                <CurveTo cx="79.45" cy="194.55" x="75.05" y="196.15" />
                <CurveTo cx="69.20" cy="198.80" x="63.95" y="198.90" />
                <CurveTo cx="58.55" cy="199.00" x="52.50" y="196.40" />
                <CurveTo cx="52.20" cy="196.00" x="51.25" y="195.40" />
                <CurveTo cx="50.45" cy="194.90" x="50.15" y="194.60" />
                <CurveTo cx="49.20" cy="193.65" x="49.05" y="191.95" />
                <LineTo x="48.20" y="184.25" />
                <CurveTo cx="48.00" cy="182.85" x="48.70" y="181.85" />
                <CurveTo cx="49.80" cy="180.25" x="52.90" y="180.00" />

                <MoveTo x="65.25" y="218.80" />
                <CurveTo cx="67.40" cy="218.80" x="68.90" y="218.10" />
                <CurveTo cx="70.45" cy="217.35" x="70.45" y="216.35" />
                <CurveTo cx="70.45" cy="215.35" x="68.90" y="214.60" />
                <CurveTo cx="67.40" cy="213.90" x="65.25" y="213.90" />
                <CurveTo cx="63.10" cy="213.90" x="61.60" y="214.60" />
                <CurveTo cx="60.10" cy="215.35" x="60.10" y="216.35" />
                <CurveTo cx="60.10" cy="217.35" x="61.60" y="218.10" />
                <CurveTo cx="63.10" cy="218.80" x="65.25" y="218.80" />

                <MoveTo x="42.65" y="87.90" />
                <CurveTo cx="42.70" cy="89.10" x="43.15" y="91.55" />
                <CurveTo cx="43.75" cy="95.35" x="43.30" y="96.45" />
                <CurveTo cx="42.60" cy="98.25" x="38.95" y="97.15" />
                <LineTo x="36.30" y="96.40" />
                <CurveTo cx="27.00" cy="93.70" x="22.35" y="91.50" />
                <CurveTo cx="14.15" cy="87.60" x="13.30" y="82.90" />
                <CurveTo cx="12.20" cy="76.85" x="16.35" y="74.95" />
                <CurveTo cx="19.90" cy="73.35" x="26.20" y="75.15" />
                <CurveTo cx="32.05" cy="76.80" x="36.90" y="80.45" />
                <CurveTo cx="42.05" cy="84.25" x="42.65" y="87.90" />

                <MoveTo x="86.50" y="87.50" />
                <CurveTo cx="86.45" cy="88.75" x="85.95" y="91.30" />
                <CurveTo cx="85.30" cy="95.05" x="85.70" y="96.20" />
                <CurveTo cx="86.45" cy="97.95" x="90.10" y="97.00" />
                <LineTo x="90.35" y="96.95" />
                <CurveTo cx="94.40" cy="95.90" x="97.00" y="95.10" />
                <CurveTo cx="101.65" cy="93.70" x="105.10" y="92.15" />
                <CurveTo cx="114.95" cy="87.85" x="115.85" y="82.65" />
                <CurveTo cx="117.00" cy="76.45" x="112.90" y="74.45" />
                <CurveTo cx="109.35" cy="72.75" x="103.05" y="74.55" />
                <CurveTo cx="97.15" cy="76.20" x="92.30" y="79.90" />
                <CurveTo cx="87.15" cy="83.75" x="86.50" y="87.50" />

                <MoveTo x="44.30" y="117.40" />
                <LineTo x="43.80" y="121.15" />
                <CurveTo cx="43.40" cy="123.70" x="39.85" y="122.85" />
                <LineTo x="24.95" y="119.10" />
                <CurveTo cx="18.30" cy="116.80" x="15.50" y="112.35" />
                <CurveTo cx="12.35" cy="107.45" x="16.05" y="102.45" />
                <CurveTo cx="17.30" cy="99.85" x="22.65" y="100.35" />
                <CurveTo cx="27.60" cy="100.80" x="34.50" y="103.75" />
                <LineTo x="35.65" y="104.35" />
                <CurveTo cx="38.80" cy="105.85" x="39.90" y="106.90" />
                <CurveTo cx="45.10" cy="111.75" x="44.30" y="117.40" />

                <MoveTo x="85.00" y="117.35" />
                <LineTo x="85.50" y="121.10" />
                <CurveTo cx="85.85" cy="123.65" x="89.40" y="122.80" />
                <LineTo x="104.20" y="119.05" />
                <CurveTo cx="110.75" cy="116.75" x="113.55" y="112.30" />
                <CurveTo cx="116.70" cy="107.35" x="113.00" y="102.40" />
                <CurveTo cx="111.75" cy="99.80" x="106.50" y="100.35" />
                <CurveTo cx="101.60" cy="100.85" x="94.75" y="103.85" />
                <LineTo x="93.60" y="104.45" />
                <CurveTo cx="90.45" cy="105.95" x="89.35" y="107.00" />
                <CurveTo cx="84.25" cy="111.85" x="85.00" y="117.35" />

                <MoveTo x="43.75" y="141.75" />
                <LineTo x="43.05" y="145.50" />
                <CurveTo cx="42.55" cy="148.10" x="39.25" y="147.00" />
                <LineTo x="25.30" y="142.40" />
                <CurveTo cx="18.60" cy="139.90" x="16.15" y="135.30" />
                <CurveTo cx="13.45" cy="130.20" x="17.90" y="125.25" />
                <CurveTo cx="19.20" cy="122.75" x="24.25" y="123.75" />
                <CurveTo cx="28.75" cy="124.60" x="35.25" y="128.05" />
                <LineTo x="35.40" y="128.15" />
                <CurveTo cx="38.65" cy="130.15" x="39.85" y="131.50" />
                <CurveTo cx="44.75" cy="136.80" x="43.75" y="141.75" />

                <MoveTo x="84.20" y="142.10" />
                <LineTo x="84.90" y="145.80" />
                <CurveTo cx="85.45" cy="148.35" x="88.80" y="147.25" />
                <LineTo x="102.90" y="142.75" />
                <CurveTo cx="109.70" cy="140.50" x="112.15" y="135.90" />
                <CurveTo cx="114.90" cy="130.90" x="110.40" y="125.85" />
                <CurveTo cx="109.05" cy="123.40" x="104.00" y="124.35" />
                <CurveTo cx="99.45" cy="125.20" x="92.80" y="128.60" />
                <LineTo x="92.40" y="128.85" />
                <CurveTo cx="89.30" cy="130.75" x="88.15" y="132.00" />
                <CurveTo cx="83.25" cy="137.35" x="84.20" y="142.10" />

                <MoveTo x="43.80" y="165.75" />
                <LineTo x="43.15" y="169.40" />
                <CurveTo cx="42.70" cy="172.00" x="39.45" y="170.90" />
                <LineTo x="25.85" y="166.60" />
                <CurveTo cx="19.80" cy="164.10" x="17.40" y="159.65" />
                <CurveTo cx="14.75" cy="154.70" x="18.40" y="149.95" />
                <CurveTo cx="19.70" cy="147.50" x="24.55" y="148.40" />
                <CurveTo cx="29.00" cy="149.20" x="35.35" y="152.50" />
                <CurveTo cx="36.95" cy="153.45" x="39.70" y="155.65" />
                <LineTo x="39.85" y="155.75" />
                <CurveTo cx="45.00" cy="159.70" x="43.80" y="165.75" />

                <MoveTo x="83.90" y="166.00" />
                <LineTo x="84.60" y="169.65" />
                <CurveTo cx="85.05" cy="172.25" x="88.40" y="171.15" />
                <LineTo x="102.35" y="166.90" />
                <CurveTo cx="108.55" cy="164.40" x="111.00" y="159.95" />
                <CurveTo cx="113.75" cy="154.95" x="110.00" y="150.25" />
                <CurveTo cx="108.70" cy="147.80" x="103.70" y="148.65" />
                <CurveTo cx="99.20" cy="149.45" x="92.60" y="152.75" />
                <CurveTo cx="91.35" cy="153.50" x="89.10" y="155.15" />
                <LineTo x="87.95" y="156.05" />
                <CurveTo cx="82.45" cy="160.00" x="83.90" y="166.00" />

                <MoveTo x="44.10" y="190.25" />
                <LineTo x="43.25" y="193.70" />
                <CurveTo cx="42.65" cy="196.15" x="39.75" y="194.85" />
                <LineTo x="27.55" y="189.85" />
                <CurveTo cx="22.20" cy="187.05" x="20.30" y="182.60" />
                <CurveTo cx="18.20" cy="177.65" x="21.90" y="173.40" />
                <CurveTo cx="23.20" cy="171.15" x="27.65" y="172.35" />
                <CurveTo cx="31.65" cy="173.40" x="37.25" y="176.95" />
                <LineTo x="37.90" y="177.40" />
                <LineTo x="38.55" y="177.85" />
                <CurveTo cx="40.55" cy="179.20" x="41.35" y="180.10" />
                <CurveTo cx="45.45" cy="184.55" x="44.10" y="190.25" />

                <MoveTo x="84.15" y="190.55" />
                <LineTo x="85.00" y="193.95" />
                <CurveTo cx="85.65" cy="196.30" x="88.60" y="195.15" />
                <LineTo x="101.05" y="190.15" />
                <CurveTo cx="106.50" cy="187.40" x="108.45" y="183.05" />
                <CurveTo cx="110.60" cy="178.15" x="106.85" y="173.95" />
                <CurveTo cx="105.50" cy="171.75" x="100.95" y="172.90" />
                <CurveTo cx="96.90" cy="173.95" x="91.15" y="177.50" />
                <CurveTo cx="89.35" cy="178.75" x="87.00" y="180.75" />
                <CurveTo cx="82.30" cy="184.70" x="84.15" y="190.55" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (19, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 19
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="137.05" y="4.80" />
                <CurveTo cx="142.65" cy="7.35" x="146.30" y="11.60" />
                <CurveTo cx="148.00" cy="13.60" x="149.60" y="18.00" />
                <CurveTo cx="151.00" cy="21.90" x="152.40" y="23.70" />
                <CurveTo cx="154.65" cy="26.65" x="159.70" y="31.85" />
                <CurveTo cx="165.55" cy="37.85" x="167.75" y="41.15" />
                <CurveTo cx="171.45" cy="46.70" x="171.85" y="52.70" />
                <CurveTo cx="175.45" cy="105.75" x="175.75" y="156.35" />
                <CurveTo cx="176.00" cy="206.30" x="173.15" y="260.00" />
                <CurveTo cx="172.80" cy="266.70" x="167.70" y="270.65" />
                <CurveTo cx="164.75" cy="273.00" x="157.65" y="275.55" />
                <CurveTo cx="150.20" cy="278.30" x="149.15" y="280.75" />
                <CurveTo cx="148.55" cy="282.00" x="145.05" y="285.75" />
                <CurveTo cx="137.00" cy="294.20" x="124.80" y="297.65" />
                <CurveTo cx="111.70" cy="301.35" x="91.30" y="300.10" />
                <CurveTo cx="65.05" cy="300.90" x="47.75" y="297.10" />
                <CurveTo cx="39.30" cy="293.85" x="33.55" y="288.85" />
                <CurveTo cx="31.15" cy="285.45" x="29.70" y="281.45" />
                <CurveTo cx="28.60" cy="278.70" x="21.25" y="276.10" />
                <CurveTo cx="14.50" cy="273.70" x="11.65" y="271.40" />
                <CurveTo cx="6.80" cy="267.45" x="6.15" y="260.35" />
                <CurveTo cx="0.85" cy="202.15" x="0.45" y="152.95" />
                <CurveTo cx="0.00" cy="101.30" x="4.80" y="50.55" />
                <CurveTo cx="5.45" cy="44.55" x="9.65" y="39.65" />
                <CurveTo cx="12.15" cy="36.80" x="17.95" y="32.50" />
                <CurveTo cx="25.10" cy="27.25" x="25.75" y="24.05" />
                <CurveTo cx="28.05" cy="12.55" x="33.00" y="8.60" />
                <CurveTo cx="40.20" cy="4.35" x="55.10" y="2.15" />
                <CurveTo cx="68.10" cy="0.25" x="85.25" y="0.15" />
                <CurveTo cx="119.25" cy="0.00" x="137.05" y="4.80" />

                <MoveTo x="78.65" y="16.50" />
                <LineTo x="86.85" y="16.50" />
                <CurveTo cx="87.35" cy="16.50" x="87.70" y="16.85" />
                <CurveTo cx="88.05" cy="17.20" x="88.05" y="17.70" />
                <CurveTo cx="88.05" cy="18.20" x="87.70" y="18.55" />
                <CurveTo cx="87.35" cy="18.90" x="86.85" y="18.90" />
                <LineTo x="78.65" y="18.90" />
                <CurveTo cx="78.15" cy="18.90" x="77.80" y="18.55" />
                <CurveTo cx="77.45" cy="18.20" x="77.45" y="17.70" />
                <CurveTo cx="77.45" cy="17.20" x="77.80" y="16.85" />
                <CurveTo cx="78.15" cy="16.50" x="78.65" y="16.50" />

                <MoveTo x="78.60" y="23.50" />
                <LineTo x="86.75" y="23.50" />
                <CurveTo cx="87.25" cy="23.50" x="87.60" y="23.85" />
                <CurveTo cx="87.95" cy="24.20" x="87.95" y="24.70" />
                <CurveTo cx="87.95" cy="25.20" x="87.60" y="25.55" />
                <CurveTo cx="87.25" cy="25.90" x="86.75" y="25.90" />
                <LineTo x="78.60" y="25.90" />
                <CurveTo cx="78.10" cy="25.90" x="77.75" y="25.55" />
                <CurveTo cx="77.40" cy="25.20" x="77.40" y="24.70" />
                <CurveTo cx="77.40" cy="24.20" x="77.75" y="23.85" />
                <CurveTo cx="78.10" cy="23.50" x="78.60" y="23.50" />

                <MoveTo x="47.85" y="13.90" />
                <LineTo x="49.00" y="13.90" />
                <CurveTo cx="50.20" cy="13.90" x="51.05" y="14.75" />
                <CurveTo cx="51.90" cy="15.60" x="51.90" y="16.80" />
                <LineTo x="51.90" y="21.40" />
                <CurveTo cx="51.90" cy="22.60" x="51.05" y="23.45" />
                <CurveTo cx="50.20" cy="24.30" x="49.00" y="24.30" />
                <LineTo x="47.85" y="24.30" />
                <CurveTo cx="46.65" cy="24.30" x="45.80" y="23.45" />
                <CurveTo cx="44.95" cy="22.60" x="44.95" y="21.40" />
                <LineTo x="44.95" y="16.80" />
                <CurveTo cx="44.95" cy="15.60" x="45.80" y="14.75" />
                <CurveTo cx="46.65" cy="13.90" x="47.85" y="13.90" />

                <MoveTo x="142.40" y="64.70" />
                <CurveTo cx="144.20" cy="64.70" x="145.50" y="66.00" />
                <CurveTo cx="146.75" cy="67.25" x="146.75" y="69.05" />
                <LineTo x="146.75" y="84.00" />
                <CurveTo cx="146.75" cy="85.80" x="145.50" y="87.10" />
                <CurveTo cx="144.20" cy="88.35" x="142.40" y="88.35" />
                <CurveTo cx="140.60" cy="88.35" x="139.30" y="87.10" />
                <CurveTo cx="138.00" cy="85.80" x="138.00" y="84.00" />
                <LineTo x="138.00" y="69.05" />
                <CurveTo cx="138.00" cy="67.25" x="139.30" y="66.00" />
                <CurveTo cx="140.60" cy="64.70" x="142.40" y="64.70" />

                <MoveTo x="142.70" y="136.65" />
                <CurveTo cx="144.50" cy="136.65" x="145.80" y="137.95" />
                <CurveTo cx="147.05" cy="139.20" x="147.05" y="141.00" />
                <LineTo x="147.05" y="155.95" />
                <CurveTo cx="147.05" cy="157.75" x="145.80" y="159.05" />
                <CurveTo cx="144.50" cy="160.30" x="142.70" y="160.30" />
                <CurveTo cx="140.90" cy="160.30" x="139.65" y="159.05" />
                <CurveTo cx="138.35" cy="157.75" x="138.35" y="155.95" />
                <LineTo x="138.35" y="141.00" />
                <CurveTo cx="138.35" cy="139.20" x="139.65" y="137.95" />
                <CurveTo cx="140.90" cy="136.65" x="142.70" y="136.65" />

                <MoveTo x="46.30" y="62.10" />
                <LineTo x="114.65" y="61.40" />
                <CurveTo cx="119.80" cy="61.40" x="119.90" y="66.80" />
                <LineTo x="121.45" y="158.45" />
                <CurveTo cx="121.50" cy="160.25" x="118.90" y="161.55" />
                <CurveTo cx="116.65" cy="162.65" x="114.40" y="162.70" />
                <LineTo x="46.45" y="163.40" />
                <CurveTo cx="41.95" cy="163.40" x="41.75" y="158.70" />
                <LineTo x="39.65" y="66.10" />
                <CurveTo cx="39.60" cy="64.30" x="42.00" y="63.15" />
                <CurveTo cx="44.00" cy="62.15" x="46.30" y="62.10" />

                <MoveTo x="77.80" y="228.85" />
                <LineTo x="91.10" y="228.85" />
                <CurveTo cx="93.65" cy="228.85" x="95.50" y="230.70" />
                <CurveTo cx="97.30" cy="232.50" x="97.30" y="235.05" />
                <CurveTo cx="97.30" cy="237.60" x="95.50" y="239.40" />
                <CurveTo cx="93.65" cy="241.20" x="91.10" y="241.20" />
                <LineTo x="77.80" y="241.20" />
                <CurveTo cx="75.25" cy="241.20" x="73.45" y="239.40" />
                <CurveTo cx="71.65" cy="237.60" x="71.65" y="235.05" />
                <CurveTo cx="71.65" cy="232.50" x="73.45" y="230.70" />
                <CurveTo cx="75.25" cy="228.85" x="77.80" y="228.85" />

                <MoveTo x="47.25" y="221.50" />
                <LineTo x="60.55" y="221.50" />
                <CurveTo cx="63.10" cy="221.50" x="64.95" y="223.30" />
                <CurveTo cx="66.75" cy="225.10" x="66.75" y="227.65" />
                <CurveTo cx="66.75" cy="230.20" x="64.95" y="232.05" />
                <CurveTo cx="63.10" cy="233.85" x="60.55" y="233.85" />
                <LineTo x="47.25" y="233.85" />
                <CurveTo cx="44.70" cy="233.85" x="42.90" y="232.05" />
                <CurveTo cx="41.10" cy="230.20" x="41.10" y="227.65" />
                <CurveTo cx="41.10" cy="225.10" x="42.90" y="223.30" />
                <CurveTo cx="44.70" cy="221.50" x="47.25" y="221.50" />

                <MoveTo x="109.85" y="221.35" />
                <LineTo x="122.75" y="221.35" />
                <CurveTo cx="125.20" cy="221.35" x="126.95" y="223.10" />
                <CurveTo cx="128.70" cy="224.90" x="128.70" y="227.35" />
                <CurveTo cx="128.70" cy="229.80" x="126.95" y="231.55" />
                <CurveTo cx="125.20" cy="233.30" x="122.75" y="233.30" />
                <LineTo x="109.85" y="233.30" />
                <CurveTo cx="107.35" cy="233.30" x="105.60" y="231.55" />
                <CurveTo cx="103.85" cy="229.80" x="103.85" y="227.35" />
                <CurveTo cx="103.85" cy="224.90" x="105.60" y="223.10" />
                <CurveTo cx="107.40" cy="221.35" x="109.85" y="221.35" />

                <MoveTo x="109.75" y="237.80" />
                <LineTo x="122.50" y="237.80" />
                <CurveTo cx="124.95" cy="237.80" x="126.70" y="239.65" />
                <CurveTo cx="128.45" cy="241.45" x="128.45" y="244.00" />
                <CurveTo cx="128.45" cy="246.50" x="126.70" y="248.35" />
                <CurveTo cx="124.95" cy="250.15" x="122.50" y="250.15" />
                <LineTo x="109.75" y="250.15" />
                <CurveTo cx="107.30" cy="250.15" x="105.55" y="248.35" />
                <CurveTo cx="103.80" cy="246.50" x="103.80" y="244.00" />
                <CurveTo cx="103.80" cy="241.45" x="105.55" y="239.65" />
                <CurveTo cx="107.30" cy="237.80" x="109.75" y="237.80" />

                <MoveTo x="78.35" y="245.20" />
                <LineTo x="91.65" y="245.20" />
                <CurveTo cx="94.20" cy="245.20" x="96.05" y="247.05" />
                <CurveTo cx="97.85" cy="248.85" x="97.85" y="251.40" />
                <CurveTo cx="97.85" cy="253.90" x="96.05" y="255.75" />
                <CurveTo cx="94.20" cy="257.55" x="91.65" y="257.55" />
                <LineTo x="78.35" y="257.55" />
                <CurveTo cx="75.80" cy="257.55" x="74.00" y="255.75" />
                <CurveTo cx="72.20" cy="253.90" x="72.20" y="251.40" />
                <CurveTo cx="72.20" cy="248.85" x="74.00" y="247.05" />
                <CurveTo cx="75.80" cy="245.20" x="78.35" y="245.20" />

                <MoveTo x="47.40" y="237.75" />
                <LineTo x="60.70" y="237.75" />
                <CurveTo cx="63.25" cy="237.75" x="65.05" y="239.55" />
                <CurveTo cx="66.85" cy="241.40" x="66.85" y="243.90" />
                <CurveTo cx="66.85" cy="246.45" x="65.05" y="248.30" />
                <CurveTo cx="63.25" cy="250.10" x="60.70" y="250.10" />
                <LineTo x="47.40" y="250.10" />
                <CurveTo cx="44.85" cy="250.10" x="43.05" y="248.30" />
                <CurveTo cx="41.20" cy="246.45" x="41.20" y="243.90" />
                <CurveTo cx="41.20" cy="241.40" x="43.05" y="239.55" />
                <CurveTo cx="44.85" cy="237.75" x="47.40" y="237.75" />

                <MoveTo x="47.45" y="255.25" />
                <LineTo x="60.75" y="255.25" />
                <CurveTo cx="63.30" cy="255.25" x="65.15" y="257.05" />
                <CurveTo cx="66.95" cy="258.90" x="66.95" y="261.40" />
                <CurveTo cx="66.95" cy="263.95" x="65.15" y="265.80" />
                <CurveTo cx="63.30" cy="267.60" x="60.75" y="267.60" />
                <LineTo x="47.45" y="267.60" />
                <CurveTo cx="44.90" cy="267.60" x="43.10" y="265.80" />
                <CurveTo cx="41.30" cy="263.95" x="41.30" y="261.40" />
                <CurveTo cx="41.30" cy="258.90" x="43.10" y="257.05" />
                <CurveTo cx="44.90" cy="255.25" x="47.45" y="255.25" />

                <MoveTo x="78.65" y="261.80" />
                <LineTo x="91.95" y="261.80" />
                <CurveTo cx="94.50" cy="261.80" x="96.30" y="263.60" />
                <CurveTo cx="98.10" cy="265.40" x="98.10" y="267.95" />
                <CurveTo cx="98.10" cy="270.50" x="96.30" y="272.30" />
                <CurveTo cx="94.50" cy="274.10" x="91.95" y="274.10" />
                <LineTo x="78.65" y="274.10" />
                <CurveTo cx="76.10" cy="274.10" x="74.30" y="272.30" />
                <CurveTo cx="72.45" cy="270.50" x="72.45" y="267.95" />
                <CurveTo cx="72.45" cy="265.40" x="74.30" y="263.60" />
                <CurveTo cx="76.10" cy="261.80" x="78.65" y="261.80" />

                <MoveTo x="79.00" y="278.50" />
                <LineTo x="92.35" y="278.50" />
                <CurveTo cx="94.90" cy="278.50" x="96.70" y="280.35" />
                <CurveTo cx="98.50" cy="282.15" x="98.50" y="284.70" />
                <CurveTo cx="98.50" cy="287.20" x="96.70" y="289.05" />
                <CurveTo cx="94.90" cy="290.85" x="92.35" y="290.85" />
                <LineTo x="79.00" y="290.85" />
                <CurveTo cx="76.50" cy="290.85" x="74.65" y="289.05" />
                <CurveTo cx="72.85" cy="287.20" x="72.85" y="284.70" />
                <CurveTo cx="72.85" cy="282.15" x="74.65" y="280.35" />
                <CurveTo cx="76.50" cy="278.50" x="79.00" y="278.50" />

                <MoveTo x="49.85" y="271.20" />
                <LineTo x="61.45" y="271.20" />
                <CurveTo cx="63.65" cy="271.20" x="65.25" y="273.00" />
                <CurveTo cx="66.85" cy="274.80" x="66.85" y="277.35" />
                <CurveTo cx="66.85" cy="279.90" x="65.25" y="281.70" />
                <CurveTo cx="63.65" cy="283.50" x="61.45" y="283.50" />
                <LineTo x="49.85" y="283.50" />
                <CurveTo cx="47.65" cy="283.50" x="46.05" y="281.70" />
                <CurveTo cx="44.45" cy="279.90" x="44.45" y="277.35" />
                <CurveTo cx="44.45" cy="274.80" x="46.05" y="273.00" />
                <CurveTo cx="47.65" cy="271.20" x="49.85" y="271.20" />

                <MoveTo x="109.85" y="270.90" />
                <LineTo x="121.45" y="270.90" />
                <CurveTo cx="123.70" cy="270.90" x="125.30" y="272.70" />
                <CurveTo cx="126.85" cy="274.50" x="126.85" y="277.05" />
                <CurveTo cx="126.85" cy="279.60" x="125.30" y="281.45" />
                <CurveTo cx="123.70" cy="283.25" x="121.45" y="283.25" />
                <LineTo x="109.85" y="283.25" />
                <CurveTo cx="107.65" cy="283.25" x="106.05" y="281.45" />
                <CurveTo cx="104.50" cy="279.60" x="104.50" y="277.05" />
                <CurveTo cx="104.50" cy="274.50" x="106.05" y="272.70" />
                <CurveTo cx="107.65" cy="270.90" x="109.85" y="270.90" />

                <MoveTo x="78.70" y="213.35" />
                <LineTo x="90.95" y="213.35" />
                <CurveTo cx="93.30" cy="213.35" x="95.00" y="214.75" />
                <CurveTo cx="96.65" cy="216.10" x="96.65" y="218.05" />
                <CurveTo cx="96.65" cy="220.00" x="95.00" y="221.40" />
                <CurveTo cx="93.30" cy="222.80" x="90.95" y="222.80" />
                <LineTo x="78.70" y="222.80" />
                <CurveTo cx="76.35" cy="222.80" x="74.65" y="221.40" />
                <CurveTo cx="73.00" cy="220.00" x="73.00" y="218.05" />
                <CurveTo cx="73.00" cy="216.10" x="74.65" y="214.75" />
                <CurveTo cx="76.35" cy="213.35" x="78.70" y="213.35" />

                <MoveTo x="46.65" y="178.90" />
                <CurveTo cx="49.75" cy="178.90" x="51.95" y="181.10" />
                <CurveTo cx="54.15" cy="183.35" x="54.15" y="186.40" />
                <LineTo x="54.15" y="207.10" />
                <CurveTo cx="54.15" cy="210.20" x="51.95" y="212.40" />
                <CurveTo cx="49.75" cy="214.60" x="46.65" y="214.60" />
                <CurveTo cx="43.55" cy="214.60" x="41.35" y="212.40" />
                <CurveTo cx="39.15" cy="210.20" x="39.15" y="207.10" />
                <LineTo x="39.15" y="186.40" />
                <CurveTo cx="39.15" cy="183.35" x="41.35" y="181.10" />
                <CurveTo cx="43.60" cy="178.90" x="46.65" y="178.90" />

                <MoveTo x="123.25" y="178.15" />
                <CurveTo cx="126.35" cy="178.15" x="128.60" y="180.40" />
                <CurveTo cx="130.80" cy="182.60" x="130.80" y="185.75" />
                <LineTo x="130.80" y="206.70" />
                <CurveTo cx="130.80" cy="209.85" x="128.60" y="212.10" />
                <CurveTo cx="126.35" cy="214.30" x="123.25" y="214.30" />
                <CurveTo cx="120.10" cy="214.30" x="117.90" y="212.10" />
                <CurveTo cx="115.65" cy="209.85" x="115.65" y="206.70" />
                <LineTo x="115.65" y="185.75" />
                <CurveTo cx="115.65" cy="182.60" x="117.90" y="180.40" />
                <CurveTo cx="120.10" cy="178.15" x="123.25" y="178.15" />

                <MoveTo x="78.60" y="177.65" />
                <LineTo x="91.20" y="177.65" />
                <CurveTo cx="96.80" cy="177.65" x="100.80" y="181.50" />
                <CurveTo cx="104.80" cy="185.35" x="104.80" y="190.80" />
                <LineTo x="104.80" y="197.25" />
                <CurveTo cx="104.80" cy="202.70" x="100.80" y="206.55" />
                <CurveTo cx="96.80" cy="210.40" x="91.20" y="210.40" />
                <LineTo x="78.60" y="210.40" />
                <CurveTo cx="73.00" cy="210.40" x="69.00" y="206.55" />
                <CurveTo cx="65.00" cy="202.70" x="65.00" y="197.25" />
                <LineTo x="65.00" y="190.80" />
                <CurveTo cx="65.00" cy="185.35" x="69.00" y="181.50" />
                <CurveTo cx="73.00" cy="177.65" x="78.60" y="177.65" />

                <MoveTo x="109.80" y="254.15" />
                <LineTo x="122.65" y="254.15" />
                <CurveTo cx="125.10" cy="254.15" x="126.85" y="255.95" />
                <CurveTo cx="128.60" cy="257.75" x="128.60" y="260.30" />
                <CurveTo cx="128.60" cy="262.85" x="126.85" y="264.70" />
                <CurveTo cx="125.10" cy="266.50" x="122.65" y="266.50" />
                <LineTo x="109.80" y="266.50" />
                <CurveTo cx="107.35" cy="266.50" x="105.60" y="264.70" />
                <CurveTo cx="103.85" cy="262.85" x="103.85" y="260.30" />
                <CurveTo cx="103.85" cy="257.75" x="105.60" y="255.95" />
                <CurveTo cx="107.35" cy="254.15" x="109.80" y="254.15" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (20, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 20
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="12.15" y="20.05" />
                <CurveTo cx="13.85" cy="22.85" x="13.85" y="27.90" />
                <LineTo x="13.85" y="28.65" />
                <LineTo x="13.85" y="237.95" />
                <CurveTo cx="13.85" cy="242.15" x="13.70" y="244.25" />
                <CurveTo cx="13.15" cy="252.65" x="11.45" y="259.30" />
                <CurveTo cx="8.55" cy="266.90" x="4.55" y="268.20" />
                <CurveTo cx="0.20" cy="269.55" x="0.40" y="259.55" />
                <LineTo x="0.40" y="27.15" />
                <CurveTo cx="0.00" cy="19.80" x="3.75" y="16.55" />
                <CurveTo cx="7.80" cy="13.00" x="12.15" y="20.05" />
                <MoveTo x="135.25" y="111.25" />
                <LineTo x="134.45" y="182.60" />
                <LineTo x="133.65" y="253.90" />
                <CurveTo cx="132.20" cy="262.65" x="129.25" y="268.30" />
                <CurveTo cx="121.80" cy="279.40" x="107.90" y="284.25" />
                <CurveTo cx="93.10" cy="289.40" x="69.75" y="288.00" />
                <CurveTo cx="43.40" cy="288.90" x="26.20" y="285.00" />
                <CurveTo cx="20.35" cy="282.85" x="15.50" y="279.55" />
                <CurveTo cx="12.00" cy="277.15" x="10.50" y="274.05" />
                <CurveTo cx="8.80" cy="270.60" x="10.90" y="267.05" />
                <CurveTo cx="12.35" cy="264.60" x="13.05" y="262.65" />
                <CurveTo cx="13.40" cy="261.60" x="13.75" y="258.55" />
                <CurveTo cx="13.95" cy="256.95" x="14.05" y="256.20" />
                <CurveTo cx="15.35" cy="248.35" x="15.20" y="235.55" />
                <LineTo x="15.50" y="90.50" />
                <LineTo x="16.10" y="90.50" />
                <LineTo x="30.25" y="90.65" />
                <LineTo x="44.30" y="90.75" />
                <CurveTo cx="45.45" cy="90.75" x="46.15" y="92.35" />
                <CurveTo cx="47.00" cy="94.45" x="48.75" y="94.90" />
                <LineTo x="66.50" y="95.10" />
                <LineTo x="90.75" y="95.00" />
                <CurveTo cx="92.25" cy="94.65" x="93.35" y="92.90" />
                <CurveTo cx="94.60" cy="91.00" x="96.50" y="91.00" />
                <LineTo x="106.25" y="91.10" />
                <LineTo x="114.45" y="91.15" />
                <LineTo x="124.40" y="91.25" />
                <CurveTo cx="132.25" cy="91.30" x="134.35" y="95.60" />
                <CurveTo cx="135.70" cy="98.35" x="135.35" y="106.90" />
                <CurveTo cx="135.25" cy="109.75" x="135.25" y="111.25" />
                <MoveTo x="20.05" y="4.75" />
                <CurveTo cx="13.10" cy="7.85" x="12.05" y="11.10" />
                <CurveTo cx="11.40" cy="13.15" x="13.30" y="17.35" />
                <CurveTo cx="15.20" cy="21.60" x="15.25" y="24.20" />
                <CurveTo cx="15.30" cy="25.20" x="15.15" y="27.35" />
                <CurveTo cx="15.05" cy="29.25" x="15.05" y="30.20" />
                <LineTo x="15.05" y="36.50" />
                <LineTo x="15.10" y="46.50" />
                <LineTo x="15.15" y="64.30" />
                <LineTo x="15.15" y="83.15" />
                <CurveTo cx="15.15" cy="83.95" x="15.10" y="85.25" />
                <CurveTo cx="15.00" cy="88.10" x="18.85" y="88.10" />
                <LineTo x="31.70" y="88.15" />
                <LineTo x="44.50" y="88.20" />
                <CurveTo cx="46.55" cy="88.20" x="47.25" y="86.10" />
                <CurveTo cx="48.05" cy="83.80" x="50.25" y="83.40" />
                <LineTo x="66.95" y="83.70" />
                <LineTo x="81.30" y="83.75" />
                <LineTo x="82.25" y="83.75" />
                <CurveTo cx="85.90" cy="83.65" x="87.90" y="83.75" />
                <CurveTo cx="91.65" cy="83.90" x="92.20" y="84.50" />
                <CurveTo cx="92.75" cy="85.15" x="93.30" y="86.40" />
                <CurveTo cx="94.20" cy="88.40" x="96.00" y="88.40" />
                <LineTo x="111.75" y="88.50" />
                <LineTo x="127.50" y="88.55" />
                <CurveTo cx="132.95" cy="88.55" x="134.00" y="87.65" />
                <CurveTo cx="134.90" cy="86.85" x="134.70" y="82.95" />
                <CurveTo cx="134.55" cy="80.45" x="134.55" y="78.85" />
                <LineTo x="134.55" y="57.55" />
                <LineTo x="133.95" y="51.80" />
                <CurveTo cx="131.55" cy="29.30" x="130.05" y="22.45" />
                <CurveTo cx="127.80" cy="12.45" x="123.10" y="8.75" />
                <CurveTo cx="115.90" cy="4.55" x="101.15" y="2.35" />
                <CurveTo cx="88.25" cy="0.40" x="71.35" y="0.30" />
                <CurveTo cx="37.70" cy="0.00" x="20.05" y="4.75" />

                <MoveTo x="102.90" y="63.45" />
                <LineTo x="112.40" y="63.45" />
                <CurveTo cx="114.55" cy="63.45" x="114.55" y="65.60" />
                <LineTo x="114.55" y="75.10" />
                <CurveTo cx="114.55" cy="77.25" x="112.40" y="77.25" />
                <LineTo x="102.90" y="77.25" />
                <CurveTo cx="100.75" cy="77.25" x="100.75" y="75.10" />
                <LineTo x="100.75" y="65.60" />
                <CurveTo cx="100.75" cy="63.45" x="102.90" y="63.45" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (21, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 21
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 3" x="50" y="420">
            <Contour x="0" y="0">
                <MoveTo x="8.50" y="298.85" />
                <CurveTo cx="10.95" cy="298.70" x="16.85" y="298.45" />
                <CurveTo cx="25.45" cy="298.05" x="30.05" y="297.80" />
                <CurveTo cx="37.90" cy="297.35" x="43.70" y="296.75" />
                <CurveTo cx="59.00" cy="295.25" x="63.85" y="292.65" />
                <CurveTo cx="68.50" cy="290.10" x="72.30" y="283.60" />
                <CurveTo cx="75.50" cy="278.00" x="76.85" y="272.05" />
                <CurveTo cx="79.10" cy="262.35" x="79.25" y="247.15" />
                <CurveTo cx="80.10" cy="237.55" x="80.65" y="224.80" />
                <CurveTo cx="81.10" cy="214.65" x="81.50" y="200.20" />
                <LineTo x="81.50" y="83.70" />
                <CurveTo cx="82.05" cy="69.45" x="81.60" y="57.85" />
                <LineTo x="81.50" y="54.85" />
                <CurveTo cx="81.10" cy="43.75" x="80.60" y="38.00" />
                <CurveTo cx="79.70" cy="28.05" x="77.85" y="21.90" />
                <CurveTo cx="74.75" cy="11.80" x="67.55" y="6.40" />
                <CurveTo cx="59.05" cy="0.00" x="45.70" y="0.90" />
                <CurveTo cx="42.70" cy="0.90" x="36.85" y="0.95" />
                <LineTo x="25.35" y="1.10" />
                <LineTo x="11.40" y="1.25" />
                <LineTo x="8.75" y="1.25" />
                <CurveTo cx="2.95" cy="1.30" x="1.90" y="1.95" />
                <CurveTo cx="0.70" cy="2.65" x="0.80" y="6.45" />
                <CurveTo cx="4.05" cy="15.40" x="6.40" y="31.00" />
                <CurveTo cx="8.40" cy="44.60" x="9.85" y="64.50" />
                <LineTo x="9.85" y="65.80" />
                <CurveTo cx="9.80" cy="67.85" x="11.60" y="67.75" />
                <CurveTo cx="12.70" cy="67.70" x="13.50" y="67.70" />
                <LineTo x="28.10" y="67.65" />
                <LineTo x="42.70" y="67.65" />
                <LineTo x="71.90" y="67.65" />
                <CurveTo cx="75.15" cy="67.65" x="75.00" y="71.25" />
                <CurveTo cx="74.95" cy="72.40" x="74.95" y="73.05" />
                <CurveTo cx="74.95" cy="73.70" x="75.00" y="74.80" />
                <CurveTo cx="75.10" cy="78.75" x="71.10" y="78.75" />
                <CurveTo cx="62.45" cy="78.75" x="45.05" y="78.60" />
                <LineTo x="29.35" y="78.45" />
                <CurveTo cx="20.60" cy="78.40" x="13.65" y="78.40" />
                <LineTo x="12.60" y="78.40" />
                <CurveTo cx="9.85" cy="78.35" x="9.85" y="81.50" />
                <LineTo x="9.85" y="82.05" />
                <LineTo x="9.85" y="118.70" />
                <LineTo x="9.85" y="120.15" />
                <CurveTo cx="9.75" cy="122.60" x="13.00" y="122.70" />
                <LineTo x="13.25" y="122.70" />
                <LineTo x="17.05" y="122.80" />
                <LineTo x="17.45" y="122.80" />
                <CurveTo cx="21.10" cy="122.90" x="21.15" y="126.60" />
                <LineTo x="21.15" y="126.90" />
                <LineTo x="21.15" y="168.95" />
                <LineTo x="21.15" y="170.35" />
                <CurveTo cx="21.20" cy="172.65" x="19.20" y="172.60" />
                <CurveTo cx="18.05" cy="172.55" x="17.20" y="172.55" />
                <LineTo x="16.00" y="172.55" />
                <LineTo x="16.35" y="172.55" />
                <LineTo x="15.15" y="172.55" />
                <CurveTo cx="14.40" cy="172.55" x="13.15" y="172.55" />
                <CurveTo cx="9.85" cy="172.50" x="9.85" y="176.90" />
                <LineTo x="9.85" y="177.20" />
                <LineTo x="9.85" y="212.60" />
                <CurveTo cx="9.85" cy="213.35" x="9.80" y="214.45" />
                <CurveTo cx="9.75" cy="216.95" x="12.35" y="216.85" />
                <LineTo x="14.00" y="216.85" />
                <LineTo x="42.25" y="216.85" />
                <LineTo x="56.35" y="216.85" />
                <LineTo x="70.50" y="216.80" />
                <CurveTo cx="74.75" cy="216.80" x="74.70" y="221.10" />
                <LineTo x="74.70" y="222.00" />
                <CurveTo cx="74.70" cy="222.75" x="74.75" y="224.00" />
                <CurveTo cx="74.95" cy="227.40" x="71.85" y="227.45" />
                <LineTo x="57.45" y="227.45" />
                <LineTo x="43.10" y="227.45" />
                <LineTo x="28.70" y="227.45" />
                <LineTo x="14.30" y="227.45" />
                <CurveTo cx="13.35" cy="227.45" x="12.05" y="227.40" />
                <CurveTo cx="9.75" cy="227.30" x="9.80" y="229.80" />
                <CurveTo cx="9.85" cy="230.90" x="9.85" y="231.60" />
                <CurveTo cx="10.00" cy="244.20" x="7.75" y="258.30" />
                <CurveTo cx="6.45" cy="266.75" x="2.75" y="283.05" />
                <CurveTo cx="1.10" cy="290.55" x="0.30" y="294.30" />
                <CurveTo cx="0.00" cy="297.70" x="0.90" y="298.30" />
                <CurveTo cx="1.80" cy="298.85" x="7.30" y="298.85" />
                <LineTo x="8.50" y="298.85" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (22, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 22
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 4" x="300" y="420">
            <Contour x="0" y="0">
                <MoveTo x="23.50" y="0.60" />
                <CurveTo cx="25.80" cy="0.75" x="30.50" y="0.95" />
                <CurveTo cx="43.55" cy="1.55" x="49.35" y="2.30" />
                <CurveTo cx="59.10" cy="3.50" x="64.05" y="6.20" />
                <CurveTo cx="68.70" cy="8.70" x="72.50" y="15.25" />
                <CurveTo cx="75.75" cy="20.85" x="77.10" y="26.80" />
                <CurveTo cx="79.35" cy="36.95" x="79.45" y="51.70" />
                <CurveTo cx="80.30" cy="61.40" x="80.90" y="74.05" />
                <CurveTo cx="81.40" cy="84.65" x="81.75" y="98.65" />
                <LineTo x="81.75" y="215.15" />
                <CurveTo cx="82.20" cy="229.75" x="81.80" y="241.00" />
                <LineTo x="81.70" y="244.00" />
                <CurveTo cx="81.30" cy="255.10" x="80.80" y="260.85" />
                <CurveTo cx="79.90" cy="270.80" x="78.05" y="276.95" />
                <CurveTo cx="75.00" cy="287.05" x="67.75" y="292.50" />
                <CurveTo cx="59.25" cy="298.85" x="45.90" y="297.95" />
                <LineTo x="18.45" y="297.95" />
                <CurveTo cx="8.90" cy="297.95" x="5.35" y="296.25" />
                <CurveTo cx="0.30" cy="293.90" x="1.15" y="286.75" />
                <LineTo x="0.85" y="9.15" />
                <CurveTo cx="0.00" cy="2.75" x="5.20" y="1.05" />
                <CurveTo cx="8.35" cy="0.00" x="17.50" y="0.40" />
                <CurveTo cx="21.50" cy="0.60" x="23.50" y="0.60" />

                <MoveTo x="21.00" y="22.80" />
                <LineTo x="22.40" y="22.80" />
                <CurveTo cx="25.00" cy="22.80" x="26.80" y="24.65" />
                <CurveTo cx="28.65" cy="26.50" x="28.65" y="29.10" />
                <LineTo x="28.65" y="34.00" />
                <CurveTo cx="28.65" cy="36.60" x="26.80" y="38.40" />
                <CurveTo cx="25.00" cy="40.25" x="22.40" y="40.25" />
                <LineTo x="21.00" y="40.25" />
                <CurveTo cx="18.40" cy="40.25" x="16.55" y="38.40" />
                <CurveTo cx="14.70" cy="36.60" x="14.70" y="34.00" />
                <LineTo x="14.70" y="29.10" />
                <CurveTo cx="14.70" cy="26.50" x="16.55" y="24.65" />
                <CurveTo cx="18.40" cy="22.80" x="21.00" y="22.80" />

                <MoveTo x="40.80" y="25.50" />
                <LineTo x="42.20" y="25.50" />
                <CurveTo cx="44.80" cy="25.50" x="46.60" y="27.35" />
                <CurveTo cx="48.45" cy="29.20" x="48.45" y="31.80" />
                <LineTo x="48.45" y="36.70" />
                <CurveTo cx="48.45" cy="39.30" x="46.60" y="41.10" />
                <CurveTo cx="44.80" cy="42.95" x="42.20" y="42.95" />
                <LineTo x="40.80" y="42.95" />
                <CurveTo cx="38.20" cy="42.95" x="36.35" y="41.10" />
                <CurveTo cx="34.50" cy="39.30" x="34.50" y="36.70" />
                <LineTo x="34.50" y="31.80" />
                <CurveTo cx="34.50" cy="29.20" x="36.35" y="27.35" />
                <CurveTo cx="38.20" cy="25.50" x="40.80" y="25.50" />

                <MoveTo x="21.25" y="46.95" />
                <LineTo x="22.65" y="46.95" />
                <CurveTo cx="25.25" cy="46.95" x="27.10" y="48.80" />
                <CurveTo cx="28.95" cy="50.60" x="28.95" y="53.20" />
                <LineTo x="28.95" y="58.10" />
                <CurveTo cx="28.95" cy="60.70" x="27.10" y="62.50" />
                <CurveTo cx="25.25" cy="64.35" x="22.65" y="64.35" />
                <LineTo x="21.25" y="64.35" />
                <CurveTo cx="18.65" cy="64.35" x="16.85" y="62.50" />
                <CurveTo cx="15.00" cy="60.70" x="15.00" y="58.10" />
                <LineTo x="15.00" y="53.20" />
                <CurveTo cx="15.00" cy="50.60" x="16.85" y="48.80" />
                <CurveTo cx="18.65" cy="46.95" x="21.25" y="46.95" />

                <MoveTo x="40.40" y="48.50" />
                <LineTo x="41.80" y="48.50" />
                <CurveTo cx="44.40" cy="48.50" x="46.25" y="50.35" />
                <CurveTo cx="48.10" cy="52.15" x="48.10" y="54.75" />
                <LineTo x="48.10" y="59.65" />
                <CurveTo cx="48.10" cy="62.25" x="46.25" y="64.10" />
                <CurveTo cx="44.40" cy="65.95" x="41.80" y="65.95" />
                <LineTo x="40.40" y="65.95" />
                <CurveTo cx="37.80" cy="65.95" x="36.00" y="64.10" />
                <CurveTo cx="34.15" cy="62.25" x="34.15" y="59.65" />
                <LineTo x="34.15" y="54.75" />
                <CurveTo cx="34.15" cy="52.15" x="36.00" y="50.35" />
                <CurveTo cx="37.80" cy="48.50" x="40.40" y="48.50" />

                <MoveTo x="21.00" y="70.70" />
                <LineTo x="22.40" y="70.70" />
                <CurveTo cx="25.00" cy="70.70" x="26.85" y="72.55" />
                <CurveTo cx="28.70" cy="74.40" x="28.70" y="77.00" />
                <LineTo x="28.70" y="81.90" />
                <CurveTo cx="28.70" cy="84.50" x="26.85" y="86.30" />
                <CurveTo cx="25.00" cy="88.15" x="22.40" y="88.15" />
                <LineTo x="21.00" y="88.15" />
                <CurveTo cx="18.40" cy="88.15" x="16.55" y="86.30" />
                <CurveTo cx="14.70" cy="84.50" x="14.70" y="81.90" />
                <LineTo x="14.70" y="77.00" />
                <CurveTo cx="14.70" cy="74.40" x="16.55" y="72.55" />
                <CurveTo cx="18.40" cy="70.70" x="21.00" y="70.70" />

                <MoveTo x="40.50" y="72.20" />
                <LineTo x="41.90" y="72.20" />
                <CurveTo cx="44.50" cy="72.20" x="46.30" y="74.05" />
                <CurveTo cx="48.15" cy="75.90" x="48.15" y="78.50" />
                <LineTo x="48.15" y="83.40" />
                <CurveTo cx="48.15" cy="86.00" x="46.30" y="87.80" />
                <CurveTo cx="44.50" cy="89.65" x="41.90" y="89.65" />
                <LineTo x="40.50" y="89.65" />
                <CurveTo cx="37.90" cy="89.65" x="36.05" y="87.80" />
                <CurveTo cx="34.20" cy="86.00" x="34.20" y="83.40" />
                <LineTo x="34.20" y="78.50" />
                <CurveTo cx="34.20" cy="75.90" x="36.05" y="74.05" />
                <CurveTo cx="37.90" cy="72.20" x="40.50" y="72.20" />

                <MoveTo x="59.95" y="28.85" />
                <LineTo x="61.35" y="28.85" />
                <CurveTo cx="63.95" cy="28.85" x="65.75" y="31.30" />
                <CurveTo cx="67.60" cy="33.75" x="67.60" y="37.20" />
                <LineTo x="67.60" y="43.70" />
                <CurveTo cx="67.60" cy="47.15" x="65.75" y="49.60" />
                <CurveTo cx="63.95" cy="52.05" x="61.35" y="52.05" />
                <LineTo x="59.95" y="52.05" />
                <CurveTo cx="57.35" cy="52.05" x="55.50" y="49.60" />
                <CurveTo cx="53.65" cy="47.15" x="53.65" y="43.70" />
                <LineTo x="53.65" y="37.20" />
                <CurveTo cx="53.65" cy="33.75" x="55.50" y="31.30" />
                <CurveTo cx="57.35" cy="28.85" x="59.95" y="28.85" />

                <MoveTo x="59.75" y="57.25" />
                <LineTo x="61.35" y="57.25" />
                <CurveTo cx="63.95" cy="57.25" x="65.75" y="59.10" />
                <CurveTo cx="67.60" cy="60.95" x="67.60" y="63.55" />
                <LineTo x="67.60" y="85.50" />
                <CurveTo cx="67.60" cy="88.10" x="65.75" y="89.95" />
                <CurveTo cx="63.95" cy="91.80" x="61.35" y="91.80" />
                <LineTo x="59.75" y="91.80" />
                <CurveTo cx="57.15" cy="91.80" x="55.35" y="89.95" />
                <CurveTo cx="53.50" cy="88.10" x="53.50" y="85.50" />
                <LineTo x="53.50" y="63.55" />
                <CurveTo cx="53.50" cy="60.95" x="55.35" y="59.10" />
                <CurveTo cx="57.15" cy="57.25" x="59.75" y="57.25" />

                <MoveTo x="21.15" y="94.95" />
                <LineTo x="22.55" y="94.95" />
                <CurveTo cx="25.15" cy="94.95" x="27.00" y="96.80" />
                <CurveTo cx="28.85" cy="98.60" x="28.85" y="101.20" />
                <LineTo x="28.85" y="106.10" />
                <CurveTo cx="28.85" cy="108.70" x="27.00" y="110.50" />
                <CurveTo cx="25.15" cy="112.35" x="22.55" y="112.35" />
                <LineTo x="21.15" y="112.35" />
                <CurveTo cx="18.55" cy="112.35" x="16.75" y="110.50" />
                <CurveTo cx="14.90" cy="108.70" x="14.90" y="106.10" />
                <LineTo x="14.90" y="101.20" />
                <CurveTo cx="14.90" cy="98.60" x="16.75" y="96.80" />
                <CurveTo cx="18.55" cy="94.95" x="21.15" y="94.95" />

                <MoveTo x="40.30" y="96.00" />
                <LineTo x="41.70" y="96.00" />
                <CurveTo cx="44.30" cy="96.00" x="46.15" y="97.85" />
                <CurveTo cx="48.00" cy="99.65" x="48.00" y="102.25" />
                <LineTo x="48.00" y="107.15" />
                <CurveTo cx="48.00" cy="109.75" x="46.15" y="111.55" />
                <CurveTo cx="44.30" cy="113.40" x="41.70" y="113.40" />
                <LineTo x="40.30" y="113.40" />
                <CurveTo cx="37.70" cy="113.40" x="35.90" y="111.55" />
                <CurveTo cx="34.05" cy="109.75" x="34.05" y="107.15" />
                <LineTo x="34.05" y="102.25" />
                <CurveTo cx="34.05" cy="99.65" x="35.90" y="97.85" />
                <CurveTo cx="37.70" cy="96.00" x="40.30" y="96.00" />

                <MoveTo x="60.45" y="97.05" />
                <LineTo x="61.85" y="97.05" />
                <CurveTo cx="64.45" cy="97.05" x="66.30" y="98.90" />
                <CurveTo cx="68.15" cy="100.70" x="68.15" y="103.30" />
                <LineTo x="68.15" y="108.20" />
                <CurveTo cx="68.15" cy="110.80" x="66.30" y="112.60" />
                <CurveTo cx="64.45" cy="114.45" x="61.85" y="114.45" />
                <LineTo x="60.45" y="114.45" />
                <CurveTo cx="57.85" cy="114.45" x="56.05" y="112.60" />
                <CurveTo cx="54.20" cy="110.80" x="54.20" y="108.20" />
                <LineTo x="54.20" y="103.30" />
                <CurveTo cx="54.20" cy="100.70" x="56.05" y="98.90" />
                <CurveTo cx="57.85" cy="97.05" x="60.45" y="97.05" />

                <MoveTo x="20.90" y="118.50" />
                <LineTo x="22.30" y="118.50" />
                <CurveTo cx="24.90" cy="118.50" x="26.75" y="120.35" />
                <CurveTo cx="28.60" cy="122.15" x="28.60" y="124.75" />
                <LineTo x="28.60" y="129.65" />
                <CurveTo cx="28.60" cy="132.25" x="26.75" y="134.05" />
                <CurveTo cx="24.90" cy="135.90" x="22.30" y="135.90" />
                <LineTo x="20.90" y="135.90" />
                <CurveTo cx="18.30" cy="135.90" x="16.45" y="134.05" />
                <CurveTo cx="14.60" cy="132.25" x="14.60" y="129.65" />
                <LineTo x="14.60" y="124.75" />
                <CurveTo cx="14.60" cy="122.15" x="16.45" y="120.35" />
                <CurveTo cx="18.30" cy="118.50" x="20.90" y="118.50" />

                <MoveTo x="39.90" y="118.90" />
                <LineTo x="41.30" y="118.90" />
                <CurveTo cx="43.90" cy="118.90" x="45.75" y="120.75" />
                <CurveTo cx="47.60" cy="122.55" x="47.60" y="125.15" />
                <LineTo x="47.60" y="130.05" />
                <CurveTo cx="47.60" cy="132.65" x="45.75" y="134.45" />
                <CurveTo cx="43.90" cy="136.30" x="41.30" y="136.30" />
                <LineTo x="39.90" y="136.30" />
                <CurveTo cx="37.30" cy="136.30" x="35.50" y="134.45" />
                <CurveTo cx="33.65" cy="132.65" x="33.65" y="130.05" />
                <LineTo x="33.65" y="125.15" />
                <CurveTo cx="33.65" cy="122.55" x="35.50" y="120.75" />
                <CurveTo cx="37.30" cy="118.90" x="39.90" y="118.90" />

                <MoveTo x="60.65" y="119.50" />
                <LineTo x="62.05" y="119.50" />
                <CurveTo cx="64.65" cy="119.50" x="66.45" y="121.35" />
                <CurveTo cx="68.30" cy="123.20" x="68.30" y="125.80" />
                <LineTo x="68.30" y="130.70" />
                <CurveTo cx="68.30" cy="133.30" x="66.45" y="135.10" />
                <CurveTo cx="64.65" cy="136.95" x="62.05" y="136.95" />
                <LineTo x="60.65" y="136.95" />
                <CurveTo cx="58.05" cy="136.95" x="56.20" y="135.10" />
                <CurveTo cx="54.35" cy="133.30" x="54.35" y="130.70" />
                <LineTo x="54.35" y="125.80" />
                <CurveTo cx="54.35" cy="123.20" x="56.20" y="121.35" />
                <CurveTo cx="58.05" cy="119.50" x="60.65" y="119.50" />

                <MoveTo x="21.10" y="142.35" />
                <LineTo x="22.50" y="142.35" />
                <CurveTo cx="25.10" cy="142.35" x="26.95" y="144.20" />
                <CurveTo cx="28.80" cy="146.05" x="28.80" y="148.65" />
                <LineTo x="28.80" y="153.55" />
                <CurveTo cx="28.80" cy="156.15" x="26.95" y="157.95" />
                <CurveTo cx="25.10" cy="159.80" x="22.50" y="159.80" />
                <LineTo x="21.10" y="159.80" />
                <CurveTo cx="18.50" cy="159.80" x="16.70" y="157.95" />
                <CurveTo cx="14.85" cy="156.15" x="14.85" y="153.55" />
                <LineTo x="14.85" y="148.65" />
                <CurveTo cx="14.85" cy="146.05" x="16.70" y="144.20" />
                <CurveTo cx="18.50" cy="142.35" x="21.10" y="142.35" />

                <MoveTo x="41.20" y="143.05" />
                <LineTo x="42.60" y="143.05" />
                <CurveTo cx="45.20" cy="143.05" x="47.00" y="144.90" />
                <CurveTo cx="48.85" cy="146.70" x="48.85" y="149.30" />
                <LineTo x="48.85" y="154.20" />
                <CurveTo cx="48.85" cy="156.80" x="47.00" y="158.60" />
                <CurveTo cx="45.20" cy="160.45" x="42.60" y="160.45" />
                <LineTo x="41.20" y="160.45" />
                <CurveTo cx="38.60" cy="160.45" x="36.75" y="158.60" />
                <CurveTo cx="34.90" cy="156.80" x="34.90" y="154.20" />
                <LineTo x="34.90" y="149.30" />
                <CurveTo cx="34.90" cy="146.70" x="36.75" y="144.90" />
                <CurveTo cx="38.60" cy="143.05" x="41.20" y="143.05" />

                <MoveTo x="60.45" y="142.20" />
                <LineTo x="61.85" y="142.20" />
                <CurveTo cx="64.45" cy="142.20" x="66.30" y="144.05" />
                <CurveTo cx="68.15" cy="145.85" x="68.15" y="148.45" />
                <LineTo x="68.15" y="153.35" />
                <CurveTo cx="68.15" cy="155.95" x="66.30" y="157.80" />
                <CurveTo cx="64.45" cy="159.65" x="61.85" y="159.65" />
                <LineTo x="60.45" y="159.65" />
                <CurveTo cx="57.85" cy="159.65" x="56.05" y="157.80" />
                <CurveTo cx="54.20" cy="155.95" x="54.20" y="153.35" />
                <LineTo x="54.20" y="148.45" />
                <CurveTo cx="54.20" cy="145.85" x="56.05" y="144.05" />
                <CurveTo cx="57.85" cy="142.20" x="60.45" y="142.20" />

                <MoveTo x="60.65" y="165.05" />
                <LineTo x="62.05" y="165.05" />
                <CurveTo cx="64.65" cy="165.05" x="66.45" y="166.90" />
                <CurveTo cx="68.30" cy="168.70" x="68.30" y="171.30" />
                <LineTo x="68.30" y="176.20" />
                <CurveTo cx="68.30" cy="178.80" x="66.45" y="180.65" />
                <CurveTo cx="64.65" cy="182.50" x="62.05" y="182.50" />
                <LineTo x="60.65" y="182.50" />
                <CurveTo cx="58.05" cy="182.50" x="56.20" y="180.65" />
                <CurveTo cx="54.35" cy="178.80" x="54.35" y="176.20" />
                <LineTo x="54.35" y="171.30" />
                <CurveTo cx="54.35" cy="168.70" x="56.20" y="166.90" />
                <CurveTo cx="58.05" cy="165.05" x="60.65" y="165.05" />

                <MoveTo x="41.65" y="165.95" />
                <LineTo x="43.05" y="165.95" />
                <CurveTo cx="45.65" cy="165.95" x="47.50" y="167.80" />
                <CurveTo cx="49.35" cy="169.65" x="49.35" y="172.25" />
                <LineTo x="49.35" y="177.10" />
                <CurveTo cx="49.35" cy="179.70" x="47.50" y="181.55" />
                <CurveTo cx="45.65" cy="183.40" x="43.05" y="183.40" />
                <LineTo x="41.65" y="183.40" />
                <CurveTo cx="39.05" cy="183.40" x="37.25" y="181.55" />
                <CurveTo cx="35.40" cy="179.70" x="35.40" y="177.10" />
                <LineTo x="35.40" y="172.25" />
                <CurveTo cx="35.40" cy="169.65" x="37.25" y="167.80" />
                <CurveTo cx="39.05" cy="165.95" x="41.65" y="165.95" />

                <MoveTo x="21.65" y="166.85" />
                <LineTo x="23.05" y="166.85" />
                <CurveTo cx="25.65" cy="166.85" x="27.50" y="168.70" />
                <CurveTo cx="29.35" cy="170.50" x="29.35" y="173.10" />
                <LineTo x="29.35" y="178.00" />
                <CurveTo cx="29.35" cy="180.60" x="27.50" y="182.40" />
                <CurveTo cx="25.65" cy="184.25" x="23.05" y="184.25" />
                <LineTo x="21.65" y="184.25" />
                <CurveTo cx="19.05" cy="184.25" x="17.25" y="182.40" />
                <CurveTo cx="15.40" cy="180.60" x="15.40" y="178.00" />
                <LineTo x="15.40" y="173.10" />
                <CurveTo cx="15.40" cy="170.50" x="17.25" y="168.70" />
                <CurveTo cx="19.05" cy="166.85" x="21.65" y="166.85" />

                <MoveTo x="21.70" y="190.30" />
                <LineTo x="23.10" y="190.30" />
                <CurveTo cx="25.70" cy="190.30" x="27.55" y="192.15" />
                <CurveTo cx="29.40" cy="193.95" x="29.40" y="196.55" />
                <LineTo x="29.40" y="201.45" />
                <CurveTo cx="29.40" cy="204.05" x="27.55" y="205.85" />
                <CurveTo cx="25.70" cy="207.70" x="23.10" y="207.70" />
                <LineTo x="21.70" y="207.70" />
                <CurveTo cx="19.10" cy="207.70" x="17.30" y="205.85" />
                <CurveTo cx="15.45" cy="204.05" x="15.45" y="201.45" />
                <LineTo x="15.45" y="196.55" />
                <CurveTo cx="15.45" cy="193.95" x="17.30" y="192.15" />
                <CurveTo cx="19.10" cy="190.30" x="21.70" y="190.30" />

                <MoveTo x="41.65" y="189.70" />
                <LineTo x="43.05" y="189.70" />
                <CurveTo cx="45.65" cy="189.70" x="47.50" y="191.55" />
                <CurveTo cx="49.35" cy="193.35" x="49.35" y="195.95" />
                <LineTo x="49.35" y="200.85" />
                <CurveTo cx="49.35" cy="203.45" x="47.50" y="205.25" />
                <CurveTo cx="45.65" cy="207.10" x="43.05" y="207.10" />
                <LineTo x="41.65" y="207.10" />
                <CurveTo cx="39.05" cy="207.10" x="37.25" y="205.25" />
                <CurveTo cx="35.40" cy="203.45" x="35.40" y="200.85" />
                <LineTo x="35.40" y="195.95" />
                <CurveTo cx="35.40" cy="193.35" x="37.25" y="191.55" />
                <CurveTo cx="39.05" cy="189.70" x="41.65" y="189.70" />

                <MoveTo x="61.00" y="188.30" />
                <LineTo x="62.35" y="188.30" />
                <CurveTo cx="64.95" cy="188.30" x="66.80" y="190.15" />
                <CurveTo cx="68.65" cy="191.95" x="68.65" y="194.55" />
                <LineTo x="68.65" y="199.45" />
                <CurveTo cx="68.65" cy="202.05" x="66.80" y="203.85" />
                <CurveTo cx="64.95" cy="205.70" x="62.35" y="205.70" />
                <LineTo x="61.00" y="205.70" />
                <CurveTo cx="58.40" cy="205.70" x="56.55" y="203.85" />
                <CurveTo cx="54.70" cy="202.05" x="54.70" y="199.45" />
                <LineTo x="54.70" y="194.55" />
                <CurveTo cx="54.70" cy="191.95" x="56.55" y="190.15" />
                <CurveTo cx="58.40" cy="188.30" x="61.00" y="188.30" />

                <MoveTo x="21.60" y="214.70" />
                <LineTo x="23.00" y="214.70" />
                <CurveTo cx="25.60" cy="214.70" x="27.40" y="216.55" />
                <CurveTo cx="29.25" cy="218.40" x="29.25" y="221.00" />
                <LineTo x="29.25" y="225.85" />
                <CurveTo cx="29.25" cy="228.45" x="27.40" y="230.30" />
                <CurveTo cx="25.60" cy="232.15" x="23.00" y="232.15" />
                <LineTo x="21.60" y="232.15" />
                <CurveTo cx="19.00" cy="232.15" x="17.15" y="230.30" />
                <CurveTo cx="15.30" cy="228.45" x="15.30" y="225.85" />
                <LineTo x="15.30" y="221.00" />
                <CurveTo cx="15.30" cy="218.40" x="17.15" y="216.55" />
                <CurveTo cx="19.00" cy="214.70" x="21.60" y="214.70" />

                <MoveTo x="41.70" y="212.90" />
                <LineTo x="43.10" y="212.90" />
                <CurveTo cx="45.70" cy="212.90" x="47.50" y="214.75" />
                <CurveTo cx="49.35" cy="216.60" x="49.35" y="219.20" />
                <LineTo x="49.35" y="224.05" />
                <CurveTo cx="49.35" cy="226.65" x="47.50" y="228.50" />
                <CurveTo cx="45.70" cy="230.35" x="43.10" y="230.35" />
                <LineTo x="41.70" y="230.35" />
                <CurveTo cx="39.10" cy="230.35" x="37.25" y="228.50" />
                <CurveTo cx="35.40" cy="226.65" x="35.40" y="224.05" />
                <LineTo x="35.40" y="219.20" />
                <CurveTo cx="35.40" cy="216.60" x="37.25" y="214.75" />
                <CurveTo cx="39.10" cy="212.90" x="41.70" y="212.90" />

                <MoveTo x="61.15" y="211.20" />
                <LineTo x="62.55" y="211.20" />
                <CurveTo cx="65.15" cy="211.20" x="67.00" y="213.05" />
                <CurveTo cx="68.85" cy="214.90" x="68.85" y="217.50" />
                <LineTo x="68.85" y="222.40" />
                <CurveTo cx="68.85" cy="225.00" x="67.00" y="226.80" />
                <CurveTo cx="65.15" cy="228.65" x="62.55" y="228.65" />
                <LineTo x="61.15" y="228.65" />
                <CurveTo cx="58.55" cy="228.65" x="56.75" y="226.80" />
                <CurveTo cx="54.90" cy="225.00" x="54.90" y="222.40" />
                <LineTo x="54.90" y="217.50" />
                <CurveTo cx="54.90" cy="214.90" x="56.75" y="213.05" />
                <CurveTo cx="58.55" cy="211.20" x="61.15" y="211.20" />

                <MoveTo x="22.05" y="238.10" />
                <LineTo x="23.45" y="238.10" />
                <CurveTo cx="26.05" cy="238.10" x="27.90" y="239.95" />
                <CurveTo cx="29.75" cy="241.80" x="29.75" y="244.40" />
                <LineTo x="29.75" y="249.25" />
                <CurveTo cx="29.75" cy="251.85" x="27.90" y="253.70" />
                <CurveTo cx="26.05" cy="255.55" x="23.45" y="255.55" />
                <LineTo x="22.05" y="255.55" />
                <CurveTo cx="19.45" cy="255.55" x="17.65" y="253.70" />
                <CurveTo cx="15.80" cy="251.85" x="15.80" y="249.25" />
                <LineTo x="15.80" y="244.40" />
                <CurveTo cx="15.80" cy="241.80" x="17.65" y="239.95" />
                <CurveTo cx="19.45" cy="238.10" x="22.05" y="238.10" />

                <MoveTo x="41.65" y="236.15" />
                <LineTo x="43.05" y="236.15" />
                <CurveTo cx="45.65" cy="236.15" x="47.50" y="238.00" />
                <CurveTo cx="49.35" cy="239.85" x="49.35" y="242.45" />
                <LineTo x="49.35" y="247.30" />
                <CurveTo cx="49.35" cy="249.90" x="47.50" y="251.75" />
                <CurveTo cx="45.65" cy="253.60" x="43.05" y="253.60" />
                <LineTo x="41.65" y="253.60" />
                <CurveTo cx="39.05" cy="253.60" x="37.25" y="251.75" />
                <CurveTo cx="35.40" cy="249.90" x="35.40" y="247.30" />
                <LineTo x="35.40" y="242.45" />
                <CurveTo cx="35.40" cy="239.85" x="37.25" y="238.00" />
                <CurveTo cx="39.05" cy="236.15" x="41.65" y="236.15" />

                <MoveTo x="61.40" y="233.75" />
                <LineTo x="62.80" y="233.75" />
                <CurveTo cx="65.40" cy="233.75" x="67.20" y="235.60" />
                <CurveTo cx="69.05" cy="237.45" x="69.05" y="240.05" />
                <LineTo x="69.05" y="244.90" />
                <CurveTo cx="69.05" cy="247.50" x="67.20" y="249.35" />
                <CurveTo cx="65.40" cy="251.20" x="62.80" y="251.20" />
                <LineTo x="61.40" y="251.20" />
                <CurveTo cx="58.80" cy="251.20" x="56.95" y="249.35" />
                <CurveTo cx="55.10" cy="247.50" x="55.10" y="244.90" />
                <LineTo x="55.10" y="240.05" />
                <CurveTo cx="55.10" cy="237.45" x="56.95" y="235.60" />
                <CurveTo cx="58.80" cy="233.75" x="61.40" y="233.75" />

                <MoveTo x="22.35" y="262.30" />
                <LineTo x="23.75" y="262.30" />
                <CurveTo cx="26.35" cy="262.30" x="28.20" y="264.15" />
                <CurveTo cx="30.05" cy="266.00" x="30.05" y="268.60" />
                <LineTo x="30.05" y="273.50" />
                <CurveTo cx="30.05" cy="276.10" x="28.20" y="277.90" />
                <CurveTo cx="26.35" cy="279.75" x="23.75" y="279.75" />
                <LineTo x="22.35" y="279.75" />
                <CurveTo cx="19.75" cy="279.75" x="17.95" y="277.90" />
                <CurveTo cx="16.10" cy="276.10" x="16.10" y="273.50" />
                <LineTo x="16.10" y="268.60" />
                <CurveTo cx="16.10" cy="266.00" x="17.95" y="264.15" />
                <CurveTo cx="19.75" cy="262.30" x="22.35" y="262.30" />

                <MoveTo x="41.80" y="259.30" />
                <LineTo x="43.20" y="259.30" />
                <CurveTo cx="45.80" cy="259.30" x="47.65" y="261.15" />
                <CurveTo cx="49.50" cy="263.00" x="49.50" y="265.60" />
                <LineTo x="49.50" y="270.45" />
                <CurveTo cx="49.50" cy="273.05" x="47.65" y="274.90" />
                <CurveTo cx="45.80" cy="276.75" x="43.20" y="276.75" />
                <LineTo x="41.80" y="276.75" />
                <CurveTo cx="39.20" cy="276.75" x="37.40" y="274.90" />
                <CurveTo cx="35.55" cy="273.05" x="35.55" y="270.45" />
                <LineTo x="35.55" y="265.60" />
                <CurveTo cx="35.55" cy="263.00" x="37.40" y="261.15" />
                <CurveTo cx="39.20" cy="259.30" x="41.80" y="259.30" />

                <MoveTo x="61.00" y="256.95" />
                <LineTo x="62.35" y="256.95" />
                <CurveTo cx="64.95" cy="256.95" x="66.80" y="258.80" />
                <CurveTo cx="68.65" cy="260.60" x="68.65" y="263.20" />
                <LineTo x="68.65" y="268.10" />
                <CurveTo cx="68.65" cy="270.70" x="66.80" y="272.50" />
                <CurveTo cx="64.95" cy="274.35" x="62.35" y="274.35" />
                <LineTo x="61.00" y="274.35" />
                <CurveTo cx="58.40" cy="274.35" x="56.55" y="272.50" />
                <CurveTo cx="54.70" cy="270.70" x="54.70" y="268.10" />
                <LineTo x="54.70" y="263.20" />
                <CurveTo cx="54.70" cy="260.60" x="56.55" y="258.80" />
                <CurveTo cx="58.40" cy="256.95" x="61.00" y="256.95" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (23, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 23
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="27.50" y="24.00" />
                <LineTo x="27.95" y="0.05" />
                <LineTo x="120.85" y="0.00" />
                <LineTo x="121.25" y="24.60" />
                <LineTo x="144.15" y="31.95" />
                <LineTo x="139.80" y="217.50" />
                <CurveTo cx="139.20" cy="248.45" x="107.25" y="255.80" />
                <CurveTo cx="91.00" cy="259.55" x="71.10" y="259.55" />
                <CurveTo cx="51.20" cy="259.55" x="36.45" y="255.80" />
                <CurveTo cx="4.70" cy="247.80" x="3.90" y="217.95" />
                <LineTo x="0.00" y="31.35" />
                <LineTo x="27.50" y="24.00" />

                <MoveTo x="34.55" y="77.75" />
                <LineTo x="108.15" y="77.75" />
                <CurveTo cx="110.90" cy="77.75" x="110.90" y="80.20" />
                <LineTo x="110.90" y="125.45" />
                <CurveTo cx="110.90" cy="127.90" x="108.15" y="127.90" />
                <LineTo x="34.55" y="127.90" />
                <CurveTo cx="31.75" cy="127.90" x="31.75" y="125.45" />
                <LineTo x="31.75" y="80.20" />
                <CurveTo cx="31.75" cy="77.75" x="34.55" y="77.75" />

                <MoveTo x="70.50" y="66.85" />
                <CurveTo cx="80.10" cy="66.85" x="86.95" y="60.05" />
                <CurveTo cx="93.75" cy="53.20" x="93.75" y="43.60" />
                <CurveTo cx="93.75" cy="34.00" x="86.95" y="27.20" />
                <CurveTo cx="80.10" cy="20.35" x="70.50" y="20.35" />
                <CurveTo cx="60.90" cy="20.35" x="54.10" y="27.20" />
                <CurveTo cx="47.30" cy="34.00" x="47.30" y="43.60" />
                <CurveTo cx="47.30" cy="53.20" x="54.10" y="60.05" />
                <CurveTo cx="60.90" cy="66.85" x="70.50" y="66.85" />

                <MoveTo x="27.40" y="167.35" />
                <LineTo x="28.65" y="168.40" />
                <LineTo x="29.95" y="169.45" />
                <LineTo x="30.45" y="169.85" />
                <CurveTo cx="32.20" cy="171.25" x="32.80" y="172.00" />
                <CurveTo cx="33.60" cy="173.10" x="33.60" y="174.55" />
                <LineTo x="34.70" y="194.80" />
                <LineTo x="34.70" y="195.05" />
                <CurveTo cx="34.70" cy="196.75" x="34.00" y="197.10" />
                <CurveTo cx="33.35" cy="197.40" x="32.70" y="197.15" />
                <CurveTo cx="32.30" cy="196.95" x="31.65" y="196.40" />
                <CurveTo cx="29.90" cy="195.00" x="28.65" y="192.85" />
                <CurveTo cx="27.55" cy="190.95" x="26.85" y="188.50" />
                <CurveTo cx="26.15" cy="186.10" x="26.15" y="183.70" />
                <LineTo x="26.20" y="175.85" />
                <LineTo x="26.25" y="168.00" />
                <CurveTo cx="26.25" cy="167.65" x="26.60" y="167.35" />
                <CurveTo cx="27.00" cy="167.05" x="27.40" y="167.35" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (24, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 24
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="21.45" y="185.15" />
                <CurveTo cx="67.75" cy="191.20" x="117.25" y="184.90" />
                <CurveTo cx="117.75" cy="184.85" x="119.00" y="184.65" />
                <CurveTo cx="122.30" cy="184.10" x="123.70" y="184.20" />
                <CurveTo cx="126.10" cy="184.30" x="126.15" y="185.85" />
                <CurveTo cx="126.25" cy="191.70" x="124.60" y="200.95" />
                <CurveTo cx="123.65" cy="206.45" x="121.10" y="217.80" />
                <CurveTo cx="120.15" cy="222.10" x="119.70" y="224.20" />
                <CurveTo cx="119.00" cy="227.45" x="114.75" y="229.75" />
                <CurveTo cx="92.50" cy="242.15" x="68.95" y="241.80" />
                <CurveTo cx="45.85" cy="241.45" x="23.95" y="228.95" />
                <CurveTo cx="23.45" cy="228.65" x="22.50" y="228.20" />
                <CurveTo cx="19.05" cy="226.40" x="18.55" y="224.20" />
                <CurveTo cx="18.00" cy="221.90" x="16.80" y="217.05" />
                <CurveTo cx="14.25" cy="207.10" x="13.35" y="202.10" />
                <CurveTo cx="11.80" cy="193.60" x="12.30" y="187.60" />
                <CurveTo cx="12.45" cy="185.70" x="15.85" y="185.20" />
                <CurveTo cx="18.35" cy="184.80" x="21.45" y="185.15" />
                <MoveTo x="17.45" y="3.00" />
                <CurveTo cx="21.00" cy="2.60" x="26.85" y="1.90" />
                <CurveTo cx="34.00" cy="1.00" x="37.60" y="0.70" />
                <CurveTo cx="43.40" cy="0.15" x="49.95" y="0.00" />
                <LineTo x="49.90" y="29.45" />
                <CurveTo cx="49.80" cy="31.05" x="50.65" y="32.55" />
                <CurveTo cx="51.55" cy="34.20" x="52.90" y="34.15" />
                <LineTo x="81.00" y="34.20" />
                <CurveTo cx="82.55" cy="34.15" x="83.15" y="32.35" />
                <CurveTo cx="83.60" cy="31.05" x="83.60" y="28.95" />
                <LineTo x="83.50" y="0.00" />
                <CurveTo cx="93.50" cy="0.20" x="99.00" y="0.60" />
                <CurveTo cx="104.10" cy="0.95" x="114.55" y="2.10" />
                <CurveTo cx="118.50" cy="2.50" x="119.30" y="7.00" />
                <CurveTo cx="123.25" cy="30.10" x="125.75" y="50.05" />
                <CurveTo cx="128.65" cy="72.80" x="129.85" y="93.20" />
                <CurveTo cx="132.55" cy="139.60" x="126.85" y="178.55" />
                <CurveTo cx="126.70" cy="179.65" x="123.45" y="180.95" />
                <CurveTo cx="120.80" cy="182.05" x="119.10" y="182.25" />
                <CurveTo cx="92.35" cy="185.40" x="67.40" y="185.55" />
                <CurveTo cx="41.05" cy="185.70" x="16.60" y="182.50" />
                <CurveTo cx="15.35" cy="182.35" x="13.50" y="181.00" />
                <CurveTo cx="11.45" cy="179.60" x="11.30" y="178.40" />
                <CurveTo cx="0.00" cy="93.75" x="14.20" y="7.40" />
                <CurveTo cx="14.95" cy="3.25" x="17.45" y="3.00" />

                <MoveTo x="23.70" y="35.90" />
                <CurveTo cx="27.70" cy="35.90" x="30.55" y="33.05" />
                <CurveTo cx="33.40" cy="30.20" x="33.40" y="26.20" />
                <CurveTo cx="33.40" cy="22.20" x="30.55" y="19.35" />
                <CurveTo cx="27.70" cy="16.50" x="23.70" y="16.50" />
                <CurveTo cx="19.70" cy="16.50" x="16.85" y="19.35" />
                <CurveTo cx="14.00" cy="22.20" x="14.00" y="26.20" />
                <CurveTo cx="14.00" cy="30.20" x="16.85" y="33.05" />
                <CurveTo cx="19.70" cy="35.90" x="23.70" y="35.90" />

                <MoveTo x="55.25" y="191.10" />
                <LineTo x="61.00" y="193.25" />
                <CurveTo cx="62.35" cy="193.75" x="62.35" y="194.40" />
                <LineTo x="62.35" y="219.30" />
                <CurveTo cx="62.35" cy="220.00" x="61.00" y="220.40" />
                <LineTo x="55.25" y="222.25" />
                <CurveTo cx="54.20" cy="222.60" x="53.45" y="222.80" />
                <CurveTo cx="52.20" cy="223.10" x="52.20" y="222.70" />
                <LineTo x="52.05" y="190.50" />
                <CurveTo cx="52.05" cy="190.15" x="53.25" y="190.45" />
                <CurveTo cx="53.90" cy="190.60" x="55.10" y="191.05" />
                <LineTo x="55.25" y="191.10" />

                <MoveTo x="82.45" y="190.80" />
                <LineTo x="76.70" y="192.95" />
                <LineTo x="76.65" y="192.95" />
                <CurveTo cx="75.35" cy="193.45" x="75.35" y="194.05" />
                <LineTo x="75.35" y="219.00" />
                <CurveTo cx="75.35" cy="219.70" x="76.70" y="220.10" />
                <LineTo x="82.45" y="221.95" />
                <CurveTo cx="83.50" cy="222.30" x="84.25" y="222.50" />
                <CurveTo cx="85.50" cy="222.80" x="85.50" y="222.40" />
                <LineTo x="85.70" y="190.20" />
                <CurveTo cx="85.70" cy="189.85" x="84.45" y="190.15" />
                <CurveTo cx="83.75" cy="190.30" x="82.55" y="190.75" />
                <LineTo x="82.45" y="190.80" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (25, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 25
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 3" x="50" y="420">
            <Contour x="0" y="0">
                <MoveTo x="128.40" y="224.05" />
                <LineTo x="126.30" y="33.75" />
                <CurveTo cx="126.10" cy="15.00" x="106.75" y="6.55" />
                <CurveTo cx="98.75" cy="3.05" x="87.60" y="1.40" />
                <CurveTo cx="78.15" cy="0.00" x="66.70" y="0.00" />
                <LineTo x="63.20" y="0.00" />
                <CurveTo cx="38.60" cy="0.00" x="23.10" y="7.25" />
                <CurveTo cx="3.65" cy="16.35" x="3.35" y="35.20" />
                <LineTo x="0.00" y="224.10" />
                <LineTo x="22.05" y="228.00" />
                <LineTo x="22.05" y="243.95" />
                <LineTo x="109.35" y="243.95" />
                <LineTo x="108.95" y="228.10" />
                <LineTo x="128.40" y="224.05" />

                <MoveTo x="39.20" y="23.70" />
                <LineTo x="90.45" y="23.70" />
                <CurveTo cx="92.75" cy="23.70" x="94.35" y="25.15" />
                <CurveTo cx="96.00" cy="26.60" x="96.00" y="28.65" />
                <CurveTo cx="96.00" cy="30.70" x="94.35" y="32.15" />
                <CurveTo cx="92.75" cy="33.60" x="90.45" y="33.60" />
                <LineTo x="39.20" y="33.60" />
                <CurveTo cx="36.90" cy="33.60" x="35.25" y="32.15" />
                <CurveTo cx="33.60" cy="30.70" x="33.60" y="28.65" />
                <CurveTo cx="33.60" cy="26.65" x="35.25" y="25.15" />
                <CurveTo cx="36.90" cy="23.70" x="39.20" y="23.70" />

                <MoveTo x="117.00" y="63.50" />
                <LineTo x="117.00" y="189.95" />
                <LineTo x="14.00" y="189.95" />
                <LineTo x="14.00" y="63.50" />
                <LineTo x="117.00" y="63.50" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (26, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 26
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 4" x="300" y="420">
            <Contour x="0" y="0">
                <MoveTo x="131.90" y="15.15" />
                <CurveTo cx="132.90" cy="36.85" x="133.15" y="60.25" />
                <CurveTo cx="133.40" cy="81.35" x="133.05" y="105.45" />
                <CurveTo cx="132.70" cy="125.85" x="131.90" y="151.80" />
                <CurveTo cx="131.40" cy="168.05" x="130.25" y="200.40" />
                <CurveTo cx="130.00" cy="205.40" x="129.60" y="207.95" />
                <CurveTo cx="128.95" cy="211.80" x="127.25" y="215.45" />
                <CurveTo cx="121.25" cy="225.20" x="105.05" y="231.25" />
                <CurveTo cx="88.65" cy="237.30" x="66.80" y="237.70" />
                <CurveTo cx="43.60" cy="238.15" x="27.45" y="231.55" />
                <CurveTo cx="11.65" cy="225.10" x="5.25" y="213.00" />
                <CurveTo cx="3.00" cy="208.50" x="3.25" y="198.90" />
                <CurveTo cx="0.75" cy="147.40" x="0.40" y="104.10" />
                <CurveTo cx="0.00" cy="59.25" x="1.90" y="15.30" />
                <LineTo x="24.50" y="11.95" />
                <LineTo x="24.40" y="0.00" />
                <LineTo x="110.25" y="0.20" />
                <LineTo x="110.30" y="11.60" />
                <LineTo x="131.90" y="15.15" />

                <MoveTo x="29.70" y="17.85" />
                <CurveTo cx="67.10" cy="15.60" x="105.85" y="18.10" />
                <CurveTo cx="111.40" cy="18.45" x="115.70" y="23.20" />
                <CurveTo cx="119.90" cy="27.95" x="120.00" y="33.70" />
                <LineTo x="120.60" y="84.50" />
                <LineTo x="95.05" y="88.20" />
                <LineTo x="96.25" y="61.85" />
                <CurveTo cx="95.35" cy="51.95" x="88.25" y="45.30" />
                <CurveTo cx="80.25" cy="37.85" x="67.30" y="37.75" />
                <CurveTo cx="54.85" cy="37.70" x="47.30" y="45.10" />
                <CurveTo cx="40.70" cy="51.60" x="38.80" y="62.50" />
                <LineTo x="38.85" y="88.15" />
                <LineTo x="13.20" y="84.30" />
                <LineTo x="15.35" y="31.80" />
                <CurveTo cx="15.55" cy="26.15" x="19.85" y="22.15" />
                <CurveTo cx="24.00" cy="18.20" x="29.70" y="17.85" />

                <MoveTo x="67.45" y="90.70" />
                <CurveTo cx="77.25" cy="90.70" x="84.15" y="83.75" />
                <CurveTo cx="91.10" cy="76.85" x="91.10" y="67.05" />
                <CurveTo cx="91.10" cy="57.25" x="84.15" y="50.35" />
                <CurveTo cx="77.25" cy="43.40" x="67.45" y="43.40" />
                <CurveTo cx="57.65" cy="43.40" x="50.75" y="50.35" />
                <CurveTo cx="43.80" cy="57.25" x="43.80" y="67.05" />
                <CurveTo cx="43.80" cy="76.85" x="50.75" y="83.75" />
                <CurveTo cx="57.65" cy="90.70" x="67.45" y="90.70" />

                <MoveTo x="8.55" y="199.75" />
                <LineTo x="10.75" y="199.40" />
                <CurveTo cx="12.00" cy="199.20" x="13.00" y="200.15" />
                <CurveTo cx="14.00" cy="201.05" x="14.20" y="202.50" />
                <LineTo x="15.10" y="208.75" />
                <CurveTo cx="15.30" cy="210.20" x="14.55" y="211.40" />
                <CurveTo cx="13.85" cy="212.55" x="12.60" y="212.75" />
                <LineTo x="10.40" y="213.10" />
                <CurveTo cx="9.20" cy="213.30" x="8.20" y="212.40" />
                <CurveTo cx="7.20" cy="211.45" x="6.95" y="210.00" />
                <LineTo x="6.10" y="203.75" />
                <CurveTo cx="5.90" cy="202.30" x="6.60" y="201.10" />
                <CurveTo cx="7.35" cy="199.95" x="8.55" y="199.75" />

                <MoveTo x="13.80" y="89.30" />
                <CurveTo cx="40.45" cy="93.45" x="66.60" y="93.75" />
                <CurveTo cx="95.40" cy="94.10" x="120.80" y="89.60" />
                <CurveTo cx="120.40" cy="113.45" x="119.05" y="140.60" />
                <CurveTo cx="117.90" cy="163.35" x="115.95" y="191.60" />
                <CurveTo cx="89.50" cy="198.80" x="66.45" y="198.80" />
                <CurveTo cx="43.20" cy="198.85" x="17.60" y="191.60" />
                <CurveTo cx="14.30" cy="141.20" x="13.80" y="89.30" />

                <MoveTo x="108.85" y="206.75" />
                <CurveTo cx="110.10" cy="206.75" x="111.00" y="205.85" />
                <CurveTo cx="111.90" cy="205.00" x="111.90" y="203.75" />
                <CurveTo cx="111.90" cy="202.50" x="111.00" y="201.60" />
                <CurveTo cx="110.15" cy="200.70" x="108.85" y="200.70" />
                <CurveTo cx="107.60" cy="200.70" x="106.75" y="201.60" />
                <CurveTo cx="105.85" cy="202.50" x="105.85" y="203.75" />
                <CurveTo cx="105.85" cy="205.00" x="106.75" y="205.85" />
                <CurveTo cx="107.60" cy="206.75" x="108.85" y="206.75" />

                <MoveTo x="125.20" y="199.20" />
                <LineTo x="123.05" y="198.85" />
                <CurveTo cx="121.85" cy="198.65" x="120.85" y="199.65" />
                <CurveTo cx="119.90" cy="200.65" x="119.70" y="202.25" />
                <LineTo x="118.85" y="209.05" />
                <CurveTo cx="118.65" cy="210.65" x="119.40" y="211.95" />
                <CurveTo cx="120.10" cy="213.20" x="121.25" y="213.40" />
                <LineTo x="123.40" y="213.75" />
                <CurveTo cx="124.55" cy="214.00" x="125.55" y="213.00" />
                <CurveTo cx="126.55" cy="212.00" x="126.75" y="210.40" />
                <LineTo x="127.60" y="203.55" />
                <CurveTo cx="127.80" cy="201.95" x="127.10" y="200.70" />
                <CurveTo cx="126.35" cy="199.40" x="125.20" y="199.20" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (27, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 27
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="75.25" y="1.65" />
                <CurveTo cx="107.85" cy="5.80" x="118.50" y="12.10" />
                <CurveTo cx="122.15" cy="14.25" x="123.05" y="17.00" />
                <CurveTo cx="123.65" cy="18.80" x="123.45" y="23.30" />
                <LineTo x="123.35" y="25.45" />
                <CurveTo cx="122.40" cy="52.90" x="122.10" y="82.85" />
                <CurveTo cx="121.85" cy="109.85" x="122.10" y="140.25" />
                <CurveTo cx="122.30" cy="166.35" x="122.90" y="197.60" />
                <CurveTo cx="123.40" cy="220.75" x="124.25" y="255.00" />
                <CurveTo cx="124.25" cy="255.95" x="124.35" y="257.65" />
                <CurveTo cx="124.55" cy="262.60" x="123.80" y="264.30" />
                <CurveTo cx="122.65" cy="266.80" x="118.00" y="269.20" />
                <CurveTo cx="110.90" cy="272.85" x="98.10" y="275.25" />
                <CurveTo cx="86.20" cy="277.40" x="69.15" y="278.55" />
                <CurveTo cx="60.60" cy="279.15" x="50.85" y="278.70" />
                <CurveTo cx="22.95" cy="277.25" x="8.95" y="271.45" />
                <CurveTo cx="3.15" cy="269.05" x="2.00" y="267.20" />
                <CurveTo cx="1.05" cy="265.65" x="1.35" y="259.90" />
                <LineTo x="1.40" y="258.55" />
                <CurveTo cx="4.50" cy="196.95" x="4.40" y="140.80" />
                <CurveTo cx="4.30" cy="80.10" x="0.50" y="23.95" />
                <LineTo x="0.40" y="22.55" />
                <CurveTo cx="0.00" cy="17.20" x="0.70" y="15.30" />
                <CurveTo cx="1.75" cy="12.40" x="6.30" y="10.15" />
                <CurveTo cx="12.15" cy="7.25" x="23.55" y="5.00" />
                <CurveTo cx="33.70" cy="3.00" x="48.90" y="1.40" />
                <CurveTo cx="62.20" cy="0.00" x="75.25" y="1.65" />

                <MoveTo x="39.70" y="270.45" />
                <CurveTo cx="41.45" cy="270.45" x="42.65" y="269.20" />
                <CurveTo cx="43.90" cy="268.00" x="43.90" y="266.25" />
                <CurveTo cx="43.90" cy="264.55" x="42.65" y="263.30" />
                <CurveTo cx="41.45" cy="262.05" x="39.70" y="262.05" />
                <CurveTo cx="38.00" cy="262.05" x="36.75" y="263.30" />
                <CurveTo cx="35.50" cy="264.55" x="35.50" y="266.25" />
                <CurveTo cx="35.50" cy="268.00" x="36.75" y="269.20" />
                <CurveTo cx="38.00" cy="270.45" x="39.70" y="270.45" />

                <MoveTo x="54.30" y="244.95" />
                <LineTo x="72.15" y="244.95" />
                <CurveTo cx="74.45" cy="244.95" x="76.15" y="246.50" />
                <CurveTo cx="77.80" cy="248.05" x="77.80" y="250.25" />
                <LineTo x="77.80" y="251.10" />
                <CurveTo cx="77.80" cy="253.30" x="76.15" y="254.85" />
                <CurveTo cx="74.50" cy="256.40" x="72.15" y="256.40" />
                <LineTo x="54.30" y="256.40" />
                <CurveTo cx="51.95" cy="256.40" x="50.30" y="254.85" />
                <CurveTo cx="48.65" cy="253.30" x="48.65" y="251.10" />
                <LineTo x="48.65" y="250.25" />
                <CurveTo cx="48.65" cy="248.05" x="50.30" y="246.50" />
                <CurveTo cx="52.00" cy="244.95" x="54.30" y="244.95" />

                <MoveTo x="54.10" y="228.20" />
                <LineTo x="71.95" y="228.20" />
                <CurveTo cx="74.30" cy="228.20" x="75.95" y="229.85" />
                <CurveTo cx="77.60" cy="231.50" x="77.60" y="233.85" />
                <LineTo x="77.60" y="234.75" />
                <CurveTo cx="77.60" cy="237.10" x="75.95" y="238.75" />
                <CurveTo cx="74.30" cy="240.40" x="71.95" y="240.40" />
                <LineTo x="54.10" y="240.40" />
                <CurveTo cx="51.80" cy="240.40" x="50.10" y="238.75" />
                <CurveTo cx="48.45" cy="237.10" x="48.45" y="234.75" />
                <LineTo x="48.45" y="233.85" />
                <CurveTo cx="48.45" cy="231.50" x="50.10" y="229.85" />
                <CurveTo cx="51.75" cy="228.20" x="54.10" y="228.20" />

                <MoveTo x="54.50" y="212.00" />
                <LineTo x="72.10" y="212.00" />
                <CurveTo cx="74.40" cy="212.00" x="76.00" y="213.55" />
                <CurveTo cx="77.65" cy="215.10" x="77.65" y="217.25" />
                <LineTo x="77.65" y="218.10" />
                <CurveTo cx="77.65" cy="220.25" x="76.00" y="221.80" />
                <CurveTo cx="74.40" cy="223.35" x="72.10" y="223.35" />
                <LineTo x="54.50" y="223.35" />
                <CurveTo cx="52.20" cy="223.35" x="50.60" y="221.80" />
                <CurveTo cx="48.95" cy="220.25" x="48.95" y="218.10" />
                <LineTo x="48.95" y="217.25" />
                <CurveTo cx="48.95" cy="215.10" x="50.60" y="213.55" />
                <CurveTo cx="52.20" cy="212.00" x="54.50" y="212.00" />

                <MoveTo x="53.50" y="194.45" />
                <LineTo x="71.35" y="194.45" />
                <CurveTo cx="73.65" cy="194.45" x="75.35" y="196.05" />
                <CurveTo cx="77.00" cy="197.65" x="77.00" y="199.85" />
                <LineTo x="77.00" y="200.75" />
                <CurveTo cx="77.00" cy="202.95" x="75.35" y="204.55" />
                <CurveTo cx="73.65" cy="206.15" x="71.35" y="206.15" />
                <LineTo x="53.50" y="206.15" />
                <CurveTo cx="51.20" cy="206.15" x="49.50" y="204.55" />
                <CurveTo cx="47.85" cy="202.95" x="47.85" y="200.75" />
                <LineTo x="47.85" y="199.85" />
                <CurveTo cx="47.85" cy="197.65" x="49.50" y="196.05" />
                <CurveTo cx="51.20" cy="194.45" x="53.50" y="194.45" />

                <MoveTo x="90.50" y="192.75" />
                <LineTo x="107.55" y="190.80" />
                <CurveTo cx="109.80" cy="190.55" x="111.55" y="191.90" />
                <CurveTo cx="113.35" cy="193.25" x="113.60" y="195.45" />
                <LineTo x="113.70" y="196.30" />
                <CurveTo cx="113.95" cy="198.45" x="112.55" y="200.20" />
                <CurveTo cx="111.15" cy="201.95" x="108.95" y="202.20" />
                <LineTo x="91.85" y="204.15" />
                <CurveTo cx="89.65" cy="204.40" x="87.85" y="203.05" />
                <CurveTo cx="86.05" cy="201.65" x="85.80" y="199.50" />
                <LineTo x="85.70" y="198.65" />
                <CurveTo cx="85.45" cy="196.45" x="86.85" y="194.75" />
                <CurveTo cx="88.25" cy="193.00" x="90.50" y="192.75" />

                <MoveTo x="90.50" y="209.75" />
                <LineTo x="107.70" y="207.75" />
                <CurveTo cx="109.95" cy="207.50" x="111.70" y="208.90" />
                <CurveTo cx="113.50" cy="210.25" x="113.75" y="212.45" />
                <LineTo x="113.85" y="213.35" />
                <CurveTo cx="114.10" cy="215.55" x="112.70" y="217.30" />
                <CurveTo cx="111.30" cy="219.05" x="109.05" y="219.30" />
                <LineTo x="91.90" y="221.25" />
                <CurveTo cx="89.65" cy="221.50" x="87.90" y="220.15" />
                <CurveTo cx="86.10" cy="218.75" x="85.85" y="216.55" />
                <LineTo x="85.70" y="215.70" />
                <CurveTo cx="85.45" cy="213.50" x="86.85" y="211.75" />
                <CurveTo cx="88.30" cy="210.00" x="90.50" y="209.75" />

                <MoveTo x="90.55" y="226.20" />
                <LineTo x="107.80" y="224.25" />
                <CurveTo cx="110.05" cy="224.00" x="111.85" y="225.35" />
                <CurveTo cx="113.65" cy="226.70" x="113.90" y="228.90" />
                <LineTo x="114.00" y="229.75" />
                <CurveTo cx="114.25" cy="231.95" x="112.85" y="233.65" />
                <CurveTo cx="111.45" cy="235.40" x="109.20" y="235.60" />
                <LineTo x="91.95" y="237.55" />
                <CurveTo cx="89.70" cy="237.80" x="87.90" y="236.45" />
                <CurveTo cx="86.10" cy="235.10" x="85.85" y="232.90" />
                <LineTo x="85.75" y="232.05" />
                <CurveTo cx="85.50" cy="229.90" x="86.90" y="228.15" />
                <CurveTo cx="88.30" cy="226.45" x="90.55" y="226.20" />

                <MoveTo x="90.70" y="243.40" />
                <LineTo x="108.05" y="241.35" />
                <CurveTo cx="110.35" cy="241.10" x="112.15" y="242.50" />
                <CurveTo cx="113.95" cy="243.90" x="114.20" y="246.20" />
                <LineTo x="114.30" y="247.10" />
                <CurveTo cx="114.55" cy="249.35" x="113.15" y="251.15" />
                <CurveTo cx="111.75" cy="252.95" x="109.45" y="253.25" />
                <LineTo x="92.10" y="255.25" />
                <CurveTo cx="89.80" cy="255.50" x="88.00" y="254.10" />
                <CurveTo cx="86.20" cy="252.70" x="85.95" y="250.40" />
                <LineTo x="85.85" y="249.50" />
                <CurveTo cx="85.60" cy="247.25" x="87.00" y="245.45" />
                <CurveTo cx="88.45" cy="243.65" x="90.70" y="243.40" />

                <MoveTo x="34.90" y="244.10" />
                <LineTo x="17.75" y="242.10" />
                <CurveTo cx="15.55" cy="241.85" x="13.75" y="243.25" />
                <CurveTo cx="11.95" cy="244.65" x="11.70" y="246.90" />
                <LineTo x="11.60" y="247.75" />
                <CurveTo cx="11.35" cy="250.00" x="12.75" y="251.75" />
                <CurveTo cx="14.15" cy="253.55" x="16.40" y="253.80" />
                <LineTo x="33.50" y="255.80" />
                <CurveTo cx="35.75" cy="256.05" x="37.50" y="254.65" />
                <CurveTo cx="39.30" cy="253.25" x="39.55" y="251.05" />
                <LineTo x="39.65" y="250.15" />
                <CurveTo cx="39.90" cy="247.95" x="38.50" y="246.15" />
                <CurveTo cx="37.10" cy="244.35" x="34.90" y="244.10" />

                <MoveTo x="35.45" y="226.80" />
                <LineTo x="18.00" y="224.75" />
                <CurveTo cx="15.75" cy="224.50" x="13.95" y="225.90" />
                <CurveTo cx="12.10" cy="227.35" x="11.85" y="229.60" />
                <LineTo x="11.75" y="230.50" />
                <CurveTo cx="11.50" cy="232.80" x="12.95" y="234.60" />
                <CurveTo cx="14.35" cy="236.40" x="16.65" y="236.65" />
                <LineTo x="34.05" y="238.70" />
                <CurveTo cx="36.35" cy="238.95" x="38.15" y="237.55" />
                <CurveTo cx="39.95" cy="236.10" x="40.20" y="233.85" />
                <LineTo x="40.30" y="232.95" />
                <CurveTo cx="40.60" cy="230.65" x="39.15" y="228.85" />
                <CurveTo cx="37.75" cy="227.05" x="35.45" y="226.80" />

                <MoveTo x="35.65" y="209.85" />
                <LineTo x="18.20" y="207.80" />
                <CurveTo cx="15.90" cy="207.55" x="14.10" y="209.00" />
                <CurveTo cx="12.25" cy="210.40" x="12.00" y="212.70" />
                <LineTo x="11.90" y="213.60" />
                <CurveTo cx="11.65" cy="215.85" x="13.10" y="217.65" />
                <CurveTo cx="14.50" cy="219.50" x="16.80" y="219.75" />
                <LineTo x="34.25" y="221.80" />
                <CurveTo cx="36.55" cy="222.05" x="38.35" y="220.65" />
                <CurveTo cx="40.15" cy="219.20" x="40.40" y="216.90" />
                <LineTo x="40.50" y="216.05" />
                <CurveTo cx="40.75" cy="213.75" x="39.35" y="211.95" />
                <CurveTo cx="37.95" cy="210.10" x="35.65" y="209.85" />

                <MoveTo x="35.35" y="192.75" />
                <LineTo x="18.00" y="190.75" />
                <CurveTo cx="15.70" cy="190.50" x="13.90" y="191.90" />
                <CurveTo cx="12.10" cy="193.30" x="11.85" y="195.55" />
                <LineTo x="11.75" y="196.45" />
                <CurveTo cx="11.50" cy="198.75" x="12.90" y="200.55" />
                <CurveTo cx="14.35" cy="202.35" x="16.60" y="202.60" />
                <LineTo x="33.95" y="204.60" />
                <CurveTo cx="36.20" cy="204.85" x="38.00" y="203.45" />
                <CurveTo cx="39.80" cy="202.05" x="40.05" y="199.80" />
                <LineTo x="40.20" y="198.90" />
                <CurveTo cx="40.45" cy="196.60" x="39.05" y="194.80" />
                <CurveTo cx="37.60" cy="193.00" x="35.35" y="192.75" />

                <MoveTo x="27.45" y="186.80" />
                <CurveTo cx="30.80" cy="186.80" x="33.15" y="184.45" />
                <CurveTo cx="35.55" cy="182.10" x="35.55" y="178.75" />
                <CurveTo cx="35.55" cy="175.45" x="33.15" y="173.10" />
                <CurveTo cx="30.80" cy="170.70" x="27.45" y="170.70" />
                <CurveTo cx="24.15" cy="170.70" x="21.80" y="173.10" />
                <CurveTo cx="19.40" cy="175.45" x="19.40" y="178.75" />
                <CurveTo cx="19.40" cy="182.10" x="21.80" y="184.45" />
                <CurveTo cx="24.15" cy="186.80" x="27.45" y="186.80" />

                <MoveTo x="98.15" y="186.80" />
                <CurveTo cx="101.45" cy="186.80" x="103.80" y="184.45" />
                <CurveTo cx="106.15" cy="182.10" x="106.15" y="178.80" />
                <CurveTo cx="106.15" cy="175.45" x="103.80" y="173.10" />
                <CurveTo cx="101.45" cy="170.75" x="98.15" y="170.75" />
                <CurveTo cx="94.80" cy="170.75" x="92.45" y="173.10" />
                <CurveTo cx="90.10" cy="175.45" x="90.10" y="178.80" />
                <CurveTo cx="90.10" cy="182.10" x="92.45" y="184.45" />
                <CurveTo cx="94.80" cy="186.80" x="98.15" y="186.80" />

                <MoveTo x="62.90" y="187.65" />
                <CurveTo cx="71.60" cy="187.65" x="77.80" y="181.50" />
                <CurveTo cx="83.95" cy="175.30" x="83.95" y="166.60" />
                <CurveTo cx="83.95" cy="157.90" x="77.80" y="151.75" />
                <CurveTo cx="71.60" cy="145.55" x="62.90" y="145.55" />
                <CurveTo cx="54.20" cy="145.55" x="48.00" y="151.75" />
                <CurveTo cx="41.80" cy="157.90" x="41.80" y="166.60" />
                <CurveTo cx="41.80" cy="175.30" x="48.00" y="181.50" />
                <CurveTo cx="54.20" cy="187.65" x="62.90" y="187.65" />

                <MoveTo x="29.50" y="158.60" />
                <CurveTo cx="32.25" cy="158.60" x="34.25" y="156.65" />
                <CurveTo cx="36.20" cy="154.70" x="36.20" y="151.95" />
                <CurveTo cx="36.20" cy="149.20" x="34.25" y="147.20" />
                <CurveTo cx="32.30" cy="145.25" x="29.50" y="145.25" />
                <CurveTo cx="26.75" cy="145.25" x="24.75" y="147.20" />
                <CurveTo cx="22.80" cy="149.20" x="22.80" y="151.95" />
                <CurveTo cx="22.80" cy="154.70" x="24.75" y="156.65" />
                <CurveTo cx="26.75" cy="158.60" x="29.50" y="158.60" />

                <MoveTo x="96.20" y="158.60" />
                <CurveTo cx="99.05" cy="158.60" x="101.10" y="156.60" />
                <CurveTo cx="103.10" cy="154.55" x="103.10" y="151.70" />
                <CurveTo cx="103.10" cy="148.85" x="101.10" y="146.85" />
                <CurveTo cx="99.05" cy="144.80" x="96.20" y="144.80" />
                <CurveTo cx="93.35" cy="144.80" x="91.35" y="146.85" />
                <CurveTo cx="89.30" cy="148.85" x="89.30" y="151.70" />
                <CurveTo cx="89.30" cy="154.55" x="91.35" y="156.60" />
                <CurveTo cx="93.35" cy="158.60" x="96.20" y="158.60" />

                <MoveTo x="23.75" y="57.30" />
                <LineTo x="102.05" y="57.30" />
                <CurveTo cx="105.35" cy="57.30" x="105.35" y="60.65" />
                <LineTo x="105.35" y="112.70" />
                <CurveTo cx="105.35" cy="116.05" x="102.05" y="116.05" />
                <LineTo x="23.75" y="116.05" />
                <CurveTo cx="20.45" cy="116.05" x="20.45" y="112.70" />
                <LineTo x="20.45" y="60.65" />
                <CurveTo cx="20.45" cy="57.30" x="23.75" y="57.30" />

                <MoveTo x="34.75" y="17.15" />
                <CurveTo cx="35.90" cy="17.15" x="36.75" y="18.00" />
                <CurveTo cx="37.60" cy="18.85" x="37.60" y="20.00" />
                <LineTo x="37.60" y="28.80" />
                <CurveTo cx="37.60" cy="30.00" x="36.75" y="30.85" />
                <CurveTo cx="35.90" cy="31.70" x="34.75" y="31.70" />
                <CurveTo cx="33.55" cy="31.70" x="32.70" y="30.85" />
                <CurveTo cx="31.85" cy="30.00" x="31.85" y="28.80" />
                <LineTo x="31.85" y="20.00" />
                <CurveTo cx="31.85" cy="18.85" x="32.70" y="18.00" />
                <CurveTo cx="33.55" cy="17.15" x="34.75" y="17.15" />

                <MoveTo x="90.30" y="16.30" />
                <CurveTo cx="91.45" cy="16.30" x="92.30" y="17.15" />
                <CurveTo cx="93.15" cy="17.95" x="93.15" y="19.15" />
                <LineTo x="93.15" y="27.95" />
                <CurveTo cx="93.15" cy="29.10" x="92.30" y="29.95" />
                <CurveTo cx="91.45" cy="30.80" x="90.30" y="30.80" />
                <CurveTo cx="89.10" cy="30.80" x="88.25" y="29.95" />
                <CurveTo cx="87.40" cy="29.10" x="87.40" y="27.95" />
                <LineTo x="87.40" y="19.15" />
                <CurveTo cx="87.40" cy="17.95" x="88.25" y="17.15" />
                <CurveTo cx="89.10" cy="16.30" x="90.30" y="16.30" />

                <MoveTo x="58.75" y="32.70" />
                <LineTo x="65.35" y="32.70" />
                <CurveTo cx="66.30" cy="32.70" x="66.95" y="33.35" />
                <CurveTo cx="67.65" cy="34.00" x="67.65" y="34.95" />
                <CurveTo cx="67.65" cy="35.90" x="66.95" y="36.55" />
                <CurveTo cx="66.30" cy="37.25" x="65.35" y="37.25" />
                <LineTo x="58.75" y="37.25" />
                <CurveTo cx="57.80" cy="37.25" x="57.15" y="36.55" />
                <CurveTo cx="56.50" cy="35.90" x="56.50" y="34.95" />
                <CurveTo cx="56.50" cy="34.00" x="57.15" y="33.35" />
                <CurveTo cx="57.80" cy="32.70" x="58.75" y="32.70" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (28, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 28
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="74.05" y="1.65" />
                <CurveTo cx="106.10" cy="5.65" x="116.65" y="11.90" />
                <CurveTo cx="120.25" cy="14.05" x="121.15" y="16.75" />
                <CurveTo cx="121.70" cy="18.50" x="121.45" y="22.95" />
                <LineTo x="121.35" y="25.00" />
                <CurveTo cx="120.40" cy="51.95" x="120.15" y="81.35" />
                <CurveTo cx="119.85" cy="107.90" x="120.10" y="137.75" />
                <CurveTo cx="120.35" cy="163.40" x="120.95" y="194.15" />
                <CurveTo cx="121.45" cy="216.90" x="122.30" y="250.50" />
                <CurveTo cx="122.30" cy="251.40" x="122.40" y="253.05" />
                <CurveTo cx="122.60" cy="257.90" x="121.85" y="259.65" />
                <CurveTo cx="120.70" cy="262.10" x="116.10" y="264.45" />
                <CurveTo cx="109.15" cy="268.05" x="96.50" y="270.35" />
                <CurveTo cx="84.85" cy="272.50" x="68.05" y="273.65" />
                <CurveTo cx="59.00" cy="274.25" x="50.00" y="273.80" />
                <CurveTo cx="22.55" cy="272.35" x="8.80" y="266.70" />
                <CurveTo cx="3.05" cy="264.30" x="1.95" y="262.45" />
                <CurveTo cx="1.00" cy="260.90" x="1.30" y="255.30" />
                <LineTo x="1.35" y="253.95" />
                <CurveTo cx="4.40" cy="193.40" x="4.30" y="138.30" />
                <CurveTo cx="4.20" cy="78.65" x="0.45" y="23.55" />
                <LineTo x="0.35" y="22.05" />
                <CurveTo cx="0.00" cy="16.90" x="0.70" y="15.00" />
                <CurveTo cx="1.70" cy="12.20" x="6.15" y="10.00" />
                <CurveTo cx="11.95" cy="7.15" x="23.15" y="4.95" />
                <CurveTo cx="33.10" cy="2.95" x="48.10" y="1.35" />
                <CurveTo cx="61.30" cy="0.00" x="74.05" y="1.65" />

                <MoveTo x="76.75" y="243.75" />
                <CurveTo cx="78.35" cy="243.75" x="79.55" y="242.60" />
                <CurveTo cx="80.70" cy="241.40" x="80.70" y="239.80" />
                <CurveTo cx="80.70" cy="238.15" x="79.55" y="237.00" />
                <CurveTo cx="78.35" cy="235.85" x="76.75" y="235.85" />
                <CurveTo cx="75.10" cy="235.85" x="73.95" y="237.00" />
                <CurveTo cx="72.80" cy="238.15" x="72.80" y="239.80" />
                <CurveTo cx="72.80" cy="241.40" x="73.95" y="242.60" />
                <CurveTo cx="75.10" cy="243.75" x="76.75" y="243.75" />

                <MoveTo x="88.25" y="233.00" />
                <CurveTo cx="89.85" cy="233.00" x="91.05" y="231.85" />
                <CurveTo cx="92.20" cy="230.65" x="92.20" y="229.05" />
                <CurveTo cx="92.20" cy="227.40" x="91.05" y="226.25" />
                <CurveTo cx="89.85" cy="225.10" x="88.25" y="225.10" />
                <CurveTo cx="86.60" cy="225.10" x="85.45" y="226.25" />
                <CurveTo cx="84.30" cy="227.40" x="84.30" y="229.05" />
                <CurveTo cx="84.30" cy="230.65" x="85.45" y="231.85" />
                <CurveTo cx="86.60" cy="233.00" x="88.25" y="233.00" />

                <MoveTo x="77.40" y="233.80" />
                <CurveTo cx="79.05" cy="233.80" x="80.20" y="232.65" />
                <CurveTo cx="81.35" cy="231.50" x="81.35" y="229.90" />
                <CurveTo cx="81.35" cy="228.25" x="80.20" y="227.10" />
                <CurveTo cx="79.05" cy="225.95" x="77.40" y="225.95" />
                <CurveTo cx="75.80" cy="225.95" x="74.60" y="227.10" />
                <CurveTo cx="73.45" cy="228.25" x="73.45" y="229.90" />
                <CurveTo cx="73.45" cy="231.50" x="74.60" y="232.65" />
                <CurveTo cx="75.80" cy="233.80" x="77.40" y="233.80" />

                <MoveTo x="87.80" y="244.15" />
                <CurveTo cx="89.40" cy="244.15" x="90.60" y="243.00" />
                <CurveTo cx="91.75" cy="241.85" x="91.75" y="240.20" />
                <CurveTo cx="91.75" cy="238.60" x="90.60" y="237.40" />
                <CurveTo cx="89.40" cy="236.25" x="87.80" y="236.25" />
                <CurveTo cx="86.15" cy="236.25" x="85.00" y="237.40" />
                <CurveTo cx="83.85" cy="238.60" x="83.85" y="240.20" />
                <CurveTo cx="83.85" cy="241.85" x="85.00" y="243.00" />
                <CurveTo cx="86.15" cy="244.15" x="87.80" y="244.15" />

                <MoveTo x="98.85" y="233.55" />
                <CurveTo cx="100.50" cy="233.55" x="101.65" y="232.40" />
                <CurveTo cx="102.80" cy="231.25" x="102.80" y="229.60" />
                <CurveTo cx="102.80" cy="228.00" x="101.65" y="226.80" />
                <CurveTo cx="100.50" cy="225.65" x="98.85" y="225.65" />
                <CurveTo cx="97.25" cy="225.65" x="96.05" y="226.80" />
                <CurveTo cx="94.90" cy="228.00" x="94.90" y="229.60" />
                <CurveTo cx="94.90" cy="231.25" x="96.05" y="232.40" />
                <CurveTo cx="97.25" cy="233.55" x="98.85" y="233.55" />

                <MoveTo x="99.40" y="244.20" />
                <CurveTo cx="101.00" cy="244.20" x="102.20" y="243.05" />
                <CurveTo cx="103.35" cy="241.90" x="103.35" y="240.25" />
                <CurveTo cx="103.35" cy="238.65" x="102.20" y="237.45" />
                <CurveTo cx="101.00" cy="236.30" x="99.40" y="236.30" />
                <CurveTo cx="97.75" cy="236.30" x="96.60" y="237.45" />
                <CurveTo cx="95.45" cy="238.65" x="95.45" y="240.25" />
                <CurveTo cx="95.45" cy="241.90" x="96.60" y="243.05" />
                <CurveTo cx="97.75" cy="244.20" x="99.40" y="244.20" />

                <MoveTo x="77.40" y="254.60" />
                <CurveTo cx="79.05" cy="254.60" x="80.20" y="253.45" />
                <CurveTo cx="81.35" cy="252.30" x="81.35" y="250.65" />
                <CurveTo cx="81.35" cy="249.05" x="80.20" y="247.85" />
                <CurveTo cx="79.05" cy="246.70" x="77.40" y="246.70" />
                <CurveTo cx="75.80" cy="246.70" x="74.60" y="247.85" />
                <CurveTo cx="73.45" cy="249.05" x="73.45" y="250.65" />
                <CurveTo cx="73.45" cy="252.30" x="74.60" y="253.45" />
                <CurveTo cx="75.80" cy="254.60" x="77.40" y="254.60" />

                <MoveTo x="87.85" y="255.20" />
                <CurveTo cx="89.45" cy="255.20" x="90.65" y="254.05" />
                <CurveTo cx="91.80" cy="252.90" x="91.80" y="251.25" />
                <CurveTo cx="91.80" cy="249.65" x="90.65" y="248.45" />
                <CurveTo cx="89.45" cy="247.30" x="87.85" y="247.30" />
                <CurveTo cx="86.20" cy="247.30" x="85.05" y="248.45" />
                <CurveTo cx="83.90" cy="249.65" x="83.90" y="251.25" />
                <CurveTo cx="83.90" cy="252.90" x="85.05" y="254.05" />
                <CurveTo cx="86.20" cy="255.20" x="87.85" y="255.20" />

                <MoveTo x="98.90" y="254.90" />
                <CurveTo cx="100.50" cy="254.90" x="101.70" y="253.75" />
                <CurveTo cx="102.85" cy="252.60" x="102.85" y="250.95" />
                <CurveTo cx="102.85" cy="249.35" x="101.70" y="248.15" />
                <CurveTo cx="100.50" cy="247.00" x="98.90" y="247.00" />
                <CurveTo cx="97.25" cy="247.00" x="96.10" y="248.15" />
                <CurveTo cx="94.95" cy="249.35" x="94.95" y="250.95" />
                <CurveTo cx="94.95" cy="252.60" x="96.10" y="253.75" />
                <CurveTo cx="97.25" cy="254.90" x="98.90" y="254.90" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (29, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 29
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="29.20" y="2.35" />
                <CurveTo cx="39.15" cy="1.35" x="45.65" y="0.85" />
                <CurveTo cx="54.40" cy="0.20" x="61.80" y="0.15" />
                <CurveTo cx="79.35" cy="0.00" x="95.65" y="3.25" />
                <CurveTo cx="108.45" cy="5.75" x="115.45" y="11.15" />
                <CurveTo cx="123.90" cy="17.60" x="123.85" y="28.00" />
                <LineTo x="123.25" y="244.25" />
                <CurveTo cx="123.20" cy="255.10" x="115.40" y="261.90" />
                <CurveTo cx="108.00" cy="268.30" x="95.95" y="269.35" />
                <CurveTo cx="78.00" cy="270.90" x="61.55" y="270.80" />
                <CurveTo cx="43.95" cy="270.70" x="27.70" y="268.75" />
                <CurveTo cx="15.70" cy="267.30" x="9.25" y="261.55" />
                <CurveTo cx="2.35" cy="255.40" x="2.25" y="244.55" />
                <LineTo x="0.10" y="27.70" />
                <CurveTo cx="0.00" cy="17.05" x="8.80" y="10.05" />
                <CurveTo cx="16.85" cy="3.65" x="29.20" y="2.35" />

                <MoveTo x="25.10" y="50.10" />
                <CurveTo cx="64.05" cy="47.55" x="102.70" y="50.10" />
                <CurveTo cx="107.85" cy="50.50" x="107.85" y="56.10" />
                <LineTo x="107.85" y="103.80" />
                <CurveTo cx="107.85" cy="109.50" x="102.70" y="109.75" />
                <CurveTo cx="78.40" cy="110.65" x="62.95" y="110.75" />
                <CurveTo cx="41.85" cy="110.90" x="25.10" y="109.75" />
                <CurveTo cx="19.15" cy="109.40" x="19.15" y="103.80" />
                <LineTo x="19.15" y="56.10" />
                <CurveTo cx="19.15" cy="50.55" x="25.10" y="50.10" />

                <MoveTo x="33.25" y="19.30" />
                <LineTo x="43.95" y="19.30" />
                <CurveTo cx="44.90" cy="19.30" x="45.55" y="19.95" />
                <CurveTo cx="46.20" cy="20.60" x="46.20" y="21.55" />
                <LineTo x="46.20" y="22.35" />
                <CurveTo cx="46.20" cy="23.25" x="45.55" y="23.95" />
                <CurveTo cx="44.90" cy="24.60" x="43.95" y="24.60" />
                <LineTo x="33.25" y="24.60" />
                <CurveTo cx="32.30" cy="24.60" x="31.65" y="23.95" />
                <CurveTo cx="31.00" cy="23.25" x="31.00" y="22.35" />
                <LineTo x="31.00" y="21.55" />
                <CurveTo cx="31.00" cy="20.60" x="31.65" y="19.95" />
                <CurveTo cx="32.30" cy="19.30" x="33.25" y="19.30" />

                <MoveTo x="83.75" y="19.40" />
                <LineTo x="94.50" y="19.40" />
                <CurveTo cx="95.40" cy="19.40" x="96.10" y="20.05" />
                <CurveTo cx="96.75" cy="20.70" x="96.75" y="21.65" />
                <LineTo x="96.75" y="22.45" />
                <CurveTo cx="96.75" cy="23.35" x="96.10" y="24.05" />
                <CurveTo cx="95.40" cy="24.70" x="94.50" y="24.70" />
                <LineTo x="83.75" y="24.70" />
                <CurveTo cx="82.80" cy="24.70" x="82.15" y="24.05" />
                <CurveTo cx="81.50" cy="23.35" x="81.50" y="22.45" />
                <LineTo x="81.50" y="21.65" />
                <CurveTo cx="81.50" cy="20.70" x="82.15" y="20.05" />
                <CurveTo cx="82.80" cy="19.40" x="83.75" y="19.40" />

                <MoveTo x="54.80" y="38.15" />
                <CurveTo cx="56.45" cy="38.15" x="57.65" y="37.00" />
                <CurveTo cx="58.80" cy="35.80" x="58.80" y="34.20" />
                <CurveTo cx="58.80" cy="32.55" x="57.65" y="31.40" />
                <CurveTo cx="56.45" cy="30.20" x="54.80" y="30.20" />
                <CurveTo cx="53.15" cy="30.20" x="52.00" y="31.40" />
                <CurveTo cx="50.85" cy="32.55" x="50.85" y="34.20" />
                <CurveTo cx="50.85" cy="35.80" x="52.00" y="37.00" />
                <CurveTo cx="53.15" cy="38.15" x="54.80" y="38.15" />

                <MoveTo x="72.90" y="37.60" />
                <CurveTo cx="74.55" cy="37.60" x="75.75" y="36.45" />
                <CurveTo cx="76.90" cy="35.25" x="76.90" y="33.60" />
                <CurveTo cx="76.90" cy="32.00" x="75.75" y="30.80" />
                <CurveTo cx="74.55" cy="29.65" x="72.90" y="29.65" />
                <CurveTo cx="71.25" cy="29.65" x="70.10" y="30.80" />
                <CurveTo cx="68.95" cy="32.00" x="68.95" y="33.60" />
                <CurveTo cx="68.95" cy="35.25" x="70.10" y="36.45" />
                <CurveTo cx="71.25" cy="37.60" x="72.90" y="37.60" />

                <MoveTo x="38.80" y="262.75" />
                <CurveTo cx="40.45" cy="262.75" x="41.60" y="261.85" />
                <CurveTo cx="42.75" cy="260.95" x="42.75" y="259.70" />
                <CurveTo cx="42.75" cy="258.45" x="41.60" y="257.60" />
                <CurveTo cx="40.45" cy="256.70" x="38.80" y="256.70" />
                <CurveTo cx="37.15" cy="256.70" x="36.00" y="257.60" />
                <CurveTo cx="34.85" cy="258.45" x="34.85" y="259.70" />
                <CurveTo cx="34.85" cy="260.95" x="36.00" y="261.85" />
                <CurveTo cx="37.15" cy="262.75" x="38.80" y="262.75" />

                <MoveTo x="17.90" y="144.20" />
                <CurveTo cx="24.10" cy="141.80" x="32.25" y="140.55" />
                <CurveTo cx="39.55" cy="139.40" x="48.40" y="139.15" />
                <CurveTo cx="46.45" cy="142.35" x="45.10" y="145.15" />
                <CurveTo cx="43.60" cy="148.30" x="42.85" y="151.10" />
                <CurveTo cx="32.95" cy="151.25" x="23.25" y="153.85" />
                <LineTo x="17.90" y="144.20" />

                <MoveTo x="108.55" y="143.95" />
                <CurveTo cx="102.00" cy="141.55" x="93.45" y="140.30" />
                <CurveTo cx="85.75" cy="139.15" x="76.45" y="138.90" />
                <CurveTo cx="80.65" cy="145.30" x="82.25" y="150.75" />
                <CurveTo cx="87.60" cy="150.85" x="92.45" y="151.50" />
                <CurveTo cx="97.60" cy="152.15" x="102.90" y="153.50" />
                <LineTo x="108.55" y="143.95" />

                <MoveTo x="9.50" y="163.15" />
                <CurveTo cx="17.20" cy="166.15" x="25.80" y="168.30" />
                <CurveTo cx="33.55" cy="170.20" x="42.10" y="171.45" />
                <CurveTo cx="44.00" cy="177.25" x="48.50" y="182.85" />
                <CurveTo cx="40.25" cy="182.45" x="32.35" y="180.90" />
                <CurveTo cx="24.65" cy="179.40" x="16.35" y="176.65" />
                <CurveTo cx="15.70" cy="174.55" x="13.75" y="170.80" />
                <CurveTo cx="12.30" cy="168.00" x="9.50" y="163.15" />

                <MoveTo x="115.10" y="162.70" />
                <CurveTo cx="107.35" cy="165.75" x="98.85" y="167.85" />
                <CurveTo cx="91.00" cy="169.85" x="82.55" y="171.05" />
                <CurveTo cx="80.55" cy="177.00" x="76.15" y="182.55" />
                <CurveTo cx="84.30" cy="182.20" x="92.30" y="180.60" />
                <CurveTo cx="99.90" cy="179.10" x="108.25" y="176.30" />
                <CurveTo cx="108.90" cy="174.20" x="110.85" y="170.40" />
                <CurveTo cx="112.30" cy="167.55" x="115.10" y="162.70" />

                <MoveTo x="62.95" y="170.35" />
                <CurveTo cx="75.95" cy="170.35" x="85.20" y="167.70" />
                <CurveTo cx="94.45" cy="165.10" x="94.45" y="161.40" />
                <CurveTo cx="94.45" cy="157.70" x="85.20" y="155.05" />
                <CurveTo cx="75.95" cy="152.40" x="62.95" y="152.40" />
                <CurveTo cx="49.95" cy="152.40" x="40.70" y="155.05" />
                <CurveTo cx="31.45" cy="157.70" x="31.45" y="161.40" />
                <CurveTo cx="31.45" cy="165.10" x="40.70" y="167.70" />
                <CurveTo cx="49.95" cy="170.35" x="62.95" y="170.35" />

                <MoveTo x="48.50" y="149.25" />
                <CurveTo cx="46.35" cy="149.05" x="49.25" y="145.20" />
                <CurveTo cx="52.25" cy="141.20" x="56.95" y="138.80" />
                <CurveTo cx="62.75" cy="135.85" x="67.75" y="137.75" />
                <CurveTo cx="73.95" cy="140.10" x="77.70" y="149.70" />
                <LineTo x="48.50" y="149.25" />

                <MoveTo x="47.90" y="173.10" />
                <CurveTo cx="45.65" cy="173.25" x="48.65" y="176.90" />
                <CurveTo cx="51.75" cy="180.65" x="56.60" y="182.90" />
                <CurveTo cx="62.60" cy="185.65" x="67.75" y="183.85" />
                <CurveTo cx="74.15" cy="181.65" x="78.05" y="172.65" />
                <LineTo x="47.90" y="173.10" />

                <MoveTo x="45.30" y="189.80" />
                <CurveTo cx="53.70" cy="190.35" x="62.50" y="190.35" />
                <CurveTo cx="70.65" cy="190.30" x="79.25" y="189.80" />
                <LineTo x="77.80" y="200.15" />
                <CurveTo cx="69.85" cy="200.85" x="62.60" y="200.90" />
                <CurveTo cx="54.70" cy="200.90" x="47.35" y="200.15" />
                <LineTo x="45.30" y="189.80" />

                <MoveTo x="47.20" y="206.50" />
                <CurveTo cx="55.35" cy="207.05" x="62.60" y="206.95" />
                <CurveTo cx="68.80" cy="206.90" x="77.55" y="206.35" />
                <LineTo x="77.60" y="216.65" />
                <CurveTo cx="69.75" cy="217.35" x="62.60" y="217.40" />
                <CurveTo cx="54.85" cy="217.40" x="47.60" y="216.65" />
                <LineTo x="47.20" y="206.50" />

                <MoveTo x="47.05" y="222.75" />
                <CurveTo cx="55.20" cy="223.35" x="62.50" y="223.30" />
                <CurveTo cx="68.60" cy="223.20" x="77.55" y="222.60" />
                <LineTo x="77.60" y="233.65" />
                <CurveTo cx="69.75" cy="234.40" x="62.55" y="234.40" />
                <CurveTo cx="54.75" cy="234.45" x="47.45" y="233.65" />
                <LineTo x="47.05" y="222.75" />

                <MoveTo x="47.20" y="239.30" />
                <CurveTo cx="55.25" cy="239.85" x="62.60" y="239.75" />
                <CurveTo cx="65.95" cy="239.70" x="69.80" y="239.55" />
                <CurveTo cx="73.10" cy="239.40" x="77.55" y="239.15" />
                <LineTo x="77.60" y="249.45" />
                <CurveTo cx="69.75" cy="250.15" x="62.60" y="250.20" />
                <CurveTo cx="54.85" cy="250.20" x="47.60" y="249.45" />
                <LineTo x="47.20" y="239.30" />

                <MoveTo x="17.15" y="196.55" />
                <CurveTo cx="11.30" cy="195.75" x="11.10" y="189.95" />
                <CurveTo cx="10.95" cy="184.10" x="18.10" y="185.05" />
                <CurveTo cx="19.30" cy="185.35" x="23.25" y="186.05" />
                <CurveTo cx="26.45" cy="186.60" x="31.20" y="187.35" />
                <LineTo x="35.15" y="187.95" />
                <LineTo x="38.55" y="188.45" />
                <LineTo x="40.90" y="188.80" />
                <LineTo x="41.90" y="188.95" />
                <LineTo x="44.30" y="200.20" />
                <LineTo x="17.15" y="196.55" />

                <MoveTo x="19.05" y="213.25" />
                <CurveTo cx="13.20" cy="212.40" x="13.55" y="206.85" />
                <CurveTo cx="13.90" cy="201.30" x="21.05" y="202.15" />
                <CurveTo cx="22.55" cy="202.55" x="28.35" y="203.45" />
                <CurveTo cx="31.65" cy="204.00" x="38.35" y="205.00" />
                <LineTo x="43.65" y="205.85" />
                <LineTo x="44.65" y="217.15" />
                <LineTo x="19.05" y="213.25" />

                <MoveTo x="20.25" y="229.45" />
                <CurveTo cx="14.45" cy="228.60" x="14.80" y="223.15" />
                <CurveTo cx="15.15" cy="217.75" x="22.25" y="218.60" />
                <CurveTo cx="23.60" cy="218.95" x="28.35" y="219.60" />
                <CurveTo cx="31.10" cy="219.95" x="37.10" y="220.75" />
                <LineTo x="44.25" y="221.75" />
                <LineTo x="44.50" y="232.80" />
                <LineTo x="20.25" y="229.45" />

                <MoveTo x="20.60" y="245.90" />
                <CurveTo cx="14.95" cy="245.05" x="15.30" y="239.90" />
                <CurveTo cx="15.65" cy="234.70" x="22.55" y="235.50" />
                <CurveTo cx="23.90" cy="235.80" x="28.85" y="236.50" />
                <CurveTo cx="31.65" cy="236.90" x="37.75" y="237.65" />
                <LineTo x="44.00" y="238.50" />
                <LineTo x="44.20" y="249.10" />
                <LineTo x="20.60" y="245.90" />

                <MoveTo x="108.20" y="196.25" />
                <CurveTo cx="114.05" cy="195.45" x="114.25" y="189.55" />
                <CurveTo cx="114.45" cy="183.65" x="107.30" y="184.60" />
                <CurveTo cx="105.80" cy="185.00" x="99.75" y="186.00" />
                <CurveTo cx="96.25" cy="186.55" x="89.05" y="187.70" />
                <LineTo x="83.00" y="188.70" />
                <LineTo x="81.30" y="199.95" />
                <LineTo x="108.20" y="196.25" />

                <MoveTo x="105.75" y="213.10" />
                <CurveTo cx="111.45" cy="212.20" x="111.10" y="206.50" />
                <CurveTo cx="110.75" cy="200.80" x="103.75" y="201.70" />
                <CurveTo cx="102.15" cy="202.15" x="95.55" y="203.20" />
                <LineTo x="90.00" y="204.10" />
                <LineTo x="85.05" y="204.90" />
                <LineTo x="82.60" y="205.30" />
                <LineTo x="81.65" y="205.45" />
                <LineTo x="80.65" y="217.05" />
                <LineTo x="105.75" y="213.10" />

                <MoveTo x="104.90" y="230.15" />
                <CurveTo cx="110.70" cy="229.30" x="110.35" y="223.65" />
                <CurveTo cx="110.00" cy="218.05" x="102.90" y="218.95" />
                <CurveTo cx="101.55" cy="219.30" x="96.85" y="219.95" />
                <CurveTo cx="94.10" cy="220.35" x="88.10" y="221.15" />
                <LineTo x="80.80" y="222.20" />
                <LineTo x="80.55" y="233.60" />
                <LineTo x="104.90" y="230.15" />

                <MoveTo x="104.35" y="245.65" />
                <CurveTo cx="109.95" cy="244.80" x="109.60" y="239.65" />
                <CurveTo cx="109.30" cy="234.50" x="102.40" y="235.30" />
                <CurveTo cx="101.10" cy="235.60" x="96.60" y="236.20" />
                <CurveTo cx="93.95" cy="236.60" x="88.20" y="237.30" />
                <LineTo x="81.00" y="238.30" />
                <LineTo x="80.75" y="248.80" />
                <LineTo x="104.35" y="245.65" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (30, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 30
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="33.15" y="4.75" />
                <CurveTo cx="60.30" cy="0.00" x="87.55" y="4.75" />
                <CurveTo cx="101.75" cy="7.25" x="110.85" y="15.85" />
                <CurveTo cx="120.65" cy="25.15" x="120.65" y="38.45" />
                <LineTo x="120.65" y="234.70" />
                <CurveTo cx="120.65" cy="248.25" x="110.90" y="257.70" />
                <CurveTo cx="101.55" cy="266.75" x="87.55" y="268.40" />
                <CurveTo cx="60.30" cy="271.65" x="33.15" y="268.40" />
                <CurveTo cx="19.10" cy="266.75" x="9.80" y="257.70" />
                <CurveTo cx="0.00" cy="248.25" x="0.00" y="234.70" />
                <LineTo x="0.00" y="38.45" />
                <CurveTo cx="0.00" cy="25.15" x="9.85" y="15.85" />
                <CurveTo cx="18.95" cy="7.25" x="33.15" y="4.75" />

                <MoveTo x="87.20" y="253.25" />
                <CurveTo cx="93.30" cy="253.25" x="97.65" y="248.95" />
                <CurveTo cx="102.00" cy="244.60" x="102.00" y="238.50" />
                <CurveTo cx="102.00" cy="232.40" x="97.65" y="228.05" />
                <CurveTo cx="93.30" cy="223.70" x="87.20" y="223.70" />
                <CurveTo cx="81.10" cy="223.70" x="76.75" y="228.05" />
                <CurveTo cx="72.40" cy="232.40" x="72.40" y="238.50" />
                <CurveTo cx="72.40" cy="244.60" x="76.75" y="248.95" />
                <CurveTo cx="81.10" cy="253.25" x="87.20" y="253.25" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (31, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 31
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="47.30" y="0.00" />
                <LineTo x="79.05" y="0.00" />
                <CurveTo cx="82.70" cy="0.00" x="85.10" y="2.50" />
                <CurveTo cx="87.55" cy="5.00" x="90.90" y="5.50" />
                <CurveTo cx="118.00" cy="9.70" x="124.40" y="26.20" />
                <CurveTo cx="125.70" cy="29.50" x="126.10" y="33.65" />
                <CurveTo cx="126.35" cy="36.25" x="126.35" y="41.25" />
                <LineTo x="126.35" y="278.45" />
                <LineTo x="126.35" y="278.95" />
                <CurveTo cx="126.35" cy="282.55" x="126.05" y="284.10" />
                <CurveTo cx="125.60" cy="286.55" x="124.05" y="289.00" />
                <CurveTo cx="119.55" cy="296.10" x="109.15" y="300.05" />
                <LineTo x="108.25" y="300.25" />
                <LineTo x="107.35" y="300.40" />
                <CurveTo cx="103.10" cy="301.30" x="100.20" y="305.55" />
                <CurveTo cx="96.95" cy="310.30" x="91.30" y="310.60" />
                <CurveTo cx="88.60" cy="310.75" x="81.50" y="310.70" />
                <CurveTo cx="77.40" cy="310.70" x="68.80" y="310.60" />
                <CurveTo cx="62.25" cy="310.50" x="60.45" y="310.50" />
                <CurveTo cx="59.05" cy="310.50" x="54.40" y="310.55" />
                <CurveTo cx="46.75" cy="310.65" x="42.95" y="310.65" />
                <CurveTo cx="36.40" cy="310.65" x="33.90" y="310.45" />
                <CurveTo cx="29.40" cy="310.00" x="27.15" y="305.30" />
                <CurveTo cx="25.20" cy="301.25" x="21.95" y="300.60" />
                <LineTo x="20.75" y="300.35" />
                <LineTo x="19.65" y="300.10" />
                <LineTo x="18.20" y="299.80" />
                <CurveTo cx="8.35" cy="297.60" x="3.10" y="288.30" />
                <CurveTo cx="0.05" cy="282.95" x="0.05" y="276.95" />
                <LineTo x="0.05" y="41.60" />
                <LineTo x="0.05" y="39.95" />
                <CurveTo cx="0.00" cy="35.10" x="0.30" y="32.75" />
                <CurveTo cx="0.75" cy="28.95" x="2.25" y="25.70" />
                <CurveTo cx="9.65" cy="10.25" x="38.05" y="5.55" />
                <CurveTo cx="40.75" cy="5.10" x="42.55" y="2.55" />
                <CurveTo cx="44.35" cy="0.00" x="47.30" y="0.00" />

                <MoveTo x="28.70" y="22.10" />
                <CurveTo cx="28.35" cy="23.50" x="28.60" y="23.70" />
                <CurveTo cx="28.80" cy="23.85" x="29.75" y="23.40" />
                <CurveTo cx="30.60" cy="23.05" x="31.15" y="22.90" />
                <CurveTo cx="38.35" cy="20.75" x="45.15" y="20.80" />
                <CurveTo cx="45.85" cy="20.80" x="46.85" y="20.90" />
                <CurveTo cx="48.05" cy="21.00" x="48.40" y="20.75" />
                <CurveTo cx="48.85" cy="20.40" x="49.25" y="18.70" />
                <CurveTo cx="49.60" cy="17.25" x="48.70" y="16.95" />
                <CurveTo cx="48.15" cy="16.70" x="46.35" y="16.90" />
                <CurveTo cx="45.30" cy="17.00" x="44.80" y="17.00" />
                <CurveTo cx="41.00" cy="17.10" x="38.05" y="17.40" />
                <CurveTo cx="35.05" cy="17.75" x="31.60" y="18.55" />
                <CurveTo cx="30.10" cy="18.85" x="29.60" y="19.50" />
                <CurveTo cx="29.25" cy="19.95" x="28.85" y="21.50" />
                <LineTo x="28.70" y="22.10" />

                <MoveTo x="98.40" y="21.90" />
                <CurveTo cx="98.75" cy="23.25" x="98.50" y="23.45" />
                <CurveTo cx="98.30" cy="23.60" x="97.40" y="23.20" />
                <CurveTo cx="96.50" cy="22.80" x="95.95" y="22.65" />
                <CurveTo cx="92.35" cy="21.60" x="88.95" y="21.10" />
                <CurveTo cx="85.35" cy="20.50" x="81.95" y="20.55" />
                <CurveTo cx="81.25" cy="20.55" x="80.30" y="20.65" />
                <CurveTo cx="79.05" cy="20.80" x="78.70" y="20.50" />
                <CurveTo cx="78.25" cy="20.15" x="77.85" y="18.45" />
                <CurveTo cx="77.45" cy="17.00" x="78.40" y="16.70" />
                <CurveTo cx="78.95" cy="16.45" x="80.80" y="16.65" />
                <CurveTo cx="81.80" cy="16.80" x="82.30" y="16.80" />
                <CurveTo cx="86.10" cy="16.85" x="89.05" y="17.20" />
                <CurveTo cx="92.00" cy="17.55" x="95.50" y="18.30" />
                <CurveTo cx="96.95" cy="18.60" x="97.50" y="19.25" />
                <CurveTo cx="97.85" cy="19.70" x="98.25" y="21.25" />
                <LineTo x="98.40" y="21.90" />

                <MoveTo x="70.00" y="34.40" />
                <CurveTo cx="70.00" cy="35.25" x="68.00" y="35.90" />
                <CurveTo cx="66.00" cy="36.50" x="63.25" y="36.50" />
                <CurveTo cx="60.50" cy="36.50" x="58.50" y="35.90" />
                <CurveTo cx="56.55" cy="35.25" x="56.55" y="34.40" />
                <CurveTo cx="56.55" cy="33.60" x="57.20" y="33.50" />
                <CurveTo cx="57.60" cy="33.45" x="58.50" y="33.75" />
                <LineTo x="58.90" y="33.85" />
                <CurveTo cx="60.75" cy="34.30" x="62.95" y="34.35" />
                <CurveTo cx="65.20" cy="34.35" x="67.00" y="33.90" />
                <LineTo x="67.50" y="33.75" />
                <CurveTo cx="68.60" cy="33.35" x="69.10" y="33.35" />
                <CurveTo cx="70.00" cy="33.40" x="70.00" y="34.40" />

                <MoveTo x="25.00" y="42.00" />
                <LineTo x="102.30" y="42.00" />
                <CurveTo cx="107.05" cy="42.00" x="107.05" y="46.70" />
                <LineTo x="107.05" y="147.20" />
                <CurveTo cx="107.05" cy="151.95" x="102.30" y="151.95" />
                <LineTo x="25.00" y="151.95" />
                <CurveTo cx="20.25" cy="151.95" x="20.25" y="147.20" />
                <LineTo x="20.25" y="46.70" />
                <CurveTo cx="20.25" cy="42.00" x="25.00" y="42.00" />

                <MoveTo x="15.35" y="178.40" />
                <LineTo x="111.30" y="178.50" />
                <LineTo x="111.30" y="188.20" />
                <CurveTo cx="63.20" cy="204.00" x="15.35" y="188.20" />
                <LineTo x="15.35" y="178.40" />

                <MoveTo x="63.95" y="217.45" />
                <CurveTo cx="67.25" cy="217.45" x="69.60" y="215.10" />
                <CurveTo cx="71.95" cy="212.75" x="71.95" y="209.45" />
                <CurveTo cx="71.95" cy="206.15" x="69.60" y="203.80" />
                <CurveTo cx="67.25" cy="201.45" x="63.95" y="201.45" />
                <CurveTo cx="60.65" cy="201.45" x="58.30" y="203.80" />
                <CurveTo cx="55.95" cy="206.15" x="55.95" y="209.45" />
                <CurveTo cx="55.95" cy="212.75" x="58.30" y="215.10" />
                <CurveTo cx="60.65" cy="217.45" x="63.95" y="217.45" />

                <MoveTo x="90.25" y="201.80" />
                <LineTo x="103.55" y="198.60" />
                <CurveTo cx="105.70" cy="198.10" x="107.60" y="199.15" />
                <CurveTo cx="109.55" cy="200.15" x="110.10" y="202.10" />
                <CurveTo cx="110.65" cy="204.05" x="109.50" y="205.80" />
                <CurveTo cx="108.35" cy="207.55" x="106.20" y="208.05" />
                <LineTo x="92.85" y="211.25" />
                <CurveTo cx="90.70" cy="211.75" x="88.80" y="210.75" />
                <CurveTo cx="86.90" cy="209.75" x="86.35" y="207.80" />
                <CurveTo cx="85.80" cy="205.85" x="86.95" y="204.10" />
                <CurveTo cx="88.10" cy="202.30" x="90.25" y="201.80" />

                <MoveTo x="36.55" y="201.45" />
                <LineTo x="23.20" y="198.25" />
                <CurveTo cx="21.05" cy="197.70" x="19.15" y="198.75" />
                <CurveTo cx="17.20" cy="199.75" x="16.70" y="201.70" />
                <CurveTo cx="16.15" cy="203.65" x="17.30" y="205.40" />
                <CurveTo cx="18.40" cy="207.15" x="20.55" y="207.70" />
                <LineTo x="33.90" y="210.90" />
                <CurveTo cx="36.05" cy="211.40" x="37.95" y="210.35" />
                <CurveTo cx="39.90" cy="209.35" x="40.40" y="207.40" />
                <CurveTo cx="40.95" cy="205.45" x="39.85" y="203.70" />
                <CurveTo cx="38.70" cy="201.95" x="36.55" y="201.45" />

                <MoveTo x="15.10" y="213.60" />
                <CurveTo cx="63.95" cy="231.40" x="111.65" y="214.35" />
                <LineTo x="111.65" y="281.30" />
                <CurveTo cx="62.75" cy="297.45" x="15.10" y="281.60" />
                <LineTo x="15.10" y="213.60" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (32, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 32
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="54.70" y="0.00" />
                <LineTo x="70.35" y="0.00" />
                <CurveTo cx="77.05" cy="0.00" x="80.75" y="0.15" />
                <CurveTo cx="86.35" cy="0.40" x="91.05" y="1.15" />
                <CurveTo cx="118.25" cy="5.45" x="124.55" y="22.30" />
                <CurveTo cx="125.85" cy="25.70" x="126.25" y="29.90" />
                <CurveTo cx="126.55" cy="32.70" x="126.55" y="37.65" />
                <LineTo x="126.55" y="121.50" />
                <CurveTo cx="126.55" cy="122.70" x="126.60" y="124.60" />
                <CurveTo cx="126.70" cy="128.30" x="126.00" y="128.90" />
                <CurveTo cx="125.30" cy="129.50" x="120.95" y="129.55" />
                <LineTo x="119.05" y="129.55" />
                <LineTo x="115.65" y="129.55" />
                <LineTo x="14.20" y="129.70" />
                <LineTo x="14.05" y="129.70" />
                <CurveTo cx="6.30" cy="129.70" x="3.90" y="128.65" />
                <CurveTo cx="0.05" cy="126.95" x="0.20" y="121.10" />
                <CurveTo cx="0.50" cy="108.80" x="0.50" y="93.45" />
                <CurveTo cx="0.50" cy="84.20" x="0.35" y="65.75" />
                <CurveTo cx="0.20" cy="47.30" x="0.20" y="38.05" />
                <LineTo x="0.20" y="36.40" />
                <CurveTo cx="0.15" cy="31.40" x="0.45" y="29.00" />
                <CurveTo cx="0.90" cy="25.10" x="2.45" y="21.80" />
                <CurveTo cx="9.75" cy="6.00" x="38.20" y="1.15" />
                <CurveTo cx="45.05" cy="0.00" x="54.70" y="0.00" />
                <MoveTo x="126.30" y="142.60" />
                <CurveTo cx="125.85" cy="163.05" x="125.85" y="188.60" />
                <CurveTo cx="125.85" cy="198.85" x="125.95" y="211.65" />
                <LineTo x="126.10" y="234.65" />
                <LineTo x="126.25" y="257.65" />
                <CurveTo cx="126.30" cy="270.45" x="126.30" y="280.65" />
                <LineTo x="126.30" y="281.15" />
                <CurveTo cx="126.30" cy="284.85" x="126.00" y="286.45" />
                <CurveTo cx="125.55" cy="288.95" x="124.00" y="291.45" />
                <CurveTo cx="119.35" cy="298.85" x="109.10" y="302.70" />
                <CurveTo cx="94.10" cy="306.05" x="78.00" y="306.90" />
                <CurveTo cx="74.55" cy="307.05" x="70.30" y="307.15" />
                <CurveTo cx="67.70" cy="307.15" x="62.60" y="307.15" />
                <LineTo x="60.40" y="307.15" />
                <CurveTo cx="51.20" cy="307.15" x="44.95" y="306.55" />
                <CurveTo cx="29.65" cy="305.10" x="18.15" y="302.45" />
                <CurveTo cx="8.30" cy="300.25" x="3.00" y="290.70" />
                <CurveTo cx="0.00" cy="285.30" x="0.00" y="279.10" />
                <LineTo x="0.00" y="143.35" />
                <CurveTo cx="0.00" cy="134.35" x="9.15" y="134.40" />
                <LineTo x="112.40" y="135.10" />
                <CurveTo cx="114.05" cy="135.10" x="116.90" y="135.00" />
                <CurveTo cx="122.50" cy="134.80" x="124.10" y="135.75" />
                <CurveTo cx="126.40" cy="137.15" x="126.30" y="142.60" />

                <MoveTo x="54.05" y="289.30" />
                <LineTo x="72.45" y="289.30" />
                <CurveTo cx="74.75" cy="289.30" x="76.35" y="290.95" />
                <CurveTo cx="78.00" cy="292.55" x="78.00" y="294.85" />
                <CurveTo cx="78.00" cy="297.15" x="76.35" y="298.75" />
                <CurveTo cx="74.75" cy="300.40" x="72.45" y="300.40" />
                <LineTo x="54.05" y="300.40" />
                <CurveTo cx="51.75" cy="300.40" x="50.15" y="298.75" />
                <CurveTo cx="48.50" cy="297.15" x="48.50" y="294.85" />
                <CurveTo cx="48.50" cy="292.55" x="50.15" y="290.95" />
                <CurveTo cx="51.75" cy="289.30" x="54.05" y="289.30" />

                <MoveTo x="64.20" y="114.00" />
                <CurveTo cx="66.75" cy="114.00" x="68.55" y="112.20" />
                <CurveTo cx="70.35" cy="110.40" x="70.35" y="107.85" />
                <CurveTo cx="70.35" cy="105.30" x="68.55" y="103.50" />
                <CurveTo cx="66.75" cy="101.70" x="64.20" y="101.70" />
                <CurveTo cx="61.70" cy="101.70" x="59.90" y="103.50" />
                <CurveTo cx="58.10" cy="105.30" x="58.10" y="107.85" />
                <CurveTo cx="58.10" cy="110.40" x="59.90" y="112.20" />
                <CurveTo cx="61.70" cy="114.00" x="64.20" y="114.00" />

                <MoveTo x="63.80" y="84.05" />
                <CurveTo cx="67.40" cy="84.05" x="69.95" y="81.50" />
                <CurveTo cx="72.50" cy="78.95" x="72.50" y="75.30" />
                <CurveTo cx="72.50" cy="71.70" x="69.95" y="69.15" />
                <CurveTo cx="67.40" cy="66.60" x="63.80" y="66.60" />
                <CurveTo cx="60.20" cy="66.60" x="57.65" y="69.15" />
                <CurveTo cx="55.05" cy="71.70" x="55.05" y="75.30" />
                <CurveTo cx="55.05" cy="78.95" x="57.65" y="81.50" />
                <CurveTo cx="60.20" cy="84.05" x="63.80" y="84.05" />

                <MoveTo x="64.30" y="62.55" />
                <CurveTo cx="68.90" cy="62.55" x="72.15" y="59.30" />
                <CurveTo cx="75.40" cy="56.05" x="75.40" y="51.45" />
                <CurveTo cx="75.40" cy="46.90" x="72.15" y="43.60" />
                <CurveTo cx="68.90" cy="40.35" x="64.30" y="40.35" />
                <CurveTo cx="59.75" cy="40.35" x="56.45" y="43.60" />
                <CurveTo cx="53.20" cy="46.90" x="53.20" y="51.45" />
                <CurveTo cx="53.20" cy="56.05" x="56.45" y="59.30" />
                <CurveTo cx="59.75" cy="62.55" x="64.30" y="62.55" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (33, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 33
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face1" x="83.00" y="77.00">
            <Contour x="0" y="0">
                <MoveTo x="28.20" y="0.00" />
                <LineTo x="28.10" y="29.65" />
                <LineTo x="27.95" y="30.10" />
                <LineTo x="27.95" y="30.25" />
                <LineTo x="21.50" y="30.85" />
                <LineTo x="16.80" y="32.40" />
                <LineTo x="15.00" y="33.35" />
                <LineTo x="8.15" y="38.65" />
                <LineTo x="2.65" y="41.40" />
                <LineTo x="0.00" y="42.25" />
                <LineTo x="0.00" y="43.70" />
                <LineTo x="0.15" y="44.65" />
                <LineTo x="0.25" y="55.95" />
                <LineTo x="0.35" y="56.75" />
                <LineTo x="0.60" y="80.65" />
                <LineTo x="2.55" y="126.00" />
                <LineTo x="3.60" y="142.20" />
                <LineTo x="6.95" y="173.30" />
                <LineTo x="7.80" y="177.35" />
                <LineTo x="9.25" y="181.10" />
                <LineTo x="11.30" y="184.20" />
                <LineTo x="13.80" y="186.95" />
                <LineTo x="18.35" y="190.10" />
                <LineTo x="20.15" y="190.80" />
                <LineTo x="23.90" y="191.75" />
                <LineTo x="27.95" y="192.10" />
                <LineTo x="29.90" y="199.55" />
                <LineTo x="33.00" y="206.55" />
                <LineTo x="34.95" y="209.65" />
                <LineTo x="39.75" y="215.40" />
                <LineTo x="45.35" y="220.35" />
                <LineTo x="51.95" y="224.15" />
                <LineTo x="59.05" y="226.80" />
                <LineTo x="62.75" y="227.75" />
                <LineTo x="70.70" y="228.50" />
                <LineTo x="78.60" y="227.75" />
                <LineTo x="85.95" y="225.60" />
                <LineTo x="92.75" y="222.35" />
                <LineTo x="98.90" y="218.05" />
                <LineTo x="104.15" y="212.65" />
                <LineTo x="108.35" y="206.55" />
                <LineTo x="111.50" y="199.55" />
                <LineTo x="113.40" y="192.10" />
                <LineTo x="117.50" y="191.75" />
                <LineTo x="121.20" y="190.80" />
                <LineTo x="123.00" y="190.10" />
                <LineTo x="124.70" y="189.10" />
                <LineTo x="127.55" y="186.95" />
                <LineTo x="130.10" y="184.20" />
                <LineTo x="132.15" y="181.10" />
                <LineTo x="133.55" y="177.35" />
                <LineTo x="134.40" y="173.30" />
                <LineTo x="136.35" y="158.05" />
                <LineTo x="137.75" y="142.20" />
                <LineTo x="139.80" y="110.15" />
                <LineTo x="140.90" y="42.10" />
                <LineTo x="138.35" y="41.30" />
                <LineTo x="133.10" y="38.65" />
                <LineTo x="127.95" y="34.45" />
                <LineTo x="124.55" y="32.40" />
                <LineTo x="119.90" y="30.85" />
                <LineTo x="113.40" y="30.25" />
                <LineTo x="113.40" y="30.10" />
                <LineTo x="113.30" y="29.65" />
                <LineTo x="113.15" y="0.00" />
                <LineTo x="28.20" y="0.00" />
                <MoveTo x="60.35" y="46.20" />
                <LineTo x="61.45" y="48.10" />
                <LineTo x="62.75" y="49.80" />
                <LineTo x="64.45" y="51.10" />
                <LineTo x="66.25" y="52.10" />
                <LineTo x="68.30" y="52.70" />
                <LineTo x="70.45" y="52.95" />
                <LineTo x="72.50" y="52.70" />
                <LineTo x="74.55" y="52.10" />
                <LineTo x="76.45" y="51.10" />
                <LineTo x="78.00" y="49.80" />
                <LineTo x="79.35" y="48.10" />
                <LineTo x="80.40" y="46.20" />
                <LineTo x="81.00" y="44.30" />
                <LineTo x="81.25" y="42.10" />
                <LineTo x="81.00" y="40.10" />
                <LineTo x="80.40" y="38.05" />
                <LineTo x="79.35" y="36.10" />
                <LineTo x="78.00" y="34.45" />
                <LineTo x="76.45" y="33.10" />
                <LineTo x="74.55" y="32.15" />
                <LineTo x="72.50" y="31.55" />
                <LineTo x="70.45" y="31.35" />
                <LineTo x="68.30" y="31.55" />
                <LineTo x="66.25" y="32.15" />
                <LineTo x="64.45" y="33.10" />
                <LineTo x="62.75" y="34.45" />
                <LineTo x="61.45" y="36.10" />
                <LineTo x="60.35" y="38.05" />
                <LineTo x="59.75" y="40.10" />
                <LineTo x="59.75" y="44.30" />
                <LineTo x="60.35" y="46.20" />
                <MoveTo x="34.80" y="86.90" />
                <LineTo x="34.45" y="86.05" />
                <LineTo x="34.45" y="66.25" />
                <LineTo x="34.80" y="65.40" />
                <LineTo x="35.75" y="63.95" />
                <LineTo x="37.10" y="63.00" />
                <LineTo x="37.95" y="62.75" />
                <LineTo x="38.90" y="62.65" />
                <LineTo x="101.90" y="62.65" />
                <LineTo x="102.85" y="62.75" />
                <LineTo x="103.70" y="63.00" />
                <LineTo x="105.15" y="63.95" />
                <LineTo x="106.10" y="65.40" />
                <LineTo x="106.35" y="66.25" />
                <LineTo x="106.45" y="67.20" />
                <LineTo x="106.45" y="85.20" />
                <LineTo x="106.35" y="86.05" />
                <LineTo x="106.10" y="86.90" />
                <LineTo x="105.15" y="88.35" />
                <LineTo x="103.70" y="89.30" />
                <LineTo x="102.85" y="89.55" />
                <LineTo x="101.90" y="89.65" />
                <LineTo x="38.90" y="89.65" />
                <LineTo x="37.95" y="89.55" />
                <LineTo x="37.10" y="89.30" />
                <LineTo x="35.75" y="88.35" />
                <LineTo x="34.80" y="86.90" />
            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Front"/>
                    <Culture key="en" value="Front"/>
                    <Culture key="ru" value="&#x0424;&#x0430;&#x0441;&#x0430;&#x0434;"/>
                </Caption>
            </Localization>            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (34, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 34
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face2" x="392.536" y="123.333">
            <Contour x="0" y="0">
                <MoveTo x="1.30" y="1.45" />
                <LineTo x="0.35" y="3.00" />
                <LineTo x="0.00" y="4.70" />
                <LineTo x="0.10" y="126.00" />
                <LineTo x="0.45" y="128.30" />
                <LineTo x="1.80" y="132.60" />
                <LineTo x="2.75" y="134.65" />
                <LineTo x="3.95" y="136.55" />
                <LineTo x="6.70" y="140.05" />
                <LineTo x="8.25" y="141.60" />
                <LineTo x="12.00" y="144.15" />
                <LineTo x="16.05" y="145.90" />
                <LineTo x="18.20" y="146.55" />
                <LineTo x="22.90" y="147.00" />
                <LineTo x="23.85" y="152.55" />
                <LineTo x="25.55" y="157.90" />
                <LineTo x="27.80" y="162.95" />
                <LineTo x="30.70" y="167.65" />
                <LineTo x="38.00" y="175.70" />
                <LineTo x="42.35" y="179.05" />
                <LineTo x="47.00" y="181.80" />
                <LineTo x="47.25" y="179.90" />
                <LineTo x="47.90" y="178.10" />
                <LineTo x="48.75" y="176.40" />
                <LineTo x="49.85" y="174.95" />
                <LineTo x="51.25" y="173.75" />
                <LineTo x="52.85" y="172.90" />
                <LineTo x="54.65" y="172.35" />
                <LineTo x="56.55" y="172.10" />
                <LineTo x="76.60" y="172.10" />
                <LineTo x="78.50" y="172.35" />
                <LineTo x="80.30" y="172.90" />
                <LineTo x="81.85" y="173.75" />
                <LineTo x="83.30" y="174.95" />
                <LineTo x="84.40" y="176.40" />
                <LineTo x="85.35" y="178.10" />
                <LineTo x="85.85" y="179.90" />
                <LineTo x="86.05" y="181.80" />
                <LineTo x="90.75" y="179.05" />
                <LineTo x="95.05" y="175.70" />
                <LineTo x="99.05" y="171.85" />
                <LineTo x="102.40" y="167.65" />
                <LineTo x="105.25" y="162.95" />
                <LineTo x="107.55" y="157.90" />
                <LineTo x="109.25" y="152.55" />
                <LineTo x="110.20" y="147.00" />
                <LineTo x="114.85" y="146.55" />
                <LineTo x="117.05" y="145.90" />
                <LineTo x="121.10" y="144.15" />
                <LineTo x="124.85" y="141.60" />
                <LineTo x="127.95" y="138.35" />
                <LineTo x="130.35" y="134.65" />
                <LineTo x="132.15" y="130.45" />
                <LineTo x="133.00" y="126.00" />
                <LineTo x="133.10" y="4.70" />
                <LineTo x="132.75" y="3.00" />
                <LineTo x="131.80" y="1.45" />
                <LineTo x="130.35" y="0.35" />
                <LineTo x="128.55" y="0.00" />
                <LineTo x="104.80" y="0.25" />
                <LineTo x="102.25" y="0.75" />
                <LineTo x="98.45" y="3.00" />
                <LineTo x="89.90" y="10.10" />
                <LineTo x="85.60" y="12.95" />
                <CurveTo cx="83.40" cy="13.95" x="81.15" y="15.00" />
                <LineTo x="76.45" y="16.45" />
                <LineTo x="71.55" y="17.30" />
                <LineTo x="66.65" y="17.55" />
                <LineTo x="61.60" y="17.30" />
                <LineTo x="56.65" y="16.45" />
                <LineTo x="52.00" y="15.00" />
                <LineTo x="47.55" y="12.95" />
                <LineTo x="45.20" y="11.55" />
                <LineTo x="33.35" y="2.05" />
                <LineTo x="30.80" y="0.75" />
                <LineTo x="25.80" y="0.00" />
                <LineTo x="4.55" y="0.00" />
                <LineTo x="2.75" y="0.35" />
                <LineTo x="1.30" y="1.45" />
            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Back"/>
                    <Culture key="en" value="Back"/>
                    <Culture key="ru" value="&#x041E;&#x0431;&#x043E;&#x0440;&#x043E;&#x0442;"/>
                </Caption>
            </Localization>            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (35, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 35
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="117.00" y="29.30" />
                <LineTo x="117.00" y="196.25" />
                <CurveTo cx="117.00" cy="209.20" x="107.55" y="219.20" />
                <CurveTo cx="98.30" cy="229.00" x="85.30" y="230.50" />
                <CurveTo cx="70.05" cy="232.20" x="59.90" y="232.50" />
                <CurveTo cx="47.10" cy="232.85" x="34.45" y="231.45" />
                <CurveTo cx="21.25" cy="229.95" x="10.80" y="219.70" />
                <CurveTo cx="0.00" cy="209.05" x="0.00" y="196.25" />
                <LineTo x="0.00" y="30.50" />
                <CurveTo cx="1.95" cy="28.45" x="8.80" y="27.20" />
                <CurveTo cx="14.70" cy="26.05" x="24.60" y="25.50" />
                <LineTo x="24.60" y="0.00" />
                <LineTo x="92.10" y="0.10" />
                <LineTo x="92.15" y="25.45" />
                <CurveTo cx="110.20" cy="25.75" x="117.00" y="29.30" />

                <MoveTo x="28.70" y="94.65" />
                <LineTo x="87.30" y="94.65" />
                <LineTo x="87.75" y="94.65" />
                <CurveTo cx="89.20" cy="94.65" x="89.20" y="95.85" />
                <LineTo x="89.20" y="96.35" />
                <LineTo x="89.20" y="128.95" />
                <CurveTo cx="89.20" cy="130.65" x="87.80" y="130.60" />
                <LineTo x="86.75" y="130.55" />
                <LineTo x="28.65" y="130.55" />
                <LineTo x="28.50" y="130.55" />
                <CurveTo cx="26.70" cy="130.55" x="26.75" y="129.35" />
                <LineTo x="26.75" y="128.70" />
                <LineTo x="26.75" y="96.35" />
                <LineTo x="26.75" y="95.90" />
                <CurveTo cx="26.75" cy="94.65" x="28.25" y="94.65" />
                <LineTo x="28.70" y="94.65" />

                <MoveTo x="58.00" y="189.60" />
                <CurveTo cx="61.45" cy="189.60" x="63.90" y="187.15" />
                <CurveTo cx="66.30" cy="184.70" x="66.30" y="181.25" />
                <CurveTo cx="66.30" cy="177.85" x="63.90" y="175.40" />
                <CurveTo cx="61.45" cy="172.95" x="58.00" y="172.95" />
                <CurveTo cx="54.60" cy="172.95" x="52.15" y="175.40" />
                <CurveTo cx="49.70" cy="177.85" x="49.70" y="181.25" />
                <CurveTo cx="49.70" cy="184.70" x="52.15" y="187.15" />
                <CurveTo cx="54.60" cy="189.60" x="58.00" y="189.60" />

                <MoveTo x="57.85" y="215.70" />
                <CurveTo cx="61.45" cy="215.70" x="64.00" y="213.15" />
                <CurveTo cx="66.55" cy="210.60" x="66.55" y="207.00" />
                <CurveTo cx="66.55" cy="203.45" x="64.00" y="200.85" />
                <CurveTo cx="61.45" cy="198.30" x="57.85" y="198.30" />
                <CurveTo cx="54.25" cy="198.30" x="51.70" y="200.85" />
                <CurveTo cx="49.15" cy="203.45" x="49.15" y="207.00" />
                <CurveTo cx="49.15" cy="210.60" x="51.70" y="213.15" />
                <CurveTo cx="54.25" cy="215.70" x="57.85" y="215.70" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (36, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 36
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="7.90" y="0.00" />
                <LineTo x="100.05" y="0.00" />
                <CurveTo cx="107.95" cy="0.00" x="107.95" y="7.15" />
                <LineTo x="107.95" y="143.20" />
                <CurveTo cx="107.95" cy="149.65" x="100.05" y="150.40" />
                <CurveTo cx="56.50" cy="162.15" x="7.90" y="150.40" />
                <CurveTo cx="0.00" cy="149.55" x="0.00" y="143.20" />
                <LineTo x="0.00" y="7.15" />
                <CurveTo cx="0.00" cy="0.00" x="7.90" y="0.00" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (37, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 37
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 3" x="50" y="420">
            <Contour x="0" y="0">
                <MoveTo x="118.25" y="199.80" />
                <LineTo x="119.95" y="39.30" />
                <CurveTo cx="120.10" cy="25.85" x="113.10" y="16.80" />
                <CurveTo cx="105.60" cy="7.15" x="91.00" y="3.90" />
                <CurveTo cx="75.65" cy="0.55" x="60.25" y="0.30" />
                <CurveTo cx="43.05" cy="0.00" x="29.75" y="3.75" />
                <CurveTo cx="15.80" cy="6.95" x="8.40" y="17.10" />
                <CurveTo cx="1.50" cy="26.50" x="1.40" y="40.10" />
                <LineTo x="0.00" y="198.20" />
                <LineTo x="26.20" y="198.90" />
                <LineTo x="26.20" y="221.60" />
                <LineTo x="92.20" y="221.75" />
                <LineTo x="92.25" y="200.15" />
                <LineTo x="118.25" y="199.80" />

                <MoveTo x="18.40" y="83.55" />
                <LineTo x="102.30" y="83.55" />
                <CurveTo cx="103.10" cy="83.55" x="103.10" y="84.35" />
                <LineTo x="103.10" y="184.95" />
                <CurveTo cx="103.10" cy="185.75" x="102.30" y="185.75" />
                <LineTo x="18.40" y="185.75" />
                <CurveTo cx="17.60" cy="185.75" x="17.60" y="184.95" />
                <LineTo x="17.60" y="84.35" />
                <CurveTo cx="17.60" cy="83.55" x="18.40" y="83.55" />

                <MoveTo x="61.50" y="15.65" />
                <CurveTo cx="66.40" cy="15.45" x="68.55" y="16.60" />
                <CurveTo cx="70.60" cy="17.65" x="69.45" y="19.30" />
                <CurveTo cx="69.45" cy="19.80" x="67.10" y="19.95" />
                <CurveTo cx="66.10" cy="20.00" x="64.55" y="20.05" />
                <LineTo x="61.45" y="20.05" />
                <CurveTo cx="57.80" cy="20.05" x="55.90" y="19.85" />
                <CurveTo cx="53.65" cy="19.60" x="53.65" y="19.05" />
                <CurveTo cx="52.85" cy="17.70" x="54.65" y="16.80" />
                <CurveTo cx="56.70" cy="15.80" x="61.50" y="15.65" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (38, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 38
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 4" x="300" y="420">
            <Contour x="0" y="0">
                <MoveTo x="117.75" y="5.05" />
                <LineTo x="117.75" y="167.75" />
                <CurveTo cx="117.75" cy="180.95" x="109.05" y="190.30" />
                <CurveTo cx="100.10" cy="199.85" x="85.85" y="201.00" />
                <CurveTo cx="71.35" cy="203.55" x="60.15" y="203.80" />
                <CurveTo cx="47.75" cy="204.05" x="34.65" y="201.65" />
                <CurveTo cx="21.00" cy="200.85" x="10.65" y="191.00" />
                <CurveTo cx="0.00" cy="180.90" x="0.00" y="167.75" />
                <LineTo x="0.00" y="5.60" />
                <LineTo x="24.60" y="5.55" />
                <LineTo x="24.60" y="0.00" />
                <LineTo x="91.90" y="0.05" />
                <LineTo x="91.95" y="5.00" />
                <LineTo x="117.75" y="5.05" />

                <MoveTo x="58.75" y="79.90" />
                <CurveTo cx="69.05" cy="79.90" x="76.40" y="72.55" />
                <CurveTo cx="83.75" cy="65.20" x="83.75" y="54.90" />
                <CurveTo cx="83.75" cy="44.55" x="76.40" y="37.25" />
                <CurveTo cx="69.05" cy="29.90" x="58.75" y="29.90" />
                <CurveTo cx="48.45" cy="29.90" x="41.10" y="37.25" />
                <CurveTo cx="33.75" cy="44.55" x="33.75" y="54.90" />
                <CurveTo cx="33.75" cy="65.20" x="41.10" y="72.55" />
                <CurveTo cx="48.45" cy="79.90" x="58.75" y="79.90" />

                <MoveTo x="47.60" y="191.00" />
                <CurveTo cx="49.40" cy="191.00" x="50.70" y="190.40" />
                <CurveTo cx="51.95" cy="189.75" x="51.95" y="188.90" />
                <CurveTo cx="51.95" cy="188.05" x="50.70" y="187.45" />
                <CurveTo cx="49.40" cy="186.85" x="47.60" y="186.85" />
                <CurveTo cx="45.80" cy="186.85" x="44.50" y="187.45" />
                <CurveTo cx="43.20" cy="188.05" x="43.20" y="188.90" />
                <CurveTo cx="43.20" cy="189.75" x="44.50" y="190.40" />
                <CurveTo cx="45.80" cy="191.00" x="47.60" y="191.00" />

                <MoveTo x="70.25" y="191.55" />
                <CurveTo cx="72.00" cy="191.55" x="73.25" y="190.95" />
                <CurveTo cx="74.50" cy="190.35" x="74.50" y="189.50" />
                <CurveTo cx="74.50" cy="188.65" x="73.25" y="188.00" />
                <CurveTo cx="72.00" cy="187.40" x="70.25" y="187.40" />
                <CurveTo cx="68.50" cy="187.40" x="67.25" y="188.00" />
                <CurveTo cx="66.00" cy="188.65" x="66.00" y="189.50" />
                <CurveTo cx="66.00" cy="190.35" x="67.25" y="190.95" />
                <CurveTo cx="68.50" cy="191.55" x="70.25" y="191.55" />

                <MoveTo x="16.85" y="101.85" />
                <LineTo x="30.45" y="101.85" />
                <CurveTo cx="33.25" cy="101.85" x="35.20" y="103.45" />
                <CurveTo cx="37.20" cy="105.05" x="37.20" y="107.30" />
                <LineTo x="37.20" y="109.40" />
                <CurveTo cx="37.20" cy="111.65" x="35.20" y="113.25" />
                <CurveTo cx="33.25" cy="114.85" x="30.45" y="114.85" />
                <LineTo x="16.85" y="114.85" />
                <CurveTo cx="14.10" cy="114.85" x="12.10" y="113.25" />
                <CurveTo cx="10.15" cy="111.65" x="10.15" y="109.40" />
                <LineTo x="10.15" y="107.30" />
                <CurveTo cx="10.15" cy="105.05" x="12.10" y="103.45" />
                <CurveTo cx="14.10" cy="101.85" x="16.85" y="101.85" />

                <MoveTo x="16.45" y="121.15" />
                <LineTo x="30.40" y="121.15" />
                <CurveTo cx="33.20" cy="121.15" x="35.25" y="122.70" />
                <CurveTo cx="37.25" cy="124.25" x="37.25" y="126.40" />
                <LineTo x="37.25" y="128.40" />
                <CurveTo cx="37.25" cy="130.60" x="35.25" y="132.15" />
                <CurveTo cx="33.20" cy="133.70" x="30.40" y="133.70" />
                <LineTo x="16.45" y="133.70" />
                <CurveTo cx="13.60" cy="133.70" x="11.60" y="132.15" />
                <CurveTo cx="9.55" cy="130.60" x="9.55" y="128.40" />
                <LineTo x="9.55" y="126.40" />
                <CurveTo cx="9.55" cy="124.25" x="11.60" y="122.70" />
                <CurveTo cx="13.60" cy="121.15" x="16.45" y="121.15" />

                <MoveTo x="16.30" y="160.25" />
                <LineTo x="30.35" y="160.25" />
                <CurveTo cx="33.20" cy="160.25" x="35.25" y="161.80" />
                <CurveTo cx="37.25" cy="163.35" x="37.25" y="165.50" />
                <LineTo x="37.25" y="167.45" />
                <CurveTo cx="37.25" cy="169.60" x="35.25" y="171.15" />
                <CurveTo cx="33.20" cy="172.70" x="30.35" y="172.70" />
                <LineTo x="16.30" y="172.70" />
                <CurveTo cx="13.45" cy="172.70" x="11.45" y="171.15" />
                <CurveTo cx="9.40" cy="169.60" x="9.40" y="167.45" />
                <LineTo x="9.40" y="165.50" />
                <CurveTo cx="9.40" cy="163.35" x="11.45" y="161.80" />
                <CurveTo cx="13.45" cy="160.25" x="16.30" y="160.25" />

                <MoveTo x="16.70" y="140.65" />
                <LineTo x="30.40" y="140.65" />
                <CurveTo cx="33.15" cy="140.65" x="35.15" y="142.20" />
                <CurveTo cx="37.15" cy="143.70" x="37.15" y="145.85" />
                <LineTo x="37.15" y="147.85" />
                <CurveTo cx="37.15" cy="150.00" x="35.15" y="151.50" />
                <CurveTo cx="33.15" cy="153.05" x="30.40" y="153.05" />
                <LineTo x="16.70" y="153.05" />
                <CurveTo cx="13.95" cy="153.05" x="11.95" y="151.50" />
                <CurveTo cx="9.95" cy="150.00" x="9.95" y="147.85" />
                <LineTo x="9.95" y="145.85" />
                <CurveTo cx="9.95" cy="143.70" x="11.95" y="142.20" />
                <CurveTo cx="13.95" cy="140.65" x="16.70" y="140.65" />

                <MoveTo x="51.90" y="106.15" />
                <LineTo x="65.40" y="106.15" />
                <CurveTo cx="68.15" cy="106.15" x="70.10" y="107.75" />
                <CurveTo cx="72.05" cy="109.35" x="72.05" y="111.65" />
                <LineTo x="72.05" y="113.70" />
                <CurveTo cx="72.05" cy="116.00" x="70.10" y="117.60" />
                <CurveTo cx="68.15" cy="119.20" x="65.40" y="119.20" />
                <LineTo x="51.90" y="119.20" />
                <CurveTo cx="49.15" cy="119.20" x="47.20" y="117.60" />
                <CurveTo cx="45.25" cy="116.00" x="45.25" y="113.70" />
                <LineTo x="45.25" y="111.65" />
                <CurveTo cx="45.25" cy="109.35" x="47.20" y="107.75" />
                <CurveTo cx="49.15" cy="106.15" x="51.90" y="106.15" />

                <MoveTo x="51.65" y="125.55" />
                <LineTo x="65.55" y="125.55" />
                <CurveTo cx="68.35" cy="125.55" x="70.40" y="127.15" />
                <CurveTo cx="72.40" cy="128.75" x="72.40" y="131.00" />
                <LineTo x="72.40" y="133.10" />
                <CurveTo cx="72.40" cy="135.35" x="70.40" y="137.00" />
                <CurveTo cx="68.35" cy="138.60" x="65.55" y="138.60" />
                <LineTo x="51.65" y="138.60" />
                <CurveTo cx="48.85" cy="138.60" x="46.80" y="137.00" />
                <CurveTo cx="44.80" cy="135.35" x="44.80" y="133.10" />
                <LineTo x="44.80" y="131.00" />
                <CurveTo cx="44.80" cy="128.75" x="46.80" y="127.15" />
                <CurveTo cx="48.85" cy="125.55" x="51.65" y="125.55" />

                <MoveTo x="51.95" y="144.80" />
                <LineTo x="65.30" y="144.80" />
                <CurveTo cx="68.05" cy="144.80" x="70.00" y="146.40" />
                <CurveTo cx="71.95" cy="148.00" x="71.95" y="150.25" />
                <LineTo x="71.95" y="152.35" />
                <CurveTo cx="71.95" cy="154.60" x="70.00" y="156.20" />
                <CurveTo cx="68.05" cy="157.80" x="65.30" y="157.80" />
                <LineTo x="51.95" y="157.80" />
                <CurveTo cx="49.20" cy="157.80" x="47.25" y="156.20" />
                <CurveTo cx="45.30" cy="154.60" x="45.30" y="152.35" />
                <LineTo x="45.30" y="150.25" />
                <CurveTo cx="45.30" cy="148.00" x="47.25" y="146.40" />
                <CurveTo cx="49.20" cy="144.80" x="51.95" y="144.80" />

                <MoveTo x="52.20" y="164.25" />
                <LineTo x="65.85" y="164.25" />
                <CurveTo cx="68.60" cy="164.25" x="70.60" y="165.80" />
                <CurveTo cx="72.55" cy="167.40" x="72.55" y="169.60" />
                <LineTo x="72.55" y="171.60" />
                <CurveTo cx="72.55" cy="173.80" x="70.60" y="175.40" />
                <CurveTo cx="68.60" cy="176.95" x="65.85" y="176.95" />
                <LineTo x="52.20" y="176.95" />
                <CurveTo cx="49.45" cy="176.95" x="47.45" y="175.40" />
                <CurveTo cx="45.50" cy="173.80" x="45.50" y="171.60" />
                <LineTo x="45.50" y="169.60" />
                <CurveTo cx="45.50" cy="167.40" x="47.45" y="165.80" />
                <CurveTo cx="49.45" cy="164.25" x="52.20" y="164.25" />

                <MoveTo x="87.15" y="101.65" />
                <LineTo x="101.40" y="101.65" />
                <CurveTo cx="104.30" cy="101.65" x="106.35" y="103.15" />
                <CurveTo cx="108.45" cy="104.65" x="108.45" y="106.70" />
                <LineTo x="108.45" y="108.60" />
                <CurveTo cx="108.45" cy="110.65" x="106.35" y="112.15" />
                <CurveTo cx="104.30" cy="113.65" x="101.40" y="113.65" />
                <LineTo x="87.15" y="113.65" />
                <CurveTo cx="84.25" cy="113.65" x="82.15" y="112.15" />
                <CurveTo cx="80.10" cy="110.65" x="80.10" y="108.60" />
                <LineTo x="80.10" y="106.70" />
                <CurveTo cx="80.10" cy="104.65" x="82.15" y="103.15" />
                <CurveTo cx="84.25" cy="101.65" x="87.15" y="101.65" />

                <MoveTo x="87.45" y="120.95" />
                <LineTo x="101.65" y="120.95" />
                <CurveTo cx="104.55" cy="120.95" x="106.60" y="122.55" />
                <CurveTo cx="108.65" cy="124.15" x="108.65" y="126.40" />
                <LineTo x="108.65" y="128.45" />
                <CurveTo cx="108.65" cy="130.65" x="106.60" y="132.25" />
                <CurveTo cx="104.55" cy="133.85" x="101.65" y="133.85" />
                <LineTo x="87.45" y="133.85" />
                <CurveTo cx="84.55" cy="133.85" x="82.50" y="132.25" />
                <CurveTo cx="80.45" cy="130.65" x="80.45" y="128.45" />
                <LineTo x="80.45" y="126.40" />
                <CurveTo cx="80.45" cy="124.15" x="82.50" y="122.55" />
                <CurveTo cx="84.55" cy="120.95" x="87.45" y="120.95" />

                <MoveTo x="87.10" y="140.30" />
                <LineTo x="101.30" y="140.30" />
                <CurveTo cx="104.20" cy="140.30" x="106.30" y="141.85" />
                <CurveTo cx="108.35" cy="143.35" x="108.35" y="145.50" />
                <LineTo x="108.35" y="147.50" />
                <CurveTo cx="108.35" cy="149.65" x="106.30" y="151.20" />
                <CurveTo cx="104.20" cy="152.75" x="101.30" y="152.75" />
                <LineTo x="87.10" y="152.75" />
                <CurveTo cx="84.20" cy="152.75" x="82.15" y="151.20" />
                <CurveTo cx="80.10" cy="149.65" x="80.10" y="147.50" />
                <LineTo x="80.10" y="145.50" />
                <CurveTo cx="80.10" cy="143.35" x="82.15" y="141.85" />
                <CurveTo cx="84.20" cy="140.30" x="87.10" y="140.30" />

                <MoveTo x="87.30" y="159.70" />
                <LineTo x="101.40" y="159.70" />
                <CurveTo cx="104.30" cy="159.70" x="106.35" y="161.25" />
                <CurveTo cx="108.40" cy="162.85" x="108.40" y="165.05" />
                <LineTo x="108.40" y="167.10" />
                <CurveTo cx="108.40" cy="169.30" x="106.35" y="170.85" />
                <CurveTo cx="104.30" cy="172.45" x="101.40" y="172.45" />
                <LineTo x="87.30" y="172.45" />
                <CurveTo cx="84.40" cy="172.45" x="82.35" y="170.85" />
                <CurveTo cx="80.30" cy="169.30" x="80.30" y="167.10" />
                <LineTo x="80.30" y="165.05" />
                <CurveTo cx="80.30" cy="162.85" x="82.35" y="161.25" />
                <CurveTo cx="84.40" cy="159.70" x="87.30" y="159.70" />

                <MoveTo x="11.90" y="33.15" />
                <LineTo x="23.90" y="33.15" />
                <LineTo x="35.90" y="33.10" />
                <CurveTo cx="58.90" cy="13.85" x="80.20" y="32.85" />
                <LineTo x="105.70" y="32.85" />
                <LineTo x="105.70" y="19.05" />
                <CurveTo cx="105.70" cy="12.60" x="98.50" y="13.20" />
                <LineTo x="17.00" y="13.20" />
                <LineTo x="15.85" y="13.45" />
                <CurveTo cx="13.75" cy="13.95" x="13.30" y="14.35" />
                <CurveTo cx="12.80" cy="14.80" x="12.15" y="16.85" />
                <LineTo x="11.90" y="17.60" />
                <LineTo x="11.90" y="33.15" />

                <MoveTo x="57.05" y="95.95" />
                <CurveTo cx="52.70" cy="95.95" x="47.40" y="95.75" />
                <CurveTo cx="44.25" cy="95.60" x="37.95" y="95.30" />
                <CurveTo cx="31.65" cy="94.95" x="28.50" y="94.80" />
                <CurveTo cx="23.15" cy="94.60" x="18.80" y="94.60" />
                <CurveTo cx="11.85" cy="94.80" x="11.40" y="90.25" />
                <LineTo x="11.40" y="70.20" />
                <LineTo x="30.95" y="70.20" />
                <LineTo x="31.20" y="70.55" />
                <CurveTo cx="38.05" cy="79.40" x="41.35" y="81.60" />
                <CurveTo cx="45.05" cy="84.05" x="57.05" y="87.35" />
                <LineTo x="57.05" y="95.95" />

                <MoveTo x="61.20" y="95.95" />
                <CurveTo cx="65.55" cy="95.95" x="71.00" y="95.75" />
                <CurveTo cx="74.30" cy="95.65" x="80.85" y="95.30" />
                <CurveTo cx="87.45" cy="95.00" x="90.75" y="94.85" />
                <CurveTo cx="96.25" cy="94.65" x="100.60" y="94.65" />
                <CurveTo cx="106.65" cy="93.95" x="106.40" y="90.10" />
                <LineTo x="106.40" y="70.30" />
                <LineTo x="87.65" y="70.30" />
                <CurveTo cx="80.50" cy="78.85" x="77.25" y="81.00" />
                <CurveTo cx="74.05" cy="83.15" x="62.20" y="87.20" />
                <LineTo x="61.15" y="87.55" />
                <LineTo x="61.20" y="91.75" />
                <LineTo x="61.20" y="95.95" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (39, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 39
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="26.85" y="5.50" />
                <CurveTo cx="50.20" cy="0.40" x="71.55" y="0.20" />
                <CurveTo cx="93.95" cy="0.00" x="118.20" y="5.05" />
                <CurveTo cx="122.55" cy="6.00" x="124.45" y="8.85" />
                <CurveTo cx="125.65" cy="10.70" x="127.00" y="16.15" />
                <LineTo x="127.30" y="17.25" />
                <CurveTo cx="135.70" cy="49.35" x="137.70" y="82.70" />
                <CurveTo cx="139.60" cy="114.25" x="135.75" y="147.35" />
                <LineTo x="135.65" y="148.20" />
                <CurveTo cx="134.85" cy="155.20" x="134.65" y="158.45" />
                <CurveTo cx="134.40" cy="163.75" x="134.95" y="168.80" />
                <CurveTo cx="138.45" cy="200.25" x="136.25" y="228.30" />
                <CurveTo cx="133.90" cy="258.05" x="125.20" y="284.50" />
                <CurveTo cx="123.80" cy="288.80" x="118.90" y="290.15" />
                <CurveTo cx="116.00" cy="290.95" x="109.10" y="290.95" />
                <LineTo x="108.95" y="290.95" />
                <LineTo x="34.05" y="291.45" />
                <CurveTo cx="28.25" cy="291.45" x="26.00" y="290.40" />
                <CurveTo cx="23.00" cy="289.05" x="21.50" y="284.50" />
                <CurveTo cx="12.35" cy="257.40" x="9.95" y="227.85" />
                <CurveTo cx="7.90" cy="202.25" x="10.65" y="170.40" />
                <LineTo x="10.90" y="167.45" />
                <CurveTo cx="11.45" cy="161.65" x="11.50" y="158.75" />
                <CurveTo cx="11.60" cy="154.05" x="10.95" y="150.00" />
                <CurveTo cx="0.00" cy="81.95" x="18.00" y="15.75" />
                <LineTo x="18.20" y="14.95" />
                <CurveTo cx="19.55" cy="9.95" x="20.70" y="8.50" />
                <CurveTo cx="22.35" cy="6.50" x="26.85" y="5.50" />

                <MoveTo x="34.85" y="64.35" />
                <LineTo x="111.15" y="64.35" />
                <CurveTo cx="112.40" cy="64.35" x="112.40" y="65.50" />
                <LineTo x="112.40" y="137.85" />
                <CurveTo cx="112.40" cy="139.00" x="111.15" y="139.00" />
                <LineTo x="34.85" y="139.00" />
                <CurveTo cx="33.65" cy="139.00" x="33.65" y="137.85" />
                <LineTo x="33.65" y="65.50" />
                <CurveTo cx="33.65" cy="64.35" x="34.85" y="64.35" />

                <MoveTo x="37.95" y="156.70" />
                <LineTo x="107.70" y="156.70" />
                <CurveTo cx="117.15" cy="156.70" x="117.15" y="166.00" />
                <LineTo x="117.15" y="185.00" />
                <CurveTo cx="117.15" cy="194.30" x="107.70" y="194.30" />
                <LineTo x="37.95" y="194.30" />
                <CurveTo cx="28.50" cy="194.30" x="28.50" y="185.00" />
                <LineTo x="28.50" y="166.00" />
                <CurveTo cx="28.50" cy="156.70" x="37.95" y="156.70" />

                <MoveTo x="36.70" y="200.60" />
                <LineTo x="108.95" y="200.60" />
                <CurveTo cx="111.40" cy="200.60" x="113.50" y="203.05" />
                <CurveTo cx="115.50" cy="205.40" x="115.40" y="208.00" />
                <LineTo x="112.95" y="259.45" />
                <CurveTo cx="112.75" cy="262.60" x="108.90" y="266.60" />
                <CurveTo cx="104.45" cy="271.20" x="99.45" y="271.30" />
                <CurveTo cx="86.80" cy="271.80" x="73.35" y="271.55" />
                <CurveTo cx="65.00" cy="271.40" x="47.50" y="270.70" />
                <LineTo x="44.75" y="270.60" />
                <CurveTo cx="39.70" cy="269.15" x="36.60" y="265.60" />
                <CurveTo cx="33.75" cy="262.40" x="33.50" y="258.85" />
                <LineTo x="29.90" y="206.75" />
                <CurveTo cx="29.75" cy="204.30" x="31.95" y="202.40" />
                <CurveTo cx="34.05" cy="200.60" x="36.70" y="200.60" />

                <MoveTo x="63.30" y="15.75" />
                <LineTo x="68.40" y="15.75" />
                <CurveTo cx="68.90" cy="15.75" x="69.25" y="16.10" />
                <CurveTo cx="69.60" cy="16.45" x="69.60" y="16.95" />
                <LineTo x="69.60" y="18.90" />
                <CurveTo cx="69.60" cy="19.40" x="69.25" y="19.75" />
                <CurveTo cx="68.90" cy="20.10" x="68.40" y="20.10" />
                <LineTo x="63.30" y="20.10" />
                <CurveTo cx="62.80" cy="20.10" x="62.45" y="19.75" />
                <CurveTo cx="62.10" cy="19.40" x="62.10" y="18.90" />
                <LineTo x="62.10" y="16.95" />
                <CurveTo cx="62.10" cy="16.45" x="62.45" y="16.10" />
                <CurveTo cx="62.80" cy="15.75" x="63.30" y="15.75" />

                <MoveTo x="76.90" y="15.65" />
                <LineTo x="82.00" y="15.65" />
                <CurveTo cx="82.50" cy="15.65" x="82.85" y="16.00" />
                <CurveTo cx="83.20" cy="16.35" x="83.20" y="16.85" />
                <LineTo x="83.20" y="18.80" />
                <CurveTo cx="83.20" cy="19.30" x="82.85" y="19.65" />
                <CurveTo cx="82.50" cy="20.00" x="82.00" y="20.00" />
                <LineTo x="76.90" y="20.00" />
                <CurveTo cx="76.40" cy="20.00" x="76.05" y="19.65" />
                <CurveTo cx="75.70" cy="19.30" x="75.70" y="18.80" />
                <LineTo x="75.70" y="16.85" />
                <CurveTo cx="75.70" cy="16.35" x="76.05" y="16.00" />
                <CurveTo cx="76.40" cy="15.65" x="76.90" y="15.65" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (40, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 40
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="26.85" y="5.60" />
                <CurveTo cx="50.20" cy="0.45" x="71.55" y="0.25" />
                <CurveTo cx="93.90" cy="0.00" x="118.15" y="5.10" />
                <CurveTo cx="122.45" cy="6.05" x="124.35" y="8.90" />
                <CurveTo cx="125.60" cy="10.75" x="127.00" y="16.10" />
                <LineTo x="127.30" y="17.30" />
                <CurveTo cx="135.70" cy="49.40" x="137.70" y="82.75" />
                <CurveTo cx="139.55" cy="114.30" x="135.70" y="147.40" />
                <LineTo x="135.65" y="147.65" />
                <CurveTo cx="134.85" cy="154.85" x="134.65" y="158.25" />
                <CurveTo cx="134.40" cy="163.70" x="134.95" y="168.85" />
                <CurveTo cx="138.40" cy="200.30" x="136.20" y="228.35" />
                <CurveTo cx="133.90" cy="258.10" x="125.20" y="284.55" />
                <CurveTo cx="123.80" cy="288.85" x="118.90" y="290.20" />
                <CurveTo cx="116.00" cy="291.00" x="109.10" y="291.00" />
                <LineTo x="108.95" y="291.00" />
                <LineTo x="34.05" y="291.50" />
                <CurveTo cx="28.20" cy="291.50" x="25.95" y="290.45" />
                <CurveTo cx="23.00" cy="289.10" x="21.45" y="284.55" />
                <CurveTo cx="12.35" cy="257.45" x="9.95" y="227.90" />
                <CurveTo cx="7.90" cy="202.30" x="10.65" y="170.45" />
                <LineTo x="10.90" y="167.80" />
                <CurveTo cx="11.40" cy="161.85" x="11.50" y="158.95" />
                <CurveTo cx="11.60" cy="154.20" x="10.95" y="150.05" />
                <CurveTo cx="0.00" cy="82.05" x="17.95" y="15.85" />
                <LineTo x="18.15" y="15.15" />
                <CurveTo cx="19.50" cy="10.05" x="20.65" y="8.60" />
                <CurveTo cx="22.30" cy="6.60" x="26.85" y="5.60" />

                <MoveTo x="99.40" y="115.55" />
                <CurveTo cx="106.10" cy="115.55" x="110.85" y="110.80" />
                <CurveTo cx="115.60" cy="106.05" x="115.60" y="99.35" />
                <CurveTo cx="115.60" cy="92.65" x="110.85" y="87.90" />
                <CurveTo cx="106.10" cy="83.15" x="99.40" y="83.15" />
                <CurveTo cx="92.75" cy="83.15" x="87.95" y="87.90" />
                <CurveTo cx="83.20" cy="92.70" x="83.20" y="99.35" />
                <CurveTo cx="83.20" cy="106.05" x="87.95" y="110.80" />
                <CurveTo cx="92.75" cy="115.55" x="99.40" y="115.55" />

                <MoveTo x="49.65" y="291.45" />
                <CurveTo cx="49.65" cy="289.45" x="51.20" y="286.65" />
                <CurveTo cx="52.95" cy="283.50" x="55.00" y="282.70" />
                <CurveTo cx="72.80" cy="275.90" x="90.55" y="282.60" />
                <CurveTo cx="92.65" cy="283.35" x="94.25" y="286.40" />
                <CurveTo cx="95.70" cy="289.10" x="95.70" y="291.10" />

                <MoveTo x="53.90" y="291.40" />
                <CurveTo cx="53.75" cy="289.85" x="54.40" y="288.20" />
                <CurveTo cx="55.15" cy="286.35" x="56.70" y="285.30" />
                <CurveTo cx="64.35" cy="282.65" x="72.55" y="282.65" />
                <CurveTo cx="80.75" cy="282.65" x="88.45" y="285.30" />
                <CurveTo cx="90.15" cy="286.10" x="91.25" y="287.85" />
                <CurveTo cx="92.20" cy="289.40" x="92.25" y="291.10" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (41, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 41
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="107.05" y="7.30" />
                <CurveTo cx="114.35" cy="10.75" x="118.65" y="24.65" />
                <CurveTo cx="121.85" cy="35.00" x="123.20" y="50.60" />
                <CurveTo cx="129.35" cy="105.75" x="126.90" y="173.60" />
                <CurveTo cx="124.65" cy="236.40" x="115.30" y="303.40" />
                <CurveTo cx="115.15" cy="309.85" x="111.10" y="313.90" />
                <CurveTo cx="107.80" cy="317.20" x="101.65" y="319.00" />
                <CurveTo cx="83.95" cy="324.35" x="63.55" y="324.10" />
                <CurveTo cx="46.50" cy="323.95" x="24.70" y="319.70" />
                <CurveTo cx="13.40" cy="317.50" x="12.75" y="300.95" />
                <CurveTo cx="9.10" cy="264.90" x="6.65" y="233.00" />
                <CurveTo cx="4.00" cy="198.45" x="2.75" y="168.00" />
                <CurveTo cx="0.00" cy="101.15" x="3.95" y="51.85" />
                <CurveTo cx="4.95" cy="37.05" x="9.05" y="24.60" />
                <CurveTo cx="13.90" cy="9.85" x="20.70" y="7.50" />
                <CurveTo cx="33.95" cy="2.85" x="48.35" y="1.30" />
                <CurveTo cx="60.50" cy="0.00" x="73.20" y="0.95" />
                <CurveTo cx="83.20" cy="1.70" x="93.35" y="3.85" />
                <CurveTo cx="99.90" cy="5.20" x="107.05" y="7.30" />

                <MoveTo x="26.45" y="77.30" />
                <LineTo x="104.15" y="77.30" />
                <CurveTo cx="109.40" cy="77.30" x="109.40" y="82.65" />
                <LineTo x="109.40" y="141.30" />
                <CurveTo cx="109.40" cy="146.60" x="104.15" y="146.60" />
                <LineTo x="26.45" y="146.60" />
                <CurveTo cx="21.15" cy="146.60" x="21.15" y="141.30" />
                <LineTo x="21.15" y="82.65" />
                <CurveTo cx="21.15" cy="77.30" x="26.45" y="77.30" />

                <MoveTo x="57.50" y="11.85" />
                <CurveTo cx="58.10" cy="12.85" x="58.05" y="13.60" />
                <CurveTo cx="57.95" cy="14.60" x="56.60" y="15.00" />
                <LineTo x="56.15" y="15.15" />
                <CurveTo cx="54.50" cy="15.60" x="53.75" y="16.05" />
                <CurveTo cx="52.35" cy="16.85" x="52.55" y="18.00" />
                <LineTo x="52.30" y="26.15" />
                <LineTo x="52.30" y="26.50" />
                <CurveTo cx="52.35" cy="27.75" x="51.90" y="28.00" />
                <CurveTo cx="51.05" cy="28.45" x="50.15" y="28.35" />
                <CurveTo cx="49.50" cy="28.25" x="49.30" y="27.70" />
                <CurveTo cx="49.20" cy="27.35" x="49.25" y="26.40" />
                <LineTo x="49.25" y="26.25" />
                <LineTo x="49.60" y="15.20" />
                <CurveTo cx="49.60" cy="14.20" x="50.90" y="13.65" />
                <LineTo x="54.85" y="12.10" />
                <LineTo x="55.05" y="12.05" />
                <CurveTo cx="55.90" cy="11.75" x="56.40" y="11.65" />
                <CurveTo cx="57.25" cy="11.50" x="57.50" y="11.85" />

                <MoveTo x="70.80" y="11.80" />
                <CurveTo cx="70.20" cy="12.80" x="70.25" y="13.55" />
                <CurveTo cx="70.35" cy="14.55" x="71.70" y="14.95" />
                <LineTo x="72.15" y="15.10" />
                <CurveTo cx="73.80" cy="15.55" x="74.55" y="16.00" />
                <CurveTo cx="75.95" cy="16.80" x="75.75" y="17.95" />
                <LineTo x="76.00" y="26.10" />
                <LineTo x="76.00" y="26.45" />
                <CurveTo cx="75.95" cy="27.70" x="76.40" y="27.95" />
                <CurveTo cx="77.25" cy="28.40" x="78.15" y="28.30" />
                <CurveTo cx="78.80" cy="28.20" x="79.00" y="27.60" />
                <CurveTo cx="79.10" cy="27.25" x="79.05" y="26.35" />
                <LineTo x="79.05" y="26.20" />
                <LineTo x="78.70" y="15.15" />
                <CurveTo cx="78.70" cy="14.15" x="77.40" y="13.60" />
                <LineTo x="73.50" y="12.05" />
                <LineTo x="73.20" y="11.95" />
                <CurveTo cx="72.30" cy="11.65" x="71.85" y="11.60" />
                <CurveTo cx="71.05" cy="11.45" x="70.80" y="11.80" />

                <MoveTo x="10.85" y="162.15" />
                <LineTo x="12.35" y="188.65" />
                <LineTo x="12.40" y="189.15" />
                <CurveTo cx="12.65" cy="194.00" x="13.40" y="195.55" />
                <CurveTo cx="14.55" cy="197.90" x="18.25" y="198.20" />
                <CurveTo cx="42.75" cy="200.30" x="62.50" y="200.50" />
                <CurveTo cx="86.65" cy="200.70" x="109.00" y="198.20" />
                <CurveTo cx="113.85" cy="197.70" x="115.40" y="195.85" />
                <CurveTo cx="116.80" cy="194.15" x="117.10" y="189.00" />
                <LineTo x="118.80" y="160.40" />
                <CurveTo cx="117.15" cy="160.70" x="113.80" y="161.35" />
                <CurveTo cx="105.25" cy="162.90" x="100.95" y="164.00" />
                <CurveTo cx="93.75" cy="165.90" x="88.15" y="168.70" />
                <CurveTo cx="83.40" cy="163.35" x="79.15" y="161.05" />
                <CurveTo cx="73.25" cy="159.20" x="64.00" y="159.55" />
                <CurveTo cx="54.90" cy="159.85" x="50.20" y="162.00" />
                <CurveTo cx="45.70" cy="164.95" x="41.80" y="169.15" />
                <CurveTo cx="27.45" cy="163.55" x="10.85" y="162.15" />

                <MoveTo x="19.65" y="205.20" />
                <CurveTo cx="65.55" cy="208.40" x="108.95" y="204.50" />
                <CurveTo cx="113.50" cy="204.10" x="114.70" y="205.85" />
                <CurveTo cx="115.70" cy="207.30" x="115.30" y="212.45" />
                <CurveTo cx="114.80" cy="218.75" x="114.05" y="230.65" />
                <CurveTo cx="113.25" cy="242.60" x="112.80" y="248.20" />
                <CurveTo cx="112.05" cy="257.45" x="111.20" y="264.30" />
                <CurveTo cx="109.00" cy="280.95" x="104.70" y="292.10" />
                <CurveTo cx="98.75" cy="302.50" x="81.70" y="305.10" />
                <CurveTo cx="74.55" cy="306.20" x="64.55" y="306.10" />
                <CurveTo cx="56.90" cy="306.00" x="45.15" y="305.15" />
                <CurveTo cx="37.20" cy="303.30" x="31.85" y="299.50" />
                <CurveTo cx="25.95" cy="295.35" x="23.65" y="289.15" />
                <CurveTo cx="20.45" cy="278.75" x="18.30" y="260.60" />
                <CurveTo cx="17.35" cy="252.55" x="16.30" y="240.45" />
                <CurveTo cx="15.65" cy="232.70" x="14.25" y="215.05" />
                <LineTo x="14.15" y="213.90" />
                <CurveTo cx="13.70" cy="208.60" x="14.35" y="207.15" />
                <CurveTo cx="15.35" cy="204.90" x="19.65" y="205.20" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (42, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 42
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="20.00" y="4.75" />
                <CurveTo cx="38.70" cy="0.00" x="59.00" y="0.30" />
                <CurveTo cx="78.30" cy="0.55" x="98.70" y="5.30" />
                <LineTo x="98.85" y="5.35" />
                <CurveTo cx="101.75" cy="6.05" x="103.30" y="6.70" />
                <CurveTo cx="106.10" cy="7.80" x="106.95" y="9.35" />
                <CurveTo cx="109.40" cy="13.75" x="111.60" y="26.90" />
                <CurveTo cx="112.35" cy="31.65" x="113.10" y="37.65" />
                <CurveTo cx="113.55" cy="41.05" x="114.15" y="46.45" />
                <CurveTo cx="114.55" cy="50.05" x="114.65" y="50.75" />
                <CurveTo cx="118.75" cy="106.10" x="116.85" y="170.95" />
                <CurveTo cx="115.00" cy="234.80" x="107.65" y="299.35" />
                <CurveTo cx="107.50" cy="305.55" x="103.15" y="309.10" />
                <CurveTo cx="100.20" cy="311.55" x="93.70" y="313.60" />
                <CurveTo cx="76.65" cy="319.00" x="58.60" y="319.30" />
                <CurveTo cx="42.20" cy="319.55" x="22.75" y="315.55" />
                <CurveTo cx="12.20" cy="313.35" x="11.55" y="296.95" />
                <CurveTo cx="7.95" cy="259.50" x="5.65" y="229.60" />
                <CurveTo cx="2.90" cy="194.55" x="1.55" y="165.20" />
                <CurveTo cx="0.00" cy="132.50" x="0.05" y="104.90" />
                <CurveTo cx="0.10" cy="75.00" x="2.00" y="50.05" />
                <CurveTo cx="2.85" cy="36.00" x="5.45" y="25.15" />
                <CurveTo cx="8.45" cy="12.40" x="13.10" y="8.20" />
                <CurveTo cx="15.55" cy="5.90" x="20.00" y="4.75" />

                <MoveTo x="76.35" y="287.70" />
                <CurveTo cx="76.05" cy="296.40" x="70.75" y="301.50" />
                <CurveTo cx="65.95" cy="306.15" x="59.10" y="306.15" />
                <CurveTo cx="52.25" cy="306.15" x="47.25" y="301.50" />
                <CurveTo cx="41.70" cy="296.30" x="40.95" y="287.35" />
                <CurveTo cx="40.65" cy="284.05" x="46.20" y="281.85" />
                <CurveTo cx="51.30" cy="279.75" x="58.50" y="279.55" />
                <CurveTo cx="65.95" cy="279.35" x="70.90" y="281.25" />
                <CurveTo cx="76.50" cy="283.45" x="76.35" y="287.70" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (43, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 43
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="47.35" y="0.85" />
                <CurveTo cx="59.90" cy="9.15" x="70.80" y="9.15" />
                <CurveTo cx="82.25" cy="9.15" x="94.25" y="0.00" />
                <CurveTo cx="96.25" cy="0.10" x="100.30" y="0.30" />
                <CurveTo cx="110.60" cy="0.70" x="115.55" y="1.25" />
                <CurveTo cx="123.75" cy="2.10" x="129.05" y="3.95" />
                <CurveTo cx="142.30" cy="8.60" x="141.90" y="21.60" />
                <LineTo x="142.40" y="179.90" />
                <CurveTo cx="142.45" cy="196.60" x="133.65" y="207.60" />
                <CurveTo cx="125.30" cy="218.10" x="109.45" y="222.75" />
                <CurveTo cx="93.25" cy="227.50" x="73.90" y="228.20" />
                <CurveTo cx="52.60" cy="228.90" x="36.50" y="224.15" />
                <CurveTo cx="20.25" cy="219.35" x="10.60" y="208.30" />
                <CurveTo cx="0.20" cy="196.35" x="0.25" y="179.90" />
                <LineTo x="0.70" y="24.90" />
                <CurveTo cx="0.00" cy="11.20" x="13.35" y="5.90" />
                <CurveTo cx="18.70" cy="3.80" x="27.15" y="2.65" />
                <CurveTo cx="32.20" cy="1.95" x="42.95" y="1.15" />
                <LineTo x="47.35" y="0.85" />

                <MoveTo x="45.55" y="122.05" />
                <LineTo x="99.20" y="122.05" />
                <CurveTo cx="101.40" cy="122.05" x="101.40" y="119.85" />
                <LineTo x="101.40" y="66.45" />
                <CurveTo cx="101.40" cy="64.20" x="99.20" y="64.20" />
                <LineTo x="45.55" y="64.20" />
                <CurveTo cx="43.40" cy="64.20" x="43.40" y="66.45" />
                <LineTo x="43.40" y="119.85" />
                <CurveTo cx="43.40" cy="122.05" x="45.55" y="122.05" />

                <MoveTo x="58.85" y="23.50" />
                <LineTo x="83.30" y="23.50" />
                <CurveTo cx="86.10" cy="23.50" x="86.10" y="26.10" />
                <LineTo x="86.10" y="36.60" />
                <CurveTo cx="86.10" cy="39.20" x="83.30" y="39.20" />
                <LineTo x="58.85" y="39.20" />
                <CurveTo cx="56.10" cy="39.20" x="56.10" y="36.60" />
                <LineTo x="56.10" y="26.10" />
                <CurveTo cx="56.10" cy="23.50" x="58.85" y="23.50" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (44, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 44
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="5.65" y="4.45" />
                <CurveTo cx="32.30" cy="0.00" x="62.85" y="0.40" />
                <CurveTo cx="88.10" cy="0.75" x="117.45" y="4.45" />
                <CurveTo cx="122.50" cy="5.10" x="122.50" y="11.65" />
                <LineTo x="122.50" y="146.35" />
                <CurveTo cx="122.50" cy="158.25" x="103.35" y="165.50" />
                <CurveTo cx="86.05" cy="172.10" x="61.25" y="172.35" />
                <CurveTo cx="36.25" cy="172.60" x="19.15" y="166.20" />
                <CurveTo cx="0.00" cy="159.00" x="0.00" y="146.35" />
                <LineTo x="0.00" y="11.65" />
                <CurveTo cx="0.00" cy="5.45" x="5.65" y="4.45" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (45, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 45
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 3" x="50" y="420">
            <Contour x="0" y="0">
                <MoveTo x="134.10" y="210.20" />
                <LineTo x="133.85" y="43.10" />
                <CurveTo cx="133.80" cy="27.50" x="125.80" y="17.95" />
                <CurveTo cx="119.65" cy="10.55" x="107.70" y="6.00" />
                <CurveTo cx="92.10" cy="1.45" x="70.05" y="0.75" />
                <CurveTo cx="46.30" cy="0.00" x="30.60" y="4.60" />
                <CurveTo cx="24.90" cy="6.95" x="21.60" y="8.75" />
                <CurveTo cx="15.90" cy="11.80" x="11.90" y="15.60" />
                <CurveTo cx="0.90" cy="25.90" x="0.85" y="42.55" />
                <LineTo x="0.00" y="210.10" />
                <CurveTo cx="68.75" cy="207.70" x="134.10" y="210.20" />

                <MoveTo x="24.55" y="55.90" />
                <LineTo x="110.00" y="55.90" />
                <CurveTo cx="112.85" cy="55.90" x="112.85" y="58.70" />
                <LineTo x="112.85" y="165.45" />
                <CurveTo cx="112.85" cy="168.25" x="110.00" y="168.25" />
                <LineTo x="24.55" y="168.25" />
                <CurveTo cx="21.70" cy="168.25" x="21.70" y="165.45" />
                <LineTo x="21.70" y="58.70" />
                <CurveTo cx="21.70" cy="55.90" x="24.55" y="55.90" />

                <MoveTo x="74.25" y="22.20" />
                <CurveTo cx="79.10" cy="21.90" x="84.50" y="21.30" />
                <CurveTo cx="89.30" cy="20.80" x="94.70" y="20.10" />
                <CurveTo cx="95.05" cy="20.05" x="95.35" y="20.30" />
                <CurveTo cx="95.65" cy="20.50" x="95.70" y="20.90" />
                <CurveTo cx="95.80" cy="21.80" x="94.85" y="21.90" />
                <CurveTo cx="84.65" cy="23.80" x="74.45" y="24.00" />
                <CurveTo cx="73.55" cy="24.10" x="73.45" y="23.20" />
                <CurveTo cx="73.40" cy="22.80" x="73.65" y="22.50" />
                <CurveTo cx="73.90" cy="22.20" x="74.25" y="22.20" />

                <MoveTo x="74.45" y="13.70" />
                <CurveTo cx="79.35" cy="14.00" x="84.65" y="14.55" />
                <CurveTo cx="89.50" cy="15.05" x="94.85" y="15.80" />
                <CurveTo cx="95.75" cy="15.90" x="95.85" y="14.95" />
                <CurveTo cx="95.95" cy="14.15" x="95.05" y="13.95" />
                <CurveTo cx="84.85" cy="12.15" x="74.60" y="11.85" />
                <CurveTo cx="73.70" cy="11.85" x="73.60" y="12.70" />
                <CurveTo cx="73.50" cy="13.60" x="74.45" y="13.70" />

                <MoveTo x="60.40" y="13.70" />
                <CurveTo cx="55.55" cy="14.00" x="50.20" y="14.60" />
                <CurveTo cx="45.45" cy="15.10" x="40.00" y="15.80" />
                <CurveTo cx="39.65" cy="15.85" x="39.35" y="15.65" />
                <CurveTo cx="39.05" cy="15.40" x="39.00" y="15.00" />
                <CurveTo cx="38.90" cy="14.20" x="39.80" y="14.00" />
                <CurveTo cx="50.00" cy="12.15" x="60.25" y="11.90" />
                <CurveTo cx="61.15" cy="11.80" x="61.25" y="12.70" />
                <CurveTo cx="61.30" cy="13.10" x="61.05" y="13.40" />
                <CurveTo cx="60.80" cy="13.70" x="60.40" y="13.70" />

                <MoveTo x="60.60" y="22.10" />
                <CurveTo cx="55.75" cy="21.80" x="50.40" y="21.20" />
                <CurveTo cx="45.55" cy="20.70" x="40.15" y="20.00" />
                <CurveTo cx="39.80" cy="19.95" x="39.50" y="20.20" />
                <CurveTo cx="39.20" cy="20.40" x="39.15" y="20.80" />
                <CurveTo cx="39.05" cy="21.70" x="40.00" y="21.80" />
                <CurveTo cx="50.20" cy="23.70" x="60.40" y="23.90" />
                <CurveTo cx="61.30" cy="24.00" x="61.40" y="23.10" />
                <CurveTo cx="61.45" cy="22.70" x="61.20" y="22.40" />
                <CurveTo cx="60.95" cy="22.10" x="60.60" y="22.10" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (46, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 46
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 4" x="300" y="420">
            <Contour x="0" y="0">
                <MoveTo x="129.35" y="0.00" />
                <LineTo x="131.50" y="167.20" />
                <CurveTo cx="131.70" cy="183.55" x="125.60" y="192.00" />
                <CurveTo cx="119.65" cy="200.15" x="104.90" y="204.60" />
                <CurveTo cx="89.90" cy="209.15" x="68.40" y="209.65" />
                <CurveTo cx="45.40" cy="210.25" x="30.10" y="205.55" />
                <CurveTo cx="15.05" cy="200.95" x="8.25" y="192.50" />
                <CurveTo cx="0.90" cy="183.40" x="0.85" y="167.30" />
                <LineTo x="0.00" y="0.50" />
                <CurveTo cx="33.25" cy="2.65" x="66.20" y="2.45" />
                <CurveTo cx="97.90" cy="2.30" x="129.35" y="0.00" />

                <MoveTo x="48.90" y="11.85" />
                <CurveTo cx="64.85" cy="10.90" x="82.05" y="12.00" />
                <CurveTo cx="93.60" cy="12.70" x="94.40" y="24.50" />
                <CurveTo cx="94.90" cy="31.55" x="94.95" y="37.15" />
                <CurveTo cx="94.95" cy="43.80" x="94.40" y="50.20" />
                <CurveTo cx="93.35" cy="61.60" x="82.95" y="62.15" />
                <CurveTo cx="73.40" cy="62.70" x="65.50" y="62.70" />
                <CurveTo cx="56.90" cy="62.75" x="48.05" y="62.15" />
                <CurveTo cx="37.05" cy="61.45" x="36.05" y="50.50" />
                <CurveTo cx="34.80" cy="36.00" x="35.55" y="24.90" />
                <CurveTo cx="35.90" cy="19.95" x="40.00" y="16.00" />
                <CurveTo cx="44.05" cy="12.10" x="48.90" y="11.85" />

                <MoveTo x="29.50" y="22.65" />
                <LineTo x="30.25" y="54.40" />
                <CurveTo cx="30.30" cy="56.85" x="31.70" y="59.05" />
                <CurveTo cx="32.65" cy="60.55" x="34.65" y="62.30" />
                <CurveTo cx="35.10" cy="62.70" x="35.75" y="63.15" />
                <CurveTo cx="36.90" cy="64.00" x="36.90" y="65.85" />
                <LineTo x="37.60" y="73.20" />
                <CurveTo cx="37.55" cy="77.55" x="34.40" y="79.20" />
                <CurveTo cx="31.20" cy="80.85" x="26.85" y="78.90" />
                <LineTo x="26.45" y="78.75" />
                <CurveTo cx="18.20" cy="75.10" x="14.30" y="72.05" />
                <CurveTo cx="7.45" cy="66.75" x="6.55" y="59.55" />
                <LineTo x="6.45" y="28.05" />
                <CurveTo cx="6.45" cy="22.20" x="13.15" y="19.30" />
                <LineTo x="23.25" y="17.95" />
                <CurveTo cx="30.00" cy="16.90" x="29.50" y="22.65" />

                <MoveTo x="101.20" y="22.60" />
                <LineTo x="100.50" y="54.40" />
                <CurveTo cx="100.40" cy="58.55" x="96.05" y="62.30" />
                <CurveTo cx="95.60" cy="62.70" x="94.95" y="63.15" />
                <CurveTo cx="93.85" cy="64.00" x="93.80" y="65.80" />
                <LineTo x="93.10" y="73.20" />
                <CurveTo cx="93.15" cy="77.55" x="96.35" y="79.20" />
                <CurveTo cx="99.50" cy="80.85" x="103.85" y="78.90" />
                <LineTo x="104.25" y="78.75" />
                <CurveTo cx="112.50" cy="75.10" x="116.40" y="72.05" />
                <CurveTo cx="123.25" cy="66.75" x="124.20" y="59.55" />
                <LineTo x="124.25" y="28.00" />
                <CurveTo cx="124.25" cy="22.20" x="117.55" y="19.30" />
                <LineTo x="107.50" y="17.95" />
                <CurveTo cx="100.65" cy="16.85" x="101.20" y="22.60" />

                <MoveTo x="51.15" y="68.35" />
                <CurveTo cx="58.80" cy="68.95" x="65.50" y="69.00" />
                <CurveTo cx="72.60" cy="69.00" x="79.80" y="68.35" />
                <CurveTo cx="81.45" cy="68.20" x="82.65" y="69.40" />
                <CurveTo cx="83.85" cy="70.60" x="83.75" y="72.25" />
                <LineTo x="83.50" y="80.50" />
                <CurveTo cx="83.45" cy="81.95" x="81.75" y="82.70" />
                <CurveTo cx="80.50" cy="83.20" x="78.40" y="83.35" />
                <CurveTo cx="70.60" cy="83.80" x="65.05" y="83.90" />
                <CurveTo cx="58.80" cy="83.95" x="51.65" y="83.55" />
                <LineTo x="51.20" y="83.55" />
                <CurveTo cx="49.35" cy="83.45" x="48.75" y="83.20" />
                <CurveTo cx="47.80" cy="82.75" x="47.65" y="81.40" />
                <LineTo x="46.75" y="72.25" />
                <CurveTo cx="46.60" cy="70.65" x="48.05" y="69.40" />
                <CurveTo cx="49.45" cy="68.20" x="51.15" y="68.35" />

                <MoveTo x="17.30" y="81.00" />
                <CurveTo cx="23.65" cy="84.05" x="26.65" y="85.25" />
                <CurveTo cx="31.25" cy="87.10" x="36.70" y="88.50" />
                <CurveTo cx="40.15" cy="91.05" x="40.65" y="93.05" />
                <LineTo x="41.85" y="174.35" />
                <CurveTo cx="41.90" cy="178.75" x="37.20" y="178.60" />
                <CurveTo cx="27.55" cy="178.30" x="20.00" y="174.65" />
                <CurveTo cx="9.85" cy="169.70" x="9.25" y="160.35" />
                <LineTo x="9.10" y="153.20" />
                <LineTo x="8.80" y="138.35" />
                <LineTo x="8.45" y="119.45" />
                <CurveTo cx="8.00" cy="95.50" x="8.00" y="84.65" />
                <CurveTo cx="9.00" cy="78.30" x="17.30" y="81.00" />

                <MoveTo x="113.65" y="80.75" />
                <LineTo x="110.25" y="82.40" />
                <CurveTo cx="105.30" cy="84.85" x="102.75" y="85.85" />
                <CurveTo cx="98.75" cy="87.55" x="94.25" y="88.65" />
                <LineTo x="93.45" y="89.25" />
                <CurveTo cx="90.70" cy="91.35" x="90.30" y="92.80" />
                <LineTo x="89.50" y="174.50" />
                <CurveTo cx="89.45" cy="178.45" x="93.75" y="178.35" />
                <CurveTo cx="103.50" cy="178.05" x="111.15" y="174.40" />
                <CurveTo cx="121.50" cy="169.45" x="122.05" y="160.10" />
                <LineTo x="122.15" y="153.00" />
                <LineTo x="122.40" y="138.10" />
                <LineTo x="122.65" y="119.20" />
                <CurveTo cx="122.80" cy="108.30" x="122.85" y="100.05" />
                <CurveTo cx="122.95" cy="90.30" x="122.95" y="84.40" />
                <CurveTo cx="121.95" cy="78.05" x="113.65" y="80.75" />

                <MoveTo x="49.55" y="90.85" />
                <CurveTo cx="58.05" cy="92.90" x="65.80" y="92.80" />
                <CurveTo cx="72.90" cy="92.75" x="82.00" y="90.85" />
                <CurveTo cx="84.55" cy="90.25" x="84.55" y="93.35" />
                <LineTo x="82.85" y="175.65" />
                <CurveTo cx="82.75" cy="179.80" x="77.85" y="179.70" />
                <LineTo x="53.10" y="179.70" />
                <CurveTo cx="48.80" cy="179.70" x="48.65" y="175.20" />
                <LineTo x="46.10" y="92.95" />
                <CurveTo cx="45.95" cy="90.05" x="49.55" y="90.85" />

                <MoveTo x="65.85" y="203.45" />
                <CurveTo cx="67.20" cy="203.45" x="68.20" y="202.50" />
                <CurveTo cx="69.15" cy="201.50" x="69.15" y="200.15" />
                <CurveTo cx="69.15" cy="198.80" x="68.20" y="197.85" />
                <CurveTo cx="67.20" cy="196.90" x="65.85" y="196.90" />
                <CurveTo cx="64.50" cy="196.90" x="63.55" y="197.85" />
                <CurveTo cx="62.55" cy="198.80" x="62.55" y="200.15" />
                <CurveTo cx="62.55" cy="201.50" x="63.55" y="202.50" />
                <CurveTo cx="64.50" cy="203.45" x="65.85" y="203.45" />

                <MoveTo x="107.20" y="193.95" />
                <LineTo x="100.50" y="196.25" />
                <CurveTo cx="99.35" cy="196.65" x="98.85" y="197.75" />
                <CurveTo cx="98.30" cy="198.90" x="98.70" y="200.10" />
                <LineTo x="98.90" y="200.75" />
                <CurveTo cx="99.30" cy="201.95" x="100.35" y="202.55" />
                <CurveTo cx="101.45" cy="203.10" x="102.55" y="202.70" />
                <LineTo x="109.25" y="200.40" />
                <CurveTo cx="110.35" cy="200.05" x="110.90" y="198.90" />
                <CurveTo cx="111.45" cy="197.75" x="111.05" y="196.55" />
                <LineTo x="110.85" y="195.90" />
                <CurveTo cx="110.45" cy="194.70" x="109.40" y="194.10" />
                <CurveTo cx="108.30" cy="193.55" x="107.20" y="193.95" />

                <MoveTo x="27.05" y="195.25" />
                <LineTo x="33.75" y="197.55" />
                <CurveTo cx="34.85" cy="197.90" x="35.40" y="199.05" />
                <CurveTo cx="35.95" cy="200.20" x="35.55" y="201.40" />
                <LineTo x="35.30" y="202.05" />
                <CurveTo cx="34.90" cy="203.25" x="33.85" y="203.85" />
                <CurveTo cx="32.75" cy="204.40" x="31.65" y="204.00" />
                <LineTo x="24.95" y="201.70" />
                <CurveTo cx="23.85" cy="201.35" x="23.30" y="200.20" />
                <CurveTo cx="22.75" cy="199.05" x="23.15" y="197.85" />
                <LineTo x="23.35" y="197.20" />
                <CurveTo cx="23.75" cy="196.00" x="24.85" y="195.40" />
                <CurveTo cx="25.95" cy="194.85" x="27.05" y="195.25" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (47, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 47
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="105.60" y="3.15" />
                <CurveTo cx="113.65" cy="6.65" x="116.10" y="10.60" />
                <CurveTo cx="118.05" cy="13.70" x="118.30" y="20.50" />
                <CurveTo cx="118.35" cy="22.95" x="118.45" y="24.45" />
                <LineTo x="118.60" y="25.75" />
                <CurveTo cx="119.00" cy="28.95" x="119.10" y="30.55" />
                <CurveTo cx="119.25" cy="33.40" x="118.65" y="33.40" />
                <LineTo x="114.65" y="30.05" />
                <CurveTo cx="112.50" cy="28.25" x="108.20" y="26.25" />
                <CurveTo cx="106.80" cy="25.60" x="106.05" y="25.20" />
                <CurveTo cx="103.00" cy="23.70" x="101.80" y="20.20" />
                <CurveTo cx="100.95" cy="17.70" x="100.95" y="13.65" />
                <LineTo x="100.95" y="7.15" />
                <CurveTo cx="100.95" cy="3.35" x="101.55" y="2.65" />
                <CurveTo cx="102.35" cy="1.70" x="105.60" y="3.15" />
                <MoveTo x="32.20" y="3.00" />
                <CurveTo cx="24.20" cy="6.50" x="21.65" y="10.50" />
                <CurveTo cx="19.70" cy="13.55" x="19.45" y="20.35" />
                <CurveTo cx="19.40" cy="22.80" x="19.30" y="24.30" />
                <LineTo x="19.25" y="24.85" />
                <LineTo x="19.15" y="25.65" />
                <CurveTo cx="18.75" cy="28.90" x="18.65" y="30.45" />
                <CurveTo cx="18.50" cy="33.30" x="19.10" y="33.30" />
                <LineTo x="20.25" y="32.30" />
                <LineTo x="21.85" y="30.95" />
                <LineTo x="23.10" y="29.90" />
                <CurveTo cx="25.30" cy="28.10" x="29.65" y="26.05" />
                <LineTo x="31.70" y="25.05" />
                <CurveTo cx="34.75" cy="23.55" x="35.95" y="20.10" />
                <CurveTo cx="36.80" cy="17.60" x="36.80" y="13.55" />
                <LineTo x="36.80" y="7.00" />
                <LineTo x="36.80" y="6.85" />
                <CurveTo cx="36.80" cy="3.15" x="36.25" y="2.50" />
                <CurveTo cx="35.45" cy="1.60" x="32.20" y="3.00" />
                <MoveTo x="52.30" y="0.35" />
                <LineTo x="90.05" y="0.35" />
                <CurveTo cx="96.60" cy="0.35" x="98.45" y="3.70" />
                <CurveTo cx="99.65" cy="5.85" x="99.55" y="12.60" />
                <CurveTo cx="99.50" cy="16.10" x="99.60" y="18.00" />
                <LineTo x="93.15" y="18.00" />
                <LineTo x="93.15" y="25.65" />
                <LineTo x="102.45" y="25.65" />
                <CurveTo cx="103.75" cy="26.70" x="106.40" y="28.10" />
                <CurveTo cx="110.80" cy="30.40" x="116.30" y="35.75" />
                <CurveTo cx="121.60" cy="40.90" x="125.85" y="46.80" />
                <CurveTo cx="129.60" cy="51.95" x="131.15" y="57.10" />
                <CurveTo cx="132.35" cy="61.00" x="133.20" y="68.55" />
                <CurveTo cx="135.75" cy="91.15" x="135.05" y="119.05" />
                <CurveTo cx="134.40" cy="147.50" x="130.60" y="171.65" />
                <CurveTo cx="129.40" cy="179.30" x="127.95" y="184.30" />
                <CurveTo cx="126.15" cy="190.50" x="123.05" y="196.25" />
                <CurveTo cx="117.60" cy="206.45" x="110.35" y="212.90" />
                <CurveTo cx="109.95" cy="213.25" x="109.30" y="213.75" />
                <CurveTo cx="107.70" cy="215.00" x="107.25" y="216.90" />
                <CurveTo cx="106.75" cy="219.05" x="104.60" y="220.15" />
                <LineTo x="104.20" y="220.35" />
                <CurveTo cx="97.80" cy="223.65" x="90.20" y="225.70" />
                <CurveTo cx="81.15" cy="228.15" x="70.80" y="228.70" />
                <CurveTo cx="46.90" cy="227.90" x="33.05" y="220.45" />
                <LineTo x="32.45" y="220.15" />
                <CurveTo cx="30.25" cy="219.00" x="29.95" y="216.75" />
                <CurveTo cx="29.70" cy="215.05" x="25.75" y="210.75" />
                <CurveTo cx="23.55" cy="208.35" x="19.10" y="204.10" />
                <CurveTo cx="17.55" cy="202.60" x="17.15" y="202.20" />
                <CurveTo cx="14.00" cy="196.55" x="11.50" y="188.95" />
                <CurveTo cx="9.95" cy="184.40" x="7.50" y="175.10" />
                <LineTo x="7.00" y="173.30" />
                <CurveTo cx="1.70" cy="153.20" x="0.60" y="125.45" />
                <CurveTo cx="0.00" cy="110.30" x="1.05" y="96.25" />
                <CurveTo cx="2.15" cy="80.60" x="5.25" y="67.45" />
                <LineTo x="5.85" y="64.85" />
                <CurveTo cx="7.30" cy="58.40" x="8.25" y="55.20" />
                <CurveTo cx="9.90" cy="49.75" x="11.95" y="46.35" />
                <CurveTo cx="14.80" cy="41.65" x="20.10" y="37.10" />
                <CurveTo cx="23.30" cy="34.35" x="30.25" y="29.50" />
                <CurveTo cx="33.85" cy="27.00" x="35.55" y="25.70" />
                <LineTo x="44.65" y="25.70" />
                <LineTo x="44.65" y="17.85" />
                <LineTo x="37.90" y="17.85" />
                <CurveTo cx="38.25" cy="14.70" x="38.10" y="8.70" />
                <LineTo x="38.10" y="8.20" />
                <CurveTo cx="38.05" cy="2.40" x="40.40" y="0.95" />
                <CurveTo cx="42.00" cy="0.00" x="47.80" y="0.25" />
                <CurveTo cx="50.65" cy="0.35" x="52.30" y="0.35" />

                <MoveTo x="49.10" y="71.90" />
                <LineTo x="88.65" y="71.90" />
                <CurveTo cx="96.95" cy="71.90" x="96.95" y="80.20" />
                <LineTo x="96.95" y="101.25" />
                <CurveTo cx="96.95" cy="109.55" x="88.65" y="109.55" />
                <LineTo x="49.10" y="109.55" />
                <CurveTo cx="40.80" cy="109.55" x="40.80" y="101.25" />
                <LineTo x="40.80" y="80.20" />
                <CurveTo cx="40.80" cy="71.90" x="49.10" y="71.90" />

                <MoveTo x="68.85" y="44.15" />
                <CurveTo cx="74.30" cy="44.15" x="78.65" y="45.85" />
                <CurveTo cx="82.95" cy="47.50" x="84.55" y="50.15" />
                <CurveTo cx="84.95" cy="50.80" x="84.80" y="51.75" />
                <CurveTo cx="84.60" cy="52.75" x="83.95" y="52.70" />
                <CurveTo cx="82.95" cy="52.60" x="81.45" y="52.20" />
                <CurveTo cx="80.55" cy="52.00" x="78.50" y="51.40" />
                <LineTo x="76.20" y="50.75" />
                <CurveTo cx="73.05" cy="50.50" x="69.55" y="50.45" />
                <CurveTo cx="65.90" cy="50.40" x="62.80" y="50.55" />
                <CurveTo cx="61.00" cy="50.90" x="58.15" y="51.65" />
                <CurveTo cx="54.70" cy="52.60" x="53.80" y="52.40" />
                <CurveTo cx="53.15" cy="52.30" x="53.00" y="51.45" />
                <CurveTo cx="52.80" cy="50.60" x="53.25" y="49.95" />
                <CurveTo cx="54.95" cy="47.40" x="59.15" y="45.80" />
                <CurveTo cx="63.50" cy="44.15" x="68.85" y="44.15" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (48, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 48
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="18.60" y="0.00" />
                <LineTo x="43.85" y="0.00" />
                <CurveTo cx="46.10" cy="0.00" x="46.00" y="1.80" />
                <CurveTo cx="45.95" cy="3.55" x="47.30" y="4.15" />
                <CurveTo cx="48.95" cy="4.95" x="53.35" y="4.80" />
                <CurveTo cx="57.40" cy="4.95" x="59.15" y="4.10" />
                <CurveTo cx="60.30" cy="3.60" x="60.60" y="1.70" />
                <CurveTo cx="60.95" cy="0.00" x="62.10" y="0.00" />
                <LineTo x="81.30" y="0.00" />
                <LineTo x="93.65" y="0.00" />
                <CurveTo cx="100.55" cy="0.00" x="102.70" y="2.60" />
                <CurveTo cx="105.25" cy="5.70" x="100.50" y="5.60" />
                <CurveTo cx="95.45" cy="5.45" x="92.75" y="8.50" />
                <CurveTo cx="88.70" cy="12.95" x="91.10" y="18.90" />
                <CurveTo cx="93.65" cy="25.10" x="101.00" y="24.60" />
                <CurveTo cx="101.50" cy="24.55" x="102.45" y="24.40" />
                <CurveTo cx="104.95" cy="23.90" x="104.95" y="26.05" />
                <LineTo x="104.95" y="26.40" />
                <CurveTo cx="104.95" cy="29.35" x="103.10" y="29.30" />
                <LineTo x="102.10" y="29.30" />
                <LineTo x="68.85" y="29.30" />
                <CurveTo cx="67.30" cy="29.30" x="67.35" y="27.75" />
                <CurveTo cx="67.35" cy="27.05" x="67.35" y="26.60" />
                <LineTo x="67.20" y="15.00" />
                <CurveTo cx="67.15" cy="9.00" x="61.60" y="9.00" />
                <LineTo x="57.25" y="9.05" />
                <LineTo x="52.90" y="9.05" />
                <CurveTo cx="51.75" cy="9.05" x="49.40" y="9.15" />
                <CurveTo cx="47.10" cy="9.25" x="45.90" y="9.25" />
                <CurveTo cx="42.60" cy="9.25" x="41.40" y="10.05" />
                <CurveTo cx="39.75" cy="11.10" x="39.80" y="14.55" />
                <LineTo x="39.95" y="27.45" />
                <CurveTo cx="40.00" cy="29.35" x="38.90" y="29.30" />
                <LineTo x="38.20" y="29.30" />
                <LineTo x="5.10" y="29.30" />
                <LineTo x="4.80" y="29.30" />
                <CurveTo cx="2.25" cy="29.30" x="2.30" y="26.60" />
                <LineTo x="2.30" y="25.25" />
                <LineTo x="2.30" y="15.90" />
                <LineTo x="2.30" y="15.05" />
                <CurveTo cx="2.25" cy="12.15" x="3.50" y="10.45" />
                <CurveTo cx="8.55" cy="3.20" x="18.60" y="0.00" />
                <MoveTo x="9.60" y="36.05" />
                <LineTo x="95.55" y="36.05" />
                <CurveTo cx="103.30" cy="36.05" x="105.35" y="40.20" />
                <CurveTo cx="106.70" cy="42.80" x="106.35" y="51.10" />
                <CurveTo cx="106.20" cy="54.60" x="106.20" y="56.50" />
                <CurveTo cx="106.20" cy="63.75" x="106.95" y="78.35" />
                <CurveTo cx="107.70" cy="92.95" x="107.70" y="100.25" />
                <LineTo x="107.70" y="105.85" />
                <CurveTo cx="107.70" cy="117.60" x="106.55" y="131.80" />
                <CurveTo cx="105.80" cy="140.30" x="103.95" y="157.35" />
                <CurveTo cx="102.65" cy="169.00" x="102.05" y="174.95" />
                <CurveTo cx="99.60" cy="200.50" x="93.25" y="207.00" />
                <CurveTo cx="85.30" cy="215.10" x="56.35" y="215.90" />
                <CurveTo cx="23.70" cy="214.40" x="14.50" y="207.25" />
                <CurveTo cx="6.10" cy="200.70" x="4.10" y="177.55" />
                <CurveTo cx="3.10" cy="165.85" x="2.55" y="159.20" />
                <CurveTo cx="1.80" cy="149.10" x="1.25" y="140.90" />
                <CurveTo cx="0.00" cy="120.70" x="0.00" y="104.20" />
                <CurveTo cx="0.00" cy="89.40" x="0.35" y="74.35" />
                <CurveTo cx="0.70" cy="60.10" x="1.40" y="44.60" />
                <LineTo x="1.40" y="44.15" />
                <CurveTo cx="1.65" cy="39.55" x="2.65" y="38.15" />
                <CurveTo cx="4.25" cy="36.05" x="9.60" y="36.05" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (49, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 49
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 3" x="50" y="420">
            <Contour x="0" y="0">
                <MoveTo x="108.45" y="110.95" />
                <CurveTo cx="108.45" cy="123.10" x="108.05" y="136.15" />
                <CurveTo cx="107.80" cy="144.15" x="107.05" y="160.20" />
                <CurveTo cx="106.65" cy="169.50" x="106.45" y="174.50" />
                <CurveTo cx="104.45" cy="182.70" x="99.85" y="188.50" />
                <CurveTo cx="96.85" cy="192.20" x="91.95" y="195.60" />
                <CurveTo cx="86.70" cy="199.25" x="85.10" y="204.55" />
                <CurveTo cx="83.85" cy="208.50" x="84.20" y="216.35" />
                <CurveTo cx="84.50" cy="222.60" x="83.00" y="223.95" />
                <CurveTo cx="81.95" cy="225.00" x="77.10" y="224.65" />
                <CurveTo cx="73.65" cy="224.45" x="71.45" y="224.45" />
                <LineTo x="59.40" y="224.55" />
                <LineTo x="47.75" y="224.65" />
                <LineTo x="36.05" y="224.75" />
                <LineTo x="34.15" y="224.75" />
                <CurveTo cx="26.60" cy="224.80" x="25.15" y="223.50" />
                <CurveTo cx="23.40" cy="222.00" x="23.35" y="214.35" />
                <CurveTo cx="23.30" cy="212.40" x="23.45" y="209.05" />
                <CurveTo cx="23.70" cy="202.50" x="22.70" y="199.35" />
                <CurveTo cx="21.45" cy="195.30" x="15.15" y="192.00" />
                <CurveTo cx="12.40" cy="190.55" x="11.05" y="189.60" />
                <CurveTo cx="7.10" cy="186.70" x="3.90" y="180.35" />
                <CurveTo cx="1.95" cy="176.65" x="1.65" y="168.55" />
                <LineTo x="1.50" y="165.20" />
                <LineTo x="0.95" y="150.95" />
                <CurveTo cx="0.65" cy="143.05" x="0.45" y="136.70" />
                <CurveTo cx="0.00" cy="120.90" x="0.00" y="108.20" />
                <CurveTo cx="1.55" cy="77.30" x="4.10" y="54.80" />
                <CurveTo cx="6.90" cy="30.15" x="11.00" y="14.60" />
                <CurveTo cx="16.95" cy="7.40" x="29.45" y="3.55" />
                <CurveTo cx="41.00" cy="0.00" x="55.15" y="0.15" />
                <CurveTo cx="69.15" cy="0.30" x="80.95" y="4.05" />
                <CurveTo cx="93.45" cy="7.95" x="99.60" y="14.80" />
                <CurveTo cx="104.30" cy="28.10" x="106.50" y="53.15" />
                <CurveTo cx="108.55" cy="76.65" x="108.45" y="110.95" />

                <MoveTo x="58.40" y="33.95" />
                <LineTo x="62.00" y="33.95" />
                <CurveTo cx="62.90" cy="33.95" x="63.50" y="34.10" />
                <CurveTo cx="64.05" cy="34.20" x="64.90" y="34.55" />
                <LineTo x="66.10" y="35.05" />
                <LineTo x="67.30" y="35.55" />
                <CurveTo cx="68.85" cy="36.20" x="68.90" y="37.30" />
                <CurveTo cx="69.00" cy="38.40" x="67.20" y="38.40" />
                <LineTo x="58.90" y="38.40" />
                <LineTo x="58.20" y="38.40" />
                <CurveTo cx="57.40" cy="38.45" x="57.25" y="38.25" />
                <CurveTo cx="57.10" cy="38.05" x="57.10" y="37.00" />
                <CurveTo cx="57.10" cy="36.55" x="57.00" y="35.75" />
                <CurveTo cx="56.90" cy="34.75" x="57.05" y="34.40" />
                <CurveTo cx="57.30" cy="33.95" x="58.40" y="33.95" />

                <MoveTo x="53.35" y="33.75" />
                <LineTo x="49.75" y="33.75" />
                <CurveTo cx="48.90" cy="33.75" x="48.25" y="33.90" />
                <CurveTo cx="47.75" cy="34.00" x="46.85" y="34.40" />
                <LineTo x="45.65" y="34.90" />
                <LineTo x="44.45" y="35.40" />
                <CurveTo cx="42.85" cy="36.05" x="42.80" y="37.10" />
                <CurveTo cx="42.70" cy="38.20" x="44.55" y="38.20" />
                <LineTo x="52.85" y="38.20" />
                <LineTo x="53.65" y="38.20" />
                <CurveTo cx="54.35" cy="38.25" x="54.50" y="38.10" />
                <CurveTo cx="54.65" cy="37.90" x="54.65" y="36.85" />
                <CurveTo cx="54.65" cy="36.40" x="54.75" y="35.65" />
                <CurveTo cx="54.85" cy="34.55" x="54.65" y="34.25" />
                <CurveTo cx="54.40" cy="33.75" x="53.35" y="33.75" />

                <MoveTo x="47.35" y="39.90" />
                <LineTo x="64.60" y="39.90" />
                <CurveTo cx="67.00" cy="39.90" x="67.00" y="40.15" />
                <CurveTo cx="67.00" cy="40.35" x="65.70" y="41.15" />
                <LineTo x="64.65" y="41.80" />
                <LineTo x="64.25" y="42.05" />
                <CurveTo cx="62.95" cy="42.90" x="62.35" y="43.05" />
                <CurveTo cx="61.60" cy="43.30" x="60.00" y="43.30" />
                <LineTo x="57.85" y="43.30" />
                <LineTo x="55.70" y="43.35" />
                <LineTo x="51.35" y="43.40" />
                <CurveTo cx="50.20" cy="43.40" x="49.60" y="43.20" />
                <CurveTo cx="49.15" cy="43.05" x="48.15" y="42.40" />
                <LineTo x="48.00" y="42.30" />
                <LineTo x="47.25" y="41.90" />
                <LineTo x="46.50" y="41.45" />
                <CurveTo cx="44.45" cy="40.30" x="44.45" y="40.05" />
                <CurveTo cx="44.45" cy="39.85" x="46.10" y="39.90" />
                <LineTo x="47.35" y="39.90" />

                <MoveTo x="24.40" y="78.10" />
                <LineTo x="84.05" y="78.10" />
                <CurveTo cx="90.55" cy="78.10" x="90.55" y="84.40" />
                <LineTo x="90.55" y="151.05" />
                <CurveTo cx="90.55" cy="157.40" x="84.05" y="157.40" />
                <LineTo x="24.40" y="157.40" />
                <CurveTo cx="17.90" cy="157.40" x="17.90" y="151.05" />
                <LineTo x="17.90" y="84.40" />
                <CurveTo cx="17.90" cy="78.10" x="24.40" y="78.10" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (50, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 50
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 4" x="300" y="420">
            <Contour x="0" y="0">
                <MoveTo x="41.10" y="0.15" />
                <CurveTo cx="47.90" cy="0.00" x="53.50" y="0.00" />
                <CurveTo cx="57.00" cy="0.00" x="63.35" y="0.05" />
                <CurveTo cx="71.50" cy="0.15" x="77.35" y="0.15" />
                <LineTo x="78.25" y="0.15" />
                <CurveTo cx="82.05" cy="0.15" x="82.65" y="1.50" />
                <CurveTo cx="82.90" cy="2.15" x="85.20" y="2.15" />
                <CurveTo cx="88.00" cy="2.15" x="89.20" y="2.90" />
                <CurveTo cx="93.55" cy="5.60" x="97.60" y="10.15" />
                <CurveTo cx="102.00" cy="15.10" x="102.30" y="18.35" />
                <CurveTo cx="104.35" cy="22.05" x="106.40" y="33.10" />
                <CurveTo cx="108.30" cy="43.35" x="108.30" y="48.50" />
                <LineTo x="108.30" y="57.80" />
                <CurveTo cx="108.30" cy="73.40" x="107.50" y="92.50" />
                <CurveTo cx="107.20" cy="100.20" x="106.70" y="109.75" />
                <CurveTo cx="106.45" cy="115.50" x="105.80" y="127.00" />
                <CurveTo cx="105.10" cy="140.20" x="104.75" y="146.90" />
                <CurveTo cx="104.00" cy="161.70" x="103.30" y="168.20" />
                <CurveTo cx="102.25" cy="177.90" x="100.15" y="183.70" />
                <CurveTo cx="95.50" cy="196.30" x="80.85" y="199.90" />
                <CurveTo cx="68.15" cy="203.00" x="52.40" y="203.15" />
                <CurveTo cx="35.80" cy="203.30" x="24.10" y="199.90" />
                <CurveTo cx="11.45" cy="196.20" x="7.05" y="184.45" />
                <CurveTo cx="3.75" cy="175.50" x="2.85" y="151.75" />
                <CurveTo cx="2.55" cy="143.30" x="1.60" y="126.50" />
                <CurveTo cx="0.80" cy="111.90" x="0.50" y="104.60" />
                <CurveTo cx="0.00" cy="92.45" x="0.00" y="82.70" />
                <LineTo x="0.00" y="49.75" />
                <CurveTo cx="0.00" cy="43.80" x="2.40" y="31.35" />
                <CurveTo cx="5.00" cy="18.15" x="7.35" y="13.65" />
                <CurveTo cx="9.85" cy="8.90" x="14.45" y="6.05" />
                <CurveTo cx="17.10" cy="4.45" x="21.70" y="2.95" />
                <CurveTo cx="24.80" cy="2.00" x="25.10" y="1.35" />
                <CurveTo cx="25.60" cy="0.35" x="27.70" y="0.45" />
                <CurveTo cx="28.20" cy="0.45" x="28.40" y="0.45" />
                <LineTo x="41.10" y="0.15" />

                <MoveTo x="53.65" y="63.60" />
                <CurveTo cx="65.15" cy="63.60" x="73.35" y="58.00" />
                <CurveTo cx="81.50" cy="52.40" x="81.50" y="44.55" />
                <CurveTo cx="81.50" cy="36.70" x="73.35" y="31.10" />
                <CurveTo cx="65.15" cy="25.50" x="53.65" y="25.50" />
                <CurveTo cx="42.15" cy="25.50" x="33.95" y="31.10" />
                <CurveTo cx="25.75" cy="36.70" x="25.75" y="44.55" />
                <CurveTo cx="25.75" cy="52.40" x="33.95" y="58.00" />
                <CurveTo cx="42.15" cy="63.60" x="53.65" y="63.60" />

                <MoveTo x="46.25" y="70.15" />
                <LineTo x="61.00" y="70.15" />
                <CurveTo cx="62.55" cy="72.75" x="64.75" y="77.65" />
                <CurveTo cx="66.90" cy="82.60" x="68.45" y="85.15" />
                <CurveTo cx="58.95" cy="86.70" x="52.90" y="86.90" />
                <CurveTo cx="45.65" cy="87.15" x="38.00" y="85.80" />
                <CurveTo cx="39.50" cy="83.25" x="42.15" y="78.00" />
                <CurveTo cx="44.75" cy="72.75" x="46.25" y="70.15" />

                <MoveTo x="75.60" y="83.40" />
                <CurveTo cx="77.00" cy="82.55" x="79.90" y="80.90" />
                <CurveTo cx="87.55" cy="76.55" x="90.50" y="73.45" />
                <CurveTo cx="95.45" cy="68.25" x="95.90" y="60.35" />
                <CurveTo cx="93.55" cy="60.35" x="88.20" y="60.25" />
                <CurveTo cx="82.85" cy="60.10" x="80.50" y="60.10" />
                <CurveTo cx="79.45" cy="62.80" x="75.70" y="65.25" />
                <CurveTo cx="72.90" cy="67.10" x="67.05" y="69.65" />
                <LineTo x="75.60" y="83.40" />

                <MoveTo x="32.70" y="83.00" />
                <CurveTo cx="31.20" cy="82.05" x="28.10" y="80.35" />
                <CurveTo cx="20.70" cy="76.30" x="17.85" y="73.30" />
                <CurveTo cx="13.15" cy="68.40" x="12.70" y="60.60" />
                <LineTo x="27.25" y="59.40" />
                <CurveTo cx="28.30" cy="62.10" x="32.45" y="64.75" />
                <CurveTo cx="35.50" cy="66.70" x="41.50" y="69.30" />
                <LineTo x="32.70" y="83.00" />

                <MoveTo x="11.85" y="55.55" />
                <CurveTo cx="13.50" cy="55.20" x="15.20" y="54.95" />
                <LineTo x="18.20" y="54.45" />
                <CurveTo cx="21.50" cy="53.90" x="23.55" y="53.50" />
                <CurveTo cx="20.75" cy="48.05" x="21.20" y="42.65" />
                <CurveTo cx="21.60" cy="37.95" x="24.85" y="31.45" />
                <LineTo x="18.80" y="31.25" />
                <CurveTo cx="14.70" cy="35.60" x="13.05" y="41.70" />
                <CurveTo cx="11.45" cy="47.55" x="11.85" y="55.55" />

                <MoveTo x="95.50" y="54.55" />
                <LineTo x="84.30" y="52.80" />
                <CurveTo cx="87.00" cy="47.45" x="86.25" y="42.50" />
                <CurveTo cx="85.70" cy="38.75" x="82.20" y="31.95" />
                <LineTo x="88.20" y="31.55" />
                <CurveTo cx="92.40" cy="35.95" x="94.10" y="41.35" />
                <CurveTo cx="95.80" cy="46.70" x="95.50" y="54.55" />

                <MoveTo x="46.05" y="95.60" />
                <LineTo x="60.80" y="95.60" />
                <CurveTo cx="63.00" cy="95.60" x="64.60" y="97.45" />
                <CurveTo cx="66.20" cy="99.35" x="66.20" y="101.95" />
                <LineTo x="66.20" y="103.20" />
                <CurveTo cx="66.20" cy="105.80" x="64.60" y="107.65" />
                <CurveTo cx="63.00" cy="109.50" x="60.80" y="109.50" />
                <LineTo x="46.05" y="109.50" />
                <CurveTo cx="43.85" cy="109.50" x="42.25" y="107.65" />
                <CurveTo cx="40.65" cy="105.80" x="40.65" y="103.20" />
                <LineTo x="40.65" y="101.95" />
                <CurveTo cx="40.65" cy="99.35" x="42.25" y="97.45" />
                <CurveTo cx="43.85" cy="95.60" x="46.05" y="95.60" />

                <MoveTo x="46.05" y="116.80" />
                <LineTo x="61.35" y="116.80" />
                <CurveTo cx="63.65" cy="116.80" x="65.30" y="118.60" />
                <CurveTo cx="66.95" cy="120.40" x="66.95" y="122.95" />
                <LineTo x="66.95" y="124.15" />
                <CurveTo cx="66.95" cy="126.70" x="65.30" y="128.50" />
                <CurveTo cx="63.65" cy="130.30" x="61.35" y="130.30" />
                <LineTo x="46.05" y="130.30" />
                <CurveTo cx="43.70" cy="130.30" x="42.05" y="128.50" />
                <CurveTo cx="40.40" cy="126.70" x="40.40" y="124.15" />
                <LineTo x="40.40" y="122.95" />
                <CurveTo cx="40.40" cy="120.40" x="42.05" y="118.60" />
                <CurveTo cx="43.70" cy="116.80" x="46.05" y="116.80" />

                <MoveTo x="46.40" y="137.65" />
                <LineTo x="61.60" y="137.65" />
                <CurveTo cx="63.90" cy="137.65" x="65.55" y="139.50" />
                <CurveTo cx="67.20" cy="141.35" x="67.20" y="143.95" />
                <LineTo x="67.20" y="145.20" />
                <CurveTo cx="67.20" cy="147.75" x="65.55" y="149.60" />
                <CurveTo cx="63.90" cy="151.45" x="61.60" y="151.45" />
                <LineTo x="46.40" y="151.45" />
                <CurveTo cx="44.10" cy="151.45" x="42.45" y="149.60" />
                <CurveTo cx="40.80" cy="147.75" x="40.80" y="145.20" />
                <LineTo x="40.80" y="143.95" />
                <CurveTo cx="40.80" cy="141.35" x="42.45" y="139.50" />
                <CurveTo cx="44.10" cy="137.65" x="46.40" y="137.65" />

                <MoveTo x="46.65" y="159.55" />
                <LineTo x="61.25" y="159.55" />
                <CurveTo cx="63.45" cy="159.55" x="65.05" y="161.25" />
                <CurveTo cx="66.65" cy="162.95" x="66.65" y="165.35" />
                <LineTo x="66.65" y="166.50" />
                <CurveTo cx="66.65" cy="168.85" x="65.05" y="170.55" />
                <CurveTo cx="63.45" cy="172.25" x="61.25" y="172.25" />
                <LineTo x="46.65" y="172.25" />
                <CurveTo cx="44.45" cy="172.25" x="42.85" y="170.55" />
                <CurveTo cx="41.30" cy="168.85" x="41.30" y="166.50" />
                <LineTo x="41.30" y="165.35" />
                <CurveTo cx="41.30" cy="162.95" x="42.85" y="161.25" />
                <CurveTo cx="44.45" cy="159.55" x="46.65" y="159.55" />

                <MoveTo x="80.70" y="106.35" />
                <CurveTo cx="87.20" cy="104.80" x="89.60" y="103.75" />
                <CurveTo cx="92.20" cy="102.60" x="95.80" y="99.50" />
                <CurveTo cx="97.30" cy="98.20" x="97.75" y="95.85" />
                <CurveTo cx="98.05" cy="94.50" x="98.05" y="91.50" />
                <LineTo x="98.05" y="91.15" />
                <CurveTo cx="98.05" cy="85.00" x="90.95" y="90.00" />
                <CurveTo cx="88.45" cy="91.75" x="86.20" y="92.60" />
                <CurveTo cx="84.70" cy="93.20" x="80.75" y="94.20" />
                <LineTo x="78.05" y="94.85" />
                <LineTo x="77.95" y="94.90" />
                <CurveTo cx="76.10" cy="95.35" x="75.80" y="95.65" />
                <CurveTo cx="75.50" cy="96.00" x="75.50" y="97.50" />
                <LineTo x="75.50" y="98.55" />
                <LineTo x="75.50" y="102.05" />
                <CurveTo cx="75.55" cy="103.95" x="75.60" y="105.50" />
                <CurveTo cx="75.65" cy="107.30" x="78.25" y="106.70" />
                <CurveTo cx="79.80" cy="106.35" x="80.70" y="106.35" />

                <MoveTo x="30.35" y="107.90" />
                <CurveTo cx="18.95" cy="104.95" x="12.25" y="98.40" />
                <CurveTo cx="11.15" cy="97.30" x="10.90" y="96.65" />
                <CurveTo cx="10.70" cy="96.10" x="10.70" y="94.45" />
                <LineTo x="10.70" y="93.90" />
                <LineTo x="10.70" y="93.20" />
                <LineTo x="10.65" y="92.80" />
                <CurveTo cx="10.60" cy="92.45" x="10.70" y="91.55" />
                <CurveTo cx="10.90" cy="89.15" x="12.15" y="88.60" />
                <CurveTo cx="13.15" cy="88.20" x="14.95" y="88.85" />
                <CurveTo cx="16.10" cy="89.30" x="18.35" y="90.30" />
                <CurveTo cx="21.40" cy="91.65" x="23.00" y="92.15" />
                <CurveTo cx="24.15" cy="92.55" x="26.75" y="93.20" />
                <CurveTo cx="29.00" cy="93.80" x="30.25" y="94.20" />
                <CurveTo cx="31.95" cy="94.70" x="32.25" y="95.05" />
                <CurveTo cx="32.55" cy="95.35" x="32.55" y="96.90" />
                <LineTo x="32.55" y="98.00" />
                <LineTo x="32.55" y="101.60" />
                <LineTo x="32.55" y="103.35" />
                <LineTo x="32.50" y="105.15" />
                <LineTo x="32.50" y="106.05" />
                <CurveTo cx="32.45" cy="107.90" x="30.35" y="107.90" />

                <MoveTo x="78.75" y="128.35" />
                <CurveTo cx="84.25" cy="127.00" x="87.15" y="125.90" />
                <CurveTo cx="90.75" cy="124.55" x="93.45" y="122.60" />
                <CurveTo cx="95.15" cy="121.40" x="95.65" y="120.80" />
                <CurveTo cx="96.40" cy="119.95" x="96.80" y="118.30" />
                <CurveTo cx="97.15" cy="116.85" x="97.45" y="115.15" />
                <CurveTo cx="97.85" cy="112.80" x="97.90" y="111.20" />
                <CurveTo cx="97.95" cy="108.70" x="96.50" y="108.65" />
                <CurveTo cx="95.60" cy="108.60" x="93.35" y="109.75" />
                <LineTo x="92.45" y="110.20" />
                <CurveTo cx="91.60" cy="110.60" x="90.00" y="111.50" />
                <CurveTo cx="87.90" cy="112.70" x="86.65" y="113.25" />
                <CurveTo cx="85.15" cy="113.90" x="81.25" y="114.95" />
                <LineTo x="78.40" y="115.70" />
                <CurveTo cx="76.55" cy="116.20" x="76.20" y="116.55" />
                <CurveTo cx="75.90" cy="116.90" x="75.95" y="118.50" />
                <LineTo x="75.95" y="119.55" />
                <LineTo x="75.95" y="123.15" />
                <LineTo x="75.95" y="124.95" />
                <LineTo x="76.00" y="126.75" />
                <CurveTo cx="76.05" cy="128.70" x="77.05" y="128.50" />
                <CurveTo cx="77.95" cy="128.35" x="78.75" y="128.35" />

                <MoveTo x="76.20" y="170.70" />
                <CurveTo cx="87.75" cy="167.75" x="94.40" y="161.65" />
                <CurveTo cx="95.50" cy="160.65" x="95.75" y="160.00" />
                <CurveTo cx="95.95" cy="159.45" x="95.95" y="157.95" />
                <LineTo x="95.95" y="157.40" />
                <CurveTo cx="95.95" cy="156.90" x="96.00" y="155.90" />
                <CurveTo cx="96.10" cy="153.85" x="95.95" y="152.85" />
                <CurveTo cx="95.80" cy="151.90" x="94.40" y="151.85" />
                <CurveTo cx="93.30" cy="151.80" x="91.30" y="152.35" />
                <CurveTo cx="89.10" cy="152.95" x="85.35" y="154.75" />
                <LineTo x="83.90" y="155.40" />
                <CurveTo cx="82.55" cy="156.00" x="79.25" y="156.85" />
                <LineTo x="77.85" y="157.20" />
                <LineTo x="76.25" y="157.65" />
                <CurveTo cx="74.50" cy="158.10" x="74.25" y="158.45" />
                <CurveTo cx="73.95" cy="158.75" x="73.95" y="160.25" />
                <LineTo x="73.95" y="161.30" />
                <LineTo x="73.95" y="164.70" />
                <CurveTo cx="74.00" cy="166.55" x="74.05" y="168.05" />
                <LineTo x="74.05" y="168.90" />
                <CurveTo cx="74.05" cy="170.70" x="76.20" y="170.70" />

                <MoveTo x="30.05" y="170.15" />
                <CurveTo cx="24.90" cy="168.75" x="21.15" y="166.70" />
                <CurveTo cx="17.65" cy="164.80" x="14.40" y="161.80" />
                <CurveTo cx="13.30" cy="160.80" x="13.05" y="160.10" />
                <CurveTo cx="12.85" cy="159.60" x="12.85" y="158.10" />
                <LineTo x="12.85" y="157.55" />
                <LineTo x="12.85" y="156.85" />
                <LineTo x="12.80" y="156.45" />
                <LineTo x="12.80" y="156.10" />
                <LineTo x="12.85" y="155.25" />
                <CurveTo cx="13.00" cy="152.90" x="14.15" y="152.35" />
                <CurveTo cx="15.05" cy="151.90" x="16.80" y="152.55" />
                <LineTo x="18.10" y="153.10" />
                <LineTo x="19.45" y="153.60" />
                <CurveTo cx="22.95" cy="155.10" x="24.80" y="155.65" />
                <CurveTo cx="26.05" cy="156.05" x="28.95" y="156.80" />
                <LineTo x="31.15" y="157.35" />
                <CurveTo cx="33.05" cy="157.95" x="33.40" y="158.55" />
                <CurveTo cx="33.90" cy="159.35" x="34.25" y="162.00" />
                <CurveTo cx="34.55" cy="164.20" x="34.70" y="166.85" />
                <CurveTo cx="34.90" cy="171.45" x="30.05" y="170.15" />

                <MoveTo x="77.15" y="149.30" />
                <CurveTo cx="89.05" cy="146.35" x="95.85" y="140.15" />
                <CurveTo cx="97.00" cy="139.10" x="97.25" y="138.40" />
                <CurveTo cx="97.45" cy="137.90" x="97.45" y="136.30" />
                <LineTo x="97.45" y="135.80" />
                <LineTo x="97.45" y="135.05" />
                <LineTo x="97.50" y="134.55" />
                <CurveTo cx="97.55" cy="133.65" x="97.45" y="132.75" />
                <CurveTo cx="97.00" cy="129.55" x="93.85" y="130.65" />
                <CurveTo cx="92.55" cy="131.10" x="90.00" y="132.15" />
                <CurveTo cx="87.60" cy="133.20" x="86.40" y="133.60" />
                <CurveTo cx="84.70" cy="134.20" x="80.75" y="135.15" />
                <LineTo x="79.05" y="135.60" />
                <LineTo x="77.25" y="136.05" />
                <LineTo x="77.15" y="136.10" />
                <CurveTo cx="75.40" cy="136.55" x="75.15" y="136.90" />
                <CurveTo cx="74.85" cy="137.20" x="74.90" y="138.70" />
                <LineTo x="74.90" y="139.75" />
                <CurveTo cx="74.85" cy="141.30" x="74.90" y="143.20" />
                <LineTo x="74.95" y="146.65" />
                <LineTo x="74.95" y="147.60" />
                <CurveTo cx="75.00" cy="149.30" x="77.15" y="149.30" />

                <MoveTo x="30.80" y="149.80" />
                <CurveTo cx="19.60" cy="147.00" x="13.00" y="140.85" />
                <CurveTo cx="11.45" cy="139.40" x="11.45" y="137.15" />
                <LineTo x="11.45" y="136.60" />
                <LineTo x="11.45" y="135.55" />
                <LineTo x="11.45" y="135.15" />
                <LineTo x="11.45" y="134.30" />
                <CurveTo cx="11.35" cy="131.15" x="13.00" y="131.00" />
                <CurveTo cx="14.05" cy="130.95" x="16.75" y="132.25" />
                <LineTo x="16.90" y="132.35" />
                <CurveTo cx="20.80" cy="134.30" x="24.05" y="135.45" />
                <CurveTo cx="25.70" cy="136.00" x="28.60" y="136.85" />
                <LineTo x="29.35" y="137.05" />
                <CurveTo cx="31.20" cy="137.55" x="31.55" y="138.20" />
                <CurveTo cx="32.25" cy="139.45" x="32.70" y="143.65" />
                <CurveTo cx="33.15" cy="147.70" x="32.85" y="149.15" />
                <CurveTo cx="32.75" cy="149.80" x="30.80" y="149.80" />

                <MoveTo x="30.35" y="128.35" />
                <CurveTo cx="19.75" cy="125.60" x="13.60" y="119.60" />
                <CurveTo cx="12.55" cy="118.60" x="12.30" y="117.95" />
                <CurveTo cx="12.15" cy="117.45" x="12.15" y="116.00" />
                <LineTo x="12.15" y="115.45" />
                <LineTo x="12.15" y="114.40" />
                <LineTo x="12.15" y="114.05" />
                <LineTo x="12.15" y="113.20" />
                <CurveTo cx="12.20" cy="110.95" x="13.05" y="110.35" />
                <CurveTo cx="13.70" cy="109.85" x="15.05" y="110.30" />
                <CurveTo cx="16.50" cy="110.80" x="19.30" y="112.05" />
                <CurveTo cx="23.00" cy="113.75" x="24.95" y="114.30" />
                <LineTo x="27.60" y="115.00" />
                <LineTo x="30.25" y="115.70" />
                <CurveTo cx="31.85" cy="116.10" x="32.15" y="116.45" />
                <CurveTo cx="32.40" cy="116.75" x="32.40" y="118.20" />
                <LineTo x="32.40" y="119.25" />
                <LineTo x="32.40" y="122.50" />
                <LineTo x="32.40" y="124.15" />
                <LineTo x="32.35" y="125.80" />
                <LineTo x="32.35" y="126.70" />
                <CurveTo cx="32.30" cy="128.35" x="30.35" y="128.35" />

                <MoveTo x="20.30" y="182.70" />
                <CurveTo cx="22.70" cy="184.25" x="23.80" y="186.20" />
                <CurveTo cx="24.85" cy="188.20" x="23.95" y="189.50" />
                <CurveTo cx="23.05" cy="190.75" x="20.75" y="190.60" />
                <CurveTo cx="18.40" cy="190.40" x="16.00" y="188.90" />
                <CurveTo cx="13.60" cy="187.35" x="12.55" y="185.35" />
                <CurveTo cx="11.45" cy="183.35" x="12.35" y="182.10" />
                <CurveTo cx="13.25" cy="180.80" x="15.60" y="181.00" />
                <CurveTo cx="17.90" cy="181.15" x="20.30" y="182.70" />

                <MoveTo x="88.45" y="183.15" />
                <CurveTo cx="86.35" cy="184.45" x="85.40" y="186.10" />
                <CurveTo cx="84.45" cy="187.75" x="85.25" y="188.80" />
                <CurveTo cx="86.05" cy="189.85" x="88.10" y="189.70" />
                <CurveTo cx="90.15" cy="189.55" x="92.25" y="188.30" />
                <CurveTo cx="94.40" cy="187.00" x="95.35" y="185.40" />
                <CurveTo cx="96.25" cy="183.75" x="95.45" y="182.70" />
                <CurveTo cx="94.65" cy="181.60" x="92.60" y="181.75" />
                <CurveTo cx="90.55" cy="181.90" x="88.45" y="183.15" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (51, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 51
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="50.20" y="0.95" />
                <LineTo x="76.35" y="0.95" />
                <CurveTo cx="100.20" cy="0.00" x="112.65" y="1.90" />
                <CurveTo cx="127.20" cy="4.15" x="127.50" y="10.30" />
                <LineTo x="124.35" y="173.30" />
                <CurveTo cx="124.75" cy="197.65" x="110.95" y="208.10" />
                <CurveTo cx="100.45" cy="216.05" x="76.35" y="218.40" />
                <CurveTo cx="62.10" cy="219.80" x="50.20" y="218.40" />
                <CurveTo cx="26.85" cy="215.70" x="16.00" y="206.90" />
                <CurveTo cx="2.65" cy="196.10" x="2.15" y="172.65" />
                <LineTo x="1.35" y="8.95" />
                <CurveTo cx="0.00" cy="3.30" x="15.00" y="1.55" />
                <CurveTo cx="21.30" cy="0.80" x="31.00" y="0.70" />
                <CurveTo cx="38.75" cy="0.65" x="50.20" y="0.95" />

                <MoveTo x="29.60" y="27.65" />
                <CurveTo cx="46.05" cy="23.90" x="63.70" y="23.95" />
                <CurveTo cx="80.50" cy="23.95" x="99.60" y="27.40" />
                <CurveTo cx="102.45" cy="27.95" x="105.20" y="31.10" />
                <CurveTo cx="107.80" cy="34.05" x="108.30" y="36.85" />
                <CurveTo cx="112.15" cy="56.85" x="112.15" y="75.40" />
                <CurveTo cx="112.15" cy="95.65" x="107.45" y="114.35" />
                <CurveTo cx="106.80" cy="116.80" x="104.15" y="120.00" />
                <CurveTo cx="101.35" cy="123.45" x="98.75" y="124.45" />
                <CurveTo cx="64.60" cy="137.45" x="31.35" y="124.25" />
                <CurveTo cx="28.60" cy="123.15" x="25.55" y="119.85" />
                <CurveTo cx="22.50" cy="116.50" x="22.00" y="114.15" />
                <CurveTo cx="17.65" cy="93.75" x="17.25" y="75.65" />
                <CurveTo cx="16.75" cy="56.30" x="20.70" y="36.85" />
                <CurveTo cx="21.25" cy="34.15" x="23.95" y="31.25" />
                <CurveTo cx="26.75" cy="28.25" x="29.60" y="27.65" />

                <MoveTo x="39.20" y="41.85" />
                <CurveTo cx="51.85" cy="41.15" x="65.50" y="41.20" />
                <CurveTo cx="78.00" cy="41.20" x="91.75" y="41.85" />
                <CurveTo cx="97.80" cy="42.25" x="99.60" y="49.45" />
                <CurveTo cx="100.30" cy="60.00" x="99.80" y="72.60" />
                <CurveTo cx="99.35" cy="83.40" x="98.05" y="95.70" />
                <CurveTo cx="97.00" cy="98.35" x="94.05" y="100.75" />
                <CurveTo cx="92.40" cy="102.05" x="89.80" y="103.50" />
                <LineTo x="89.30" y="103.80" />
                <CurveTo cx="75.65" cy="107.65" x="64.80" y="107.50" />
                <CurveTo cx="54.75" cy="107.35" x="41.75" y="103.55" />
                <CurveTo cx="39.25" cy="102.55" x="36.90" y="100.75" />
                <CurveTo cx="33.75" cy="98.30" x="32.50" y="95.45" />
                <CurveTo cx="31.10" cy="83.20" x="30.80" y="72.25" />
                <CurveTo cx="30.40" cy="59.85" x="31.35" y="49.45" />
                <CurveTo cx="32.95" cy="43.05" x="39.20" y="41.85" />

                <MoveTo x="43.35" y="48.40" />
                <LineTo x="86.95" y="48.40" />
                <CurveTo cx="93.30" cy="48.40" x="93.65" y="54.55" />
                <CurveTo cx="94.45" cy="68.25" x="93.65" y="85.65" />
                <CurveTo cx="93.45" cy="90.30" x="86.95" y="91.75" />
                <CurveTo cx="76.70" cy="94.10" x="66.40" y="94.20" />
                <CurveTo cx="55.10" cy="94.30" x="43.35" y="91.75" />
                <CurveTo cx="37.00" cy="90.45" x="36.65" y="85.65" />
                <CurveTo cx="36.05" cy="77.10" x="36.05" y="70.10" />
                <CurveTo cx="36.00" cy="61.85" x="36.65" y="54.55" />
                <CurveTo cx="37.20" cy="48.40" x="43.35" y="48.40" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (52, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 52
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="120.40" y="0.00" />
                <LineTo x="119.60" y="152.60" />
                <CurveTo cx="119.55" cy="163.80" x="111.05" y="171.55" />
                <CurveTo cx="103.35" cy="178.55" x="90.95" y="181.10" />
                <CurveTo cx="58.35" cy="187.85" x="29.45" y="181.10" />
                <CurveTo cx="16.95" cy="178.20" x="9.40" y="171.40" />
                <CurveTo cx="0.85" cy="163.70" x="0.80" y="152.60" />
                <LineTo x="0.00" y="0.00" />
                <LineTo x="120.40" y="0.00" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (53, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 53
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 3" x="50" y="420">
            <Contour x="0" y="0">
                <MoveTo x="43.05" y="0.95" />
                <LineTo x="44.25" y="0.90" />
                <CurveTo cx="55.45" cy="0.25" x="60.90" y="0.20" />
                <CurveTo cx="69.95" cy="0.00" x="77.60" y="0.70" />
                <CurveTo cx="81.90" cy="0.30" x="87.85" y="1.55" />
                <CurveTo cx="95.05" cy="3.05" x="101.05" y="6.45" />
                <CurveTo cx="117.25" cy="15.70" x="117.75" y="34.60" />
                <LineTo x="122.80" y="230.60" />
                <LineTo x="59.95" y="230.70" />
                <LineTo x="59.95" y="211.20" />
                <LineTo x="0.00" y="211.20" />
                <LineTo x="3.00" y="33.65" />
                <CurveTo cx="3.30" cy="15.90" x="18.40" y="7.05" />
                <CurveTo cx="29.90" cy="0.25" x="43.05" y="0.95" />

                <MoveTo x="22.00" y="58.95" />
                <LineTo x="99.25" y="58.95" />
                <CurveTo cx="102.85" cy="58.95" x="102.85" y="62.80" />
                <LineTo x="102.85" y="166.30" />
                <CurveTo cx="102.85" cy="170.20" x="99.25" y="170.20" />
                <LineTo x="22.00" y="170.20" />
                <CurveTo cx="18.35" cy="170.20" x="18.35" y="166.30" />
                <LineTo x="18.35" y="62.80" />
                <CurveTo cx="18.35" cy="58.95" x="22.00" y="58.95" />

                <MoveTo x="51.10" y="18.10" />
                <LineTo x="58.80" y="18.10" />
                <LineTo x="58.80" y="21.05" />
                <LineTo x="46.70" y="21.05" />
                <CurveTo cx="46.55" cy="19.80" x="47.70" y="19.00" />
                <CurveTo cx="48.85" cy="18.15" x="51.10" y="18.10" />

                <MoveTo x="69.10" y="27.90" />
                <LineTo x="61.40" y="27.90" />
                <LineTo x="61.40" y="24.90" />
                <LineTo x="73.50" y="24.90" />
                <CurveTo cx="73.65" cy="26.15" x="72.50" y="26.95" />
                <CurveTo cx="71.30" cy="27.85" x="69.10" y="27.90" />

                <MoveTo x="51.50" y="27.85" />
                <LineTo x="59.20" y="27.85" />
                <LineTo x="59.20" y="24.90" />
                <LineTo x="47.10" y="24.90" />
                <CurveTo cx="46.95" cy="26.15" x="48.10" y="26.95" />
                <CurveTo cx="49.25" cy="27.80" x="51.50" y="27.85" />

                <MoveTo x="68.60" y="18.15" />
                <LineTo x="60.90" y="18.15" />
                <LineTo x="60.90" y="21.15" />
                <LineTo x="73.00" y="21.15" />
                <CurveTo cx="73.15" cy="19.90" x="72.00" y="19.10" />
                <CurveTo cx="70.80" cy="18.20" x="68.60" y="18.15" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (54, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 54
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 4" x="300" y="420">
            <Contour x="0" y="0">
                <MoveTo x="128.05" y="0.00" />
                <LineTo x="130.95" y="12.00" />
                <LineTo x="133.90" y="14.95" />
                <LineTo x="132.70" y="39.90" />
                <LineTo x="128.40" y="42.10" />
                <LineTo x="126.60" y="169.75" />
                <CurveTo cx="126.25" cy="191.45" x="114.50" y="203.25" />
                <CurveTo cx="103.25" cy="214.50" x="84.10" y="214.50" />
                <LineTo x="47.90" y="214.50" />
                <CurveTo cx="29.20" cy="214.50" x="17.70" y="202.70" />
                <CurveTo cx="5.85" cy="190.50" x="5.65" y="169.75" />
                <LineTo x="4.65" y="42.25" />
                <LineTo x="0.80" y="39.65" />
                <LineTo x="0.00" y="14.60" />
                <LineTo x="4.10" y="10.60" />
                <LineTo x="6.95" y="0.15" />
                <LineTo x="128.05" y="0.00" />

                <MoveTo x="99.30" y="194.65" />
                <LineTo x="106.60" y="192.10" />
                <CurveTo cx="107.80" cy="191.65" x="108.15" y="192.95" />
                <LineTo x="108.30" y="193.60" />
                <CurveTo cx="108.75" cy="194.80" x="107.50" y="195.30" />
                <LineTo x="100.25" y="197.90" />
                <CurveTo cx="99.05" cy="198.25" x="98.70" y="197.00" />
                <LineTo x="98.50" y="196.40" />
                <CurveTo cx="98.15" cy="195.10" x="99.30" y="194.65" />

                <MoveTo x="9.70" y="77.35" />
                <CurveTo cx="12.60" cy="78.30" x="18.60" y="79.90" />
                <CurveTo cx="25.60" cy="81.75" x="28.90" y="82.90" />
                <CurveTo cx="34.35" cy="84.85" x="38.05" y="87.30" />
                <CurveTo cx="44.15" cy="91.25" x="43.85" y="104.60" />
                <LineTo x="47.85" y="104.60" />
                <CurveTo cx="47.65" cy="101.70" x="49.65" y="97.80" />
                <CurveTo cx="52.00" cy="93.10" x="55.75" y="91.40" />
                <CurveTo cx="67.40" cy="93.00" x="78.00" y="91.70" />
                <CurveTo cx="80.65" cy="93.35" x="83.30" y="98.35" />
                <CurveTo cx="85.85" cy="103.15" x="85.35" y="105.05" />
                <LineTo x="88.90" y="104.95" />
                <CurveTo cx="89.65" cy="91.00" x="95.95" y="86.75" />
                <CurveTo cx="99.95" cy="84.10" x="113.15" y="81.00" />
                <CurveTo cx="119.65" cy="79.45" x="123.00" y="78.50" />
                <LineTo x="119.05" y="181.50" />
                <CurveTo cx="90.00" cy="192.00" x="64.25" y="191.90" />
                <CurveTo cx="37.85" cy="191.80" x="12.10" y="180.60" />
                <LineTo x="9.70" y="77.35" />

                <MoveTo x="81.45" y="30.85" />
                <CurveTo cx="86.45" cy="38.45" x="91.95" y="42.10" />
                <CurveTo cx="96.95" cy="45.45" x="104.55" y="46.85" />
                <CurveTo cx="108.95" cy="58.10" x="104.80" y="69.60" />
                <CurveTo cx="96.95" cy="71.65" x="91.90" y="74.70" />
                <CurveTo cx="85.65" cy="78.45" x="81.80" y="84.70" />
                <CurveTo cx="67.30" cy="89.95" x="52.10" y="84.55" />
                <CurveTo cx="47.05" cy="77.90" x="42.95" y="74.80" />
                <CurveTo cx="37.15" cy="70.45" x="30.15" y="70.20" />
                <CurveTo cx="24.05" cy="58.35" x="29.90" y="45.95" />
                <LineTo x="31.95" y="45.50" />
                <CurveTo cx="38.15" cy="44.15" x="41.30" y="42.20" />
                <CurveTo cx="46.55" cy="38.95" x="51.40" y="31.10" />
                <CurveTo cx="57.95" cy="28.95" x="66.65" y="28.80" />
                <CurveTo cx="75.60" cy="28.70" x="81.45" y="30.85" />

                <MoveTo x="10.45" y="18.85" />
                <LineTo x="10.45" y="39.05" />
                <CurveTo cx="22.85" cy="33.15" x="32.65" y="30.25" />
                <CurveTo cx="39.90" cy="27.35" x="41.80" y="21.90" />
                <LineTo x="42.35" y="20.35" />
                <CurveTo cx="44.05" cy="15.60" x="44.50" y="12.35" />
                <CurveTo cx="45.30" cy="7.20" x="37.95" y="9.30" />
                <CurveTo cx="30.50" cy="10.50" x="20.75" y="14.05" />
                <CurveTo cx="11.60" cy="17.35" x="10.45" y="18.85" />

                <MoveTo x="123.25" y="19.55" />
                <LineTo x="123.25" y="39.05" />
                <CurveTo cx="111.05" cy="33.30" x="101.65" y="30.55" />
                <CurveTo cx="94.60" cy="27.75" x="92.75" y="22.50" />
                <LineTo x="92.25" y="21.10" />
                <CurveTo cx="90.55" cy="16.50" x="90.10" y="13.30" />
                <CurveTo cx="89.40" cy="8.30" x="96.45" y="10.35" />
                <CurveTo cx="103.75" cy="11.50" x="113.25" y="14.90" />
                <CurveTo cx="122.10" cy="18.10" x="123.25" y="19.55" />

                <MoveTo x="56.85" y="8.00" />
                <LineTo x="77.25" y="8.00" />
                <CurveTo cx="85.55" cy="8.00" x="85.10" y="12.85" />
                <CurveTo cx="84.75" cy="16.15" x="81.55" y="19.65" />
                <CurveTo cx="78.40" cy="23.05" x="75.70" y="23.35" />
                <CurveTo cx="71.45" cy="23.00" x="67.35" y="22.95" />
                <CurveTo cx="62.75" cy="22.90" x="58.95" y="23.25" />
                <CurveTo cx="52.40" cy="20.55" x="50.65" y="14.15" />
                <CurveTo cx="48.75" cy="7.55" x="56.85" y="8.00" />

                <MoveTo x="95.90" y="210.45" />
                <CurveTo cx="97.60" cy="210.45" x="98.85" y="209.20" />
                <CurveTo cx="100.05" cy="207.95" x="100.05" y="206.20" />
                <CurveTo cx="100.05" cy="204.45" x="98.85" y="203.20" />
                <CurveTo cx="97.60" cy="201.95" x="95.90" y="201.95" />
                <CurveTo cx="94.15" cy="201.95" x="92.90" y="203.20" />
                <CurveTo cx="91.70" cy="204.45" x="91.70" y="206.20" />
                <CurveTo cx="91.70" cy="207.95" x="92.90" y="209.20" />
                <CurveTo cx="94.15" cy="210.45" x="95.90" y="210.45" />

                <MoveTo x="35.50" y="210.45" />
                <CurveTo cx="37.35" cy="210.45" x="38.65" y="209.20" />
                <CurveTo cx="39.95" cy="207.95" x="39.95" y="206.20" />
                <CurveTo cx="39.95" cy="204.45" x="38.65" y="203.25" />
                <CurveTo cx="37.35" cy="202.00" x="35.50" y="202.00" />
                <CurveTo cx="33.70" cy="202.00" x="32.40" y="203.25" />
                <CurveTo cx="31.10" cy="204.50" x="31.10" y="206.20" />
                <CurveTo cx="31.10" cy="207.95" x="32.40" y="209.20" />
                <CurveTo cx="33.70" cy="210.45" x="35.50" y="210.45" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (55, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 55
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="33.95" y="0.90" />
                <LineTo x="105.50" y="0.20" />
                <CurveTo cx="121.55" cy="0.00" x="131.25" y="8.20" />
                <CurveTo cx="140.15" cy="15.65" x="145.40" y="31.75" />
                <CurveTo cx="147.20" cy="37.30" x="147.95" y="43.10" />
                <CurveTo cx="148.50" cy="47.75" x="148.60" y="54.60" />
                <CurveTo cx="148.80" cy="73.75" x="148.65" y="95.65" />
                <LineTo x="148.65" y="97.45" />
                <CurveTo cx="148.60" cy="103.40" x="148.50" y="106.40" />
                <CurveTo cx="148.40" cy="111.40" x="148.10" y="115.40" />
                <CurveTo cx="147.95" cy="117.25" x="147.70" y="121.45" />
                <CurveTo cx="147.50" cy="125.95" x="147.30" y="128.25" />
                <CurveTo cx="146.60" cy="137.65" x="145.05" y="146.55" />
                <CurveTo cx="143.25" cy="156.80" x="140.60" y="164.35" />
                <CurveTo cx="134.45" cy="181.90" x="122.85" y="194.50" />
                <CurveTo cx="108.85" cy="209.75" x="92.45" y="210.55" />
                <CurveTo cx="87.85" cy="210.80" x="78.60" y="210.60" />
                <CurveTo cx="70.60" cy="210.45" x="66.60" y="210.60" />
                <CurveTo cx="43.50" cy="211.35" x="29.80" y="198.15" />
                <CurveTo cx="19.20" cy="188.00" x="10.80" y="164.95" />
                <CurveTo cx="7.50" cy="155.85" x="5.70" y="146.50" />
                <CurveTo cx="4.15" cy="138.25" x="3.45" y="127.65" />
                <LineTo x="3.05" y="121.40" />
                <LineTo x="2.60" y="115.15" />
                <LineTo x="2.30" y="110.30" />
                <LineTo x="1.95" y="105.40" />
                <LineTo x="1.30" y="95.65" />
                <CurveTo cx="0.60" cy="85.40" x="0.35" y="77.90" />
                <CurveTo cx="0.00" cy="68.30" x="0.15" y="60.10" />
                <CurveTo cx="0.50" cy="45.25" x="3.70" y="30.30" />
                <CurveTo cx="6.20" cy="18.55" x="14.25" y="10.15" />
                <CurveTo cx="22.95" cy="1.00" x="33.95" y="0.90" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (56, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 56
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="26.55" y="0.65" />
                <LineTo x="93.40" y="0.10" />
                <CurveTo cx="108.00" cy="0.00" x="111.60" y="7.95" />
                <CurveTo cx="114.15" cy="13.75" x="114.15" y="37.85" />
                <LineTo x="114.15" y="41.15" />
                <LineTo x="114.15" y="124.25" />
                <CurveTo cx="114.15" cy="142.50" x="107.60" y="155.90" />
                <CurveTo cx="101.35" cy="168.65" x="90.25" y="175.05" />
                <CurveTo cx="77.40" cy="182.45" x="57.50" y="182.40" />
                <CurveTo cx="37.75" cy="182.30" x="25.40" y="175.00" />
                <CurveTo cx="14.65" cy="168.60" x="7.35" y="151.80" />
                <CurveTo cx="0.20" cy="135.25" x="0.20" y="117.55" />
                <LineTo x="0.20" y="41.15" />
                <CurveTo cx="0.20" cy="39.05" x="0.15" y="35.10" />
                <CurveTo cx="0.00" cy="15.25" x="3.25" y="9.30" />
                <CurveTo cx="8.35" cy="0.10" x="26.55" y="0.65" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (57, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 57
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 3" x="50" y="420">
            <Contour x="0" y="0">
                <MoveTo x="59.00" y="0.00" />
                <LineTo x="66.75" y="0.00" />
                <CurveTo cx="79.60" cy="0.00" x="88.45" y="3.05" />
                <CurveTo cx="99.05" cy="6.65" x="106.30" y="15.20" />
                <CurveTo cx="113.75" cy="23.95" x="118.55" y="39.15" />
                <CurveTo cx="122.75" cy="52.40" x="123.40" y="64.70" />
                <LineTo x="123.90" y="74.45" />
                <CurveTo cx="124.60" cy="89.45" x="124.95" y="96.90" />
                <CurveTo cx="125.45" cy="109.35" x="125.65" y="119.30" />
                <CurveTo cx="126.15" cy="144.20" x="124.85" y="164.15" />
                <CurveTo cx="124.85" cy="184.00" x="108.35" y="190.55" />
                <LineTo x="107.05" y="191.10" />
                <CurveTo cx="104.70" cy="192.05" x="101.85" y="192.60" />
                <LineTo x="101.75" y="225.00" />
                <LineTo x="26.10" y="224.70" />
                <LineTo x="25.95" y="192.65" />
                <CurveTo cx="23.45" cy="192.20" x="21.05" y="191.35" />
                <LineTo x="19.95" y="190.95" />
                <CurveTo cx="10.90" cy="187.75" x="6.30" y="181.70" />
                <CurveTo cx="1.15" cy="174.90" x="1.15" y="164.10" />
                <CurveTo cx="0.00" cy="147.30" x="0.65" y="126.25" />
                <CurveTo cx="0.90" cy="117.80" x="1.45" y="107.30" />
                <CurveTo cx="1.80" cy="100.95" x="2.55" y="88.30" />
                <CurveTo cx="3.55" cy="71.95" x="3.95" y="63.80" />
                <CurveTo cx="4.55" cy="50.85" x="7.40" y="39.75" />
                <CurveTo cx="11.10" cy="25.50" x="17.90" y="17.20" />
                <CurveTo cx="25.25" cy="8.15" x="36.35" y="3.85" />
                <CurveTo cx="46.25" cy="0.00" x="59.00" y="0.00" />

                <MoveTo x="29.45" y="59.60" />
                <LineTo x="96.95" y="59.60" />
                <CurveTo cx="103.00" cy="59.60" x="103.00" y="65.90" />
                <LineTo x="103.00" y="156.15" />
                <CurveTo cx="103.00" cy="162.50" x="96.95" y="162.50" />
                <LineTo x="29.45" y="162.50" />
                <CurveTo cx="23.40" cy="162.50" x="23.40" y="156.15" />
                <LineTo x="23.40" y="65.90" />
                <CurveTo cx="23.40" cy="59.60" x="29.45" y="59.60" />

                <MoveTo x="62.35" y="36.25" />
                <CurveTo cx="65.90" cy="36.25" x="68.45" y="35.55" />
                <CurveTo cx="71.00" cy="34.85" x="71.00" y="33.90" />
                <CurveTo cx="71.00" cy="32.95" x="68.45" y="32.30" />
                <CurveTo cx="65.90" cy="31.60" x="62.35" y="31.60" />
                <CurveTo cx="58.75" cy="31.60" x="56.25" y="32.30" />
                <CurveTo cx="53.70" cy="32.95" x="53.70" y="33.90" />
                <CurveTo cx="53.70" cy="34.90" x="56.25" y="35.55" />
                <CurveTo cx="58.75" cy="36.25" x="62.35" y="36.25" />

                <MoveTo x="63.95" y="23.65" />
                <CurveTo cx="67.10" cy="23.45" x="69.75" y="24.85" />
                <CurveTo cx="72.35" cy="26.25" x="75.05" y="29.65" />
                <CurveTo cx="71.55" cy="30.05" x="69.50" y="30.15" />
                <CurveTo cx="66.60" cy="30.25" x="63.95" y="29.85" />
                <LineTo x="63.95" y="23.65" />

                <MoveTo x="62.05" y="23.65" />
                <CurveTo cx="58.85" cy="23.45" x="56.20" y="24.90" />
                <CurveTo cx="53.55" cy="26.35" x="50.85" y="29.75" />
                <CurveTo cx="52.60" cy="29.95" x="53.70" y="30.00" />
                <CurveTo cx="55.20" cy="30.15" x="56.45" y="30.20" />
                <CurveTo cx="59.40" cy="30.25" x="62.05" y="29.90" />
                <LineTo x="62.05" y="23.65" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (58, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 58
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 4" x="300" y="420">
            <Contour x="0" y="0">
                <MoveTo x="26.30" y="0.30" />
                <LineTo x="98.05" y="0.00" />
                <LineTo x="98.50" y="11.60" />
                <CurveTo cx="102.90" cy="10.65" x="112.15" y="19.70" />
                <CurveTo cx="123.20" cy="30.50" x="123.65" y="44.70" />
                <CurveTo cx="124.10" cy="49.60" x="123.65" y="65.25" />
                <CurveTo cx="123.30" cy="77.75" x="122.35" y="96.55" />
                <CurveTo cx="121.70" cy="109.95" x="120.85" y="125.15" />
                <LineTo x="120.30" y="134.30" />
                <LineTo x="120.10" y="137.70" />
                <LineTo x="120.05" y="138.30" />
                <LineTo x="119.95" y="139.95" />
                <CurveTo cx="119.75" cy="142.85" x="119.40" y="146.05" />
                <CurveTo cx="118.35" cy="156.20" x="116.50" y="165.25" />
                <CurveTo cx="113.90" cy="177.95" x="110.10" y="186.60" />
                <CurveTo cx="105.35" cy="197.45" x="98.95" y="201.40" />
                <CurveTo cx="84.85" cy="210.15" x="59.60" y="209.15" />
                <CurveTo cx="34.05" cy="208.15" x="21.45" y="197.85" />
                <CurveTo cx="15.60" cy="193.05" x="11.45" y="182.55" />
                <CurveTo cx="8.15" cy="174.10" x="6.10" y="162.40" />
                <CurveTo cx="4.65" cy="154.05" x="3.95" y="144.90" />
                <CurveTo cx="3.75" cy="142.05" x="3.60" y="139.45" />
                <LineTo x="3.55" y="137.40" />
                <LineTo x="3.35" y="133.80" />
                <LineTo x="2.85" y="124.25" />
                <CurveTo cx="2.05" cy="108.40" x="1.45" y="94.65" />
                <CurveTo cx="0.60" cy="75.45" x="0.35" y="63.25" />
                <CurveTo cx="0.00" cy="48.00" x="0.60" y="44.70" />
                <CurveTo cx="0.65" cy="34.90" x="9.15" y="24.10" />
                <CurveTo cx="17.50" cy="13.55" x="25.95" y="11.80" />
                <LineTo x="26.30" y="0.30" />

                <MoveTo x="28.30" y="184.75" />
                <CurveTo cx="30.90" cy="187.30" x="31.95" y="189.85" />
                <CurveTo cx="32.95" cy="192.40" x="31.80" y="193.45" />
                <CurveTo cx="30.65" cy="194.50" x="28.05" y="193.45" />
                <CurveTo cx="25.40" cy="192.40" x="22.85" y="189.85" />
                <CurveTo cx="20.25" cy="187.30" x="19.25" y="184.75" />
                <CurveTo cx="18.20" cy="182.20" x="19.35" y="181.15" />
                <CurveTo cx="20.45" cy="180.10" x="23.10" y="181.15" />
                <CurveTo cx="25.75" cy="182.25" x="28.30" y="184.75" />

                <MoveTo x="94.45" y="186.45" />
                <CurveTo cx="91.85" cy="189.05" x="90.80" y="191.65" />
                <CurveTo cx="89.75" cy="194.20" x="90.90" y="195.30" />
                <CurveTo cx="92.05" cy="196.35" x="94.75" y="195.25" />
                <CurveTo cx="97.45" cy="194.20" x="100.05" y="191.60" />
                <CurveTo cx="102.70" cy="189.00" x="103.70" y="186.45" />
                <CurveTo cx="104.75" cy="183.90" x="103.60" y="182.80" />
                <CurveTo cx="102.45" cy="181.75" x="99.75" y="182.85" />
                <CurveTo cx="97.10" cy="183.90" x="94.45" y="186.45" />

                <MoveTo x="45.05" y="17.85" />
                <CurveTo cx="63.35" cy="15.00" x="79.80" y="17.85" />
                <CurveTo cx="89.60" cy="19.50" x="90.15" y="27.40" />
                <CurveTo cx="91.25" cy="41.00" x="90.15" y="52.60" />
                <CurveTo cx="89.45" cy="61.00" x="79.80" y="62.20" />
                <CurveTo cx="63.50" cy="64.20" x="45.05" y="62.20" />
                <CurveTo cx="35.25" cy="61.10" x="34.70" y="52.60" />
                <CurveTo cx="33.60" cy="38.70" x="34.70" y="27.40" />
                <CurveTo cx="35.40" cy="19.30" x="45.05" y="17.85" />

                <MoveTo x="29.45" y="18.85" />
                <LineTo x="29.20" y="43.35" />
                <CurveTo cx="28.90" cy="43.85" x="27.70" y="44.05" />
                <CurveTo cx="27.05" cy="44.15" x="26.80" y="44.25" />
                <LineTo x="11.40" y="44.10" />
                <CurveTo cx="11.65" cy="42.45" x="12.05" y="39.05" />
                <CurveTo cx="12.65" cy="33.80" x="13.15" y="31.35" />
                <CurveTo cx="14.05" cy="27.20" x="15.50" y="24.60" />
                <CurveTo cx="19.20" cy="18.00" x="28.55" y="18.50" />
                <LineTo x="29.45" y="18.85" />

                <MoveTo x="96.65" y="19.10" />
                <LineTo x="96.10" y="43.65" />
                <CurveTo cx="96.35" cy="44.15" x="97.65" y="44.40" />
                <CurveTo cx="98.30" cy="44.50" x="98.55" y="44.60" />
                <LineTo x="114.45" y="44.85" />
                <CurveTo cx="114.25" cy="43.20" x="113.95" y="39.70" />
                <CurveTo cx="113.45" cy="34.45" x="113.00" y="31.95" />
                <CurveTo cx="112.30" cy="27.85" x="110.85" y="25.20" />
                <CurveTo cx="107.20" cy="18.55" x="97.60" y="18.80" />
                <LineTo x="96.65" y="19.10" />

                <MoveTo x="10.95" y="50.55" />
                <LineTo x="29.30" y="50.80" />
                <CurveTo cx="28.70" cy="58.00" x="30.55" y="61.60" />
                <CurveTo cx="32.50" cy="65.35" x="38.40" y="67.80" />
                <LineTo x="38.55" y="67.85" />
                <CurveTo cx="39.30" cy="68.10" x="39.30" y="68.65" />
                <LineTo x="39.30" y="69.15" />
                <LineTo x="39.70" y="81.90" />
                <CurveTo cx="39.75" cy="82.75" x="35.20" y="82.50" />
                <CurveTo cx="29.95" cy="82.15" x="25.20" y="79.80" />
                <CurveTo cx="18.90" cy="76.60" x="15.30" y="70.30" />
                <CurveTo cx="10.85" cy="62.50" x="10.95" y="50.55" />

                <MoveTo x="113.85" y="51.50" />
                <LineTo x="96.30" y="51.75" />
                <CurveTo cx="97.05" cy="58.85" x="94.85" y="62.55" />
                <CurveTo cx="92.75" cy="66.15" x="86.65" y="68.35" />
                <LineTo x="86.60" y="68.35" />
                <CurveTo cx="85.75" cy="68.70" x="85.75" y="69.20" />
                <LineTo x="85.75" y="69.70" />
                <LineTo x="85.35" y="82.20" />
                <CurveTo cx="85.30" cy="83.05" x="89.85" y="82.80" />
                <CurveTo cx="95.05" cy="82.45" x="99.70" y="80.15" />
                <CurveTo cx="105.95" cy="77.00" x="109.55" y="70.85" />
                <CurveTo cx="113.95" cy="63.20" x="113.85" y="51.50" />

                <MoveTo x="49.40" y="70.45" />
                <CurveTo cx="63.40" cy="72.90" x="75.80" y="70.45" />
                <CurveTo cx="76.30" cy="70.35" x="77.20" y="70.10" />
                <CurveTo cx="79.85" cy="69.40" x="79.65" y="71.05" />
                <LineTo x="78.30" y="82.95" />
                <CurveTo cx="77.95" cy="85.75" x="75.00" y="85.95" />
                <LineTo x="63.25" y="86.55" />
                <LineTo x="49.40" y="85.95" />
                <CurveTo cx="46.75" cy="85.80" x="46.40" y="82.95" />
                <LineTo x="44.80" y="70.95" />
                <CurveTo cx="44.60" cy="69.60" x="48.85" y="70.35" />
                <LineTo x="49.40" y="70.45" />

                <MoveTo x="44.60" y="95.50" />
                <CurveTo cx="61.90" cy="96.65" x="80.35" y="95.60" />
                <LineTo x="78.10" y="106.70" />
                <CurveTo cx="77.90" cy="108.50" x="75.45" y="109.90" />
                <CurveTo cx="74.45" cy="110.40" x="74.10" y="110.75" />
                <CurveTo cx="68.10" cy="111.30" x="62.60" y="111.35" />
                <CurveTo cx="56.65" cy="111.40" x="51.10" y="110.85" />
                <CurveTo cx="49.50" cy="110.25" x="48.70" y="109.55" />
                <CurveTo cx="47.55" cy="108.60" x="47.20" y="106.95" />
                <LineTo x="44.60" y="95.50" />

                <MoveTo x="48.30" y="157.30" />
                <CurveTo cx="56.20" cy="157.85" x="62.35" y="157.85" />
                <CurveTo cx="68.65" cy="157.90" x="76.55" y="157.40" />
                <LineTo x="75.15" y="166.75" />
                <CurveTo cx="75.00" cy="168.05" x="74.30" y="169.05" />
                <CurveTo cx="73.90" cy="169.65" x="72.90" y="170.50" />
                <LineTo x="72.50" y="170.85" />
                <CurveTo cx="62.65" cy="171.95" x="53.15" y="170.80" />
                <CurveTo cx="51.80" cy="170.30" x="51.15" y="169.65" />
                <CurveTo cx="50.25" cy="168.80" x="49.90" y="167.30" />
                <LineTo x="48.30" y="157.30" />

                <MoveTo x="45.65" y="136.55" />
                <CurveTo cx="61.95" cy="137.70" x="78.20" y="136.65" />
                <LineTo x="75.80" y="147.75" />
                <CurveTo cx="75.65" cy="149.30" x="74.05" y="149.90" />
                <CurveTo cx="73.20" cy="150.20" x="72.85" y="150.55" />
                <CurveTo cx="63.10" cy="151.30" x="52.70" y="150.50" />
                <LineTo x="51.90" y="150.20" />
                <CurveTo cx="50.40" cy="149.65" x="49.75" y="149.20" />
                <CurveTo cx="48.65" cy="148.45" x="48.35" y="147.20" />
                <LineTo x="45.65" y="136.55" />

                <MoveTo x="45.05" y="116.35" />
                <CurveTo cx="62.25" cy="117.50" x="79.35" y="116.45" />
                <LineTo x="76.85" y="127.85" />
                <CurveTo cx="76.65" cy="129.45" x="75.05" y="130.05" />
                <CurveTo cx="74.15" cy="130.40" x="73.75" y="130.75" />
                <CurveTo cx="68.20" cy="131.10" x="63.10" y="131.10" />
                <CurveTo cx="57.60" cy="131.10" x="52.50" y="130.65" />
                <LineTo x="51.80" y="130.40" />
                <CurveTo cx="50.15" cy="129.90" x="49.45" y="129.45" />
                <CurveTo cx="48.25" cy="128.65" x="47.95" y="127.30" />
                <LineTo x="45.05" y="116.35" />

                <MoveTo x="12.00" y="85.15" />
                <CurveTo cx="11.20" cy="84.80" x="10.80" y="86.05" />
                <CurveTo cx="10.50" cy="87.00" x="10.55" y="88.40" />
                <LineTo x="11.00" y="95.55" />
                <CurveTo cx="11.10" cy="98.55" x="14.30" y="100.65" />
                <CurveTo cx="16.15" cy="101.85" x="20.10" y="103.20" />
                <LineTo x="21.05" y="103.55" />
                <CurveTo cx="23.25" cy="104.25" x="25.75" y="105.00" />
                <CurveTo cx="29.80" cy="106.15" x="33.00" y="106.90" />
                <CurveTo cx="42.55" cy="109.00" x="42.05" y="106.60" />
                <CurveTo cx="40.05" cy="96.80" x="35.95" y="93.30" />
                <CurveTo cx="33.35" cy="91.05" x="25.95" y="89.50" />
                <CurveTo cx="17.45" cy="87.75" x="12.00" y="85.15" />

                <MoveTo x="112.80" y="85.80" />
                <CurveTo cx="113.55" cy="85.45" x="113.95" y="86.65" />
                <CurveTo cx="114.25" cy="87.60" x="114.20" y="88.95" />
                <LineTo x="113.95" y="95.70" />
                <CurveTo cx="113.85" cy="98.95" x="111.30" y="100.75" />
                <CurveTo cx="109.80" cy="101.80" x="105.90" y="102.95" />
                <CurveTo cx="104.50" cy="103.40" x="103.85" y="103.60" />
                <CurveTo cx="101.40" cy="104.35" x="99.25" y="105.00" />
                <CurveTo cx="95.20" cy="106.15" x="92.10" y="106.80" />
                <CurveTo cx="82.70" cy="108.90" x="83.20" y="106.55" />
                <CurveTo cx="85.20" cy="97.10" x="89.20" y="93.70" />
                <CurveTo cx="91.80" cy="91.50" x="99.05" y="90.00" />
                <CurveTo cx="107.40" cy="88.30" x="112.80" y="85.80" />

                <MoveTo x="13.80" y="108.05" />
                <CurveTo cx="13.05" cy="107.70" x="12.70" y="108.95" />
                <CurveTo cx="12.45" cy="109.85" x="12.50" y="111.25" />
                <LineTo x="12.95" y="117.95" />
                <CurveTo cx="13.15" cy="121.20" x="15.65" y="122.90" />
                <CurveTo cx="17.15" cy="123.90" x="20.95" y="124.95" />
                <CurveTo cx="22.25" cy="125.30" x="22.85" y="125.50" />
                <CurveTo cx="25.10" cy="126.15" x="27.30" y="126.70" />
                <CurveTo cx="31.20" cy="127.75" x="34.25" y="128.30" />
                <CurveTo cx="43.30" cy="130.05" x="42.75" y="127.75" />
                <CurveTo cx="40.60" cy="118.35" x="36.65" y="115.10" />
                <CurveTo cx="34.10" cy="113.00" x="27.10" y="111.80" />
                <CurveTo cx="19.00" cy="110.35" x="13.80" y="108.05" />

                <MoveTo x="111.15" y="108.00" />
                <CurveTo cx="111.90" cy="107.65" x="112.25" y="108.85" />
                <CurveTo cx="112.55" cy="109.80" x="112.45" y="111.15" />
                <LineTo x="112.00" y="117.80" />
                <CurveTo cx="111.80" cy="121.00" x="109.30" y="122.65" />
                <CurveTo cx="107.80" cy="123.65" x="104.00" y="124.70" />
                <CurveTo cx="102.55" cy="125.10" x="101.90" y="125.30" />
                <LineTo x="101.50" y="125.40" />
                <CurveTo cx="98.70" cy="126.15" x="96.60" y="126.70" />
                <CurveTo cx="92.70" cy="127.60" x="89.75" y="128.15" />
                <CurveTo cx="81.15" cy="129.70" x="81.65" y="127.50" />
                <CurveTo cx="83.90" cy="118.20" x="87.90" y="115.00" />
                <CurveTo cx="90.50" cy="112.90" x="97.65" y="111.65" />
                <CurveTo cx="105.85" cy="110.25" x="111.15" y="108.00" />

                <MoveTo x="15.50" y="128.85" />
                <CurveTo cx="14.75" cy="128.50" x="14.45" y="129.70" />
                <CurveTo cx="14.20" cy="130.65" x="14.30" y="131.95" />
                <LineTo x="14.70" y="138.50" />
                <CurveTo cx="14.85" cy="141.70" x="17.20" y="143.35" />
                <CurveTo cx="18.60" cy="144.30" x="22.15" y="145.35" />
                <CurveTo cx="23.45" cy="145.70" x="24.05" y="145.90" />
                <CurveTo cx="26.00" cy="146.50" x="28.25" y="147.10" />
                <CurveTo cx="31.90" cy="148.10" x="34.75" y="148.65" />
                <CurveTo cx="43.30" cy="150.35" x="42.80" y="148.10" />
                <CurveTo cx="40.75" cy="138.95" x="37.05" y="135.75" />
                <CurveTo cx="34.65" cy="133.70" x="28.00" y="132.50" />
                <CurveTo cx="20.45" cy="131.10" x="15.50" y="128.85" />

                <MoveTo x="108.60" y="129.15" />
                <CurveTo cx="109.35" cy="128.80" x="109.65" y="130.00" />
                <CurveTo cx="109.90" cy="130.85" x="109.85" y="132.20" />
                <LineTo x="109.40" y="138.70" />
                <CurveTo cx="109.25" cy="141.85" x="106.90" y="143.50" />
                <CurveTo cx="105.55" cy="144.45" x="102.00" y="145.50" />
                <CurveTo cx="100.75" cy="145.85" x="100.15" y="146.05" />
                <CurveTo cx="98.25" cy="146.65" x="95.95" y="147.25" />
                <CurveTo cx="92.35" cy="148.20" x="89.50" y="148.80" />
                <CurveTo cx="81.00" cy="150.50" x="81.50" y="148.25" />
                <CurveTo cx="83.50" cy="139.15" x="87.20" y="136.00" />
                <CurveTo cx="89.60" cy="133.95" x="96.20" y="132.75" />
                <CurveTo cx="103.70" cy="131.35" x="108.60" y="129.15" />

                <MoveTo x="18.95" y="150.80" />
                <CurveTo cx="18.30" cy="150.45" x="18.00" y="151.65" />
                <CurveTo cx="17.75" cy="152.50" x="17.80" y="153.80" />
                <LineTo x="18.20" y="160.15" />
                <CurveTo cx="18.35" cy="163.20" x="20.50" y="164.80" />
                <CurveTo cx="21.80" cy="165.75" x="25.05" y="166.75" />
                <CurveTo cx="26.30" cy="167.15" x="26.90" y="167.35" />
                <LineTo x="27.55" y="167.55" />
                <CurveTo cx="30.05" cy="168.30" x="31.90" y="168.75" />
                <CurveTo cx="35.20" cy="169.65" x="37.65" y="170.15" />
                <CurveTo cx="44.80" cy="171.50" x="44.35" y="169.45" />
                <CurveTo cx="42.45" cy="160.55" x="39.00" y="157.50" />
                <CurveTo cx="36.75" cy="155.50" x="30.60" y="154.30" />
                <CurveTo cx="23.55" cy="153.00" x="18.95" y="150.80" />

                <MoveTo x="106.10" y="150.75" />
                <CurveTo cx="106.75" cy="150.40" x="107.05" y="151.55" />
                <CurveTo cx="107.30" cy="152.45" x="107.25" y="153.75" />
                <LineTo x="106.85" y="160.05" />
                <CurveTo cx="106.70" cy="163.10" x="104.50" y="164.65" />
                <CurveTo cx="103.25" cy="165.60" x="100.00" y="166.60" />
                <CurveTo cx="98.80" cy="166.95" x="98.25" y="167.15" />
                <CurveTo cx="96.25" cy="167.75" x="94.35" y="168.30" />
                <CurveTo cx="90.95" cy="169.25" x="88.35" y="169.80" />
                <CurveTo cx="80.45" cy="171.40" x="80.95" y="169.25" />
                <CurveTo cx="82.85" cy="160.45" x="86.25" y="157.35" />
                <CurveTo cx="88.45" cy="155.40" x="94.55" y="154.25" />
                <CurveTo cx="101.55" cy="152.90" x="106.10" y="150.75" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (59, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 59
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="103.90" y="15.30" />
                <CurveTo cx="106.25" cy="36.35" x="107.35" y="66.00" />
                <CurveTo cx="108.35" cy="92.05" x="108.35" y="124.05" />
                <CurveTo cx="108.35" cy="151.55" x="107.65" y="182.20" />
                <CurveTo cx="107.05" cy="207.70" x="106.05" y="232.95" />
                <CurveTo cx="105.85" cy="265.75" x="94.85" y="277.70" />
                <CurveTo cx="84.10" cy="289.30" x="54.80" y="289.65" />
                <CurveTo cx="26.40" cy="289.95" x="14.15" y="276.15" />
                <CurveTo cx="2.50" cy="263.05" x="2.80" y="234.15" />
                <CurveTo cx="2.60" cy="223.85" x="1.95" y="200.50" />
                <CurveTo cx="1.20" cy="170.30" x="0.85" y="154.30" />
                <CurveTo cx="0.30" cy="127.25" x="0.15" y="106.00" />
                <CurveTo cx="0.00" cy="80.05" x="0.45" y="59.60" />
                <CurveTo cx="0.95" cy="36.15" x="2.30" y="19.00" />
                <CurveTo cx="1.60" cy="11.25" x="13.50" y="6.80" />
                <CurveTo cx="13.70" cy="6.80" x="13.70" y="13.70" />
                <CurveTo cx="13.70" cy="20.55" x="13.80" y="23.00" />
                <CurveTo cx="14.00" cy="27.25" x="17.95" y="28.00" />
                <CurveTo cx="25.35" cy="29.40" x="32.70" y="29.35" />
                <CurveTo cx="41.00" cy="29.30" x="47.45" y="27.30" />
                <CurveTo cx="50.40" cy="26.40" x="50.50" y="22.90" />
                <CurveTo cx="50.60" cy="20.40" x="50.40" y="10.75" />
                <CurveTo cx="50.20" cy="1.45" x="50.40" y="1.45" />
                <CurveTo cx="72.55" cy="0.00" x="85.55" y="2.50" />
                <CurveTo cx="102.15" cy="5.70" x="103.90" y="15.30" />

                <MoveTo x="16.80" y="54.60" />
                <LineTo x="91.00" y="54.60" />
                <CurveTo cx="95.35" cy="54.60" x="95.35" y="58.90" />
                <LineTo x="95.35" y="131.55" />
                <CurveTo cx="95.35" cy="135.85" x="91.00" y="135.85" />
                <LineTo x="16.80" y="135.85" />
                <CurveTo cx="12.45" cy="135.85" x="12.45" y="131.55" />
                <LineTo x="12.45" y="58.90" />
                <CurveTo cx="12.45" cy="54.60" x="16.80" y="54.60" />

                <MoveTo x="57.30" y="33.70" />
                <LineTo x="58.35" y="33.80" />
                <CurveTo cx="61.10" cy="34.10" x="61.80" y="34.55" />
                <CurveTo cx="62.70" cy="35.15" x="63.25" y="37.45" />
                <CurveTo cx="63.30" cy="37.65" x="63.10" y="37.80" />
                <CurveTo cx="62.90" cy="37.95" x="62.65" y="37.95" />
                <LineTo x="57.30" y="37.95" />
                <CurveTo cx="56.70" cy="37.95" x="56.70" y="37.45" />
                <LineTo x="56.70" y="34.20" />
                <CurveTo cx="56.70" cy="34.00" x="56.90" y="33.85" />
                <CurveTo cx="57.10" cy="33.65" x="57.30" y="33.70" />

                <MoveTo x="50.20" y="33.70" />
                <LineTo x="49.15" y="33.80" />
                <CurveTo cx="46.40" cy="34.10" x="45.70" y="34.55" />
                <CurveTo cx="44.80" cy="35.15" x="44.25" y="37.45" />
                <CurveTo cx="44.20" cy="37.65" x="44.40" y="37.85" />
                <CurveTo cx="44.60" cy="38.00" x="44.85" y="38.00" />
                <LineTo x="50.20" y="38.00" />
                <CurveTo cx="50.80" cy="38.00" x="50.80" y="37.45" />
                <LineTo x="50.80" y="34.25" />
                <CurveTo cx="50.80" cy="33.60" x="50.20" y="33.70" />

                <MoveTo x="75.35" y="30.70" />
                <LineTo x="77.50" y="30.70" />
                <CurveTo cx="78.20" cy="30.70" x="78.65" y="31.55" />
                <CurveTo cx="79.00" cy="32.30" x="79.00" y="33.45" />
                <CurveTo cx="78.95" cy="34.55" x="78.60" y="35.30" />
                <CurveTo cx="78.15" cy="36.15" x="77.50" y="36.15" />
                <LineTo x="75.25" y="36.15" />
                <LineTo x="74.45" y="36.10" />
                <CurveTo cx="74.35" cy="36.05" x="74.50" y="35.70" />
                <CurveTo cx="74.70" cy="35.30" x="74.80" y="35.00" />
                <CurveTo cx="75.45" cy="33.30" x="74.70" y="31.50" />
                <LineTo x="74.60" y="31.30" />
                <CurveTo cx="74.30" cy="30.65" x="74.80" y="30.70" />
                <LineTo x="75.35" y="30.70" />

                <MoveTo x="31.20" y="30.65" />
                <LineTo x="29.10" y="30.65" />
                <CurveTo cx="28.35" cy="30.65" x="27.95" y="31.50" />
                <CurveTo cx="27.60" cy="32.30" x="27.60" y="33.40" />
                <CurveTo cx="27.60" cy="34.50" x="28.00" y="35.30" />
                <CurveTo cx="28.45" cy="36.15" x="29.10" y="36.15" />
                <LineTo x="31.35" y="36.15" />
                <LineTo x="32.15" y="36.05" />
                <CurveTo cx="32.25" cy="36.00" x="32.05" y="35.60" />
                <CurveTo cx="31.85" cy="35.25" x="31.75" y="34.95" />
                <CurveTo cx="31.45" cy="34.05" x="31.45" y="33.25" />
                <CurveTo cx="31.45" cy="32.40" x="31.90" y="31.45" />
                <LineTo x="31.95" y="31.30" />
                <LineTo x="32.15" y="30.70" />
                <CurveTo cx="32.15" cy="30.60" x="31.75" y="30.65" />
                <LineTo x="31.20" y="30.65" />

                <MoveTo x="37.60" y="159.10" />
                <CurveTo cx="45.25" cy="156.30" x="54.20" y="156.40" />
                <CurveTo cx="61.85" cy="156.50" x="71.00" y="158.70" />
                <CurveTo cx="72.55" cy="159.05" x="74.35" y="161.25" />
                <CurveTo cx="76.25" cy="163.55" x="76.20" y="165.40" />
                <CurveTo cx="76.10" cy="167.00" x="76.00" y="170.15" />
                <CurveTo cx="75.65" cy="180.00" x="74.50" y="183.95" />
                <CurveTo cx="72.65" cy="190.35" x="67.15" y="192.60" />
                <CurveTo cx="61.35" cy="195.00" x="53.75" y="194.90" />
                <CurveTo cx="45.85" cy="194.80" x="40.55" y="191.95" />
                <CurveTo cx="35.40" cy="189.15" x="33.65" y="182.75" />
                <CurveTo cx="32.60" cy="179.00" x="32.40" y="170.50" />
                <CurveTo cx="32.30" cy="167.00" x="32.20" y="165.35" />
                <CurveTo cx="32.10" cy="163.70" x="34.00" y="161.65" />
                <CurveTo cx="35.75" cy="159.75" x="37.60" y="159.10" />

                <MoveTo x="28.95" y="165.40" />
                <CurveTo cx="30.45" cy="169.10" x="29.40" y="173.95" />
                <CurveTo cx="29.20" cy="175.05" x="27.60" y="175.80" />
                <CurveTo cx="26.10" cy="176.55" x="25.10" y="176.25" />
                <CurveTo cx="20.20" cy="174.75" x="15.90" y="172.00" />
                <CurveTo cx="12.60" cy="169.90" x="8.05" y="165.90" />
                <CurveTo cx="5.65" cy="155.60" x="15.55" y="157.75" />
                <CurveTo cx="24.60" cy="159.75" x="28.95" y="165.40" />

                <MoveTo x="79.15" y="165.70" />
                <CurveTo cx="77.65" cy="169.40" x="78.70" y="174.25" />
                <CurveTo cx="78.90" cy="175.35" x="80.50" y="176.10" />
                <CurveTo cx="82.05" cy="176.80" x="83.00" y="176.50" />
                <CurveTo cx="87.90" cy="175.00" x="92.20" y="172.30" />
                <CurveTo cx="95.50" cy="170.20" x="100.05" y="166.20" />
                <CurveTo cx="102.45" cy="155.85" x="92.55" y="158.05" />
                <CurveTo cx="83.50" cy="160.00" x="79.15" y="165.70" />

                <MoveTo x="30.30" y="187.75" />
                <CurveTo cx="37.30" cy="190.65" x="35.55" y="198.85" />
                <CurveTo cx="33.75" cy="206.95" x="25.45" y="203.00" />
                <CurveTo cx="17.10" cy="197.20" x="11.80" y="192.05" />
                <CurveTo cx="6.20" cy="186.50" x="6.70" y="184.20" />
                <CurveTo cx="7.05" cy="177.55" x="13.50" y="179.55" />
                <CurveTo cx="17.95" cy="180.85" x="24.40" y="184.20" />
                <CurveTo cx="30.30" cy="187.25" x="30.30" y="187.75" />

                <MoveTo x="77.40" y="188.05" />
                <CurveTo cx="70.40" cy="190.95" x="72.15" y="199.10" />
                <CurveTo cx="73.95" cy="207.25" x="82.25" y="203.25" />
                <CurveTo cx="90.55" cy="197.50" x="95.85" y="192.30" />
                <CurveTo cx="101.45" cy="186.80" x="100.95" y="184.50" />
                <CurveTo cx="100.70" cy="177.85" x="94.15" y="179.85" />
                <CurveTo cx="89.70" cy="181.15" x="83.30" y="184.50" />
                <CurveTo cx="77.40" cy="187.55" x="77.40" y="188.05" />

                <MoveTo x="14.50" y="202.75" />
                <CurveTo cx="16.50" cy="204.00" x="23.35" y="206.30" />
                <CurveTo cx="27.55" cy="207.70" x="29.85" y="208.55" />
                <CurveTo cx="31.65" cy="209.35" x="32.85" y="211.40" />
                <CurveTo cx="33.80" cy="212.90" x="35.45" y="217.20" />
                <CurveTo cx="36.60" cy="218.95" x="35.40" y="220.55" />
                <CurveTo cx="34.25" cy="222.15" x="32.20" y="221.70" />
                <CurveTo cx="25.45" cy="220.45" x="21.40" y="219.00" />
                <CurveTo cx="17.35" cy="217.55" x="12.35" y="214.60" />
                <CurveTo cx="9.20" cy="211.90" x="8.00" y="209.60" />
                <CurveTo cx="7.00" cy="207.50" x="7.10" y="204.85" />
                <CurveTo cx="7.25" cy="202.25" x="9.80" y="201.85" />
                <CurveTo cx="11.90" cy="201.55" x="14.50" y="202.75" />

                <MoveTo x="93.95" y="202.00" />
                <CurveTo cx="91.85" cy="203.30" x="87.55" y="205.10" />
                <CurveTo cx="84.10" cy="206.60" x="78.60" y="208.65" />
                <CurveTo cx="76.80" cy="209.40" x="75.60" y="211.45" />
                <CurveTo cx="74.70" cy="212.95" x="73.00" y="217.25" />
                <CurveTo cx="71.90" cy="219.00" x="73.05" y="220.60" />
                <CurveTo cx="74.20" cy="222.20" x="76.25" y="221.80" />
                <CurveTo cx="83.00" cy="220.55" x="87.05" y="219.10" />
                <CurveTo cx="91.10" cy="217.65" x="96.05" y="214.70" />
                <CurveTo cx="99.25" cy="212.00" x="100.45" y="209.65" />
                <CurveTo cx="101.45" cy="207.55" x="101.35" y="204.90" />
                <CurveTo cx="101.20" cy="202.25" x="98.65" y="201.50" />
                <CurveTo cx="96.40" cy="200.90" x="93.95" y="202.00" />

                <MoveTo x="20.05" y="253.50" />
                <CurveTo cx="22.05" cy="254.75" x="27.10" y="256.45" />
                <CurveTo cx="30.55" cy="257.60" x="32.80" y="258.45" />
                <CurveTo cx="34.60" cy="259.20" x="35.80" y="261.25" />
                <CurveTo cx="36.70" cy="262.75" x="38.40" y="267.05" />
                <CurveTo cx="39.55" cy="268.80" x="38.35" y="270.40" />
                <CurveTo cx="37.20" cy="272.00" x="35.15" y="271.60" />
                <CurveTo cx="33.30" cy="271.10" x="30.50" y="270.40" />
                <CurveTo cx="26.40" cy="269.40" x="24.75" y="268.80" />
                <CurveTo cx="22.45" cy="267.95" x="18.75" y="266.00" />
                <CurveTo cx="15.75" cy="263.65" x="14.00" y="260.85" />
                <CurveTo cx="12.10" cy="257.70" x="12.40" y="254.90" />
                <CurveTo cx="12.65" cy="252.80" x="15.35" y="252.70" />
                <CurveTo cx="17.30" cy="252.55" x="20.05" y="253.50" />

                <MoveTo x="88.40" y="253.60" />
                <CurveTo cx="86.45" cy="254.80" x="81.50" y="256.45" />
                <CurveTo cx="78.00" cy="257.65" x="75.65" y="258.50" />
                <CurveTo cx="73.85" cy="259.30" x="72.65" y="261.35" />
                <CurveTo cx="71.70" cy="262.85" x="70.05" y="267.15" />
                <CurveTo cx="68.90" cy="268.90" x="70.10" y="270.50" />
                <CurveTo cx="71.25" cy="272.10" x="73.30" y="271.65" />
                <CurveTo cx="75.10" cy="271.20" x="77.80" y="270.50" />
                <CurveTo cx="82.00" cy="269.50" x="83.65" y="268.90" />
                <CurveTo cx="85.95" cy="268.05" x="89.70" y="266.05" />
                <CurveTo cx="92.70" cy="263.70" x="94.45" y="260.90" />
                <CurveTo cx="96.35" cy="257.80" x="96.00" y="255.00" />
                <CurveTo cx="95.75" cy="252.90" x="93.10" y="252.75" />
                <CurveTo cx="91.05" cy="252.65" x="88.40" y="253.60" />

                <MoveTo x="17.65" y="236.05" />
                <CurveTo cx="19.90" cy="237.45" x="27.30" y="240.10" />
                <CurveTo cx="30.40" cy="241.20" x="32.15" y="241.85" />
                <CurveTo cx="33.80" cy="242.55" x="34.90" y="244.50" />
                <CurveTo cx="35.65" cy="245.80" x="37.15" y="249.85" />
                <LineTo x="37.50" y="250.70" />
                <CurveTo cx="38.65" cy="252.40" x="37.50" y="254.05" />
                <CurveTo cx="36.35" cy="255.65" x="34.30" y="255.20" />
                <CurveTo cx="27.40" cy="253.95" x="23.60" y="252.60" />
                <CurveTo cx="19.55" cy="251.20" x="14.65" y="248.30" />
                <CurveTo cx="12.45" cy="246.40" x="11.35" y="244.20" />
                <CurveTo cx="10.05" cy="241.60" x="10.05" y="238.30" />
                <CurveTo cx="10.05" cy="235.90" x="12.90" y="235.45" />
                <CurveTo cx="15.30" cy="235.00" x="17.65" y="236.05" />

                <MoveTo x="90.75" y="236.15" />
                <CurveTo cx="88.55" cy="237.45" x="81.50" y="240.00" />
                <CurveTo cx="78.15" cy="241.20" x="76.30" y="241.90" />
                <CurveTo cx="74.65" cy="242.65" x="73.55" y="244.55" />
                <CurveTo cx="72.80" cy="245.85" x="71.25" y="249.85" />
                <LineTo x="70.90" y="250.75" />
                <CurveTo cx="69.80" cy="252.50" x="70.95" y="254.10" />
                <CurveTo cx="72.10" cy="255.70" x="74.15" y="255.30" />
                <CurveTo cx="81.05" cy="254.05" x="84.90" y="252.70" />
                <CurveTo cx="88.90" cy="251.30" x="93.80" y="248.40" />
                <CurveTo cx="96.00" cy="246.40" x="97.10" y="244.25" />
                <CurveTo cx="98.40" cy="241.70" x="98.40" y="238.40" />
                <CurveTo cx="98.35" cy="236.00" x="95.55" y="235.55" />
                <CurveTo cx="93.15" cy="235.10" x="90.75" y="236.15" />

                <MoveTo x="16.40" y="219.45" />
                <CurveTo cx="18.15" cy="220.55" x="21.20" y="221.80" />
                <CurveTo cx="23.10" cy="222.60" x="27.90" y="224.35" />
                <LineTo x="30.85" y="225.45" />
                <CurveTo cx="32.65" cy="226.20" x="33.85" y="228.25" />
                <CurveTo cx="34.75" cy="229.80" x="36.45" y="234.10" />
                <CurveTo cx="37.60" cy="235.85" x="36.40" y="237.45" />
                <CurveTo cx="35.25" cy="239.05" x="33.20" y="238.60" />
                <CurveTo cx="26.45" cy="237.35" x="22.40" y="235.90" />
                <CurveTo cx="18.35" cy="234.45" x="13.35" y="231.50" />
                <CurveTo cx="10.20" cy="228.70" x="9.15" y="226.55" />
                <CurveTo cx="8.10" cy="224.50" x="8.35" y="221.95" />
                <CurveTo cx="8.60" cy="219.30" x="11.40" y="218.75" />
                <CurveTo cx="13.80" cy="218.30" x="16.40" y="219.45" />

                <MoveTo x="92.05" y="219.55" />
                <CurveTo cx="90.30" cy="220.60" x="87.20" y="221.90" />
                <CurveTo cx="85.25" cy="222.70" x="80.50" y="224.45" />
                <LineTo x="77.60" y="225.55" />
                <CurveTo cx="75.80" cy="226.30" x="74.60" y="228.35" />
                <CurveTo cx="73.65" cy="229.85" x="72.00" y="234.15" />
                <CurveTo cx="70.85" cy="235.90" x="72.00" y="237.50" />
                <CurveTo cx="73.15" cy="239.10" x="75.25" y="238.70" />
                <CurveTo cx="82.00" cy="237.45" x="86.05" y="236.00" />
                <CurveTo cx="90.05" cy="234.55" x="95.05" y="231.55" />
                <CurveTo cx="98.20" cy="228.80" x="99.30" y="226.65" />
                <CurveTo cx="100.35" cy="224.60" x="100.10" y="222.00" />
                <CurveTo cx="99.85" cy="219.40" x="97.05" y="218.85" />
                <CurveTo cx="94.65" cy="218.40" x="92.05" y="219.55" />

                <MoveTo x="39.10" y="210.95" />
                <CurveTo cx="46.35" cy="212.15" x="53.60" y="212.10" />
                <CurveTo cx="60.60" cy="212.10" x="68.20" y="210.95" />
                <CurveTo cx="69.65" cy="210.75" x="70.35" y="212.35" />
                <CurveTo cx="71.00" cy="213.90" x="70.35" y="215.25" />
                <CurveTo cx="69.75" cy="217.45" x="68.20" y="219.35" />
                <CurveTo cx="67.25" cy="220.55" x="64.95" y="222.65" />
                <LineTo x="63.90" y="223.65" />
                <CurveTo cx="59.35" cy="224.15" x="54.30" y="224.20" />
                <CurveTo cx="50.40" cy="224.20" x="44.70" y="223.90" />
                <CurveTo cx="42.45" cy="222.80" x="41.05" y="221.10" />
                <CurveTo cx="39.40" cy="219.10" x="37.80" y="215.25" />
                <CurveTo cx="37.50" cy="214.05" x="37.80" y="212.50" />
                <CurveTo cx="38.20" cy="210.80" x="39.10" y="210.95" />

                <MoveTo x="40.30" y="227.40" />
                <CurveTo cx="48.15" cy="228.30" x="54.20" y="228.25" />
                <CurveTo cx="59.95" cy="228.20" x="67.25" y="227.20" />
                <CurveTo cx="68.70" cy="227.00" x="69.20" y="228.70" />
                <CurveTo cx="69.65" cy="230.35" x="69.00" y="231.70" />
                <CurveTo cx="68.30" cy="234.35" x="67.05" y="236.25" />
                <CurveTo cx="66.00" cy="237.80" x="63.80" y="239.90" />
                <CurveTo cx="58.85" cy="240.70" x="54.45" y="240.75" />
                <CurveTo cx="50.55" cy="240.85" x="45.50" y="240.35" />
                <CurveTo cx="43.35" cy="239.30" x="41.85" y="237.30" />
                <CurveTo cx="40.20" cy="235.25" x="38.60" y="231.30" />
                <CurveTo cx="38.30" cy="230.20" x="38.80" y="228.80" />
                <CurveTo cx="39.35" cy="227.30" x="40.30" y="227.40" />

                <MoveTo x="41.95" y="243.65" />
                <CurveTo cx="49.30" cy="244.90" x="54.45" y="244.80" />
                <CurveTo cx="58.80" cy="244.70" x="66.55" y="243.45" />
                <CurveTo cx="68.00" cy="243.20" x="68.70" y="244.95" />
                <CurveTo cx="69.35" cy="246.60" x="68.70" y="247.95" />
                <CurveTo cx="67.80" cy="250.75" x="66.45" y="252.70" />
                <CurveTo cx="65.40" cy="254.20" x="63.30" y="256.15" />
                <CurveTo cx="58.30" cy="256.95" x="54.70" y="257.00" />
                <CurveTo cx="51.55" cy="257.05" x="46.50" y="256.60" />
                <CurveTo cx="44.35" cy="255.50" x="42.75" y="253.45" />
                <CurveTo cx="41.00" cy="251.25" x="39.40" y="247.30" />
                <CurveTo cx="38.65" cy="246.30" x="39.60" y="244.85" />
                <CurveTo cx="40.55" cy="243.40" x="41.95" y="243.65" />

                <MoveTo x="43.60" y="260.55" />
                <CurveTo cx="50.80" cy="261.10" x="54.65" y="261.05" />
                <CurveTo cx="56.65" cy="261.00" x="59.25" y="260.85" />
                <CurveTo cx="60.95" cy="260.70" x="65.15" y="260.35" />
                <LineTo x="65.60" y="260.30" />
                <CurveTo cx="67.10" cy="260.20" x="67.90" y="261.70" />
                <CurveTo cx="68.65" cy="263.10" x="68.00" y="264.40" />
                <CurveTo cx="67.20" cy="266.85" x="65.70" y="268.80" />
                <CurveTo cx="64.75" cy="270.00" x="62.60" y="272.00" />
                <LineTo x="61.95" y="272.60" />
                <CurveTo cx="56.90" cy="273.40" x="54.60" y="273.45" />
                <CurveTo cx="52.65" cy="273.50" x="47.70" y="273.05" />
                <CurveTo cx="43.75" cy="271.05" x="40.60" y="263.55" />
                <CurveTo cx="39.90" cy="262.60" x="41.05" y="261.50" />
                <CurveTo cx="42.15" cy="260.45" x="43.60" y="260.55" />

                <MoveTo x="55.05" y="283.80" />
                <CurveTo cx="56.40" cy="283.80" x="57.35" y="283.35" />
                <CurveTo cx="58.30" cy="282.85" x="58.30" y="282.20" />
                <CurveTo cx="58.30" cy="281.55" x="57.35" y="281.05" />
                <CurveTo cx="56.40" cy="280.60" x="55.05" y="280.60" />
                <CurveTo cx="53.70" cy="280.60" x="52.75" y="281.05" />
                <CurveTo cx="51.80" cy="281.55" x="51.80" y="282.20" />
                <CurveTo cx="51.80" cy="282.85" x="52.75" y="283.35" />
                <CurveTo cx="53.70" cy="283.80" x="55.05" y="283.80" />

                <MoveTo x="44.05" y="197.35" />
                <CurveTo cx="49.55" cy="198.75" x="54.30" y="198.85" />
                <CurveTo cx="59.70" cy="198.95" x="64.55" y="197.35" />
                <CurveTo cx="65.80" cy="196.95" x="66.80" y="198.50" />
                <CurveTo cx="67.75" cy="199.95" x="67.75" y="201.95" />
                <CurveTo cx="67.75" cy="203.70" x="66.80" y="204.95" />
                <CurveTo cx="65.95" cy="206.05" x="64.55" y="206.50" />
                <CurveTo cx="54.80" cy="209.40" x="44.05" y="206.50" />
                <CurveTo cx="42.65" cy="206.10" x="41.75" y="205.00" />
                <CurveTo cx="40.80" cy="203.75" x="40.80" y="201.95" />
                <CurveTo cx="40.80" cy="200.00" x="41.75" y="198.55" />
                <CurveTo cx="42.75" cy="197.05" x="44.05" y="197.35" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (60, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 60
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="104.05" y="9.30" />
                <CurveTo cx="106.40" cy="23.60" x="107.50" y="43.65" />
                <CurveTo cx="108.50" cy="61.35" x="108.50" y="83.05" />
                <CurveTo cx="108.55" cy="101.70" x="107.85" y="122.40" />
                <CurveTo cx="107.25" cy="139.85" x="106.20" y="156.80" />
                <CurveTo cx="105.90" cy="178.30" x="94.20" y="186.55" />
                <CurveTo cx="83.35" cy="194.20" x="55.20" y="194.85" />
                <CurveTo cx="28.30" cy="195.45" x="15.50" y="186.00" />
                <CurveTo cx="3.10" cy="176.90" x="2.95" y="157.65" />
                <CurveTo cx="2.75" cy="150.70" x="2.15" y="134.95" />
                <CurveTo cx="1.35" cy="114.45" x="1.00" y="103.60" />
                <CurveTo cx="0.45" cy="85.25" x="0.30" y="70.85" />
                <CurveTo cx="0.00" cy="33.50" x="2.50" y="11.80" />
                <CurveTo cx="1.95" cy="7.70" x="8.75" y="4.95" />
                <CurveTo cx="14.25" cy="2.75" x="24.25" y="1.50" />
                <CurveTo cx="31.55" cy="0.55" x="40.60" y="0.20" />
                <CurveTo cx="45.25" cy="0.05" x="49.25" y="0.05" />
                <LineTo x="49.35" y="0.05" />
                <CurveTo cx="61.85" cy="0.00" x="68.10" y="0.10" />
                <CurveTo cx="78.30" cy="0.30" x="85.10" y="1.05" />
                <CurveTo cx="102.35" cy="3.00" x="104.05" y="9.30" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (61, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 61
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="57.10" y="0.05" />
                <LineTo x="63.75" y="0.05" />
                <LineTo x="67.10" y="0.05" />
                <LineTo x="70.40" y="0.00" />
                <CurveTo cx="82.75" cy="0.00" x="92.35" y="3.65" />
                <CurveTo cx="117.55" cy="13.00" x="122.95" y="48.00" />
                <CurveTo cx="123.80" cy="53.35" x="124.05" y="59.15" />
                <CurveTo cx="124.25" cy="63.55" x="124.10" y="70.40" />
                <CurveTo cx="123.55" cy="99.70" x="123.55" y="136.20" />
                <CurveTo cx="123.55" cy="150.85" x="123.65" y="169.10" />
                <CurveTo cx="123.70" cy="180.10" x="123.80" y="202.05" />
                <LineTo x="123.95" y="228.55" />
                <LineTo x="123.95" y="230.35" />
                <CurveTo cx="124.05" cy="235.80" x="123.90" y="238.50" />
                <CurveTo cx="123.70" cy="243.00" x="123.00" y="246.60" />
                <CurveTo cx="119.35" cy="265.55" x="102.65" y="271.70" />
                <CurveTo cx="99.50" cy="272.90" x="93.80" y="273.20" />
                <CurveTo cx="91.35" cy="273.35" x="90.00" y="273.50" />
                <CurveTo cx="77.70" cy="274.90" x="63.40" y="274.85" />
                <CurveTo cx="50.55" cy="274.75" x="34.25" y="273.50" />
                <LineTo x="33.65" y="273.45" />
                <CurveTo cx="29.00" cy="273.10" x="26.90" y="272.65" />
                <CurveTo cx="23.50" cy="272.00" x="20.40" y="270.30" />
                <CurveTo cx="5.55" cy="262.20" x="4.10" y="239.35" />
                <CurveTo cx="3.75" cy="234.00" x="3.75" y="223.10" />
                <CurveTo cx="3.75" cy="216.00" x="3.65" y="212.50" />
                <CurveTo cx="3.25" cy="199.75" x="2.35" y="174.35" />
                <CurveTo cx="1.75" cy="157.65" x="1.50" y="149.30" />
                <CurveTo cx="1.05" cy="135.40" x="0.75" y="124.30" />
                <CurveTo cx="0.00" cy="96.45" x="0.00" y="74.15" />
                <CurveTo cx="0.00" cy="65.60" x="0.20" y="60.90" />
                <CurveTo cx="0.55" cy="53.65" x="1.50" y="47.70" />
                <CurveTo cx="7.35" cy="11.70" x="34.10" y="2.80" />
                <CurveTo cx="39.30" cy="1.05" x="45.45" y="0.45" />
                <CurveTo cx="49.65" cy="0.05" x="57.10" y="0.05" />

                <MoveTo x="54.60" y="15.15" />
                <CurveTo cx="61.40" cy="13.95" x="68.65" y="15.15" />
                <CurveTo cx="69.70" cy="15.30" x="70.35" y="15.95" />
                <CurveTo cx="71.10" cy="16.65" x="71.10" y="17.60" />
                <CurveTo cx="71.10" cy="18.65" x="70.35" y="19.45" />
                <CurveTo cx="69.60" cy="20.25" x="68.65" y="20.05" />
                <CurveTo cx="61.65" cy="18.50" x="54.60" y="20.05" />
                <CurveTo cx="53.65" cy="20.25" x="52.90" y="19.45" />
                <CurveTo cx="52.15" cy="18.65" x="52.15" y="17.60" />
                <CurveTo cx="52.15" cy="16.65" x="52.90" y="15.95" />
                <CurveTo cx="53.55" cy="15.30" x="54.60" y="15.15" />

                <MoveTo x="23.45" y="56.30" />
                <LineTo x="100.30" y="56.30" />
                <CurveTo cx="103.45" cy="56.30" x="104.35" y="60.35" />
                <CurveTo cx="110.35" cy="87.30" x="104.35" y="113.50" />
                <CurveTo cx="103.40" cy="117.60" x="100.30" y="117.60" />
                <LineTo x="23.45" y="117.60" />
                <CurveTo cx="20.00" cy="117.60" x="19.35" y="113.50" />
                <CurveTo cx="15.15" cy="86.00" x="19.35" y="60.35" />
                <CurveTo cx="20.10" cy="56.30" x="23.45" y="56.30" />

                <MoveTo x="36.35" y="148.60" />
                <CurveTo cx="51.30" cy="145.45" x="64.75" y="145.45" />
                <CurveTo cx="77.40" cy="145.45" x="89.15" y="148.15" />
                <CurveTo cx="93.55" cy="149.20" x="93.05" y="152.20" />
                <CurveTo cx="92.75" cy="154.15" x="89.85" y="158.05" />
                <CurveTo cx="84.45" cy="165.40" x="75.55" y="170.60" />
                <CurveTo cx="69.85" cy="173.95" x="63.70" y="174.00" />
                <CurveTo cx="56.45" cy="174.00" x="50.40" y="170.25" />
                <CurveTo cx="45.25" cy="167.05" x="41.65" y="163.35" />
                <CurveTo cx="38.80" cy="160.45" x="35.15" y="155.35" />
                <CurveTo cx="33.40" cy="152.80" x="33.40" y="151.30" />
                <CurveTo cx="33.40" cy="149.25" x="36.35" y="148.60" />

                <MoveTo x="94.35" y="178.50" />
                <CurveTo cx="98.10" cy="178.45" x="102.55" y="175.75" />
                <CurveTo cx="106.55" cy="173.25" x="110.40" y="169.05" />
                <CurveTo cx="111.50" cy="167.15" x="111.85" y="164.70" />
                <CurveTo cx="112.00" cy="163.20" x="111.95" y="160.10" />
                <LineTo x="111.95" y="158.50" />
                <CurveTo cx="111.15" cy="156.10" x="109.05" y="155.15" />
                <CurveTo cx="107.35" cy="154.30" x="106.50" y="154.85" />
                <CurveTo cx="105.95" cy="155.15" x="105.15" y="156.80" />
                <LineTo x="105.10" y="156.90" />
                <CurveTo cx="103.25" cy="160.50" x="102.25" y="162.25" />
                <CurveTo cx="100.60" cy="165.15" x="99.05" y="167.30" />
                <CurveTo cx="98.30" cy="168.85" x="94.50" y="172.50" />
                <CurveTo cx="91.20" cy="175.65" x="90.65" y="176.80" />
                <CurveTo cx="90.60" cy="178.50" x="93.30" y="178.50" />
                <CurveTo cx="94.00" cy="178.50" x="94.35" y="178.50" />

                <MoveTo x="32.30" y="178.90" />
                <CurveTo cx="27.65" cy="179.10" x="22.70" y="175.70" />
                <CurveTo cx="18.15" cy="172.55" x="15.75" y="168.25" />
                <CurveTo cx="14.70" cy="165.85" x="14.85" y="160.75" />
                <LineTo x="14.85" y="159.05" />
                <CurveTo cx="15.65" cy="156.70" x="17.70" y="155.65" />
                <CurveTo cx="19.30" cy="154.85" x="20.15" y="155.30" />
                <CurveTo cx="20.70" cy="155.60" x="21.50" y="157.15" />
                <LineTo x="21.65" y="157.40" />
                <LineTo x="21.70" y="157.55" />
                <CurveTo cx="24.90" cy="163.75" x="26.80" y="166.60" />
                <CurveTo cx="30.00" cy="171.35" x="33.80" y="174.10" />
                <CurveTo cx="34.15" cy="174.35" x="34.75" y="174.75" />
                <CurveTo cx="36.00" cy="175.55" x="36.00" y="177.20" />
                <CurveTo cx="36.00" cy="178.90" x="33.35" y="178.90" />
                <CurveTo cx="32.65" cy="178.90" x="32.30" y="178.90" />

                <MoveTo x="48.55" y="187.90" />
                <CurveTo cx="63.60" cy="186.45" x="79.10" y="187.90" />
                <LineTo x="78.25" y="199.45" />
                <CurveTo cx="64.40" cy="200.95" x="49.40" y="199.45" />
                <LineTo x="48.55" y="187.90" />

                <MoveTo x="51.05" y="208.10" />
                <CurveTo cx="64.20" cy="206.65" x="77.75" y="208.10" />
                <LineTo x="77.00" y="219.30" />
                <CurveTo cx="64.90" cy="220.75" x="51.80" y="219.30" />
                <LineTo x="51.05" y="208.10" />

                <MoveTo x="53.20" y="227.40" />
                <CurveTo cx="64.55" cy="225.85" x="76.25" y="227.40" />
                <LineTo x="75.60" y="239.05" />
                <CurveTo cx="65.45" cy="240.55" x="54.25" y="239.15" />
                <LineTo x="53.20" y="227.40" />

                <MoveTo x="55.65" y="246.75" />
                <CurveTo cx="64.65" cy="245.25" x="74.40" y="246.75" />
                <LineTo x="73.85" y="258.25" />
                <CurveTo cx="65.40" cy="259.75" x="56.50" y="258.30" />
                <LineTo x="55.65" y="246.75" />

                <MoveTo x="82.95" y="257.15" />
                <CurveTo cx="91.75" cy="256.95" x="94.90" y="254.15" />
                <CurveTo cx="97.30" cy="252.05" x="95.05" y="250.05" />
                <CurveTo cx="93.25" cy="248.50" x="89.60" y="247.65" />
                <CurveTo cx="86.55" cy="246.95" x="82.25" y="246.80" />
                <CurveTo cx="80.80" cy="246.70" x="80.45" y="247.00" />
                <CurveTo cx="80.20" cy="247.25" x="80.25" y="248.30" />
                <CurveTo cx="80.30" cy="248.95" x="80.30" y="249.30" />
                <CurveTo cx="80.30" cy="250.20" x="80.40" y="252.05" />
                <CurveTo cx="80.45" cy="253.85" x="80.45" y="254.75" />
                <LineTo x="80.45" y="255.65" />
                <CurveTo cx="80.40" cy="256.80" x="80.70" y="257.00" />
                <CurveTo cx="81.05" cy="257.20" x="82.95" y="257.15" />

                <MoveTo x="46.15" y="257.40" />
                <CurveTo cx="37.40" cy="257.20" x="34.25" y="254.40" />
                <CurveTo cx="31.85" cy="252.30" x="34.10" y="250.30" />
                <CurveTo cx="35.85" cy="248.70" x="39.55" y="247.85" />
                <CurveTo cx="42.50" cy="247.15" x="46.85" y="246.95" />
                <CurveTo cx="48.30" cy="246.90" x="48.65" y="247.20" />
                <CurveTo cx="48.90" cy="247.45" x="48.85" y="248.55" />
                <CurveTo cx="48.80" cy="249.15" x="48.80" y="249.55" />
                <LineTo x="48.80" y="250.90" />
                <LineTo x="48.75" y="252.25" />
                <LineTo x="48.70" y="253.60" />
                <LineTo x="48.70" y="254.95" />
                <LineTo x="48.70" y="255.80" />
                <CurveTo cx="48.75" cy="257.05" x="48.45" y="257.25" />
                <CurveTo cx="48.10" cy="257.45" x="46.15" y="257.40" />

                <MoveTo x="85.50" y="237.30" />
                <CurveTo cx="97.25" cy="237.05" x="101.35" y="234.40" />
                <CurveTo cx="104.55" cy="232.35" x="101.55" y="230.40" />
                <CurveTo cx="99.20" cy="228.85" x="94.35" y="228.05" />
                <CurveTo cx="90.30" cy="227.35" x="84.60" y="227.20" />
                <CurveTo cx="82.65" cy="227.15" x="82.20" y="227.45" />
                <CurveTo cx="81.90" cy="227.70" x="81.95" y="228.75" />
                <CurveTo cx="82.00" cy="229.35" x="82.00" y="229.70" />
                <CurveTo cx="82.00" cy="230.60" x="82.10" y="232.35" />
                <CurveTo cx="82.15" cy="234.10" x="82.15" y="234.95" />
                <LineTo x="82.15" y="235.80" />
                <CurveTo cx="82.10" cy="236.95" x="82.50" y="237.15" />
                <CurveTo cx="82.95" cy="237.35" x="85.50" y="237.30" />

                <MoveTo x="42.70" y="238.50" />
                <CurveTo cx="31.40" cy="238.30" x="27.40" y="235.40" />
                <CurveTo cx="24.30" cy="233.15" x="27.20" y="231.10" />
                <CurveTo cx="29.50" cy="229.45" x="34.20" y="228.55" />
                <CurveTo cx="37.95" cy="227.85" x="43.55" y="227.65" />
                <CurveTo cx="45.45" cy="227.60" x="45.85" y="227.95" />
                <CurveTo cx="46.20" cy="228.20" x="46.15" y="229.40" />
                <CurveTo cx="46.10" cy="230.00" x="46.10" y="230.35" />
                <LineTo x="46.05" y="232.20" />
                <CurveTo cx="45.90" cy="234.70" x="45.95" y="236.00" />
                <LineTo x="45.95" y="236.90" />
                <CurveTo cx="46.00" cy="238.15" x="45.60" y="238.35" />
                <CurveTo cx="45.15" cy="238.55" x="42.70" y="238.50" />

                <MoveTo x="87.70" y="217.50" />
                <CurveTo cx="100.40" cy="217.25" x="104.80" y="214.60" />
                <CurveTo cx="108.30" cy="212.45" x="105.05" y="210.55" />
                <CurveTo cx="102.45" cy="209.00" x="97.25" y="208.15" />
                <CurveTo cx="92.85" cy="207.45" x="86.70" y="207.30" />
                <CurveTo cx="84.60" cy="207.25" x="84.15" y="207.55" />
                <CurveTo cx="83.75" cy="207.80" x="83.85" y="208.85" />
                <CurveTo cx="83.90" cy="209.45" x="83.90" y="209.80" />
                <CurveTo cx="83.90" cy="210.70" x="84.00" y="212.50" />
                <CurveTo cx="84.05" cy="214.25" x="84.05" y="215.15" />
                <LineTo x="84.05" y="216.00" />
                <CurveTo cx="83.95" cy="217.15" x="84.45" y="217.35" />
                <CurveTo cx="84.90" cy="217.55" x="87.70" y="217.50" />

                <MoveTo x="40.60" y="218.50" />
                <CurveTo cx="28.70" cy="218.25" x="24.55" y="215.60" />
                <CurveTo cx="22.75" cy="214.40" x="22.70" y="213.55" />
                <CurveTo cx="22.65" cy="212.65" x="24.35" y="211.55" />
                <CurveTo cx="26.75" cy="210.00" x="31.65" y="209.20" />
                <CurveTo cx="35.75" cy="208.50" x="41.50" y="208.35" />
                <CurveTo cx="43.50" cy="208.30" x="43.90" y="208.60" />
                <CurveTo cx="44.25" cy="208.85" x="44.20" y="209.95" />
                <CurveTo cx="44.15" cy="210.50" x="44.15" y="210.85" />
                <CurveTo cx="44.15" cy="211.75" x="44.10" y="213.50" />
                <CurveTo cx="44.00" cy="215.30" x="44.00" y="216.15" />
                <LineTo x="44.00" y="217.00" />
                <CurveTo cx="44.05" cy="218.15" x="43.65" y="218.35" />
                <CurveTo cx="43.20" cy="218.55" x="40.60" y="218.50" />

                <MoveTo x="38.40" y="198.65" />
                <CurveTo cx="25.90" cy="198.45" x="21.45" y="195.70" />
                <CurveTo cx="18.00" cy="193.55" x="21.20" y="191.65" />
                <CurveTo cx="23.75" cy="190.10" x="28.95" y="189.25" />
                <CurveTo cx="33.25" cy="188.55" x="39.35" y="188.40" />
                <CurveTo cx="41.45" cy="188.35" x="41.90" y="188.65" />
                <CurveTo cx="42.25" cy="188.90" x="42.20" y="189.95" />
                <CurveTo cx="42.15" cy="190.55" x="42.15" y="190.90" />
                <CurveTo cx="42.15" cy="191.70" x="42.05" y="193.30" />
                <CurveTo cx="41.95" cy="195.30" x="42.00" y="196.25" />
                <LineTo x="42.00" y="197.10" />
                <CurveTo cx="42.05" cy="198.30" x="41.60" y="198.45" />
                <CurveTo cx="41.15" cy="198.70" x="38.40" y="198.65" />

                <MoveTo x="88.75" y="197.90" />
                <CurveTo cx="101.30" cy="197.70" x="105.80" y="195.00" />
                <CurveTo cx="109.25" cy="193.00" x="106.05" y="191.05" />
                <CurveTo cx="103.45" cy="189.50" x="98.25" y="188.70" />
                <CurveTo cx="93.90" cy="188.00" x="87.80" y="187.85" />
                <CurveTo cx="85.70" cy="187.80" x="85.25" y="188.10" />
                <CurveTo cx="84.85" cy="188.35" x="84.95" y="189.40" />
                <CurveTo cx="85.00" cy="190.00" x="85.00" y="190.35" />
                <CurveTo cx="85.00" cy="191.20" x="85.05" y="192.90" />
                <CurveTo cx="85.15" cy="194.65" x="85.15" y="195.55" />
                <LineTo x="85.10" y="196.40" />
                <CurveTo cx="85.05" cy="197.55" x="85.50" y="197.75" />
                <CurveTo cx="86.00" cy="197.95" x="88.75" y="197.90" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (62, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 62
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="114.45" y="260.75" />
                <CurveTo cx="111.85" cy="270.85" x="106.55" y="274.15" />
                <CurveTo cx="102.25" cy="276.80" x="88.75" y="278.40" />
                <CurveTo cx="76.70" cy="279.80" x="62.65" y="279.75" />
                <CurveTo cx="50.00" cy="279.65" x="34.05" y="278.40" />
                <LineTo x="33.25" y="278.35" />
                <CurveTo cx="28.75" cy="277.95" x="26.70" y="277.50" />
                <CurveTo cx="23.40" cy="276.80" x="20.40" y="275.10" />
                <CurveTo cx="14.65" cy="271.80" x="12.30" y="269.00" />
                <CurveTo cx="9.60" cy="265.80" x="8.25" y="259.70" />
                <CurveTo cx="8.10" cy="259.10" x="7.85" y="258.20" />
                <CurveTo cx="7.20" cy="255.70" x="10.50" y="256.00" />
                <LineTo x="18.25" y="256.80" />
                <LineTo x="26.00" y="257.60" />
                <CurveTo cx="28.75" cy="257.90" x="31.95" y="256.50" />
                <LineTo x="32.10" y="256.45" />
                <CurveTo cx="65.00" cy="242.35" x="87.75" y="257.65" />
                <CurveTo cx="89.75" cy="259.00" x="92.40" y="258.80" />
                <LineTo x="102.25" y="257.95" />
                <LineTo x="111.35" y="257.20" />
                <LineTo x="112.60" y="257.10" />
                <CurveTo cx="115.45" cy="256.80" x="114.65" y="260.00" />
                <LineTo x="114.45" y="260.75" />
                <MoveTo x="56.05" y="0.10" />
                <LineTo x="69.10" y="0.10" />
                <CurveTo cx="81.00" cy="0.00" x="90.65" y="3.75" />
                <CurveTo cx="115.45" cy="13.35" x="120.70" y="48.90" />
                <CurveTo cx="121.55" cy="54.45" x="121.80" y="60.30" />
                <CurveTo cx="122.00" cy="64.95" x="121.85" y="71.70" />
                <CurveTo cx="121.60" cy="86.55" x="120.95" y="104.65" />
                <CurveTo cx="120.60" cy="115.50" x="119.75" y="137.00" />
                <CurveTo cx="119.10" cy="152.15" x="118.85" y="159.70" />
                <CurveTo cx="118.35" cy="172.30" x="118.10" y="182.50" />
                <CurveTo cx="117.35" cy="207.85" x="117.45" y="228.85" />
                <CurveTo cx="117.50" cy="232.70" x="117.45" y="236.80" />
                <CurveTo cx="117.35" cy="241.90" x="117.10" y="245.55" />
                <CurveTo cx="116.85" cy="250.00" x="109.90" y="252.45" />
                <CurveTo cx="104.20" cy="254.40" x="92.20" y="255.45" />
                <CurveTo cx="78.30" cy="245.80" x="60.75" y="246.05" />
                <CurveTo cx="44.20" cy="246.25" x="30.25" y="255.10" />
                <LineTo x="28.30" y="254.75" />
                <CurveTo cx="16.25" cy="252.70" x="11.90" y="251.40" />
                <CurveTo cx="4.95" cy="249.35" x="3.65" y="246.05" />
                <CurveTo cx="3.30" cy="240.85" x="3.50" y="228.85" />
                <CurveTo cx="3.65" cy="220.30" x="3.55" y="216.45" />
                <CurveTo cx="3.15" cy="203.30" x="2.30" y="177.00" />
                <CurveTo cx="1.70" cy="160.10" x="1.45" y="151.65" />
                <CurveTo cx="1.00" cy="137.55" x="0.70" y="126.30" />
                <CurveTo cx="0.00" cy="98.10" x="0.00" y="75.55" />
                <CurveTo cx="0.00" cy="66.55" x="0.20" y="62.10" />
                <CurveTo cx="0.50" cy="54.65" x="1.45" y="48.60" />
                <CurveTo cx="7.15" cy="12.05" x="33.45" y="2.90" />
                <CurveTo cx="38.60" cy="1.10" x="44.65" y="0.50" />
                <CurveTo cx="48.75" cy="0.10" x="56.05" y="0.10" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (63, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 63
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="45.90" y="2.40" />
                <CurveTo cx="55.20" cy="0.30" x="65.65" y="0.15" />
                <CurveTo cx="76.55" cy="0.00" x="85.40" y="2.00" />
                <CurveTo cx="101.15" cy="5.55" x="112.05" y="17.20" />
                <CurveTo cx="122.95" cy="28.85" x="125.80" y="45.25" />
                <CurveTo cx="131.10" cy="75.60" x="130.75" y="109.85" />
                <CurveTo cx="130.40" cy="142.20" x="125.15" y="174.35" />
                <CurveTo cx="124.05" cy="181.00" x="123.90" y="188.75" />
                <LineTo x="122.10" y="267.80" />
                <CurveTo cx="121.70" cy="284.55" x="110.40" y="296.30" />
                <CurveTo cx="99.00" cy="308.15" x="82.95" y="308.15" />
                <LineTo x="46.75" y="308.15" />
                <CurveTo cx="30.65" cy="308.15" x="19.20" y="296.30" />
                <CurveTo cx="7.80" cy="284.50" x="7.60" y="267.80" />
                <LineTo x="6.70" y="189.95" />
                <CurveTo cx="6.65" cy="183.85" x="5.45" y="175.35" />
                <CurveTo cx="0.35" cy="140.85" x="0.20" y="110.25" />
                <CurveTo cx="0.00" cy="75.25" x="6.35" y="45.00" />
                <CurveTo cx="9.80" cy="28.45" x="19.95" y="17.35" />
                <CurveTo cx="30.40" cy="5.85" x="45.90" y="2.40" />

                <MoveTo x="27.90" y="84.90" />
                <CurveTo cx="66.45" cy="83.25" x="104.90" y="84.90" />
                <CurveTo cx="108.00" cy="85.05" x="108.35" y="88.20" />
                <CurveTo cx="111.25" cy="114.05" x="108.35" y="138.60" />
                <CurveTo cx="108.00" cy="141.75" x="104.90" y="141.85" />
                <CurveTo cx="66.45" cy="143.50" x="27.90" y="141.85" />
                <CurveTo cx="24.70" cy="141.75" x="24.45" y="138.60" />
                <CurveTo cx="23.35" cy="125.25" x="23.30" y="113.65" />
                <CurveTo cx="23.20" cy="100.40" x="24.45" y="88.20" />
                <CurveTo cx="24.80" cy="85.05" x="27.90" y="84.90" />

                <MoveTo x="61.95" y="27.20" />
                <CurveTo cx="67.35" cy="26.35" x="72.70" y="27.20" />
                <CurveTo cx="73.65" cy="27.30" x="74.25" y="27.90" />
                <CurveTo cx="74.90" cy="28.45" x="74.90" y="29.30" />
                <CurveTo cx="74.90" cy="30.20" x="74.25" y="30.90" />
                <CurveTo cx="73.55" cy="31.60" x="72.70" y="31.45" />
                <CurveTo cx="70.35" cy="31.05" x="67.50" y="31.05" />
                <CurveTo cx="65.05" cy="31.10" x="61.95" y="31.45" />
                <CurveTo cx="61.05" cy="31.50" x="60.40" y="30.85" />
                <CurveTo cx="59.75" cy="30.20" x="59.75" y="29.30" />
                <CurveTo cx="59.75" cy="28.45" x="60.40" y="27.90" />
                <CurveTo cx="61.00" cy="27.30" x="61.95" y="27.20" />

                <MoveTo x="47.30" y="36.95" />
                <LineTo x="48.40" y="37.20" />
                <CurveTo cx="49.75" cy="37.45" x="49.35" y="38.60" />
                <LineTo x="47.95" y="42.85" />
                <CurveTo cx="47.60" cy="44.00" x="46.30" y="43.70" />
                <LineTo x="45.20" y="43.40" />
                <CurveTo cx="44.65" cy="43.30" x="44.40" y="42.85" />
                <CurveTo cx="44.10" cy="42.45" x="44.25" y="42.00" />
                <LineTo x="45.65" y="37.75" />
                <CurveTo cx="46.05" cy="36.60" x="47.30" y="36.95" />

                <MoveTo x="87.60" y="36.85" />
                <LineTo x="86.50" y="37.10" />
                <CurveTo cx="85.20" cy="37.45" x="85.55" y="38.55" />
                <LineTo x="86.95" y="42.80" />
                <CurveTo cx="87.35" cy="43.95" x="88.60" y="43.60" />
                <LineTo x="89.70" y="43.35" />
                <CurveTo cx="91.05" cy="43.10" x="90.65" y="41.95" />
                <LineTo x="89.25" y="37.65" />
                <CurveTo cx="88.90" cy="36.55" x="87.60" y="36.85" />

                <MoveTo x="65.65" y="197.10" />
                <CurveTo cx="70.80" cy="197.10" x="74.50" y="193.40" />
                <CurveTo cx="78.15" cy="189.75" x="78.15" y="184.55" />
                <CurveTo cx="78.15" cy="179.40" x="74.50" y="175.70" />
                <CurveTo cx="70.85" cy="172.05" x="65.65" y="172.05" />
                <CurveTo cx="60.50" cy="172.05" x="56.80" y="175.70" />
                <CurveTo cx="53.15" cy="179.40" x="53.15" y="184.55" />
                <CurveTo cx="53.15" cy="189.75" x="56.80" y="193.40" />
                <CurveTo cx="60.50" cy="197.10" x="65.65" y="197.10" />

                <MoveTo x="25.80" y="163.45" />
                <CurveTo cx="30.80" cy="169.25" x="37.10" y="174.00" />
                <CurveTo cx="43.55" cy="178.80" x="49.95" y="181.35" />
                <CurveTo cx="54.90" cy="169.80" x="65.70" y="169.60" />
                <CurveTo cx="76.70" cy="169.45" x="81.25" y="181.80" />
                <CurveTo cx="87.75" cy="179.85" x="94.40" y="174.55" />
                <CurveTo cx="99.85" cy="170.20" x="105.45" y="163.55" />
                <CurveTo cx="86.90" cy="156.65" x="65.70" y="156.55" />
                <CurveTo cx="44.15" cy="156.45" x="25.80" y="163.45" />

                <MoveTo x="24.70" y="173.50" />
                <CurveTo cx="30.65" cy="174.55" x="35.45" y="177.65" />
                <CurveTo cx="41.95" cy="181.75" x="41.50" y="187.45" />
                <CurveTo cx="37.40" cy="197.20" x="29.55" y="192.15" />
                <CurveTo cx="22.05" cy="187.40" x="21.45" y="178.75" />
                <CurveTo cx="21.25" cy="176.80" x="22.05" y="175.15" />
                <CurveTo cx="22.95" cy="173.20" x="24.70" y="173.50" />

                <MoveTo x="105.95" y="173.55" />
                <CurveTo cx="100.00" cy="174.60" x="95.20" y="177.70" />
                <CurveTo cx="88.70" cy="181.85" x="89.15" y="187.55" />
                <CurveTo cx="93.25" cy="197.25" x="101.10" y="192.25" />
                <CurveTo cx="108.55" cy="187.45" x="109.20" y="178.80" />
                <CurveTo cx="109.35" cy="176.80" x="108.60" y="175.20" />
                <CurveTo cx="107.70" cy="173.25" x="105.95" y="173.55" />

                <MoveTo x="83.40" y="207.10" />
                <CurveTo cx="81.00" cy="218.65" x="91.80" y="217.25" />
                <CurveTo cx="100.90" cy="216.05" x="107.70" y="210.00" />
                <CurveTo cx="109.80" cy="208.15" x="110.10" y="204.70" />
                <CurveTo cx="110.40" cy="201.65" x="109.30" y="199.70" />
                <CurveTo cx="107.20" cy="197.10" x="95.75" y="200.05" />
                <CurveTo cx="84.20" cy="203.05" x="83.40" y="207.10" />

                <MoveTo x="46.60" y="207.00" />
                <CurveTo cx="49.00" cy="218.55" x="38.25" y="217.15" />
                <CurveTo cx="29.15" cy="215.95" x="22.30" y="209.90" />
                <CurveTo cx="20.25" cy="208.05" x="19.90" y="204.60" />
                <CurveTo cx="19.60" cy="201.55" x="20.75" y="199.60" />
                <CurveTo cx="22.80" cy="197.00" x="34.25" y="199.95" />
                <CurveTo cx="45.80" cy="202.95" x="46.60" y="207.00" />

                <MoveTo x="83.35" y="228.75" />
                <CurveTo cx="80.90" cy="240.30" x="91.70" y="238.90" />
                <CurveTo cx="100.80" cy="237.70" x="107.60" y="231.65" />
                <CurveTo cx="109.70" cy="229.80" x="110.00" y="226.35" />
                <CurveTo cx="110.30" cy="223.30" x="109.20" y="221.35" />
                <CurveTo cx="107.10" cy="218.75" x="95.70" y="221.70" />
                <CurveTo cx="84.10" cy="224.70" x="83.35" y="228.75" />

                <MoveTo x="46.70" y="228.65" />
                <CurveTo cx="49.10" cy="240.20" x="38.35" y="238.80" />
                <CurveTo cx="29.25" cy="237.60" x="22.40" y="231.55" />
                <CurveTo cx="20.35" cy="229.70" x="20.00" y="226.25" />
                <CurveTo cx="19.70" cy="223.20" x="20.85" y="221.25" />
                <CurveTo cx="22.90" cy="218.65" x="34.35" y="221.60" />
                <CurveTo cx="45.90" cy="224.60" x="46.70" y="228.65" />

                <MoveTo x="83.25" y="250.50" />
                <CurveTo cx="80.85" cy="262.00" x="91.60" y="260.55" />
                <CurveTo cx="100.60" cy="259.35" x="107.50" y="253.40" />
                <CurveTo cx="109.60" cy="251.60" x="109.90" y="248.15" />
                <CurveTo cx="110.20" cy="245.05" x="109.10" y="243.10" />
                <CurveTo cx="107.00" cy="240.50" x="95.60" y="243.45" />
                <CurveTo cx="84.00" cy="246.45" x="83.25" y="250.50" />

                <MoveTo x="46.80" y="250.40" />
                <CurveTo cx="49.20" cy="261.95" x="38.45" y="260.55" />
                <CurveTo cx="29.35" cy="259.35" x="22.50" y="253.30" />
                <CurveTo cx="20.45" cy="251.45" x="20.10" y="248.00" />
                <CurveTo cx="19.80" cy="244.95" x="20.90" y="243.00" />
                <CurveTo cx="23.00" cy="240.40" x="34.45" y="243.35" />
                <CurveTo cx="46.00" cy="246.35" x="46.80" y="250.40" />

                <MoveTo x="83.10" y="272.20" />
                <CurveTo cx="81.00" cy="282.70" x="89.50" y="282.50" />
                <CurveTo cx="96.85" cy="282.35" x="102.95" y="276.70" />
                <CurveTo cx="105.00" cy="274.75" x="105.85" y="271.15" />
                <CurveTo cx="106.65" cy="267.70" x="105.70" y="265.90" />
                <CurveTo cx="103.70" cy="263.25" x="93.85" y="265.70" />
                <CurveTo cx="83.85" cy="268.15" x="83.10" y="272.20" />

                <MoveTo x="46.90" y="272.10" />
                <CurveTo cx="49.00" cy="282.60" x="40.55" y="282.40" />
                <CurveTo cx="33.15" cy="282.25" x="27.10" y="276.60" />
                <CurveTo cx="25.05" cy="274.70" x="24.15" y="271.05" />
                <CurveTo cx="23.35" cy="267.55" x="24.30" y="265.80" />
                <CurveTo cx="26.30" cy="263.15" x="36.15" y="265.60" />
                <CurveTo cx="46.15" cy="268.05" x="46.90" y="272.10" />

                <MoveTo x="57.70" y="203.60" />
                <LineTo x="72.85" y="203.60" />
                <CurveTo cx="76.55" cy="203.60" x="78.00" y="206.00" />
                <CurveTo cx="79.30" cy="208.10" x="78.40" y="211.20" />
                <CurveTo cx="77.50" cy="214.25" x="75.00" y="216.45" />
                <CurveTo cx="72.25" cy="218.80" x="68.80" y="218.80" />
                <LineTo x="61.00" y="218.80" />
                <CurveTo cx="57.85" cy="218.80" x="55.20" y="216.45" />
                <CurveTo cx="52.85" cy="214.25" x="52.00" y="211.20" />
                <CurveTo cx="51.10" cy="208.10" x="52.45" y="206.00" />
                <CurveTo cx="53.90" cy="203.60" x="57.70" y="203.60" />

                <MoveTo x="57.20" y="226.15" />
                <LineTo x="72.40" y="226.15" />
                <CurveTo cx="76.15" cy="226.15" x="77.55" y="228.50" />
                <CurveTo cx="78.80" cy="230.65" x="77.80" y="233.75" />
                <CurveTo cx="76.85" cy="236.80" x="74.35" y="238.95" />
                <CurveTo cx="71.60" cy="241.30" x="68.30" y="241.30" />
                <LineTo x="60.50" y="241.30" />
                <CurveTo cx="57.55" cy="241.30" x="54.95" y="238.95" />
                <CurveTo cx="52.55" cy="236.75" x="51.65" y="233.75" />
                <CurveTo cx="50.70" cy="230.60" x="51.95" y="228.50" />
                <CurveTo cx="53.35" cy="226.15" x="57.20" y="226.15" />

                <MoveTo x="57.30" y="248.15" />
                <LineTo x="72.20" y="248.15" />
                <CurveTo cx="75.85" cy="248.15" x="77.25" y="250.50" />
                <CurveTo cx="78.55" cy="252.55" x="77.65" y="255.65" />
                <CurveTo cx="76.75" cy="258.65" x="74.25" y="260.75" />
                <CurveTo cx="71.55" cy="263.10" x="68.20" y="263.10" />
                <LineTo x="60.50" y="263.10" />
                <CurveTo cx="57.40" cy="263.10" x="54.85" y="260.75" />
                <CurveTo cx="52.50" cy="258.65" x="51.70" y="255.65" />
                <CurveTo cx="50.85" cy="252.55" x="52.15" y="250.50" />
                <CurveTo cx="53.60" cy="248.15" x="57.30" y="248.15" />

                <MoveTo x="57.60" y="269.55" />
                <LineTo x="72.35" y="269.55" />
                <CurveTo cx="75.95" cy="269.55" x="77.35" y="271.90" />
                <CurveTo cx="78.55" cy="274.00" x="77.70" y="277.05" />
                <CurveTo cx="76.80" cy="280.10" x="74.35" y="282.20" />
                <CurveTo cx="71.70" cy="284.55" x="68.35" y="284.55" />
                <LineTo x="60.80" y="284.55" />
                <CurveTo cx="57.80" cy="284.55" x="55.25" y="282.20" />
                <CurveTo cx="52.95" cy="280.05" x="52.10" y="277.05" />
                <CurveTo cx="51.25" cy="273.95" x="52.50" y="271.90" />
                <CurveTo cx="53.90" cy="269.55" x="57.60" y="269.55" />

                <MoveTo x="48.10" y="290.60" />
                <CurveTo cx="50.70" cy="291.25" x="52.30" y="292.55" />
                <CurveTo cx="53.90" cy="293.90" x="53.60" y="295.15" />
                <CurveTo cx="53.30" cy="296.40" x="51.25" y="296.90" />
                <CurveTo cx="49.25" cy="297.35" x="46.65" y="296.75" />
                <CurveTo cx="44.05" cy="296.15" x="42.45" y="294.80" />
                <CurveTo cx="40.80" cy="293.50" x="41.10" y="292.20" />
                <CurveTo cx="41.40" cy="290.95" x="43.45" y="290.45" />
                <CurveTo cx="45.50" cy="290.00" x="48.10" y="290.60" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (64, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 64
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="42.75" y="2.50" />
                <CurveTo cx="51.35" cy="0.30" x="61.10" y="0.15" />
                <CurveTo cx="71.25" cy="0.00" x="79.50" y="2.10" />
                <CurveTo cx="94.15" cy="5.80" x="104.25" y="18.10" />
                <CurveTo cx="114.40" cy="30.40" x="117.05" y="47.70" />
                <CurveTo cx="121.95" cy="79.60" x="121.65" y="115.75" />
                <CurveTo cx="121.35" cy="149.85" x="116.45" y="183.70" />
                <CurveTo cx="115.45" cy="191.05" x="115.30" y="198.90" />
                <LineTo x="113.80" y="265.05" />
                <CurveTo cx="113.95" cy="273.60" x="105.30" y="273.20" />
                <LineTo x="14.90" y="272.95" />
                <CurveTo cx="6.30" cy="274.15" x="6.95" y="264.40" />
                <LineTo x="6.25" y="200.15" />
                <CurveTo cx="6.20" cy="193.25" x="5.05" y="184.80" />
                <CurveTo cx="0.30" cy="148.45" x="0.15" y="116.20" />
                <CurveTo cx="0.00" cy="79.30" x="5.90" y="47.45" />
                <CurveTo cx="9.15" cy="30.00" x="18.60" y="18.25" />
                <CurveTo cx="28.30" cy="6.20" x="42.75" y="2.50" />

                <MoveTo x="91.85" y="44.70" />
                <CurveTo cx="96.50" cy="44.70" x="99.80" y="41.40" />
                <CurveTo cx="103.05" cy="38.10" x="103.05" y="33.50" />
                <CurveTo cx="103.05" cy="28.85" x="99.80" y="25.60" />
                <CurveTo cx="96.50" cy="22.30" x="91.85" y="22.30" />
                <CurveTo cx="87.25" cy="22.30" x="83.95" y="25.60" />
                <CurveTo cx="80.65" cy="28.85" x="80.65" y="33.50" />
                <CurveTo cx="80.65" cy="38.10" x="83.95" y="41.40" />
                <CurveTo cx="87.25" cy="44.70" x="91.85" y="44.70" />

            </Contour>
            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (65, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 65
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face1" x="104.25" y="67.35">
            <Contour x="0" y="0">
                <MoveTo x="0.55" y="7.35" />
                <LineTo x="0.45" y="7.45" />
                <LineTo x="0.45" y="207.80" />
                <LineTo x="0.50" y="207.90" />
                <LineTo x="0.55" y="207.95" />
                <LineTo x="0.80" y="208.15" />
                <LineTo x="3.45" y="209.70" />
                <LineTo x="8.90" y="211.85" />
                <LineTo x="16.05" y="213.80" />
                <LineTo x="39.00" y="217.05" />
                <LineTo x="72.60" y="218.05" />
                <LineTo x="88.20" y="217.35" />
                <LineTo x="99.65" y="216.35" />
                <LineTo x="114.90" y="213.80" />
                <LineTo x="123.65" y="211.15" />
                <LineTo x="126.95" y="209.70" />
                <LineTo x="129.85" y="207.95" />
                <LineTo x="129.90" y="207.90" />
                <LineTo x="129.90" y="7.45" />
                <LineTo x="129.85" y="7.35" />
                <LineTo x="129.05" y="7.15" />
                <LineTo x="127.80" y="6.70" />
                <LineTo x="121.70" y="5.20" />
                <LineTo x="90.85" y="0.25" />
                <LineTo x="90.10" y="0.10" />
                <LineTo x="88.25" y="0.00" />
                <LineTo x="88.25" y="0.05" />
                <LineTo x="88.35" y="0.35" />
                <LineTo x="88.45" y="1.25" />
                <LineTo x="90.95" y="15.90" />
                <LineTo x="91.15" y="16.60" />
                <LineTo x="92.00" y="21.35" />
                <LineTo x="91.95" y="22.50" />
                <LineTo x="91.75" y="23.55" />
                <LineTo x="91.35" y="24.55" />
                <LineTo x="90.60" y="25.30" />
                <LineTo x="89.60" y="25.95" />
                <LineTo x="88.15" y="26.45" />
                <LineTo x="86.35" y="26.65" />
                <LineTo x="46.05" y="26.65" />
                <LineTo x="44.25" y="26.45" />
                <LineTo x="42.80" y="25.95" />
                <LineTo x="41.75" y="25.30" />
                <LineTo x="41.05" y="24.55" />
                <LineTo x="40.60" y="23.55" />
                <LineTo x="40.45" y="22.50" />
                <LineTo x="40.50" y="20.20" />
                <LineTo x="43.95" y="1.25" />
                <LineTo x="44.00" y="0.70" />
                <LineTo x="44.10" y="0.35" />
                <LineTo x="44.10" y="0.00" />
                <LineTo x="40.70" y="0.30" />
                <LineTo x="13.55" y="4.30" />
                <LineTo x="1.95" y="6.90" />
                <LineTo x="1.35" y="7.15" />
                <LineTo x="0.55" y="7.35" />
                <MoveTo x="17.15" y="134.80" />
                <LineTo x="17.65" y="134.95" />
                <LineTo x="25.80" y="136.20" />
                <LineTo x="53.10" y="138.75" />
                <LineTo x="65.90" y="139.20" />
                <LineTo x="74.10" y="139.00" />
                <LineTo x="107.55" y="135.95" />
                <LineTo x="114.10" y="134.95" />
                <LineTo x="114.60" y="134.80" />
                <LineTo x="115.15" y="134.75" />
                <LineTo x="117.95" y="133.85" />
                <LineTo x="118.80" y="133.45" />
                <LineTo x="120.10" y="132.60" />
                <LineTo x="121.20" y="131.65" />
                <LineTo x="122.05" y="130.55" />
                <LineTo x="122.65" y="129.45" />
                <LineTo x="123.30" y="127.20" />
                <LineTo x="123.50" y="126.05" />
                <LineTo x="123.55" y="56.30" />
                <LineTo x="123.45" y="55.55" />
                <LineTo x="122.95" y="53.45" />
                <LineTo x="122.40" y="52.20" />
                <LineTo x="120.90" y="50.10" />
                <LineTo x="120.00" y="49.25" />
                <LineTo x="117.90" y="47.95" />
                <LineTo x="115.60" y="47.10" />
                <LineTo x="114.35" y="46.85" />
                <LineTo x="17.35" y="46.85" />
                <LineTo x="14.95" y="47.45" />
                <LineTo x="12.75" y="48.55" />
                <LineTo x="11.70" y="49.25" />
                <LineTo x="10.80" y="50.10" />
                <LineTo x="9.35" y="52.20" />
                <LineTo x="8.60" y="54.10" />
                <LineTo x="8.30" y="55.55" />
                <LineTo x="8.25" y="56.30" />
                <LineTo x="8.25" y="122.50" />
                <LineTo x="8.15" y="123.00" />
                <LineTo x="8.40" y="127.20" />
                <LineTo x="9.10" y="129.45" />
                <LineTo x="9.75" y="130.55" />
                <LineTo x="10.55" y="131.65" />
                <LineTo x="12.25" y="133.00" />
                <LineTo x="13.75" y="133.85" />
                <LineTo x="16.60" y="134.75" />
                <LineTo x="17.15" y="134.80" />
                <MoveTo x="0.00" y="258.75" />
                <LineTo x="0.00" y="223.05" />
                <LineTo x="0.05" y="223.05" />
                <LineTo x="0.10" y="223.15" />
                <LineTo x="0.50" y="223.30" />
                <LineTo x="7.45" y="226.15" />
                <LineTo x="12.00" y="227.45" />
                <LineTo x="36.65" y="231.60" />
                <LineTo x="64.80" y="232.90" />
                <LineTo x="77.55" y="232.65" />
                <LineTo x="99.95" y="230.75" />
                <LineTo x="112.35" y="228.65" />
                <LineTo x="124.20" y="225.45" />
                <LineTo x="129.50" y="223.15" />
                <LineTo x="129.60" y="223.05" />
                <LineTo x="129.60" y="258.75" />
                <LineTo x="129.55" y="258.75" />
                <LineTo x="129.45" y="258.80" />
                <LineTo x="124.40" y="260.45" />
                <LineTo x="110.00" y="263.75" />
                <LineTo x="76.40" y="267.90" />
                <LineTo x="67.50" y="268.30" />
                <LineTo x="53.25" y="267.90" />
                <LineTo x="19.70" y="263.75" />
                <LineTo x="0.65" y="259.00" />
                <LineTo x="0.20" y="258.80" />
                <LineTo x="0.05" y="258.75" />
                <LineTo x="0.00" y="258.75" />
                <MoveTo x="54.75" y="184.55" />
                <LineTo x="57.50" y="187.00" />
                <LineTo x="60.80" y="188.75" />
                <LineTo x="64.45" y="189.65" />
                <LineTo x="68.15" y="189.65" />
                <LineTo x="71.85" y="188.75" />
                <CurveTo cx="73.50" cy="187.85" x="75.10" y="187.00" />
                <LineTo x="77.90" y="184.55" />
                <LineTo x="79.10" y="183.05" />
                <LineTo x="80.05" y="181.50" />
                <LineTo x="81.35" y="178.00" />
                <LineTo x="81.80" y="174.30" />
                <LineTo x="81.35" y="170.55" />
                <LineTo x="80.05" y="167.10" />
                <LineTo x="79.10" y="165.45" />
                <LineTo x="77.90" y="163.95" />
                <LineTo x="75.10" y="161.50" />
                <LineTo x="71.85" y="159.75" />
                <LineTo x="68.15" y="158.85" />
                <LineTo x="64.45" y="158.85" />
                <LineTo x="60.80" y="159.75" />
                <LineTo x="57.50" y="161.50" />
                <LineTo x="56.05" y="162.65" />
                <LineTo x="53.55" y="165.45" />
                <LineTo x="51.80" y="168.75" />
                <LineTo x="51.25" y="170.55" />
                <LineTo x="50.85" y="174.30" />
                <LineTo x="51.25" y="178.00" />
                <LineTo x="52.55" y="181.50" />
                <LineTo x="54.75" y="184.55" />
                <MoveTo x="52.20" y="200.05" />
                <LineTo x="52.15" y="200.55" />
                <LineTo x="52.20" y="201.60" />
                <LineTo x="52.50" y="202.55" />
                <LineTo x="52.95" y="203.40" />
                <LineTo x="53.55" y="204.10" />
                <LineTo x="54.25" y="204.60" />
                <LineTo x="54.65" y="204.75" />
                <LineTo x="54.90" y="204.80" />
                <LineTo x="55.05" y="204.90" />
                <LineTo x="55.20" y="204.90" />
                <LineTo x="55.85" y="205.05" />
                <LineTo x="56.00" y="205.05" />
                <LineTo x="56.05" y="205.15" />
                <LineTo x="56.10" y="205.15" />
                <LineTo x="56.30" y="205.20" />
                <LineTo x="61.60" y="206.15" />
                <LineTo x="66.50" y="206.30" />
                <LineTo x="74.95" y="205.60" />
                <LineTo x="78.00" y="204.90" />
                <LineTo x="78.25" y="204.75" />
                <LineTo x="78.30" y="204.75" />
                <LineTo x="78.95" y="204.45" />
                <LineTo x="79.35" y="204.15" />
                <LineTo x="79.95" y="203.50" />
                <LineTo x="80.60" y="202.20" />
                <LineTo x="80.75" y="201.25" />
                <LineTo x="80.75" y="200.20" />
                <LineTo x="80.55" y="199.20" />
                <LineTo x="80.35" y="198.70" />
                <LineTo x="79.80" y="197.85" />
                <LineTo x="79.15" y="197.25" />
                <LineTo x="78.45" y="196.80" />
                <LineTo x="78.00" y="196.65" />
                <LineTo x="77.60" y="196.60" />
                <LineTo x="77.15" y="196.60" />
                <LineTo x="76.75" y="196.65" />
                <LineTo x="76.50" y="196.75" />
                <LineTo x="76.40" y="196.75" />
                <LineTo x="75.35" y="197.05" />
                <LineTo x="71.85" y="197.70" />
                <LineTo x="63.35" y="197.95" />
                <LineTo x="57.55" y="196.95" />
                <LineTo x="57.45" y="196.90" />
                <LineTo x="57.25" y="196.90" />
                <LineTo x="56.45" y="196.65" />
                <LineTo x="56.30" y="196.65" />
                <LineTo x="56.15" y="196.60" />
                <LineTo x="55.50" y="196.60" />
                <LineTo x="55.10" y="196.65" />
                <LineTo x="54.25" y="196.95" />
                <LineTo x="53.90" y="197.15" />
                <LineTo x="53.25" y="197.75" />
                <LineTo x="52.70" y="198.55" />
                <LineTo x="52.20" y="200.05" />
            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Front"/>
                    <Culture key="en" value="Front"/>
                    <Culture key="ru" value="&#x0424;&#x0430;&#x0441;&#x0430;&#x0434;"/>
                </Caption>
            </Localization>            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (66, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 66
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face2" x="378.25" y="67.50">
            <Contour x="0" y="0">
                <MoveTo x="0.00" y="0.00" />
                <LineTo x="0.05" y="102.55" />
                <LineTo x="1.75" y="112.35" />
                <LineTo x="3.05" y="116.00" />
                <LineTo x="7.65" y="124.75" />
                <LineTo x="8.55" y="128.05" />
                <LineTo x="8.80" y="129.55" />
                <LineTo x="9.00" y="134.25" />
                <LineTo x="8.90" y="135.40" />
                <LineTo x="8.90" y="135.55" />
                <LineTo x="8.95" y="135.60" />
                <LineTo x="10.15" y="136.05" />
                <LineTo x="28.50" y="140.05" />
                <LineTo x="39.40" y="141.70" />
                <LineTo x="63.85" y="143.80" />
                <LineTo x="70.15" y="143.90" />
                <LineTo x="89.00" y="143.00" />
                <LineTo x="118.90" y="138.75" />
                <LineTo x="131.00" y="135.75" />
                <LineTo x="131.35" y="135.60" />
                <LineTo x="131.40" y="135.55" />
                <LineTo x="131.40" y="135.40" />
                <LineTo x="131.30" y="134.25" />
                <LineTo x="131.35" y="131.00" />
                <LineTo x="132.20" y="126.45" />
                <LineTo x="133.40" y="123.10" />
                <LineTo x="137.25" y="116.00" />
                <LineTo x="139.45" y="108.85" />
                <LineTo x="140.25" y="102.55" />
                <LineTo x="140.30" y="0.00" />
                <LineTo x="118.00" y="4.10" />
                <LineTo x="88.20" y="6.95" />
                <LineTo x="79.40" y="7.35" />
                <LineTo x="39.80" y="6.10" />
                <LineTo x="28.90" y="4.95" />
                <LineTo x="0.30" y="0.10" />
                <LineTo x="0.05" y="0.00" />
                <LineTo x="0.00" y="0.00" />
                <MoveTo x="53.70" y="68.55" />
                <LineTo x="54.45" y="70.30" />
                <LineTo x="56.55" y="73.60" />
                <LineTo x="59.30" y="76.35" />
                <LineTo x="62.55" y="78.40" />
                <LineTo x="66.20" y="79.70" />
                <LineTo x="70.10" y="80.10" />
                <LineTo x="73.95" y="79.70" />
                <LineTo x="77.60" y="78.40" />
                <LineTo x="80.90" y="76.35" />
                <LineTo x="83.65" y="73.60" />
                <LineTo x="85.70" y="70.30" />
                <LineTo x="86.40" y="68.55" />
                <LineTo x="87.30" y="64.70" />
                <LineTo x="87.45" y="62.80" />
                <LineTo x="87.30" y="60.85" />
                <LineTo x="86.40" y="57.10" />
                <LineTo x="85.70" y="55.30" />
                <LineTo x="83.65" y="51.95" />
                <LineTo x="80.90" y="49.20" />
                <LineTo x="77.60" y="47.20" />
                <LineTo x="75.80" y="46.40" />
                <LineTo x="73.95" y="45.85" />
                <LineTo x="70.10" y="45.45" />
                <LineTo x="66.20" y="45.85" />
                <LineTo x="64.35" y="46.40" />
                <LineTo x="62.55" y="47.20" />
                <LineTo x="59.30" y="49.20" />
                <LineTo x="57.80" y="50.55" />
                <LineTo x="55.40" y="53.55" />
                <LineTo x="53.70" y="57.10" />
                <LineTo x="53.20" y="58.95" />
                <LineTo x="52.75" y="62.80" />
                <LineTo x="52.90" y="64.70" />
                <LineTo x="53.70" y="68.55" />
                <MoveTo x="16.95" y="149.30" />
                <LineTo x="26.60" y="151.30" />
                <LineTo x="37.95" y="152.95" />
                <LineTo x="65.30" y="154.75" />
                <LineTo x="98.70" y="153.40" />
                <LineTo x="114.20" y="151.30" />
                <LineTo x="126.70" y="148.60" />
                <LineTo x="131.70" y="147.15" />
                <LineTo x="131.55" y="249.40" />
                <LineTo x="116.00" y="253.40" />
                <LineTo x="92.10" y="256.40" />
                <LineTo x="70.40" y="257.10" />
                <LineTo x="39.30" y="255.55" />
                <LineTo x="21.30" y="252.70" />
                <LineTo x="8.90" y="249.25" />
                <LineTo x="9.05" y="147.15" />
                <LineTo x="16.95" y="149.30" />
                <MoveTo x="49.95" y="227.10" />
                <LineTo x="90.90" y="227.10" />
                <LineTo x="91.80" y="227.20" />
                <LineTo x="92.65" y="227.45" />
                <LineTo x="93.45" y="227.90" />
                <LineTo x="94.10" y="228.45" />
                <LineTo x="94.65" y="229.10" />
                <LineTo x="95.05" y="229.90" />
                <LineTo x="95.30" y="230.70" />
                <LineTo x="95.40" y="231.60" />
                <LineTo x="95.40" y="244.30" />
                <LineTo x="95.30" y="245.20" />
                <LineTo x="95.05" y="246.00" />
                <LineTo x="94.65" y="246.80" />
                <LineTo x="94.10" y="247.45" />
                <LineTo x="93.45" y="248.00" />
                <LineTo x="92.65" y="248.40" />
                <LineTo x="90.90" y="248.80" />
                <LineTo x="49.95" y="248.80" />
                <LineTo x="49.00" y="248.65" />
                <LineTo x="48.15" y="248.40" />
                <LineTo x="47.40" y="248.00" />
                <LineTo x="46.75" y="247.45" />
                <LineTo x="46.15" y="246.80" />
                <LineTo x="45.75" y="246.00" />
                <LineTo x="45.50" y="245.20" />
                <LineTo x="45.50" y="230.70" />
                <LineTo x="45.75" y="229.90" />
                <LineTo x="46.15" y="229.10" />
                <LineTo x="46.75" y="228.45" />
                <LineTo x="47.40" y="227.90" />
                <LineTo x="48.15" y="227.45" />
                <LineTo x="49.00" y="227.20" />
                <LineTo x="49.95" y="227.10" />
            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Back"/>
                    <Culture key="en" value="Back"/>
                    <Culture key="ru" value="&#x041E;&#x0431;&#x043E;&#x0440;&#x043E;&#x0442;"/>
                </Caption>
            </Localization>            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (67, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 67
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face1" x="89.25" y="72.75">
            <Contour x="0" y="0">
                <MoveTo x="137.40" y="51.50" />
                <LineTo x="124.55" y="45.35" />
                <LineTo x="117.60" y="42.95" />
                <LineTo x="115.80" y="42.50" />
                <LineTo x="115.40" y="42.50" />
                <LineTo x="115.40" y="1.95" />
                <LineTo x="115.30" y="1.80" />
                <LineTo x="115.20" y="1.80" />
                <LineTo x="114.60" y="1.55" />
                <LineTo x="114.10" y="1.45" />
                <LineTo x="113.85" y="1.45" />
                <LineTo x="113.50" y="1.30" />
                <LineTo x="113.25" y="1.30" />
                <LineTo x="112.80" y="1.20" />
                <LineTo x="112.40" y="1.20" />
                <LineTo x="111.95" y="1.10" />
                <LineTo x="111.35" y="1.10" />
                <LineTo x="110.85" y="0.95" />
                <LineTo x="110.15" y="0.95" />
                <LineTo x="109.55" y="0.85" />
                <LineTo x="108.80" y="0.85" />
                <LineTo x="108.00" y="0.75" />
                <LineTo x="107.15" y="0.75" />
                <LineTo x="106.20" y="0.60" />
                <LineTo x="103.05" y="0.50" />
                <LineTo x="101.85" y="0.35" />
                <LineTo x="87.35" y="0.00" />
                <LineTo x="50.40" y="0.35" />
                <LineTo x="49.20" y="0.50" />
                <LineTo x="46.05" y="0.60" />
                <LineTo x="45.10" y="0.75" />
                <LineTo x="42.70" y="0.85" />
                <LineTo x="42.00" y="0.95" />
                <LineTo x="41.40" y="0.95" />
                <LineTo x="40.80" y="1.10" />
                <LineTo x="40.30" y="1.10" />
                <LineTo x="39.80" y="1.20" />
                <LineTo x="39.35" y="1.20" />
                <LineTo x="39.00" y="1.30" />
                <LineTo x="38.60" y="1.30" />
                <LineTo x="38.40" y="1.45" />
                <LineTo x="38.00" y="1.45" />
                <LineTo x="37.65" y="1.55" />
                <LineTo x="37.05" y="1.80" />
                <LineTo x="36.95" y="1.80" />
                <LineTo x="36.80" y="1.95" />
                <LineTo x="36.80" y="42.50" />
                <LineTo x="36.35" y="42.50" />
                <LineTo x="27.60" y="45.35" />
                <LineTo x="18.35" y="49.55" />
                <LineTo x="1.05" y="59.65" />
                <LineTo x="0.95" y="59.75" />
                <LineTo x="0.00" y="115.30" />
                <LineTo x="2.40" y="168.50" />
                <LineTo x="3.60" y="179.75" />
                <LineTo x="3.60" y="180.95" />
                <LineTo x="3.70" y="181.70" />
                <LineTo x="4.40" y="184.45" />
                <LineTo x="4.80" y="185.05" />
                <LineTo x="5.00" y="185.65" />
                <LineTo x="5.50" y="186.25" />
                <LineTo x="5.85" y="186.85" />
                <LineTo x="7.30" y="188.30" />
                <LineTo x="8.40" y="189.15" />
                <LineTo x="9.70" y="189.85" />
                <LineTo x="12.35" y="190.70" />
                <LineTo x="13.05" y="190.70" />
                <LineTo x="13.90" y="190.80" />
                <LineTo x="26.60" y="190.80" />
                <LineTo x="27.80" y="191.05" />
                <LineTo x="28.40" y="191.30" />
                <LineTo x="30.00" y="192.35" />
                <LineTo x="30.70" y="193.45" />
                <LineTo x="31.20" y="194.65" />
                <LineTo x="31.30" y="195.25" />
                <LineTo x="31.20" y="212.65" />
                <LineTo x="32.00" y="217.70" />
                <LineTo x="33.10" y="220.55" />
                <LineTo x="35.15" y="223.95" />
                <LineTo x="39.20" y="228.25" />
                <LineTo x="42.60" y="230.30" />
                <LineTo x="42.70" y="230.40" />
                <LineTo x="42.80" y="230.40" />
                <LineTo x="42.95" y="230.55" />
                <LineTo x="42.95" y="246.60" />
                <LineTo x="43.20" y="246.60" />
                <LineTo x="53.60" y="248.65" />
                <LineTo x="62.60" y="249.75" />
                <LineTo x="82.80" y="250.20" />
                <LineTo x="89.50" y="249.75" />
                <LineTo x="103.80" y="247.70" />
                <LineTo x="108.95" y="246.60" />
                <LineTo x="109.20" y="246.60" />
                <LineTo x="109.30" y="230.40" />
                <LineTo x="109.40" y="230.40" />
                <LineTo x="109.65" y="230.30" />
                <LineTo x="113.00" y="228.25" />
                <LineTo x="115.90" y="225.50" />
                <LineTo x="117.10" y="223.95" />
                <LineTo x="119.15" y="220.55" />
                <LineTo x="120.20" y="217.70" />
                <LineTo x="120.70" y="215.65" />
                <LineTo x="121.05" y="212.65" />
                <LineTo x="121.05" y="206.15" />
                <LineTo x="120.95" y="205.20" />
                <LineTo x="120.95" y="195.25" />
                <LineTo x="121.20" y="194.05" />
                <LineTo x="121.40" y="193.45" />
                <LineTo x="122.25" y="192.35" />
                <LineTo x="122.60" y="192.00" />
                <LineTo x="123.70" y="191.30" />
                <LineTo x="124.30" y="191.05" />
                <LineTo x="125.60" y="190.80" />
                <LineTo x="138.35" y="190.80" />
                <LineTo x="139.05" y="190.70" />
                <LineTo x="139.80" y="190.70" />
                <LineTo x="140.50" y="190.45" />
                <LineTo x="141.95" y="190.10" />
                <LineTo x="142.55" y="189.85" />
                <LineTo x="144.35" y="188.75" />
                <LineTo x="146.40" y="186.85" />
                <LineTo x="148.05" y="183.75" />
                <LineTo x="148.55" y="181.70" />
                <LineTo x="148.55" y="180.95" />
                <LineTo x="148.65" y="180.60" />
                <LineTo x="148.65" y="179.75" />
                <LineTo x="149.75" y="168.50" />
                <LineTo x="152.40" y="109.55" />
                <LineTo x="152.15" y="78.85" />
                <LineTo x="151.30" y="60.00" />
                <LineTo x="151.20" y="59.75" />
                <LineTo x="150.60" y="59.30" />
                <LineTo x="137.40" y="51.50" />
                <MoveTo x="43.40" y="81.95" />
                <LineTo x="44.75" y="80.15" />
                <LineTo x="45.35" y="79.70" />
                <LineTo x="46.05" y="79.30" />
                <LineTo x="47.50" y="78.85" />
                <LineTo x="48.20" y="78.75" />
                <LineTo x="48.95" y="78.75" />
                <LineTo x="49.05" y="78.60" />
                <LineTo x="70.40" y="77.90" />
                <LineTo x="71.60" y="77.75" />
                <LineTo x="74.50" y="77.75" />
                <LineTo x="74.75" y="77.65" />
                <LineTo x="76.55" y="77.65" />
                <LineTo x="76.90" y="77.75" />
                <LineTo x="79.20" y="77.75" />
                <LineTo x="80.40" y="77.90" />
                <LineTo x="99.70" y="78.50" />
                <LineTo x="100.20" y="78.60" />
                <LineTo x="103.20" y="78.60" />
                <LineTo x="103.30" y="78.75" />
                <LineTo x="103.80" y="78.75" />
                <LineTo x="105.35" y="78.95" />
                <LineTo x="107.25" y="80.05" />
                <LineTo x="107.85" y="80.65" />
                <LineTo x="108.80" y="81.85" />
                <LineTo x="109.55" y="84.00" />
                <LineTo x="109.55" y="126.60" />
                <LineTo x="109.20" y="128.15" />
                <LineTo x="108.45" y="129.50" />
                <LineTo x="108.00" y="130.10" />
                <LineTo x="106.20" y="131.55" />
                <LineTo x="104.75" y="132.00" />
                <LineTo x="103.90" y="132.15" />
                <LineTo x="102.60" y="132.15" />
                <LineTo x="102.35" y="132.25" />
                <LineTo x="99.60" y="132.25" />
                <LineTo x="99.00" y="132.35" />
                <LineTo x="79.65" y="132.95" />
                <LineTo x="79.05" y="133.10" />
                <LineTo x="76.20" y="133.10" />
                <LineTo x="75.95" y="133.20" />
                <LineTo x="75.20" y="133.20" />
                <LineTo x="75.00" y="133.10" />
                <LineTo x="72.10" y="133.10" />
                <LineTo x="70.90" y="132.95" />
                <LineTo x="49.80" y="132.25" />
                <LineTo x="49.55" y="132.15" />
                <LineTo x="48.45" y="132.15" />
                <LineTo x="46.90" y="131.90" />
                <LineTo x="45.45" y="131.15" />
                <LineTo x="44.85" y="130.70" />
                <LineTo x="43.40" y="129.00" />
                <LineTo x="43.05" y="128.30" />
                <LineTo x="42.80" y="127.55" />
                <LineTo x="42.60" y="126.00" />
                <LineTo x="42.60" y="84.95" />
                <LineTo x="42.80" y="83.40" />
                <LineTo x="43.40" y="81.95" />
            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Front"/>
                    <Culture key="en" value="Front"/>
                    <Culture key="ru" value="&#x0424;&#x0430;&#x0441;&#x0430;&#x0434;"/>
                </Caption>
            </Localization>            
        </Face>
'

INSERT INTO dbo.Face
  (Id, Contour, Icon)
  VALUES (68, 0x00, NULL)

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 68
WRITETEXT Face.Contour @ptrContour
	'<?xml version="1.0" encoding="utf-8" ?> 
        <Face id="Face2" x="385.15" y="103.75">
            <Contour x="0" y="0">
                <MoveTo x="140.90" y="51.85" />
                <LineTo x="139.00" y="51.75" />
                <LineTo x="135.85" y="50.65" />
                <LineTo x="133.20" y="48.75" />
                <LineTo x="131.05" y="46.20" />
                <LineTo x="129.85" y="44.20" />
                <LineTo x="127.20" y="37.20" />
                <LineTo x="127.00" y="36.00" />
                <LineTo x="126.60" y="34.70" />
                <LineTo x="125.30" y="24.60" />
                <LineTo x="125.20" y="20.30" />
                <LineTo x="125.05" y="19.35" />
                <LineTo x="125.05" y="14.30" />
                <LineTo x="125.20" y="13.80" />
                <LineTo x="125.20" y="13.00" />
                <LineTo x="124.95" y="10.95" />
                <LineTo x="124.70" y="10.00" />
                <LineTo x="124.00" y="8.30" />
                <LineTo x="123.15" y="6.85" />
                <LineTo x="121.95" y="5.65" />
                <LineTo x="120.00" y="4.20" />
                <LineTo x="117.75" y="3.15" />
                <LineTo x="116.20" y="2.65" />
                <LineTo x="113.20" y="2.05" />
                <LineTo x="112.70" y="2.05" />
                <LineTo x="112.20" y="1.95" />
                <LineTo x="111.75" y="1.95" />
                <LineTo x="111.15" y="1.80" />
                <LineTo x="110.55" y="1.80" />
                <LineTo x="109.95" y="1.70" />
                <LineTo x="108.40" y="1.70" />
                <LineTo x="71.20" y="0.00" />
                <LineTo x="34.45" y="1.45" />
                <LineTo x="33.60" y="1.60" />
                <LineTo x="32.80" y="1.60" />
                <LineTo x="32.05" y="1.70" />
                <LineTo x="31.35" y="1.70" />
                <LineTo x="30.60" y="1.80" />
                <LineTo x="30.00" y="1.80" />
                <LineTo x="29.40" y="1.95" />
                <LineTo x="28.80" y="1.95" />
                <LineTo x="28.35" y="2.05" />
                <LineTo x="27.60" y="2.05" />
                <LineTo x="22.95" y="3.25" />
                <LineTo x="20.05" y="4.80" />
                <LineTo x="19.35" y="5.30" />
                <LineTo x="17.65" y="7.10" />
                <LineTo x="17.20" y="7.80" />
                <LineTo x="16.45" y="9.50" />
                <LineTo x="15.85" y="12.25" />
                <LineTo x="15.75" y="23.20" />
                <LineTo x="15.60" y="24.25" />
                <LineTo x="15.60" y="25.45" />
                <LineTo x="14.65" y="33.00" />
                <LineTo x="12.75" y="40.45" />
                <LineTo x="11.80" y="42.75" />
                <LineTo x="10.10" y="45.85" />
                <LineTo x="7.95" y="48.50" />
                <LineTo x="5.40" y="50.40" />
                <LineTo x="4.45" y="50.90" />
                <LineTo x="1.20" y="51.85" />
                <LineTo x="0.00" y="51.85" />
                <LineTo x="1.20" y="87.50" />
                <LineTo x="1.35" y="88.60" />
                <LineTo x="2.90" y="105.75" />
                <LineTo x="3.00" y="106.20" />
                <LineTo x="3.00" y="106.80" />
                <LineTo x="4.00" y="113.05" />
                <LineTo x="4.10" y="114.35" />
                <LineTo x="5.80" y="124.10" />
                <LineTo x="5.90" y="125.40" />
                <LineTo x="6.85" y="130.80" />
                <LineTo x="7.00" y="131.30" />
                <LineTo x="7.00" y="131.40" />
                <LineTo x="7.35" y="131.40" />
                <LineTo x="7.60" y="131.55" />
                <LineTo x="8.05" y="131.65" />
                <LineTo x="8.55" y="131.65" />
                <LineTo x="9.15" y="131.90" />
                <LineTo x="15.75" y="133.35" />
                <LineTo x="16.45" y="133.60" />
                <LineTo x="17.20" y="133.70" />
                <LineTo x="17.90" y="133.95" />
                <LineTo x="18.50" y="134.05" />
                <LineTo x="19.00" y="134.30" />
                <LineTo x="19.45" y="134.40" />
                <LineTo x="22.35" y="135.50" />
                <LineTo x="22.45" y="135.50" />
                <LineTo x="22.45" y="135.60" />
                <LineTo x="22.60" y="135.60" />
                <LineTo x="22.70" y="136.10" />
                <LineTo x="23.40" y="137.80" />
                <LineTo x="25.70" y="141.75" />
                <LineTo x="27.40" y="143.90" />
                <LineTo x="30.25" y="146.55" />
                <LineTo x="33.85" y="148.95" />
                <LineTo x="34.95" y="149.40" />
                <LineTo x="36.50" y="150.25" />
                <LineTo x="36.60" y="150.25" />
                <LineTo x="36.75" y="150.35" />
                <LineTo x="37.70" y="155.40" />
                <LineTo x="37.70" y="163.95" />
                <LineTo x="37.00" y="176.40" />
                <LineTo x="39.75" y="177.15" />
                <LineTo x="56.20" y="179.80" />
                <LineTo x="68.80" y="180.35" />
                <LineTo x="76.95" y="180.25" />
                <LineTo x="94.00" y="178.70" />
                <LineTo x="103.10" y="176.65" />
                <LineTo x="103.70" y="176.40" />
                <LineTo x="103.95" y="176.40" />
                <LineTo x="103.95" y="174.75" />
                <LineTo x="103.80" y="172.20" />
                <LineTo x="103.35" y="164.80" />
                <LineTo x="103.20" y="162.25" />
                <LineTo x="103.20" y="156.25" />
                <LineTo x="103.45" y="153.85" />
                <LineTo x="104.30" y="150.25" />
                <LineTo x="107.40" y="148.70" />
                <LineTo x="110.90" y="146.30" />
                <LineTo x="114.40" y="142.95" />
                <LineTo x="114.85" y="142.20" />
                <LineTo x="115.45" y="141.50" />
                <LineTo x="116.40" y="139.95" />
                <LineTo x="118.45" y="135.60" />
                <LineTo x="118.45" y="135.50" />
                <LineTo x="118.60" y="135.50" />
                <LineTo x="118.80" y="135.35" />
                <LineTo x="121.10" y="134.55" />
                <LineTo x="121.35" y="134.40" />
                <LineTo x="121.45" y="134.40" />
                <LineTo x="121.60" y="134.30" />
                <LineTo x="122.65" y="134.05" />
                <LineTo x="123.25" y="133.80" />
                <LineTo x="124.00" y="133.70" />
                <LineTo x="124.70" y="133.45" />
                <LineTo x="129.40" y="132.35" />
                <LineTo x="130.10" y="132.25" />
                <LineTo x="130.80" y="132.00" />
                <LineTo x="133.10" y="131.55" />
                <LineTo x="133.45" y="131.55" />
                <LineTo x="133.70" y="131.40" />
                <LineTo x="133.95" y="131.40" />
                <LineTo x="134.05" y="130.60" />
                <LineTo x="134.30" y="129.60" />
                <LineTo x="134.40" y="128.55" />
                <LineTo x="134.90" y="126.25" />
                <LineTo x="135.00" y="124.95" />
                <LineTo x="136.70" y="115.20" />
                <LineTo x="136.80" y="113.90" />
                <LineTo x="137.55" y="109.20" />
                <LineTo x="137.80" y="108.25" />
                <LineTo x="137.90" y="106.45" />
                <LineTo x="138.00" y="106.35" />
                <LineTo x="138.00" y="106.10" />
                <LineTo x="138.15" y="105.25" />
                <LineTo x="139.80" y="85.70" />
                <LineTo x="139.95" y="84.50" />
                <LineTo x="140.90" y="51.85" />
            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Back"/>
                    <Culture key="en" value="Back"/>
                    <Culture key="ru" value="&#x041E;&#x0431;&#x043E;&#x0440;&#x043E;&#x0442;"/>
                </Caption>
            </Localization>            
        </Face>
'

set identity_insert dbo.Face off

GO

/*
Fill FaceToDevice table
*/

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (1, 1)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (2, 1)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (3, 1)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (4, 1)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (5, 2)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (6, 2)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (7,  3)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (8,  3)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (9,  3)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (10, 3)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (11, 4)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (12, 4)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (13, 4)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (14, 4)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (15, 5)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (16, 5)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (17, 5)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (18, 5)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (19, 6)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (20, 6)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (21, 6)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (22, 6)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (23, 7)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (24, 7)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (25, 7)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (26, 7)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (27, 8)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (28, 8)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (29, 9)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (30, 9)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (31, 10)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (32, 10)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (33, 11)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (34, 11)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (35, 12)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (36, 12)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (37, 12)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (38, 12)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (39, 13)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (40, 13)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (41, 14)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (42, 14)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (43, 15)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (44, 15)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (45, 15)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (46, 15)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (47, 16)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (48, 16)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (49, 16)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (50, 16)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (51, 17)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (52, 17)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (53, 17)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (54, 17)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (55, 18)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (56, 18)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (57, 18)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (58, 18)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (59, 19)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (60, 19)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (61, 20)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (62, 20)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (63, 21)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (64, 21)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (65, 22)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (66, 22)

INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (67, 23)
INSERT INTO dbo.FaceToDevice (FaceId, DeviceId) VALUES (68, 23)

GO

/*
Fill Carrier table
*/

set identity_insert dbo.Carrier on

INSERT INTO dbo.Carrier
  (Id, Name, Icon)
  VALUES (1, N'Megafon', NULL)

INSERT INTO dbo.Carrier
  (Id, Name, Icon)
  VALUES (2, N'BeeLine', NULL)

INSERT INTO dbo.Carrier
  (Id, Name, Icon)
  VALUES (3, N'EuroSet', NULL)

set identity_insert dbo.Carrier off

GO

/*
Fill DeviceToCarrier table
*/

INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (1, 1)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (2, 1)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (3, 1)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (4, 1)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (5, 1)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (6, 1)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (7, 1)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (8, 1)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (9, 1)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (10, 1)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (11, 1)

INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (7, 2)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (8, 2)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (9, 2)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (10, 2)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (11, 2)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (12, 2)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (13, 2)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (14, 2)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (15, 2)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (16, 2)

INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (12, 3)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (13, 3)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (14, 3)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (15, 3)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (16, 3)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (17, 3)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (18, 3)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (19, 3)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (20, 3)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (21, 3)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (22, 3)
INSERT INTO dbo.DeviceToCarrier (DeviceId, CarrierId) VALUES (23, 3)

GO
