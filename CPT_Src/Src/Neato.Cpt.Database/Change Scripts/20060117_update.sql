/*
Field type is changed:
    table FaceToPaper: FaceX, FaceY -> real
    table Paper: Width, Height -> real
Removed old procedures: dbo.PrGetDeviceFacesById, dbo.PrGetDeviceFaceById
Insert data to tables for CPT (phones)
Table Device: drop column Faces

+++++++++++
- Create tables: FaceLayout, FaceLayoutItem
- Insert data for tables: FaceLayout, FaceLayoutItem
*/

BEGIN TRANSACTION

ALTER TABLE dbo.FaceToPaper
	DROP CONSTRAINT FK_FaceToPaper_Face

ALTER TABLE dbo.FaceToPaper
	DROP CONSTRAINT FK_FaceToPaper_Paper

CREATE TABLE dbo.Tmp_FaceToPaper
	(
	FaceId int NOT NULL,
	PaperId int NOT NULL,
	FaceX real NOT NULL,
	FaceY real NOT NULL
	)  ON [PRIMARY]

IF EXISTS(SELECT * FROM dbo.FaceToPaper)
	EXEC('INSERT INTO dbo.Tmp_FaceToPaper (FaceId, PaperId, FaceX, FaceY)
		SELECT FaceId, PaperId, CONVERT(real, FaceX), CONVERT(real, FaceY) FROM dbo.FaceToPaper (HOLDLOCK TABLOCKX)')

DROP TABLE dbo.FaceToPaper

EXECUTE sp_rename N'dbo.Tmp_FaceToPaper', N'FaceToPaper', 'OBJECT'

ALTER TABLE dbo.FaceToPaper WITH NOCHECK ADD CONSTRAINT
	FK_FaceToPaper_Paper FOREIGN KEY
	(
	PaperId
	) REFERENCES dbo.Paper
	(
	Id
	) ON UPDATE CASCADE
    

ALTER TABLE dbo.FaceToPaper WITH NOCHECK ADD CONSTRAINT
	FK_FaceToPaper_Face FOREIGN KEY
	(
	FaceId
	) REFERENCES dbo.Face
	(
	Id
	) ON UPDATE CASCADE

GO
COMMIT

---------------------------------------
BEGIN TRANSACTION
ALTER TABLE dbo.Paper
	DROP CONSTRAINT FK_Paper_PaperBrand

CREATE TABLE dbo.Tmp_Paper
	(
	Id int NOT NULL IDENTITY (1, 1),
	[Name] nvarchar(30) COLLATE Latin1_General_CI_AS NOT NULL,
	BrandId int NOT NULL,
	Icon image NULL,
	Width real NOT NULL,
	Height real NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_Paper ON

IF EXISTS(SELECT * FROM dbo.Paper)
	 EXEC('INSERT INTO dbo.Tmp_Paper (Id, Name, BrandId, Icon, Width, Height)
		SELECT Id, Name, BrandId, Icon, CONVERT(real, Width), CONVERT(real, Height) FROM dbo.Paper (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_Paper OFF

ALTER TABLE dbo.FaceToPaper
	DROP CONSTRAINT FK_FaceToPaper_Paper

DROP TABLE dbo.Paper

EXECUTE sp_rename N'dbo.Tmp_Paper', N'Paper', 'OBJECT'

ALTER TABLE dbo.Paper ADD CONSTRAINT
	PK_Paper PRIMARY KEY CLUSTERED 
	(
	Id
	) ON [PRIMARY]


ALTER TABLE dbo.Paper WITH NOCHECK ADD CONSTRAINT
	FK_Paper_PaperBrand FOREIGN KEY
	(
	BrandId
	) REFERENCES dbo.PaperBrand
	(
	Id
	) ON UPDATE CASCADE
	
ALTER TABLE dbo.FaceToPaper WITH NOCHECK ADD CONSTRAINT
	FK_FaceToPaper_Paper FOREIGN KEY
	(
	PaperId
	) REFERENCES dbo.Paper
	(
	Id
	) ON UPDATE CASCADE
	
GO
COMMIT

---------------------------------------
BEGIN TRANSACTION

-- PaperBrand Data
IF NOT EXISTS (SELECT TOP 1 * FROM dbo.PaperBrand WHERE Id = 1)
BEGIN
SET IDENTITY_INSERT PaperBrand ON

INSERT INTO PaperBrand ([Id], [Name], [Icon]) 
  VALUES (1, 'Neato', 0x47494638396164002800A200009D9C9C323030FFFFFFF4F4F4615F5F716F6F514F4FDEDEDE21F90400000000002C00000000640028000003FF28BADCFE30CA49ABBD38EBCDBBFF60288E64699E68AAAE6CEBBE702CCF746DDF78AE9307401881A0A17050000C04026020381492D06881E983121B832714A03844BF86E5A04AB8768F80621350000A958D833B4837A801F469734EEF53FB015C0C787D828480746204850B5E017602638841820B057900990566877A72759BA14B0287417A0A038B8CA47DA1A20359806A8E04450775999A950A756AA84CACA64C9F9410A541BEB7808675C071977D04C38BB5C181C0B0BEBC8907CE0B9DC373BB838895C78146CD0F4EE54DD445D08FDD117C43F3DF79E1DC07FCBE84739088894B27A41F3F60EC00494B15A0DA3125DAF0213263CD13C02F77280D24F424FF11C12051C274B914461CC36AC4A28D6230864FC18F1627212398898E34686C3C5A9B38525E3C3B97AAB99B342E520F9706FE08DB8329D74A42694001B9A9F35018A782D8255566EA5D231F2E23A23ADAA70838A6C51E4065B84C802A74D62039D00A0F91D05F3E56414869361F5AB80EA0EE7C04EB2D3321DE1A91B46557EC9A44CEFAC11AE068E6D994622603139C121D5BB856BB6966124BEE435BDD444B4AAB2068A8397A2E0334409B76ECB4A12319FEF8A8366D318B51F171F828D45BB9AD650A952D93B0009C5DFC0E183818D014BA1241F69D2472D013DF10BF955142DA872BE3A3D8081D93440DFB2759CB9CDF449E7D9A05EFC993FAEE3B4D62CD340789A5197EA38DF68B3793B1942081052EB82083B0E0975A6A89ED60E1851866A8E1861C76E8E187208628E28824966862090900003B)
  
SET IDENTITY_INSERT PaperBrand OFF
END

-- Paper Data
IF NOT EXISTS (SELECT TOP 1 * FROM dbo.Paper WHERE Id IN (1, 2, 3))
BEGIN
    set identity_insert dbo.Paper on
    INSERT INTO dbo.Paper (Id, [Name], BrandId, Width, Height) VALUES (1, N'Neato - Motorola v180', 1, 612, 396)

    INSERT INTO dbo.Paper (Id, [Name], BrandId, Width, Height) VALUES (2, N'Neato - Motorola RAZR', 1, 612, 396)

    INSERT INTO dbo.Paper (Id, [Name], BrandId, Width, Height) VALUES (3, N'Neato - Samsung X495/497', 1, 612, 396)
    set identity_insert dbo.Paper off
END

-- FaceToPaper Data
IF NOT EXISTS (SELECT TOP 1 * FROM dbo.FaceToPaper WHERE
    (FaceId BETWEEN 1 AND 2) AND PaperId = 1 OR
    (FaceId BETWEEN 3 AND 4) AND PaperId = 2 OR
    (FaceId BETWEEN 5 AND 6) AND PaperId = 3
)
BEGIN
    INSERT INTO dbo.FaceToPaper (FaceId, PaperId, FaceX, FaceY) VALUES (1, 1, 82.5, 78.00)

    INSERT INTO dbo.FaceToPaper (FaceId, PaperId, FaceX, FaceY) VALUES (2, 1, 392.5, 123.5)

    INSERT INTO dbo.FaceToPaper (FaceId, PaperId, FaceX, FaceY) VALUES (3, 2, 104.25, 67.35)

    INSERT INTO dbo.FaceToPaper (FaceId, PaperId, FaceX, FaceY) VALUES (4, 2, 378.25, 67.50)

    INSERT INTO dbo.FaceToPaper (FaceId, PaperId, FaceX, FaceY) VALUES (5, 3, 82.5, 78.00)

    INSERT INTO dbo.FaceToPaper (FaceId, PaperId, FaceX, FaceY) VALUES (6, 3, 385.15, 103.75)
END
GO

-- FaceLocalization Data
IF NOT EXISTS (SELECT TOP 1 * FROM dbo.FaceLocalization WHERE
    FaceId = 1 AND Culture IN ('', 'en', 'ru') OR
    FaceId = 2 AND Culture IN ('', 'en', 'ru') OR
    FaceId = 3 AND Culture IN ('', 'en', 'ru') OR
    FaceId = 4 AND Culture IN ('', 'en', 'ru') OR
    FaceId = 5 AND Culture IN ('', 'en', 'ru') OR
    FaceId = 6 AND Culture IN ('', 'en', 'ru')
)
BEGIN
    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1, N'', N'Front')
    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1, N'en', N'Front')
    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1, N'ru', N'�����')

    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (2, N'', N'Back')
    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (2, N'en', N'Back')
    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (2, N'ru', N'������')

    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (3, N'', N'Front')
    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (3, N'en', N'Front')
    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (3, N'ru', N'�����')

    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (4, N'', N'Back')
    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (4, N'en', N'Back')
    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (4, N'ru', N'������')

    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (5, N'', N'Front')
    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (5, N'en', N'Front')
    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (5, N'ru', N'�����')

    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (6, N'', N'Back')
    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (6, N'en', N'Back')
    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (6, N'ru', N'������')
END

-- Face Data
DECLARE @ptrContour binary(16)

SELECT @ptrContour = TEXTPTR(Contour) FROM dbo.Face WHERE [Id] = 1
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
    <MoveTo x="81.15" y="42.15" />
    <CurveTo cx="81.15" cy="46.60" x="78.00" y="49.80" />
    <CurveTo cx="74.80" cy="52.95" x="70.35" y="52.95" />
    <CurveTo cx="65.90" cy="52.95" x="62.70" y="49.80" />
    <CurveTo cx="59.55" cy="46.60" x="59.55" y="42.15" />
    <CurveTo cx="59.55" cy="37.70" x="62.70" y="34.50" />
    <CurveTo cx="65.90" cy="31.35" x="70.35" y="31.35" />
    <CurveTo cx="74.80" cy="31.35" x="78.00" y="34.50" />
    <CurveTo cx="81.15" cy="37.70" x="81.15" y="42.15" />
    <MoveTo x="106.35" y="85.20" />
    <CurveTo cx="106.35" cy="87.10" x="105.05" y="88.40" />
    <CurveTo cx="103.75" cy="89.70" x="101.85" y="89.70" />
    <LineTo x="38.85" y="89.70" />
    <CurveTo cx="37.00" cy="89.70" x="35.65" y="88.40" />
    <CurveTo cx="34.35" cy="87.10" x="34.35" y="85.20" />
    <LineTo x="34.35" y="67.20" />
    <CurveTo cx="34.35" cy="65.35" x="35.65" y="64.00" />
    <CurveTo cx="37.00" cy="62.70" x="38.85" y="62.70" />
    <LineTo x="101.85" y="62.70" />
    <CurveTo cx="103.75" cy="62.70" x="105.05" y="64.00" />
    <CurveTo cx="106.35" cy="65.35" x="106.35" y="67.20" />
    <LineTo x="106.35" y="85.20" />
    <MoveTo x="113.30" y="30.30" />
    <LineTo x="113.15" y="0.00" />
    <LineTo x="28.15" y="0.00" />
    <LineTo x="27.95" y="30.30" />
    <CurveTo cx="21.30" cy="30.30" x="16.85" y="32.40" />
    <CurveTo cx="14.20" cy="33.65" x="10.65" y="36.80" />
    <CurveTo cx="6.50" cy="40.65" x="0.00" y="42.25" />
    <LineTo x="0.25" y="57.40" />
    <CurveTo cx="0.25" cy="124.40" x="6.85" y="173.40" />
    <CurveTo cx="8.00" cy="181.90" x="13.70" y="187.00" />
    <CurveTo cx="19.45" cy="192.15" x="28.00" y="192.15" />
    <CurveTo cx="30.30" cy="207.65" x="42.45" y="218.05" />
    <CurveTo cx="54.60" cy="228.50" x="70.65" y="228.50" />
    <CurveTo cx="86.70" cy="228.50" x="98.90" y="218.05" />
    <CurveTo cx="111.00" cy="207.65" x="113.30" y="192.15" />
    <CurveTo cx="121.80" cy="192.15" x="127.55" y="187.00" />
    <CurveTo cx="133.25" cy="181.90" x="134.40" y="173.40" />
    <CurveTo cx="141.00" cy="124.85" x="141.00" y="57.40" />
    <LineTo x="140.85" y="42.15" />
    <CurveTo cx="134.55" cy="40.35" x="130.60" y="36.80" />
    <CurveTo cx="127.15" cy="33.65" x="124.45" y="32.40" />
    <CurveTo cx="120.00" cy="30.30" x="113.30" y="30.30" />
</Contour>
'

SELECT @ptrContour = TEXTPTR(Contour) FROM dbo.Face WHERE [Id] = 2
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
    <MoveTo x="128.55" y="0.00" />
    <LineTo x="110.00" y="0.00" />
    <CurveTo cx="102.80" cy="0.00" x="99.80" y="2.00" />
    <CurveTo cx="98.70" cy="2.75" x="93.65" y="7.05" />
    <CurveTo cx="89.15" cy="10.90" x="85.55" y="12.90" />
    <CurveTo cx="77.25" cy="17.50" x="66.55" y="17.50" />
    <CurveTo cx="55.90" cy="17.50" x="47.55" y="12.90" />
    <CurveTo cx="43.95" cy="10.90" x="39.45" y="7.05" />
    <LineTo x="33.30" y="2.00" />
    <CurveTo cx="30.30" cy="0.00" x="23.05" y="0.00" />
    <LineTo x="4.55" y="0.00" />
    <CurveTo cx="2.70" cy="0.00" x="1.35" y="1.45" />
    <CurveTo cx="0.05" cy="2.85" x="0.05" y="4.70" />
    <LineTo x="0.00" y="123.65" />
    <CurveTo cx="0.00" cy="133.20" x="6.70" y="140.05" />
    <CurveTo cx="13.45" cy="147.00" x="22.90" y="147.00" />
    <CurveTo cx="24.10" cy="158.20" x="30.70" y="167.60" />
    <CurveTo cx="37.20" cy="176.90" x="47.05" y="181.75" />
    <CurveTo cx="47.05" cy="177.85" x="49.85" y="175.00" />
    <CurveTo cx="52.65" cy="172.10" x="56.50" y="172.10" />
    <LineTo x="76.60" y="172.10" />
    <CurveTo cx="80.45" cy="172.10" x="83.25" y="175.00" />
    <CurveTo cx="86.00" cy="177.85" x="86.00" y="181.75" />
    <CurveTo cx="95.85" cy="176.90" x="102.35" y="167.60" />
    <CurveTo cx="108.95" cy="158.20" x="110.15" y="147.00" />
    <CurveTo cx="119.60" cy="147.00" x="126.35" y="140.05" />
    <CurveTo cx="133.10" cy="133.20" x="133.10" y="123.65" />
    <LineTo x="133.05" y="4.70" />
    <CurveTo cx="133.05" cy="2.85" x="131.70" y="1.45" />
    <CurveTo cx="130.35" cy="0.00" x="128.55" y="0.00" />
</Contour>
'

SELECT @ptrContour = TEXTPTR(Contour) FROM dbo.Face WHERE [Id] = 3
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
    <MoveTo x="109.40" y="2.90" />
    <CurveTo cx="94.60" cy="0.40" x="88.25" y="0.00" />
    <LineTo x="91.30" y="17.50" />
    <CurveTo cx="92.30" cy="21.80" x="91.75" y="23.55" />
    <CurveTo cx="90.80" cy="26.65" x="85.60" y="26.65" />
    <LineTo x="46.75" y="26.65" />
    <CurveTo cx="41.60" cy="26.65" x="40.65" y="23.55" />
    <CurveTo cx="40.10" cy="21.80" x="41.05" y="17.50" />
    <LineTo x="44.15" y="0.00" />
    <CurveTo cx="37.55" cy="0.40" x="21.95" y="2.90" />
    <CurveTo cx="4.95" cy="5.60" x="0.45" y="7.40" />
    <LineTo x="0.45" y="207.90" />
    <CurveTo cx="6.35" cy="212.40" x="22.70" y="215.15" />
    <CurveTo cx="40.40" cy="218.15" x="65.20" y="218.15" />
    <CurveTo cx="91.05" cy="218.15" x="108.35" y="215.15" />
    <CurveTo cx="123.95" cy="212.45" x="129.90" y="207.90" />
    <LineTo x="129.90" y="7.40" />
    <CurveTo cx="125.35" cy="5.60" x="109.40" y="2.90" />
    <MoveTo x="0.00" y="223.10" />
    <CurveTo cx="7.30" cy="227.25" x="24.20" y="230.00" />
    <CurveTo cx="42.55" cy="232.95" x="64.80" y="232.95" />
    <CurveTo cx="87.05" cy="232.95" x="105.40" y="230.00" />
    <CurveTo cx="122.35" cy="227.25" x="129.60" y="223.10" />
    <LineTo x="129.60" y="258.80" />
    <CurveTo cx="119.40" cy="262.60" x="98.85" y="265.55" />
    <CurveTo cx="79.10" cy="268.40" x="64.80" y="268.40" />
    <CurveTo cx="50.50" cy="268.40" x="30.75" y="265.55" />
    <CurveTo cx="10.15" cy="262.60" x="0.00" y="258.80" />
    <LineTo x="0.00" y="223.10" />
    <MoveTo x="112.95" y="46.80" />
    <LineTo x="18.80" y="46.80" />
    <CurveTo cx="14.50" cy="46.80" x="11.55" y="49.45" />
    <CurveTo cx="8.20" cy="52.35" x="8.20" y="57.30" />
    <LineTo x="8.20" y="122.45" />
    <CurveTo cx="8.20" cy="127.30" x="9.05" y="129.25" />
    <CurveTo cx="10.85" cy="133.60" x="17.05" y="134.85" />
    <CurveTo cx="23.95" cy="136.20" x="37.95" y="137.55" />
    <CurveTo cx="54.45" cy="139.20" x="65.90" y="139.20" />
    <CurveTo cx="77.05" cy="139.20" x="93.60" y="137.55" />
    <CurveTo cx="107.90" cy="136.15" x="114.70" y="134.85" />
    <CurveTo cx="120.90" cy="133.60" x="122.75" y="129.25" />
    <CurveTo cx="123.55" cy="127.30" x="123.55" y="122.45" />
    <LineTo x="123.55" y="57.30" />
    <CurveTo cx="123.55" cy="52.35" x="120.20" y="49.45" />
    <CurveTo cx="117.15" cy="46.80" x="112.95" y="46.80" />
    <MoveTo x="55.35" y="163.35" />
    <CurveTo cx="59.90" cy="158.80" x="66.30" y="158.80" />
    <CurveTo cx="72.70" cy="158.80" x="77.25" y="163.35" />
    <CurveTo cx="81.80" cy="167.90" x="81.80" y="174.30" />
    <CurveTo cx="81.80" cy="180.70" x="77.25" y="185.25" />
    <CurveTo cx="72.70" cy="189.80" x="66.30" y="189.80" />
    <CurveTo cx="59.90" cy="189.80" x="55.35" y="185.25" />
    <CurveTo cx="50.80" cy="180.70" x="50.80" y="174.30" />
    <CurveTo cx="50.80" cy="167.90" x="55.35" y="163.35" />
    <MoveTo x="52.20" y="200.05" />
    <CurveTo cx="51.95" cy="201.75" x="52.75" y="203.15" />
    <CurveTo cx="53.60" cy="204.60" x="55.00" y="204.90" />
    <LineTo x="56.05" y="205.15" />
    <CurveTo cx="61.95" cy="206.40" x="64.80" y="206.40" />
    <CurveTo cx="73.40" cy="206.40" x="78.20" y="204.85" />
    <CurveTo cx="79.60" cy="204.40" x="80.30" y="202.90" />
    <CurveTo cx="81.05" cy="201.40" x="80.65" y="199.70" />
    <CurveTo cx="80.25" cy="198.05" x="79.05" y="197.20" />
    <CurveTo cx="77.75" cy="196.30" x="76.35" y="196.75" />
    <CurveTo cx="72.50" cy="198.00" x="64.80" y="198.00" />
    <CurveTo cx="62.20" cy="198.00" x="57.30" y="196.90" />
    <LineTo x="56.30" y="196.70" />
    <CurveTo cx="54.85" cy="196.35" x="53.65" y="197.35" />
    <CurveTo cx="52.45" cy="198.35" x="52.20" y="200.05" />
</Contour>
'

SELECT @ptrContour = TEXTPTR(Contour) FROM dbo.Face WHERE [Id] = 4
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
    <MoveTo x="120.40" y="3.75" />
    <CurveTo cx="95.40" cy="7.45" x="70.20" y="7.45" />
    <CurveTo cx="45.00" cy="7.45" x="19.95" y="3.75" />
    <LineTo x="0.00" y="0.00" />
    <LineTo x="0.00" y="99.70" />
    <CurveTo cx="0.00" cy="111.90" x="5.40" y="120.30" />
    <CurveTo cx="10.20" cy="127.70" x="8.90" y="135.60" />
    <CurveTo cx="14.60" cy="137.85" x="31.60" y="140.60" />
    <CurveTo cx="52.50" cy="143.90" x="70.20" y="143.90" />
    <CurveTo cx="87.90" cy="143.90" x="108.75" y="140.60" />
    <CurveTo cx="125.75" cy="137.85" x="131.45" y="135.60" />
    <CurveTo cx="131.15" cy="133.65" x="131.40" y="130.75" />
    <CurveTo cx="131.95" cy="124.95" x="134.95" y="120.30" />
    <CurveTo cx="140.35" cy="112.00" x="140.35" y="99.70" />
    <LineTo x="140.35" y="0.00" />
    <CurveTo cx="132.90" cy="1.85" x="120.40" y="3.75" />
    <MoveTo x="82.35" y="75.05" />
    <CurveTo cx="77.25" cy="80.15" x="70.10" y="80.15" />
    <CurveTo cx="62.95" cy="80.15" x="57.85" y="75.05" />
    <CurveTo cx="52.75" cy="69.95" x="52.75" y="62.80" />
    <CurveTo cx="52.75" cy="55.65" x="57.85" y="50.55" />
    <CurveTo cx="62.95" cy="45.45" x="70.10" y="45.45" />
    <CurveTo cx="77.25" cy="45.45" x="82.35" y="50.55" />
    <CurveTo cx="87.45" cy="55.65" x="87.45" y="62.80" />
    <CurveTo cx="87.45" cy="69.95" x="82.35" y="75.05" />
    <MoveTo x="70.45" y="154.80" />
    <CurveTo cx="108.20" cy="154.80" x="131.75" y="147.15" />
    <LineTo x="131.60" y="249.40" />
    <CurveTo cx="107.80" cy="257.15" x="70.45" y="257.15" />
    <CurveTo cx="32.90" cy="257.15" x="8.90" y="249.30" />
    <LineTo x="9.10" y="147.15" />
    <CurveTo cx="32.70" cy="154.80" x="70.45" y="154.80" />
    <MoveTo x="49.95" y="227.15" />
    <LineTo x="90.95" y="227.15" />
    <CurveTo cx="92.80" cy="227.15" x="94.10" y="228.45" />
    <CurveTo cx="95.45" cy="229.80" x="95.45" y="231.65" />
    <LineTo x="95.45" y="244.30" />
    <CurveTo cx="95.45" cy="246.15" x="94.10" y="247.50" />
    <CurveTo cx="92.80" cy="248.80" x="90.95" y="248.80" />
    <LineTo x="49.95" y="248.80" />
    <CurveTo cx="48.10" cy="248.80" x="46.75" y="247.50" />
    <CurveTo cx="45.45" cy="246.20" x="45.45" y="244.30" />
    <LineTo x="45.45" y="231.65" />
    <CurveTo cx="45.45" cy="229.80" x="46.75" y="228.45" />
    <CurveTo cx="48.10" cy="227.15" x="49.95" y="227.15" />
</Contour>
'

SELECT @ptrContour = TEXTPTR(Contour) FROM dbo.Face WHERE [Id] = 5
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
    <MoveTo x="110.10" y="85.05" />
    <LineTo x="110.10" y="125.95" />
    <CurveTo cx="110.10" cy="128.55" x="108.25" y="130.40" />
    <CurveTo cx="106.40" cy="132.25" x="103.80" y="132.25" />
    <LineTo x="76.10" y="133.25" />
    <LineTo x="49.40" y="132.25" />
    <CurveTo cx="46.80" cy="132.25" x="44.95" y="130.40" />
    <CurveTo cx="43.10" cy="128.55" x="43.10" y="125.95" />
    <LineTo x="43.10" y="85.05" />
    <CurveTo cx="43.10" cy="82.45" x="44.95" y="80.60" />
    <CurveTo cx="46.80" cy="78.75" x="49.40" y="78.75" />
    <LineTo x="76.10" y="77.75" />
    <LineTo x="103.80" y="78.75" />
    <CurveTo cx="106.45" cy="78.75" x="108.25" y="80.60" />
    <CurveTo cx="110.10" cy="82.45" x="110.10" y="85.05" />
    <MoveTo x="76.60" y="0.00" />
    <CurveTo cx="37.65" cy="0.00" x="37.25" y="2.05" />
    <LineTo x="37.25" y="42.50" />
    <CurveTo cx="23.50" cy="45.45" x="1.45" y="59.85" />
    <CurveTo cx="0.00" cy="79.45" x="0.35" y="112.25" />
    <CurveTo cx="0.85" cy="151.55" x="4.05" y="180.35" />
    <CurveTo cx="4.05" cy="184.70" x="7.15" y="187.80" />
    <CurveTo cx="10.25" cy="190.90" x="14.60" y="190.90" />
    <LineTo x="26.95" y="190.95" />
    <CurveTo cx="28.90" cy="190.95" x="30.30" y="192.35" />
    <CurveTo cx="31.75" cy="193.75" x="31.75" y="195.70" />
    <LineTo x="31.65" y="211.80" />
    <CurveTo cx="31.65" cy="217.90" x="34.95" y="223.10" />
    <CurveTo cx="38.15" cy="228.10" x="43.40" y="230.60" />
    <LineTo x="43.50" y="246.70" />
    <CurveTo cx="59.60" cy="250.35" x="76.25" y="250.45" />
    <LineTo x="76.95" y="250.45" />
    <CurveTo cx="93.60" cy="250.35" x="109.70" y="246.70" />
    <LineTo x="109.75" y="230.60" />
    <CurveTo cx="115.05" cy="228.10" x="118.25" y="223.10" />
    <CurveTo cx="121.55" cy="217.90" x="121.55" y="211.80" />
    <LineTo x="121.45" y="195.70" />
    <CurveTo cx="121.45" cy="193.75" x="122.90" y="192.35" />
    <CurveTo cx="124.35" cy="190.95" x="126.25" y="190.95" />
    <LineTo x="138.60" y="190.90" />
    <CurveTo cx="142.95" cy="190.90" x="146.05" y="187.80" />
    <CurveTo cx="149.10" cy="184.70" x="149.10" y="180.35" />
    <CurveTo cx="152.35" cy="151.55" x="152.85" y="112.25" />
    <CurveTo cx="153.20" cy="79.45" x="151.75" y="59.85" />
    <CurveTo cx="129.60" cy="45.40" x="115.90" y="42.50" />
    <LineTo x="115.90" y="2.05" />
    <CurveTo cx="115.55" cy="0.00" x="76.60" y="0.00" />
</Contour>
'

SELECT @ptrContour = TEXTPTR(Contour) FROM dbo.Face WHERE [Id] = 6
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
    <MoveTo x="125.15" y="13.30" />
    <CurveTo cx="125.15" cy="3.45" x="113.15" y="2.00" />
    <CurveTo cx="97.85" cy="0.00" x="70.50" y="0.00" />
    <CurveTo cx="43.10" cy="0.00" x="27.80" y="2.00" />
    <CurveTo cx="15.80" cy="3.45" x="15.80" y="13.30" />
    <LineTo x="15.80" y="13.45" />
    <CurveTo cx="15.80" cy="26.85" x="14.45" y="33.75" />
    <CurveTo cx="11.00" cy="51.50" x="0.00" y="51.80" />
    <CurveTo cx="0.70" cy="78.65" x="1.30" y="88.80" />
    <CurveTo cx="1.70" cy="94.95" x="3.00" y="106.30" />
    <CurveTo cx="3.85" cy="114.00" x="7.00" y="131.30" />
    <LineTo x="19.50" y="134.30" />
    <LineTo x="22.50" y="135.45" />
    <CurveTo cx="26.25" cy="145.60" x="36.70" y="150.25" />
    <CurveTo cx="38.10" cy="155.30" x="37.75" y="163.05" />
    <LineTo x="37.00" y="176.30" />
    <CurveTo cx="49.00" cy="180.30" x="70.50" y="180.30" />
    <CurveTo cx="92.00" cy="180.30" x="104.00" y="176.30" />
    <LineTo x="103.25" y="163.05" />
    <CurveTo cx="102.85" cy="155.30" x="104.25" y="150.25" />
    <CurveTo cx="114.70" cy="145.60" x="118.45" y="135.45" />
    <LineTo x="121.50" y="134.30" />
    <CurveTo cx="124.50" cy="133.30" x="134.00" y="131.30" />
    <CurveTo cx="136.90" cy="115.45" x="138.00" y="106.30" />
    <CurveTo cx="139.30" cy="94.95" x="139.70" y="88.80" />
    <CurveTo cx="140.30" cy="78.65" x="141.00" y="51.80" />
    <CurveTo cx="130.00" cy="51.50" x="126.50" y="33.75" />
    <CurveTo cx="125.15" cy="26.80" x="125.15" y="13.45" />
    <LineTo x="125.15" y="13.30" />
</Contour>
'

COMMIT 

------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PrGetDeviceFacesById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PrGetDeviceFacesById]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PrGetDeviceFaceById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PrGetDeviceFaceById]
GO

------------------------------------

BEGIN TRANSACTION
EXEC sp_columns @table_name = 'Device', @table_owner='dbo', @column_name = 'Faces'
IF (@@ROWCOUNT > 0) 
BEGIN
    ALTER TABLE dbo.Device
	    DROP COLUMN Faces
END
GO
COMMIT

------------------------------------

-- Create table FaceLayout
BEGIN TRANSACTION

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_FaceLayoutItem_FaceLayout]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[FaceLayoutItem] DROP CONSTRAINT FK_FaceLayoutItem_FaceLayout
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FaceLayout]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[FaceLayout]
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FaceLayout]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[FaceLayout] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[FaceId] [int] NOT NULL 
) ON [PRIMARY]
END
GO

ALTER TABLE [dbo].[FaceLayout] WITH NOCHECK ADD 
	CONSTRAINT [PK_FaceLayout] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[FaceLayout] ADD 
	CONSTRAINT [FK_FaceLayout_Face] FOREIGN KEY 
	(
		[FaceId]
	) REFERENCES [dbo].[Face] (
		[Id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

-- Create table FaceLayoutItem
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FaceLayoutItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[FaceLayoutItem]
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FaceLayoutItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[FaceLayoutItem] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[FaceLayoutId] [int] NOT NULL ,
	[X] [real] NOT NULL ,
	[Y] [real] NOT NULL ,
	[Position] [tinyint] NOT NULL ,
	[Text] [nvarchar] (50) COLLATE Latin1_General_BIN NOT NULL ,
	[Font] [nvarchar] (50) COLLATE Latin1_General_BIN NOT NULL ,
	[Size] [tinyint] NOT NULL ,
	[Bold] [bit] NOT NULL ,
	[Italic] [bit] NOT NULL ,
	[Color] [int] NOT NULL ,
	[Angle] [real] NOT NULL 
) ON [PRIMARY]
END
GO

ALTER TABLE [dbo].[FaceLayoutItem] WITH NOCHECK ADD 
	CONSTRAINT [PK_FaceLayoutItem] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[FaceLayoutItem] ADD 
	CONSTRAINT [DF_FaceLayoutItem_X] DEFAULT (0) FOR [X],
	CONSTRAINT [DF_FaceLayoutItem_Y] DEFAULT (0) FOR [Y],
	CONSTRAINT [DF_FaceLayoutItem_Position] DEFAULT (0) FOR [Position],
	CONSTRAINT [DF_FaceLayoutItem_Text] DEFAULT (N'Enter text') FOR [Text],
	CONSTRAINT [DF_FaceLayoutItem_Font] DEFAULT (N'Arial') FOR [Font],
	CONSTRAINT [DF_FaceLayoutItem_Size2] DEFAULT (16) FOR [Size],
	CONSTRAINT [DF_FaceLayoutItem_Bold] DEFAULT (0) FOR [Bold],
	CONSTRAINT [DF_FaceLayoutItem_Italic] DEFAULT (0) FOR [Italic],
	CONSTRAINT [DF_FaceLayoutItem_Color] DEFAULT (0) FOR [Color],
	CONSTRAINT [DF_FaceLayoutItem_Angle] DEFAULT (0) FOR [Angle]
GO

ALTER TABLE [dbo].[FaceLayoutItem] ADD 
	CONSTRAINT [FK_FaceLayoutItem_FaceLayout] FOREIGN KEY 
	(
		[FaceLayoutId]
	) REFERENCES [dbo].[FaceLayout] (
		[Id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

-- Add data to table FaceLayout
SET IDENTITY_INSERT [dbo].[FaceLayout] ON

INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (1, 3)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (2, 3)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (3, 4)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (4, 4)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (5, 4)

SET IDENTITY_INSERT [dbo].[FaceLayout] OFF
-- Add data to table FaceLayoutItem
SET IDENTITY_INSERT [dbo].[FaceLayoutItem] ON

INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle]) VALUES (1, 1, 0, 0, 0, 'Name', 'Arial', 24, 0, 0, 0, 0)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle]) VALUES (2, 1, 30, 100, 1, 'Phone', 'Verdana', 16, 1, 1, 255, 15)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle]) VALUES (3, 2, 0, 50, 0, 'text1', 'Arial', 32, 0, 0, 255, 90)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle]) VALUES (4, 2, 50, 200, 1, 'text2', 'Verdana', 8, 1, 1, 0, 0)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle]) VALUES (5, 2, 100, 0, 2, 'text3', 'Verdana', 16, 0, 1, 0, 0)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle]) VALUES (6, 3, 50, 100, 0, 'text1', 'Arial', 9, 1, 0, 255, 45)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle]) VALUES (7, 4, 20, 0, 0, 'text1', 'Arial', 10, 0, 0, 255, 0)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle]) VALUES (8, 4, 20, 100, 1, 'text2', 'Verdana', 12, 1, 0, 0, 0)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle]) VALUES (9, 5, 100, 0, 0, 'text1', 'Verdana', 14, 1, 1, 5, 30)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle]) VALUES (10, 5, 50, 90, 1, 'text2', 'Verdana', 9, 0, 1, 5, 0)
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle]) VALUES (11, 5, 0, 50, 2, 'text3', 'Verdana', 12, 0, 1, 0, 90)

SET IDENTITY_INSERT [dbo].[FaceLayoutItem] OFF

COMMIT