BEGIN TRANSACTION
EXEC sp_columns @table_name = 'Customer', @table_owner='dbo', @column_name = 'pswuid'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE [dbo].[Customer] ADD [pswuid] VARCHAR(50) NULL
END
GO

---create tables
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MailType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[MailType] (
	[MailId] [int] IDENTITY (1, 1) NOT NULL ,
	[Description] [varchar] (50) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MailFormat]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[MailFormat] (
	[MailId] [int] NOT NULL ,
	[Culture] [varchar] (50) COLLATE Latin1_General_BIN NOT NULL ,
	[Subject] [nvarchar] (100) COLLATE Latin1_General_BIN NOT NULL ,
	[Body] [nvarchar] (1000) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]
END

GO

---create primary keys constraints
if not exists (select * from dbo.sysobjects where id = object_id(N'PK_MailType') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[MailType] WITH NOCHECK ADD 
	CONSTRAINT [PK_MailType] PRIMARY KEY  CLUSTERED 
	(
		[MailId]
	)  ON [PRIMARY] 
GO

---create foreign key constraints
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_MailFormat_MailType]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[MailFormat] ADD 
	CONSTRAINT [FK_MailFormat_MailType] FOREIGN KEY 
	(
		[MailId]
	) REFERENCES [dbo].[MailType] (
		[MailId]
	)
GO

-------------------------------------------
SET IDENTITY_INSERT [dbo].[Page] ON
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (18, N'ForgotPassword.aspx', N'ForgotPassword Page')
SET IDENTITY_INSERT [dbo].[Page] OFF

INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (18, 8, 0, 'L', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (18, 1, 1, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (18, 34, 2, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (18, 12, 3, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (18, 34, 4, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (18, 13, 5, 'R', 'Footer', 'N')

SET IDENTITY_INSERT [dbo].[MailType] ON
INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (1, 'Forgot Password')
SET IDENTITY_INSERT [dbo].[MailType] OFF

delete [dbo].[MailFormat]
INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (1, '', N'Password Reminder From BodyGlove', N'Hello {0}:
Here is the password you requested. Click {1} to generate your new password. After, please use it with your email address to log in at BodyGlove.')

COMMIT TRANSACTION