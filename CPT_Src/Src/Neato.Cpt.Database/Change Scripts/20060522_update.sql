
/* Changes
- add column "State" in table "Paper"
*/

BEGIN TRANSACTION

EXEC sp_columns @table_name = 'Paper', @table_owner='dbo', @column_name = 'State'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Paper ADD
	State nchar(1) COLLATE Latin1_General_BIN NOT NULL DEFAULT ('a')
END
GO

COMMIT 


-- BEGIN CarrierToRetailer
BEGIN TRANSACTION
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CarrierToRetailer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE dbo.CarrierToRetailer
	(
	RetailerId int NOT NULL,
	CarrierId int NOT NULL
	)  ON [PRIMARY]
GO
if not exists (select * from dbo.sysobjects where id = object_id(N'FK_CarrierToRetailer_Retailers') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.CarrierToRetailer ADD CONSTRAINT
	FK_CarrierToRetailer_Retailers FOREIGN KEY
	(
	RetailerId
	) REFERENCES dbo.Retailers
	(
	Id
	) ON UPDATE CASCADE
	 ON DELETE CASCADE
	
GO
if not exists (select * from dbo.sysobjects where id = object_id(N'FK_CarrierToRetailer_Carrier') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.CarrierToRetailer ADD CONSTRAINT
	FK_CarrierToRetailer_Carrier FOREIGN KEY
	(
	CarrierId
	) REFERENCES dbo.Carrier
	(
	Id
	) ON UPDATE CASCADE
	 ON DELETE CASCADE
	
GO
COMMIT
-- END CarrierToRetailer
