if not exists (select * from [dbo].[Page] where [PageId]=22)
BEGIN

SET IDENTITY_INSERT [dbo].[Page] ON
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (22, N'default.aspx?action=login', N'Login')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (23, N'default.aspx?action=logout', N'Logout')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (24, N'default.aspx?action=newprofile', N'New profile')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (25, N'default.aspx?action=editprofile', N'Edit profile')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (26, N'default.aspx?action=forgotpassword', N'Forgot password')
SET IDENTITY_INSERT [dbo].[Page] OFF

UPDATE [dbo].[Link] SET [PageId] = 22 WHERE [LinkId] = 20
UPDATE [dbo].[Link] SET [PageId] = 24 WHERE [LinkId] = 21
UPDATE [dbo].[Link] SET [PageId] = 25 WHERE [LinkId] = 22
UPDATE [dbo].[Link] SET [PageId] = 26 WHERE [LinkId] = 23
UPDATE [dbo].[Link] SET [PageId] = 23 WHERE [LinkId] = 24

END 