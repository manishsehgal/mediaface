DECLARE @url NVARCHAR(200)

SELECT TOP 1 @url = Url FROM [dbo].[Page] WHERE [PageId]=4
IF @url <> 'http://www.fellowes.com'
BEGIN
ALTER TABLE [dbo].[LinkLocalization] ALTER COLUMN [Text] [nvarchar] (100) NOT NULL

UPDATE [dbo].[Page] SET [Url]=N'http://www.fellowes.com' WHERE [PageId]=4
UPDATE [dbo].[Page] SET [Url]=N'http://www.fellowes.com/Fellowes/site/support/support_contact.aspx' WHERE [PageId]=5
UPDATE [dbo].[Page] SET [Url]=N'http://www.neato.com', [Description]=N'Neato Page' WHERE [PageId]=10
UPDATE [dbo].[Page] SET [Url]=N'http://www.fellowes.com/Fellowes/site/products/CategoryIndex.aspx' WHERE [PageId]=19

UPDATE [dbo].[LinkLocalization] SET [Text]=N'Copyright &copy; 2006, Fellowes, Inc.' WHERE [LinkId]=8
UPDATE [dbo].[LinkLocalization] SET [Text]=N'Neato' WHERE [LinkId]=10

UPDATE [dbo].[LinkPlace] SET [IsNewWindow]=N'Y' WHERE [LinkId] IN (SELECT [LinkId] FROM [dbo].[Link] WHERE [PageId]=5)

UPDATE [dbo].[MailFormat] SET [Subject]=N'Password Reminder From Fellowes', [Body]=N'Hello {0}:
Here is the password you requested. Click {1} to generate your new password. After, please use it with your email address to log in at Fellowes.' WHERE [MailId]=1
UPDATE [dbo].[MailFormat] SET [Subject]=N'Tell-A-Friend From Fellowes', [Body]=N'{0}

This email was sent from the tell-a-friend feature at the above mentioned site. To remove yourself from any further tell-a-friend emails, please click {1}.' WHERE [MailId]=2

END
