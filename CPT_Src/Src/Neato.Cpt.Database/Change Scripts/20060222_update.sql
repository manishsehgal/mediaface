/*
- update MailFormat & MailType
- create SupportPage table
*/

BEGIN TRANSACTION

-- update MailFormat & MailType

-- MailType
SET IDENTITY_INSERT [dbo].[MailType] ON
    IF Not Exists (SELECT * FROM [dbo].[MailType] WHERE [MailId] = 4)
        BEGIN
            INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (4, 'Tell us what you think')
        END
SET IDENTITY_INSERT [dbo].[MailType] OFF

UPDATE [dbo].[MailType]
SET [Description] = 'Add my device'
WHERE [Description] = 'Add New Phone'

-- MailFormat
UPDATE [dbo].[MailFormat]
SET [Subject] = 'Password Reminder From Neato',
    [Body] = 'Hello %UserName:
Here is the password you requested. Click %NewPasswordLink to generate your new password. After, please use it with your email address to log in at Neato.'
WHERE [Subject] = 'Password Reminder From Fellowes'

UPDATE [dbo].[MailFormat]
SET [Subject] = 'Tell-A-Friend From Neato',
    [Body] = '%MailText


%PrintzHomeLink

This email was sent from the tell-a-friend feature at the above mentioned site. To remove yourself from any further tell-a-friend emails, please click %UnsubscribeLink.'
WHERE [Subject] = 'Tell-A-Friend From Fellowes'

UPDATE [dbo].[MailFormat]
SET [Subject] = 'Add my device'
WHERE [Subject] = 'Add New Phone'

IF Not Exists (SELECT * FROM [dbo].[MailFormat] WHERE [MailId] = 4 AND [Culture] = '')
    BEGIN
        INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (4, '', N'Thanks for your feedback!',
        N'We hear you. We`ll log your comments and email back ASAP to answer your questions.
If you request changes to the site or product, we will notify you if we are able to accommodate your request.

Thanks again for your comments and email us any time!')
    END
    
--create SupportPage table
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SupportPage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[SupportPage] (
	[Culture] [nvarchar] (10) COLLATE Latin1_General_BIN NOT NULL ,
	[HtmlText] [ntext] COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END

GO

IF Not Exists(SELECT * FROM dbo.SupportPage WHERE Culture='')
BEGIN
INSERT INTO SupportPage ([Culture], [HtmlText])
VALUES('', '<p>
At Fellowes-NEATO, one of our top goals is helping you -our customer- find the support and service that you need and deserve. 
Our extensive online support section provides a number of self-service options as well as several ways for you to get in 
touch with us.
</p>
<p>
<b><u>Contact Us</u></b>
<br><br>
Corporate Headquarters<br>
Fellowes, Inc.<br>
1789 Norwood Avenue<br>
Itasca, IL 60143<br>
1-630-893-1600<br>
</p>
<p>
<b>Customer Support</b>
<br>
For website, customer service and technical support, call us Monday through Friday between 7:30 AM and 5:00 PM CT, or send 
an email anytime!  
</p>
Call 1-888-312-7561 or email <a href=mailto:printz@neato.com>printz@neato.com</a>.')
END    

COMMIT TRANSACTION