BEGIN TRANSACTION
EXEC sp_columns @table_name = 'Customer', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
BEGIN
	ALTER TABLE dbo.Customer ADD
		RetailerId int NOT NULL CONSTRAINT DF_Customer_RetailerId DEFAULT 1
END
GO
COMMIT

BEGIN TRANSACTION
if not exists (select * from dbo.sysobjects where id = object_id(N'FK_Customer_Retailers') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
BEGIN
ALTER TABLE dbo.Customer ADD CONSTRAINT
	FK_Customer_Retailers FOREIGN KEY
	(
	RetailerId
	) REFERENCES dbo.Retailers
	(
	Id
	) ON UPDATE CASCADE
	 ON DELETE CASCADE
END
GO
COMMIT

BEGIN TRANSACTION
ALTER TABLE dbo.Customer
	DROP CONSTRAINT CK_Customer
GO
ALTER TABLE dbo.Customer
	DROP CONSTRAINT IX_Customer
GO
ALTER TABLE dbo.Customer ADD CONSTRAINT
	IX_Customer UNIQUE NONCLUSTERED 
	(
	Email,
	RetailerId
	) ON [PRIMARY]

GO
ALTER TABLE dbo.Customer WITH NOCHECK ADD CONSTRAINT
	CK_Customer CHECK (([EmailOptions] = 'H' or [EmailOptions] = 'T'))
GO
COMMIT


BEGIN TRANSACTION

EXEC sp_columns @table_name = 'Tracking_Features', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Tracking_Features ADD
	RetailerId int NOT NULL CONSTRAINT DF_Tracking_Features_RetailerId DEFAULT (1)
END
GO

EXEC sp_columns @table_name = 'Tracking_Images', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Tracking_Images ADD
	RetailerId int NOT NULL CONSTRAINT DF_Tracking_Images_RetailerId DEFAULT (1)
END
GO

EXEC sp_columns @table_name = 'Tracking_Devices', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Tracking_Devices ADD
	RetailerId int NOT NULL CONSTRAINT DF_Tracking_Devices_RetailerId DEFAULT (1)
END
GO

EXEC sp_columns @table_name = 'Tracking_UserAction', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Tracking_UserAction ADD
	RetailerId int NOT NULL CONSTRAINT DF_Tracking_UserAction_RetailerId DEFAULT (1)
END
GO

EXEC sp_columns @table_name = 'DeviceRequest', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.DeviceRequest ADD
	RetailerId int NOT NULL CONSTRAINT DF_DeviceRequest_RetailerId DEFAULT (1)
END
GO

COMMIT TRANSACTION