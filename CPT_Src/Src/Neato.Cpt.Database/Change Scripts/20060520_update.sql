BEGIN TRANSACTION

EXEC sp_columns @table_name = 'Tracking_Features', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Tracking_Features ADD
	RetailerId int NOT NULL CONSTRAINT DF_Tracking_Features_RetailerId DEFAULT (1)
END
GO

EXEC sp_columns @table_name = 'Tracking_Images', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Tracking_Images ADD
	RetailerId int NOT NULL CONSTRAINT DF_Tracking_Images_RetailerId DEFAULT (1)
END
GO

EXEC sp_columns @table_name = 'Tracking_Devices', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Tracking_Devices ADD
	RetailerId int NOT NULL CONSTRAINT DF_Tracking_Devices_RetailerId DEFAULT (1)
END
GO

EXEC sp_columns @table_name = 'Tracking_UserAction', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Tracking_UserAction ADD
	RetailerId int NOT NULL CONSTRAINT DF_Tracking_UserAction_RetailerId DEFAULT (1)
END
GO

EXEC sp_columns @table_name = 'DeviceRequest', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.DeviceRequest ADD
	RetailerId int NOT NULL CONSTRAINT DF_DeviceRequest_RetailerId DEFAULT (1)
END
GO

COMMIT TRANSACTION