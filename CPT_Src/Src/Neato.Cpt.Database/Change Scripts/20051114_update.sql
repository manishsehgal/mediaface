/*
Tables are added:
    - ImageLibFolder
    - ImageLibItem
    - ImageLibFolderItems
    - ImageLibFolderLocalization
    - ImageLibItemKeyword
*/

BEGIN TRANSACTION

-----------------------------------------------

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ImageLibFolder]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
CREATE TABLE dbo.ImageLibFolder
	(
	[FolderId] INT IDENTITY (1, 1) NOT NULL ,
	[ParentFolderId] INT NULL
	)  ON [PRIMARY]
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_ImageLibFolder') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE dbo.ImageLibFolder ADD CONSTRAINT
	PK_ImageLibFolder PRIMARY KEY CLUSTERED 
	(
	FolderId
	) ON [PRIMARY]
GO

-----------------------------------------------

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ImageLibItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
CREATE TABLE dbo.ImageLibItem
	(
	[ItemId] INT IDENTITY (1, 1) NOT NULL,
	[Stream] IMAGE NOT NULL
	)  ON [PRIMARY]
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_ImageLibItem') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE dbo.ImageLibItem ADD CONSTRAINT
	PK_ImageLibItem PRIMARY KEY CLUSTERED 
	(
	ItemId
	) ON [PRIMARY]
GO

-----------------------------------------------

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ImageLibFolderItems]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
CREATE TABLE dbo.ImageLibFolderItems
	(
	[FolderId] INT NOT NULL ,
	[ItemId] INT NOT NULL
	)  ON [PRIMARY]
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_ImageLibFolderItems') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE dbo.ImageLibFolderItems ADD CONSTRAINT
	PK_ImageLibFolderItems PRIMARY KEY CLUSTERED 
	(
	FolderId,
	ItemId
	) ON [PRIMARY]
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'FK_ImageLibFolderItems_ImageLibFolder') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.ImageLibFolderItems ADD CONSTRAINT
	FK_ImageLibFolderItems_ImageLibFolder FOREIGN KEY
	(
	FolderId
	) REFERENCES dbo.ImageLibFolder
	(
	FolderId
	)
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'FK_ImageLibFolderItems_ImageLibItem') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.ImageLibFolderItems ADD CONSTRAINT
	FK_ImageLibFolderItems_ImageLibItem FOREIGN KEY
	(
	ItemId
	) REFERENCES dbo.ImageLibItem
	(
	ItemId
	)
GO

-----------------------------------------------

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ImageLibItemKeyword]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
CREATE TABLE dbo.ImageLibItemKeyword
	(
	[ItemId] INT NOT NULL,
	[Culture] NVARCHAR(10) NOT NULL,
	[Keyword] NVARCHAR(255) NOT NULL
	)  ON [PRIMARY]
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'FK_ImageLibItemKeyword_ImageLibItem') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.ImageLibItemKeyword ADD CONSTRAINT
	FK_ImageLibItemKeyword_ImageLibItem FOREIGN KEY
	(
	ItemId
	) REFERENCES dbo.ImageLibItem
	(
	ItemId
	)
GO

-----------------------------------------------

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ImageLibFolderLocalization]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
CREATE TABLE dbo.ImageLibFolderLocalization
	(
	[FolderId] INT NOT NULL,
	[Culture] NVARCHAR(10) NOT NULL,
	[Caption] NVARCHAR(255) NOT NULL
	)  ON [PRIMARY]
end
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'FK_ImageLibFolderLocalization_ImageLibFolder') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.ImageLibFolderLocalization ADD CONSTRAINT
	FK_ImageLibFolderLocalization_ImageLibFolder FOREIGN KEY
	(
	FolderId
	) REFERENCES dbo.ImageLibFolder
	(
	FolderId
	)
GO

-----------------------------------------------


COMMIT