/*
Fields are renamed and their types are changed: table Paper: PaperW-> Width, PaperH -> Height; type -> float
Field type is changed: table FaceToPaper: FaceX, FaceY -> float
*/

BEGIN TRANSACTION

DECLARE @PaperWExist int
DECLARE @PaperHExist int

EXEC sp_columns @table_name = 'Paper', @table_owner='dbo', @column_name = 'PaperW'
SET @PaperWExist = @@ROWCOUNT

EXEC sp_columns @table_name = 'Paper', @table_owner='dbo', @column_name = 'PaperH'
SET @PaperHExist = @@ROWCOUNT

IF (@PaperWExist <> 0 and @PaperHExist <> 0)
BEGIN
    ALTER TABLE dbo.Paper
	    DROP CONSTRAINT FK_Paper_PaperBrand

    CREATE TABLE dbo.Tmp_Paper
	    (
	    Id int NOT NULL IDENTITY (1, 1),
	    [Name] nvarchar(30) COLLATE Latin1_General_CI_AS NOT NULL,
	    BrandId int NOT NULL,
	    Icon image NULL,
	    Width float(53) NOT NULL,
	    Height float(53) NOT NULL
	    )  ON [PRIMARY]
	    TEXTIMAGE_ON [PRIMARY]

    SET IDENTITY_INSERT dbo.Tmp_Paper ON
    IF EXISTS(SELECT * FROM dbo.Paper)
	    EXEC('INSERT INTO dbo.Tmp_Paper (Id, Name, BrandId, Icon, Width, Height)
		    SELECT Id, Name, BrandId, Icon, CONVERT(float(53), PaperW), CONVERT(float(53), PaperH) FROM dbo.Paper (HOLDLOCK TABLOCKX)')
    SET IDENTITY_INSERT dbo.Tmp_Paper OFF

    ALTER TABLE dbo.FaceToPaper
	    DROP CONSTRAINT FK_FaceToPaper_Paper

    DROP TABLE dbo.Paper

    EXECUTE sp_rename N'dbo.Tmp_Paper', N'Paper', 'OBJECT'

    ALTER TABLE dbo.Paper ADD CONSTRAINT
	    PK_Paper PRIMARY KEY CLUSTERED 
	    (
	    Id
	    ) ON [PRIMARY]

    ALTER TABLE dbo.Paper WITH NOCHECK ADD CONSTRAINT
	    FK_Paper_PaperBrand FOREIGN KEY
	    (
	    BrandId
	    ) REFERENCES dbo.PaperBrand
	    (
	    Id
	    ) ON UPDATE CASCADE
    	
    ALTER TABLE dbo.FaceToPaper WITH NOCHECK ADD CONSTRAINT
	    FK_FaceToPaper_Paper FOREIGN KEY
	    (
	    PaperId
	    ) REFERENCES dbo.Paper
	    (
	    Id
	    ) ON UPDATE CASCADE
END
GO
COMMIT
--------------------------------------

BEGIN TRANSACTION
ALTER TABLE dbo.FaceToPaper
	DROP CONSTRAINT FK_FaceToPaper_Face

ALTER TABLE dbo.FaceToPaper
	DROP CONSTRAINT FK_FaceToPaper_Paper

CREATE TABLE dbo.Tmp_FaceToPaper
	(
	FaceId int NOT NULL,
	PaperId int NOT NULL,
	FaceX float(53) NOT NULL,
	FaceY float(53) NOT NULL
	)  ON [PRIMARY]

IF EXISTS(SELECT * FROM dbo.FaceToPaper)
	 EXEC('INSERT INTO dbo.Tmp_FaceToPaper (FaceId, PaperId, FaceX, FaceY)
		SELECT FaceId, PaperId, CONVERT(float(53), FaceX), CONVERT(float(53), FaceY) FROM dbo.FaceToPaper (HOLDLOCK TABLOCKX)')

DROP TABLE dbo.FaceToPaper

EXECUTE sp_rename N'dbo.Tmp_FaceToPaper', N'FaceToPaper', 'OBJECT'

ALTER TABLE dbo.FaceToPaper WITH NOCHECK ADD CONSTRAINT
	FK_FaceToPaper_Paper FOREIGN KEY
	(
	PaperId
	) REFERENCES dbo.Paper
	(
	Id
	) ON UPDATE CASCADE
	

ALTER TABLE dbo.FaceToPaper WITH NOCHECK ADD CONSTRAINT
	FK_FaceToPaper_Face FOREIGN KEY
	(
	FaceId
	) REFERENCES dbo.Face
	(
	Id
	) ON UPDATE CASCADE
	
GO
COMMIT



 