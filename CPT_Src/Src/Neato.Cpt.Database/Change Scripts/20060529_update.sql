/* Changes
- add column ImageLibFolder.SortOrder
- insert correct data for column ImageLibFolder.SortOrder
*/

BEGIN TRANSACTION

CREATE TABLE #t(x BIT)

-- add column ImageLibFolder.SortOrder
EXEC sp_columns @table_name = 'ImageLibFolder', @table_owner='dbo', @column_name = 'SortOrder'
IF @@ROWCOUNT = 0
BEGIN
    ALTER TABLE dbo.ImageLibFolder ADD	SortOrder INT NOT NULL
    CONSTRAINT DF_ImageLibFolder_SortOrder DEFAULT (0)
    
    INSERT INTO #t VALUES (1)
END
GO

-- insert correct data for column ImageLibFolder.SortOrder
DECLARE @rows INT
SELECT @rows=count(*) FROM #t

IF @rows > 0
BEGIN
    DROP TABLE #t
    UPDATE dbo.ImageLibFolder SET SortOrder = FolderId
END
GO	

COMMIT