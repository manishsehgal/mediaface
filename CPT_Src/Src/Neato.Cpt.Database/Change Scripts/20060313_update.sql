/*
    Remove old procedures:
        - PrPaperEnumByPaperBrand
*/

if EXISTS (SELECT * FROM dbo.sysobjects WHERE [id] = OBJECT_ID(N'[dbo].[PrPaperEnumByPaperBrand]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PrPaperEnumByPaperBrand]
GO
 