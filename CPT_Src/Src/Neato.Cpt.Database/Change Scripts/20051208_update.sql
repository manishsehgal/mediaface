 /* Changes
- data to tables: Device (add face caption to 2 phones)
*/ 

BEGIN TRANSACTION

DECLARE @ptrFaces binary(16)

--- Phone: AudioVox 8940 ---
SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 1
UPDATETEXT Device.Faces @ptrFaces 0 NULL
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
        <Face id="Face 1" x="50" y="50">
            <Contour x="0" y="0">
                <MoveTo x="26.95" y="17.75" />
                <LineTo x="65.55" y="17.70" />
                <LineTo x="84.85" y="17.65" />
                <LineTo x="104.15" y="17.65" />
                <LineTo x="105.70" y="17.65" />
                <CurveTo cx="108.85" cy="17.65" x="108.85" y="14.05" />
                <LineTo x="108.85" y="13.25" />
                <LineTo x="108.85" y="8.75" />
                <LineTo x="108.85" y="6.50" />
                <LineTo x="108.90" y="4.25" />
                <LineTo x="108.90" y="2.90" />
                <CurveTo cx="108.85" cy="0.00" x="112.65" y="0.55" />
                <LineTo x="112.75" y="0.55" />
                <CurveTo cx="121.45" cy="1.85" x="124.70" y="7.05" />
                <CurveTo cx="127.90" cy="12.15" x="127.90" y="24.60" />
                <LineTo x="127.90" y="106.05" />
                <CurveTo cx="127.90" cy="131.40" x="126.25" y="154.25" />
                <CurveTo cx="124.55" cy="177.60" x="120.90" y="202.10" />
                <CurveTo cx="120.30" cy="205.95" x="119.80" y="208.45" />
                <CurveTo cx="117.45" cy="219.40" x="112.00" y="224.65" />
                <CurveTo cx="105.75" cy="230.60" x="92.35" y="232.55" />
                <LineTo x="35.00" y="232.55" />
                <CurveTo cx="21.25" cy="230.95" x="14.90" y="225.65" />
                <CurveTo cx="9.25" cy="221.00" x="6.80" y="210.90" />
                <CurveTo cx="2.50" cy="193.00" x="0.95" y="168.40" />
                <CurveTo cx="0.35" cy="158.50" x="0.15" y="146.50" />
                <CurveTo cx="0.00" cy="137.85" x="0.00" y="125.25" />
                <LineTo x="0.15" y="74.45" />
                <CurveTo cx="0.20" cy="46.10" x="0.20" y="23.10" />
                <CurveTo cx="0.20" cy="10.55" x="4.45" y="5.60" />
                <CurveTo cx="8.30" cy="1.15" x="18.30" y="0.25" />
                <CurveTo cx="21.25" cy="0.00" x="22.05" y="0.45" />
                <CurveTo cx="23.15" cy="1.15" x="23.15" y="4.05" />
                <LineTo x="23.15" y="13.30" />
                <CurveTo cx="23.15" cy="13.95" x="23.10" y="14.95" />
                <CurveTo cx="23.05" cy="17.75" x="26.80" y="17.75" />
                <LineTo x="26.95" y="17.75" />

                <MoveTo x="35.15" y="67.75" />
                <LineTo x="91.95" y="67.75" />
                <CurveTo cx="96.25" cy="67.75" x="96.25" y="72.20" />
                <LineTo x="96.25" y="111.85" />
                <CurveTo cx="96.25" cy="116.25" x="91.95" y="116.25" />
                <LineTo x="35.15" y="116.25" />
                <CurveTo cx="30.90" cy="116.25" x="30.90" y="111.85" />
                <LineTo x="30.90" y="72.20" />
                <CurveTo cx="30.90" cy="67.75" x="35.15" y="67.75" />

                <MoveTo x="63.35" y="186.95" />
                <CurveTo cx="68.55" cy="186.95" x="72.20" y="183.25" />
                <CurveTo cx="75.90" cy="179.55" x="75.90" y="174.35" />
                <CurveTo cx="75.90" cy="169.20" x="72.20" y="165.50" />
                <CurveTo cx="68.55" cy="161.80" x="63.35" y="161.80" />
                <CurveTo cx="58.15" cy="161.80" x="54.45" y="165.50" />
                <CurveTo cx="50.75" cy="169.15" x="50.75" y="174.35" />
                <CurveTo cx="50.75" cy="179.55" x="54.45" y="183.25" />
                <CurveTo cx="58.15" cy="186.95" x="63.35" y="186.95" />

                <MoveTo x="45.45" y="206.60" />
                <LineTo x="81.85" y="206.60" />
                <CurveTo cx="83.85" cy="206.60" x="85.25" y="208.00" />
                <CurveTo cx="86.65" cy="209.40" x="86.65" y="211.40" />
                <CurveTo cx="86.65" cy="213.40" x="85.25" y="214.80" />
                <CurveTo cx="83.85" cy="216.20" x="81.85" y="216.20" />
                <LineTo x="45.45" y="216.20" />
                <CurveTo cx="43.45" cy="216.20" x="42.05" y="214.80" />
                <CurveTo cx="40.65" cy="213.40" x="40.65" y="211.40" />
                <CurveTo cx="40.65" cy="209.40" x="42.05" y="208.00" />
                <CurveTo cx="43.45" cy="206.60" x="45.45" y="206.60" />

            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Front"/>
                    <Culture key="en" value="Front"/>
                    <Culture key="ru" value="&#x0424;&#x0430;&#x0441;&#x0430;&#x0434;"/>
                </Caption>
            </Localization>            
        </Face>
        <Face id="Face 2" x="300" y="50">
            <Contour x="0" y="0">
                <MoveTo x="13.50" y="203.80" />
                <LineTo x="15.95" y="203.75" />
                <LineTo x="19.60" y="203.65" />
                <LineTo x="23.05" y="203.55" />
                <CurveTo cx="28.90" cy="203.45" x="32.60" y="203.55" />
                <CurveTo cx="38.15" cy="203.70" x="42.10" y="204.35" />
                <CurveTo cx="45.35" cy="204.90" x="45.20" y="208.85" />
                <CurveTo cx="45.15" cy="209.65" x="45.05" y="210.80" />
                <CurveTo cx="44.80" cy="213.10" x="46.20" y="214.30" />
                <LineTo x="84.60" y="215.30" />
                <CurveTo cx="87.95" cy="212.25" x="87.60" y="207.45" />
                <CurveTo cx="87.30" cy="203.45" x="91.80" y="203.75" />
                <CurveTo cx="93.45" cy="203.85" x="94.45" y="203.85" />
                <LineTo x="103.10" y="203.95" />
                <LineTo x="111.90" y="204.05" />
                <CurveTo cx="112.85" cy="204.05" x="114.85" y="203.90" />
                <CurveTo cx="122.70" cy="203.40" x="122.70" y="207.60" />
                <CurveTo cx="122.70" cy="221.05" x="111.75" y="224.90" />
                <CurveTo cx="105.10" cy="227.25" x="86.70" y="226.85" />
                <CurveTo cx="81.05" cy="226.70" x="78.25" y="226.70" />
                <LineTo x="51.65" y="226.70" />
                <CurveTo cx="50.05" cy="226.60" x="45.70" y="226.50" />
                <CurveTo cx="38.25" cy="226.30" x="34.05" y="225.95" />
                <CurveTo cx="26.85" cy="225.30" x="21.65" y="223.70" />
                <CurveTo cx="7.60" cy="219.35" x="5.95" y="207.60" />
                <CurveTo cx="5.95" cy="203.75" x="11.30" y="203.80" />
                <CurveTo cx="12.70" cy="203.85" x="13.50" y="203.80" />
                <MoveTo x="53.80" y="0.05" />
                <LineTo x="107.35" y="0.05" />
                <LineTo x="109.25" y="0.05" />
                <CurveTo cx="116.55" cy="0.00" x="118.80" y="1.05" />
                <CurveTo cx="122.35" cy="2.70" x="122.35" y="8.55" />
                <LineTo x="122.35" y="38.60" />
                <CurveTo cx="122.35" cy="44.25" x="121.30" y="45.30" />
                <CurveTo cx="120.20" cy="46.40" x="114.50" y="46.40" />
                <LineTo x="52.35" y="46.40" />
                <CurveTo cx="47.25" cy="46.40" x="45.85" y="45.20" />
                <CurveTo cx="44.30" cy="43.90" x="44.30" y="39.15" />
                <LineTo x="44.30" y="6.65" />
                <CurveTo cx="44.30" cy="0.05" x="53.80" y="0.05" />
                <MoveTo x="9.80" y="51.50" />
                <LineTo x="120.90" y="51.50" />
                <CurveTo cx="131.05" cy="51.50" x="131.05" y="61.50" />
                <CurveTo cx="131.05" cy="77.30" x="130.30" y="96.80" />
                <CurveTo cx="129.85" cy="108.45" x="128.65" y="131.70" />
                <CurveTo cx="127.55" cy="152.25" x="127.10" y="162.55" />
                <CurveTo cx="126.40" cy="179.75" x="126.25" y="193.65" />
                <CurveTo cx="126.20" cy="199.40" x="122.65" y="201.00" />
                <CurveTo cx="120.40" cy="202.05" x="113.20" y="201.95" />
                <LineTo x="110.70" y="201.95" />
                <LineTo x="17.85" y="201.95" />
                <LineTo x="16.75" y="201.95" />
                <CurveTo cx="10.65" cy="201.95" x="8.50" y="201.15" />
                <CurveTo cx="5.00" cy="199.85" x="4.85" y="195.45" />
                <LineTo x="0.00" y="60.55" />
                <CurveTo cx="0.00" cy="51.50" x="9.80" y="51.50" />

                <MoveTo x="84.45" y="5.00" />
                <LineTo x="113.15" y="5.00" />
                <CurveTo cx="117.10" cy="5.00" x="117.10" y="9.00" />
                <LineTo x="117.10" y="39.20" />
                <CurveTo cx="117.10" cy="43.20" x="113.15" y="43.20" />
                <LineTo x="84.45" y="43.20" />
                <CurveTo cx="80.45" cy="43.20" x="80.45" y="39.20" />
                <LineTo x="80.45" y="9.00" />
                <CurveTo cx="80.45" cy="5.00" x="84.45" y="5.00" />

            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Back"/>
                    <Culture key="en" value="Back"/>
                    <Culture key="ru" value="&#x041E;&#x0431;&#x043E;&#x0440;&#x043E;&#x0442;"/>
                </Caption>
            </Localization>            
            
        </Face>
        <Face id="Face 3" x="50" y="420">
            <Contour x="0" y="0">
                <MoveTo x="42.40" y="0.00" />
                <LineTo x="76.50" y="0.00" />
                <CurveTo cx="97.80" cy="0.95" x="107.90" y="5.10" />
                <CurveTo cx="118.15" cy="9.50" x="120.45" y="23.50" />
                <LineTo x="124.20" y="72.95" />
                <CurveTo cx="125.25" cy="86.90" x="125.25" y="102.50" />
                <LineTo x="125.25" y="197.10" />
                <LineTo x="125.25" y="200.65" />
                <CurveTo cx="125.35" cy="217.20" x="123.20" y="223.05" />
                <CurveTo cx="119.70" cy="232.65" x="108.15" y="232.65" />
                <LineTo x="107.20" y="232.65" />
                <CurveTo cx="104.20" cy="232.65" x="104.35" y="230.35" />
                <CurveTo cx="104.40" cy="229.30" x="104.40" y="228.60" />
                <LineTo x="104.45" y="220.70" />
                <LineTo x="104.50" y="212.75" />
                <CurveTo cx="104.50" cy="210.20" x="102.60" y="210.25" />
                <CurveTo cx="101.75" cy="210.25" x="101.25" y="210.25" />
                <LineTo x="24.40" y="209.95" />
                <CurveTo cx="19.95" cy="209.95" x="20.00" y="214.05" />
                <LineTo x="20.05" y="216.50" />
                <CurveTo cx="20.15" cy="224.65" x="20.00" y="228.75" />
                <LineTo x="20.00" y="228.90" />
                <CurveTo cx="19.95" cy="232.65" x="16.30" y="232.65" />
                <LineTo x="15.30" y="232.65" />
                <LineTo x="14.45" y="232.60" />
                <LineTo x="13.55" y="232.55" />
                <LineTo x="12.55" y="232.55" />
                <CurveTo cx="6.55" cy="232.55" x="3.75" y="225.00" />
                <CurveTo cx="1.80" cy="219.70" x="0.65" y="207.20" />
                <CurveTo cx="0.05" cy="200.55" x="0.00" y="191.70" />
                <CurveTo cx="0.00" cy="186.60" x="0.20" y="177.65" />
                <CurveTo cx="0.35" cy="172.35" x="0.35" y="170.40" />
                <LineTo x="0.45" y="134.15" />
                <LineTo x="0.55" y="113.30" />
                <LineTo x="0.60" y="92.90" />
                <CurveTo cx="0.70" cy="77.50" x="1.70" y="62.65" />
                <LineTo x="2.75" y="48.45" />
                <LineTo x="3.25" y="41.35" />
                <LineTo x="3.75" y="34.25" />
                <CurveTo cx="4.30" cy="25.25" x="5.40" y="20.55" />
                <CurveTo cx="6.80" cy="14.70" x="9.80" y="11.40" />
                <CurveTo cx="17.15" cy="2.10" x="42.40" y="0.00" />

                <MoveTo x="57.85" y="11.20" />
                <LineTo x="66.60" y="11.20" />
                <CurveTo cx="67.45" cy="11.20" x="68.05" y="11.80" />
                <CurveTo cx="68.65" cy="12.40" x="68.65" y="13.25" />
                <CurveTo cx="68.65" cy="14.10" x="68.05" y="14.65" />
                <CurveTo cx="67.45" cy="15.25" x="66.60" y="15.25" />
                <LineTo x="57.85" y="15.25" />
                <CurveTo cx="57.05" cy="15.25" x="56.45" y="14.65" />
                <CurveTo cx="55.85" cy="14.10" x="55.85" y="13.25" />
                <CurveTo cx="55.85" cy="12.40" x="56.45" y="11.80" />
                <CurveTo cx="57.00" cy="11.20" x="57.85" y="11.20" />

                <MoveTo x="39.25" y="22.20" />
                <LineTo x="85.40" y="22.20" />
                <CurveTo cx="87.40" cy="22.20" x="88.75" y="23.60" />
                <CurveTo cx="90.15" cy="25.00" x="90.15" y="26.95" />
                <CurveTo cx="90.15" cy="28.90" x="88.75" y="30.30" />
                <CurveTo cx="87.40" cy="31.70" x="85.40" y="31.70" />
                <LineTo x="39.25" y="31.70" />
                <CurveTo cx="37.30" cy="31.70" x="35.90" y="30.30" />
                <CurveTo cx="34.50" cy="28.90" x="34.50" y="26.95" />
                <CurveTo cx="34.50" cy="25.00" x="35.90" y="23.60" />
                <CurveTo cx="37.30" cy="22.20" x="39.25" y="22.20" />

                <MoveTo x="23.80" y="59.75" />
                <LineTo x="100.40" y="59.75" />
                <CurveTo cx="107.65" cy="59.75" x="107.65" y="67.00" />
                <LineTo x="107.65" y="163.90" />
                <CurveTo cx="107.65" cy="171.20" x="100.40" y="171.20" />
                <LineTo x="23.80" y="171.20" />
                <CurveTo cx="16.50" cy="171.20" x="16.50" y="163.90" />
                <LineTo x="16.50" y="67.00" />
                <CurveTo cx="16.50" cy="59.75" x="23.80" y="59.75" />

            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Inside 1"/>
                    <Culture key="en" value="Inside 1"/>
                    <Culture key="ru" value="&#x0412;&#x043D;&#x0443;&#x0442;&#x0440;&#x0438;&#x00A0;&#x0031;"/>
                </Caption>
            </Localization>            
            
        </Face>
        <Face id="Face 4" x="300" y="420">
            <Contour x="0" y="0">
                <MoveTo x="19.85" y="0.00" />
                <LineTo x="107.10" y="0.00" />
                <CurveTo cx="121.55" cy="0.00" x="127.85" y="6.05" />
                <CurveTo cx="134.30" cy="12.20" x="134.30" y="26.65" />
                <CurveTo cx="134.30" cy="42.50" x="133.90" y="62.25" />
                <CurveTo cx="133.70" cy="74.15" x="133.10" y="97.85" />
                <CurveTo cx="132.45" cy="122.55" x="132.25" y="134.90" />
                <CurveTo cx="131.85" cy="155.50" x="131.90" y="172.00" />
                <CurveTo cx="131.95" cy="195.90" x="126.05" y="204.85" />
                <CurveTo cx="118.30" cy="216.65" x="95.65" y="215.40" />
                <LineTo x="61.10" y="215.20" />
                <LineTo x="57.40" y="215.20" />
                <CurveTo cx="49.50" cy="215.15" x="45.55" y="215.00" />
                <CurveTo cx="38.95" cy="214.75" x="33.75" y="213.95" />
                <CurveTo cx="19.40" cy="211.70" x="13.20" y="205.70" />
                <CurveTo cx="5.25" cy="199.90" x="2.95" y="187.40" />
                <CurveTo cx="1.35" cy="179.05" x="1.35" y="156.90" />
                <CurveTo cx="1.35" cy="138.65" x="1.15" y="119.75" />
                <CurveTo cx="1.00" cy="108.40" x="0.70" y="87.25" />
                <CurveTo cx="0.35" cy="65.80" x="0.20" y="54.20" />
                <CurveTo cx="0.00" cy="34.85" x="0.00" y="16.00" />
                <CurveTo cx="0.00" cy="8.70" x="6.10" y="4.20" />
                <CurveTo cx="11.75" cy="0.00" x="19.85" y="0.00" />

                <MoveTo x="66.90" y="202.05" />
                <CurveTo cx="67.40" cy="202.05" x="67.75" y="202.45" />
                <CurveTo cx="68.15" cy="202.80" x="68.15" y="203.30" />
                <LineTo x="68.15" y="209.85" />
                <CurveTo cx="68.15" cy="210.35" x="67.75" y="210.75" />
                <CurveTo cx="67.40" cy="211.10" x="66.90" y="211.10" />
                <CurveTo cx="66.40" cy="211.10" x="66.00" y="210.75" />
                <CurveTo cx="65.65" cy="210.40" x="65.65" y="209.85" />
                <LineTo x="65.65" y="203.30" />
                <CurveTo cx="65.65" cy="202.80" x="66.00" y="202.45" />
                <CurveTo cx="66.40" cy="202.05" x="66.90" y="202.05" />

                <MoveTo x="52.80" y="176.45" />
                <CurveTo cx="67.55" cy="177.25" x="80.75" y="176.45" />
                <LineTo x="80.75" y="194.40" />
                <CurveTo cx="66.45" cy="195.40" x="52.80" y="194.40" />
                <LineTo x="52.80" y="176.45" />

                <MoveTo x="51.90" y="148.10" />
                <CurveTo cx="65.45" cy="149.35" x="81.35" y="148.10" />
                <LineTo x="81.35" y="166.15" />
                <CurveTo cx="66.75" cy="167.55" x="51.90" y="166.15" />
                <LineTo x="51.90" y="148.10" />

                <MoveTo x="51.90" y="120.10" />
                <CurveTo cx="65.55" cy="121.35" x="81.55" y="120.10" />
                <LineTo x="81.55" y="138.30" />
                <CurveTo cx="66.50" cy="139.65" x="51.90" y="138.30" />
                <LineTo x="51.90" y="120.10" />

                <MoveTo x="51.90" y="92.25" />
                <CurveTo cx="65.80" cy="93.55" x="82.05" y="92.25" />
                <LineTo x="82.05" y="111.20" />
                <CurveTo cx="66.75" cy="112.65" x="51.90" y="111.20" />
                <LineTo x="51.90" y="92.25" />

                <MoveTo x="16.95" y="172.70" />
                <LineTo x="42.70" y="174.25" />
                <LineTo x="42.70" y="192.30" />
                <LineTo x="39.10" y="192.30" />
                <CurveTo cx="19.95" cy="191.25" x="16.95" y="180.70" />
                <LineTo x="16.95" y="172.70" />

                <MoveTo x="115.50" y="173.65" />
                <LineTo x="90.05" y="175.30" />
                <LineTo x="90.05" y="192.15" />
                <LineTo x="93.65" y="192.15" />
                <CurveTo cx="109.55" cy="191.35" x="114.15" y="184.05" />
                <CurveTo cx="115.10" cy="182.55" x="115.35" y="180.95" />
                <CurveTo cx="115.50" cy="179.95" x="115.50" y="177.75" />
                <LineTo x="115.50" y="176.90" />
                <LineTo x="115.50" y="173.65" />

                <MoveTo x="117.85" y="145.35" />
                <LineTo x="90.55" y="148.00" />
                <LineTo x="90.55" y="165.60" />
                <LineTo x="116.45" y="162.90" />
                <LineTo x="117.85" y="145.35" />

                <MoveTo x="118.25" y="116.70" />
                <LineTo x="91.50" y="119.45" />
                <LineTo x="91.50" y="137.55" />
                <LineTo x="117.45" y="134.75" />
                <LineTo x="118.25" y="116.70" />

                <MoveTo x="92.10" y="91.95" />
                <LineTo x="109.80" y="91.30" />
                <LineTo x="110.20" y="91.30" />
                <CurveTo cx="113.35" cy="91.15" x="114.45" y="91.60" />
                <CurveTo cx="116.05" cy="92.20" x="117.50" y="94.80" />
                <CurveTo cx="120.00" cy="99.55" x="119.20" y="108.00" />
                <LineTo x="92.10" y="110.40" />
                <LineTo x="92.10" y="91.95" />

                <MoveTo x="41.55" y="91.90" />
                <LineTo x="24.30" y="91.25" />
                <LineTo x="23.85" y="91.25" />
                <CurveTo cx="20.85" cy="91.15" x="19.80" y="91.60" />
                <CurveTo cx="18.20" cy="92.20" x="16.85" y="94.70" />
                <CurveTo cx="14.40" cy="99.15" x="15.20" y="107.40" />
                <LineTo x="41.55" y="109.70" />
                <LineTo x="41.55" y="91.90" />

                <MoveTo x="14.60" y="116.90" />
                <LineTo x="41.45" y="119.65" />
                <LineTo x="41.45" y="137.70" />
                <LineTo x="15.05" y="134.85" />
                <LineTo x="14.60" y="116.90" />

                <MoveTo x="16.10" y="145.05" />
                <LineTo x="42.00" y="147.70" />
                <LineTo x="42.00" y="165.45" />
                <LineTo x="16.10" y="162.60" />
                <LineTo x="16.10" y="145.05" />

                <MoveTo x="33.40" y="25.25" />
                <LineTo x="33.85" y="58.50" />
                <CurveTo cx="36.50" cy="65.15" x="40.90" y="66.50" />
                <LineTo x="40.90" y="79.60" />
                <CurveTo cx="29.25" cy="80.45" x="22.55" y="78.15" />
                <CurveTo cx="15.20" cy="75.60" x="13.60" y="69.10" />
                <LineTo x="13.60" y="35.00" />
                <CurveTo cx="16.00" cy="28.75" x="21.70" y="26.45" />
                <CurveTo cx="26.30" cy="24.60" x="33.40" y="25.25" />

                <MoveTo x="100.45" y="25.55" />
                <LineTo x="100.05" y="58.80" />
                <CurveTo cx="97.45" cy="65.50" x="93.25" y="66.75" />
                <LineTo x="93.25" y="79.90" />
                <CurveTo cx="104.50" cy="80.75" x="110.90" y="78.40" />
                <CurveTo cx="117.95" cy="75.85" x="119.55" y="69.40" />
                <LineTo x="119.55" y="35.30" />
                <CurveTo cx="117.25" cy="29.00" x="111.75" y="26.75" />
                <CurveTo cx="107.30" cy="24.90" x="100.45" y="25.55" />

                <MoveTo x="54.10" y="69.70" />
                <LineTo x="79.30" y="69.70" />
                <CurveTo cx="81.00" cy="69.70" x="82.25" y="70.80" />
                <CurveTo cx="83.50" cy="71.90" x="83.50" y="73.45" />
                <LineTo x="83.50" y="77.70" />
                <CurveTo cx="83.50" cy="79.25" x="82.25" y="80.35" />
                <CurveTo cx="81.00" cy="81.45" x="79.30" y="81.45" />
                <LineTo x="54.10" y="81.45" />
                <CurveTo cx="52.40" cy="81.45" x="51.15" y="80.35" />
                <CurveTo cx="49.90" cy="79.25" x="49.90" y="77.70" />
                <LineTo x="49.90" y="73.45" />
                <CurveTo cx="49.90" cy="71.90" x="51.15" y="70.80" />
                <CurveTo cx="52.40" cy="69.70" x="54.10" y="69.70" />

                <MoveTo x="50.45" y="12.05" />
                <LineTo x="85.00" y="12.05" />
                <CurveTo cx="93.70" cy="12.05" x="93.70" y="21.50" />
                <LineTo x="93.70" y="52.95" />
                <CurveTo cx="93.70" cy="62.40" x="85.00" y="62.40" />
                <LineTo x="50.45" y="62.40" />
                <CurveTo cx="41.75" cy="62.40" x="41.75" y="52.95" />
                <LineTo x="41.75" y="21.50" />
                <CurveTo cx="41.75" cy="12.05" x="50.45" y="12.05" />

            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Inside 2"/>
                    <Culture key="en" value="Inside 2"/>
                    <Culture key="ru" value="&#x0412;&#x043D;&#x0443;&#x0442;&#x0440;&#x0438;&#x00A0;&#x0032;"/>
                </Caption>
            </Localization>            
            
        </Face>
    </Faces>
    <Paper w="597.5" h="845.05"/> 
    <Properties name="AudioVox 8940"/>
</Project>
'

--- Phone: Motorola v180 ---
SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE [Id] = 11
UPDATETEXT Device.Faces @ptrFaces 0 NULL
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
        <Face id="Face1" x="83.00" y="77.00">
            <Contour x="0" y="0">
                <MoveTo x="28.20" y="0.00" />
                <LineTo x="28.10" y="29.65" />
                <LineTo x="27.95" y="30.10" />
                <LineTo x="27.95" y="30.25" />
                <LineTo x="21.50" y="30.85" />
                <LineTo x="16.80" y="32.40" />
                <LineTo x="15.00" y="33.35" />
                <LineTo x="8.15" y="38.65" />
                <LineTo x="2.65" y="41.40" />
                <LineTo x="0.00" y="42.25" />
                <LineTo x="0.00" y="43.70" />
                <LineTo x="0.15" y="44.65" />
                <LineTo x="0.25" y="55.95" />
                <LineTo x="0.35" y="56.75" />
                <LineTo x="0.60" y="80.65" />
                <LineTo x="2.55" y="126.00" />
                <LineTo x="3.60" y="142.20" />
                <LineTo x="6.95" y="173.30" />
                <LineTo x="7.80" y="177.35" />
                <LineTo x="9.25" y="181.10" />
                <LineTo x="11.30" y="184.20" />
                <LineTo x="13.80" y="186.95" />
                <LineTo x="18.35" y="190.10" />
                <LineTo x="20.15" y="190.80" />
                <LineTo x="23.90" y="191.75" />
                <LineTo x="27.95" y="192.10" />
                <LineTo x="29.90" y="199.55" />
                <LineTo x="33.00" y="206.55" />
                <LineTo x="34.95" y="209.65" />
                <LineTo x="39.75" y="215.40" />
                <LineTo x="45.35" y="220.35" />
                <LineTo x="51.95" y="224.15" />
                <LineTo x="59.05" y="226.80" />
                <LineTo x="62.75" y="227.75" />
                <LineTo x="70.70" y="228.50" />
                <LineTo x="78.60" y="227.75" />
                <LineTo x="85.95" y="225.60" />
                <LineTo x="92.75" y="222.35" />
                <LineTo x="98.90" y="218.05" />
                <LineTo x="104.15" y="212.65" />
                <LineTo x="108.35" y="206.55" />
                <LineTo x="111.50" y="199.55" />
                <LineTo x="113.40" y="192.10" />
                <LineTo x="117.50" y="191.75" />
                <LineTo x="121.20" y="190.80" />
                <LineTo x="123.00" y="190.10" />
                <LineTo x="124.70" y="189.10" />
                <LineTo x="127.55" y="186.95" />
                <LineTo x="130.10" y="184.20" />
                <LineTo x="132.15" y="181.10" />
                <LineTo x="133.55" y="177.35" />
                <LineTo x="134.40" y="173.30" />
                <LineTo x="136.35" y="158.05" />
                <LineTo x="137.75" y="142.20" />
                <LineTo x="139.80" y="110.15" />
                <LineTo x="140.90" y="42.10" />
                <LineTo x="138.35" y="41.30" />
                <LineTo x="133.10" y="38.65" />
                <LineTo x="127.95" y="34.45" />
                <LineTo x="124.55" y="32.40" />
                <LineTo x="119.90" y="30.85" />
                <LineTo x="113.40" y="30.25" />
                <LineTo x="113.40" y="30.10" />
                <LineTo x="113.30" y="29.65" />
                <LineTo x="113.15" y="0.00" />
                <LineTo x="28.20" y="0.00" />
                <MoveTo x="60.35" y="46.20" />
                <LineTo x="61.45" y="48.10" />
                <LineTo x="62.75" y="49.80" />
                <LineTo x="64.45" y="51.10" />
                <LineTo x="66.25" y="52.10" />
                <LineTo x="68.30" y="52.70" />
                <LineTo x="70.45" y="52.95" />
                <LineTo x="72.50" y="52.70" />
                <LineTo x="74.55" y="52.10" />
                <LineTo x="76.45" y="51.10" />
                <LineTo x="78.00" y="49.80" />
                <LineTo x="79.35" y="48.10" />
                <LineTo x="80.40" y="46.20" />
                <LineTo x="81.00" y="44.30" />
                <LineTo x="81.25" y="42.10" />
                <LineTo x="81.00" y="40.10" />
                <LineTo x="80.40" y="38.05" />
                <LineTo x="79.35" y="36.10" />
                <LineTo x="78.00" y="34.45" />
                <LineTo x="76.45" y="33.10" />
                <LineTo x="74.55" y="32.15" />
                <LineTo x="72.50" y="31.55" />
                <LineTo x="70.45" y="31.35" />
                <LineTo x="68.30" y="31.55" />
                <LineTo x="66.25" y="32.15" />
                <LineTo x="64.45" y="33.10" />
                <LineTo x="62.75" y="34.45" />
                <LineTo x="61.45" y="36.10" />
                <LineTo x="60.35" y="38.05" />
                <LineTo x="59.75" y="40.10" />
                <LineTo x="59.75" y="44.30" />
                <LineTo x="60.35" y="46.20" />
                <MoveTo x="34.80" y="86.90" />
                <LineTo x="34.45" y="86.05" />
                <LineTo x="34.45" y="66.25" />
                <LineTo x="34.80" y="65.40" />
                <LineTo x="35.75" y="63.95" />
                <LineTo x="37.10" y="63.00" />
                <LineTo x="37.95" y="62.75" />
                <LineTo x="38.90" y="62.65" />
                <LineTo x="101.90" y="62.65" />
                <LineTo x="102.85" y="62.75" />
                <LineTo x="103.70" y="63.00" />
                <LineTo x="105.15" y="63.95" />
                <LineTo x="106.10" y="65.40" />
                <LineTo x="106.35" y="66.25" />
                <LineTo x="106.45" y="67.20" />
                <LineTo x="106.45" y="85.20" />
                <LineTo x="106.35" y="86.05" />
                <LineTo x="106.10" y="86.90" />
                <LineTo x="105.15" y="88.35" />
                <LineTo x="103.70" y="89.30" />
                <LineTo x="102.85" y="89.55" />
                <LineTo x="101.90" y="89.65" />
                <LineTo x="38.90" y="89.65" />
                <LineTo x="37.95" y="89.55" />
                <LineTo x="37.10" y="89.30" />
                <LineTo x="35.75" y="88.35" />
                <LineTo x="34.80" y="86.90" />
            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Front"/>
                    <Culture key="en" value="Front"/>
                    <Culture key="ru" value="&#x0424;&#x0430;&#x0441;&#x0430;&#x0434;"/>
                </Caption>
            </Localization>            
        </Face>
        <Face id="Face2" x="392.536" y="123.333">
            <Contour x="0" y="0">
                <MoveTo x="1.30" y="1.45" />
                <LineTo x="0.35" y="3.00" />
                <LineTo x="0.00" y="4.70" />
                <LineTo x="0.10" y="126.00" />
                <LineTo x="0.45" y="128.30" />
                <LineTo x="1.80" y="132.60" />
                <LineTo x="2.75" y="134.65" />
                <LineTo x="3.95" y="136.55" />
                <LineTo x="6.70" y="140.05" />
                <LineTo x="8.25" y="141.60" />
                <LineTo x="12.00" y="144.15" />
                <LineTo x="16.05" y="145.90" />
                <LineTo x="18.20" y="146.55" />
                <LineTo x="22.90" y="147.00" />
                <LineTo x="23.85" y="152.55" />
                <LineTo x="25.55" y="157.90" />
                <LineTo x="27.80" y="162.95" />
                <LineTo x="30.70" y="167.65" />
                <LineTo x="38.00" y="175.70" />
                <LineTo x="42.35" y="179.05" />
                <LineTo x="47.00" y="181.80" />
                <LineTo x="47.25" y="179.90" />
                <LineTo x="47.90" y="178.10" />
                <LineTo x="48.75" y="176.40" />
                <LineTo x="49.85" y="174.95" />
                <LineTo x="51.25" y="173.75" />
                <LineTo x="52.85" y="172.90" />
                <LineTo x="54.65" y="172.35" />
                <LineTo x="56.55" y="172.10" />
                <LineTo x="76.60" y="172.10" />
                <LineTo x="78.50" y="172.35" />
                <LineTo x="80.30" y="172.90" />
                <LineTo x="81.85" y="173.75" />
                <LineTo x="83.30" y="174.95" />
                <LineTo x="84.40" y="176.40" />
                <LineTo x="85.35" y="178.10" />
                <LineTo x="85.85" y="179.90" />
                <LineTo x="86.05" y="181.80" />
                <LineTo x="90.75" y="179.05" />
                <LineTo x="95.05" y="175.70" />
                <LineTo x="99.05" y="171.85" />
                <LineTo x="102.40" y="167.65" />
                <LineTo x="105.25" y="162.95" />
                <LineTo x="107.55" y="157.90" />
                <LineTo x="109.25" y="152.55" />
                <LineTo x="110.20" y="147.00" />
                <LineTo x="114.85" y="146.55" />
                <LineTo x="117.05" y="145.90" />
                <LineTo x="121.10" y="144.15" />
                <LineTo x="124.85" y="141.60" />
                <LineTo x="127.95" y="138.35" />
                <LineTo x="130.35" y="134.65" />
                <LineTo x="132.15" y="130.45" />
                <LineTo x="133.00" y="126.00" />
                <LineTo x="133.10" y="4.70" />
                <LineTo x="132.75" y="3.00" />
                <LineTo x="131.80" y="1.45" />
                <LineTo x="130.35" y="0.35" />
                <LineTo x="128.55" y="0.00" />
                <LineTo x="104.80" y="0.25" />
                <LineTo x="102.25" y="0.75" />
                <LineTo x="98.45" y="3.00" />
                <LineTo x="89.90" y="10.10" />
                <LineTo x="85.60" y="12.95" />
                <CurveTo cx="83.40" cy="13.95" x="81.15" y="15.00" />
                <LineTo x="76.45" y="16.45" />
                <LineTo x="71.55" y="17.30" />
                <LineTo x="66.65" y="17.55" />
                <LineTo x="61.60" y="17.30" />
                <LineTo x="56.65" y="16.45" />
                <LineTo x="52.00" y="15.00" />
                <LineTo x="47.55" y="12.95" />
                <LineTo x="45.20" y="11.55" />
                <LineTo x="33.35" y="2.05" />
                <LineTo x="30.80" y="0.75" />
                <LineTo x="25.80" y="0.00" />
                <LineTo x="4.55" y="0.00" />
                <LineTo x="2.75" y="0.35" />
                <LineTo x="1.30" y="1.45" />
            </Contour>
            <Localization>
                <Caption>
                    <Culture key="" value="Back"/>
                    <Culture key="en" value="Back"/>
                    <Culture key="ru" value="&#x041E;&#x0431;&#x043E;&#x0440;&#x043E;&#x0442;"/>
                </Caption>
            </Localization>            
        </Face>
    </Faces>
    <Paper w="612" h="396"/> 
    <Properties name="Motorola v180"/>
</Project>
'

go

COMMIT TRANSACTION