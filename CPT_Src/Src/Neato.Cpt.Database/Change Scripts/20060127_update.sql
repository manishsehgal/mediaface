/*
 Phone: Motorola v180 - contour and position updated
 Phone: Motorola Razor V3 - contour and position updated
    
*/
DECLARE @ptrContour binary(16)

BEGIN TRANSACTION
--- Phone: Motorola v180 ---
SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 1
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
    <MoveTo x="113.3" y="30.3" />
    <LineTo x="113.15" y="0" />
    <LineTo x="28.15" y="0" />
    <LineTo x="27.95" y="30.3" />
    <CurveTo cx="21.3" cy="30.3" x="16.85" y="32.4" />
    <CurveTo cx="14.2" cy="33.65" x="10.65" y="36.8" />
    <CurveTo cx="6.5" cy="40.65" x="0" y="42.25" />
    <LineTo x="0.25" y="57.4" />
    <CurveTo cx="0.25" cy="124.4" x="6.85" y="173.4" />
    <CurveTo cx="8" cy="181.9" x="13.7" y="187" />
    <CurveTo cx="19.45" cy="192.15" x="28" y="192.15" />
    <CurveTo cx="30.3" cy="207.65" x="42.45" y="218.05" />
    <CurveTo cx="54.6" cy="228.5" x="70.65" y="228.5" />
    <CurveTo cx="86.7" cy="228.5" x="98.9" y="218.05" />
    <CurveTo cx="111" cy="207.65" x="113.3" y="192.15" />
    <CurveTo cx="121.8" cy="192.15" x="127.55" y="187" />
    <CurveTo cx="133.25" cy="181.9" x="134.4" y="173.4" />
    <CurveTo cx="141" cy="124.85" x="141" y="57.4" />
    <LineTo x="140.85" y="42.15" />
    <CurveTo cx="134.55" cy="40.35" x="130.6" y="36.8" />
    <CurveTo cx="127.15" cy="33.65" x="124.45" y="32.4" />
    <CurveTo cx="120" cy="30.3" x="113.3" y="30.3" />
    <MoveTo x="106.35" y="85.2" />
    <CurveTo cx="106.35" cy="87.1" x="105.05" y="88.4" />
    <CurveTo cx="103.75" cy="89.7" x="101.85" y="89.7" />
    <LineTo x="38.85" y="89.7" />
    <CurveTo cx="37" cy="89.7" x="35.65" y="88.4" />
    <CurveTo cx="34.35" cy="87.1" x="34.35" y="85.2" />
    <LineTo x="34.35" y="67.2" />
    <CurveTo cx="34.35" cy="65.35" x="35.65" y="64" />
    <CurveTo cx="37" cy="62.7" x="38.85" y="62.7" />
    <LineTo x="101.85" y="62.7" />
    <CurveTo cx="103.75" cy="62.7" x="105.05" y="64" />
    <CurveTo cx="106.35" cy="65.35" x="106.35" y="67.2" />
    <LineTo x="106.35" y="85.2" />
</Contour>
'

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 2
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
    <MoveTo x="128.55" y="0" />
    <LineTo x="110" y="0" />
    <CurveTo cx="102.8" cy="0" x="99.8" y="2" />
    <CurveTo cx="98.7" cy="2.75" x="93.65" y="7.05" />
    <CurveTo cx="89.15" cy="10.9" x="85.55" y="12.9" />
    <CurveTo cx="77.25" cy="17.5" x="66.55" y="17.5" />
    <CurveTo cx="55.9" cy="17.5" x="47.55" y="12.9" />
    <CurveTo cx="43.95" cy="10.9" x="39.45" y="7.05" />
    <CurveTo cx="34.4" cy="2.75" x="33.3" y="2" />
    <CurveTo cx="30.3" cy="0" x="23.05" y="0" />
    <LineTo x="4.55" y="0" />
    <CurveTo cx="2.7" cy="0" x="1.35" y="1.45" />
    <CurveTo cx="0.05" cy="2.85" x="0.05" y="4.7" />
    <LineTo x="0" y="123.65" />
    <CurveTo cx="0" cy="133.2" x="6.7" y="140.05" />
    <CurveTo cx="13.45" cy="147" x="22.9" y="147" />
    <CurveTo cx="24.1" cy="158.2" x="30.7" y="167.6" />
    <CurveTo cx="37.2" cy="176.9" x="47.05" y="181.75" />
    <CurveTo cx="47.05" cy="177.85" x="49.85" y="175" />
    <CurveTo cx="52.65" cy="172.1" x="56.5" y="172.1" />
    <LineTo x="76.6" y="172.1" />
    <CurveTo cx="80.45" cy="172.1" x="83.25" y="175" />
    <CurveTo cx="86" cy="177.85" x="86" y="181.75" />
    <CurveTo cx="95.85" cy="176.9" x="102.35" y="167.6" />
    <CurveTo cx="108.95" cy="158.2" x="110.15" y="147" />
    <CurveTo cx="119.6" cy="147" x="126.35" y="140.05" />
    <CurveTo cx="133.1" cy="133.2" x="133.1" y="123.65" />
    <LineTo x="133.05" y="4.7" />
    <CurveTo cx="133.05" cy="2.85" x="131.7" y="1.45" />
    <CurveTo cx="130.35" cy="0" x="128.55" y="0" />
</Contour>
'

--- Phone: Motorola Razor V3 ---
SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 3
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
    <MoveTo x="113.08" y="3.15" />
    <CurveTo cx="97.98" cy="0.6" x="91.38" y="0.2" />
    <LineTo x="87.68" y="0" />
    <LineTo x="88.28" y="3.7" />
    <LineTo x="91.38" y="21.38" />
    <LineTo x="91.38" y="21.58" />
    <CurveTo cx="92.28" cy="25.53" x="91.73" y="26.23" />
    <CurveTo cx="91.13" cy="26.93" x="88.58" y="26.93" />
    <LineTo x="49.73" y="26.93" />
    <CurveTo cx="47.13" cy="26.93" x="46.63" y="26.23" />
    <CurveTo cx="46.03" cy="25.48" x="46.93" y="21.58" />
    <LineTo x="46.98" y="21.38" />
    <LineTo x="50.08" y="3.7" />
    <LineTo x="50.68" y="0" />
    <LineTo x="46.93" y="0.2" />
    <CurveTo cx="40.18" cy="0.6" x="24.5" y="3.15" />
    <CurveTo cx="7.15" cy="5.95" x="2.35" y="7.85" />
    <LineTo x="0.45" y="8.6" />
    <LineTo x="0.45" y="212.63" />
    <LineTo x="1.6" y="213.53" />
    <CurveTo cx="8.25" cy="218.63" x="25.95" y="221.53" />
    <CurveTo cx="43.58" cy="224.4" x="68.17" y="224.4" />
    <CurveTo cx="120.53" cy="224.4" x="134.72" y="213.53" />
    <LineTo x="135.88" y="212.63" />
    <LineTo x="135.88" y="8.6" />
    <LineTo x="134.03" y="7.85" />
    <CurveTo cx="129.28" cy="5.95" x="113.08" y="3.15" />
    <MoveTo x="0" y="221.18" />
    <LineTo x="0" y="264.15" />
    <LineTo x="1.95" y="264.9" />
    <CurveTo cx="12.5" cy="268.85" x="33.43" y="271.85" />
    <CurveTo cx="53.28" cy="274.65" x="67.78" y="274.65" />
    <CurveTo cx="82.33" cy="274.65" x="102.13" y="271.85" />
    <CurveTo cx="123.08" cy="268.85" x="133.63" y="264.9" />
    <LineTo x="135.58" y="264.15" />
    <LineTo x="135.58" y="221.18" />
    <LineTo x="131.08" y="223.75" />
    <CurveTo cx="124.48" cy="227.55" x="108.48" y="230.2" />
    <CurveTo cx="90.28" cy="233.2" x="67.78" y="233.2" />
    <CurveTo cx="45.28" cy="233.2" x="27.08" y="230.2" />
    <CurveTo cx="11.15" cy="227.55" x="4.5" y="223.75" />
    <LineTo x="0" y="221.18" />
    <MoveTo x="123.53" y="60.53" />
    <LineTo x="123.53" y="126.48" />
    <CurveTo cx="123.58" cy="130.18" x="122.78" y="131.72" />
    <CurveTo cx="121.43" cy="134.28" x="117.08" y="135.13" />
    <CurveTo cx="110.33" cy="136.47" x="96.08" y="137.88" />
    <CurveTo cx="79.78" cy="139.47" x="68.88" y="139.47" />
    <CurveTo cx="57.78" cy="139.47" x="41.63" y="137.88" />
    <CurveTo cx="27.48" cy="136.47" x="20.65" y="135.13" />
    <CurveTo cx="16.3" cy="134.28" x="14.95" y="131.72" />
    <CurveTo cx="14.15" cy="130.18" x="14.2" y="126.48" />
    <LineTo x="14.2" y="60.53" />
    <CurveTo cx="14.2" cy="56.58" x="16.95" y="54.58" />
    <CurveTo cx="19.05" cy="53.03" x="21.8" y="53.03" />
    <LineTo x="115.93" y="53.03" />
    <CurveTo cx="118.98" cy="53.03" x="121.08" y="54.83" />
    <CurveTo cx="123.53" cy="56.93" x="123.53" y="60.53" />
    <MoveTo x="59.63" y="203.08" />
    <CurveTo cx="64.83" cy="204.28" x="67.78" y="204.28" />
    <CurveTo cx="76.03" cy="204.28" x="80.28" y="202.88" />
    <CurveTo cx="80.53" cy="202.88" x="80.73" y="203.63" />
    <CurveTo cx="81.08" cy="205.03" x="80.28" y="205.23" />
    <CurveTo cx="76.03" cy="206.63" x="67.78" y="206.63" />
    <CurveTo cx="64.78" cy="206.63" x="59.68" y="205.48" />
    <LineTo x="58.63" y="205.23" />
    <CurveTo cx="58.13" cy="205.08" x="58.13" y="204.08" />
    <LineTo x="58.13" y="203.73" />
    <LineTo x="58.18" y="203.73" />
    <CurveTo cx="58.28" cy="202.88" x="58.63" y="202.88" />
    <LineTo x="59.63" y="203.08" />
</Contour>
'

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 4
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
    <MoveTo x="142.58" y="0.95" />
    <CurveTo cx="134.93" cy="2.85" x="122.58" y="4.65" />
    <CurveTo cx="97.83" cy="8.35" x="73.2" y="8.35" />
    <CurveTo cx="48.55" cy="8.35" x="23.8" y="4.65" />
    <LineTo x="3.75" y="0.95" />
    <LineTo x="0.05" y="0" />
    <LineTo x="0" y="103.63" />
    <CurveTo cx="0" cy="116.73" x="5.9" y="125.83" />
    <CurveTo cx="9.95" cy="132.13" x="8.95" y="139.03" />
    <LineTo x="8.55" y="141.38" />
    <LineTo x="10.8" y="142.28" />
    <CurveTo cx="17" cy="144.68" x="34.45" y="147.43" />
    <CurveTo cx="55.5" cy="150.78" x="73.2" y="150.78" />
    <CurveTo cx="90.88" cy="150.78" x="111.93" y="147.43" />
    <CurveTo cx="129.38" cy="144.68" x="135.53" y="142.28" />
    <LineTo x="137.78" y="141.38" />
    <LineTo x="137.43" y="139.03" />
    <CurveTo cx="137.13" cy="137.28" x="137.38" y="134.78" />
    <CurveTo cx="137.93" cy="129.78" x="140.47" y="125.83" />
    <CurveTo cx="146.33" cy="116.78" x="146.33" y="103.63" />
    <LineTo x="146.33" y="0" />
    <LineTo x="142.58" y="0.95" />
    <MoveTo x="9.1" y="146.93" />
    <LineTo x="8.85" y="255.35" />
    <LineTo x="10.95" y="256.05" />
    <CurveTo cx="35.2" cy="264.05" x="73.45" y="264.05" />
    <CurveTo cx="111.13" cy="264.05" x="135.47" y="256.2" />
    <LineTo x="137.53" y="255.5" />
    <LineTo x="137.72" y="146.93" />
    <LineTo x="133.83" y="148.18" />
    <CurveTo cx="110.48" cy="155.68" x="73.45" y="155.68" />
    <CurveTo cx="36.2" cy="155.68" x="13.05" y="148.18" />
    <LineTo x="9.1" y="146.93" />
    <MoveTo x="51.45" y="248.2" />
    <LineTo x="51.45" y="235.55" />
    <CurveTo cx="51.45" cy="234.05" x="52.95" y="234.05" />
    <LineTo x="93.93" y="234.05" />
    <CurveTo cx="95.43" cy="234.05" x="95.43" y="235.55" />
    <LineTo x="95.43" y="248.2" />
    <CurveTo cx="95.43" cy="249.7" x="93.93" y="249.7" />
    <LineTo x="52.95" y="249.7" />
    <CurveTo cx="51.45" cy="249.7" x="51.45" y="248.2" />
</Contour>
'

UPDATE dbo.FaceToPaper SET FaceX = 82.95, FaceY = 77.45 WHERE FaceId = 1 AND PaperId = 1;

UPDATE dbo.FaceToPaper SET FaceX = 392.65, FaceY = 123.7 WHERE FaceId = 2 AND PaperId = 1;

UPDATE dbo.FaceToPaper SET FaceX = 100.70, FaceY = 65.15 WHERE FaceId = 3 AND PaperId = 2;

UPDATE dbo.FaceToPaper SET FaceX = 375.38, FaceY = 64.50 WHERE FaceId = 4 AND PaperId = 2;

COMMIT TRANSACTION


----------------------------------------

 /*
	 Table FaceLayoutItem:
        - added field: Align
        
     Added 3 layouts for phones: a) MotoRAZR; b) MotoV180
*/

BEGIN TRANSACTION

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FaceLayoutItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN

ALTER TABLE [dbo].[FaceLayoutItem] ADD
  	[Align] [varchar] (10) COLLATE Latin1_General_BIN NOT NULL
	CONSTRAINT [DF_FaceLayoutItem_Align] DEFAULT ('left')
	
END

GO

------------

DELETE FROM [dbo].[FaceLayoutItem]
DELETE FROM [dbo].[FaceLayout]

SET IDENTITY_INSERT [dbo].[FaceLayout] ON

--Moto RAZR - 2 faces * 3 layouts
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (6, 3)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (7, 4)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (8, 3)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (9, 4)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (10, 3)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (11, 4)

--Moto v180 - 2 faces * 3 layouts
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (12, 1)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (13, 2)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (14, 1)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (15, 2)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (16, 1)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (17, 2)

SET IDENTITY_INSERT [dbo].[FaceLayout] OFF

SET IDENTITY_INSERT [dbo].[FaceLayoutItem] ON

INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (12, 6, 43, 28, 0, 'text 1', 'Arial', 16, 0, 0, 13369344, 0, 17, 50, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (13, 6, 41, 140, 1, 'text 2', 'Arial', 16, 0, 0, 13260, 0, 17, 50, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (14, 6, 28, 236, 2, 'text 3', 'Arial', 24, 0, 0, 3368448, 0, 17, 75, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (15, 7, 47, 17, 0, 'text 1', 'Arial', 16, 0, 0, 13369344, 0, 17, 50, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (16, 7, 47, 88, 1, 'text 2', 'Arial', 16, 0, 0, 13260, 0, 17, 50, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (17, 7, 35, 175, 2, 'text 3', 'Arial', 24, 0, 0, 3368448, 0, 17, 75, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (18, 8, 15, 198, 0, 'text 1', 'Arial', 16, 0, 0, 13369344, -90, 17, 45, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (19, 8, 113, 151, 1, 'text 2', 'Arial', 16, 0, 0, 13260, 90, 17, 45, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (20, 8, 27, 236, 2, 'text 3', 'Arial', 24, 0, 0, 3368448, 0, 17, 80, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (21, 9, 16, 92, 0, 'text 1', 'Arial', 16, 0, 0, 13369344, -90, 17, 50, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (22, 9, 123, 44, 1, 'text 2', 'Arial', 16, 0, 0, 13260, 90, 17, 50, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (23, 9, 35, 176, 2, 'text 3', 'Arial', 24, 0, 0, 3368448, 0, 17, 75, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (24, 10, 46, 142, 0, 'text 1', 'Arial', 14, 0, 0, 13369344, 0, 17, 45, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (25, 10, 3, 235, 1, 'text 2', 'Arial', 14, 0, 0, 13260, 11, 17, 50, 'left')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (26, 10, 85, 242, 2, 'text 3', 'Arial', 14, 0, 0, 3368448, -8, 17, 50, 'right')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (27, 11, 51, 20, 0, 'text 1', 'Arial', 14, 0, 0, 13369344, 0, 17, 50, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (28, 11, 13, 156, 1, 'text 2', 'Arial', 14, 0, 0, 13260, 11, 17, 50, 'left')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (29, 11, 86, 163, 2, 'text 3', 'Arial', 14, 0, 0, 3368448, -8, 17, 50, 'right')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (30, 12, 51, 6, 0, 'text 1', 'Arial', 14, 0, 0, 13369344, 0, 17, 40, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (31, 12, 40, 123, 1, 'text 2', 'Arial', 22, 0, 0, 13260, 0, 17, 60, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (32, 12, 47, 191, 2, 'text 3', 'Arial', 16, 0, 0, 3368448, 0, 17, 45, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (33, 13, 45, 20, 0, 'text 1', 'Arial', 14, 0, 0, 13369344, 0, 17, 45, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (34, 13, 34, 78, 1, 'text 2', 'Arial', 22, 0, 0, 13260, 0, 17, 65, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (35, 13, 44, 147, 2, 'text 3', 'Arial', 16, 0, 0, 3368448, 0, 17, 45, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (36, 14, 51, 6, 0, 'text 1', 'Arial', 14, 0, 0, 13369344, 0, 17, 40, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (37, 14, 134, 102, 1, 'text 2', 'Arial', 16, 0, 0, 13260, 90, 17, 50, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (38, 14, 8, 152, 2, 'text 3', 'Arial', 16, 0, 0, 3368448, -90, 17, 50, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (39, 15, 45, 20, 0, 'text 1', 'Arial', 14, 0, 0, 13369344, 0, 17, 42, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (40, 15, 131, 66, 1, 'text 2', 'Arial', 16, 0, 0, 13260, 90, 17, 47, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (41, 15, 4, 114, 2, 'text 3', 'Arial', 16, 0, 0, 3368448, -90, 17, 49, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (42, 16, 51, 6, 0, 'text 1', 'Arial', 14, 0, 0, 13369344, 0, 17, 40, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (43, 16, 39, 186, 1, 'text 2', 'Arial', 10, 0, 0, 13260, 57, 13, 35, 'right')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (44, 16, 88, 212, 2, 'text 3', 'Arial', 10, 0, 0, 3368448, -57, 13, 35, 'left')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (45, 17, 47, 18, 0, 'text 1', 'Arial', 14, 0, 0, 13369344, 0, 17, 40, 'center')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (46, 17, 34, 144, 1, 'text 2', 'Arial', 10, 0, 0, 13260, 57, 13, 35, 'right')
INSERT INTO [dbo].[FaceLayoutItem] ([Id], [FaceLayoutId], [X], [Y], [Position], [Text], [Font], [Size], [Bold], [Italic], [Color], [Angle], [Height], [Width], [Align])  VALUES (47, 17, 84, 170, 2, 'text 3', 'Arial', 10, 0, 0, 3368448, -57, 13, 35, 'left')

SET IDENTITY_INSERT [dbo].[FaceLayoutItem] OFF

COMMIT