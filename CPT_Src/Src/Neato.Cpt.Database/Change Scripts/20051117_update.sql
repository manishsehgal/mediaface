/*
    Table, FK and PK are renamed: 
        dbo.ImageLibFolderItems -> dbo.ImageLibFolderItem
        FK_ImageLibFolderItems_ImageLibFolder -> FK_ImageLibFolderItem_ImageLibFolder
        FK_ImageLibFolderItems_ImageLibItem -> FK_ImageLibFolderItem_ImageLibItem
        PK_ImageLibFolderItems -> PK_ImageLibFolderItem
    Insert data to Image Library tables
*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
if (
    exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ImageLibFolderItems]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
    and
    exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ImageLibFolderItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
)  
begin
    drop table [dbo].[ImageLibFolderItems]
end

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ImageLibFolderItems]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
begin
    EXECUTE sp_rename N'dbo.ImageLibFolderItems', N'ImageLibFolderItem', 'OBJECT'
    EXECUTE sp_rename N'FK_ImageLibFolderItems_ImageLibFolder', N'FK_ImageLibFolderItem_ImageLibFolder', 'OBJECT'
    EXECUTE sp_rename N'FK_ImageLibFolderItems_ImageLibItem', N'FK_ImageLibFolderItem_ImageLibItem', 'OBJECT'
    EXECUTE sp_rename N'PK_ImageLibFolderItems', N'PK_ImageLibFolderItem', 'OBJECT'
end

COMMIT

-----------------------------------v Insert Image Library Data v---------------------------------
BEGIN TRANSACTION
GO

if (
    not exists (SELECT TOP 1 * FROM dbo.ImageLibFolder)
    and
    exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ImageTemp]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
    and
    exists (SELECT TOP 1 * FROM dbo.ImageTemp)
)
begin

    set identity_insert dbo.ImageLibFolder on
    INSERT INTO dbo.ImageLibFolder(FolderId, ParentFolderId) VALUES (1, NULL) -- Main
    INSERT INTO dbo.ImageLibFolder(FolderId, ParentFolderId) VALUES (2, 1)    -- Main\Business
    set identity_insert dbo.ImageLibFolder off

    INSERT INTO dbo.ImageLibFolderLocalization(FolderId, Culture, Caption) VALUES (1, N'', N'Main')
    INSERT INTO dbo.ImageLibFolderLocalization(FolderId, Culture, Caption) VALUES (1, N'en', N'Main')
    INSERT INTO dbo.ImageLibFolderLocalization(FolderId, Culture, Caption) VALUES (1, N'ru', N'Главная')

    INSERT INTO dbo.ImageLibFolderLocalization(FolderId, Culture, Caption) VALUES (2, N'', N'Business')
    INSERT INTO dbo.ImageLibFolderLocalization(FolderId, Culture, Caption) VALUES (2, N'en', N'Business')
    INSERT INTO dbo.ImageLibFolderLocalization(FolderId, Culture, Caption) VALUES (2, N'ru', N'Бизнес')

    SET IDENTITY_INSERT dbo.ImageLibItem ON
    INSERT INTO dbo.ImageLibItem (ItemId, Stream) SELECT ItemId = ImgId, Stream=Img FROM dbo.ImageTemp TABLOCKX
    SET IDENTITY_INSERT dbo.ImageLibItem OFF

    -- Main
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (1, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (1, N'en', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (1, N'en-US', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (1, N'ru', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (1, N'ru-RU', N'')

    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (2, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (3, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (4, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (5, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (6, N'', N'')

    -- Business
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (7, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (8, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (9, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (10, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (11, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (12, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (13, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (14, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (15, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (16, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (17, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (18, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (19, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (20, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (20, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (21, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (22, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (23, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (24, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (25, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (26, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (27, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (28, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (29, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (30, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (40, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (41, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (42, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (43, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (44, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (45, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (46, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (47, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (48, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (49, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (50, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (50, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (51, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (52, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (53, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (54, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (55, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (56, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (57, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (58, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (59, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (60, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (61, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (62, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (63, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (64, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (65, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (66, N'', N'')
    INSERT INTO dbo.ImageLibItemKeyword(ItemId, Culture, Keyword) VALUES (67, N'', N'')

    -- Main
    INSERT INTO dbo.ImageLibFolderItem(FolderId, ItemId)
    SELECT FolderId = 1, ItemId = ImgId FROM dbo.ImageTemp
    TABLOCKX 
    WHERE ImgId between 1 and 6

    -- Business
    INSERT INTO dbo.ImageLibFolderItem(FolderId, ItemId)
    SELECT FolderId = 2, ItemId = ImgId FROM dbo.ImageTemp
    TABLOCKX
    WHERE ImgId between 7 and 67

end
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ImageTemp]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ImageTemp]
GO
-----------------------------------^ Insert Image Library Data ^---------------------------------

COMMIT
