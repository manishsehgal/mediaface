 /* Changes
- new table: tracking images
*/ 

BEGIN TRANSACTION

-- table

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Tracking_Images]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[Tracking_Images] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[ImageId] [int] NOT NULL ,
	[User] [varchar] (200) COLLATE Latin1_General_BIN NOT NULL ,
	[Time] [datetime] NOT NULL 
) ON [PRIMARY]


ALTER TABLE [dbo].[Tracking_Images] WITH NOCHECK ADD 
	CONSTRAINT [PK_ImagesTracking] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY]
ALTER TABLE [dbo].[Tracking_Images] ADD 
	CONSTRAINT [FK_Tracking_Images_ImageLibItem] FOREIGN KEY 
	(
		[ImageId]
	) REFERENCES [dbo].[ImageLibItem] (
		[ItemId]
	) ON DELETE CASCADE
	
END



COMMIT

