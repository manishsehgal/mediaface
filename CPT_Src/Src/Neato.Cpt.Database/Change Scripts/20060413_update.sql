BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
CREATE TABLE dbo.Tmp_IPFilter
	(
	Id int NOT NULL IDENTITY (1, 1),
	BeginField1 tinyint NOT NULL,
	BeginField2 tinyint NOT NULL,
	BeginField3 tinyint NOT NULL,
	BeginField4 tinyint NOT NULL,
	EndField1 tinyint NOT NULL,
	EndField2 tinyint NOT NULL,
	EndField3 tinyint NOT NULL,
	EndField4 tinyint NOT NULL,
	Description nvarchar(100) NOT NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_IPFilter ON
GO
IF EXISTS(SELECT * FROM dbo.IPFilter)
	 EXEC('INSERT INTO dbo.Tmp_IPFilter (Id, BeginField1, BeginField2, BeginField3, BeginField4, EndField1, EndField2, EndField3, EndField4, Description)
		SELECT Id, BeginField1, BeginField2, BeginField3, BeginField4, EndField1, EndField2, EndField3, EndField4, Description FROM dbo.IPFilter (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_IPFilter OFF
GO
DROP TABLE dbo.IPFilter
GO
EXECUTE sp_rename N'dbo.Tmp_IPFilter', N'IPFilter', 'OBJECT'
GO
ALTER TABLE dbo.IPFilter ADD CONSTRAINT
	PK_IPFilter PRIMARY KEY CLUSTERED 
	(
	Id
	) ON [PRIMARY]

GO

ALTER TABLE dbo.DeviceRequest ALTER COLUMN Comment NVARCHAR(2000) NOT NULL
COMMIT
