 -- BEGIN RENAME: Phone -> Device 

if exists(SELECT * FROM dbo.[MailFormat] WHERE ([MailId]=5))
	update dbo.[MailFormat]
	set [Subject]='Your device was added!'
	where [MailId]=5
GO
	
if exists(SELECT * FROM dbo.[MailType] WHERE ([MailId]=5))
	update dbo.[MailType]
	set [Description]='Device request notification'
	where [MailId]=5
GO
	
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Tracking_Phones]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	EXECUTE sp_rename N'dbo.Tracking_Phones.Phone', N'Device', 'COLUMN'
	EXECUTE sp_rename N'dbo.Tracking_Phones.Manufacturer', N'DeviceBrand', 'COLUMN'
	EXECUTE sp_rename N'dbo.Tracking_Phones', N'Tracking_Devices'
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PhoneRequest]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	EXECUTE sp_rename N'dbo.PhoneRequest.PhoneModel', N'Device', 'COLUMN'
	EXECUTE sp_rename N'dbo.PhoneRequest.PhoneManufacturer', N'DeviceBrand', 'COLUMN'
	EXECUTE sp_rename N'dbo.PhoneRequest', N'DeviceRequest'
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTrackingPhonesEnumBrands]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.PrTrackingPhonesEnumBrands
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTrackingPhonesEnumByTimeAndBrand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.PrTrackingPhonesEnumByTimeAndBrand
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTrackingPhonesIns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.PrTrackingPhonesIns
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPhoneRequestDel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.PrPhoneRequestDel
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPhoneRequestEmailsGetByDates]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.PrPhoneRequestEmailsGetByDates
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPhoneRequestEnum]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.PrPhoneRequestEnum
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPhoneRequestEnumByCarrierPhoneManufacturerPhoneModel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.PrPhoneRequestEnumByCarrierPhoneManufacturerPhoneModel
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPhoneRequestGetByDatesAndBrand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.PrPhoneRequestGetByDatesAndBrand
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPhoneRequestIns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.PrPhoneRequestIns
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPhoneRequestUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.PrPhoneRequestUpd
GO

-- END RENAME: Phone -> Device 
