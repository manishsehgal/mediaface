/*
Added retailers table
*/ 
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Retailers]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[Retailers] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) COLLATE Latin1_General_CS_AS NOT NULL ,
	[Icon] [image] NOT NULL ,
	[Url] [varchar] (200) COLLATE Latin1_General_CS_AS NOT NULL,
	[Enabled] [bit] NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'PK_Retailers') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[Retailers] DROP CONSTRAINT PK_Retailers
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_Retailers') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[Retailers] WITH NOCHECK ADD 
	CONSTRAINT PK_Retailers PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

if not exists (select * from Page where [Url] = 'BuyNow.aspx')
BEGIN
    DECLARE @PageId INT
    INSERT INTO [Page] ([Url], [Description]) VALUES ('BuyNow.aspx', 'Buy Now')
    SET @PageId = SCOPE_IDENTITY()

    DECLARE @LinkId INT
    INSERT INTO [Link] ([PageId]) VALUES (@PageId)
    SET @LinkId = SCOPE_IDENTITY()

    INSERT INTO LinkLocalization (LinkId, Culture, Text) VALUES (@LinkId, '', 'BUY NOW')

    INSERT INTO LinkPlace (PageId, LinkId, SortOrder, Align, Place, IsNewWindow, IsVisible)
    VALUES (1, @LinkId, 6, '', 'Header', 'N', 1)
END