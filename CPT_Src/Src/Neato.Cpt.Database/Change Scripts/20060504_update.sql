/* Changes
- add table BuyMoreSkinsPage
- add table TermsOfUsePage
*/ 

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BuyMoreSkinsPage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[BuyMoreSkinsPage] (
	[Culture] [nvarchar] (10) COLLATE Latin1_General_BIN NOT NULL ,
	[HtmlText] [ntext] COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TermsOfUsePage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[TermsOfUsePage] (
	[Culture] [nvarchar] (10) COLLATE Latin1_General_BIN NOT NULL ,
	[HtmlText] [ntext] COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

if not exists (select * from BuyMoreSkinsPage)
BEGIN
INSERT INTO BuyMoreSkinsPage ([Culture], [HtmlText])
VALUES('', 'Where can I buy more skins') 
END

if not exists (select * from TermsOfUsePage)
BEGIN
INSERT INTO TermsOfUsePage ([Culture], [HtmlText])
VALUES('', 'Terms Of Use') 
END

if not exists (select * from Page where [Url] = 'BuyMoreSkins.aspx')
BEGIN
    DECLARE @PageId INT
    INSERT INTO [Page] ([Url], [Description]) VALUES ('BuyMoreSkins.aspx', 'Where can I buy more skins')
    SET @PageId = SCOPE_IDENTITY()

    DECLARE @LinkId INT
    INSERT INTO [Link] ([PageId]) VALUES (@PageId)
    SET @LinkId = SCOPE_IDENTITY()

    INSERT INTO LinkLocalization (LinkId, Culture, Text) VALUES (@LinkId, '', 'WHERE CAN I BUY MORE SKINS')

    INSERT INTO LinkPlace (PageId, LinkId, SortOrder, Align, Place, IsNewWindow, IsVisible)
    VALUES (1, @LinkId, 20, 'R', 'Footer', 'N', 1)


    INSERT INTO [Page] ([Url], [Description]) VALUES ('TermsOfUse.aspx', 'Terms Of Use')
    SET @PageId = SCOPE_IDENTITY()

    INSERT INTO [Link] ([PageId]) VALUES (@PageId)
    SET @LinkId = SCOPE_IDENTITY()

    INSERT INTO LinkLocalization (LinkId, Culture, Text) VALUES (@LinkId, '', 'TERMS OF USE')

    INSERT INTO LinkPlace (PageId, LinkId, SortOrder, Align, Place, IsNewWindow, IsVisible)
    VALUES (1, @LinkId, 21, 'R', 'Footer', 'N', 1)
END