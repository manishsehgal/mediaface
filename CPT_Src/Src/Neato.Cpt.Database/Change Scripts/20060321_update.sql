 /* Changes
- new table: tracking phones
- new procedures: PrTrackingPhonesEnumCarriers
		  PrTrackingPhonesEnumByTimeAndCarrier
		  PrTrackingPhonesIns
- load fresh data for tables: MailType; MailFormat
- add column "Created" in table "PhoneRequest"
- change collation for fields: PhoneRequest.PhoneManufacturer, DeviceBrand.Name

*/ 

BEGIN TRANSACTION

-- table

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Tracking_Phones]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[Tracking_Phones] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Phone] [varchar] (200) COLLATE Latin1_General_BIN NOT NULL ,
	[Manufacturer] [varchar] (200) COLLATE Latin1_General_BIN NOT NULL ,
	[User] [varchar] (200) COLLATE Latin1_General_BIN NOT NULL ,
	[Time] [datetime] NOT NULL 
) ON [PRIMARY]


ALTER TABLE [dbo].[Tracking_Phones] WITH NOCHECK ADD 
	CONSTRAINT [PK_PhonesTracking] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
END




-- load fresh data for tables: MailType; MailFormat
DELETE FROM [dbo].[MailFormat]
DELETE FROM [dbo].[MailType]

SET IDENTITY_INSERT [dbo].[MailType] ON
INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (1, 'Forgot Password')
INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (2, 'Tell-a-Friend')
INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (3, 'Add my device')
INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (4, 'Tell us what you think')
INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (5, 'Phone request notification')
SET IDENTITY_INSERT [dbo].[MailType] OFF

INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (1, '', N'Password Reminder From Neato', N'Hello %UserName:
Here is the password you requested. Click %NewPasswordLink to generate your new password. After, please use it with your email address to log in at Neato.')
INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (2, '', N'Tell-A-Friend From Neato', N'%MailText


%PrintzHomeLink

This email was sent from the tell-a-friend feature at the above mentioned site. To remove yourself from any further tell-a-friend emails, please click %UnsubscribeLink.')
INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (3, '', N'Thanks for your request!', N'Feeling creative?  We respect that. We’ll let you know ASAP when your device has been added to the site and your Printz™ skin is ready to design.  Check your email soon!

Thanks again for your interest and feel free to email us with any questions.')
INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (4, '', N'Thanks for your feedback!', N'We hear you. We’ll log your comments and email back ASAP to answer your questions.
If you request changes to the site or product, we will notify you if we are able to accommodate your request.

Thanks again for your comments and email us any time!')
INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (5, '', N'Your phone was added!', N'Your device %DeviceName has been added to the site and your Printz™ skin is ready to design. 
%PrintzHomeLink')

-- add column "Created" in table "PhoneRequest"
EXEC sp_columns @table_name = 'PhoneRequest', @table_owner='dbo', @column_name = 'Created'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.PhoneRequest ADD
	Created datetime NOT NULL CONSTRAINT DF_PhoneRequest_Created DEFAULT (getdate())
END
GO

-- change collation for fields: PhoneRequest.PhoneManufacturer, DeviceBrand.Name
EXEC sp_columns @table_name = 'PhoneRequest', @table_owner='dbo', @column_name = 'PhoneManufacturer'
IF @@ROWCOUNT <> 0
BEGIN
    ALTER TABLE dbo.PhoneRequest ALTER COLUMN PhoneManufacturer nvarchar(100) COLLATE Latin1_General_CI_AS NOT NULL
END
GO

EXEC sp_columns @table_name = 'DeviceBrand', @table_owner='dbo', @column_name = 'Name'
IF @@ROWCOUNT <> 0
BEGIN
    ALTER TABLE dbo.DeviceBrand ALTER COLUMN Name nvarchar(30) COLLATE Latin1_General_CI_AS NOT NULL
END
GO

COMMIT