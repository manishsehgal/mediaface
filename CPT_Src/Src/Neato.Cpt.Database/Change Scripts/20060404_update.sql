/*Changes - Google tracking*/

BEGIN TRANSACTION

-----------------------------------------------
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GoogleCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[GoogleCode] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Code] [ntext] COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_GoogleCode') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[GoogleCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_GoogleCode] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GoogleTrackPage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[GoogleTrackPage] (
	[PageUrl] [nvarchar] (200) COLLATE Latin1_General_CI_AI NOT NULL ,
	[EnableScript] [bit] NOT NULL 
) ON [PRIMARY]
END

GO


if not exists (select top 1 * from dbo.GoogleCode)
BEGIN
SET IDENTITY_INSERT dbo.GoogleCode ON
INSERT INTO dbo.GoogleCode ([Id], [Code])
VALUES(1, '<script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>
<script type="text/javascript">
_uacct = "UA-211127-2";
urchinTracker();
</script>')
SET IDENTITY_INSERT dbo.GoogleCode OFF
END

-----------------------------------------------


COMMIT