/*
create PlaceProperty Table
*/ 

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlaceProperty]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[PlaceProperty] (
	[RetailerId] [int] NOT NULL ,
	[Place] [varchar] (50) COLLATE Latin1_General_BIN NOT NULL ,
	[BackgroundColor] [varchar] (50) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'FK_PlaceProperty_Retailers') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[PlaceProperty] ADD 
	CONSTRAINT [FK_PlaceProperty_Retailers] FOREIGN KEY 
	(
		[RetailerId]
	) REFERENCES [dbo].[Retailers] (
		[Id]
	)
GO