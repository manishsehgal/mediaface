BEGIN TRANSACTION

EXEC sp_columns @table_name = 'Customer', @table_owner = 'dbo', @column_name = 'LastEditedDeviceId'
IF @@rowcount = 0 
ALTER TABLE dbo.Customer ADD
	LastEditedDeviceId int NULL
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Customer_Device_LastEdited]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.Customer ADD CONSTRAINT
FK_Customer_Device_LastEdited FOREIGN KEY
(
    LastEditedDeviceId
) REFERENCES dbo.Device
(
    Id
)
GO

COMMIT
 