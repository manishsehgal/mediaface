/* Changes
- add special user table
- add column BuyMoreSkinsPage.RetailerId
- add column TermsOfUsePage.RetailerId
- add column Announcement.RetailerId
*/

BEGIN TRANSACTION

-- add special user table
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SpecialUser]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[SpecialUser] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Email] [varchar] (100) COLLATE Latin1_General_BIN NOT NULL DEFAULT (''),
	[ShowPaper] [bit] NOT NULL DEFAULT (0),
	[NotifyPaperState] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Logo]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Logo] (
	[RetailerId] [int] NOT NULL ,
	[Logo] [image] NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Logo_Retailers]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Logo] ADD 
	CONSTRAINT [FK_Logo_Retailers] FOREIGN KEY 
	(
		[RetailerId]
	) REFERENCES [dbo].[Retailers] (
		[Id]
	)
GO

if not exists (select * from [dbo].[Logo] where [RetailerId]=1)
INSERT INTO [dbo].[Logo] ([RetailerId], [Logo]) VALUES (1, 0x47494638396183004b00a20700abababfefefed8d8d82525250a0a0a4b4b4b79797900000021f90401000007002c0000000083004b000003ff78badcfe30ca49abbd38ebcdbbff60288e64699e68aaae6cebbe702ccf746ddf78aeef7cefffc0a070482c1a8f340201c92c180c058792e933040206e994ba1b3cb5dc21a1e025877d8382f37acd9e71e582804d07bc67cea70140efb7ef300500737e7e0202006e802b82857e0090518b2a7b908e7488064b1f509313569798039c87848a9e0e7ca16c921e04027b58a3a80ea05788006b7dad144a4aaa0102038904039bb40da664c607638457bc104a69cf7464c7c80e0369d80b726c06b30fdbd585d1d911c3ddd5e70be58eede8a9c1e207df6df50c83c1bcc005ddf21eb0c9c74ec39c7fd9080953b085c1803900ead95a38c117333ef1003d130090ff01a17af73246303067143354e5f279bc42f04ac40c1f9b753452eca482724b7c2563c9f0e6400ca0fe3504a1e4db1c8e0d062152a9e1e702027e8c15c3c62627b3674ca5c51cba612a54780a62559500058a9a713119f8b1ba6949019e530fd099f9e0594eae17b6adba55aacfa63d809f0063c570ca606ce59002bcb280191d4d159b11ca2973c3dbbda124d9dafbcfd8a86af5be621250f86f0000770f6ca6bb3280b8b84d31c38b324cf6d82d761b38e2a8d367376016c632632dc176547785122502ec4fc1acb9ba1d8d6a78653134e207e6a4f680f99a5ec60b0a857bd07c13d6e8854857961b40209b0ad59b7d7825beb443680cfd9c724007bdfafcff97a8b7c91fe805205233e0e115db7bda6c93d32d0082238168ed3dd59f856c08f3101b763c081e86aca8914603a2d575480614560886020cd23756042eaa28d98bcedd324b1a1c2a70996b0ca4d8c76bd5a8b5110614fe07c186090e902105c0a16707880dec08c990ce1130981f8561b60d06487e28cd85084a188194243ed32384515ec2d0738e7468cc2a6668209a8000c528c92f170234d103253e75d97ef638529a8f833c395535be78c541976d95b96433c6ec28a037506032ce2dd828198c36e91566cc608a24ea4d8f52b99261a30c5543e7019a1268cf668f36d0aa91ad4aa1cb6385859960a20a8adac18ec18c718ba095018bc513efa0296bacffcdbc65a443b03e5bdba6f2b9d0ea5e02c4390501b0ae32a937ccb2aa6c2d983cf91424a82968c113073260ec255b5e2a1eba9808759f4bcb9e960e26803e859dac921c92cb05eff6210caaf93ed68a95c03c6b8f42f93afcd49ed26c40926a3c72b99bbd6b3e006cb6f2eaeb80a4eee6284d1e1c73f0d64b6fe9954b561e27875747c09a2bab422ab56a33b7f8f2894d311abce5e14bb0a84688cd12dc6332c175a4a30a520ea982f46548f73828c1b461a100ba1c4132b18116d4acc1b422432029484d97e6e0bf1758b189d7185b094b00c740d56fcc4b6340e6986983db33d06bb23df24b4a8ea27558ea2919c53e513871087653ec9d019214a1756ac5a957ab3bc1219dc8e5b8388adfe4061f7690345edb7903754bd50e3dc3ba0942ebc379324f5eac23cc302e618b1b33f7c6c0442249ae82922f87e59244ea09329f5ba6e44aa2ef8d01f37a634bb94012be4a5e8f9a8ed37b00caf6168bd983173c2262f4e128d8d2e14c4e3cb03b10d91bcdd0e929bc0f8115ac1bd5ae0c82d16dcf4b2fb0c2dd9c839071d08f078011dc091211101b5869800d6401302018c1f411a68249c0a0071200003b)
GO

-- add column BuyMoreSkinsPage.RetailerId
EXEC sp_columns @table_name = 'BuyMoreSkinsPage', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.BuyMoreSkinsPage ADD
	RetailerId int NOT NULL CONSTRAINT DF_BuyMoreSkinsPage_RetailerId DEFAULT (1)
END
GO

-- add column TermsOfUsePage.RetailerId
EXEC sp_columns @table_name = 'TermsOfUsePage', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.TermsOfUsePage ADD
	RetailerId int NOT NULL CONSTRAINT DF_TermsOfUsePage_RetailerId DEFAULT (1)
END
GO

-- add column Announcement.RetailerId
EXEC sp_columns @table_name = 'Announcement', @table_owner='dbo', @column_name = 'RetailerId'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Announcement ADD
	RetailerId int NOT NULL CONSTRAINT DF_Announcement_RetailerId DEFAULT (1)
END
GO

COMMIT

