/* Changes - data to table: MailFormat
*/

UPDATE [dbo].[MailFormat]
SET [Body] = 'Your device %DeviceName has been added to the site and your Printz™ skin is ready to design. 
%PrintzHomeLink'
WHERE [MailId] = 5 AND [Culture]=''