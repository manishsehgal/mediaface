/* Changes
- add column "Created" in table "Customer"
*/

BEGIN TRANSACTION

EXEC sp_columns @table_name = 'Customer', @table_owner='dbo', @column_name = 'Created'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Customer ADD
	Created datetime NOT NULL CONSTRAINT DF_Customer_Created DEFAULT (getdate())
END

COMMIT