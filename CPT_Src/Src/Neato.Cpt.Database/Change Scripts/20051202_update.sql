BEGIN TRANSACTION

ALTER TABLE [dbo].[Page] ALTER COLUMN [Url] NVARCHAR(200) COLLATE Latin1_General_BIN NOT NULL

SET IDENTITY_INSERT [dbo].[Page] ON
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (19, N'http://www.bodyglove.com/products/find.php?uselist=showcase', N'BuyNow Page')
SET IDENTITY_INSERT [dbo].[Page] OFF

SET IDENTITY_INSERT [dbo].[Link] ON
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (35, 19)
SET IDENTITY_INSERT [dbo].[Link] OFF

INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (35, N'', N'BUY NOW')

INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 35, 6, ' ', 'Header', 'Y')


COMMIT TRANSACTION 