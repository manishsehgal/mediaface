BEGIN TRANSACTION

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Customer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[Customer] (
	[CustomerId] [int] IDENTITY (1, 1) NOT NULL ,
	[FirstName] [nvarchar] (100) COLLATE Latin1_General_BIN NOT NULL ,
	[LastName] [nvarchar] (100) COLLATE Latin1_General_BIN NOT NULL ,
	[Email] [varchar] (100) COLLATE Latin1_General_BIN NOT NULL ,
	[Password] [varchar] (100) COLLATE Latin1_General_BIN NOT NULL ,
	[EmailOptions] [char] (1) COLLATE Latin1_General_BIN NOT NULL ,
	[ReceiveInfo] [bit] NOT NULL ,
	[XCalibration] [float] NULL ,
	[YCalibration] [float] NULL ,
	[pswuid] [varchar] (50) COLLATE Latin1_General_BIN NULL 
) ON [PRIMARY]

ALTER TABLE [dbo].[Customer] WITH NOCHECK ADD 
	CONSTRAINT [PK_Customer] PRIMARY KEY  CLUSTERED 
	(
		[CustomerId]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[Customer] ADD 
	CONSTRAINT [IX_Customer] UNIQUE  NONCLUSTERED 
	(
		[Email]
	)  ON [PRIMARY] ,
	CONSTRAINT [CK_Customer] CHECK ([EmailOptions] = 'H' or [EmailOptions] = 'T')
END


COMMIT TRANSACTION
