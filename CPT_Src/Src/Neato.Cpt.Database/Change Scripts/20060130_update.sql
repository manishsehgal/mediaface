/*
 Phone: Samsung X495/497 - position updated
    
*/

BEGIN TRANSACTION
--- Phone: Samsung X495/497 ---

UPDATE dbo.FaceToPaper SET FaceX = 89.25, FaceY = 72.75 WHERE FaceId = 5 AND PaperId = 3;

COMMIT TRANSACTION
