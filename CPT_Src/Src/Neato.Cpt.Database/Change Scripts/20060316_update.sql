/*
Universal contour for Nokia 6101
*/

DECLARE @ptrContour binary(16)
SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 9
WRITETEXT Face.Contour @ptrContour
'
<Contour x="0" y="0">
	<MoveTo x="84.25" y="38.25" />
	<CurveTo cx="86.85" cy="40.85" x="86.85" y="43.85" />
	<LineTo x="86.85" y="63.6" />
	<CurveTo cx="86.85" cy="66.55" x="84.25" y="69.2" />
	<CurveTo cx="81.65" cy="71.8" x="78.65" y="71.8" />
	<LineTo x="56.6" y="71.8" />
	<CurveTo cx="53.65" cy="71.8" x="51" y="69.2" />
	<CurveTo cx="48.4" cy="66.55" x="48.4" y="63.6" />
	<LineTo x="48.4" y="43.85" />
	<CurveTo cx="48.4" cy="40.85" x="51" y="38.25" />
	<CurveTo cx="53.65" cy="35.65" x="56.6" y="35.65" />
	<LineTo x="78.65" y="35.65" />
	<CurveTo cx="81.65" cy="35.65" x="84.25" y="38.25" />
	<MoveTo x="66.5" y="102.6" />
	<CurveTo cx="79.65" cy="102.6" x="102" y="103.6" />
	<CurveTo cx="104.95" cy="103.6" x="107.1" y="105.7" />
	<CurveTo cx="109.2" cy="107.8" x="109.2" y="110.8" />
	<LineTo x="109.2" y="158.5" />
	<CurveTo cx="109.2" cy="161.45" x="107.1" y="163.6" />
	<CurveTo cx="104.95" cy="165.7" x="102" y="165.7" />
	<CurveTo cx="80.7" cy="166.7" x="66.5" y="166.7" />
	<CurveTo cx="57.6" cy="166.7" x="44.3" y="166.2" />
	<LineTo x="32.8" y="165.7" />
	<CurveTo cx="29.8" cy="165.7" x="27.7" y="163.6" />
	<CurveTo cx="25.6" cy="161.45" x="25.6" y="158.5" />
	<LineTo x="25.6" y="110.8" />
	<CurveTo cx="25.6" cy="107.8" x="27.7" y="105.7" />
	<CurveTo cx="29.8" cy="103.6" x="32.8" y="103.6" />
	<LineTo x="66.5" y="102.6" />
	<MoveTo x="135" y="40.1" />
	<LineTo x="135" y="137.1" />
	<CurveTo cx="135" cy="166.35" x="131.4" y="191.25" />
	<CurveTo cx="129.5" cy="204.1" x="127" y="214.6" />
	<CurveTo cx="122.7" cy="237.5" x="104" y="240.6" />
	<CurveTo cx="85.55" cy="243.6" x="67.5" y="243.6" />
	<CurveTo cx="49.25" cy="243.6" x="31" y="240.6" />
	<CurveTo cx="12.15" cy="237.45" x="8" y="214.6" />
	<CurveTo cx="5.5" cy="204.1" x="3.65" y="191.25" />
	<CurveTo cx="0" cy="166.35" x="0" y="137.1" />
	<LineTo x="0" y="40.1" />
	<LineTo x="17.5" y="33.6" />
	<LineTo x="28.5" y="33.6" />
	<LineTo x="28.5" y="0" />
	<LineTo x="106.5" y="0" />
	<LineTo x="106.5" y="33.6" />
	<LineTo x="117.5" y="33.6" />
	<LineTo x="135" y="40.1" />
	<CutLine x1="-15" y1="55" x2="150" y2="55" />
	<CutLine x1="-15" y1="135" x2="150" y2="135" />
</Contour>
'
SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 10
WRITETEXT Face.Contour @ptrContour
'
<Contour x="0" y="0">
	<MoveTo x="26.3" y="1.5" />
	<LineTo x="71.3" y="0" />
	<CurveTo cx="96.3" cy="0" x="142.55" y="3" />
	<CurveTo cx="139.8" cy="161.85" x="139.8" y="172.6" />
	<CurveTo cx="139.8" cy="175.65" x="135.6" y="180.8" />
	<CurveTo cx="131.3" cy="186.15" x="124.55" y="191.1" />
	<CurveTo cx="107.65" cy="203.6" x="91.3" y="203.6" />
	<CurveTo cx="91.3" cy="197" x="87.7" y="193.75" />
	<CurveTo cx="82.95" cy="189.5" x="71.3" y="189.5" />
	<CurveTo cx="59.65" cy="189.5" x="54.95" y="193.75" />
	<CurveTo cx="51.3" cy="197" x="51.3" y="203.6" />
	<CurveTo cx="34.95" cy="203.6" x="18.05" y="191.1" />
	<CurveTo cx="11.3" cy="186.15" x="7" y="180.8" />
	<CurveTo cx="2.8" cy="175.65" x="2.8" y="172.6" />
	<CurveTo cx="2.8" cy="161.85" x="0" y="3" />
	<LineTo x="26.3" y="1.5" />
</Contour>
'