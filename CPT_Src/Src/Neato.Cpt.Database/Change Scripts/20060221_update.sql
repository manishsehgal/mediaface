/*
- create OverviewPage table
- create HelpPage table
*/ 

-- Phone Request begin
EXEC sp_columns @table_name = 'PhoneRequest', @table_owner = 'dbo', @column_name = 'Carrier'
IF @@rowcount = 0
BEGIN
    ALTER TABLE [dbo].[PhoneRequest] ADD
        [Carrier] nvarchar(100) NULL

    CREATE TABLE #t(x BIT)
END
GO

EXEC sp_tables @table_name = '#t'
IF @@rowcount <> 0
BEGIN
    UPDATE [dbo].[PhoneRequest]
        SET [Carrier] = ''
    
    ALTER TABLE [dbo].[PhoneRequest]
        ALTER COLUMN [Carrier] nvarchar(100) NOT NULL

    DROP TABLE #t
END
GO
-- Phone Request end

-- OverviewPage begin
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OverviewPage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[OverviewPage] (
	[Culture] [nvarchar] (10) COLLATE Latin1_General_BIN NOT NULL ,
	[HtmlText] [ntext] COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END

GO

IF Not Exists(SELECT * FROM dbo.OverviewPage WHERE Culture='')
BEGIN

INSERT INTO OverviewPage ([Culture], [HtmlText])
VALUES('', '<P><SPAN id=lblOverviewContent>Printz™ skins are made with a durable vinyl material that conforms to the shape of your device and protects it from bumps and scratches. Printz™ skins also feature a residue-free adhesive, and are water-resistant so your design won’t rub off if it gets wet.</SPAN></P>
<P><SPAN id=lblSteps>To personalize your Printz™ skin:</SPAN></P>
<OL>
<LI><SPAN id=lblDesignIt><B>Design It!</B> Create your unique design by using our online design studio.<BR>Make your skin match your style!</SPAN> 
<LI><SPAN id=lblPrintIt><B>Print It!</B> Enjoy a professional, photo quality finish right from your inkjet printer at home.<BR>It’s quick and convenient.</SPAN> 
<LI><SPAN id=lblStickIt><B>Stick It!</B> Applying and removing your skin is simple – just peel and press for a custom fit.</SPAN> </LI></OL>')

END
-- OverviewPage end

-- HelpPage begin
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HelpPage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[HelpPage] (
	[Culture] [nvarchar] (10) COLLATE Latin1_General_BIN NOT NULL ,
	[HtmlText] [ntext] COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END

GO

IF Not Exists(SELECT * FROM dbo.HelpPage WHERE Culture='')
BEGIN
INSERT INTO HelpPage ([Culture], [HtmlText])
VALUES('', '<dl class="dynamicLinks">
  <span id="ctlCustomerService_lblHeader" class="ListCaption">Customer Service</span>
  <dd><a class="LinkText Active" href="mailto:feltech@neato.com" target="_blank">CONTACT US</a></dd>
</dl>
<dl class="dynamicLinks">
  <span id="ctlTechnicalSupport_lblHeader" class="ListCaption">Technical Support</span>
  <dd><a class="LinkText Active" href="mailto:feltech@neato.com" target="_blank">CONTACT US</a></dd>
</dl>
<dl class="dynamicLinks">
  <span id="ctlTermsAndConditions_lblHeader" class="ListCaption">Terms and Conditions</span>
  <dd><a class="LinkText Active" href="mailto:feltech@neato.com" target="_blank">CONTACT US</a></dd>
</dl>
<dl class="dynamicLinks">
  <span id="ctlAffiliatesProgram_lblHeader" class="ListCaption">Affiliates Program</span>
  <dd><a class="LinkText Active" href="mailto:feltech@neato.com" target="_blank">CONTACT US</a></dd>
</dl>')
END
-- HelpPage end

-- Device begin
UPDATE dbo.Device SET [Model] = N'V180' WHERE  Id = 1
UPDATE dbo.Device SET [Model] = N'Razr - V3, V3c, V3i' WHERE  Id = 2
UPDATE dbo.Device SET [Model] = N'X495/497' WHERE  Id = 3
UPDATE dbo.Device SET [Model] = N'V551' WHERE  Id = 4
UPDATE dbo.Device SET [Model] = N'6101' WHERE  Id = 5
-- Device end

-- DeviceBrand begin
UPDATE dbo.DeviceBrand SET [Name]=N'Siemens' WHERE Id=1
UPDATE dbo.DeviceBrand SET [Name]=N'Motorola' WHERE Id=2
UPDATE dbo.DeviceBrand SET [Name]=N'LG' WHERE Id=3
UPDATE dbo.DeviceBrand SET [Name]=N'Nokia' WHERE Id=4
UPDATE dbo.DeviceBrand SET [Name]=N'Samsung' WHERE Id=5
UPDATE dbo.DeviceBrand SET [Name]=N'AudioVox' WHERE Id=6
UPDATE dbo.DeviceBrand SET [Name]=N'Kyocera' WHERE Id=7
-- DeviceBrand end
--Carrier Names
UPDATE dbo.Carrier SET [Name]=N'Cingular' WHERE Id=1
UPDATE dbo.Carrier SET [Name]=N'T-mobile' WHERE Id=2
UPDATE dbo.Carrier SET [Name]=N'Cellular One / Dobson' WHERE Id=3
UPDATE dbo.Carrier SET [Name]=N'Alltel' WHERE Id=4
UPDATE dbo.Carrier SET [Name]=N'US Cellular' WHERE Id=5
UPDATE dbo.Carrier SET [Name]=N'Verizon Wireless' WHERE Id=6
UPDATE dbo.Carrier SET [Name]=N'Cellular One / Western Wireless' WHERE Id=7

GO
---------[Announcement]
ALTER TABLE [dbo].[Announcement] DROP CONSTRAINT [PK_Announcement]
GO
CREATE TABLE [dbo].[tmp_rg_xx_Announcement]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Time] [datetime] NOT NULL,
[IsActive] [bit] NOT NULL,
[Text] [ntext] COLLATE Latin1_General_BIN NOT NULL
)
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_Announcement] ON
GO
INSERT INTO [dbo].[tmp_rg_xx_Announcement]([Id], [Time], [IsActive], [Text]) SELECT [Id], [Time], [IsActive], [Text] FROM [dbo].[Announcement]
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_Announcement] OFF
GO
DROP TABLE [dbo].[Announcement]
GO
sp_rename N'[dbo].[tmp_rg_xx_Announcement]', N'Announcement'
GO
ALTER TABLE [dbo].[Announcement] ADD CONSTRAINT [PK_Announcement] PRIMARY KEY CLUSTERED  ([Id])
GO

