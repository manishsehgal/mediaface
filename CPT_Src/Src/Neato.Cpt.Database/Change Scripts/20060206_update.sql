/*
 - New LinkPlace and Page
 - Phone: Motorola v180 - contour and position updated
 - Phone: Motorola Razor V3 - contour and position updated
    
*/

if not exists (select * from [dbo].[Page] where [PageId]=21)
BEGIN

SET IDENTITY_INSERT [dbo].[Page] ON
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (21, N'ErrorPage.aspx', N'Error Page')
SET IDENTITY_INSERT [dbo].[Page] OFF

END

if not exists (select * from [dbo].[LinkPlace] where [PageId]=21 and [LinkId]=5)
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (21, 5, 0, ' ', 'CustomerService', 'N')


-------------------------------
DECLARE @ptrContour binary(16)

BEGIN TRANSACTION
--- Phone: Motorola v180 ---
SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 1
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
    <MoveTo x="106.45" y="89.7" />
    <LineTo x="46.45" y="89.7" />
    <LineTo x="46.45" y="74.7" />
    <LineTo x="106.45" y="74.7" />
    <LineTo x="106.45" y="89.7" />
    <MoveTo x="125.25" y="30.75" />
    <LineTo x="125.25" y="0" />
    <LineTo x="28.25" y="0" />
    <LineTo x="28.25" y="30.75" />
    <CurveTo cx="19.7" cy="31.95" x="12.75" y="38.35" />
    <CurveTo cx="9.6" cy="41.2" x="4.6" y="42.45" />
    <LineTo x="0" y="43.65" />
    <LineTo x="0.35" y="63.5" />
    <CurveTo cx="0.35" cy="131.05" x="7" y="180.2" />
    <CurveTo cx="8.3" cy="189.7" x="14.25" y="195.9" />
    <CurveTo cx="20.1" cy="202.1" x="29.15" y="203.55" />
    <CurveTo cx="33.2" cy="219.7" x="46.5" y="230.1" />
    <CurveTo cx="59.9" cy="240.5" x="76.75" y="240.5" />
    <CurveTo cx="93.6" cy="240.5" x="107" y="230.1" />
    <CurveTo cx="120.3" cy="219.7" x="124.35" y="203.55" />
    <CurveTo cx="133.35" cy="202.1" x="139.25" y="195.9" />
    <CurveTo cx="145.2" cy="189.7" x="146.45" y="180.2" />
    <CurveTo cx="153.1" cy="131.05" x="153.1" y="63.4" />
    <LineTo x="152.9" y="43.6" />
    <LineTo x="148.55" y="42.35" />
    <CurveTo cx="143.75" cy="41.05" x="140.75" y="38.35" />
    <CurveTo cx="133.65" cy="31.95" x="125.25" y="30.75" />
</Contour>
'

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 2
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
    <MoveTo x="134.55" y="0" />
    <LineTo x="115" y="0" />
    <CurveTo cx="107" cy="0" x="102.5" y="3" />
    <CurveTo cx="100.9" cy="4" x="97.25" y="7.15" />
    <CurveTo cx="92.1" cy="11.7" x="88.6" y="13.65" />
    <CurveTo cx="81.8" cy="17.5" x="72.55" y="17.5" />
    <CurveTo cx="63.3" cy="17.5" x="56.45" y="13.65" />
    <CurveTo cx="53.1" cy="11.8" x="47.85" y="7.15" />
    <CurveTo cx="43.8" cy="3.75" x="42.6" y="3" />
    <CurveTo cx="38.1" cy="0" x="30.05" y="0" />
    <LineTo x="10.55" y="0" />
    <CurveTo cx="6.3" cy="0" x="3.15" y="3.2" />
    <CurveTo cx="0.05" cy="6.35" x="0.05" y="10.7" />
    <LineTo x="0" y="129.6" />
    <CurveTo cx="0" cy="140.4" x="6.8" y="148.6" />
    <CurveTo cx="13.5" cy="156.7" x="23.9" y="158.55" />
    <CurveTo cx="26.25" cy="169.85" x="33.25" y="178.95" />
    <CurveTo cx="40.25" cy="188.1" x="50.4" y="193.15" />
    <LineTo x="59.05" y="197.4" />
    <LineTo x="59.05" y="187.75" />
    <CurveTo cx="59.05" cy="186.35" x="60.1" y="185.2" />
    <CurveTo cx="61.2" cy="184.1" x="62.5" y="184.1" />
    <LineTo x="82.6" y="184.1" />
    <CurveTo cx="83.9" cy="184.1" x="84.95" y="185.2" />
    <CurveTo cx="86" cy="186.35" x="86" y="187.75" />
    <LineTo x="86" y="197.4" />
    <LineTo x="94.65" y="193.15" />
    <CurveTo cx="104.85" cy="188.1" x="111.85" y="178.95" />
    <CurveTo cx="118.8" cy="169.85" x="121.2" y="158.55" />
    <CurveTo cx="131.55" cy="156.7" x="138.3" y="148.6" />
    <CurveTo cx="145.1" cy="140.4" x="145.1" y="129.65" />
    <LineTo x="145.05" y="10.7" />
    <CurveTo cx="145.05" cy="6.35" x="141.95" y="3.2" />
    <CurveTo cx="138.85" cy="0" x="134.55" y="0" />
</Contour>
'

--- Phone: Motorola Razor V3 ---
SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 3
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
    <MoveTo x="142.75" y="218.45" />
    <LineTo x="142.9" y="10" />
    <LineTo x="138.8" y="8.35" />
    <CurveTo cx="133.75" cy="6.35" x="117.3" y="3.5" />
    <CurveTo cx="102.05" cy="0.9" x="95.1" y="0.45" />
    <LineTo x="87" y="0" />
    <LineTo x="88.3" y="8.05" />
    <CurveTo cx="91.2" cy="24.75" x="91.45" y="25.85" />
    <LineTo x="91.7" y="27.15" />
    <LineTo x="53.6" y="27.15" />
    <LineTo x="53.9" y="25.85" />
    <CurveTo cx="55.15" cy="20.4" x="57" y="8.05" />
    <LineTo x="58.3" y="0" />
    <LineTo x="50.2" y="0.45" />
    <CurveTo cx="43.05" cy="0.9" x="27.25" y="3.45" />
    <CurveTo cx="9.6" cy="6.3" x="4.5" y="8.35" />
    <LineTo x="0.4" y="10" />
    <LineTo x="0.2" y="218.45" />
    <MoveTo x="0.2" y="218.45" />
    <LineTo x="0" y="270.3" />
    <LineTo x="4.2" y="271.85" />
    <CurveTo cx="15" cy="275.9" x="36.35" y="278.95" />
    <CurveTo cx="56.5" cy="281.85" x="71.3" y="281.85" />
    <CurveTo cx="86.05" cy="281.85" x="106.25" y="278.95" />
    <CurveTo cx="127.6" cy="275.9" x="138.35" y="271.85" />
    <LineTo x="142.6" y="270.3" />
    <LineTo x="142.75" y="218.45" />
    <MoveTo x="123.55" y="64.25" />
    <LineTo x="123.55" y="130.2" />
    <CurveTo cx="123.6" cy="133.1" x="123.15" y="133.9" />
    <CurveTo cx="122.5" cy="134.9" x="119.9" y="135.45" />
    <CurveTo cx="113.2" cy="136.75" x="99.2" y="138.1" />
    <CurveTo cx="83.05" cy="139.7" x="72.35" y="139.7" />
    <CurveTo cx="61.45" cy="139.7" x="45.5" y="138.15" />
    <CurveTo cx="31.65" cy="136.8" x="24.8" y="135.45" />
    <CurveTo cx="22.4" cy="135" x="21.75" y="134.1" />
    <CurveTo cx="21.15" cy="133.4" x="21.15" y="131.25" />
    <LineTo x="21.15" y="64.25" />
    <CurveTo cx="21.15" cy="60.25" x="25.3" y="60.25" />
    <LineTo x="119.4" y="60.25" />
    <CurveTo cx="123.55" cy="60.25" x="123.55" y="64.25" />
</Contour>
'

SELECT @ptrContour = TEXTPTR(Contour) FROM Face WHERE [Id] = 4
UPDATETEXT Face.Contour @ptrContour 0 NULL
'
<Contour x="0" y="0">
    <MoveTo x="95.4" y="250.6" />
    <LineTo x="95.4" y="250.6" />
    <MoveTo x="144.95" y="147.3" />
    <LineTo x="144.35" y="142.9" />
    <CurveTo cx="144.15" cy="141.45" x="144.4" y="139.4" />
    <CurveTo cx="144.85" cy="135.3" x="146.9" y="132.15" />
    <CurveTo cx="153.3" cy="122.25" x="153.3" y="108.05" />
    <LineTo x="153.3" y="0" />
    <LineTo x="145.25" y="2" />
    <CurveTo cx="137.7" cy="3.85" x="125.45" y="5.65" />
    <CurveTo cx="100.95" cy="9.3" x="76.65" y="9.3" />
    <CurveTo cx="52.35" cy="9.3" x="27.9" y="5.65" />
    <LineTo x="8.1" y="2" />
    <LineTo x="0" y="0" />
    <LineTo x="0" y="108.05" />
    <CurveTo cx="0" cy="122.25" x="6.4" y="132.15" />
    <CurveTo cx="9.05" cy="136.2" x="9.05" y="141.4" />
    <LineTo x="8.95" y="142.95" />
    <LineTo x="8.6" y="147.25" />
    <MoveTo x="8.6" y="147.25" />
    <LineTo x="9.05" y="240.1" />
    <LineTo x="8.8" y="262.3" />
    <LineTo x="13.35" y="263.8" />
    <CurveTo cx="38.15" cy="271.95" x="76.9" y="271.95" />
    <CurveTo cx="115.3" cy="271.95" x="140.05" y="263.9" />
    <LineTo x="144.5" y="262.5" />
    <LineTo x="144.95" y="147.3" />
    <MoveTo x="95.4" y="250.6" />
    <LineTo x="58.4" y="250.6" />
    <LineTo x="58.4" y="241.95" />
    <LineTo x="95.4" y="241.95" />
    <LineTo x="95.4" y="250.6" />
</Contour>
'

UPDATE dbo.FaceToPaper SET FaceX = 76.85, FaceY = 71.45 WHERE FaceId = 1 AND PaperId = 1;

UPDATE dbo.FaceToPaper SET FaceX = 386.65, FaceY = 117.7 WHERE FaceId = 2 AND PaperId = 1;

UPDATE dbo.FaceToPaper SET FaceX = 97.2, FaceY = 61.28 WHERE FaceId = 3 AND PaperId = 2;

UPDATE dbo.FaceToPaper SET FaceX = 371.9, FaceY = 59.93 WHERE FaceId = 4 AND PaperId = 2;

COMMIT TRANSACTION
