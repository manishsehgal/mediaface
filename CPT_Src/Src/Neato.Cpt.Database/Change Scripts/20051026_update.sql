/* Changes
- data to tables: Device
*/ 

BEGIN TRANSACTION

DECLARE @ptrFaces binary(16)

--- Phone: Motorola_v180 ---

IF Exists(SELECT * FROM Device WHERE (Model = N'Motorola_v180'))
BEGIN

SELECT @ptrFaces = TEXTPTR(Faces) FROM Device WHERE (Model = N'Motorola_v180')
WRITETEXT Device.Faces @ptrFaces
	'<?xml version="1.0" encoding="utf-8" ?> 
<Project>
    <Faces>
        <Face id="Face1" x="83.00" y="77.00">
            <Contour x="0" y="0">
                <MoveTo x="28.20" y="0.00" />
                <LineTo x="28.10" y="29.65" />
                <LineTo x="27.95" y="30.10" />
                <LineTo x="27.95" y="30.25" />
                <LineTo x="21.50" y="30.85" />
                <LineTo x="16.80" y="32.40" />
                <LineTo x="15.00" y="33.35" />
                <LineTo x="8.15" y="38.65" />
                <LineTo x="2.65" y="41.40" />
                <LineTo x="0.00" y="42.25" />
                <LineTo x="0.00" y="43.70" />
                <LineTo x="0.15" y="44.65" />
                <LineTo x="0.25" y="55.95" />
                <LineTo x="0.35" y="56.75" />
                <LineTo x="0.60" y="80.65" />
                <LineTo x="2.55" y="126.00" />
                <LineTo x="3.60" y="142.20" />
                <LineTo x="6.95" y="173.30" />
                <LineTo x="7.80" y="177.35" />
                <LineTo x="9.25" y="181.10" />
                <LineTo x="11.30" y="184.20" />
                <LineTo x="13.80" y="186.95" />
                <LineTo x="18.35" y="190.10" />
                <LineTo x="20.15" y="190.80" />
                <LineTo x="23.90" y="191.75" />
                <LineTo x="27.95" y="192.10" />
                <LineTo x="29.90" y="199.55" />
                <LineTo x="33.00" y="206.55" />
                <LineTo x="34.95" y="209.65" />
                <LineTo x="39.75" y="215.40" />
                <LineTo x="45.35" y="220.35" />
                <LineTo x="51.95" y="224.15" />
                <LineTo x="59.05" y="226.80" />
                <LineTo x="62.75" y="227.75" />
                <LineTo x="70.70" y="228.50" />
                <LineTo x="78.60" y="227.75" />
                <LineTo x="85.95" y="225.60" />
                <LineTo x="92.75" y="222.35" />
                <LineTo x="98.90" y="218.05" />
                <LineTo x="104.15" y="212.65" />
                <LineTo x="108.35" y="206.55" />
                <LineTo x="111.50" y="199.55" />
                <LineTo x="113.40" y="192.10" />
                <LineTo x="117.50" y="191.75" />
                <LineTo x="121.20" y="190.80" />
                <LineTo x="123.00" y="190.10" />
                <LineTo x="124.70" y="189.10" />
                <LineTo x="127.55" y="186.95" />
                <LineTo x="130.10" y="184.20" />
                <LineTo x="132.15" y="181.10" />
                <LineTo x="133.55" y="177.35" />
                <LineTo x="134.40" y="173.30" />
                <LineTo x="136.35" y="158.05" />
                <LineTo x="137.75" y="142.20" />
                <LineTo x="139.80" y="110.15" />
                <LineTo x="140.90" y="42.10" />
                <LineTo x="138.35" y="41.30" />
                <LineTo x="133.10" y="38.65" />
                <LineTo x="127.95" y="34.45" />
                <LineTo x="124.55" y="32.40" />
                <LineTo x="119.90" y="30.85" />
                <LineTo x="113.40" y="30.25" />
                <LineTo x="113.40" y="30.10" />
                <LineTo x="113.30" y="29.65" />
                <LineTo x="113.15" y="0.00" />
                <LineTo x="28.20" y="0.00" />
                <MoveTo x="60.35" y="46.20" />
                <LineTo x="61.45" y="48.10" />
                <LineTo x="62.75" y="49.80" />
                <LineTo x="64.45" y="51.10" />
                <LineTo x="66.25" y="52.10" />
                <LineTo x="68.30" y="52.70" />
                <LineTo x="70.45" y="52.95" />
                <LineTo x="72.50" y="52.70" />
                <LineTo x="74.55" y="52.10" />
                <LineTo x="76.45" y="51.10" />
                <LineTo x="78.00" y="49.80" />
                <LineTo x="79.35" y="48.10" />
                <LineTo x="80.40" y="46.20" />
                <LineTo x="81.00" y="44.30" />
                <LineTo x="81.25" y="42.10" />
                <LineTo x="81.00" y="40.10" />
                <LineTo x="80.40" y="38.05" />
                <LineTo x="79.35" y="36.10" />
                <LineTo x="78.00" y="34.45" />
                <LineTo x="76.45" y="33.10" />
                <LineTo x="74.55" y="32.15" />
                <LineTo x="72.50" y="31.55" />
                <LineTo x="70.45" y="31.35" />
                <LineTo x="68.30" y="31.55" />
                <LineTo x="66.25" y="32.15" />
                <LineTo x="64.45" y="33.10" />
                <LineTo x="62.75" y="34.45" />
                <LineTo x="61.45" y="36.10" />
                <LineTo x="60.35" y="38.05" />
                <LineTo x="59.75" y="40.10" />
                <LineTo x="59.75" y="44.30" />
                <LineTo x="60.35" y="46.20" />
                <MoveTo x="34.80" y="86.90" />
                <LineTo x="34.45" y="86.05" />
                <LineTo x="34.45" y="66.25" />
                <LineTo x="34.80" y="65.40" />
                <LineTo x="35.75" y="63.95" />
                <LineTo x="37.10" y="63.00" />
                <LineTo x="37.95" y="62.75" />
                <LineTo x="38.90" y="62.65" />
                <LineTo x="101.90" y="62.65" />
                <LineTo x="102.85" y="62.75" />
                <LineTo x="103.70" y="63.00" />
                <LineTo x="105.15" y="63.95" />
                <LineTo x="106.10" y="65.40" />
                <LineTo x="106.35" y="66.25" />
                <LineTo x="106.45" y="67.20" />
                <LineTo x="106.45" y="85.20" />
                <LineTo x="106.35" y="86.05" />
                <LineTo x="106.10" y="86.90" />
                <LineTo x="105.15" y="88.35" />
                <LineTo x="103.70" y="89.30" />
                <LineTo x="102.85" y="89.55" />
                <LineTo x="101.90" y="89.65" />
                <LineTo x="38.90" y="89.65" />
                <LineTo x="37.95" y="89.55" />
                <LineTo x="37.10" y="89.30" />
                <LineTo x="35.75" y="88.35" />
                <LineTo x="34.80" y="86.90" />
            </Contour>
            <Fill color="#DDDDEE"/>
        </Face>
        <Face id="Face2" x="392.536" y="123.333">
            <Contour x="0" y="0">
                <MoveTo x="1.30" y="1.45" />
                <LineTo x="0.35" y="3.00" />
                <LineTo x="0.00" y="4.70" />
                <LineTo x="0.10" y="126.00" />
                <LineTo x="0.45" y="128.30" />
                <LineTo x="1.80" y="132.60" />
                <LineTo x="2.75" y="134.65" />
                <LineTo x="3.95" y="136.55" />
                <LineTo x="6.70" y="140.05" />
                <LineTo x="8.25" y="141.60" />
                <LineTo x="12.00" y="144.15" />
                <LineTo x="16.05" y="145.90" />
                <LineTo x="18.20" y="146.55" />
                <LineTo x="22.90" y="147.00" />
                <LineTo x="23.85" y="152.55" />
                <LineTo x="25.55" y="157.90" />
                <LineTo x="27.80" y="162.95" />
                <LineTo x="30.70" y="167.65" />
                <LineTo x="38.00" y="175.70" />
                <LineTo x="42.35" y="179.05" />
                <LineTo x="47.00" y="181.80" />
                <LineTo x="47.25" y="179.90" />
                <LineTo x="47.90" y="178.10" />
                <LineTo x="48.75" y="176.40" />
                <LineTo x="49.85" y="174.95" />
                <LineTo x="51.25" y="173.75" />
                <LineTo x="52.85" y="172.90" />
                <LineTo x="54.65" y="172.35" />
                <LineTo x="56.55" y="172.10" />
                <LineTo x="76.60" y="172.10" />
                <LineTo x="78.50" y="172.35" />
                <LineTo x="80.30" y="172.90" />
                <LineTo x="81.85" y="173.75" />
                <LineTo x="83.30" y="174.95" />
                <LineTo x="84.40" y="176.40" />
                <LineTo x="85.35" y="178.10" />
                <LineTo x="85.85" y="179.90" />
                <LineTo x="86.05" y="181.80" />
                <LineTo x="90.75" y="179.05" />
                <LineTo x="95.05" y="175.70" />
                <LineTo x="99.05" y="171.85" />
                <LineTo x="102.40" y="167.65" />
                <LineTo x="105.25" y="162.95" />
                <LineTo x="107.55" y="157.90" />
                <LineTo x="109.25" y="152.55" />
                <LineTo x="110.20" y="147.00" />
                <LineTo x="114.85" y="146.55" />
                <LineTo x="117.05" y="145.90" />
                <LineTo x="121.10" y="144.15" />
                <LineTo x="124.85" y="141.60" />
                <LineTo x="127.95" y="138.35" />
                <LineTo x="130.35" y="134.65" />
                <LineTo x="132.15" y="130.45" />
                <LineTo x="133.00" y="126.00" />
                <LineTo x="133.10" y="4.70" />
                <LineTo x="132.75" y="3.00" />
                <LineTo x="131.80" y="1.45" />
                <LineTo x="130.35" y="0.35" />
                <LineTo x="128.55" y="0.00" />
                <LineTo x="104.80" y="0.25" />
                <LineTo x="102.25" y="0.75" />
                <LineTo x="98.45" y="3.00" />
                <LineTo x="89.90" y="10.10" />
                <LineTo x="85.60" y="12.95" />
                <CurveTo cx="83.40" cy="13.95" x="81.15" y="15.00" />
                <LineTo x="76.45" y="16.45" />
                <LineTo x="71.55" y="17.30" />
                <LineTo x="66.65" y="17.55" />
                <LineTo x="61.60" y="17.30" />
                <LineTo x="56.65" y="16.45" />
                <LineTo x="52.00" y="15.00" />
                <LineTo x="47.55" y="12.95" />
                <LineTo x="45.20" y="11.55" />
                <LineTo x="33.35" y="2.05" />
                <LineTo x="30.80" y="0.75" />
                <LineTo x="25.80" y="0.00" />
                <LineTo x="4.55" y="0.00" />
                <LineTo x="2.75" y="0.35" />
                <LineTo x="1.30" y="1.45" />
            </Contour>
            <Fill color="#DDDDEE"/>
        </Face>
    </Faces>
    <Paper w="613.20" h="394.665"/> 
</Project>
'

END

go 

COMMIT TRANSACTION