@echo off
REM: Command File Created by Microsoft Visual Database Tools
REM: Date Generated: 16.08.2005
REM: Authentication type: SQL Server
REM: Usage: CommandFilename [Server] [Database] [Login] [Password]

if '%1' == '' goto usage
if '%2' == '' goto usage
if '%3' == '' goto usage

if '%1' == '/?' goto usage
if '%1' == '-?' goto usage
if '%1' == '?' goto usage
if '%1' == '/help' goto usage

rem: osql -S %1 -d %2 -U %3 -P %4 -b -i "20051018_update.sql"
rem: if %ERRORLEVEL% NEQ 0 goto errors 

rem: osql -S %1 -d %2 -U %3 -P %4 -b -i "20051021_update.sql"
rem: if %ERRORLEVEL% NEQ 0 goto errors 

rem: osql -S %1 -d %2 -U %3 -P %4 -b -i "20051026_update.sql"
rem: if %ERRORLEVEL% NEQ 0 goto errors 

rem: osql -S %1 -d %2 -U %3 -P %4 -b -i "20051028_update.sql"
rem: if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051114_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem cd "..\Create Scripts\FillData"
rem call UploadImagesToDatabase.cmd %1 %2 %3 %4 
rem if %ERRORLEVEL% NEQ 0 goto errors
rem cd "..\..\Change Scripts"

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051117_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051121_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051123_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051124_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051128_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051129_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051130_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051202_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051207_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051208_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051219_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051221_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051223_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051227_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem cd "..\Create Scripts\FillData"
rem call UploadImagesToDatabase.cmd %1 %2 %3 %4 
rem if %ERRORLEVEL% NEQ 0 goto errors
rem cd "..\..\Change Scripts"

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20051229_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060112_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060117_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060118_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060120_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060125_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060126_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060127_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060130_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060201_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060202_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060205_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors 

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060206_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060207_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060209_update.sql"
if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060210_update.sql"
if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060214_update.sql"
if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060215_update.sql"
if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060216_update.sql"
if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060217_update.sql"
if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060220_update.usql"
if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060221_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060222_update.sql"
if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060226_update.sql"
if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060227_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060228_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060301_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060303_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060306_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060309_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060313_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060316_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060317_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060320_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060321_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060322_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060323_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060324_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060330_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060404_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060407_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060410_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060411_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060412_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060413_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060504_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060505_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060515_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060516_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060517_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060519_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060522_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060523_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060524_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060526_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060529_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060606_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060620_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

rem osql -S %1 -d %2 -U %3 -P %4 -b -i "20060707_update.sql"
rem if %ERRORLEVEL% NEQ 0 goto errors

osql -S %1 -d %2 -U %3 -P %4 -b -i "20060807_update.sql"
if %ERRORLEVEL% NEQ 0 goto errors


goto finish

REM: How to use screen
:usage
echo.
echo Usage: MyScript Server Database User [Password]
echo Server: the name of the target SQL Server
echo Database: the name of the target database
echo User: the login name on the target server
echo Password: the password for the login on the target server (optional)
echo.
echo Example: MyScript.cmd MainServer MainDatabase MyName MyPassword
echo.
echo.
goto done

REM: error handler
:errors
echo.
echo WARNING! Error(s) were detected!
echo --------------------------------
echo Please evaluate the situation and, if needed,
echo restart this command file. You may need to
echo supply command parameters when executing
echo this command file.
echo.
pause
goto done

REM: finished execution
:finish
echo.
echo Script execution is complete!
:done
@echo on
