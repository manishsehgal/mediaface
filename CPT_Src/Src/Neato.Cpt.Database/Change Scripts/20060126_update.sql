/*
	 Table FaceLayoutItem:
        - added fields and fill data: Height, Width
*/

BEGIN TRANSACTION

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FaceLayoutItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN

ALTER TABLE [dbo].[FaceLayoutItem] ADD
    [Height] [int] NOT NULL
    CONSTRAINT [DF_FaceLayoutItem_Height] DEFAULT (17),
  	[Width] [int] NOT NULL 
	CONSTRAINT [DF_FaceLayoutItem_Width] DEFAULT (60)
	
END

GO

COMMIT