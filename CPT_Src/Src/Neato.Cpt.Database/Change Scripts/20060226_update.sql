/* Changes
- data to tables:
    - MailType (Phone request notification)
    - Page, Link, LinkLocalization, LinkPlace (Support page link)
*/ 

-- Phone request notification begin
BEGIN TRANSACTION

IF Not Exists (SELECT * FROM [dbo].[MailType] WHERE [MailId] = 5)
BEGIN
SET IDENTITY_INSERT [dbo].[MailType] ON
INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (5, 'Phone request notification')
SET IDENTITY_INSERT [dbo].[MailType] OFF
END

IF Not Exists (SELECT * FROM [dbo].[MailFormat] WHERE [MailId] = 5)
INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (5, '', N'Your phone was added!', N'Your phone was added!')

COMMIT
-- Phone request notification end

-- Dynamic links redesign
BEGIN TRANSACTION
IF NOT Exists (SELECT * FROM [dbo].[Page] WHERE [PageId] = 1 AND [Url] = 'Default Menu Page')
BEGIN
delete dbo.LinkPlace
delete dbo.LinkLocalization
delete dbo.Link
delete dbo.Page

SET IDENTITY_INSERT [dbo].[Page] ON
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (1, N'Default Menu Page', N'Default Menu Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (2, N'default.aspx', N'Home Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (3, N'overview.aspx', N'Overview Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (4, N'Support.aspx', N'Support Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (5, N'tellafriend.aspx', N'Tell a Friend Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (6, N'', N'CopyRight Label')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (7, N'mailto:feltech@neato.com', N'Contacts Page')
SET IDENTITY_INSERT [dbo].[Page] OFF

SET IDENTITY_INSERT [dbo].[Link] ON
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (2, 2)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (3, 3)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (4, 4)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (5, 5)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (6, 6)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (7, 7)
SET IDENTITY_INSERT [dbo].[Link] OFF

INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (2, N'', N'HOME')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (3, N'', N'OVERVIEW')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (4, N'', N'SUPPORT')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (5, N'', N'TELL A FRIEND')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (6, N'', N'Copyright &copy; 2006, Fellowes, Inc.')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (7, N'', N'CONTACT US')

INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 2, 0, ' ', 'Header', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 3, 1, ' ', 'Header', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 4, 2, ' ', 'Header', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 5, 3, ' ', 'Header', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 6, 4, 'L', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 2, 5, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 7, 6, 'R', 'Footer', 'Y')
END
COMMIT
-- Dynamic links redesign end