 /* Changes
- new table: tracking features

*/ 

BEGIN TRANSACTION

-- table

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Tracking_Features]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN


CREATE TABLE [dbo].[Tracking_Features] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Login] [varchar] (200) COLLATE Latin1_General_CS_AS NOT NULL ,
	[Action] [varchar] (200) COLLATE Latin1_General_CS_AS NOT NULL ,
	[Time] [datetime] NOT NULL 
) ON [PRIMARY]

ALTER TABLE [dbo].[Tracking_Features] WITH NOCHECK ADD 
	CONSTRAINT [PK_FeaturesTracking] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 

END


COMMIT

