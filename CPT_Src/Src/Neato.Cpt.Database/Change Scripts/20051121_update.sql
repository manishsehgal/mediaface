if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Link]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[Link] (
	[LinkId] [int] IDENTITY (1, 1) NOT NULL ,
	[PageId] [int] NOT NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LinkLocalization]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[LinkLocalization] (
	[LinkId] [int] NOT NULL ,
	[Culture] [nvarchar] (10) COLLATE Latin1_General_BIN NOT NULL ,
	[Text] [nvarchar] (50) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]
END

GO


if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LinkPlace]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[LinkPlace] (
	[PageId] [int] NOT NULL ,
	[LinkId] [int] NOT NULL ,
	[SortOrder] [int] NOT NULL ,
	[Align] [char] (1) COLLATE Latin1_General_BIN NOT NULL ,
	[Place] [char] (1) COLLATE Latin1_General_BIN NOT NULL ,
	[IsNewWindow] [char] (1) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]
ALTER TABLE [dbo].[LinkPlace] ADD CONSTRAINT [CK_LinkPlacePlace] CHECK ([Place] = 'H' or [Place] = 'F')
ALTER TABLE [dbo].[LinkPlace] ADD CONSTRAINT [CK_LinkPlaceAlign] CHECK ([Align] = 'R' or [Align] = 'L' or [Align] = '')
ALTER TABLE [dbo].[LinkPlace] ADD CONSTRAINT [CK_LinkPlaceNewWindow] CHECK ([IsNewWindow] = 'N' or [IsNewWindow] = 'Y')
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Page]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[Page] (
	[PageId] [int] IDENTITY (1, 1) NOT NULL ,
	[Url] [nvarchar] (50) COLLATE Latin1_General_BIN NOT NULL ,
	[Description] [nvarchar] (50) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]
END

GO

---------------------------------------------
if not exists (select * from dbo.sysobjects where id = object_id(N'PK_Links') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[Link] WITH NOCHECK ADD 
	CONSTRAINT [PK_Links] PRIMARY KEY  CLUSTERED 
	(
		[LinkId]
	)  ON [PRIMARY] 
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_Page') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[Page] WITH NOCHECK ADD 
	CONSTRAINT [PK_Page] PRIMARY KEY  CLUSTERED 
	(
		[PageId]
	)  ON [PRIMARY] 
GO

--------------------------------------------------

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Link_Page]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Link] ADD 
	CONSTRAINT [FK_Link_Page] FOREIGN KEY 
	(
		[PageId]
	) REFERENCES [dbo].[Page] (
		[PageId]
	)
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_LinkLocalization_Link]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[LinkLocalization] ADD 
	CONSTRAINT [FK_LinkLocalization_Link] FOREIGN KEY 
	(
		[LinkId]
	) REFERENCES [dbo].[Link] (
		[LinkId]
	)
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_LinkPlace_Link]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[LinkPlace] ADD 
	CONSTRAINT [FK_LinkPlace_Link] FOREIGN KEY 
	(
		[LinkId]
	) REFERENCES [dbo].[Link] (
		[LinkId]
	)
	
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_LinkPlace_Page]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[LinkPlace] ADD 
	CONSTRAINT [FK_LinkPlace_Page] FOREIGN KEY 
	(
		[PageId]
	) REFERENCES [dbo].[Page] (
		[PageId]
	)
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_LinkLocalization_Link]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[LinkLocalization] ADD 
	CONSTRAINT [FK_LinkLocalization_Link] FOREIGN KEY 
	(
		[LinkId]
	) REFERENCES [dbo].[Link] (
		[LinkId]
	)
GO

----------------------------------------------------------
if not exists (select top 1* from dbo.Page)
BEGIN
SET IDENTITY_INSERT [dbo].[Page] ON
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (1, N'default.aspx', N'Home Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (2, N'overview.aspx', N'Overview Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (3, N'help.aspx', N'Help Ppage')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (4, N'http://www.bodyglove.com', N'Company Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (5, N'mailto:info@bodyglove.com', N'Contacts Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (6, N'sitemap.aspx', N'Site Map Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (7, N'tellafriend.aspx', N'Tell a Friend Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (8, N'', N'CopyRight Label')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (9, N'', N'Our Brannds Label')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (10, N'http://www.bodyglove.com', N'BodyGlove Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (11, N'http://www.fellowes.com', N'Fellowes Page')
SET IDENTITY_INSERT [dbo].[Page] OFF
END

if not exists (select top 1* from dbo.Link)
BEGIN
SET IDENTITY_INSERT [dbo].[Link] ON
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (1, 1)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (2, 2)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (3, 3)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (4, 4)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (5, 5)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (6, 6)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (7, 7)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (8, 8)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (9, 9)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (10, 10)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (11, 11)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (12, 6)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (13, 5)
SET IDENTITY_INSERT [dbo].[Link] OFF
END

if not exists (select top 1* from dbo.LinkLocalization)
BEGIN
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (1, N'', N'Home')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (2, N'', N'OVERVIEW')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (3, N'', N'HELP')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (4, N'', N'COMPANY')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (5, N'', N'CONTACTS')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (6, N'', N'SITE MAP')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (7, N'', N'TELL A FRIEND')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (8, N'', N'Body Glove&reg; 2005')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (9, N'', N'Our brands:')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (10, N'', N'Body Glove')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (11, N'', N'Fellowes')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (12, N'', N'Site Map')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (13, N'', N'Contact Us') 
END

if not exists (select top 1* from dbo.LinkPlace)
BEGIN
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 2, 0, ' ', 'H', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 3, 1, ' ', 'H', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 4, 2, ' ', 'H', 'Y')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 5, 3, ' ', 'H', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 6, 4, ' ', 'H', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 7, 5, ' ', 'H', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 8, 7, 'L', 'F', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 9, 8, 'R', 'F', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 10, 9, 'R', 'F', 'Y')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 11, 10, 'R', 'F', 'Y')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (2, 8, 0, 'L', 'F', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (2, 1, 1, 'R', 'F', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (2, 12, 2, 'R', 'F', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (2, 13, 3, 'R', 'F', 'N')
END