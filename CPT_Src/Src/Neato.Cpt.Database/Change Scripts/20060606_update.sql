/* Changes
- add column Retailers.Mp3Available
- add exitpage
*/

BEGIN TRANSACTION

-- add column Retailers.Mp3Available
EXEC sp_columns @table_name = 'Retailers', @table_owner='dbo', @column_name = 'Mp3Available'
IF @@ROWCOUNT = 0
BEGIN
    ALTER TABLE dbo.Retailers ADD Mp3Available BIT NOT NULL
    CONSTRAINT DF_Retailers_Mp3Available DEFAULT (0)
END
GO


if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ExitPage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[ExitPage] (
	[RetailerId] [int] NOT NULL ,
	[HtmlText] [ntext] COLLATE Latin1_General_BIN NOT NULL ,
	[Enabled] [bit] NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_ExitPage_Retailers]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[ExitPage] ADD 
	CONSTRAINT [FK_ExitPage_Retailers] FOREIGN KEY 
	(
		[RetailerId]
	) REFERENCES [dbo].[Retailers] (
		[Id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

COMMIT