/* Changes
- modify type of column Retailers.Mp3Available
- add column Retailers.PhoneAvailable
- add column Retailers.StickerAvailable
*/

BEGIN TRANSACTION

-- modify type of column Retailers.Mp3Available

if exists (select * from dbo.sysobjects where id = object_id(N'DF_Retailers_Mp3Available') and OBJECTPROPERTY(id, N'IsDefaultCnst') = 1)
BEGIN
    ALTER TABLE [dbo].[Retailers] DROP CONSTRAINT [DF_Retailers_Mp3Available]
END
GO

EXEC sp_columns @table_name = 'Retailers', @table_owner='dbo', @column_name = 'Mp3Available'
IF @@ROWCOUNT > 0
BEGIN
    ALTER TABLE [dbo].[Retailers] ALTER COLUMN [Mp3Available] [int] NOT NULL
    ALTER TABLE [dbo].[Retailers] ADD CONSTRAINT [DF_Retailers_Mp3Available] DEFAULT (0) FOR [Mp3Available]
END
GO

EXEC sp_columns @table_name = 'Retailers', @table_owner='dbo', @column_name = 'PhoneAvailable'
IF @@ROWCOUNT = 0
BEGIN
    ALTER TABLE [dbo].[Retailers] ADD [PhoneAvailable] [int] NOT NULL
    CONSTRAINT [DF_Retailers_PhoneAvailable] DEFAULT (1)
END
GO

EXEC sp_columns @table_name = 'Retailers', @table_owner='dbo', @column_name = 'StickerAvailable'
IF @@ROWCOUNT = 0
BEGIN
    ALTER TABLE [dbo].[Retailers] ADD [StickerAvailable] [int] NOT NULL
    CONSTRAINT [DF_Retailers_StickerAvailable] DEFAULT (0)
END
GO

IF NOT EXISTS (SELECT Id FROM [dbo].[DeviceType] WHERE Id = 6)
BEGIN
  INSERT INTO [dbo].[DeviceType] ([Id], [Name]) VALUES (6, 'Sticker')
END

GO

COMMIT