ALTER TABLE [dbo].[Customer] DROP
    CONSTRAINT [IX_Customer]
GO

ALTER TABLE [dbo].[Customer]
    ALTER COLUMN [FirstName] [nvarchar] (100) COLLATE Latin1_General_CI_AI NOT NULL
ALTER TABLE [dbo].[Customer]
    ALTER COLUMN [LastName] [nvarchar] (100) COLLATE Latin1_General_CI_AI NOT NULL
ALTER TABLE [dbo].[Customer]
    ALTER COLUMN [Email] [varchar] (100) COLLATE Latin1_General_CI_AI NOT NULL

GO

ALTER TABLE [dbo].[Customer] ADD 
    CONSTRAINT [IX_Customer] UNIQUE  NONCLUSTERED 
    (
    [Email]
    )  ON [PRIMARY]
GO