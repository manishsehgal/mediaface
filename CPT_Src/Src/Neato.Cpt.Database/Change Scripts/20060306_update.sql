/*
    Remove old procedures:
        - PrDeviceBrandGetByCarrierId
        - PrDeviceBrandsEnumerate
    Add new column:
        SortOrder -> Device table
*/

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceBrandGetByCarrierId]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceBrandGetByCarrierId]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceBrandsEnumerate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceBrandsEnumerate]
GO

EXEC sp_columns @table_name = 'Device', @table_owner='dbo', @column_name = 'SortOrder'
IF @@ROWCOUNT = 0
BEGIN
ALTER TABLE dbo.Device ADD
	SortOrder int NOT NULL CONSTRAINT DF_Device_SortOrder DEFAULT 0
END
GO

EXEC sp_columns @table_name = 'Device', @table_owner='dbo', @column_name = 'Model'
IF @@ROWCOUNT <> 0
BEGIN
    ALTER TABLE dbo.Device ALTER COLUMN Model nvarchar(50) COLLATE Latin1_General_CI_AS NOT NULL
END

EXEC sp_columns @table_name = 'PhoneRequest', @table_owner='dbo', @column_name = 'PhoneModel'
IF @@ROWCOUNT <> 0
BEGIN
    ALTER TABLE dbo.PhoneRequest ALTER COLUMN PhoneModel nvarchar(100) COLLATE Latin1_General_CI_AS NOT NULL
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPasswordUidGet]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrPasswordUidGet]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPasswordUidUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrPasswordUidUpd]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HelpPage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[HelpPage]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'PK_PhoneRequest') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE dbo.PhoneRequest
	DROP CONSTRAINT PK_PhoneRequest
GO
	
ALTER TABLE dbo.PhoneRequest ADD CONSTRAINT
	PK_PhoneRequest PRIMARY KEY CLUSTERED 
	(
	Id
	) ON [PRIMARY]
GO
