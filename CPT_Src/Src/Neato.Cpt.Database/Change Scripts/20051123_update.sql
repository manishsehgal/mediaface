BEGIN TRANSACTION

--- Begin Update DynamicLink Schema
--delete tables
if exists (select * from dbo.sysobjects where id = object_id(N'[LinkLocalization]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [LinkLocalization]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[LinkPlace]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [LinkPlace]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Link]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [Link]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Page]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [Page]
GO

--- create tables
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Link]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[Link] (
	[LinkId] [int] IDENTITY (1, 1) NOT NULL ,
	[PageId] [int] NOT NULL 
) ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LinkLocalization]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[LinkLocalization] (
	[LinkId] [int] NOT NULL ,
	[Culture] [nvarchar] (10) COLLATE Latin1_General_BIN NOT NULL ,
	[Text] [nvarchar] (50) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LinkPlace]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[LinkPlace] (
	[PageId] [int] NOT NULL ,
	[LinkId] [int] NOT NULL ,
	[SortOrder] [int] NOT NULL ,
	[Align] [char] (1) COLLATE Latin1_General_BIN NOT NULL ,
	[Place] [varchar] (50) COLLATE Latin1_General_BIN NOT NULL ,
	[IsNewWindow] [char] (1) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]
END
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Page]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [dbo].[Page] (
	[PageId] [int] IDENTITY (1, 1) NOT NULL ,
	[Url] [nvarchar] (50) COLLATE Latin1_General_BIN NOT NULL ,
	[Description] [nvarchar] (50) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]
END

---create primary keys constraints
ALTER TABLE [dbo].[Page] WITH NOCHECK ADD 
	CONSTRAINT [PK_Page] PRIMARY KEY  CLUSTERED 
	(
		[PageId]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Link] WITH NOCHECK ADD 
	CONSTRAINT [PK_Links] PRIMARY KEY  CLUSTERED 
	(
		[LinkId]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[LinkPlace] ADD 
	CONSTRAINT [CK_LinkPlaceAlign] CHECK ([Align] = 'R' or [Align] = 'L' or [Align] = ''),
	CONSTRAINT [CK_LinkPlaceNewWindow] CHECK ([IsNewWindow] = 'N' or [IsNewWindow] = 'Y')
GO

---create foreign key constraints
ALTER TABLE [dbo].[Link] ADD 
	CONSTRAINT [FK_Link_Page] FOREIGN KEY 
	(
		[PageId]
	) REFERENCES [dbo].[Page] (
		[PageId]
	)
GO

ALTER TABLE [dbo].[LinkLocalization] ADD 
	CONSTRAINT [FK_LinkLocalization_Link] FOREIGN KEY 
	(
		[LinkId]
	) REFERENCES [dbo].[Link] (
		[LinkId]
	)
GO

ALTER TABLE [dbo].[LinkPlace] ADD 
	CONSTRAINT [FK_LinkPlace_Link] FOREIGN KEY 
	(
		[LinkId]
	) REFERENCES [dbo].[Link] (
		[LinkId]
	),
	CONSTRAINT [FK_LinkPlace_Page] FOREIGN KEY 
	(
		[PageId]
	) REFERENCES [dbo].[Page] (
		[PageId]
	)
GO
--- End Update DynamicLink Schema

--- data
if not exists (select top 1* from dbo.Page)
BEGIN
SET IDENTITY_INSERT [dbo].[Page] ON
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (1, N'default.aspx', N'Home Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (2, N'overview.aspx', N'Overview Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (3, N'help.aspx', N'Help Ppage')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (4, N'http://www.bodyglove.com', N'Company Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (5, N'mailto:info@bodyglove.com', N'Contacts Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (6, N'sitemap.aspx', N'Site Map Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (7, N'tellafriend.aspx', N'Tell a Friend Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (8, N'', N'CopyRight Label')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (9, N'', N'Our Brannds Label')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (10, N'http://www.bodyglove.com', N'BodyGlove Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (11, N'http://www.fellowes.com', N'Fellowes Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (12, N'DeviceSelect.aspx', N'DeviceSelect Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (13, N'Designer.aspx', N'Designer Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (14, N'PrintPdf.aspx', N'PrintPdf Page')
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description]) VALUES (15, N'CustomerAccount.aspx', N'CustomerAccount Page')
SET IDENTITY_INSERT [dbo].[Page] OFF

END

if not exists (select top 1* from dbo.Link)
BEGIN
SET IDENTITY_INSERT [dbo].[Link] ON
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (1, 1)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (2, 2)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (3, 3)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (4, 4)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (5, 5)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (6, 6)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (7, 7)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (8, 8)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (9, 9)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (10, 10)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (11, 11)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (12, 6)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (13, 5)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (14, 2)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (15, 7)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (16, 5)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (17, 5)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (18, 5)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (19, 5)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (20, 1)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (21, 1)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (22, 1)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (23, 1)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (24, 1)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (25, 1)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (26, 1)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (27, 1)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (28, 1)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (29, 1)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (30, 1)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (31, 1)
INSERT INTO [dbo].[Link] ([LinkId], [PageId]) VALUES (32, 4)
SET IDENTITY_INSERT [dbo].[Link] OFF
END

if not exists (select top 1* from dbo.LinkLocalization)
BEGIN
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (1, N'', N'Home')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (2, N'', N'OVERVIEW')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (3, N'', N'HELP')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (4, N'', N'COMPANY')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (5, N'', N'CONTACTS')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (6, N'', N'SITE MAP')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (7, N'', N'TELL A FRIEND')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (8, N'', N'Body Glove&reg; 2005')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (9, N'', N'Our brands:')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (10, N'', N'Body Glove')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (11, N'', N'Fellowes')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (12, N'', N'Site Map')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (13, N'', N'Contact Us')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (14, N'', N'Overview')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (15, N'', N'Tell a Friend')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (16, N'', N'Customer Service')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (17, N'', N'Technical Support')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (18, N'', N'Terms of Use')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (19, N'', N'Affiliates Program')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (20, N'', N'Log in')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (21, N'', N'I am a new user')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (22, N'', N'Edit my profile')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (23, N'', N'I forgot my password')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (24, N'', N'Log out')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (25, N'', N'Select device and model')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (26, N'', N'Recall last edited model')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (27, N'', N'Open my design')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (28, N'', N'View my public designs')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (29, N'', N'View all public designs')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (30, N'', N'Quick Search')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (31, N'', N'Advanced Search')
INSERT INTO [dbo].[LinkLocalization] ([LinkId], [Culture], [Text]) VALUES (32, N'', N'Company')
END

if not exists (select top 1* from dbo.LinkPlace)
BEGIN
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 2, 0, ' ', 'Header', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 3, 1, ' ', 'Header', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 4, 2, ' ', 'Header', 'Y')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 5, 3, ' ', 'Header', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 6, 4, ' ', 'Header', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 7, 5, ' ', 'Header', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 8, 7, 'L', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 9, 8, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 10, 9, 'R', 'Footer', 'Y')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (1, 11, 10, 'R', 'Footer', 'Y')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (2, 8, 0, 'L', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (2, 1, 1, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (2, 12, 2, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (2, 13, 3, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (3, 8, 0, 'L', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (3, 1, 1, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (3, 12, 2, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (3, 13, 3, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (3, 13, 4, ' ', 'CustomerService', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (3, 13, 5, ' ', 'TechnicalSupport', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (3, 13, 6, ' ', 'TermsAndConditions', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (3, 13, 7, ' ', 'AffiliatesProgram', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 8, 0, 'L', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 1, 1, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 12, 2, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 13, 3, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 1, 4, ' ', 'General', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 14, 5, ' ', 'General', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 15, 6, ' ', 'General', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 16, 7, ' ', 'Help', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 17, 8, ' ', 'Help', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 18, 9, ' ', 'Help', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 19, 10, ' ', 'Help', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 20, 11, ' ', 'RegisterAndLogin', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 21, 12, ' ', 'RegisterAndLogin', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 22, 13, ' ', 'RegisterAndLogin', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 23, 14, ' ', 'RegisterAndLogin', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 24, 15, ' ', 'RegisterAndLogin', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 25, 16, ' ', 'DesignYourDevice', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 26, 17, ' ', 'DesignYourDevice', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 27, 18, ' ', 'DesignYourDevice', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 28, 19, ' ', 'DesignYourDevice', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 29, 20, ' ', 'DesignYourDevice', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 30, 21, ' ', 'Search', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 31, 22, ' ', 'Search', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 32, 23, ' ', 'CompanyInfo', 'Y')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 13, 24, ' ', 'CompanyInfo', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 10, 25, ' ', 'OurBrands', 'Y')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (6, 11, 26, ' ', 'OurBrands', 'Y')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (7, 8, 0, 'L', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (7, 1, 1, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (7, 13, 2, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (7, 12, 3, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (12, 8, 0, 'L', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (12, 1, 1, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (12, 12, 2, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (12, 13, 3, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (13, 8, 0, 'L', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (13, 1, 1, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (13, 13, 3, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (14, 8, 0, 'L', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (14, 13, 1, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (15, 8, 0, 'L', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (15, 1, 1, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (15, 13, 2, 'R', 'Footer', 'N')
INSERT INTO [dbo].[LinkPlace] ([PageId], [LinkId], [SortOrder], [Align], [Place], [IsNewWindow]) VALUES (15, 12, 3, 'R', 'Footer', 'N')
END

COMMIT