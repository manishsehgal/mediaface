/*
    Changes:
        - modify table: Customer
        - del column&constraint: LastEditedPaperId
*/
BEGIN TRANSACTION

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Customer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Customer_Paper_LastEdited]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Customer_Paper_LastEdited]	

EXEC sp_columns @table_name = 'Customer', @table_owner='dbo', @column_name = 'LastEditedPaperId'
IF (@@ROWCOUNT <> 0) ALTER TABLE dbo.Customer DROP COLUMN [LastEditedPaperId]

END

COMMIT

GO