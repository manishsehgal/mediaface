/*Changes -  - added 2 columns to facelayout item*/

BEGIN TRANSACTION

-----------------------------------------------
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PaperType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[PaperType] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) COLLATE Latin1_General_BIN NOT NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'PK_PaperType') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[PaperType] WITH NOCHECK ADD 
	CONSTRAINT [PK_PaperType] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

if not exists (select top 1 * from dbo.PaperType)
BEGIN
SET IDENTITY_INSERT [dbo].[PaperType] ON
INSERT INTO [dbo].[PaperType] ([Id], [Name]) VALUES (1, 'Die-Cut')
INSERT INTO [dbo].[PaperType] ([Id], [Name]) VALUES (2, 'Universal')
SET IDENTITY_INSERT [dbo].[PaperType] OFF
END
GO

EXEC sp_columns @table_name = 'Paper', @table_owner = 'dbo', @column_name = 'PaperType'
IF @@rowcount = 0 
ALTER TABLE Paper ADD
    PaperType int NOT NULL CONSTRAINT [DF_Paper_PaperType] DEFAULT (1)
GO

if not exists (select * from dbo.sysobjects where id = object_id(N'FK_Paper_PaperType') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Paper] WITH NOCHECK ADD
CONSTRAINT [FK_Paper_PaperType] FOREIGN KEY ([PaperType]) REFERENCES [dbo].[PaperType]

-----------------------------------------------
EXEC sp_columns @table_name = 'FaceLayoutItem', @table_owner='dbo', @column_name = 'UnitType'
IF @@ROWCOUNT = 0
BEGIN
    ALTER TABLE dbo.FaceLayoutItem ADD  UnitType [varchar] (50) COLLATE Latin1_General_CS_AS  NOT NULL DEFAULT ('TextUnit')
END

EXEC sp_columns @table_name = 'FaceLayoutItem', @table_owner='dbo', @column_name = 'EffectNode'
IF @@ROWCOUNT = 0
BEGIN
    ALTER TABLE dbo.FaceLayoutItem ADD  EffectNode [ntext] COLLATE Latin1_General_CS_AS NULL 
END


COMMIT 