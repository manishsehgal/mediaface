/*
    Changes:
        - update Paper names
*/

UPDATE dbo.Paper SET [Name] = N'Motorola v180' WHERE [Name] = N'Neato - Motorola v180'
UPDATE dbo.Paper SET [Name] = N'Motorola Razr - V3, V3c, V3i' WHERE [Name] = N'Neato - Motorola RAZR'
UPDATE dbo.Paper SET [Name] = N'Samsung X495/497' WHERE [Name] = N'Neato - Samsung X495/497'
go

ALTER TABLE [dbo].[Announcement] ALTER COLUMN [Time] datetime NOT NULL