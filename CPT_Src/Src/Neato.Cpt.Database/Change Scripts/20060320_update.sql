/*
    Changes:
        - manufacturer added to names of paper fo phones: 
          Sanyo SCP-200, Sanyo VI-2300, UTStarcom PPC-6700 / XV-6700 / HTC Apache
*/

-- Paper Data
IF EXISTS (SELECT * FROM dbo.Paper WHERE (Name='SCP-200'))
    UPDATE dbo.Paper SET Name=N'Sanyo SCP-200' WHERE (Name='SCP-200')
IF EXISTS (SELECT * FROM dbo.Paper WHERE (Name='VI-2300'))
    UPDATE dbo.Paper SET Name=N'Sanyo VI-2300' WHERE (Name='VI-2300')
IF EXISTS (SELECT * FROM dbo.Paper WHERE (Name='PPC-6700, XV-6700, HTC Apache'))
    UPDATE dbo.Paper SET Name=N'UTStarcom PPC-6700, XV-6700, HTC Apache' WHERE (Name='PPC-6700, XV-6700, HTC Apache')

