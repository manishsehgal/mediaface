 /* Changes
- new table: Faq and data for this table
*/ 

BEGIN TRANSACTION

-- table
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Faq]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Faq] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Question] [ntext] COLLATE Latin1_General_BIN NOT NULL ,
	[Answer] [ntext] COLLATE Latin1_General_BIN NOT NULL ,
	[SortOrder] [int] NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

ALTER TABLE [dbo].[Faq] WITH NOCHECK ADD 
	CONSTRAINT [PK_Faq] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 

ALTER TABLE [dbo].[Faq] ADD 
	CONSTRAINT [DF_Faq_Question] DEFAULT ('') FOR [Question],
	CONSTRAINT [DF_Faq_Answer] DEFAULT ('') FOR [Answer],
	CONSTRAINT [DF_Faq_SortOrder] DEFAULT (1) FOR [SortOrder]

END

GO

-- data
IF NOT Exists (SELECT * FROM [dbo].[Faq] WHERE ([Id] >= 1 AND [Id] <= 22))
BEGIN

SET IDENTITY_INSERT dbo.Faq ON

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(1, 'What is a Printz™ skin?', '', 1)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(2, 'How do I find out if you make Printz™ skins for my device or model? ', 'Go to the Home page under "Select Your Device" and click on the device you wish to check.  Available models will appear on the right side of your screen.', 2)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(3, 'How often are devices added to the Home page?', 'As hot new devices are launched, we''ll create Printz™ skins for them and add them to the site.', 3)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(4, 'How do I create my own look?', 'You can create a unique look by using the design tools.  You can add color, manipulate text and images, add shapes, or even paint – the possibilities are endless!', 4)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(5, 'Are Printz™ skins easy to remove?', 'Super easy!  To remove, just peel the old skin off.  Printz™ skins won''t damage your device or leave any residue.', 5)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(6, 'What parts of my device does it cover?', 'Printz™ skins cover the main surfaces on the front and back of your device. The keys, buttons and the screen will not be covered.', 6)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(7, 'Do Printz™ skins leave residue?', 'No. Our skins are made with an adhesive that won''t leave any residue behind when removed from your device.', 7)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(8, 'Can I order refills?', 'No.  To get more Printz™ skins, please return to the original place of purchase.', 8)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(9, 'How long does it take to apply a Printz™ skin?', 'It takes no time at all!', 9)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(10, 'Do Printz™ skins come with a warranty or guarantee?', 'Fellowes is committed to providing you with high-quality products. If your Printz™ skins do not provide complete satisfaction, please call 1-888-312-7561 for assistance.', 10)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(11, 'How long do Printz™ skins last?', 'A single skin can last up to a year depending on how much wear and tear is applied. To keep your design fresh, we recommend changing your skin monthly.', 11)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(12, 'Will Printz™ skins cover my camera lens?', 'No. Our skins are specifically designed to fit each device. All functional buttons remain unblocked and accessible.', 12)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(13, 'Can I place two skins on top of each other?', 'Yes. Make sure they are perfectly aligned so you do not have trouble accessing functional features.', 13)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(14, 'Are Printz™ skins water-resistant?', 'Yes. They are made with a tough water-and tear-resistant vinyl material.', 14)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(15, 'Where can I purchase Printz™ skins?', 'Printz™ skins can currently be purchased at Best Buy.', 15)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(16, 'Can I upload any image and text I want?', 'Yes. As long as your image is in a jpeg, gif, bmp, png, or tif format, you can place it in your design.  Your creations are only limited by your imagination!', 16)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(17, 'How can I tell which phone model I have?', 'Take your battery out of your phone and look behind it.  Your model number will be printed inside your phone.', 17)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(18, 'Which printers work with Printz™ skins?', 'For best results, use an inkjet printer ONLY.', 18)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(19, 'Will Printz™ skins work with laser printers?', 'They are not recommended for use with laser printers.', 19)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(20, 'Can I design Printz™ skins on a Mac and a PC?', 'Yes! Our Skins can be designed on either a Mac or a PC.', 20)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(21, 'Why do I need to print a practice page?', 'All printers are set-up differently; by using a practice sheet before printing on the actual skin, you can ensure that your printer is set correctly so your design is properly aligned.', 21)

INSERT INTO dbo.Faq ([Id], [Question], [Answer], [SortOrder]) 
VALUES(22, 'What if my model is not listed?', 'If you don''t see your device on our site, send us a request. We''ll notify you as soon as it is available on our site! (Send to email)', 22)
  
SET IDENTITY_INSERT dbo.Faq OFF

END

COMMIT

EXEC sp_columns @table_name = 'LinkPlace', @table_owner = 'dbo', @column_name = 'IsVisible'
IF @@rowcount = 0
BEGIN
    ALTER TABLE [dbo].[LinkPlace] ADD
        [IsVisible] BIT NOT NULL,
        CONSTRAINT [DF_LinkPlace_IsVisible] DEFAULT (1) FOR [IsVisible]
END
GO