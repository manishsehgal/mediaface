my $Dir =  "O:/PROJECTSRES/Neato/CPT/TEMP/"; 

open F, "<" . $Dir . "_phonesList.txt";
open O, ">dbo.MyTegoPhones.sql";

	print O "DECLARE \@TId AS INT\n";
	print O "DECLARE \@DId AS INT\n";
	print O "DECLARE \@MId AS INT\n";
	print O "DECLARE \@DeviceId AS INT\n";
	print O "DECLARE \@DeviceBrandId AS INT\n";

my $DeviceId=5000;

	print O "set \@DeviceBrandId = (SELECT MAX(Id) AS MaxId FROM dbo.DeviceBrand)\n";
	print O "set \@DeviceId = (SELECT MAX(Id) AS MaxId FROM dbo.Device)\n";
	print O "if \@DeviceId < $DeviceId\n";
	print O "  set \@DeviceId = $DeviceId\n";
	print O "set \@TId = (select TOP 1 Id from dbo.DeviceType where (Name = \'Phone\'))\n\n";

while($line=<F>) {
	@fields=split('\t',$line,4);
	$manufacturer = @fields[1];
	$model = @fields[2];

# add Device

	print O "--- $manufacturer $model ---\n\n";
# get or add DeviceBrand
	print O "if exists (select * from dbo.DeviceBrand where (UPPER(Name) = \'" . uc($manufacturer) . "\'))\n";
	print O "   set \@MId = (select TOP 1 Id from dbo.DeviceBrand where (UPPER(Name) = \'" . uc($manufacturer) . "\'))\n";
	print O "else begin\n";
	print O "  set \@DeviceBrandId = (\@DeviceBrandId+1)\n";
	print O "  SET IDENTITY_INSERT dbo.DeviceBrand ON\n";
	print O "  INSERT INTO dbo.DeviceBrand ([Id], [Name], [Icon]) VALUES (\@DeviceBrandId, \'$manufacturer\', null)\n";
	print O "  SET IDENTITY_INSERT dbo.DeviceBrand OFF\n";
	print O "  set \@MId = \@DeviceBrandId\n";
	print O "end\n";
# get or add Device if not already in DB 
	print O "if exists (select * from dbo.Device where (BrandId = \@MId and UPPER(Model) = \'" . uc($model) . "\'))\n";
	print O "  set \@DId = (select TOP 1 Id from dbo.Device where (BrandId = \@MId and UPPER(Model) = \'" . uc($model) . "\'))\n";
	print O "else begin\n";
	print O "  set \@DeviceId = (\@DeviceId+1)\n";
	print O "  set identity_insert dbo.Device on\n";
	print O "  INSERT INTO dbo.Device (Id, Model, BrandId, DeviceTypeId, Icon, IconBig, SortOrder)\n";
	print O "    VALUES (\@DeviceId, N\'$model\', \@MId, \@TId,\n";
	open I, "<".$Dir."Thumbnails/$manufacturer" . "_$model.jpg";
# print "<".$Dir."Thumbnails/$manufacturer" . "_$model.jpg\n";
	binmode(I);
	$s="";
	while(!eof(I)) { 
		$s=$s.sprintf("%02X",ord(getc(I))); 
	}

	if($s eq "") {
		print O "NULL,NULL,\n";
	} else {
		print O "    0x$s,\n";
		print O "    0x$s,\n";
	}

	print O "    0\n  )\n";
	print O "  set identity_insert dbo.Device off\n\n";
	print O "  set \@DId = \@DeviceId\n";
	print O "end\n";

}

close F;
close O;