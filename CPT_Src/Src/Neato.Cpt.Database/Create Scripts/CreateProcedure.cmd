@echo off
REM: Command File Created by Microsoft Visual Database Tools
REM: Date Generated: 20.06.2006
REM: Authentication type: SQL Server
REM: Usage: CommandFilename [Server] [Database] [Login] [Password]

if '%1' == '' goto usage
if '%2' == '' goto usage
if '%3' == '' goto usage

if '%1' == '/?' goto usage
if '%1' == '-?' goto usage
if '%1' == '?' goto usage
if '%1' == '/help' goto usage

osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrAnnouncementDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrAnnouncementEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrAnnouncementEnumLast.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrAnnouncementIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrAnnouncementUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrBrandGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrBuyMoreSkinsPageEnumByCulture.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrBuyMoreSkinsPageIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrBuyMoreSkinsPageUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarrierAddToRetailer.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarrierDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarrierGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarrierIconGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarrierIconUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarrierIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarrierRemoveFromRetailer.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarriersEnumerate.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCarrierUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerGetByEmail.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerSearch.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrCustomerUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceAddToCarrier.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceBrandDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceBrandEnumerate.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceBrandIconGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceBrandIconUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceBrandIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceBrandUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceEnumByPaper.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceEnumerate.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceIconBigGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceIconBigUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceIconGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceIconUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRemoveFromCarrier.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRequestDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRequestEmailsGetByDates.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRequestEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRequestEnumByCarrierPhoneManufacturerPhoneModel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRequestGetByDatesAndBrand.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRequestIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceRequestUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceTypeIconGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceTypesEnumerate.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDeviceUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDynamicLinkDelByPageId.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDynamicLinkDelByParam.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDynamicLinkEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDynamicLinkEnumByParam.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDynamicLinkIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrDynamicLinkUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrExitPageGet.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrExitPageIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrExitPageUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceBindToDevice.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceBindToPaper.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceLayoutEnumByPaperId.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceLocalizationDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceLocalizationIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceUnbindFromDevice.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceUnbindFromPaper.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaceUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaqDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaqEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaqIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrFaqUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrGoogleCodeGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrGoogleCodeUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrGoogleTrackPageGetByPageUrl.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrGoogleTrackPageIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrGoogleTrackPageUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrHelpPageDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrHelpPageEnumByCulture.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrHelpPageIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrHelpPageUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibFolderDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibFolderGetIdByCaption.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibFolderIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibFolderLocalizationUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibFolderSortOrderSwap.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibItemDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibItemEnumByFolderId.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibItemGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrImageLibItemIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrIPFilterDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrIPFilterEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrIPFilterIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrIPFilterUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrIsAnyDeviceForBrandAndRetailer.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrIsAnyDevicesForCarrierAndRetailer.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLastEditedPaperDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLastEditedPaperEnumByCustomer.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLastEditedPaperIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLastEditedPaperUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLogoDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLogoEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLogoIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrLogoUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrMailFormatEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrMailFormatGetByCulture.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrMailFormatUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrOverviewPageDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrOverviewPageEnumByCulture.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrOverviewPageIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrOverviewPageUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPageEnumByUrl.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPageIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperAddToRetailer.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandDel.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandIconGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandIconUpd.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandIns.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandsEnumByDeviceTypeId.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandsEnumerate.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperBrandUpd.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperEnumByParam.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperIconGetById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperIconUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperRemoveFromRetailer.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperToRetailerEnum.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperTypeEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPaperUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPlacePropertyDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPlacePropertyEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPlacePropertyIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrPlacePropertyUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailerBuyNowDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailerBuyNowEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailerBuyNowIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailersDel.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailersEnum.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailersGetByCarrierId.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailersGetByPaperId.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailersGetIconById.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailersIconUpd.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailersIns.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrRetailersUpd.PRC"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSpecialUserDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSpecialUserEnumByParam.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSpecialUserGetById.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSpecialUserIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSpecialUserUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSupportPageDel.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSupportPageEnumByCulture.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSupportPageIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrSupportPageUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTellAFriendRefusedUserGet.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTellAFriendRefusedUserIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTermsOfUsePageEnumByCulture.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTermsOfUsePageIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTermsOfUsePageUpd.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTestImageLibItemIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingDevicesEnumBrands.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingDevicesEnumByTimeAndBrand.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingDevicesIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingFeaturesEnumByTime.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingFeaturesIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingImagesEnumByTime.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingImagesIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingUserActionEnum.prc"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "Stored Procedures\dbo.PrTrackingUserActionIns.prc"
if %ERRORLEVEL% NEQ 0 goto errors

goto finish

REM: How to use screen
:usage
echo.
echo Usage: MyScript Server Database User [Password]
echo Server: the name of the target SQL Server
echo Database: the name of the target database
echo User: the login name on the target server
echo Password: the password for the login on the target server (optional)
echo.
echo Example: MyScript.cmd MainServer MainDatabase MyName MyPassword
echo.
echo.
goto done

REM: error handler
:errors
echo.
echo WARNING! Error(s) were detected!
echo --------------------------------
echo Please evaluate the situation and, if needed,
echo restart this command file. You may need to
echo supply command parameters when executing
echo this command file.
echo.
pause
goto done

REM: finished execution
:finish
echo.
echo Script execution is complete!
:done
@echo on
