BEGIN TRANSACTION
SET IDENTITY_INSERT [dbo].[PaperType] ON
INSERT INTO [dbo].[PaperType] ([Id], [Name]) VALUES (1, 'Die-Cut')
INSERT INTO [dbo].[PaperType] ([Id], [Name]) VALUES (2, 'Universal')
SET IDENTITY_INSERT [dbo].[PaperType] OFF
COMMIT TRANSACTION 