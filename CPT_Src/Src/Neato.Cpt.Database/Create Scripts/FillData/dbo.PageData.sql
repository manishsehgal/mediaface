SET IDENTITY_INSERT [dbo].[Page] ON
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description], [RetailerId]) VALUES (1, N'Default Menu Page', N'Default Menu Page', 1)
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description], [RetailerId]) VALUES (2, N'default.aspx', N'Home Page', 1)
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description], [RetailerId]) VALUES (3, N'overview.aspx', N'Overview Page', 1)
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description], [RetailerId]) VALUES (4, N'Support.aspx', N'Support Page', 1)
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description], [RetailerId]) VALUES (5, N'tellafriend.aspx', N'Tell a Friend Page', 1)
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description], [RetailerId]) VALUES (6, N'', N'CopyRight Label', 1)
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description], [RetailerId]) VALUES (7, N'mailto:feltech@neato.com', N'Contacts Page', 1)
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description], [RetailerId]) VALUES (8, N'BuyMoreSkins.aspx', N'Where can I buy more skins', 1)
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description], [RetailerId]) VALUES (9, N'TermsOfUse.aspx', N'Terms Of Use', 1)
INSERT INTO [dbo].[Page] ([PageId], [Url], [Description], [RetailerId]) VALUES (10, N'', N'Neato logo', 1)
SET IDENTITY_INSERT [dbo].[Page] OFF
