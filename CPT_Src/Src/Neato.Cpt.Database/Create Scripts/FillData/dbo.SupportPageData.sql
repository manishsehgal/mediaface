INSERT INTO SupportPage ([Culture], [HtmlText])
VALUES('', '<p>
At Fellowes-NEATO, one of our top goals is helping you -our customer- find the support and service that you need and deserve. 
Our extensive online support section provides a number of self-service options as well as several ways for you to get in 
touch with us.
</p>
<p>
<b><u>Contact Us</u></b>
<br><br>
Corporate Headquarters<br>
Fellowes, Inc.<br>
1789 Norwood Avenue<br>
Itasca, IL 60143<br>
1-630-893-1600<br>
</p>
<p>
<b>Customer Support</b>
<br>
For website, customer service and technical support, call us Monday through Friday between 7:30 AM and 5:00 PM CT, or send 
an email anytime!  
</p>
Call 1-888-312-7561 or email <a href=mailto:printz@neato.com>printz@neato.com</a>.')