INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1, N'', N'Front')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1, N'en', N'Front')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1, N'ru', N'�����')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (2, N'', N'Back')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (2, N'en', N'Back')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (2, N'ru', N'������')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (3, N'', N'Front')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (3, N'en', N'Front')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (3, N'ru', N'�����')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (4, N'', N'Back')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (4, N'en', N'Back')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (4, N'ru', N'������')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (5, N'', N'Front')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (5, N'en', N'Front')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (5, N'ru', N'�����')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (6, N'', N'Back')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (6, N'en', N'Back')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (6, N'ru', N'������')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (7, N'', N'Front')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (7, N'en', N'Front')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (7, N'ru', N'�����')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (8, N'', N'Back')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (8, N'en', N'Back')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (8, N'ru', N'������')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (9, N'', N'Front')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (9, N'en', N'Front')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (9, N'ru', N'�����')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (10, N'', N'Back')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (10, N'en', N'Back')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (10, N'ru', N'������')
GO
    
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1001, N'', N'Business Card Sleeve')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1001, N'en', N'Business Card Sleeve')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1001, N'ru', N'Business Card Sleeve')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1002, N'', N'Audio J Card (inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1002, N'en', N'Audio J Card (inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1002, N'ru', N'Audio J Card (inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1003, N'', N'Slim DVD Case Insert')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1003, N'en', N'Slim DVD Case Insert')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1003, N'ru', N'Slim DVD Case Insert')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1004, N'', N'DAT Label 2')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1004, N'en', N'DAT Label 2')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1004, N'ru', N'DAT Label 2')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1005, N'', N'VHS Video Sleeve')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1005, N'en', N'VHS Video Sleeve')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1005, N'ru', N'VHS Video Sleeve')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1006, N'', N'Floppy Disk Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1006, N'en', N'Floppy Disk Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1006, N'ru', N'Floppy Disk Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1007, N'', N'Super Jewel Box DVD Booklet (Inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1007, N'en', N'Super Jewel Box DVD Booklet (Inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1007, N'ru', N'Super Jewel Box DVD Booklet (Inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1008, N'', N'Audio J Card (outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1008, N'en', N'Audio J Card (outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1008, N'ru', N'Audio J Card (outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1009, N'', N'Super Jewel Box CD Booklet (Inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1009, N'en', N'Super Jewel Box CD Booklet (Inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1009, N'ru', N'Super Jewel Box CD Booklet (Inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1010, N'', N'CD Jewel Case Insert Card (outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1010, N'en', N'CD Jewel Case Insert Card (outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1010, N'ru', N'CD Jewel Case Insert Card (outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1011, N'', N'Zip Insert (outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1011, N'en', N'Zip Insert (outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1011, N'ru', N'Zip Insert (outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1012, N'', N'Super Disk Insert (outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1012, N'en', N'Super Disk Insert (outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1012, N'ru', N'Super Disk Insert (outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1013, N'', N'Business Card Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1013, N'en', N'Business Card Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1013, N'ru', N'Business Card Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1014, N'', N'Utility Label 3')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1014, N'en', N'Utility Label 3')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1014, N'ru', N'Utility Label 3')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1015, N'', N'Jewel Case Spine Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1015, N'en', N'Jewel Case Spine Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1015, N'ru', N'Jewel Case Spine Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1016, N'', N'Super Jewel Box CD Booklet (Outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1016, N'en', N'Super Jewel Box CD Booklet (Outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1016, N'ru', N'Super Jewel Box CD Booklet (Outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1017, N'', N'Audio Cassette Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1017, N'en', N'Audio Cassette Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1017, N'ru', N'Audio Cassette Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1018, N'', N'Super Jewel Box DVD Tray Liner (Inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1018, N'en', N'Super Jewel Box DVD Tray Liner (Inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1018, N'ru', N'Super Jewel Box DVD Tray Liner (Inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1019, N'', N'VHS Face Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1019, N'en', N'VHS Face Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1019, N'ru', N'VHS Face Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1020, N'', N'DAT Label 1')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1020, N'en', N'DAT Label 1')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1020, N'ru', N'DAT Label 1')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1021, N'', N'Zip Insert (inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1021, N'en', N'Zip Insert (inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1021, N'ru', N'Zip Insert (inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1022, N'', N'JAZ Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1022, N'en', N'JAZ Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1022, N'ru', N'JAZ Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1023, N'', N'Utility Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1023, N'en', N'Utility Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1023, N'ru', N'Utility Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1024, N'', N'Minidisk Label 2')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1024, N'en', N'Minidisk Label 2')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1024, N'ru', N'Minidisk Label 2')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1025, N'', N'Super Disk Insert (inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1025, N'en', N'Super Disk Insert (inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1025, N'ru', N'Super Disk Insert (inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1026, N'', N'Video Wrap (Insert)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1026, N'en', N'Video Wrap (Insert)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1026, N'ru', N'Video Wrap (Insert)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1027, N'', N'CD Print & File Pouch (non-adhesive)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1027, N'en', N'CD Print & File Pouch (non-adhesive)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1027, N'ru', N'CD Print & File Pouch (non-adhesive)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1028, N'', N'Minidisk Label 3')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1028, N'en', N'Minidisk Label 3')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1028, N'ru', N'Minidisk Label 3')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1029, N'', N'CD Jewel Case Tray Liner')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1029, N'en', N'CD Jewel Case Tray Liner')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1029, N'ru', N'CD Jewel Case Tray Liner')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1030, N'', N'Minidisk Label 1')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1030, N'en', N'Minidisk Label 1')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1030, N'ru', N'Minidisk Label 1')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1031, N'', N'Mini CD/DVD Case Insert')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1031, N'en', N'Mini CD/DVD Case Insert')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1031, N'ru', N'Mini CD/DVD Case Insert')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1032, N'', N'JAZ Insert')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1032, N'en', N'JAZ Insert')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1032, N'ru', N'JAZ Insert')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1033, N'', N'VHS Spine Labels')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1033, N'en', N'VHS Spine Labels')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1033, N'ru', N'VHS Spine Labels')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1034, N'', N'Anylabel')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1034, N'en', N'Anylabel')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1034, N'ru', N'Anylabel')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1035, N'', N'CD/DVD Label (3up)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1035, N'en', N'CD/DVD Label (3up)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1035, N'ru', N'CD/DVD Label (3up)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1036, N'', N'SlimLine Jewel Case Tray Liner (outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1036, N'en', N'SlimLine Jewel Case Tray Liner (outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1036, N'ru', N'SlimLine Jewel Case Tray Liner (outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1037, N'', N'Utility Label 2')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1037, N'en', N'Utility Label 2')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1037, N'ru', N'Utility Label 2')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1038, N'', N'CD Jewel Case Booklet')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1038, N'en', N'CD Jewel Case Booklet')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1038, N'ru', N'CD Jewel Case Booklet')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1039, N'', N'CD Print & File Pouch (adhesive)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1039, N'en', N'CD Print & File Pouch (adhesive)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1039, N'ru', N'CD Print & File Pouch (adhesive)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1040, N'', N'DAT J Card (inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1040, N'en', N'DAT J Card (inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1040, N'ru', N'DAT J Card (inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1041, N'', N'CD/DVD Full Coverage Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1041, N'en', N'CD/DVD Full Coverage Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1041, N'ru', N'CD/DVD Full Coverage Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1042, N'', N'Super Jewel Box CD Tray Liner (Inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1042, N'en', N'Super Jewel Box CD Tray Liner (Inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1042, N'ru', N'Super Jewel Box CD Tray Liner (Inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1043, N'', N'Mini CD Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1043, N'en', N'Mini CD Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1043, N'ru', N'Mini CD Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1044, N'', N'CD Jewel Case Insert Card (inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1044, N'en', N'CD Jewel Case Insert Card (inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1044, N'ru', N'CD Jewel Case Insert Card (inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1045, N'', N'CD/DVD Core Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1045, N'en', N'CD/DVD Core Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1045, N'ru', N'CD/DVD Core Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1046, N'', N'Adhesive Spine (CD Plus Sheets)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1046, N'en', N'Adhesive Spine (CD Plus Sheets)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1046, N'ru', N'Adhesive Spine (CD Plus Sheets)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1047, N'', N'Super Jewel Box CD Tray Liner (Outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1047, N'en', N'Super Jewel Box CD Tray Liner (Outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1047, N'ru', N'Super Jewel Box CD Tray Liner (Outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1048, N'', N'CD/DVD Label (2up)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1048, N'en', N'CD/DVD Label (2up)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1048, N'ru', N'CD/DVD Label (2up)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1049, N'', N'DAT J Card (outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1049, N'en', N'DAT J Card (outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1049, N'ru', N'DAT J Card (outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1050, N'', N'Super Jewel Box DVD Tray Liner (Outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1050, N'en', N'Super Jewel Box DVD Tray Liner (Outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1050, N'ru', N'Super Jewel Box DVD Tray Liner (Outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1051, N'', N'Super Jewel Box DVD Booklet (Outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1051, N'en', N'Super Jewel Box DVD Booklet (Outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1051, N'ru', N'Super Jewel Box DVD Booklet (Outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1052, N'', N'SlimLine Jewel Case Tray Liner (inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1052, N'en', N'SlimLine Jewel Case Tray Liner (inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1052, N'ru', N'SlimLine Jewel Case Tray Liner (inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1053, N'', N'HandiCD Sleeve')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1053, N'en', N'HandiCD Sleeve')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1053, N'ru', N'HandiCD Sleeve')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1054, N'', N'Super Disk Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1054, N'en', N'Super Disk Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1054, N'ru', N'Super Disk Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1055, N'', N'DVD Case Insert')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1055, N'en', N'DVD Case Insert')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1055, N'ru', N'DVD Case Insert')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1056, N'', N'DVD Case Booklet (inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1056, N'en', N'DVD Case Booklet (inside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1056, N'ru', N'DVD Case Booklet (inside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1057, N'', N'DVD Case Booklet (outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1057, N'en', N'DVD Case Booklet (outside)')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1057, N'ru', N'DVD Case Booklet (outside)')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1058, N'', N'HandiCD Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1058, N'en', N'HandiCD Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1058, N'ru', N'HandiCD Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1059, N'', N'Digital Vinyl CD Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1059, N'en', N'Digital Vinyl CD Label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1059, N'ru', N'Digital Vinyl CD Label')
GO

INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1060, N'', N'Zip label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1060, N'en', N'Zip label')
INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName) VALUES (1060, N'ru', N'Zip label')
GO

