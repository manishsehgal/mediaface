INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (1, '', N'Password Reminder From Neato', N'Hello %UserName:
Here is the password you requested. Click %NewPasswordLink to generate your new password. After, please use it with your email address to log in at Neato.')

INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (2, '', N'Tell-A-Friend From Neato', N'%MailText


%PrintzHomeLink

This email was sent from the tell-a-friend feature at the above mentioned site. To remove yourself from any further tell-a-friend emails, please click %UnsubscribeLink.')

INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (3, '', N'Thanks for your request!', N'Feeling creative?  We respect that. We�ll let you know ASAP when your device has been added to the site and your Printz� skin is ready to design.  Check your email soon!

Thanks again for your interest and feel free to email us with any questions.')

INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (4, '', N'Thanks for your feedback!', N'We hear you. We�ll log your comments and email back ASAP to answer your questions.
If you request changes to the site or product, we will notify you if we are able to accommodate your request.

Thanks again for your comments and email us any time!')

INSERT INTO [dbo].[MailFormat] ([MailId], [Culture], [Subject], [Body]) VALUES (5, '', N'Your device was added!', N'Your device %DeviceName has been added to the site and your Printz� skin is ready to design. 
%PrintzHomeLink')