SET IDENTITY_INSERT [dbo].[MailTypeVariable] ON
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (1, 1, 1)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (2, 1, 2)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (3, 2, 3)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (4, 2, 4)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (5, 2, 5)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (6, 5, 6)
INSERT INTO [dbo].[MailTypeVariable] ([Id], [MailId], [MailVariableId]) VALUES (7, 5, 4)
SET IDENTITY_INSERT [dbo].[MailTypeVariable] OFF
