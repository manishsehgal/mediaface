SET IDENTITY_INSERT [dbo].[FaceLayout] ON

--Moto RAZR - 2 faces * 3 layouts
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (6, 3)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (7, 4)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (8, 3)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (9, 4)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (10, 3)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (11, 4)

--Moto v180 - 2 faces * 3 layouts
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (12, 1)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (13, 2)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (14, 1)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (15, 2)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (16, 1)
INSERT INTO [dbo].[FaceLayout] ([Id], [FaceId]) VALUES (17, 2)

SET IDENTITY_INSERT [dbo].[FaceLayout] OFF
