SET IDENTITY_INSERT [dbo].[MailType] ON
INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (1, 'Forgot Password')
INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (2, 'Tell-a-Friend')
INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (3, 'Add my device')
INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (4, 'Tell us what you think')
INSERT INTO [dbo].[MailType] ([MailId], [Description]) VALUES (5, 'Device request notification')
SET IDENTITY_INSERT [dbo].[MailType] OFF