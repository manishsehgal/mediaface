SET IDENTITY_INSERT [dbo].[MailVariable] ON
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (1, '%UserName')
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (2, '%NewPasswordLink')
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (3, '%MailText')
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (4, '%PrintzHomeLink')
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (5, '%UnsubscribeLink')
INSERT INTO [dbo].[MailVariable] ([Id], [Name]) VALUES (6, '%DeviceName')
SET IDENTITY_INSERT [dbo].[MailVariable] OFF