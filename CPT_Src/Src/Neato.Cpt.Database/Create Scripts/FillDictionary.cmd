@echo off
REM: Command File Created by Microsoft Visual Database Tools
REM: Date Generated: 28.10.2005
REM: Authentication type: SQL Server
REM: Usage: CommandFilename [Server] [Database] [Login] [Password]

if '%1' == '' goto usage
if '%2' == '' goto usage
if '%3' == '' goto usage

if '%1' == '/?' goto usage
if '%1' == '-?' goto usage
if '%1' == '?' goto usage
if '%1' == '/help' goto usage

osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.RetailersData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.DeviceTypeData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.BrandData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.DeviceData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.FaceData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.FaceLocalizationData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.FaceToDeviceData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.FaqData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.CarrierData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.CarrierToRetailerData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.DeviceToCarrierData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.PaperTypeData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.PaperBrandData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.PaperData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.PaperToRetailerData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.FaceToPaperData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.PageData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.LinkData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.LinkLocalizationData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.LinkPlaceData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.MailTypeData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.MailFormatData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.MailVariableData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.MailTypeVariableData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.FaceLayoutData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.FaceLayoutItemData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.OverviewPageData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.SupportPageData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.GoogleCodeData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.BuyMoreSkinsPageData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.TermsOfUsePageData.sql"
if %ERRORLEVEL% NEQ 0 goto errors
osql -S %1 -d %2 -U %3 -P %4 -b -i "FillData\dbo.LogoData.sql"
if %ERRORLEVEL% NEQ 0 goto errors

goto finish

REM: How to use screen
:usage
echo.
echo Usage: MyScript Server Database User [Password]
echo Server: the name of the target SQL Server
echo Database: the name of the target database
echo User: the login name on the target server
echo Password: the password for the login on the target server (optional)
echo.
echo Example: MyScript.cmd MainServer MainDatabase MyName MyPassword
echo.
echo.
goto done

REM: error handler
:errors
echo.
echo WARNING! Error(s) were detected!
echo --------------------------------
echo Please evaluate the situation and, if needed,
echo restart this command file. You may need to
echo supply command parameters when executing
echo this command file.
echo.
pause
goto done

REM: finished execution
:finish
echo.
echo Script execution is complete!
:done
@echo on
