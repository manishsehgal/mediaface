SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPaperBrandUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrPaperBrandUpd]
GO


CREATE PROCEDURE dbo.PrPaperBrandUpd
(
    @PaperBrandId INT,
    @Name NVARCHAR(30)
)
AS
BEGIN
    UPDATE dbo.PaperBrand
    SET
        [Name] = @Name
    WHERE
        Id = @PaperBrandId
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrPaperBrandUpd]  TO [cpt_users]
GO

