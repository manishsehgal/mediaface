SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrFaceBindToDevice]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrFaceBindToDevice]
GO

CREATE  PROCEDURE dbo.PrFaceBindToDevice
(
    @FaceId INT,
    @DeviceId INT
)
AS
BEGIN
    INSERT INTO dbo.FaceToDevice
    (
        FaceId, 
        DeviceId
    )
    SELECT
        Inserted.FaceId, Inserted.DeviceId
    FROM (SELECT @FaceId AS FaceId, @DeviceId AS DeviceId) Inserted
        LEFT OUTER JOIN dbo.FaceToDevice
        ON Inserted.FaceId = FaceToDevice.FaceId AND Inserted.DeviceId = FaceToDevice.DeviceId
    WHERE FaceToDevice.FaceId IS NULL
    GROUP BY Inserted.FaceId, Inserted.DeviceId
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrFaceBindToDevice]  TO [cpt_users]
GO

  