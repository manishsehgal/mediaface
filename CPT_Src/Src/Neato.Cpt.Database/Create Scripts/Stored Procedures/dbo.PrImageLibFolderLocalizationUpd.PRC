SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrImageLibFolderLocalizationUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrImageLibFolderLocalizationUpd]
GO



CREATE PROCEDURE dbo.PrImageLibFolderLocalizationUpd
(
    @FolderId INT,
    @Caption NVARCHAR(255),
    @Culture NVARCHAR(10)
)
AS
BEGIN
    UPDATE dbo.ImageLibFolderLocalization
    SET
        [Caption] = @Caption
    WHERE
        [FolderId] = @FolderId AND [Culture] = @Culture
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrImageLibFolderLocalizationUpd]  TO [cpt_users]
GO

