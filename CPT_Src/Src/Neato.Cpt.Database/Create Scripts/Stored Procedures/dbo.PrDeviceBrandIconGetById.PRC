SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceBrandIconGetById]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceBrandIconGetById]
GO

CREATE PROCEDURE dbo.PrDeviceBrandIconGetById
    @DeviceBrandId INT
AS
SET NOCOUNT ON

SELECT Icon FROM dbo.DeviceBrand 
WHERE Id = @DeviceBrandId

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceBrandIconGetById]  TO [cpt_users]
GO

