SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrFaceIns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrFaceIns]
GO

CREATE  PROCEDURE dbo.PrFaceIns
(
    @Contour IMAGE = NULL,
    @FaceId INT OUTPUT
)
AS
BEGIN
    INSERT INTO dbo.Face (Contour) VALUES (@Contour)
    SET @FaceId = SCOPE_IDENTITY()
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrFaceIns]  TO [cpt_users]
GO

