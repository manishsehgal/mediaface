SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPaperIns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrPaperIns]
GO


CREATE PROCEDURE dbo.PrPaperIns
(
    @Name NVARCHAR(64), 
    @PaperBrandId INT,
    @Width REAL,
    @Height REAL,
    @PaperType INT,
    @PaperState NCHAR(1),
    @PaperId INT OUTPUT
)
AS
BEGIN
    INSERT INTO dbo.Paper
    (
        [Name],
        BrandId,
        Width,
        Height,
        PaperType,
        State
    )
    VALUES
    (
        @Name,
        @PaperBrandId,
        @Width,
        @Height,
        @PaperType,
        @PaperState
    )
    SET @PaperId = SCOPE_IDENTITY()
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrPaperIns]  TO [cpt_users]
GO

