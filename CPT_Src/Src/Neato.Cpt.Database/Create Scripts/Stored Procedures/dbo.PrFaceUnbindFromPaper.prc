SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrFaceUnbindFromPaper]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrFaceUnbindFromPaper]
GO

CREATE PROCEDURE dbo.PrFaceUnbindFromPaper
(
    @FaceId INT = NULL, 
    @PaperId INT = NULL
)
AS
BEGIN
    DELETE FROM dbo.FaceToPaper
    WHERE
        (@FaceId IS NULL OR FaceId = @FaceId)
        AND
        (@PaperId IS NULL OR PaperId = @PaperId)
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrFaceUnbindFromPaper]  TO [cpt_users]
GO

    