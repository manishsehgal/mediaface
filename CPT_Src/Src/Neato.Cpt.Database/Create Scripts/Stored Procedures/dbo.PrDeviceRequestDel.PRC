SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceRequestDel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceRequestDel]
GO



CREATE PROCEDURE dbo.PrDeviceRequestDel
(
	@Id INT
)
AS
DELETE FROM dbo.DeviceRequest
WHERE Id=@Id
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceRequestDel]  TO [cpt_users]
GO

