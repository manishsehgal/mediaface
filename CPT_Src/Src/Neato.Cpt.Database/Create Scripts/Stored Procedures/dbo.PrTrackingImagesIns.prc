SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTrackingImagesIns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrTrackingImagesIns]
GO
CREATE PROCEDURE dbo.PrTrackingImagesIns
(
    @Id int output,
    @ImageId int,
    @Login varchar(200),
    @Time datetime,
    @RetailerId int
)
AS
    INSERT INTO dbo.Tracking_Images ([ImageId],[User], [Time],[RetailerId]) VALUES (@ImageId,@Login, @Time,@RetailerId)
    SET @Id = SCOPE_IDENTITY()
RETURN 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrTrackingImagesIns]  TO [cpt_users]
GO

