SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrFaceBindToPaper]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrFaceBindToPaper]
GO


CREATE  PROCEDURE dbo.PrFaceBindToPaper
(
    @FaceId INT,
    @PaperId INT,
    @FaceX REAL,
    @FaceY REAL,
    @Rotation REAL
)
AS
BEGIN
    INSERT INTO dbo.FaceToPaper
    (
        FaceId, 
        PaperId,
        FaceX,
        FaceY,
        Rotation
    )
    SELECT
        Inserted.FaceId, Inserted.PaperId, @FaceX, @FaceY, @Rotation
    FROM (SELECT @FaceId AS FaceId, @PaperId AS PaperId) Inserted
        LEFT OUTER JOIN dbo.FaceToPaper
        ON Inserted.FaceId = FaceToPaper.FaceId AND Inserted.PaperId = FaceToPaper.PaperId
    WHERE FaceToPaper.FaceId IS NULL
    GROUP BY Inserted.FaceId, Inserted.PaperId
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrFaceBindToPaper]  TO [cpt_users]
GO

