SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceIconBigUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceIconBigUpd]
GO

CREATE PROCEDURE dbo.PrDeviceIconBigUpd
    @DeviceId INT,
    @Icon image
AS

UPDATE dbo.Device SET IconBig = @Icon WHERE Id = @DeviceId

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceIconBigUpd]  TO [cpt_users]
GO

   