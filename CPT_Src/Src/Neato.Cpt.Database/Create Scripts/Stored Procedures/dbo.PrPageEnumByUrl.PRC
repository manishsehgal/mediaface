SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPageEnumByUrl]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrPageEnumByUrl]
GO

CREATE  PROCEDURE dbo.PrPageEnumByUrl
(
    @RetailerId INT,
    @Url NVARCHAR(200)
)
AS
BEGIN
    SELECT
        PageId,
        Url,
        Description
    FROM Page
    WHERE
        Url = @Url AND RetailerId = @RetailerId
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrPageEnumByUrl]  TO [cpt_users]
GO

