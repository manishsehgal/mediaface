SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrImageLibItemEnumByFolderId]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrImageLibItemEnumByFolderId]
GO

CREATE PROCEDURE dbo.PrImageLibItemEnumByFolderId
	(
		@FolderId INT
	)
AS

SELECT     ImageLibItem.ItemId
FROM         ImageLibFolderItem INNER JOIN
                      ImageLibItem ON ImageLibFolderItem.ItemId = ImageLibItem.ItemId
WHERE     (ImageLibFolderItem.FolderId = @FolderId)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrImageLibItemEnumByFolderId]  TO [cpt_users]
GO

