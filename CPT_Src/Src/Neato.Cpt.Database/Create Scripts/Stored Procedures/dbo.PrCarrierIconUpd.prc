SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrCarrierIconUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrCarrierIconUpd]
GO

CREATE PROCEDURE dbo.PrCarrierIconUpd
    @CarrierId INT,
    @Icon image
AS

UPDATE dbo.Carrier SET Icon = @Icon WHERE Id = @CarrierId

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrCarrierIconUpd]  TO [cpt_users]
GO

 