SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPaperBrandDel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrPaperBrandDel]
GO

CREATE  PROCEDURE dbo.PrPaperBrandDel
(
    @PaperBrandId INT
)
AS
DELETE FROM dbo.PaperBrand WHERE Id = @PaperBrandId
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrPaperBrandDel]  TO [cpt_users]
GO

