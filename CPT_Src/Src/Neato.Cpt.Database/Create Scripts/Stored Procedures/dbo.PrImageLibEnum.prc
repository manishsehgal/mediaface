SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrImageLibEnum]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrImageLibEnum]
GO




CREATE PROCEDURE dbo.PrImageLibEnum
(
@RetailerId INT
)
AS

CREATE TABLE #FolderIdLinks
(
FolderId INT,
ItemId INT
)

INSERT INTO #FolderIdLinks
SELECT 
        dbo.ImageLibFolderItem.FolderId,
        dbo.ImageLibFolderItem.ItemId 
FROM 
    dbo.ImageLibFolderItem
    INNER JOIN dbo.ImageLibFolder ON
    dbo.ImageLibFolderItem.FolderId = dbo.ImageLibFolder.FolderId 
WHERE 
    dbo.ImageLibFolder.RetailerId = @RetailerId



SELECT
    FolderId,
    ParentFolderId,
    SortOrder
FROM dbo.ImageLibFolder
WHERE RetailerId = @RetailerId
ORDER BY SortOrder

SELECT DISTINCT
    dbo.ImageLibFolderLocalization.FolderId,
    dbo.ImageLibFolderLocalization.Culture,
    dbo.ImageLibFolderLocalization.Caption
FROM dbo.ImageLibFolderLocalization
RIGHT JOIN dbo.ImageLibFolder
ON dbo.ImageLibFolder.FolderId = dbo.ImageLibFolderLocalization.FolderId
WHERE dbo.ImageLibFolder.RetailerId = @RetailerId

SELECT
    dbo.ImageLibItem.ItemId
FROM dbo.ImageLibItem
INNER JOIN #FolderIdLinks
ON #FolderIdLinks.ItemId = dbo.ImageLibItem.ItemId

SELECT 
    dbo.ImageLibItemKeyword.ItemId,
    dbo.ImageLibItemKeyword.Culture,
    dbo.ImageLibItemKeyword.Keyword
FROM dbo.ImageLibItemKeyword
INNER JOIN #FolderIdLinks
ON #FolderIdLinks.ItemId = dbo.ImageLibItemKeyword.ItemId

SELECT 
    dbo.ImageLibFolderItem.FolderId,
    dbo.ImageLibFolderItem.ItemId
FROM dbo.ImageLibFolderItem
INNER JOIN dbo.ImageLibFolder
ON dbo.ImageLibFolderItem.FolderId = dbo.ImageLibFolder.FolderId
WHERE dbo.ImageLibFolder.RetailerId = @RetailerId

RETURN


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrImageLibEnum]  TO [cpt_users]
GO

