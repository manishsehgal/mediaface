SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceEnumerate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceEnumerate]
GO


CREATE PROCEDURE dbo.PrDeviceEnumerate
    @DeviceTypeId INT = NULL,
    @BrandId      INT = NULL,
    @CarrierId    INT = NULL,
    @FaceId       INT = NULL
AS
SET NOCOUNT ON

SELECT 
    Device.Id,
    Device.Model,
    Device.BrandId,
    Device.DeviceTypeId,
    Device.SortOrder
FROM
    dbo.Device
    LEFT OUTER JOIN dbo.DeviceToCarrier
    ON DeviceToCarrier.DeviceId = Device.Id
    LEFT OUTER JOIN dbo.FaceToDevice
    ON FaceToDevice.DeviceId = Device.Id
WHERE
    (@DeviceTypeId IS NULL OR Device.DeviceTypeId = @DeviceTypeId)
    AND
    (@BrandId IS NULL OR Device.BrandId = @BrandId)
    AND
    (@CarrierId IS NULL OR DeviceToCarrier.CarrierId = @CarrierId)
    AND
    (@FaceId IS NULL OR FaceToDevice.FaceId = @FaceId)
GROUP BY 
    Device.SortOrder,
    Device.Model,
    Device.BrandId,
    Device.DeviceTypeId,
    Device.Id
ORDER BY Device.SortOrder DESC, Device.Model

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceEnumerate]  TO [cpt_users]
GO

