SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrCarrierAddToRetailer]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrCarrierAddToRetailer]
GO


CREATE PROCEDURE dbo.PrCarrierAddToRetailer
(
	@CarrierId INT,
	@RetailerId INT
)
AS
BEGIN
    INSERT INTO dbo.CarrierToRetailer
    (
        [CarrierId], [RetailerId]
    )
    VALUES
    (
        @CarrierId, @RetailerId
    )
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrCarrierAddToRetailer]  TO [cpt_users]
GO

