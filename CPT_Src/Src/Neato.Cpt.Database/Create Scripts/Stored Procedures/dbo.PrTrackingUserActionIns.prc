SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTrackingUserActionIns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrTrackingUserActionIns]
GO
CREATE PROCEDURE dbo.PrTrackingUserActionIns
(
    @Id int output,
    @Action varchar(200),
    @Login varchar(200),
    @Time datetime,
    @RetailerId int
)
AS
    INSERT INTO dbo.Tracking_UserAction (Action, Login, [Time],[RetailerId]) VALUES (@Action, @Login, @Time,@RetailerId)
    SET @Id = SCOPE_IDENTITY()
RETURN 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrTrackingUserActionIns]  TO [cpt_users]
GO

