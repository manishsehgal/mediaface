SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrSpecialUserGetById]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrSpecialUserGetById]
GO

CREATE PROCEDURE dbo.PrSpecialUserGetById
(
    @SpecialUserId int
)
AS
SELECT
    Id,
	Email,
	ShowPaper,
	NotifyPaperState
FROM
    dbo.SpecialUser
WHERE
    SpecialUser.Id = @SpecialUserId

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
GRANT  EXECUTE  ON [dbo].[PrSpecialUserGetById]  TO [cpt_users]
GO

