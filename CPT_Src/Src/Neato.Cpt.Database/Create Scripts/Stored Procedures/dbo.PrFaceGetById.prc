SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrFaceGetById]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrFaceGetById]
GO


CREATE PROCEDURE dbo.PrFaceGetById
    @FaceId INT
AS
BEGIN
    SELECT
        Id,
        Contour
    FROM dbo.Face
    WHERE
        Id = @FaceId
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrFaceGetById]  TO [cpt_users]
GO

 