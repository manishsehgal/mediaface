SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceRequestEmailsGetByDates]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceRequestEmailsGetByDates]
GO



CREATE PROCEDURE dbo.PrDeviceRequestEmailsGetByDates
	(
        @StartDate DATETIME,
        @EndDate DATETIME,
	    @RetailerId int
	)
AS

CREATE TABLE #Data
(
Email NVARCHAR (50),
[Model name] NVARCHAR (50),
Retailer NVARCHAR (50),
RetailerDisplayName NVARCHAR (50) NULL
)

INSERT INTO #Data
SELECT  dbo.DeviceRequest.EmailAddress AS Email,
        dbo.DeviceRequest.DeviceBrand + ' ' +
        dbo.DeviceRequest.Device AS [Model name],
        dbo.Retailers.Name AS Retailer,
        dbo.Retailers.DisplayName AS RetailerDisplayName
FROM    dbo.DeviceRequest
INNER JOIN dbo.Retailers
ON dbo.Retailers.Id = dbo.DeviceRequest.RetailerId
WHERE   
        (dbo.DeviceRequest.Created >= @StartDate AND
        dbo.DeviceRequest.Created <= @EndDate) AND
        (dbo.DeviceRequest.RetailerId = @RetailerId OR @RetailerId IS NULL)
ORDER BY dbo.DeviceRequest.DeviceBrand, dbo.DeviceRequest.Device

UPDATE #Data
SET Retailer = RetailerDisplayName
WHERE RetailerDisplayName IS NOT NULL AND RetailerDisplayName <> ''

SELECT Email, [Model name], Retailer FROM #Data
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceRequestEmailsGetByDates]  TO [cpt_users]
GO

