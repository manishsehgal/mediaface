SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrIPFilterIns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrIPFilterIns]
GO




CREATE PROCEDURE [dbo].[PrIPFilterIns]
(
	 @BeginField1 tinyint
	,@BeginField2 tinyint
	,@BeginField3 tinyint
	,@BeginField4 tinyint
	,@EndField1 tinyint
	,@EndField2 tinyint
	,@EndField3 tinyint
	,@EndField4 tinyint
	,@Description varchar(100)
	,@Id int output
)
AS
BEGIN
	INSERT INTO [dbo].[IPFilter]
	(
		 [BeginField1]
		,[BeginField2]
		,[BeginField3]
		,[BeginField4]
		,[EndField1]
		,[EndField2]
		,[EndField3]
		,[EndField4]
		,[Description]
	)
	VALUES
	(
		 @BeginField1
		,@BeginField2
		,@BeginField3
		,@BeginField4
		,@EndField1
		,@EndField2
		,@EndField3
		,@EndField4
		,@Description
	)
    SET @Id = SCOPE_IDENTITY()
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrIPFilterIns]  TO [cpt_users]
GO

