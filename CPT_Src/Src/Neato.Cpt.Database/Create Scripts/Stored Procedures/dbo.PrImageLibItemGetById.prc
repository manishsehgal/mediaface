SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrImageLibItemGetById]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrImageLibItemGetById]
GO

CREATE PROCEDURE dbo.PrImageLibItemGetById
(
    @ItemId int
)
AS
SELECT TOP 1 ItemId, Stream FROM dbo.ImageLibItem WHERE ItemId = @ItemId
RETURN 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrImageLibItemGetById]  TO [cpt_users]
GO

