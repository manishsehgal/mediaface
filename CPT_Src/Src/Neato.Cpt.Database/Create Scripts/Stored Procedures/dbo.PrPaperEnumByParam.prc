SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPaperEnumByParam]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrPaperEnumByParam]
GO


CREATE PROCEDURE dbo.PrPaperEnumByParam
(
    @PaperBrandId int = NULL,
    @DeviceTypeId int = NULL,
    @DeviceId int = NULL,
    @PaperName NVARCHAR(64) = NULL,
    @RetailerId int = NULL,
    @PaperState NCHAR(16) = NULL
)
AS
    SELECT DISTINCT
	    Paper.Id,
	    Paper.[Name],
	    Paper.BrandId,
	    Paper.Width,
	    Paper.Height,
        Paper.PaperType,
        Paper.State
    FROM dbo.Paper
        LEFT OUTER JOIN dbo.FaceToPaper ON FaceToPaper.PaperId = Paper.Id
            LEFT OUTER JOIN dbo.FaceToDevice ON FaceToDevice.FaceId = FaceToPaper.FaceId
                LEFT OUTER JOIN dbo.Device ON Device.Id = FaceToDevice.DeviceId
		    LEFT JOIN dbo.PaperToRetailer ON Paper.Id = PaperToRetailer.PaperId
    WHERE
        (@PaperBrandId IS NULL OR Paper.BrandId = @PaperBrandId)
        AND
        (@DeviceTypeId IS NULL OR Device.DeviceTypeId = @DeviceTypeId)
        AND
        (@DeviceId IS NULL OR FaceToDevice.DeviceId = @DeviceId)
        AND
        (@PaperName IS NULL OR Paper.[Name] LIKE '%' + @PaperName + '%')
		AND 
		 (@RetailerId IS NULL OR PaperToRetailer.RetailerId = @RetailerId)
        AND
        (
--            (@PaperState IS NULL OR Paper.State = @PaperState)
        	(@PaperState IS NULL AND Paper.State = 'a')
	        OR
	        (
	            @PaperState IS NOT NULL AND
		        (
		            (CHARINDEX('a',@PaperState)>0 AND Paper.State = 'a')
		            OR
		            (CHARINDEX('i',@PaperState)>0 AND Paper.State = 'i')
		            OR
		            (CHARINDEX('t',@PaperState)>0 AND Paper.State = 't')
		        )
	        )
        )
    ORDER BY Paper.[Name]
RETURN

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrPaperEnumByParam]  TO [cpt_users]
GO

