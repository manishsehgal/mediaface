SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceDel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceDel]
GO

CREATE  PROCEDURE dbo.PrDeviceDel
(
    @DeviceId INT
)
AS
DELETE FROM dbo.Device WHERE Id = @DeviceId

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceDel]  TO [cpt_users]
GO
  