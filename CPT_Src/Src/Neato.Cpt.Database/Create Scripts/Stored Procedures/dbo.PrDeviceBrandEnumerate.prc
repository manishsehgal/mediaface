SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceBrandEnumerate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceBrandEnumerate]
GO

CREATE PROCEDURE dbo.PrDeviceBrandEnumerate
    @CarrierId INT = NULL,
    @DeviceTypeId INT = NULL,
    @PaperState NCHAR(16) = NULL
AS
SET NOCOUNT ON


IF (@PaperState IS NULL)
BEGIN

SELECT
    DeviceBrand.Id,
    DeviceBrand.Name
FROM
    dbo.DeviceBrand
    LEFT OUTER JOIN dbo.Device
    ON DeviceBrand.Id = Device.BrandId
    LEFT OUTER JOIN DeviceToCarrier
    ON Device.Id = DeviceToCarrier.DeviceId
WHERE
    (@DeviceTypeId IS NULL OR Device.DeviceTypeId = @DeviceTypeId)
    AND
    (@CarrierId IS NULL OR DeviceToCarrier.CarrierId = @CarrierId)
GROUP BY DeviceBrand.Id, DeviceBrand.Name
ORDER BY DeviceBrand.Name

END
ELSE
BEGIN

SELECT
    DeviceBrand.Id,
    DeviceBrand.Name
FROM
    dbo.DeviceBrand
    LEFT OUTER JOIN dbo.Device
    ON DeviceBrand.Id = Device.BrandId
    LEFT OUTER JOIN dbo.DeviceToCarrier
    ON Device.Id = DeviceToCarrier.DeviceId
    LEFT OUTER JOIN dbo.FaceToDevice
    ON FaceToDevice.DeviceId = Device.Id
    LEFT OUTER JOIN dbo.Face
    ON Face.Id = FaceToDevice.FaceId
    LEFT OUTER JOIN dbo.FaceToPaper
    ON FaceToPaper.FaceId = Face.Id
    LEFT OUTER JOIN dbo.Paper
    ON Paper.Id = FaceToPaper.PaperId
WHERE
    (@DeviceTypeId IS NULL OR Device.DeviceTypeId = @DeviceTypeId)
    AND
    (@CarrierId IS NULL OR DeviceToCarrier.CarrierId = @CarrierId)
    AND
    (
        (CHARINDEX('a',@PaperState)>0 AND Paper.State = 'a')
	    OR
	    (CHARINDEX('i',@PaperState)>0 AND Paper.State = 'i')
	    OR
	    (CHARINDEX('t',@PaperState)>0 AND Paper.State = 't')
    )
GROUP BY DeviceBrand.Id, DeviceBrand.Name
ORDER BY DeviceBrand.Name

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceBrandEnumerate]  TO [cpt_users]
GO

