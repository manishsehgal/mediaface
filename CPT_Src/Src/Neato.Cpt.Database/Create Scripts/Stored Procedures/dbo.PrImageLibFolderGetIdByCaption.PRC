SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrImageLibFolderGetIdByCaption]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrImageLibFolderGetIdByCaption]
GO

CREATE PROCEDURE dbo.PrImageLibFolderGetIdByCaption
(
    @Culture NVARCHAR(10),
    @Caption NVARCHAR(255),
    @RetailerId int
)
AS

SELECT     ImageLibFolderLocalization.FolderId
FROM         ImageLibFolderLocalization, ImageLibFolder
WHERE     (ImageLibFolderLocalization.Culture = @Culture) AND (ImageLibFolderLocalization.Caption = @Caption)
	AND ImageLibFolder.RetailerId = @RetailerId 
	AND ImageLibFolder.FolderId = ImageLibFolderLocalization.FolderId


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrImageLibFolderGetIdByCaption]  TO [cpt_users]
GO

