SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrCustomerDel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrCustomerDel]
GO

CREATE  PROCEDURE dbo.PrCustomerDel
(
    @CustomerId INT
)
AS
DELETE FROM dbo.Customer WHERE CustomerId = @CustomerId
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrCustomerDel]  TO [cpt_users]
GO

