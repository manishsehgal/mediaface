SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceTypesEnumerate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceTypesEnumerate]
GO


CREATE PROCEDURE dbo.PrDeviceTypesEnumerate
AS
SET NOCOUNT ON

SELECT 
    Id,
    Name
FROM dbo.DeviceType
ORDER BY Id

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceTypesEnumerate]  TO [cpt_users]
GO

