SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceBrandDel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceBrandDel]
GO


CREATE  PROCEDURE dbo.PrDeviceBrandDel
(
    @DeviceBrandId INT
)
AS
DELETE FROM dbo.DeviceBrand WHERE Id = @DeviceBrandId

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceBrandDel]  TO [cpt_users]
GO
 