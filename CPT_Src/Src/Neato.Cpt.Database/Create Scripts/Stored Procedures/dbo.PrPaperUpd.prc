SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPaperUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrPaperUpd]
GO


CREATE PROCEDURE dbo.PrPaperUpd
(
    @PaperId INT,
    @Name NVARCHAR(64), 
    @PaperBrandId INT,
    @Width REAL,
    @Height REAL,
    @PaperType INT,
    @PaperState NCHAR(1)
)
AS
BEGIN
    UPDATE dbo.Paper
    SET
        [Name] = @Name,
        BrandId = @PaperBrandId,
        Width = @Width,
        Height = @Height,
        PaperType = @PaperType,
        State = @PaperState
    WHERE
        Id = @PaperId
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrPaperUpd]  TO [cpt_users]
GO

