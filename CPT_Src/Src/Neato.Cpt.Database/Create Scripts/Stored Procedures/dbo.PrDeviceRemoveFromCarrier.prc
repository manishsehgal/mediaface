SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceRemoveFromCarrier]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceRemoveFromCarrier]
GO

CREATE PROCEDURE dbo.PrDeviceRemoveFromCarrier
(
    @DeviceId INT = NULL, 
    @CarrierId INT = NULL
)
AS
BEGIN
    DELETE FROM dbo.DeviceToCarrier
    WHERE
        (@DeviceId IS NULL OR DeviceId = @DeviceId)
        AND
        (@CarrierId IS NULL OR CarrierId = @CarrierId)
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceRemoveFromCarrier]  TO [cpt_users]
GO

   