SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTrackingUserActionEnum]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrTrackingUserActionEnum]
GO
CREATE PROCEDURE dbo.PrTrackingUserActionEnum
(
@RetailerId int
)
AS
    SELECT [Time], Login, Action FROM dbo.Tracking_UserAction
    WHERE (RetailerId = @RetailerId OR @RetailerId IS NULL)
    ORDER BY [Time] DESC 
RETURN 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrTrackingUserActionEnum]  TO [cpt_users]
GO

