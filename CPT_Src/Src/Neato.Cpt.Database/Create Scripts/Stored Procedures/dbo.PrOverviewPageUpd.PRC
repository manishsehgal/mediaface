SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrOverviewPageUpd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrOverviewPageUpd]
GO



CREATE PROCEDURE dbo.PrOverviewPageUpd
(
    @Culture NVARCHAR(10),
    @HtmlText TEXT,
    @RetailerId int
)
AS
BEGIN
    UPDATE dbo.OverviewPage
    SET
        HtmlText = @HtmlText
    WHERE
        Culture = @Culture
	AND
	RetailerId = @RetailerId
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrOverviewPageUpd]  TO [cpt_users]
GO

