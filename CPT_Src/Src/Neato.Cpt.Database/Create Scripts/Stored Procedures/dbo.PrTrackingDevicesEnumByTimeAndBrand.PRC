SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrTrackingDevicesEnumByTimeAndBrand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrTrackingDevicesEnumByTimeAndBrand]
GO

CREATE PROCEDURE dbo.PrTrackingDevicesEnumByTimeAndBrand
(
    @DeviceBrand varchar(200)=NULL,
    @TimeStart datetime ,
    @TimeEnd datetime,
    @RetailerId int
  
)
AS
begin
    SELECT [DeviceBrand],[Device], COUNT(Device) as Count
    FROM dbo.Tracking_Devices
    WHERE (
	        (@TimeEnd IS NULL OR Time <= @TimeEnd) AND
	        (@TimeStart IS NULL OR Time >= @TimeStart) AND
		(@DeviceBrand IS NULL OR DeviceBrand = @DeviceBrand )
		AND (RetailerId = @RetailerId OR @RetailerId IS NULL)
	  )
    GROUP by [Device],[DeviceBrand]
    Order by Count DESC
end
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrTrackingDevicesEnumByTimeAndBrand]  TO [cpt_users]
GO

