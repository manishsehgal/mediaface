SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrPlacePropertyDel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrPlacePropertyDel]
GO

CREATE PROCEDURE dbo.PrPlacePropertyDel
(
    @RetailerId INT,
    @Place varchar(50)
)
AS
DELETE 
    [dbo].[PlaceProperty]
WHERE	
    RetailerId = @RetailerId AND Place = @Place
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrPlacePropertyDel]  TO [cpt_users]
GO

