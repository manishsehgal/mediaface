 SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrFaceLocalizationIns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrFaceLocalizationIns]
GO

CREATE  PROCEDURE dbo.PrFaceLocalizationIns
(
    @FaceId INT,
    @Culture NVARCHAR(10),
    @FaceName NVARCHAR(255)
)
AS
BEGIN
    INSERT INTO dbo.FaceLocalization (FaceId, Culture, FaceName)
    SELECT
        Inserted.FaceId, Inserted.Culture, @FaceName
    FROM (SELECT @FaceId AS FaceId, @Culture AS Culture) Inserted
        LEFT OUTER JOIN dbo.FaceLocalization
        ON Inserted.FaceId = FaceLocalization.FaceId AND Inserted.Culture = FaceLocalization.Culture
    WHERE FaceLocalization.FaceId IS NULL
    GROUP BY Inserted.FaceId, Inserted.Culture
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrFaceLocalizationIns]  TO [cpt_users]
GO

