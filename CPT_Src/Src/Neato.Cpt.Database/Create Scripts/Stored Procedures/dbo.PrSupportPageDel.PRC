SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrSupportPageDel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrSupportPageDel]
GO

CREATE PROCEDURE dbo.PrSupportPageDel
(
    @Culture NVARCHAR(10),
    @RetailerId INT
)
AS
DELETE FROM dbo.SupportPage
WHERE Culture = @Culture
AND [RetailerId] = @RetailerId


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrSupportPageDel]  TO [cpt_users]
GO

