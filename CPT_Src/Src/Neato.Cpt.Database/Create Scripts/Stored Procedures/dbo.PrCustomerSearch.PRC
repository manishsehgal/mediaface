SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrCustomerSearch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrCustomerSearch]
GO





CREATE PROCEDURE dbo.PrCustomerSearch
(
    @Email VARCHAR(100) = NULL,
    @FirstName VARCHAR(100) = NULL,
    @LastName VARCHAR(100) = NULL,
    @StartDate DATETIME,
    @EndDate DATETIME,
    @RetailerId INT = NULL
 )
AS
BEGIN
    SELECT
        CustomerId,
        FirstName,
        LastName,
        Email,
        Password,
        pswuid,
        EmailOptions,
        ReceiveInfo,
        Created,
        RetailerId
    FROM
        dbo.Customer
    WHERE
        ((Email = @Email) OR
        (FirstName = @FirstName) OR       
        (LastName = @LastName) OR 
        (@Email IS NULL AND @FirstName IS NULL AND @LastName IS NULL))
        AND (Created >= @StartDate AND Created <= @EndDate)
        AND (RetailerId = @RetailerId OR @RetailerId IS NULL)
    ORDER BY Email, LastName, FirstName
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrCustomerSearch]  TO [cpt_users]
GO

