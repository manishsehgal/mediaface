SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrFaceUnbindFromDevice]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrFaceUnbindFromDevice]
GO

CREATE PROCEDURE dbo.PrFaceUnbindFromDevice
(
    @FaceId INT = NULL, 
    @DeviceId INT = NULL
)
AS
BEGIN
    DELETE FROM dbo.FaceToDevice
    WHERE
        (@FaceId IS NULL OR FaceId = @FaceId)
        AND
        (@DeviceId IS NULL OR DeviceId = @DeviceId)
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrFaceUnbindFromDevice]  TO [cpt_users]
GO

     