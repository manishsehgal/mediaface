SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PrDeviceIconGetById]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PrDeviceIconGetById]
GO

CREATE PROCEDURE dbo.PrDeviceIconGetById
    @DeviceId INT
AS
SET NOCOUNT ON

SELECT Icon FROM dbo.Device WHERE Id = @DeviceId

GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[PrDeviceIconGetById]  TO [cpt_users]
GO

