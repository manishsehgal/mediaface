<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" />

<xsl:param name="tag" />

<!-- Prepare data in unified format -->
    
<xsl:template match="/">
    <xsl:element name="metricdata">
        <xsl:element name="metric">
            <xsl:attribute name="id">loc</xsl:attribute>
            <xsl:element name="tag">
                <xsl:attribute name="name"><xsl:value-of select="$tag"/></xsl:attribute>
                <xsl:element name="module">
                    <xsl:attribute name="name"></xsl:attribute>
                    <xsl:apply-templates select="/locdata/stat[@language='LOC']"/>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:element>
</xsl:template>

<xsl:template match="/locdata/stat">
    <xsl:element name="counter">
        <xsl:attribute name="name">lines</xsl:attribute>
        <xsl:value-of select="@lines"/>
    </xsl:element>
    <xsl:element name="counter">
        <xsl:attribute name="name">files</xsl:attribute>
        <xsl:value-of select="@files"/>
    </xsl:element>
</xsl:template>

<!-- Adding totals (called from summary.totals.xslt) -->

<xsl:template match="/metricdata" mode="addtotals.loc">
    <xsl:element name="metric">
        <xsl:attribute name="id">loc</xsl:attribute>
        <xsl:element name="tag">
            <xsl:attribute name="name">Total</xsl:attribute>
            <xsl:element name="module">
                <xsl:attribute name="name"></xsl:attribute>
                <xsl:element name="counter">
                    <xsl:attribute name="name">lines</xsl:attribute>
                    <xsl:value-of select="sum(/metricdata/metric[@id='loc']/tag/module/counter[@name='lines'])"/>
                </xsl:element>
                <xsl:element name="counter">
                    <xsl:attribute name="name">files</xsl:attribute>
                    <xsl:value-of select="sum(/metricdata/metric[@id='loc']/tag/module/counter[@name='files'])"/>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:element>
</xsl:template>

<!-- Format HTML with results (called from summary.html.xslt) -->

<xsl:template match="/metricdata" mode="formathtml.loc">
    <xsl:apply-templates select="." mode="writeheader" >
        <xsl:with-param name="metric">loc</xsl:with-param>
        <xsl:with-param name="metricName">Lines of code</xsl:with-param>
    </xsl:apply-templates>
    
    <xsl:element name="table">
        <xsl:attribute name="class">rptdata</xsl:attribute>
        <xsl:element name="tr">
            <xsl:element name="th">Component</xsl:element>
            <xsl:element name="th">Files</xsl:element>
            <xsl:element name="th">Lines</xsl:element>
        </xsl:element>
        <xsl:apply-templates select="metric[@id='loc']/tag[@name != '']" mode="formathtml.loc" />
        <xsl:apply-templates select="metric[@id='loc']/tag[@name = '']" mode="formathtml.loc" />
    </xsl:element>
</xsl:template>
    
<xsl:template match="/metricdata/metric/tag" mode="formathtml.loc">
    <xsl:element name="tr">
        <xsl:if test="@name = 'Total'">
            <xsl:attribute name="class">totalrow</xsl:attribute>
        </xsl:if>

        <xsl:element name="td">
            <xsl:value-of select="@name" />
        </xsl:element>
        <xsl:element name="td">
            <xsl:value-of select="sum(module/counter[@name='files'])" />
        </xsl:element>
        <xsl:element name="td">
            <xsl:value-of select="sum(module/counter[@name='lines'])" />
        </xsl:element>
        
    </xsl:element>
</xsl:template>

</xsl:stylesheet>
