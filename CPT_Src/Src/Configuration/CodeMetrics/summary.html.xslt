<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="format.loc.xslt" />
<xsl:import href="format.unittest.xslt" />
<xsl:import href="format.coverage.xslt" />
<xsl:import href="format.complexity.xslt" />
<xsl:import href="format.fxcop.xslt" />

<xsl:output method="html" encoding="UTF-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" />

<xsl:template match="/">
    <xsl:element name="html">
        <xsl:element name="head">
<xsl:element name="style">
body
{
    font-family: Arial, Sans-Serif;
    font-size: 0.9em;
    color: black;
}

.summary 
{
    color: black;
    border: 1, solid, red;
    border-collapse: collapse;
}

.summary td
{
    border: none 0 black;
    border-collapse: collapse;
    padding-right: 0.5em;
}

.rptdata 
{
    border-collapse: collapse;
    margin: 0.1em;
}

div.caption
{
    font-size: 1.1em;
    text-align: left;
    font-weight: bold;
    padding-top: 0.1em;
    padding-left: 0.2em;
    padding-bottom: 0.1em;
    color: darkblue;
}

.rptdata th
{
    background-color: lemonchiffon;
}

.rptdata th, .rptdata td
{
    border: solid 1px gray;
    color: black;
    padding-left: 0.6em;
    padding-right: 0.6em;
    padding-top: 0.2em;
    padding-bottom: 0.2em;
    text-align: left;
    font-weight: bold;
}

.rptdata tr.totalrow
{
    background-color: lightcyan;
}

td.good
{
    color: green;
}

td.warning
{
    color: goldenrod;
}

td.bad
{
    color: red;
}
</xsl:element>
        <xsl:element name="title">
            <xsl:value-of select="/metricdata/@project" />
            <xsl:text> project metrics</xsl:text>
        </xsl:element>

        </xsl:element>
        <xsl:element name="body">
            <xsl:element name="h3">
                <xsl:text>The metrics report for the </xsl:text>
                <xsl:element name="strong"><xsl:value-of select="/metricdata/@project" /></xsl:element>
                <xsl:text> project:</xsl:text>
            </xsl:element>
            
            <xsl:element name="table">
                <xsl:attribute name="class">summary</xsl:attribute>
                <xsl:element name="tr">
                    <xsl:element name="td">Application version:</xsl:element>
                    <xsl:element name="td"><xsl:value-of select="/metricdata/@appversion" /></xsl:element>
                </xsl:element>
                <xsl:element name="tr">
                    <xsl:element name="td">Metrics process started:</xsl:element>
                    <xsl:element name="td"><xsl:value-of select="/metricdata/@metricsstarted" /></xsl:element>
                </xsl:element>
                <xsl:element name="tr">
                    <xsl:element name="td">Metrics process finished:</xsl:element>
                    <xsl:element name="td"><xsl:value-of select="/metricdata/@metricsfinished" /></xsl:element>
                </xsl:element>
            </xsl:element>
            <xsl:element name="br" />
            
            <xsl:apply-templates select="/metricdata" mode="formathtml.loc" />
            <xsl:element name="br" />
            <xsl:apply-templates select="/metricdata" mode="formathtml.unittest" />
            <xsl:element name="br" />
            <xsl:apply-templates select="/metricdata" mode="formathtml.coverage" />
            <xsl:element name="br" />
            <xsl:apply-templates select="/metricdata" mode="formathtml.complexity" />
            <xsl:element name="br" />
            <xsl:apply-templates select="/metricdata" mode="formathtml.fxcop" />
        
        </xsl:element>
    </xsl:element>
    
</xsl:template>

<xsl:template match="/metricdata" mode="writeheader">
    <xsl:param name="metric"/>
    <xsl:param name="metricName"/>
    <xsl:if test="count(/metricdata/metric[@id=$metric])=0">
        <xsl:element name="h3">
            <xsl:attribute name="class">bad</xsl:attribute>
            <xsl:text>No metric data for </xsl:text>
            <xsl:value-of select="$metric"></xsl:value-of>
            <xsl:text>!</xsl:text>
        </xsl:element>
    </xsl:if>
    
    <xsl:element name="div">
        <xsl:attribute name="class">caption</xsl:attribute>
        <xsl:value-of select="$metricName"></xsl:value-of>
    </xsl:element>
    
</xsl:template>

</xsl:stylesheet>