<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://star-sw.com/metrics/functions"
    >
    
<xsl:output method="xml" />

<xsl:param name="tag" />
    
<!-- Prepare data in unified format -->
    
<msxsl:script language="JScript" implements-prefix="user">
    function extractNUnitModuleName(iterator) {
        iterator.MoveNext();
        var parts = String(iterator.Current).split('\\');
        var projectFile = parts[parts.length - 1];
        var lastDotPosition = projectFile.lastIndexOf('.');
        return (lastDotPosition == -1) ? projectFile : projectFile.substr(0, lastDotPosition);
    }
</msxsl:script>

<xsl:template match="/">
    <xsl:element name="metricdata">
        <xsl:element name="metric">
            <xsl:attribute name="id">unittest</xsl:attribute>
            <xsl:element name="tag">
                <xsl:attribute name="name"><xsl:value-of select="$tag"/></xsl:attribute>
                <xsl:apply-templates select="/test-results"/>
            </xsl:element>
        </xsl:element>
    </xsl:element>
</xsl:template>

<xsl:template match="/test-results">
    <xsl:element name="module">
        <xsl:attribute name="name"><xsl:value-of select="user:extractNUnitModuleName(@name)"/></xsl:attribute>
        <xsl:element name="counter">
            <xsl:attribute name="name">total</xsl:attribute>
            <xsl:value-of select="@total"/>
        </xsl:element>
        <xsl:element name="counter">
            <xsl:attribute name="name">failures</xsl:attribute>
            <xsl:value-of select="@failures"/>
        </xsl:element>
        <xsl:element name="counter">
            <xsl:attribute name="name">not-run</xsl:attribute>
            <xsl:value-of select="@not-run"/>
        </xsl:element>
        <xsl:element name="counter">
            <xsl:attribute name="name">datetime</xsl:attribute>
            <xsl:value-of select="@date"/>
        </xsl:element>
        <xsl:element name="counter">
            <xsl:attribute name="name">run-time</xsl:attribute>
            <xsl:value-of select="test-suite/@time"/>
        </xsl:element>
    </xsl:element>
</xsl:template>

<!-- Adding totals (called from summary.totals.xslt) -->

<xsl:template match="/metricdata" mode="addtotals.unittest">
    <xsl:element name="metric">
        <xsl:attribute name="id">unittest</xsl:attribute>
        <xsl:element name="tag">
            <xsl:attribute name="name"></xsl:attribute>
            <xsl:element name="module">
                <xsl:attribute name="name">Total</xsl:attribute>
                <xsl:element name="counter">
                    <xsl:attribute name="name">total</xsl:attribute>
                    <xsl:value-of select="sum(/metricdata/metric[@id='unittest']/tag/module/counter[@name='total'])"/>
                </xsl:element>
                <xsl:element name="counter">
                    <xsl:attribute name="name">failures</xsl:attribute>
                    <xsl:value-of select="sum(/metricdata/metric[@id='unittest']/tag/module/counter[@name='failures'])"/>
                </xsl:element>
                <xsl:element name="counter">
                    <xsl:attribute name="name">not-run</xsl:attribute>
                    <xsl:value-of select="sum(/metricdata/metric[@id='unittest']/tag/module/counter[@name='not-run'])"/>
                </xsl:element>
                <xsl:element name="counter">
                    <xsl:attribute name="name">datetime</xsl:attribute>
                    <xsl:text />
                </xsl:element>
                <xsl:element name="counter">
                    <xsl:attribute name="name">run-time</xsl:attribute>
                    <xsl:text />
                </xsl:element>                
            </xsl:element>
        </xsl:element>
    </xsl:element>
</xsl:template>

<!-- Format HTML with results (called from summary.html.xslt) -->


<xsl:template match="/metricdata" mode="formathtml.unittest">
    <xsl:apply-templates select="." mode="writeheader" >
        <xsl:with-param name="metric">unittest</xsl:with-param>
        <xsl:with-param name="metricName">Unit test results</xsl:with-param>
    </xsl:apply-templates>

    <xsl:element name="table">
        <xsl:attribute name="class">rptdata</xsl:attribute>
        <xsl:element name="tr">
            <xsl:element name="th">Project</xsl:element>
            <xsl:element name="th">Total tests</xsl:element>
            <xsl:element name="th">Failures</xsl:element>
            <xsl:element name="th">Tests not run</xsl:element>
            <xsl:element name="th">Start time</xsl:element>
            <xsl:element name="th">Run time</xsl:element>
        </xsl:element>
        <xsl:apply-templates select="metric[@id='unittest']/tag/module[@name != '']" mode="formathtml.unittest" />
        <xsl:apply-templates select="metric[@id='unittest']/tag/module[@name = '']" mode="formathtml.unittest" />
    </xsl:element>
</xsl:template>

<xsl:template match="/metricdata/metric/tag/module" mode="formathtml.unittest">
    <xsl:element name="tr">
        <xsl:if test="@name = 'Total'">
            <xsl:attribute name="class">totalrow</xsl:attribute>
        </xsl:if>

        <xsl:element name="td">
            <xsl:value-of select="@name" />
        </xsl:element>
        <xsl:element name="td"><xsl:value-of select="counter[@name='total']" /></xsl:element>
        
        <xsl:element name="td">
            <xsl:attribute name="class">
            <xsl:choose>
                <xsl:when test="0 = number(counter[@name='failures'])">good</xsl:when>
                <xsl:otherwise>bad</xsl:otherwise>
            </xsl:choose>
            </xsl:attribute>
            <xsl:value-of select="counter[@name='failures']" />
        </xsl:element>
        
        <xsl:element name="td">
            <xsl:attribute name="class">
            <xsl:choose>
                <xsl:when test="0 = number(counter[@name='not-run'])">good</xsl:when>
                <xsl:otherwise>warning</xsl:otherwise>
            </xsl:choose>
            </xsl:attribute>
            <xsl:value-of select="counter[@name='not-run']" />
        </xsl:element>
        
        <xsl:element name="td"><xsl:value-of select="counter[@name='datetime']" /></xsl:element>
        <xsl:element name="td"><xsl:value-of select="counter[@name='run-time']" /></xsl:element>
    </xsl:element>
</xsl:template>

</xsl:stylesheet>
