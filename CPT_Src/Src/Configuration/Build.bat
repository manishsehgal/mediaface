NAnt -t:net-1.1 -f:Build.nant -D:ini.file=Build.ini
@echo off
if NOT ERRORLEVEL 0 exit
if exist Neato.Cpt.Test.dll-results.html (
    start Neato.Cpt.Test.dll-results.html
) else (
    start logs\Compiler.log
)