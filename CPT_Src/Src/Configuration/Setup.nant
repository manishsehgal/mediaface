<?xml version="1.0" ?>
<project default="Finish">
    <!-- ini properties -->
    <fail unless="${property::exists('ini.file')}" message="ini.file property must be set" />
    <foreach item="Line" in="${ini.file}" delim="=" property="key,value" trim="Both">
        <ifnot test="${key == '' or string::starts-with(key, '[') or string::starts-with(key, '//')}">
            <property name="${key}" value="${value}" />
            <echo message="${key} = ${value}" if="false" />
        </ifnot>
    </foreach>
    <property name="app.root.path" value="${path::get-full-path(app.root.path)}" />
    <!-- ini properties -->

    <!-- SCC usage -->
    <property name="scc.used.flag" value="false" unless="${property::exists('scc.used.flag')}" />
    <if test="${scc.used.flag}">
        <p4info user="scc.user" client="scc.client" host="scc.host" root="scc.root" failonerror="false" />
        <echo message="SCC root not found. Probably, no perforce client installed" unless="${property::exists('scc.root')}" />
        <if test="${property::exists('scc.root')}">
            <echo message="SCC root = '${scc.root}'" />
            <ifnot test="${string::starts-with(string::to-lower(app.root.path), string::to-lower(scc.root))}" >
                <fail>Application path is '${app.root.path}', but SCC path is '${scc.root}'</fail>
            </ifnot>
        </if>
    </if>
    <property name="use.scc" value="${property::exists('scc.root') and scc.used.flag}" />
    <!-- SCC usage -->

    <!-- Create Database flag -->
    <property name="app.db.create.flag" value="false" unless="${property::exists('app.db.create.flag')}" />
    <!-- Create Database flag -->
    
    <property readonly="true" name="log.file" value="${path::combine(path::combine(app.root.path, setup.logs.path), setup.log.file)}" />

    <target name="Prepare">
        <mkdir dir="${path::get-directory-name(log.file)}" unless="${directory::exists(path::get-directory-name(log.file))}" />
        <delete file="${log.file}" if="${file::exists(log.file)}" />
        <record action="Start" autoflush="true" name="${log.file}" level="Verbose" />
        <echo message="Logging started." />
        <echo message="Setup log path: ${log.file}" />

		<property readonly="true" name="app.src.path"       value="${path::combine(app.root.path, 'Src')}" />
		<property readonly="true" name="app.bin.path"       value="${path::combine(app.root.path, 'Bin')}" />

        <echo message="Taking files from SCC for ${app.src.path}" if="${use.scc}" />
        <p4sync view="${app.src.path}/..." verbose="true" if="${use.scc}"/>
        <echo message="Taking files from SCC for ${app.bin.path}" if="${use.scc}" />
        <p4sync view="${app.bin.path}/..." verbose="true" if="${use.scc}"/>
    </target>

    <target name="CreateDesignerIISFolder" depends="Prepare">
        <echo message="Creating virtual folder '${app.webdesigner.virtualdir.name}' on ${app.webdesigner.virtualdir.server}" />
        <echo message="in folder ${path::combine(app.root.path, app.webdesigner.virtualdir.path)}" />
        <mkdir dir="${path::combine(app.root.path, app.webdesigner.virtualdir.path)}" unless="${directory::exists(path::combine(app.root.path, app.webdesigner.virtualdir.path))}" />
        <mkiisdir 
            iisserver="${app.webdesigner.virtualdir.server}"
            dirpath="${path::combine(app.root.path, app.webdesigner.virtualdir.path)}"
            vdirname="${app.webdesigner.virtualdir.name}"
            appfriendlyname="${app.webdesigner.virtualdir.name}"
            authanonymous="true"
            authbasic="false"
            authntlm="false"
                        
            accesssource="false"
            accesswrite="false"
            accessread="true"
            contentindexed="true"
            enabledirbrowsing="false"

            accessexecute="false"
            accessscript="true"
        />
    </target>

    <target name="CreateAdminIISFolder" depends="Prepare">
        <echo message="Creating virtual folder '${app.webadmin.virtualdir.name}' on ${app.webadmin.virtualdir.server}" />
        <echo message="in folder ${path::combine(app.root.path, app.webadmin.virtualdir.path)}" />
        <mkdir dir="${path::combine(app.root.path, app.webadmin.virtualdir.path)}" unless="${directory::exists(path::combine(app.root.path, app.webadmin.virtualdir.path))}" />
        <mkiisdir 
            iisserver="${app.webadmin.virtualdir.server}"
            dirpath="${path::combine(app.root.path, app.webadmin.virtualdir.path)}"
            vdirname="${app.webadmin.virtualdir.name}"
            appfriendlyname="${app.webadmin.virtualdir.name}"
            authanonymous="false"
            authbasic="false"
            authntlm="true"
       
            accesssource="false"
            accesswrite="false"
            accessread="true"
            contentindexed="true"
            enabledirbrowsing="false"

            accessexecute="false"
            accessscript="true"
        />
    </target>

    <target name="RegisterEventLog" depends="Prepare">
        <echo message="Applying .reg file ${path::combine(app.root.path, app.eventlog.reg.file)}" />
        <exec program="regedit.exe" >
            <arg value="/s" />
            <arg value="${path::combine(app.root.path, app.eventlog.reg.file)}" />
        </exec>
    </target>

    <target name="UpdateConfigFiles" depends="Prepare">
        <foreach item="File" property="config.file">
            <in>
                <items basedir="${path::combine(app.root.path, app.webdesigner.path)}" >
                    <include name="web.config" />
                </items>
            </in>
            <do>
                <property name="web.config.file" value="${config.file}" />
                <call target="AdjustConfig" />
            </do>
        </foreach>
        <foreach item="File" property="config.file">
            <in>
                <items basedir="${path::combine(app.root.path, app.webadmin.path)}" >
                    <include name="web.config" />
                </items>
            </in>
            <do>
                <property name="web.config.file" value="${config.file}" />
                <call target="AdjustConfig" />
            </do>
        </foreach>
        <foreach item="File" property="config.file">
            <in>
                <items basedir="${path::combine(app.root.path, app.fillimagelib.path)}" >
                    <include name="**/app.config" />
                    <include name="**/*.exe.config" />
                </items>
            </in>
            <do>
                <property name="web.config.file" value="${config.file}" />
                <call target="AdjustConfig" />
            </do>
        </foreach>
    </target>

    <target name="AdjustConfig"> 
        <echo message="Modify config file ${web.config.file}" />
        <p4edit view="${web.config.file}" if="${use.scc}" />
        <attrib file="${web.config.file}" readonly="false" />
        
        <!-- Write DB -->
        <property name="config.dbsettingslist" value="database,pwd,uid,server" />
        <foreach item="String" in="${config.dbsettingslist}" delim=" ," property="appsetting" trim="Both">
            <xmlpoke file="${web.config.file}" verbose="false"
                xpath="/configuration/dataConfiguration/xmlSerializerSection/def:enterpriseLibrary.databaseSettings/def:connectionStrings/def:connectionString[@name='CPT Connection']/def:parameters/def:parameter[@name='${appsetting}']/@value" 
                value="${property::get-value('app.db.' + appsetting)}" >
                <namespaces>
                    <namespace prefix="def" uri="http://www.microsoft.com/practices/enterpriselibrary/08-31-2004/data" />
                </namespaces>
            </xmlpoke>               
            <echo message="property ${appsetting} set to ${property::get-value('app.db.' + appsetting)}"/>
        </foreach>

        <xmlpoke file="${config.file}" verbose="false"
            xpath="/configuration/appSettings/add[@key='PublicDesignerUrl']/@value" value="${delivery.config.app.PublicDesignerUrl}" />
        <echo message="property PublicDesignerUrl set to ${delivery.config.app.PublicDesignerUrl}"/>

        <xmlpoke file="${config.file}" verbose="false"
            xpath="/configuration/appSettings/add[@key='IntranetDesignerUrl']/@value" value="${delivery.config.app.IntranetDesignerUrl}" />
        <echo message="property IntranetDesignerUrl set to ${delivery.config.app.IntranetDesignerUrl}"/>

        <xmlpoke file="${config.file}" verbose="false"
            xpath="/configuration/appSettings/add[@key='SmtpServer']/@value" value="${delivery.config.app.SmtpServer}" />
        <echo message="property SmtpServer set to ${delivery.config.app.SmtpServer}"/>

        <xmlpoke file="${config.file}" verbose="false"
            xpath="/configuration/appSettings/add[@key='SmtpLogin']/@value" value="${delivery.config.app.SmtpLogin}" />
        <echo message="property SmtpLogin set to ${delivery.config.app.SmtpLogin}"/>

        <xmlpoke file="${config.file}" verbose="false"
            xpath="/configuration/appSettings/add[@key='SmtpPassword']/@value" value="${delivery.config.app.SmtpPassword}" />
        <echo message="property SmtpPassword set to ${delivery.config.app.SmtpPassword}"/>

        <xmlpoke file="${config.file}" verbose="false"
            xpath="/configuration/appSettings/add[@key='MailFrom']/@value" value="${delivery.config.app.MailFrom}" />
        <echo message="property DesignerUrl set to ${delivery.config.app.MailFrom}"/>

        <xmlpoke file="${config.file}" verbose="false"
            xpath="/configuration/system.web/compilation/@debug" value="${delivery.config.compilation.debug}" />
        <echo message="property compilation.debug set to ${delivery.config.compilation.debug}"/>

        <xmlpoke file="${config.file}" verbose="false"
            xpath="/configuration/system.web/customErrors/@mode" value="${delivery.config.customErrors.mode}" />
        <echo message="property customErrors.mode set to ${delivery.config.customErrors.mode}"/>
    </target>

    <target name="CreateDB" if="${app.db.create.flag}" depends="Prepare"> 
        <echo message="Create database ${app.db.database}" />
        <exec program="Create.cmd" verbose="true" workingdir="${path::combine(app.root.path, app.dbproject.path)}" basedir="${path::combine(app.root.path, app.dbproject.path)}">
            <arg value="${app.db.server}" />
            <arg value="${app.db.database}" />
            <arg value="${app.db.adminlogin}" />
            <arg value="${app.db.adminpassword}" />
        </exec>
        
        
    </target>
    
    <target name="Finish" depends="CreateAdminIISFolder,CreateDesignerIISFolder,UpdateConfigFiles, CreateDB" />
</project>
